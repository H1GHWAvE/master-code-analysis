package com.chelpus.root.utils;

import android.support.v4.view.MotionEventCompat;
import com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.LogCollector;
import com.android.vending.billing.InAppBillingService.LUCK.LogOutputStream;
import com.android.vending.billing.InAppBillingService.LUCK.PatchesItem;
import com.android.vending.billing.InAppBillingService.LUCK.SearchItem;
import com.chelpus.Utils;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.nio.BufferUnderflowException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.zip.Adler32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.util.InternalZipConstants;
import org.json.JSONException;
import org.json.JSONObject;
import org.tukaani.xz.common.Util;

public class createapkcustom {
    static final int BUFFER = 2048;
    private static final int all = 4;
    public static String appdir = "/sdcard/";
    private static final int armeabi = 0;
    private static final int armeabiv7a = 1;
    private static final int beginTag = 0;
    public static ArrayList<File> classesFiles = new ArrayList();
    private static final int classesTag = 1;
    public static File crkapk = null;
    public static String dir = "/sdcard/";
    public static String dir2 = "/sdcard/";
    private static final int endTag = 4;
    private static final int fileInApkTag = 10;
    public static boolean goodResult = false;
    private static String group = BuildConfig.VERSION_NAME;
    private static final int libTagALL = 2;
    private static final int libTagARMEABI = 6;
    private static final int libTagARMEABIV7A = 7;
    private static final int libTagMIPS = 8;
    private static final int libTagx86 = 9;
    private static ArrayList<String> libs = new ArrayList();
    public static File localFile2 = null;
    public static boolean manualpatch = false;
    private static final int mips = 2;
    public static boolean multidex = false;
    public static boolean multilib_patch = false;
    public static String packageName = BuildConfig.VERSION_NAME;
    private static final int packageTag = 5;
    private static ArrayList<PatchesItem> pat = null;
    private static ArrayList<String> patchedLibs = new ArrayList();
    public static boolean patchteil = false;
    private static PrintStream print = null;
    public static String sddir = "/sdcard/";
    private static ArrayList<Byte> search = null;
    private static ArrayList<SearchItem> ser = null;
    public static int tag = 0;
    public static String tooldir = "/sdcard/";
    public static boolean unpack = false;
    private static final int x86 = 3;

    public static class Decompress {
        private String _location;
        private String _zipFile;

        public Decompress(String zipFile, String location) {
            this._zipFile = zipFile;
            this._location = location;
            _dirChecker(BuildConfig.VERSION_NAME);
        }

        public void unzip() {
            int i;
            try {
                FileInputStream fin = new FileInputStream(this._zipFile);
                ZipInputStream zin = new ZipInputStream(fin);
                while (true) {
                    ZipEntry ze = zin.getNextEntry();
                    if (ze == null) {
                        zin.close();
                        fin.close();
                        return;
                    } else if (ze.isDirectory()) {
                        _dirChecker(ze.getName());
                    } else if (ze.getName().endsWith(".so")) {
                        String[] tail = ze.getName().split("\\/+");
                        String data_dir = BuildConfig.VERSION_NAME;
                        for (i = createapkcustom.beginTag; i < tail.length - 1; i += createapkcustom.classesTag) {
                            if (!tail[i].equals(BuildConfig.VERSION_NAME)) {
                                data_dir = data_dir + InternalZipConstants.ZIP_FILE_SEPARATOR + tail[i];
                            }
                        }
                        _dirChecker(data_dir);
                        FileOutputStream fout = new FileOutputStream(this._location + ze.getName());
                        byte[] buffer = new byte[Util.BLOCK_HEADER_SIZE_MAX];
                        while (true) {
                            int length = zin.read(buffer);
                            if (length == -1) {
                                break;
                            }
                            fout.write(buffer, createapkcustom.beginTag, length);
                        }
                        zin.closeEntry();
                        fout.close();
                    }
                }
            } catch (Exception e) {
                createapkcustom.print.println("Decompressunzip " + e);
                try {
                    ZipFile zipFile = new ZipFile(this._zipFile);
                    List fileHeaderList = zipFile.getFileHeaders();
                    for (i = createapkcustom.beginTag; i < fileHeaderList.size(); i += createapkcustom.classesTag) {
                        FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
                        if (fileHeader.getFileName().endsWith(".so")) {
                            createapkcustom.print.println(fileHeader.getFileName());
                            zipFile.extractFile(fileHeader.getFileName(), this._location);
                        }
                    }
                } catch (ZipException e1) {
                    e1.printStackTrace();
                } catch (Exception e12) {
                    e12.printStackTrace();
                }
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public String unzip(String file) {
            try {
                FileInputStream fin = new FileInputStream(this._zipFile);
                ZipInputStream zin = new ZipInputStream(fin);
                while (true) {
                    ZipEntry ze = zin.getNextEntry();
                    if (ze == null) {
                        break;
                    } else if (ze.isDirectory()) {
                        _dirChecker(ze.getName());
                    } else {
                        if (file.startsWith(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                            file = file.replaceFirst(InternalZipConstants.ZIP_FILE_SEPARATOR, BuildConfig.VERSION_NAME);
                        }
                        if (ze.getName().equals(file)) {
                            break;
                        }
                    }
                }
                zin.close();
                fin.close();
            } catch (Exception e) {
                createapkcustom.print.println("Decompressunzip " + e);
                try {
                    ZipFile zipFile = new ZipFile(this._zipFile);
                    List fileHeaderList = zipFile.getFileHeaders();
                    for (int i = createapkcustom.beginTag; i < fileHeaderList.size(); i += createapkcustom.classesTag) {
                        FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
                        if (fileHeader.getFileName().equals(file)) {
                            createapkcustom.print.println(fileHeader.getFileName());
                            zipFile.extractFile(fileHeader.getFileName(), this._location);
                            return this._location + fileHeader.getFileName();
                        }
                    }
                } catch (ZipException e1) {
                    e1.printStackTrace();
                } catch (Exception e12) {
                    e12.printStackTrace();
                }
            }
            return BuildConfig.VERSION_NAME;
        }

        private void _dirChecker(String dir) {
            File f = new File(this._location + dir);
            if (f.isFile()) {
                f.delete();
            }
            if (!f.exists()) {
                f.mkdirs();
            }
        }
    }

    public static String main(String[] paramArrayOfString) {
        OutputStream logOutputStream = new LogOutputStream("System.out");
        print = new PrintStream(logOutputStream);
        print.println("SU Java-Code Running!");
        patchedLibs.clear();
        packageName = paramArrayOfString[beginTag];
        appdir = paramArrayOfString[mips];
        sddir = paramArrayOfString[x86];
        tooldir = paramArrayOfString[endTag];
        clearTemp();
        manualpatch = false;
        String finalText = BuildConfig.VERSION_NAME;
        String beginText = BuildConfig.VERSION_NAME;
        boolean error = false;
        boolean end = false;
        getClassesDex();
        try {
            InputStream fileInputStream = new FileInputStream(paramArrayOfString[classesTag]);
            BufferedReader br = new BufferedReader(new InputStreamReader(fileInputStream, "UTF-8"));
            String data = BuildConfig.VERSION_NAME;
            String[] txtdata = new String[1000];
            new String[classesTag][beginTag] = BuildConfig.VERSION_NAME;
            byte[] byteOrig = null;
            int[] mask = null;
            boolean result = true;
            boolean sumresult = true;
            boolean libr = false;
            boolean begin = false;
            boolean mark_search = false;
            boolean fileInApk = false;
            String value1 = BuildConfig.VERSION_NAME;
            String value2 = BuildConfig.VERSION_NAME;
            String value3 = BuildConfig.VERSION_NAME;
            pat = new ArrayList();
            ser = new ArrayList();
            search = new ArrayList();
            int r = beginTag;
            while (true) {
                data = br.readLine();
                if (data != null) {
                    String[] orhex;
                    int t;
                    String[] rephex;
                    int[] rep_mask;
                    byte[] byteReplace;
                    if (!data.equals(BuildConfig.VERSION_NAME)) {
                        data = Utils.apply_TAGS(data, packageName);
                    }
                    txtdata[r] = data;
                    if (begin && (txtdata[r].contains("[") || txtdata[r].contains("]") || txtdata[r].contains("{"))) {
                        print.println(BuildConfig.VERSION_NAME + beginText + LogCollector.LINE_SEPARATOR);
                        begin = false;
                    }
                    if (begin) {
                        beginText = beginText + LogCollector.LINE_SEPARATOR + txtdata[r];
                    }
                    if (txtdata[r].contains("[") && txtdata[r].contains("]")) {
                        Iterator it;
                        String lib;
                        switch (tag) {
                            case classesTag /*1*/:
                                if (pat.size() > 0) {
                                    getClassesDex();
                                    if (classesFiles != null && classesFiles.size() > 0) {
                                        if (classesFiles.size() > classesTag) {
                                            multidex = true;
                                        }
                                        it = classesFiles.iterator();
                                        while (it.hasNext()) {
                                            File cl = (File) it.next();
                                            localFile2 = cl;
                                            print.println("---------------------------------");
                                            print.println("Patch for " + cl.getName() + ":");
                                            print.println("---------------------------------\n");
                                            if (!manualpatch) {
                                                result = patchProcess(pat);
                                            }
                                            if (!result) {
                                                sumresult = false;
                                            }
                                        }
                                    }
                                    multidex = false;
                                    goodResult = false;
                                    ser.clear();
                                    pat.clear();
                                    tag = 200;
                                    break;
                                }
                                break;
                            case mips /*2*/:
                                it = libs.iterator();
                                while (it.hasNext()) {
                                    lib = (String) it.next();
                                    localFile2 = new File(lib);
                                    print.println("---------------------------");
                                    print.println("Patch for libraries \n" + localFile2.getPath().replace(sddir + "/tmp", BuildConfig.VERSION_NAME) + ":");
                                    print.println("---------------------------\n");
                                    if (!manualpatch) {
                                        result = patchProcess(pat);
                                    }
                                    if (!result) {
                                        sumresult = false;
                                    }
                                    patchedLibs.add(lib);
                                }
                                multilib_patch = false;
                                goodResult = false;
                                ser.clear();
                                pat.clear();
                                tag = 200;
                                break;
                            case libTagARMEABI /*6*/:
                                it = libs.iterator();
                                while (it.hasNext()) {
                                    lib = (String) it.next();
                                    localFile2 = new File(lib);
                                    print.println("--------------------------------");
                                    print.println("Patch for (armeabi) libraries \n" + localFile2.getPath().replace(sddir + "/tmp", BuildConfig.VERSION_NAME) + ":");
                                    print.println("--------------------------------\n");
                                    if (!manualpatch) {
                                        result = patchProcess(pat);
                                    }
                                    if (!result) {
                                        sumresult = false;
                                    }
                                    patchedLibs.add(lib);
                                }
                                multilib_patch = false;
                                goodResult = false;
                                ser.clear();
                                pat.clear();
                                tag = 200;
                                break;
                            case libTagARMEABIV7A /*7*/:
                                it = libs.iterator();
                                while (it.hasNext()) {
                                    lib = (String) it.next();
                                    localFile2 = new File(lib);
                                    print.println("---------------------------------------");
                                    print.println("Patch for (armeabi-v7a) libraries \n" + localFile2.getPath().replace(sddir + "/tmp", BuildConfig.VERSION_NAME) + ":");
                                    print.println("---------------------------------------\n");
                                    if (!manualpatch) {
                                        result = patchProcess(pat);
                                    }
                                    if (!result) {
                                        sumresult = false;
                                    }
                                    patchedLibs.add(lib);
                                }
                                multilib_patch = false;
                                goodResult = false;
                                ser.clear();
                                pat.clear();
                                tag = 200;
                                break;
                            case libTagMIPS /*8*/:
                                it = libs.iterator();
                                while (it.hasNext()) {
                                    lib = (String) it.next();
                                    localFile2 = new File(lib);
                                    print.println("---------------------------");
                                    print.println("Patch for (MIPS) libraries \n" + localFile2.getPath().replace(sddir + "/tmp", BuildConfig.VERSION_NAME) + ":");
                                    print.println("---------------------------\n");
                                    if (!manualpatch) {
                                        result = patchProcess(pat);
                                    }
                                    if (!result) {
                                        sumresult = false;
                                    }
                                    patchedLibs.add(lib);
                                }
                                multilib_patch = false;
                                goodResult = false;
                                ser.clear();
                                pat.clear();
                                tag = 200;
                                break;
                            case libTagx86 /*9*/:
                                it = libs.iterator();
                                while (it.hasNext()) {
                                    lib = (String) it.next();
                                    localFile2 = new File(lib);
                                    print.println("---------------------------");
                                    print.println("Patch for (x86) libraries \n" + localFile2.getPath().replace(sddir + "/tmp", BuildConfig.VERSION_NAME) + ":");
                                    print.println("---------------------------\n");
                                    if (!manualpatch) {
                                        result = patchProcess(pat);
                                    }
                                    if (!result) {
                                        sumresult = false;
                                    }
                                    patchedLibs.add(lib);
                                }
                                multilib_patch = false;
                                goodResult = false;
                                ser.clear();
                                pat.clear();
                                tag = 200;
                                break;
                            case fileInApkTag /*10*/:
                                print.println("---------------------------");
                                print.println("Patch for file from apk\n" + localFile2.getPath().replace(sddir + "/tmp", BuildConfig.VERSION_NAME) + ":");
                                print.println("---------------------------\n");
                                if (!manualpatch) {
                                    result = patchProcess(pat);
                                }
                                if (!result) {
                                    sumresult = false;
                                }
                                patchedLibs.add(localFile2.getAbsolutePath());
                                ser.clear();
                                pat.clear();
                                tag = 200;
                                break;
                        }
                    }
                    if (txtdata[r].contains("[BEGIN]")) {
                        tag = beginTag;
                        begin = true;
                    }
                    if (txtdata[r].contains("[CLASSES]") || txtdata[r].contains("[ODEX]")) {
                        tag = classesTag;
                    }
                    if (txtdata[r].contains("[PACKAGE]")) {
                        tag = packageTag;
                    }
                    if (libr) {
                        ser.clear();
                        pat.clear();
                        try {
                            value1 = new JSONObject(txtdata[r]).getString("name");
                        } catch (JSONException e) {
                            print.println("Error LP: Error name of libraries read!");
                        }
                        switch (tag) {
                            case mips /*2*/:
                                libs.clear();
                                libs = searchlib(endTag, value1);
                                break;
                            case libTagARMEABI /*6*/:
                                libs.clear();
                                libs = searchlib(beginTag, value1);
                                break;
                            case libTagARMEABIV7A /*7*/:
                                libs.clear();
                                libs = searchlib(classesTag, value1);
                                break;
                            case libTagMIPS /*8*/:
                                libs.clear();
                                libs = searchlib(mips, value1);
                                break;
                            case libTagx86 /*9*/:
                                libs.clear();
                                libs = searchlib(x86, value1);
                                break;
                        }
                        libr = false;
                    }
                    if (fileInApk) {
                        ser.clear();
                        pat.clear();
                        try {
                            value1 = new JSONObject(txtdata[r]).getString("name");
                        } catch (JSONException e2) {
                            print.println("Error LP: Error name of file from apk read!");
                        }
                        String file = getFileFromApk(value1);
                        if (new File(file).exists()) {
                            localFile2 = new File(file);
                        } else {
                            print.println("file for patch not found in apk.");
                        }
                        fileInApk = false;
                    }
                    if (txtdata[r].contains("[LIB-ARMEABI]")) {
                        tag = libTagARMEABI;
                        unpack = false;
                        fileInApk = false;
                        libr = true;
                    }
                    if (txtdata[r].contains("[LIB-ARMEABI-V7A]")) {
                        tag = libTagARMEABIV7A;
                        unpack = false;
                        fileInApk = false;
                        libr = true;
                    }
                    if (txtdata[r].contains("[LIB-MIPS]")) {
                        tag = libTagMIPS;
                        unpack = false;
                        fileInApk = false;
                        libr = true;
                    }
                    if (txtdata[r].contains("[LIB-X86]")) {
                        tag = libTagx86;
                        unpack = false;
                        fileInApk = false;
                        libr = true;
                    }
                    if (txtdata[r].contains("[LIB]")) {
                        tag = mips;
                        unpack = false;
                        fileInApk = false;
                        libr = true;
                    }
                    if (txtdata[r].contains("[FILE_IN_APK]")) {
                        tag = fileInApkTag;
                        unpack = false;
                        libr = false;
                        fileInApk = true;
                    }
                    if (txtdata[r].contains("group")) {
                        try {
                            group = new JSONObject(txtdata[r]).getString("group");
                        } catch (JSONException e3) {
                            print.println("Error LP: Error original hex read!");
                            group = BuildConfig.VERSION_NAME;
                        }
                    }
                    if (txtdata[r].contains("original")) {
                        if (mark_search) {
                            sumresult = searchProcess(ser);
                            mark_search = false;
                        }
                        try {
                            value1 = new JSONObject(txtdata[r]).getString("original");
                        } catch (JSONException e4) {
                            print.println("Error LP: Error original hex read!");
                        }
                        value1 = value1.trim();
                        orhex = new String[value1.split("[ \t]+").length];
                        orhex = value1.split("[ \t]+");
                        mask = new int[orhex.length];
                        byteOrig = new byte[orhex.length];
                        t = beginTag;
                        while (t < orhex.length) {
                            if (orhex[t].contains("*") && !orhex[t].contains("**")) {
                                error = true;
                                orhex[t] = "60";
                            }
                            if (orhex[t].contains("**") || orhex[t].matches("\\?+")) {
                                orhex[t] = "60";
                                mask[t] = classesTag;
                            } else {
                                try {
                                    mask[t] = beginTag;
                                } catch (Exception e5) {
                                    print.println(" " + e5);
                                }
                            }
                            if (orhex[t].contains("W") || orhex[t].contains("w") || orhex[t].contains("R") || orhex[t].contains(InternalZipConstants.READ_MODE)) {
                                mask[t] = Integer.valueOf(orhex[t].toLowerCase().replace("w", BuildConfig.VERSION_NAME).replace(InternalZipConstants.READ_MODE, BuildConfig.VERSION_NAME)).intValue() + mips;
                                orhex[t] = "60";
                            }
                            byteOrig[t] = Integer.valueOf(orhex[t], 16).byteValue();
                            t += classesTag;
                        }
                    }
                    if (txtdata[r].contains("\"object\"")) {
                        try {
                            value3 = new JSONObject(txtdata[r]).getString("object");
                        } catch (JSONException e6) {
                            print.println("Error LP: Error number by object!");
                        }
                        Process localProcess9 = Runtime.getRuntime().exec("dalvikvm -Xverify:none -Xdexopt:none -cp " + paramArrayOfString[packageTag] + " " + paramArrayOfString[libTagARMEABI] + ".createnerorunpatch " + paramArrayOfString[beginTag] + " " + "object" + value3 + " " + sddir + " " + tooldir + LogCollector.LINE_SEPARATOR);
                        localProcess9.waitFor();
                        DataInputStream dataInputStream = new DataInputStream(localProcess9.getInputStream());
                        byte[] arrayOfByte = new byte[dataInputStream.available()];
                        dataInputStream.read(arrayOfByte);
                        String str = new String(arrayOfByte);
                        localProcess9.destroy();
                        if (str.contains("Done")) {
                            print.println("Object patched!\n\n");
                            sumresult = true;
                        } else {
                            print.println("Object not found!\n\n");
                            sumresult = false;
                        }
                        fixadler(localFile2);
                        manualpatch = true;
                    }
                    if (txtdata[r].contains("search")) {
                        try {
                            value3 = new JSONObject(txtdata[r]).getString("search");
                        } catch (JSONException e7) {
                            print.println("Error LP: Error search hex read!");
                        }
                        value3 = value3.trim();
                        orhex = new String[value3.split("[ \t]+").length];
                        orhex = value3.split("[ \t]+");
                        mask = new int[orhex.length];
                        byteOrig = new byte[orhex.length];
                        t = beginTag;
                        while (t < orhex.length) {
                            try {
                                if (orhex[t].contains("*") && !orhex[t].contains("**")) {
                                    error = true;
                                    orhex[t] = "60";
                                }
                                if (orhex[t].contains("**") || orhex[t].matches("\\?+")) {
                                    orhex[t] = "60";
                                    mask[t] = classesTag;
                                } else {
                                    mask[t] = beginTag;
                                }
                                if (orhex[t].toUpperCase().contains("R")) {
                                    mask[t] = Integer.valueOf(orhex[t].replace("R", BuildConfig.VERSION_NAME)).intValue() + mips;
                                    orhex[t] = "60";
                                }
                                byteOrig[t] = Integer.valueOf(orhex[t], 16).byteValue();
                                t += classesTag;
                            } catch (Exception e52) {
                                print.println("search pattern read: " + e52);
                            }
                        }
                        if (error) {
                            result = false;
                            print.println("Error LP: Patterns to search not valid!\n");
                        } else {
                            mark_search = true;
                            try {
                                SearchItem searchItem = new SearchItem(byteOrig, mask);
                                searchItem.repByte = new byte[byteOrig.length];
                                ser.add(searchItem);
                            } catch (Exception e522) {
                                print.println(" " + e522);
                            }
                        }
                    }
                    if (txtdata[r].contains("replaced")) {
                        try {
                            value2 = new JSONObject(txtdata[r]).getString("replaced");
                        } catch (JSONException e8) {
                            print.println("Error LP: Error replaced hex read!");
                        }
                        value2 = value2.trim();
                        rephex = new String[value2.split("[ \t]+").length];
                        rephex = value2.split("[ \t]+");
                        rep_mask = new int[rephex.length];
                        byteReplace = new byte[rephex.length];
                        t = beginTag;
                        while (t < rephex.length) {
                            try {
                                if (rephex[t].contains("*") && !rephex[t].contains("**")) {
                                    error = true;
                                    rephex[t] = "60";
                                }
                                if (rephex[t].contains("**") || rephex[t].matches("\\?+")) {
                                    rephex[t] = "60";
                                    rep_mask[t] = beginTag;
                                } else {
                                    rep_mask[t] = classesTag;
                                }
                                if (rephex[t].toLowerCase().contains("sq")) {
                                    rephex[t] = "60";
                                    rep_mask[t] = 253;
                                }
                                if (rephex[t].contains("s1") || rephex[t].contains("S1")) {
                                    rephex[t] = "60";
                                    rep_mask[t] = 254;
                                }
                                if (rephex[t].contains("s0") || rephex[t].contains("S0")) {
                                    rephex[t] = "60";
                                    rep_mask[t] = MotionEventCompat.ACTION_MASK;
                                }
                                if (rephex[t].contains("W") || rephex[t].contains("w") || rephex[t].contains("R") || rephex[t].contains("R")) {
                                    rep_mask[t] = Integer.valueOf(rephex[t].toLowerCase().replace("w", BuildConfig.VERSION_NAME).replace(InternalZipConstants.READ_MODE, BuildConfig.VERSION_NAME)).intValue() + mips;
                                    rephex[t] = "60";
                                }
                                byteReplace[t] = Integer.valueOf(rephex[t], 16).byteValue();
                                t += classesTag;
                            } catch (Exception e5222) {
                                print.println(" " + e5222);
                            }
                        }
                        if (rep_mask.length != mask.length || byteOrig.length != byteReplace.length || byteReplace.length < endTag || byteOrig.length < endTag) {
                            error = true;
                        }
                        if (error) {
                            result = false;
                            print.println("Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!");
                        }
                        if (!error) {
                            pat.add(new PatchesItem(byteOrig, mask, byteReplace, rep_mask, group, false));
                            group = BuildConfig.VERSION_NAME;
                        }
                    }
                    if (txtdata[r].contains("insert")) {
                        try {
                            value2 = new JSONObject(txtdata[r]).getString("insert");
                        } catch (JSONException e9) {
                            Utils.sendFromRoot("Error LP: Error insert hex read!");
                        }
                        value2 = value2.trim();
                        rephex = new String[value2.split("[ \t]+").length];
                        rephex = value2.split("[ \t]+");
                        rep_mask = new int[rephex.length];
                        byteReplace = new byte[rephex.length];
                        t = beginTag;
                        while (t < rephex.length) {
                            if (rephex[t].contains("*") && !rephex[t].contains("**")) {
                                error = true;
                                rephex[t] = "60";
                            }
                            if (rephex[t].contains("**") || rephex[t].matches("\\?+")) {
                                rephex[t] = "60";
                                rep_mask[t] = beginTag;
                            } else {
                                try {
                                    rep_mask[t] = classesTag;
                                } catch (Exception e52222) {
                                    Utils.sendFromRoot(" " + e52222);
                                }
                            }
                            if (rephex[t].toLowerCase().contains("sq")) {
                                rephex[t] = "60";
                                rep_mask[t] = 253;
                            }
                            if (rephex[t].contains("s1") || rephex[t].contains("S1")) {
                                rephex[t] = "60";
                                rep_mask[t] = 254;
                            }
                            if (rephex[t].contains("s0") || rephex[t].contains("S0")) {
                                rephex[t] = "60";
                                rep_mask[t] = MotionEventCompat.ACTION_MASK;
                            }
                            if (rephex[t].contains("W") || rephex[t].contains("w") || rephex[t].contains("R") || rephex[t].contains("R")) {
                                rep_mask[t] = Integer.valueOf(rephex[t].toLowerCase().replace("w", BuildConfig.VERSION_NAME).replace(InternalZipConstants.READ_MODE, BuildConfig.VERSION_NAME)).intValue() + mips;
                                rephex[t] = "60";
                            }
                            byteReplace[t] = Integer.valueOf(rephex[t], 16).byteValue();
                            t += classesTag;
                        }
                        if (byteReplace.length < endTag || byteOrig.length < endTag) {
                            error = true;
                        }
                        if (error) {
                            result = false;
                            Utils.sendFromRoot("Error LP: Dimensions of the original hex-string and repleced must be >3.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!");
                        }
                        if (!error) {
                            if (multilib_patch || multidex) {
                                pat.add(new PatchesItem(byteOrig, mask, byteReplace, rep_mask, "all_lib", true));
                            } else {
                                pat.add(new PatchesItem(byteOrig, mask, byteReplace, rep_mask, group, true));
                            }
                            group = BuildConfig.VERSION_NAME;
                        }
                    }
                    if (txtdata[r].contains("replace_from_file")) {
                        try {
                            value2 = new JSONObject(txtdata[r]).getString("replace_from_file");
                        } catch (JSONException e10) {
                            print.println("Error LP: Error replaced hex read!");
                        }
                        value2 = value2.trim();
                        File arrayFile = new File(Utils.getDirs(new File(paramArrayOfString[classesTag])) + InternalZipConstants.ZIP_FILE_SEPARATOR + value2);
                        int len = (int) arrayFile.length();
                        byteReplace = new byte[len];
                        try {
                            do {
                            } while (new FileInputStream(arrayFile).read(byteReplace) > 0);
                        } catch (Exception e522222) {
                            e522222.printStackTrace();
                        }
                        rep_mask = new int[len];
                        Arrays.fill(rep_mask, classesTag);
                        if (error) {
                            result = false;
                            print.println("Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!");
                        }
                        if (!error) {
                            pat.add(new PatchesItem(byteOrig, mask, byteReplace, rep_mask, group, false));
                            group = BuildConfig.VERSION_NAME;
                        }
                    }
                    if (end) {
                        finalText = finalText + LogCollector.LINE_SEPARATOR + txtdata[r];
                    }
                    if (data.contains("[END]")) {
                        tag = endTag;
                        end = true;
                    }
                    r += classesTag;
                } else {
                    if (patchedLibs.size() > 0) {
                        zipLib(patchedLibs);
                    }
                    if (sumresult) {
                        print.println(BuildConfig.VERSION_NAME + finalText);
                    }
                    if (!sumresult) {
                        if (patchteil) {
                            print.println("Not all patterns are replaced, but the program can work, test it!\nCustom Patch not valid for this Version of the Programm or already patched. ");
                        } else {
                            print.println("Custom Patch not valid for this Version of the Programm or already patched. ");
                        }
                    }
                    clearTemp();
                    fileInputStream.close();
                    clearTemp();
                    result = logOutputStream.allresult;
                    try {
                        logOutputStream.close();
                    } catch (IOException e11) {
                        e11.printStackTrace();
                    }
                    return result;
                }
            }
        } catch (FileNotFoundException e12) {
            print.println("Custom Patch not Found!\n");
        } catch (IOException e22) {
            print.println("Patch process Error LP: " + e22);
        } catch (InterruptedException e32) {
            e32.printStackTrace();
        }
    }

    public static boolean patchProcess(java.util.ArrayList<com.android.vending.billing.InAppBillingService.LUCK.PatchesItem> r24) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxOverflowException: Regions stack size limit reached
	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:42)
	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:66)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:280)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:167)
*/
        /*
        r19 = 1;
        r3 = new java.io.RandomAccessFile;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = localFile2;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5 = "rw";	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3.<init>(r4, r5);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r2 = r3.getChannel();	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = java.nio.channels.FileChannel.MapMode.READ_WRITE;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = 0;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r6 = r2.size();	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r6 = (int) r6;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r6 = (long) r6;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r13 = r2.map(r3, r4, r6);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r24.toArray();	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r3.length;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r0 = new com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[r3];	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r20 = r0;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r24.size();	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r0 = new com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[r3];	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r20 = r0;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r0 = r24;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r1 = r20;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r0.toArray(r1);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = (com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[]) r3;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r0 = r3;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r0 = (com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[]) r0;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r20 = r0;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r11 = -1;
    L_0x003e:
        r3 = r13.hasRemaining();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r3 == 0) goto L_0x02ad;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x0044:
        r3 = r11 + 1;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r13.position(r3);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r11 = r13.position();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r10 = r13.get();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r14 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x0052:
        r0 = r20;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r0.length;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r14 >= r3) goto L_0x003e;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x0057:
        r13.position(r11);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.origByte;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r10 == r3) goto L_0x008e;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x0063:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.origMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 1;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r3 == r4) goto L_0x008e;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x006d:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.origMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 1;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r3 <= r4) goto L_0x0269;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x0077:
        r3 = search;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.origMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4[r5];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4 + -2;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.get(r4);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = (java.lang.Byte) r3;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.byteValue();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r10 != r3) goto L_0x0269;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x008e:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r3 != 0) goto L_0x009e;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x0097:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3[r4] = r10;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x009e:
        r3 = r20[r14];	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3[r4];	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r4 = 1;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        if (r3 <= r4) goto L_0x00cf;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
    L_0x00a8:
        r3 = r20[r14];	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3[r4];	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r4 = 253; // 0xfd float:3.55E-43 double:1.25E-321;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        if (r3 >= r4) goto L_0x00cf;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
    L_0x00b3:
        r3 = r20[r14];	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r4 = r3.repByte;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r5 = 0;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = search;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r6 = r20[r14];	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r6 = r6.repMask;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r7 = 0;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r6 = r6[r7];	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r6 = r6 + -2;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3.get(r6);	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = (java.lang.Byte) r3;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3.byteValue();	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r4[r5] = r3;	 Catch:{ Exception -> 0x026d, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
    L_0x00cf:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 253; // 0xfd float:3.55E-43 double:1.25E-321;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r3 != r4) goto L_0x00e9;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x00da:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = r10 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r6 = r10 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r6 = r6 * 16;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = r5 + r6;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = (byte) r5;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3[r4] = r5;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x00e9:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 254; // 0xfe float:3.56E-43 double:1.255E-321;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r3 != r4) goto L_0x0100;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x00f4:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = r10 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = r5 + 16;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = (byte) r5;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3[r4] = r5;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x0100:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 255; // 0xff float:3.57E-43 double:1.26E-321;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r3 != r4) goto L_0x0115;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x010b:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = r10 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = (byte) r5;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3[r4] = r5;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x0115:
        r15 = 1;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r11 + 1;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r13.position(r3);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r21 = r13.get();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x011f:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.origByte;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r15];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r0 = r21;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r0 == r3) goto L_0x0153;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x0129:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.origMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r15];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 1;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r3 <= r4) goto L_0x014a;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x0132:
        r3 = search;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.origMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4[r15];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4 + -2;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.get(r4);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = (java.lang.Byte) r3;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.byteValue();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r0 = r21;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r0 == r3) goto L_0x0153;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x014a:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.origMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r15];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 1;
        if (r3 != r4) goto L_0x0269;
    L_0x0153:
        r3 = r20[r14];	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3[r15];	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        if (r3 != 0) goto L_0x0161;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
    L_0x015b:
        r3 = r20[r14];	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3.repByte;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3[r15] = r21;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
    L_0x0161:
        r3 = r20[r14];	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3[r15];	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r4 = 1;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        if (r3 <= r4) goto L_0x0190;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
    L_0x016a:
        r3 = r20[r14];	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3[r15];	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r4 = 253; // 0xfd float:3.55E-43 double:1.25E-321;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        if (r3 >= r4) goto L_0x0190;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
    L_0x0174:
        r3 = r20[r14];	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3[r15];	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r23 = r3 + -2;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r20[r14];	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r4 = r3.repByte;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = search;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r0 = r23;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3.get(r0);	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = (java.lang.Byte) r3;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r3 = r3.byteValue();	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
        r4[r15] = r3;	 Catch:{ Exception -> 0x02f7, IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, FileNotFoundException -> 0x0405 }
    L_0x0190:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 253; // 0xfd float:3.55E-43 double:1.25E-321;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r3 != r4) goto L_0x01aa;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x019b:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = r10 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r6 = r10 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r6 = r6 * 16;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = r5 + r6;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = (byte) r5;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3[r4] = r5;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x01aa:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r15];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 254; // 0xfe float:3.56E-43 double:1.255E-321;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r3 != r4) goto L_0x01bf;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x01b4:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r21 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4 + 16;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = (byte) r4;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3[r15] = r4;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x01bf:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r15];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 255; // 0xff float:3.57E-43 double:1.26E-321;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r3 != r4) goto L_0x01d2;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x01c9:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r21 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = (byte) r4;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3[r15] = r4;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x01d2:
        r15 = r15 + 1;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.origByte;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.length;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r15 != r3) goto L_0x0331;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x01db:
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.insert;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        if (r3 == 0) goto L_0x0221;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x01e1:
        r18 = r13.position();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r2.size();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = (int) r3;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r16 = r3 - r18;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r0 = r16;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r8 = new byte[r0];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r0 = r16;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r13.get(r8, r3, r0);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r9 = java.nio.ByteBuffer.wrap(r8);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.length;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.origByte;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.length;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3 - r4;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3 + r18;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = (long) r3;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r2.position(r3);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r2.write(r9);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = java.nio.channels.FileChannel.MapMode.READ_WRITE;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r6 = r2.size();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r6 = (int) r6;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r6 = (long) r6;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r13 = r2.map(r3, r4, r6);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r0 = r18;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r13.position(r0);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x0221:
        r3 = (long) r11;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r2.position(r3);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r9 = java.nio.ByteBuffer.wrap(r3);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r2.write(r9);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r13.force();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = print;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = new java.lang.StringBuilder;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4.<init>();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = "\nPattern N";	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r5);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = r14 + 1;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r5);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = ": Patch done! \n(Offset: ";	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r5);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = java.lang.Integer.toHexString(r11);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r5);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = ")\n";	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r5);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.toString();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3.println(r4);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 1;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3.result = r4;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = 1;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        patchteil = r3;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x0269:
        r14 = r14 + 1;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        goto L_0x0052;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x026d:
        r12 = move-exception;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r23 = r3 + -2;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = print;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = new java.lang.StringBuilder;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4.<init>();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = "Byte N";	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r5);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r0 = r23;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r0);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = " not found! Please edit search pattern for byte ";	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r5);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r0 = r23;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r0);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = ".";	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r5);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.toString();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3.println(r4);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        goto L_0x00cf;
    L_0x02a5:
        r12 = move-exception;
        r3 = print;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = "Byte by search not found! Please edit pattern for search.\n";	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3.println(r4);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x02ad:
        r2.close();	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r14 = 0;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x02b1:
        r0 = r20;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r0.length;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r14 >= r3) goto L_0x040d;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x02b6:
        r3 = r20[r14];	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r3.result;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 != 0) goto L_0x03ec;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x02bc:
        r22 = 0;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r20[r14];	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r3.group;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = "";	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r3.equals(r4);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 != 0) goto L_0x0337;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x02ca:
        r23 = 0;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x02cc:
        r0 = r20;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r0.length;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r0 = r23;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r0 >= r3) goto L_0x0337;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x02d3:
        r3 = r20[r14];	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r3.group;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r20[r23];	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r4.group;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r3.equals(r4);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 == 0) goto L_0x02f4;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x02e1:
        r3 = r20[r23];	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r3.result;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 == 0) goto L_0x02f4;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x02e7:
        r3 = multidex;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 != 0) goto L_0x02ef;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x02eb:
        r3 = multilib_patch;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 == 0) goto L_0x02f2;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x02ef:
        r3 = 1;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        goodResult = r3;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x02f2:
        r22 = 1;
    L_0x02f4:
        r23 = r23 + 1;
        goto L_0x02cc;
    L_0x02f7:
        r12 = move-exception;
        r3 = r20[r14];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = r3[r15];	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r23 = r3 + -2;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3 = print;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = new java.lang.StringBuilder;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4.<init>();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = "Byte N";	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r5);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r0 = r23;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r0);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = " not found! Please edit search pattern for byte ";	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r5);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r0 = r23;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r0);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r5 = ".";	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.append(r5);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r4 = r4.toString();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        r3.println(r4);	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        goto L_0x0190;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x032e:
        r3 = move-exception;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        goto L_0x02ad;	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
    L_0x0331:
        r21 = r13.get();	 Catch:{ IndexOutOfBoundsException -> 0x02a5, BufferUnderflowException -> 0x032e, Exception -> 0x0439, FileNotFoundException -> 0x0405 }
        goto L_0x011f;
    L_0x0337:
        if (r22 != 0) goto L_0x037d;
    L_0x0339:
        r3 = goodResult;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 != 0) goto L_0x037d;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x033d:
        r3 = multidex;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 == 0) goto L_0x0381;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x0341:
        r3 = goodResult;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 != 0) goto L_0x037d;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x0345:
        r3 = localFile2;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = classesFiles;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5 = classesFiles;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5 = r5.size();	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5 = r5 + -1;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r4.get(r5);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r3.equals(r4);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 == 0) goto L_0x037d;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x035b:
        r3 = print;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = new java.lang.StringBuilder;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4.<init>();	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5 = "\nPattern N";	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r4.append(r5);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5 = r14 + 1;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r4.append(r5);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5 = ":\nError LP: Pattern not found!\nor patch is already applied?!\n";	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r4.append(r5);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r4.toString();	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3.println(r4);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r19 = 0;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x037d:
        r14 = r14 + 1;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        goto L_0x02b1;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x0381:
        r3 = multilib_patch;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 == 0) goto L_0x03c9;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x0385:
        r3 = goodResult;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 != 0) goto L_0x037d;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x0389:
        r4 = localFile2;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5 = new java.io.File;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = libs;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r6 = libs;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r6 = r6.size();	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r6 = r6 + -1;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r3.get(r6);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = (java.lang.String) r3;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5.<init>(r3);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r4.equals(r5);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 == 0) goto L_0x037d;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x03a6:
        r3 = print;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = new java.lang.StringBuilder;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4.<init>();	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5 = "\nPattern N";	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r4.append(r5);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5 = r14 + 1;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r4.append(r5);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5 = ":\nError LP: Pattern not found!\nor patch is already applied?!\n";	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r4.append(r5);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r4.toString();	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3.println(r4);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r19 = 0;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        goto L_0x037d;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x03c9:
        r3 = print;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = new java.lang.StringBuilder;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4.<init>();	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5 = "\nPattern N";	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r4.append(r5);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5 = r14 + 1;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r4.append(r5);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r5 = ":\nError LP: Pattern not found!\nor patch is already applied?!\n";	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r4.append(r5);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = r4.toString();	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3.println(r4);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r19 = 0;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        goto L_0x037d;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x03ec:
        r3 = multidex;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 != 0) goto L_0x03f4;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x03f0:
        r3 = multilib_patch;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 == 0) goto L_0x037d;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x03f4:
        r3 = r20[r14];	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r3.group;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r4 = "";	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        r3 = r3.equals(r4);	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        if (r3 != 0) goto L_0x037d;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
    L_0x0400:
        r3 = 1;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        goodResult = r3;	 Catch:{ FileNotFoundException -> 0x0405, BufferUnderflowException -> 0x0437, Exception -> 0x0419 }
        goto L_0x037d;
    L_0x0405:
        r17 = move-exception;
        r3 = print;
        r4 = "Error LP: Program files are not found!\nMove Program to internal storage.";
        r3.println(r4);
    L_0x040d:
        r3 = tag;
        r4 = 1;
        if (r3 != r4) goto L_0x0417;
    L_0x0412:
        r3 = localFile2;
        fixadler(r3);
    L_0x0417:
        r3 = 1;
        return r3;
    L_0x0419:
        r12 = move-exception;
        r3 = print;
        r4 = new java.lang.StringBuilder;
        r4.<init>();
        r5 = "Exception e";
        r4 = r4.append(r5);
        r5 = r12.toString();
        r4 = r4.append(r5);
        r4 = r4.toString();
        r3.println(r4);
        goto L_0x040d;
    L_0x0437:
        r3 = move-exception;
        goto L_0x040d;
    L_0x0439:
        r3 = move-exception;
        goto L_0x02ad;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chelpus.root.utils.createapkcustom.patchProcess(java.util.ArrayList):boolean");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean searchProcess(ArrayList<SearchItem> searchlist) {
        boolean patch = true;
        try {
            int g;
            FileChannel ChannelDex2 = new RandomAccessFile(localFile2, InternalZipConstants.WRITE_MODE).getChannel();
            MappedByteBuffer fileBytes2 = ChannelDex2.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex2.size()));
            SearchItem[] patches = new SearchItem[searchlist.toArray().length];
            patches = (SearchItem[]) searchlist.toArray(new SearchItem[searchlist.size()]);
            long j = 0;
            while (fileBytes2.hasRemaining()) {
                int curentPos = fileBytes2.position();
                byte curentByte = fileBytes2.get();
                g = beginTag;
                while (g < patches.length) {
                    fileBytes2.position(curentPos);
                    if (!patches[g].result && (curentByte == patches[g].origByte[beginTag] || patches[g].origMask[beginTag] != 0)) {
                        if (patches[g].origMask[beginTag] != 0) {
                            patches[g].repByte[beginTag] = curentByte;
                        }
                        int i = classesTag;
                        fileBytes2.position(curentPos + classesTag);
                        byte prufbyte = fileBytes2.get();
                        while (true) {
                            if ((patches[g].result || prufbyte != patches[g].origByte[i]) && patches[g].origMask[i] == 0) {
                                break;
                            }
                            if (patches[g].origMask[i] > 0) {
                                patches[g].repByte[i] = prufbyte;
                            }
                            i += classesTag;
                            if (i == patches[g].origByte.length) {
                                break;
                            }
                            prufbyte = fileBytes2.get();
                        }
                    }
                    g += classesTag;
                }
                fileBytes2.position(curentPos + classesTag);
                j++;
            }
            ChannelDex2.close();
            for (g = beginTag; g < patches.length; g += classesTag) {
                if (!patches[g].result) {
                    print.println("Bytes by serach N" + (g + classesTag) + ":\nError LP: Bytes not found!");
                    patch = false;
                }
            }
            g = beginTag;
            while (g < patches.length) {
                if (patches[g].result) {
                    print.println("\nBytes by search N" + (g + classesTag) + ":");
                }
                int w = beginTag;
                while (w < patches[g].origMask.length) {
                    if (patches[g].origMask[w] > classesTag) {
                        int y = patches[g].origMask[w] - 2;
                        search.set(y, Byte.valueOf(patches[g].repByte[w]));
                        if (patches[g].result) {
                            print.print("R" + y + "=" + Integer.toHexString(((Byte) search.get(y)).byteValue()).toUpperCase() + " ");
                        }
                    }
                    w += classesTag;
                }
                print.println(BuildConfig.VERSION_NAME);
                g += classesTag;
            }
        } catch (FileNotFoundException e) {
            print.println("Error LP: Program files are not found!\nMove Program to internal storage.");
        } catch (BufferUnderflowException e2) {
            print.println("Exception e" + e2.toString());
        } catch (Exception e3) {
            print.println("Exception e" + e3.toString());
        }
        return patch;
    }

    public static void getClassesDex() {
        try {
            File apk = new File(appdir);
            crkapk = new File(sddir + "/Modified/" + packageName + ".apk");
            Utils.copyFile(apk, crkapk);
            unzip(crkapk);
            if (classesFiles == null || classesFiles.size() == 0) {
                throw new FileNotFoundException();
            } else if (classesFiles != null && classesFiles.size() > 0) {
                Iterator it = classesFiles.iterator();
                while (it.hasNext()) {
                    if (!((File) it.next()).exists()) {
                        throw new FileNotFoundException();
                    }
                }
            }
        } catch (FileNotFoundException e) {
            print.println("Error LP: unzip classes.dex fault!\n\n");
        } catch (Exception e2) {
            print.println("Extract classes.dex error: " + e2.toString());
        }
    }

    public static ArrayList<String> searchlib(int architectura, String libname) {
        ArrayList<String> libs = new ArrayList();
        try {
            File apk = new File(appdir);
            crkapk = new File(sddir + "/Modified/" + packageName + ".apk");
            if (!crkapk.exists()) {
                Utils.copyFile(apk, crkapk);
            }
            extractLibs(crkapk);
            if (!libname.trim().equals("*")) {
                switch (architectura) {
                    case beginTag /*0*/:
                        libs.clear();
                        String arh = sddir + "/tmp/lib/armeabi/" + libname;
                        if (new File(sddir + "/tmp/lib/armeabi/" + libname).exists()) {
                            libs.add(arh);
                            break;
                        }
                        throw new FileNotFoundException();
                    case classesTag /*1*/:
                        libs.clear();
                        String arh1 = sddir + "/tmp/lib/armeabi-v7a/" + libname;
                        if (new File(sddir + "/tmp/lib/armeabi-v7a/" + libname).exists()) {
                            libs.add(arh1);
                            break;
                        }
                        throw new FileNotFoundException();
                    case mips /*2*/:
                        libs.clear();
                        String arh2 = sddir + "/tmp/lib/mips/" + libname;
                        if (new File(sddir + "/tmp/lib/mips/" + libname).exists()) {
                            libs.add(arh2);
                            break;
                        }
                        throw new FileNotFoundException();
                    case x86 /*3*/:
                        libs.clear();
                        String arh3 = sddir + "/tmp/lib/x86/" + libname;
                        if (new File(sddir + "/tmp/lib/x86/" + libname).exists()) {
                            libs.add(arh3);
                            break;
                        }
                        throw new FileNotFoundException();
                    case endTag /*4*/:
                        String arh4 = sddir + "/tmp/lib/armeabi/" + libname;
                        if (new File(arh4).exists()) {
                            libs.add(arh4);
                        }
                        arh4 = sddir + "/tmp/lib/armeabi-v7a/" + libname;
                        if (new File(arh4).exists()) {
                            libs.add(arh4);
                        }
                        arh4 = sddir + "/tmp/lib/mips/" + libname;
                        if (new File(arh4).exists()) {
                            libs.add(arh4);
                        }
                        arh4 = sddir + "/tmp/lib/x86/" + libname;
                        if (new File(arh4).exists()) {
                            libs.add(arh4);
                            break;
                        }
                        break;
                    default:
                        break;
                }
            }
            multilib_patch = true;
            ArrayList<File> foundlibs = new ArrayList();
            new Utils(BuildConfig.VERSION_NAME).findFileEndText(new File(sddir + "/tmp/lib/"), ".so", foundlibs);
            if (foundlibs.size() > 0) {
                Iterator it = foundlibs.iterator();
                while (it.hasNext()) {
                    File file = (File) it.next();
                    if (file.length() > 0) {
                        libs.add(file.getAbsolutePath());
                    }
                }
            }
        } catch (FileNotFoundException e) {
            print.println("Lib not found!" + e.toString());
        } catch (Exception e2) {
            print.println("Lib select error: " + e2.toString());
        }
        return libs;
    }

    public static String getFileFromApk(String file) {
        try {
            File apk = new File(appdir);
            crkapk = new File(sddir + "/Modified/" + packageName + ".apk");
            if (!crkapk.exists()) {
                Utils.copyFile(apk, crkapk);
            }
            return extractFile(crkapk, file);
        } catch (Exception e) {
            print.println("Lib select error: " + e.toString());
            return BuildConfig.VERSION_NAME;
        }
    }

    public static void fixadler(File destFile) {
        try {
            FileInputStream localFileInputStream = new FileInputStream(destFile);
            byte[] arrayOfByte = new byte[localFileInputStream.available()];
            localFileInputStream.read(arrayOfByte);
            calcSignature(arrayOfByte, beginTag);
            calcChecksum(arrayOfByte, beginTag);
            localFileInputStream.close();
            FileOutputStream localFileOutputStream = new FileOutputStream(destFile);
            localFileOutputStream.write(arrayOfByte);
            localFileOutputStream.close();
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

    private static final void calcChecksum(byte[] paramArrayOfByte, int j) {
        Adler32 localAdler32 = new Adler32();
        localAdler32.update(paramArrayOfByte, 12, paramArrayOfByte.length - (j + 12));
        int i = (int) localAdler32.getValue();
        paramArrayOfByte[j + libTagMIPS] = (byte) i;
        paramArrayOfByte[j + libTagx86] = (byte) (i >> libTagMIPS);
        paramArrayOfByte[j + fileInApkTag] = (byte) (i >> 16);
        paramArrayOfByte[j + 11] = (byte) (i >> 24);
    }

    private static final void calcSignature(byte[] paramArrayOfByte, int j) {
        try {
            MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-1");
            localMessageDigest.update(paramArrayOfByte, 32, paramArrayOfByte.length - (j + 32));
            try {
                int i = localMessageDigest.digest(paramArrayOfByte, j + 12, 20);
                if (i != 20) {
                    throw new RuntimeException("unexpected digest write:" + i + "bytes");
                }
            } catch (DigestException localDigestException) {
                throw new RuntimeException(localDigestException);
            }
        } catch (NoSuchAlgorithmException localNoSuchAlgorithmException) {
            throw new RuntimeException(localNoSuchAlgorithmException);
        }
    }

    public static void unzip(File apk) {
        classesFiles.clear();
        try {
            FileInputStream fin = new FileInputStream(apk);
            ZipInputStream zin = new ZipInputStream(fin);
            while (true) {
                ZipEntry ze = zin.getNextEntry();
                if (ze == null) {
                    zin.close();
                    fin.close();
                    return;
                } else if (ze.getName().toLowerCase().startsWith("classes") && ze.getName().endsWith(".dex") && !ze.getName().contains(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                    FileOutputStream fout = new FileOutputStream(sddir + "/Modified/" + ze.getName());
                    byte[] buffer = new byte[Util.BLOCK_HEADER_SIZE_MAX];
                    while (true) {
                        int length = zin.read(buffer);
                        if (length == -1) {
                            break;
                        }
                        fout.write(buffer, beginTag, length);
                    }
                    classesFiles.add(new File(sddir + "/Modified/" + ze.getName()));
                    zin.closeEntry();
                    fout.close();
                }
            }
        } catch (Exception e) {
            try {
                new ZipFile(apk).extractFile("classes.dex", sddir + "/Modified/");
                classesFiles.add(new File(sddir + "/Modified/" + "classes.dex"));
            } catch (ZipException e1) {
                print.println("Error LP: Error classes.dex decompress! " + e1);
                print.println("Exception e1" + e.toString());
            } catch (Exception e12) {
                print.println("Error LP: Error classes.dex decompress! " + e12);
                print.println("Exception e1" + e.toString());
            }
        }
    }

    public static void clearTemp() {
        try {
            File tempdex = new File(sddir + "/Modified/classes.dex.apk");
            if (tempdex.exists()) {
                tempdex.delete();
            }
            new Utils("createcustompatch").deleteFolder(new File(sddir + "/tmp/"));
        } catch (Exception e) {
            print.println(BuildConfig.VERSION_NAME + e.toString());
        }
    }

    public static void extractLibs(File apk) {
        String zipFile = apk.getAbsolutePath();
        String unzipLocation = sddir + "/tmp/";
        if (!new File(sddir + "/tmp/lib/").exists()) {
            new Decompress(zipFile, unzipLocation).unzip();
        }
    }

    public static String extractFile(File apk, String file) {
        return new Decompress(apk.getAbsolutePath(), sddir + "/tmp/").unzip(file);
    }

    public static void zipLib(ArrayList<String> filesIn) {
        try {
            ArrayList<AddFilesItem> files = new ArrayList();
            Iterator it = filesIn.iterator();
            while (it.hasNext()) {
                files.add(new AddFilesItem((String) it.next(), sddir + "/tmp/"));
            }
            Utils.addFilesToZip(crkapk.getAbsolutePath(), crkapk.getAbsolutePath() + "checlpis.zip", files);
            crkapk.delete();
            if (!crkapk.exists()) {
                new File(crkapk.getAbsolutePath() + "checlpis.zip").renameTo(crkapk);
            }
        } catch (Exception e) {
            print.println("Error LP: Error libs compress! " + e);
        }
    }
}
