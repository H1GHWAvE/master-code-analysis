package com.chelpus.root.utils;

import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;
import java.io.File;
import net.lingala.zip4j.util.InternalZipConstants;

public class clearDalvikCache {

    static class C05481 {
        C05481() {
        }
    }

    public static void main(String[] paramArrayOfString) {
        Utils.startRootJava(new C05481());
        listAppsFragment.toolfilesdir = paramArrayOfString[0];
        if (new File(listAppsFragment.toolfilesdir + "/ClearDalvik.on").exists()) {
            new File(listAppsFragment.toolfilesdir + "/ClearDalvik.on").delete();
            if (new File(listAppsFragment.toolfilesdir + "/ClearDalvik.on").exists()) {
                System.out.println("LuckyPatcher: (error) try delete ClearDalvik.on - fault!");
            } else {
                try {
                    for (File dir : new File("/mnt/asec").listFiles()) {
                        if (dir.isDirectory()) {
                            for (File file : dir.listFiles()) {
                                if (file.getAbsoluteFile().toString().endsWith(".odex")) {
                                    Utils.remount(file.getAbsolutePath(), InternalZipConstants.WRITE_MODE);
                                    file.delete();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                }
                try {
                    if (new File("/data/dalvik-cache").exists()) {
                        for (File tail : new File("/data/dalvik-cache").listFiles()) {
                            if (tail.isDirectory()) {
                                File[] oats = tail.listFiles();
                                if (oats != null && oats.length > 0) {
                                    for (File oat : oats) {
                                        if (oat.isFile()) {
                                            oat.delete();
                                        }
                                    }
                                }
                            }
                            if (tail.isFile()) {
                                tail.delete();
                            }
                        }
                    }
                } catch (Exception e2) {
                }
                try {
                    if (new File("/cache/dalvik-cache").exists()) {
                        for (String tail2 : new File("/cache/dalvik-cache").list()) {
                            new File("/cache/dalvik-cache/" + tail2).delete();
                        }
                    }
                } catch (Exception e3) {
                }
                try {
                    if (new File("/sd-ext/data/dalvik-cache").exists()) {
                        for (String tail22 : new File("/sd-ext/data/dalvik-cache").list()) {
                            new File("/sd-ext/data/dalvik-cache/" + tail22).delete();
                        }
                    }
                } catch (Exception e4) {
                }
                try {
                    if (new File("/sd-ext/data/cache/dalvik-cache").exists()) {
                        for (String tail222 : new File("/sd-ext/data/cache/dalvik-cache").list()) {
                            new File("/sd-ext/data/cache/dalvik-cache/" + tail222).delete();
                        }
                    }
                } catch (Exception e5) {
                }
                try {
                    if (new File("/data/app").exists()) {
                        for (String tail2222 : new File("/data/app").list()) {
                            if (tail2222.endsWith(".odex")) {
                                new File("/data/app/" + tail2222).delete();
                            }
                        }
                    }
                } catch (Exception e6) {
                }
                System.out.println("LuckyPatcher: Dalvik-Cache deleted.");
                try {
                    File odex;
                    Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                    for (File sys_file : new File("/system/app").listFiles()) {
                        odex = new File(Utils.getPlaceForOdex(sys_file.getAbsolutePath(), true));
                        if (odex.exists() && sys_file.getAbsoluteFile().toString().endsWith(".apk") && Utils.classes_test(sys_file)) {
                            odex.delete();
                        }
                    }
                    for (File sys_file2 : new File("/system/priv-app").listFiles()) {
                        odex = new File(Utils.getPlaceForOdex(sys_file2.getAbsolutePath(), true));
                        if (odex.exists() && sys_file2.getAbsoluteFile().toString().endsWith(".apk") && Utils.classes_test(sys_file2)) {
                            odex.delete();
                        }
                    }
                    System.out.println("LuckyPatcher: System apps deodex all.");
                } catch (Exception e7) {
                }
            }
        }
        Utils.exitFromRootJava();
    }
}
