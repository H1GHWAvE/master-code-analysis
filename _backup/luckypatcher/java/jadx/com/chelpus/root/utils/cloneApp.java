package com.chelpus.root.utils;

import com.android.vending.billing.InAppBillingService.LUCK.AxmlExample;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.LogOutputStream;
import com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.util.InternalZipConstants;
import org.tukaani.xz.common.Util;

public class cloneApp {
    public static String appdir = "/sdcard/";
    public static ArrayList<File> classesFiles = new ArrayList();
    public static File crkapk;
    public static String dir = "/sdcard/";
    public static String dir2 = "/sdcard/";
    public static String dirapp = "/data/app/";
    public static ArrayList<File> filestopatch = null;
    public static PrintStream print;
    public static String result;
    public static String sddir = "/sdcard/";
    public static boolean system = false;

    static class C05501 {
        C05501() {
        }
    }

    public static void main(String[] paramArrayOfString) {
        LogOutputStream pout = new LogOutputStream("System.out");
        print = new PrintStream(pout);
        Utils.startRootJava(new C05501());
        Utils.kill(paramArrayOfString[0]);
        print.println("Support-Code Running!");
        ArrayList<PatchesItemAuto> patchesList = new ArrayList();
        filestopatch = new ArrayList();
        try {
            for (File file : new File(paramArrayOfString[2]).listFiles()) {
                if (!(!file.isFile() || file.getName().equals("busybox") || file.getName().equals("reboot") || file.getName().equals("dalvikvm"))) {
                    file.delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (paramArrayOfString[6] != null) {
                Utils.sendFromRoot(paramArrayOfString[6]);
            }
        } catch (NullPointerException e2) {
        } catch (Exception e3) {
        }
        listAppsFragment.startUnderRoot = Boolean.valueOf(false);
        try {
            String packageName = paramArrayOfString[0];
            appdir = paramArrayOfString[1];
            sddir = paramArrayOfString[3];
            clearTempSD();
            File apk = new File(appdir);
            unzipSD(apk);
            crkapk = new File(sddir + "/Modified/" + packageName + ".apk");
            Utils.copyFile(apk, crkapk);
            byte[] pn = packageName.getBytes();
            for (int h = 0; h < pn.length; h++) {
                if (h == pn.length - 1) {
                    pn[h] = (byte) (pn[h] + 1);
                }
            }
            String newPackageName = new String(pn);
            File androidManifest = new File(sddir + "/Modified/AndroidManifest.xml");
            if (androidManifest.exists()) {
                try {
                    if (!new AxmlExample().changePackageName(androidManifest, packageName, newPackageName)) {
                        androidManifest.delete();
                    }
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
                if (classesFiles == null || classesFiles.size() == 0) {
                    throw new FileNotFoundException();
                }
                filestopatch.clear();
                Iterator it = classesFiles.iterator();
                while (it.hasNext()) {
                    File cl = (File) it.next();
                    if (cl.exists()) {
                        filestopatch.add(cl);
                    } else {
                        throw new FileNotFoundException();
                    }
                }
                it = filestopatch.iterator();
                while (it.hasNext()) {
                    File filepatch = (File) it.next();
                    Utils.sendFromRoot("Find string id.");
                    Utils.sendFromRoot("String analysis.");
                    print.println("String analysis.");
                    Utils.sendFromRoot("Analise Results:");
                    Utils.fixadler(filepatch);
                }
                Utils.sendFromRoot("Optional Steps After Patch:");
                result = pout.allresult;
                return;
            }
            throw new FileNotFoundException();
        } catch (Exception e42) {
            e42.printStackTrace();
        }
    }

    public static void unzipSD(File apk) {
        try {
            FileInputStream fin = new FileInputStream(apk);
            ZipInputStream zin = new ZipInputStream(fin);
            boolean classesdex = false;
            while (true) {
                ZipEntry ze = zin.getNextEntry();
                if (ze != null) {
                    FileOutputStream fout;
                    byte[] buffer;
                    int length;
                    if (ze.getName().startsWith("classes") && ze.getName().endsWith(".dex") && !ze.getName().contains(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                        fout = new FileOutputStream(sddir + "/Modified/" + ze.getName());
                        buffer = new byte[Util.BLOCK_HEADER_SIZE_MAX];
                        while (true) {
                            length = zin.read(buffer);
                            if (length == -1) {
                                break;
                            }
                            fout.write(buffer, 0, length);
                        }
                        classesFiles.add(new File(sddir + "/Modified/" + ze.getName()));
                        classesdex = true;
                    }
                    if (ze.getName().equals("AndroidManifest.xml")) {
                        fout = new FileOutputStream(sddir + "/Modified/" + "AndroidManifest.xml");
                        buffer = new byte[Util.BLOCK_HEADER_SIZE_MAX];
                        while (true) {
                            length = zin.read(buffer);
                            if (length == -1) {
                                break;
                            }
                            fout.write(buffer, 0, length);
                        }
                        if (classesdex) {
                            zin.closeEntry();
                            fout.close();
                        }
                    }
                } else {
                    zin.close();
                    fin.close();
                    return;
                }
            }
        } catch (Exception e) {
            try {
                ZipFile zipFile = new ZipFile(apk);
                zipFile.extractFile("classes.dex", sddir + "/Modified/");
                classesFiles.add(new File(sddir + "/Modified/" + "classes.dex"));
                zipFile.extractFile("AndroidManifest.xml", sddir + "/Modified/");
            } catch (ZipException e1) {
                Utils.sendFromRoot("Error classes.dex decompress! " + e1);
                Utils.sendFromRoot("Exception e1" + e.toString());
            } catch (Exception e12) {
                Utils.sendFromRoot("Error classes.dex decompress! " + e12);
                Utils.sendFromRoot("Exception e1" + e.toString());
            }
            Utils.sendFromRoot("Decompressunzip " + e);
        }
    }

    public static void clearTempSD() {
        try {
            File tempdex = new File(sddir + "/Modified/classes.dex.apk");
            if (tempdex.exists()) {
                tempdex.delete();
            }
        } catch (Exception e) {
            Utils.sendFromRoot(BuildConfig.VERSION_NAME + e.toString());
        }
    }
}
