package com.chelpus.root.utils;

import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.CommandItem;
import com.android.vending.billing.InAppBillingService.LUCK.LogOutputStream;
import com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto;
import com.android.vending.billing.InAppBillingService.LUCK.StringItem;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Common;
import com.chelpus.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.util.InternalZipConstants;
import org.tukaani.xz.common.Util;

public class odexrunpatch {
    public static boolean ART = false;
    private static boolean amazon = true;
    public static String appdir = "/sdcard/";
    public static ArrayList<File> classesFiles = new ArrayList();
    public static boolean copyDC = false;
    private static boolean createAPK = false;
    public static File crkapk;
    private static boolean dependencies = true;
    public static String dir = "/sdcard/";
    public static String dirapp = "/data/app/";
    public static ArrayList<File> filestopatch = null;
    private static boolean pattern1 = true;
    private static boolean pattern2 = true;
    private static boolean pattern3 = true;
    private static boolean pattern4 = true;
    private static boolean pattern5 = true;
    private static boolean pattern6 = true;
    public static PrintStream print;
    public static String result;
    private static boolean samsung = true;
    public static String sddir = "/sdcard/";
    public static boolean system = false;
    public static String uid = BuildConfig.VERSION_NAME;

    static class C05691 {
        C05691() {
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void main(String[] paramArrayOfString) {
        Iterator it;
        OutputStream logOutputStream = new LogOutputStream("System.out");
        print = new PrintStream(logOutputStream);
        Utils.startRootJava(new C05691());
        Utils.kill(paramArrayOfString[0]);
        ArrayList<PatchesItemAuto> patchesList = new ArrayList();
        pattern1 = true;
        pattern2 = true;
        pattern3 = true;
        pattern4 = true;
        pattern5 = true;
        pattern6 = true;
        dependencies = true;
        amazon = true;
        samsung = true;
        filestopatch = new ArrayList();
        try {
            for (File file : new File(paramArrayOfString[3]).listFiles()) {
                if (!(!file.isFile() || file.getName().equals("busybox") || file.getName().equals("reboot") || file.getName().equals("dalvikvm"))) {
                    file.delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (!paramArrayOfString[1].contains("pattern1")) {
                pattern1 = false;
            }
            if (!paramArrayOfString[1].contains("pattern2")) {
                pattern2 = false;
            }
            if (!paramArrayOfString[1].contains("pattern3")) {
                pattern3 = false;
            }
            if (!paramArrayOfString[1].contains("pattern4")) {
                pattern4 = false;
            }
            if (!paramArrayOfString[1].contains("pattern5")) {
                pattern5 = false;
            }
            if (!paramArrayOfString[1].contains("pattern6")) {
                pattern6 = false;
            }
            if (!paramArrayOfString[1].contains("dependencies")) {
                dependencies = false;
            }
            if (!paramArrayOfString[1].contains("amazon")) {
                amazon = false;
            }
            if (!paramArrayOfString[1].contains("samsung")) {
                samsung = false;
            }
            if (paramArrayOfString[6].contains("createAPK")) {
                createAPK = true;
            }
            if (paramArrayOfString[6] != null && paramArrayOfString[6].contains("ART")) {
                ART = true;
            }
            if (paramArrayOfString[7] != null) {
                uid = paramArrayOfString[7];
            }
            Utils.sendFromRoot(paramArrayOfString[6]);
        } catch (NullPointerException e2) {
        } catch (Exception e3) {
        }
        try {
            if (paramArrayOfString[5].contains("copyDC")) {
                copyDC = true;
            }
        } catch (NullPointerException e4) {
        } catch (Exception e5) {
        }
        if (createAPK) {
            listAppsFragment.startUnderRoot = Boolean.valueOf(false);
        }
        ArrayList<String> origStr = new ArrayList();
        ArrayList<String> replStr = new ArrayList();
        ArrayList<Boolean> trigger = new ArrayList();
        ArrayList<String> ResultText = new ArrayList();
        ArrayList<String> markers = new ArrayList();
        ArrayList<Boolean> dontConvert = new ArrayList();
        byte[] bArr = new byte[29];
        bArr = new byte[]{(byte) 5, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 2, (byte) 1, (byte) 0, (byte) 0, (byte) 3, (byte) 1, (byte) 0, (byte) 0, (byte) 15, (byte) 0, (byte) 0, (byte) 0, (byte) 26, (byte) 0, (byte) 0, (byte) 0, (byte) 15, (byte) 0, (byte) 0, (byte) 0, (byte) 89};
        byte[] bArr2 = new byte[29];
        bArr2 = new byte[]{(byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 1};
        byte[] bArr3 = new byte[29];
        bArr3 = new byte[]{(byte) 5, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 2, (byte) 1, (byte) 0, (byte) 0, (byte) 3, (byte) 1, (byte) 0, (byte) 0, (byte) 15, (byte) 0, (byte) 0, (byte) 0, (byte) 15, (byte) 0, (byte) 0, (byte) 0, (byte) 15, (byte) 0, (byte) 0, (byte) 0, (byte) 89};
        byte[] bArr4 = new byte[29];
        bArr4 = new byte[]{(byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 2, (byte) 0, (byte) 0, (byte) 0, (byte) 3, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0};
        origStr.add("1A ?? FF FF");
        replStr.add("1A ?? ?? ??");
        trigger.add(Boolean.valueOf(true));
        ResultText.add("(pak intekekt 0)");
        markers.add("search_pack");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1B ?? FF FF FF FF");
        replStr.add("1B ?? ?? ?? ?? ??");
        trigger.add(Boolean.valueOf(true));
        ResultText.add("(pak intekekt 0)");
        markers.add("search_pack");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1A ?? FF FF");
        replStr.add("1A ?? ?? ??");
        trigger.add(Boolean.valueOf(true));
        ResultText.add("(sha intekekt 2)");
        markers.add("search");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1B ?? FF FF FF FF");
        replStr.add("1B ?? ?? ?? ?? ??");
        trigger.add(Boolean.valueOf(true));
        ResultText.add("(sha intekekt 2 32 bit)");
        markers.add("search");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("0A ?? 39 ?? ?? 00");
        replStr.add("12 S1 39 ?? ?? 00");
        trigger.add(Boolean.valueOf(false));
        ResultText.add("lvl patch N2!\n(sha intekekt 3)");
        markers.add("search");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("6E 20 FF FF ?? 00 0A ??");
        replStr.add("6E 20 ?? ?? ?? 00 12 S1");
        trigger.add(Boolean.valueOf(false));
        ResultText.add("support2 Fixed!\n(sha intekekt 4)");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 62 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 44 00 01 00 2B 00 ?? ?? ?? ?? 62 ?? ?? ?? 11");
        replStr.add("70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 62 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 12 10 00 00 2B 00 ?? ?? ?? ?? 62 ?? ?? ?? 11");
        trigger.add(Boolean.valueOf(pattern3));
        ResultText.add("lvl patch N2!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 39 ?? ?? ?? 53 ?? ?? ?? 31");
        replStr.add("12 S1 12 S1 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 39 ?? ?? ?? 53 ?? ?? ?? 31");
        trigger.add(Boolean.valueOf(pattern5));
        ResultText.add("lvl patch N3!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("0a ?? 38 ?? 0e 00 1a ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 72");
        replStr.add("0a ?? 33 00 ?? ?? 1a ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 72");
        trigger.add(Boolean.valueOf(pattern2));
        ResultText.add("lvl patch N4!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1A ?? ?? ?? 70 ?? ?? ?? ?? ?? 27 ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? 12");
        replStr.add("1A ?? ?? ?? 70 ?? ?? ?? ?? ?? 27 ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? 12");
        trigger.add(Boolean.valueOf(pattern4));
        ResultText.add("lvl patch N5!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? 12 ??");
        replStr.add("22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? 12 ??");
        trigger.add(Boolean.valueOf(pattern4));
        ResultText.add("lvl patch N5!\nparse response code");
        markers.add("patch5");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? 31 ?? ?? ?? 3B ?? ?? ?? 01");
        replStr.add("12 S1 12 S1 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? 31 ?? ?? ?? 3B ?? ?? ?? 01");
        trigger.add(Boolean.valueOf(pattern5));
        ResultText.add("lvl patch N3!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 01 ?? 28");
        replStr.add("2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 01 ?? 28");
        trigger.add(Boolean.valueOf(pattern4));
        ResultText.add("lvl patch N7!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 01 ?? 28");
        replStr.add("2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 01 ?? 28");
        trigger.add(Boolean.valueOf(pattern4));
        ResultText.add("lvl patch N7!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 28");
        replStr.add("2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 28");
        trigger.add(Boolean.valueOf(pattern4));
        ResultText.add("lvl patch N7!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A");
        replStr.add("2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12");
        trigger.add(Boolean.valueOf(pattern4));
        ResultText.add("lvl patch N7!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 21 ?? 12 ?? 35 ?? ?? ?? 22 ?? ?? ?? 1A ?? ?? ?? 70 ?? ?? ?? ?? ?? 27 ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A");
        replStr.add("0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 21 ?? 12 ?? 35 ?? ?? ?? 22 ?? ?? ?? 1A ?? ?? ?? 70 ?? ?? ?? ?? ?? 27 ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12");
        trigger.add(Boolean.valueOf(pattern4));
        ResultText.add("lvl patch N5!\n");
        markers.add("patch5");
        dontConvert.add(Boolean.valueOf(false));
        byte[] bArr5 = new byte[32];
        bArr5 = new byte[]{(byte) 26, (byte) 102, (byte) 102, (byte) 102, (byte) 113, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 113, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 33, (byte) 102, (byte) 102, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102};
        byte[] bArr6 = new byte[32];
        bArr6 = new byte[]{(byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1};
        byte[] bArr7 = new byte[32];
        bArr7 = new byte[]{(byte) 26, (byte) 102, (byte) 102, (byte) 102, (byte) 113, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 113, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 33, (byte) 102, (byte) 102, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102};
        byte[] bArr8 = new byte[32];
        bArr8 = new byte[]{(byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0};
        byte[] bArr9 = new byte[13];
        bArr9 = new byte[]{(byte) 0, (byte) 70, (byte) 102, (byte) 102, (byte) 102, (byte) 113, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 102};
        byte[] bArr10 = new byte[13];
        bArr10 = new byte[]{(byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1};
        byte[] bArr11 = new byte[13];
        bArr11 = new byte[]{(byte) 0, (byte) 70, (byte) 102, (byte) 102, (byte) 102, (byte) 113, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 102};
        byte[] bArr12 = new byte[13];
        bArr12 = new byte[]{(byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 0};
        origStr.add("2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 62 ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 62 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A");
        replStr.add("2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 62 ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 62 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12");
        trigger.add(Boolean.valueOf(pattern4));
        ResultText.add("lvl patch N7!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? ?? ?? 33 ?? ?? ?? 53");
        replStr.add("12 S1 12 S1 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? ?? ?? 33 ?? ?? ?? 53");
        trigger.add(Boolean.valueOf(pattern5));
        ResultText.add("lvl patch N3!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("6C 61 63 6B 79 70 61 74 63 68");
        replStr.add("6C 75 63 75 79 70 75 74 63 68");
        trigger.add(Boolean.valueOf(true));
        ResultText.add(BuildConfig.VERSION_NAME);
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(true));
        origStr.add("6C 75 63 6B 79 70 61 74 63 68");
        replStr.add("6C 75 63 75 79 70 75 74 75 68");
        trigger.add(Boolean.valueOf(true));
        ResultText.add(BuildConfig.VERSION_NAME);
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(true));
        origStr.add("64 69 6D 6F 6E 76 69 64 65 6F 2E");
        replStr.add("64 69 6D 69 6E 69 69 64 65 6F 2E");
        trigger.add(Boolean.valueOf(true));
        ResultText.add(BuildConfig.VERSION_NAME);
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(true));
        origStr.add("12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 33 ?? ?? ?? 53 ?? ?? ?? 31");
        replStr.add("12 S1 12 S1 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 33 ?? ?? ?? 53 ?? ?? ?? 31");
        trigger.add(Boolean.valueOf(pattern5));
        ResultText.add("lvl patch N3!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("6E 10 ?? ?? ?? ?? 0A ?? 6E 10 ?? ?? ?? ?? 0C ?? 6E 10 ?? ?? ?? ?? 0C ?? 6E 40");
        replStr.add("6E 10 ?? ?? ?? ?? 12 ?? 6E 10 ?? ?? ?? ?? 0C ?? 6E 10 ?? ?? ?? ?? 0C ?? 6E 40");
        trigger.add(Boolean.valueOf(pattern4));
        ResultText.add("lvl patch N7!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("62 01 ?? ?? 6E 20 ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A 01 71 10 ?? ?? ?? ?? 0C 02 71 10 ?? ?? ?? ?? 6E 10 ?? ?? ?? ?? 0C 02 6E 10 ?? ?? ?? ?? 0C 03 6E 40 ?? ?? ?? ?? 28");
        replStr.add("62 01 ?? ?? 6E 20 ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 12 01 71 10 ?? ?? ?? ?? 0C 02 71 10 ?? ?? ?? ?? 6E 10 ?? ?? ?? ?? 0C 02 6E 10 ?? ?? ?? ?? 0C 03 6E 40 ?? ?? ?? ?? 28");
        trigger.add(Boolean.valueOf(pattern4));
        ResultText.add("lvl patch N7!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 39 ?? ?? ?? 53 ?? ?? ?? 31");
        replStr.add("12 S0 12 S0 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 39 ?? ?? ?? 53 ?? ?? ?? 31");
        trigger.add(Boolean.valueOf(pattern6));
        ResultText.add("lvl patch N3!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 33 ?? ?? ?? 53 ?? ?? ?? 31");
        replStr.add("12 S0 12 S0 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 33 ?? ?? ?? 53 ?? ?? ?? 31");
        trigger.add(Boolean.valueOf(pattern6));
        ResultText.add("lvl patch N3!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? ?? ?? 33 ?? ?? ?? 53");
        replStr.add("12 S0 12 S0 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? ?? ?? 33 ?? ?? ?? 53");
        trigger.add(Boolean.valueOf(pattern6));
        ResultText.add("lvl patch N3!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? 31 ?? ?? ?? 3B ?? ?? ?? 01");
        replStr.add("12 S0 12 S0 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? 31 ?? ?? ?? 3B ?? ?? ?? 01");
        trigger.add(Boolean.valueOf(pattern6));
        ResultText.add("lvl patch N3!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("13 00 00 01 33 R0 ?? ?? 54 ?? ?? ?? 71 10 ?? ?? ?? ?? 0C 01");
        replStr.add("13 W0 00 01 33 00 00 01 54 ?? ?? ?? 71 10 ?? ?? ?? ?? 0C 01");
        trigger.add(Boolean.valueOf(pattern4));
        ResultText.add("lvl patch N3!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1A 05 ?? ?? 63 00 ?? ?? 38 00 09 00 62 00 ?? ?? 1A 01 ?? ?? 6E 20 ?? ?? ?? ?? 54 60 ?? ?? 1A 01 ?? ?? 6E 20 ?? ?? ?? ?? ?? ?? 38 00 0A 00 62 00");
        replStr.add("1A 05 ?? ?? 63 00 ?? ?? 38 00 09 00 62 00 ?? ?? 1A 01 ?? ?? 6E 20 ?? ?? ?? ?? 54 60 ?? ?? 1A 01 ?? ?? 6E 20 ?? ?? ?? ?? ?? ?? 33 00 ?? ?? 62 00");
        trigger.add(Boolean.valueOf(amazon));
        ResultText.add("amazon patch N1!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("6E 20 ?? ?? ?? ?? 0C 00 38 00 05 00 12 10 ?? ?? 0F 00 12 00 ?? ?? 0D 00 ?? ?? 27 00");
        replStr.add("6E 20 ?? ?? ?? ?? 0C 00 33 00 ?? ?? 12 10 ?? ?? 0F 00 12 00 ?? ?? 0D 00 ?? ?? 27 00");
        trigger.add(Boolean.valueOf(amazon));
        ResultText.add("amazon patch N1!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 ?? ?? ?? 59 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 27");
        replStr.add("13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 00 ?? ?? 59 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 27");
        trigger.add(Boolean.valueOf(samsung));
        ResultText.add("samsung patch N1!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("13 ?? 09 00 13 ?? 0B 00 6E ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 ?? ?? ?? 59 ?? ?? ?? 22 ?? ?? ?? 12 ?? 70 ?? ?? ?? ?? ?? 27");
        replStr.add("13 ?? 09 00 13 ?? 0B 00 6E ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 00 ?? ?? 59 ?? ?? ?? 22 ?? ?? ?? 12 ?? 70 ?? ?? ?? ?? ?? 27");
        trigger.add(Boolean.valueOf(samsung));
        ResultText.add("samsung patch N1!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0A ?? 0F ?? 00 00");
        replStr.add("54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 12 S1 0F ?? 00 00");
        trigger.add(Boolean.valueOf(samsung));
        ResultText.add("samsung patch N2!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? 0F ?? 00 00");
        replStr.add("54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12 S1 0F ?? 00 00");
        trigger.add(Boolean.valueOf(samsung));
        ResultText.add("samsung patch N2!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0A ?? 0F ?? 00 00");
        replStr.add("54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 12 S1 0F ?? 00 00");
        trigger.add(Boolean.valueOf(samsung));
        ResultText.add("samsung patch N3!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("54 ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ??");
        replStr.add("54 ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? 33 00 ?? ??");
        trigger.add(Boolean.valueOf(samsung));
        ResultText.add("samsung patch N3!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("13 ?? 32 00 33 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12");
        replStr.add("13 ?? 32 00 32 00 ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12");
        trigger.add(Boolean.valueOf(samsung));
        ResultText.add("samsung patch N4!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("13 ?? 32 00 33 ?? ?? ?? 70 ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 54 ?? ?? ?? 59 ?? ?? ?? ?? ?? 28");
        replStr.add("13 ?? 32 00 32 00 ?? ?? 70 ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 54 ?? ?? ?? 59 ?? ?? ?? ?? ?? 28");
        trigger.add(Boolean.valueOf(samsung));
        ResultText.add("samsung patch N4!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("38 ?? 06 00 32 ?? 04 00 33 ?? ?? ?? 1A ?? ?? ?? 71");
        replStr.add("12 ?? 00 00 32 00 04 00 33 ?? ?? ?? ?? ?? ?? ?? ??");
        trigger.add(Boolean.valueOf(pattern4));
        ResultText.add("lvl patch N6!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        if (!paramArrayOfString[0].contains("com.buak.Link2SD")) {
            origStr.add("00 05 2E 6F 64 65 78 00");
            replStr.add("00 05 2E 6F 64 65 79 00");
            trigger.add(Boolean.valueOf(pattern4));
            ResultText.add("lvl patch N7!\n");
            markers.add(BuildConfig.VERSION_NAME);
            dontConvert.add(Boolean.valueOf(true));
        }
        origStr.add("00 04 6F 64 65 78 00");
        replStr.add("00 04 6F 64 65 79 00");
        trigger.add(Boolean.valueOf(pattern4));
        ResultText.add("lvl patch N7!\n");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(true));
        origStr.add("2F 4C 75 63 6B 79 50 61 74 63 68 65 72");
        replStr.add("2F 4C 75 63 6B 79 50 79 74 63 68 65 72");
        trigger.add(Boolean.valueOf(true));
        ResultText.add(BuildConfig.VERSION_NAME);
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(true));
        if (ART) {
            origStr.add("13 ?? 09 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??");
            replStr.add("12 S0 00 00 12 S0 12 S0 6E ?? ?? ?? ?? ?? 0C ??");
            trigger.add(Boolean.valueOf(dependencies));
            ResultText.add("com.android.vending dependencies removed\n");
            markers.add(BuildConfig.VERSION_NAME);
            dontConvert.add(Boolean.valueOf(true));
            origStr.add("12 ?? 12 ?? 13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ??");
            replStr.add("12 S0 12 S0 12 S0 00 00 6E ?? ?? ?? ?? ?? 0C ??");
            trigger.add(Boolean.valueOf(dependencies));
            ResultText.add("com.android.vending dependencies removed\n");
            markers.add(BuildConfig.VERSION_NAME);
            dontConvert.add(Boolean.valueOf(true));
        } else {
            origStr.add("13 ?? 09 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??");
            replStr.add("12 00 0F 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??");
            trigger.add(Boolean.valueOf(dependencies));
            ResultText.add("com.android.vending dependencies removed\n");
            markers.add(BuildConfig.VERSION_NAME);
            dontConvert.add(Boolean.valueOf(true));
            origStr.add("12 ?? 12 ?? 13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ??");
            replStr.add("12 ?? 12 ?? 12 00 0F 00 6E ?? ?? ?? ?? ?? 0C ??");
            trigger.add(Boolean.valueOf(dependencies));
            ResultText.add("com.android.vending dependencies removed\n");
            markers.add(BuildConfig.VERSION_NAME);
            dontConvert.add(Boolean.valueOf(true));
        }
        Utils.convertToPatchItemAuto(patchesList, origStr, replStr, trigger, ResultText, markers, dontConvert, Boolean.valueOf(false));
        try {
            if (listAppsFragment.startUnderRoot.booleanValue()) {
                Utils.remount(paramArrayOfString[2], "RW");
            }
            String packageName;
            File apk;
            File cl;
            if (createAPK || ART) {
                if (createAPK) {
                    packageName = paramArrayOfString[0];
                    appdir = paramArrayOfString[2];
                    sddir = paramArrayOfString[5];
                    clearTempSD();
                    apk = new File(appdir);
                    unzipSD(apk);
                    crkapk = new File(sddir + "/Modified/" + packageName + ".apk");
                    Utils.copyFile(apk, crkapk);
                    if (classesFiles == null || classesFiles.size() == 0) {
                        throw new FileNotFoundException();
                    }
                    filestopatch.clear();
                    it = classesFiles.iterator();
                    while (it.hasNext()) {
                        cl = (File) it.next();
                        if (cl.exists()) {
                            filestopatch.add(cl);
                        } else {
                            throw new FileNotFoundException();
                        }
                    }
                }
                if (ART) {
                    packageName = paramArrayOfString[0];
                    appdir = paramArrayOfString[2];
                    sddir = paramArrayOfString[3];
                    clearTempSD();
                    unzipART(new File(appdir));
                    if (classesFiles == null || classesFiles.size() == 0) {
                        throw new FileNotFoundException();
                    }
                    filestopatch.clear();
                    it = classesFiles.iterator();
                    while (it.hasNext()) {
                        cl = (File) it.next();
                        if (cl.exists()) {
                            filestopatch.add(cl);
                        } else {
                            throw new FileNotFoundException();
                        }
                    }
                }
            }
            dir = paramArrayOfString[3];
            dirapp = paramArrayOfString[2];
            clearTemp();
            if (paramArrayOfString[4].equals("not_system")) {
                system = false;
            }
            if (paramArrayOfString[4].equals("system")) {
                system = true;
            }
            filestopatch.clear();
            Utils.sendFromRoot("CLASSES mode create odex enabled.");
            packageName = paramArrayOfString[0];
            appdir = paramArrayOfString[2];
            sddir = paramArrayOfString[3];
            clearTempSD();
            apk = new File(appdir);
            Utils.sendFromRoot("Get classes.dex.");
            print.println("Get classes.dex.");
            unzipART(apk);
            if (classesFiles == null || classesFiles.size() == 0) {
                throw new FileNotFoundException();
            }
            filestopatch.clear();
            it = classesFiles.iterator();
            while (it.hasNext()) {
                cl = (File) it.next();
                if (cl.exists()) {
                    try {
                        filestopatch.add(cl);
                    } catch (Exception e6) {
                        Utils.sendFromRoot(BuildConfig.VERSION_NAME + e6);
                        e6.printStackTrace();
                    } catch (FileNotFoundException localFileNotFoundException) {
                        Utils.sendFromRoot("Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*");
                        localFileNotFoundException.printStackTrace();
                    }
                } else {
                    throw new FileNotFoundException();
                }
            }
            String odexstr = Utils.getPlaceForOdex(paramArrayOfString[2], true);
            File odexfile = new File(odexstr);
            if (odexfile.exists()) {
                odexfile.delete();
            }
            File file2 = new File(odexstr.replace("-1", "-2"));
            if (file2.exists()) {
                file2.delete();
            }
            file2 = new File(odexstr.replace("-2", "-1"));
            if (file2.exists()) {
                file2.delete();
            }
            Iterator it2 = filestopatch.iterator();
            while (it2.hasNext()) {
                CommandItem item;
                File file_patch = (File) it2.next();
                Utils.sendFromRoot("Find string id.");
                ArrayList<String> strings = new ArrayList();
                strings.add(Common.GOOGLEPLAY_PKG);
                strings.add("SHA1withRSA");
                strings.add("com.android.vending.billing.InAppBillingService.BIND");
                strings.add("Ljava/security/Signature;");
                strings.add("verify");
                Utils.sendFromRoot("String analysis.");
                print.println("String analysis.");
                ArrayList<StringItem> offsets = Utils.getStringIds(file_patch.getAbsolutePath(), strings, false);
                boolean f2 = false;
                boolean f3 = false;
                ArrayList<CommandItem> commands = new ArrayList();
                commands.add(new CommandItem("Ljava/security/Signature;", "verify"));
                Iterator it3 = offsets.iterator();
                while (it3.hasNext()) {
                    StringItem it4 = (StringItem) it3.next();
                    it = commands.iterator();
                    while (it.hasNext()) {
                        item = (CommandItem) it.next();
                        if (item.object.equals(it4.str)) {
                            item.Object = it4.offset;
                        }
                        if (item.method.equals(it4.str)) {
                            item.Method = it4.offset;
                        }
                    }
                    if (it4.str.equals("com.android.vending.billing.InAppBillingService.BIND")) {
                        ((PatchesItemAuto) patchesList.get(0)).origByte[2] = it4.offset[0];
                        ((PatchesItemAuto) patchesList.get(0)).origByte[3] = it4.offset[1];
                        ((PatchesItemAuto) patchesList.get(1)).origByte[2] = it4.offset[0];
                        ((PatchesItemAuto) patchesList.get(1)).origByte[3] = it4.offset[1];
                        ((PatchesItemAuto) patchesList.get(1)).origByte[4] = it4.offset[2];
                        ((PatchesItemAuto) patchesList.get(1)).origByte[5] = it4.offset[3];
                        f2 = false;
                    }
                    if (it4.str.equals("SHA1withRSA")) {
                        ((PatchesItemAuto) patchesList.get(2)).origByte[2] = it4.offset[0];
                        ((PatchesItemAuto) patchesList.get(2)).origByte[3] = it4.offset[1];
                        ((PatchesItemAuto) patchesList.get(3)).origByte[2] = it4.offset[0];
                        ((PatchesItemAuto) patchesList.get(3)).origByte[3] = it4.offset[1];
                        ((PatchesItemAuto) patchesList.get(3)).origByte[4] = it4.offset[2];
                        ((PatchesItemAuto) patchesList.get(3)).origByte[5] = it4.offset[3];
                        f3 = true;
                    }
                }
                if (!(false && f2)) {
                    ((PatchesItemAuto) patchesList.get(0)).pattern = false;
                    ((PatchesItemAuto) patchesList.get(1)).pattern = false;
                }
                if (!f3) {
                    ((PatchesItemAuto) patchesList.get(2)).pattern = false;
                    ((PatchesItemAuto) patchesList.get(3)).pattern = false;
                    ((PatchesItemAuto) patchesList.get(4)).pattern = false;
                }
                Utils.sendFromRoot("Parse data for patch.");
                print.println("Parse data for patch.");
                Utils.getMethodsIds(file_patch.getAbsolutePath(), commands, false);
                System.out.println(Integer.toHexString(((CommandItem) commands.get(0)).index_command[0]));
                System.out.println(Integer.toHexString(((CommandItem) commands.get(0)).index_command[1]));
                it3 = commands.iterator();
                while (it3.hasNext()) {
                    item = (CommandItem) it3.next();
                    if (item.found_index_command && item.object.equals("Ljava/security/Signature;")) {
                        ((PatchesItemAuto) patchesList.get(11)).origByte[2] = item.index_command[0];
                        ((PatchesItemAuto) patchesList.get(11)).origByte[3] = item.index_command[1];
                        ((PatchesItemAuto) patchesList.get(11)).pattern = true;
                    }
                }
                PatchesItemAuto[] patchList = new PatchesItemAuto[patchesList.size()];
                int u = 0;
                it = patchesList.iterator();
                while (it.hasNext()) {
                    patchList[u] = (PatchesItemAuto) it.next();
                    u++;
                }
                long time = System.currentTimeMillis();
                FileChannel ChannelDex = new RandomAccessFile(file_patch, InternalZipConstants.WRITE_MODE).getChannel();
                Utils.sendFromRoot("Size file:" + ChannelDex.size());
                MappedByteBuffer fileBytes = ChannelDex.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex.size()));
                boolean mark5 = false;
                int start_for_diaposon = 0;
                int start_for_diaposon_pak = 0;
                int curentPos2 = 0;
                int period = 0;
                while (fileBytes.hasRemaining()) {
                    int i;
                    byte prufbyte;
                    if (!createAPK && fileBytes.position() - period > 149999) {
                        Utils.sendFromRoot("Progress size:" + fileBytes.position());
                        period = fileBytes.position();
                    }
                    int curentPos = fileBytes.position();
                    byte curentByte = fileBytes.get();
                    if (mark5 || false) {
                        curentPos2++;
                    }
                    if (curentPos2 > 380) {
                        mark5 = false;
                        curentPos2 = 0;
                    }
                    if (curentByte == bArr[0] && pattern1) {
                        if (bArr4[0] == (byte) 0) {
                            bArr3[0] = curentByte;
                        }
                        i = 1;
                        int c_no_license = 1000;
                        int c_license = 1000;
                        fileBytes.position(curentPos + 1);
                        prufbyte = fileBytes.get();
                        byte repbyte_no_license = (byte) 0;
                        byte repbyte_license = (byte) 0;
                        while (true) {
                            if (prufbyte != bArr[i] && bArr2[i] == (byte) 0) {
                                break;
                            }
                            if (bArr4[i] == (byte) 2 && prufbyte == fileBytes.get(fileBytes.position() + 7)) {
                                c_license = i;
                                repbyte_no_license = prufbyte;
                            }
                            if (bArr4[i] == (byte) 0 || bArr4[i] == (byte) 2) {
                                bArr3[i] = prufbyte;
                            }
                            if (bArr4[i] == (byte) 3) {
                                repbyte_license = prufbyte;
                                c_no_license = i;
                            }
                            i++;
                            if (i == bArr.length) {
                                break;
                            }
                            prufbyte = fileBytes.get();
                        }
                        if (c_no_license < 1000 && repbyte_license != (byte) 0) {
                            bArr3[c_no_license] = repbyte_no_license;
                            if (c_license + 7 < 1000 && repbyte_no_license != (byte) 0) {
                                bArr3[c_license + 8] = repbyte_license;
                                fileBytes.position(curentPos);
                                fileBytes.put(bArr3);
                                fileBytes.force();
                                Utils.sendFromRoot("lvl patch N1!\n");
                                print.print("lvl patch N1!\n");
                            }
                        }
                    }
                    ArrayList<Byte> move_byte = new ArrayList();
                    ArrayList<Integer> move_byte_position = new ArrayList();
                    boolean increase = false;
                    int b = 0;
                    while (b < patchList.length) {
                        PatchesItemAuto curpatch;
                        PatchesItemAuto patches = patchList[b];
                        fileBytes.position(curentPos);
                        if (patches.markerTrig && (b == 5 || b == 6 || b == 7 || b == 8 || b == 9 || b == 10)) {
                            if (!increase) {
                                start_for_diaposon_pak++;
                                increase = true;
                            }
                            if (start_for_diaposon_pak < 40) {
                                if (curentByte == patches.origByte[0]) {
                                    move_byte.clear();
                                    move_byte_position.clear();
                                    if (patches.repMask[0] == 0) {
                                        patches.repByte[0] = curentByte;
                                    }
                                    i = 1;
                                    fileBytes.position(curentPos + 1);
                                    prufbyte = fileBytes.get();
                                    while (true) {
                                        if (prufbyte != patches.origByte[i] && patches.origMask[i] != 1 && patches.origMask[i] != 20 && patches.origMask[i] != 21 && patches.origMask[i] != 23) {
                                            break;
                                        }
                                        if (patches.repMask[i] == 0) {
                                            patches.repByte[i] = prufbyte;
                                        }
                                        if (patches.repMask[i] == 20) {
                                            patches.repByte[i] = (byte) (prufbyte & 15);
                                        }
                                        if (patches.repMask[i] == 21) {
                                            patches.repByte[i] = (byte) ((prufbyte & 15) + 16);
                                        }
                                        if (patches.origMask[i] == 23) {
                                            move_byte.add(patches.origByte[i], Byte.valueOf(prufbyte));
                                        }
                                        if (patches.repMask[i] == 23) {
                                            move_byte_position.add(patches.repByte[i], Integer.valueOf(i));
                                        }
                                        i++;
                                        if (i == patches.origByte.length) {
                                            break;
                                        }
                                        prufbyte = fileBytes.get();
                                    }
                                }
                                fileBytes.position(curentPos + 1);
                            } else {
                                patches.markerTrig = false;
                                it = patchesList.iterator();
                                while (it.hasNext()) {
                                    curpatch = (PatchesItemAuto) it.next();
                                    if (curpatch.marker.equals(patches.marker)) {
                                        curpatch.markerTrig = false;
                                    }
                                }
                                start_for_diaposon_pak = 0;
                                fileBytes.position(curentPos + 1);
                            }
                        }
                        if (patches.markerTrig && b == 4) {
                            start_for_diaposon++;
                            if (start_for_diaposon < 90) {
                                if (curentByte == patches.origByte[0]) {
                                    move_byte.clear();
                                    move_byte_position.clear();
                                    if (patches.repMask[0] == 0) {
                                        patches.repByte[0] = curentByte;
                                    }
                                    i = 1;
                                    fileBytes.position(curentPos + 1);
                                    prufbyte = fileBytes.get();
                                    while (true) {
                                        if (prufbyte != patches.origByte[i] && patches.origMask[i] != 1 && patches.origMask[i] != 20 && patches.origMask[i] != 21 && patches.origMask[i] != 23) {
                                            break;
                                        }
                                        if (patches.repMask[i] == 0) {
                                            patches.repByte[i] = prufbyte;
                                        }
                                        if (patches.repMask[i] == 20) {
                                            patches.repByte[i] = (byte) (prufbyte & 15);
                                        }
                                        if (patches.repMask[i] == 21) {
                                            patches.repByte[i] = (byte) ((prufbyte & 15) + 16);
                                        }
                                        if (patches.origMask[i] == 23) {
                                            move_byte.add(patches.origByte[i], Byte.valueOf(prufbyte));
                                        }
                                        if (patches.repMask[i] == 23) {
                                            move_byte_position.add(patches.repByte[i], Integer.valueOf(i));
                                        }
                                        i++;
                                        if (i == patches.origByte.length) {
                                            break;
                                        }
                                        prufbyte = fileBytes.get();
                                    }
                                }
                                fileBytes.position(curentPos + 1);
                            } else {
                                patches.markerTrig = false;
                                it = patchesList.iterator();
                                while (it.hasNext()) {
                                    curpatch = (PatchesItemAuto) it.next();
                                    if (curpatch.marker.equals(patches.marker)) {
                                        curpatch.markerTrig = false;
                                    }
                                }
                                start_for_diaposon = 0;
                                fileBytes.position(curentPos + 1);
                            }
                        }
                        if (!patches.markerTrig && curentByte == patches.origByte[0] && patches.pattern) {
                            move_byte.clear();
                            move_byte_position.clear();
                            if (patches.repMask[0] == 0) {
                                patches.repByte[0] = curentByte;
                            }
                            i = 1;
                            fileBytes.position(curentPos + 1);
                            prufbyte = fileBytes.get();
                            while (true) {
                                if (prufbyte != patches.origByte[i] && patches.origMask[i] != 1 && patches.origMask[i] != 20 && patches.origMask[i] != 21 && patches.origMask[i] != 23) {
                                    break;
                                }
                                if (patches.repMask[i] == 0) {
                                    patches.repByte[i] = prufbyte;
                                }
                                if (patches.repMask[i] == 20) {
                                    patches.repByte[i] = (byte) (prufbyte & 15);
                                }
                                if (patches.repMask[i] == 21) {
                                    patches.repByte[i] = (byte) ((prufbyte & 15) + 16);
                                }
                                if (patches.origMask[i] == 23) {
                                    move_byte.add(patches.origByte[i], Byte.valueOf(prufbyte));
                                }
                                if (patches.repMask[i] == 23) {
                                    move_byte_position.add(patches.repByte[i], Integer.valueOf(i));
                                }
                                i++;
                                if (i == patches.origByte.length) {
                                    break;
                                }
                                prufbyte = fileBytes.get();
                            }
                            fileBytes.position(curentPos + 1);
                        }
                        b++;
                    }
                    if (curentByte == bArr5[0] && pattern4) {
                        if (bArr8[0] == (byte) 0) {
                            bArr7[0] = curentByte;
                        }
                        i = 1;
                        fileBytes.position(curentPos + 1);
                        prufbyte = fileBytes.get();
                        while (true) {
                            if (prufbyte != bArr5[i] && bArr6[i] != (byte) 1) {
                                break;
                            }
                            if (bArr8[i] == (byte) 0) {
                                bArr7[i] = prufbyte;
                            }
                            i++;
                            if (i == bArr5.length) {
                                break;
                            }
                            prufbyte = fileBytes.get();
                        }
                    }
                    if (curentByte < (byte) 16 && mark5 && pattern4) {
                        if (bArr12[0] == (byte) 0) {
                            bArr11[0] = curentByte;
                        }
                        i = 1;
                        fileBytes.position(curentPos + 1);
                        prufbyte = fileBytes.get();
                        while (true) {
                            if (prufbyte != bArr9[i] && bArr10[i] != (byte) 1) {
                                break;
                            }
                            if (bArr12[i] == (byte) 0) {
                                bArr11[i] = prufbyte;
                            }
                            i++;
                            if (i == bArr9.length) {
                                break;
                            }
                            prufbyte = fileBytes.get();
                        }
                    }
                    fileBytes.position(curentPos + 1);
                }
                ChannelDex.close();
                Utils.sendFromRoot(BuildConfig.VERSION_NAME + ((System.currentTimeMillis() - time) / 1000));
                Utils.sendFromRoot("Analise Results:");
            }
        } catch (FileNotFoundException localFileNotFoundException2) {
            Utils.sendFromRoot("Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*");
            localFileNotFoundException2.printStackTrace();
        } catch (Exception e62) {
            Utils.sendFromRoot("Exception e" + e62.toString());
            e62.printStackTrace();
            e62.printStackTrace();
        }
        it = filestopatch.iterator();
        while (it.hasNext()) {
            Utils.fixadler((File) it.next());
            clearTempSD();
        }
        if (!createAPK) {
            int r = Utils.create_ODEX_root(paramArrayOfString[3], classesFiles, paramArrayOfString[2], uid, Utils.getOdexForCreate(paramArrayOfString[2], uid));
            Utils.sendFromRoot("chelpus_return_" + r);
            if (r == 0 && !ART) {
                Utils.afterPatch(paramArrayOfString[1], paramArrayOfString[2], Utils.getPlaceForOdex(paramArrayOfString[2], true), uid, paramArrayOfString[3]);
            }
        }
        if (!createAPK) {
            Utils.exitFromRootJava();
        }
        result = logOutputStream.allresult;
    }

    public static void clearTemp() {
        try {
            File tempdex = new File(dir + "/AndroidManifest.xml");
            if (tempdex.exists()) {
                tempdex.delete();
            }
            if (classesFiles != null && classesFiles.size() > 0) {
                Iterator it = classesFiles.iterator();
                while (it.hasNext()) {
                    File cl = (File) it.next();
                    if (cl.exists()) {
                        cl.delete();
                    }
                }
            }
            tempdex = new File(dir + "/classes.dex");
            if (tempdex.exists()) {
                tempdex.delete();
            }
            tempdex = new File(dir + "/classes.dex.apk");
            if (tempdex.exists()) {
                tempdex.delete();
            }
        } catch (Exception e) {
            Utils.sendFromRoot(BuildConfig.VERSION_NAME + e.toString());
        }
    }

    public static boolean byteverify(MappedByteBuffer fileBytes, int curentPos, byte curentByte, byte[] byteOrig, byte[] mask, byte[] byteReplace, byte[] rep_mask, String log, boolean pattern) {
        if (curentByte == byteOrig[0] && pattern) {
            if (rep_mask[0] == (byte) 0) {
                byteReplace[0] = curentByte;
            }
            int i = 1;
            fileBytes.position(curentPos + 1);
            byte prufbyte = fileBytes.get();
            while (true) {
                if (prufbyte != byteOrig[i] && mask[i] != (byte) 1) {
                    break;
                }
                if (rep_mask[i] == (byte) 0) {
                    byteReplace[i] = prufbyte;
                }
                i++;
                if (i == byteOrig.length) {
                    fileBytes.position(curentPos);
                    fileBytes.put(byteReplace);
                    fileBytes.force();
                    Utils.sendFromRoot(log);
                    return true;
                }
                prufbyte = fileBytes.get();
            }
            fileBytes.position(curentPos + 1);
        }
        return false;
    }

    public static void unzipSD(File apk) {
        try {
            FileInputStream fin = new FileInputStream(apk);
            ZipInputStream zin = new ZipInputStream(fin);
            while (true) {
                ZipEntry ze = zin.getNextEntry();
                if (ze == null) {
                    zin.close();
                    fin.close();
                    return;
                } else if (ze.getName().toLowerCase().startsWith("classes") && ze.getName().endsWith(".dex") && !ze.getName().contains(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                    FileOutputStream fout = new FileOutputStream(sddir + "/Modified/" + ze.getName());
                    byte[] buffer = new byte[Util.BLOCK_HEADER_SIZE_MAX];
                    while (true) {
                        int length = zin.read(buffer);
                        if (length == -1) {
                            break;
                        }
                        fout.write(buffer, 0, length);
                    }
                    zin.closeEntry();
                    fout.close();
                    classesFiles.add(new File(sddir + "/Modified/" + ze.getName()));
                }
            }
        } catch (Exception e) {
            try {
                new ZipFile(apk).extractFile("classes.dex", sddir + "/Modified/");
                classesFiles.add(new File(sddir + "/Modified/" + "classes.dex"));
            } catch (ZipException e1) {
                Utils.sendFromRoot("Error classes.dex decompress! " + e1);
                Utils.sendFromRoot("Exception e1" + e.toString());
            } catch (Exception e12) {
                Utils.sendFromRoot("Error classes.dex decompress! " + e12);
                Utils.sendFromRoot("Exception e1" + e.toString());
            }
        }
    }

    public static void unzipART(File apk) {
        boolean found2 = false;
        try {
            FileInputStream fin = new FileInputStream(apk);
            ZipInputStream zipInputStream = new ZipInputStream(fin);
            for (ZipEntry ze = zipInputStream.getNextEntry(); ze != null && true; ze = zipInputStream.getNextEntry()) {
                String haystack = ze.getName();
                if (haystack.toLowerCase().startsWith("classes") && haystack.endsWith(".dex") && !haystack.contains(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                    FileOutputStream fout = new FileOutputStream(sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + haystack);
                    byte[] buffer = new byte[InternalZipConstants.UFT8_NAMES_FLAG];
                    while (true) {
                        int length = zipInputStream.read(buffer);
                        if (length == -1) {
                            break;
                        }
                        fout.write(buffer, 0, length);
                    }
                    zipInputStream.closeEntry();
                    fout.close();
                    classesFiles.add(new File(sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + haystack));
                    Utils.cmdParam("chmod", "777", sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + haystack);
                }
                if (haystack.equals("AndroidManifest.xml")) {
                    FileOutputStream fout2 = new FileOutputStream(sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + "AndroidManifest.xml");
                    byte[] buffer2 = new byte[InternalZipConstants.UFT8_NAMES_FLAG];
                    while (true) {
                        int length2 = zipInputStream.read(buffer2);
                        if (length2 == -1) {
                            break;
                        }
                        fout2.write(buffer2, 0, length2);
                    }
                    zipInputStream.closeEntry();
                    fout2.close();
                    Utils.cmdParam("chmod", "777", sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + "AndroidManifest.xml");
                    found2 = true;
                }
                if (false && found2) {
                    break;
                }
            }
            zipInputStream.close();
            fin.close();
        } catch (Exception e) {
            try {
                ZipFile zipFile = new ZipFile(apk);
                zipFile.extractFile("classes.dex", sddir);
                classesFiles.add(new File(sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + "classes.dex"));
                Utils.cmdParam("chmod", "777", sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + "classes.dex");
                zipFile.extractFile("AndroidManifest.xml", sddir);
                Utils.cmdParam("chmod", "777", sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + "AndroidManifest.xml");
            } catch (ZipException e1) {
                Utils.sendFromRoot("Error classes.dex decompress! " + e1);
                Utils.sendFromRoot("Exception e1" + e.toString());
            } catch (Exception e12) {
                Utils.sendFromRoot("Error classes.dex decompress! " + e12);
                Utils.sendFromRoot("Exception e1" + e.toString());
            }
            Utils.sendFromRoot("Exception e" + e.toString());
        }
    }

    public static void clearTempSD() {
        try {
            File tempdex = new File(sddir + "/Modified/classes.dex.apk");
            if (tempdex.exists()) {
                tempdex.delete();
            }
        } catch (Exception e) {
            Utils.sendFromRoot(BuildConfig.VERSION_NAME + e.toString());
        }
    }
}
