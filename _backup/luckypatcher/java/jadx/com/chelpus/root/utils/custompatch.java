package com.chelpus.root.utils;

import android.database.sqlite.SQLiteDatabase;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.LogCollector;
import com.android.vending.billing.InAppBillingService.LUCK.PatchesItem;
import com.android.vending.billing.InAppBillingService.LUCK.SearchItem;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.nio.BufferUnderflowException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.util.InternalZipConstants;
import org.json.JSONException;
import org.json.JSONObject;

public class custompatch {
    public static boolean ART = false;
    static final int BUFFER = 2048;
    public static final byte[] MAGIC = new byte[]{(byte) 100, (byte) 101, (byte) 121, (byte) 10, (byte) 48, (byte) 51, (byte) 53, (byte) 0};
    public static boolean OdexPatch = false;
    public static int adler = 0;
    public static boolean armv7 = false;
    public static ArrayList<File> arrayFile2 = new ArrayList();
    private static final int beginTag = 0;
    public static ArrayList<File> classesFiles = new ArrayList();
    private static final int classesTag = 1;
    public static boolean convert = false;
    private static String dataBase = BuildConfig.VERSION_NAME;
    private static boolean dataBaseExist = false;
    public static String dir = "/sdcard/";
    public static String dir2 = "/sdcard/";
    public static String dirapp = "/data/app/";
    private static final int endTag = 4;
    private static final int fileInApkTag = 14;
    public static boolean fixunpack = false;
    public static boolean goodResult = false;
    private static String group = BuildConfig.VERSION_NAME;
    private static final int libTagALL = 2;
    private static final int libTagARMEABI = 6;
    private static final int libTagARMEABIV7A = 7;
    private static final int libTagMIPS = 8;
    private static final int libTagx86 = 9;
    public static File localFile2 = null;
    public static String log = BuildConfig.VERSION_NAME;
    public static boolean manualpatch = false;
    public static boolean multidex = false;
    public static boolean multilib_patch = false;
    public static boolean odex = false;
    private static final int odexTag = 10;
    public static boolean odexpatch = false;
    private static final int odexpatchTag = 11;
    private static final int otherfilesTag = 3;
    private static final int packageTag = 5;
    private static ArrayList<PatchesItem> pat = null;
    public static boolean patchteil = false;
    public static String pkgName = BuildConfig.VERSION_NAME;
    public static String sddir = "/data/app/";
    private static ArrayList<Byte> search = null;
    private static String searchStr = BuildConfig.VERSION_NAME;
    private static ArrayList<SearchItem> ser = null;
    private static final int set_copy_file_Tag = 15;
    private static final int set_permissions_Tag = 13;
    private static final int sqlTag = 12;
    public static boolean system = false;
    public static int tag;
    public static String uid = BuildConfig.VERSION_NAME;
    public static boolean unpack = false;
    private static boolean withFramework = true;

    static class C05571 {
        C05571() {
        }
    }

    public static void addToLog(String str) {
        log += str + LogCollector.LINE_SEPARATOR;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void main(String[] paramArrayOfString) {
        try {
            InputStream fileInputStream;
            BufferedReader bufferedReader;
            addToLog("SU Java-Code Running! " + new C05571().getClass().getEnclosingClass().getName());
            Utils.startRootJava(null);
            pkgName = paramArrayOfString[beginTag];
            try {
                Utils.kill(pkgName);
                Utils.remount(paramArrayOfString[libTagALL], InternalZipConstants.WRITE_MODE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Log.d(BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME);
            } catch (UnsatisfiedLinkError e2) {
                addToLog("withoutFramework");
                withFramework = false;
            }
            try {
                if (paramArrayOfString[libTagx86] != null && paramArrayOfString[libTagx86].contains("ART")) {
                    ART = true;
                }
                if (paramArrayOfString[odexTag] != null) {
                    uid = paramArrayOfString[odexTag];
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            try {
                File[] files = new File(paramArrayOfString[endTag]).listFiles();
                int length = files.length;
                for (int i = beginTag; i < length; i += classesTag) {
                    File file = files[i];
                    if (!(!file.isFile() || file.getName().equals("busybox") || file.getName().equals("reboot") || file.getName().equals("dalvikvm"))) {
                        file.delete();
                    }
                }
            } catch (Exception e32) {
                e32.printStackTrace();
            }
            sddir = paramArrayOfString[otherfilesTag];
            dir = paramArrayOfString[endTag];
            dir2 = paramArrayOfString[endTag];
            dirapp = paramArrayOfString[libTagALL];
            clearTemp();
            String dalvikfixfile = BuildConfig.VERSION_NAME;
            String finalText = BuildConfig.VERSION_NAME;
            String beginText = BuildConfig.VERSION_NAME;
            boolean error = false;
            boolean end = false;
            boolean dalvikfix = false;
            if (paramArrayOfString[libTagARMEABI].equals("not_system")) {
                system = false;
            }
            if (paramArrayOfString[libTagARMEABI].equals("system")) {
                system = true;
            }
            if (system) {
                File appapk = new File(dirapp);
                File appodex = new File(Utils.getPlaceForOdex(dirapp, true));
                if (appapk.exists() && appodex.exists() && !Utils.classes_test(appapk)) {
                    odexpatch = true;
                    localFile2 = appodex;
                    addToLog("\nOdex Application.\nOnly ODEX patch is enabled.\n");
                }
            }
            String line = BuildConfig.VERSION_NAME;
            OdexPatch = false;
            try {
                fileInputStream = new FileInputStream(paramArrayOfString[classesTag]);
                bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream, "UTF-8"));
                while (true) {
                    line = bufferedReader.readLine();
                    if (line == null) {
                        break;
                    }
                    if (line.toUpperCase().contains("[ODEX-PATCH]")) {
                        OdexPatch = true;
                    }
                    if (line.toUpperCase().contains("[LIB-ARMEABI-V7A]")) {
                        armv7 = true;
                    }
                }
                bufferedReader.close();
                fileInputStream.close();
            } catch (IOException e4) {
            }
            searchDalvik(paramArrayOfString[libTagALL]);
            try {
                fileInputStream = new FileInputStream(paramArrayOfString[classesTag]);
                Reader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
                bufferedReader = new BufferedReader(inputStreamReader);
                String data = BuildConfig.VERSION_NAME;
                String[] txtdata = new String[2000];
                new String[classesTag][beginTag] = BuildConfig.VERSION_NAME;
                byte[] byteOrig = null;
                int[] mask = null;
                String file_name = BuildConfig.VERSION_NAME;
                String permissionsSet = BuildConfig.VERSION_NAME;
                String path_for_copy = BuildConfig.VERSION_NAME;
                boolean result = true;
                boolean sumresult = true;
                boolean libr = false;
                boolean begin = false;
                boolean mark_search = false;
                boolean other = false;
                boolean permissions = false;
                boolean copy_file = false;
                String value1 = BuildConfig.VERSION_NAME;
                String value2 = BuildConfig.VERSION_NAME;
                String value3 = BuildConfig.VERSION_NAME;
                pat = new ArrayList();
                ser = new ArrayList();
                search = new ArrayList();
                int r = beginTag;
                while (true) {
                    data = bufferedReader.readLine();
                    if (data != null) {
                        File file2;
                        String[] strArr;
                        if (!data.equals(BuildConfig.VERSION_NAME)) {
                            data = Utils.apply_TAGS(data, pkgName);
                        }
                        txtdata[r] = data;
                        if (txtdata[r].toUpperCase().contains("[PACKAGE]")) {
                            tag = packageTag;
                            unpack = true;
                            File file3 = new File(paramArrayOfString[libTagALL]);
                            if (!file3.exists()) {
                                file3 = new File(paramArrayOfString[libTagALL].replace("-1/", "-2/"));
                                if (file3.exists()) {
                                    paramArrayOfString[libTagALL] = paramArrayOfString[libTagALL].replace("-1/", "-2/");
                                }
                            }
                            if (!file3.exists()) {
                                file3 = new File(paramArrayOfString[libTagALL].replace("-1/", InternalZipConstants.ZIP_FILE_SEPARATOR));
                                if (file3.exists()) {
                                    paramArrayOfString[libTagALL] = paramArrayOfString[libTagALL].replace("-1/", BuildConfig.VERSION_NAME);
                                }
                            }
                            unzip(file3);
                            if (!(odexpatch || OdexPatch)) {
                                String odexstr = Utils.getPlaceForOdex(paramArrayOfString[libTagALL], true);
                                File odexfile = new File(odexstr);
                                if (odexfile.exists()) {
                                    odexfile.delete();
                                }
                                file2 = new File(odexstr.replace("-1", "-2"));
                                if (file2.exists()) {
                                    file2.delete();
                                }
                                file2 = new File(odexstr.replace("-2", "-1"));
                                if (file2.exists()) {
                                    file2.delete();
                                }
                            }
                        }
                        if (begin && (txtdata[r].contains("[") || txtdata[r].contains("]") || txtdata[r].contains("{"))) {
                            addToLog(BuildConfig.VERSION_NAME + beginText + LogCollector.LINE_SEPARATOR);
                            begin = false;
                        }
                        if (begin) {
                            beginText = beginText + LogCollector.LINE_SEPARATOR + txtdata[r];
                        }
                        if (txtdata[r].contains("[") && txtdata[r].contains("]")) {
                            Iterator it;
                            switch (tag) {
                                case classesTag /*1*/:
                                    if (!odexpatch) {
                                        if (unpack) {
                                            if (classesFiles != null && classesFiles.size() > 0) {
                                                if (classesFiles.size() > classesTag) {
                                                    multidex = true;
                                                }
                                                it = classesFiles.iterator();
                                                while (it.hasNext()) {
                                                    File cl = (File) it.next();
                                                    localFile2 = cl;
                                                    if (pat.size() > 0) {
                                                        addToLog("---------------------------------");
                                                        addToLog("Patch for " + cl.getName() + ":");
                                                        addToLog("---------------------------------\n");
                                                        if (!searchStr.equals(BuildConfig.VERSION_NAME)) {
                                                            addToLog(searchStr);
                                                        }
                                                        if (!manualpatch) {
                                                            result = patchProcess(pat);
                                                        }
                                                        if (!result) {
                                                            sumresult = false;
                                                        }
                                                    }
                                                }
                                                multidex = false;
                                                goodResult = false;
                                                ser.clear();
                                                pat.clear();
                                                tag = 200;
                                                searchStr = BuildConfig.VERSION_NAME;
                                                break;
                                            }
                                        }
                                        searchDalvik(paramArrayOfString[libTagALL]);
                                        if (new File(Utils.getPlaceForOdex(dirapp, true)).exists() && !localFile2.getName().endsWith(".odex")) {
                                            new File(Utils.getPlaceForOdex(dirapp, true)).delete();
                                            addToLog("---------------------------------");
                                            addToLog("odex file removed before\npatch for dalvik-cache...");
                                            addToLog("---------------------------------\n");
                                        }
                                        if (pat.size() > 0) {
                                            addToLog("---------------------------------");
                                            addToLog("Patch for dalvik-cache:");
                                            addToLog("---------------------------------\n");
                                            if (!searchStr.equals(BuildConfig.VERSION_NAME)) {
                                                addToLog(searchStr);
                                            }
                                            if (!manualpatch) {
                                                result = patchProcess(pat);
                                            }
                                            if (!result) {
                                                sumresult = false;
                                            }
                                            ser.clear();
                                            pat.clear();
                                            tag = 200;
                                            searchStr = BuildConfig.VERSION_NAME;
                                            break;
                                        }
                                    }
                                    localFile2 = new File(Utils.getPlaceForOdex(dirapp, true));
                                    if (pat.size() > 0) {
                                        addToLog("---------------------------------");
                                        addToLog("classes.dex not found!\nApply patch for odex:");
                                        addToLog("---------------------------------\n");
                                        if (!searchStr.equals(BuildConfig.VERSION_NAME)) {
                                            addToLog(searchStr);
                                        }
                                        if (!manualpatch) {
                                            result = patchProcess(pat);
                                        }
                                        if (!result) {
                                            sumresult = false;
                                        }
                                        ser.clear();
                                        pat.clear();
                                        tag = 200;
                                        searchStr = BuildConfig.VERSION_NAME;
                                        break;
                                    }
                                    break;
                                case libTagALL /*2*/:
                                    convert = false;
                                    if (pat.size() > 0 && arrayFile2 != null && arrayFile2.size() > 0) {
                                        it = arrayFile2.iterator();
                                        while (it.hasNext()) {
                                            localFile2 = (File) it.next();
                                            addToLog("---------------------------");
                                            addToLog("Patch for libraries \n" + localFile2.getAbsolutePath() + ":");
                                            addToLog("---------------------------\n");
                                            if (!searchStr.equals(BuildConfig.VERSION_NAME)) {
                                                addToLog(searchStr);
                                            }
                                            result = patchProcess(pat);
                                            if (!result) {
                                                sumresult = false;
                                            }
                                        }
                                        multilib_patch = false;
                                        goodResult = false;
                                        arrayFile2.clear();
                                    }
                                    ser.clear();
                                    pat.clear();
                                    tag = 200;
                                    searchStr = BuildConfig.VERSION_NAME;
                                    break;
                                case otherfilesTag /*3*/:
                                    convert = false;
                                    if (pat.size() > 0) {
                                        if (!manualpatch) {
                                            result = patchProcess(pat);
                                        }
                                        if (!result) {
                                            sumresult = false;
                                        }
                                    }
                                    ser.clear();
                                    pat.clear();
                                    tag = 200;
                                    searchStr = BuildConfig.VERSION_NAME;
                                    break;
                                case libTagARMEABI /*6*/:
                                    convert = false;
                                    if (pat.size() > 0 && arrayFile2 != null && arrayFile2.size() > 0) {
                                        it = arrayFile2.iterator();
                                        while (it.hasNext()) {
                                            localFile2 = (File) it.next();
                                            addToLog("--------------------------------");
                                            addToLog("Patch for (armeabi) libraries \n" + localFile2.getName() + ":");
                                            addToLog("--------------------------------\n");
                                            if (!searchStr.equals(BuildConfig.VERSION_NAME)) {
                                                addToLog(searchStr);
                                            }
                                            result = patchProcess(pat);
                                            if (!result) {
                                                sumresult = false;
                                            }
                                        }
                                        multilib_patch = false;
                                        goodResult = false;
                                        arrayFile2.clear();
                                    }
                                    ser.clear();
                                    pat.clear();
                                    tag = 200;
                                    searchStr = BuildConfig.VERSION_NAME;
                                    break;
                                case libTagARMEABIV7A /*7*/:
                                    convert = false;
                                    if (pat.size() > 0 && arrayFile2 != null && arrayFile2.size() > 0) {
                                        it = arrayFile2.iterator();
                                        while (it.hasNext()) {
                                            localFile2 = (File) it.next();
                                            addToLog("---------------------------------------");
                                            addToLog("Patch for (armeabi-v7a) libraries \n" + localFile2.getName() + ":");
                                            addToLog("---------------------------------------\n");
                                            if (!searchStr.equals(BuildConfig.VERSION_NAME)) {
                                                addToLog(searchStr);
                                            }
                                            result = patchProcess(pat);
                                            if (!result) {
                                                sumresult = false;
                                            }
                                        }
                                        multilib_patch = false;
                                        goodResult = false;
                                        arrayFile2.clear();
                                    }
                                    ser.clear();
                                    pat.clear();
                                    tag = 200;
                                    searchStr = BuildConfig.VERSION_NAME;
                                    break;
                                case libTagMIPS /*8*/:
                                    convert = false;
                                    if (pat.size() > 0 && arrayFile2 != null && arrayFile2.size() > 0) {
                                        it = arrayFile2.iterator();
                                        while (it.hasNext()) {
                                            localFile2 = (File) it.next();
                                            addToLog("---------------------------");
                                            addToLog("Patch for (MIPS) libraries \n" + localFile2.getName() + ":");
                                            addToLog("---------------------------\n");
                                            if (!searchStr.equals(BuildConfig.VERSION_NAME)) {
                                                addToLog(searchStr);
                                            }
                                            result = patchProcess(pat);
                                            if (!result) {
                                                sumresult = false;
                                            }
                                        }
                                        multilib_patch = false;
                                        goodResult = false;
                                        arrayFile2.clear();
                                    }
                                    ser.clear();
                                    pat.clear();
                                    tag = 200;
                                    searchStr = BuildConfig.VERSION_NAME;
                                    break;
                                case libTagx86 /*9*/:
                                    convert = false;
                                    if (pat.size() > 0 && arrayFile2 != null && arrayFile2.size() > 0) {
                                        it = arrayFile2.iterator();
                                        while (it.hasNext()) {
                                            localFile2 = (File) it.next();
                                            addToLog("---------------------------");
                                            addToLog("Patch for (x86) libraries \n" + localFile2.getName() + ":");
                                            addToLog("---------------------------\n");
                                            if (!searchStr.equals(BuildConfig.VERSION_NAME)) {
                                                addToLog(searchStr);
                                            }
                                            result = patchProcess(pat);
                                            if (!result) {
                                                sumresult = false;
                                            }
                                        }
                                        multilib_patch = false;
                                        goodResult = false;
                                        arrayFile2.clear();
                                    }
                                    ser.clear();
                                    pat.clear();
                                    tag = 200;
                                    searchStr = BuildConfig.VERSION_NAME;
                                    break;
                                case odexTag /*10*/:
                                    convert = false;
                                    if (pat.size() > 0) {
                                        addToLog("---------------------------------");
                                        addToLog("Dalvik-cache fixed to odex!\nPatch for odex:");
                                        addToLog("---------------------------------\n");
                                        if (!searchStr.equals(BuildConfig.VERSION_NAME)) {
                                            addToLog(searchStr);
                                        }
                                        result = patchProcess(pat);
                                        if (!result) {
                                            sumresult = false;
                                        }
                                    }
                                    addToLog("---------------------------------");
                                    addToLog("Dalvik-cache fixed to odex!");
                                    addToLog("---------------------------------\n");
                                    if (!searchStr.equals(BuildConfig.VERSION_NAME)) {
                                        addToLog(searchStr);
                                    }
                                    if (!unpack) {
                                        searchDalvik(paramArrayOfString[libTagALL]);
                                    }
                                    searchDalvikOdex(paramArrayOfString[beginTag], paramArrayOfString[libTagALL]);
                                    if (localFile2.exists()) {
                                        dalvikfix = true;
                                        dalvikfixfile = localFile2.getAbsolutePath();
                                    }
                                    ser.clear();
                                    pat.clear();
                                    tag = 200;
                                    searchStr = BuildConfig.VERSION_NAME;
                                    break;
                                case odexpatchTag /*11*/:
                                    convert = false;
                                    addToLog("---------------------------");
                                    addToLog("Patch for odex:");
                                    addToLog("---------------------------\n");
                                    if (!searchStr.equals(BuildConfig.VERSION_NAME)) {
                                        addToLog(searchStr);
                                    }
                                    localFile2 = new File(Utils.getPlaceForOdex(dirapp, true));
                                    if (!localFile2.exists()) {
                                        addToLog("Odex not found! Please use befor other Patch, and after run Custom Patch!");
                                    }
                                    if (localFile2.length() == 0) {
                                        localFile2.delete();
                                        addToLog("Odex not found! Please use befor other Patch, and after run Custom Patch!");
                                    }
                                    unpack = false;
                                    odex = false;
                                    if (pat.size() > 0) {
                                        result = patchProcess(pat);
                                        if (!result) {
                                            sumresult = false;
                                        }
                                    }
                                    ser.clear();
                                    pat.clear();
                                    tag = 200;
                                    searchStr = BuildConfig.VERSION_NAME;
                                    break;
                                case sqlTag /*12*/:
                                    convert = false;
                                    ser.clear();
                                    pat.clear();
                                    tag = 200;
                                    searchStr = BuildConfig.VERSION_NAME;
                                    break;
                                case set_permissions_Tag /*13*/:
                                    permissions = false;
                                    if (localFile2.exists()) {
                                        addToLog("---------------------------");
                                        addToLog("Set permissions " + permissionsSet + " for file:\n" + file_name);
                                        addToLog("---------------------------\n");
                                        strArr = new String[otherfilesTag];
                                        strArr[beginTag] = "chmod";
                                        strArr[classesTag] = BuildConfig.VERSION_NAME + permissionsSet;
                                        strArr[libTagALL] = localFile2.getAbsolutePath();
                                        Utils.run_all_no_root(strArr);
                                        break;
                                    }
                                    break;
                                case fileInApkTag /*14*/:
                                    addToLog("---------------------------");
                                    addToLog("Patch for file from apk:\n");
                                    addToLog("---------------------------\n");
                                    addToLog("You must run rebuild for this application with custom patch, then patch will work.\n");
                                    break;
                                case set_copy_file_Tag /*15*/:
                                    copy_file = false;
                                    File dir_for_copy = Utils.getDirs(new File(path_for_copy));
                                    dir_for_copy.mkdirs();
                                    strArr = new String[otherfilesTag];
                                    strArr[beginTag] = "chmod";
                                    strArr[classesTag] = "777";
                                    strArr[libTagALL] = dir_for_copy.getAbsolutePath();
                                    Utils.run_all_no_root(strArr);
                                    String dir_for_test = dir_for_copy.getAbsolutePath();
                                    addToLog("---------------------------");
                                    addToLog("Copy file " + file_name + " to:\n" + path_for_copy);
                                    addToLog("---------------------------\n");
                                    if (!new File(sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + file_name).exists()) {
                                        addToLog("Error: File " + file_name + " not found to dir\n" + sddir + InternalZipConstants.ZIP_FILE_SEPARATOR);
                                        break;
                                    }
                                    Utils.copyFile(new File(sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + file_name), new File(path_for_copy));
                                    if (new File(path_for_copy).exists() && new File(path_for_copy).length() == new File(sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + file_name).length()) {
                                        addToLog("File copied success.");
                                    } else {
                                        addToLog("File copied with error. Try again.");
                                    }
                                    strArr = new String[otherfilesTag];
                                    strArr[beginTag] = "chmod";
                                    strArr[classesTag] = "777";
                                    strArr[libTagALL] = path_for_copy;
                                    Utils.run_all_no_root(strArr);
                                    break;
                            }
                        }
                        if (txtdata[r].toUpperCase().contains("[BEGIN]")) {
                            tag = beginTag;
                            begin = true;
                        }
                        if (txtdata[r].toUpperCase().contains("[CLASSES]")) {
                            tag = classesTag;
                            if (unpack) {
                                localFile2 = new File(dir + InternalZipConstants.ZIP_FILE_SEPARATOR + "classes.dex");
                            }
                        }
                        if (txtdata[r].toUpperCase().contains("[ODEX]")) {
                            tag = odexTag;
                        }
                        if (txtdata[r].toUpperCase().contains("[SQLITE]")) {
                            tag = sqlTag;
                        }
                        if (tag == sqlTag) {
                            ser.clear();
                            pat.clear();
                            if (txtdata[r].contains("database")) {
                                try {
                                    File db;
                                    value1 = new JSONObject(txtdata[r]).getString("database");
                                    file2 = new File(value1);
                                    strArr = new String[otherfilesTag];
                                    strArr[beginTag] = "chmod";
                                    strArr[classesTag] = "777";
                                    strArr[libTagALL] = value1;
                                    Utils.run_all_no_root(strArr);
                                    if (!withFramework) {
                                        Utils.copyFile(file2, new File(dir + InternalZipConstants.ZIP_FILE_SEPARATOR + file2.getName()));
                                    }
                                    if (file2.exists()) {
                                        dataBaseExist = true;
                                    } else {
                                        localFile2 = null;
                                        searchfile(paramArrayOfString[beginTag], file2.getName());
                                        if (localFile2 == null || !localFile2.exists()) {
                                            value1 = "Error LP: File of Database not Found!";
                                        } else {
                                            db = localFile2;
                                            strArr = new String[otherfilesTag];
                                            strArr[beginTag] = "chmod";
                                            strArr[classesTag] = "777";
                                            strArr[libTagALL] = value1;
                                            Utils.run_all_no_root(strArr);
                                            if (!withFramework) {
                                                Utils.copyFile(db, new File(dir + InternalZipConstants.ZIP_FILE_SEPARATOR + db.getName()));
                                            }
                                            dataBaseExist = true;
                                        }
                                    }
                                    if (dataBaseExist) {
                                        dataBase = db.getAbsolutePath();
                                    }
                                    addToLog("---------------------------");
                                    addToLog("Open SqLite database\n" + db.getAbsolutePath());
                                    addToLog("---------------------------\n");
                                } catch (JSONException e5) {
                                    addToLog("Error LP: Error Name of Database read!");
                                }
                            }
                            if (txtdata[r].contains("execute") && dataBaseExist) {
                                try {
                                    value1 = new JSONObject(txtdata[r]).getString("execute");
                                    addToLog("Execute:\n" + value1);
                                    if (withFramework) {
                                        try {
                                            SQLiteDatabase db3 = SQLiteDatabase.openDatabase(dataBase, null, beginTag);
                                            db3.execSQL(value1);
                                            db3.close();
                                        } catch (Exception e322) {
                                            System.out.println("LuckyPatcher: SQL error - " + e322);
                                        } catch (FileNotFoundException e6) {
                                            addToLog("Custom Patch not Found. It's problem with root. Don't have access to SD card from root.  If you use SuperSu, try disable \"mount namespace separation\" in SuperSu. If it not help, please update SuperSu and update binary file su from SuperSu.");
                                        }
                                    }
                                } catch (JSONException e7) {
                                    addToLog("Error LP: Error SQL exec read!");
                                }
                            }
                        }
                        if (other) {
                            ser.clear();
                            pat.clear();
                            try {
                                value1 = new JSONObject(txtdata[r]).getString("name");
                                addToLog("---------------------------");
                                addToLog("Patch for file \n" + value1 + ":");
                                addToLog("---------------------------\n");
                            } catch (JSONException e8) {
                                addToLog("Error LP: Error name of file read!");
                            }
                            searchfile(paramArrayOfString[beginTag], value1);
                            other = false;
                        }
                        if (permissions) {
                            if (txtdata[r].contains("file_name")) {
                                try {
                                    file_name = new JSONObject(txtdata[r]).getString("file_name");
                                } catch (JSONException e9) {
                                    e9.printStackTrace();
                                    addToLog("Error LP: Error name of file read!");
                                }
                                searchfile(paramArrayOfString[beginTag], file_name);
                            }
                            if (txtdata[r].contains("permissions") && txtdata[r].contains("{") && !txtdata[r].contains("[") && !txtdata[r].contains("]")) {
                                try {
                                    permissionsSet = new JSONObject(txtdata[r]).getString("permissions");
                                } catch (JSONException e10) {
                                    addToLog("Error LP: Error permissions read!");
                                }
                            }
                        }
                        if (copy_file) {
                            if (txtdata[r].contains("file_name")) {
                                try {
                                    file_name = new JSONObject(txtdata[r]).getString("file_name");
                                } catch (JSONException e92) {
                                    e92.printStackTrace();
                                    addToLog("Error LP: Error name of file read!");
                                }
                            }
                            if (txtdata[r].contains("\"to\":") && txtdata[r].contains("{") && !txtdata[r].contains("[") && !txtdata[r].contains("]")) {
                                try {
                                    path_for_copy = new JSONObject(txtdata[r]).getString("to");
                                } catch (JSONException e11) {
                                    addToLog("Error LP: Error file copy read!");
                                }
                            }
                        }
                        if (libr) {
                            ser.clear();
                            pat.clear();
                            try {
                                value1 = new JSONObject(txtdata[r]).getString("name");
                            } catch (JSONException e12) {
                                addToLog("Error LP: Error name of libraries read!");
                            }
                            arrayFile2.clear();
                            arrayFile2 = searchlib(paramArrayOfString[beginTag], value1, paramArrayOfString[libTagALL]);
                            libr = false;
                        }
                        if (txtdata[r].toUpperCase().contains("[LIB]")) {
                            tag = libTagALL;
                            libr = true;
                            other = false;
                            permissions = false;
                            odex = false;
                            unpack = false;
                            copy_file = false;
                        }
                        if (txtdata[r].toUpperCase().contains("[LIB-ARMEABI]")) {
                            if (paramArrayOfString[libTagARMEABIV7A].toLowerCase().equals("armeabi") || (paramArrayOfString[libTagARMEABIV7A].toLowerCase().equals("armeabi-v7a") && !armv7)) {
                                tag = libTagARMEABI;
                                libr = true;
                                other = false;
                                permissions = false;
                                odex = false;
                                unpack = false;
                                copy_file = false;
                            } else {
                                tag = 200;
                            }
                        }
                        if (txtdata[r].toUpperCase().contains("[LIB-ARMEABI-V7A]")) {
                            if (paramArrayOfString[libTagARMEABIV7A].toLowerCase().equals("armeabi-v7a")) {
                                tag = libTagARMEABIV7A;
                                libr = true;
                                other = false;
                                permissions = false;
                                odex = false;
                                unpack = false;
                                copy_file = false;
                            } else {
                                tag = 200;
                            }
                        }
                        if (txtdata[r].toUpperCase().contains("[LIB-MIPS]")) {
                            if (paramArrayOfString[libTagARMEABIV7A].toLowerCase().equals("mips")) {
                                tag = libTagMIPS;
                                libr = true;
                                other = false;
                                permissions = false;
                                odex = false;
                                unpack = false;
                                copy_file = false;
                            } else {
                                tag = 200;
                            }
                        }
                        if (txtdata[r].toUpperCase().contains("[LIB-X86]")) {
                            if (paramArrayOfString[libTagARMEABIV7A].toLowerCase().equals("x86")) {
                                tag = libTagx86;
                                libr = true;
                                other = false;
                                permissions = false;
                                odex = false;
                                unpack = false;
                                copy_file = false;
                            } else {
                                tag = 200;
                            }
                        }
                        if (txtdata[r].toUpperCase().contains("[OTHER FILES]")) {
                            tag = otherfilesTag;
                            libr = false;
                            other = true;
                            permissions = false;
                            odex = false;
                            unpack = false;
                            copy_file = false;
                        }
                        if (txtdata[r].toUpperCase().contains("[SET_PERMISSIONS]")) {
                            tag = set_permissions_Tag;
                            libr = false;
                            other = false;
                            permissions = true;
                            odex = false;
                            unpack = false;
                            copy_file = false;
                        }
                        if (txtdata[r].toUpperCase().contains("[COPY_FILE]")) {
                            tag = set_copy_file_Tag;
                            libr = false;
                            other = false;
                            permissions = false;
                            odex = false;
                            unpack = false;
                            copy_file = true;
                        }
                        if (txtdata[r].toUpperCase().contains("[ODEX-PATCH]")) {
                            tag = odexpatchTag;
                            unpack = false;
                            copy_file = false;
                            libr = false;
                            other = false;
                            permissions = false;
                            odex = true;
                        }
                        if (txtdata[r].toUpperCase().contains("[FILE_IN_APK]")) {
                            tag = fileInApkTag;
                            unpack = false;
                            libr = false;
                            other = false;
                            permissions = false;
                            odex = false;
                            copy_file = false;
                        }
                        if (txtdata[r].contains("group") && txtdata[r].contains("{") && txtdata[r].contains("}")) {
                            try {
                                group = new JSONObject(txtdata[r]).getString("group");
                            } catch (JSONException e13) {
                                addToLog("Error LP: Error original hex read!");
                                group = BuildConfig.VERSION_NAME;
                            }
                        }
                        try {
                            String[] orhex;
                            int t;
                            String[] rephex;
                            int[] rep_mask;
                            byte[] byteReplace;
                            if (txtdata[r].contains("original") && txtdata[r].contains("{") && txtdata[r].contains("}")) {
                                if (mark_search) {
                                    sumresult = searchProcess(ser);
                                    mark_search = false;
                                }
                                try {
                                    value1 = new JSONObject(txtdata[r]).getString("original");
                                } catch (JSONException e14) {
                                    addToLog("Error LP: Error original hex read!");
                                }
                                value1 = value1.trim();
                                if (convert) {
                                    value1 = Utils.rework(value1);
                                }
                                orhex = new String[value1.split("[ \t]+").length];
                                orhex = value1.split("[ \t]+");
                                mask = new int[orhex.length];
                                byteOrig = new byte[orhex.length];
                                t = beginTag;
                                while (t < orhex.length) {
                                    if (orhex[t].contains("*") && !orhex[t].contains("**")) {
                                        error = true;
                                        orhex[t] = "60";
                                    }
                                    if (orhex[t].contains("**") || orhex[t].matches("\\?+")) {
                                        orhex[t] = "60";
                                        mask[t] = classesTag;
                                    } else {
                                        mask[t] = beginTag;
                                    }
                                    if (orhex[t].contains("W") || orhex[t].contains("w") || orhex[t].contains("R") || orhex[t].contains(InternalZipConstants.READ_MODE)) {
                                        mask[t] = Integer.valueOf(orhex[t].toLowerCase().replace("w", BuildConfig.VERSION_NAME).replace(InternalZipConstants.READ_MODE, BuildConfig.VERSION_NAME)).intValue() + libTagALL;
                                        orhex[t] = "60";
                                    }
                                    byteOrig[t] = Integer.valueOf(orhex[t], 16).byteValue();
                                    t += classesTag;
                                }
                            }
                            if (txtdata[r].contains("\"object\"") && txtdata[r].contains("{") && txtdata[r].contains("}")) {
                                try {
                                    value3 = new JSONObject(txtdata[r]).getString("object");
                                } catch (JSONException e15) {
                                    addToLog("Error LP: Error number by object!");
                                }
                                Process localProcess9 = Runtime.getRuntime().exec("dalvikvm -Xverify:none -Xdexopt:none -cp " + paramArrayOfString[packageTag] + " " + paramArrayOfString[libTagMIPS] + ".nerorunpatch " + paramArrayOfString[beginTag] + " " + "object" + value3 + LogCollector.LINE_SEPARATOR);
                                localProcess9.waitFor();
                                DataInputStream dataInputStream = new DataInputStream(localProcess9.getInputStream());
                                byte[] arrayOfByte = new byte[dataInputStream.available()];
                                dataInputStream.read(arrayOfByte);
                                String str = new String(arrayOfByte);
                                localProcess9.destroy();
                                if (str.contains("Done")) {
                                    addToLog("Object patched!\n\n");
                                    sumresult = true;
                                } else {
                                    addToLog("Object not found!\n\n");
                                    sumresult = false;
                                }
                                manualpatch = true;
                            }
                            if (txtdata[r].contains("search") && txtdata[r].contains("{") && txtdata[r].contains("}")) {
                                try {
                                    value3 = new JSONObject(txtdata[r]).getString("search");
                                } catch (JSONException e16) {
                                    addToLog("Error LP: Error search hex read!");
                                }
                                value3 = value3.trim();
                                if (convert) {
                                    value3 = Utils.rework(value3);
                                }
                                orhex = new String[value3.split("[ \t]+").length];
                                orhex = value3.split("[ \t]+");
                                mask = new int[orhex.length];
                                byteOrig = new byte[orhex.length];
                                t = beginTag;
                                while (t < orhex.length) {
                                    if (orhex[t].contains("*") && !orhex[t].contains("**")) {
                                        error = true;
                                        orhex[t] = "60";
                                    }
                                    if (orhex[t].contains("**") || orhex[t].matches("\\?+")) {
                                        orhex[t] = "60";
                                        mask[t] = classesTag;
                                    } else {
                                        mask[t] = beginTag;
                                    }
                                    if (orhex[t].toUpperCase().contains("R")) {
                                        mask[t] = Integer.valueOf(orhex[t].replace("R", BuildConfig.VERSION_NAME)).intValue() + libTagALL;
                                        orhex[t] = "60";
                                    }
                                    byteOrig[t] = Integer.valueOf(orhex[t], 16).byteValue();
                                    t += classesTag;
                                }
                                if (error) {
                                    result = false;
                                    addToLog("Error LP: Patterns to search not valid!\n");
                                } else {
                                    mark_search = true;
                                    SearchItem searchItem = new SearchItem(byteOrig, mask);
                                    searchItem.repByte = new byte[byteOrig.length];
                                    ser.add(searchItem);
                                }
                            }
                            if (txtdata[r].contains("replaced") && txtdata[r].contains("{") && txtdata[r].contains("}")) {
                                try {
                                    value2 = new JSONObject(txtdata[r]).getString("replaced");
                                } catch (JSONException e17) {
                                    addToLog("Error LP: Error replaced hex read!");
                                }
                                value2 = value2.trim();
                                if (convert) {
                                    value2 = Utils.rework(value2);
                                }
                                rephex = new String[value2.split("[ \t]+").length];
                                rephex = value2.split("[ \t]+");
                                rep_mask = new int[rephex.length];
                                byteReplace = new byte[rephex.length];
                                t = beginTag;
                                while (t < rephex.length) {
                                    if (rephex[t].contains("*") && !rephex[t].contains("**")) {
                                        error = true;
                                        rephex[t] = "60";
                                    }
                                    if (rephex[t].contains("**") || rephex[t].matches("\\?+")) {
                                        rephex[t] = "60";
                                        rep_mask[t] = beginTag;
                                    } else {
                                        rep_mask[t] = classesTag;
                                    }
                                    if (rephex[t].toLowerCase().contains("sq")) {
                                        rephex[t] = "60";
                                        rep_mask[t] = 253;
                                    }
                                    if (rephex[t].contains("s1") || rephex[t].contains("S1")) {
                                        rephex[t] = "60";
                                        rep_mask[t] = 254;
                                    }
                                    if (rephex[t].contains("s0") || rephex[t].contains("S0")) {
                                        rephex[t] = "60";
                                        rep_mask[t] = MotionEventCompat.ACTION_MASK;
                                    }
                                    if (rephex[t].contains("W") || rephex[t].contains("w") || rephex[t].contains("R") || rephex[t].contains("R")) {
                                        rep_mask[t] = Integer.valueOf(rephex[t].toLowerCase().replace("w", BuildConfig.VERSION_NAME).replace(InternalZipConstants.READ_MODE, BuildConfig.VERSION_NAME)).intValue() + libTagALL;
                                        rephex[t] = "60";
                                    }
                                    byteReplace[t] = Integer.valueOf(rephex[t], 16).byteValue();
                                    t += classesTag;
                                }
                                if (rep_mask.length != mask.length || byteOrig.length != byteReplace.length || byteReplace.length < endTag || byteOrig.length < endTag) {
                                    error = true;
                                }
                                if (error) {
                                    result = false;
                                    addToLog("Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!");
                                }
                                if (!error) {
                                    if (multilib_patch || multidex) {
                                        pat.add(new PatchesItem(byteOrig, mask, byteReplace, rep_mask, "all_lib", false));
                                    } else {
                                        pat.add(new PatchesItem(byteOrig, mask, byteReplace, rep_mask, group, false));
                                    }
                                    group = BuildConfig.VERSION_NAME;
                                }
                            }
                            if (txtdata[r].contains("insert") && txtdata[r].contains("{") && txtdata[r].contains("}")) {
                                try {
                                    value2 = new JSONObject(txtdata[r]).getString("insert");
                                } catch (JSONException e18) {
                                    addToLog("Error LP: Error insert hex read!");
                                }
                                value2 = value2.trim();
                                if (convert) {
                                    value2 = Utils.rework(value2);
                                }
                                rephex = new String[value2.split("[ \t]+").length];
                                rephex = value2.split("[ \t]+");
                                rep_mask = new int[rephex.length];
                                byteReplace = new byte[rephex.length];
                                t = beginTag;
                                while (t < rephex.length) {
                                    if (rephex[t].contains("*") && !rephex[t].contains("**")) {
                                        error = true;
                                        rephex[t] = "60";
                                    }
                                    if (rephex[t].contains("**") || rephex[t].matches("\\?+")) {
                                        rephex[t] = "60";
                                        rep_mask[t] = beginTag;
                                    } else {
                                        rep_mask[t] = classesTag;
                                    }
                                    if (rephex[t].toLowerCase().contains("sq")) {
                                        rephex[t] = "60";
                                        rep_mask[t] = 253;
                                    }
                                    if (rephex[t].contains("s1") || rephex[t].contains("S1")) {
                                        rephex[t] = "60";
                                        rep_mask[t] = 254;
                                    }
                                    if (rephex[t].contains("s0") || rephex[t].contains("S0")) {
                                        rephex[t] = "60";
                                        rep_mask[t] = MotionEventCompat.ACTION_MASK;
                                    }
                                    if (rephex[t].contains("W") || rephex[t].contains("w") || rephex[t].contains("R") || rephex[t].contains("R")) {
                                        rep_mask[t] = Integer.valueOf(rephex[t].toLowerCase().replace("w", BuildConfig.VERSION_NAME).replace(InternalZipConstants.READ_MODE, BuildConfig.VERSION_NAME)).intValue() + libTagALL;
                                        rephex[t] = "60";
                                    }
                                    byteReplace[t] = Integer.valueOf(rephex[t], 16).byteValue();
                                    t += classesTag;
                                }
                                if (byteReplace.length < endTag || byteOrig.length < endTag) {
                                    error = true;
                                }
                                if (error) {
                                    result = false;
                                    addToLog("Error LP: Dimensions of the original hex-string and repleced must be >3.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!");
                                }
                                if (!error) {
                                    pat.add(new PatchesItem(byteOrig, mask, byteReplace, rep_mask, group, true));
                                    group = BuildConfig.VERSION_NAME;
                                }
                            }
                            if (txtdata[r].contains("replace_from_file") && txtdata[r].contains("{") && txtdata[r].contains("}")) {
                                try {
                                    value2 = new JSONObject(txtdata[r]).getString("replace_from_file");
                                } catch (JSONException e19) {
                                    addToLog("Error LP: Error replaced hex read!");
                                }
                                value2 = value2.trim();
                                File arrayFile = new File(Utils.getDirs(new File(paramArrayOfString[classesTag])) + InternalZipConstants.ZIP_FILE_SEPARATOR + value2);
                                int len = (int) arrayFile.length();
                                byteReplace = new byte[len];
                                FileInputStream fileInputStream2 = new FileInputStream(arrayFile);
                                do {
                                } while (fileInputStream2.read(byteReplace) > 0);
                                fileInputStream2.close();
                                rep_mask = new int[len];
                                Arrays.fill(rep_mask, classesTag);
                                if (error) {
                                    result = false;
                                    addToLog("Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!");
                                }
                                if (!error) {
                                    pat.add(new PatchesItem(byteOrig, mask, byteReplace, rep_mask, group, false));
                                    group = BuildConfig.VERSION_NAME;
                                }
                            }
                        } catch (Exception e3222) {
                            e3222.printStackTrace();
                        } catch (FileNotFoundException e62) {
                            addToLog("Custom Patch not Found. It's problem with root. Don't have access to SD card from root.  If you use SuperSu, try disable \"mount namespace separation\" in SuperSu. If it not help, please update SuperSu and update binary file su from SuperSu.");
                        }
                        if (txtdata[r].toUpperCase().contains("[ADD-BOOT]")) {
                            addToLog("Patch on Reboot added!");
                        }
                        if (end) {
                            finalText = finalText + LogCollector.LINE_SEPARATOR + txtdata[r];
                        }
                        if (data.contains("[END]")) {
                            tag = endTag;
                            end = true;
                        }
                        r += classesTag;
                    } else {
                        bufferedReader.close();
                        if (sumresult) {
                            addToLog(BuildConfig.VERSION_NAME + finalText);
                        }
                        if (!sumresult) {
                            if (patchteil) {
                                addToLog("Not all patterns are replaced, but the program can work, test it!\nCustom Patch not valid for this Version of the Programm or already patched. ");
                            } else {
                                addToLog("Custom Patch not valid for this Version of the Programm or already patched. ");
                            }
                        }
                        if (dalvikfix) {
                            addToLog("Changes Fix to: " + dalvikfixfile);
                        }
                        if (fixunpack) {
                            Utils.sendFromRoot("Analise Results:");
                            int res = Utils.create_ODEX_root(dir, classesFiles, dirapp, uid, Utils.getOdexForCreate(dirapp, uid));
                            if (res != 0) {
                                addToLog("Create odex error " + res);
                            }
                            if (res == 0) {
                                addToLog("\n~Package reworked!~\nChanges Fix to: " + Utils.getPlaceForOdex(paramArrayOfString[libTagALL], true));
                            }
                        }
                        inputStreamReader.close();
                        fileInputStream.close();
                        Utils.sendFromRootCP(log);
                        listAppsFragment.startUnderRoot = Boolean.valueOf(false);
                        Utils.exitFromRootJava();
                        return;
                    }
                }
            } catch (FileNotFoundException e622) {
                addToLog("Custom Patch not Found. It's problem with root. Don't have access to SD card from root.  If you use SuperSu, try disable \"mount namespace separation\" in SuperSu. If it not help, please update SuperSu and update binary file su from SuperSu.");
            } catch (Exception e32222) {
                e32222.printStackTrace();
                addToLog(BuildConfig.VERSION_NAME + e32222);
            }
        } catch (Exception e322222) {
            e322222.printStackTrace();
        }
    }

    public static boolean patchProcess(java.util.ArrayList<com.android.vending.billing.InAppBillingService.LUCK.PatchesItem> r30) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxOverflowException: Regions stack size limit reached
	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:42)
	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:66)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:280)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:167)
*/
        /*
        r23 = 1;
        if (r30 == 0) goto L_0x0020;
    L_0x0004:
        r3 = r30.size();
        if (r3 <= 0) goto L_0x0020;
    L_0x000a:
        r3 = r30.iterator();
    L_0x000e:
        r4 = r3.hasNext();
        if (r4 == 0) goto L_0x0020;
    L_0x0014:
        r19 = r3.next();
        r19 = (com.android.vending.billing.InAppBillingService.LUCK.PatchesItem) r19;
        r4 = 0;
        r0 = r19;
        r0.result = r4;
        goto L_0x000e;
    L_0x0020:
        r3 = 3;
        r3 = new java.lang.String[r3];
        r4 = 0;
        r5 = "chmod";
        r3[r4] = r5;
        r4 = 1;
        r5 = "777";
        r3[r4] = r5;
        r4 = 2;
        r5 = localFile2;
        r5 = r5.getAbsolutePath();
        r3[r4] = r5;
        com.chelpus.Utils.run_all_no_root(r3);
        r3 = new java.io.RandomAccessFile;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = localFile2;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r5 = "rw";	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3.<init>(r4, r5);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r2 = r3.getChannel();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = new java.lang.StringBuilder;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3.<init>();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = "Size file:";	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.append(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = r2.size();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.append(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.toString();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        com.chelpus.Utils.sendFromRootCP(r3);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = java.nio.channels.FileChannel.MapMode.READ_WRITE;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = 0;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r6 = r2.size();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r6 = (int) r6;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r6 = (long) r6;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r16 = r2.map(r3, r4, r6);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r30.toArray();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.length;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r0 = new com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[r3];	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r25 = r0;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r30.size();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r0 = new com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[r3];	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r25 = r0;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r0 = r30;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r1 = r25;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r0.toArray(r1);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = (com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[]) r3;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r0 = r3;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r0 = (com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[]) r0;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r25 = r0;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r13 = -1;
        r26 = 0;
        r8 = 0;
    L_0x0092:
        r3 = r16.hasRemaining();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r3 == 0) goto L_0x034a;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0098:
        if (r8 != 0) goto L_0x034a;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x009a:
        r3 = r16.position();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3 - r26;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 512000; // 0x7d000 float:7.17465E-40 double:2.529616E-318;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r3 <= r4) goto L_0x00c3;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x00a5:
        r3 = new java.lang.StringBuilder;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3.<init>();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = "Progress size:";	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r16.position();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.toString();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        com.chelpus.Utils.sendFromRootCP(r3);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r26 = r16.position();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x00c3:
        r3 = r13 + 1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0 = r16;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0.position(r3);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r13 = r16.position();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r12 = r16.get();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r17 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x00d4:
        r0 = r25;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r0.length;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0 = r17;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r0 >= r3) goto L_0x0092;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x00db:
        r0 = r16;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0.position(r13);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.origByte;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r12 == r3) goto L_0x0114;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x00e9:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.origMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r3 == r4) goto L_0x0114;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x00f3:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.origMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r3 <= r4) goto L_0x0307;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x00fd:
        r3 = search;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r4.origMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r5 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r4[r5];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r4 + -2;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.get(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = (java.lang.Byte) r3;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.byteValue();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r12 != r3) goto L_0x0307;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0114:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r3 != 0) goto L_0x0124;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x011d:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3[r4] = r12;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0124:
        r3 = r25[r17];	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3[r4];	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r4 = 1;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        if (r3 <= r4) goto L_0x0155;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
    L_0x012e:
        r3 = r25[r17];	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3[r4];	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r4 = 253; // 0xfd float:3.55E-43 double:1.25E-321;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        if (r3 >= r4) goto L_0x0155;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
    L_0x0139:
        r3 = r25[r17];	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r4 = r3.repByte;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r5 = 0;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = search;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r6 = r25[r17];	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r6 = r6.repMask;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r7 = 0;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r6 = r6[r7];	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r6 = r6 + -2;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3.get(r6);	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = (java.lang.Byte) r3;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3.byteValue();	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r4[r5] = r3;	 Catch:{ Exception -> 0x030b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
    L_0x0155:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 253; // 0xfd float:3.55E-43 double:1.25E-321;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r3 != r4) goto L_0x016f;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0160:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r5 = r12 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r6 = r12 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r6 = r6 * 16;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r5 = r5 + r6;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r5 = (byte) r5;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3[r4] = r5;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x016f:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 254; // 0xfe float:3.56E-43 double:1.255E-321;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r3 != r4) goto L_0x0186;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x017a:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r5 = r12 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r5 = r5 + 16;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r5 = (byte) r5;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3[r4] = r5;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0186:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 255; // 0xff float:3.57E-43 double:1.26E-321;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r3 != r4) goto L_0x019b;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0191:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r5 = r12 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r5 = (byte) r5;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3[r4] = r5;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x019b:
        r18 = 1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r13 + 1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0 = r16;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0.position(r3);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r27 = r16.get();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x01a8:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.origByte;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r18];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0 = r27;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r0 == r3) goto L_0x01dc;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x01b2:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.origMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r18];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r3 <= r4) goto L_0x01d3;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x01bb:
        r3 = search;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r4.origMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r4[r18];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r4 + -2;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.get(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = (java.lang.Byte) r3;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.byteValue();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0 = r27;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r0 == r3) goto L_0x01dc;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x01d3:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.origMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r18];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 1;
        if (r3 != r4) goto L_0x0307;
    L_0x01dc:
        r3 = r25[r17];	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3[r18];	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        if (r3 != 0) goto L_0x01ea;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
    L_0x01e4:
        r3 = r25[r17];	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3.repByte;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3[r18] = r27;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
    L_0x01ea:
        r3 = r25[r17];	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3[r18];	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r4 = 1;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        if (r3 <= r4) goto L_0x0219;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
    L_0x01f3:
        r3 = r25[r17];	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3[r18];	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r4 = 253; // 0xfd float:3.55E-43 double:1.25E-321;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        if (r3 >= r4) goto L_0x0219;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
    L_0x01fd:
        r3 = r25[r17];	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3[r18];	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r29 = r3 + -2;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r25[r17];	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r4 = r3.repByte;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = search;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r0 = r29;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3.get(r0);	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = (java.lang.Byte) r3;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r3 = r3.byteValue();	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
        r4[r18] = r3;	 Catch:{ Exception -> 0x039b, IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, FileNotFoundException -> 0x049d }
    L_0x0219:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 253; // 0xfd float:3.55E-43 double:1.25E-321;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r3 != r4) goto L_0x0233;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0224:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r5 = r12 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r6 = r12 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r6 = r6 * 16;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r5 = r5 + r6;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r5 = (byte) r5;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3[r4] = r5;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0233:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r18];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 254; // 0xfe float:3.56E-43 double:1.255E-321;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r3 != r4) goto L_0x0248;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x023d:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r27 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r4 + 16;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = (byte) r4;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3[r18] = r4;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0248:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r18];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 255; // 0xff float:3.57E-43 double:1.26E-321;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r3 != r4) goto L_0x025b;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0252:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r27 & 15;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = (byte) r4;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3[r18] = r4;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x025b:
        r18 = r18 + 1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.origByte;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.length;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0 = r18;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r0 != r3) goto L_0x03d7;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0266:
        r15 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r3.origMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r5 = r4.length;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x026d:
        if (r3 >= r5) goto L_0x0274;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x026f:
        r9 = r4[r3];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r9 != 0) goto L_0x03d3;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0273:
        r15 = 1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0274:
        if (r15 != 0) goto L_0x0277;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0276:
        r8 = 1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0277:
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.insert;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        if (r3 == 0) goto L_0x02c1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x027d:
        r22 = r16.position();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r2.size();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = (int) r3;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r20 = r3 - r22;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0 = r20;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r10 = new byte[r0];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0 = r16;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r1 = r20;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0.get(r10, r3, r1);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r11 = java.nio.ByteBuffer.wrap(r10);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.length;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r4.origByte;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r4.length;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3 - r4;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3 + r22;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = (long) r3;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r2.position(r3);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r2.write(r11);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = java.nio.channels.FileChannel.MapMode.READ_WRITE;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r6 = r2.size();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r6 = (int) r6;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r6 = (long) r6;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r16 = r2.map(r3, r4, r6);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0 = r16;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r1 = r22;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0.position(r1);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x02c1:
        r3 = (long) r13;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r2.position(r3);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repByte;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r11 = java.nio.ByteBuffer.wrap(r3);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r2.write(r11);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r16.force();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = new java.lang.StringBuilder;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3.<init>();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = "\nPattern N";	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = r17 + 1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = ": Patch done! \n(Offset: ";	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = java.lang.Integer.toHexString(r13);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = ")\n";	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.toString();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        addToLog(r3);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3.result = r4;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = 1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        patchteil = r3;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x0307:
        r17 = r17 + 1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        goto L_0x00d4;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x030b:
        r14 = move-exception;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = 0;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r4];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r29 = r3 + -2;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = new java.lang.StringBuilder;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3.<init>();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = "Byte N";	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0 = r29;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r0);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = " not found! Please edit search pattern for byte ";	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0 = r29;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r0);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = ".";	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.toString();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        addToLog(r3);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        goto L_0x0155;
    L_0x0341:
        r14 = move-exception;
        r14.printStackTrace();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = "Byte by search not found! Please edit pattern for search.\n";	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        addToLog(r3);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x034a:
        r2.close();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = "Analise Results:";	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        com.chelpus.Utils.sendFromRootCP(r3);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r17 = 0;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0354:
        r0 = r25;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r0.length;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r0 = r17;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r0 >= r3) goto L_0x04a3;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x035b:
        r3 = r25[r17];	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.result;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r3 != 0) goto L_0x0485;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0361:
        r28 = 0;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r25[r17];	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.group;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = "";	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.equals(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r3 != 0) goto L_0x03dd;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x036f:
        r0 = r25;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = r0.length;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = 0;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0373:
        if (r3 >= r4) goto L_0x03dd;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0375:
        r24 = r25[r3];	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r5 = r25[r17];	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r5 = r5.group;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r0 = r24;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r6 = r0.group;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r5 = r5.equals(r6);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r5 == 0) goto L_0x0398;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0385:
        r0 = r24;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r5 = r0.result;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r5 == 0) goto L_0x0398;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x038b:
        r5 = multidex;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r5 != 0) goto L_0x0393;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x038f:
        r5 = multilib_patch;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r5 == 0) goto L_0x0396;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0393:
        r5 = 1;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        goodResult = r5;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0396:
        r28 = 1;
    L_0x0398:
        r3 = r3 + 1;
        goto L_0x0373;
    L_0x039b:
        r14 = move-exception;
        r3 = r25[r17];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.repMask;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3[r18];	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r29 = r3 + -2;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = new java.lang.StringBuilder;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3.<init>();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = "Byte N";	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0 = r29;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r0);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = " not found! Please edit search pattern for byte ";	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r0 = r29;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r0);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r4 = ".";	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.append(r4);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        r3 = r3.toString();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        addToLog(r3);	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        goto L_0x0219;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x03d0:
        r3 = move-exception;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        goto L_0x034a;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x03d3:
        r3 = r3 + 1;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        goto L_0x026d;	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
    L_0x03d7:
        r27 = r16.get();	 Catch:{ IndexOutOfBoundsException -> 0x0341, BufferUnderflowException -> 0x03d0, Exception -> 0x04f0, FileNotFoundException -> 0x049d }
        goto L_0x01a8;
    L_0x03dd:
        if (r28 != 0) goto L_0x0421;
    L_0x03df:
        r3 = goodResult;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r3 != 0) goto L_0x0421;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x03e3:
        r3 = multidex;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r3 == 0) goto L_0x0425;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x03e7:
        r3 = goodResult;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r3 != 0) goto L_0x0421;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x03eb:
        r3 = localFile2;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = classesFiles;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r5 = classesFiles;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r5 = r5.size();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r5 = r5 + -1;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = r4.get(r5);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.equals(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r3 == 0) goto L_0x0421;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0401:
        r3 = new java.lang.StringBuilder;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3.<init>();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = "\nPattern N";	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.append(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = r17 + 1;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.append(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = ":\nError LP: Pattern not found!\nor patch is already applied?!\n";	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.append(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.toString();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        addToLog(r3);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r23 = 0;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0421:
        r17 = r17 + 1;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        goto L_0x0354;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0425:
        r3 = multilib_patch;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r3 == 0) goto L_0x0464;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0429:
        r3 = goodResult;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r3 != 0) goto L_0x0421;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x042d:
        r3 = localFile2;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = arrayFile2;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r5 = arrayFile2;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r5 = r5.size();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r5 = r5 + -1;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = r4.get(r5);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.equals(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r3 == 0) goto L_0x0421;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0443:
        r3 = new java.lang.StringBuilder;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3.<init>();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = "\nPattern N";	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.append(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = r17 + 1;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.append(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = ":\nError LP: Pattern not found!\nor patch is already applied?!\n";	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.append(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.toString();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        addToLog(r3);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r23 = 0;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        goto L_0x0421;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0464:
        r3 = new java.lang.StringBuilder;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3.<init>();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = "\nPattern N";	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.append(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = r17 + 1;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.append(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = ":\nError LP: Pattern not found!\nor patch is already applied?!\n";	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.append(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.toString();	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        addToLog(r3);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r23 = 0;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        goto L_0x0421;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0485:
        r3 = multidex;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r3 != 0) goto L_0x048d;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0489:
        r3 = multilib_patch;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r3 == 0) goto L_0x0421;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x048d:
        r3 = r25[r17];	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.group;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r4 = "";	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        r3 = r3.equals(r4);	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        if (r3 != 0) goto L_0x0421;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
    L_0x0499:
        r3 = 1;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        goodResult = r3;	 Catch:{ FileNotFoundException -> 0x049d, BufferUnderflowException -> 0x04ee, Exception -> 0x04d2 }
        goto L_0x0421;
    L_0x049d:
        r21 = move-exception;
        r3 = "Error LP: Program files are not found!\nMove Program to internal storage.";
        addToLog(r3);
    L_0x04a3:
        r3 = tag;
        r4 = 1;
        if (r3 != r4) goto L_0x04ac;
    L_0x04a8:
        r3 = unpack;
        if (r3 == 0) goto L_0x04b2;
    L_0x04ac:
        r3 = tag;
        r4 = 10;
        if (r3 != r4) goto L_0x04bd;
    L_0x04b2:
        r3 = ART;
        if (r3 != 0) goto L_0x04bd;
    L_0x04b6:
        r3 = localFile2;
        r4 = dirapp;
        com.chelpus.Utils.fixadlerOdex(r3, r4);
    L_0x04bd:
        r3 = unpack;
        if (r3 == 0) goto L_0x04d1;
    L_0x04c1:
        r3 = odexpatch;
        if (r3 != 0) goto L_0x04d1;
    L_0x04c5:
        r3 = OdexPatch;
        if (r3 != 0) goto L_0x04d1;
    L_0x04c9:
        r3 = localFile2;
        com.chelpus.Utils.fixadler(r3);
        r3 = 1;
        fixunpack = r3;
    L_0x04d1:
        return r23;
    L_0x04d2:
        r14 = move-exception;
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = "Exception e";
        r3 = r3.append(r4);
        r4 = r14.toString();
        r3 = r3.append(r4);
        r3 = r3.toString();
        addToLog(r3);
        goto L_0x04a3;
    L_0x04ee:
        r3 = move-exception;
        goto L_0x04a3;
    L_0x04f0:
        r3 = move-exception;
        goto L_0x034a;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chelpus.root.utils.custompatch.patchProcess(java.util.ArrayList):boolean");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean searchProcess(ArrayList<SearchItem> searchlist) {
        boolean patch = true;
        searchStr = BuildConfig.VERSION_NAME;
        if (searchlist != null && searchlist.size() > 0) {
            Iterator it = searchlist.iterator();
            while (it.hasNext()) {
                ((SearchItem) it.next()).result = false;
            }
        }
        try {
            int g;
            FileChannel ChannelDex2 = new RandomAccessFile(localFile2, InternalZipConstants.WRITE_MODE).getChannel();
            MappedByteBuffer fileBytes2 = ChannelDex2.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex2.size()));
            SearchItem[] patches = new SearchItem[searchlist.toArray().length];
            patches = (SearchItem[]) searchlist.toArray(new SearchItem[searchlist.size()]);
            long j = 0;
            while (fileBytes2.hasRemaining()) {
                int curentPos = fileBytes2.position();
                byte curentByte = fileBytes2.get();
                g = beginTag;
                while (g < patches.length) {
                    fileBytes2.position(curentPos);
                    if (!patches[g].result && (curentByte == patches[g].origByte[beginTag] || patches[g].origMask[beginTag] != 0)) {
                        if (patches[g].origMask[beginTag] != 0) {
                            patches[g].repByte[beginTag] = curentByte;
                        }
                        int i = classesTag;
                        fileBytes2.position(curentPos + classesTag);
                        byte prufbyte = fileBytes2.get();
                        while (true) {
                            if ((patches[g].result || prufbyte != patches[g].origByte[i]) && patches[g].origMask[i] == 0) {
                                break;
                            }
                            if (patches[g].origMask[i] > 0) {
                                patches[g].repByte[i] = prufbyte;
                            }
                            i += classesTag;
                            if (i == patches[g].origByte.length) {
                                break;
                            }
                            prufbyte = fileBytes2.get();
                        }
                    }
                    g += classesTag;
                }
                fileBytes2.position(curentPos + classesTag);
                j++;
            }
            ChannelDex2.close();
            for (g = beginTag; g < patches.length; g += classesTag) {
                if (!patches[g].result) {
                    searchStr += "Bytes by serach N" + (g + classesTag) + ":\nError LP: Bytes not found!" + LogCollector.LINE_SEPARATOR;
                    patch = false;
                }
            }
            g = beginTag;
            while (g < patches.length) {
                if (patches[g].result) {
                    searchStr += "\nBytes by search N" + (g + classesTag) + ":" + LogCollector.LINE_SEPARATOR;
                }
                int w = beginTag;
                while (w < patches[g].origMask.length) {
                    if (patches[g].origMask[w] > classesTag) {
                        int y = patches[g].origMask[w] - 2;
                        search.set(y, Byte.valueOf(patches[g].repByte[w]));
                        if (patches[g].result) {
                            byte[] bytik = new byte[classesTag];
                            bytik[beginTag] = ((Byte) search.get(y)).byteValue();
                            searchStr += "R" + y + "=" + Utils.bytesToHex(bytik).toUpperCase() + " ";
                        }
                    }
                    w += classesTag;
                }
                searchStr += LogCollector.LINE_SEPARATOR;
                g += classesTag;
            }
        } catch (FileNotFoundException e) {
            addToLog("Error LP: Program files are not found!\nMove Program to internal storage.");
        } catch (BufferUnderflowException e2) {
            addToLog("Exception e" + e2.toString());
        } catch (Exception e3) {
            addToLog("Exception e" + e3.toString());
        }
        return patch;
    }

    public static void searchDalvik(String apk_file) {
        File dalv = Utils.getFileDalvikCache(apk_file);
        if (dalv != null) {
            try {
                localFile2 = dalv;
            } catch (FileNotFoundException e) {
                if (!system) {
                    addToLog("Error LP: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*");
                    return;
                }
                return;
            } catch (Exception e2) {
                addToLog(BuildConfig.VERSION_NAME + e2);
                return;
            }
        }
        if (odexpatch) {
            localFile2 = new File(Utils.getPlaceForOdex(dirapp, true));
        }
        try {
            if (!localFile2.exists()) {
                localFile2 = new File(Utils.getPlaceForOdex(dirapp, true));
            }
        } catch (Exception e3) {
            localFile2 = new File(Utils.getPlaceForOdex(dirapp, true));
        } catch (FileNotFoundException e4) {
            if (!system) {
                addToLog("Error LP: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*");
                return;
            }
            return;
        }
        if (!localFile2.exists()) {
            throw new FileNotFoundException();
        }
    }

    public static void searchDalvikOdex(String param, String param2) throws FileNotFoundException {
        searchDalvik(param);
        try {
            if (odexpatch) {
                localFile2 = new File(Utils.getPlaceForOdex(dirapp, true));
                return;
            }
            String backTemp = Utils.getOdexForCreate(param2, uid);
            File backFile = new File(backTemp);
            if (backFile.exists()) {
                backFile.delete();
            }
            backFile = new File(backTemp.replace("-2", "-1"));
            if (backFile.exists()) {
                backFile.delete();
            }
            backFile = new File(backTemp.replace("-1", "-2"));
            if (backFile.exists()) {
                backFile.delete();
            }
            backFile = new File(backTemp.replace("-2", BuildConfig.VERSION_NAME));
            if (backFile.exists()) {
                backFile.delete();
            }
            backFile = new File(backTemp.replace("-1", BuildConfig.VERSION_NAME));
            if (backFile.exists()) {
                backFile.delete();
            }
            backFile = new File(backTemp);
            Utils.copyFile(localFile2, backFile);
            if (backFile.exists() && backFile.length() == localFile2.length()) {
                String[] strArr = new String[otherfilesTag];
                strArr[beginTag] = "chmod";
                strArr[classesTag] = "644";
                strArr[libTagALL] = backFile.getAbsolutePath();
                Utils.run_all_no_root(strArr);
                strArr = new String[otherfilesTag];
                strArr[beginTag] = "chown";
                strArr[classesTag] = "1000." + uid;
                strArr[libTagALL] = backFile.getAbsolutePath();
                Utils.run_all_no_root(strArr);
                strArr = new String[otherfilesTag];
                strArr[beginTag] = "chown";
                strArr[classesTag] = "1000:" + uid;
                strArr[libTagALL] = backFile.getAbsolutePath();
                Utils.run_all_no_root(strArr);
                localFile2 = backFile;
            }
        } catch (Exception e) {
            e.printStackTrace();
            addToLog("Exception e" + e.toString());
        }
    }

    public static ArrayList<File> searchlib(String pkgName, String libname, String apk_file) {
        ArrayList<File> libs = new ArrayList();
        try {
            ArrayList<String> dataDirs = new ArrayList();
            dataDirs.add("/data/data/");
            dataDirs.add("/mnt/asec/");
            dataDirs.add("/sd-ext/data/");
            dataDirs.add("/data/sdext2/");
            dataDirs.add("/data/sdext1/");
            dataDirs.add("/data/sdext/");
            ArrayList<String> pakDirs = new ArrayList();
            pakDirs.add(pkgName);
            pakDirs.add(pkgName + "-1");
            pakDirs.add(pkgName + "-2");
            String index;
            Iterator it;
            String dir;
            File d;
            String result;
            Iterator it2;
            if (libname.trim().equals("*")) {
                int length;
                int i;
                File file;
                multilib_patch = true;
                File[] getFiles = new File("/data/data/" + pkgName + "/lib/").listFiles();
                if (getFiles != null && getFiles.length > 0) {
                    length = getFiles.length;
                    for (i = beginTag; i < length; i += classesTag) {
                        file = getFiles[i];
                        if (file.length() > 0 && file.getName().endsWith(".so")) {
                            Utils.addFileToList(file, libs);
                        }
                    }
                }
                index = BuildConfig.VERSION_NAME;
                index = new File(apk_file).getName().replace(pkgName, BuildConfig.VERSION_NAME).replace(".apk", BuildConfig.VERSION_NAME);
                if (new File("/data/app-lib").exists()) {
                    getFiles = new File("/data/app-lib/" + pkgName + index + InternalZipConstants.ZIP_FILE_SEPARATOR).listFiles();
                    if (getFiles != null && getFiles.length > 0) {
                        length = getFiles.length;
                        for (i = beginTag; i < length; i += classesTag) {
                            file = getFiles[i];
                            if (file.length() > 0 && file.getName().endsWith(".so")) {
                                Utils.addFileToList(file, libs);
                            }
                        }
                    }
                }
                it = dataDirs.iterator();
                while (it.hasNext()) {
                    Utils ad;
                    ArrayList<File> foundFiles;
                    dir = (String) it.next();
                    d = new File(dir + pkgName + index + InternalZipConstants.ZIP_FILE_SEPARATOR);
                    if (d.exists()) {
                        ad = new Utils("sdf");
                        foundFiles = new ArrayList();
                        result = ad.findFileEndText(d, ".so", foundFiles);
                        if (!result.equals(BuildConfig.VERSION_NAME) && foundFiles.size() > 0) {
                            it2 = foundFiles.iterator();
                            while (it2.hasNext()) {
                                Utils.addFileToList((File) it2.next(), libs);
                                addToLog("Found lib:" + result);
                            }
                        }
                    }
                    File d1 = new File(dir + pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR);
                    if (d1.exists()) {
                        ad = new Utils("sdf");
                        foundFiles = new ArrayList();
                        result = ad.findFileEndText(d1, ".so", foundFiles);
                        if (!result.equals(BuildConfig.VERSION_NAME) && foundFiles.size() > 0) {
                            it2 = foundFiles.iterator();
                            while (it2.hasNext()) {
                                Utils.addFileToList((File) it2.next(), libs);
                                addToLog("Found lib:" + result);
                            }
                        }
                    }
                }
                return libs;
            }
            localFile2 = new File("/data/data/" + pkgName + "/lib/" + libname);
            if (localFile2.exists()) {
                Utils.addFileToList(localFile2, libs);
            } else {
                it = dataDirs.iterator();
                while (it.hasNext()) {
                    dir = (String) it.next();
                    it2 = pakDirs.iterator();
                    while (it2.hasNext()) {
                        d = new File(dir + ((String) it2.next()));
                        if (d.exists()) {
                            result = new Utils("sdf").findFile(d, libname);
                            if (!result.equals(BuildConfig.VERSION_NAME)) {
                                localFile2 = new File(result);
                                Utils.addFileToList(localFile2, libs);
                                addToLog("Found lib:" + result);
                            }
                        }
                    }
                }
            }
            File dir2 = Utils.getDirs(new File(apk_file));
            if (new File(dir2.getAbsoluteFile() + "/lib").exists()) {
                localFile2 = new File(dir2.getAbsoluteFile() + "/lib/arm/" + libname);
                if (localFile2.exists()) {
                    Utils.addFileToList(localFile2, libs);
                }
                localFile2 = new File(dir2.getAbsoluteFile() + "/lib/x86/" + libname);
                if (localFile2.exists()) {
                    Utils.addFileToList(localFile2, libs);
                }
                localFile2 = new File(dir2.getAbsoluteFile() + "/lib/mips/" + libname);
                if (localFile2.exists()) {
                    Utils.addFileToList(localFile2, libs);
                }
            }
            if (!localFile2.exists()) {
                localFile2 = new File("/system/lib/" + libname);
            }
            if (libs.size() == 0) {
                throw new FileNotFoundException();
            }
            index = BuildConfig.VERSION_NAME;
            index = new File(apk_file).getName().replace(pkgName, BuildConfig.VERSION_NAME).replace(".apk", BuildConfig.VERSION_NAME);
            if (!new File("/data/app-lib/" + pkgName + index + InternalZipConstants.ZIP_FILE_SEPARATOR + libname).exists()) {
                return libs;
            }
            Utils.addFileToList(new File("/data/app-lib/" + pkgName + index + InternalZipConstants.ZIP_FILE_SEPARATOR + libname), libs);
            return libs;
        } catch (FileNotFoundException e) {
            addToLog("Error LP: " + localFile2 + " are not found!\n\nCheck the location libraries to solve problems!\n");
            return null;
        } catch (Exception e2) {
            addToLog("Exception e" + e2.toString());
            return null;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void searchfile(String param, String name) {
        try {
            if (new File(name).exists() && !name.startsWith("/mnt/sdcard")) {
                localFile2 = new File(name);
            } else if (name.startsWith("/mnt/sdcard") || name.startsWith("/sdcard")) {
                ArrayList<String> replacers = new ArrayList();
                replacers.add("/mnt/sdcard/");
                replacers.add("/storage/emulated/legacy/");
                replacers.add("/storage/emulated/0/");
                replacers.add("/storage/sdcard0/");
                replacers.add("/storage/sdcard/");
                replacers.add("/storage/sdcard1/");
                replacers.add("/sdcard/");
                Iterator it = replacers.iterator();
                while (it.hasNext()) {
                    String replacer = (String) it.next();
                    localFile2 = new File(name.replace("/mnt/sdcard/", replacer));
                    if (localFile2.exists()) {
                        new File(replacer + "test.tmp").createNewFile();
                        if (new File(replacer + "test.tmp").exists()) {
                            new File(replacer + "test.tmp").delete();
                            break;
                        }
                        localFile2 = new File("/figjvaja_papka");
                    }
                }
                if (!localFile2.exists()) {
                    throw new FileNotFoundException();
                }
            } else if (name.startsWith("/data/data/" + pkgName + "/shared_prefs/") || name.startsWith("/dbdata/databases/" + pkgName + "/shared_prefs/") || name.startsWith("/shared_prefs/")) {
                String rep_str = BuildConfig.VERSION_NAME;
                if (name.startsWith("/data/data/" + pkgName + "/shared_prefs/")) {
                    rep_str = "/data/data/" + pkgName + "/shared_prefs/";
                }
                if (name.startsWith("/dbdata/databases/" + pkgName + "/shared_prefs/")) {
                    rep_str = "/dbdata/databases/" + pkgName + "/shared_prefs/";
                }
                if (name.startsWith("/shared_prefs/")) {
                    rep_str = "/shared_prefs/";
                }
                localFile2 = new File(name.replace(rep_str, "/data/data/" + pkgName + "/shared_prefs/"));
                if (!localFile2.exists()) {
                    localFile2 = new File(name.replace(rep_str, "/dbdata/databases/" + pkgName + "/shared_prefs/"));
                }
                if (!localFile2.exists()) {
                    throw new FileNotFoundException();
                }
            } else {
                localFile2 = new File("/data/data/" + param + name);
                if (!localFile2.exists()) {
                    localFile2 = new File("/mnt/asec/" + param + "-1" + name);
                }
                if (!localFile2.exists()) {
                    localFile2 = new File("/mnt/asec/" + param + "-2" + name);
                }
                if (!localFile2.exists()) {
                    localFile2 = new File("/mnt/asec/" + param + name);
                }
                if (!localFile2.exists()) {
                    localFile2 = new File("/sd-ext/data/" + param + name);
                }
                if (!localFile2.exists()) {
                    localFile2 = new File("/data/sdext2/" + param + name);
                }
                if (!localFile2.exists()) {
                    localFile2 = new File("/data/sdext1/" + param + name);
                }
                if (!localFile2.exists()) {
                    localFile2 = new File("/data/sdext/" + param + name);
                }
                if (!localFile2.exists()) {
                    String re = new Utils("fgh").findFile(new File("/data/data/" + param), name);
                    if (!re.equals(BuildConfig.VERSION_NAME)) {
                        localFile2 = new File(re);
                    }
                }
                if (!localFile2.exists()) {
                    throw new FileNotFoundException();
                }
            }
        } catch (FileNotFoundException e) {
            addToLog("Error LP: " + name + " are not found!\n\nRun the application file to appear in the folder with the data.!\n");
        } catch (Exception e2) {
            addToLog("Exception e" + e2.toString());
        }
    }

    public static void unzip(File apk) {
        classesFiles.clear();
        boolean found2 = false;
        String[] strArr;
        try {
            FileInputStream fin = new FileInputStream(apk);
            ZipInputStream zipInputStream = new ZipInputStream(fin);
            for (ZipEntry ze = zipInputStream.getNextEntry(); ze != null && true; ze = zipInputStream.getNextEntry()) {
                String haystack = ze.getName();
                if (haystack.toLowerCase().startsWith("classes") && haystack.endsWith(".dex") && !haystack.contains(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                    FileOutputStream fout = new FileOutputStream(dir + InternalZipConstants.ZIP_FILE_SEPARATOR + haystack);
                    byte[] buffer = new byte[BUFFER];
                    while (true) {
                        int length = zipInputStream.read(buffer);
                        if (length == -1) {
                            break;
                        }
                        fout.write(buffer, beginTag, length);
                    }
                    zipInputStream.closeEntry();
                    fout.close();
                    classesFiles.add(new File(dir + InternalZipConstants.ZIP_FILE_SEPARATOR + haystack));
                    strArr = new String[otherfilesTag];
                    strArr[beginTag] = "chmod";
                    strArr[classesTag] = "777";
                    strArr[libTagALL] = dir + InternalZipConstants.ZIP_FILE_SEPARATOR + haystack;
                    Utils.run_all_no_root(strArr);
                }
                if (haystack.equals("AndroidManifest.xml")) {
                    FileOutputStream fout2 = new FileOutputStream(dir + InternalZipConstants.ZIP_FILE_SEPARATOR + "AndroidManifest.xml");
                    byte[] buffer2 = new byte[BUFFER];
                    while (true) {
                        int length2 = zipInputStream.read(buffer2);
                        if (length2 == -1) {
                            break;
                        }
                        fout2.write(buffer2, beginTag, length2);
                    }
                    zipInputStream.closeEntry();
                    fout2.close();
                    strArr = new String[otherfilesTag];
                    strArr[beginTag] = "chmod";
                    strArr[classesTag] = "777";
                    strArr[libTagALL] = dir + InternalZipConstants.ZIP_FILE_SEPARATOR + "AndroidManifest.xml";
                    Utils.run_all_no_root(strArr);
                    found2 = true;
                }
                if (false && found2) {
                    break;
                }
            }
            zipInputStream.close();
            fin.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                ZipFile zipFile = new ZipFile(apk);
                zipFile.extractFile("classes.dex", dir);
                strArr = new String[otherfilesTag];
                strArr[beginTag] = "chmod";
                strArr[classesTag] = "777";
                strArr[libTagALL] = dir + InternalZipConstants.ZIP_FILE_SEPARATOR + "classes.dex";
                Utils.run_all_no_root(strArr);
                classesFiles.add(new File(dir + InternalZipConstants.ZIP_FILE_SEPARATOR + "classes.dex"));
                zipFile.extractFile("AndroidManifest.xml", dir);
                strArr = new String[otherfilesTag];
                strArr[beginTag] = "chmod";
                strArr[classesTag] = "777";
                strArr[libTagALL] = dir + InternalZipConstants.ZIP_FILE_SEPARATOR + "AndroidManifest.xml";
                Utils.run_all_no_root(strArr);
            } catch (ZipException e1) {
                e1.printStackTrace();
                addToLog("Error LP: Error classes.dex decompress! " + e1);
                addToLog("Exception e1" + e.toString());
            } catch (Exception e12) {
                e12.printStackTrace();
                addToLog("Error LP: Error classes.dex decompress! " + e12);
                addToLog("Exception e1" + e.toString());
            }
            addToLog("Exception e" + e.toString());
        }
    }

    public static void clearTemp() {
        try {
            File tempdex = new File(dir + "/AndroidManifest.xml");
            if (tempdex.exists()) {
                tempdex.delete();
            }
            if (classesFiles != null && classesFiles.size() > 0) {
                Iterator it = classesFiles.iterator();
                while (it.hasNext()) {
                    tempdex = new File(dir + InternalZipConstants.ZIP_FILE_SEPARATOR + ((File) it.next()).getName());
                    if (tempdex.exists()) {
                        tempdex.delete();
                    }
                }
            }
            tempdex = new File(dir + "/classes.dex");
            if (tempdex.exists()) {
                tempdex.delete();
            }
            tempdex = new File(dir + "/classes1.dex");
            if (tempdex.exists()) {
                tempdex.delete();
            }
            tempdex = new File(dir + "/classes2.dex");
            if (tempdex.exists()) {
                tempdex.delete();
            }
            tempdex = new File(dir + "/classes3.dex");
            if (tempdex.exists()) {
                tempdex.delete();
            }
            tempdex = new File(dir + "/classes4.dex");
            if (tempdex.exists()) {
                tempdex.delete();
            }
            tempdex = new File(dir + "/classes5.dex");
            if (tempdex.exists()) {
                tempdex.delete();
            }
            tempdex = new File(dir + "/classes6.dex");
            if (tempdex.exists()) {
                tempdex.delete();
            }
            tempdex = new File(dir + "/classes.dex.apk");
            if (tempdex.exists()) {
                tempdex.delete();
            }
            tempdex = new File(dir + "/classes.dex.dex");
            if (tempdex.exists()) {
                tempdex.delete();
            }
        } catch (Exception e) {
            addToLog(BuildConfig.VERSION_NAME + e.toString());
        }
    }
}
