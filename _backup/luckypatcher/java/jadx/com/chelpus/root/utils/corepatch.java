package com.chelpus.root.utils;

import com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.util.InternalZipConstants;

public class corepatch {
    public static final byte[] MAGIC = new byte[]{(byte) 100, (byte) 101, (byte) 121, (byte) 10, (byte) 48, (byte) 51, (byte) 53, (byte) 0};
    public static int adler;
    public static MappedByteBuffer fileBytes = null;
    public static byte[] lastByteReplace = null;
    public static int lastPatchPosition = 0;
    public static boolean not_found_bytes_for_patch = false;
    public static boolean onlyDalvik = false;
    public static String toolfilesdir = BuildConfig.VERSION_NAME;

    static class C05551 {
        C05551() {
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void main(String[] paramArrayOfString) {
        Exception e;
        boolean pattern2;
        boolean pattern3;
        boolean pattern4;
        boolean patchOat2;
        boolean patchOat4;
        FileChannel ChannelDex;
        int posit;
        File file;
        String indexDir;
        boolean patchOat5;
        boolean patchOat6;
        File fp;
        boolean isOat;
        boolean patch4;
        long j;
        String destination;
        ArrayList<AddFilesItem> files;
        File d;
        String source;
        Utils.startRootJava(new C05551());
        boolean pattern1 = false;
        boolean pattern22 = false;
        boolean pattern32 = false;
        boolean pattern42 = false;
        byte[] byteOrig2 = null;
        byte[] mask2 = null;
        byte[] byteReplace2 = null;
        byte[] rep_mask2 = null;
        byte[] byteOrigOatUpd1 = null;
        byte[] maskOatUpd1 = null;
        byte[] byteReplaceOatUpd1 = null;
        byte[] rep_maskOatUpd1 = null;
        byte[] byteOrigOat1 = null;
        byte[] maskOat1 = null;
        byte[] byteReplaceOat1 = null;
        byte[] rep_maskOat1 = null;
        byte[] byteOrigOat2 = null;
        byte[] maskOat2 = null;
        byte[] byteReplaceOat2 = null;
        byte[] rep_maskOat2 = null;
        byte[] byteOrigOat3 = null;
        byte[] maskOat3 = null;
        byte[] byteReplaceOat3 = null;
        byte[] rep_maskOat3 = null;
        byte[] byteOrigOatUpd4 = null;
        byte[] maskOatUpd4 = null;
        byte[] byteReplaceOatUpd4 = null;
        byte[] rep_maskOatUpd4 = null;
        byte[] byteOrigOatUpd5 = null;
        byte[] maskOatUpd5 = null;
        byte[] byteReplaceOatUpd5 = null;
        byte[] rep_maskOatUpd5 = null;
        byte[] byteOrigOat4 = null;
        byte[] maskOat4 = null;
        byte[] byteReplaceOat4 = null;
        byte[] rep_maskOat4 = null;
        byte[] byteOrigOat5 = null;
        byte[] maskOat5 = null;
        byte[] byteReplaceOat5 = null;
        byte[] rep_maskOat5 = null;
        byte[] byteOrigOat6 = null;
        byte[] maskOat6 = null;
        byte[] byteReplaceOat6 = null;
        byte[] rep_maskOat6 = null;
        byte[] byteOrigOat7 = null;
        byte[] maskOat7 = null;
        byte[] byteReplaceOat7 = null;
        byte[] rep_maskOat7 = null;
        byte[] byteOrigSOat1 = null;
        byte[] maskSOat1 = null;
        byte[] byteReplaceSOat1 = null;
        byte[] rep_maskSOat1 = null;
        byte[] byteOrigSOat2 = null;
        byte[] maskSOat2 = null;
        byte[] byteReplaceSOat2 = null;
        byte[] rep_maskSOat2 = null;
        byte[] byteOrigSOat3 = null;
        byte[] maskSOat3 = null;
        byte[] byteReplaceSOat3 = null;
        byte[] rep_maskSOat3 = null;
        byte[] byteOrigSOat4 = null;
        byte[] maskSOat4 = null;
        byte[] byteReplaceSOat4 = null;
        byte[] rep_maskSOat4 = null;
        byte[] byteOrigSOat5 = null;
        byte[] maskSOat5 = null;
        byte[] byteReplaceSOat5 = null;
        byte[] rep_maskSOat5 = null;
        byte[] byteOrigSOat6 = null;
        byte[] maskSOat6 = null;
        byte[] byteReplaceSOat6 = null;
        byte[] rep_maskSOat6 = null;
        byte[] byteOrigSOat7 = null;
        byte[] maskSOat7 = null;
        byte[] byteReplaceSOat7 = null;
        byte[] rep_maskSOat7 = null;
        byte[] byteOrigSOat8 = null;
        byte[] maskSOat8 = null;
        byte[] byteReplaceSOat8 = null;
        byte[] rep_maskSOat8 = null;
        byte[] byteOrigSOat9 = null;
        byte[] maskSOat9 = null;
        byte[] byteReplaceSOat9 = null;
        byte[] rep_maskSOat9 = null;
        byte[] byteOrigSOat10 = null;
        byte[] maskSOat10 = null;
        byte[] byteReplaceSOat10 = null;
        byte[] rep_maskSOat10 = null;
        byte[] byteOrigSOat11 = null;
        byte[] maskSOat11 = null;
        byte[] byteReplaceSOat11 = null;
        byte[] rep_maskSOat11 = null;
        byte[] byteOrigSOat12 = null;
        byte[] maskSOat12 = null;
        byte[] byteReplaceSOat12 = null;
        byte[] rep_maskSOat12 = null;
        byte[] byteOrigSOat13 = null;
        byte[] maskSOat13 = null;
        byte[] byteReplaceSOat13 = null;
        byte[] rep_maskSOat13 = null;
        byte[] bArr = null;
        byte[] bArr2 = null;
        byte[] bArr3 = null;
        byte[] bArr4 = null;
        byte[] bArr5 = null;
        byte[] bArr6 = null;
        byte[] bArr7 = null;
        byte[] bArr8 = null;
        byte[] bArr9 = null;
        byte[] bArr10 = null;
        byte[] bArr11 = null;
        byte[] bArr12 = null;
        byte[] bArr13 = null;
        byte[] bArr14 = null;
        byte[] bArr15 = null;
        byte[] bArr16 = null;
        byte[] bArr17 = null;
        byte[] bArr18 = null;
        byte[] bArr19 = null;
        byte[] bArr20 = null;
        byte[] bArr21 = null;
        byte[] bArr22 = null;
        byte[] bArr23 = null;
        byte[] bArr24 = null;
        byte[] bArr25 = null;
        byte[] bArr26 = null;
        byte[] bArr27 = null;
        byte[] bArr28 = null;
        int counter8 = 0;
        byte[] bArr29 = null;
        byte[] bArr30 = null;
        byte[] bArr31 = null;
        byte[] bArr32 = null;
        int counter9 = 0;
        byte[] bArr33 = null;
        byte[] bArr34 = null;
        byte[] bArr35 = null;
        byte[] bArr36 = null;
        byte[] bArr37 = null;
        byte[] bArr38 = null;
        byte[] bArr39 = null;
        byte[] bArr40 = null;
        byte[] bArr41 = null;
        byte[] bArr42 = null;
        byte[] bArr43 = null;
        byte[] bArr44 = null;
        byte[] byteOrigS13 = null;
        byte[] maskS13 = null;
        byte[] byteReplaceS13 = null;
        byte[] rep_maskS13 = null;
        byte[] byteOrig3 = null;
        byte[] byteReplace3 = null;
        byte[] byteOrig4 = null;
        byte[] mask4 = null;
        byte[] byteReplace4 = null;
        byte[] rep_mask4 = null;
        byte[] byteOrig5 = null;
        byte[] mask5 = null;
        byte[] byteReplace5 = null;
        byte[] rep_mask5 = null;
        byte[] byteOrig6 = null;
        byte[] mask6 = null;
        byte[] byteReplace6 = null;
        byte[] rep_mask6 = null;
        byte[] byteOrig7 = null;
        byte[] mask7 = null;
        byte[] byteReplace7 = null;
        byte[] rep_mask7 = null;
        byte[] byteOrig8 = null;
        byte[] mask8 = null;
        byte[] byteReplace8 = null;
        byte[] rep_mask8 = null;
        byte[] byteOrig9 = null;
        byte[] mask9 = null;
        byte[] byteReplace9 = null;
        byte[] rep_mask9 = null;
        byte[] bArr45 = null;
        byte[] bArr46 = null;
        byte[] bArr47 = null;
        byte[] bArr48 = null;
        byte[] bArr49 = null;
        byte[] bArr50 = null;
        byte[] bArr51 = null;
        byte[] bArr52 = null;
        byte[] bArr53 = null;
        byte[] bArr54 = null;
        byte[] bArr55 = null;
        byte[] bArr56 = null;
        byte[] bArr57 = null;
        byte[] bArr58 = null;
        byte[] bArr59 = null;
        byte[] bArr60 = null;
        byte[] bArr61 = null;
        byte[] bArr62 = null;
        byte[] bArr63 = null;
        byte[] bArr64 = null;
        byte[] bArr65 = null;
        byte[] bArr66 = null;
        byte[] bArr67 = null;
        byte[] bArr68 = null;
        byte[] bArr69 = null;
        byte[] bArr70 = null;
        byte[] bArr71 = null;
        byte[] bArr72 = null;
        byte[] bArr73 = null;
        byte[] bArr74 = null;
        byte[] bArr75 = null;
        byte[] bArr76 = null;
        byte[] bArr77 = null;
        byte[] bArr78 = null;
        byte[] bArr79 = null;
        byte[] bArr80 = null;
        byte[] bArr81 = null;
        byte[] bArr82 = null;
        byte[] bArr83 = null;
        byte[] bArr84 = null;
        byte[] bArr85 = null;
        byte[] bArr86 = null;
        byte[] bArr87 = null;
        byte[] bArr88 = null;
        byte[] bArr89 = null;
        byte[] bArr90 = null;
        byte[] bArr91 = null;
        byte[] bArr92 = null;
        try {
            System.out.println(paramArrayOfString[0]);
            System.out.println(paramArrayOfString[1]);
            System.out.println(paramArrayOfString[2]);
            System.out.println(paramArrayOfString[3]);
            System.out.println(paramArrayOfString[4]);
            if (paramArrayOfString[4] != null && paramArrayOfString[4].equals("OnlyDalvik")) {
                onlyDalvik = true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (paramArrayOfString[3] != null) {
            toolfilesdir = paramArrayOfString[3];
        }
        if (!(paramArrayOfString[4].equals("framework") || paramArrayOfString[4].equals("OnlyDalvik"))) {
            Utils.remount("/system", InternalZipConstants.WRITE_MODE);
        }
        if ((paramArrayOfString[1].contains("core.odex") || paramArrayOfString[1].contains("core.jar") || paramArrayOfString[1].contains("core-libart.jar") || paramArrayOfString[1].contains("boot.oat")) && paramArrayOfString[4].equals("framework")) {
            pattern1 = true;
            pattern22 = true;
            listAppsFragment.startUnderRoot = Boolean.valueOf(false);
            paramArrayOfString[0] = "patch";
        }
        if ((paramArrayOfString[1].contains("services.jar") || paramArrayOfString[1].contains("services.odex")) && paramArrayOfString[4].equals("framework")) {
            pattern32 = true;
            pattern42 = false;
            listAppsFragment.startUnderRoot = Boolean.valueOf(false);
            paramArrayOfString[0] = "patch";
        }
        if (paramArrayOfString[0].contains("patch")) {
            if (paramArrayOfString[0].contains("_patch1")) {
                pattern1 = true;
            }
            if (paramArrayOfString[0].contains("_patch2")) {
                pattern2 = true;
            } else {
                pattern2 = pattern22;
            }
            if (paramArrayOfString[0].contains("_patch3")) {
                pattern3 = true;
            } else {
                pattern3 = pattern32;
            }
            if (paramArrayOfString[0].contains("_patch4")) {
                pattern4 = true;
            } else {
                pattern4 = pattern42;
            }
            String original = "11 90 11 99 01 29 0F D1 01 26 28 1C C0 68 39 1C";
            String replace = "?? ?? 01 21 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigOatUpd1 = new byte[original.split("[ \t]+").length];
            maskOatUpd1 = new byte[original.split("[ \t]+").length];
            byteReplaceOatUpd1 = new byte[replace.split("[ \t]+").length];
            rep_maskOatUpd1 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOatUpd1, maskOatUpd1, byteReplaceOatUpd1, rep_maskOatUpd1);
            Utils.convertStringToArraysPatch(original, replace, byteOrigOatUpd1, maskOatUpd1, byteReplaceOatUpd1, rep_maskOatUpd1);
            original = "39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigOat1 = new byte[original.split("[ \t]+").length];
            maskOat1 = new byte[original.split("[ \t]+").length];
            byteReplaceOat1 = new byte[replace.split("[ \t]+").length];
            rep_maskOat1 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOat1, maskOat1, byteReplaceOat1, rep_maskOat1);
            original = "08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigOat2 = new byte[original.split("[ \t]+").length];
            maskOat2 = new byte[original.split("[ \t]+").length];
            byteReplaceOat2 = new byte[replace.split("[ \t]+").length];
            rep_maskOat2 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOat2, maskOat2, byteReplaceOat2, rep_maskOat2);
            original = "56 45 00 F0 07 80 01 3C 00 F0 31 80 05 98";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20";
            byteOrigOat3 = new byte[original.split("[ \t]+").length];
            maskOat3 = new byte[original.split("[ \t]+").length];
            byteReplaceOat3 = new byte[replace.split("[ \t]+").length];
            rep_maskOat3 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOat3, maskOat3, byteReplaceOat3, rep_maskOat3);
            original = "89 44 24 40 8B 5C 24 40 83 FB 01 75 32 BE 01 00 00 00 8B C5 8B 40 0C 8B CF";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigOatUpd4 = new byte[original.split("[ \t]+").length];
            maskOatUpd4 = new byte[original.split("[ \t]+").length];
            byteReplaceOatUpd4 = new byte[replace.split("[ \t]+").length];
            rep_maskOatUpd4 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOatUpd4, maskOatUpd4, byteReplaceOatUpd4, rep_maskOatUpd4);
            original = "8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01";
            byteOrigOat4 = new byte[original.split("[ \t]+").length];
            maskOat4 = new byte[original.split("[ \t]+").length];
            byteReplaceOat4 = new byte[replace.split("[ \t]+").length];
            rep_maskOat4 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOat4, maskOat4, byteReplaceOat4, rep_maskOat4);
            original = "8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5 8B 6C 24 20";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01 ?? ?? ?? ??";
            byteOrigOat5 = new byte[original.split("[ \t]+").length];
            maskOat5 = new byte[original.split("[ \t]+").length];
            byteReplaceOat5 = new byte[replace.split("[ \t]+").length];
            rep_maskOat5 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOat5, maskOat5, byteReplaceOat5, rep_maskOat5);
            original = "33 D2 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00";
            replace = "B3 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigOat6 = new byte[original.split("[ \t]+").length];
            maskOat6 = new byte[original.split("[ \t]+").length];
            byteReplaceOat6 = new byte[replace.split("[ \t]+").length];
            rep_maskOat6 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOat6, maskOat6, byteReplaceOat6, rep_maskOat6);
            original = "E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 61 02 00 54 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 35 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigOatUpd5 = new byte[original.split("[ \t]+").length];
            maskOatUpd5 = new byte[original.split("[ \t]+").length];
            byteReplaceOatUpd5 = new byte[replace.split("[ \t]+").length];
            rep_maskOatUpd5 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOatUpd5, maskOatUpd5, byteReplaceOatUpd5, rep_maskOatUpd5);
            original = "F7 03 01 AA F9 03 02 AA FA 03 1F 2A F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? 3A 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigOat7 = new byte[original.split("[ \t]+").length];
            maskOat7 = new byte[original.split("[ \t]+").length];
            byteReplaceOat7 = new byte[replace.split("[ \t]+").length];
            rep_maskOat7 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOat7, maskOat7, byteReplaceOat7, rep_maskOat7);
            original = "CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 0B 98";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20";
            byteOrigSOat1 = new byte[original.split("[ \t]+").length];
            maskSOat1 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat1 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat1 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat1, maskSOat1, byteReplaceSOat1, rep_maskSOat1);
            original = "00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9D 42";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9B ??";
            byteOrigSOat2 = new byte[original.split("[ \t]+").length];
            maskSOat2 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat2 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat2 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat2, maskSOat2, byteReplaceSOat2, rep_maskSOat2);
            original = "2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 D0 F8 BC 01 D0 F8 28 E0 F0 47";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 0D B0 BD E8 E0 8D ?? ??";
            byteOrigSOat3 = new byte[original.split("[ \t]+").length];
            maskSOat3 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat3 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat3 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat3, maskSOat3, byteReplaceSOat3, rep_maskSOat3);
            original = "1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 F0 47 14 90 14 ?? 00 ?? ?? D1";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20 ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigSOat4 = new byte[original.split("[ \t]+").length];
            maskSOat4 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat4 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat4 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat4, maskSOat4, byteReplaceSOat4, rep_maskSOat4);
            original = "F0 47 82 46 BA F1 00 0F ?? D1 01 3C ?? F0 ?? ?? 28 ?? 00 2B";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigSOat5 = new byte[original.split("[ \t]+").length];
            maskSOat5 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat5 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat5 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat5, maskSOat5, byteReplaceSOat5, rep_maskSOat5);
            original = "F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? D1 D9 F8 ?? ?? 39 1C";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ??";
            byteOrigSOat6 = new byte[original.split("[ \t]+").length];
            maskSOat6 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat6 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat6 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat6, maskSOat6, byteReplaceSOat6, rep_maskSOat6);
            original = "50 F8 0C 00 D0 F8 ?? E0 F0 47 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigSOat7 = new byte[original.split("[ \t]+").length];
            maskSOat7 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat7 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat7 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat7, maskSOat7, byteReplaceSOat7, rep_maskSOat7);
            original = "BA 01 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 75 44 8B 54 24 58 85 D2 75 23";
            replace = "?? 00 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? 90 90";
            byteOrigSOat8 = new byte[original.split("[ \t]+").length];
            maskSOat8 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat8 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat8 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat8, maskSOat8, byteReplaceSOat8, rep_maskSOat8);
            original = "85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B E9";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ED";
            byteOrigSOat9 = new byte[original.split("[ \t]+").length];
            maskSOat9 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat9 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat9 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat9, maskSOat9, byteReplaceSOat9, rep_maskSOat9);
            original = "3D 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3B 02 00 B5 9C 01 00 B5";
            replace = "FD 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FD 03 1F 2A FD 03 1F 2A";
            byteOrigSOat10 = new byte[original.split("[ \t]+").length];
            maskSOat10 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat10 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat10 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat10, maskSOat10, byteReplaceSOat10, rep_maskSOat10);
            original = "3C 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3A 02 00 B5 9B 01 00 B5";
            replace = "FC 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FC 03 1F 2A FC 03 1F 2A";
            byteOrigSOat11 = new byte[original.split("[ \t]+").length];
            maskSOat11 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat11 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat11 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat11, maskSOat11, byteReplaceSOat11, rep_maskSOat11);
            original = "E2 33 00 B9 E3 33 40 B9 9F 02 03 6B 6A 13 00 54 A0 16 40 B9";
            replace = "?? ?? ?? ?? E3 03 14 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigSOat12 = new byte[original.split("[ \t]+").length];
            maskSOat12 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat12 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat12 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat12, maskSOat12, byteReplaceSOat12, rep_maskSOat12);
            original = "D6 02 19 12 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA";
            replace = "D6 02 19 32 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigSOat13 = new byte[original.split("[ \t]+").length];
            maskSOat13 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat13 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat13 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat13, maskSOat13, byteReplaceSOat13, rep_maskSOat13);
            bArr = new byte[16];
            bArr = new byte[]{(byte) 57, (byte) 102, (byte) 8, (byte) 0, (byte) 57, (byte) 102, (byte) 4, (byte) 0, (byte) 18, (byte) 22, (byte) 15, (byte) 6, (byte) 18, (byte) -10, (byte) 40, (byte) -2};
            bArr2 = new byte[16];
            byte[] bArr93 = bArr2;
            boolean[] zArr = new boolean[]{false, true, false, false, false, true, false, false, false, false, false, false, false, false, false, false};
            bArr3 = new byte[16];
            bArr3 = new byte[]{(byte) 57, (byte) 102, (byte) 8, (byte) 0, (byte) 57, (byte) 102, (byte) 4, (byte) 0, (byte) 18, (byte) 22, (byte) 18, (byte) 6, (byte) 18, (byte) 6, (byte) 15, (byte) 6};
            bArr4 = new byte[16];
            bArr93 = bArr4;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, true, true, true, true, true, true};
            bArr5 = new byte[24];
            bArr5 = new byte[]{(byte) 10, (byte) 102, (byte) 57, (byte) 102, (byte) 102, (byte) 102, (byte) 26, (byte) 102, (byte) 102, (byte) 102, (byte) 26, (byte) 102, (byte) 102, (byte) 102, (byte) 113, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 19, (byte) 102, (byte) -19, (byte) -1};
            bArr6 = new byte[24];
            bArr93 = bArr6;
            zArr = new boolean[]{false, true, false, true, true, true, false, true, true, true, false, true, true, true, false, true, true, true, true, true, false, true, false, false};
            bArr7 = new byte[24];
            bArr7 = new byte[]{(byte) 18, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 26, (byte) 102, (byte) 102, (byte) 102, (byte) 26, (byte) 102, (byte) 102, (byte) 102, (byte) 113, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 19, (byte) 102, (byte) -19, (byte) -1};
            bArr8 = new byte[24];
            bArr93 = bArr8;
            zArr = new boolean[]{true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            bArr9 = new byte[19];
            bArr9 = new byte[]{Byte.MIN_VALUE, (byte) 0, (byte) 57, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 53, (byte) -121, (byte) 102, (byte) 102, (byte) 26};
            bArr10 = new byte[19];
            bArr93 = bArr10;
            zArr = new boolean[]{false, false, false, true, true, true, true, true, true, true, true, true, true, true, false, false, true, true, false};
            bArr11 = new byte[19];
            bArr11 = new byte[]{Byte.MIN_VALUE, (byte) 0, (byte) 57, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 53, (byte) -120, (byte) 102, (byte) 102, (byte) 26};
            bArr12 = new byte[19];
            bArr93 = bArr12;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false};
            bArr13 = new byte[14];
            bArr13 = new byte[]{(byte) 57, (byte) 102, (byte) 7, (byte) 0, (byte) 57, (byte) 102, (byte) 3, (byte) 0, (byte) 15, (byte) 6, (byte) 18, (byte) -10, (byte) 40, (byte) -2};
            bArr14 = new byte[14];
            bArr93 = bArr14;
            zArr = new boolean[]{false, true, false, false, false, true, false, false, false, false, false, false, false, false};
            bArr15 = new byte[14];
            bArr15 = new byte[]{(byte) 57, (byte) 102, (byte) 7, (byte) 0, (byte) 57, (byte) 102, (byte) 3, (byte) 0, (byte) 18, (byte) 6, (byte) 18, (byte) 6, (byte) 15, (byte) 6};
            bArr16 = new byte[14];
            bArr93 = bArr16;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, true, true, true, true, true, true};
            bArr17 = new byte[20];
            bArr17 = new byte[]{(byte) 84, (byte) 102, (byte) 102, (byte) 102, (byte) 110, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 102, (byte) 56, (byte) 4, (byte) 4, (byte) 0, (byte) 18, (byte) 20, (byte) 15, (byte) 4};
            bArr18 = new byte[20];
            bArr93 = bArr18;
            zArr = new boolean[]{false, true, true, true, false, false, true, true, true, true, false, true, false, false, false, false, false, false, false, false};
            bArr19 = new byte[20];
            bArr19 = new byte[]{(byte) 84, (byte) 102, (byte) 102, (byte) 102, (byte) 110, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 102, (byte) 57, (byte) 4, (byte) 4, (byte) 0, (byte) 18, (byte) 20, (byte) 15, (byte) 4};
            bArr20 = new byte[20];
            bArr93 = bArr20;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, true, false, true, false, false, false, false, false, false, false};
            bArr21 = new byte[14];
            bArr21 = new byte[]{(byte) 10, (byte) 102, (byte) 57, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 19, (byte) 102, (byte) -19, (byte) -1};
            bArr22 = new byte[14];
            bArr93 = bArr22;
            zArr = new boolean[]{false, true, false, true, true, true, false, true, true, true, false, true, false, false};
            bArr23 = new byte[14];
            bArr23 = new byte[]{(byte) 18, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 19, (byte) 102, (byte) -19, (byte) -1};
            bArr24 = new byte[14];
            bArr93 = bArr24;
            zArr = new boolean[]{true, false, true, false, false, false, false, false, false, false, false, false, false, false};
            bArr25 = new byte[28];
            bArr25 = new byte[]{(byte) 29, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 57, (byte) 102, (byte) 102, (byte) 0, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 30, (byte) 102};
            bArr26 = new byte[28];
            bArr93 = bArr26;
            zArr = new boolean[]{false, true, true, true, true, true, true, true, false, true, false, true, true, false, true, true, true, true, true, true, true, true, true, true, false, true, false, true};
            bArr27 = new byte[28];
            bArr27 = new byte[]{(byte) 29, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 51, (byte) 102, (byte) 102, (byte) 0, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 30, (byte) 102};
            bArr28 = new byte[28];
            bArr93 = bArr28;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            counter8 = 2;
            bArr29 = new byte[38];
            bArr29 = new byte[]{(byte) 29, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 57, (byte) 102, (byte) 102, (byte) 0, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 2, (byte) 102, (byte) 102, (byte) 102, (byte) 2, (byte) 102, (byte) 102, (byte) 102};
            bArr30 = new byte[38];
            bArr30 = new byte[]{(byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1};
            bArr31 = new byte[38];
            bArr31 = new byte[]{(byte) 29, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 51, (byte) 102, (byte) 102, (byte) 0, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 2, (byte) 102, (byte) 102, (byte) 102, (byte) 2, (byte) 102, (byte) 102, (byte) 102};
            bArr32 = new byte[38];
            bArr32 = new byte[]{(byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 2, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0};
            counter9 = 2;
            bArr33 = new byte[29];
            bArr33 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 102, (byte) 57, (byte) 102, (byte) 102, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 26};
            bArr34 = new byte[29];
            bArr93 = bArr34;
            zArr = new boolean[]{false, true, true, true, false, true, true, true, false, true, true, true, true, true, true, true, true, true, false, true, false, true, true, true, false, true, true, true, false};
            bArr35 = new byte[29];
            bArr35 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 26};
            bArr36 = new byte[29];
            bArr93 = bArr36;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, false, false, false, false, false, false, false, false};
            bArr37 = new byte[29];
            bArr37 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 0, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 102, (byte) 57, (byte) 102, (byte) 102, (byte) 102, (byte) 56};
            bArr38 = new byte[29];
            bArr93 = bArr38;
            zArr = new boolean[]{false, true, true, false, false, true, true, true, true, true, true, true, true, true, false, true, true, true, true, true, true, true, false, true, false, true, true, true, false};
            bArr39 = new byte[29];
            bArr39 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 0, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 56};
            bArr40 = new byte[29];
            bArr93 = bArr40;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, false, false, false, false};
            bArr41 = new byte[25];
            bArr41 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 102, (byte) 57, (byte) 102, (byte) 102, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 26};
            bArr42 = new byte[25];
            bArr93 = bArr42;
            zArr = new boolean[]{false, true, true, true, false, true, true, true, true, true, true, true, true, true, false, true, false, true, true, true, false, true, true, true, false};
            bArr43 = new byte[25];
            bArr43 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 26};
            bArr44 = new byte[25];
            bArr93 = bArr44;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, false, false, false, false, false, false, false, false};
            original = "D5 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00";
            replace = "D6 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigS13 = new byte[original.split("[ \t]+").length];
            maskS13 = new byte[original.split("[ \t]+").length];
            byteReplaceS13 = new byte[replace.split("[ \t]+").length];
            rep_maskS13 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigS13, maskS13, byteReplaceS13, rep_maskS13);
            bArr45 = new byte[35];
            bArr45 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr46 = new byte[35];
            bArr46 = new byte[]{(byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 0};
            bArr47 = new byte[35];
            bArr47 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr48 = new byte[35];
            bArr93 = bArr48;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            bArr49 = new byte[36];
            bArr49 = new byte[]{(byte) 82, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 110, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr50 = new byte[36];
            bArr50 = new byte[]{(byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 0};
            bArr51 = new byte[36];
            bArr51 = new byte[]{(byte) 82, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 110, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr52 = new byte[36];
            bArr93 = bArr52;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            bArr53 = new byte[30];
            bArr53 = new byte[]{(byte) 18, (byte) 3, (byte) 33, (byte) 65, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 102, (byte) 102, (byte) 15, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 4, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            bArr54 = new byte[30];
            bArr93 = bArr54;
            zArr = new boolean[]{false, false, false, false, false, true, false, true, true, false, true, true, false, false, false, false, false, true, false, true, true, false, false, true, false, false, false, true, false, false};
            bArr55 = new byte[30];
            bArr55 = new byte[]{(byte) 18, (byte) 19, (byte) 33, (byte) 65, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 102, (byte) 102, (byte) 15, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 4, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            bArr56 = new byte[30];
            bArr93 = bArr56;
            zArr = new boolean[]{true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            bArr57 = new byte[28];
            bArr57 = new byte[]{(byte) 18, (byte) 1, (byte) 33, (byte) 66, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 15, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 4, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            bArr58 = new byte[28];
            bArr93 = bArr58;
            zArr = new boolean[]{false, false, false, false, false, true, false, true, true, false, false, false, false, false, false, true, false, true, true, false, false, true, false, false, false, true, false, false};
            bArr59 = new byte[28];
            bArr59 = new byte[]{(byte) 18, (byte) 17, (byte) 33, (byte) 66, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 15, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 4, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            bArr60 = new byte[28];
            bArr93 = bArr60;
            zArr = new boolean[]{true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            bArr61 = new byte[43];
            bArr61 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 14, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr62 = new byte[43];
            bArr93 = bArr62;
            zArr = new boolean[]{false, true, true, false, false, false, false, false, false, false, false, true, true, false, false, true, true, true, true, true, true, true, true, true, true, false, false, true, true, true, true, false, false, true, false, true, true, true, true, false, false, false, false};
            bArr63 = new byte[43];
            bArr63 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 14, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr64 = new byte[43];
            bArr93 = bArr64;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            bArr65 = new byte[44];
            bArr65 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr66 = new byte[44];
            bArr93 = bArr66;
            zArr = new boolean[]{false, true, true, true, false, true, true, true, false, true, true, true, false, true, true, true, false, true, false, true, true, true, false, true, true, true, false, true, true, true, true, true, false, true, true, true, true, true, true, true, false, false, false, false};
            bArr67 = new byte[44];
            bArr67 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr68 = new byte[44];
            bArr93 = bArr68;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            bArr69 = new byte[70];
            bArr69 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 33, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr70 = new byte[70];
            bArr93 = bArr70;
            zArr = new boolean[]{false, true, true, true, false, true, true, true, false, true, true, true, false, true, true, true, false, true, false, true, true, true, false, true, true, true, false, true, true, true, true, true, false, true, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false};
            bArr71 = new byte[70];
            bArr71 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 33, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr72 = new byte[70];
            bArr93 = bArr72;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            bArr73 = new byte[56];
            bArr73 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 26, (byte) 102, (byte) 102, (byte) 102, (byte) 113, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr74 = new byte[56];
            bArr93 = bArr74;
            zArr = new boolean[]{false, true, true, true, false, true, true, true, false, true, true, true, false, true, true, true, false, true, false, true, true, true, false, true, true, true, false, true, true, true, false, true, true, true, true, true, false, true, false, true, true, true, true, true, false, true, true, true, true, true, true, true, false, false, false, false};
            bArr75 = new byte[56];
            bArr75 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 26, (byte) 102, (byte) 102, (byte) 102, (byte) 113, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr76 = new byte[56];
            bArr93 = bArr76;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, true, true};
            bArr77 = new byte[41];
            bArr77 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr78 = new byte[41];
            bArr93 = bArr78;
            zArr = new boolean[]{false, true, true, false, false, false, false, false, false, false, false, true, true, false, false, true, true, false, false, true, true, true, true, false, false, true, true, true, true, true, true, true, false, true, true, true, true, false, false, false, false};
            bArr79 = new byte[41];
            bArr79 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr80 = new byte[41];
            bArr93 = bArr80;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            bArr81 = new byte[50];
            bArr81 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr82 = new byte[50];
            bArr93 = bArr82;
            zArr = new boolean[]{false, true, true, true, false, true, true, true, false, true, true, true, false, true, true, true, false, true, false, true, true, true, false, true, true, true, false, true, true, true, true, true, false, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false};
            bArr83 = new byte[50];
            bArr83 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr84 = new byte[50];
            bArr93 = bArr84;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            bArr85 = new byte[26];
            bArr85 = new byte[]{(byte) 18, (byte) 2, (byte) 33, (byte) 83, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 15, (byte) 2, (byte) 18, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            bArr86 = new byte[26];
            bArr93 = bArr86;
            zArr = new boolean[]{false, false, false, false, false, true, false, true, true, false, false, false, false, false, false, false, false, true, false, true, true, false, false, true, false, false};
            bArr87 = new byte[26];
            bArr87 = new byte[]{(byte) 18, (byte) 18, (byte) 33, (byte) 83, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 15, (byte) 2, (byte) 18, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            bArr88 = new byte[26];
            bArr93 = bArr88;
            zArr = new boolean[]{true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            bArr89 = new byte[36];
            bArr89 = new byte[]{(byte) -14, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) -8, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr90 = new byte[36];
            bArr93 = bArr90;
            zArr = new boolean[]{false, false, true, true, false, false, false, false, false, false, false, false, true, true, false, false, true, true, false, false, true, true, true, true, false, false, false, false, true, true, true, true, false, false, false, false};
            bArr91 = new byte[36];
            bArr91 = new byte[]{(byte) -14, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) -8, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr92 = new byte[36];
            bArr93 = bArr92;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            pattern42 = pattern4;
            pattern32 = pattern3;
            pattern22 = pattern2;
        }
        if (paramArrayOfString[0].contains("restore")) {
            if (paramArrayOfString[0].contains("restoreCore")) {
                pattern1 = true;
                pattern2 = true;
            } else {
                pattern2 = pattern22;
            }
            if (paramArrayOfString[0].contains("restoreServices")) {
                pattern4 = true;
                pattern3 = true;
            } else {
                pattern4 = pattern42;
                pattern3 = pattern32;
            }
            original = "11 90 01 21 01 29 0F D1 01 26 28 1C C0 68 39 1C";
            replace = "?? ?? 11 99 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigOatUpd1 = new byte[original.split("[ \t]+").length];
            maskOatUpd1 = new byte[original.split("[ \t]+").length];
            byteReplaceOatUpd1 = new byte[replace.split("[ \t]+").length];
            rep_maskOatUpd1 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOatUpd1, maskOatUpd1, byteReplaceOatUpd1, rep_maskOatUpd1);
            Utils.convertStringToArraysPatch(original, replace, byteOrigOatUpd1, maskOatUpd1, byteReplaceOatUpd1, rep_maskOatUpd1);
            original = "39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigOat1 = new byte[original.split("[ \t]+").length];
            maskOat1 = new byte[original.split("[ \t]+").length];
            byteReplaceOat1 = new byte[replace.split("[ \t]+").length];
            rep_maskOat1 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOat1, maskOat1, byteReplaceOat1, rep_maskOat1);
            original = "08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigOat2 = new byte[original.split("[ \t]+").length];
            maskOat2 = new byte[original.split("[ \t]+").length];
            byteReplaceOat2 = new byte[replace.split("[ \t]+").length];
            rep_maskOat2 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOat2, maskOat2, byteReplaceOat2, rep_maskOat2);
            original = "56 45 00 F0 07 80 01 3C 00 F0 31 80 01 20";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 05 98";
            byteOrigOat3 = new byte[original.split("[ \t]+").length];
            maskOat3 = new byte[original.split("[ \t]+").length];
            byteReplaceOat3 = new byte[replace.split("[ \t]+").length];
            rep_maskOat3 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOat3, maskOat3, byteReplaceOat3, rep_maskOat3);
            original = "89 44 24 40 8B 5C 24 40 83 FB 01 90 90 BE 01 00 00 00 8B C5 8B 40 0C 8B CF";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 32 BE ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigOatUpd4 = new byte[original.split("[ \t]+").length];
            maskOatUpd4 = new byte[original.split("[ \t]+").length];
            byteReplaceOatUpd4 = new byte[replace.split("[ \t]+").length];
            rep_maskOatUpd4 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOatUpd4, maskOatUpd4, byteReplaceOatUpd4, rep_maskOatUpd4);
            original = "8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5";
            byteOrigOat4 = new byte[original.split("[ \t]+").length];
            maskOat4 = new byte[original.split("[ \t]+").length];
            byteReplaceOat4 = new byte[replace.split("[ \t]+").length];
            rep_maskOat4 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOat4, maskOat4, byteReplaceOat4, rep_maskOat4);
            original = "8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01 8B 6C 24 20";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5 ?? ?? ?? ??";
            byteOrigOat5 = new byte[original.split("[ \t]+").length];
            maskOat5 = new byte[original.split("[ \t]+").length];
            byteReplaceOat5 = new byte[replace.split("[ \t]+").length];
            rep_maskOat5 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOat5, maskOat5, byteReplaceOat5, rep_maskOat5);
            original = "B3 01 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00";
            replace = "33 D2 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigOat6 = new byte[original.split("[ \t]+").length];
            maskOat6 = new byte[original.split("[ \t]+").length];
            byteReplaceOat6 = new byte[replace.split("[ \t]+").length];
            rep_maskOat6 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOat6, maskOat6, byteReplaceOat6, rep_maskOat6);
            original = "E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 35 00 80 52 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 61 02 00 54 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigOatUpd5 = new byte[original.split("[ \t]+").length];
            maskOatUpd5 = new byte[original.split("[ \t]+").length];
            byteReplaceOatUpd5 = new byte[replace.split("[ \t]+").length];
            rep_maskOatUpd5 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOatUpd5, maskOatUpd5, byteReplaceOatUpd5, rep_maskOatUpd5);
            original = "F7 03 01 AA F9 03 02 AA 3A 00 80 52 F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? FA 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigOat7 = new byte[original.split("[ \t]+").length];
            maskOat7 = new byte[original.split("[ \t]+").length];
            byteReplaceOat7 = new byte[replace.split("[ \t]+").length];
            rep_maskOat7 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigOat7, maskOat7, byteReplaceOat7, rep_maskOat7);
            original = "CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 00 20";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0B 98";
            byteOrigSOat1 = new byte[original.split("[ \t]+").length];
            maskSOat1 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat1 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat1 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat1, maskSOat1, byteReplaceSOat1, rep_maskSOat1);
            original = "00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9B 42";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9D ??";
            byteOrigSOat2 = new byte[original.split("[ \t]+").length];
            maskSOat2 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat2 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat2 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat2, maskSOat2, byteReplaceSOat2, rep_maskSOat2);
            original = "2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 01 20 0D B0 BD E8 E0 8D F0 47";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D0 F8 BC 01 D0 F8 28 E0 ?? ??";
            byteOrigSOat3 = new byte[original.split("[ \t]+").length];
            maskSOat3 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat3 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat3 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat3, maskSOat3, byteReplaceSOat3, rep_maskSOat3);
            original = "1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 00 20 14 90 14 ?? 00 ?? ?? D1";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigSOat4 = new byte[original.split("[ \t]+").length];
            maskSOat4 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat4 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat4 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat4, maskSOat4, byteReplaceSOat4, rep_maskSOat4);
            original = "F0 47 82 46 BA F1 00 0F ?? E0 01 3C ?? F0 ?? ?? 28 ?? 00 2B";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigSOat5 = new byte[original.split("[ \t]+").length];
            maskSOat5 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat5 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat5 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat5, maskSOat5, byteReplaceSOat5, rep_maskSOat5);
            original = "F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? E0 D9 F8 ?? ?? 39 1C";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ??";
            byteOrigSOat6 = new byte[original.split("[ \t]+").length];
            maskSOat6 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat6 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat6 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat6, maskSOat6, byteReplaceSOat6, rep_maskSOat6);
            original = "50 F8 0C 00 D0 F8 ?? E0 01 20 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigSOat7 = new byte[original.split("[ \t]+").length];
            maskSOat7 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat7 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat7 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat7, maskSOat7, byteReplaceSOat7, rep_maskSOat7);
            original = "BA 00 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 90 90 8B 54 24 58 85 D2 90 90";
            replace = "?? 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 44 ?? ?? ?? ?? ?? ?? 75 23";
            byteOrigSOat8 = new byte[original.split("[ \t]+").length];
            maskSOat8 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat8 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat8 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat8, maskSOat8, byteReplaceSOat8, rep_maskSOat8);
            original = "85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B ED";
            replace = "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E9";
            byteOrigSOat9 = new byte[original.split("[ \t]+").length];
            maskSOat9 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat9 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat9 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat9, maskSOat9, byteReplaceSOat9, rep_maskSOat9);
            original = "FD 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FD 03 1F 2A FD 03 1F 2A";
            replace = "3D 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3B 02 00 B5 9C 01 00 B5";
            byteOrigSOat10 = new byte[original.split("[ \t]+").length];
            maskSOat10 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat10 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat10 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat10, maskSOat10, byteReplaceSOat10, rep_maskSOat10);
            original = "FC 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FC 03 1F 2A FC 03 1F 2A";
            replace = "3C 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3A 02 00 B5 9B 01 00 B5";
            byteOrigSOat11 = new byte[original.split("[ \t]+").length];
            maskSOat11 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat11 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat11 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat11, maskSOat11, byteReplaceSOat11, rep_maskSOat11);
            original = "E2 33 00 B9 E3 03 14 2A 9F 02 03 6B 6A 13 00 54 A0 16 40 B9";
            replace = "?? ?? ?? ?? E3 33 40 B9 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigSOat12 = new byte[original.split("[ \t]+").length];
            maskSOat12 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat12 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat12 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat12, maskSOat12, byteReplaceSOat12, rep_maskSOat12);
            original = "D6 02 19 32 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA";
            replace = "D6 02 19 12 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigSOat13 = new byte[original.split("[ \t]+").length];
            maskSOat13 = new byte[original.split("[ \t]+").length];
            byteReplaceSOat13 = new byte[replace.split("[ \t]+").length];
            rep_maskSOat13 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigSOat13, maskSOat13, byteReplaceSOat13, rep_maskSOat13);
            byteOrig2 = new byte[35];
            byteOrig2 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0, (byte) 0, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            mask2 = new byte[35];
            bArr93 = mask2;
            zArr = new boolean[]{false, true, true, false, false, false, false, false, false, false, false, true, true, false, false, true, true, false, false, true, true, true, true, false, false, true, false, true, true, true, true, false, false, false, false};
            byteReplace2 = new byte[35];
            byteReplace2 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            rep_mask2 = new byte[35];
            bArr93 = rep_mask2;
            zArr = new boolean[]{false, false, false, true, true, true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            byteOrig3 = new byte[36];
            byteOrig3 = new byte[]{(byte) 82, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0, (byte) 0, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 110, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            byteReplace3 = new byte[36];
            byteReplace3 = new byte[]{(byte) 82, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 110, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            byteOrig4 = new byte[30];
            byteOrig4 = new byte[]{(byte) 18, (byte) 16, (byte) 15, (byte) 0, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 102, (byte) 102, (byte) 15, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 4, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            mask4 = new byte[30];
            bArr93 = mask4;
            zArr = new boolean[]{false, false, false, false, false, true, false, true, true, false, true, true, false, false, false, false, false, true, false, true, true, false, false, true, false, false, false, true, false, false};
            byteReplace4 = new byte[30];
            byteReplace4 = new byte[]{(byte) 18, (byte) 3, (byte) 33, (byte) 65, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 102, (byte) 102, (byte) 15, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 4, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            rep_mask4 = new byte[30];
            bArr93 = rep_mask4;
            zArr = new boolean[]{true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            byteOrig5 = new byte[28];
            byteOrig5 = new byte[]{(byte) 18, (byte) 16, (byte) 15, (byte) 0, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 15, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 4, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            mask5 = new byte[28];
            bArr93 = mask5;
            zArr = new boolean[]{false, false, false, false, false, true, false, true, true, false, false, false, false, false, false, true, false, true, true, false, false, true, false, false, false, true, false, false};
            byteReplace5 = new byte[28];
            byteReplace5 = new byte[]{(byte) 18, (byte) 1, (byte) 33, (byte) 66, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 15, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 4, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            rep_mask5 = new byte[28];
            bArr93 = rep_mask5;
            zArr = new boolean[]{true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            byteOrig6 = new byte[43];
            byteOrig6 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0, (byte) 0, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            mask6 = new byte[43];
            bArr93 = mask6;
            zArr = new boolean[]{false, true, true, false, false, false, false, false, false, false, false, true, true, false, false, true, true, true, true, true, true, true, true, true, true, false, false, true, true, true, true, false, false, true, false, true, true, true, true, false, false, false, false};
            byteReplace6 = new byte[43];
            byteReplace6 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 14, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            rep_mask6 = new byte[43];
            bArr93 = rep_mask6;
            zArr = new boolean[]{false, false, false, true, true, true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            byteOrig7 = new byte[30];
            byteOrig7 = new byte[]{(byte) 82, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0, (byte) 0, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 56, (byte) 3, (byte) 11, (byte) 0};
            mask7 = new byte[30];
            bArr93 = mask7;
            zArr = new boolean[]{false, false, true, true, false, false, false, false, false, false, false, false, true, true, false, false, true, true, false, false, true, true, true, true, false, false, false, false, false, false};
            byteReplace7 = new byte[30];
            byteReplace7 = new byte[]{(byte) 82, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 56, (byte) 3, (byte) 11, (byte) 0};
            rep_mask7 = new byte[30];
            bArr93 = rep_mask7;
            zArr = new boolean[]{false, false, false, false, true, true, true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            byteOrig8 = new byte[30];
            byteOrig8 = new byte[]{(byte) -14, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0, (byte) 0, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 56, (byte) 3, (byte) 11, (byte) 0};
            mask8 = new byte[30];
            bArr93 = mask8;
            zArr = new boolean[]{false, false, true, true, false, false, false, false, false, false, false, false, true, true, false, false, true, true, false, false, true, true, true, true, false, false, false, false, false, false};
            byteReplace8 = new byte[30];
            byteReplace8 = new byte[]{(byte) -14, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 56, (byte) 3, (byte) 11, (byte) 0};
            rep_mask8 = new byte[30];
            bArr93 = rep_mask8;
            zArr = new boolean[]{false, false, false, false, true, true, true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            byteOrig9 = new byte[38];
            byteOrig9 = new byte[]{(byte) -14, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0, (byte) 0, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 113, (byte) 16, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 1, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 56, (byte) 3, (byte) 11, (byte) 0};
            mask9 = new byte[38];
            bArr93 = mask9;
            zArr = new boolean[]{false, false, true, true, false, false, false, false, false, false, false, false, true, true, false, false, true, true, false, false, true, true, true, true, false, false, false, false, true, true, true, true, false, false, false, false, false, false};
            byteReplace9 = new byte[38];
            byteReplace9 = new byte[]{(byte) -14, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 14, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 113, (byte) 16, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 1, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 56, (byte) 3, (byte) 11, (byte) 0};
            rep_mask9 = new byte[38];
            bArr93 = rep_mask9;
            zArr = new boolean[]{false, false, false, false, true, true, true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            bArr = new byte[16];
            bArr = new byte[]{(byte) 57, (byte) 102, (byte) 8, (byte) 0, (byte) 57, (byte) 102, (byte) 4, (byte) 0, (byte) 18, (byte) 22, (byte) 18, (byte) 6, (byte) 18, (byte) 6, (byte) 15, (byte) 6};
            bArr2 = new byte[16];
            bArr93 = bArr2;
            zArr = new boolean[]{false, true, false, false, false, true, false, false, false, false, false, false, false, false, false, false};
            bArr3 = new byte[16];
            bArr3 = new byte[]{(byte) 57, (byte) 102, (byte) 8, (byte) 0, (byte) 57, (byte) 102, (byte) 4, (byte) 0, (byte) 18, (byte) 22, (byte) 15, (byte) 6, (byte) 18, (byte) -10, (byte) 40, (byte) -2};
            bArr4 = new byte[16];
            bArr93 = bArr4;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, true, true, true, true, true, true};
            bArr5 = new byte[24];
            bArr5 = new byte[]{(byte) 18, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 26, (byte) 102, (byte) 102, (byte) 102, (byte) 26, (byte) 102, (byte) 102, (byte) 102, (byte) 113, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 19, (byte) 102, (byte) -19, (byte) -1};
            bArr6 = new byte[24];
            bArr93 = bArr6;
            zArr = new boolean[]{false, true, false, true, true, true, false, true, true, true, false, true, true, true, false, true, true, true, true, true, false, true, false, false};
            bArr7 = new byte[24];
            bArr7 = new byte[]{(byte) 10, (byte) 102, (byte) 57, (byte) 102, (byte) 102, (byte) 102, (byte) 26, (byte) 102, (byte) 102, (byte) 102, (byte) 26, (byte) 102, (byte) 102, (byte) 102, (byte) 113, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 19, (byte) 102, (byte) -19, (byte) -1};
            bArr8 = new byte[24];
            bArr93 = bArr8;
            zArr = new boolean[]{true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            bArr9 = new byte[19];
            bArr9 = new byte[]{Byte.MIN_VALUE, (byte) 0, (byte) 57, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 53, (byte) -120, (byte) 102, (byte) 102, (byte) 26};
            bArr10 = new byte[19];
            bArr93 = bArr10;
            zArr = new boolean[]{false, false, false, true, true, true, true, true, true, true, true, true, true, true, false, false, true, true, false};
            bArr11 = new byte[19];
            bArr11 = new byte[]{Byte.MIN_VALUE, (byte) 0, (byte) 57, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 53, (byte) -121, (byte) 102, (byte) 102, (byte) 26};
            bArr12 = new byte[19];
            bArr93 = bArr12;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false};
            bArr13 = new byte[14];
            bArr13 = new byte[]{(byte) 57, (byte) 102, (byte) 7, (byte) 0, (byte) 57, (byte) 102, (byte) 3, (byte) 0, (byte) 18, (byte) 6, (byte) 18, (byte) 6, (byte) 15, (byte) 6};
            bArr14 = new byte[14];
            bArr93 = bArr14;
            zArr = new boolean[]{false, true, false, false, false, true, false, false, false, false, false, false, false, false};
            bArr15 = new byte[14];
            bArr15 = new byte[]{(byte) 57, (byte) 102, (byte) 7, (byte) 0, (byte) 57, (byte) 102, (byte) 3, (byte) 0, (byte) 15, (byte) 6, (byte) 18, (byte) -10, (byte) 40, (byte) -2};
            bArr16 = new byte[14];
            bArr93 = bArr16;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, true, true, true, true, true, true};
            bArr17 = new byte[20];
            bArr17 = new byte[]{(byte) 84, (byte) 102, (byte) 102, (byte) 102, (byte) 110, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 102, (byte) 57, (byte) 4, (byte) 4, (byte) 0, (byte) 18, (byte) 20, (byte) 15, (byte) 4};
            bArr18 = new byte[20];
            bArr93 = bArr18;
            zArr = new boolean[]{false, true, true, true, false, false, true, true, true, true, false, true, false, false, false, false, false, false, false, false};
            bArr19 = new byte[20];
            bArr19 = new byte[]{(byte) 84, (byte) 102, (byte) 102, (byte) 102, (byte) 110, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 102, (byte) 56, (byte) 4, (byte) 4, (byte) 0, (byte) 18, (byte) 20, (byte) 15, (byte) 4};
            bArr20 = new byte[20];
            bArr93 = bArr20;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, true, false, true, false, false, false, false, false, false, false};
            bArr21 = new byte[14];
            bArr21 = new byte[]{(byte) 18, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 19, (byte) 102, (byte) -19, (byte) -1};
            bArr22 = new byte[14];
            bArr93 = bArr22;
            zArr = new boolean[]{false, true, false, true, true, true, false, true, true, true, false, true, false, false};
            bArr23 = new byte[14];
            bArr23 = new byte[]{(byte) 10, (byte) 102, (byte) 57, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 19, (byte) 102, (byte) -19, (byte) -1};
            bArr24 = new byte[14];
            bArr93 = bArr24;
            zArr = new boolean[]{true, false, true, false, false, false, false, false, false, false, false, false, false, false};
            bArr25 = new byte[28];
            bArr25 = new byte[]{(byte) 29, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 51, (byte) 102, (byte) 102, (byte) 0, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 30, (byte) 102};
            bArr26 = new byte[28];
            bArr93 = bArr26;
            zArr = new boolean[]{false, true, true, true, true, true, true, true, false, true, false, true, true, false, true, true, true, true, true, true, true, true, true, true, false, true, false, true};
            bArr27 = new byte[28];
            bArr27 = new byte[]{(byte) 29, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 57, (byte) 102, (byte) 102, (byte) 0, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 30, (byte) 102};
            bArr28 = new byte[28];
            bArr93 = bArr28;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            counter8 = 0;
            bArr29 = new byte[38];
            bArr29 = new byte[]{(byte) 29, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 51, (byte) 102, (byte) 102, (byte) 0, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 2, (byte) 102, (byte) 102, (byte) 102, (byte) 2, (byte) 102, (byte) 102, (byte) 102};
            bArr30 = new byte[38];
            bArr30 = new byte[]{(byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1};
            bArr31 = new byte[38];
            bArr31 = new byte[]{(byte) 29, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 57, (byte) 102, (byte) 102, (byte) 0, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 2, (byte) 102, (byte) 102, (byte) 102, (byte) 2, (byte) 102, (byte) 102, (byte) 102};
            bArr32 = new byte[38];
            bArr32 = new byte[]{(byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 3, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0};
            counter9 = 0;
            bArr33 = new byte[29];
            bArr33 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 26};
            bArr34 = new byte[29];
            bArr93 = bArr34;
            zArr = new boolean[]{false, true, true, true, false, true, true, true, false, true, true, true, true, true, true, true, true, true, false, true, false, true, true, true, false, true, true, true, false};
            bArr35 = new byte[29];
            bArr35 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 102, (byte) 57, (byte) 102, (byte) 102, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 26};
            bArr36 = new byte[29];
            bArr93 = bArr36;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, false, false, false, false, false, false, false, false};
            bArr37 = new byte[29];
            bArr37 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 0, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 56};
            bArr38 = new byte[29];
            bArr93 = bArr38;
            zArr = new boolean[]{false, true, true, false, false, true, true, true, true, true, true, true, true, true, false, true, true, true, true, true, true, true, false, true, false, true, true, true, false};
            bArr39 = new byte[29];
            bArr39 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 0, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 102, (byte) 57, (byte) 102, (byte) 102, (byte) 102, (byte) 56};
            bArr40 = new byte[29];
            bArr93 = bArr40;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, false, false, false, false};
            bArr41 = new byte[25];
            bArr41 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 26};
            bArr42 = new byte[25];
            bArr93 = bArr42;
            zArr = new boolean[]{false, true, true, true, false, true, true, true, true, true, true, true, true, true, false, true, false, true, true, true, false, true, true, true, false};
            bArr43 = new byte[25];
            bArr43 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 8, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 102, (byte) 57, (byte) 102, (byte) 102, (byte) 102, (byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 26};
            bArr44 = new byte[25];
            bArr93 = bArr44;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, false, false, false, false, false, false, false, false};
            original = "D6 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00";
            replace = "D5 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??";
            byteOrigS13 = new byte[original.split("[ \t]+").length];
            maskS13 = new byte[original.split("[ \t]+").length];
            byteReplaceS13 = new byte[replace.split("[ \t]+").length];
            rep_maskS13 = new byte[replace.split("[ \t]+").length];
            Utils.convertStringToArraysPatch(original, replace, byteOrigS13, maskS13, byteReplaceS13, rep_maskS13);
            bArr45 = new byte[35];
            bArr45 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr46 = new byte[35];
            bArr46 = new byte[]{(byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 1, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 0};
            bArr47 = new byte[35];
            bArr47 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr48 = new byte[35];
            bArr93 = bArr48;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            bArr49 = new byte[36];
            bArr49 = new byte[]{(byte) 82, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 110, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr50 = new byte[36];
            bArr50 = new byte[]{(byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 0};
            bArr51 = new byte[36];
            bArr51 = new byte[]{(byte) 82, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 110, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr52 = new byte[36];
            bArr93 = bArr52;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            bArr53 = new byte[30];
            bArr53 = new byte[]{(byte) 18, (byte) 19, (byte) 33, (byte) 65, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 102, (byte) 102, (byte) 15, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 4, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            bArr54 = new byte[30];
            bArr93 = bArr54;
            zArr = new boolean[]{false, false, false, false, false, true, false, true, true, false, true, true, false, false, false, false, false, true, false, true, true, false, false, true, false, false, false, true, false, false};
            bArr55 = new byte[30];
            bArr55 = new byte[]{(byte) 18, (byte) 3, (byte) 33, (byte) 65, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 102, (byte) 102, (byte) 15, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 4, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            bArr56 = new byte[30];
            bArr93 = bArr56;
            zArr = new boolean[]{true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            bArr57 = new byte[28];
            bArr57 = new byte[]{(byte) 18, (byte) 17, (byte) 33, (byte) 66, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 15, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 4, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            bArr58 = new byte[28];
            bArr93 = bArr58;
            zArr = new boolean[]{false, false, false, false, false, true, false, true, true, false, false, false, false, false, false, true, false, true, true, false, false, true, false, false, false, true, false, false};
            bArr59 = new byte[28];
            bArr59 = new byte[]{(byte) 18, (byte) 1, (byte) 33, (byte) 66, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 15, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 4, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            bArr60 = new byte[28];
            bArr93 = bArr60;
            zArr = new boolean[]{true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            bArr61 = new byte[43];
            bArr61 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 14, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr62 = new byte[43];
            bArr93 = bArr62;
            zArr = new boolean[]{false, true, true, false, false, false, false, false, false, false, false, true, true, false, false, true, true, true, true, true, true, true, true, true, true, false, false, true, true, true, true, false, false, true, false, true, true, true, true, false, false, false, false};
            bArr63 = new byte[43];
            bArr63 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 14, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr64 = new byte[43];
            bArr93 = bArr64;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            bArr65 = new byte[44];
            bArr65 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr66 = new byte[44];
            bArr93 = bArr66;
            zArr = new boolean[]{false, true, true, true, false, true, true, true, false, true, true, true, false, true, true, true, false, true, false, true, true, true, false, true, true, true, false, true, true, true, true, true, false, true, true, true, true, true, true, true, false, false, false, false};
            bArr67 = new byte[44];
            bArr67 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr68 = new byte[44];
            bArr93 = bArr68;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            bArr69 = new byte[70];
            bArr69 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr70 = new byte[70];
            bArr93 = bArr70;
            zArr = new boolean[]{false, true, true, true, false, true, true, true, false, true, true, true, false, true, true, true, false, true, false, true, true, true, false, true, true, true, false, true, true, true, true, true, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false};
            bArr71 = new byte[70];
            bArr71 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr72 = new byte[70];
            bArr93 = bArr72;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            bArr73 = new byte[56];
            bArr73 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 26, (byte) 102, (byte) 102, (byte) 102, (byte) 113, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr74 = new byte[56];
            bArr93 = bArr74;
            zArr = new boolean[]{false, true, true, true, false, true, true, true, false, true, true, true, false, true, true, true, false, true, false, true, true, true, false, true, true, true, false, true, true, true, false, true, true, true, true, true, false, true, false, true, true, true, true, true, false, true, true, true, true, true, true, true, false, false, false, false};
            bArr75 = new byte[56];
            bArr75 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 26, (byte) 102, (byte) 102, (byte) 102, (byte) 113, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 12, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr76 = new byte[56];
            bArr93 = bArr76;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, true, true};
            bArr77 = new byte[41];
            bArr77 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr78 = new byte[41];
            bArr93 = bArr78;
            zArr = new boolean[]{false, true, true, false, false, false, false, false, false, false, false, true, true, false, false, true, true, false, false, true, true, true, true, false, false, true, true, true, true, true, true, true, false, true, true, true, true, false, false, false, false};
            bArr79 = new byte[41];
            bArr79 = new byte[]{(byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr80 = new byte[41];
            bArr93 = bArr80;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            bArr81 = new byte[50];
            bArr81 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr82 = new byte[50];
            bArr93 = bArr82;
            zArr = new boolean[]{false, true, true, true, false, true, true, true, false, true, true, true, false, true, true, true, false, true, false, true, true, true, false, true, true, true, false, true, true, true, true, true, false, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false};
            bArr83 = new byte[50];
            bArr83 = new byte[]{(byte) 56, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) 58, (byte) 102, (byte) 102, (byte) 102, (byte) -112, (byte) 102, (byte) 102, (byte) 102, (byte) 33, (byte) 102, (byte) 55, (byte) 102, (byte) 102, (byte) 102, (byte) 34, (byte) 102, (byte) 102, (byte) 102, (byte) 112, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr84 = new byte[50];
            bArr93 = bArr84;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            bArr85 = new byte[26];
            bArr85 = new byte[]{(byte) 18, (byte) 18, (byte) 33, (byte) 83, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 15, (byte) 2, (byte) 18, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            bArr86 = new byte[26];
            bArr93 = bArr86;
            zArr = new boolean[]{false, false, false, false, false, true, false, true, true, false, false, false, false, false, false, false, false, true, false, true, true, false, false, true, false, false};
            bArr87 = new byte[26];
            bArr87 = new byte[]{(byte) 18, (byte) 2, (byte) 33, (byte) 83, (byte) 33, (byte) 102, (byte) 50, (byte) 102, (byte) 102, (byte) 0, (byte) 15, (byte) 2, (byte) 18, (byte) 1, (byte) 18, (byte) 0, (byte) 33, (byte) 102, (byte) 53, (byte) 102, (byte) 102, (byte) 0, (byte) 72, (byte) 102, (byte) 5, (byte) 0};
            bArr88 = new byte[26];
            bArr93 = bArr88;
            zArr = new boolean[]{true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            bArr89 = new byte[36];
            bArr89 = new byte[]{(byte) -14, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) -8, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 18, (byte) 16, (byte) 15, (byte) 0};
            bArr90 = new byte[36];
            bArr93 = bArr90;
            zArr = new boolean[]{false, false, true, true, false, false, false, false, false, false, false, false, true, true, false, false, true, true, false, false, true, true, true, true, false, false, false, false, true, true, true, true, false, false, false, false};
            bArr91 = new byte[36];
            bArr91 = new byte[]{(byte) -14, (byte) 32, (byte) 102, (byte) 102, (byte) 18, (byte) 49, (byte) 50, (byte) 16, (byte) 10, (byte) 0, (byte) 34, (byte) 0, (byte) 102, (byte) 102, (byte) 26, (byte) 1, (byte) 102, (byte) 102, (byte) 112, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 39, (byte) 0, (byte) -8, (byte) 32, (byte) 102, (byte) 102, (byte) 102, (byte) 102, (byte) 10, (byte) 0, (byte) 15, (byte) 0};
            bArr92 = new byte[36];
            bArr93 = bArr92;
            zArr = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false};
            pattern42 = pattern4;
            pattern32 = pattern3;
            pattern22 = pattern2;
        }
        if (paramArrayOfString[0].contains("restore") || paramArrayOfString[0].contains("patch") || paramArrayOfString[0].contains("all")) {
            boolean patchOat1;
            boolean patchOat3;
            int curentPos;
            byte curentByte;
            File file2;
            boolean good_odex;
            Iterator it;
            int counter;
            int counter2;
            ArrayList<File> filesToPatch = new ArrayList();
            filesToPatch.clear();
            if (pattern1 || pattern22) {
                boolean patchOatUpd1;
                Iterator it2;
                ArrayList<File> oatForPatch = new ArrayList();
                if (new File("/system/framework/x86/boot.oat").exists()) {
                    oatForPatch.add(new File("/system/framework/x86/boot.oat"));
                }
                if (new File("/system/framework/arm64/boot.oat").exists()) {
                    oatForPatch.add(new File("/system/framework/arm64/boot.oat"));
                }
                if (new File("/system/framework/arm/boot.oat").exists()) {
                    oatForPatch.add(new File("/system/framework/arm/boot.oat"));
                }
                if (oatForPatch.size() > 0 && listAppsFragment.api >= 21) {
                    patchOatUpd1 = false;
                    patchOat1 = false;
                    patchOat2 = false;
                    patchOat3 = false;
                    patchOat4 = false;
                    it2 = oatForPatch.iterator();
                    while (it2.hasNext()) {
                        File fileForPatch = (File) it2.next();
                        System.out.println("oat file for patch:" + fileForPatch.getAbsolutePath());
                        boolean uni = false;
                        boolean p11 = false;
                        boolean p12 = false;
                        boolean p2 = false;
                        try {
                            ChannelDex = new RandomAccessFile(fileForPatch, InternalZipConstants.WRITE_MODE).getChannel();
                            fileBytes = ChannelDex.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex.size()));
                            fileBytes.position(4120);
                            posit = Utils.convertFourBytesToInt(fileBytes.get(), fileBytes.get(), fileBytes.get(), fileBytes.get());
                            System.out.println("Start position:" + posit);
                            fileBytes.position(posit);
                            while (fileBytes.hasRemaining()) {
                                try {
                                    curentPos = fileBytes.position();
                                    curentByte = fileBytes.get();
                                    if (applyPatch(curentPos, curentByte, byteOrigOatUpd1, maskOatUpd1, byteReplaceOatUpd1, rep_maskOatUpd1, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("position:" + curentPos);
                                            System.out.println("Oat Core1uni patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core1uni restored!\n");
                                        }
                                        patchOatUpd1 = true;
                                        uni = true;
                                        p11 = true;
                                        p12 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOat1, maskOat1, byteReplaceOat1, rep_maskOat1, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("position:" + curentPos);
                                            System.out.println("Oat Core11 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core11 restored!\n");
                                        }
                                        patchOat1 = true;
                                        p11 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOatUpd4, maskOatUpd4, byteReplaceOatUpd4, rep_maskOatUpd4, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("position:" + curentPos);
                                            System.out.println("Oat Core1uni patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core1uni restored!\n");
                                        }
                                        patchOat1 = true;
                                        p11 = true;
                                        p12 = true;
                                        uni = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOatUpd5, maskOatUpd5, byteReplaceOatUpd5, rep_maskOatUpd5, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("position:" + curentPos);
                                            System.out.println("Oat Core1uni patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core1uni restored!\n");
                                        }
                                        patchOat1 = true;
                                        uni = true;
                                        p11 = true;
                                        p12 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOat4, maskOat4, byteReplaceOat4, rep_maskOat4, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("position:" + curentPos);
                                            System.out.println("Oat Core11 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core11 restored!\n");
                                        }
                                        patchOat1 = true;
                                        p11 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOat2, maskOat2, byteReplaceOat2, rep_maskOat2, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Oat Core12 patched!\n");
                                            System.out.println("position:" + curentPos);
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core12 restored!\n");
                                        }
                                        patchOat2 = true;
                                        p12 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOat5, maskOat5, byteReplaceOat5, rep_maskOat5, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Oat Core12 patched!\n");
                                            System.out.println("position:" + curentPos);
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core12 restored!\n");
                                        }
                                        patchOat2 = true;
                                        p12 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOat3, maskOat3, byteReplaceOat3, rep_maskOat3, pattern22)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Oat Core2 patched!\n");
                                            System.out.println("position:" + curentPos);
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core2 restored!\n");
                                        }
                                        patchOat3 = true;
                                        p2 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOat6, maskOat6, byteReplaceOat6, rep_maskOat6, pattern22)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Oat Core2 patched!\n");
                                            System.out.println("position:" + curentPos);
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core2 restored!\n");
                                        }
                                        patchOat3 = true;
                                        p2 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOat7, maskOat7, byteReplaceOat7, rep_maskOat7, pattern22)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Oat Core2 patched!\n");
                                            System.out.println("position:" + curentPos);
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core2 restored!\n");
                                        }
                                        patchOat3 = true;
                                        p2 = true;
                                    }
                                    if ((!pattern1 || !pattern22 || !uni || !p2) && ((!pattern1 || !pattern22 || !uni || !p11 || !p12 || !p2) && ((!pattern1 || pattern22 || !uni) && ((!pattern1 || pattern22 || !uni || !p11 || !p12) && (pattern1 || !pattern22 || !p2))))) {
                                        fileBytes.position(curentPos + 1);
                                    }
                                } catch (Exception e22) {
                                    System.out.println(BuildConfig.VERSION_NAME + e22);
                                }
                            }
                            ChannelDex.close();
                        } catch (IOException e3) {
                            e3.printStackTrace();
                        }
                    }
                    if (patchOatUpd1 || patchOat1 || patchOat2 || patchOat3) {
                        new File("/data/dalvik-cache/arm/system@framework@boot.oat").delete();
                        new File("/data/dalvik-cache/arm/system@framework@boot.art").delete();
                        new File("/data/dalvik-cache/arm64/system@framework@boot.oat").delete();
                        new File("/data/dalvik-cache/arm64/system@framework@boot.art").delete();
                        new File("/data/dalvik-cache/x86/system@framework@boot.oat").delete();
                        new File("/data/dalvik-cache/x86/system@framework@boot.art").delete();
                        Utils.cmdParam(listAppsFragment.toolfilesdir + "/reboot");
                    }
                }
                try {
                    if (paramArrayOfString[4].contains("framework")) {
                        if ((paramArrayOfString[1].contains("core.jar") || paramArrayOfString[1].contains("core-libart.jar")) && Utils.classes_test(new File(paramArrayOfString[1]))) {
                            file = new File(paramArrayOfString[1]);
                            unzip(serjar, Utils.getDirs(serjar).getAbsolutePath());
                            file = new File(Utils.getDirs(serjar).getAbsolutePath() + "/classes.dex");
                            if (file.exists()) {
                                filesToPatch.add(file);
                            }
                        }
                        if (paramArrayOfString[1].contains("core.odex") || paramArrayOfString[1].contains("boot.oat")) {
                            filesToPatch.add(new File(paramArrayOfString[1]));
                        }
                    } else if (paramArrayOfString[4].contains("ART")) {
                        if (new File("/system/framework/core-libart.odex").exists() && new File("/system/framework/core-libart.odex").length() != 0) {
                            filesToPatch.add(new File("/system/framework/core-libart.odex"));
                        }
                        if (Utils.classes_test(new File("/system/framework/core-libart.jar"))) {
                            file = new File("/system/framework/core-libart.jar");
                            unzip(serjar, "/data/app");
                            file = new File("/data/app/classes.dex");
                            if (file.exists()) {
                                filesToPatch.add(file);
                            }
                        }
                    } else if (onlyDalvik) {
                        System.out.println("OnlyDalvik: add for patch " + paramArrayOfString[1]);
                        file = new File(paramArrayOfString[1]);
                        try {
                            if (file.exists()) {
                                filesToPatch.add(file);
                                file2 = file;
                            } else {
                                throw new FileNotFoundException();
                            }
                        } catch (FileNotFoundException e4) {
                            file2 = localFile2;
                            System.out.println("Error: core.odex not found!\n\nPlease Odex core.jar and try again!");
                            System.out.println("Start patch for services.jar");
                            indexDir = BuildConfig.VERSION_NAME;
                            if (new File("/system/framework/arm/services.odex").exists()) {
                                indexDir = "/arm";
                            }
                            if (new File("/system/framework/arm64/services.odex").exists()) {
                                indexDir = "/arm64";
                            }
                            if (new File("/system/framework/x86/services.odex").exists()) {
                                indexDir = "/x86";
                            }
                            if (new File("/system/framework/arm/services.odex.xz").exists()) {
                                indexDir = "/arm";
                            }
                            if (new File("/system/framework/arm64/services.odex.xz").exists()) {
                                indexDir = "/arm64";
                            }
                            if (new File("/system/framework/x86/services.odex.xz").exists()) {
                                indexDir = "/x86";
                            }
                            patchOat1 = false;
                            patchOat2 = false;
                            patchOat3 = false;
                            patchOat4 = false;
                            patchOat5 = false;
                            patchOat6 = false;
                            file = new File("/system/framework" + indexDir + "/services.odex");
                            good_odex = true;
                            if (new File("/system/framework" + indexDir + "/services.odex.xz").exists()) {
                                System.out.println("try unpack services.odex.xz");
                                if (Utils.XZDecompress(new File("/system/framework" + indexDir + "/services.odex.xz"), "/system/framework" + indexDir)) {
                                    good_odex = false;
                                    new File("/system/framework" + indexDir + "/services.odex").delete();
                                    System.out.println("not enought space for unpack services.odex.xz");
                                } else {
                                    Utils.run_all_no_root("chmod", "644", "/system/framework" + indexDir + "/services.odex");
                                    Utils.run_all_no_root("chown", "0:0", "/system/framework" + indexDir + "/services.odex");
                                    Utils.run_all_no_root("chown", "0.0", "/system/framework" + indexDir + "/services.odex");
                                }
                            }
                            if (good_odex) {
                                System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                            } else {
                                try {
                                    ChannelDex = new RandomAccessFile(file, InternalZipConstants.WRITE_MODE).getChannel();
                                    fileBytes = ChannelDex.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex.size()));
                                    fileBytes.position(4120);
                                    posit = Utils.convertFourBytesToInt(fileBytes.get(), fileBytes.get(), fileBytes.get(), fileBytes.get());
                                    System.out.println("Start position:" + posit);
                                    fileBytes.position(posit);
                                    while (fileBytes.hasRemaining()) {
                                        try {
                                            curentPos = fileBytes.position();
                                            curentByte = fileBytes.get();
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat1, maskSOat1, byteReplaceSOat1, rep_maskSOat1, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\n");
                                                }
                                                patchOat1 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat2, maskSOat2, byteReplaceSOat2, rep_maskSOat2, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                                }
                                                patchOat2 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat3, maskSOat3, byteReplaceSOat3, rep_maskSOat3, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                                }
                                                patchOat3 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat4, maskSOat4, byteReplaceSOat4, rep_maskSOat4, pattern42)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!queryIntentServices\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!queryIntentServices\n");
                                                }
                                                patchOat4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat5, maskSOat5, byteReplaceSOat5, rep_maskSOat5, pattern42)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!buildResolveList\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!buildResolveList\n");
                                                }
                                                patchOat5 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat6, maskSOat6, byteReplaceSOat6, rep_maskSOat6, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!FixForCM\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!FixForCM\n");
                                                }
                                                patchOat6 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat7, maskSOat7, byteReplaceSOat7, rep_maskSOat7, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!FixForCM\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!FixForCM\n");
                                                }
                                                patchOat6 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat8, maskSOat8, byteReplaceSOat8, rep_maskSOat8, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\n");
                                                }
                                                patchOat1 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat9, maskSOat9, byteReplaceSOat9, rep_maskSOat9, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                                }
                                                patchOat2 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat12, maskSOat12, byteReplaceSOat12, rep_maskSOat12, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                                }
                                                patchOat2 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat13, maskSOat13, byteReplaceSOat13, rep_maskSOat13, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                                }
                                                patchOat2 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat10, maskSOat10, byteReplaceSOat10, rep_maskSOat10, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\n");
                                                }
                                                patchOat1 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat11, maskSOat11, byteReplaceSOat11, rep_maskSOat11, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\n");
                                                }
                                                patchOat1 = true;
                                            }
                                            if (!pattern32) {
                                            }
                                            fileBytes.position(curentPos + 1);
                                        } catch (Exception e222) {
                                            System.out.println(BuildConfig.VERSION_NAME + e222);
                                        }
                                    }
                                    ChannelDex.close();
                                } catch (IOException e32) {
                                    e32.printStackTrace();
                                }
                            }
                            new File("/data/dalvik-cache/arm/system@framework@services.jar@classes.dex").delete();
                            new File("/data/dalvik-cache/arm/system@framework@services.jar@classes.art").delete();
                            new File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.dex").delete();
                            new File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.art").delete();
                            new File("/data/dalvik-cache/x86/system@framework@services.jar@classes.dex").delete();
                            new File("/data/dalvik-cache/x86/system@framework@services.jar@classes.art").delete();
                            Utils.cmdParam(listAppsFragment.toolfilesdir + "/reboot");
                            try {
                                if (!paramArrayOfString[4].contains("framework")) {
                                    file = new File(paramArrayOfString[1]);
                                    unzip(serjar, Utils.getDirs(serjar).getAbsolutePath());
                                    file = new File(Utils.getDirs(serjar).getAbsolutePath() + "/classes.dex");
                                    if (file.exists()) {
                                        filesToPatch.add(file);
                                    }
                                    if (paramArrayOfString[1].contains("services.odex")) {
                                        filesToPatch.add(new File(paramArrayOfString[1]));
                                    }
                                } else if (!paramArrayOfString[4].contains("ART")) {
                                    System.out.println("Add services.odex for patch");
                                    filesToPatch.add(new File("/system/framework/services.odex"));
                                    if (Utils.classes_test(new File("/system/framework/services.jar"))) {
                                        System.out.println("services.jar contain classes,dex");
                                        file = new File("/system/framework/services.jar");
                                        unzip(serjar, "/data/app");
                                        file = new File("/data/app/classes.dex");
                                        if (file.exists()) {
                                            System.out.println("Add classes.dex for patch");
                                            filesToPatch.add(file);
                                        }
                                    }
                                } else if (onlyDalvik) {
                                    System.out.println("Vhodjashij file " + paramArrayOfString[2]);
                                    file = new File(paramArrayOfString[2]);
                                    if (file.exists()) {
                                        throw new FileNotFoundException();
                                    }
                                    if (file.toString().contains("system@framework@services.jar@classes.dex")) {
                                        file2 = file;
                                    } else {
                                        System.out.println("Vhodjashij file byl dalvick-cache " + paramArrayOfString[2]);
                                        file = new File("/system/framework/services.jar");
                                        unzip(serjar, "/data/app");
                                        file = new File("/data/app/classes.dex");
                                        if (file.exists()) {
                                            file2 = file;
                                        } else {
                                            file2 = file;
                                        }
                                        try {
                                            file = new File("/system/framework/services.odex");
                                            file.delete();
                                        } catch (Exception e2222) {
                                            e2222.printStackTrace();
                                            System.out.println(BuildConfig.VERSION_NAME + e2222);
                                        } catch (FileNotFoundException e5) {
                                        }
                                    }
                                    System.out.println("Add file for patch " + file2.toString());
                                    filesToPatch.add(file2);
                                } else {
                                    file = new File(paramArrayOfString[2]);
                                    try {
                                        if (file.exists()) {
                                            throw new FileNotFoundException();
                                        }
                                        filesToPatch.add(file);
                                        file2 = file;
                                    } catch (FileNotFoundException e6) {
                                        file2 = localFile2;
                                    } catch (Exception e7) {
                                        e2222 = e7;
                                        file2 = localFile2;
                                    }
                                }
                                it = filesToPatch.iterator();
                                while (it.hasNext()) {
                                    fp = (File) it.next();
                                    System.out.println("Start patch for " + fp.toString());
                                    file2 = fp;
                                    isOat = false;
                                    if (Utils.isELFfiles(file2)) {
                                        isOat = true;
                                    }
                                    ChannelDex = new RandomAccessFile(file2, InternalZipConstants.WRITE_MODE).getChannel();
                                    fileBytes = ChannelDex.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex.size()));
                                    patch4 = false;
                                    counter = 0;
                                    counter2 = 0;
                                    if (!isOat) {
                                        lastByteReplace = null;
                                        lastPatchPosition = 0;
                                        j = 0;
                                        while (fileBytes.hasRemaining()) {
                                            curentPos = fileBytes.position();
                                            curentByte = fileBytes.get();
                                            if (applyPatch(curentPos, curentByte, bArr, bArr2, bArr3, bArr4, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\n");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr13, bArr14, bArr15, bArr16, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\n");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr17, bArr18, bArr19, bArr20, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr21, bArr22, bArr23, bArr24, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\nCM12");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\nCM12");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatchCounter(curentPos, curentByte, bArr25, bArr26, bArr27, bArr28, counter, counter8, pattern42)) {
                                                counter++;
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\nRemove Package from intent");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\nRemove Package from intent");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatchCounter(curentPos, curentByte, bArr29, bArr30, bArr31, bArr32, counter2, counter9, pattern42)) {
                                                counter2++;
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\nRemove Package from intent");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\nRemove Package from intent");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr33, bArr34, bArr35, bArr36, pattern42)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\nbuildResolveList");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\nbuildResolveList");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr37, bArr38, bArr39, bArr40, pattern42)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\nbuildResolveList");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\nbuildResolveList");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr41, bArr42, bArr43, bArr44, pattern42)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\nbuildResolveList");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\nbuildResolveList");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr5, bArr6, bArr7, bArr8, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\nCM11");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\nCM11");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr9, bArr10, bArr11, bArr12, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 policy patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 policy restored!\n");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigS13, maskS13, byteReplaceS13, rep_maskS13, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 policy patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 policy restored!\n");
                                                }
                                                patch4 = true;
                                            }
                                            fileBytes.position(curentPos + 1);
                                            j++;
                                        }
                                        fileBytes.position(lastPatchPosition);
                                        fileBytes.put(lastByteReplace);
                                        fileBytes.force();
                                    } else {
                                        patchOat1 = false;
                                        patchOat2 = false;
                                        patchOat3 = false;
                                        patchOat4 = false;
                                        patchOat5 = false;
                                        patchOat6 = false;
                                        fileBytes.position(4120);
                                        posit = Utils.convertFourBytesToInt(fileBytes.get(), fileBytes.get(), fileBytes.get(), fileBytes.get());
                                        System.out.println("Start position:" + posit);
                                        fileBytes.position(posit);
                                        while (fileBytes.hasRemaining()) {
                                            curentPos = fileBytes.position();
                                            curentByte = fileBytes.get();
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat1, maskSOat1, byteReplaceSOat1, rep_maskSOat1, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\n");
                                                }
                                                patchOat1 = true;
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat2, maskSOat2, byteReplaceSOat2, rep_maskSOat2, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                                }
                                                patchOat2 = true;
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat3, maskSOat3, byteReplaceSOat3, rep_maskSOat3, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                                }
                                                patchOat3 = true;
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat4, maskSOat4, byteReplaceSOat4, rep_maskSOat4, pattern42)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!queryIntentServices\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!queryIntentServices\n");
                                                }
                                                patchOat4 = true;
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat5, maskSOat5, byteReplaceSOat5, rep_maskSOat5, pattern42)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!buildResolveList\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!buildResolveList\n");
                                                }
                                                patchOat5 = true;
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat6, maskSOat6, byteReplaceSOat6, rep_maskSOat6, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!FixForCM\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!FixForCM\n");
                                                }
                                                patchOat6 = true;
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat7, maskSOat7, byteReplaceSOat7, rep_maskSOat7, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!FixForCM\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!FixForCM\n");
                                                }
                                                patchOat6 = true;
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat8, maskSOat8, byteReplaceSOat8, rep_maskSOat8, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\n");
                                                }
                                                patchOat1 = true;
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat10, maskSOat10, byteReplaceSOat10, rep_maskSOat10, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\n");
                                                }
                                                patchOat1 = true;
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat11, maskSOat11, byteReplaceSOat11, rep_maskSOat11, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\n");
                                                }
                                                patchOat1 = true;
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat9, maskSOat9, byteReplaceSOat9, rep_maskSOat9, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                                }
                                                patchOat2 = true;
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat12, maskSOat12, byteReplaceSOat12, rep_maskSOat12, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                                }
                                                patchOat2 = true;
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigSOat13, maskSOat13, byteReplaceSOat13, rep_maskSOat13, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                                }
                                                patchOat2 = true;
                                                patch4 = true;
                                            }
                                            if (!pattern32) {
                                            }
                                            fileBytes.position(curentPos + 1);
                                        }
                                    }
                                    ChannelDex.close();
                                    if (paramArrayOfString[4].contains("framework")) {
                                        if (file2.toString().endsWith("/classes.dex")) {
                                            Utils.fixadlerOdex(file2, "/system/framework/services.jar");
                                        } else {
                                            Utils.fixadler(file2);
                                        }
                                        System.out.println("LuckyPatcher: dalvik-cache patched! ");
                                        System.out.println("start");
                                        destination = "/system/framework/services.backup";
                                        if (Utils.copyFile("/system/framework/services.jar", destination, true, false)) {
                                            new File(destination).delete();
                                            file = new File(paramArrayOfString[2]);
                                            try {
                                                ChannelDex = new RandomAccessFile(file, InternalZipConstants.WRITE_MODE).getChannel();
                                                fileBytes = ChannelDex.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex.size()));
                                                counter = 0;
                                                counter2 = 0;
                                                lastByteReplace = null;
                                                lastPatchPosition = 0;
                                                patch4 = false;
                                                j = 0;
                                                while (fileBytes.hasRemaining()) {
                                                    curentPos = fileBytes.position();
                                                    curentByte = fileBytes.get();
                                                    if (applyPatch(curentPos, curentByte, bArr, bArr2, bArr3, bArr4, pattern32)) {
                                                        if (paramArrayOfString[0].contains("patch")) {
                                                            System.out.println("Core4 patched!\n");
                                                        }
                                                        if (paramArrayOfString[0].contains("restore")) {
                                                            System.out.println("Core4 restored!\n");
                                                        }
                                                        patch4 = true;
                                                    }
                                                    if (applyPatch(curentPos, curentByte, bArr13, bArr14, bArr15, bArr16, pattern32)) {
                                                        if (paramArrayOfString[0].contains("patch")) {
                                                            System.out.println("Core4 patched!\n");
                                                        }
                                                        if (paramArrayOfString[0].contains("restore")) {
                                                            System.out.println("Core4 restored!\n");
                                                        }
                                                        patch4 = true;
                                                    }
                                                    if (applyPatch(curentPos, curentByte, bArr17, bArr18, bArr19, bArr20, pattern32)) {
                                                        if (paramArrayOfString[0].contains("patch")) {
                                                            System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                                        }
                                                        if (paramArrayOfString[0].contains("restore")) {
                                                            System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                                        }
                                                        patch4 = true;
                                                    }
                                                    if (applyPatch(curentPos, curentByte, bArr21, bArr22, bArr23, bArr24, pattern32)) {
                                                        if (paramArrayOfString[0].contains("patch")) {
                                                            System.out.println("Core4 patched!\nCM12");
                                                        }
                                                        if (paramArrayOfString[0].contains("restore")) {
                                                            System.out.println("Core4 restored!\nCM12");
                                                        }
                                                        patch4 = true;
                                                    }
                                                    if (applyPatchCounter(curentPos, curentByte, bArr25, bArr26, bArr27, bArr28, counter, counter8, pattern42)) {
                                                        counter++;
                                                        if (paramArrayOfString[0].contains("patch")) {
                                                            System.out.println("Core4 patched!\nRemove Package from intent");
                                                        }
                                                        if (paramArrayOfString[0].contains("restore")) {
                                                            System.out.println("Core4 restored!\nRemove Package from intent");
                                                        }
                                                        patch4 = true;
                                                    }
                                                    if (applyPatchCounter(curentPos, curentByte, bArr29, bArr30, bArr31, bArr32, counter2, counter9, pattern42)) {
                                                        counter2++;
                                                        if (paramArrayOfString[0].contains("patch")) {
                                                            System.out.println("Core4 patched!\nRemove Package from intent");
                                                        }
                                                        if (paramArrayOfString[0].contains("restore")) {
                                                            System.out.println("Core4 restored!\nRemove Package from intent");
                                                        }
                                                        patch4 = true;
                                                    }
                                                    if (applyPatch(curentPos, curentByte, bArr33, bArr34, bArr35, bArr36, pattern42)) {
                                                        if (paramArrayOfString[0].contains("patch")) {
                                                            System.out.println("Core4 patched!\nbuildResolveList");
                                                        }
                                                        if (paramArrayOfString[0].contains("restore")) {
                                                            System.out.println("Core4 restored!\nbuildResolveList");
                                                        }
                                                        patch4 = true;
                                                    }
                                                    if (applyPatch(curentPos, curentByte, bArr37, bArr38, bArr39, bArr40, pattern42)) {
                                                        if (paramArrayOfString[0].contains("patch")) {
                                                            System.out.println("Core4 patched!\nbuildResolveList");
                                                        }
                                                        if (paramArrayOfString[0].contains("restore")) {
                                                            System.out.println("Core4 restored!\nbuildResolveList");
                                                        }
                                                        patch4 = true;
                                                    }
                                                    if (applyPatch(curentPos, curentByte, bArr41, bArr42, bArr43, bArr44, pattern42)) {
                                                        if (paramArrayOfString[0].contains("patch")) {
                                                            System.out.println("Core4 patched!\nbuildResolveList");
                                                        }
                                                        if (paramArrayOfString[0].contains("restore")) {
                                                            System.out.println("Core4 restored!\nbuildResolveList");
                                                        }
                                                        patch4 = true;
                                                    }
                                                    if (applyPatch(curentPos, curentByte, bArr5, bArr6, bArr7, bArr8, pattern32)) {
                                                        if (paramArrayOfString[0].contains("patch")) {
                                                            System.out.println("Core4 patched!\nCM11");
                                                        }
                                                        if (paramArrayOfString[0].contains("restore")) {
                                                            System.out.println("Core4 restored!\nCM11");
                                                        }
                                                        patch4 = true;
                                                    }
                                                    if (applyPatch(curentPos, curentByte, bArr9, bArr10, bArr11, bArr12, pattern32)) {
                                                        if (paramArrayOfString[0].contains("patch")) {
                                                            System.out.println("Core4 policy patched!\n");
                                                        }
                                                        if (paramArrayOfString[0].contains("restore")) {
                                                            System.out.println("Core4 policy restored!\n");
                                                        }
                                                        patch4 = true;
                                                    }
                                                    if (applyPatch(curentPos, curentByte, byteOrigS13, maskS13, byteReplaceS13, rep_maskS13, pattern32)) {
                                                        if (paramArrayOfString[0].contains("patch")) {
                                                            System.out.println("Core4 policy patched!\n");
                                                        }
                                                        if (paramArrayOfString[0].contains("restore")) {
                                                            System.out.println("Core4 policy restored!\n");
                                                        }
                                                        patch4 = true;
                                                    }
                                                    fileBytes.position(curentPos + 1);
                                                    j++;
                                                }
                                            } catch (Exception e22222) {
                                                System.out.println(BuildConfig.VERSION_NAME + e22222);
                                            } catch (FileNotFoundException e62) {
                                                file2 = localFile2;
                                            }
                                            fileBytes.position(lastPatchPosition);
                                            fileBytes.put(lastByteReplace);
                                            fileBytes.force();
                                            ChannelDex.close();
                                            if (file.toString().endsWith("/classes.dex")) {
                                                Utils.fixadlerOdex(file, "/system/framework/services.jar");
                                            } else {
                                                Utils.fixadler(file);
                                            }
                                            System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                            file2 = file;
                                        } else {
                                            System.out.println("good space");
                                            new File(destination).delete();
                                            files = new ArrayList();
                                            System.out.println("add files");
                                            files.add(new AddFilesItem("/data/app/classes.dex", "/data/app/"));
                                            Utils.addFilesToZip("/system/framework/services.jar", "/system/framework/services.backup", files);
                                            System.out.println("add files finish");
                                            Utils.run_all_no_root("chmod", "0644", "/system/framework/services.backup");
                                            Utils.run_all_no_root("chown", "0:0", "/system/framework/services.backup");
                                            Utils.run_all_no_root("chmod", "0.0", "/system/framework/services.backup");
                                            file = new File("/system/framework/services.odex");
                                            d = Utils.getFileDalvikCache("/system/framework/services.jar");
                                            if (file.exists()) {
                                                System.out.println("fix odex na osnove rebuild services");
                                                Utils.fixadlerOdex(file, "/system/framework/services.backup");
                                            }
                                            new File(listAppsFragment.toolfilesdir + "/ClearDalvik.on").createNewFile();
                                            new File("/system/framework/services.jar").delete();
                                            Utils.run_all_no_root("rm", "/system/framework/services.jar");
                                            new File("/system/framework/services.backup").renameTo(new File("/system/framework/services.jar"));
                                            if (!new File("/system/framework/services.jar").exists()) {
                                                Utils.run_all_no_root("mv", "/system/framework/services.backup", "/system/framework/services.jar");
                                            }
                                            if (d != null) {
                                                Utils.run_all_no_root("rm", d.getAbsolutePath());
                                                if (d.exists()) {
                                                    d.delete();
                                                }
                                            }
                                            new File("/data/dalvik-cache/arm/system@framework@arm@boot.oat").delete();
                                            new File("/data/dalvik-cache/arm/system@framework@arm@boot.art").delete();
                                            new File("/data/dalvik-cache/arm64/system@framework@arm@boot.oat").delete();
                                            new File("/data/dalvik-cache/arm64/system@framework@arm@boot.art").delete();
                                            new File("/data/dalvik-cache/x86/system@framework@arm@boot.oat").delete();
                                            new File("/data/dalvik-cache/x86/system@framework@arm@boot.art").delete();
                                            Utils.run_all_no_root("rm", "/data/dalvik-cache/*.dex");
                                            Utils.run_all_no_root("rm", "/data/dalvik-cache/*.oat");
                                            Utils.run_all_no_root("rm", "/data/dalvik-cache/*.art");
                                            Utils.run_all_no_root("rm", "/data/dalvik-cache/arm/*.dex");
                                            Utils.run_all_no_root("rm", "/data/dalvik-cache/arm/*.art");
                                            Utils.run_all_no_root("rm", "/data/dalvik-cache/arm/*.oat");
                                            Utils.run_all_no_root("rm", "/data/dalvik-cache/arm64/*.dex");
                                            Utils.run_all_no_root("rm", "/data/dalvik-cache/arm64/*.art");
                                            Utils.run_all_no_root("rm", "/data/dalvik-cache/arm64/*.oat");
                                            Utils.run_all_no_root("rm", "/data/dalvik-cache/x86/*.dex");
                                            Utils.run_all_no_root("rm", "/data/dalvik-cache/x86/*.art");
                                            Utils.run_all_no_root("rm", "/data/dalvik-cache/x86/*.oat");
                                            System.out.println("finish");
                                        }
                                        if (!onlyDalvik) {
                                            if (new File("/system/framework/services.patched").exists()) {
                                                System.out.println("LuckyPatcher: root found services.patched! ");
                                            }
                                            if (new File("/system/framework/services.odex").exists()) {
                                                System.out.println("LuckyPatcher: root found services.odex! ");
                                            }
                                            if (!paramArrayOfString[0].contains("restore")) {
                                                new File("/system/framework/patch3.done").delete();
                                            }
                                        }
                                    } else {
                                        System.out.println("Rebuild file!");
                                        if (file2.toString().endsWith("/classes.dex")) {
                                            Utils.fixadler(file2);
                                            if (patch4) {
                                                not_found_bytes_for_patch = true;
                                            } else {
                                                not_found_bytes_for_patch = false;
                                                destination = paramArrayOfString[1].replace("/services.jar", "/services-patched.jar");
                                                source = paramArrayOfString[1];
                                                new File(destination).delete();
                                                files = new ArrayList();
                                                System.out.println("add files");
                                                files.add(new AddFilesItem(file2.getAbsolutePath(), Utils.getDirs(file2).getAbsolutePath() + InternalZipConstants.ZIP_FILE_SEPARATOR));
                                                Utils.addFilesToZip(source, destination, files);
                                                System.out.println("add files finish");
                                                new File(source).delete();
                                            }
                                        }
                                        if (file2.toString().endsWith("/services.odex")) {
                                            if (!Utils.isELFfiles(file2)) {
                                                Utils.fixadlerOdex(file2, null);
                                            }
                                            if (!patch4) {
                                                file2.renameTo(new File(file2.getAbsolutePath().replace("/services.odex", "/services-patched.odex")));
                                            }
                                        }
                                    }
                                }
                            } catch (FileNotFoundException e52) {
                            } catch (Exception e8) {
                                e22222 = e8;
                            }
                            Utils.exitFromRootJava();
                        } catch (Exception e9) {
                            e22222 = e9;
                            file2 = localFile2;
                            System.out.println("Exception e" + e22222.toString());
                            System.out.println("Start patch for services.jar");
                            indexDir = BuildConfig.VERSION_NAME;
                            if (new File("/system/framework/arm/services.odex").exists()) {
                                indexDir = "/arm";
                            }
                            if (new File("/system/framework/arm64/services.odex").exists()) {
                                indexDir = "/arm64";
                            }
                            if (new File("/system/framework/x86/services.odex").exists()) {
                                indexDir = "/x86";
                            }
                            if (new File("/system/framework/arm/services.odex.xz").exists()) {
                                indexDir = "/arm";
                            }
                            if (new File("/system/framework/arm64/services.odex.xz").exists()) {
                                indexDir = "/arm64";
                            }
                            if (new File("/system/framework/x86/services.odex.xz").exists()) {
                                indexDir = "/x86";
                            }
                            patchOat1 = false;
                            patchOat2 = false;
                            patchOat3 = false;
                            patchOat4 = false;
                            patchOat5 = false;
                            patchOat6 = false;
                            file = new File("/system/framework" + indexDir + "/services.odex");
                            good_odex = true;
                            if (new File("/system/framework" + indexDir + "/services.odex.xz").exists()) {
                                System.out.println("try unpack services.odex.xz");
                                if (Utils.XZDecompress(new File("/system/framework" + indexDir + "/services.odex.xz"), "/system/framework" + indexDir)) {
                                    good_odex = false;
                                    new File("/system/framework" + indexDir + "/services.odex").delete();
                                    System.out.println("not enought space for unpack services.odex.xz");
                                } else {
                                    Utils.run_all_no_root("chmod", "644", "/system/framework" + indexDir + "/services.odex");
                                    Utils.run_all_no_root("chown", "0:0", "/system/framework" + indexDir + "/services.odex");
                                    Utils.run_all_no_root("chown", "0.0", "/system/framework" + indexDir + "/services.odex");
                                }
                            }
                            if (good_odex) {
                                ChannelDex = new RandomAccessFile(file, InternalZipConstants.WRITE_MODE).getChannel();
                                fileBytes = ChannelDex.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex.size()));
                                fileBytes.position(4120);
                                posit = Utils.convertFourBytesToInt(fileBytes.get(), fileBytes.get(), fileBytes.get(), fileBytes.get());
                                System.out.println("Start position:" + posit);
                                fileBytes.position(posit);
                                while (fileBytes.hasRemaining()) {
                                    curentPos = fileBytes.position();
                                    curentByte = fileBytes.get();
                                    if (applyPatch(curentPos, curentByte, byteOrigSOat1, maskSOat1, byteReplaceSOat1, rep_maskSOat1, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!\n");
                                        }
                                        patchOat1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigSOat2, maskSOat2, byteReplaceSOat2, rep_maskSOat2, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                        }
                                        patchOat2 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigSOat3, maskSOat3, byteReplaceSOat3, rep_maskSOat3, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                        }
                                        patchOat3 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigSOat4, maskSOat4, byteReplaceSOat4, rep_maskSOat4, pattern42)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!queryIntentServices\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!queryIntentServices\n");
                                        }
                                        patchOat4 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigSOat5, maskSOat5, byteReplaceSOat5, rep_maskSOat5, pattern42)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!buildResolveList\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!buildResolveList\n");
                                        }
                                        patchOat5 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigSOat6, maskSOat6, byteReplaceSOat6, rep_maskSOat6, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!FixForCM\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!FixForCM\n");
                                        }
                                        patchOat6 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigSOat7, maskSOat7, byteReplaceSOat7, rep_maskSOat7, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!FixForCM\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!FixForCM\n");
                                        }
                                        patchOat6 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigSOat8, maskSOat8, byteReplaceSOat8, rep_maskSOat8, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!\n");
                                        }
                                        patchOat1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigSOat9, maskSOat9, byteReplaceSOat9, rep_maskSOat9, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                        }
                                        patchOat2 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigSOat12, maskSOat12, byteReplaceSOat12, rep_maskSOat12, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                        }
                                        patchOat2 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigSOat13, maskSOat13, byteReplaceSOat13, rep_maskSOat13, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                        }
                                        patchOat2 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigSOat10, maskSOat10, byteReplaceSOat10, rep_maskSOat10, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!\n");
                                        }
                                        patchOat1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigSOat11, maskSOat11, byteReplaceSOat11, rep_maskSOat11, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!\n");
                                        }
                                        patchOat1 = true;
                                    }
                                    if (!pattern32) {
                                    }
                                    fileBytes.position(curentPos + 1);
                                }
                                ChannelDex.close();
                            } else {
                                System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                            }
                            new File("/data/dalvik-cache/arm/system@framework@services.jar@classes.dex").delete();
                            new File("/data/dalvik-cache/arm/system@framework@services.jar@classes.art").delete();
                            new File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.dex").delete();
                            new File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.art").delete();
                            new File("/data/dalvik-cache/x86/system@framework@services.jar@classes.dex").delete();
                            new File("/data/dalvik-cache/x86/system@framework@services.jar@classes.art").delete();
                            Utils.cmdParam(listAppsFragment.toolfilesdir + "/reboot");
                            if (!paramArrayOfString[4].contains("framework")) {
                                file = new File(paramArrayOfString[1]);
                                unzip(serjar, Utils.getDirs(serjar).getAbsolutePath());
                                file = new File(Utils.getDirs(serjar).getAbsolutePath() + "/classes.dex");
                                if (file.exists()) {
                                    filesToPatch.add(file);
                                }
                                if (paramArrayOfString[1].contains("services.odex")) {
                                    filesToPatch.add(new File(paramArrayOfString[1]));
                                }
                            } else if (!paramArrayOfString[4].contains("ART")) {
                                System.out.println("Add services.odex for patch");
                                filesToPatch.add(new File("/system/framework/services.odex"));
                                if (Utils.classes_test(new File("/system/framework/services.jar"))) {
                                    System.out.println("services.jar contain classes,dex");
                                    file = new File("/system/framework/services.jar");
                                    unzip(serjar, "/data/app");
                                    file = new File("/data/app/classes.dex");
                                    if (file.exists()) {
                                        System.out.println("Add classes.dex for patch");
                                        filesToPatch.add(file);
                                    }
                                }
                            } else if (onlyDalvik) {
                                file = new File(paramArrayOfString[2]);
                                if (file.exists()) {
                                    throw new FileNotFoundException();
                                }
                                filesToPatch.add(file);
                                file2 = file;
                            } else {
                                System.out.println("Vhodjashij file " + paramArrayOfString[2]);
                                file = new File(paramArrayOfString[2]);
                                if (file.exists()) {
                                    throw new FileNotFoundException();
                                }
                                if (file.toString().contains("system@framework@services.jar@classes.dex")) {
                                    System.out.println("Vhodjashij file byl dalvick-cache " + paramArrayOfString[2]);
                                    file = new File("/system/framework/services.jar");
                                    unzip(serjar, "/data/app");
                                    file = new File("/data/app/classes.dex");
                                    if (file.exists()) {
                                        file2 = file;
                                    } else {
                                        file2 = file;
                                    }
                                    file = new File("/system/framework/services.odex");
                                    file.delete();
                                } else {
                                    file2 = file;
                                }
                                System.out.println("Add file for patch " + file2.toString());
                                filesToPatch.add(file2);
                            }
                            it = filesToPatch.iterator();
                            while (it.hasNext()) {
                                fp = (File) it.next();
                                System.out.println("Start patch for " + fp.toString());
                                file2 = fp;
                                isOat = false;
                                if (Utils.isELFfiles(file2)) {
                                    isOat = true;
                                }
                                ChannelDex = new RandomAccessFile(file2, InternalZipConstants.WRITE_MODE).getChannel();
                                fileBytes = ChannelDex.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex.size()));
                                patch4 = false;
                                counter = 0;
                                counter2 = 0;
                                if (!isOat) {
                                    lastByteReplace = null;
                                    lastPatchPosition = 0;
                                    j = 0;
                                    while (fileBytes.hasRemaining()) {
                                        curentPos = fileBytes.position();
                                        curentByte = fileBytes.get();
                                        if (applyPatch(curentPos, curentByte, bArr, bArr2, bArr3, bArr4, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!\n");
                                            }
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, bArr13, bArr14, bArr15, bArr16, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!\n");
                                            }
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, bArr17, bArr18, bArr19, bArr20, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                            }
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, bArr21, bArr22, bArr23, bArr24, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!\nCM12");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!\nCM12");
                                            }
                                            patch4 = true;
                                        }
                                        if (applyPatchCounter(curentPos, curentByte, bArr25, bArr26, bArr27, bArr28, counter, counter8, pattern42)) {
                                            counter++;
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!\nRemove Package from intent");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!\nRemove Package from intent");
                                            }
                                            patch4 = true;
                                        }
                                        if (applyPatchCounter(curentPos, curentByte, bArr29, bArr30, bArr31, bArr32, counter2, counter9, pattern42)) {
                                            counter2++;
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!\nRemove Package from intent");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!\nRemove Package from intent");
                                            }
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, bArr33, bArr34, bArr35, bArr36, pattern42)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!\nbuildResolveList");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!\nbuildResolveList");
                                            }
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, bArr37, bArr38, bArr39, bArr40, pattern42)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!\nbuildResolveList");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!\nbuildResolveList");
                                            }
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, bArr41, bArr42, bArr43, bArr44, pattern42)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!\nbuildResolveList");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!\nbuildResolveList");
                                            }
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, bArr5, bArr6, bArr7, bArr8, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!\nCM11");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!\nCM11");
                                            }
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, bArr9, bArr10, bArr11, bArr12, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 policy patched!\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 policy restored!\n");
                                            }
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, byteOrigS13, maskS13, byteReplaceS13, rep_maskS13, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 policy patched!\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 policy restored!\n");
                                            }
                                            patch4 = true;
                                        }
                                        fileBytes.position(curentPos + 1);
                                        j++;
                                    }
                                    fileBytes.position(lastPatchPosition);
                                    fileBytes.put(lastByteReplace);
                                    fileBytes.force();
                                } else {
                                    patchOat1 = false;
                                    patchOat2 = false;
                                    patchOat3 = false;
                                    patchOat4 = false;
                                    patchOat5 = false;
                                    patchOat6 = false;
                                    fileBytes.position(4120);
                                    posit = Utils.convertFourBytesToInt(fileBytes.get(), fileBytes.get(), fileBytes.get(), fileBytes.get());
                                    System.out.println("Start position:" + posit);
                                    fileBytes.position(posit);
                                    while (fileBytes.hasRemaining()) {
                                        curentPos = fileBytes.position();
                                        curentByte = fileBytes.get();
                                        if (applyPatch(curentPos, curentByte, byteOrigSOat1, maskSOat1, byteReplaceSOat1, rep_maskSOat1, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!\n");
                                            }
                                            patchOat1 = true;
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, byteOrigSOat2, maskSOat2, byteReplaceSOat2, rep_maskSOat2, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!InstallLocationPolice\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!InstallLocationPolice\n");
                                            }
                                            patchOat2 = true;
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, byteOrigSOat3, maskSOat3, byteReplaceSOat3, rep_maskSOat3, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                            }
                                            patchOat3 = true;
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, byteOrigSOat4, maskSOat4, byteReplaceSOat4, rep_maskSOat4, pattern42)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!queryIntentServices\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!queryIntentServices\n");
                                            }
                                            patchOat4 = true;
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, byteOrigSOat5, maskSOat5, byteReplaceSOat5, rep_maskSOat5, pattern42)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!buildResolveList\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!buildResolveList\n");
                                            }
                                            patchOat5 = true;
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, byteOrigSOat6, maskSOat6, byteReplaceSOat6, rep_maskSOat6, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!FixForCM\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!FixForCM\n");
                                            }
                                            patchOat6 = true;
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, byteOrigSOat7, maskSOat7, byteReplaceSOat7, rep_maskSOat7, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!FixForCM\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!FixForCM\n");
                                            }
                                            patchOat6 = true;
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, byteOrigSOat8, maskSOat8, byteReplaceSOat8, rep_maskSOat8, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!\n");
                                            }
                                            patchOat1 = true;
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, byteOrigSOat10, maskSOat10, byteReplaceSOat10, rep_maskSOat10, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!\n");
                                            }
                                            patchOat1 = true;
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, byteOrigSOat11, maskSOat11, byteReplaceSOat11, rep_maskSOat11, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!\n");
                                            }
                                            patchOat1 = true;
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, byteOrigSOat9, maskSOat9, byteReplaceSOat9, rep_maskSOat9, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!InstallLocationPolice\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!InstallLocationPolice\n");
                                            }
                                            patchOat2 = true;
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, byteOrigSOat12, maskSOat12, byteReplaceSOat12, rep_maskSOat12, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!InstallLocationPolice\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!InstallLocationPolice\n");
                                            }
                                            patchOat2 = true;
                                            patch4 = true;
                                        }
                                        if (applyPatch(curentPos, curentByte, byteOrigSOat13, maskSOat13, byteReplaceSOat13, rep_maskSOat13, pattern32)) {
                                            if (paramArrayOfString[0].contains("patch")) {
                                                System.out.println("Core4 patched!InstallLocationPolice\n");
                                            }
                                            if (paramArrayOfString[0].contains("restore")) {
                                                System.out.println("Core4 restored!InstallLocationPolice\n");
                                            }
                                            patchOat2 = true;
                                            patch4 = true;
                                        }
                                        if (!pattern32) {
                                        }
                                        fileBytes.position(curentPos + 1);
                                    }
                                }
                                ChannelDex.close();
                                if (paramArrayOfString[4].contains("framework")) {
                                    System.out.println("Rebuild file!");
                                    if (file2.toString().endsWith("/classes.dex")) {
                                        Utils.fixadler(file2);
                                        if (patch4) {
                                            not_found_bytes_for_patch = false;
                                            destination = paramArrayOfString[1].replace("/services.jar", "/services-patched.jar");
                                            source = paramArrayOfString[1];
                                            new File(destination).delete();
                                            files = new ArrayList();
                                            System.out.println("add files");
                                            files.add(new AddFilesItem(file2.getAbsolutePath(), Utils.getDirs(file2).getAbsolutePath() + InternalZipConstants.ZIP_FILE_SEPARATOR));
                                            Utils.addFilesToZip(source, destination, files);
                                            System.out.println("add files finish");
                                            new File(source).delete();
                                        } else {
                                            not_found_bytes_for_patch = true;
                                        }
                                    }
                                    if (file2.toString().endsWith("/services.odex")) {
                                        if (Utils.isELFfiles(file2)) {
                                            Utils.fixadlerOdex(file2, null);
                                        }
                                        if (!patch4) {
                                            file2.renameTo(new File(file2.getAbsolutePath().replace("/services.odex", "/services-patched.odex")));
                                        }
                                    }
                                } else {
                                    if (file2.toString().endsWith("/classes.dex")) {
                                        Utils.fixadler(file2);
                                    } else {
                                        Utils.fixadlerOdex(file2, "/system/framework/services.jar");
                                    }
                                    System.out.println("LuckyPatcher: dalvik-cache patched! ");
                                    System.out.println("start");
                                    destination = "/system/framework/services.backup";
                                    if (Utils.copyFile("/system/framework/services.jar", destination, true, false)) {
                                        System.out.println("good space");
                                        new File(destination).delete();
                                        files = new ArrayList();
                                        System.out.println("add files");
                                        files.add(new AddFilesItem("/data/app/classes.dex", "/data/app/"));
                                        Utils.addFilesToZip("/system/framework/services.jar", "/system/framework/services.backup", files);
                                        System.out.println("add files finish");
                                        Utils.run_all_no_root("chmod", "0644", "/system/framework/services.backup");
                                        Utils.run_all_no_root("chown", "0:0", "/system/framework/services.backup");
                                        Utils.run_all_no_root("chmod", "0.0", "/system/framework/services.backup");
                                        file = new File("/system/framework/services.odex");
                                        d = Utils.getFileDalvikCache("/system/framework/services.jar");
                                        if (file.exists()) {
                                            System.out.println("fix odex na osnove rebuild services");
                                            Utils.fixadlerOdex(file, "/system/framework/services.backup");
                                        }
                                        new File(listAppsFragment.toolfilesdir + "/ClearDalvik.on").createNewFile();
                                        new File("/system/framework/services.jar").delete();
                                        Utils.run_all_no_root("rm", "/system/framework/services.jar");
                                        new File("/system/framework/services.backup").renameTo(new File("/system/framework/services.jar"));
                                        if (new File("/system/framework/services.jar").exists()) {
                                            Utils.run_all_no_root("mv", "/system/framework/services.backup", "/system/framework/services.jar");
                                        }
                                        if (d != null) {
                                            Utils.run_all_no_root("rm", d.getAbsolutePath());
                                            if (d.exists()) {
                                                d.delete();
                                            }
                                        }
                                        new File("/data/dalvik-cache/arm/system@framework@arm@boot.oat").delete();
                                        new File("/data/dalvik-cache/arm/system@framework@arm@boot.art").delete();
                                        new File("/data/dalvik-cache/arm64/system@framework@arm@boot.oat").delete();
                                        new File("/data/dalvik-cache/arm64/system@framework@arm@boot.art").delete();
                                        new File("/data/dalvik-cache/x86/system@framework@arm@boot.oat").delete();
                                        new File("/data/dalvik-cache/x86/system@framework@arm@boot.art").delete();
                                        Utils.run_all_no_root("rm", "/data/dalvik-cache/*.dex");
                                        Utils.run_all_no_root("rm", "/data/dalvik-cache/*.oat");
                                        Utils.run_all_no_root("rm", "/data/dalvik-cache/*.art");
                                        Utils.run_all_no_root("rm", "/data/dalvik-cache/arm/*.dex");
                                        Utils.run_all_no_root("rm", "/data/dalvik-cache/arm/*.art");
                                        Utils.run_all_no_root("rm", "/data/dalvik-cache/arm/*.oat");
                                        Utils.run_all_no_root("rm", "/data/dalvik-cache/arm64/*.dex");
                                        Utils.run_all_no_root("rm", "/data/dalvik-cache/arm64/*.art");
                                        Utils.run_all_no_root("rm", "/data/dalvik-cache/arm64/*.oat");
                                        Utils.run_all_no_root("rm", "/data/dalvik-cache/x86/*.dex");
                                        Utils.run_all_no_root("rm", "/data/dalvik-cache/x86/*.art");
                                        Utils.run_all_no_root("rm", "/data/dalvik-cache/x86/*.oat");
                                        System.out.println("finish");
                                    } else {
                                        new File(destination).delete();
                                        file = new File(paramArrayOfString[2]);
                                        ChannelDex = new RandomAccessFile(file, InternalZipConstants.WRITE_MODE).getChannel();
                                        fileBytes = ChannelDex.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex.size()));
                                        counter = 0;
                                        counter2 = 0;
                                        lastByteReplace = null;
                                        lastPatchPosition = 0;
                                        patch4 = false;
                                        j = 0;
                                        while (fileBytes.hasRemaining()) {
                                            curentPos = fileBytes.position();
                                            curentByte = fileBytes.get();
                                            if (applyPatch(curentPos, curentByte, bArr, bArr2, bArr3, bArr4, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\n");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr13, bArr14, bArr15, bArr16, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\n");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr17, bArr18, bArr19, bArr20, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr21, bArr22, bArr23, bArr24, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\nCM12");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\nCM12");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatchCounter(curentPos, curentByte, bArr25, bArr26, bArr27, bArr28, counter, counter8, pattern42)) {
                                                counter++;
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\nRemove Package from intent");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\nRemove Package from intent");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatchCounter(curentPos, curentByte, bArr29, bArr30, bArr31, bArr32, counter2, counter9, pattern42)) {
                                                counter2++;
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\nRemove Package from intent");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\nRemove Package from intent");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr33, bArr34, bArr35, bArr36, pattern42)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\nbuildResolveList");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\nbuildResolveList");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr37, bArr38, bArr39, bArr40, pattern42)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\nbuildResolveList");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\nbuildResolveList");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr41, bArr42, bArr43, bArr44, pattern42)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\nbuildResolveList");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\nbuildResolveList");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr5, bArr6, bArr7, bArr8, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 patched!\nCM11");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 restored!\nCM11");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, bArr9, bArr10, bArr11, bArr12, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 policy patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 policy restored!\n");
                                                }
                                                patch4 = true;
                                            }
                                            if (applyPatch(curentPos, curentByte, byteOrigS13, maskS13, byteReplaceS13, rep_maskS13, pattern32)) {
                                                if (paramArrayOfString[0].contains("patch")) {
                                                    System.out.println("Core4 policy patched!\n");
                                                }
                                                if (paramArrayOfString[0].contains("restore")) {
                                                    System.out.println("Core4 policy restored!\n");
                                                }
                                                patch4 = true;
                                            }
                                            fileBytes.position(curentPos + 1);
                                            j++;
                                        }
                                        fileBytes.position(lastPatchPosition);
                                        fileBytes.put(lastByteReplace);
                                        fileBytes.force();
                                        ChannelDex.close();
                                        if (file.toString().endsWith("/classes.dex")) {
                                            Utils.fixadler(file);
                                        } else {
                                            Utils.fixadlerOdex(file, "/system/framework/services.jar");
                                        }
                                        System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                        file2 = file;
                                    }
                                    if (!onlyDalvik) {
                                        if (new File("/system/framework/services.patched").exists()) {
                                            System.out.println("LuckyPatcher: root found services.patched! ");
                                        }
                                        if (new File("/system/framework/services.odex").exists()) {
                                            System.out.println("LuckyPatcher: root found services.odex! ");
                                        }
                                        if (!paramArrayOfString[0].contains("restore")) {
                                            new File("/system/framework/patch3.done").delete();
                                        }
                                    }
                                }
                            }
                            Utils.exitFromRootJava();
                        }
                    } else {
                        file = new File(paramArrayOfString[1]);
                        if (file.exists()) {
                            if (file.toString().contains("system@framework@core.jar@classes.dex")) {
                                file = new File("/system/framework/core.odex");
                                if (file.exists() && file.length() == 0) {
                                    file.delete();
                                }
                            }
                            filesToPatch.add(file);
                            file2 = file;
                        } else {
                            throw new FileNotFoundException();
                        }
                    }
                    it2 = filesToPatch.iterator();
                    while (it2.hasNext()) {
                        file2 = (File) it2.next();
                        System.out.println("file for patch: " + file2.getAbsolutePath() + " size:" + file2.length());
                        ChannelDex = new RandomAccessFile(file2, InternalZipConstants.WRITE_MODE).getChannel();
                        fileBytes = ChannelDex.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex.size()));
                        boolean patch1 = false;
                        if (file2.getName().contains("boot.oat")) {
                            patchOatUpd1 = false;
                            patchOat1 = false;
                            patchOat2 = false;
                            patchOat3 = false;
                            Object obj = null;
                            fileBytes.position(4120);
                            posit = Utils.convertFourBytesToInt(fileBytes.get(), fileBytes.get(), fileBytes.get(), fileBytes.get());
                            System.out.println("Start position:" + posit);
                            fileBytes.position(posit);
                            while (fileBytes.hasRemaining()) {
                                try {
                                    curentPos = fileBytes.position();
                                    curentByte = fileBytes.get();
                                    if (applyPatch(curentPos, curentByte, byteOrigOatUpd1, maskOatUpd1, byteReplaceOatUpd1, rep_maskOatUpd1, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("position:" + curentPos);
                                            System.out.println("Oat Core1uni patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core1uni restored!\n");
                                        }
                                        patchOat1 = true;
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOat1, maskOat1, byteReplaceOat1, rep_maskOat1, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("position:" + curentPos);
                                            System.out.println("Oat Core11 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core11 restored!\n");
                                        }
                                        patchOat1 = true;
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOatUpd4, maskOatUpd4, byteReplaceOatUpd4, rep_maskOatUpd4, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("position:" + curentPos);
                                            System.out.println("Oat Core1uni patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core1uni restored!\n");
                                        }
                                        patchOat1 = true;
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOatUpd5, maskOatUpd5, byteReplaceOatUpd5, rep_maskOatUpd5, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("position:" + curentPos);
                                            System.out.println("Oat Core1uni patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core1uni restored!\n");
                                        }
                                        patchOat1 = true;
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOat4, maskOat4, byteReplaceOat4, rep_maskOat4, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("position:" + curentPos);
                                            System.out.println("Oat Core11 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core11 restored!\n");
                                        }
                                        patchOat1 = true;
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOat2, maskOat2, byteReplaceOat2, rep_maskOat2, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Oat Core12 patched!\n");
                                            System.out.println("position:" + curentPos);
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core12 restored!\n");
                                        }
                                        patchOat2 = true;
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOat5, maskOat5, byteReplaceOat5, rep_maskOat5, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Oat Core12 patched!\n");
                                            System.out.println("position:" + curentPos);
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core12 restored!\n");
                                        }
                                        patchOat2 = true;
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOat3, maskOat3, byteReplaceOat3, rep_maskOat3, pattern22)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Oat Core2 patched!\n");
                                            System.out.println("position:" + curentPos);
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core2 restored!\n");
                                        }
                                        patchOat3 = true;
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOat6, maskOat6, byteReplaceOat6, rep_maskOat6, pattern22)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Oat Core2 patched!\n");
                                            System.out.println("position:" + curentPos);
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core2 restored!\n");
                                        }
                                        patchOat3 = true;
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigOat7, maskOat7, byteReplaceOat7, rep_maskOat7, pattern22)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Oat Core2 patched!\n");
                                            System.out.println("position:" + curentPos);
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Oat Core2 restored!\n");
                                        }
                                        patchOat3 = true;
                                        patch1 = true;
                                    }
                                    if ((pattern1 && (pattern22 & patchOatUpd1) != 0 && patchOat1 && patchOat2 && patchOat3) || ((pattern1 && !pattern22 && patchOatUpd1 && patchOat1 && patchOat2) || (!pattern1 && pattern22 && patchOat3))) {
                                        patch1 = true;
                                        break;
                                    }
                                    fileBytes.position(curentPos + 1);
                                } catch (Exception e222222) {
                                    System.out.println(BuildConfig.VERSION_NAME + e222222);
                                } catch (FileNotFoundException e10) {
                                }
                            }
                        } else {
                            j = 0;
                            while (fileBytes.hasRemaining()) {
                                try {
                                    curentPos = fileBytes.position();
                                    curentByte = fileBytes.get();
                                    if (applyPatch(curentPos, curentByte, byteOrig2, mask2, byteReplace2, rep_mask2, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrig3, mask2, byteReplace3, rep_mask2, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrig4, mask4, byteReplace4, rep_mask4, pattern22)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core unsigned install patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core unsigned install restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrig5, mask5, byteReplace5, rep_mask5, pattern22)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core unsigned install patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core unsigned install restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrig6, mask6, byteReplace6, rep_mask6, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrig7, mask7, byteReplace7, rep_mask7, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core 2 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core 2 restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrig8, mask8, byteReplace8, rep_mask8, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core 2 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core 2 restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrig9, mask9, byteReplace9, rep_mask9, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core 2 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core 2 restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr45, bArr46, bArr47, bArr48, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr49, bArr50, bArr51, bArr52, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr53, bArr54, bArr55, bArr56, pattern22)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core unsigned install patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core unsigned install restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr57, bArr58, bArr59, bArr60, pattern22)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core unsigned install patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core unsigned install restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr85, bArr86, bArr87, bArr88, pattern22)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core unsigned install patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core unsigned install restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr89, bArr90, bArr91, bArr92, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr61, bArr62, bArr63, bArr64, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr65, bArr66, bArr67, bArr68, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core 2 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core 2 restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr69, bArr70, bArr71, bArr72, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core 2 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core 2 restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr73, bArr74, bArr75, bArr76, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core 2 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core 2 restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr77, bArr78, bArr79, bArr80, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core 2 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core 2 restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr81, bArr82, bArr83, bArr84, pattern1)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core 2 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core 2 restored!\n");
                                        }
                                        patch1 = true;
                                    }
                                    fileBytes.position(curentPos + 1);
                                    j++;
                                } catch (Exception e2222222) {
                                    System.out.println(BuildConfig.VERSION_NAME + e2222222);
                                } catch (FileNotFoundException e102) {
                                }
                            }
                        }
                        ChannelDex.close();
                        if (paramArrayOfString[4].contains("framework")) {
                            if (file2.toString().endsWith("/classes.dex")) {
                                not_found_bytes_for_patch = false;
                                Utils.fixadler(file2);
                                if (patch1) {
                                    destination = paramArrayOfString[1].replace(".jar", "-patched.jar");
                                    source = paramArrayOfString[1];
                                    new File(destination).delete();
                                    files = new ArrayList();
                                    System.out.println("add files");
                                    files.add(new AddFilesItem(file2.getAbsolutePath(), Utils.getDirs(file2).getAbsolutePath() + InternalZipConstants.ZIP_FILE_SEPARATOR));
                                    try {
                                        Utils.addFilesToZip(source, destination, files);
                                        System.out.println("add files finish");
                                        new File(source).delete();
                                    } catch (Exception e22222222) {
                                        e22222222.printStackTrace();
                                        new File(destination).delete();
                                    } catch (FileNotFoundException e1022) {
                                    }
                                } else {
                                    not_found_bytes_for_patch = true;
                                }
                            }
                            if (file2.toString().endsWith("/core.odex")) {
                                Utils.fixadlerOdex(file2, null);
                                if (patch1) {
                                    file2.renameTo(new File(file2.getAbsolutePath().replace("/core.odex", "/core-patched.odex")));
                                }
                            }
                            if (file2.toString().endsWith("/boot.oat") && patch1) {
                                file2.renameTo(new File(file2.getAbsolutePath().replace("/boot.oat", "/boot-patched.oat")));
                            }
                        } else {
                            if (file2.toString().endsWith("/classes.dex")) {
                                Utils.fixadler(file2);
                            } else {
                                Utils.fixadlerOdex(file2, "/system/framework/core.jar");
                            }
                            if (paramArrayOfString[4].contains("ART")) {
                                if (file2.toString().contains("/classes.dex") && patch1) {
                                    System.out.println("start");
                                    destination = "/system/framework/core-libart.backup";
                                    if (Utils.copyFile("/system/framework/core-libart.jar", destination, true, false)) {
                                        System.out.println("good space");
                                        new File(destination).delete();
                                        files = new ArrayList();
                                        System.out.println("add files");
                                        files.add(new AddFilesItem("/data/app/classes.dex", "/data/app/"));
                                        try {
                                            Utils.addFilesToZip("/system/framework/core-libart.jar", "/system/framework/core-libart.backup", files);
                                            System.out.println("add files finish");
                                            Utils.run_all_no_root("chmod", "0644", "/system/framework/core-libart.backup");
                                            Utils.run_all_no_root("chown", "0:0", "/system/framework/core-libart.backup");
                                            Utils.run_all_no_root("chmod", "0.0", "/system/framework/core-libart.backup");
                                            Utils.run_all_no_root("rm", "/system/framework/core-libart.jar");
                                            if (new File("/system/framework/core-libart.jar").exists()) {
                                                new File("/system/framework/core-libart.jar").delete();
                                            }
                                            Utils.run_all_no_root("mv", "/system/framework/core.backup", "/system/framework/core-libart.jar");
                                            if (!new File("/system/framework/core-libart.jar").exists()) {
                                                new File("/system/framework/core-libart.backup").renameTo(new File("/system/framework/core-libart.jar"));
                                            }
                                            Utils.run_all_no_root("chmod", "0644", "/system/framework/core-libart.jar");
                                            Utils.run_all_no_root("chown", "0:0", "/system/framework/core-libart.jar");
                                            Utils.run_all_no_root("chmod", "0.0", "/system/framework/core-libart.jar");
                                            File dalv = Utils.getFileDalvikCache("/system/framework/core-libart.jar");
                                            if (dalv != null) {
                                                Utils.run_all_no_root("rm", dalv.getAbsolutePath());
                                                if (dalv.exists()) {
                                                    dalv.delete();
                                                }
                                            }
                                            new File("/data/dalvik-cache/arm/system@framework@arm@boot.oat").delete();
                                            new File("/data/dalvik-cache/arm/system@framework@arm@boot.art").delete();
                                            new File("/data/dalvik-cache/arm64/system@framework@arm@boot.oat").delete();
                                            new File("/data/dalvik-cache/arm64/system@framework@arm@boot.art").delete();
                                            new File("/data/dalvik-cache/x86/system@framework@arm@boot.oat").delete();
                                            new File("/data/dalvik-cache/x86/system@framework@arm@boot.art").delete();
                                        } catch (Exception e222222222) {
                                            e222222222.printStackTrace();
                                            System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                            new File("/system/framework/core.backup").delete();
                                        } catch (FileNotFoundException e10222) {
                                        }
                                        System.out.println("finish");
                                    } else {
                                        new File(destination).delete();
                                        System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                    }
                                }
                            } else if (!onlyDalvik) {
                                if (file2.toString().contains("system@framework@core.jar@classes.dex") && patch1) {
                                    System.out.println("LuckyPatcher: dalvik-cache patched! ");
                                    file = new File("/system/framework/core.patched");
                                    if (Utils.copyFile(file2.getAbsolutePath(), "/system/framework/core.patched", true, false)) {
                                        Utils.run_all_no_root("chmod", "0644", "/system/framework/core.patched");
                                        Utils.run_all_no_root("chown", "0.0", destination);
                                        Utils.run_all_no_root("chown", "0:0", destination);
                                    } else {
                                        file.delete();
                                        System.out.println("LuckyPatcher: not space to system for odex core.jar! ");
                                        System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                    }
                                }
                                if (new File("/system/framework/core.patched").exists()) {
                                    System.out.println("LuckyPatcher: root found core.patched! ");
                                }
                                if (new File("/system/framework/core.odex").exists()) {
                                    System.out.println("LuckyPatcher: root found core.odex! ");
                                }
                            }
                        }
                    }
                } catch (FileNotFoundException e102222) {
                } catch (Exception e11) {
                    e222222222 = e11;
                }
            }
            if (pattern32 || pattern42) {
                System.out.println("Start patch for services.jar");
                indexDir = BuildConfig.VERSION_NAME;
                if (new File("/system/framework/arm/services.odex").exists()) {
                    indexDir = "/arm";
                }
                if (new File("/system/framework/arm64/services.odex").exists()) {
                    indexDir = "/arm64";
                }
                if (new File("/system/framework/x86/services.odex").exists()) {
                    indexDir = "/x86";
                }
                if (new File("/system/framework/arm/services.odex.xz").exists()) {
                    indexDir = "/arm";
                }
                if (new File("/system/framework/arm64/services.odex.xz").exists()) {
                    indexDir = "/arm64";
                }
                if (new File("/system/framework/x86/services.odex.xz").exists()) {
                    indexDir = "/x86";
                }
                if (!indexDir.equals(BuildConfig.VERSION_NAME) && listAppsFragment.api >= 21) {
                    patchOat1 = false;
                    patchOat2 = false;
                    patchOat3 = false;
                    patchOat4 = false;
                    patchOat5 = false;
                    patchOat6 = false;
                    file = new File("/system/framework" + indexDir + "/services.odex");
                    good_odex = true;
                    if (new File("/system/framework" + indexDir + "/services.odex.xz").exists()) {
                        System.out.println("try unpack services.odex.xz");
                        if (Utils.XZDecompress(new File("/system/framework" + indexDir + "/services.odex.xz"), "/system/framework" + indexDir)) {
                            good_odex = false;
                            new File("/system/framework" + indexDir + "/services.odex").delete();
                            System.out.println("not enought space for unpack services.odex.xz");
                        } else {
                            Utils.run_all_no_root("chmod", "644", "/system/framework" + indexDir + "/services.odex");
                            Utils.run_all_no_root("chown", "0:0", "/system/framework" + indexDir + "/services.odex");
                            Utils.run_all_no_root("chown", "0.0", "/system/framework" + indexDir + "/services.odex");
                        }
                    }
                    if (good_odex) {
                        ChannelDex = new RandomAccessFile(file, InternalZipConstants.WRITE_MODE).getChannel();
                        fileBytes = ChannelDex.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex.size()));
                        fileBytes.position(4120);
                        posit = Utils.convertFourBytesToInt(fileBytes.get(), fileBytes.get(), fileBytes.get(), fileBytes.get());
                        System.out.println("Start position:" + posit);
                        fileBytes.position(posit);
                        while (fileBytes.hasRemaining()) {
                            curentPos = fileBytes.position();
                            curentByte = fileBytes.get();
                            if (applyPatch(curentPos, curentByte, byteOrigSOat1, maskSOat1, byteReplaceSOat1, rep_maskSOat1, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\n");
                                }
                                patchOat1 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat2, maskSOat2, byteReplaceSOat2, rep_maskSOat2, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                }
                                patchOat2 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat3, maskSOat3, byteReplaceSOat3, rep_maskSOat3, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                }
                                patchOat3 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat4, maskSOat4, byteReplaceSOat4, rep_maskSOat4, pattern42)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!queryIntentServices\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!queryIntentServices\n");
                                }
                                patchOat4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat5, maskSOat5, byteReplaceSOat5, rep_maskSOat5, pattern42)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!buildResolveList\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!buildResolveList\n");
                                }
                                patchOat5 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat6, maskSOat6, byteReplaceSOat6, rep_maskSOat6, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!FixForCM\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!FixForCM\n");
                                }
                                patchOat6 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat7, maskSOat7, byteReplaceSOat7, rep_maskSOat7, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!FixForCM\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!FixForCM\n");
                                }
                                patchOat6 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat8, maskSOat8, byteReplaceSOat8, rep_maskSOat8, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\n");
                                }
                                patchOat1 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat9, maskSOat9, byteReplaceSOat9, rep_maskSOat9, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                }
                                patchOat2 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat12, maskSOat12, byteReplaceSOat12, rep_maskSOat12, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                }
                                patchOat2 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat13, maskSOat13, byteReplaceSOat13, rep_maskSOat13, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                }
                                patchOat2 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat10, maskSOat10, byteReplaceSOat10, rep_maskSOat10, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\n");
                                }
                                patchOat1 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat11, maskSOat11, byteReplaceSOat11, rep_maskSOat11, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\n");
                                }
                                patchOat1 = true;
                            }
                            if (!pattern32 && patchOat1 && patchOat2 && patchOat3 && patchOat4 && patchOat5 && patchOat6) {
                                new File("/data/dalvik-cache/arm/system@framework@services.jar@classes.dex").delete();
                                new File("/data/dalvik-cache/arm/system@framework@services.jar@classes.art").delete();
                                new File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.dex").delete();
                                new File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.art").delete();
                                new File("/data/dalvik-cache/x86/system@framework@services.jar@classes.dex").delete();
                                new File("/data/dalvik-cache/x86/system@framework@services.jar@classes.art").delete();
                                Utils.cmdParam(listAppsFragment.toolfilesdir + "/reboot");
                                break;
                            }
                            fileBytes.position(curentPos + 1);
                        }
                        ChannelDex.close();
                    } else {
                        System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                    }
                    if (patchOat1 || patchOat2 || patchOat3 || patchOat4 || patchOat5 || patchOat6) {
                        new File("/data/dalvik-cache/arm/system@framework@services.jar@classes.dex").delete();
                        new File("/data/dalvik-cache/arm/system@framework@services.jar@classes.art").delete();
                        new File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.dex").delete();
                        new File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.art").delete();
                        new File("/data/dalvik-cache/x86/system@framework@services.jar@classes.dex").delete();
                        new File("/data/dalvik-cache/x86/system@framework@services.jar@classes.art").delete();
                        Utils.cmdParam(listAppsFragment.toolfilesdir + "/reboot");
                    }
                }
                if (!paramArrayOfString[4].contains("framework")) {
                    if (paramArrayOfString[1].contains("services.jar") && Utils.classes_test(new File(paramArrayOfString[1]))) {
                        file = new File(paramArrayOfString[1]);
                        unzip(serjar, Utils.getDirs(serjar).getAbsolutePath());
                        file = new File(Utils.getDirs(serjar).getAbsolutePath() + "/classes.dex");
                        if (file.exists()) {
                            filesToPatch.add(file);
                        }
                    }
                    if (paramArrayOfString[1].contains("services.odex")) {
                        filesToPatch.add(new File(paramArrayOfString[1]));
                    }
                } else if (!paramArrayOfString[4].contains("ART")) {
                    if (new File("/system/framework/services.odex").exists() && new File("/system/framework/services.odex").length() != 0) {
                        System.out.println("Add services.odex for patch");
                        filesToPatch.add(new File("/system/framework/services.odex"));
                    }
                    if (Utils.classes_test(new File("/system/framework/services.jar"))) {
                        System.out.println("services.jar contain classes,dex");
                        file = new File("/system/framework/services.jar");
                        unzip(serjar, "/data/app");
                        file = new File("/data/app/classes.dex");
                        if (file.exists()) {
                            System.out.println("Add classes.dex for patch");
                            filesToPatch.add(file);
                        }
                    }
                } else if (onlyDalvik) {
                    file = new File(paramArrayOfString[2]);
                    if (file.exists()) {
                        throw new FileNotFoundException();
                    }
                    filesToPatch.add(file);
                    file2 = file;
                } else {
                    System.out.println("Vhodjashij file " + paramArrayOfString[2]);
                    file = new File(paramArrayOfString[2]);
                    if (file.exists()) {
                        throw new FileNotFoundException();
                    }
                    if (file.toString().contains("system@framework@services.jar@classes.dex")) {
                        System.out.println("Vhodjashij file byl dalvick-cache " + paramArrayOfString[2]);
                        file = new File("/system/framework/services.jar");
                        unzip(serjar, "/data/app");
                        file = new File("/data/app/classes.dex");
                        if (file.exists()) {
                            file2 = file;
                        } else {
                            file2 = file;
                        }
                        file = new File("/system/framework/services.odex");
                        if (file.exists() && file.length() == 0) {
                            file.delete();
                        }
                    } else {
                        file2 = file;
                    }
                    System.out.println("Add file for patch " + file2.toString());
                    filesToPatch.add(file2);
                }
                it = filesToPatch.iterator();
                while (it.hasNext()) {
                    fp = (File) it.next();
                    System.out.println("Start patch for " + fp.toString());
                    file2 = fp;
                    isOat = false;
                    if (Utils.isELFfiles(file2)) {
                        isOat = true;
                    }
                    ChannelDex = new RandomAccessFile(file2, InternalZipConstants.WRITE_MODE).getChannel();
                    fileBytes = ChannelDex.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex.size()));
                    patch4 = false;
                    counter = 0;
                    counter2 = 0;
                    if (!isOat) {
                        patchOat1 = false;
                        patchOat2 = false;
                        patchOat3 = false;
                        patchOat4 = false;
                        patchOat5 = false;
                        patchOat6 = false;
                        fileBytes.position(4120);
                        posit = Utils.convertFourBytesToInt(fileBytes.get(), fileBytes.get(), fileBytes.get(), fileBytes.get());
                        System.out.println("Start position:" + posit);
                        fileBytes.position(posit);
                        while (fileBytes.hasRemaining()) {
                            curentPos = fileBytes.position();
                            curentByte = fileBytes.get();
                            if (applyPatch(curentPos, curentByte, byteOrigSOat1, maskSOat1, byteReplaceSOat1, rep_maskSOat1, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\n");
                                }
                                patchOat1 = true;
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat2, maskSOat2, byteReplaceSOat2, rep_maskSOat2, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                }
                                patchOat2 = true;
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat3, maskSOat3, byteReplaceSOat3, rep_maskSOat3, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                }
                                patchOat3 = true;
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat4, maskSOat4, byteReplaceSOat4, rep_maskSOat4, pattern42)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!queryIntentServices\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!queryIntentServices\n");
                                }
                                patchOat4 = true;
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat5, maskSOat5, byteReplaceSOat5, rep_maskSOat5, pattern42)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!buildResolveList\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!buildResolveList\n");
                                }
                                patchOat5 = true;
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat6, maskSOat6, byteReplaceSOat6, rep_maskSOat6, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!FixForCM\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!FixForCM\n");
                                }
                                patchOat6 = true;
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat7, maskSOat7, byteReplaceSOat7, rep_maskSOat7, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!FixForCM\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!FixForCM\n");
                                }
                                patchOat6 = true;
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat8, maskSOat8, byteReplaceSOat8, rep_maskSOat8, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\n");
                                }
                                patchOat1 = true;
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat10, maskSOat10, byteReplaceSOat10, rep_maskSOat10, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\n");
                                }
                                patchOat1 = true;
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat11, maskSOat11, byteReplaceSOat11, rep_maskSOat11, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\n");
                                }
                                patchOat1 = true;
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat9, maskSOat9, byteReplaceSOat9, rep_maskSOat9, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                }
                                patchOat2 = true;
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat12, maskSOat12, byteReplaceSOat12, rep_maskSOat12, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                }
                                patchOat2 = true;
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigSOat13, maskSOat13, byteReplaceSOat13, rep_maskSOat13, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!InstallLocationPolice\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!InstallLocationPolice\n");
                                }
                                patchOat2 = true;
                                patch4 = true;
                            }
                            if (!pattern32 && patchOat1 && patchOat2 && patchOat3 && patchOat4 && patchOat5 && patchOat6) {
                                break;
                            }
                            fileBytes.position(curentPos + 1);
                        }
                    } else {
                        lastByteReplace = null;
                        lastPatchPosition = 0;
                        j = 0;
                        while (fileBytes.hasRemaining()) {
                            curentPos = fileBytes.position();
                            curentByte = fileBytes.get();
                            if (applyPatch(curentPos, curentByte, bArr, bArr2, bArr3, bArr4, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\n");
                                }
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, bArr13, bArr14, bArr15, bArr16, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\n");
                                }
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, bArr17, bArr18, bArr19, bArr20, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                }
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, bArr21, bArr22, bArr23, bArr24, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\nCM12");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\nCM12");
                                }
                                patch4 = true;
                            }
                            if (applyPatchCounter(curentPos, curentByte, bArr25, bArr26, bArr27, bArr28, counter, counter8, pattern42)) {
                                counter++;
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\nRemove Package from intent");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\nRemove Package from intent");
                                }
                                patch4 = true;
                            }
                            if (applyPatchCounter(curentPos, curentByte, bArr29, bArr30, bArr31, bArr32, counter2, counter9, pattern42)) {
                                counter2++;
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\nRemove Package from intent");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\nRemove Package from intent");
                                }
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, bArr33, bArr34, bArr35, bArr36, pattern42)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\nbuildResolveList");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\nbuildResolveList");
                                }
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, bArr37, bArr38, bArr39, bArr40, pattern42)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\nbuildResolveList");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\nbuildResolveList");
                                }
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, bArr41, bArr42, bArr43, bArr44, pattern42)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\nbuildResolveList");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\nbuildResolveList");
                                }
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, bArr5, bArr6, bArr7, bArr8, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 patched!\nCM11");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 restored!\nCM11");
                                }
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, bArr9, bArr10, bArr11, bArr12, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 policy patched!\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 policy restored!\n");
                                }
                                patch4 = true;
                            }
                            if (applyPatch(curentPos, curentByte, byteOrigS13, maskS13, byteReplaceS13, rep_maskS13, pattern32)) {
                                if (paramArrayOfString[0].contains("patch")) {
                                    System.out.println("Core4 policy patched!\n");
                                }
                                if (paramArrayOfString[0].contains("restore")) {
                                    System.out.println("Core4 policy restored!\n");
                                }
                                patch4 = true;
                            }
                            fileBytes.position(curentPos + 1);
                            j++;
                        }
                        if (lastPatchPosition > 0 && lastByteReplace != null) {
                            fileBytes.position(lastPatchPosition);
                            fileBytes.put(lastByteReplace);
                            fileBytes.force();
                        }
                    }
                    ChannelDex.close();
                    if (paramArrayOfString[4].contains("framework")) {
                        System.out.println("Rebuild file!");
                        if (file2.toString().endsWith("/classes.dex")) {
                            Utils.fixadler(file2);
                            if (patch4) {
                                not_found_bytes_for_patch = false;
                                destination = paramArrayOfString[1].replace("/services.jar", "/services-patched.jar");
                                source = paramArrayOfString[1];
                                new File(destination).delete();
                                files = new ArrayList();
                                System.out.println("add files");
                                files.add(new AddFilesItem(file2.getAbsolutePath(), Utils.getDirs(file2).getAbsolutePath() + InternalZipConstants.ZIP_FILE_SEPARATOR));
                                Utils.addFilesToZip(source, destination, files);
                                System.out.println("add files finish");
                                new File(source).delete();
                            } else {
                                not_found_bytes_for_patch = true;
                            }
                        }
                        if (file2.toString().endsWith("/services.odex")) {
                            if (Utils.isELFfiles(file2)) {
                                Utils.fixadlerOdex(file2, null);
                            }
                            if (!patch4) {
                                file2.renameTo(new File(file2.getAbsolutePath().replace("/services.odex", "/services-patched.odex")));
                            }
                        }
                    } else {
                        if (file2.toString().endsWith("/classes.dex")) {
                            Utils.fixadler(file2);
                        } else {
                            Utils.fixadlerOdex(file2, "/system/framework/services.jar");
                        }
                        if (file2.toString().contains("system@framework@services.jar@classes.dex") && patch4) {
                            System.out.println("LuckyPatcher: dalvik-cache patched! ");
                        }
                        if (file2.toString().contains("/classes.dex") && patch4) {
                            System.out.println("start");
                            destination = "/system/framework/services.backup";
                            if (Utils.copyFile("/system/framework/services.jar", destination, true, false)) {
                                System.out.println("good space");
                                new File(destination).delete();
                                files = new ArrayList();
                                System.out.println("add files");
                                files.add(new AddFilesItem("/data/app/classes.dex", "/data/app/"));
                                Utils.addFilesToZip("/system/framework/services.jar", "/system/framework/services.backup", files);
                                System.out.println("add files finish");
                                Utils.run_all_no_root("chmod", "0644", "/system/framework/services.backup");
                                Utils.run_all_no_root("chown", "0:0", "/system/framework/services.backup");
                                Utils.run_all_no_root("chmod", "0.0", "/system/framework/services.backup");
                                file = new File("/system/framework/services.odex");
                                d = Utils.getFileDalvikCache("/system/framework/services.jar");
                                if (file.exists()) {
                                    System.out.println("fix odex na osnove rebuild services");
                                    Utils.fixadlerOdex(file, "/system/framework/services.backup");
                                }
                                new File(listAppsFragment.toolfilesdir + "/ClearDalvik.on").createNewFile();
                                new File("/system/framework/services.jar").delete();
                                Utils.run_all_no_root("rm", "/system/framework/services.jar");
                                new File("/system/framework/services.backup").renameTo(new File("/system/framework/services.jar"));
                                if (new File("/system/framework/services.jar").exists()) {
                                    Utils.run_all_no_root("mv", "/system/framework/services.backup", "/system/framework/services.jar");
                                }
                                if (d != null) {
                                    Utils.run_all_no_root("rm", d.getAbsolutePath());
                                    if (d.exists()) {
                                        d.delete();
                                    }
                                }
                                new File("/data/dalvik-cache/arm/system@framework@arm@boot.oat").delete();
                                new File("/data/dalvik-cache/arm/system@framework@arm@boot.art").delete();
                                new File("/data/dalvik-cache/arm64/system@framework@arm@boot.oat").delete();
                                new File("/data/dalvik-cache/arm64/system@framework@arm@boot.art").delete();
                                new File("/data/dalvik-cache/x86/system@framework@arm@boot.oat").delete();
                                new File("/data/dalvik-cache/x86/system@framework@arm@boot.art").delete();
                                Utils.run_all_no_root("rm", "/data/dalvik-cache/*.dex");
                                Utils.run_all_no_root("rm", "/data/dalvik-cache/*.oat");
                                Utils.run_all_no_root("rm", "/data/dalvik-cache/*.art");
                                Utils.run_all_no_root("rm", "/data/dalvik-cache/arm/*.dex");
                                Utils.run_all_no_root("rm", "/data/dalvik-cache/arm/*.art");
                                Utils.run_all_no_root("rm", "/data/dalvik-cache/arm/*.oat");
                                Utils.run_all_no_root("rm", "/data/dalvik-cache/arm64/*.dex");
                                Utils.run_all_no_root("rm", "/data/dalvik-cache/arm64/*.art");
                                Utils.run_all_no_root("rm", "/data/dalvik-cache/arm64/*.oat");
                                Utils.run_all_no_root("rm", "/data/dalvik-cache/x86/*.dex");
                                Utils.run_all_no_root("rm", "/data/dalvik-cache/x86/*.art");
                                Utils.run_all_no_root("rm", "/data/dalvik-cache/x86/*.oat");
                                System.out.println("finish");
                            } else {
                                new File(destination).delete();
                                file = new File(paramArrayOfString[2]);
                                ChannelDex = new RandomAccessFile(file, InternalZipConstants.WRITE_MODE).getChannel();
                                fileBytes = ChannelDex.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex.size()));
                                counter = 0;
                                counter2 = 0;
                                lastByteReplace = null;
                                lastPatchPosition = 0;
                                patch4 = false;
                                j = 0;
                                while (fileBytes.hasRemaining()) {
                                    curentPos = fileBytes.position();
                                    curentByte = fileBytes.get();
                                    if (applyPatch(curentPos, curentByte, bArr, bArr2, bArr3, bArr4, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!\n");
                                        }
                                        patch4 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr13, bArr14, bArr15, bArr16, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!\n");
                                        }
                                        patch4 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr17, bArr18, bArr19, bArr20, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                        }
                                        patch4 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr21, bArr22, bArr23, bArr24, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!\nCM12");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!\nCM12");
                                        }
                                        patch4 = true;
                                    }
                                    if (applyPatchCounter(curentPos, curentByte, bArr25, bArr26, bArr27, bArr28, counter, counter8, pattern42)) {
                                        counter++;
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!\nRemove Package from intent");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!\nRemove Package from intent");
                                        }
                                        patch4 = true;
                                    }
                                    if (applyPatchCounter(curentPos, curentByte, bArr29, bArr30, bArr31, bArr32, counter2, counter9, pattern42)) {
                                        counter2++;
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!\nRemove Package from intent");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!\nRemove Package from intent");
                                        }
                                        patch4 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr33, bArr34, bArr35, bArr36, pattern42)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!\nbuildResolveList");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!\nbuildResolveList");
                                        }
                                        patch4 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr37, bArr38, bArr39, bArr40, pattern42)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!\nbuildResolveList");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!\nbuildResolveList");
                                        }
                                        patch4 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr41, bArr42, bArr43, bArr44, pattern42)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!\nbuildResolveList");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!\nbuildResolveList");
                                        }
                                        patch4 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr5, bArr6, bArr7, bArr8, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 patched!\nCM11");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 restored!\nCM11");
                                        }
                                        patch4 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, bArr9, bArr10, bArr11, bArr12, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 policy patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 policy restored!\n");
                                        }
                                        patch4 = true;
                                    }
                                    if (applyPatch(curentPos, curentByte, byteOrigS13, maskS13, byteReplaceS13, rep_maskS13, pattern32)) {
                                        if (paramArrayOfString[0].contains("patch")) {
                                            System.out.println("Core4 policy patched!\n");
                                        }
                                        if (paramArrayOfString[0].contains("restore")) {
                                            System.out.println("Core4 policy restored!\n");
                                        }
                                        patch4 = true;
                                    }
                                    fileBytes.position(curentPos + 1);
                                    j++;
                                }
                                if (lastPatchPosition > 0 && lastByteReplace != null) {
                                    fileBytes.position(lastPatchPosition);
                                    fileBytes.put(lastByteReplace);
                                    fileBytes.force();
                                }
                                ChannelDex.close();
                                if (file.toString().endsWith("/classes.dex")) {
                                    Utils.fixadler(file);
                                } else {
                                    Utils.fixadlerOdex(file, "/system/framework/services.jar");
                                }
                                System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                file2 = file;
                            }
                        }
                        if (!onlyDalvik) {
                            if (new File("/system/framework/services.patched").exists()) {
                                System.out.println("LuckyPatcher: root found services.patched! ");
                            }
                            if (new File("/system/framework/services.odex").exists()) {
                                System.out.println("LuckyPatcher: root found services.odex! ");
                            }
                            if (!paramArrayOfString[0].contains("restore")) {
                                new File("/system/framework/patch3.done").delete();
                            }
                        }
                    }
                }
            }
        }
        Utils.exitFromRootJava();
        System.out.println("Exception e" + e222222222.toString());
        Utils.exitFromRootJava();
        System.out.println("Error: services.odex not found!\n\nPlease Odex services.jar and try again!");
        Utils.exitFromRootJava();
    }

    public static void unzip(File apk, String dirr) {
        boolean found1 = false;
        String dir = dirr;
        try {
            FileInputStream fin = new FileInputStream(apk);
            ZipInputStream zin = new ZipInputStream(fin);
            for (ZipEntry ze = zin.getNextEntry(); ze != null && true; ze = zin.getNextEntry()) {
                if (ze.getName().equals("classes.dex")) {
                    FileOutputStream fout = new FileOutputStream(dir + InternalZipConstants.ZIP_FILE_SEPARATOR + "classes.dex");
                    byte[] buffer = new byte[InternalZipConstants.UFT8_NAMES_FLAG];
                    while (true) {
                        int length = zin.read(buffer);
                        if (length == -1) {
                            break;
                        }
                        fout.write(buffer, 0, length);
                    }
                    Utils.run_all_no_root("chmod", "777", dir + InternalZipConstants.ZIP_FILE_SEPARATOR + "classes.dex");
                    Utils.run_all_no_root("chown", "0.0", dir + InternalZipConstants.ZIP_FILE_SEPARATOR + "classes.dex");
                    Utils.run_all_no_root("chown", "0:0", dir + InternalZipConstants.ZIP_FILE_SEPARATOR + "classes.dex");
                    zin.closeEntry();
                    fout.close();
                    found1 = true;
                }
                if (found1) {
                    break;
                }
            }
            zin.close();
            fin.close();
        } catch (Exception e) {
            try {
                new ZipFile(apk).extractFile("classes.dex", dir);
            } catch (ZipException e1) {
                System.out.println("Error classes.dex decompress! " + e1);
                System.out.println("Exception e1" + e.toString());
            } catch (Exception e12) {
                System.out.println("Error classes.dex decompress! " + e12);
                System.out.println("Exception e1" + e.toString());
            }
            System.out.println("Exception e" + e.toString());
        }
    }

    private static boolean applyPatch(int curentPos, byte currentByte, byte[] byteOrig, byte[] mask, byte[] byteReplace, byte[] rep_mask, boolean pattern) {
        if (byteOrig != null && currentByte == byteOrig[0] && pattern) {
            if (rep_mask[0] == (byte) 0) {
                byteReplace[0] = currentByte;
            }
            int i = 1;
            fileBytes.position(curentPos + 1);
            byte prufbyte = fileBytes.get();
            while (true) {
                if (prufbyte != byteOrig[i] && mask[i] != (byte) 1) {
                    break;
                }
                if (rep_mask[i] == (byte) 0) {
                    byteReplace[i] = prufbyte;
                }
                if (rep_mask[i] == (byte) 3) {
                    byteReplace[i] = (byte) (prufbyte & 15);
                }
                if (rep_mask[i] == (byte) 2) {
                    byteReplace[i] = (byte) ((prufbyte & 15) + ((prufbyte & 15) * 16));
                }
                i++;
                if (i == byteOrig.length) {
                    fileBytes.position(curentPos);
                    fileBytes.put(byteReplace);
                    fileBytes.force();
                    return true;
                }
                prufbyte = fileBytes.get();
            }
            fileBytes.position(curentPos + 1);
        }
        return false;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean applyPatchCounter(int curentPos, byte currentByte, byte[] byteOrig, byte[] mask, byte[] byteReplace, byte[] rep_mask, int counter, int limitCounter, boolean pattern) {
        if (byteOrig != null && currentByte == byteOrig[0] && pattern) {
            if (rep_mask[0] == (byte) 0) {
                byteReplace[0] = currentByte;
            }
            int i = 1;
            fileBytes.position(curentPos + 1);
            byte prufbyte = fileBytes.get();
            while (true) {
                if (prufbyte != byteOrig[i] && mask[i] != (byte) 1) {
                    break;
                }
                if (rep_mask[i] == (byte) 0) {
                    byteReplace[i] = prufbyte;
                }
                if (rep_mask[i] == (byte) 3) {
                    byteReplace[i] = (byte) (prufbyte & 15);
                }
                if (rep_mask[i] == (byte) 2) {
                    byteReplace[i] = (byte) ((prufbyte & 15) + ((prufbyte & 15) * 16));
                }
                i++;
                if (i == byteOrig.length) {
                    break;
                }
                prufbyte = fileBytes.get();
            }
            if (counter < limitCounter) {
                return true;
            }
            lastPatchPosition = curentPos;
            lastByteReplace = new byte[byteReplace.length];
            System.arraycopy(byteReplace, 0, lastByteReplace, 0, byteReplace.length);
            return true;
        }
        return false;
    }
}
