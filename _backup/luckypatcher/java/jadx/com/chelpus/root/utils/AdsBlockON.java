package com.chelpus.root.utils;

import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.LogCollector;
import com.chelpus.Utils;
import java.io.File;
import java.io.RandomAccessFile;
import net.lingala.zip4j.util.InternalZipConstants;

public class AdsBlockON {

    static class C05401 {
        C05401() {
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void main(String[] params) {
        Utils.startRootJava(new C05401());
        String starttag = "#Lucky Patcher block Ads start#";
        String endtag = "#Lucky Patcher block Ads finish#";
        String hosts = params[0];
        String tmp_hosts = params[1];
        String raw = params[2];
        String toolfilesdir = params[3];
        String data_hosts = "/data/data/hosts";
        boolean hostsToData = false;
        String sym_path = Utils.getSimulink(hosts);
        if (!sym_path.equals(BuildConfig.VERSION_NAME)) {
            data_hosts = sym_path;
        }
        try {
            RandomAccessFile ram;
            Utils.remount("/system", InternalZipConstants.WRITE_MODE);
            Utils.run_all_no_root("chmod", "777", "/system/etc/hosts");
            Utils.run_all_no_root("chattr", "-ai", "/system/etc/hosts");
            if (new File(data_hosts).exists() && new File(data_hosts).length() == new File(hosts).length()) {
                hostsToData = true;
                System.out.println("Hosts to Data");
                Utils.run_all_no_root("chmod", "777", "/data/data/hosts");
                Utils.run_all_no_root("chattr", "-ai", "/data/data/hosts");
            }
            if (!new File(hosts).exists()) {
                Utils.save_text_to_file(new File("/system/etc/hosts"), "127.0.0.1       localhost\n");
            }
            if (hostsToData) {
                ram = new RandomAccessFile(data_hosts, InternalZipConstants.READ_MODE);
            } else {
                ram = new RandomAccessFile(hosts, InternalZipConstants.READ_MODE);
            }
            ram.seek(0);
            RandomAccessFile randomAccessFile = new RandomAccessFile(tmp_hosts, InternalZipConstants.WRITE_MODE);
            randomAccessFile.setLength(0);
            randomAccessFile.seek(0);
            boolean write = true;
            while (true) {
                String line = ram.readLine();
                if (line == null) {
                    break;
                }
                if (line.contains(starttag) || !write) {
                    write = false;
                } else {
                    randomAccessFile.writeBytes(line + LogCollector.LINE_SEPARATOR);
                }
                if (line.contains(endtag)) {
                    write = true;
                }
            }
            ram.close();
            randomAccessFile.close();
            if (Utils.save_text_to_end_file_from_file(new File(tmp_hosts), new File(raw))) {
                System.out.println(Utils.remount("/system", InternalZipConstants.WRITE_MODE));
                Utils.run_all_no_root("chmod", "777", "/system/etc/hosts");
                System.out.println(Utils.remount("/system", InternalZipConstants.WRITE_MODE));
                if (hostsToData) {
                    new File(hosts).delete();
                    Utils.copyFile(new File(tmp_hosts), new File("/data/data/hosts"));
                    if (new File(tmp_hosts).length() != new File("/data/data/hosts").length()) {
                        System.out.println("no_space_to_data");
                        new File(tmp_hosts).delete();
                        new File(data_hosts).delete();
                        Utils.save_text_to_file(new File("/system/etc/hosts"), "127.0.0.1       localhost\n");
                        Utils.run_all_no_root("chmod", "0644", "/system/etc/hosts");
                        Utils.remount("/system", "ro");
                        return;
                    }
                    if (new File(hosts).length() != new File(data_hosts).length()) {
                        Utils.run_all_no_root("chmod", "0777", "/system/etc/hosts");
                        new File(hosts).delete();
                        Utils.run_all_no_root("chmod", "0777", "/data/data/hosts");
                        Utils.run_all_no_root("chattr", "-ai", "/data/data/hosts");
                        Utils.run_all_no_root("chown", "0.0", "/data/data/hosts");
                        Utils.run_all_no_root("chown", "0:0", "/data/data/hosts");
                        Utils.run_all_no_root("ln", "-s", "/data/data/hosts", "/system/etc/hosts");
                        Utils.run_all_no_root("chmod", "0644", "/data/data/hosts");
                    } else {
                        Utils.run_all_no_root("chmod", "0644", "/data/data/hosts");
                        Utils.run_all_no_root("chattr", "-ai", "/data/data/hosts");
                        Utils.run_all_no_root("chown", "0.0", "/data/data/hosts");
                        Utils.run_all_no_root("chown", "0:0", "/data/data/hosts");
                    }
                    new File(tmp_hosts).delete();
                    Utils.run_all_no_root("chmod", "0644", "/system/etc/hosts");
                    Utils.remount("/system", "ro");
                } else {
                    Utils.copyFile(new File(tmp_hosts), new File("/system/etc/hosts"));
                    if (new File(tmp_hosts).length() != new File("/system/etc/hosts").length()) {
                        System.out.println(tmp_hosts.length());
                        System.out.println(new File("/system/etc/hosts").length());
                        new File(hosts).delete();
                        Utils.copyFile(new File(tmp_hosts), new File("/data/data/hosts"));
                        if (new File(tmp_hosts).length() != new File("/data/data/hosts").length()) {
                            System.out.println("no_space_to_data");
                            new File(tmp_hosts).delete();
                            new File(data_hosts).delete();
                            Utils.save_text_to_file(new File("/system/etc/hosts"), "127.0.0.1       localhost\n");
                            Utils.run_all_no_root("chmod", "0644", "/system/etc/hosts");
                            Utils.remount("/system", "ro");
                            return;
                        }
                        Utils.run_all_no_root("chmod", "0644", "/data/data/hosts");
                        Utils.run_all_no_root("chattr", "-ai", "/data/data/hosts");
                        Utils.run_all_no_root("chown", "0.0", "/data/data/hosts");
                        Utils.run_all_no_root("chown", "0:0", "/data/data/hosts");
                        Utils.run_all_no_root("ln", "-s", "/data/data/hosts", "/system/etc/hosts");
                        new File(tmp_hosts).delete();
                        Utils.run_all_no_root("chmod", "0644", "/system/etc/hosts");
                        Utils.remount("/system", "ro");
                    }
                }
                System.out.println("host updated!");
            } else {
                System.out.println("no_space_to_data");
            }
            new File(tmp_hosts).delete();
            Utils.run_all_no_root("chmod", "0644", "/system/etc/hosts");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("unknown error");
        } catch (OutOfMemoryError E) {
            E.printStackTrace();
            System.out.println("out.of.memory");
        }
        Utils.exitFromRootJava();
    }
}
