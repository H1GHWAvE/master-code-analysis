package android.support.v4.internal.view;

import android.graphics.drawable.Drawable;
import android.view.ContextMenu;
import android.view.View;

public interface SupportContextMenu extends SupportMenu, ContextMenu {
    void clearHeader();

    SupportContextMenu setHeaderIcon(int i);

    SupportContextMenu setHeaderIcon(Drawable drawable);

    SupportContextMenu setHeaderTitle(int i);

    SupportContextMenu setHeaderTitle(CharSequence charSequence);

    SupportContextMenu setHeaderView(View view);
}
