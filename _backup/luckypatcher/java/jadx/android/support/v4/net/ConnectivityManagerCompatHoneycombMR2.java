package android.support.v4.net;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import org.tukaani.xz.LZMA2Options;
import pxb.android.axml.AxmlParser;

class ConnectivityManagerCompatHoneycombMR2 {
    ConnectivityManagerCompatHoneycombMR2() {
    }

    public static boolean isActiveNetworkMetered(ConnectivityManager cm) {
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) {
            return true;
        }
        switch (info.getType()) {
            case LZMA2Options.MODE_UNCOMPRESSED /*0*/:
            case AxmlParser.START_TAG /*2*/:
            case AxmlParser.END_TAG /*3*/:
            case AxmlParser.START_NS /*4*/:
            case AxmlParser.END_NS /*5*/:
            case AxmlParser.TEXT /*6*/:
                return true;
            case AxmlParser.START_FILE /*1*/:
            case AxmlParser.END_FILE /*7*/:
            case LZMA2Options.PRESET_MAX /*9*/:
                return false;
            default:
                return true;
        }
    }
}
