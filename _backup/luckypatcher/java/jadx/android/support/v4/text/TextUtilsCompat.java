package android.support.v4.text;

import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import java.util.Locale;
import pxb.android.axml.AxmlParser;

public class TextUtilsCompat {
    private static String ARAB_SCRIPT_SUBTAG = "Arab";
    private static String HEBR_SCRIPT_SUBTAG = "Hebr";
    public static final Locale ROOT = new Locale(BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME);

    public static String htmlEncode(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch (c) {
                case listAppsFragment.SUPPORT_PATCH_CONTEXT_DIALOG /*34*/:
                    sb.append("&quot;");
                    break;
                case listAppsFragment.CONTEXT_DIALOG_STATIC /*38*/:
                    sb.append("&amp;");
                    break;
                case listAppsFragment.CONTEXT_DIALOG_Xposed_Patches /*39*/:
                    sb.append("&#39;");
                    break;
                case '<':
                    sb.append("&lt;");
                    break;
                case '>':
                    sb.append("&gt;");
                    break;
                default:
                    sb.append(c);
                    break;
            }
        }
        return sb.toString();
    }

    public static int getLayoutDirectionFromLocale(Locale locale) {
        if (!(locale == null || locale.equals(ROOT))) {
            String scriptSubtag = ICUCompat.getScript(ICUCompat.addLikelySubtags(locale.toString()));
            if (scriptSubtag == null) {
                return getLayoutDirectionFromFirstChar(locale);
            }
            if (scriptSubtag.equalsIgnoreCase(ARAB_SCRIPT_SUBTAG) || scriptSubtag.equalsIgnoreCase(HEBR_SCRIPT_SUBTAG)) {
                return 1;
            }
        }
        return 0;
    }

    private static int getLayoutDirectionFromFirstChar(Locale locale) {
        switch (Character.getDirectionality(locale.getDisplayName(locale).charAt(0))) {
            case AxmlParser.START_FILE /*1*/:
            case AxmlParser.START_TAG /*2*/:
                return 1;
            default:
                return 0;
        }
    }
}
