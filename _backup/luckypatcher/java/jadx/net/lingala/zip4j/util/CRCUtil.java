package net.lingala.zip4j.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.CRC32;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.progress.ProgressMonitor;

public class CRCUtil {
    private static final int BUF_SIZE = 16384;

    public static long computeFileCRC(String inputFile) throws ZipException {
        return computeFileCRC(inputFile, null);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long computeFileCRC(String inputFile, ProgressMonitor progressMonitor) throws ZipException {
        Throwable e;
        Throwable th;
        if (Zip4jUtil.isStringNotNullAndNotEmpty(inputFile)) {
            InputStream inputStream = null;
            try {
                Zip4jUtil.checkFileReadAccess(inputFile);
                InputStream inputStream2 = new FileInputStream(new File(inputFile));
                try {
                    byte[] buff = new byte[BUF_SIZE];
                    CRC32 crc32 = new CRC32();
                    while (true) {
                        int readLen = inputStream2.read(buff);
                        if (readLen == -1) {
                            break;
                        }
                        crc32.update(buff, 0, readLen);
                        if (progressMonitor != null) {
                            progressMonitor.updateWorkCompleted((long) readLen);
                            if (progressMonitor.isCancelAllTasks()) {
                                break;
                            }
                        }
                        return r6;
                    }
                    progressMonitor.setResult(3);
                    progressMonitor.setState(0);
                    long j = 0;
                    if (inputStream2 != null) {
                        try {
                            inputStream2.close();
                        } catch (IOException e2) {
                            throw new ZipException("error while closing the file after calculating crc");
                        }
                    }
                    return j;
                } catch (IOException e3) {
                    e = e3;
                    inputStream = inputStream2;
                    try {
                        throw new ZipException(e);
                    } catch (Throwable th2) {
                        th = th2;
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e4) {
                                throw new ZipException("error while closing the file after calculating crc");
                            }
                        }
                        throw th;
                    }
                } catch (Exception e5) {
                    e = e5;
                    inputStream = inputStream2;
                    throw new ZipException(e);
                } catch (Throwable th3) {
                    th = th3;
                    inputStream = inputStream2;
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    throw th;
                }
            } catch (IOException e6) {
                e = e6;
                throw new ZipException(e);
            } catch (Exception e7) {
                e = e7;
                throw new ZipException(e);
            }
        }
        throw new ZipException("input file is null or empty, cannot calculate CRC for the file");
    }
}
