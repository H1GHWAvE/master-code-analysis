package pxb.android.axml;

import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import net.lingala.zip4j.util.InternalZipConstants;
import org.tukaani.xz.DeltaOptions;
import pxb.android.ResConst;
import pxb.android.StringItems;

public class AxmlParser implements ResConst {
    public static final int END_FILE = 7;
    public static final int END_NS = 5;
    public static final int END_TAG = 3;
    public static final int START_FILE = 1;
    public static final int START_NS = 4;
    public static final int START_TAG = 2;
    public static final int TEXT = 6;
    private int attributeCount;
    private IntBuffer attrs;
    private int classAttribute;
    private int fileSize;
    private int idAttribute;
    private ByteBuffer in;
    private int lineNumber;
    private int nameIdx;
    private int nsIdx;
    private int prefixIdx;
    private int[] resourceIds;
    private String[] strings;
    private int styleAttribute;
    private int textIdx;

    public AxmlParser(byte[] data) {
        this(ByteBuffer.wrap(data));
    }

    public AxmlParser(ByteBuffer in) {
        this.fileSize = -1;
        this.in = in.order(ByteOrder.LITTLE_ENDIAN);
    }

    public int getAttrCount() {
        return this.attributeCount;
    }

    public int getAttributeCount() {
        return this.attributeCount;
    }

    public String getAttrName(int i) {
        return this.strings[this.attrs.get((i * END_NS) + START_FILE)];
    }

    public String getAttrNs(int i) {
        int idx = this.attrs.get((i * END_NS) + 0);
        return idx >= 0 ? this.strings[idx] : null;
    }

    String getAttrRawString(int i) {
        int idx = this.attrs.get((i * END_NS) + START_TAG);
        if (idx >= 0) {
            return this.strings[idx];
        }
        return null;
    }

    public int getAttrResId(int i) {
        if (this.resourceIds != null) {
            int idx = this.attrs.get((i * END_NS) + START_FILE);
            if (idx >= 0 && idx < this.resourceIds.length) {
                return this.resourceIds[idx];
            }
        }
        return -1;
    }

    public int getAttrType(int i) {
        return this.attrs.get((i * END_NS) + END_TAG) >> 24;
    }

    public Object getAttrValue(int i) {
        int v = this.attrs.get((i * END_NS) + START_NS);
        if (i == this.idAttribute) {
            return ValueWrapper.wrapId(v, getAttrRawString(i));
        }
        if (i == this.styleAttribute) {
            return ValueWrapper.wrapStyle(v, getAttrRawString(i));
        }
        if (i == this.classAttribute) {
            return ValueWrapper.wrapClass(v, getAttrRawString(i));
        }
        switch (getAttrType(i)) {
            case END_TAG /*3*/:
                return this.strings[v];
            case listAppsFragment.Create_ADS_PATCH_CONTEXT_DIALOG /*18*/:
                return Boolean.valueOf(v != 0);
            default:
                return Integer.valueOf(v);
        }
    }

    public int getLineNumber() {
        return this.lineNumber;
    }

    public String getName() {
        return this.strings[this.nameIdx];
    }

    public String getNamespacePrefix() {
        return this.strings[this.prefixIdx];
    }

    public String getNamespaceUri() {
        return this.nsIdx >= 0 ? this.strings[this.nsIdx] : null;
    }

    public String getText() {
        return this.strings[this.textIdx];
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int next() throws IOException {
        if (this.fileSize >= 0) {
            int p = this.in.position();
            while (p < this.fileSize) {
                int event;
                int type = this.in.getInt() & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH;
                int size = this.in.getInt();
                switch (type) {
                    case START_FILE /*1*/:
                        this.strings = StringItems.read(this.in);
                        this.in.position(p + size);
                        continue;
                    case DeltaOptions.DISTANCE_MAX /*256*/:
                        this.lineNumber = this.in.getInt();
                        this.in.getInt();
                        this.prefixIdx = this.in.getInt();
                        this.nsIdx = this.in.getInt();
                        event = START_NS;
                        break;
                    case ResConst.RES_XML_END_NAMESPACE_TYPE /*257*/:
                        this.in.position(p + size);
                        event = END_NS;
                        break;
                    case ResConst.RES_XML_START_ELEMENT_TYPE /*258*/:
                        this.lineNumber = this.in.getInt();
                        this.in.getInt();
                        this.nsIdx = this.in.getInt();
                        this.nameIdx = this.in.getInt();
                        if (this.in.getInt() == 1310740) {
                            this.attributeCount = this.in.getShort() & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH;
                            this.idAttribute = (this.in.getShort() & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH) - 1;
                            this.classAttribute = (this.in.getShort() & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH) - 1;
                            this.styleAttribute = (this.in.getShort() & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH) - 1;
                            this.attrs = this.in.asIntBuffer();
                            event = START_TAG;
                            break;
                        }
                        throw new RuntimeException();
                    case ResConst.RES_XML_END_ELEMENT_TYPE /*259*/:
                        this.in.position(p + size);
                        event = END_TAG;
                        break;
                    case ResConst.RES_XML_CDATA_TYPE /*260*/:
                        this.lineNumber = this.in.getInt();
                        this.in.getInt();
                        this.textIdx = this.in.getInt();
                        this.in.getInt();
                        this.in.getInt();
                        event = TEXT;
                        break;
                    case ResConst.RES_XML_RESOURCE_MAP_TYPE /*384*/:
                        int count = (size / START_NS) - 2;
                        this.resourceIds = new int[count];
                        for (int i = 0; i < count; i += START_FILE) {
                            this.resourceIds[i] = this.in.getInt();
                        }
                        this.in.position(p + size);
                        continue;
                    default:
                        throw new RuntimeException();
                }
                this.in.position(p + size);
                return event;
            }
            return END_FILE;
        } else if ((this.in.getInt() & InternalZipConstants.MAX_ALLOWED_ZIP_COMMENT_LENGTH) != END_TAG) {
            throw new RuntimeException();
        } else {
            this.fileSize = this.in.getInt();
            return START_FILE;
        }
    }
}
