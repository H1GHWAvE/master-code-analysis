package kellinwood.logging;

import java.io.PrintStream;

public class StreamLogger
  extends AbstractLogger
{
  PrintStream out;
  
  public StreamLogger(String paramString, PrintStream paramPrintStream)
  {
    super(paramString);
    this.out = paramPrintStream;
  }
  
  protected void write(String paramString1, String paramString2, Throwable paramThrowable)
  {
    this.out.print(format(paramString1, paramString2));
    if (paramThrowable != null) {
      paramThrowable.printStackTrace(this.out);
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/logging/StreamLogger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */