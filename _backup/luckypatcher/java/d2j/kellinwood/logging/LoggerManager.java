package kellinwood.logging;

import java.util.Map;
import java.util.TreeMap;

public class LoggerManager
{
  static LoggerFactory factory = new NullLoggerFactory();
  static Map<String, LoggerInterface> loggers = new TreeMap();
  
  public static LoggerInterface getLogger(String paramString)
  {
    LoggerInterface localLoggerInterface2 = (LoggerInterface)loggers.get(paramString);
    LoggerInterface localLoggerInterface1 = localLoggerInterface2;
    if (localLoggerInterface2 == null)
    {
      localLoggerInterface1 = factory.getLogger(paramString);
      loggers.put(paramString, localLoggerInterface1);
    }
    return localLoggerInterface1;
  }
  
  public static void setLoggerFactory(LoggerFactory paramLoggerFactory)
  {
    factory = paramLoggerFactory;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/logging/LoggerManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */