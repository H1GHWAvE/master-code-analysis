package kellinwood.logging;

public class ConsoleLoggerFactory
  implements LoggerFactory
{
  public LoggerInterface getLogger(String paramString)
  {
    return new StreamLogger(paramString, System.out);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/logging/ConsoleLoggerFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */