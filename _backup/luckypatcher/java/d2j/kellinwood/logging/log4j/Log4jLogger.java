package kellinwood.logging.log4j;

import kellinwood.logging.LoggerInterface;
import org.apache.log4j.Logger;

public class Log4jLogger
  implements LoggerInterface
{
  Logger log;
  
  public Log4jLogger(String paramString)
  {
    this.log = Logger.getLogger(paramString);
  }
  
  public void debug(String paramString)
  {
    this.log.debug(paramString);
  }
  
  public void debug(String paramString, Throwable paramThrowable)
  {
    this.log.debug(paramString, paramThrowable);
  }
  
  public void error(String paramString)
  {
    this.log.error(paramString);
  }
  
  public void error(String paramString, Throwable paramThrowable)
  {
    this.log.error(paramString, paramThrowable);
  }
  
  public void info(String paramString)
  {
    this.log.info(paramString);
  }
  
  public void info(String paramString, Throwable paramThrowable)
  {
    this.log.info(paramString, paramThrowable);
  }
  
  public boolean isDebugEnabled()
  {
    return this.log.isDebugEnabled();
  }
  
  public boolean isErrorEnabled()
  {
    return true;
  }
  
  public boolean isInfoEnabled()
  {
    return this.log.isInfoEnabled();
  }
  
  public boolean isWarningEnabled()
  {
    return true;
  }
  
  public void warning(String paramString)
  {
    this.log.warn(paramString);
  }
  
  public void warning(String paramString, Throwable paramThrowable)
  {
    this.log.warn(paramString, paramThrowable);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/logging/log4j/Log4jLogger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */