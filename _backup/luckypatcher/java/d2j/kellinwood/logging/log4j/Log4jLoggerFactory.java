package kellinwood.logging.log4j;

import kellinwood.logging.LoggerFactory;
import kellinwood.logging.LoggerInterface;

public class Log4jLoggerFactory
  implements LoggerFactory
{
  public LoggerInterface getLogger(String paramString)
  {
    return new Log4jLogger(paramString);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/logging/log4j/Log4jLoggerFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */