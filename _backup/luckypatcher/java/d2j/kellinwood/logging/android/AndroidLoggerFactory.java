package kellinwood.logging.android;

import kellinwood.logging.LoggerFactory;
import kellinwood.logging.LoggerInterface;

public class AndroidLoggerFactory
  implements LoggerFactory
{
  public LoggerInterface getLogger(String paramString)
  {
    return new AndroidLogger(paramString);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/logging/android/AndroidLoggerFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */