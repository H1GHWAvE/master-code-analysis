package kellinwood.logging.android;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import kellinwood.logging.AbstractLogger;

public class AndroidLogger
  extends AbstractLogger
{
  boolean isDebugToastEnabled = false;
  boolean isErrorToastEnabled = true;
  boolean isInfoToastEnabled = false;
  boolean isWarningToastEnabled = true;
  Context toastContext;
  
  public AndroidLogger(String paramString)
  {
    super(paramString);
    int i = this.category.lastIndexOf('.');
    if (i > 0) {
      this.category = this.category.substring(i + 1);
    }
  }
  
  public void debugLO(String paramString, Throwable paramThrowable)
  {
    boolean bool = this.isDebugToastEnabled;
    this.isDebugToastEnabled = false;
    writeFixNullMessage("DEBUG", paramString, paramThrowable);
    this.isDebugToastEnabled = bool;
  }
  
  public void errorLO(String paramString, Throwable paramThrowable)
  {
    boolean bool = this.isErrorToastEnabled;
    this.isErrorToastEnabled = false;
    writeFixNullMessage("ERROR", paramString, paramThrowable);
    this.isErrorToastEnabled = bool;
  }
  
  public Context getToastContext()
  {
    return this.toastContext;
  }
  
  public void infoLO(String paramString, Throwable paramThrowable)
  {
    boolean bool = this.isInfoToastEnabled;
    this.isInfoToastEnabled = false;
    writeFixNullMessage("INFO", paramString, paramThrowable);
    this.isInfoToastEnabled = bool;
  }
  
  public boolean isDebugEnabled()
  {
    return Log.isLoggable(this.category, 3);
  }
  
  public boolean isDebugToastEnabled()
  {
    return this.isDebugToastEnabled;
  }
  
  public boolean isErrorEnabled()
  {
    return Log.isLoggable(this.category, 6);
  }
  
  public boolean isErrorToastEnabled()
  {
    return this.isErrorToastEnabled;
  }
  
  public boolean isInfoEnabled()
  {
    return Log.isLoggable(this.category, 4);
  }
  
  public boolean isInfoToastEnabled()
  {
    return this.isInfoToastEnabled;
  }
  
  public boolean isWarningEnabled()
  {
    return Log.isLoggable(this.category, 5);
  }
  
  public boolean isWarningToastEnabled()
  {
    return this.isWarningToastEnabled;
  }
  
  public void setDebugToastEnabled(boolean paramBoolean)
  {
    this.isDebugToastEnabled = paramBoolean;
  }
  
  public void setErrorToastEnabled(boolean paramBoolean)
  {
    this.isErrorToastEnabled = paramBoolean;
  }
  
  public void setInfoToastEnabled(boolean paramBoolean)
  {
    this.isInfoToastEnabled = paramBoolean;
  }
  
  public void setToastContext(Context paramContext)
  {
    this.toastContext = paramContext;
  }
  
  public void setWarningToastEnabled(boolean paramBoolean)
  {
    this.isWarningToastEnabled = paramBoolean;
  }
  
  protected void toast(String paramString)
  {
    try
    {
      if (this.toastContext != null) {
        Toast.makeText(this.toastContext, paramString, 1).show();
      }
      return;
    }
    catch (Throwable localThrowable)
    {
      Log.e(this.category, paramString, localThrowable);
    }
  }
  
  public void warningLO(String paramString, Throwable paramThrowable)
  {
    boolean bool = this.isWarningToastEnabled;
    this.isWarningToastEnabled = false;
    writeFixNullMessage("WARNING", paramString, paramThrowable);
    this.isWarningToastEnabled = bool;
  }
  
  public void write(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if ("ERROR".equals(paramString1)) {
      if (paramThrowable != null)
      {
        Log.e(this.category, paramString2, paramThrowable);
        if (this.isErrorToastEnabled) {
          toast(paramString2);
        }
      }
    }
    for (;;)
    {
      return;
      Log.e(this.category, paramString2);
      break;
      if ("DEBUG".equals(paramString1))
      {
        if (paramThrowable != null) {
          Log.d(this.category, paramString2, paramThrowable);
        }
        while (this.isDebugToastEnabled)
        {
          toast(paramString2);
          return;
          Log.d(this.category, paramString2);
        }
      }
      else if ("WARNING".equals(paramString1))
      {
        if (paramThrowable != null) {
          Log.w(this.category, paramString2, paramThrowable);
        }
        while (this.isWarningToastEnabled)
        {
          toast(paramString2);
          return;
          Log.w(this.category, paramString2);
        }
      }
      else if ("INFO".equals(paramString1))
      {
        if (paramThrowable != null) {
          Log.i(this.category, paramString2, paramThrowable);
        }
        while (this.isInfoToastEnabled)
        {
          toast(paramString2);
          return;
          Log.i(this.category, paramString2);
        }
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/logging/android/AndroidLogger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */