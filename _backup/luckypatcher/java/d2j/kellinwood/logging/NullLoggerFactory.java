package kellinwood.logging;

public class NullLoggerFactory
  implements LoggerFactory
{
  static LoggerInterface logger = new LoggerInterface()
  {
    public void debug(String paramAnonymousString) {}
    
    public void debug(String paramAnonymousString, Throwable paramAnonymousThrowable) {}
    
    public void error(String paramAnonymousString) {}
    
    public void error(String paramAnonymousString, Throwable paramAnonymousThrowable) {}
    
    public void info(String paramAnonymousString) {}
    
    public void info(String paramAnonymousString, Throwable paramAnonymousThrowable) {}
    
    public boolean isDebugEnabled()
    {
      return false;
    }
    
    public boolean isErrorEnabled()
    {
      return false;
    }
    
    public boolean isInfoEnabled()
    {
      return false;
    }
    
    public boolean isWarningEnabled()
    {
      return false;
    }
    
    public void warning(String paramAnonymousString) {}
    
    public void warning(String paramAnonymousString, Throwable paramAnonymousThrowable) {}
  };
  
  public LoggerInterface getLogger(String paramString)
  {
    return logger;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/logging/NullLoggerFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */