package kellinwood.logging;

public abstract interface LoggerFactory
{
  public abstract LoggerInterface getLogger(String paramString);
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/logging/LoggerFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */