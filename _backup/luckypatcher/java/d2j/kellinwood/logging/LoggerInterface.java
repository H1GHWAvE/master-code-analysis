package kellinwood.logging;

public abstract interface LoggerInterface
{
  public static final String DEBUG = "DEBUG";
  public static final String ERROR = "ERROR";
  public static final String INFO = "INFO";
  public static final String WARNING = "WARNING";
  
  public abstract void debug(String paramString);
  
  public abstract void debug(String paramString, Throwable paramThrowable);
  
  public abstract void error(String paramString);
  
  public abstract void error(String paramString, Throwable paramThrowable);
  
  public abstract void info(String paramString);
  
  public abstract void info(String paramString, Throwable paramThrowable);
  
  public abstract boolean isDebugEnabled();
  
  public abstract boolean isErrorEnabled();
  
  public abstract boolean isInfoEnabled();
  
  public abstract boolean isWarningEnabled();
  
  public abstract void warning(String paramString);
  
  public abstract void warning(String paramString, Throwable paramThrowable);
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/logging/LoggerInterface.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */