package kellinwood.logging;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class AbstractLogger
  implements LoggerInterface
{
  protected String category;
  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
  
  public AbstractLogger(String paramString)
  {
    this.category = paramString;
  }
  
  public void debug(String paramString)
  {
    writeFixNullMessage("DEBUG", paramString, null);
  }
  
  public void debug(String paramString, Throwable paramThrowable)
  {
    writeFixNullMessage("DEBUG", paramString, paramThrowable);
  }
  
  public void error(String paramString)
  {
    writeFixNullMessage("ERROR", paramString, null);
  }
  
  public void error(String paramString, Throwable paramThrowable)
  {
    writeFixNullMessage("ERROR", paramString, paramThrowable);
  }
  
  protected String format(String paramString1, String paramString2)
  {
    return String.format("%s %s %s: %s\n", new Object[] { this.dateFormat.format(new Date()), paramString1, this.category, paramString2 });
  }
  
  public void info(String paramString)
  {
    writeFixNullMessage("INFO", paramString, null);
  }
  
  public void info(String paramString, Throwable paramThrowable)
  {
    writeFixNullMessage("INFO", paramString, paramThrowable);
  }
  
  public boolean isDebugEnabled()
  {
    return true;
  }
  
  public boolean isErrorEnabled()
  {
    return true;
  }
  
  public boolean isInfoEnabled()
  {
    return true;
  }
  
  public boolean isWarningEnabled()
  {
    return true;
  }
  
  public void warning(String paramString)
  {
    writeFixNullMessage("WARNING", paramString, null);
  }
  
  public void warning(String paramString, Throwable paramThrowable)
  {
    writeFixNullMessage("WARNING", paramString, paramThrowable);
  }
  
  protected abstract void write(String paramString1, String paramString2, Throwable paramThrowable);
  
  protected void writeFixNullMessage(String paramString1, String paramString2, Throwable paramThrowable)
  {
    String str = paramString2;
    if (paramString2 == null) {
      if (paramThrowable == null) {
        break label29;
      }
    }
    label29:
    for (str = paramThrowable.getClass().getName();; str = "null")
    {
      write(paramString1, str, paramThrowable);
      return;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/logging/AbstractLogger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */