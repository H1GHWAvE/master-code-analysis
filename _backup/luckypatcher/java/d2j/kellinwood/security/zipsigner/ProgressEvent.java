package kellinwood.security.zipsigner;

public class ProgressEvent
{
  public static final int PRORITY_IMPORTANT = 1;
  public static final int PRORITY_NORMAL = 0;
  private String message;
  private int percentDone;
  private int priority;
  
  public String getMessage()
  {
    return this.message;
  }
  
  public int getPercentDone()
  {
    return this.percentDone;
  }
  
  public int getPriority()
  {
    return this.priority;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
  
  public void setPercentDone(int paramInt)
  {
    this.percentDone = paramInt;
  }
  
  public void setPriority(int paramInt)
  {
    this.priority = paramInt;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/ProgressEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */