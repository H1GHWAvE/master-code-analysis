package kellinwood.security.zipsigner;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Method;
import kellinwood.logging.LoggerInterface;

public class Base64
{
  static Method aDecodeMethod;
  static Method aEncodeMethod = null;
  static Method bDecodeMethod;
  static Object bDecoder;
  static Method bEncodeMethod;
  static Object bEncoder;
  static LoggerInterface logger;
  
  /* Error */
  static
  {
    // Byte code:
    //   0: aconst_null
    //   1: putstatic 22	kellinwood/security/zipsigner/Base64:aEncodeMethod	Ljava/lang/reflect/Method;
    //   4: aconst_null
    //   5: putstatic 24	kellinwood/security/zipsigner/Base64:aDecodeMethod	Ljava/lang/reflect/Method;
    //   8: aconst_null
    //   9: putstatic 26	kellinwood/security/zipsigner/Base64:bEncoder	Ljava/lang/Object;
    //   12: aconst_null
    //   13: putstatic 28	kellinwood/security/zipsigner/Base64:bEncodeMethod	Ljava/lang/reflect/Method;
    //   16: aconst_null
    //   17: putstatic 30	kellinwood/security/zipsigner/Base64:bDecoder	Ljava/lang/Object;
    //   20: aconst_null
    //   21: putstatic 32	kellinwood/security/zipsigner/Base64:bDecodeMethod	Ljava/lang/reflect/Method;
    //   24: aconst_null
    //   25: putstatic 34	kellinwood/security/zipsigner/Base64:logger	Lkellinwood/logging/LoggerInterface;
    //   28: ldc 2
    //   30: invokevirtual 40	java/lang/Class:getName	()Ljava/lang/String;
    //   33: invokestatic 46	kellinwood/logging/LoggerManager:getLogger	(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
    //   36: putstatic 34	kellinwood/security/zipsigner/Base64:logger	Lkellinwood/logging/LoggerInterface;
    //   39: ldc 48
    //   41: invokestatic 52	java/lang/Class:forName	(Ljava/lang/String;)Ljava/lang/Class;
    //   44: astore_0
    //   45: aload_0
    //   46: ldc 54
    //   48: iconst_2
    //   49: anewarray 36	java/lang/Class
    //   52: dup
    //   53: iconst_0
    //   54: ldc 56
    //   56: aastore
    //   57: dup
    //   58: iconst_1
    //   59: getstatic 62	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   62: aastore
    //   63: invokevirtual 66	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   66: putstatic 22	kellinwood/security/zipsigner/Base64:aEncodeMethod	Ljava/lang/reflect/Method;
    //   69: aload_0
    //   70: ldc 68
    //   72: iconst_2
    //   73: anewarray 36	java/lang/Class
    //   76: dup
    //   77: iconst_0
    //   78: ldc 56
    //   80: aastore
    //   81: dup
    //   82: iconst_1
    //   83: getstatic 62	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   86: aastore
    //   87: invokevirtual 66	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   90: putstatic 24	kellinwood/security/zipsigner/Base64:aDecodeMethod	Ljava/lang/reflect/Method;
    //   93: getstatic 34	kellinwood/security/zipsigner/Base64:logger	Lkellinwood/logging/LoggerInterface;
    //   96: new 70	java/lang/StringBuilder
    //   99: dup
    //   100: invokespecial 73	java/lang/StringBuilder:<init>	()V
    //   103: aload_0
    //   104: invokevirtual 40	java/lang/Class:getName	()Ljava/lang/String;
    //   107: invokevirtual 77	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: ldc 79
    //   112: invokevirtual 77	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   115: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   118: invokeinterface 88 2 0
    //   123: ldc 90
    //   125: invokestatic 52	java/lang/Class:forName	(Ljava/lang/String;)Ljava/lang/Class;
    //   128: astore_0
    //   129: aload_0
    //   130: invokevirtual 94	java/lang/Class:newInstance	()Ljava/lang/Object;
    //   133: putstatic 26	kellinwood/security/zipsigner/Base64:bEncoder	Ljava/lang/Object;
    //   136: aload_0
    //   137: ldc 54
    //   139: iconst_4
    //   140: anewarray 36	java/lang/Class
    //   143: dup
    //   144: iconst_0
    //   145: ldc 56
    //   147: aastore
    //   148: dup
    //   149: iconst_1
    //   150: getstatic 62	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   153: aastore
    //   154: dup
    //   155: iconst_2
    //   156: getstatic 62	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   159: aastore
    //   160: dup
    //   161: iconst_3
    //   162: ldc 96
    //   164: aastore
    //   165: invokevirtual 66	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   168: putstatic 28	kellinwood/security/zipsigner/Base64:bEncodeMethod	Ljava/lang/reflect/Method;
    //   171: getstatic 34	kellinwood/security/zipsigner/Base64:logger	Lkellinwood/logging/LoggerInterface;
    //   174: new 70	java/lang/StringBuilder
    //   177: dup
    //   178: invokespecial 73	java/lang/StringBuilder:<init>	()V
    //   181: aload_0
    //   182: invokevirtual 40	java/lang/Class:getName	()Ljava/lang/String;
    //   185: invokevirtual 77	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   188: ldc 79
    //   190: invokevirtual 77	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   193: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   196: invokeinterface 88 2 0
    //   201: aload_0
    //   202: ldc 68
    //   204: iconst_4
    //   205: anewarray 36	java/lang/Class
    //   208: dup
    //   209: iconst_0
    //   210: ldc 56
    //   212: aastore
    //   213: dup
    //   214: iconst_1
    //   215: getstatic 62	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   218: aastore
    //   219: dup
    //   220: iconst_2
    //   221: getstatic 62	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   224: aastore
    //   225: dup
    //   226: iconst_3
    //   227: ldc 96
    //   229: aastore
    //   230: invokevirtual 66	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   233: putstatic 32	kellinwood/security/zipsigner/Base64:bDecodeMethod	Ljava/lang/reflect/Method;
    //   236: getstatic 22	kellinwood/security/zipsigner/Base64:aEncodeMethod	Ljava/lang/reflect/Method;
    //   239: ifnonnull +49 -> 288
    //   242: getstatic 28	kellinwood/security/zipsigner/Base64:bEncodeMethod	Ljava/lang/reflect/Method;
    //   245: ifnonnull +43 -> 288
    //   248: new 98	java/lang/IllegalStateException
    //   251: dup
    //   252: ldc 100
    //   254: invokespecial 102	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   257: athrow
    //   258: astore_0
    //   259: getstatic 34	kellinwood/security/zipsigner/Base64:logger	Lkellinwood/logging/LoggerInterface;
    //   262: ldc 104
    //   264: aload_0
    //   265: invokeinterface 108 3 0
    //   270: goto -147 -> 123
    //   273: astore_0
    //   274: getstatic 34	kellinwood/security/zipsigner/Base64:logger	Lkellinwood/logging/LoggerInterface;
    //   277: ldc 110
    //   279: aload_0
    //   280: invokeinterface 108 3 0
    //   285: goto -49 -> 236
    //   288: return
    //   289: astore_0
    //   290: goto -54 -> 236
    //   293: astore_0
    //   294: goto -171 -> 123
    // Local variable table:
    //   start	length	slot	name	signature
    //   44	158	0	localClass	Class
    //   258	7	0	localException1	Exception
    //   273	7	0	localException2	Exception
    //   289	1	0	localClassNotFoundException1	ClassNotFoundException
    //   293	1	0	localClassNotFoundException2	ClassNotFoundException
    // Exception table:
    //   from	to	target	type
    //   39	123	258	java/lang/Exception
    //   123	236	273	java/lang/Exception
    //   123	236	289	java/lang/ClassNotFoundException
    //   39	123	293	java/lang/ClassNotFoundException
  }
  
  public static byte[] decode(byte[] paramArrayOfByte)
  {
    try
    {
      if (aDecodeMethod != null) {
        return (byte[])aDecodeMethod.invoke(null, new Object[] { paramArrayOfByte, Integer.valueOf(2) });
      }
      if (bDecodeMethod != null)
      {
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        bDecodeMethod.invoke(bEncoder, new Object[] { paramArrayOfByte, Integer.valueOf(0), Integer.valueOf(paramArrayOfByte.length), localByteArrayOutputStream });
        paramArrayOfByte = localByteArrayOutputStream.toByteArray();
        return paramArrayOfByte;
      }
    }
    catch (Exception paramArrayOfByte)
    {
      throw new IllegalStateException(paramArrayOfByte.getClass().getName() + ": " + paramArrayOfByte.getMessage());
    }
    throw new IllegalStateException("No base64 encoder implementation is available.");
  }
  
  public static String encode(byte[] paramArrayOfByte)
  {
    try
    {
      if (aEncodeMethod != null) {
        return new String((byte[])aEncodeMethod.invoke(null, new Object[] { paramArrayOfByte, Integer.valueOf(2) }));
      }
      if (bEncodeMethod != null)
      {
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        bEncodeMethod.invoke(bEncoder, new Object[] { paramArrayOfByte, Integer.valueOf(0), Integer.valueOf(paramArrayOfByte.length), localByteArrayOutputStream });
        paramArrayOfByte = new String(localByteArrayOutputStream.toByteArray());
        return paramArrayOfByte;
      }
    }
    catch (Exception paramArrayOfByte)
    {
      throw new IllegalStateException(paramArrayOfByte.getClass().getName() + ": " + paramArrayOfByte.getMessage());
    }
    throw new IllegalStateException("No base64 encoder implementation is available.");
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/Base64.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */