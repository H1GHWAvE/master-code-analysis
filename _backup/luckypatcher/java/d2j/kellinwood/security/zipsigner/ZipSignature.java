package kellinwood.security.zipsigner;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.PrivateKey;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

public class ZipSignature
{
  byte[] afterAlgorithmIdBytes = { 4, 20 };
  byte[] algorithmIdBytes = { 48, 9, 6, 5, 43, 14, 3, 2, 26, 5, 0 };
  byte[] beforeAlgorithmIdBytes = { 48, 33 };
  Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
  MessageDigest md = MessageDigest.getInstance("SHA1");
  
  public ZipSignature()
    throws IOException, GeneralSecurityException
  {}
  
  public void initSign(PrivateKey paramPrivateKey)
    throws InvalidKeyException
  {
    this.cipher.init(1, paramPrivateKey);
  }
  
  public byte[] sign()
    throws BadPaddingException, IllegalBlockSizeException
  {
    this.cipher.update(this.beforeAlgorithmIdBytes);
    this.cipher.update(this.algorithmIdBytes);
    this.cipher.update(this.afterAlgorithmIdBytes);
    this.cipher.update(this.md.digest());
    return this.cipher.doFinal();
  }
  
  public void update(byte[] paramArrayOfByte)
  {
    this.md.update(paramArrayOfByte);
  }
  
  public void update(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    this.md.update(paramArrayOfByte, paramInt1, paramInt2);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/ZipSignature.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */