package kellinwood.security.zipsigner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class HexDumpEncoder
{
  static HexEncoder encoder = new HexEncoder();
  
  public static String encode(byte[] paramArrayOfByte)
  {
    for (;;)
    {
      StringBuilder localStringBuilder1;
      int i;
      StringBuilder localStringBuilder2;
      StringBuilder localStringBuilder3;
      try
      {
        Object localObject = new ByteArrayOutputStream();
        encoder.encode(paramArrayOfByte, 0, paramArrayOfByte.length, (OutputStream)localObject);
        localObject = ((ByteArrayOutputStream)localObject).toByteArray();
        localStringBuilder1 = new StringBuilder();
        i = 0;
        if (i >= localObject.length) {
          break label311;
        }
        int k = Math.min(i + 32, localObject.length);
        localStringBuilder2 = new StringBuilder();
        localStringBuilder3 = new StringBuilder();
        localStringBuilder2.append(String.format("%08x: ", new Object[] { Integer.valueOf(i / 2) }));
        j = i;
        if (j < k)
        {
          localStringBuilder2.append(Character.valueOf((char)localObject[j]));
          localStringBuilder2.append(Character.valueOf((char)localObject[(j + 1)]));
          if ((j + 2) % 4 == 0) {
            localStringBuilder2.append(' ');
          }
          int m = paramArrayOfByte[(j / 2)];
          if ((m >= 32) && (m < 127)) {
            localStringBuilder3.append(Character.valueOf((char)m));
          } else {
            localStringBuilder3.append('.');
          }
        }
      }
      catch (IOException paramArrayOfByte)
      {
        throw new IllegalStateException(paramArrayOfByte.getClass().getName() + ": " + paramArrayOfByte.getMessage());
      }
      localStringBuilder1.append(localStringBuilder2.toString());
      int j = localStringBuilder2.length();
      while (j < 50)
      {
        localStringBuilder1.append(' ');
        j += 1;
      }
      localStringBuilder1.append("  ");
      localStringBuilder1.append(localStringBuilder3);
      localStringBuilder1.append("\n");
      i += 32;
      continue;
      label311:
      paramArrayOfByte = localStringBuilder1.toString();
      return paramArrayOfByte;
      j += 2;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/HexDumpEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */