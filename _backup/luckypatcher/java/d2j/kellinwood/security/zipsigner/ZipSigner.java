package kellinwood.security.zipsigner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.security.DigestOutputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.TreeMap;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.Cipher;
import kellinwood.logging.LoggerInterface;
import kellinwood.logging.LoggerManager;
import kellinwood.zipio.ZioEntry;
import kellinwood.zipio.ZipInput;
import kellinwood.zipio.ZipOutput;

public class ZipSigner
{
  private static final String CERT_RSA_NAME = "META-INF/CERT.RSA";
  private static final String CERT_SF_NAME = "META-INF/CERT.SF";
  public static final String KEY_NONE = "none";
  public static final String KEY_TESTKEY = "testkey";
  public static final String MODE_AUTO = "auto";
  public static final String MODE_AUTO_NONE = "auto-none";
  public static final String MODE_AUTO_TESTKEY = "auto-testkey";
  public static final String[] SUPPORTED_KEY_MODES = { "auto-testkey", "auto", "auto-none", "media", "platform", "shared", "testkey", "none" };
  static LoggerInterface log = null;
  private static Pattern stripPattern = Pattern.compile("^META-INF/(.*)[.](SF|RSA|DSA)$");
  Map<String, String> autoKeyDetect = new HashMap();
  AutoKeyObservable autoKeyObservable = new AutoKeyObservable();
  private boolean canceled = false;
  KeySet keySet = null;
  String keymode = "testkey";
  Map<String, KeySet> loadedKeys = new HashMap();
  private ProgressHelper progressHelper = new ProgressHelper();
  private ResourceAdapter resourceAdapter = new DefaultResourceAdapter();
  
  public ZipSigner()
    throws ClassNotFoundException, IllegalAccessException, InstantiationException
  {
    this.autoKeyDetect.put("aa9852bc5a53272ac8031d49b65e4b0e", "media");
    this.autoKeyDetect.put("e60418c4b638f20d0721e115674ca11f", "platform");
    this.autoKeyDetect.put("3e24e49741b60c215c010dc6048fca7d", "shared");
    this.autoKeyDetect.put("dab2cead827ef5313f28e22b6fa8479f", "testkey");
  }
  
  private Manifest addDigestsToManifest(Map<String, ZioEntry> paramMap)
    throws IOException, GeneralSecurityException
  {
    Manifest localManifest1 = null;
    Object localObject = (ZioEntry)paramMap.get("META-INF/MANIFEST.MF");
    if (localObject != null)
    {
      localManifest1 = new Manifest();
      localManifest1.read(((ZioEntry)localObject).getInputStream());
    }
    Manifest localManifest2 = new Manifest();
    localObject = localManifest2.getMainAttributes();
    MessageDigest localMessageDigest;
    byte[] arrayOfByte;
    boolean bool;
    Iterator localIterator;
    if (localManifest1 != null)
    {
      ((Attributes)localObject).putAll(localManifest1.getMainAttributes());
      localMessageDigest = MessageDigest.getInstance("SHA1");
      arrayOfByte = new byte['Ȁ'];
      localObject = new TreeMap();
      ((TreeMap)localObject).putAll(paramMap);
      bool = getLogger().isDebugEnabled();
      if (bool) {
        getLogger().debug("Manifest entries:");
      }
      localIterator = ((TreeMap)localObject).values().iterator();
    }
    for (;;)
    {
      if (localIterator.hasNext())
      {
        paramMap = (ZioEntry)localIterator.next();
        if (!this.canceled) {}
      }
      else
      {
        return localManifest2;
        ((Attributes)localObject).putValue("Manifest-Version", "1.0");
        ((Attributes)localObject).putValue("Created-By", "1.0 (Android SignApk)");
        break;
      }
      String str = paramMap.getName();
      if (bool) {
        getLogger().debug(str);
      }
      if ((!paramMap.isDirectory()) && (!str.equals("META-INF/MANIFEST.MF")) && (!str.equals("META-INF/CERT.SF")) && (!str.equals("META-INF/CERT.RSA")) && ((stripPattern == null) || (!stripPattern.matcher(str).matches())))
      {
        this.progressHelper.progress(0, this.resourceAdapter.getString(ResourceAdapter.Item.GENERATING_MANIFEST, new Object[0]));
        paramMap = paramMap.getInputStream();
        for (;;)
        {
          int i = paramMap.read(arrayOfByte);
          if (i <= 0) {
            break;
          }
          localMessageDigest.update(arrayOfByte, 0, i);
        }
        localObject = null;
        paramMap = (Map<String, ZioEntry>)localObject;
        if (localManifest1 != null)
        {
          Attributes localAttributes = localManifest1.getAttributes(str);
          paramMap = (Map<String, ZioEntry>)localObject;
          if (localAttributes != null) {
            paramMap = new Attributes(localAttributes);
          }
        }
        localObject = paramMap;
        if (paramMap == null) {
          localObject = new Attributes();
        }
        ((Attributes)localObject).putValue("SHA1-Digest", Base64.encode(localMessageDigest.digest()));
        localManifest2.getEntries().put(str, localObject);
      }
    }
  }
  
  private void copyFiles(Map<String, ZioEntry> paramMap, ZipOutput paramZipOutput)
    throws IOException
  {
    int i = 1;
    Iterator localIterator = paramMap.values().iterator();
    for (;;)
    {
      ZioEntry localZioEntry;
      if (localIterator.hasNext())
      {
        localZioEntry = (ZioEntry)localIterator.next();
        if (!this.canceled) {}
      }
      else
      {
        return;
      }
      this.progressHelper.progress(0, this.resourceAdapter.getString(ResourceAdapter.Item.COPYING_ZIP_ENTRY, new Object[] { Integer.valueOf(i), Integer.valueOf(paramMap.size()) }));
      i += 1;
      paramZipOutput.write(localZioEntry);
    }
  }
  
  private void copyFiles(Manifest paramManifest, Map<String, ZioEntry> paramMap, ZipOutput paramZipOutput, long paramLong)
    throws IOException
  {
    paramManifest = new ArrayList(paramManifest.getEntries().keySet());
    Collections.sort(paramManifest);
    int i = 1;
    Iterator localIterator = paramManifest.iterator();
    for (;;)
    {
      if (localIterator.hasNext())
      {
        localObject = (String)localIterator.next();
        if (!this.canceled) {}
      }
      else
      {
        return;
      }
      this.progressHelper.progress(0, this.resourceAdapter.getString(ResourceAdapter.Item.COPYING_ZIP_ENTRY, new Object[] { Integer.valueOf(i), Integer.valueOf(paramManifest.size()) }));
      i += 1;
      Object localObject = (ZioEntry)paramMap.get(localObject);
      ((ZioEntry)localObject).setTime(paramLong);
      paramZipOutput.write((ZioEntry)localObject);
    }
  }
  
  /* Error */
  private java.security.spec.KeySpec decryptPrivateKey(byte[] paramArrayOfByte, String paramString)
    throws GeneralSecurityException
  {
    // Byte code:
    //   0: new 344	javax/crypto/EncryptedPrivateKeyInfo
    //   3: dup
    //   4: aload_1
    //   5: invokespecial 347	javax/crypto/EncryptedPrivateKeyInfo:<init>	([B)V
    //   8: astore_1
    //   9: aload_2
    //   10: invokevirtual 351	java/lang/String:toCharArray	()[C
    //   13: astore_2
    //   14: aload_1
    //   15: invokevirtual 354	javax/crypto/EncryptedPrivateKeyInfo:getAlgName	()Ljava/lang/String;
    //   18: invokestatic 359	javax/crypto/SecretKeyFactory:getInstance	(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;
    //   21: new 361	javax/crypto/spec/PBEKeySpec
    //   24: dup
    //   25: aload_2
    //   26: invokespecial 364	javax/crypto/spec/PBEKeySpec:<init>	([C)V
    //   29: invokevirtual 368	javax/crypto/SecretKeyFactory:generateSecret	(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;
    //   32: astore_2
    //   33: aload_1
    //   34: invokevirtual 354	javax/crypto/EncryptedPrivateKeyInfo:getAlgName	()Ljava/lang/String;
    //   37: invokestatic 373	javax/crypto/Cipher:getInstance	(Ljava/lang/String;)Ljavax/crypto/Cipher;
    //   40: astore_3
    //   41: aload_3
    //   42: iconst_2
    //   43: aload_2
    //   44: aload_1
    //   45: invokevirtual 377	javax/crypto/EncryptedPrivateKeyInfo:getAlgParameters	()Ljava/security/AlgorithmParameters;
    //   48: invokevirtual 381	javax/crypto/Cipher:init	(ILjava/security/Key;Ljava/security/AlgorithmParameters;)V
    //   51: aload_1
    //   52: aload_3
    //   53: invokevirtual 385	javax/crypto/EncryptedPrivateKeyInfo:getKeySpec	(Ljavax/crypto/Cipher;)Ljava/security/spec/PKCS8EncodedKeySpec;
    //   56: astore_1
    //   57: aload_1
    //   58: areturn
    //   59: astore_1
    //   60: aconst_null
    //   61: areturn
    //   62: astore_1
    //   63: invokestatic 177	kellinwood/security/zipsigner/ZipSigner:getLogger	()Lkellinwood/logging/LoggerInterface;
    //   66: ldc_w 387
    //   69: invokeinterface 390 2 0
    //   74: aload_1
    //   75: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	76	0	this	ZipSigner
    //   0	76	1	paramArrayOfByte	byte[]
    //   0	76	2	paramString	String
    //   40	13	3	localCipher	Cipher
    // Exception table:
    //   from	to	target	type
    //   0	9	59	java/io/IOException
    //   51	57	62	java/security/spec/InvalidKeySpecException
  }
  
  private void generateSignatureFile(Manifest paramManifest, OutputStream paramOutputStream)
    throws IOException, GeneralSecurityException
  {
    paramOutputStream.write("Signature-Version: 1.0\r\n".getBytes());
    paramOutputStream.write("Created-By: 1.0 (Android SignApk)\r\n".getBytes());
    MessageDigest localMessageDigest = MessageDigest.getInstance("SHA1");
    PrintStream localPrintStream = new PrintStream(new DigestOutputStream(new ByteArrayOutputStream(), localMessageDigest), true, "UTF-8");
    paramManifest.write(localPrintStream);
    localPrintStream.flush();
    paramOutputStream.write(("SHA1-Digest-Manifest: " + Base64.encode(localMessageDigest.digest()) + "\r\n\r\n").getBytes());
    paramManifest = paramManifest.getEntries().entrySet().iterator();
    for (;;)
    {
      if (paramManifest.hasNext())
      {
        localObject = (Map.Entry)paramManifest.next();
        if (!this.canceled) {}
      }
      else
      {
        return;
      }
      this.progressHelper.progress(0, this.resourceAdapter.getString(ResourceAdapter.Item.GENERATING_SIGNATURE_FILE, new Object[0]));
      String str = "Name: " + (String)((Map.Entry)localObject).getKey() + "\r\n";
      localPrintStream.print(str);
      Object localObject = ((Attributes)((Map.Entry)localObject).getValue()).entrySet().iterator();
      while (((Iterator)localObject).hasNext())
      {
        Map.Entry localEntry = (Map.Entry)((Iterator)localObject).next();
        localPrintStream.print(localEntry.getKey() + ": " + localEntry.getValue() + "\r\n");
      }
      localPrintStream.print("\r\n");
      localPrintStream.flush();
      paramOutputStream.write(str.getBytes());
      paramOutputStream.write(("SHA1-Digest: " + Base64.encode(localMessageDigest.digest()) + "\r\n\r\n").getBytes());
    }
  }
  
  public static LoggerInterface getLogger()
  {
    if (log == null) {
      log = LoggerManager.getLogger(ZipSigner.class.getName());
    }
    return log;
  }
  
  public static String[] getSupportedKeyModes()
  {
    return SUPPORTED_KEY_MODES;
  }
  
  private void writeSignatureBlock(KeySet paramKeySet, byte[] paramArrayOfByte, OutputStream paramOutputStream)
    throws IOException, GeneralSecurityException
  {
    if (paramKeySet.getSigBlockTemplate() != null)
    {
      Object localObject = new ZipSignature();
      ((ZipSignature)localObject).initSign(paramKeySet.getPrivateKey());
      ((ZipSignature)localObject).update(paramArrayOfByte);
      localObject = ((ZipSignature)localObject).sign();
      paramOutputStream.write(paramKeySet.getSigBlockTemplate());
      paramOutputStream.write((byte[])localObject);
      if (getLogger().isDebugEnabled())
      {
        paramOutputStream = MessageDigest.getInstance("SHA1");
        paramOutputStream.update(paramArrayOfByte);
        paramArrayOfByte = paramOutputStream.digest();
        getLogger().debug("Sig File SHA1: \n" + HexDumpEncoder.encode(paramArrayOfByte));
        getLogger().debug("Signature: \n" + HexDumpEncoder.encode((byte[])localObject));
        paramArrayOfByte = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        paramArrayOfByte.init(2, paramKeySet.getPublicKey());
        paramKeySet = paramArrayOfByte.doFinal((byte[])localObject);
        getLogger().debug("Signature Decrypted: \n" + HexDumpEncoder.encode(paramKeySet));
      }
      return;
    }
    try
    {
      paramOutputStream.write((byte[])Class.forName("kellinwood.security.zipsigner.optional.SignatureBlockGenerator").getMethod("generate", new Class[] { KeySet.class, new byte[1].getClass() }).invoke(null, new Object[] { paramKeySet, paramArrayOfByte }));
      return;
    }
    catch (Exception paramKeySet)
    {
      throw new RuntimeException(paramKeySet.getMessage(), paramKeySet);
    }
  }
  
  public void addAutoKeyObserver(Observer paramObserver)
  {
    this.autoKeyObservable.addObserver(paramObserver);
  }
  
  public void addProgressListener(ProgressListener paramProgressListener)
  {
    this.progressHelper.addProgressListener(paramProgressListener);
  }
  
  protected String autoDetectKey(String paramString, Map<String, ZioEntry> paramMap)
    throws NoSuchAlgorithmException, IOException
  {
    boolean bool = getLogger().isDebugEnabled();
    if (!paramString.startsWith("auto")) {
      return paramString;
    }
    Object localObject1 = null;
    Iterator localIterator = paramMap.entrySet().iterator();
    paramMap = (Map<String, ZioEntry>)localObject1;
    Object localObject2;
    while (localIterator.hasNext())
    {
      localObject1 = (Map.Entry)localIterator.next();
      localObject2 = (String)((Map.Entry)localObject1).getKey();
      if ((((String)localObject2).startsWith("META-INF/")) && (((String)localObject2).endsWith(".RSA")))
      {
        localObject2 = MessageDigest.getInstance("MD5");
        localObject1 = ((ZioEntry)((Map.Entry)localObject1).getValue()).getData();
        if (localObject1.length >= 1458) {
          break label173;
        }
      }
    }
    if (paramString.equals("auto-testkey"))
    {
      if (bool) {
        getLogger().debug("Falling back to key=" + paramMap);
      }
      return "testkey";
      label173:
      ((MessageDigest)localObject2).update((byte[])localObject1, 0, 1458);
      paramMap = ((MessageDigest)localObject2).digest();
      localObject1 = new StringBuilder();
      int j = paramMap.length;
      int i = 0;
      while (i < j)
      {
        ((StringBuilder)localObject1).append(String.format("%02x", new Object[] { Byte.valueOf(paramMap[i]) }));
        i += 1;
      }
      paramMap = ((StringBuilder)localObject1).toString();
      localObject1 = (String)this.autoKeyDetect.get(paramMap);
      if (bool)
      {
        if (localObject1 == null) {
          break label312;
        }
        getLogger().debug(String.format("Auto-determined key=%s using md5=%s", new Object[] { localObject1, paramMap }));
      }
      for (;;)
      {
        paramMap = (Map<String, ZioEntry>)localObject1;
        if (localObject1 == null) {
          break;
        }
        return (String)localObject1;
        label312:
        getLogger().debug(String.format("Auto key determination failed for md5=%s", new Object[] { paramMap }));
      }
    }
    if (paramString.equals("auto-none"))
    {
      if (bool) {
        getLogger().debug("Unable to determine key, returning: none");
      }
      return "none";
    }
    return null;
  }
  
  public void cancel()
  {
    this.canceled = true;
  }
  
  public KeySet getKeySet()
  {
    return this.keySet;
  }
  
  public String getKeymode()
  {
    return this.keymode;
  }
  
  public ResourceAdapter getResourceAdapter()
  {
    return this.resourceAdapter;
  }
  
  public boolean isCanceled()
  {
    return this.canceled;
  }
  
  public void issueLoadingCertAndKeysProgressEvent()
  {
    this.progressHelper.progress(1, this.resourceAdapter.getString(ResourceAdapter.Item.LOADING_CERTIFICATE_AND_KEY, new Object[0]));
  }
  
  public void loadKeys(String paramString)
    throws IOException, GeneralSecurityException
  {
    this.keySet = ((KeySet)this.loadedKeys.get(paramString));
    if (this.keySet != null) {}
    do
    {
      do
      {
        return;
        this.keySet = new KeySet();
        this.keySet.setName(paramString);
        this.loadedKeys.put(paramString, this.keySet);
      } while ("none".equals(paramString));
      issueLoadingCertAndKeysProgressEvent();
      URL localURL = getClass().getResource("/keys/" + paramString + ".pk8");
      this.keySet.setPrivateKey(readPrivateKey(localURL, null));
      localURL = getClass().getResource("/keys/" + paramString + ".x509.pem");
      this.keySet.setPublicKey(readPublicKey(localURL));
      paramString = getClass().getResource("/keys/" + paramString + ".sbt");
    } while (paramString == null);
    this.keySet.setSigBlockTemplate(readContentAsBytes(paramString));
  }
  
  public void loadProvider(String paramString)
    throws ClassNotFoundException, IllegalAccessException, InstantiationException
  {
    Security.insertProviderAt((Provider)Class.forName(paramString).newInstance(), 1);
  }
  
  public byte[] readContentAsBytes(InputStream paramInputStream)
    throws IOException
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    byte[] arrayOfByte = new byte['ࠀ'];
    for (int i = paramInputStream.read(arrayOfByte); i != -1; i = paramInputStream.read(arrayOfByte)) {
      localByteArrayOutputStream.write(arrayOfByte, 0, i);
    }
    return localByteArrayOutputStream.toByteArray();
  }
  
  public byte[] readContentAsBytes(URL paramURL)
    throws IOException
  {
    return readContentAsBytes(paramURL.openStream());
  }
  
  /* Error */
  public PrivateKey readPrivateKey(URL paramURL, String paramString)
    throws IOException, GeneralSecurityException
  {
    // Byte code:
    //   0: new 688	java/io/DataInputStream
    //   3: dup
    //   4: aload_1
    //   5: invokevirtual 684	java/net/URL:openStream	()Ljava/io/InputStream;
    //   8: invokespecial 690	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   11: astore_3
    //   12: aload_0
    //   13: aload_3
    //   14: invokevirtual 686	kellinwood/security/zipsigner/ZipSigner:readContentAsBytes	(Ljava/io/InputStream;)[B
    //   17: astore 4
    //   19: aload_0
    //   20: aload 4
    //   22: aload_2
    //   23: invokespecial 692	kellinwood/security/zipsigner/ZipSigner:decryptPrivateKey	([BLjava/lang/String;)Ljava/security/spec/KeySpec;
    //   26: astore_2
    //   27: aload_2
    //   28: astore_1
    //   29: aload_2
    //   30: ifnonnull +13 -> 43
    //   33: new 694	java/security/spec/PKCS8EncodedKeySpec
    //   36: dup
    //   37: aload 4
    //   39: invokespecial 695	java/security/spec/PKCS8EncodedKeySpec:<init>	([B)V
    //   42: astore_1
    //   43: ldc_w 697
    //   46: invokestatic 702	java/security/KeyFactory:getInstance	(Ljava/lang/String;)Ljava/security/KeyFactory;
    //   49: aload_1
    //   50: invokevirtual 706	java/security/KeyFactory:generatePrivate	(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
    //   53: astore_2
    //   54: aload_3
    //   55: invokevirtual 709	java/io/DataInputStream:close	()V
    //   58: aload_2
    //   59: areturn
    //   60: astore_2
    //   61: ldc_w 711
    //   64: invokestatic 702	java/security/KeyFactory:getInstance	(Ljava/lang/String;)Ljava/security/KeyFactory;
    //   67: aload_1
    //   68: invokevirtual 706	java/security/KeyFactory:generatePrivate	(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
    //   71: astore_1
    //   72: aload_3
    //   73: invokevirtual 709	java/io/DataInputStream:close	()V
    //   76: aload_1
    //   77: areturn
    //   78: astore_1
    //   79: aload_3
    //   80: invokevirtual 709	java/io/DataInputStream:close	()V
    //   83: aload_1
    //   84: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	85	0	this	ZipSigner
    //   0	85	1	paramURL	URL
    //   0	85	2	paramString	String
    //   11	69	3	localDataInputStream	java.io.DataInputStream
    //   17	21	4	arrayOfByte	byte[]
    // Exception table:
    //   from	to	target	type
    //   43	54	60	java/security/spec/InvalidKeySpecException
    //   12	27	78	finally
    //   33	43	78	finally
    //   43	54	78	finally
    //   61	72	78	finally
  }
  
  public X509Certificate readPublicKey(URL paramURL)
    throws IOException, GeneralSecurityException
  {
    paramURL = paramURL.openStream();
    try
    {
      X509Certificate localX509Certificate = (X509Certificate)CertificateFactory.getInstance("X.509").generateCertificate(paramURL);
      return localX509Certificate;
    }
    finally
    {
      paramURL.close();
    }
  }
  
  public void removeProgressListener(ProgressListener paramProgressListener)
  {
    try
    {
      this.progressHelper.removeProgressListener(paramProgressListener);
      return;
    }
    finally
    {
      paramProgressListener = finally;
      throw paramProgressListener;
    }
  }
  
  public void resetCanceled()
  {
    this.canceled = false;
  }
  
  public void setKeymode(String paramString)
    throws IOException, GeneralSecurityException
  {
    if (getLogger().isDebugEnabled()) {
      getLogger().debug("setKeymode: " + paramString);
    }
    this.keymode = paramString;
    if (this.keymode.startsWith("auto"))
    {
      this.keySet = null;
      return;
    }
    this.progressHelper.initProgress();
    loadKeys(this.keymode);
  }
  
  public void setKeys(String paramString1, X509Certificate paramX509Certificate, PrivateKey paramPrivateKey, String paramString2, byte[] paramArrayOfByte)
  {
    this.keySet = new KeySet(paramString1, paramX509Certificate, paramPrivateKey, paramString2, paramArrayOfByte);
  }
  
  public void setKeys(String paramString, X509Certificate paramX509Certificate, PrivateKey paramPrivateKey, byte[] paramArrayOfByte)
  {
    this.keySet = new KeySet(paramString, paramX509Certificate, paramPrivateKey, paramArrayOfByte);
  }
  
  public void setResourceAdapter(ResourceAdapter paramResourceAdapter)
  {
    this.resourceAdapter = paramResourceAdapter;
  }
  
  public void signZip(String paramString1, String paramString2)
    throws IOException, GeneralSecurityException
  {
    if (new File(paramString1).getCanonicalFile().equals(new File(paramString2).getCanonicalFile())) {
      throw new IllegalArgumentException(this.resourceAdapter.getString(ResourceAdapter.Item.INPUT_SAME_AS_OUTPUT_ERROR, new Object[0]));
    }
    this.progressHelper.initProgress();
    this.progressHelper.progress(1, this.resourceAdapter.getString(ResourceAdapter.Item.PARSING_CENTRAL_DIRECTORY, new Object[0]));
    signZip(ZipInput.read(paramString1).getEntries(), new FileOutputStream(paramString2), paramString2);
  }
  
  public void signZip(URL paramURL, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
    throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException, GeneralSecurityException
  {
    signZip(paramURL, paramString1, paramString2.toCharArray(), paramString3, paramString4.toCharArray(), "SHA1withRSA", paramString5, paramString6);
  }
  
  public void signZip(URL paramURL, String paramString1, char[] paramArrayOfChar1, String paramString2, char[] paramArrayOfChar2, String paramString3, String paramString4, String paramString5)
    throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException, GeneralSecurityException
  {
    Object localObject2 = null;
    Object localObject1 = paramString1;
    if (paramString1 == null) {
      paramString1 = (String)localObject2;
    }
    try
    {
      localObject1 = KeyStore.getDefaultType();
      paramString1 = (String)localObject2;
      localObject1 = KeyStore.getInstance((String)localObject1);
      paramString1 = (String)localObject2;
      paramURL = paramURL.openStream();
      paramString1 = paramURL;
      ((KeyStore)localObject1).load(paramURL, paramArrayOfChar1);
      paramString1 = paramURL;
      setKeys("custom", (X509Certificate)((KeyStore)localObject1).getCertificate(paramString2), (PrivateKey)((KeyStore)localObject1).getKey(paramString2, paramArrayOfChar2), paramString3, null);
      paramString1 = paramURL;
      signZip(paramString4, paramString5);
      return;
    }
    finally
    {
      if (paramString1 != null) {
        paramString1.close();
      }
    }
  }
  
  public void signZip(Map<String, ZioEntry> paramMap, OutputStream paramOutputStream, String paramString)
    throws IOException, GeneralSecurityException
  {
    boolean bool1 = getLogger().isDebugEnabled();
    this.progressHelper.initProgress();
    Object localObject1;
    if (this.keySet == null)
    {
      if (!this.keymode.startsWith("auto")) {
        throw new IllegalStateException("No keys configured for signing the file!");
      }
      localObject1 = autoDetectKey(this.keymode, paramMap);
      if (localObject1 == null) {
        throw new AutoKeyException(this.resourceAdapter.getString(ResourceAdapter.Item.AUTO_KEY_SELECTION_ERROR, new Object[] { new File(paramString).getName() }));
      }
      this.autoKeyObservable.notifyObservers(localObject1);
      loadKeys((String)localObject1);
    }
    do
    {
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  for (;;)
                  {
                    int i;
                    try
                    {
                      paramOutputStream = new ZipOutput(paramOutputStream);
                    }
                    finally
                    {
                      ZioEntry localZioEntry;
                      Object localObject2;
                      long l;
                      boolean bool2;
                      paramOutputStream = null;
                      paramOutputStream.close();
                      if ((this.canceled) && (paramString != null)) {}
                      try
                      {
                        new File(paramString).delete();
                        throw paramMap;
                      }
                      catch (Throwable paramOutputStream)
                      {
                        getLogger().warning(paramOutputStream.getClass().getName() + ":" + paramOutputStream.getMessage());
                        continue;
                      }
                    }
                    try
                    {
                      if ("none".equals(this.keySet.getName()))
                      {
                        this.progressHelper.setProgressTotalItems(paramMap.size());
                        this.progressHelper.setProgressCurrentItem(0);
                        copyFiles(paramMap, paramOutputStream);
                        paramOutputStream.close();
                        if ((this.canceled) && (paramString != null)) {}
                        try
                        {
                          new File(paramString).delete();
                          return;
                        }
                        catch (Throwable paramMap)
                        {
                          getLogger().warning(paramMap.getClass().getName() + ":" + paramMap.getMessage());
                          return;
                        }
                      }
                      i = 0;
                      localObject1 = paramMap.values().iterator();
                    }
                    finally
                    {
                      continue;
                      i += 3;
                    }
                  }
                  if (!((Iterator)localObject1).hasNext()) {
                    break;
                  }
                  localZioEntry = (ZioEntry)((Iterator)localObject1).next();
                  localObject2 = localZioEntry.getName();
                } while ((localZioEntry.isDirectory()) || (((String)localObject2).equals("META-INF/MANIFEST.MF")) || (((String)localObject2).equals("META-INF/CERT.SF")) || (((String)localObject2).equals("META-INF/CERT.RSA")));
                if (stripPattern == null) {
                  break label1089;
                }
              } while (stripPattern.matcher((CharSequence)localObject2).matches());
              break label1089;
              this.progressHelper.setProgressTotalItems(i + 1);
              this.progressHelper.setProgressCurrentItem(0);
              l = this.keySet.getPublicKey().getNotBefore().getTime() + 3600000L;
              localObject1 = addDigestsToManifest(paramMap);
              bool2 = this.canceled;
              if (!bool2) {
                break;
              }
              paramOutputStream.close();
            } while ((!this.canceled) || (paramString == null));
            try
            {
              new File(paramString).delete();
              return;
            }
            catch (Throwable paramMap)
            {
              getLogger().warning(paramMap.getClass().getName() + ":" + paramMap.getMessage());
              return;
            }
            localZioEntry = new ZioEntry("META-INF/MANIFEST.MF");
            localZioEntry.setTime(l);
            ((Manifest)localObject1).write(localZioEntry.getOutputStream());
            paramOutputStream.write(localZioEntry);
            localZioEntry = new ZioEntry("META-INF/CERT.SF");
            localZioEntry.setTime(l);
            localObject2 = new ByteArrayOutputStream();
            generateSignatureFile((Manifest)localObject1, (OutputStream)localObject2);
            bool2 = this.canceled;
            if (!bool2) {
              break;
            }
            paramOutputStream.close();
          } while ((!this.canceled) || (paramString == null));
          try
          {
            new File(paramString).delete();
            return;
          }
          catch (Throwable paramMap)
          {
            getLogger().warning(paramMap.getClass().getName() + ":" + paramMap.getMessage());
            return;
          }
          localObject2 = ((ByteArrayOutputStream)localObject2).toByteArray();
          if (bool1) {
            getLogger().debug("Signature File: \n" + new String((byte[])localObject2) + "\n" + HexDumpEncoder.encode((byte[])localObject2));
          }
          localZioEntry.getOutputStream().write((byte[])localObject2);
          paramOutputStream.write(localZioEntry);
          this.progressHelper.progress(0, this.resourceAdapter.getString(ResourceAdapter.Item.GENERATING_SIGNATURE_BLOCK, new Object[0]));
          localZioEntry = new ZioEntry("META-INF/CERT.RSA");
          localZioEntry.setTime(l);
          writeSignatureBlock(this.keySet, (byte[])localObject2, localZioEntry.getOutputStream());
          paramOutputStream.write(localZioEntry);
          bool1 = this.canceled;
          if (!bool1) {
            break;
          }
          paramOutputStream.close();
        } while ((!this.canceled) || (paramString == null));
        try
        {
          new File(paramString).delete();
          return;
        }
        catch (Throwable paramMap)
        {
          getLogger().warning(paramMap.getClass().getName() + ":" + paramMap.getMessage());
          return;
        }
        copyFiles((Manifest)localObject1, paramMap, paramOutputStream, l);
        bool1 = this.canceled;
        if (!bool1) {
          break;
        }
        paramOutputStream.close();
      } while ((!this.canceled) || (paramString == null));
      try
      {
        new File(paramString).delete();
        return;
      }
      catch (Throwable paramMap)
      {
        getLogger().warning(paramMap.getClass().getName() + ":" + paramMap.getMessage());
        return;
      }
      paramOutputStream.close();
    } while ((!this.canceled) || (paramString == null));
    try
    {
      new File(paramString).delete();
      return;
    }
    catch (Throwable paramMap)
    {
      getLogger().warning(paramMap.getClass().getName() + ":" + paramMap.getMessage());
      return;
    }
  }
  
  public void signZip(Map<String, ZioEntry> paramMap, String paramString)
    throws IOException, GeneralSecurityException
  {
    this.progressHelper.initProgress();
    signZip(paramMap, new FileOutputStream(paramString), paramString);
  }
  
  public static class AutoKeyObservable
    extends Observable
  {
    public void notifyObservers(Object paramObject)
    {
      super.setChanged();
      super.notifyObservers(paramObject);
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/ZipSigner.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */