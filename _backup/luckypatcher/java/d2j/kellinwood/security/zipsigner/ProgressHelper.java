package kellinwood.security.zipsigner;

import java.util.ArrayList;
import java.util.Iterator;

public class ProgressHelper
{
  private ArrayList<ProgressListener> listeners = new ArrayList();
  private int progressCurrentItem = 0;
  private ProgressEvent progressEvent = new ProgressEvent();
  private int progressTotalItems = 0;
  
  public void addProgressListener(ProgressListener paramProgressListener)
  {
    try
    {
      ArrayList localArrayList = (ArrayList)this.listeners.clone();
      localArrayList.add(paramProgressListener);
      this.listeners = localArrayList;
      return;
    }
    finally
    {
      paramProgressListener = finally;
      throw paramProgressListener;
    }
  }
  
  public int getProgressCurrentItem()
  {
    return this.progressCurrentItem;
  }
  
  public int getProgressTotalItems()
  {
    return this.progressTotalItems;
  }
  
  public void initProgress()
  {
    this.progressTotalItems = 10000;
    this.progressCurrentItem = 0;
  }
  
  public void progress(int paramInt, String paramString)
  {
    this.progressCurrentItem += 1;
    if (this.progressTotalItems == 0) {}
    for (int i = 0;; i = this.progressCurrentItem * 100 / this.progressTotalItems)
    {
      Iterator localIterator = this.listeners.iterator();
      while (localIterator.hasNext())
      {
        ProgressListener localProgressListener = (ProgressListener)localIterator.next();
        this.progressEvent.setMessage(paramString);
        this.progressEvent.setPercentDone(i);
        this.progressEvent.setPriority(paramInt);
        localProgressListener.onProgress(this.progressEvent);
      }
    }
  }
  
  public void removeProgressListener(ProgressListener paramProgressListener)
  {
    try
    {
      ArrayList localArrayList = (ArrayList)this.listeners.clone();
      localArrayList.remove(paramProgressListener);
      this.listeners = localArrayList;
      return;
    }
    finally
    {
      paramProgressListener = finally;
      throw paramProgressListener;
    }
  }
  
  public void setProgressCurrentItem(int paramInt)
  {
    this.progressCurrentItem = paramInt;
  }
  
  public void setProgressTotalItems(int paramInt)
  {
    this.progressTotalItems = paramInt;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/ProgressHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */