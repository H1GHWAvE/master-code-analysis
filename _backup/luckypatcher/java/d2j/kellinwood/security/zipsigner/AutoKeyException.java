package kellinwood.security.zipsigner;

public class AutoKeyException
  extends RuntimeException
{
  private static final long serialVersionUID = 1L;
  
  public AutoKeyException(String paramString)
  {
    super(paramString);
  }
  
  public AutoKeyException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/AutoKeyException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */