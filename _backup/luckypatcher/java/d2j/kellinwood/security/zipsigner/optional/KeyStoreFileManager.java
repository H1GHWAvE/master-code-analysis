package kellinwood.security.zipsigner.optional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.KeyStore;
import java.security.Provider;
import java.security.Security;
import java.security.cert.Certificate;
import kellinwood.logging.LoggerInterface;
import kellinwood.logging.LoggerManager;
import org.spongycastle.jce.provider.BouncyCastleProvider;

public class KeyStoreFileManager
{
  static LoggerInterface logger;
  static Provider provider = new BouncyCastleProvider();
  
  static
  {
    logger = LoggerManager.getLogger(KeyStoreFileManager.class.getName());
    Security.addProvider(getProvider());
  }
  
  public static boolean containsKey(String paramString1, String paramString2, String paramString3)
    throws Exception
  {
    return loadKeyStore(paramString1, paramString2).containsAlias(paramString3);
  }
  
  /* Error */
  static void copyFile(File paramFile1, File paramFile2, boolean paramBoolean)
    throws IOException
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 68	java/io/File:exists	()Z
    //   4: ifeq +42 -> 46
    //   7: aload_1
    //   8: invokevirtual 71	java/io/File:isDirectory	()Z
    //   11: ifeq +35 -> 46
    //   14: new 62	java/io/IOException
    //   17: dup
    //   18: new 73	java/lang/StringBuilder
    //   21: dup
    //   22: invokespecial 74	java/lang/StringBuilder:<init>	()V
    //   25: ldc 76
    //   27: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   30: aload_1
    //   31: invokevirtual 83	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   34: ldc 85
    //   36: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   39: invokevirtual 88	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   42: invokespecial 91	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   45: athrow
    //   46: new 93	java/io/FileInputStream
    //   49: dup
    //   50: aload_0
    //   51: invokespecial 96	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   54: astore 6
    //   56: new 98	java/io/FileOutputStream
    //   59: dup
    //   60: aload_1
    //   61: invokespecial 99	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   64: astore 7
    //   66: sipush 4096
    //   69: newarray <illegal type>
    //   71: astore 8
    //   73: lconst_0
    //   74: lstore 4
    //   76: aload 6
    //   78: aload 8
    //   80: invokevirtual 103	java/io/FileInputStream:read	([B)I
    //   83: istore_3
    //   84: iconst_m1
    //   85: iload_3
    //   86: if_icmpeq +22 -> 108
    //   89: aload 7
    //   91: aload 8
    //   93: iconst_0
    //   94: iload_3
    //   95: invokevirtual 107	java/io/FileOutputStream:write	([BII)V
    //   98: lload 4
    //   100: iload_3
    //   101: i2l
    //   102: ladd
    //   103: lstore 4
    //   105: goto -29 -> 76
    //   108: aload 7
    //   110: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   113: aload 6
    //   115: invokevirtual 111	java/io/FileInputStream:close	()V
    //   118: aload_0
    //   119: invokevirtual 115	java/io/File:length	()J
    //   122: aload_1
    //   123: invokevirtual 115	java/io/File:length	()J
    //   126: lcmp
    //   127: ifeq +60 -> 187
    //   130: new 62	java/io/IOException
    //   133: dup
    //   134: new 73	java/lang/StringBuilder
    //   137: dup
    //   138: invokespecial 74	java/lang/StringBuilder:<init>	()V
    //   141: ldc 117
    //   143: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   146: aload_0
    //   147: invokevirtual 83	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   150: ldc 119
    //   152: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   155: aload_1
    //   156: invokevirtual 83	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   159: ldc 121
    //   161: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   164: invokevirtual 88	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   167: invokespecial 91	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   170: athrow
    //   171: astore_0
    //   172: aload 7
    //   174: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   177: aload_0
    //   178: athrow
    //   179: astore_0
    //   180: aload 6
    //   182: invokevirtual 111	java/io/FileInputStream:close	()V
    //   185: aload_0
    //   186: athrow
    //   187: iload_2
    //   188: ifeq +12 -> 200
    //   191: aload_1
    //   192: aload_0
    //   193: invokevirtual 124	java/io/File:lastModified	()J
    //   196: invokevirtual 128	java/io/File:setLastModified	(J)Z
    //   199: pop
    //   200: return
    //   201: astore 7
    //   203: goto -90 -> 113
    //   206: astore_1
    //   207: goto -30 -> 177
    //   210: astore 6
    //   212: goto -94 -> 118
    //   215: astore_1
    //   216: goto -31 -> 185
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	219	0	paramFile1	File
    //   0	219	1	paramFile2	File
    //   0	219	2	paramBoolean	boolean
    //   83	18	3	i	int
    //   74	30	4	l	long
    //   54	127	6	localFileInputStream	java.io.FileInputStream
    //   210	1	6	localIOException1	IOException
    //   64	109	7	localFileOutputStream	FileOutputStream
    //   201	1	7	localIOException2	IOException
    //   71	21	8	arrayOfByte	byte[]
    // Exception table:
    //   from	to	target	type
    //   66	73	171	finally
    //   76	84	171	finally
    //   89	98	171	finally
    //   56	66	179	finally
    //   108	113	179	finally
    //   172	177	179	finally
    //   177	179	179	finally
    //   108	113	201	java/io/IOException
    //   172	177	206	java/io/IOException
    //   113	118	210	java/io/IOException
    //   180	185	215	java/io/IOException
  }
  
  public static KeyStore createKeyStore(String paramString, char[] paramArrayOfChar)
    throws Exception
  {
    if (paramString.toLowerCase().endsWith(".bks")) {}
    for (paramString = KeyStore.getInstance("bks", new BouncyCastleProvider());; paramString = new JksKeyStore())
    {
      paramString.load(null, paramArrayOfChar);
      return paramString;
    }
  }
  
  public static void deleteKey(String paramString1, String paramString2, String paramString3)
    throws Exception
  {
    KeyStore localKeyStore = loadKeyStore(paramString1, paramString2);
    localKeyStore.deleteEntry(paramString3);
    writeKeyStore(localKeyStore, paramString1, paramString2);
  }
  
  /* Error */
  public static java.security.KeyStore.Entry getKeyEntry(String paramString1, String paramString2, String paramString3, String paramString4)
    throws Exception
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 6
    //   3: aconst_null
    //   4: astore 5
    //   6: aload 6
    //   8: astore 4
    //   10: aload_0
    //   11: aload_1
    //   12: invokestatic 51	kellinwood/security/zipsigner/optional/KeyStoreFileManager:loadKeyStore	(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;
    //   15: astore 7
    //   17: aload 6
    //   19: astore 4
    //   21: invokestatic 169	kellinwood/security/zipsigner/optional/PasswordObfuscator:getInstance	()Lkellinwood/security/zipsigner/optional/PasswordObfuscator;
    //   24: aload_0
    //   25: aload_2
    //   26: aload_3
    //   27: invokevirtual 173	kellinwood/security/zipsigner/optional/PasswordObfuscator:decodeAliasPassword	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[C
    //   30: astore_0
    //   31: aload_0
    //   32: astore 4
    //   34: new 175	java/security/KeyStore$PasswordProtection
    //   37: dup
    //   38: aload_0
    //   39: invokespecial 178	java/security/KeyStore$PasswordProtection:<init>	([C)V
    //   42: astore_1
    //   43: aload 7
    //   45: aload_2
    //   46: aload_1
    //   47: invokevirtual 182	java/security/KeyStore:getEntry	(Ljava/lang/String;Ljava/security/KeyStore$ProtectionParameter;)Ljava/security/KeyStore$Entry;
    //   50: astore_2
    //   51: aload_0
    //   52: ifnull +7 -> 59
    //   55: aload_0
    //   56: invokestatic 185	kellinwood/security/zipsigner/optional/PasswordObfuscator:flush	([C)V
    //   59: aload_1
    //   60: ifnull +7 -> 67
    //   63: aload_1
    //   64: invokevirtual 188	java/security/KeyStore$PasswordProtection:destroy	()V
    //   67: aload_2
    //   68: areturn
    //   69: astore_2
    //   70: aload 5
    //   72: astore_1
    //   73: aload 4
    //   75: astore_0
    //   76: aload_0
    //   77: ifnull +7 -> 84
    //   80: aload_0
    //   81: invokestatic 185	kellinwood/security/zipsigner/optional/PasswordObfuscator:flush	([C)V
    //   84: aload_1
    //   85: ifnull +7 -> 92
    //   88: aload_1
    //   89: invokevirtual 188	java/security/KeyStore$PasswordProtection:destroy	()V
    //   92: aload_2
    //   93: athrow
    //   94: astore_2
    //   95: goto -19 -> 76
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	98	0	paramString1	String
    //   0	98	1	paramString2	String
    //   0	98	2	paramString3	String
    //   0	98	3	paramString4	String
    //   8	66	4	localObject1	Object
    //   4	67	5	localObject2	Object
    //   1	17	6	localObject3	Object
    //   15	29	7	localKeyStore	KeyStore
    // Exception table:
    //   from	to	target	type
    //   10	17	69	finally
    //   21	31	69	finally
    //   34	43	69	finally
    //   43	51	94	finally
  }
  
  public static Provider getProvider()
  {
    return provider;
  }
  
  public static KeyStore loadKeyStore(String paramString1, String paramString2)
    throws Exception
  {
    Object localObject = null;
    char[] arrayOfChar = null;
    if (paramString2 != null) {}
    try
    {
      arrayOfChar = PasswordObfuscator.getInstance().decodeKeystorePassword(paramString1, paramString2);
      localObject = arrayOfChar;
      paramString1 = loadKeyStore(paramString1, arrayOfChar);
      if (arrayOfChar != null) {
        PasswordObfuscator.flush(arrayOfChar);
      }
      return paramString1;
    }
    finally
    {
      if (localObject != null) {
        PasswordObfuscator.flush((char[])localObject);
      }
    }
  }
  
  /* Error */
  public static KeyStore loadKeyStore(String paramString, char[] paramArrayOfChar)
    throws Exception
  {
    // Byte code:
    //   0: new 152	kellinwood/security/zipsigner/optional/JksKeyStore
    //   3: dup
    //   4: invokespecial 153	kellinwood/security/zipsigner/optional/JksKeyStore:<init>	()V
    //   7: astore_2
    //   8: new 93	java/io/FileInputStream
    //   11: dup
    //   12: aload_0
    //   13: invokespecial 195	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   16: astore_3
    //   17: aload_2
    //   18: aload_3
    //   19: aload_1
    //   20: invokevirtual 150	java/security/KeyStore:load	(Ljava/io/InputStream;[C)V
    //   23: aload_3
    //   24: invokevirtual 111	java/io/FileInputStream:close	()V
    //   27: aload_2
    //   28: areturn
    //   29: astore_2
    //   30: ldc -114
    //   32: invokestatic 35	kellinwood/security/zipsigner/optional/KeyStoreFileManager:getProvider	()Ljava/security/Provider;
    //   35: invokestatic 146	java/security/KeyStore:getInstance	(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyStore;
    //   38: astore_2
    //   39: new 93	java/io/FileInputStream
    //   42: dup
    //   43: aload_0
    //   44: invokespecial 195	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   47: astore_0
    //   48: aload_2
    //   49: aload_0
    //   50: aload_1
    //   51: invokevirtual 150	java/security/KeyStore:load	(Ljava/io/InputStream;[C)V
    //   54: aload_0
    //   55: invokevirtual 111	java/io/FileInputStream:close	()V
    //   58: aload_2
    //   59: areturn
    //   60: astore_0
    //   61: new 197	java/lang/RuntimeException
    //   64: dup
    //   65: new 73	java/lang/StringBuilder
    //   68: dup
    //   69: invokespecial 74	java/lang/StringBuilder:<init>	()V
    //   72: ldc -57
    //   74: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   77: aload_0
    //   78: invokevirtual 202	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   81: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   84: invokevirtual 88	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   87: aload_0
    //   88: invokespecial 205	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   91: athrow
    //   92: astore_2
    //   93: goto -63 -> 30
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	96	0	paramString	String
    //   0	96	1	paramArrayOfChar	char[]
    //   7	21	2	localJksKeyStore	JksKeyStore
    //   29	1	2	localException1	Exception
    //   38	21	2	localKeyStore	KeyStore
    //   92	1	2	localException2	Exception
    //   16	8	3	localFileInputStream	java.io.FileInputStream
    // Exception table:
    //   from	to	target	type
    //   0	8	29	java/lang/Exception
    //   30	58	60	java/lang/Exception
    //   8	27	92	java/lang/Exception
  }
  
  public static String renameKey(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
    throws Exception
  {
    Object localObject2 = null;
    Object localObject1 = localObject2;
    KeyStore localKeyStore;
    String str;
    try
    {
      localKeyStore = loadKeyStore(paramString1, paramString2);
      str = paramString4;
      localObject1 = localObject2;
      if ((localKeyStore instanceof JksKeyStore))
      {
        localObject1 = localObject2;
        str = paramString4.toLowerCase();
      }
      localObject1 = localObject2;
      if (localKeyStore.containsAlias(str))
      {
        localObject1 = localObject2;
        throw new KeyNameConflictException();
      }
    }
    finally
    {
      PasswordObfuscator.flush((char[])localObject1);
    }
    localObject1 = localObject2;
    paramString4 = PasswordObfuscator.getInstance().decodeAliasPassword(paramString1, paramString3, paramString5);
    localObject1 = paramString4;
    localKeyStore.setKeyEntry(str, localKeyStore.getKey(paramString3, paramString4), paramString4, new Certificate[] { localKeyStore.getCertificate(paramString3) });
    localObject1 = paramString4;
    localKeyStore.deleteEntry(paramString3);
    localObject1 = paramString4;
    writeKeyStore(localKeyStore, paramString1, paramString2);
    PasswordObfuscator.flush(paramString4);
    return str;
  }
  
  public static void renameTo(File paramFile1, File paramFile2)
    throws IOException
  {
    copyFile(paramFile1, paramFile2, true);
    if (!paramFile1.delete()) {
      throw new IOException("Failed to delete " + paramFile1);
    }
  }
  
  public static void setProvider(Provider paramProvider)
  {
    if (provider != null) {
      Security.removeProvider(provider.getName());
    }
    provider = paramProvider;
    Security.addProvider(paramProvider);
  }
  
  public static void validateKeyPassword(String paramString1, String paramString2, String paramString3)
    throws Exception
  {
    Object localObject2 = null;
    Object localObject1 = localObject2;
    try
    {
      KeyStore localKeyStore = loadKeyStore(paramString1, (char[])null);
      localObject1 = localObject2;
      paramString1 = PasswordObfuscator.getInstance().decodeAliasPassword(paramString1, paramString2, paramString3);
      localObject1 = paramString1;
      localKeyStore.getKey(paramString2, paramString1);
      if (paramString1 != null) {
        PasswordObfuscator.flush(paramString1);
      }
      return;
    }
    finally
    {
      if (localObject1 != null) {
        PasswordObfuscator.flush((char[])localObject1);
      }
    }
  }
  
  public static void validateKeystorePassword(String paramString1, String paramString2)
    throws Exception
  {
    try
    {
      loadKeyStore(paramString1, paramString2);
      return;
    }
    finally
    {
      if (0 != 0) {
        PasswordObfuscator.flush(null);
      }
    }
  }
  
  public static void writeKeyStore(KeyStore paramKeyStore, String paramString1, String paramString2)
    throws Exception
  {
    String str = null;
    try
    {
      paramString2 = PasswordObfuscator.getInstance().decodeKeystorePassword(paramString1, paramString2);
      str = paramString2;
      writeKeyStore(paramKeyStore, paramString1, paramString2);
      return;
    }
    finally
    {
      if (str != null) {
        PasswordObfuscator.flush(str);
      }
    }
  }
  
  public static void writeKeyStore(KeyStore paramKeyStore, String paramString, char[] paramArrayOfChar)
    throws Exception
  {
    File localFile = new File(paramString);
    try
    {
      if (localFile.exists())
      {
        paramString = File.createTempFile(localFile.getName(), null, localFile.getParentFile());
        FileOutputStream localFileOutputStream = new FileOutputStream(paramString);
        paramKeyStore.store(localFileOutputStream, paramArrayOfChar);
        localFileOutputStream.flush();
        localFileOutputStream.close();
        renameTo(paramString, localFile);
        return;
      }
      paramString = new FileOutputStream(paramString);
      paramKeyStore.store(paramString, paramArrayOfChar);
      paramString.close();
      return;
    }
    catch (Exception paramKeyStore) {}
    try
    {
      paramString = new PrintWriter(new FileWriter(File.createTempFile("zipsigner-error", ".log", localFile.getParentFile())));
      paramKeyStore.printStackTrace(paramString);
      paramString.flush();
      paramString.close();
      throw paramKeyStore;
    }
    catch (Exception paramString)
    {
      for (;;) {}
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/optional/KeyStoreFileManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */