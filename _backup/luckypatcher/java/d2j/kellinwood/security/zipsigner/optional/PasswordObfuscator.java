package kellinwood.security.zipsigner.optional;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import kellinwood.logging.LoggerInterface;
import kellinwood.logging.LoggerManager;
import kellinwood.security.zipsigner.Base64;

public class PasswordObfuscator
{
  private static PasswordObfuscator instance = null;
  static final String x = "harold-and-maude";
  LoggerInterface logger = LoggerManager.getLogger(PasswordObfuscator.class.getName());
  SecretKeySpec skeySpec = new SecretKeySpec("harold-and-maude".getBytes(), "AES");
  
  public static void flush(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte == null) {}
    for (;;)
    {
      return;
      int i = 0;
      while (i < paramArrayOfByte.length)
      {
        paramArrayOfByte[i] = 0;
        i += 1;
      }
    }
  }
  
  public static void flush(char[] paramArrayOfChar)
  {
    if (paramArrayOfChar == null) {}
    for (;;)
    {
      return;
      int i = 0;
      while (i < paramArrayOfChar.length)
      {
        paramArrayOfChar[i] = '\000';
        i += 1;
      }
    }
  }
  
  public static PasswordObfuscator getInstance()
  {
    if (instance == null) {
      instance = new PasswordObfuscator();
    }
    return instance;
  }
  
  public char[] decode(String paramString1, String paramString2)
  {
    if (paramString2 == null) {
      return null;
    }
    for (;;)
    {
      Object localObject;
      int i;
      int j;
      int k;
      try
      {
        localObject = Cipher.getInstance("AES/ECB/PKCS5Padding");
        ((Cipher)localObject).init(2, new SecretKeySpec("harold-and-maude".getBytes(), "AES"));
        localObject = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(((Cipher)localObject).doFinal(Base64.decode(paramString2.getBytes())))));
        paramString2 = new char[''];
        i = 0;
        j = ((BufferedReader)localObject).read(paramString2, i, 128 - i);
        if (j != -1)
        {
          i += j;
          continue;
        }
        if (i <= paramString1.length()) {
          return null;
        }
        localObject = new char[i - paramString1.length()];
        k = 0;
        j = paramString1.length();
      }
      catch (Exception paramString1)
      {
        this.logger.error("Failed to decode password", paramString1);
        return null;
      }
      flush(paramString2);
      return (char[])localObject;
      while (j < i)
      {
        localObject[k] = paramString2[j];
        k += 1;
        j += 1;
      }
    }
  }
  
  public char[] decodeAliasPassword(String paramString1, String paramString2, String paramString3)
  {
    return decode(paramString1 + paramString2, paramString3);
  }
  
  public char[] decodeKeystorePassword(String paramString1, String paramString2)
  {
    return decode(paramString1, paramString2);
  }
  
  public String encode(String paramString1, String paramString2)
  {
    if (paramString2 == null) {
      return null;
    }
    paramString2 = paramString2.toCharArray();
    paramString1 = encode(paramString1, paramString2);
    flush(paramString2);
    return paramString1;
  }
  
  public String encode(String paramString, char[] paramArrayOfChar)
  {
    if (paramArrayOfChar == null) {
      return null;
    }
    try
    {
      Cipher localCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
      localCipher.init(1, this.skeySpec);
      ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
      OutputStreamWriter localOutputStreamWriter = new OutputStreamWriter(localByteArrayOutputStream);
      localOutputStreamWriter.write(paramString);
      localOutputStreamWriter.write(paramArrayOfChar);
      localOutputStreamWriter.flush();
      paramString = Base64.encode(localCipher.doFinal(localByteArrayOutputStream.toByteArray()));
      return paramString;
    }
    catch (Exception paramString)
    {
      this.logger.error("Failed to obfuscate password", paramString);
    }
    return null;
  }
  
  public String encodeAliasPassword(String paramString1, String paramString2, String paramString3)
  {
    return encode(paramString1 + paramString2, paramString3);
  }
  
  public String encodeAliasPassword(String paramString1, String paramString2, char[] paramArrayOfChar)
  {
    return encode(paramString1 + paramString2, paramArrayOfChar);
  }
  
  public String encodeKeystorePassword(String paramString1, String paramString2)
  {
    return encode(paramString1, paramString2);
  }
  
  public String encodeKeystorePassword(String paramString, char[] paramArrayOfChar)
  {
    return encode(paramString, paramArrayOfChar);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/optional/PasswordObfuscator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */