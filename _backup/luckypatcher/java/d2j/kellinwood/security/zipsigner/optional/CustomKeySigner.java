package kellinwood.security.zipsigner.optional;

import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import kellinwood.security.zipsigner.ZipSigner;

public class CustomKeySigner
{
  public static void signZip(ZipSigner paramZipSigner, String paramString1, char[] paramArrayOfChar1, String paramString2, char[] paramArrayOfChar2, String paramString3, String paramString4, String paramString5)
    throws Exception
  {
    paramZipSigner.issueLoadingCertAndKeysProgressEvent();
    paramString1 = KeyStoreFileManager.loadKeyStore(paramString1, paramArrayOfChar1);
    paramZipSigner.setKeys("custom", (X509Certificate)paramString1.getCertificate(paramString2), (PrivateKey)paramString1.getKey(paramString2, paramArrayOfChar2), paramString3, null);
    paramZipSigner.signZip(paramString4, paramString5);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/optional/CustomKeySigner.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */