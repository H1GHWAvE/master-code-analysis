package kellinwood.security.zipsigner.optional;

import java.security.MessageDigest;
import kellinwood.logging.LoggerInterface;
import kellinwood.logging.LoggerManager;
import kellinwood.security.zipsigner.Base64;
import org.spongycastle.util.encoders.HexTranslator;

public class Fingerprint
{
  static LoggerInterface logger = LoggerManager.getLogger(Fingerprint.class.getName());
  
  public static String base64Fingerprint(String paramString, byte[] paramArrayOfByte)
  {
    try
    {
      paramString = calcDigest(paramString, paramArrayOfByte);
      if (paramString == null) {
        return null;
      }
      paramString = Base64.encode(paramString);
      return paramString;
    }
    catch (Exception paramString)
    {
      logger.error(paramString.getMessage(), paramString);
    }
    return null;
  }
  
  static byte[] calcDigest(String paramString, byte[] paramArrayOfByte)
  {
    try
    {
      paramString = MessageDigest.getInstance(paramString);
      paramString.update(paramArrayOfByte);
      paramString = paramString.digest();
      return paramString;
    }
    catch (Exception paramString)
    {
      logger.error(paramString.getMessage(), paramString);
    }
    return null;
  }
  
  public static String hexFingerprint(String paramString, byte[] paramArrayOfByte)
  {
    for (;;)
    {
      int i;
      try
      {
        paramArrayOfByte = calcDigest(paramString, paramArrayOfByte);
        if (paramArrayOfByte == null) {
          return null;
        }
        HexTranslator localHexTranslator = new HexTranslator();
        paramString = new byte[paramArrayOfByte.length * 2];
        localHexTranslator.encode(paramArrayOfByte, 0, paramArrayOfByte.length, paramString, 0);
        paramArrayOfByte = new StringBuilder();
        i = 0;
        if (i < paramString.length)
        {
          paramArrayOfByte.append((char)paramString[i]);
          paramArrayOfByte.append((char)paramString[(i + 1)]);
          if (i != paramString.length - 2) {
            paramArrayOfByte.append(':');
          }
        }
        else
        {
          paramString = paramArrayOfByte.toString().toUpperCase();
          return paramString;
        }
      }
      catch (Exception paramString)
      {
        logger.error(paramString.getMessage(), paramString);
        return null;
      }
      i += 2;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/optional/Fingerprint.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */