package kellinwood.security.zipsigner.optional;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;
import org.spongycastle.asn1.ASN1ObjectIdentifier;
import org.spongycastle.asn1.x500.style.BCStyle;
import org.spongycastle.jce.X509Principal;

public class DistinguishedNameValues
  extends LinkedHashMap<ASN1ObjectIdentifier, String>
{
  public DistinguishedNameValues()
  {
    put(BCStyle.C, null);
    put(BCStyle.ST, null);
    put(BCStyle.L, null);
    put(BCStyle.STREET, null);
    put(BCStyle.O, null);
    put(BCStyle.OU, null);
    put(BCStyle.CN, null);
  }
  
  public X509Principal getPrincipal()
  {
    Vector localVector1 = new Vector();
    Vector localVector2 = new Vector();
    Iterator localIterator = entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      if ((localEntry.getValue() != null) && (!((String)localEntry.getValue()).equals("")))
      {
        localVector1.add(localEntry.getKey());
        localVector2.add(localEntry.getValue());
      }
    }
    return new X509Principal(localVector1, localVector2);
  }
  
  public String put(ASN1ObjectIdentifier paramASN1ObjectIdentifier, String paramString)
  {
    String str = paramString;
    if (paramString != null)
    {
      str = paramString;
      if (paramString.equals("")) {
        str = null;
      }
    }
    if (containsKey(paramASN1ObjectIdentifier))
    {
      super.put(paramASN1ObjectIdentifier, str);
      return str;
    }
    super.put(paramASN1ObjectIdentifier, str);
    return str;
  }
  
  public void setCommonName(String paramString)
  {
    put(BCStyle.CN, paramString);
  }
  
  public void setCountry(String paramString)
  {
    put(BCStyle.C, paramString);
  }
  
  public void setLocality(String paramString)
  {
    put(BCStyle.L, paramString);
  }
  
  public void setOrganization(String paramString)
  {
    put(BCStyle.O, paramString);
  }
  
  public void setOrganizationalUnit(String paramString)
  {
    put(BCStyle.OU, paramString);
  }
  
  public void setState(String paramString)
  {
    put(BCStyle.ST, paramString);
  }
  
  public void setStreet(String paramString)
  {
    put(BCStyle.STREET, paramString);
  }
  
  public int size()
  {
    int i = 0;
    Iterator localIterator = values().iterator();
    while (localIterator.hasNext()) {
      if ((String)localIterator.next() != null) {
        i += 1;
      }
    }
    return i;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/optional/DistinguishedNameValues.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */