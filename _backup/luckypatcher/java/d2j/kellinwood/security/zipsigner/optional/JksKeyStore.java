package kellinwood.security.zipsigner.optional;

import java.security.KeyStore;

public class JksKeyStore
  extends KeyStore
{
  public JksKeyStore()
  {
    super(new JKS(), KeyStoreFileManager.getProvider(), "jks");
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/optional/JksKeyStore.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */