package kellinwood.security.zipsigner.optional;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.util.Date;
import kellinwood.security.zipsigner.KeySet;
import org.spongycastle.x509.X509V3CertificateGenerator;

public class CertCreator
{
  public static KeySet createKey(String paramString1, int paramInt1, String paramString2, String paramString3, int paramInt2, DistinguishedNameValues paramDistinguishedNameValues)
  {
    try
    {
      paramString1 = KeyPairGenerator.getInstance(paramString1);
      paramString1.initialize(paramInt1);
      KeyPair localKeyPair = paramString1.generateKeyPair();
      X509V3CertificateGenerator localX509V3CertificateGenerator = new X509V3CertificateGenerator();
      paramDistinguishedNameValues = paramDistinguishedNameValues.getPrincipal();
      for (paramString1 = BigInteger.valueOf(new SecureRandom().nextInt()); paramString1.compareTo(BigInteger.ZERO) < 0; paramString1 = BigInteger.valueOf(new SecureRandom().nextInt())) {}
      localX509V3CertificateGenerator.setSerialNumber(paramString1);
      localX509V3CertificateGenerator.setIssuerDN(paramDistinguishedNameValues);
      localX509V3CertificateGenerator.setNotBefore(new Date(System.currentTimeMillis() - 2592000000L));
      localX509V3CertificateGenerator.setNotAfter(new Date(System.currentTimeMillis() + 31622400000L * paramInt2));
      localX509V3CertificateGenerator.setSubjectDN(paramDistinguishedNameValues);
      localX509V3CertificateGenerator.setPublicKey(localKeyPair.getPublic());
      localX509V3CertificateGenerator.setSignatureAlgorithm(paramString3);
      paramString1 = localX509V3CertificateGenerator.generate(localKeyPair.getPrivate(), "BC");
      paramString3 = new KeySet();
      paramString3.setName(paramString2);
      paramString3.setPrivateKey(localKeyPair.getPrivate());
      paramString3.setPublicKey(paramString1);
      return paramString3;
    }
    catch (Exception paramString1)
    {
      throw new RuntimeException(paramString1.getMessage(), paramString1);
    }
  }
  
  public static KeySet createKey(String paramString1, char[] paramArrayOfChar1, String paramString2, int paramInt1, String paramString3, char[] paramArrayOfChar2, String paramString4, int paramInt2, DistinguishedNameValues paramDistinguishedNameValues)
  {
    try
    {
      paramString2 = createKey(paramString2, paramInt1, paramString3, paramString4, paramInt2, paramDistinguishedNameValues);
      paramString4 = KeyStoreFileManager.loadKeyStore(paramString1, paramArrayOfChar1);
      paramString4.setKeyEntry(paramString3, paramString2.getPrivateKey(), paramArrayOfChar2, new Certificate[] { paramString2.getPublicKey() });
      KeyStoreFileManager.writeKeyStore(paramString4, paramString1, paramArrayOfChar1);
      return paramString2;
    }
    catch (RuntimeException paramString1)
    {
      throw paramString1;
    }
    catch (Exception paramString1)
    {
      throw new RuntimeException(paramString1.getMessage(), paramString1);
    }
  }
  
  public static KeySet createKeystoreAndKey(String paramString1, char[] paramArrayOfChar1, String paramString2, int paramInt1, String paramString3, char[] paramArrayOfChar2, String paramString4, int paramInt2, DistinguishedNameValues paramDistinguishedNameValues)
  {
    try
    {
      paramString2 = createKey(paramString2, paramInt1, paramString3, paramString4, paramInt2, paramDistinguishedNameValues);
      paramString4 = KeyStoreFileManager.createKeyStore(paramString1, paramArrayOfChar1);
      paramString4.setKeyEntry(paramString3, paramString2.getPrivateKey(), paramArrayOfChar2, new Certificate[] { paramString2.getPublicKey() });
      if (new File(paramString1).exists()) {
        throw new IOException("File already exists: " + paramString1);
      }
    }
    catch (RuntimeException paramString1)
    {
      throw paramString1;
      KeyStoreFileManager.writeKeyStore(paramString4, paramString1, paramArrayOfChar1);
      return paramString2;
    }
    catch (Exception paramString1)
    {
      throw new RuntimeException(paramString1.getMessage(), paramString1);
    }
  }
  
  public static void createKeystoreAndKey(String paramString1, char[] paramArrayOfChar, String paramString2, DistinguishedNameValues paramDistinguishedNameValues)
  {
    createKeystoreAndKey(paramString1, paramArrayOfChar, "RSA", 2048, paramString2, paramArrayOfChar, "SHA1withRSA", 30, paramDistinguishedNameValues);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/optional/CertCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */