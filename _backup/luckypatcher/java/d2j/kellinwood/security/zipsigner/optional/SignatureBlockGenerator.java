package kellinwood.security.zipsigner.optional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kellinwood.security.zipsigner.KeySet;
import org.spongycastle.asn1.cms.ContentInfo;
import org.spongycastle.cert.jcajce.JcaCertStore;
import org.spongycastle.cms.CMSProcessableByteArray;
import org.spongycastle.cms.CMSSignedData;
import org.spongycastle.cms.CMSSignedDataGenerator;
import org.spongycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.spongycastle.operator.ContentSigner;
import org.spongycastle.operator.jcajce.JcaContentSignerBuilder;
import org.spongycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.spongycastle.util.Store;

public class SignatureBlockGenerator
{
  public static byte[] generate(KeySet paramKeySet, byte[] paramArrayOfByte)
  {
    try
    {
      Object localObject = new ArrayList();
      paramArrayOfByte = new CMSProcessableByteArray(paramArrayOfByte);
      ((List)localObject).add(paramKeySet.getPublicKey());
      localObject = new JcaCertStore((Collection)localObject);
      CMSSignedDataGenerator localCMSSignedDataGenerator = new CMSSignedDataGenerator();
      ContentSigner localContentSigner = new JcaContentSignerBuilder(paramKeySet.getSignatureAlgorithm()).setProvider("SC").build(paramKeySet.getPrivateKey());
      JcaSignerInfoGeneratorBuilder localJcaSignerInfoGeneratorBuilder = new JcaSignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder().setProvider("SC").build());
      localJcaSignerInfoGeneratorBuilder.setDirectSignature(true);
      localCMSSignedDataGenerator.addSignerInfoGenerator(localJcaSignerInfoGeneratorBuilder.build(localContentSigner, paramKeySet.getPublicKey()));
      localCMSSignedDataGenerator.addCertificates((Store)localObject);
      paramKeySet = localCMSSignedDataGenerator.generate(paramArrayOfByte, false).toASN1Structure().getEncoded("DER");
      return paramKeySet;
    }
    catch (Exception paramKeySet)
    {
      throw new RuntimeException(paramKeySet.getMessage(), paramKeySet);
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/optional/SignatureBlockGenerator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */