package kellinwood.security.zipsigner.optional;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.DigestInputStream;
import java.security.DigestOutputStream;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStoreException;
import java.security.KeyStoreSpi;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import javax.crypto.EncryptedPrivateKeyInfo;
import javax.crypto.spec.SecretKeySpec;

public class JKS
  extends KeyStoreSpi
{
  private static final int MAGIC = -17957139;
  private static final int PRIVATE_KEY = 1;
  private static final int TRUSTED_CERT = 2;
  private final Vector aliases = new Vector();
  private final HashMap certChains = new HashMap();
  private final HashMap dates = new HashMap();
  private final HashMap privateKeys = new HashMap();
  private final HashMap trustedCerts = new HashMap();
  
  private static byte[] charsToBytes(char[] paramArrayOfChar)
  {
    byte[] arrayOfByte = new byte[paramArrayOfChar.length * 2];
    int i = 0;
    int j = 0;
    while (i < paramArrayOfChar.length)
    {
      int k = j + 1;
      arrayOfByte[j] = ((byte)(paramArrayOfChar[i] >>> '\b'));
      j = k + 1;
      arrayOfByte[k] = ((byte)paramArrayOfChar[i]);
      i += 1;
    }
    return arrayOfByte;
  }
  
  private static byte[] decryptKey(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
    throws UnrecoverableKeyException
  {
    byte[] arrayOfByte3;
    try
    {
      paramArrayOfByte1 = new EncryptedPrivateKeyInfo(paramArrayOfByte1).getEncryptedData();
      byte[] arrayOfByte1 = new byte[20];
      System.arraycopy(paramArrayOfByte1, 0, arrayOfByte1, 0, 20);
      byte[] arrayOfByte2 = new byte[20];
      System.arraycopy(paramArrayOfByte1, paramArrayOfByte1.length - 20, arrayOfByte2, 0, 20);
      arrayOfByte3 = new byte[paramArrayOfByte1.length - 40];
      MessageDigest localMessageDigest = MessageDigest.getInstance("SHA1");
      int i = 0;
      if (i < arrayOfByte3.length)
      {
        localMessageDigest.reset();
        localMessageDigest.update(paramArrayOfByte2);
        localMessageDigest.update(arrayOfByte1);
        localMessageDigest.digest(arrayOfByte1, 0, arrayOfByte1.length);
        int k = 0;
        int j = i;
        for (;;)
        {
          i = j;
          if (k >= arrayOfByte1.length) {
            break;
          }
          i = j;
          if (j >= arrayOfByte3.length) {
            break;
          }
          arrayOfByte3[j] = ((byte)(arrayOfByte1[k] ^ paramArrayOfByte1[(j + 20)]));
          j += 1;
          k += 1;
        }
      }
      localMessageDigest.reset();
      localMessageDigest.update(paramArrayOfByte2);
      localMessageDigest.update(arrayOfByte3);
      if (!MessageDigest.isEqual(arrayOfByte2, localMessageDigest.digest())) {
        throw new UnrecoverableKeyException("checksum mismatch");
      }
    }
    catch (Exception paramArrayOfByte1)
    {
      throw new UnrecoverableKeyException(paramArrayOfByte1.getMessage());
    }
    return arrayOfByte3;
  }
  
  private static byte[] encryptKey(Key paramKey, byte[] paramArrayOfByte)
    throws KeyStoreException
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("SHA1");
      SecureRandom.getInstance("SHA1PRNG");
      paramKey = paramKey.getEncoded();
      byte[] arrayOfByte1 = new byte[paramKey.length + 40];
      byte[] arrayOfByte2 = SecureRandom.getSeed(20);
      System.arraycopy(arrayOfByte2, 0, arrayOfByte1, 0, 20);
      int i = 0;
      if (i < paramKey.length)
      {
        localMessageDigest.reset();
        localMessageDigest.update(paramArrayOfByte);
        localMessageDigest.update(arrayOfByte2);
        localMessageDigest.digest(arrayOfByte2, 0, arrayOfByte2.length);
        int k = 0;
        int j = i;
        for (;;)
        {
          i = j;
          if (k >= arrayOfByte2.length) {
            break;
          }
          i = j;
          if (j >= paramKey.length) {
            break;
          }
          arrayOfByte1[(j + 20)] = ((byte)(arrayOfByte2[k] ^ paramKey[j]));
          j += 1;
          k += 1;
        }
      }
      localMessageDigest.reset();
      localMessageDigest.update(paramArrayOfByte);
      localMessageDigest.update(paramKey);
      localMessageDigest.digest(arrayOfByte1, arrayOfByte1.length - 20, 20);
      paramKey = new EncryptedPrivateKeyInfo("1.3.6.1.4.1.42.2.17.1.1", arrayOfByte1).getEncoded();
      return paramKey;
    }
    catch (Exception paramKey)
    {
      throw new KeyStoreException(paramKey.getMessage());
    }
  }
  
  private static Certificate readCert(DataInputStream paramDataInputStream)
    throws IOException, CertificateException, NoSuchAlgorithmException
  {
    String str = paramDataInputStream.readUTF();
    byte[] arrayOfByte = new byte[paramDataInputStream.readInt()];
    paramDataInputStream.read(arrayOfByte);
    return CertificateFactory.getInstance(str).generateCertificate(new ByteArrayInputStream(arrayOfByte));
  }
  
  private static void writeCert(DataOutputStream paramDataOutputStream, Certificate paramCertificate)
    throws IOException, CertificateException
  {
    paramDataOutputStream.writeUTF(paramCertificate.getType());
    paramCertificate = paramCertificate.getEncoded();
    paramDataOutputStream.writeInt(paramCertificate.length);
    paramDataOutputStream.write(paramCertificate);
  }
  
  public Enumeration engineAliases()
  {
    return this.aliases.elements();
  }
  
  public boolean engineContainsAlias(String paramString)
  {
    paramString = paramString.toLowerCase();
    return this.aliases.contains(paramString);
  }
  
  public void engineDeleteEntry(String paramString)
    throws KeyStoreException
  {
    paramString = paramString.toLowerCase();
    this.aliases.remove(paramString);
  }
  
  public Certificate engineGetCertificate(String paramString)
  {
    paramString = paramString.toLowerCase();
    if (engineIsKeyEntry(paramString))
    {
      Certificate[] arrayOfCertificate = (Certificate[])this.certChains.get(paramString);
      if ((arrayOfCertificate != null) && (arrayOfCertificate.length > 0)) {
        return arrayOfCertificate[0];
      }
    }
    return (Certificate)this.trustedCerts.get(paramString);
  }
  
  public String engineGetCertificateAlias(Certificate paramCertificate)
  {
    Iterator localIterator = this.trustedCerts.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if (paramCertificate.equals(this.trustedCerts.get(str))) {
        return str;
      }
    }
    return null;
  }
  
  public Certificate[] engineGetCertificateChain(String paramString)
  {
    paramString = paramString.toLowerCase();
    return (Certificate[])this.certChains.get(paramString);
  }
  
  public Date engineGetCreationDate(String paramString)
  {
    paramString = paramString.toLowerCase();
    return (Date)this.dates.get(paramString);
  }
  
  public Key engineGetKey(String paramString, char[] paramArrayOfChar)
    throws NoSuchAlgorithmException, UnrecoverableKeyException
  {
    paramString = paramString.toLowerCase();
    if (!this.privateKeys.containsKey(paramString)) {
      return null;
    }
    paramArrayOfChar = decryptKey((byte[])this.privateKeys.get(paramString), charsToBytes(paramArrayOfChar));
    Certificate[] arrayOfCertificate = engineGetCertificateChain(paramString);
    if (arrayOfCertificate.length > 0) {
      try
      {
        paramString = KeyFactory.getInstance(arrayOfCertificate[0].getPublicKey().getAlgorithm()).generatePrivate(new PKCS8EncodedKeySpec(paramArrayOfChar));
        return paramString;
      }
      catch (InvalidKeySpecException paramString)
      {
        throw new UnrecoverableKeyException(paramString.getMessage());
      }
    }
    return new SecretKeySpec(paramArrayOfChar, paramString);
  }
  
  public boolean engineIsCertificateEntry(String paramString)
  {
    paramString = paramString.toLowerCase();
    return this.trustedCerts.containsKey(paramString);
  }
  
  public boolean engineIsKeyEntry(String paramString)
  {
    paramString = paramString.toLowerCase();
    return this.privateKeys.containsKey(paramString);
  }
  
  public void engineLoad(InputStream paramInputStream, char[] paramArrayOfChar)
    throws IOException, NoSuchAlgorithmException, CertificateException
  {
    MessageDigest localMessageDigest = MessageDigest.getInstance("SHA");
    if (paramArrayOfChar != null) {
      localMessageDigest.update(charsToBytes(paramArrayOfChar));
    }
    localMessageDigest.update("Mighty Aphrodite".getBytes("UTF-8"));
    this.aliases.clear();
    this.trustedCerts.clear();
    this.privateKeys.clear();
    this.certChains.clear();
    this.dates.clear();
    if (paramInputStream == null) {}
    Object localObject1;
    do
    {
      return;
      paramInputStream = new DataInputStream(new DigestInputStream(paramInputStream, localMessageDigest));
      if (paramInputStream.readInt() != -17957139) {
        throw new IOException("not a JavaKeyStore");
      }
      paramInputStream.readInt();
      int k = paramInputStream.readInt();
      this.aliases.ensureCapacity(k);
      if (k < 0) {
        throw new IOException("negative entry count");
      }
      int i = 0;
      if (i < k)
      {
        int j = paramInputStream.readInt();
        localObject1 = paramInputStream.readUTF();
        this.aliases.add(localObject1);
        this.dates.put(localObject1, new Date(paramInputStream.readLong()));
        switch (j)
        {
        default: 
          throw new IOException("malformed key store");
        case 1: 
          Object localObject2 = new byte[paramInputStream.readInt()];
          paramInputStream.read((byte[])localObject2);
          this.privateKeys.put(localObject1, localObject2);
          int m = paramInputStream.readInt();
          localObject2 = new Certificate[m];
          j = 0;
          while (j < m)
          {
            localObject2[j] = readCert(paramInputStream);
            j += 1;
          }
          this.certChains.put(localObject1, localObject2);
        }
        for (;;)
        {
          i += 1;
          break;
          this.trustedCerts.put(localObject1, readCert(paramInputStream));
        }
      }
      localObject1 = new byte[20];
      paramInputStream.read((byte[])localObject1);
    } while ((paramArrayOfChar == null) || (!MessageDigest.isEqual((byte[])localObject1, localMessageDigest.digest())));
    throw new IOException("signature not verified");
  }
  
  public void engineSetCertificateEntry(String paramString, Certificate paramCertificate)
    throws KeyStoreException
  {
    paramString = paramString.toLowerCase();
    if (this.privateKeys.containsKey(paramString)) {
      throw new KeyStoreException("\"" + paramString + "\" is a private key entry");
    }
    if (paramCertificate == null) {
      throw new NullPointerException();
    }
    this.trustedCerts.put(paramString, paramCertificate);
    if (!this.aliases.contains(paramString))
    {
      this.dates.put(paramString, new Date());
      this.aliases.add(paramString);
    }
  }
  
  public void engineSetKeyEntry(String paramString, Key paramKey, char[] paramArrayOfChar, Certificate[] paramArrayOfCertificate)
    throws KeyStoreException
  {
    paramString = paramString.toLowerCase();
    if (this.trustedCerts.containsKey(paramString)) {
      throw new KeyStoreException("\"" + paramString + " is a trusted certificate entry");
    }
    this.privateKeys.put(paramString, encryptKey(paramKey, charsToBytes(paramArrayOfChar)));
    if (paramArrayOfCertificate != null) {
      this.certChains.put(paramString, paramArrayOfCertificate);
    }
    for (;;)
    {
      if (!this.aliases.contains(paramString))
      {
        this.dates.put(paramString, new Date());
        this.aliases.add(paramString);
      }
      return;
      this.certChains.put(paramString, new Certificate[0]);
    }
  }
  
  public void engineSetKeyEntry(String paramString, byte[] paramArrayOfByte, Certificate[] paramArrayOfCertificate)
    throws KeyStoreException
  {
    paramString = paramString.toLowerCase();
    if (this.trustedCerts.containsKey(paramString)) {
      throw new KeyStoreException("\"" + paramString + "\" is a trusted certificate entry");
    }
    for (;;)
    {
      try
      {
        new EncryptedPrivateKeyInfo(paramArrayOfByte);
        this.privateKeys.put(paramString, paramArrayOfByte);
        if (paramArrayOfCertificate != null)
        {
          this.certChains.put(paramString, paramArrayOfCertificate);
          if (!this.aliases.contains(paramString))
          {
            this.dates.put(paramString, new Date());
            this.aliases.add(paramString);
          }
          return;
        }
      }
      catch (IOException paramString)
      {
        throw new KeyStoreException("encoded key is not an EncryptedPrivateKeyInfo");
      }
      this.certChains.put(paramString, new Certificate[0]);
    }
  }
  
  public int engineSize()
  {
    return this.aliases.size();
  }
  
  public void engineStore(OutputStream paramOutputStream, char[] paramArrayOfChar)
    throws IOException, NoSuchAlgorithmException, CertificateException
  {
    MessageDigest localMessageDigest = MessageDigest.getInstance("SHA1");
    localMessageDigest.update(charsToBytes(paramArrayOfChar));
    localMessageDigest.update("Mighty Aphrodite".getBytes("UTF-8"));
    paramOutputStream = new DataOutputStream(new DigestOutputStream(paramOutputStream, localMessageDigest));
    paramOutputStream.writeInt(-17957139);
    paramOutputStream.writeInt(2);
    paramOutputStream.writeInt(this.aliases.size());
    paramArrayOfChar = this.aliases.elements();
    while (paramArrayOfChar.hasMoreElements())
    {
      Object localObject = (String)paramArrayOfChar.nextElement();
      if (this.trustedCerts.containsKey(localObject))
      {
        paramOutputStream.writeInt(2);
        paramOutputStream.writeUTF((String)localObject);
        paramOutputStream.writeLong(((Date)this.dates.get(localObject)).getTime());
        writeCert(paramOutputStream, (Certificate)this.trustedCerts.get(localObject));
      }
      else
      {
        paramOutputStream.writeInt(1);
        paramOutputStream.writeUTF((String)localObject);
        paramOutputStream.writeLong(((Date)this.dates.get(localObject)).getTime());
        byte[] arrayOfByte = (byte[])this.privateKeys.get(localObject);
        paramOutputStream.writeInt(arrayOfByte.length);
        paramOutputStream.write(arrayOfByte);
        localObject = (Certificate[])this.certChains.get(localObject);
        paramOutputStream.writeInt(localObject.length);
        int i = 0;
        while (i < localObject.length)
        {
          writeCert(paramOutputStream, localObject[i]);
          i += 1;
        }
      }
    }
    paramOutputStream.write(localMessageDigest.digest());
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/optional/JKS.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */