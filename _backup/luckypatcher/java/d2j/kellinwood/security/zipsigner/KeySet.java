package kellinwood.security.zipsigner;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;

public class KeySet
{
  String name;
  PrivateKey privateKey = null;
  X509Certificate publicKey = null;
  byte[] sigBlockTemplate = null;
  String signatureAlgorithm = "SHA1withRSA";
  
  public KeySet() {}
  
  public KeySet(String paramString1, X509Certificate paramX509Certificate, PrivateKey paramPrivateKey, String paramString2, byte[] paramArrayOfByte)
  {
    this.name = paramString1;
    this.publicKey = paramX509Certificate;
    this.privateKey = paramPrivateKey;
    if (paramString2 != null) {
      this.signatureAlgorithm = paramString2;
    }
    this.sigBlockTemplate = paramArrayOfByte;
  }
  
  public KeySet(String paramString, X509Certificate paramX509Certificate, PrivateKey paramPrivateKey, byte[] paramArrayOfByte)
  {
    this.name = paramString;
    this.publicKey = paramX509Certificate;
    this.privateKey = paramPrivateKey;
    this.sigBlockTemplate = paramArrayOfByte;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public PrivateKey getPrivateKey()
  {
    return this.privateKey;
  }
  
  public X509Certificate getPublicKey()
  {
    return this.publicKey;
  }
  
  public byte[] getSigBlockTemplate()
  {
    return this.sigBlockTemplate;
  }
  
  public String getSignatureAlgorithm()
  {
    return this.signatureAlgorithm;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setPrivateKey(PrivateKey paramPrivateKey)
  {
    this.privateKey = paramPrivateKey;
  }
  
  public void setPublicKey(X509Certificate paramX509Certificate)
  {
    this.publicKey = paramX509Certificate;
  }
  
  public void setSigBlockTemplate(byte[] paramArrayOfByte)
  {
    this.sigBlockTemplate = paramArrayOfByte;
  }
  
  public void setSignatureAlgorithm(String paramString)
  {
    if (paramString == null) {
      return;
    }
    this.signatureAlgorithm = paramString;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/KeySet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */