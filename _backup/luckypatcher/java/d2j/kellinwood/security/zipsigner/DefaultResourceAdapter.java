package kellinwood.security.zipsigner;

public class DefaultResourceAdapter
  implements ResourceAdapter
{
  public String getString(ResourceAdapter.Item paramItem, Object... paramVarArgs)
  {
    switch (paramItem)
    {
    default: 
      throw new IllegalArgumentException("Unknown item " + paramItem);
    case ???: 
      return "Input and output files are the same.  Specify a different name for the output.";
    case ???: 
      return "Unable to auto-select key for signing " + paramVarArgs[0];
    case ???: 
      return "Loading certificate and private key";
    case ???: 
      return "Parsing the input's central directory";
    case ???: 
      return "Generating manifest";
    case ???: 
      return "Generating signature file";
    case ???: 
      return "Generating signature block file";
    }
    return String.format("Copying zip entry %d of %d", new Object[] { paramVarArgs[0], paramVarArgs[1] });
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/security/zipsigner/DefaultResourceAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */