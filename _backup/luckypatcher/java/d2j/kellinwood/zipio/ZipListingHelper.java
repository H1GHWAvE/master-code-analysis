package kellinwood.zipio;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import kellinwood.logging.LoggerInterface;

public class ZipListingHelper
{
  static DateFormat dateFormat = new SimpleDateFormat("MM-dd-yy HH:mm");
  
  public static void listEntry(LoggerInterface paramLoggerInterface, ZioEntry paramZioEntry)
  {
    int i = 0;
    if (paramZioEntry.getSize() > 0) {
      i = (paramZioEntry.getSize() - paramZioEntry.getCompressedSize()) * 100 / paramZioEntry.getSize();
    }
    int j = paramZioEntry.getSize();
    if (paramZioEntry.getCompression() == 0) {}
    for (String str = "Stored";; str = "Defl:N")
    {
      paramLoggerInterface.debug(String.format("%8d  %6s %8d %4d%% %s  %08x  %s", new Object[] { Integer.valueOf(j), str, Integer.valueOf(paramZioEntry.getCompressedSize()), Integer.valueOf(i), dateFormat.format(new Date(paramZioEntry.getTime())), Integer.valueOf(paramZioEntry.getCrc32()), paramZioEntry.getName() }));
      return;
    }
  }
  
  public static void listHeader(LoggerInterface paramLoggerInterface)
  {
    paramLoggerInterface.debug(" Length   Method    Size  Ratio   Date   Time   CRC-32    Name");
    paramLoggerInterface.debug("--------  ------  ------- -----   ----   ----   ------    ----");
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/zipio/ZipListingHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */