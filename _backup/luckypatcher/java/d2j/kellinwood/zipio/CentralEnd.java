package kellinwood.zipio;

import java.io.IOException;
import kellinwood.logging.LoggerInterface;
import kellinwood.logging.LoggerManager;

public class CentralEnd
{
  private static LoggerInterface log;
  public int centralDirectorySize;
  public short centralStartDisk = 0;
  public int centralStartOffset;
  public String fileComment;
  public short numCentralEntries;
  public short numberThisDisk = 0;
  public int signature = 101010256;
  public short totalCentralEntries;
  
  private void doRead(ZipInput paramZipInput)
    throws IOException
  {
    boolean bool = getLogger().isDebugEnabled();
    this.numberThisDisk = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("This disk number: 0x%04x", new Object[] { Short.valueOf(this.numberThisDisk) }));
    }
    this.centralStartDisk = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("Central dir start disk number: 0x%04x", new Object[] { Short.valueOf(this.centralStartDisk) }));
    }
    this.numCentralEntries = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("Central entries on this disk: 0x%04x", new Object[] { Short.valueOf(this.numCentralEntries) }));
    }
    this.totalCentralEntries = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("Total number of central entries: 0x%04x", new Object[] { Short.valueOf(this.totalCentralEntries) }));
    }
    this.centralDirectorySize = paramZipInput.readInt();
    if (bool) {
      log.debug(String.format("Central directory size: 0x%08x", new Object[] { Integer.valueOf(this.centralDirectorySize) }));
    }
    this.centralStartOffset = paramZipInput.readInt();
    if (bool) {
      log.debug(String.format("Central directory offset: 0x%08x", new Object[] { Integer.valueOf(this.centralStartOffset) }));
    }
    this.fileComment = paramZipInput.readString(paramZipInput.readShort());
    if (bool) {
      log.debug(".ZIP file comment: " + this.fileComment);
    }
  }
  
  public static LoggerInterface getLogger()
  {
    if (log == null) {
      log = LoggerManager.getLogger(CentralEnd.class.getName());
    }
    return log;
  }
  
  public static CentralEnd read(ZipInput paramZipInput)
    throws IOException
  {
    if (paramZipInput.readInt() != 101010256)
    {
      paramZipInput.seek(paramZipInput.getFilePointer() - 4L);
      return null;
    }
    CentralEnd localCentralEnd = new CentralEnd();
    localCentralEnd.doRead(paramZipInput);
    return localCentralEnd;
  }
  
  public void write(ZipOutput paramZipOutput)
    throws IOException
  {
    getLogger().isDebugEnabled();
    paramZipOutput.writeInt(this.signature);
    paramZipOutput.writeShort(this.numberThisDisk);
    paramZipOutput.writeShort(this.centralStartDisk);
    paramZipOutput.writeShort(this.numCentralEntries);
    paramZipOutput.writeShort(this.totalCentralEntries);
    paramZipOutput.writeInt(this.centralDirectorySize);
    paramZipOutput.writeInt(this.centralStartOffset);
    paramZipOutput.writeShort((short)this.fileComment.length());
    paramZipOutput.writeString(this.fileComment);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/zipio/CentralEnd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */