package kellinwood.zipio;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import kellinwood.logging.LoggerInterface;
import kellinwood.logging.LoggerManager;

public class ZioEntryInputStream
  extends InputStream
{
  boolean debug = this.log.isDebugEnabled();
  LoggerInterface log = LoggerManager.getLogger(getClass().getName());
  OutputStream monitor = null;
  int offset = 0;
  RandomAccessFile raf;
  boolean returnDummyByte = false;
  int size;
  
  public ZioEntryInputStream(ZioEntry paramZioEntry)
    throws IOException
  {
    this.size = paramZioEntry.getCompressedSize();
    this.raf = paramZioEntry.getZipInput().in;
    if (paramZioEntry.getDataPosition() >= 0L)
    {
      if (this.debug) {
        this.log.debug(String.format("Seeking to %d", new Object[] { Long.valueOf(paramZioEntry.getDataPosition()) }));
      }
      this.raf.seek(paramZioEntry.getDataPosition());
      return;
    }
    paramZioEntry.readLocalHeader();
  }
  
  private int readBytes(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    int i;
    if (this.size - this.offset == 0) {
      if (this.returnDummyByte)
      {
        this.returnDummyByte = false;
        paramArrayOfByte[paramInt1] = 0;
        i = 1;
      }
    }
    int j;
    do
    {
      return i;
      return -1;
      i = Math.min(paramInt2, available());
      j = this.raf.read(paramArrayOfByte, paramInt1, i);
      if (j > 0)
      {
        if (this.monitor != null) {
          this.monitor.write(paramArrayOfByte, paramInt1, j);
        }
        this.offset += j;
      }
      i = j;
    } while (!this.debug);
    this.log.debug(String.format("Read %d bytes for read(b,%d,%d)", new Object[] { Integer.valueOf(j), Integer.valueOf(paramInt1), Integer.valueOf(paramInt2) }));
    return j;
  }
  
  public int available()
    throws IOException
  {
    int j = this.size - this.offset;
    if (this.debug) {
      this.log.debug(String.format("Available = %d", new Object[] { Integer.valueOf(j) }));
    }
    int i = j;
    if (j == 0)
    {
      i = j;
      if (this.returnDummyByte) {
        i = 1;
      }
    }
    return i;
  }
  
  public void close()
    throws IOException
  {}
  
  public boolean markSupported()
  {
    return false;
  }
  
  public int read()
    throws IOException
  {
    int i = 0;
    if (this.size - this.offset == 0) {
      if (this.returnDummyByte) {
        this.returnDummyByte = false;
      }
    }
    int j;
    do
    {
      return i;
      return -1;
      j = this.raf.read();
      if (j >= 0)
      {
        if (this.monitor != null) {
          this.monitor.write(j);
        }
        if (this.debug) {
          this.log.debug("Read 1 byte");
        }
        this.offset += 1;
        return j;
      }
      i = j;
    } while (!this.debug);
    this.log.debug("Read 0 bytes");
    return j;
  }
  
  public int read(byte[] paramArrayOfByte)
    throws IOException
  {
    return readBytes(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    return readBytes(paramArrayOfByte, paramInt1, paramInt2);
  }
  
  public void setMonitorStream(OutputStream paramOutputStream)
  {
    this.monitor = paramOutputStream;
  }
  
  public void setReturnDummyByte(boolean paramBoolean)
  {
    this.returnDummyByte = paramBoolean;
  }
  
  public long skip(long paramLong)
    throws IOException
  {
    paramLong = Math.min(paramLong, available());
    this.raf.seek(this.raf.getFilePointer() + paramLong);
    if (this.debug) {
      this.log.debug(String.format("Skipped %d bytes", new Object[] { Long.valueOf(paramLong) }));
    }
    return paramLong;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/zipio/ZioEntryInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */