package kellinwood.zipio;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.SequenceInputStream;
import java.util.Date;
import java.util.zip.CRC32;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;
import kellinwood.logging.LoggerInterface;
import kellinwood.logging.LoggerManager;

public class ZioEntry
  implements Cloneable
{
  private static byte[] alignBytes = new byte[4];
  private static LoggerInterface log;
  private int compressedSize;
  private short compression;
  private int crc32;
  private byte[] data = null;
  private long dataPosition = -1L;
  private short diskNumberStart;
  private ZioEntryOutputStream entryOut = null;
  private int externalAttributes;
  private byte[] extraData;
  private String fileComment;
  private String filename;
  private short generalPurposeBits;
  private short internalAttributes;
  private int localHeaderOffset;
  private short modificationDate;
  private short modificationTime;
  private short numAlignBytes = 0;
  private int size;
  private short versionMadeBy;
  private short versionRequired;
  private ZipInput zipInput;
  
  public ZioEntry(String paramString)
  {
    this.filename = paramString;
    this.fileComment = "";
    this.compression = 8;
    this.extraData = new byte[0];
    setTime(System.currentTimeMillis());
  }
  
  public ZioEntry(String paramString1, String paramString2)
    throws IOException
  {
    this.zipInput = new ZipInput(paramString2);
    this.filename = paramString1;
    this.fileComment = "";
    this.compression = 0;
    this.size = ((int)this.zipInput.getFileLength());
    this.compressedSize = this.size;
    if (getLogger().isDebugEnabled()) {
      getLogger().debug(String.format("Computing CRC for %s, size=%d", new Object[] { paramString2, Integer.valueOf(this.size) }));
    }
    paramString1 = new CRC32();
    byte[] arrayOfByte = new byte['ᾠ'];
    int i = 0;
    while (i != this.size)
    {
      int j = this.zipInput.read(arrayOfByte, 0, Math.min(arrayOfByte.length, this.size - i));
      if (j > 0)
      {
        paramString1.update(arrayOfByte, 0, j);
        i += j;
      }
    }
    this.crc32 = ((int)paramString1.getValue());
    this.zipInput.seek(0L);
    this.dataPosition = 0L;
    this.extraData = new byte[0];
    setTime(new File(paramString2).lastModified());
  }
  
  public ZioEntry(String paramString1, String paramString2, short paramShort, int paramInt1, int paramInt2, int paramInt3)
    throws IOException
  {
    this.zipInput = new ZipInput(paramString2);
    this.filename = paramString1;
    this.fileComment = "";
    this.compression = paramShort;
    this.crc32 = paramInt1;
    this.compressedSize = paramInt2;
    this.size = paramInt3;
    this.dataPosition = 0L;
    this.extraData = new byte[0];
    setTime(new File(paramString2).lastModified());
  }
  
  public ZioEntry(ZipInput paramZipInput)
  {
    this.zipInput = paramZipInput;
  }
  
  private void doRead(ZipInput paramZipInput)
    throws IOException
  {
    boolean bool = getLogger().isDebugEnabled();
    this.versionMadeBy = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("Version made by: 0x%04x", new Object[] { Short.valueOf(this.versionMadeBy) }));
    }
    this.versionRequired = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("Version required: 0x%04x", new Object[] { Short.valueOf(this.versionRequired) }));
    }
    this.generalPurposeBits = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("General purpose bits: 0x%04x", new Object[] { Short.valueOf(this.generalPurposeBits) }));
    }
    if ((this.generalPurposeBits & 0xF7F1) != 0) {
      throw new IllegalStateException("Can't handle general purpose bits == " + String.format("0x%04x", new Object[] { Short.valueOf(this.generalPurposeBits) }));
    }
    this.compression = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("Compression: 0x%04x", new Object[] { Short.valueOf(this.compression) }));
    }
    this.modificationTime = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("Modification time: 0x%04x", new Object[] { Short.valueOf(this.modificationTime) }));
    }
    this.modificationDate = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("Modification date: 0x%04x", new Object[] { Short.valueOf(this.modificationDate) }));
    }
    this.crc32 = paramZipInput.readInt();
    if (bool) {
      log.debug(String.format("CRC-32: 0x%04x", new Object[] { Integer.valueOf(this.crc32) }));
    }
    this.compressedSize = paramZipInput.readInt();
    if (bool) {
      log.debug(String.format("Compressed size: 0x%04x", new Object[] { Integer.valueOf(this.compressedSize) }));
    }
    this.size = paramZipInput.readInt();
    if (bool) {
      log.debug(String.format("Size: 0x%04x", new Object[] { Integer.valueOf(this.size) }));
    }
    short s1 = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("File name length: 0x%04x", new Object[] { Short.valueOf(s1) }));
    }
    short s2 = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("Extra length: 0x%04x", new Object[] { Short.valueOf(s2) }));
    }
    short s3 = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("File comment length: 0x%04x", new Object[] { Short.valueOf(s3) }));
    }
    this.diskNumberStart = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("Disk number start: 0x%04x", new Object[] { Short.valueOf(this.diskNumberStart) }));
    }
    this.internalAttributes = paramZipInput.readShort();
    if (bool) {
      log.debug(String.format("Internal attributes: 0x%04x", new Object[] { Short.valueOf(this.internalAttributes) }));
    }
    this.externalAttributes = paramZipInput.readInt();
    if (bool) {
      log.debug(String.format("External attributes: 0x%08x", new Object[] { Integer.valueOf(this.externalAttributes) }));
    }
    this.localHeaderOffset = paramZipInput.readInt();
    if (bool) {
      log.debug(String.format("Local header offset: 0x%08x", new Object[] { Integer.valueOf(this.localHeaderOffset) }));
    }
    this.filename = paramZipInput.readString(s1);
    if (bool) {
      log.debug("Filename: " + this.filename);
    }
    this.extraData = paramZipInput.readBytes(s2);
    this.fileComment = paramZipInput.readString(s3);
    if (bool) {
      log.debug("File comment: " + this.fileComment);
    }
    this.generalPurposeBits = ((short)(this.generalPurposeBits & 0x800));
    if (this.size == 0)
    {
      this.compressedSize = 0;
      this.compression = 0;
      this.crc32 = 0;
    }
  }
  
  public static LoggerInterface getLogger()
  {
    if (log == null) {
      log = LoggerManager.getLogger(ZioEntry.class.getName());
    }
    return log;
  }
  
  public static ZioEntry read(ZipInput paramZipInput)
    throws IOException
  {
    if (paramZipInput.readInt() != 33639248)
    {
      paramZipInput.seek(paramZipInput.getFilePointer() - 4L);
      return null;
    }
    ZioEntry localZioEntry = new ZioEntry(paramZipInput);
    localZioEntry.doRead(paramZipInput);
    return localZioEntry;
  }
  
  public ZioEntry getClonedEntry(String paramString)
  {
    try
    {
      ZioEntry localZioEntry = (ZioEntry)clone();
      localZioEntry.setName(paramString);
      return localZioEntry;
    }
    catch (CloneNotSupportedException paramString)
    {
      throw new IllegalStateException("clone() failed!");
    }
  }
  
  public int getCompressedSize()
  {
    return this.compressedSize;
  }
  
  public short getCompression()
  {
    return this.compression;
  }
  
  public int getCrc32()
  {
    return this.crc32;
  }
  
  public byte[] getData()
    throws IOException
  {
    Object localObject;
    if (this.data != null)
    {
      localObject = this.data;
      return (byte[])localObject;
    }
    byte[] arrayOfByte = new byte[this.size];
    InputStream localInputStream = getInputStream();
    int i = 0;
    for (;;)
    {
      localObject = arrayOfByte;
      if (i == this.size) {
        break;
      }
      int j = localInputStream.read(arrayOfByte, i, this.size - i);
      if (j < 0) {
        throw new IllegalStateException(String.format("Read failed, expecting %d bytes, got %d instead", new Object[] { Integer.valueOf(this.size), Integer.valueOf(i) }));
      }
      i += j;
    }
  }
  
  public long getDataPosition()
  {
    return this.dataPosition;
  }
  
  public short getDiskNumberStart()
  {
    return this.diskNumberStart;
  }
  
  public ZioEntryOutputStream getEntryOut()
  {
    return this.entryOut;
  }
  
  public int getExternalAttributes()
  {
    return this.externalAttributes;
  }
  
  public byte[] getExtraData()
  {
    return this.extraData;
  }
  
  public String getFileComment()
  {
    return this.fileComment;
  }
  
  public short getGeneralPurposeBits()
  {
    return this.generalPurposeBits;
  }
  
  public InputStream getInputStream()
    throws IOException
  {
    return getInputStream(null);
  }
  
  public InputStream getInputStream(OutputStream paramOutputStream)
    throws IOException
  {
    if (this.entryOut != null)
    {
      this.entryOut.close();
      this.size = this.entryOut.getSize();
      this.data = ((ByteArrayOutputStream)this.entryOut.getWrappedStream()).toByteArray();
      this.compressedSize = this.data.length;
      this.crc32 = this.entryOut.getCRC();
      this.entryOut = null;
      paramOutputStream = new ByteArrayInputStream(this.data);
      if (this.compression == 0) {
        return paramOutputStream;
      }
      return new InflaterInputStream(new SequenceInputStream(paramOutputStream, new ByteArrayInputStream(new byte[1])), new Inflater(true));
    }
    ZioEntryInputStream localZioEntryInputStream = new ZioEntryInputStream(this);
    if (paramOutputStream != null) {
      localZioEntryInputStream.setMonitorStream(paramOutputStream);
    }
    if (this.compression != 0)
    {
      localZioEntryInputStream.setReturnDummyByte(true);
      return new InflaterInputStream(localZioEntryInputStream, new Inflater(true));
    }
    return localZioEntryInputStream;
  }
  
  public short getInternalAttributes()
  {
    return this.internalAttributes;
  }
  
  public int getLocalHeaderOffset()
  {
    return this.localHeaderOffset;
  }
  
  public String getName()
  {
    return this.filename;
  }
  
  public OutputStream getOutputStream()
  {
    this.entryOut = new ZioEntryOutputStream(this.compression, new ByteArrayOutputStream());
    return this.entryOut;
  }
  
  public int getSize()
  {
    return this.size;
  }
  
  public long getTime()
  {
    return new Date((this.modificationDate >> 9 & 0x7F) + 80, (this.modificationDate >> 5 & 0xF) - 1, this.modificationDate & 0x1F, this.modificationTime >> 11 & 0x1F, this.modificationTime >> 5 & 0x3F, this.modificationTime << 1 & 0x3E).getTime();
  }
  
  public short getVersionMadeBy()
  {
    return this.versionMadeBy;
  }
  
  public short getVersionRequired()
  {
    return this.versionRequired;
  }
  
  public ZipInput getZipInput()
  {
    return this.zipInput;
  }
  
  public boolean isDirectory()
  {
    return this.filename.endsWith("/");
  }
  
  public void readLocalHeader()
    throws IOException
  {
    ZipInput localZipInput = this.zipInput;
    boolean bool = getLogger().isDebugEnabled();
    localZipInput.seek(this.localHeaderOffset);
    if (bool) {
      getLogger().debug(String.format("FILE POSITION: 0x%08x", new Object[] { Long.valueOf(localZipInput.getFilePointer()) }));
    }
    if (localZipInput.readInt() != 67324752) {
      throw new IllegalStateException(String.format("Local header not found at pos=0x%08x, file=%s", new Object[] { Long.valueOf(localZipInput.getFilePointer()), this.filename }));
    }
    short s1 = localZipInput.readShort();
    if (bool) {
      log.debug(String.format("Version required: 0x%04x", new Object[] { Short.valueOf(s1) }));
    }
    s1 = localZipInput.readShort();
    if (bool) {
      log.debug(String.format("General purpose bits: 0x%04x", new Object[] { Short.valueOf(s1) }));
    }
    s1 = localZipInput.readShort();
    if (bool) {
      log.debug(String.format("Compression: 0x%04x", new Object[] { Short.valueOf(s1) }));
    }
    s1 = localZipInput.readShort();
    if (bool) {
      log.debug(String.format("Modification time: 0x%04x", new Object[] { Short.valueOf(s1) }));
    }
    s1 = localZipInput.readShort();
    if (bool) {
      log.debug(String.format("Modification date: 0x%04x", new Object[] { Short.valueOf(s1) }));
    }
    int i = localZipInput.readInt();
    if (bool) {
      log.debug(String.format("CRC-32: 0x%04x", new Object[] { Integer.valueOf(i) }));
    }
    i = localZipInput.readInt();
    if (bool) {
      log.debug(String.format("Compressed size: 0x%04x", new Object[] { Integer.valueOf(i) }));
    }
    i = localZipInput.readInt();
    if (bool) {
      log.debug(String.format("Size: 0x%04x", new Object[] { Integer.valueOf(i) }));
    }
    s1 = localZipInput.readShort();
    if (bool) {
      log.debug(String.format("File name length: 0x%04x", new Object[] { Short.valueOf(s1) }));
    }
    short s2 = localZipInput.readShort();
    if (bool) {
      log.debug(String.format("Extra length: 0x%04x", new Object[] { Short.valueOf(s2) }));
    }
    String str = localZipInput.readString(s1);
    if (bool) {
      log.debug("Filename: " + str);
    }
    localZipInput.readBytes(s2);
    this.dataPosition = localZipInput.getFilePointer();
    if (bool) {
      log.debug(String.format("Data position: 0x%08x", new Object[] { Long.valueOf(this.dataPosition) }));
    }
  }
  
  public void setCompression(int paramInt)
  {
    this.compression = ((short)paramInt);
  }
  
  public void setName(String paramString)
  {
    this.filename = paramString;
  }
  
  public void setTime(long paramLong)
  {
    Date localDate = new Date(paramLong);
    int i = localDate.getYear() + 1900;
    if (i < 1980) {}
    for (paramLong = 2162688L;; paramLong = i - 1980 << 25 | localDate.getMonth() + 1 << 21 | localDate.getDate() << 16 | localDate.getHours() << 11 | localDate.getMinutes() << 5 | localDate.getSeconds() >> 1)
    {
      this.modificationDate = ((short)(int)(paramLong >> 16));
      this.modificationTime = ((short)(int)(0xFFFF & paramLong));
      return;
    }
  }
  
  public void write(ZipOutput paramZipOutput)
    throws IOException
  {
    getLogger().isDebugEnabled();
    paramZipOutput.writeInt(33639248);
    paramZipOutput.writeShort(this.versionMadeBy);
    paramZipOutput.writeShort(this.versionRequired);
    paramZipOutput.writeShort(this.generalPurposeBits);
    paramZipOutput.writeShort(this.compression);
    paramZipOutput.writeShort(this.modificationTime);
    paramZipOutput.writeShort(this.modificationDate);
    paramZipOutput.writeInt(this.crc32);
    paramZipOutput.writeInt(this.compressedSize);
    paramZipOutput.writeInt(this.size);
    paramZipOutput.writeShort((short)this.filename.length());
    paramZipOutput.writeShort((short)(this.extraData.length + this.numAlignBytes));
    paramZipOutput.writeShort((short)this.fileComment.length());
    paramZipOutput.writeShort(this.diskNumberStart);
    paramZipOutput.writeShort(this.internalAttributes);
    paramZipOutput.writeInt(this.externalAttributes);
    paramZipOutput.writeInt(this.localHeaderOffset);
    paramZipOutput.writeString(this.filename);
    paramZipOutput.writeBytes(this.extraData);
    if (this.numAlignBytes > 0) {
      paramZipOutput.writeBytes(alignBytes, 0, this.numAlignBytes);
    }
    paramZipOutput.writeString(this.fileComment);
  }
  
  public void writeLocalEntry(ZipOutput paramZipOutput)
    throws IOException
  {
    if ((this.data == null) && (this.dataPosition < 0L) && (this.zipInput != null)) {
      readLocalHeader();
    }
    this.localHeaderOffset = paramZipOutput.getFilePointer();
    boolean bool = getLogger().isDebugEnabled();
    if (bool) {
      getLogger().debug(String.format("Writing local header at 0x%08x - %s", new Object[] { Integer.valueOf(this.localHeaderOffset), this.filename }));
    }
    if (this.entryOut != null)
    {
      this.entryOut.close();
      this.size = this.entryOut.getSize();
      this.data = ((ByteArrayOutputStream)this.entryOut.getWrappedStream()).toByteArray();
      this.compressedSize = this.data.length;
      this.crc32 = this.entryOut.getCRC();
    }
    paramZipOutput.writeInt(67324752);
    paramZipOutput.writeShort(this.versionRequired);
    paramZipOutput.writeShort(this.generalPurposeBits);
    paramZipOutput.writeShort(this.compression);
    paramZipOutput.writeShort(this.modificationTime);
    paramZipOutput.writeShort(this.modificationDate);
    paramZipOutput.writeInt(this.crc32);
    paramZipOutput.writeInt(this.compressedSize);
    paramZipOutput.writeInt(this.size);
    paramZipOutput.writeShort((short)this.filename.length());
    this.numAlignBytes = 0;
    int i;
    if (this.compression == 0)
    {
      i = (short)(int)((paramZipOutput.getFilePointer() + 2 + this.filename.length() + this.extraData.length) % 4L);
      if (i > 0) {
        this.numAlignBytes = ((short)(4 - i));
      }
    }
    paramZipOutput.writeShort((short)(this.extraData.length + this.numAlignBytes));
    paramZipOutput.writeString(this.filename);
    paramZipOutput.writeBytes(this.extraData);
    if (this.numAlignBytes > 0) {
      paramZipOutput.writeBytes(alignBytes, 0, this.numAlignBytes);
    }
    if (bool) {
      getLogger().debug(String.format("Data position 0x%08x", new Object[] { Integer.valueOf(paramZipOutput.getFilePointer()) }));
    }
    if (this.data != null)
    {
      paramZipOutput.writeBytes(this.data);
      if (bool) {
        getLogger().debug(String.format("Wrote %d bytes", new Object[] { Integer.valueOf(this.data.length) }));
      }
    }
    long l;
    for (;;)
    {
      return;
      if (bool) {
        getLogger().debug(String.format("Seeking to position 0x%08x", new Object[] { Long.valueOf(this.dataPosition) }));
      }
      this.zipInput.seek(this.dataPosition);
      i = Math.min(this.compressedSize, 8096);
      byte[] arrayOfByte = new byte[i];
      int j;
      for (l = 0L; l != this.compressedSize; l += j)
      {
        j = this.zipInput.in.read(arrayOfByte, 0, (int)Math.min(this.compressedSize - l, i));
        if (j <= 0) {
          break label569;
        }
        paramZipOutput.writeBytes(arrayOfByte, 0, j);
        if (bool) {
          getLogger().debug(String.format("Wrote %d bytes", new Object[] { Integer.valueOf(j) }));
        }
      }
    }
    label569:
    throw new IllegalStateException(String.format("EOF reached while copying %s with %d bytes left to go", new Object[] { this.filename, Long.valueOf(this.compressedSize - l) }));
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/zipio/ZioEntry.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */