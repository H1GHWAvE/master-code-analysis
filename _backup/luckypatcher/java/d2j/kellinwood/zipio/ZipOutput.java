package kellinwood.zipio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import kellinwood.logging.LoggerInterface;
import kellinwood.logging.LoggerManager;

public class ZipOutput
{
  static LoggerInterface log;
  List<ZioEntry> entriesWritten = new LinkedList();
  int filePointer = 0;
  Set<String> namesWritten = new HashSet();
  OutputStream out = null;
  String outputFilename;
  
  public ZipOutput(File paramFile)
    throws IOException
  {
    this.outputFilename = paramFile.getAbsolutePath();
    init(paramFile);
  }
  
  public ZipOutput(OutputStream paramOutputStream)
    throws IOException
  {
    this.out = paramOutputStream;
  }
  
  public ZipOutput(String paramString)
    throws IOException
  {
    this.outputFilename = paramString;
    init(new File(this.outputFilename));
  }
  
  private static LoggerInterface getLogger()
  {
    if (log == null) {
      log = LoggerManager.getLogger(ZipOutput.class.getName());
    }
    return log;
  }
  
  private void init(File paramFile)
    throws IOException
  {
    if (paramFile.exists()) {
      paramFile.delete();
    }
    this.out = new FileOutputStream(paramFile);
    if (getLogger().isDebugEnabled()) {
      ZipListingHelper.listHeader(getLogger());
    }
  }
  
  public void close()
    throws IOException
  {
    CentralEnd localCentralEnd = new CentralEnd();
    localCentralEnd.centralStartOffset = getFilePointer();
    short s = (short)this.entriesWritten.size();
    localCentralEnd.totalCentralEntries = s;
    localCentralEnd.numCentralEntries = s;
    Iterator localIterator = this.entriesWritten.iterator();
    while (localIterator.hasNext()) {
      ((ZioEntry)localIterator.next()).write(this);
    }
    localCentralEnd.centralDirectorySize = (getFilePointer() - localCentralEnd.centralStartOffset);
    localCentralEnd.fileComment = "";
    localCentralEnd.write(this);
    if (this.out != null) {}
    try
    {
      this.out.close();
      return;
    }
    catch (Throwable localThrowable) {}
  }
  
  public int getFilePointer()
    throws IOException
  {
    return this.filePointer;
  }
  
  public void write(ZioEntry paramZioEntry)
    throws IOException
  {
    String str = paramZioEntry.getName();
    if (this.namesWritten.contains(str)) {
      getLogger().warning("Skipping duplicate file in output: " + str);
    }
    do
    {
      return;
      paramZioEntry.writeLocalEntry(this);
      this.entriesWritten.add(paramZioEntry);
      this.namesWritten.add(str);
    } while (!getLogger().isDebugEnabled());
    ZipListingHelper.listEntry(getLogger(), paramZioEntry);
  }
  
  public void writeBytes(byte[] paramArrayOfByte)
    throws IOException
  {
    this.out.write(paramArrayOfByte);
    this.filePointer += paramArrayOfByte.length;
  }
  
  public void writeBytes(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    this.out.write(paramArrayOfByte, paramInt1, paramInt2);
    this.filePointer += paramInt2;
  }
  
  public void writeInt(int paramInt)
    throws IOException
  {
    byte[] arrayOfByte = new byte[4];
    int j = 0;
    int i = paramInt;
    paramInt = j;
    while (paramInt < 4)
    {
      arrayOfByte[paramInt] = ((byte)(i & 0xFF));
      i >>= 8;
      paramInt += 1;
    }
    this.out.write(arrayOfByte);
    this.filePointer += 4;
  }
  
  public void writeShort(short paramShort)
    throws IOException
  {
    byte[] arrayOfByte = new byte[2];
    short s = 0;
    int i = paramShort;
    paramShort = s;
    while (paramShort < 2)
    {
      arrayOfByte[paramShort] = ((byte)(i & 0xFF));
      i = (short)(i >> 8);
      paramShort += 1;
    }
    this.out.write(arrayOfByte);
    this.filePointer += 2;
  }
  
  public void writeString(String paramString)
    throws IOException
  {
    paramString = paramString.getBytes();
    this.out.write(paramString);
    this.filePointer += paramString.length;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/kellinwood/zipio/ZipOutput.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */