package android.support.v4.view.accessibility;

import android.view.accessibility.AccessibilityNodeInfo;

class AccessibilityNodeInfoCompatJellybeanMr2
{
  public static String getViewIdResourceName(Object paramObject)
  {
    return ((AccessibilityNodeInfo)paramObject).getViewIdResourceName();
  }
  
  public static void setViewIdResourceName(Object paramObject, String paramString)
  {
    ((AccessibilityNodeInfo)paramObject).setViewIdResourceName(paramString);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/android/support/v4/view/accessibility/AccessibilityNodeInfoCompatJellybeanMr2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */