package android.support.v4.os;

import android.os.Parcelable.Creator;

class ParcelableCompatCreatorHoneycombMR2Stub
{
  static <T> Parcelable.Creator<T> instantiate(ParcelableCompatCreatorCallbacks<T> paramParcelableCompatCreatorCallbacks)
  {
    return new ParcelableCompatCreatorHoneycombMR2(paramParcelableCompatCreatorCallbacks);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/android/support/v4/os/ParcelableCompatCreatorHoneycombMR2Stub.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */