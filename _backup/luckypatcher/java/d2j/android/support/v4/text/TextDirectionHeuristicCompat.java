package android.support.v4.text;

public abstract interface TextDirectionHeuristicCompat
{
  public abstract boolean isRtl(CharSequence paramCharSequence, int paramInt1, int paramInt2);
  
  public abstract boolean isRtl(char[] paramArrayOfChar, int paramInt1, int paramInt2);
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/android/support/v4/text/TextDirectionHeuristicCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */