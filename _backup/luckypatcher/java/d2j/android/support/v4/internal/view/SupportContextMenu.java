package android.support.v4.internal.view;

import android.graphics.drawable.Drawable;
import android.view.ContextMenu;
import android.view.View;

public abstract interface SupportContextMenu
  extends SupportMenu, ContextMenu
{
  public abstract void clearHeader();
  
  public abstract SupportContextMenu setHeaderIcon(int paramInt);
  
  public abstract SupportContextMenu setHeaderIcon(Drawable paramDrawable);
  
  public abstract SupportContextMenu setHeaderTitle(int paramInt);
  
  public abstract SupportContextMenu setHeaderTitle(CharSequence paramCharSequence);
  
  public abstract SupportContextMenu setHeaderView(View paramView);
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/android/support/v4/internal/view/SupportContextMenu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */