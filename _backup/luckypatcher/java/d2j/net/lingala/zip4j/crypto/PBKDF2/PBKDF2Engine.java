package net.lingala.zip4j.crypto.PBKDF2;

import net.lingala.zip4j.util.Raw;

public class PBKDF2Engine
{
  protected PBKDF2Parameters parameters;
  protected PRF prf;
  
  public PBKDF2Engine()
  {
    this.parameters = null;
    this.prf = null;
  }
  
  public PBKDF2Engine(PBKDF2Parameters paramPBKDF2Parameters)
  {
    this.parameters = paramPBKDF2Parameters;
    this.prf = null;
  }
  
  public PBKDF2Engine(PBKDF2Parameters paramPBKDF2Parameters, PRF paramPRF)
  {
    this.parameters = paramPBKDF2Parameters;
    this.prf = paramPRF;
  }
  
  protected void INT(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    paramArrayOfByte[(paramInt1 + 0)] = ((byte)(paramInt2 / 16777216));
    paramArrayOfByte[(paramInt1 + 1)] = ((byte)(paramInt2 / 65536));
    paramArrayOfByte[(paramInt1 + 2)] = ((byte)(paramInt2 / 256));
    paramArrayOfByte[(paramInt1 + 3)] = ((byte)paramInt2);
  }
  
  protected byte[] PBKDF2(PRF paramPRF, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    byte[] arrayOfByte = paramArrayOfByte;
    if (paramArrayOfByte == null) {
      arrayOfByte = new byte[0];
    }
    int k = paramPRF.getHLen();
    int m = ceil(paramInt2, k);
    paramArrayOfByte = new byte[m * k];
    int j = 0;
    int i = 1;
    while (i <= m)
    {
      _F(paramArrayOfByte, j, paramPRF, arrayOfByte, paramInt1, i);
      j += k;
      i += 1;
    }
    if (paramInt2 - (m - 1) * k < k)
    {
      paramPRF = new byte[paramInt2];
      System.arraycopy(paramArrayOfByte, 0, paramPRF, 0, paramInt2);
      return paramPRF;
    }
    return paramArrayOfByte;
  }
  
  protected void _F(byte[] paramArrayOfByte1, int paramInt1, PRF paramPRF, byte[] paramArrayOfByte2, int paramInt2, int paramInt3)
  {
    int i = paramPRF.getHLen();
    byte[] arrayOfByte2 = new byte[i];
    byte[] arrayOfByte1 = new byte[paramArrayOfByte2.length + 4];
    System.arraycopy(paramArrayOfByte2, 0, arrayOfByte1, 0, paramArrayOfByte2.length);
    INT(arrayOfByte1, paramArrayOfByte2.length, paramInt3);
    paramInt3 = 0;
    paramArrayOfByte2 = arrayOfByte1;
    while (paramInt3 < paramInt2)
    {
      paramArrayOfByte2 = paramPRF.doFinal(paramArrayOfByte2);
      xor(arrayOfByte2, paramArrayOfByte2);
      paramInt3 += 1;
    }
    System.arraycopy(arrayOfByte2, 0, paramArrayOfByte1, paramInt1, i);
  }
  
  protected void assertPRF(byte[] paramArrayOfByte)
  {
    if (this.prf == null) {
      this.prf = new MacBasedPRF(this.parameters.getHashAlgorithm());
    }
    this.prf.init(paramArrayOfByte);
  }
  
  protected int ceil(int paramInt1, int paramInt2)
  {
    int i = 0;
    if (paramInt1 % paramInt2 > 0) {
      i = 1;
    }
    return paramInt1 / paramInt2 + i;
  }
  
  public byte[] deriveKey(char[] paramArrayOfChar)
  {
    return deriveKey(paramArrayOfChar, 0);
  }
  
  public byte[] deriveKey(char[] paramArrayOfChar, int paramInt)
  {
    if (paramArrayOfChar == null) {
      throw new NullPointerException();
    }
    assertPRF(Raw.convertCharArrayToByteArray(paramArrayOfChar));
    int i = paramInt;
    if (paramInt == 0) {
      i = this.prf.getHLen();
    }
    return PBKDF2(this.prf, this.parameters.getSalt(), this.parameters.getIterationCount(), i);
  }
  
  public PBKDF2Parameters getParameters()
  {
    return this.parameters;
  }
  
  public PRF getPseudoRandomFunction()
  {
    return this.prf;
  }
  
  public void setParameters(PBKDF2Parameters paramPBKDF2Parameters)
  {
    this.parameters = paramPBKDF2Parameters;
  }
  
  public void setPseudoRandomFunction(PRF paramPRF)
  {
    this.prf = paramPRF;
  }
  
  public boolean verifyKey(char[] paramArrayOfChar)
  {
    byte[] arrayOfByte = getParameters().getDerivedKey();
    if ((arrayOfByte == null) || (arrayOfByte.length == 0)) {}
    do
    {
      return false;
      paramArrayOfChar = deriveKey(paramArrayOfChar, arrayOfByte.length);
    } while ((paramArrayOfChar == null) || (paramArrayOfChar.length != arrayOfByte.length));
    int i = 0;
    for (;;)
    {
      if (i >= paramArrayOfChar.length) {
        break label62;
      }
      if (paramArrayOfChar[i] != arrayOfByte[i]) {
        break;
      }
      i += 1;
    }
    label62:
    return true;
  }
  
  protected void xor(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    int i = 0;
    while (i < paramArrayOfByte1.length)
    {
      paramArrayOfByte1[i] = ((byte)(paramArrayOfByte1[i] ^ paramArrayOfByte2[i]));
      i += 1;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */