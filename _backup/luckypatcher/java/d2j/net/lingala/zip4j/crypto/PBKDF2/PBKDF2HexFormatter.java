package net.lingala.zip4j.crypto.PBKDF2;

class PBKDF2HexFormatter
{
  public boolean fromString(PBKDF2Parameters paramPBKDF2Parameters, String paramString)
  {
    if ((paramPBKDF2Parameters == null) || (paramString == null)) {
      return true;
    }
    Object localObject = paramString.split(":");
    if ((localObject == null) || (localObject.length != 3)) {
      return true;
    }
    paramString = BinTools.hex2bin(localObject[0]);
    int i = Integer.parseInt(localObject[1]);
    localObject = BinTools.hex2bin(localObject[2]);
    paramPBKDF2Parameters.setSalt(paramString);
    paramPBKDF2Parameters.setIterationCount(i);
    paramPBKDF2Parameters.setDerivedKey((byte[])localObject);
    return false;
  }
  
  public String toString(PBKDF2Parameters paramPBKDF2Parameters)
  {
    return BinTools.bin2hex(paramPBKDF2Parameters.getSalt()) + ":" + String.valueOf(paramPBKDF2Parameters.getIterationCount()) + ":" + BinTools.bin2hex(paramPBKDF2Parameters.getDerivedKey());
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/crypto/PBKDF2/PBKDF2HexFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */