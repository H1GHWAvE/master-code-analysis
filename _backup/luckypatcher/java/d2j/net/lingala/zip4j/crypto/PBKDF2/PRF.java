package net.lingala.zip4j.crypto.PBKDF2;

abstract interface PRF
{
  public abstract byte[] doFinal(byte[] paramArrayOfByte);
  
  public abstract int getHLen();
  
  public abstract void init(byte[] paramArrayOfByte);
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/crypto/PBKDF2/PRF.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */