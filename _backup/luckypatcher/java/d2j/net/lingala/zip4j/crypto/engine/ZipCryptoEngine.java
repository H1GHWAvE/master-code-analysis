package net.lingala.zip4j.crypto.engine;

public class ZipCryptoEngine
{
  private static final int[] CRC_TABLE = new int['Ā'];
  private final int[] keys = new int[3];
  
  static
  {
    int i = 0;
    while (i < 256)
    {
      int j = i;
      int k = 0;
      if (k < 8)
      {
        if ((j & 0x1) == 1) {
          j = j >>> 1 ^ 0xEDB88320;
        }
        for (;;)
        {
          k += 1;
          break;
          j >>>= 1;
        }
      }
      CRC_TABLE[i] = j;
      i += 1;
    }
  }
  
  private int crc32(int paramInt, byte paramByte)
  {
    return paramInt >>> 8 ^ CRC_TABLE[((paramInt ^ paramByte) & 0xFF)];
  }
  
  public byte decryptByte()
  {
    int i = this.keys[2] | 0x2;
    return (byte)((i ^ 0x1) * i >>> 8);
  }
  
  public void initKeys(char[] paramArrayOfChar)
  {
    this.keys[0] = 305419896;
    this.keys[1] = 591751049;
    this.keys[2] = 878082192;
    int i = 0;
    while (i < paramArrayOfChar.length)
    {
      updateKeys((byte)(paramArrayOfChar[i] & 0xFF));
      i += 1;
    }
  }
  
  public void updateKeys(byte paramByte)
  {
    this.keys[0] = crc32(this.keys[0], paramByte);
    int[] arrayOfInt = this.keys;
    arrayOfInt[1] += (this.keys[0] & 0xFF);
    this.keys[1] = (this.keys[1] * 134775813 + 1);
    this.keys[2] = crc32(this.keys[2], (byte)(this.keys[1] >> 24));
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/crypto/engine/ZipCryptoEngine.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */