package net.lingala.zip4j.crypto.PBKDF2;

class BinTools
{
  public static final String hex = "0123456789ABCDEF";
  
  public static String bin2hex(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte == null) {
      return "";
    }
    StringBuffer localStringBuffer = new StringBuffer(paramArrayOfByte.length * 2);
    int j = paramArrayOfByte.length;
    int i = 0;
    while (i < j)
    {
      int k = (paramArrayOfByte[i] + 256) % 256;
      localStringBuffer.append("0123456789ABCDEF".charAt(k / 16 & 0xF));
      localStringBuffer.append("0123456789ABCDEF".charAt(k % 16 & 0xF));
      i += 1;
    }
    return localStringBuffer.toString();
  }
  
  public static int hex2bin(char paramChar)
  {
    if ((paramChar >= '0') && (paramChar <= '9')) {
      return paramChar - '0';
    }
    if ((paramChar >= 'A') && (paramChar <= 'F')) {
      return paramChar - 'A' + 10;
    }
    if ((paramChar >= 'a') && (paramChar <= 'f')) {
      return paramChar - 'a' + 10;
    }
    throw new IllegalArgumentException("Input string may only contain hex digits, but found '" + paramChar + "'");
  }
  
  public static byte[] hex2bin(String paramString)
  {
    String str = paramString;
    if (paramString == null) {
      str = "";
    }
    for (;;)
    {
      paramString = new byte[str.length() / 2];
      int j = 0;
      int i = 0;
      while (j < str.length())
      {
        int k = j + 1;
        char c1 = str.charAt(j);
        j = k + 1;
        char c2 = str.charAt(k);
        paramString[i] = ((byte)(hex2bin(c1) * 16 + hex2bin(c2)));
        i += 1;
      }
      if (paramString.length() % 2 != 0) {
        str = "0" + paramString;
      }
    }
    return paramString;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/crypto/PBKDF2/BinTools.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */