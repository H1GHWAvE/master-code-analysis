package net.lingala.zip4j.crypto;

import net.lingala.zip4j.crypto.engine.ZipCryptoEngine;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;

public class StandardDecrypter
  implements IDecrypter
{
  private byte[] crc = new byte[4];
  private FileHeader fileHeader;
  private ZipCryptoEngine zipCryptoEngine;
  
  public StandardDecrypter(FileHeader paramFileHeader, byte[] paramArrayOfByte)
    throws ZipException
  {
    if (paramFileHeader == null) {
      throw new ZipException("one of more of the input parameters were null in StandardDecryptor");
    }
    this.fileHeader = paramFileHeader;
    this.zipCryptoEngine = new ZipCryptoEngine();
    init(paramArrayOfByte);
  }
  
  public int decryptData(byte[] paramArrayOfByte)
    throws ZipException
  {
    return decryptData(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public int decryptData(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws ZipException
  {
    if ((paramInt1 < 0) || (paramInt2 < 0)) {
      throw new ZipException("one of the input parameters were null in standard decrpyt data");
    }
    int i = paramInt1;
    while (i < paramInt1 + paramInt2)
    {
      int j = paramArrayOfByte[i];
      try
      {
        j = (this.zipCryptoEngine.decryptByte() ^ j & 0xFF) & 0xFF;
        this.zipCryptoEngine.updateKeys((byte)j);
        paramArrayOfByte[i] = ((byte)j);
        i += 1;
      }
      catch (Exception paramArrayOfByte)
      {
        throw new ZipException(paramArrayOfByte);
      }
    }
    return paramInt2;
  }
  
  public void init(byte[] paramArrayOfByte)
    throws ZipException
  {
    byte[] arrayOfByte = this.fileHeader.getCrcBuff();
    this.crc[3] = ((byte)(arrayOfByte[3] & 0xFF));
    this.crc[2] = ((byte)(arrayOfByte[3] >> 8 & 0xFF));
    this.crc[1] = ((byte)(arrayOfByte[3] >> 16 & 0xFF));
    this.crc[0] = ((byte)(arrayOfByte[3] >> 24 & 0xFF));
    if ((this.crc[2] > 0) || (this.crc[1] > 0) || (this.crc[0] > 0)) {
      throw new IllegalStateException("Invalid CRC in File Header");
    }
    if ((this.fileHeader.getPassword() == null) || (this.fileHeader.getPassword().length <= 0)) {
      throw new ZipException("Wrong password!", 5);
    }
    this.zipCryptoEngine.initKeys(this.fileHeader.getPassword());
    int j = paramArrayOfByte[0];
    int i = 0;
    while (i < 12) {
      try
      {
        this.zipCryptoEngine.updateKeys((byte)(this.zipCryptoEngine.decryptByte() ^ j));
        if (i + 1 != 12) {
          j = paramArrayOfByte[(i + 1)];
        }
        i += 1;
      }
      catch (Exception paramArrayOfByte)
      {
        throw new ZipException(paramArrayOfByte);
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/crypto/StandardDecrypter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */