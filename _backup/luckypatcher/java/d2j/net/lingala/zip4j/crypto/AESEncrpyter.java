package net.lingala.zip4j.crypto;

import java.util.Random;
import net.lingala.zip4j.crypto.PBKDF2.MacBasedPRF;
import net.lingala.zip4j.crypto.PBKDF2.PBKDF2Engine;
import net.lingala.zip4j.crypto.PBKDF2.PBKDF2Parameters;
import net.lingala.zip4j.crypto.engine.AESEngine;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.util.Raw;

public class AESEncrpyter
  implements IEncrypter
{
  private int KEY_LENGTH;
  private int MAC_LENGTH;
  private final int PASSWORD_VERIFIER_LENGTH = 2;
  private int SALT_LENGTH;
  private AESEngine aesEngine;
  private byte[] aesKey;
  private byte[] counterBlock;
  private byte[] derivedPasswordVerifier;
  private boolean finished;
  private byte[] iv;
  private int keyStrength;
  private int loopCount = 0;
  private MacBasedPRF mac;
  private byte[] macKey;
  private int nonce = 1;
  private char[] password;
  private byte[] saltBytes;
  
  public AESEncrpyter(char[] paramArrayOfChar, int paramInt)
    throws ZipException
  {
    if ((paramArrayOfChar == null) || (paramArrayOfChar.length == 0)) {
      throw new ZipException("input password is empty or null in AES encrypter constructor");
    }
    if ((paramInt != 1) && (paramInt != 3)) {
      throw new ZipException("Invalid key strength in AES encrypter constructor");
    }
    this.password = paramArrayOfChar;
    this.keyStrength = paramInt;
    this.finished = false;
    this.counterBlock = new byte[16];
    this.iv = new byte[16];
    init();
  }
  
  private byte[] deriveKey(byte[] paramArrayOfByte, char[] paramArrayOfChar)
    throws ZipException
  {
    try
    {
      paramArrayOfByte = new PBKDF2Engine(new PBKDF2Parameters("HmacSHA1", "ISO-8859-1", paramArrayOfByte, 1000)).deriveKey(paramArrayOfChar, this.KEY_LENGTH + this.MAC_LENGTH + 2);
      return paramArrayOfByte;
    }
    catch (Exception paramArrayOfByte)
    {
      throw new ZipException(paramArrayOfByte);
    }
  }
  
  private static byte[] generateSalt(int paramInt)
    throws ZipException
  {
    if ((paramInt != 8) && (paramInt != 16)) {
      throw new ZipException("invalid salt size, cannot generate salt");
    }
    int i = 0;
    if (paramInt == 8) {
      i = 2;
    }
    if (paramInt == 16) {
      i = 4;
    }
    byte[] arrayOfByte = new byte[paramInt];
    paramInt = 0;
    while (paramInt < i)
    {
      int j = new Random().nextInt();
      arrayOfByte[(paramInt * 4 + 0)] = ((byte)(j >> 24));
      arrayOfByte[(paramInt * 4 + 1)] = ((byte)(j >> 16));
      arrayOfByte[(paramInt * 4 + 2)] = ((byte)(j >> 8));
      arrayOfByte[(paramInt * 4 + 3)] = ((byte)j);
      paramInt += 1;
    }
    return arrayOfByte;
  }
  
  private void init()
    throws ZipException
  {
    switch (this.keyStrength)
    {
    case 2: 
    default: 
      throw new ZipException("invalid aes key strength, cannot determine key sizes");
    case 1: 
      this.KEY_LENGTH = 16;
      this.MAC_LENGTH = 16;
    }
    byte[] arrayOfByte;
    for (this.SALT_LENGTH = 8;; this.SALT_LENGTH = 16)
    {
      this.saltBytes = generateSalt(this.SALT_LENGTH);
      arrayOfByte = deriveKey(this.saltBytes, this.password);
      if ((arrayOfByte != null) && (arrayOfByte.length == this.KEY_LENGTH + this.MAC_LENGTH + 2)) {
        break;
      }
      throw new ZipException("invalid key generated, cannot decrypt file");
      this.KEY_LENGTH = 32;
      this.MAC_LENGTH = 32;
    }
    this.aesKey = new byte[this.KEY_LENGTH];
    this.macKey = new byte[this.MAC_LENGTH];
    this.derivedPasswordVerifier = new byte[2];
    System.arraycopy(arrayOfByte, 0, this.aesKey, 0, this.KEY_LENGTH);
    System.arraycopy(arrayOfByte, this.KEY_LENGTH, this.macKey, 0, this.MAC_LENGTH);
    System.arraycopy(arrayOfByte, this.KEY_LENGTH + this.MAC_LENGTH, this.derivedPasswordVerifier, 0, 2);
    this.aesEngine = new AESEngine(this.aesKey);
    this.mac = new MacBasedPRF("HmacSHA1");
    this.mac.init(this.macKey);
  }
  
  public int encryptData(byte[] paramArrayOfByte)
    throws ZipException
  {
    if (paramArrayOfByte == null) {
      throw new ZipException("input bytes are null, cannot perform AES encrpytion");
    }
    return encryptData(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public int encryptData(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws ZipException
  {
    if (this.finished) {
      throw new ZipException("AES Encrypter is in finished state (A non 16 byte block has already been passed to encrypter)");
    }
    if (paramInt2 % 16 != 0) {
      this.finished = true;
    }
    int i = paramInt1;
    while (i < paramInt1 + paramInt2)
    {
      if (i + 16 <= paramInt1 + paramInt2) {}
      for (int j = 16;; j = paramInt1 + paramInt2 - i)
      {
        this.loopCount = j;
        Raw.prepareBuffAESIVBytes(this.iv, this.nonce, 16);
        this.aesEngine.processBlock(this.iv, this.counterBlock);
        j = 0;
        while (j < this.loopCount)
        {
          paramArrayOfByte[(i + j)] = ((byte)(paramArrayOfByte[(i + j)] ^ this.counterBlock[j]));
          j += 1;
        }
      }
      this.mac.update(paramArrayOfByte, i, this.loopCount);
      this.nonce += 1;
      i += 16;
    }
    return paramInt2;
  }
  
  public byte[] getDerivedPasswordVerifier()
  {
    return this.derivedPasswordVerifier;
  }
  
  public byte[] getFinalMac()
  {
    byte[] arrayOfByte1 = this.mac.doFinal();
    byte[] arrayOfByte2 = new byte[10];
    System.arraycopy(arrayOfByte1, 0, arrayOfByte2, 0, 10);
    return arrayOfByte2;
  }
  
  public int getPasswordVeriifierLength()
  {
    return 2;
  }
  
  public byte[] getSaltBytes()
  {
    return this.saltBytes;
  }
  
  public int getSaltLength()
  {
    return this.SALT_LENGTH;
  }
  
  public void setDerivedPasswordVerifier(byte[] paramArrayOfByte)
  {
    this.derivedPasswordVerifier = paramArrayOfByte;
  }
  
  public void setSaltBytes(byte[] paramArrayOfByte)
  {
    this.saltBytes = paramArrayOfByte;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/crypto/AESEncrpyter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */