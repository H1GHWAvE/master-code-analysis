package net.lingala.zip4j.core;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.ZipInputStream;
import net.lingala.zip4j.model.CentralDirectory;
import net.lingala.zip4j.model.EndCentralDirRecord;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.UnzipParameters;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.progress.ProgressMonitor;
import net.lingala.zip4j.unzip.Unzip;
import net.lingala.zip4j.util.ArchiveMaintainer;
import net.lingala.zip4j.util.InternalZipConstants;
import net.lingala.zip4j.util.Zip4jUtil;
import net.lingala.zip4j.zip.ZipEngine;

public class ZipFile
{
  private String file;
  private String fileNameCharset;
  private boolean isEncrypted;
  private int mode;
  private ProgressMonitor progressMonitor;
  private boolean runInThread;
  private ZipModel zipModel;
  
  public ZipFile(File paramFile)
    throws ZipException
  {
    if (paramFile == null) {
      throw new ZipException("Input zip file parameter is not null", 1);
    }
    this.file = paramFile.getPath();
    this.mode = 2;
    this.progressMonitor = new ProgressMonitor();
    this.runInThread = false;
  }
  
  public ZipFile(String paramString)
    throws ZipException
  {
    this(new File(paramString));
  }
  
  private void addFolder(File paramFile, ZipParameters paramZipParameters, boolean paramBoolean)
    throws ZipException
  {
    checkZipModel();
    if (this.zipModel == null) {
      throw new ZipException("internal error: zip model is null");
    }
    if ((paramBoolean) && (this.zipModel.isSplitArchive())) {
      throw new ZipException("This is a split archive. Zip file format does not allow updating split/spanned files");
    }
    new ZipEngine(this.zipModel).addFolderToZip(paramFile, paramZipParameters, this.progressMonitor, this.runInThread);
  }
  
  private void checkZipModel()
    throws ZipException
  {
    if (this.zipModel == null)
    {
      if (Zip4jUtil.checkFileExists(this.file)) {
        readZipInfo();
      }
    }
    else {
      return;
    }
    createNewZipModel();
  }
  
  private void createNewZipModel()
  {
    this.zipModel = new ZipModel();
    this.zipModel.setZipFile(this.file);
    this.zipModel.setFileNameCharset(this.fileNameCharset);
  }
  
  /* Error */
  private void readZipInfo()
    throws ZipException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 36	net/lingala/zip4j/core/ZipFile:file	Ljava/lang/String;
    //   4: invokestatic 85	net/lingala/zip4j/util/Zip4jUtil:checkFileExists	(Ljava/lang/String;)Z
    //   7: ifne +13 -> 20
    //   10: new 20	net/lingala/zip4j/exception/ZipException
    //   13: dup
    //   14: ldc 106
    //   16: invokespecial 62	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   19: athrow
    //   20: aload_0
    //   21: getfield 36	net/lingala/zip4j/core/ZipFile:file	Ljava/lang/String;
    //   24: invokestatic 109	net/lingala/zip4j/util/Zip4jUtil:checkFileReadAccess	(Ljava/lang/String;)Z
    //   27: ifne +13 -> 40
    //   30: new 20	net/lingala/zip4j/exception/ZipException
    //   33: dup
    //   34: ldc 111
    //   36: invokespecial 62	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   39: athrow
    //   40: aload_0
    //   41: getfield 38	net/lingala/zip4j/core/ZipFile:mode	I
    //   44: iconst_2
    //   45: if_icmpeq +13 -> 58
    //   48: new 20	net/lingala/zip4j/exception/ZipException
    //   51: dup
    //   52: ldc 113
    //   54: invokespecial 62	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   57: athrow
    //   58: aconst_null
    //   59: astore_1
    //   60: aconst_null
    //   61: astore_3
    //   62: new 115	java/io/RandomAccessFile
    //   65: dup
    //   66: new 30	java/io/File
    //   69: dup
    //   70: aload_0
    //   71: getfield 36	net/lingala/zip4j/core/ZipFile:file	Ljava/lang/String;
    //   74: invokespecial 50	java/io/File:<init>	(Ljava/lang/String;)V
    //   77: ldc 117
    //   79: invokespecial 120	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   82: astore_2
    //   83: aload_0
    //   84: getfield 59	net/lingala/zip4j/core/ZipFile:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   87: ifnonnull +40 -> 127
    //   90: aload_0
    //   91: new 122	net/lingala/zip4j/core/HeaderReader
    //   94: dup
    //   95: aload_2
    //   96: invokespecial 125	net/lingala/zip4j/core/HeaderReader:<init>	(Ljava/io/RandomAccessFile;)V
    //   99: aload_0
    //   100: getfield 97	net/lingala/zip4j/core/ZipFile:fileNameCharset	Ljava/lang/String;
    //   103: invokevirtual 129	net/lingala/zip4j/core/HeaderReader:readAllHeaders	(Ljava/lang/String;)Lnet/lingala/zip4j/model/ZipModel;
    //   106: putfield 59	net/lingala/zip4j/core/ZipFile:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   109: aload_0
    //   110: getfield 59	net/lingala/zip4j/core/ZipFile:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   113: ifnull +14 -> 127
    //   116: aload_0
    //   117: getfield 59	net/lingala/zip4j/core/ZipFile:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   120: aload_0
    //   121: getfield 36	net/lingala/zip4j/core/ZipFile:file	Ljava/lang/String;
    //   124: invokevirtual 95	net/lingala/zip4j/model/ZipModel:setZipFile	(Ljava/lang/String;)V
    //   127: aload_2
    //   128: ifnull +7 -> 135
    //   131: aload_2
    //   132: invokevirtual 132	java/io/RandomAccessFile:close	()V
    //   135: return
    //   136: astore_2
    //   137: aload_3
    //   138: astore_1
    //   139: new 20	net/lingala/zip4j/exception/ZipException
    //   142: dup
    //   143: aload_2
    //   144: invokespecial 135	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   147: athrow
    //   148: astore_2
    //   149: aload_1
    //   150: ifnull +7 -> 157
    //   153: aload_1
    //   154: invokevirtual 132	java/io/RandomAccessFile:close	()V
    //   157: aload_2
    //   158: athrow
    //   159: astore_1
    //   160: return
    //   161: astore_1
    //   162: goto -5 -> 157
    //   165: astore_3
    //   166: aload_2
    //   167: astore_1
    //   168: aload_3
    //   169: astore_2
    //   170: goto -21 -> 149
    //   173: astore_3
    //   174: aload_2
    //   175: astore_1
    //   176: aload_3
    //   177: astore_2
    //   178: goto -39 -> 139
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	181	0	this	ZipFile
    //   59	95	1	localObject1	Object
    //   159	1	1	localIOException1	java.io.IOException
    //   161	1	1	localIOException2	java.io.IOException
    //   167	9	1	localObject2	Object
    //   82	50	2	localRandomAccessFile	java.io.RandomAccessFile
    //   136	8	2	localFileNotFoundException1	java.io.FileNotFoundException
    //   148	19	2	localObject3	Object
    //   169	9	2	localObject4	Object
    //   61	77	3	localObject5	Object
    //   165	4	3	localObject6	Object
    //   173	4	3	localFileNotFoundException2	java.io.FileNotFoundException
    // Exception table:
    //   from	to	target	type
    //   62	83	136	java/io/FileNotFoundException
    //   62	83	148	finally
    //   139	148	148	finally
    //   131	135	159	java/io/IOException
    //   153	157	161	java/io/IOException
    //   83	127	165	finally
    //   83	127	173	java/io/FileNotFoundException
  }
  
  public void addFile(File paramFile, ZipParameters paramZipParameters)
    throws ZipException
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(paramFile);
    addFiles(localArrayList, paramZipParameters);
  }
  
  public void addFiles(ArrayList paramArrayList, ZipParameters paramZipParameters)
    throws ZipException
  {
    checkZipModel();
    if (this.zipModel == null) {
      throw new ZipException("internal error: zip model is null");
    }
    if (paramArrayList == null) {
      throw new ZipException("input file ArrayList is null, cannot add files");
    }
    if (!Zip4jUtil.checkArrayListTypes(paramArrayList, 1)) {
      throw new ZipException("One or more elements in the input ArrayList is not of type File");
    }
    if (paramZipParameters == null) {
      throw new ZipException("input parameters are null, cannot add files to zip");
    }
    if (this.progressMonitor.getState() == 1) {
      throw new ZipException("invalid operation - Zip4j is in busy state");
    }
    if ((Zip4jUtil.checkFileExists(this.file)) && (this.zipModel.isSplitArchive())) {
      throw new ZipException("Zip file already exists. Zip file format does not allow updating split/spanned files");
    }
    new ZipEngine(this.zipModel).addFiles(paramArrayList, paramZipParameters, this.progressMonitor, this.runInThread);
  }
  
  public void addFolder(File paramFile, ZipParameters paramZipParameters)
    throws ZipException
  {
    if (paramFile == null) {
      throw new ZipException("input path is null, cannot add folder to zip file");
    }
    if (paramZipParameters == null) {
      throw new ZipException("input parameters are null, cannot add folder to zip file");
    }
    addFolder(paramFile, paramZipParameters, true);
  }
  
  public void addFolder(String paramString, ZipParameters paramZipParameters)
    throws ZipException
  {
    if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("input path is null or empty, cannot add folder to zip file");
    }
    addFolder(new File(paramString), paramZipParameters);
  }
  
  public void addStream(InputStream paramInputStream, ZipParameters paramZipParameters)
    throws ZipException
  {
    if (paramInputStream == null) {
      throw new ZipException("inputstream is null, cannot add file to zip");
    }
    if (paramZipParameters == null) {
      throw new ZipException("zip parameters are null");
    }
    setRunInThread(false);
    checkZipModel();
    if (this.zipModel == null) {
      throw new ZipException("internal error: zip model is null");
    }
    if ((Zip4jUtil.checkFileExists(this.file)) && (this.zipModel.isSplitArchive())) {
      throw new ZipException("Zip file already exists. Zip file format does not allow updating split/spanned files");
    }
    new ZipEngine(this.zipModel).addStreamToZip(paramInputStream, paramZipParameters);
  }
  
  public void createZipFile(File paramFile, ZipParameters paramZipParameters)
    throws ZipException
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(paramFile);
    createZipFile(localArrayList, paramZipParameters, false, -1L);
  }
  
  public void createZipFile(File paramFile, ZipParameters paramZipParameters, boolean paramBoolean, long paramLong)
    throws ZipException
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(paramFile);
    createZipFile(localArrayList, paramZipParameters, paramBoolean, paramLong);
  }
  
  public void createZipFile(ArrayList paramArrayList, ZipParameters paramZipParameters)
    throws ZipException
  {
    createZipFile(paramArrayList, paramZipParameters, false, -1L);
  }
  
  public void createZipFile(ArrayList paramArrayList, ZipParameters paramZipParameters, boolean paramBoolean, long paramLong)
    throws ZipException
  {
    if (!Zip4jUtil.isStringNotNullAndNotEmpty(this.file)) {
      throw new ZipException("zip file path is empty");
    }
    if (Zip4jUtil.checkFileExists(this.file)) {
      throw new ZipException("zip file: " + this.file + " already exists. To add files to existing zip file use addFile method");
    }
    if (paramArrayList == null) {
      throw new ZipException("input file ArrayList is null, cannot create zip file");
    }
    if (!Zip4jUtil.checkArrayListTypes(paramArrayList, 1)) {
      throw new ZipException("One or more elements in the input ArrayList is not of type File");
    }
    createNewZipModel();
    this.zipModel.setSplitArchive(paramBoolean);
    this.zipModel.setSplitLength(paramLong);
    addFiles(paramArrayList, paramZipParameters);
  }
  
  public void createZipFileFromFolder(File paramFile, ZipParameters paramZipParameters, boolean paramBoolean, long paramLong)
    throws ZipException
  {
    if (paramFile == null) {
      throw new ZipException("folderToAdd is null, cannot create zip file from folder");
    }
    if (paramZipParameters == null) {
      throw new ZipException("input parameters are null, cannot create zip file from folder");
    }
    if (Zip4jUtil.checkFileExists(this.file)) {
      throw new ZipException("zip file: " + this.file + " already exists. To add files to existing zip file use addFolder method");
    }
    createNewZipModel();
    this.zipModel.setSplitArchive(paramBoolean);
    if (paramBoolean) {
      this.zipModel.setSplitLength(paramLong);
    }
    addFolder(paramFile, paramZipParameters, false);
  }
  
  public void createZipFileFromFolder(String paramString, ZipParameters paramZipParameters, boolean paramBoolean, long paramLong)
    throws ZipException
  {
    if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("folderToAdd is empty or null, cannot create Zip File from folder");
    }
    createZipFileFromFolder(new File(paramString), paramZipParameters, paramBoolean, paramLong);
  }
  
  public void extractAll(String paramString)
    throws ZipException
  {
    extractAll(paramString, null);
  }
  
  public void extractAll(String paramString, UnzipParameters paramUnzipParameters)
    throws ZipException
  {
    if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("output path is null or invalid");
    }
    if (!Zip4jUtil.checkOutputFolder(paramString)) {
      throw new ZipException("invalid output path");
    }
    if (this.zipModel == null) {
      readZipInfo();
    }
    if (this.zipModel == null) {
      throw new ZipException("Internal error occurred when extracting zip file");
    }
    if (this.progressMonitor.getState() == 1) {
      throw new ZipException("invalid operation - Zip4j is in busy state");
    }
    new Unzip(this.zipModel).extractAll(paramUnzipParameters, paramString, this.progressMonitor, this.runInThread);
  }
  
  public void extractFile(String paramString1, String paramString2)
    throws ZipException
  {
    extractFile(paramString1, paramString2, null);
  }
  
  public void extractFile(String paramString1, String paramString2, UnzipParameters paramUnzipParameters)
    throws ZipException
  {
    extractFile(paramString1, paramString2, paramUnzipParameters, null);
  }
  
  public void extractFile(String paramString1, String paramString2, UnzipParameters paramUnzipParameters, String paramString3)
    throws ZipException
  {
    if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramString1)) {
      throw new ZipException("file to extract is null or empty, cannot extract file");
    }
    if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramString2)) {
      throw new ZipException("destination string path is empty or null, cannot extract file");
    }
    readZipInfo();
    paramString1 = Zip4jUtil.getFileHeader(this.zipModel, paramString1);
    if (paramString1 == null) {
      throw new ZipException("file header not found for given file name, cannot extract file");
    }
    if (this.progressMonitor.getState() == 1) {
      throw new ZipException("invalid operation - Zip4j is in busy state");
    }
    paramString1.extractFile(this.zipModel, paramString2, paramUnzipParameters, paramString3, this.progressMonitor, this.runInThread);
  }
  
  public void extractFile(FileHeader paramFileHeader, String paramString)
    throws ZipException
  {
    extractFile(paramFileHeader, paramString, null);
  }
  
  public void extractFile(FileHeader paramFileHeader, String paramString, UnzipParameters paramUnzipParameters)
    throws ZipException
  {
    extractFile(paramFileHeader, paramString, paramUnzipParameters, null);
  }
  
  public void extractFile(FileHeader paramFileHeader, String paramString1, UnzipParameters paramUnzipParameters, String paramString2)
    throws ZipException
  {
    if (paramFileHeader == null) {
      throw new ZipException("input file header is null, cannot extract file");
    }
    if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramString1)) {
      throw new ZipException("destination path is empty or null, cannot extract file");
    }
    readZipInfo();
    if (this.progressMonitor.getState() == 1) {
      throw new ZipException("invalid operation - Zip4j is in busy state");
    }
    paramFileHeader.extractFile(this.zipModel, paramString1, paramUnzipParameters, paramString2, this.progressMonitor, this.runInThread);
  }
  
  public String getComment()
    throws ZipException
  {
    return getComment(null);
  }
  
  public String getComment(String paramString)
    throws ZipException
  {
    String str = paramString;
    if (paramString == null) {
      if (!Zip4jUtil.isSupportedCharset("windows-1254")) {
        break label51;
      }
    }
    label51:
    for (str = "windows-1254"; Zip4jUtil.checkFileExists(this.file); str = InternalZipConstants.CHARSET_DEFAULT)
    {
      checkZipModel();
      if (this.zipModel != null) {
        break label69;
      }
      throw new ZipException("zip model is null, cannot read comment");
    }
    throw new ZipException("zip file does not exist, cannot read comment");
    label69:
    if (this.zipModel.getEndCentralDirRecord() == null) {
      throw new ZipException("end of central directory record is null, cannot read comment");
    }
    if ((this.zipModel.getEndCentralDirRecord().getCommentBytes() == null) || (this.zipModel.getEndCentralDirRecord().getCommentBytes().length <= 0)) {
      return null;
    }
    try
    {
      paramString = new String(this.zipModel.getEndCentralDirRecord().getCommentBytes(), str);
      return paramString;
    }
    catch (UnsupportedEncodingException paramString)
    {
      throw new ZipException(paramString);
    }
  }
  
  public File getFile()
  {
    return new File(this.file);
  }
  
  public FileHeader getFileHeader(String paramString)
    throws ZipException
  {
    if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("input file name is emtpy or null, cannot get FileHeader");
    }
    readZipInfo();
    if ((this.zipModel == null) || (this.zipModel.getCentralDirectory() == null)) {
      return null;
    }
    return Zip4jUtil.getFileHeader(this.zipModel, paramString);
  }
  
  public List getFileHeaders()
    throws ZipException
  {
    readZipInfo();
    if ((this.zipModel == null) || (this.zipModel.getCentralDirectory() == null)) {
      return null;
    }
    return this.zipModel.getCentralDirectory().getFileHeaders();
  }
  
  public ZipInputStream getInputStream(FileHeader paramFileHeader)
    throws ZipException
  {
    if (paramFileHeader == null) {
      throw new ZipException("FileHeader is null, cannot get InputStream");
    }
    checkZipModel();
    if (this.zipModel == null) {
      throw new ZipException("zip model is null, cannot get inputstream");
    }
    return new Unzip(this.zipModel).getInputStream(paramFileHeader);
  }
  
  public ProgressMonitor getProgressMonitor()
  {
    return this.progressMonitor;
  }
  
  public ArrayList getSplitZipFiles()
    throws ZipException
  {
    checkZipModel();
    return Zip4jUtil.getSplitZipFiles(this.zipModel);
  }
  
  public boolean isEncrypted()
    throws ZipException
  {
    if (this.zipModel == null)
    {
      readZipInfo();
      if (this.zipModel == null) {
        throw new ZipException("Zip Model is null");
      }
    }
    if ((this.zipModel.getCentralDirectory() == null) || (this.zipModel.getCentralDirectory().getFileHeaders() == null)) {
      throw new ZipException("invalid zip file");
    }
    ArrayList localArrayList = this.zipModel.getCentralDirectory().getFileHeaders();
    int i = 0;
    for (;;)
    {
      if (i < localArrayList.size())
      {
        FileHeader localFileHeader = (FileHeader)localArrayList.get(i);
        if ((localFileHeader != null) && (localFileHeader.isEncrypted())) {
          this.isEncrypted = true;
        }
      }
      else
      {
        return this.isEncrypted;
      }
      i += 1;
    }
  }
  
  public boolean isRunInThread()
  {
    return this.runInThread;
  }
  
  public boolean isSplitArchive()
    throws ZipException
  {
    if (this.zipModel == null)
    {
      readZipInfo();
      if (this.zipModel == null) {
        throw new ZipException("Zip Model is null");
      }
    }
    return this.zipModel.isSplitArchive();
  }
  
  public boolean isValidZipFile()
  {
    try
    {
      readZipInfo();
      return true;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public void mergeSplitFiles(File paramFile)
    throws ZipException
  {
    if (paramFile == null) {
      throw new ZipException("outputZipFile is null, cannot merge split files");
    }
    if (paramFile.exists()) {
      throw new ZipException("output Zip File already exists");
    }
    checkZipModel();
    if (this.zipModel == null) {
      throw new ZipException("zip model is null, corrupt zip file?");
    }
    ArchiveMaintainer localArchiveMaintainer = new ArchiveMaintainer();
    localArchiveMaintainer.initProgressMonitorForMergeOp(this.zipModel, this.progressMonitor);
    localArchiveMaintainer.mergeSplitZipFiles(this.zipModel, paramFile, this.progressMonitor, this.runInThread);
  }
  
  public void removeFile(String paramString)
    throws ZipException
  {
    if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("file name is empty or null, cannot remove file");
    }
    if ((this.zipModel == null) && (Zip4jUtil.checkFileExists(this.file))) {
      readZipInfo();
    }
    if (this.zipModel.isSplitArchive()) {
      throw new ZipException("Zip file format does not allow updating split/spanned files");
    }
    FileHeader localFileHeader = Zip4jUtil.getFileHeader(this.zipModel, paramString);
    if (localFileHeader == null) {
      throw new ZipException("could not find file header for file: " + paramString);
    }
    removeFile(localFileHeader);
  }
  
  public void removeFile(FileHeader paramFileHeader)
    throws ZipException
  {
    if (paramFileHeader == null) {
      throw new ZipException("file header is null, cannot remove file");
    }
    if ((this.zipModel == null) && (Zip4jUtil.checkFileExists(this.file))) {
      readZipInfo();
    }
    if (this.zipModel.isSplitArchive()) {
      throw new ZipException("Zip file format does not allow updating split/spanned files");
    }
    ArchiveMaintainer localArchiveMaintainer = new ArchiveMaintainer();
    localArchiveMaintainer.initProgressMonitorForRemoveOp(this.zipModel, paramFileHeader, this.progressMonitor);
    localArchiveMaintainer.removeZipFile(this.zipModel, paramFileHeader, this.progressMonitor, this.runInThread);
  }
  
  public void setComment(String paramString)
    throws ZipException
  {
    if (paramString == null) {
      throw new ZipException("input comment is null, cannot update zip file");
    }
    if (!Zip4jUtil.checkFileExists(this.file)) {
      throw new ZipException("zip file does not exist, cannot set comment for zip file");
    }
    readZipInfo();
    if (this.zipModel == null) {
      throw new ZipException("zipModel is null, cannot update zip file");
    }
    if (this.zipModel.getEndCentralDirRecord() == null) {
      throw new ZipException("end of central directory is null, cannot set comment");
    }
    new ArchiveMaintainer().setComment(this.zipModel, paramString);
  }
  
  public void setFileNameCharset(String paramString)
    throws ZipException
  {
    if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("null or empty charset name");
    }
    if (!Zip4jUtil.isSupportedCharset(paramString)) {
      throw new ZipException("unsupported charset: " + paramString);
    }
    this.fileNameCharset = paramString;
  }
  
  public void setPassword(String paramString)
    throws ZipException
  {
    if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramString)) {
      throw new NullPointerException();
    }
    setPassword(paramString.toCharArray());
  }
  
  public void setPassword(char[] paramArrayOfChar)
    throws ZipException
  {
    if (this.zipModel == null)
    {
      readZipInfo();
      if (this.zipModel == null) {
        throw new ZipException("Zip Model is null");
      }
    }
    if ((this.zipModel.getCentralDirectory() == null) || (this.zipModel.getCentralDirectory().getFileHeaders() == null)) {
      throw new ZipException("invalid zip file");
    }
    int i = 0;
    while (i < this.zipModel.getCentralDirectory().getFileHeaders().size())
    {
      if ((this.zipModel.getCentralDirectory().getFileHeaders().get(i) != null) && (((FileHeader)this.zipModel.getCentralDirectory().getFileHeaders().get(i)).isEncrypted())) {
        ((FileHeader)this.zipModel.getCentralDirectory().getFileHeaders().get(i)).setPassword(paramArrayOfChar);
      }
      i += 1;
    }
  }
  
  public void setRunInThread(boolean paramBoolean)
  {
    this.runInThread = paramBoolean;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/core/ZipFile.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */