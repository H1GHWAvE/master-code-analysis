package net.lingala.zip4j.core;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.AESExtraDataRecord;
import net.lingala.zip4j.model.CentralDirectory;
import net.lingala.zip4j.model.DigitalSignature;
import net.lingala.zip4j.model.EndCentralDirRecord;
import net.lingala.zip4j.model.ExtraDataRecord;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.LocalFileHeader;
import net.lingala.zip4j.model.Zip64EndCentralDirLocator;
import net.lingala.zip4j.model.Zip64EndCentralDirRecord;
import net.lingala.zip4j.model.Zip64ExtendedInfo;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.util.Raw;
import net.lingala.zip4j.util.Zip4jUtil;

public class HeaderReader
{
  private RandomAccessFile zip4jRaf = null;
  private ZipModel zipModel;
  
  public HeaderReader(RandomAccessFile paramRandomAccessFile)
  {
    this.zip4jRaf = paramRandomAccessFile;
  }
  
  private byte[] getLongByteFromIntByte(byte[] paramArrayOfByte)
    throws ZipException
  {
    if (paramArrayOfByte == null) {
      throw new ZipException("input parameter is null, cannot expand to 8 bytes");
    }
    if (paramArrayOfByte.length != 4) {
      throw new ZipException("invalid byte length, cannot expand to 8 bytes");
    }
    return new byte[] { paramArrayOfByte[0], paramArrayOfByte[1], paramArrayOfByte[2], paramArrayOfByte[3], 0, 0, 0, 0 };
  }
  
  private AESExtraDataRecord readAESExtraDataRecord(ArrayList paramArrayList)
    throws ZipException
  {
    if (paramArrayList == null) {
      return null;
    }
    int i = 0;
    label8:
    if (i < paramArrayList.size())
    {
      localObject = (ExtraDataRecord)paramArrayList.get(i);
      if (localObject != null) {
        break label36;
      }
    }
    label36:
    while (((ExtraDataRecord)localObject).getHeader() != 39169L)
    {
      i += 1;
      break label8;
      break;
    }
    if (((ExtraDataRecord)localObject).getData() == null) {
      throw new ZipException("corrput AES extra data records");
    }
    paramArrayList = new AESExtraDataRecord();
    paramArrayList.setSignature(39169L);
    paramArrayList.setDataSize(((ExtraDataRecord)localObject).getSizeOfData());
    Object localObject = ((ExtraDataRecord)localObject).getData();
    paramArrayList.setVersionNumber(Raw.readShortLittleEndian((byte[])localObject, 0));
    byte[] arrayOfByte = new byte[2];
    System.arraycopy(localObject, 2, arrayOfByte, 0, 2);
    paramArrayList.setVendorID(new String(arrayOfByte));
    paramArrayList.setAesStrength(localObject[4] & 0xFF);
    paramArrayList.setCompressionMethod(Raw.readShortLittleEndian((byte[])localObject, 5));
    return paramArrayList;
  }
  
  private void readAndSaveAESExtraDataRecord(FileHeader paramFileHeader)
    throws ZipException
  {
    if (paramFileHeader == null) {
      throw new ZipException("file header is null in reading Zip64 Extended Info");
    }
    if ((paramFileHeader.getExtraDataRecords() == null) || (paramFileHeader.getExtraDataRecords().size() <= 0)) {}
    AESExtraDataRecord localAESExtraDataRecord;
    do
    {
      return;
      localAESExtraDataRecord = readAESExtraDataRecord(paramFileHeader.getExtraDataRecords());
    } while (localAESExtraDataRecord == null);
    paramFileHeader.setAesExtraDataRecord(localAESExtraDataRecord);
    paramFileHeader.setEncryptionMethod(99);
  }
  
  private void readAndSaveAESExtraDataRecord(LocalFileHeader paramLocalFileHeader)
    throws ZipException
  {
    if (paramLocalFileHeader == null) {
      throw new ZipException("file header is null in reading Zip64 Extended Info");
    }
    if ((paramLocalFileHeader.getExtraDataRecords() == null) || (paramLocalFileHeader.getExtraDataRecords().size() <= 0)) {}
    AESExtraDataRecord localAESExtraDataRecord;
    do
    {
      return;
      localAESExtraDataRecord = readAESExtraDataRecord(paramLocalFileHeader.getExtraDataRecords());
    } while (localAESExtraDataRecord == null);
    paramLocalFileHeader.setAesExtraDataRecord(localAESExtraDataRecord);
    paramLocalFileHeader.setEncryptionMethod(99);
  }
  
  private void readAndSaveExtraDataRecord(FileHeader paramFileHeader)
    throws ZipException
  {
    if (this.zip4jRaf == null) {
      throw new ZipException("invalid file handler when trying to read extra data record");
    }
    if (paramFileHeader == null) {
      throw new ZipException("file header is null");
    }
    int i = paramFileHeader.getExtraFieldLength();
    if (i <= 0) {
      return;
    }
    paramFileHeader.setExtraDataRecords(readExtraDataRecords(i));
  }
  
  private void readAndSaveExtraDataRecord(LocalFileHeader paramLocalFileHeader)
    throws ZipException
  {
    if (this.zip4jRaf == null) {
      throw new ZipException("invalid file handler when trying to read extra data record");
    }
    if (paramLocalFileHeader == null) {
      throw new ZipException("file header is null");
    }
    int i = paramLocalFileHeader.getExtraFieldLength();
    if (i <= 0) {
      return;
    }
    paramLocalFileHeader.setExtraDataRecords(readExtraDataRecords(i));
  }
  
  private void readAndSaveZip64ExtendedInfo(FileHeader paramFileHeader)
    throws ZipException
  {
    if (paramFileHeader == null) {
      throw new ZipException("file header is null in reading Zip64 Extended Info");
    }
    if ((paramFileHeader.getExtraDataRecords() == null) || (paramFileHeader.getExtraDataRecords().size() <= 0)) {}
    Zip64ExtendedInfo localZip64ExtendedInfo;
    do
    {
      do
      {
        return;
        localZip64ExtendedInfo = readZip64ExtendedInfo(paramFileHeader.getExtraDataRecords(), paramFileHeader.getUncompressedSize(), paramFileHeader.getCompressedSize(), paramFileHeader.getOffsetLocalHeader(), paramFileHeader.getDiskNumberStart());
      } while (localZip64ExtendedInfo == null);
      paramFileHeader.setZip64ExtendedInfo(localZip64ExtendedInfo);
      if (localZip64ExtendedInfo.getUnCompressedSize() != -1L) {
        paramFileHeader.setUncompressedSize(localZip64ExtendedInfo.getUnCompressedSize());
      }
      if (localZip64ExtendedInfo.getCompressedSize() != -1L) {
        paramFileHeader.setCompressedSize(localZip64ExtendedInfo.getCompressedSize());
      }
      if (localZip64ExtendedInfo.getOffsetLocalHeader() != -1L) {
        paramFileHeader.setOffsetLocalHeader(localZip64ExtendedInfo.getOffsetLocalHeader());
      }
    } while (localZip64ExtendedInfo.getDiskNumberStart() == -1);
    paramFileHeader.setDiskNumberStart(localZip64ExtendedInfo.getDiskNumberStart());
  }
  
  private void readAndSaveZip64ExtendedInfo(LocalFileHeader paramLocalFileHeader)
    throws ZipException
  {
    if (paramLocalFileHeader == null) {
      throw new ZipException("file header is null in reading Zip64 Extended Info");
    }
    if ((paramLocalFileHeader.getExtraDataRecords() == null) || (paramLocalFileHeader.getExtraDataRecords().size() <= 0)) {}
    Zip64ExtendedInfo localZip64ExtendedInfo;
    do
    {
      do
      {
        return;
        localZip64ExtendedInfo = readZip64ExtendedInfo(paramLocalFileHeader.getExtraDataRecords(), paramLocalFileHeader.getUncompressedSize(), paramLocalFileHeader.getCompressedSize(), -1L, -1);
      } while (localZip64ExtendedInfo == null);
      paramLocalFileHeader.setZip64ExtendedInfo(localZip64ExtendedInfo);
      if (localZip64ExtendedInfo.getUnCompressedSize() != -1L) {
        paramLocalFileHeader.setUncompressedSize(localZip64ExtendedInfo.getUnCompressedSize());
      }
    } while (localZip64ExtendedInfo.getCompressedSize() == -1L);
    paramLocalFileHeader.setCompressedSize(localZip64ExtendedInfo.getCompressedSize());
  }
  
  private CentralDirectory readCentralDirectory()
    throws ZipException
  {
    if (this.zip4jRaf == null) {
      throw new ZipException("random access file was null", 3);
    }
    if (this.zipModel.getEndCentralDirRecord() == null) {
      throw new ZipException("EndCentralRecord was null, maybe a corrupt zip file");
    }
    CentralDirectory localCentralDirectory;
    ArrayList localArrayList;
    int i;
    byte[] arrayOfByte1;
    byte[] arrayOfByte2;
    int j;
    FileHeader localFileHeader;
    int k;
    try
    {
      localCentralDirectory = new CentralDirectory();
      localArrayList = new ArrayList();
      Object localObject1 = this.zipModel.getEndCentralDirRecord();
      long l = ((EndCentralDirRecord)localObject1).getOffsetOfStartOfCentralDir();
      i = ((EndCentralDirRecord)localObject1).getTotNoOfEntriesInCentralDir();
      if (this.zipModel.isZip64Format())
      {
        l = this.zipModel.getZip64EndCentralDirRecord().getOffsetStartCenDirWRTStartDiskNo();
        i = (int)this.zipModel.getZip64EndCentralDirRecord().getTotNoOfEntriesInCentralDir();
      }
      this.zip4jRaf.seek(l);
      arrayOfByte1 = new byte[4];
      arrayOfByte2 = new byte[2];
      localObject1 = new byte[8];
      j = 0;
      if (j >= i) {
        break label956;
      }
      localFileHeader = new FileHeader();
      readIntoBuff(this.zip4jRaf, arrayOfByte1);
      k = Raw.readIntLittleEndian(arrayOfByte1, 0);
      if (k != 33639248L) {
        throw new ZipException("Expected central directory entry not found (#" + (j + 1) + ")");
      }
    }
    catch (IOException localIOException)
    {
      throw new ZipException(localIOException);
    }
    localFileHeader.setSignature(k);
    readIntoBuff(this.zip4jRaf, arrayOfByte2);
    localFileHeader.setVersionMadeBy(Raw.readShortLittleEndian(arrayOfByte2, 0));
    readIntoBuff(this.zip4jRaf, arrayOfByte2);
    localFileHeader.setVersionNeededToExtract(Raw.readShortLittleEndian(arrayOfByte2, 0));
    readIntoBuff(this.zip4jRaf, arrayOfByte2);
    boolean bool;
    label304:
    label354:
    int m;
    Object localObject2;
    Object localObject3;
    if ((Raw.readShortLittleEndian(arrayOfByte2, 0) & 0x800) != 0)
    {
      bool = true;
      localFileHeader.setFileNameUTF8Encoded(bool);
      k = arrayOfByte2[0];
      if ((k & 0x1) != 0) {
        localFileHeader.setEncrypted(true);
      }
      localFileHeader.setGeneralPurposeFlag((byte[])arrayOfByte2.clone());
      if (k >> 3 != 1) {
        break label1075;
      }
      bool = true;
      localFileHeader.setDataDescriptorExists(bool);
      readIntoBuff(this.zip4jRaf, arrayOfByte2);
      localFileHeader.setCompressionMethod(Raw.readShortLittleEndian(arrayOfByte2, 0));
      readIntoBuff(this.zip4jRaf, arrayOfByte1);
      localFileHeader.setLastModFileTime(Raw.readIntLittleEndian(arrayOfByte1, 0));
      readIntoBuff(this.zip4jRaf, arrayOfByte1);
      localFileHeader.setCrc32(Raw.readIntLittleEndian(arrayOfByte1, 0));
      localFileHeader.setCrcBuff((byte[])arrayOfByte1.clone());
      readIntoBuff(this.zip4jRaf, arrayOfByte1);
      localFileHeader.setCompressedSize(Raw.readLongLittleEndian(getLongByteFromIntByte(arrayOfByte1), 0));
      readIntoBuff(this.zip4jRaf, arrayOfByte1);
      localFileHeader.setUncompressedSize(Raw.readLongLittleEndian(getLongByteFromIntByte(arrayOfByte1), 0));
      readIntoBuff(this.zip4jRaf, arrayOfByte2);
      k = Raw.readShortLittleEndian(arrayOfByte2, 0);
      localFileHeader.setFileNameLength(k);
      readIntoBuff(this.zip4jRaf, arrayOfByte2);
      localFileHeader.setExtraFieldLength(Raw.readShortLittleEndian(arrayOfByte2, 0));
      readIntoBuff(this.zip4jRaf, arrayOfByte2);
      m = Raw.readShortLittleEndian(arrayOfByte2, 0);
      localFileHeader.setFileComment(new String(arrayOfByte2));
      readIntoBuff(this.zip4jRaf, arrayOfByte2);
      localFileHeader.setDiskNumberStart(Raw.readShortLittleEndian(arrayOfByte2, 0));
      readIntoBuff(this.zip4jRaf, arrayOfByte2);
      localFileHeader.setInternalFileAttr((byte[])arrayOfByte2.clone());
      readIntoBuff(this.zip4jRaf, arrayOfByte1);
      localFileHeader.setExternalFileAttr((byte[])arrayOfByte1.clone());
      readIntoBuff(this.zip4jRaf, arrayOfByte1);
      localFileHeader.setOffsetLocalHeader(Raw.readLongLittleEndian(getLongByteFromIntByte(arrayOfByte1), 0) & 0xFFFFFFFF);
      if (k > 0)
      {
        localObject2 = new byte[k];
        readIntoBuff(this.zip4jRaf, (byte[])localObject2);
        if (Zip4jUtil.isStringNotNullAndNotEmpty(this.zipModel.getFileNameCharset())) {}
        for (localObject2 = new String((byte[])localObject2, this.zipModel.getFileNameCharset()); localObject2 == null; localObject2 = Zip4jUtil.decodeFileName((byte[])localObject2, localFileHeader.isFileNameUTF8Encoded())) {
          throw new ZipException("fileName is null when reading central directory");
        }
        localObject3 = localObject2;
        if (((String)localObject2).indexOf(":" + System.getProperty("file.separator")) >= 0) {
          localObject3 = ((String)localObject2).substring(((String)localObject2).indexOf(":" + System.getProperty("file.separator")) + 2);
        }
        localFileHeader.setFileName((String)localObject3);
        if (((String)localObject3).endsWith("/")) {
          break label1081;
        }
        if (!((String)localObject3).endsWith("\\")) {
          break label1087;
        }
        break label1081;
      }
    }
    for (;;)
    {
      localFileHeader.setDirectory(bool);
      for (;;)
      {
        readAndSaveExtraDataRecord(localFileHeader);
        readAndSaveZip64ExtendedInfo(localFileHeader);
        readAndSaveAESExtraDataRecord(localFileHeader);
        if (m > 0)
        {
          localObject2 = new byte[m];
          readIntoBuff(this.zip4jRaf, (byte[])localObject2);
          localFileHeader.setFileComment(new String((byte[])localObject2));
        }
        localArrayList.add(localFileHeader);
        j += 1;
        break;
        localFileHeader.setFileName(null);
      }
      label956:
      localCentralDirectory.setFileHeaders(localArrayList);
      localObject2 = new DigitalSignature();
      readIntoBuff(this.zip4jRaf, arrayOfByte1);
      i = Raw.readIntLittleEndian(arrayOfByte1, 0);
      if (i != 84233040L) {
        return localCentralDirectory;
      }
      ((DigitalSignature)localObject2).setHeaderSignature(i);
      readIntoBuff(this.zip4jRaf, arrayOfByte2);
      i = Raw.readShortLittleEndian(arrayOfByte2, 0);
      ((DigitalSignature)localObject2).setSizeOfData(i);
      if (i <= 0) {
        break label1093;
      }
      localObject3 = new byte[i];
      readIntoBuff(this.zip4jRaf, (byte[])localObject3);
      ((DigitalSignature)localObject2).setSignatureData(new String((byte[])localObject3));
      return localCentralDirectory;
      bool = false;
      break label304;
      label1075:
      bool = false;
      break label354;
      label1081:
      bool = true;
      continue;
      label1087:
      bool = false;
    }
    label1093:
    return localCentralDirectory;
  }
  
  private EndCentralDirRecord readEndOfCentralDirectoryRecord()
    throws ZipException
  {
    if (this.zip4jRaf == null) {
      throw new ZipException("random access file was null", 3);
    }
    for (;;)
    {
      long l;
      try
      {
        arrayOfByte1 = new byte[4];
        l = this.zip4jRaf.length();
        EndCentralDirRecord localEndCentralDirRecord = new EndCentralDirRecord();
        i = 0;
        l -= 22L;
        this.zip4jRaf.seek(l);
        i += 1;
        if ((Raw.readLeInt(this.zip4jRaf, arrayOfByte1) != 101010256L) && (i <= 3000)) {
          break label383;
        }
        if (Raw.readIntLittleEndian(arrayOfByte1, 0) != 101010256L) {
          throw new ZipException("zip headers not found. probably not a zip file");
        }
      }
      catch (IOException localIOException)
      {
        throw new ZipException("Probably not a zip file or a corrupted zip file", localIOException, 4);
      }
      byte[] arrayOfByte1 = new byte[4];
      byte[] arrayOfByte2 = new byte[2];
      localIOException.setSignature(101010256L);
      readIntoBuff(this.zip4jRaf, arrayOfByte2);
      localIOException.setNoOfThisDisk(Raw.readShortLittleEndian(arrayOfByte2, 0));
      readIntoBuff(this.zip4jRaf, arrayOfByte2);
      localIOException.setNoOfThisDiskStartOfCentralDir(Raw.readShortLittleEndian(arrayOfByte2, 0));
      readIntoBuff(this.zip4jRaf, arrayOfByte2);
      localIOException.setTotNoOfEntriesInCentralDirOnThisDisk(Raw.readShortLittleEndian(arrayOfByte2, 0));
      readIntoBuff(this.zip4jRaf, arrayOfByte2);
      localIOException.setTotNoOfEntriesInCentralDir(Raw.readShortLittleEndian(arrayOfByte2, 0));
      readIntoBuff(this.zip4jRaf, arrayOfByte1);
      localIOException.setSizeOfCentralDir(Raw.readIntLittleEndian(arrayOfByte1, 0));
      readIntoBuff(this.zip4jRaf, arrayOfByte1);
      localIOException.setOffsetOfStartOfCentralDir(Raw.readLongLittleEndian(getLongByteFromIntByte(arrayOfByte1), 0));
      readIntoBuff(this.zip4jRaf, arrayOfByte2);
      int i = Raw.readShortLittleEndian(arrayOfByte2, 0);
      localIOException.setCommentLength(i);
      if (i > 0)
      {
        arrayOfByte1 = new byte[i];
        readIntoBuff(this.zip4jRaf, arrayOfByte1);
        localIOException.setComment(new String(arrayOfByte1));
        localIOException.setCommentBytes(arrayOfByte1);
      }
      while (localIOException.getNoOfThisDisk() > 0)
      {
        this.zipModel.setSplitArchive(true);
        return localIOException;
        localIOException.setComment(null);
      }
      this.zipModel.setSplitArchive(false);
      return localIOException;
      label383:
      l -= 1L;
    }
  }
  
  /* Error */
  private ArrayList readExtraDataRecords(int paramInt)
    throws ZipException
  {
    // Byte code:
    //   0: iload_1
    //   1: ifgt +9 -> 10
    //   4: aconst_null
    //   5: astore 5
    //   7: aload 5
    //   9: areturn
    //   10: iload_1
    //   11: newarray <illegal type>
    //   13: astore 5
    //   15: aload_0
    //   16: getfield 15	net/lingala/zip4j/core/HeaderReader:zip4jRaf	Ljava/io/RandomAccessFile;
    //   19: aload 5
    //   21: invokevirtual 465	java/io/RandomAccessFile:read	([B)I
    //   24: pop
    //   25: iconst_0
    //   26: istore_2
    //   27: new 32	java/util/ArrayList
    //   30: dup
    //   31: invokespecial 211	java/util/ArrayList:<init>	()V
    //   34: astore 6
    //   36: iload_2
    //   37: iload_1
    //   38: if_icmpge +63 -> 101
    //   41: new 42	net/lingala/zip4j/model/ExtraDataRecord
    //   44: dup
    //   45: invokespecial 466	net/lingala/zip4j/model/ExtraDataRecord:<init>	()V
    //   48: astore 7
    //   50: aload 7
    //   52: aload 5
    //   54: iload_2
    //   55: invokestatic 74	net/lingala/zip4j/util/Raw:readShortLittleEndian	([BI)I
    //   58: i2l
    //   59: invokevirtual 469	net/lingala/zip4j/model/ExtraDataRecord:setHeader	(J)V
    //   62: iload_2
    //   63: iconst_2
    //   64: iadd
    //   65: istore 4
    //   67: aload 5
    //   69: iload 4
    //   71: invokestatic 74	net/lingala/zip4j/util/Raw:readShortLittleEndian	([BI)I
    //   74: istore_3
    //   75: iload_3
    //   76: istore_2
    //   77: iload_3
    //   78: iconst_2
    //   79: iadd
    //   80: iload_1
    //   81: if_icmple +34 -> 115
    //   84: aload 5
    //   86: iload 4
    //   88: invokestatic 473	net/lingala/zip4j/util/Raw:readShortBigEndian	([BI)S
    //   91: istore_3
    //   92: iload_3
    //   93: istore_2
    //   94: iload_3
    //   95: iconst_2
    //   96: iadd
    //   97: iload_1
    //   98: if_icmple +17 -> 115
    //   101: aload 6
    //   103: astore 5
    //   105: aload 6
    //   107: invokevirtual 36	java/util/ArrayList:size	()I
    //   110: ifgt -103 -> 7
    //   113: aconst_null
    //   114: areturn
    //   115: aload 7
    //   117: iload_2
    //   118: invokevirtual 474	net/lingala/zip4j/model/ExtraDataRecord:setSizeOfData	(I)V
    //   121: iload 4
    //   123: iconst_2
    //   124: iadd
    //   125: istore_3
    //   126: iload_2
    //   127: ifle +25 -> 152
    //   130: iload_2
    //   131: newarray <illegal type>
    //   133: astore 8
    //   135: aload 5
    //   137: iload_3
    //   138: aload 8
    //   140: iconst_0
    //   141: iload_2
    //   142: invokestatic 83	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
    //   145: aload 7
    //   147: aload 8
    //   149: invokevirtual 477	net/lingala/zip4j/model/ExtraDataRecord:setData	([B)V
    //   152: iload_3
    //   153: iload_2
    //   154: iadd
    //   155: istore_2
    //   156: aload 6
    //   158: aload 7
    //   160: invokevirtual 390	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   163: pop
    //   164: goto -128 -> 36
    //   167: astore 5
    //   169: new 20	net/lingala/zip4j/exception/ZipException
    //   172: dup
    //   173: aload 5
    //   175: invokespecial 270	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   178: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	179	0	this	HeaderReader
    //   0	179	1	paramInt	int
    //   26	130	2	i	int
    //   74	81	3	j	int
    //   65	60	4	k	int
    //   5	131	5	localObject	Object
    //   167	7	5	localIOException	IOException
    //   34	123	6	localArrayList	ArrayList
    //   48	111	7	localExtraDataRecord	ExtraDataRecord
    //   133	15	8	arrayOfByte	byte[]
    // Exception table:
    //   from	to	target	type
    //   10	25	167	java/io/IOException
    //   27	36	167	java/io/IOException
    //   41	62	167	java/io/IOException
    //   67	75	167	java/io/IOException
    //   84	92	167	java/io/IOException
    //   105	113	167	java/io/IOException
    //   115	121	167	java/io/IOException
    //   130	152	167	java/io/IOException
    //   156	164	167	java/io/IOException
  }
  
  private byte[] readIntoBuff(RandomAccessFile paramRandomAccessFile, byte[] paramArrayOfByte)
    throws ZipException
  {
    try
    {
      if (paramRandomAccessFile.read(paramArrayOfByte, 0, paramArrayOfByte.length) != -1) {
        return paramArrayOfByte;
      }
      throw new ZipException("unexpected end of file when reading short buff");
    }
    catch (IOException paramRandomAccessFile)
    {
      throw new ZipException("IOException when reading short buff", paramRandomAccessFile);
    }
  }
  
  private Zip64EndCentralDirLocator readZip64EndCentralDirLocator()
    throws ZipException
  {
    if (this.zip4jRaf == null) {
      throw new ZipException("invalid file handler when trying to read Zip64EndCentralDirLocator");
    }
    try
    {
      Zip64EndCentralDirLocator localZip64EndCentralDirLocator = new Zip64EndCentralDirLocator();
      setFilePointerToReadZip64EndCentralDirLoc();
      byte[] arrayOfByte1 = new byte[4];
      byte[] arrayOfByte2 = new byte[8];
      readIntoBuff(this.zip4jRaf, arrayOfByte1);
      int i = Raw.readIntLittleEndian(arrayOfByte1, 0);
      if (i == 117853008L)
      {
        this.zipModel.setZip64Format(true);
        localZip64EndCentralDirLocator.setSignature(i);
        readIntoBuff(this.zip4jRaf, arrayOfByte1);
        localZip64EndCentralDirLocator.setNoOfDiskStartOfZip64EndOfCentralDirRec(Raw.readIntLittleEndian(arrayOfByte1, 0));
        readIntoBuff(this.zip4jRaf, arrayOfByte2);
        localZip64EndCentralDirLocator.setOffsetZip64EndOfCentralDirRec(Raw.readLongLittleEndian(arrayOfByte2, 0));
        readIntoBuff(this.zip4jRaf, arrayOfByte1);
        localZip64EndCentralDirLocator.setTotNumberOfDiscs(Raw.readIntLittleEndian(arrayOfByte1, 0));
        return localZip64EndCentralDirLocator;
      }
      this.zipModel.setZip64Format(false);
      return null;
    }
    catch (Exception localException)
    {
      throw new ZipException(localException);
    }
  }
  
  private Zip64EndCentralDirRecord readZip64EndCentralDirRec()
    throws ZipException
  {
    if (this.zipModel.getZip64EndCentralDirLocator() == null) {
      throw new ZipException("invalid zip64 end of central directory locator");
    }
    long l = this.zipModel.getZip64EndCentralDirLocator().getOffsetZip64EndOfCentralDirRec();
    if (l < 0L) {
      throw new ZipException("invalid offset for start of end of central directory record");
    }
    byte[] arrayOfByte1;
    byte[] arrayOfByte2;
    byte[] arrayOfByte3;
    int i;
    try
    {
      this.zip4jRaf.seek(l);
      Zip64EndCentralDirRecord localZip64EndCentralDirRecord = new Zip64EndCentralDirRecord();
      arrayOfByte1 = new byte[2];
      arrayOfByte2 = new byte[4];
      arrayOfByte3 = new byte[8];
      readIntoBuff(this.zip4jRaf, arrayOfByte2);
      i = Raw.readIntLittleEndian(arrayOfByte2, 0);
      if (i != 101075792L) {
        throw new ZipException("invalid signature for zip64 end of central directory record");
      }
    }
    catch (IOException localIOException)
    {
      throw new ZipException(localIOException);
    }
    l = i;
    localIOException.setSignature(l);
    readIntoBuff(this.zip4jRaf, arrayOfByte3);
    localIOException.setSizeOfZip64EndCentralDirRec(Raw.readLongLittleEndian(arrayOfByte3, 0));
    readIntoBuff(this.zip4jRaf, arrayOfByte1);
    localIOException.setVersionMadeBy(Raw.readShortLittleEndian(arrayOfByte1, 0));
    readIntoBuff(this.zip4jRaf, arrayOfByte1);
    localIOException.setVersionNeededToExtract(Raw.readShortLittleEndian(arrayOfByte1, 0));
    readIntoBuff(this.zip4jRaf, arrayOfByte2);
    localIOException.setNoOfThisDisk(Raw.readIntLittleEndian(arrayOfByte2, 0));
    readIntoBuff(this.zip4jRaf, arrayOfByte2);
    localIOException.setNoOfThisDiskStartOfCentralDir(Raw.readIntLittleEndian(arrayOfByte2, 0));
    readIntoBuff(this.zip4jRaf, arrayOfByte3);
    localIOException.setTotNoOfEntriesInCentralDirOnThisDisk(Raw.readLongLittleEndian(arrayOfByte3, 0));
    readIntoBuff(this.zip4jRaf, arrayOfByte3);
    localIOException.setTotNoOfEntriesInCentralDir(Raw.readLongLittleEndian(arrayOfByte3, 0));
    readIntoBuff(this.zip4jRaf, arrayOfByte3);
    localIOException.setSizeOfCentralDir(Raw.readLongLittleEndian(arrayOfByte3, 0));
    readIntoBuff(this.zip4jRaf, arrayOfByte3);
    localIOException.setOffsetStartCenDirWRTStartDiskNo(Raw.readLongLittleEndian(arrayOfByte3, 0));
    l = localIOException.getSizeOfZip64EndCentralDirRec() - 44L;
    if (l > 0L)
    {
      arrayOfByte1 = new byte[(int)l];
      readIntoBuff(this.zip4jRaf, arrayOfByte1);
      localIOException.setExtensibleDataSector(arrayOfByte1);
    }
    return localIOException;
  }
  
  private Zip64ExtendedInfo readZip64ExtendedInfo(ArrayList paramArrayList, long paramLong1, long paramLong2, long paramLong3, int paramInt)
    throws ZipException
  {
    int i = 0;
    ExtraDataRecord localExtraDataRecord;
    byte[] arrayOfByte1;
    if (i < paramArrayList.size())
    {
      localExtraDataRecord = (ExtraDataRecord)paramArrayList.get(i);
      if (localExtraDataRecord == null) {}
      while (localExtraDataRecord.getHeader() != 1L)
      {
        i += 1;
        break;
      }
      paramArrayList = new Zip64ExtendedInfo();
      arrayOfByte1 = localExtraDataRecord.getData();
      if (localExtraDataRecord.getSizeOfData() > 0) {
        break label72;
      }
    }
    label72:
    int j;
    do
    {
      return null;
      byte[] arrayOfByte2 = new byte[8];
      byte[] arrayOfByte3 = new byte[4];
      j = 0;
      int m = 0;
      int k = j;
      i = m;
      if ((0xFFFF & paramLong1) == 65535L)
      {
        k = j;
        i = m;
        if (localExtraDataRecord.getSizeOfData() < 0)
        {
          System.arraycopy(arrayOfByte1, 0, arrayOfByte2, 0, 8);
          paramArrayList.setUnCompressedSize(Raw.readLongLittleEndian(arrayOfByte2, 0));
          k = 0 + 8;
          i = 1;
        }
      }
      m = k;
      j = i;
      if ((0xFFFF & paramLong2) == 65535L)
      {
        m = k;
        j = i;
        if (k < localExtraDataRecord.getSizeOfData())
        {
          System.arraycopy(arrayOfByte1, k, arrayOfByte2, 0, 8);
          paramArrayList.setCompressedSize(Raw.readLongLittleEndian(arrayOfByte2, 0));
          m = k + 8;
          j = 1;
        }
      }
      k = m;
      i = j;
      if ((0xFFFF & paramLong3) == 65535L)
      {
        k = m;
        i = j;
        if (m < localExtraDataRecord.getSizeOfData())
        {
          System.arraycopy(arrayOfByte1, m, arrayOfByte2, 0, 8);
          paramArrayList.setOffsetLocalHeader(Raw.readLongLittleEndian(arrayOfByte2, 0));
          k = m + 8;
          i = 1;
        }
      }
      j = i;
      if ((0xFFFF & paramInt) == 65535)
      {
        j = i;
        if (k < localExtraDataRecord.getSizeOfData())
        {
          System.arraycopy(arrayOfByte1, k, arrayOfByte3, 0, 4);
          paramArrayList.setDiskNumberStart(Raw.readIntLittleEndian(arrayOfByte3, 0));
          j = 1;
        }
      }
    } while (j == 0);
    return paramArrayList;
  }
  
  private void setFilePointerToReadZip64EndCentralDirLoc()
    throws ZipException
  {
    for (;;)
    {
      long l;
      try
      {
        byte[] arrayOfByte = new byte[4];
        l = this.zip4jRaf.length() - 22L;
        this.zip4jRaf.seek(l);
        if (Raw.readLeInt(this.zip4jRaf, arrayOfByte) == 101010256L)
        {
          this.zip4jRaf.seek(this.zip4jRaf.getFilePointer() - 4L - 4L - 8L - 4L - 4L);
          return;
        }
      }
      catch (IOException localIOException)
      {
        throw new ZipException(localIOException);
      }
      l -= 1L;
    }
  }
  
  public ZipModel readAllHeaders()
    throws ZipException
  {
    return readAllHeaders(null);
  }
  
  public ZipModel readAllHeaders(String paramString)
    throws ZipException
  {
    this.zipModel = new ZipModel();
    this.zipModel.setFileNameCharset(paramString);
    this.zipModel.setEndCentralDirRecord(readEndOfCentralDirectoryRecord());
    this.zipModel.setZip64EndCentralDirLocator(readZip64EndCentralDirLocator());
    if (this.zipModel.isZip64Format())
    {
      this.zipModel.setZip64EndCentralDirRecord(readZip64EndCentralDirRec());
      if ((this.zipModel.getZip64EndCentralDirRecord() == null) || (this.zipModel.getZip64EndCentralDirRecord().getNoOfThisDisk() <= 0)) {
        break label109;
      }
      this.zipModel.setSplitArchive(true);
    }
    for (;;)
    {
      this.zipModel.setCentralDirectory(readCentralDirectory());
      return this.zipModel;
      label109:
      this.zipModel.setSplitArchive(false);
    }
  }
  
  public LocalFileHeader readLocalFileHeader(FileHeader paramFileHeader)
    throws ZipException
  {
    if ((paramFileHeader == null) || (this.zip4jRaf == null)) {
      throw new ZipException("invalid read parameters for local header");
    }
    long l2 = paramFileHeader.getOffsetLocalHeader();
    long l1 = l2;
    if (paramFileHeader.getZip64ExtendedInfo() != null)
    {
      l1 = l2;
      if (paramFileHeader.getZip64ExtendedInfo().getOffsetLocalHeader() > 0L) {
        l1 = paramFileHeader.getOffsetLocalHeader();
      }
    }
    if (l1 < 0L) {
      throw new ZipException("invalid local header offset");
    }
    LocalFileHeader localLocalFileHeader;
    Object localObject1;
    Object localObject2;
    Object localObject3;
    int i;
    try
    {
      this.zip4jRaf.seek(l1);
      localLocalFileHeader = new LocalFileHeader();
      localObject1 = new byte[2];
      localObject2 = new byte[4];
      localObject3 = new byte[8];
      readIntoBuff(this.zip4jRaf, (byte[])localObject2);
      i = Raw.readIntLittleEndian((byte[])localObject2, 0);
      if (i != 67324752L) {
        throw new ZipException("invalid local header signature for file: " + paramFileHeader.getFileName());
      }
    }
    catch (IOException paramFileHeader)
    {
      throw new ZipException(paramFileHeader);
    }
    localLocalFileHeader.setSignature(i);
    readIntoBuff(this.zip4jRaf, (byte[])localObject1);
    localLocalFileHeader.setVersionNeededToExtract(Raw.readShortLittleEndian((byte[])localObject1, 0));
    readIntoBuff(this.zip4jRaf, (byte[])localObject1);
    int j;
    if ((Raw.readShortLittleEndian((byte[])localObject1, 0) & 0x800) != 0)
    {
      bool = true;
      localLocalFileHeader.setFileNameUTF8Encoded(bool);
      j = localObject1[0];
      if ((j & 0x1) != 0) {
        localLocalFileHeader.setEncrypted(true);
      }
      localLocalFileHeader.setGeneralPurposeFlag((byte[])localObject1);
      localObject3 = Integer.toBinaryString(j);
      if (((String)localObject3).length() >= 4) {
        if (((String)localObject3).charAt(3) != '1') {
          break label820;
        }
      }
    }
    label787:
    label820:
    for (boolean bool = true;; bool = false)
    {
      localLocalFileHeader.setDataDescriptorExists(bool);
      readIntoBuff(this.zip4jRaf, (byte[])localObject1);
      localLocalFileHeader.setCompressionMethod(Raw.readShortLittleEndian((byte[])localObject1, 0));
      readIntoBuff(this.zip4jRaf, (byte[])localObject2);
      localLocalFileHeader.setLastModFileTime(Raw.readIntLittleEndian((byte[])localObject2, 0));
      readIntoBuff(this.zip4jRaf, (byte[])localObject2);
      localLocalFileHeader.setCrc32(Raw.readIntLittleEndian((byte[])localObject2, 0));
      localLocalFileHeader.setCrcBuff((byte[])((byte[])localObject2).clone());
      readIntoBuff(this.zip4jRaf, (byte[])localObject2);
      localLocalFileHeader.setCompressedSize(Raw.readLongLittleEndian(getLongByteFromIntByte((byte[])localObject2), 0));
      readIntoBuff(this.zip4jRaf, (byte[])localObject2);
      localLocalFileHeader.setUncompressedSize(Raw.readLongLittleEndian(getLongByteFromIntByte((byte[])localObject2), 0));
      readIntoBuff(this.zip4jRaf, (byte[])localObject1);
      int m = Raw.readShortLittleEndian((byte[])localObject1, 0);
      localLocalFileHeader.setFileNameLength(m);
      readIntoBuff(this.zip4jRaf, (byte[])localObject1);
      int k = Raw.readShortLittleEndian((byte[])localObject1, 0);
      localLocalFileHeader.setExtraFieldLength(k);
      i = 0 + 4 + 2 + 2 + 2 + 4 + 4 + 4 + 4 + 2 + 2;
      if (m > 0)
      {
        localObject1 = new byte[m];
        readIntoBuff(this.zip4jRaf, (byte[])localObject1);
        localObject2 = Zip4jUtil.decodeFileName((byte[])localObject1, localLocalFileHeader.isFileNameUTF8Encoded());
        if (localObject2 == null) {
          throw new ZipException("file name is null, cannot assign file name to local file header");
        }
        localObject1 = localObject2;
        if (((String)localObject2).indexOf(":" + System.getProperty("file.separator")) >= 0) {
          localObject1 = ((String)localObject2).substring(((String)localObject2).indexOf(":" + System.getProperty("file.separator")) + 2);
        }
        localLocalFileHeader.setFileName((String)localObject1);
        i = m + 30;
        readAndSaveExtraDataRecord(localLocalFileHeader);
        localLocalFileHeader.setOffsetStartOfData(i + k + l1);
        localLocalFileHeader.setPassword(paramFileHeader.getPassword());
        readAndSaveZip64ExtendedInfo(localLocalFileHeader);
        readAndSaveAESExtraDataRecord(localLocalFileHeader);
        if ((localLocalFileHeader.isEncrypted()) && (localLocalFileHeader.getEncryptionMethod() != 99)) {
          break label787;
        }
      }
      for (;;)
      {
        if (localLocalFileHeader.getCrc32() <= 0L)
        {
          localLocalFileHeader.setCrc32(paramFileHeader.getCrc32());
          localLocalFileHeader.setCrcBuff(paramFileHeader.getCrcBuff());
        }
        if (localLocalFileHeader.getCompressedSize() <= 0L) {
          localLocalFileHeader.setCompressedSize(paramFileHeader.getCompressedSize());
        }
        if (localLocalFileHeader.getUncompressedSize() > 0L) {
          break label826;
        }
        localLocalFileHeader.setUncompressedSize(paramFileHeader.getUncompressedSize());
        return localLocalFileHeader;
        localLocalFileHeader.setFileName(null);
        break;
        if ((j & 0x40) == 64) {
          localLocalFileHeader.setEncryptionMethod(1);
        } else {
          localLocalFileHeader.setEncryptionMethod(0);
        }
      }
      bool = false;
      break;
    }
    label826:
    return localLocalFileHeader;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/core/HeaderReader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */