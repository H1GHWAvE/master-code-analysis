package net.lingala.zip4j.core;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.SplitOutputStream;
import net.lingala.zip4j.model.AESExtraDataRecord;
import net.lingala.zip4j.model.CentralDirectory;
import net.lingala.zip4j.model.EndCentralDirRecord;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.LocalFileHeader;
import net.lingala.zip4j.model.Zip64EndCentralDirLocator;
import net.lingala.zip4j.model.Zip64EndCentralDirRecord;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.util.Raw;
import net.lingala.zip4j.util.Zip4jUtil;

public class HeaderWriter
{
  private final int ZIP64_EXTRA_BUF = 50;
  
  private byte[] byteArrayListToByteArray(List paramList)
    throws ZipException
  {
    if (paramList == null) {
      throw new ZipException("input byte array list is null, cannot conver to byte array");
    }
    Object localObject;
    if (paramList.size() <= 0)
    {
      localObject = null;
      return (byte[])localObject;
    }
    byte[] arrayOfByte = new byte[paramList.size()];
    int i = 0;
    for (;;)
    {
      localObject = arrayOfByte;
      if (i >= paramList.size()) {
        break;
      }
      arrayOfByte[i] = Byte.parseByte((String)paramList.get(i));
      i += 1;
    }
  }
  
  private void copyByteArrayToArrayList(byte[] paramArrayOfByte, List paramList)
    throws ZipException
  {
    if ((paramList == null) || (paramArrayOfByte == null)) {
      throw new ZipException("one of the input parameters is null, cannot copy byte array to array list");
    }
    int i = 0;
    while (i < paramArrayOfByte.length)
    {
      paramList.add(Byte.toString(paramArrayOfByte[i]));
      i += 1;
    }
  }
  
  private int countNumberOfFileHeaderEntriesOnDisk(ArrayList paramArrayList, int paramInt)
    throws ZipException
  {
    if (paramArrayList == null) {
      throw new ZipException("file headers are null, cannot calculate number of entries on this disk");
    }
    int j = 0;
    int i = 0;
    while (i < paramArrayList.size())
    {
      int k = j;
      if (((FileHeader)paramArrayList.get(i)).getDiskNumberStart() == paramInt) {
        k = j + 1;
      }
      i += 1;
      j = k;
    }
    return j;
  }
  
  private void processHeaderData(ZipModel paramZipModel, OutputStream paramOutputStream)
    throws ZipException
  {
    int i = 0;
    try
    {
      if ((paramOutputStream instanceof SplitOutputStream))
      {
        paramZipModel.getEndCentralDirRecord().setOffsetOfStartOfCentralDir(((SplitOutputStream)paramOutputStream).getFilePointer());
        i = ((SplitOutputStream)paramOutputStream).getCurrSplitFileCounter();
      }
      if (paramZipModel.isZip64Format())
      {
        if (paramZipModel.getZip64EndCentralDirRecord() == null) {
          paramZipModel.setZip64EndCentralDirRecord(new Zip64EndCentralDirRecord());
        }
        if (paramZipModel.getZip64EndCentralDirLocator() == null) {
          paramZipModel.setZip64EndCentralDirLocator(new Zip64EndCentralDirLocator());
        }
        paramZipModel.getZip64EndCentralDirLocator().setNoOfDiskStartOfZip64EndOfCentralDirRec(i);
        paramZipModel.getZip64EndCentralDirLocator().setTotNumberOfDiscs(i + 1);
      }
      paramZipModel.getEndCentralDirRecord().setNoOfThisDisk(i);
      paramZipModel.getEndCentralDirRecord().setNoOfThisDiskStartOfCentralDir(i);
      return;
    }
    catch (IOException paramZipModel)
    {
      throw new ZipException(paramZipModel);
    }
  }
  
  private void updateCompressedSizeInLocalFileHeader(SplitOutputStream paramSplitOutputStream, LocalFileHeader paramLocalFileHeader, long paramLong1, long paramLong2, byte[] paramArrayOfByte, boolean paramBoolean)
    throws ZipException
  {
    if (paramSplitOutputStream == null) {
      throw new ZipException("invalid output stream, cannot update compressed size for local file header");
    }
    try
    {
      if (!paramLocalFileHeader.isWriteComprSizeInZip64ExtraRecord()) {
        break label120;
      }
      if (paramArrayOfByte.length != 8) {
        throw new ZipException("attempting to write a non 8-byte compressed size block for a zip64 file");
      }
    }
    catch (IOException paramSplitOutputStream)
    {
      throw new ZipException(paramSplitOutputStream);
    }
    long l = paramLong1 + paramLong2 + 4L + 4L + 2L + 2L + paramLocalFileHeader.getFileNameLength() + 2L + 2L + 8L;
    paramLong1 = l;
    if (paramLong2 == 22L) {
      paramLong1 = l + 8L;
    }
    paramSplitOutputStream.seek(paramLong1);
    paramSplitOutputStream.write(paramArrayOfByte);
    return;
    label120:
    paramSplitOutputStream.seek(paramLong1 + paramLong2);
    paramSplitOutputStream.write(paramArrayOfByte);
  }
  
  private int writeCentralDirectory(ZipModel paramZipModel, OutputStream paramOutputStream, List paramList)
    throws ZipException
  {
    if ((paramZipModel == null) || (paramOutputStream == null)) {
      throw new ZipException("input parameters is null, cannot write central directory");
    }
    int k;
    if ((paramZipModel.getCentralDirectory() == null) || (paramZipModel.getCentralDirectory().getFileHeaders() == null) || (paramZipModel.getCentralDirectory().getFileHeaders().size() <= 0))
    {
      k = 0;
      return k;
    }
    int i = 0;
    int j = 0;
    for (;;)
    {
      k = i;
      if (j >= paramZipModel.getCentralDirectory().getFileHeaders().size()) {
        break;
      }
      i += writeFileHeader(paramZipModel, (FileHeader)paramZipModel.getCentralDirectory().getFileHeaders().get(j), paramOutputStream, paramList);
      j += 1;
    }
  }
  
  private void writeEndOfCentralDirectoryRecord(ZipModel paramZipModel, OutputStream paramOutputStream, int paramInt, long paramLong, List paramList)
    throws ZipException
  {
    if ((paramZipModel == null) || (paramOutputStream == null)) {
      throw new ZipException("zip model or output stream is null, cannot write end of central directory record");
    }
    byte[] arrayOfByte1;
    byte[] arrayOfByte2;
    try
    {
      paramOutputStream = new byte[2];
      arrayOfByte1 = new byte[4];
      arrayOfByte2 = new byte[8];
      Raw.writeIntLittleEndian(arrayOfByte1, 0, (int)paramZipModel.getEndCentralDirRecord().getSignature());
      copyByteArrayToArrayList(arrayOfByte1, paramList);
      Raw.writeShortLittleEndian(paramOutputStream, 0, (short)paramZipModel.getEndCentralDirRecord().getNoOfThisDisk());
      copyByteArrayToArrayList(paramOutputStream, paramList);
      Raw.writeShortLittleEndian(paramOutputStream, 0, (short)paramZipModel.getEndCentralDirRecord().getNoOfThisDiskStartOfCentralDir());
      copyByteArrayToArrayList(paramOutputStream, paramList);
      if ((paramZipModel.getCentralDirectory() == null) || (paramZipModel.getCentralDirectory().getFileHeaders() == null)) {
        throw new ZipException("invalid central directory/file headers, cannot write end of central directory record");
      }
    }
    catch (Exception paramZipModel)
    {
      throw new ZipException(paramZipModel);
    }
    int j = paramZipModel.getCentralDirectory().getFileHeaders().size();
    if (paramZipModel.isSplitArchive()) {}
    for (int i = countNumberOfFileHeaderEntriesOnDisk(paramZipModel.getCentralDirectory().getFileHeaders(), paramZipModel.getEndCentralDirRecord().getNoOfThisDisk());; i = j)
    {
      Raw.writeShortLittleEndian(paramOutputStream, 0, (short)i);
      copyByteArrayToArrayList(paramOutputStream, paramList);
      Raw.writeShortLittleEndian(paramOutputStream, 0, (short)j);
      copyByteArrayToArrayList(paramOutputStream, paramList);
      Raw.writeIntLittleEndian(arrayOfByte1, 0, paramInt);
      copyByteArrayToArrayList(arrayOfByte1, paramList);
      if (paramLong > 4294967295L)
      {
        Raw.writeLongLittleEndian(arrayOfByte2, 0, 4294967295L);
        System.arraycopy(arrayOfByte2, 0, arrayOfByte1, 0, 4);
        copyByteArrayToArrayList(arrayOfByte1, paramList);
      }
      for (;;)
      {
        paramInt = 0;
        if (paramZipModel.getEndCentralDirRecord().getComment() != null) {
          paramInt = paramZipModel.getEndCentralDirRecord().getCommentLength();
        }
        Raw.writeShortLittleEndian(paramOutputStream, 0, (short)paramInt);
        copyByteArrayToArrayList(paramOutputStream, paramList);
        if (paramInt <= 0) {
          break;
        }
        copyByteArrayToArrayList(paramZipModel.getEndCentralDirRecord().getCommentBytes(), paramList);
        return;
        Raw.writeLongLittleEndian(arrayOfByte2, 0, paramLong);
        System.arraycopy(arrayOfByte2, 0, arrayOfByte1, 0, 4);
        copyByteArrayToArrayList(arrayOfByte1, paramList);
      }
      return;
    }
  }
  
  private int writeFileHeader(ZipModel paramZipModel, FileHeader paramFileHeader, OutputStream paramOutputStream, List paramList)
    throws ZipException
  {
    if ((paramFileHeader == null) || (paramOutputStream == null)) {
      throw new ZipException("input parameters is null, cannot write local file header");
    }
    int j;
    for (;;)
    {
      int k;
      int m;
      int n;
      try
      {
        paramOutputStream = new byte[2];
        byte[] arrayOfByte3 = new byte[4];
        byte[] arrayOfByte1 = new byte[8];
        byte[] arrayOfByte2 = new byte[2];
        byte[] tmp40_38 = arrayOfByte2;
        tmp40_38[0] = 0;
        byte[] tmp45_40 = tmp40_38;
        tmp45_40[1] = 0;
        tmp45_40;
        k = 0;
        m = 0;
        Raw.writeIntLittleEndian(arrayOfByte3, 0, paramFileHeader.getSignature());
        copyByteArrayToArrayList(arrayOfByte3, paramList);
        Raw.writeShortLittleEndian(paramOutputStream, 0, (short)paramFileHeader.getVersionMadeBy());
        copyByteArrayToArrayList(paramOutputStream, paramList);
        Raw.writeShortLittleEndian(paramOutputStream, 0, (short)paramFileHeader.getVersionNeededToExtract());
        copyByteArrayToArrayList(paramOutputStream, paramList);
        copyByteArrayToArrayList(paramFileHeader.getGeneralPurposeFlag(), paramList);
        Raw.writeShortLittleEndian(paramOutputStream, 0, (short)paramFileHeader.getCompressionMethod());
        copyByteArrayToArrayList(paramOutputStream, paramList);
        Raw.writeIntLittleEndian(arrayOfByte3, 0, paramFileHeader.getLastModFileTime());
        copyByteArrayToArrayList(arrayOfByte3, paramList);
        Raw.writeIntLittleEndian(arrayOfByte3, 0, (int)paramFileHeader.getCrc32());
        copyByteArrayToArrayList(arrayOfByte3, paramList);
        i = 0 + 4 + 2 + 2 + 2 + 2 + 4 + 4;
        if ((paramFileHeader.getCompressedSize() >= 4294967295L) || (paramFileHeader.getUncompressedSize() + 50L >= 4294967295L))
        {
          Raw.writeLongLittleEndian(arrayOfByte1, 0, 4294967295L);
          System.arraycopy(arrayOfByte1, 0, arrayOfByte3, 0, 4);
          copyByteArrayToArrayList(arrayOfByte3, paramList);
          copyByteArrayToArrayList(arrayOfByte3, paramList);
          i = i + 4 + 4;
          k = 1;
          Raw.writeShortLittleEndian(paramOutputStream, 0, (short)paramFileHeader.getFileNameLength());
          copyByteArrayToArrayList(paramOutputStream, paramList);
          arrayOfByte3 = new byte[4];
          if (paramFileHeader.getOffsetLocalHeader() > 4294967295L)
          {
            Raw.writeLongLittleEndian(arrayOfByte1, 0, 4294967295L);
            System.arraycopy(arrayOfByte1, 0, arrayOfByte3, 0, 4);
            m = 1;
            break label881;
            j = i;
            if (paramFileHeader.getAesExtraDataRecord() != null) {
              j = i + 11;
            }
            Raw.writeShortLittleEndian(paramOutputStream, 0, (short)j);
            copyByteArrayToArrayList(paramOutputStream, paramList);
            copyByteArrayToArrayList(arrayOfByte2, paramList);
            Raw.writeShortLittleEndian(paramOutputStream, 0, (short)paramFileHeader.getDiskNumberStart());
            copyByteArrayToArrayList(paramOutputStream, paramList);
            copyByteArrayToArrayList(arrayOfByte2, paramList);
            if (paramFileHeader.getExternalFileAttr() == null) {
              break label817;
            }
            copyByteArrayToArrayList(paramFileHeader.getExternalFileAttr(), paramList);
            copyByteArrayToArrayList(arrayOfByte3, paramList);
            if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramZipModel.getFileNameCharset())) {
              break label849;
            }
            arrayOfByte2 = paramFileHeader.getFileName().getBytes(paramZipModel.getFileNameCharset());
            copyByteArrayToArrayList(arrayOfByte2, paramList);
            i = arrayOfByte2.length + 46;
            break label934;
            paramZipModel.setZip64Format(true);
            Raw.writeShortLittleEndian(paramOutputStream, 0, (short)1);
            copyByteArrayToArrayList(paramOutputStream, paramList);
            j = 0;
            if (k == 0) {
              break label951;
            }
            j = 0 + 16;
            break label951;
            Raw.writeShortLittleEndian(paramOutputStream, 0, (short)n);
            copyByteArrayToArrayList(paramOutputStream, paramList);
            j = i + 2 + 2;
            i = j;
            if (k != 0)
            {
              Raw.writeLongLittleEndian(arrayOfByte1, 0, paramFileHeader.getUncompressedSize());
              copyByteArrayToArrayList(arrayOfByte1, paramList);
              Raw.writeLongLittleEndian(arrayOfByte1, 0, paramFileHeader.getCompressedSize());
              copyByteArrayToArrayList(arrayOfByte1, paramList);
              i = j + 8 + 8;
            }
            j = i;
            if (m != 0)
            {
              Raw.writeLongLittleEndian(arrayOfByte1, 0, paramFileHeader.getOffsetLocalHeader());
              copyByteArrayToArrayList(arrayOfByte1, paramList);
              j = i + 8;
            }
            if (paramFileHeader.getAesExtraDataRecord() == null) {
              break;
            }
            paramZipModel = paramFileHeader.getAesExtraDataRecord();
            Raw.writeShortLittleEndian(paramOutputStream, 0, (short)(int)paramZipModel.getSignature());
            copyByteArrayToArrayList(paramOutputStream, paramList);
            Raw.writeShortLittleEndian(paramOutputStream, 0, (short)paramZipModel.getDataSize());
            copyByteArrayToArrayList(paramOutputStream, paramList);
            Raw.writeShortLittleEndian(paramOutputStream, 0, (short)paramZipModel.getVersionNumber());
            copyByteArrayToArrayList(paramOutputStream, paramList);
            copyByteArrayToArrayList(paramZipModel.getVendorID().getBytes(), paramList);
            copyByteArrayToArrayList(new byte[] { (byte)paramZipModel.getAesStrength() }, paramList);
            Raw.writeShortLittleEndian(paramOutputStream, 0, (short)paramZipModel.getCompressionMethod());
            copyByteArrayToArrayList(paramOutputStream, paramList);
            return j + 11;
          }
        }
        else
        {
          Raw.writeLongLittleEndian(arrayOfByte1, 0, paramFileHeader.getCompressedSize());
          System.arraycopy(arrayOfByte1, 0, arrayOfByte3, 0, 4);
          copyByteArrayToArrayList(arrayOfByte3, paramList);
          Raw.writeLongLittleEndian(arrayOfByte1, 0, paramFileHeader.getUncompressedSize());
          System.arraycopy(arrayOfByte1, 0, arrayOfByte3, 0, 4);
          copyByteArrayToArrayList(arrayOfByte3, paramList);
          i = i + 4 + 4;
          continue;
        }
        Raw.writeLongLittleEndian(arrayOfByte1, 0, paramFileHeader.getOffsetLocalHeader());
        System.arraycopy(arrayOfByte1, 0, arrayOfByte3, 0, 4);
      }
      catch (Exception paramZipModel)
      {
        throw new ZipException(paramZipModel);
      }
      label817:
      copyByteArrayToArrayList(new byte[] { 0, 0, 0, 0 }, paramList);
      continue;
      label849:
      copyByteArrayToArrayList(Zip4jUtil.convertCharset(paramFileHeader.getFileName()), paramList);
      int i = Zip4jUtil.getEncodedStringLength(paramFileHeader.getFileName());
      i += 46;
      break label934;
      label881:
      i = 0;
      if ((k != 0) || (m != 0))
      {
        i = 0 + 4;
        j = i;
        if (k != 0) {
          j = i + 16;
        }
        i = j;
        if (m != 0)
        {
          i = j + 8;
          continue;
          label934:
          if (k == 0)
          {
            j = i;
            if (m != 0)
            {
              continue;
              label951:
              n = j;
              if (m != 0) {
                n = j + 8;
              }
            }
          }
        }
      }
    }
    return j;
  }
  
  private void writeZip64EndOfCentralDirectoryLocator(ZipModel paramZipModel, OutputStream paramOutputStream, List paramList)
    throws ZipException
  {
    if ((paramZipModel == null) || (paramOutputStream == null)) {
      throw new ZipException("zip model or output stream is null, cannot write zip64 end of central directory locator");
    }
    try
    {
      paramOutputStream = new byte[4];
      byte[] arrayOfByte = new byte[8];
      Raw.writeIntLittleEndian(paramOutputStream, 0, 117853008);
      copyByteArrayToArrayList(paramOutputStream, paramList);
      Raw.writeIntLittleEndian(paramOutputStream, 0, paramZipModel.getZip64EndCentralDirLocator().getNoOfDiskStartOfZip64EndOfCentralDirRec());
      copyByteArrayToArrayList(paramOutputStream, paramList);
      Raw.writeLongLittleEndian(arrayOfByte, 0, paramZipModel.getZip64EndCentralDirLocator().getOffsetZip64EndOfCentralDirRec());
      copyByteArrayToArrayList(arrayOfByte, paramList);
      Raw.writeIntLittleEndian(paramOutputStream, 0, paramZipModel.getZip64EndCentralDirLocator().getTotNumberOfDiscs());
      copyByteArrayToArrayList(paramOutputStream, paramList);
      return;
    }
    catch (ZipException paramZipModel)
    {
      throw paramZipModel;
    }
    catch (Exception paramZipModel)
    {
      throw new ZipException(paramZipModel);
    }
  }
  
  private void writeZip64EndOfCentralDirectoryRecord(ZipModel paramZipModel, OutputStream paramOutputStream, int paramInt, long paramLong, List paramList)
    throws ZipException
  {
    if ((paramZipModel == null) || (paramOutputStream == null)) {
      throw new ZipException("zip model or output stream is null, cannot write zip64 end of central directory record");
    }
    byte[] arrayOfByte3;
    int i;
    try
    {
      paramOutputStream = new byte[2];
      arrayOfByte1 = new byte[2];
      byte[] tmp30_28 = arrayOfByte1;
      tmp30_28[0] = 0;
      byte[] tmp35_30 = tmp30_28;
      tmp35_30[1] = 0;
      tmp35_30;
      byte[] arrayOfByte2 = new byte[4];
      arrayOfByte3 = new byte[8];
      Raw.writeIntLittleEndian(arrayOfByte2, 0, 101075792);
      copyByteArrayToArrayList(arrayOfByte2, paramList);
      Raw.writeLongLittleEndian(arrayOfByte3, 0, 44L);
      copyByteArrayToArrayList(arrayOfByte3, paramList);
      if ((paramZipModel.getCentralDirectory() != null) && (paramZipModel.getCentralDirectory().getFileHeaders() != null) && (paramZipModel.getCentralDirectory().getFileHeaders().size() > 0))
      {
        Raw.writeShortLittleEndian(paramOutputStream, 0, (short)((FileHeader)paramZipModel.getCentralDirectory().getFileHeaders().get(0)).getVersionMadeBy());
        copyByteArrayToArrayList(paramOutputStream, paramList);
        Raw.writeShortLittleEndian(paramOutputStream, 0, (short)((FileHeader)paramZipModel.getCentralDirectory().getFileHeaders().get(0)).getVersionNeededToExtract());
        copyByteArrayToArrayList(paramOutputStream, paramList);
        Raw.writeIntLittleEndian(arrayOfByte2, 0, paramZipModel.getEndCentralDirRecord().getNoOfThisDisk());
        copyByteArrayToArrayList(arrayOfByte2, paramList);
        Raw.writeIntLittleEndian(arrayOfByte2, 0, paramZipModel.getEndCentralDirRecord().getNoOfThisDiskStartOfCentralDir());
        copyByteArrayToArrayList(arrayOfByte2, paramList);
        i = 0;
        if ((paramZipModel.getCentralDirectory() != null) && (paramZipModel.getCentralDirectory().getFileHeaders() != null)) {
          break label280;
        }
        throw new ZipException("invalid central directory/file headers, cannot write end of central directory record");
      }
    }
    catch (ZipException paramZipModel)
    {
      for (;;)
      {
        byte[] arrayOfByte1;
        throw paramZipModel;
        copyByteArrayToArrayList(arrayOfByte1, paramList);
        copyByteArrayToArrayList(arrayOfByte1, paramList);
      }
    }
    catch (Exception paramZipModel)
    {
      throw new ZipException(paramZipModel);
    }
    label280:
    int j = paramZipModel.getCentralDirectory().getFileHeaders().size();
    if (paramZipModel.isSplitArchive()) {
      countNumberOfFileHeaderEntriesOnDisk(paramZipModel.getCentralDirectory().getFileHeaders(), paramZipModel.getEndCentralDirRecord().getNoOfThisDisk());
    }
    for (;;)
    {
      Raw.writeLongLittleEndian(arrayOfByte3, 0, i);
      copyByteArrayToArrayList(arrayOfByte3, paramList);
      Raw.writeLongLittleEndian(arrayOfByte3, 0, j);
      copyByteArrayToArrayList(arrayOfByte3, paramList);
      Raw.writeLongLittleEndian(arrayOfByte3, 0, paramInt);
      copyByteArrayToArrayList(arrayOfByte3, paramList);
      Raw.writeLongLittleEndian(arrayOfByte3, 0, paramLong);
      copyByteArrayToArrayList(arrayOfByte3, paramList);
      return;
      i = j;
    }
  }
  
  private void writeZipHeaderBytes(ZipModel paramZipModel, OutputStream paramOutputStream, byte[] paramArrayOfByte)
    throws ZipException
  {
    if (paramArrayOfByte == null) {
      throw new ZipException("invalid buff to write as zip headers");
    }
    try
    {
      if (((paramOutputStream instanceof SplitOutputStream)) && (((SplitOutputStream)paramOutputStream).checkBuffSizeAndStartNextSplitFile(paramArrayOfByte.length)))
      {
        finalizeZipFile(paramZipModel, paramOutputStream);
        return;
      }
      paramOutputStream.write(paramArrayOfByte);
      return;
    }
    catch (IOException paramZipModel)
    {
      throw new ZipException(paramZipModel);
    }
  }
  
  /* Error */
  public void finalizeZipFile(ZipModel paramZipModel, OutputStream paramOutputStream)
    throws ZipException
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnull +7 -> 8
    //   4: aload_2
    //   5: ifnonnull +14 -> 19
    //   8: new 17	net/lingala/zip4j/exception/ZipException
    //   11: dup
    //   12: ldc_w 360
    //   15: invokespecial 22	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   18: athrow
    //   19: aload_0
    //   20: aload_1
    //   21: aload_2
    //   22: invokespecial 362	net/lingala/zip4j/core/HeaderWriter:processHeaderData	(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;)V
    //   25: aload_1
    //   26: invokevirtual 78	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   29: invokevirtual 365	net/lingala/zip4j/model/EndCentralDirRecord:getOffsetOfStartOfCentralDir	()J
    //   32: lstore 4
    //   34: new 59	java/util/ArrayList
    //   37: dup
    //   38: invokespecial 366	java/util/ArrayList:<init>	()V
    //   41: astore 6
    //   43: aload_0
    //   44: aload_1
    //   45: aload_2
    //   46: aload 6
    //   48: invokespecial 368	net/lingala/zip4j/core/HeaderWriter:writeCentralDirectory	(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;Ljava/util/List;)I
    //   51: istore_3
    //   52: aload_1
    //   53: invokevirtual 95	net/lingala/zip4j/model/ZipModel:isZip64Format	()Z
    //   56: ifeq +107 -> 163
    //   59: aload_1
    //   60: invokevirtual 99	net/lingala/zip4j/model/ZipModel:getZip64EndCentralDirRecord	()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
    //   63: ifnonnull +14 -> 77
    //   66: aload_1
    //   67: new 101	net/lingala/zip4j/model/Zip64EndCentralDirRecord
    //   70: dup
    //   71: invokespecial 102	net/lingala/zip4j/model/Zip64EndCentralDirRecord:<init>	()V
    //   74: invokevirtual 106	net/lingala/zip4j/model/ZipModel:setZip64EndCentralDirRecord	(Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;)V
    //   77: aload_1
    //   78: invokevirtual 110	net/lingala/zip4j/model/ZipModel:getZip64EndCentralDirLocator	()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
    //   81: ifnonnull +14 -> 95
    //   84: aload_1
    //   85: new 112	net/lingala/zip4j/model/Zip64EndCentralDirLocator
    //   88: dup
    //   89: invokespecial 113	net/lingala/zip4j/model/Zip64EndCentralDirLocator:<init>	()V
    //   92: invokevirtual 117	net/lingala/zip4j/model/ZipModel:setZip64EndCentralDirLocator	(Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;)V
    //   95: aload_1
    //   96: invokevirtual 110	net/lingala/zip4j/model/ZipModel:getZip64EndCentralDirLocator	()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
    //   99: iload_3
    //   100: i2l
    //   101: lload 4
    //   103: ladd
    //   104: invokevirtual 371	net/lingala/zip4j/model/Zip64EndCentralDirLocator:setOffsetZip64EndOfCentralDirRec	(J)V
    //   107: aload_2
    //   108: instanceof 72
    //   111: ifeq +76 -> 187
    //   114: aload_1
    //   115: invokevirtual 110	net/lingala/zip4j/model/ZipModel:getZip64EndCentralDirLocator	()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
    //   118: aload_2
    //   119: checkcast 72	net/lingala/zip4j/io/SplitOutputStream
    //   122: invokevirtual 91	net/lingala/zip4j/io/SplitOutputStream:getCurrSplitFileCounter	()I
    //   125: invokevirtual 121	net/lingala/zip4j/model/Zip64EndCentralDirLocator:setNoOfDiskStartOfZip64EndOfCentralDirRec	(I)V
    //   128: aload_1
    //   129: invokevirtual 110	net/lingala/zip4j/model/ZipModel:getZip64EndCentralDirLocator	()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
    //   132: aload_2
    //   133: checkcast 72	net/lingala/zip4j/io/SplitOutputStream
    //   136: invokevirtual 91	net/lingala/zip4j/io/SplitOutputStream:getCurrSplitFileCounter	()I
    //   139: iconst_1
    //   140: iadd
    //   141: invokevirtual 124	net/lingala/zip4j/model/Zip64EndCentralDirLocator:setTotNumberOfDiscs	(I)V
    //   144: aload_0
    //   145: aload_1
    //   146: aload_2
    //   147: iload_3
    //   148: lload 4
    //   150: aload 6
    //   152: invokespecial 373	net/lingala/zip4j/core/HeaderWriter:writeZip64EndOfCentralDirectoryRecord	(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;IJLjava/util/List;)V
    //   155: aload_0
    //   156: aload_1
    //   157: aload_2
    //   158: aload 6
    //   160: invokespecial 375	net/lingala/zip4j/core/HeaderWriter:writeZip64EndOfCentralDirectoryLocator	(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;Ljava/util/List;)V
    //   163: aload_0
    //   164: aload_1
    //   165: aload_2
    //   166: iload_3
    //   167: lload 4
    //   169: aload 6
    //   171: invokespecial 377	net/lingala/zip4j/core/HeaderWriter:writeEndOfCentralDirectoryRecord	(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;IJLjava/util/List;)V
    //   174: aload_0
    //   175: aload_1
    //   176: aload_2
    //   177: aload_0
    //   178: aload 6
    //   180: invokespecial 379	net/lingala/zip4j/core/HeaderWriter:byteArrayListToByteArray	(Ljava/util/List;)[B
    //   183: invokespecial 381	net/lingala/zip4j/core/HeaderWriter:writeZipHeaderBytes	(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;[B)V
    //   186: return
    //   187: aload_1
    //   188: invokevirtual 110	net/lingala/zip4j/model/ZipModel:getZip64EndCentralDirLocator	()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
    //   191: iconst_0
    //   192: invokevirtual 121	net/lingala/zip4j/model/Zip64EndCentralDirLocator:setNoOfDiskStartOfZip64EndOfCentralDirRec	(I)V
    //   195: aload_1
    //   196: invokevirtual 110	net/lingala/zip4j/model/ZipModel:getZip64EndCentralDirLocator	()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
    //   199: iconst_1
    //   200: invokevirtual 124	net/lingala/zip4j/model/Zip64EndCentralDirLocator:setTotNumberOfDiscs	(I)V
    //   203: goto -59 -> 144
    //   206: astore_1
    //   207: aload_1
    //   208: athrow
    //   209: astore_1
    //   210: new 17	net/lingala/zip4j/exception/ZipException
    //   213: dup
    //   214: aload_1
    //   215: invokespecial 133	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   218: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	219	0	this	HeaderWriter
    //   0	219	1	paramZipModel	ZipModel
    //   0	219	2	paramOutputStream	OutputStream
    //   51	116	3	i	int
    //   32	136	4	l	long
    //   41	138	6	localArrayList	ArrayList
    // Exception table:
    //   from	to	target	type
    //   19	77	206	net/lingala/zip4j/exception/ZipException
    //   77	95	206	net/lingala/zip4j/exception/ZipException
    //   95	144	206	net/lingala/zip4j/exception/ZipException
    //   144	163	206	net/lingala/zip4j/exception/ZipException
    //   163	186	206	net/lingala/zip4j/exception/ZipException
    //   187	203	206	net/lingala/zip4j/exception/ZipException
    //   19	77	209	java/lang/Exception
    //   77	95	209	java/lang/Exception
    //   95	144	209	java/lang/Exception
    //   144	163	209	java/lang/Exception
    //   163	186	209	java/lang/Exception
    //   187	203	209	java/lang/Exception
  }
  
  public void finalizeZipFileWithoutValidations(ZipModel paramZipModel, OutputStream paramOutputStream)
    throws ZipException
  {
    if ((paramZipModel == null) || (paramOutputStream == null)) {
      throw new ZipException("input parameters is null, cannot finalize zip file without validations");
    }
    try
    {
      ArrayList localArrayList = new ArrayList();
      long l = paramZipModel.getEndCentralDirRecord().getOffsetOfStartOfCentralDir();
      int i = writeCentralDirectory(paramZipModel, paramOutputStream, localArrayList);
      if (paramZipModel.isZip64Format())
      {
        if (paramZipModel.getZip64EndCentralDirRecord() == null) {
          paramZipModel.setZip64EndCentralDirRecord(new Zip64EndCentralDirRecord());
        }
        if (paramZipModel.getZip64EndCentralDirLocator() == null) {
          paramZipModel.setZip64EndCentralDirLocator(new Zip64EndCentralDirLocator());
        }
        paramZipModel.getZip64EndCentralDirLocator().setOffsetZip64EndOfCentralDirRec(i + l);
        writeZip64EndOfCentralDirectoryRecord(paramZipModel, paramOutputStream, i, l, localArrayList);
        writeZip64EndOfCentralDirectoryLocator(paramZipModel, paramOutputStream, localArrayList);
      }
      writeEndOfCentralDirectoryRecord(paramZipModel, paramOutputStream, i, l, localArrayList);
      writeZipHeaderBytes(paramZipModel, paramOutputStream, byteArrayListToByteArray(localArrayList));
      return;
    }
    catch (ZipException paramZipModel)
    {
      throw paramZipModel;
    }
    catch (Exception paramZipModel)
    {
      throw new ZipException(paramZipModel);
    }
  }
  
  public void updateLocalFileHeader(LocalFileHeader paramLocalFileHeader, long paramLong, int paramInt1, ZipModel paramZipModel, byte[] paramArrayOfByte, int paramInt2, SplitOutputStream paramSplitOutputStream)
    throws ZipException
  {
    if ((paramLocalFileHeader == null) || (paramLong < 0L) || (paramZipModel == null)) {
      throw new ZipException("invalid input parameters, cannot update local file header");
    }
    int i = 0;
    for (;;)
    {
      long l1;
      try
      {
        if (paramInt2 == paramSplitOutputStream.getCurrSplitFileCounter()) {
          break label319;
        }
        Object localObject2 = new File(paramZipModel.getZipFile());
        localObject1 = ((File)localObject2).getParent();
        localObject2 = Zip4jUtil.getZipFileNameWithoutExt(((File)localObject2).getName());
        localObject1 = (String)localObject1 + System.getProperty("file.separator");
        if (paramInt2 < 9)
        {
          localObject1 = (String)localObject1 + (String)localObject2 + ".z0" + (paramInt2 + 1);
          localObject1 = new SplitOutputStream(new File((String)localObject1));
          paramInt2 = 1;
          l1 = ((SplitOutputStream)localObject1).getFilePointer();
        }
        switch (paramInt1)
        {
        case 14: 
          if (paramInt2 == 0) {
            break label308;
          }
          ((SplitOutputStream)localObject1).close();
          return;
          localObject1 = (String)localObject1 + (String)localObject2 + ".z" + (paramInt2 + 1);
          continue;
          ((SplitOutputStream)localObject1).seek(paramInt1 + paramLong);
          ((SplitOutputStream)localObject1).write(paramArrayOfByte);
          break;
        case 18: 
        case 22: 
          l2 = paramInt1;
        }
      }
      catch (Exception paramLocalFileHeader)
      {
        throw new ZipException(paramLocalFileHeader);
      }
      long l2;
      updateCompressedSizeInLocalFileHeader((SplitOutputStream)localObject1, paramLocalFileHeader, paramLong, l2, paramArrayOfByte, paramZipModel.isZip64Format());
      continue;
      label308:
      paramSplitOutputStream.seek(l1);
      return;
      continue;
      label319:
      Object localObject1 = paramSplitOutputStream;
      paramInt2 = i;
    }
  }
  
  public int writeExtendedLocalHeader(LocalFileHeader paramLocalFileHeader, OutputStream paramOutputStream)
    throws ZipException, IOException
  {
    if ((paramLocalFileHeader == null) || (paramOutputStream == null)) {
      throw new ZipException("input parameters is null, cannot write extended local header");
    }
    ArrayList localArrayList = new ArrayList();
    byte[] arrayOfByte = new byte[4];
    Raw.writeIntLittleEndian(arrayOfByte, 0, 134695760);
    copyByteArrayToArrayList(arrayOfByte, localArrayList);
    Raw.writeIntLittleEndian(arrayOfByte, 0, (int)paramLocalFileHeader.getCrc32());
    copyByteArrayToArrayList(arrayOfByte, localArrayList);
    long l2 = paramLocalFileHeader.getCompressedSize();
    long l1 = l2;
    if (l2 >= 2147483647L) {
      l1 = 2147483647L;
    }
    Raw.writeIntLittleEndian(arrayOfByte, 0, (int)l1);
    copyByteArrayToArrayList(arrayOfByte, localArrayList);
    l2 = paramLocalFileHeader.getUncompressedSize();
    l1 = l2;
    if (l2 >= 2147483647L) {
      l1 = 2147483647L;
    }
    Raw.writeIntLittleEndian(arrayOfByte, 0, (int)l1);
    copyByteArrayToArrayList(arrayOfByte, localArrayList);
    paramLocalFileHeader = byteArrayListToByteArray(localArrayList);
    paramOutputStream.write(paramLocalFileHeader);
    return paramLocalFileHeader.length;
  }
  
  public int writeLocalFileHeader(ZipModel paramZipModel, LocalFileHeader paramLocalFileHeader, OutputStream paramOutputStream)
    throws ZipException
  {
    if (paramLocalFileHeader == null) {
      throw new ZipException("input parameters are null, cannot write local file header");
    }
    for (;;)
    {
      try
      {
        localArrayList = new ArrayList();
        arrayOfByte1 = new byte[2];
        arrayOfByte2 = new byte[4];
        arrayOfByte3 = new byte[8];
        Raw.writeIntLittleEndian(arrayOfByte2, 0, paramLocalFileHeader.getSignature());
        copyByteArrayToArrayList(arrayOfByte2, localArrayList);
        Raw.writeShortLittleEndian(arrayOfByte1, 0, (short)paramLocalFileHeader.getVersionNeededToExtract());
        copyByteArrayToArrayList(arrayOfByte1, localArrayList);
        copyByteArrayToArrayList(paramLocalFileHeader.getGeneralPurposeFlag(), localArrayList);
        Raw.writeShortLittleEndian(arrayOfByte1, 0, (short)paramLocalFileHeader.getCompressionMethod());
        copyByteArrayToArrayList(arrayOfByte1, localArrayList);
        Raw.writeIntLittleEndian(arrayOfByte2, 0, paramLocalFileHeader.getLastModFileTime());
        copyByteArrayToArrayList(arrayOfByte2, localArrayList);
        Raw.writeIntLittleEndian(arrayOfByte2, 0, (int)paramLocalFileHeader.getCrc32());
        copyByteArrayToArrayList(arrayOfByte2, localArrayList);
        j = 0;
        if (50L + paramLocalFileHeader.getUncompressedSize() < 4294967295L) {
          continue;
        }
        Raw.writeLongLittleEndian(arrayOfByte3, 0, 4294967295L);
        System.arraycopy(arrayOfByte3, 0, arrayOfByte2, 0, 4);
        copyByteArrayToArrayList(arrayOfByte2, localArrayList);
        copyByteArrayToArrayList(arrayOfByte2, localArrayList);
        paramZipModel.setZip64Format(true);
        j = 1;
        paramLocalFileHeader.setWriteComprSizeInZip64ExtraRecord(true);
        Raw.writeShortLittleEndian(arrayOfByte1, 0, (short)paramLocalFileHeader.getFileNameLength());
        copyByteArrayToArrayList(arrayOfByte1, localArrayList);
        int i = 0;
        if (j != 0) {
          i = 0 + 20;
        }
        int k = i;
        if (paramLocalFileHeader.getAesExtraDataRecord() != null) {
          k = i + 11;
        }
        Raw.writeShortLittleEndian(arrayOfByte1, 0, (short)k);
        copyByteArrayToArrayList(arrayOfByte1, localArrayList);
        if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramZipModel.getFileNameCharset())) {
          continue;
        }
        copyByteArrayToArrayList(paramLocalFileHeader.getFileName().getBytes(paramZipModel.getFileNameCharset()), localArrayList);
      }
      catch (ZipException paramZipModel)
      {
        ArrayList localArrayList;
        byte[] arrayOfByte1;
        byte[] arrayOfByte2;
        byte[] arrayOfByte3;
        int j;
        throw paramZipModel;
        copyByteArrayToArrayList(Zip4jUtil.convertCharset(paramLocalFileHeader.getFileName()), localArrayList);
        continue;
      }
      catch (Exception paramZipModel)
      {
        throw new ZipException(paramZipModel);
      }
      if (j != 0)
      {
        Raw.writeShortLittleEndian(arrayOfByte1, 0, (short)1);
        copyByteArrayToArrayList(arrayOfByte1, localArrayList);
        Raw.writeShortLittleEndian(arrayOfByte1, 0, (short)16);
        copyByteArrayToArrayList(arrayOfByte1, localArrayList);
        Raw.writeLongLittleEndian(arrayOfByte3, 0, paramLocalFileHeader.getUncompressedSize());
        copyByteArrayToArrayList(arrayOfByte3, localArrayList);
        copyByteArrayToArrayList(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }, localArrayList);
      }
      if (paramLocalFileHeader.getAesExtraDataRecord() != null)
      {
        paramZipModel = paramLocalFileHeader.getAesExtraDataRecord();
        Raw.writeShortLittleEndian(arrayOfByte1, 0, (short)(int)paramZipModel.getSignature());
        copyByteArrayToArrayList(arrayOfByte1, localArrayList);
        Raw.writeShortLittleEndian(arrayOfByte1, 0, (short)paramZipModel.getDataSize());
        copyByteArrayToArrayList(arrayOfByte1, localArrayList);
        Raw.writeShortLittleEndian(arrayOfByte1, 0, (short)paramZipModel.getVersionNumber());
        copyByteArrayToArrayList(arrayOfByte1, localArrayList);
        copyByteArrayToArrayList(paramZipModel.getVendorID().getBytes(), localArrayList);
        copyByteArrayToArrayList(new byte[] { (byte)paramZipModel.getAesStrength() }, localArrayList);
        Raw.writeShortLittleEndian(arrayOfByte1, 0, (short)paramZipModel.getCompressionMethod());
        copyByteArrayToArrayList(arrayOfByte1, localArrayList);
      }
      paramZipModel = byteArrayListToByteArray(localArrayList);
      paramOutputStream.write(paramZipModel);
      return paramZipModel.length;
      Raw.writeLongLittleEndian(arrayOfByte3, 0, paramLocalFileHeader.getCompressedSize());
      System.arraycopy(arrayOfByte3, 0, arrayOfByte2, 0, 4);
      copyByteArrayToArrayList(arrayOfByte2, localArrayList);
      Raw.writeLongLittleEndian(arrayOfByte3, 0, paramLocalFileHeader.getUncompressedSize());
      System.arraycopy(arrayOfByte3, 0, arrayOfByte2, 0, 4);
      copyByteArrayToArrayList(arrayOfByte2, localArrayList);
      paramLocalFileHeader.setWriteComprSizeInZip64ExtraRecord(false);
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/core/HeaderWriter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */