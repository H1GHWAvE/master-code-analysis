package net.lingala.zip4j.unzip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.zip.CRC32;
import net.lingala.zip4j.crypto.AESDecrypter;
import net.lingala.zip4j.crypto.IDecrypter;
import net.lingala.zip4j.crypto.StandardDecrypter;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.InflaterInputStream;
import net.lingala.zip4j.io.PartInputStream;
import net.lingala.zip4j.io.ZipInputStream;
import net.lingala.zip4j.model.AESExtraDataRecord;
import net.lingala.zip4j.model.EndCentralDirRecord;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.LocalFileHeader;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.util.Raw;
import net.lingala.zip4j.util.Zip4jUtil;

public class UnzipEngine
{
  private CRC32 crc;
  private int currSplitFileCounter = 0;
  private IDecrypter decrypter;
  private FileHeader fileHeader;
  private LocalFileHeader localFileHeader;
  private ZipModel zipModel;
  
  public UnzipEngine(ZipModel paramZipModel, FileHeader paramFileHeader)
    throws ZipException
  {
    if ((paramZipModel == null) || (paramFileHeader == null)) {
      throw new ZipException("Invalid parameters passed to StoreUnzip. One or more of the parameters were null");
    }
    this.zipModel = paramZipModel;
    this.fileHeader = paramFileHeader;
    this.crc = new CRC32();
  }
  
  private int calculateAESSaltLength(AESExtraDataRecord paramAESExtraDataRecord)
    throws ZipException
  {
    if (paramAESExtraDataRecord == null) {
      throw new ZipException("unable to determine salt length: AESExtraDataRecord is null");
    }
    switch (paramAESExtraDataRecord.getAesStrength())
    {
    default: 
      throw new ZipException("unable to determine salt length: invalid aes key strength");
    case 1: 
      return 8;
    case 2: 
      return 12;
    }
    return 16;
  }
  
  /* Error */
  private boolean checkLocalHeader()
    throws ZipException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 6
    //   3: aconst_null
    //   4: astore 5
    //   6: aload_0
    //   7: invokespecial 65	net/lingala/zip4j/unzip/UnzipEngine:checkSplitFile	()Ljava/io/RandomAccessFile;
    //   10: astore 8
    //   12: aload 8
    //   14: astore 7
    //   16: aload 8
    //   18: ifnonnull +36 -> 54
    //   21: aload 8
    //   23: astore 5
    //   25: aload 8
    //   27: astore 6
    //   29: new 67	java/io/RandomAccessFile
    //   32: dup
    //   33: new 69	java/io/File
    //   36: dup
    //   37: aload_0
    //   38: getfield 32	net/lingala/zip4j/unzip/UnzipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   41: invokevirtual 75	net/lingala/zip4j/model/ZipModel:getZipFile	()Ljava/lang/String;
    //   44: invokespecial 76	java/io/File:<init>	(Ljava/lang/String;)V
    //   47: ldc 78
    //   49: invokespecial 81	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   52: astore 7
    //   54: aload 7
    //   56: astore 5
    //   58: aload 7
    //   60: astore 6
    //   62: aload_0
    //   63: new 83	net/lingala/zip4j/core/HeaderReader
    //   66: dup
    //   67: aload 7
    //   69: invokespecial 86	net/lingala/zip4j/core/HeaderReader:<init>	(Ljava/io/RandomAccessFile;)V
    //   72: aload_0
    //   73: getfield 34	net/lingala/zip4j/unzip/UnzipEngine:fileHeader	Lnet/lingala/zip4j/model/FileHeader;
    //   76: invokevirtual 90	net/lingala/zip4j/core/HeaderReader:readLocalFileHeader	(Lnet/lingala/zip4j/model/FileHeader;)Lnet/lingala/zip4j/model/LocalFileHeader;
    //   79: putfield 92	net/lingala/zip4j/unzip/UnzipEngine:localFileHeader	Lnet/lingala/zip4j/model/LocalFileHeader;
    //   82: aload 7
    //   84: astore 5
    //   86: aload 7
    //   88: astore 6
    //   90: aload_0
    //   91: getfield 92	net/lingala/zip4j/unzip/UnzipEngine:localFileHeader	Lnet/lingala/zip4j/model/LocalFileHeader;
    //   94: ifnonnull +52 -> 146
    //   97: aload 7
    //   99: astore 5
    //   101: aload 7
    //   103: astore 6
    //   105: new 20	net/lingala/zip4j/exception/ZipException
    //   108: dup
    //   109: ldc 94
    //   111: invokespecial 30	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   114: athrow
    //   115: astore 7
    //   117: aload 5
    //   119: astore 6
    //   121: new 20	net/lingala/zip4j/exception/ZipException
    //   124: dup
    //   125: aload 7
    //   127: invokespecial 97	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   130: athrow
    //   131: astore 5
    //   133: aload 6
    //   135: ifnull +8 -> 143
    //   138: aload 6
    //   140: invokevirtual 100	java/io/RandomAccessFile:close	()V
    //   143: aload 5
    //   145: athrow
    //   146: aload 7
    //   148: astore 5
    //   150: aload 7
    //   152: astore 6
    //   154: aload_0
    //   155: getfield 92	net/lingala/zip4j/unzip/UnzipEngine:localFileHeader	Lnet/lingala/zip4j/model/LocalFileHeader;
    //   158: invokevirtual 105	net/lingala/zip4j/model/LocalFileHeader:getCompressionMethod	()I
    //   161: istore_1
    //   162: aload 7
    //   164: astore 5
    //   166: aload 7
    //   168: astore 6
    //   170: aload_0
    //   171: getfield 34	net/lingala/zip4j/unzip/UnzipEngine:fileHeader	Lnet/lingala/zip4j/model/FileHeader;
    //   174: invokevirtual 108	net/lingala/zip4j/model/FileHeader:getCompressionMethod	()I
    //   177: istore_2
    //   178: iload_1
    //   179: iload_2
    //   180: if_icmpeq +24 -> 204
    //   183: iconst_0
    //   184: istore 4
    //   186: iload 4
    //   188: istore_3
    //   189: aload 7
    //   191: ifnull +11 -> 202
    //   194: aload 7
    //   196: invokevirtual 100	java/io/RandomAccessFile:close	()V
    //   199: iload 4
    //   201: istore_3
    //   202: iload_3
    //   203: ireturn
    //   204: iconst_1
    //   205: istore_3
    //   206: aload 7
    //   208: ifnull -6 -> 202
    //   211: aload 7
    //   213: invokevirtual 100	java/io/RandomAccessFile:close	()V
    //   216: iconst_1
    //   217: ireturn
    //   218: astore 5
    //   220: iconst_1
    //   221: ireturn
    //   222: astore 5
    //   224: iconst_0
    //   225: ireturn
    //   226: astore 5
    //   228: iconst_0
    //   229: ireturn
    //   230: astore 5
    //   232: iconst_1
    //   233: ireturn
    //   234: astore 6
    //   236: goto -93 -> 143
    //   239: astore 6
    //   241: goto -98 -> 143
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	244	0	this	UnzipEngine
    //   161	20	1	i	int
    //   177	4	2	j	int
    //   188	18	3	bool1	boolean
    //   184	16	4	bool2	boolean
    //   4	114	5	localObject1	Object
    //   131	13	5	localObject2	Object
    //   148	17	5	localObject3	Object
    //   218	1	5	localIOException1	IOException
    //   222	1	5	localIOException2	IOException
    //   226	1	5	localException1	Exception
    //   230	1	5	localException2	Exception
    //   1	168	6	localObject4	Object
    //   234	1	6	localIOException3	IOException
    //   239	1	6	localException3	Exception
    //   14	88	7	localRandomAccessFile1	RandomAccessFile
    //   115	97	7	localFileNotFoundException	FileNotFoundException
    //   10	16	8	localRandomAccessFile2	RandomAccessFile
    // Exception table:
    //   from	to	target	type
    //   6	12	115	java/io/FileNotFoundException
    //   29	54	115	java/io/FileNotFoundException
    //   62	82	115	java/io/FileNotFoundException
    //   90	97	115	java/io/FileNotFoundException
    //   105	115	115	java/io/FileNotFoundException
    //   154	162	115	java/io/FileNotFoundException
    //   170	178	115	java/io/FileNotFoundException
    //   6	12	131	finally
    //   29	54	131	finally
    //   62	82	131	finally
    //   90	97	131	finally
    //   105	115	131	finally
    //   121	131	131	finally
    //   154	162	131	finally
    //   170	178	131	finally
    //   211	216	218	java/io/IOException
    //   194	199	222	java/io/IOException
    //   194	199	226	java/lang/Exception
    //   211	216	230	java/lang/Exception
    //   138	143	234	java/io/IOException
    //   138	143	239	java/lang/Exception
  }
  
  private RandomAccessFile checkSplitFile()
    throws ZipException
  {
    if (this.zipModel.isSplitArchive())
    {
      int i = this.fileHeader.getDiskNumberStart();
      this.currSplitFileCounter = (i + 1);
      Object localObject = this.zipModel.getZipFile();
      if (i == this.zipModel.getEndCentralDirRecord().getNoOfThisDisk()) {
        localObject = this.zipModel.getZipFile();
      }
      try
      {
        RandomAccessFile localRandomAccessFile2 = new RandomAccessFile((String)localObject, "r");
        localObject = localRandomAccessFile2;
        if (this.currSplitFileCounter != 1) {
          return localObject;
        }
        byte[] arrayOfByte = new byte[4];
        localRandomAccessFile2.read(arrayOfByte);
        localObject = localRandomAccessFile2;
        if (Raw.readIntLittleEndian(arrayOfByte, 0) == 134695760L) {
          return ???;
        }
        throw new ZipException("invalid first part split file signature");
      }
      catch (FileNotFoundException localFileNotFoundException)
      {
        for (;;)
        {
          throw new ZipException(localFileNotFoundException);
          String str;
          if (i >= 9) {
            str = localFileNotFoundException.substring(0, localFileNotFoundException.lastIndexOf(".")) + ".z" + (i + 1);
          } else {
            str = str.substring(0, str.lastIndexOf(".")) + ".z0" + (i + 1);
          }
        }
      }
      catch (IOException localIOException)
      {
        throw new ZipException(localIOException);
      }
    }
    RandomAccessFile localRandomAccessFile1 = null;
    return localRandomAccessFile1;
  }
  
  /* Error */
  private void closeStreams(java.io.InputStream paramInputStream, java.io.OutputStream paramOutputStream)
    throws ZipException
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnull +7 -> 8
    //   4: aload_1
    //   5: invokevirtual 174	java/io/InputStream:close	()V
    //   8: aload_2
    //   9: ifnull +7 -> 16
    //   12: aload_2
    //   13: invokevirtual 177	java/io/OutputStream:close	()V
    //   16: return
    //   17: astore_1
    //   18: aload_1
    //   19: ifnull +48 -> 67
    //   22: aload_1
    //   23: invokevirtual 180	java/io/IOException:getMessage	()Ljava/lang/String;
    //   26: invokestatic 186	net/lingala/zip4j/util/Zip4jUtil:isStringNotNullAndNotEmpty	(Ljava/lang/String;)Z
    //   29: ifeq +38 -> 67
    //   32: aload_1
    //   33: invokevirtual 180	java/io/IOException:getMessage	()Ljava/lang/String;
    //   36: ldc -68
    //   38: invokevirtual 191	java/lang/String:indexOf	(Ljava/lang/String;)I
    //   41: iflt +26 -> 67
    //   44: new 20	net/lingala/zip4j/exception/ZipException
    //   47: dup
    //   48: aload_1
    //   49: invokevirtual 180	java/io/IOException:getMessage	()Ljava/lang/String;
    //   52: invokespecial 30	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   55: athrow
    //   56: astore_1
    //   57: aload_2
    //   58: ifnull +7 -> 65
    //   61: aload_2
    //   62: invokevirtual 177	java/io/OutputStream:close	()V
    //   65: aload_1
    //   66: athrow
    //   67: aload_2
    //   68: ifnull -52 -> 16
    //   71: aload_2
    //   72: invokevirtual 177	java/io/OutputStream:close	()V
    //   75: return
    //   76: astore_1
    //   77: return
    //   78: astore_1
    //   79: return
    //   80: astore_2
    //   81: goto -16 -> 65
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	84	0	this	UnzipEngine
    //   0	84	1	paramInputStream	java.io.InputStream
    //   0	84	2	paramOutputStream	java.io.OutputStream
    // Exception table:
    //   from	to	target	type
    //   4	8	17	java/io/IOException
    //   4	8	56	finally
    //   22	56	56	finally
    //   12	16	76	java/io/IOException
    //   71	75	78	java/io/IOException
    //   61	65	80	java/io/IOException
  }
  
  private RandomAccessFile createFileHandler(String paramString)
    throws ZipException
  {
    if ((this.zipModel == null) || (!Zip4jUtil.isStringNotNullAndNotEmpty(this.zipModel.getZipFile()))) {
      throw new ZipException("input parameter is null in getFilePointer");
    }
    try
    {
      if (this.zipModel.isSplitArchive()) {
        return checkSplitFile();
      }
      paramString = new RandomAccessFile(new File(this.zipModel.getZipFile()), paramString);
      return paramString;
    }
    catch (FileNotFoundException paramString)
    {
      throw new ZipException(paramString);
    }
    catch (Exception paramString)
    {
      throw new ZipException(paramString);
    }
  }
  
  private byte[] getAESPasswordVerifier(RandomAccessFile paramRandomAccessFile)
    throws ZipException
  {
    try
    {
      byte[] arrayOfByte = new byte[2];
      paramRandomAccessFile.read(arrayOfByte);
      return arrayOfByte;
    }
    catch (IOException paramRandomAccessFile)
    {
      throw new ZipException(paramRandomAccessFile);
    }
  }
  
  private byte[] getAESSalt(RandomAccessFile paramRandomAccessFile)
    throws ZipException
  {
    if (this.localFileHeader.getAesExtraDataRecord() == null) {
      return null;
    }
    try
    {
      byte[] arrayOfByte = new byte[calculateAESSaltLength(this.localFileHeader.getAesExtraDataRecord())];
      paramRandomAccessFile.seek(this.localFileHeader.getOffsetStartOfData());
      paramRandomAccessFile.read(arrayOfByte);
      return arrayOfByte;
    }
    catch (IOException paramRandomAccessFile)
    {
      throw new ZipException(paramRandomAccessFile);
    }
  }
  
  private String getOutputFileNameWithPath(String paramString1, String paramString2)
    throws ZipException
  {
    if (Zip4jUtil.isStringNotNullAndNotEmpty(paramString2)) {}
    for (;;)
    {
      return paramString1 + System.getProperty("file.separator") + paramString2;
      paramString2 = this.fileHeader.getFileName();
    }
  }
  
  private FileOutputStream getOutputStream(String paramString1, String paramString2)
    throws ZipException
  {
    if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramString1)) {
      throw new ZipException("invalid output path");
    }
    try
    {
      paramString1 = new File(getOutputFileNameWithPath(paramString1, paramString2));
      if (!paramString1.getParentFile().exists()) {
        paramString1.getParentFile().mkdirs();
      }
      if (paramString1.exists()) {
        paramString1.delete();
      }
      paramString1 = new FileOutputStream(paramString1);
      return paramString1;
    }
    catch (FileNotFoundException paramString1)
    {
      throw new ZipException(paramString1);
    }
  }
  
  private byte[] getStandardDecrypterHeaderBytes(RandomAccessFile paramRandomAccessFile)
    throws ZipException
  {
    try
    {
      byte[] arrayOfByte = new byte[12];
      paramRandomAccessFile.seek(this.localFileHeader.getOffsetStartOfData());
      paramRandomAccessFile.read(arrayOfByte, 0, 12);
      return arrayOfByte;
    }
    catch (IOException paramRandomAccessFile)
    {
      throw new ZipException(paramRandomAccessFile);
    }
    catch (Exception paramRandomAccessFile)
    {
      throw new ZipException(paramRandomAccessFile);
    }
  }
  
  private void init(RandomAccessFile paramRandomAccessFile)
    throws ZipException
  {
    if (this.localFileHeader == null) {
      throw new ZipException("local file header is null, cannot initialize input stream");
    }
    try
    {
      initDecrypter(paramRandomAccessFile);
      return;
    }
    catch (ZipException paramRandomAccessFile)
    {
      throw paramRandomAccessFile;
    }
    catch (Exception paramRandomAccessFile)
    {
      throw new ZipException(paramRandomAccessFile);
    }
  }
  
  private void initDecrypter(RandomAccessFile paramRandomAccessFile)
    throws ZipException
  {
    if (this.localFileHeader == null) {
      throw new ZipException("local file header is null, cannot init decrypter");
    }
    if (this.localFileHeader.isEncrypted())
    {
      if (this.localFileHeader.getEncryptionMethod() == 0) {
        this.decrypter = new StandardDecrypter(this.fileHeader, getStandardDecrypterHeaderBytes(paramRandomAccessFile));
      }
    }
    else {
      return;
    }
    if (this.localFileHeader.getEncryptionMethod() == 99)
    {
      this.decrypter = new AESDecrypter(this.localFileHeader, getAESSalt(paramRandomAccessFile), getAESPasswordVerifier(paramRandomAccessFile));
      return;
    }
    throw new ZipException("unsupported encryption method");
  }
  
  public void checkCRC()
    throws ZipException
  {
    if (this.fileHeader != null)
    {
      Object localObject1;
      Object localObject2;
      if (this.fileHeader.getEncryptionMethod() == 99)
      {
        if ((this.decrypter != null) && ((this.decrypter instanceof AESDecrypter)))
        {
          localObject1 = ((AESDecrypter)this.decrypter).getCalculatedAuthenticationBytes();
          localObject2 = ((AESDecrypter)this.decrypter).getStoredMac();
          byte[] arrayOfByte = new byte[10];
          if ((arrayOfByte == null) || (localObject2 == null)) {
            throw new ZipException("CRC (MAC) check failed for " + this.fileHeader.getFileName());
          }
          System.arraycopy(localObject1, 0, arrayOfByte, 0, 10);
          if (!Arrays.equals(arrayOfByte, (byte[])localObject2)) {
            throw new ZipException("invalid CRC (MAC) for file: " + this.fileHeader.getFileName());
          }
        }
      }
      else if ((this.crc.getValue() & 0xFFFFFFFF) != this.fileHeader.getCrc32())
      {
        localObject2 = "invalid CRC for file: " + this.fileHeader.getFileName();
        localObject1 = localObject2;
        if (this.localFileHeader.isEncrypted())
        {
          localObject1 = localObject2;
          if (this.localFileHeader.getEncryptionMethod() == 0) {
            localObject1 = (String)localObject2 + " - Wrong Password?";
          }
        }
        throw new ZipException((String)localObject1);
      }
    }
  }
  
  public IDecrypter getDecrypter()
  {
    return this.decrypter;
  }
  
  public FileHeader getFileHeader()
  {
    return this.fileHeader;
  }
  
  public ZipInputStream getInputStream()
    throws ZipException
  {
    if (this.fileHeader == null) {
      throw new ZipException("file header is null, cannot get inputstream");
    }
    Object localObject3 = null;
    Object localObject1 = null;
    try
    {
      localObject5 = createFileHandler("r");
      localObject1 = localObject5;
      localObject3 = localObject5;
      if (!checkLocalHeader())
      {
        localObject1 = localObject5;
        localObject3 = localObject5;
        throw new ZipException("local header and file header do not match");
      }
    }
    catch (ZipException localZipException)
    {
      if (localObject1 == null) {}
    }
    catch (Exception localException)
    {
      try
      {
        ((RandomAccessFile)localObject1).close();
        throw localZipException;
        localObject1 = localObject5;
        localObject4 = localObject5;
        init((RandomAccessFile)localObject5);
        localObject1 = localObject5;
        localObject4 = localObject5;
        l4 = this.localFileHeader.getCompressedSize();
        localObject1 = localObject5;
        localObject4 = localObject5;
        l3 = this.localFileHeader.getOffsetStartOfData();
        localObject1 = localObject5;
        l1 = l3;
        l2 = l4;
        localObject4 = localObject5;
        int i;
        if (this.localFileHeader.isEncrypted())
        {
          localObject1 = localObject5;
          localObject4 = localObject5;
          if (this.localFileHeader.getEncryptionMethod() != 99) {
            break label483;
          }
          localObject1 = localObject5;
          localObject4 = localObject5;
          if ((this.decrypter instanceof AESDecrypter))
          {
            localObject1 = localObject5;
            localObject4 = localObject5;
            i = ((AESDecrypter)this.decrypter).getSaltLength();
            localObject1 = localObject5;
            localObject4 = localObject5;
            l2 = l4 - (((AESDecrypter)this.decrypter).getPasswordVerifierLength() + i + 10);
            localObject1 = localObject5;
            localObject4 = localObject5;
            i = ((AESDecrypter)this.decrypter).getSaltLength();
            localObject1 = localObject5;
            localObject4 = localObject5;
            l1 = l3 + (((AESDecrypter)this.decrypter).getPasswordVerifierLength() + i);
          }
        }
        else
        {
          localObject1 = localObject5;
          localObject4 = localObject5;
          i = this.fileHeader.getCompressionMethod();
          localObject1 = localObject5;
          localObject4 = localObject5;
          if (this.fileHeader.getEncryptionMethod() == 99)
          {
            localObject1 = localObject5;
            localObject4 = localObject5;
            if (this.fileHeader.getAesExtraDataRecord() == null) {
              break label526;
            }
            localObject1 = localObject5;
            localObject4 = localObject5;
            i = this.fileHeader.getAesExtraDataRecord().getCompressionMethod();
          }
          localObject1 = localObject5;
          localObject4 = localObject5;
          ((RandomAccessFile)localObject5).seek(l1);
          switch (i)
          {
          case 0: 
            localObject1 = localObject5;
            localObject4 = localObject5;
            throw new ZipException("compression type not supported");
            localException = localException;
            if (localObject4 == null) {}
            break;
          }
        }
      }
      catch (IOException localIOException1)
      {
        for (;;)
        {
          try
          {
            Object localObject5;
            long l4;
            long l3;
            ((RandomAccessFile)localObject4).close();
            throw new ZipException(localException);
            Object localObject2 = localObject5;
            Object localObject4 = localObject5;
            throw new ZipException("invalid decryptor when trying to calculate compressed size for AES encrypted file: " + this.fileHeader.getFileName());
            label483:
            localObject2 = localObject5;
            long l1 = l3;
            long l2 = l4;
            localObject4 = localObject5;
            if (this.localFileHeader.getEncryptionMethod() == 0)
            {
              l2 = l4 - 12L;
              l1 = l3 + 12L;
              continue;
              label526:
              localObject2 = localObject5;
              localObject4 = localObject5;
              throw new ZipException("AESExtraDataRecord does not exist for AES encrypted file: " + this.fileHeader.getFileName());
              localObject2 = localObject5;
              localObject4 = localObject5;
              return new ZipInputStream(new PartInputStream((RandomAccessFile)localObject5, l1, l2, this));
              localObject2 = localObject5;
              localObject4 = localObject5;
              localObject5 = new ZipInputStream(new InflaterInputStream((RandomAccessFile)localObject5, l1, l2, this));
              return (ZipInputStream)localObject5;
              localIOException1 = localIOException1;
            }
          }
          catch (IOException localIOException2) {}
        }
      }
    }
  }
  
  public LocalFileHeader getLocalFileHeader()
  {
    return this.localFileHeader;
  }
  
  public ZipModel getZipModel()
  {
    return this.zipModel;
  }
  
  public RandomAccessFile startNextSplitFile()
    throws IOException, FileNotFoundException
  {
    String str1 = this.zipModel.getZipFile();
    if (this.currSplitFileCounter == this.zipModel.getEndCentralDirRecord().getNoOfThisDisk()) {
      str1 = this.zipModel.getZipFile();
    }
    String str2;
    for (;;)
    {
      this.currSplitFileCounter += 1;
      try
      {
        if (Zip4jUtil.checkFileExists(str1)) {
          break;
        }
        throw new IOException("zip split file does not exist: " + str1);
      }
      catch (ZipException localZipException)
      {
        throw new IOException(localZipException.getMessage());
      }
      if (this.currSplitFileCounter >= 9) {
        str2 = localZipException.substring(0, localZipException.lastIndexOf(".")) + ".z" + (this.currSplitFileCounter + 1);
      } else {
        str2 = str2.substring(0, str2.lastIndexOf(".")) + ".z0" + (this.currSplitFileCounter + 1);
      }
    }
    return new RandomAccessFile(str2, "r");
  }
  
  /* Error */
  public void unzipFile(net.lingala.zip4j.progress.ProgressMonitor paramProgressMonitor, String paramString1, String paramString2, net.lingala.zip4j.model.UnzipParameters paramUnzipParameters)
    throws ZipException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 32	net/lingala/zip4j/unzip/UnzipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   4: ifnull +17 -> 21
    //   7: aload_0
    //   8: getfield 34	net/lingala/zip4j/unzip/UnzipEngine:fileHeader	Lnet/lingala/zip4j/model/FileHeader;
    //   11: ifnull +10 -> 21
    //   14: aload_2
    //   15: invokestatic 186	net/lingala/zip4j/util/Zip4jUtil:isStringNotNullAndNotEmpty	(Ljava/lang/String;)Z
    //   18: ifne +14 -> 32
    //   21: new 20	net/lingala/zip4j/exception/ZipException
    //   24: dup
    //   25: ldc_w 384
    //   28: invokespecial 30	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   31: athrow
    //   32: aconst_null
    //   33: astore 16
    //   35: aconst_null
    //   36: astore 17
    //   38: aconst_null
    //   39: astore 12
    //   41: aconst_null
    //   42: astore 14
    //   44: aconst_null
    //   45: astore 15
    //   47: aconst_null
    //   48: astore 13
    //   50: aload 12
    //   52: astore 8
    //   54: aload 13
    //   56: astore 9
    //   58: aload 16
    //   60: astore 6
    //   62: aload 14
    //   64: astore 7
    //   66: aload 17
    //   68: astore 10
    //   70: aload 15
    //   72: astore 11
    //   74: sipush 4096
    //   77: newarray <illegal type>
    //   79: astore 18
    //   81: aload 12
    //   83: astore 8
    //   85: aload 13
    //   87: astore 9
    //   89: aload 16
    //   91: astore 6
    //   93: aload 14
    //   95: astore 7
    //   97: aload 17
    //   99: astore 10
    //   101: aload 15
    //   103: astore 11
    //   105: aload_0
    //   106: invokevirtual 386	net/lingala/zip4j/unzip/UnzipEngine:getInputStream	()Lnet/lingala/zip4j/io/ZipInputStream;
    //   109: astore 12
    //   111: aload 12
    //   113: astore 8
    //   115: aload 13
    //   117: astore 9
    //   119: aload 12
    //   121: astore 6
    //   123: aload 14
    //   125: astore 7
    //   127: aload 12
    //   129: astore 10
    //   131: aload 15
    //   133: astore 11
    //   135: aload_0
    //   136: aload_2
    //   137: aload_3
    //   138: invokespecial 388	net/lingala/zip4j/unzip/UnzipEngine:getOutputStream	(Ljava/lang/String;Ljava/lang/String;)Ljava/io/FileOutputStream;
    //   141: astore 13
    //   143: aload 12
    //   145: astore 8
    //   147: aload 13
    //   149: astore 9
    //   151: aload 12
    //   153: astore 6
    //   155: aload 13
    //   157: astore 7
    //   159: aload 12
    //   161: astore 10
    //   163: aload 13
    //   165: astore 11
    //   167: aload 12
    //   169: aload 18
    //   171: invokevirtual 389	java/io/InputStream:read	([B)I
    //   174: istore 5
    //   176: iload 5
    //   178: iconst_m1
    //   179: if_icmpeq +166 -> 345
    //   182: aload 12
    //   184: astore 8
    //   186: aload 13
    //   188: astore 9
    //   190: aload 12
    //   192: astore 6
    //   194: aload 13
    //   196: astore 7
    //   198: aload 12
    //   200: astore 10
    //   202: aload 13
    //   204: astore 11
    //   206: aload 13
    //   208: aload 18
    //   210: iconst_0
    //   211: iload 5
    //   213: invokevirtual 393	java/io/OutputStream:write	([BII)V
    //   216: aload 12
    //   218: astore 8
    //   220: aload 13
    //   222: astore 9
    //   224: aload 12
    //   226: astore 6
    //   228: aload 13
    //   230: astore 7
    //   232: aload 12
    //   234: astore 10
    //   236: aload 13
    //   238: astore 11
    //   240: aload_1
    //   241: iload 5
    //   243: i2l
    //   244: invokevirtual 398	net/lingala/zip4j/progress/ProgressMonitor:updateWorkCompleted	(J)V
    //   247: aload 12
    //   249: astore 8
    //   251: aload 13
    //   253: astore 9
    //   255: aload 12
    //   257: astore 6
    //   259: aload 13
    //   261: astore 7
    //   263: aload 12
    //   265: astore 10
    //   267: aload 13
    //   269: astore 11
    //   271: aload_1
    //   272: invokevirtual 401	net/lingala/zip4j/progress/ProgressMonitor:isCancelAllTasks	()Z
    //   275: ifeq -132 -> 143
    //   278: aload 12
    //   280: astore 8
    //   282: aload 13
    //   284: astore 9
    //   286: aload 12
    //   288: astore 6
    //   290: aload 13
    //   292: astore 7
    //   294: aload 12
    //   296: astore 10
    //   298: aload 13
    //   300: astore 11
    //   302: aload_1
    //   303: iconst_3
    //   304: invokevirtual 405	net/lingala/zip4j/progress/ProgressMonitor:setResult	(I)V
    //   307: aload 12
    //   309: astore 8
    //   311: aload 13
    //   313: astore 9
    //   315: aload 12
    //   317: astore 6
    //   319: aload 13
    //   321: astore 7
    //   323: aload 12
    //   325: astore 10
    //   327: aload 13
    //   329: astore 11
    //   331: aload_1
    //   332: iconst_0
    //   333: invokevirtual 408	net/lingala/zip4j/progress/ProgressMonitor:setState	(I)V
    //   336: aload_0
    //   337: aload 12
    //   339: aload 13
    //   341: invokespecial 410	net/lingala/zip4j/unzip/UnzipEngine:closeStreams	(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    //   344: return
    //   345: aload 12
    //   347: astore 8
    //   349: aload 13
    //   351: astore 9
    //   353: aload 12
    //   355: astore 6
    //   357: aload 13
    //   359: astore 7
    //   361: aload 12
    //   363: astore 10
    //   365: aload 13
    //   367: astore 11
    //   369: aload_0
    //   370: aload 12
    //   372: aload 13
    //   374: invokespecial 410	net/lingala/zip4j/unzip/UnzipEngine:closeStreams	(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    //   377: aload 12
    //   379: astore 8
    //   381: aload 13
    //   383: astore 9
    //   385: aload 12
    //   387: astore 6
    //   389: aload 13
    //   391: astore 7
    //   393: aload 12
    //   395: astore 10
    //   397: aload 13
    //   399: astore 11
    //   401: aload_0
    //   402: getfield 34	net/lingala/zip4j/unzip/UnzipEngine:fileHeader	Lnet/lingala/zip4j/model/FileHeader;
    //   405: new 69	java/io/File
    //   408: dup
    //   409: aload_0
    //   410: aload_2
    //   411: aload_3
    //   412: invokespecial 231	net/lingala/zip4j/unzip/UnzipEngine:getOutputFileNameWithPath	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   415: invokespecial 76	java/io/File:<init>	(Ljava/lang/String;)V
    //   418: aload 4
    //   420: invokestatic 416	net/lingala/zip4j/unzip/UnzipUtil:applyFileAttributes	(Lnet/lingala/zip4j/model/FileHeader;Ljava/io/File;Lnet/lingala/zip4j/model/UnzipParameters;)V
    //   423: aload_0
    //   424: aload 12
    //   426: aload 13
    //   428: invokespecial 410	net/lingala/zip4j/unzip/UnzipEngine:closeStreams	(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    //   431: return
    //   432: astore_1
    //   433: aload 8
    //   435: astore 6
    //   437: aload 9
    //   439: astore 7
    //   441: new 20	net/lingala/zip4j/exception/ZipException
    //   444: dup
    //   445: aload_1
    //   446: invokespecial 97	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   449: athrow
    //   450: astore_1
    //   451: aload_0
    //   452: aload 6
    //   454: aload 7
    //   456: invokespecial 410	net/lingala/zip4j/unzip/UnzipEngine:closeStreams	(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    //   459: aload_1
    //   460: athrow
    //   461: astore_1
    //   462: aload 10
    //   464: astore 6
    //   466: aload 11
    //   468: astore 7
    //   470: new 20	net/lingala/zip4j/exception/ZipException
    //   473: dup
    //   474: aload_1
    //   475: invokespecial 97	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   478: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	479	0	this	UnzipEngine
    //   0	479	1	paramProgressMonitor	net.lingala.zip4j.progress.ProgressMonitor
    //   0	479	2	paramString1	String
    //   0	479	3	paramString2	String
    //   0	479	4	paramUnzipParameters	net.lingala.zip4j.model.UnzipParameters
    //   174	68	5	i	int
    //   60	405	6	localObject1	Object
    //   64	405	7	localObject2	Object
    //   52	382	8	localZipInputStream1	ZipInputStream
    //   56	382	9	localFileOutputStream1	FileOutputStream
    //   68	395	10	localObject3	Object
    //   72	395	11	localObject4	Object
    //   39	386	12	localZipInputStream2	ZipInputStream
    //   48	379	13	localFileOutputStream2	FileOutputStream
    //   42	82	14	localObject5	Object
    //   45	87	15	localObject6	Object
    //   33	57	16	localObject7	Object
    //   36	62	17	localObject8	Object
    //   79	130	18	arrayOfByte	byte[]
    // Exception table:
    //   from	to	target	type
    //   74	81	432	java/io/IOException
    //   105	111	432	java/io/IOException
    //   135	143	432	java/io/IOException
    //   167	176	432	java/io/IOException
    //   206	216	432	java/io/IOException
    //   240	247	432	java/io/IOException
    //   271	278	432	java/io/IOException
    //   302	307	432	java/io/IOException
    //   331	336	432	java/io/IOException
    //   369	377	432	java/io/IOException
    //   401	423	432	java/io/IOException
    //   74	81	450	finally
    //   105	111	450	finally
    //   135	143	450	finally
    //   167	176	450	finally
    //   206	216	450	finally
    //   240	247	450	finally
    //   271	278	450	finally
    //   302	307	450	finally
    //   331	336	450	finally
    //   369	377	450	finally
    //   401	423	450	finally
    //   441	450	450	finally
    //   470	479	450	finally
    //   74	81	461	java/lang/Exception
    //   105	111	461	java/lang/Exception
    //   135	143	461	java/lang/Exception
    //   167	176	461	java/lang/Exception
    //   206	216	461	java/lang/Exception
    //   240	247	461	java/lang/Exception
    //   271	278	461	java/lang/Exception
    //   302	307	461	java/lang/Exception
    //   331	336	461	java/lang/Exception
    //   369	377	461	java/lang/Exception
    //   401	423	461	java/lang/Exception
  }
  
  public void updateCRC(int paramInt)
  {
    this.crc.update(paramInt);
  }
  
  public void updateCRC(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (paramArrayOfByte != null) {
      this.crc.update(paramArrayOfByte, paramInt1, paramInt2);
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/unzip/UnzipEngine.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */