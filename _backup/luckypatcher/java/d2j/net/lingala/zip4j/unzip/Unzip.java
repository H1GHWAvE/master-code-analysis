package net.lingala.zip4j.unzip;

import java.io.File;
import java.util.ArrayList;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.ZipInputStream;
import net.lingala.zip4j.model.CentralDirectory;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.UnzipParameters;
import net.lingala.zip4j.model.Zip64ExtendedInfo;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.progress.ProgressMonitor;
import net.lingala.zip4j.util.InternalZipConstants;
import net.lingala.zip4j.util.Zip4jUtil;

public class Unzip
{
  private ZipModel zipModel;
  
  public Unzip(ZipModel paramZipModel)
    throws ZipException
  {
    if (paramZipModel == null) {
      throw new ZipException("ZipModel is null");
    }
    this.zipModel = paramZipModel;
  }
  
  private long calculateTotalWork(ArrayList paramArrayList)
    throws ZipException
  {
    if (paramArrayList == null) {
      throw new ZipException("fileHeaders is null, cannot calculate total work");
    }
    long l = 0L;
    int i = 0;
    if (i < paramArrayList.size())
    {
      FileHeader localFileHeader = (FileHeader)paramArrayList.get(i);
      if ((localFileHeader.getZip64ExtendedInfo() != null) && (localFileHeader.getZip64ExtendedInfo().getUnCompressedSize() > 0L)) {}
      for (l += localFileHeader.getZip64ExtendedInfo().getCompressedSize();; l += localFileHeader.getCompressedSize())
      {
        i += 1;
        break;
      }
    }
    return l;
  }
  
  private void checkOutputDirectoryStructure(FileHeader paramFileHeader, String paramString1, String paramString2)
    throws ZipException
  {
    if ((paramFileHeader == null) || (!Zip4jUtil.isStringNotNullAndNotEmpty(paramString1))) {
      throw new ZipException("Cannot check output directory structure...one of the parameters was null");
    }
    paramFileHeader = paramFileHeader.getFileName();
    if (Zip4jUtil.isStringNotNullAndNotEmpty(paramString2)) {
      paramFileHeader = paramString2;
    }
    if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramFileHeader)) {}
    for (;;)
    {
      return;
      paramFileHeader = paramString1 + paramFileHeader;
      try
      {
        paramFileHeader = new File(new File(paramFileHeader).getParent());
        if (paramFileHeader.exists()) {
          continue;
        }
        paramFileHeader.mkdirs();
        return;
      }
      catch (Exception paramFileHeader)
      {
        throw new ZipException(paramFileHeader);
      }
    }
  }
  
  private void initExtractAll(ArrayList paramArrayList, UnzipParameters paramUnzipParameters, ProgressMonitor paramProgressMonitor, String paramString)
    throws ZipException
  {
    int i = 0;
    for (;;)
    {
      if (i < paramArrayList.size())
      {
        initExtractFile((FileHeader)paramArrayList.get(i), paramString, paramUnzipParameters, null, paramProgressMonitor);
        if (paramProgressMonitor.isCancelAllTasks())
        {
          paramProgressMonitor.setResult(3);
          paramProgressMonitor.setState(0);
        }
      }
      else
      {
        return;
      }
      i += 1;
    }
  }
  
  private void initExtractFile(FileHeader paramFileHeader, String paramString1, UnzipParameters paramUnzipParameters, String paramString2, ProgressMonitor paramProgressMonitor)
    throws ZipException
  {
    if (paramFileHeader == null) {
      throw new ZipException("fileHeader is null");
    }
    try
    {
      paramProgressMonitor.setFileName(paramFileHeader.getFileName());
      String str = paramString1;
      if (!paramString1.endsWith(InternalZipConstants.FILE_SEPARATOR)) {
        str = paramString1 + InternalZipConstants.FILE_SEPARATOR;
      }
      boolean bool = paramFileHeader.isDirectory();
      if (bool) {
        try
        {
          paramFileHeader = paramFileHeader.getFileName();
          if (!Zip4jUtil.isStringNotNullAndNotEmpty(paramFileHeader)) {
            return;
          }
          paramFileHeader = new File(str + paramFileHeader);
          if (!paramFileHeader.exists())
          {
            paramFileHeader.mkdirs();
            return;
          }
        }
        catch (Exception paramFileHeader)
        {
          paramProgressMonitor.endProgressMonitorError(paramFileHeader);
          throw new ZipException(paramFileHeader);
        }
      }
      return;
    }
    catch (ZipException paramFileHeader)
    {
      paramProgressMonitor.endProgressMonitorError(paramFileHeader);
      throw paramFileHeader;
      checkOutputDirectoryStructure(paramFileHeader, str, paramString2);
      paramFileHeader = new UnzipEngine(this.zipModel, paramFileHeader);
      try
      {
        paramFileHeader.unzipFile(paramProgressMonitor, str, paramString2, paramUnzipParameters);
        return;
      }
      catch (Exception paramFileHeader)
      {
        paramProgressMonitor.endProgressMonitorError(paramFileHeader);
        throw new ZipException(paramFileHeader);
      }
    }
    catch (Exception paramFileHeader)
    {
      paramProgressMonitor.endProgressMonitorError(paramFileHeader);
      throw new ZipException(paramFileHeader);
    }
  }
  
  public void extractAll(final UnzipParameters paramUnzipParameters, final String paramString, final ProgressMonitor paramProgressMonitor, boolean paramBoolean)
    throws ZipException
  {
    final Object localObject = this.zipModel.getCentralDirectory();
    if ((localObject == null) || (((CentralDirectory)localObject).getFileHeaders() == null)) {
      throw new ZipException("invalid central directory in zipModel");
    }
    localObject = ((CentralDirectory)localObject).getFileHeaders();
    paramProgressMonitor.setCurrentOperation(1);
    paramProgressMonitor.setTotalWork(calculateTotalWork((ArrayList)localObject));
    paramProgressMonitor.setState(1);
    if (paramBoolean)
    {
      new Thread("Zip4j")
      {
        public void run()
        {
          try
          {
            Unzip.this.initExtractAll(localObject, paramUnzipParameters, paramProgressMonitor, paramString);
            paramProgressMonitor.endProgressMonitorSuccess();
            return;
          }
          catch (ZipException localZipException) {}
        }
      }.start();
      return;
    }
    initExtractAll((ArrayList)localObject, paramUnzipParameters, paramProgressMonitor, paramString);
  }
  
  public void extractFile(final FileHeader paramFileHeader, final String paramString1, final UnzipParameters paramUnzipParameters, final String paramString2, final ProgressMonitor paramProgressMonitor, boolean paramBoolean)
    throws ZipException
  {
    if (paramFileHeader == null) {
      throw new ZipException("fileHeader is null");
    }
    paramProgressMonitor.setCurrentOperation(1);
    paramProgressMonitor.setTotalWork(paramFileHeader.getCompressedSize());
    paramProgressMonitor.setState(1);
    paramProgressMonitor.setPercentDone(0);
    paramProgressMonitor.setFileName(paramFileHeader.getFileName());
    if (paramBoolean)
    {
      new Thread("Zip4j")
      {
        public void run()
        {
          try
          {
            Unzip.this.initExtractFile(paramFileHeader, paramString1, paramUnzipParameters, paramString2, paramProgressMonitor);
            paramProgressMonitor.endProgressMonitorSuccess();
            return;
          }
          catch (ZipException localZipException) {}
        }
      }.start();
      return;
    }
    initExtractFile(paramFileHeader, paramString1, paramUnzipParameters, paramString2, paramProgressMonitor);
    paramProgressMonitor.endProgressMonitorSuccess();
  }
  
  public ZipInputStream getInputStream(FileHeader paramFileHeader)
    throws ZipException
  {
    return new UnzipEngine(this.zipModel, paramFileHeader).getInputStream();
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/unzip/Unzip.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */