package net.lingala.zip4j.util;

import java.io.DataInput;
import java.io.IOException;
import net.lingala.zip4j.exception.ZipException;

public class Raw
{
  public static byte bitArrayToByte(int[] paramArrayOfInt)
    throws ZipException
  {
    if (paramArrayOfInt == null) {
      throw new ZipException("bit array is null, cannot calculate byte from bits");
    }
    if (paramArrayOfInt.length != 8) {
      throw new ZipException("invalid bit array length, cannot calculate byte");
    }
    if (!checkBits(paramArrayOfInt)) {
      throw new ZipException("invalid bits provided, bits contain other values than 0 or 1");
    }
    int j = 0;
    int i = 0;
    while (i < paramArrayOfInt.length)
    {
      j = (int)(j + Math.pow(2.0D, i) * paramArrayOfInt[i]);
      i += 1;
    }
    return (byte)j;
  }
  
  private static boolean checkBits(int[] paramArrayOfInt)
  {
    boolean bool2 = true;
    int i = 0;
    for (;;)
    {
      boolean bool1 = bool2;
      if (i < paramArrayOfInt.length)
      {
        if ((paramArrayOfInt[i] != 0) && (paramArrayOfInt[i] != 1)) {
          bool1 = false;
        }
      }
      else {
        return bool1;
      }
      i += 1;
    }
  }
  
  public static byte[] convertCharArrayToByteArray(char[] paramArrayOfChar)
  {
    if (paramArrayOfChar == null) {
      throw new NullPointerException();
    }
    byte[] arrayOfByte = new byte[paramArrayOfChar.length];
    int i = 0;
    while (i < paramArrayOfChar.length)
    {
      arrayOfByte[i] = ((byte)paramArrayOfChar[i]);
      i += 1;
    }
    return arrayOfByte;
  }
  
  public static void prepareBuffAESIVBytes(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    paramArrayOfByte[0] = ((byte)paramInt1);
    paramArrayOfByte[1] = ((byte)(paramInt1 >> 8));
    paramArrayOfByte[2] = ((byte)(paramInt1 >> 16));
    paramArrayOfByte[3] = ((byte)(paramInt1 >> 24));
    paramArrayOfByte[4] = 0;
    paramArrayOfByte[5] = 0;
    paramArrayOfByte[6] = 0;
    paramArrayOfByte[7] = 0;
    paramArrayOfByte[8] = 0;
    paramArrayOfByte[9] = 0;
    paramArrayOfByte[10] = 0;
    paramArrayOfByte[11] = 0;
    paramArrayOfByte[12] = 0;
    paramArrayOfByte[13] = 0;
    paramArrayOfByte[14] = 0;
    paramArrayOfByte[15] = 0;
  }
  
  public static int readIntLittleEndian(byte[] paramArrayOfByte, int paramInt)
  {
    return paramArrayOfByte[paramInt] & 0xFF | (paramArrayOfByte[(paramInt + 1)] & 0xFF) << 8 | (paramArrayOfByte[(paramInt + 2)] & 0xFF | (paramArrayOfByte[(paramInt + 3)] & 0xFF) << 8) << 16;
  }
  
  public static int readLeInt(DataInput paramDataInput, byte[] paramArrayOfByte)
    throws ZipException
  {
    try
    {
      paramDataInput.readFully(paramArrayOfByte, 0, 4);
      return paramArrayOfByte[0] & 0xFF | (paramArrayOfByte[1] & 0xFF) << 8 | (paramArrayOfByte[2] & 0xFF | (paramArrayOfByte[3] & 0xFF) << 8) << 16;
    }
    catch (IOException paramDataInput)
    {
      throw new ZipException(paramDataInput);
    }
  }
  
  public static long readLongLittleEndian(byte[] paramArrayOfByte, int paramInt)
  {
    return (((((((0L | paramArrayOfByte[(paramInt + 7)] & 0xFF) << 8 | paramArrayOfByte[(paramInt + 6)] & 0xFF) << 8 | paramArrayOfByte[(paramInt + 5)] & 0xFF) << 8 | paramArrayOfByte[(paramInt + 4)] & 0xFF) << 8 | paramArrayOfByte[(paramInt + 3)] & 0xFF) << 8 | paramArrayOfByte[(paramInt + 2)] & 0xFF) << 8 | paramArrayOfByte[(paramInt + 1)] & 0xFF) << 8 | paramArrayOfByte[paramInt] & 0xFF;
  }
  
  public static final short readShortBigEndian(byte[] paramArrayOfByte, int paramInt)
  {
    int i = (short)((short)(paramArrayOfByte[paramInt] & 0xFF | 0x0) << 8);
    return (short)(paramArrayOfByte[(paramInt + 1)] & 0xFF | i);
  }
  
  public static int readShortLittleEndian(byte[] paramArrayOfByte, int paramInt)
  {
    return paramArrayOfByte[paramInt] & 0xFF | (paramArrayOfByte[(paramInt + 1)] & 0xFF) << 8;
  }
  
  public static byte[] toByteArray(int paramInt)
  {
    return new byte[] { (byte)paramInt, (byte)(paramInt >> 8), (byte)(paramInt >> 16), (byte)(paramInt >> 24) };
  }
  
  public static byte[] toByteArray(int paramInt1, int paramInt2)
  {
    byte[] arrayOfByte1 = new byte[paramInt2];
    byte[] arrayOfByte2 = toByteArray(paramInt1);
    paramInt1 = 0;
    while ((paramInt1 < arrayOfByte2.length) && (paramInt1 < paramInt2))
    {
      arrayOfByte1[paramInt1] = arrayOfByte2[paramInt1];
      paramInt1 += 1;
    }
    return arrayOfByte1;
  }
  
  public static final void writeIntLittleEndian(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    paramArrayOfByte[(paramInt1 + 3)] = ((byte)(paramInt2 >>> 24));
    paramArrayOfByte[(paramInt1 + 2)] = ((byte)(paramInt2 >>> 16));
    paramArrayOfByte[(paramInt1 + 1)] = ((byte)(paramInt2 >>> 8));
    paramArrayOfByte[paramInt1] = ((byte)(paramInt2 & 0xFF));
  }
  
  public static void writeLongLittleEndian(byte[] paramArrayOfByte, int paramInt, long paramLong)
  {
    paramArrayOfByte[(paramInt + 7)] = ((byte)(int)(paramLong >>> 56));
    paramArrayOfByte[(paramInt + 6)] = ((byte)(int)(paramLong >>> 48));
    paramArrayOfByte[(paramInt + 5)] = ((byte)(int)(paramLong >>> 40));
    paramArrayOfByte[(paramInt + 4)] = ((byte)(int)(paramLong >>> 32));
    paramArrayOfByte[(paramInt + 3)] = ((byte)(int)(paramLong >>> 24));
    paramArrayOfByte[(paramInt + 2)] = ((byte)(int)(paramLong >>> 16));
    paramArrayOfByte[(paramInt + 1)] = ((byte)(int)(paramLong >>> 8));
    paramArrayOfByte[paramInt] = ((byte)(int)(0xFF & paramLong));
  }
  
  public static final void writeShortLittleEndian(byte[] paramArrayOfByte, int paramInt, short paramShort)
  {
    paramArrayOfByte[(paramInt + 1)] = ((byte)(paramShort >>> 8));
    paramArrayOfByte[paramInt] = ((byte)(paramShort & 0xFF));
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/util/Raw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */