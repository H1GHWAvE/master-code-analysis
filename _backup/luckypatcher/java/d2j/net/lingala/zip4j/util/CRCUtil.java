package net.lingala.zip4j.util;

import net.lingala.zip4j.exception.ZipException;

public class CRCUtil
{
  private static final int BUF_SIZE = 16384;
  
  public static long computeFileCRC(String paramString)
    throws ZipException
  {
    return computeFileCRC(paramString, null);
  }
  
  /* Error */
  public static long computeFileCRC(String paramString, net.lingala.zip4j.progress.ProgressMonitor paramProgressMonitor)
    throws ZipException
  {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic 30	net/lingala/zip4j/util/Zip4jUtil:isStringNotNullAndNotEmpty	(Ljava/lang/String;)Z
    //   4: ifne +13 -> 17
    //   7: new 16	net/lingala/zip4j/exception/ZipException
    //   10: dup
    //   11: ldc 32
    //   13: invokespecial 35	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   16: athrow
    //   17: aconst_null
    //   18: astore 10
    //   20: aconst_null
    //   21: astore 9
    //   23: aconst_null
    //   24: astore 8
    //   26: aload 10
    //   28: astore 7
    //   30: aload_0
    //   31: invokestatic 38	net/lingala/zip4j/util/Zip4jUtil:checkFileReadAccess	(Ljava/lang/String;)Z
    //   34: pop
    //   35: aload 10
    //   37: astore 7
    //   39: new 40	java/io/FileInputStream
    //   42: dup
    //   43: new 42	java/io/File
    //   46: dup
    //   47: aload_0
    //   48: invokespecial 43	java/io/File:<init>	(Ljava/lang/String;)V
    //   51: invokespecial 46	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   54: astore_0
    //   55: sipush 16384
    //   58: newarray <illegal type>
    //   60: astore 7
    //   62: new 48	java/util/zip/CRC32
    //   65: dup
    //   66: invokespecial 49	java/util/zip/CRC32:<init>	()V
    //   69: astore 8
    //   71: aload_0
    //   72: aload 7
    //   74: invokevirtual 55	java/io/InputStream:read	([B)I
    //   77: istore_2
    //   78: iload_2
    //   79: iconst_m1
    //   80: if_icmpeq +69 -> 149
    //   83: aload 8
    //   85: aload 7
    //   87: iconst_0
    //   88: iload_2
    //   89: invokevirtual 59	java/util/zip/CRC32:update	([BII)V
    //   92: aload_1
    //   93: ifnull -22 -> 71
    //   96: aload_1
    //   97: iload_2
    //   98: i2l
    //   99: invokevirtual 65	net/lingala/zip4j/progress/ProgressMonitor:updateWorkCompleted	(J)V
    //   102: aload_1
    //   103: invokevirtual 69	net/lingala/zip4j/progress/ProgressMonitor:isCancelAllTasks	()Z
    //   106: ifeq -35 -> 71
    //   109: aload_1
    //   110: iconst_3
    //   111: invokevirtual 73	net/lingala/zip4j/progress/ProgressMonitor:setResult	(I)V
    //   114: aload_1
    //   115: iconst_0
    //   116: invokevirtual 76	net/lingala/zip4j/progress/ProgressMonitor:setState	(I)V
    //   119: lconst_0
    //   120: lstore 5
    //   122: lload 5
    //   124: lstore_3
    //   125: aload_0
    //   126: ifnull +10 -> 136
    //   129: aload_0
    //   130: invokevirtual 79	java/io/InputStream:close	()V
    //   133: lload 5
    //   135: lstore_3
    //   136: lload_3
    //   137: lreturn
    //   138: astore_0
    //   139: new 16	net/lingala/zip4j/exception/ZipException
    //   142: dup
    //   143: ldc 81
    //   145: invokespecial 35	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   148: athrow
    //   149: aload 8
    //   151: invokevirtual 85	java/util/zip/CRC32:getValue	()J
    //   154: lstore 5
    //   156: lload 5
    //   158: lstore_3
    //   159: aload_0
    //   160: ifnull -24 -> 136
    //   163: aload_0
    //   164: invokevirtual 79	java/io/InputStream:close	()V
    //   167: lload 5
    //   169: lreturn
    //   170: astore_0
    //   171: new 16	net/lingala/zip4j/exception/ZipException
    //   174: dup
    //   175: ldc 81
    //   177: invokespecial 35	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   180: athrow
    //   181: astore_0
    //   182: aload 8
    //   184: astore 7
    //   186: new 16	net/lingala/zip4j/exception/ZipException
    //   189: dup
    //   190: aload_0
    //   191: invokespecial 88	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   194: athrow
    //   195: astore_0
    //   196: aload 7
    //   198: ifnull +8 -> 206
    //   201: aload 7
    //   203: invokevirtual 79	java/io/InputStream:close	()V
    //   206: aload_0
    //   207: athrow
    //   208: astore_0
    //   209: aload 9
    //   211: astore 7
    //   213: new 16	net/lingala/zip4j/exception/ZipException
    //   216: dup
    //   217: aload_0
    //   218: invokespecial 88	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   221: athrow
    //   222: astore_0
    //   223: new 16	net/lingala/zip4j/exception/ZipException
    //   226: dup
    //   227: ldc 81
    //   229: invokespecial 35	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   232: athrow
    //   233: astore_1
    //   234: aload_0
    //   235: astore 7
    //   237: aload_1
    //   238: astore_0
    //   239: goto -43 -> 196
    //   242: astore_1
    //   243: aload_0
    //   244: astore 7
    //   246: aload_1
    //   247: astore_0
    //   248: goto -35 -> 213
    //   251: astore_1
    //   252: aload_0
    //   253: astore 7
    //   255: aload_1
    //   256: astore_0
    //   257: goto -71 -> 186
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	260	0	paramString	String
    //   0	260	1	paramProgressMonitor	net.lingala.zip4j.progress.ProgressMonitor
    //   77	21	2	i	int
    //   124	35	3	l1	long
    //   120	48	5	l2	long
    //   28	226	7	localObject1	Object
    //   24	159	8	localCRC32	java.util.zip.CRC32
    //   21	189	9	localObject2	Object
    //   18	18	10	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   129	133	138	java/io/IOException
    //   163	167	170	java/io/IOException
    //   30	35	181	java/io/IOException
    //   39	55	181	java/io/IOException
    //   30	35	195	finally
    //   39	55	195	finally
    //   186	195	195	finally
    //   213	222	195	finally
    //   30	35	208	java/lang/Exception
    //   39	55	208	java/lang/Exception
    //   201	206	222	java/io/IOException
    //   55	71	233	finally
    //   71	78	233	finally
    //   83	92	233	finally
    //   96	119	233	finally
    //   149	156	233	finally
    //   55	71	242	java/lang/Exception
    //   71	78	242	java/lang/Exception
    //   83	92	242	java/lang/Exception
    //   96	119	242	java/lang/Exception
    //   149	156	242	java/lang/Exception
    //   55	71	251	java/io/IOException
    //   71	78	251	java/io/IOException
    //   83	92	251	java/io/IOException
    //   96	119	251	java/io/IOException
    //   149	156	251	java/io/IOException
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/util/CRCUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */