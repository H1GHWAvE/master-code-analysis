package net.lingala.zip4j.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.CentralDirectory;
import net.lingala.zip4j.model.EndCentralDirRecord;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipModel;

public class Zip4jUtil
{
  public static boolean checkArrayListTypes(ArrayList paramArrayList, int paramInt)
    throws ZipException
  {
    if (paramArrayList == null) {
      throw new ZipException("input arraylist is null, cannot check types");
    }
    if (paramArrayList.size() <= 0) {}
    int j;
    do
    {
      return true;
      j = 0;
      switch (paramInt)
      {
      default: 
        paramInt = j;
      }
    } while (paramInt == 0);
    return false;
    int i = 0;
    for (;;)
    {
      paramInt = j;
      if (i >= paramArrayList.size()) {
        break;
      }
      if (!(paramArrayList.get(i) instanceof File))
      {
        paramInt = 1;
        break;
      }
      i += 1;
    }
    i = 0;
    for (;;)
    {
      paramInt = j;
      if (i >= paramArrayList.size()) {
        break;
      }
      if (!(paramArrayList.get(i) instanceof String))
      {
        paramInt = 1;
        break;
      }
      i += 1;
    }
  }
  
  public static boolean checkFileExists(File paramFile)
    throws ZipException
  {
    if (paramFile == null) {
      throw new ZipException("cannot check if file exists: input file is null");
    }
    return paramFile.exists();
  }
  
  public static boolean checkFileExists(String paramString)
    throws ZipException
  {
    if (!isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("path is null");
    }
    return checkFileExists(new File(paramString));
  }
  
  public static boolean checkFileReadAccess(String paramString)
    throws ZipException
  {
    if (!isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("path is null");
    }
    if (!checkFileExists(paramString)) {
      throw new ZipException("file does not exist: " + paramString);
    }
    try
    {
      boolean bool = new File(paramString).canRead();
      return bool;
    }
    catch (Exception paramString)
    {
      throw new ZipException("cannot read zip file");
    }
  }
  
  public static boolean checkFileWriteAccess(String paramString)
    throws ZipException
  {
    if (!isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("path is null");
    }
    if (!checkFileExists(paramString)) {
      throw new ZipException("file does not exist: " + paramString);
    }
    try
    {
      boolean bool = new File(paramString).canWrite();
      return bool;
    }
    catch (Exception paramString)
    {
      throw new ZipException("cannot read zip file");
    }
  }
  
  public static boolean checkOutputFolder(String paramString)
    throws ZipException
  {
    if (!isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException(new NullPointerException("output path is null"));
    }
    paramString = new File(paramString);
    if (paramString.exists())
    {
      if (!paramString.isDirectory()) {
        throw new ZipException("output folder is not valid");
      }
      if (!paramString.canWrite()) {
        throw new ZipException("no write access to output folder");
      }
    }
    else
    {
      try
      {
        paramString.mkdirs();
        if (!paramString.isDirectory()) {
          throw new ZipException("output folder is not valid");
        }
      }
      catch (Exception paramString)
      {
        throw new ZipException("Cannot create destination folder");
      }
      if (!paramString.canWrite()) {
        throw new ZipException("no write access to destination folder");
      }
    }
    return true;
  }
  
  public static byte[] convertCharset(String paramString)
    throws ZipException
  {
    try
    {
      Object localObject = detectCharSet(paramString);
      if (((String)localObject).equals("Cp850")) {
        return paramString.getBytes("Cp850");
      }
      if (((String)localObject).equals("UTF8")) {
        return paramString.getBytes("UTF8");
      }
      localObject = paramString.getBytes();
      return (byte[])localObject;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      return paramString.getBytes();
    }
    catch (Exception paramString)
    {
      throw new ZipException(paramString);
    }
  }
  
  public static String decodeFileName(byte[] paramArrayOfByte, boolean paramBoolean)
  {
    if (paramBoolean) {
      try
      {
        String str = new String(paramArrayOfByte, "UTF8");
        return str;
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        return new String(paramArrayOfByte);
      }
    }
    return getCp850EncodedString(paramArrayOfByte);
  }
  
  public static String detectCharSet(String paramString)
    throws ZipException
  {
    if (paramString == null) {
      throw new ZipException("input string is null, cannot detect charset");
    }
    try
    {
      if (paramString.equals(new String(paramString.getBytes("Cp850"), "Cp850"))) {
        return "Cp850";
      }
      if (paramString.equals(new String(paramString.getBytes("UTF8"), "UTF8"))) {
        return "UTF8";
      }
      paramString = InternalZipConstants.CHARSET_DEFAULT;
      return paramString;
    }
    catch (UnsupportedEncodingException paramString)
    {
      return InternalZipConstants.CHARSET_DEFAULT;
    }
    catch (Exception paramString) {}
    return InternalZipConstants.CHARSET_DEFAULT;
  }
  
  public static long dosToJavaTme(int paramInt)
  {
    Calendar localCalendar = Calendar.getInstance();
    localCalendar.set((paramInt >> 25 & 0x7F) + 1980, (paramInt >> 21 & 0xF) - 1, paramInt >> 16 & 0x1F, paramInt >> 11 & 0x1F, paramInt >> 5 & 0x3F, (paramInt & 0x1F) * 2);
    localCalendar.set(14, 0);
    return localCalendar.getTime().getTime();
  }
  
  public static String getAbsoluteFilePath(String paramString)
    throws ZipException
  {
    if (!isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("filePath is null or empty, cannot get absolute file path");
    }
    return new File(paramString).getAbsolutePath();
  }
  
  public static long[] getAllHeaderSignatures()
  {
    return new long[] { 67324752L, 134695760L, 33639248L, 101010256L, 84233040L, 134630224L, 134695760L, 117853008L, 101075792L, 1L, 39169L };
  }
  
  public static String getCp850EncodedString(byte[] paramArrayOfByte)
  {
    try
    {
      String str = new String(paramArrayOfByte, "Cp850");
      return str;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException) {}
    return new String(paramArrayOfByte);
  }
  
  public static int getEncodedStringLength(String paramString)
    throws ZipException
  {
    if (!isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("input string is null, cannot calculate encoded String length");
    }
    return getEncodedStringLength(paramString, detectCharSet(paramString));
  }
  
  public static int getEncodedStringLength(String paramString1, String paramString2)
    throws ZipException
  {
    if (!isStringNotNullAndNotEmpty(paramString1)) {
      throw new ZipException("input string is null, cannot calculate encoded String length");
    }
    if (!isStringNotNullAndNotEmpty(paramString2)) {
      throw new ZipException("encoding is not defined, cannot calculate string length");
    }
    for (;;)
    {
      try
      {
        if (!paramString2.equals("Cp850")) {
          continue;
        }
        paramString2 = ByteBuffer.wrap(paramString1.getBytes("Cp850"));
        paramString1 = paramString2;
      }
      catch (UnsupportedEncodingException paramString2)
      {
        paramString1 = ByteBuffer.wrap(paramString1.getBytes());
        continue;
      }
      catch (Exception paramString1)
      {
        throw new ZipException(paramString1);
      }
      return paramString1.limit();
      if (paramString2.equals("UTF8"))
      {
        paramString2 = ByteBuffer.wrap(paramString1.getBytes("UTF8"));
        paramString1 = paramString2;
      }
      else
      {
        paramString2 = ByteBuffer.wrap(paramString1.getBytes(paramString2));
        paramString1 = paramString2;
      }
    }
  }
  
  public static FileHeader getFileHeader(ZipModel paramZipModel, String paramString)
    throws ZipException
  {
    if (paramZipModel == null) {
      throw new ZipException("zip model is null, cannot determine file header for fileName: " + paramString);
    }
    if (!isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("file name is null, cannot determine file header for fileName: " + paramString);
    }
    Object localObject2 = getFileHeaderWithExactMatch(paramZipModel, paramString);
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      localObject2 = paramString.replaceAll("\\\\", "/");
      paramString = getFileHeaderWithExactMatch(paramZipModel, (String)localObject2);
      localObject1 = paramString;
      if (paramString == null) {
        localObject1 = getFileHeaderWithExactMatch(paramZipModel, ((String)localObject2).replaceAll("/", "\\\\"));
      }
    }
    return (FileHeader)localObject1;
  }
  
  public static FileHeader getFileHeaderWithExactMatch(ZipModel paramZipModel, String paramString)
    throws ZipException
  {
    if (paramZipModel == null) {
      throw new ZipException("zip model is null, cannot determine file header with exact match for fileName: " + paramString);
    }
    if (!isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("file name is null, cannot determine file header with exact match for fileName: " + paramString);
    }
    if (paramZipModel.getCentralDirectory() == null) {
      throw new ZipException("central directory is null, cannot determine file header with exact match for fileName: " + paramString);
    }
    if (paramZipModel.getCentralDirectory().getFileHeaders() == null) {
      throw new ZipException("file Headers are null, cannot determine file header with exact match for fileName: " + paramString);
    }
    if (paramZipModel.getCentralDirectory().getFileHeaders().size() <= 0) {
      return null;
    }
    paramZipModel = paramZipModel.getCentralDirectory().getFileHeaders();
    int i = 0;
    if (i < paramZipModel.size())
    {
      FileHeader localFileHeader = (FileHeader)paramZipModel.get(i);
      String str = localFileHeader.getFileName();
      if (!isStringNotNullAndNotEmpty(str)) {}
      while (!paramString.equalsIgnoreCase(str))
      {
        i += 1;
        break;
      }
      return localFileHeader;
    }
    return null;
  }
  
  public static long getFileLengh(File paramFile)
    throws ZipException
  {
    if (paramFile == null) {
      throw new ZipException("input file is null, cannot calculate file length");
    }
    if (paramFile.isDirectory()) {
      return -1L;
    }
    return paramFile.length();
  }
  
  public static long getFileLengh(String paramString)
    throws ZipException
  {
    if (!isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("invalid file name");
    }
    return getFileLengh(new File(paramString));
  }
  
  public static String getFileNameFromFilePath(File paramFile)
    throws ZipException
  {
    if (paramFile == null) {
      throw new ZipException("input file is null, cannot get file name");
    }
    if (paramFile.isDirectory()) {
      return null;
    }
    return paramFile.getName();
  }
  
  public static ArrayList getFilesInDirectoryRec(File paramFile, boolean paramBoolean)
    throws ZipException
  {
    if (paramFile == null) {
      throw new ZipException("input path is null, cannot read files in the directory");
    }
    ArrayList localArrayList = new ArrayList();
    List localList = Arrays.asList(paramFile.listFiles());
    if (!paramFile.canRead()) {}
    label105:
    for (;;)
    {
      return localArrayList;
      int i = 0;
      for (;;)
      {
        if (i >= localList.size()) {
          break label105;
        }
        paramFile = (File)localList.get(i);
        if ((paramFile.isHidden()) && (!paramBoolean)) {
          break;
        }
        localArrayList.add(paramFile);
        if (paramFile.isDirectory()) {
          localArrayList.addAll(getFilesInDirectoryRec(paramFile, paramBoolean));
        }
        i += 1;
      }
    }
  }
  
  public static int getIndexOfFileHeader(ZipModel paramZipModel, FileHeader paramFileHeader)
    throws ZipException
  {
    if ((paramZipModel == null) || (paramFileHeader == null)) {
      throw new ZipException("input parameters is null, cannot determine index of file header");
    }
    if (paramZipModel.getCentralDirectory() == null) {
      throw new ZipException("central directory is null, ccannot determine index of file header");
    }
    if (paramZipModel.getCentralDirectory().getFileHeaders() == null) {
      throw new ZipException("file Headers are null, cannot determine index of file header");
    }
    if (paramZipModel.getCentralDirectory().getFileHeaders().size() <= 0) {
      return -1;
    }
    paramFileHeader = paramFileHeader.getFileName();
    if (!isStringNotNullAndNotEmpty(paramFileHeader)) {
      throw new ZipException("file name in file header is empty or null, cannot determine index of file header");
    }
    paramZipModel = paramZipModel.getCentralDirectory().getFileHeaders();
    int i = 0;
    if (i < paramZipModel.size())
    {
      String str = ((FileHeader)paramZipModel.get(i)).getFileName();
      if (!isStringNotNullAndNotEmpty(str)) {}
      while (!paramFileHeader.equalsIgnoreCase(str))
      {
        i += 1;
        break;
      }
      return i;
    }
    return -1;
  }
  
  public static long getLastModifiedFileTime(File paramFile, TimeZone paramTimeZone)
    throws ZipException
  {
    if (paramFile == null) {
      throw new ZipException("input file is null, cannot read last modified file time");
    }
    if (!paramFile.exists()) {
      throw new ZipException("input file does not exist, cannot read last modified file time");
    }
    return paramFile.lastModified();
  }
  
  public static String getRelativeFileName(String paramString1, String paramString2, String paramString3)
    throws ZipException
  {
    if (!isStringNotNullAndNotEmpty(paramString1)) {
      throw new ZipException("input file path/name is empty, cannot calculate relative file name");
    }
    if (isStringNotNullAndNotEmpty(paramString3))
    {
      String str = new File(paramString3).getPath();
      paramString3 = str;
      if (!str.endsWith(InternalZipConstants.FILE_SEPARATOR)) {
        paramString3 = str + InternalZipConstants.FILE_SEPARATOR;
      }
      str = paramString1.substring(paramString3.length());
      paramString3 = str;
      if (str.startsWith(System.getProperty("file.separator"))) {
        paramString3 = str.substring(1);
      }
      paramString1 = new File(paramString1);
      if (paramString1.isDirectory())
      {
        paramString1 = paramString3.replaceAll("\\\\", "/");
        paramString1 = paramString1 + "/";
      }
    }
    for (;;)
    {
      paramString3 = paramString1;
      if (isStringNotNullAndNotEmpty(paramString2)) {
        paramString3 = paramString2 + paramString1;
      }
      if (isStringNotNullAndNotEmpty(paramString3)) {
        break;
      }
      throw new ZipException("Error determining file name");
      paramString3 = paramString3.substring(0, paramString3.lastIndexOf(paramString1.getName())).replaceAll("\\\\", "/");
      paramString1 = paramString3 + paramString1.getName();
      continue;
      paramString3 = new File(paramString1);
      if (paramString3.isDirectory()) {
        paramString1 = paramString3.getName() + "/";
      } else {
        paramString1 = getFileNameFromFilePath(new File(paramString1));
      }
    }
    return paramString3;
  }
  
  public static ArrayList getSplitZipFiles(ZipModel paramZipModel)
    throws ZipException
  {
    if (paramZipModel == null) {
      throw new ZipException("cannot get split zip files: zipmodel is null");
    }
    if (paramZipModel.getEndCentralDirRecord() == null)
    {
      localObject = null;
      return (ArrayList)localObject;
    }
    ArrayList localArrayList = new ArrayList();
    String str2 = paramZipModel.getZipFile();
    String str3 = new File(str2).getName();
    if (!isStringNotNullAndNotEmpty(str2)) {
      throw new ZipException("cannot get split zip files: zipfile is null");
    }
    if (!paramZipModel.isSplitArchive())
    {
      localArrayList.add(str2);
      return localArrayList;
    }
    int j = paramZipModel.getEndCentralDirRecord().getNoOfThisDisk();
    if (j == 0)
    {
      localArrayList.add(str2);
      return localArrayList;
    }
    int i = 0;
    for (;;)
    {
      localObject = localArrayList;
      if (i > j) {
        break;
      }
      if (i != j) {
        break label147;
      }
      localArrayList.add(paramZipModel.getZipFile());
      i += 1;
    }
    label147:
    Object localObject = ".z0";
    if (i > 9) {
      localObject = ".z";
    }
    if (str3.indexOf(".") >= 0) {}
    for (String str1 = str2.substring(0, str2.lastIndexOf("."));; str1 = str2)
    {
      localArrayList.add(str1 + (String)localObject + (i + 1));
      break;
    }
  }
  
  public static String getZipFileNameWithoutExt(String paramString)
    throws ZipException
  {
    if (!isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("zip file name is empty or null, cannot determine zip file name");
    }
    String str = paramString;
    if (paramString.indexOf(System.getProperty("file.separator")) >= 0) {
      str = paramString.substring(paramString.lastIndexOf(System.getProperty("file.separator")));
    }
    paramString = str;
    if (str.indexOf(".") > 0) {
      paramString = str.substring(0, str.lastIndexOf("."));
    }
    return paramString;
  }
  
  public static boolean isStringNotNullAndNotEmpty(String paramString)
  {
    return (paramString != null) && (paramString.trim().length() > 0);
  }
  
  public static boolean isSupportedCharset(String paramString)
    throws ZipException
  {
    if (!isStringNotNullAndNotEmpty(paramString)) {
      throw new ZipException("charset is null or empty, cannot check if it is supported");
    }
    try
    {
      new String("a".getBytes(), paramString);
      return true;
    }
    catch (UnsupportedEncodingException paramString)
    {
      return false;
    }
    catch (Exception paramString)
    {
      throw new ZipException(paramString);
    }
  }
  
  public static boolean isWindows()
  {
    return System.getProperty("os.name").toLowerCase().indexOf("win") >= 0;
  }
  
  public static long javaToDosTime(long paramLong)
  {
    Calendar localCalendar = Calendar.getInstance();
    localCalendar.setTimeInMillis(paramLong);
    int i = localCalendar.get(1);
    if (i < 1980) {
      return 2162688L;
    }
    return i - 1980 << 25 | localCalendar.get(2) + 1 << 21 | localCalendar.get(5) << 16 | localCalendar.get(11) << 11 | localCalendar.get(12) << 5 | localCalendar.get(13) >> 1;
  }
  
  public static void setFileArchive(File paramFile)
    throws ZipException
  {}
  
  public static void setFileHidden(File paramFile)
    throws ZipException
  {}
  
  public static void setFileReadOnly(File paramFile)
    throws ZipException
  {
    if (paramFile == null) {
      throw new ZipException("input file is null. cannot set read only file attribute");
    }
    if (paramFile.exists()) {
      paramFile.setReadOnly();
    }
  }
  
  public static void setFileSystemMode(File paramFile)
    throws ZipException
  {}
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/util/Zip4jUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */