package net.lingala.zip4j.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.CentralDirectory;
import net.lingala.zip4j.model.EndCentralDirRecord;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.Zip64EndCentralDirLocator;
import net.lingala.zip4j.model.Zip64EndCentralDirRecord;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.progress.ProgressMonitor;

public class ArchiveMaintainer
{
  private long calculateTotalWorkForMergeOp(ZipModel paramZipModel)
    throws ZipException
  {
    long l1 = 0L;
    long l2 = l1;
    if (paramZipModel.isSplitArchive())
    {
      int j = paramZipModel.getEndCentralDirRecord().getNoOfThisDisk();
      String str2 = paramZipModel.getZipFile();
      int i = 0;
      l2 = l1;
      if (i <= j)
      {
        String str1;
        if (paramZipModel.getEndCentralDirRecord().getNoOfThisDisk() == 0) {
          str1 = paramZipModel.getZipFile();
        }
        for (;;)
        {
          l1 += Zip4jUtil.getFileLengh(new File(str1));
          i += 1;
          break;
          if (9 >= 0) {
            str1 = str2.substring(0, str2.lastIndexOf(".")) + ".z" + 1;
          } else {
            str1 = str2.substring(0, str2.lastIndexOf(".")) + ".z0" + 1;
          }
        }
      }
    }
    return l2;
  }
  
  private long calculateTotalWorkForRemoveOp(ZipModel paramZipModel, FileHeader paramFileHeader)
    throws ZipException
  {
    return Zip4jUtil.getFileLengh(new File(paramZipModel.getZipFile())) - paramFileHeader.getCompressedSize();
  }
  
  private void copyFile(RandomAccessFile paramRandomAccessFile, OutputStream paramOutputStream, long paramLong1, long paramLong2, ProgressMonitor paramProgressMonitor)
    throws ZipException
  {
    if ((paramRandomAccessFile == null) || (paramOutputStream == null)) {
      throw new ZipException("input or output stream is null, cannot copy file");
    }
    if (paramLong1 < 0L) {
      throw new ZipException("starting offset is negative, cannot copy file");
    }
    if (paramLong2 < 0L) {
      throw new ZipException("end offset is negative, cannot copy file");
    }
    if (paramLong1 > paramLong2) {
      throw new ZipException("start offset is greater than end offset, cannot copy file");
    }
    if (paramLong1 == paramLong2) {}
    for (;;)
    {
      return;
      if (paramProgressMonitor.isCancelAllTasks())
      {
        paramProgressMonitor.setResult(3);
        paramProgressMonitor.setState(0);
        return;
      }
      try
      {
        paramRandomAccessFile.seek(paramLong1);
        l1 = 0L;
        l2 = paramLong2 - paramLong1;
        if (paramLong2 - paramLong1 < 4096L)
        {
          arrayOfByte = new byte[(int)(paramLong2 - paramLong1)];
          paramLong1 = l1;
          i = paramRandomAccessFile.read(arrayOfByte);
          if (i == -1) {
            continue;
          }
          paramOutputStream.write(arrayOfByte, 0, i);
          paramProgressMonitor.updateWorkCompleted(i);
          if (!paramProgressMonitor.isCancelAllTasks()) {
            break label203;
          }
          paramProgressMonitor.setResult(3);
          return;
        }
      }
      catch (IOException paramRandomAccessFile)
      {
        for (;;)
        {
          long l1;
          long l2;
          int i;
          throw new ZipException(paramRandomAccessFile);
          byte[] arrayOfByte = new byte['က'];
          paramLong1 = l1;
          continue;
          paramLong2 = paramLong1 + i;
          if (paramLong2 == l2) {
            break;
          }
          paramLong1 = paramLong2;
          if (arrayOfByte.length + paramLong2 > l2)
          {
            arrayOfByte = new byte[(int)(l2 - paramLong2)];
            paramLong1 = paramLong2;
          }
        }
      }
      catch (Exception paramRandomAccessFile)
      {
        label203:
        throw new ZipException(paramRandomAccessFile);
      }
    }
  }
  
  private RandomAccessFile createFileHandler(ZipModel paramZipModel, String paramString)
    throws ZipException
  {
    if ((paramZipModel == null) || (!Zip4jUtil.isStringNotNullAndNotEmpty(paramZipModel.getZipFile()))) {
      throw new ZipException("input parameter is null in getFilePointer, cannot create file handler to remove file");
    }
    try
    {
      paramZipModel = new RandomAccessFile(new File(paramZipModel.getZipFile()), paramString);
      return paramZipModel;
    }
    catch (FileNotFoundException paramZipModel)
    {
      throw new ZipException(paramZipModel);
    }
  }
  
  private RandomAccessFile createSplitZipFileHandler(ZipModel paramZipModel, int paramInt)
    throws ZipException
  {
    if (paramZipModel == null) {
      throw new ZipException("zip model is null, cannot create split file handler");
    }
    if (paramInt < 0) {
      throw new ZipException("invlaid part number, cannot create split file handler");
    }
    try
    {
      localObject = paramZipModel.getZipFile();
      if (paramInt == paramZipModel.getEndCentralDirRecord().getNoOfThisDisk())
      {
        paramZipModel = paramZipModel.getZipFile();
        localObject = new File(paramZipModel);
        if (Zip4jUtil.checkFileExists((File)localObject)) {
          break label186;
        }
        throw new ZipException("split file does not exist: " + paramZipModel);
      }
    }
    catch (FileNotFoundException paramZipModel)
    {
      Object localObject;
      for (;;)
      {
        throw new ZipException(paramZipModel);
        if (paramInt >= 9) {
          paramZipModel = ((String)localObject).substring(0, ((String)localObject).lastIndexOf(".")) + ".z" + (paramInt + 1);
        } else {
          paramZipModel = ((String)localObject).substring(0, ((String)localObject).lastIndexOf(".")) + ".z0" + (paramInt + 1);
        }
      }
      paramZipModel = new RandomAccessFile((File)localObject, "r");
      return paramZipModel;
    }
    catch (Exception paramZipModel)
    {
      label186:
      throw new ZipException(paramZipModel);
    }
  }
  
  /* Error */
  private void initMergeSplitZipFile(ZipModel paramZipModel, File paramFile, ProgressMonitor paramProgressMonitor)
    throws ZipException
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull +20 -> 21
    //   4: new 17	net/lingala/zip4j/exception/ZipException
    //   7: dup
    //   8: ldc -84
    //   10: invokespecial 101	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   13: astore_1
    //   14: aload_3
    //   15: aload_1
    //   16: invokevirtual 175	net/lingala/zip4j/progress/ProgressMonitor:endProgressMonitorError	(Ljava/lang/Throwable;)V
    //   19: aload_1
    //   20: athrow
    //   21: aload_1
    //   22: invokevirtual 30	net/lingala/zip4j/model/ZipModel:isSplitArchive	()Z
    //   25: ifne +20 -> 45
    //   28: new 17	net/lingala/zip4j/exception/ZipException
    //   31: dup
    //   32: ldc -79
    //   34: invokespecial 101	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   37: astore_1
    //   38: aload_3
    //   39: aload_1
    //   40: invokevirtual 175	net/lingala/zip4j/progress/ProgressMonitor:endProgressMonitorError	(Ljava/lang/Throwable;)V
    //   43: aload_1
    //   44: athrow
    //   45: aconst_null
    //   46: astore 21
    //   48: aconst_null
    //   49: astore 22
    //   51: aconst_null
    //   52: astore 18
    //   54: aconst_null
    //   55: astore 23
    //   57: aconst_null
    //   58: astore 19
    //   60: aconst_null
    //   61: astore 24
    //   63: aconst_null
    //   64: astore 20
    //   66: new 179	java/util/ArrayList
    //   69: dup
    //   70: invokespecial 180	java/util/ArrayList:<init>	()V
    //   73: astore 25
    //   75: lconst_0
    //   76: lstore 8
    //   78: iconst_0
    //   79: istore 10
    //   81: aload 20
    //   83: astore 14
    //   85: aload 18
    //   87: astore 16
    //   89: aload 23
    //   91: astore 12
    //   93: aload 21
    //   95: astore 13
    //   97: aload 24
    //   99: astore 15
    //   101: aload 22
    //   103: astore 17
    //   105: aload_1
    //   106: invokevirtual 34	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   109: invokevirtual 40	net/lingala/zip4j/model/EndCentralDirRecord:getNoOfThisDisk	()I
    //   112: istore 7
    //   114: iload 7
    //   116: ifgt +91 -> 207
    //   119: aload 20
    //   121: astore 14
    //   123: aload 18
    //   125: astore 16
    //   127: aload 23
    //   129: astore 12
    //   131: aload 21
    //   133: astore 13
    //   135: aload 24
    //   137: astore 15
    //   139: aload 22
    //   141: astore 17
    //   143: new 17	net/lingala/zip4j/exception/ZipException
    //   146: dup
    //   147: ldc -74
    //   149: invokespecial 101	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   152: athrow
    //   153: astore_1
    //   154: aload 14
    //   156: astore 12
    //   158: aload 16
    //   160: astore 13
    //   162: aload_3
    //   163: aload_1
    //   164: invokevirtual 175	net/lingala/zip4j/progress/ProgressMonitor:endProgressMonitorError	(Ljava/lang/Throwable;)V
    //   167: aload 14
    //   169: astore 12
    //   171: aload 16
    //   173: astore 13
    //   175: new 17	net/lingala/zip4j/exception/ZipException
    //   178: dup
    //   179: aload_1
    //   180: invokespecial 143	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   183: athrow
    //   184: astore_1
    //   185: aload 13
    //   187: ifnull +8 -> 195
    //   190: aload 13
    //   192: invokevirtual 185	java/io/OutputStream:close	()V
    //   195: aload 12
    //   197: ifnull +8 -> 205
    //   200: aload 12
    //   202: invokevirtual 186	java/io/RandomAccessFile:close	()V
    //   205: aload_1
    //   206: athrow
    //   207: aload 20
    //   209: astore 14
    //   211: aload 18
    //   213: astore 16
    //   215: aload 23
    //   217: astore 12
    //   219: aload 21
    //   221: astore 13
    //   223: aload 24
    //   225: astore 15
    //   227: aload 22
    //   229: astore 17
    //   231: aload_0
    //   232: aload_2
    //   233: invokespecial 190	net/lingala/zip4j/util/ArchiveMaintainer:prepareOutputStreamForMerge	(Ljava/io/File;)Ljava/io/OutputStream;
    //   236: astore 18
    //   238: iconst_0
    //   239: istore 4
    //   241: aload 19
    //   243: astore_2
    //   244: iload 4
    //   246: iload 7
    //   248: if_icmpgt +603 -> 851
    //   251: aload_2
    //   252: astore 14
    //   254: aload 18
    //   256: astore 16
    //   258: aload_2
    //   259: astore 12
    //   261: aload 18
    //   263: astore 13
    //   265: aload_2
    //   266: astore 15
    //   268: aload 18
    //   270: astore 17
    //   272: aload_0
    //   273: aload_1
    //   274: iload 4
    //   276: invokespecial 192	net/lingala/zip4j/util/ArchiveMaintainer:createSplitZipFileHandler	(Lnet/lingala/zip4j/model/ZipModel;I)Ljava/io/RandomAccessFile;
    //   279: astore_2
    //   280: iconst_0
    //   281: istore 6
    //   283: aload_2
    //   284: astore 14
    //   286: aload 18
    //   288: astore 16
    //   290: aload_2
    //   291: astore 12
    //   293: aload 18
    //   295: astore 13
    //   297: aload_2
    //   298: astore 15
    //   300: aload 18
    //   302: astore 17
    //   304: new 194	java/lang/Long
    //   307: dup
    //   308: aload_2
    //   309: invokevirtual 197	java/io/RandomAccessFile:length	()J
    //   312: invokespecial 199	java/lang/Long:<init>	(J)V
    //   315: astore 19
    //   317: iload 10
    //   319: istore 11
    //   321: iload 6
    //   323: istore 5
    //   325: iload 4
    //   327: ifne +249 -> 576
    //   330: aload_2
    //   331: astore 14
    //   333: aload 18
    //   335: astore 16
    //   337: aload_2
    //   338: astore 12
    //   340: aload 18
    //   342: astore 13
    //   344: iload 10
    //   346: istore 11
    //   348: iload 6
    //   350: istore 5
    //   352: aload_2
    //   353: astore 15
    //   355: aload 18
    //   357: astore 17
    //   359: aload_1
    //   360: invokevirtual 203	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   363: ifnull +213 -> 576
    //   366: aload_2
    //   367: astore 14
    //   369: aload 18
    //   371: astore 16
    //   373: aload_2
    //   374: astore 12
    //   376: aload 18
    //   378: astore 13
    //   380: iload 10
    //   382: istore 11
    //   384: iload 6
    //   386: istore 5
    //   388: aload_2
    //   389: astore 15
    //   391: aload 18
    //   393: astore 17
    //   395: aload_1
    //   396: invokevirtual 203	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   399: invokevirtual 209	net/lingala/zip4j/model/CentralDirectory:getFileHeaders	()Ljava/util/ArrayList;
    //   402: ifnull +174 -> 576
    //   405: aload_2
    //   406: astore 14
    //   408: aload 18
    //   410: astore 16
    //   412: aload_2
    //   413: astore 12
    //   415: aload 18
    //   417: astore 13
    //   419: iload 10
    //   421: istore 11
    //   423: iload 6
    //   425: istore 5
    //   427: aload_2
    //   428: astore 15
    //   430: aload 18
    //   432: astore 17
    //   434: aload_1
    //   435: invokevirtual 203	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   438: invokevirtual 209	net/lingala/zip4j/model/CentralDirectory:getFileHeaders	()Ljava/util/ArrayList;
    //   441: invokevirtual 212	java/util/ArrayList:size	()I
    //   444: ifle +132 -> 576
    //   447: aload_2
    //   448: astore 14
    //   450: aload 18
    //   452: astore 16
    //   454: aload_2
    //   455: astore 12
    //   457: aload 18
    //   459: astore 13
    //   461: aload_2
    //   462: astore 15
    //   464: aload 18
    //   466: astore 17
    //   468: iconst_4
    //   469: newarray <illegal type>
    //   471: astore 20
    //   473: aload_2
    //   474: astore 14
    //   476: aload 18
    //   478: astore 16
    //   480: aload_2
    //   481: astore 12
    //   483: aload 18
    //   485: astore 13
    //   487: aload_2
    //   488: astore 15
    //   490: aload 18
    //   492: astore 17
    //   494: aload_2
    //   495: lconst_0
    //   496: invokevirtual 125	java/io/RandomAccessFile:seek	(J)V
    //   499: aload_2
    //   500: astore 14
    //   502: aload 18
    //   504: astore 16
    //   506: aload_2
    //   507: astore 12
    //   509: aload 18
    //   511: astore 13
    //   513: aload_2
    //   514: astore 15
    //   516: aload 18
    //   518: astore 17
    //   520: aload_2
    //   521: aload 20
    //   523: invokevirtual 131	java/io/RandomAccessFile:read	([B)I
    //   526: pop
    //   527: aload_2
    //   528: astore 14
    //   530: aload 18
    //   532: astore 16
    //   534: aload_2
    //   535: astore 12
    //   537: aload 18
    //   539: astore 13
    //   541: iload 10
    //   543: istore 11
    //   545: iload 6
    //   547: istore 5
    //   549: aload_2
    //   550: astore 15
    //   552: aload 18
    //   554: astore 17
    //   556: aload 20
    //   558: iconst_0
    //   559: invokestatic 218	net/lingala/zip4j/util/Raw:readIntLittleEndian	([BI)I
    //   562: i2l
    //   563: ldc2_w 219
    //   566: lcmp
    //   567: ifne +9 -> 576
    //   570: iconst_4
    //   571: istore 5
    //   573: iconst_1
    //   574: istore 11
    //   576: iload 4
    //   578: iload 7
    //   580: if_icmpne +40 -> 620
    //   583: aload_2
    //   584: astore 14
    //   586: aload 18
    //   588: astore 16
    //   590: aload_2
    //   591: astore 12
    //   593: aload 18
    //   595: astore 13
    //   597: aload_2
    //   598: astore 15
    //   600: aload 18
    //   602: astore 17
    //   604: new 194	java/lang/Long
    //   607: dup
    //   608: aload_1
    //   609: invokevirtual 34	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   612: invokevirtual 223	net/lingala/zip4j/model/EndCentralDirRecord:getOffsetOfStartOfCentralDir	()J
    //   615: invokespecial 199	java/lang/Long:<init>	(J)V
    //   618: astore 19
    //   620: aload_2
    //   621: astore 14
    //   623: aload 18
    //   625: astore 16
    //   627: aload_2
    //   628: astore 12
    //   630: aload 18
    //   632: astore 13
    //   634: aload_2
    //   635: astore 15
    //   637: aload 18
    //   639: astore 17
    //   641: aload_0
    //   642: aload_2
    //   643: aload 18
    //   645: iload 5
    //   647: i2l
    //   648: aload 19
    //   650: invokevirtual 226	java/lang/Long:longValue	()J
    //   653: aload_3
    //   654: invokespecial 228	net/lingala/zip4j/util/ArchiveMaintainer:copyFile	(Ljava/io/RandomAccessFile;Ljava/io/OutputStream;JJLnet/lingala/zip4j/progress/ProgressMonitor;)V
    //   657: aload_2
    //   658: astore 14
    //   660: aload 18
    //   662: astore 16
    //   664: aload_2
    //   665: astore 12
    //   667: aload 18
    //   669: astore 13
    //   671: aload_2
    //   672: astore 15
    //   674: aload 18
    //   676: astore 17
    //   678: lload 8
    //   680: aload 19
    //   682: invokevirtual 226	java/lang/Long:longValue	()J
    //   685: iload 5
    //   687: i2l
    //   688: lsub
    //   689: ladd
    //   690: lstore 8
    //   692: aload_2
    //   693: astore 14
    //   695: aload 18
    //   697: astore 16
    //   699: aload_2
    //   700: astore 12
    //   702: aload 18
    //   704: astore 13
    //   706: aload_2
    //   707: astore 15
    //   709: aload 18
    //   711: astore 17
    //   713: aload_3
    //   714: invokevirtual 112	net/lingala/zip4j/progress/ProgressMonitor:isCancelAllTasks	()Z
    //   717: ifeq +74 -> 791
    //   720: aload_2
    //   721: astore 14
    //   723: aload 18
    //   725: astore 16
    //   727: aload_2
    //   728: astore 12
    //   730: aload 18
    //   732: astore 13
    //   734: aload_2
    //   735: astore 15
    //   737: aload 18
    //   739: astore 17
    //   741: aload_3
    //   742: iconst_3
    //   743: invokevirtual 116	net/lingala/zip4j/progress/ProgressMonitor:setResult	(I)V
    //   746: aload_2
    //   747: astore 14
    //   749: aload 18
    //   751: astore 16
    //   753: aload_2
    //   754: astore 12
    //   756: aload 18
    //   758: astore 13
    //   760: aload_2
    //   761: astore 15
    //   763: aload 18
    //   765: astore 17
    //   767: aload_3
    //   768: iconst_0
    //   769: invokevirtual 119	net/lingala/zip4j/progress/ProgressMonitor:setState	(I)V
    //   772: aload 18
    //   774: ifnull +8 -> 782
    //   777: aload 18
    //   779: invokevirtual 185	java/io/OutputStream:close	()V
    //   782: aload_2
    //   783: ifnull +7 -> 790
    //   786: aload_2
    //   787: invokevirtual 186	java/io/RandomAccessFile:close	()V
    //   790: return
    //   791: aload_2
    //   792: astore 14
    //   794: aload 18
    //   796: astore 16
    //   798: aload_2
    //   799: astore 12
    //   801: aload 18
    //   803: astore 13
    //   805: aload_2
    //   806: astore 15
    //   808: aload 18
    //   810: astore 17
    //   812: aload 25
    //   814: aload 19
    //   816: invokevirtual 232	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   819: pop
    //   820: aload_2
    //   821: astore 12
    //   823: aload 18
    //   825: astore 13
    //   827: aload_2
    //   828: astore 15
    //   830: aload 18
    //   832: astore 17
    //   834: aload_2
    //   835: invokevirtual 186	java/io/RandomAccessFile:close	()V
    //   838: iload 4
    //   840: iconst_1
    //   841: iadd
    //   842: istore 4
    //   844: iload 11
    //   846: istore 10
    //   848: goto -604 -> 244
    //   851: aload_2
    //   852: astore 14
    //   854: aload 18
    //   856: astore 16
    //   858: aload_2
    //   859: astore 12
    //   861: aload 18
    //   863: astore 13
    //   865: aload_2
    //   866: astore 15
    //   868: aload 18
    //   870: astore 17
    //   872: aload_1
    //   873: invokevirtual 236	net/lingala/zip4j/model/ZipModel:clone	()Ljava/lang/Object;
    //   876: checkcast 26	net/lingala/zip4j/model/ZipModel
    //   879: astore_1
    //   880: aload_2
    //   881: astore 14
    //   883: aload 18
    //   885: astore 16
    //   887: aload_2
    //   888: astore 12
    //   890: aload 18
    //   892: astore 13
    //   894: aload_2
    //   895: astore 15
    //   897: aload 18
    //   899: astore 17
    //   901: aload_1
    //   902: invokevirtual 34	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   905: lload 8
    //   907: invokevirtual 239	net/lingala/zip4j/model/EndCentralDirRecord:setOffsetOfStartOfCentralDir	(J)V
    //   910: aload_2
    //   911: astore 14
    //   913: aload 18
    //   915: astore 16
    //   917: aload_2
    //   918: astore 12
    //   920: aload 18
    //   922: astore 13
    //   924: aload_2
    //   925: astore 15
    //   927: aload 18
    //   929: astore 17
    //   931: aload_0
    //   932: aload_1
    //   933: aload 25
    //   935: iload 10
    //   937: invokespecial 243	net/lingala/zip4j/util/ArchiveMaintainer:updateSplitZipModel	(Lnet/lingala/zip4j/model/ZipModel;Ljava/util/ArrayList;Z)V
    //   940: aload_2
    //   941: astore 14
    //   943: aload 18
    //   945: astore 16
    //   947: aload_2
    //   948: astore 12
    //   950: aload 18
    //   952: astore 13
    //   954: aload_2
    //   955: astore 15
    //   957: aload 18
    //   959: astore 17
    //   961: new 245	net/lingala/zip4j/core/HeaderWriter
    //   964: dup
    //   965: invokespecial 246	net/lingala/zip4j/core/HeaderWriter:<init>	()V
    //   968: aload_1
    //   969: aload 18
    //   971: invokevirtual 250	net/lingala/zip4j/core/HeaderWriter:finalizeZipFileWithoutValidations	(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;)V
    //   974: aload_2
    //   975: astore 14
    //   977: aload 18
    //   979: astore 16
    //   981: aload_2
    //   982: astore 12
    //   984: aload 18
    //   986: astore 13
    //   988: aload_2
    //   989: astore 15
    //   991: aload 18
    //   993: astore 17
    //   995: aload_3
    //   996: invokevirtual 253	net/lingala/zip4j/progress/ProgressMonitor:endProgressMonitorSuccess	()V
    //   999: aload 18
    //   1001: ifnull +8 -> 1009
    //   1004: aload 18
    //   1006: invokevirtual 185	java/io/OutputStream:close	()V
    //   1009: aload_2
    //   1010: ifnull -220 -> 790
    //   1013: aload_2
    //   1014: invokevirtual 186	java/io/RandomAccessFile:close	()V
    //   1017: return
    //   1018: astore_1
    //   1019: return
    //   1020: astore_1
    //   1021: aload 15
    //   1023: astore 12
    //   1025: aload 17
    //   1027: astore 13
    //   1029: aload_3
    //   1030: aload_1
    //   1031: invokevirtual 175	net/lingala/zip4j/progress/ProgressMonitor:endProgressMonitorError	(Ljava/lang/Throwable;)V
    //   1034: aload 15
    //   1036: astore 12
    //   1038: aload 17
    //   1040: astore 13
    //   1042: new 17	net/lingala/zip4j/exception/ZipException
    //   1045: dup
    //   1046: aload_1
    //   1047: invokespecial 143	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   1050: athrow
    //   1051: astore_1
    //   1052: goto -270 -> 782
    //   1055: astore_1
    //   1056: return
    //   1057: astore 12
    //   1059: goto -221 -> 838
    //   1062: astore_1
    //   1063: goto -54 -> 1009
    //   1066: astore_2
    //   1067: goto -872 -> 195
    //   1070: astore_2
    //   1071: goto -866 -> 205
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1074	0	this	ArchiveMaintainer
    //   0	1074	1	paramZipModel	ZipModel
    //   0	1074	2	paramFile	File
    //   0	1074	3	paramProgressMonitor	ProgressMonitor
    //   239	604	4	i	int
    //   323	363	5	j	int
    //   281	265	6	k	int
    //   112	469	7	m	int
    //   76	830	8	l	long
    //   79	857	10	bool1	boolean
    //   319	526	11	bool2	boolean
    //   91	946	12	localObject1	Object
    //   1057	1	12	localIOException	IOException
    //   95	946	13	localObject2	Object
    //   83	893	14	localObject3	Object
    //   99	936	15	localObject4	Object
    //   87	893	16	localOutputStream1	OutputStream
    //   103	936	17	localObject5	Object
    //   52	953	18	localOutputStream2	OutputStream
    //   58	757	19	localLong	Long
    //   64	493	20	arrayOfByte	byte[]
    //   46	174	21	localObject6	Object
    //   49	179	22	localObject7	Object
    //   55	161	23	localObject8	Object
    //   61	163	24	localObject9	Object
    //   73	861	25	localArrayList	ArrayList
    // Exception table:
    //   from	to	target	type
    //   105	114	153	java/io/IOException
    //   143	153	153	java/io/IOException
    //   231	238	153	java/io/IOException
    //   272	280	153	java/io/IOException
    //   304	317	153	java/io/IOException
    //   359	366	153	java/io/IOException
    //   395	405	153	java/io/IOException
    //   434	447	153	java/io/IOException
    //   468	473	153	java/io/IOException
    //   494	499	153	java/io/IOException
    //   520	527	153	java/io/IOException
    //   556	570	153	java/io/IOException
    //   604	620	153	java/io/IOException
    //   641	657	153	java/io/IOException
    //   678	692	153	java/io/IOException
    //   713	720	153	java/io/IOException
    //   741	746	153	java/io/IOException
    //   767	772	153	java/io/IOException
    //   812	820	153	java/io/IOException
    //   872	880	153	java/io/IOException
    //   901	910	153	java/io/IOException
    //   931	940	153	java/io/IOException
    //   961	974	153	java/io/IOException
    //   995	999	153	java/io/IOException
    //   105	114	184	finally
    //   143	153	184	finally
    //   162	167	184	finally
    //   175	184	184	finally
    //   231	238	184	finally
    //   272	280	184	finally
    //   304	317	184	finally
    //   359	366	184	finally
    //   395	405	184	finally
    //   434	447	184	finally
    //   468	473	184	finally
    //   494	499	184	finally
    //   520	527	184	finally
    //   556	570	184	finally
    //   604	620	184	finally
    //   641	657	184	finally
    //   678	692	184	finally
    //   713	720	184	finally
    //   741	746	184	finally
    //   767	772	184	finally
    //   812	820	184	finally
    //   834	838	184	finally
    //   872	880	184	finally
    //   901	910	184	finally
    //   931	940	184	finally
    //   961	974	184	finally
    //   995	999	184	finally
    //   1029	1034	184	finally
    //   1042	1051	184	finally
    //   1013	1017	1018	java/io/IOException
    //   105	114	1020	java/lang/Exception
    //   143	153	1020	java/lang/Exception
    //   231	238	1020	java/lang/Exception
    //   272	280	1020	java/lang/Exception
    //   304	317	1020	java/lang/Exception
    //   359	366	1020	java/lang/Exception
    //   395	405	1020	java/lang/Exception
    //   434	447	1020	java/lang/Exception
    //   468	473	1020	java/lang/Exception
    //   494	499	1020	java/lang/Exception
    //   520	527	1020	java/lang/Exception
    //   556	570	1020	java/lang/Exception
    //   604	620	1020	java/lang/Exception
    //   641	657	1020	java/lang/Exception
    //   678	692	1020	java/lang/Exception
    //   713	720	1020	java/lang/Exception
    //   741	746	1020	java/lang/Exception
    //   767	772	1020	java/lang/Exception
    //   812	820	1020	java/lang/Exception
    //   834	838	1020	java/lang/Exception
    //   872	880	1020	java/lang/Exception
    //   901	910	1020	java/lang/Exception
    //   931	940	1020	java/lang/Exception
    //   961	974	1020	java/lang/Exception
    //   995	999	1020	java/lang/Exception
    //   777	782	1051	java/io/IOException
    //   786	790	1055	java/io/IOException
    //   834	838	1057	java/io/IOException
    //   1004	1009	1062	java/io/IOException
    //   190	195	1066	java/io/IOException
    //   200	205	1070	java/io/IOException
  }
  
  private OutputStream prepareOutputStreamForMerge(File paramFile)
    throws ZipException
  {
    if (paramFile == null) {
      throw new ZipException("outFile is null, cannot create outputstream");
    }
    try
    {
      paramFile = new FileOutputStream(paramFile);
      return paramFile;
    }
    catch (FileNotFoundException paramFile)
    {
      throw new ZipException(paramFile);
    }
    catch (Exception paramFile)
    {
      throw new ZipException(paramFile);
    }
  }
  
  private void restoreFileName(File paramFile, String paramString)
    throws ZipException
  {
    if (paramFile.delete())
    {
      if (!new File(paramString).renameTo(paramFile)) {
        throw new ZipException("cannot rename modified zip file");
      }
    }
    else {
      throw new ZipException("cannot delete old zip file");
    }
  }
  
  private void updateSplitEndCentralDirectory(ZipModel paramZipModel)
    throws ZipException
  {
    if (paramZipModel == null) {}
    try
    {
      throw new ZipException("zip model is null - cannot update end of central directory for split zip model");
    }
    catch (ZipException paramZipModel)
    {
      throw paramZipModel;
      if (paramZipModel.getCentralDirectory() == null) {
        throw new ZipException("corrupt zip model - getCentralDirectory, cannot update split zip model");
      }
    }
    catch (Exception paramZipModel)
    {
      throw new ZipException(paramZipModel);
    }
    paramZipModel.getEndCentralDirRecord().setNoOfThisDisk(0);
    paramZipModel.getEndCentralDirRecord().setNoOfThisDiskStartOfCentralDir(0);
    paramZipModel.getEndCentralDirRecord().setTotNoOfEntriesInCentralDir(paramZipModel.getCentralDirectory().getFileHeaders().size());
    paramZipModel.getEndCentralDirRecord().setTotNoOfEntriesInCentralDirOnThisDisk(paramZipModel.getCentralDirectory().getFileHeaders().size());
  }
  
  private void updateSplitFileHeader(ZipModel paramZipModel, ArrayList paramArrayList, boolean paramBoolean)
    throws ZipException
  {
    for (;;)
    {
      int m;
      int j;
      try
      {
        if (paramZipModel.getCentralDirectory() == null) {
          throw new ZipException("corrupt zip model - getCentralDirectory, cannot update split zip model");
        }
      }
      catch (ZipException paramZipModel)
      {
        throw paramZipModel;
        m = paramZipModel.getCentralDirectory().getFileHeaders().size();
        int i = 0;
        if (paramBoolean)
        {
          i = 4;
          break label177;
          if (k < ((FileHeader)paramZipModel.getCentralDirectory().getFileHeaders().get(j)).getDiskNumberStart())
          {
            Object localObject;
            localObject += ((Long)paramArrayList.get(k)).longValue();
            k += 1;
            continue;
          }
          ((FileHeader)paramZipModel.getCentralDirectory().getFileHeaders().get(j)).setOffsetLocalHeader(((FileHeader)paramZipModel.getCentralDirectory().getFileHeaders().get(j)).getOffsetLocalHeader() + l - i);
          ((FileHeader)paramZipModel.getCentralDirectory().getFileHeaders().get(j)).setDiskNumberStart(0);
          j += 1;
        }
      }
      catch (Exception paramZipModel)
      {
        throw new ZipException(paramZipModel);
      }
      label177:
      do
      {
        return;
        j = 0;
      } while (j >= m);
      long l = 0L;
      int k = 0;
    }
  }
  
  private void updateSplitZip64EndCentralDirLocator(ZipModel paramZipModel, ArrayList paramArrayList)
    throws ZipException
  {
    if (paramZipModel == null) {
      throw new ZipException("zip model is null, cannot update split Zip64 end of central directory locator");
    }
    if (paramZipModel.getZip64EndCentralDirLocator() == null) {
      return;
    }
    paramZipModel.getZip64EndCentralDirLocator().setNoOfDiskStartOfZip64EndOfCentralDirRec(0);
    long l = 0L;
    int i = 0;
    while (i < paramArrayList.size())
    {
      l += ((Long)paramArrayList.get(i)).longValue();
      i += 1;
    }
    paramZipModel.getZip64EndCentralDirLocator().setOffsetZip64EndOfCentralDirRec(paramZipModel.getZip64EndCentralDirLocator().getOffsetZip64EndOfCentralDirRec() + l);
    paramZipModel.getZip64EndCentralDirLocator().setTotNumberOfDiscs(1);
  }
  
  private void updateSplitZip64EndCentralDirRec(ZipModel paramZipModel, ArrayList paramArrayList)
    throws ZipException
  {
    if (paramZipModel == null) {
      throw new ZipException("zip model is null, cannot update split Zip64 end of central directory record");
    }
    if (paramZipModel.getZip64EndCentralDirRecord() == null) {
      return;
    }
    paramZipModel.getZip64EndCentralDirRecord().setNoOfThisDisk(0);
    paramZipModel.getZip64EndCentralDirRecord().setNoOfThisDiskStartOfCentralDir(0);
    paramZipModel.getZip64EndCentralDirRecord().setTotNoOfEntriesInCentralDirOnThisDisk(paramZipModel.getEndCentralDirRecord().getTotNoOfEntriesInCentralDir());
    long l = 0L;
    int i = 0;
    while (i < paramArrayList.size())
    {
      l += ((Long)paramArrayList.get(i)).longValue();
      i += 1;
    }
    paramZipModel.getZip64EndCentralDirRecord().setOffsetStartCenDirWRTStartDiskNo(paramZipModel.getZip64EndCentralDirRecord().getOffsetStartCenDirWRTStartDiskNo() + l);
  }
  
  private void updateSplitZipModel(ZipModel paramZipModel, ArrayList paramArrayList, boolean paramBoolean)
    throws ZipException
  {
    if (paramZipModel == null) {
      throw new ZipException("zip model is null, cannot update split zip model");
    }
    paramZipModel.setSplitArchive(false);
    updateSplitFileHeader(paramZipModel, paramArrayList, paramBoolean);
    updateSplitEndCentralDirectory(paramZipModel);
    if (paramZipModel.isZip64Format())
    {
      updateSplitZip64EndCentralDirLocator(paramZipModel, paramArrayList);
      updateSplitZip64EndCentralDirRec(paramZipModel, paramArrayList);
    }
  }
  
  public void initProgressMonitorForMergeOp(ZipModel paramZipModel, ProgressMonitor paramProgressMonitor)
    throws ZipException
  {
    if (paramZipModel == null) {
      throw new ZipException("zip model is null, cannot calculate total work for merge op");
    }
    paramProgressMonitor.setCurrentOperation(4);
    paramProgressMonitor.setFileName(paramZipModel.getZipFile());
    paramProgressMonitor.setTotalWork(calculateTotalWorkForMergeOp(paramZipModel));
    paramProgressMonitor.setState(1);
  }
  
  public void initProgressMonitorForRemoveOp(ZipModel paramZipModel, FileHeader paramFileHeader, ProgressMonitor paramProgressMonitor)
    throws ZipException
  {
    if ((paramZipModel == null) || (paramFileHeader == null) || (paramProgressMonitor == null)) {
      throw new ZipException("one of the input parameters is null, cannot calculate total work");
    }
    paramProgressMonitor.setCurrentOperation(2);
    paramProgressMonitor.setFileName(paramFileHeader.getFileName());
    paramProgressMonitor.setTotalWork(calculateTotalWorkForRemoveOp(paramZipModel, paramFileHeader));
    paramProgressMonitor.setState(1);
  }
  
  /* Error */
  public HashMap initRemoveZipFile(ZipModel paramZipModel, FileHeader paramFileHeader, ProgressMonitor paramProgressMonitor)
    throws ZipException
  {
    // Byte code:
    //   0: aload_2
    //   1: ifnull +7 -> 8
    //   4: aload_1
    //   5: ifnonnull +14 -> 19
    //   8: new 17	net/lingala/zip4j/exception/ZipException
    //   11: dup
    //   12: ldc_w 395
    //   15: invokespecial 101	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   18: athrow
    //   19: aconst_null
    //   20: astore 34
    //   22: aconst_null
    //   23: astore 25
    //   25: aconst_null
    //   26: astore 29
    //   28: aconst_null
    //   29: astore 27
    //   31: aconst_null
    //   32: astore 35
    //   34: aconst_null
    //   35: astore 26
    //   37: aconst_null
    //   38: astore 30
    //   40: aconst_null
    //   41: astore 33
    //   43: aconst_null
    //   44: astore 32
    //   46: aconst_null
    //   47: astore 31
    //   49: aconst_null
    //   50: astore 28
    //   52: iconst_0
    //   53: istore 4
    //   55: iconst_0
    //   56: istore 11
    //   58: iconst_0
    //   59: istore 6
    //   61: iconst_0
    //   62: istore 8
    //   64: iconst_0
    //   65: istore 9
    //   67: iconst_0
    //   68: istore 10
    //   70: iconst_0
    //   71: istore 5
    //   73: aconst_null
    //   74: astore 24
    //   76: aconst_null
    //   77: astore 36
    //   79: aconst_null
    //   80: astore 20
    //   82: new 397	java/util/HashMap
    //   85: dup
    //   86: invokespecial 398	java/util/HashMap:<init>	()V
    //   89: astore 37
    //   91: aload 20
    //   93: astore 22
    //   95: aload 24
    //   97: astore 23
    //   99: aload 36
    //   101: astore 21
    //   103: aload_1
    //   104: aload_2
    //   105: invokestatic 402	net/lingala/zip4j/util/Zip4jUtil:getIndexOfFileHeader	(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;)I
    //   108: istore 7
    //   110: iload 7
    //   112: ifge +130 -> 242
    //   115: aload 20
    //   117: astore 22
    //   119: aload 24
    //   121: astore 23
    //   123: aload 36
    //   125: astore 21
    //   127: new 17	net/lingala/zip4j/exception/ZipException
    //   130: dup
    //   131: ldc_w 404
    //   134: invokespecial 101	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   137: athrow
    //   138: astore_2
    //   139: aconst_null
    //   140: astore 24
    //   142: aload 27
    //   144: astore 26
    //   146: aload 22
    //   148: astore 20
    //   150: aload 28
    //   152: astore_1
    //   153: aload_1
    //   154: astore 21
    //   156: aload 24
    //   158: astore 22
    //   160: iload 5
    //   162: istore 4
    //   164: aload 20
    //   166: astore 23
    //   168: aload 26
    //   170: astore 25
    //   172: aload_3
    //   173: aload_2
    //   174: invokevirtual 175	net/lingala/zip4j/progress/ProgressMonitor:endProgressMonitorError	(Ljava/lang/Throwable;)V
    //   177: aload_1
    //   178: astore 21
    //   180: aload 24
    //   182: astore 22
    //   184: iload 5
    //   186: istore 4
    //   188: aload 20
    //   190: astore 23
    //   192: aload 26
    //   194: astore 25
    //   196: aload_2
    //   197: athrow
    //   198: astore_1
    //   199: aload 23
    //   201: astore 20
    //   203: aload 21
    //   205: astore 23
    //   207: aload 23
    //   209: ifnull +8 -> 217
    //   212: aload 23
    //   214: invokevirtual 186	java/io/RandomAccessFile:close	()V
    //   217: aload 22
    //   219: ifnull +8 -> 227
    //   222: aload 22
    //   224: invokevirtual 185	java/io/OutputStream:close	()V
    //   227: iload 4
    //   229: ifeq +2101 -> 2330
    //   232: aload_0
    //   233: aload 25
    //   235: aload 20
    //   237: invokespecial 406	net/lingala/zip4j/util/ArchiveMaintainer:restoreFileName	(Ljava/io/File;Ljava/lang/String;)V
    //   240: aload_1
    //   241: athrow
    //   242: aload 20
    //   244: astore 22
    //   246: aload 24
    //   248: astore 23
    //   250: aload 36
    //   252: astore 21
    //   254: aload_1
    //   255: invokevirtual 30	net/lingala/zip4j/model/ZipModel:isSplitArchive	()Z
    //   258: ifeq +97 -> 355
    //   261: aload 20
    //   263: astore 22
    //   265: aload 24
    //   267: astore 23
    //   269: aload 36
    //   271: astore 21
    //   273: new 17	net/lingala/zip4j/exception/ZipException
    //   276: dup
    //   277: ldc_w 408
    //   280: invokespecial 101	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   283: athrow
    //   284: astore_2
    //   285: aconst_null
    //   286: astore 24
    //   288: aload 29
    //   290: astore 26
    //   292: aload 23
    //   294: astore 20
    //   296: iload 6
    //   298: istore 5
    //   300: aload 30
    //   302: astore_1
    //   303: aload_1
    //   304: astore 21
    //   306: aload 24
    //   308: astore 22
    //   310: iload 5
    //   312: istore 4
    //   314: aload 20
    //   316: astore 23
    //   318: aload 26
    //   320: astore 25
    //   322: aload_3
    //   323: aload_2
    //   324: invokevirtual 175	net/lingala/zip4j/progress/ProgressMonitor:endProgressMonitorError	(Ljava/lang/Throwable;)V
    //   327: aload_1
    //   328: astore 21
    //   330: aload 24
    //   332: astore 22
    //   334: iload 5
    //   336: istore 4
    //   338: aload 20
    //   340: astore 23
    //   342: aload 26
    //   344: astore 25
    //   346: new 17	net/lingala/zip4j/exception/ZipException
    //   349: dup
    //   350: aload_2
    //   351: invokespecial 143	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   354: athrow
    //   355: aload 20
    //   357: astore 22
    //   359: aload 24
    //   361: astore 23
    //   363: aload 36
    //   365: astore 21
    //   367: invokestatic 413	java/lang/System:currentTimeMillis	()J
    //   370: lstore 12
    //   372: aload 20
    //   374: astore 22
    //   376: aload 24
    //   378: astore 23
    //   380: aload 36
    //   382: astore 21
    //   384: new 57	java/lang/StringBuilder
    //   387: dup
    //   388: invokespecial 58	java/lang/StringBuilder:<init>	()V
    //   391: aload_1
    //   392: invokevirtual 44	net/lingala/zip4j/model/ZipModel:getZipFile	()Ljava/lang/String;
    //   395: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   398: lload 12
    //   400: ldc2_w 414
    //   403: lrem
    //   404: invokevirtual 418	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   407: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   410: astore 20
    //   412: aload 20
    //   414: astore 22
    //   416: aload 20
    //   418: astore 23
    //   420: aload 20
    //   422: astore 21
    //   424: new 46	java/io/File
    //   427: dup
    //   428: aload 20
    //   430: invokespecial 49	java/io/File:<init>	(Ljava/lang/String;)V
    //   433: astore 24
    //   435: aload 20
    //   437: astore 22
    //   439: aload 20
    //   441: astore 23
    //   443: aload 20
    //   445: astore 21
    //   447: aload 24
    //   449: invokevirtual 421	java/io/File:exists	()Z
    //   452: ifeq +86 -> 538
    //   455: aload 20
    //   457: astore 22
    //   459: aload 20
    //   461: astore 23
    //   463: aload 20
    //   465: astore 21
    //   467: invokestatic 413	java/lang/System:currentTimeMillis	()J
    //   470: lstore 12
    //   472: aload 20
    //   474: astore 22
    //   476: aload 20
    //   478: astore 23
    //   480: aload 20
    //   482: astore 21
    //   484: new 57	java/lang/StringBuilder
    //   487: dup
    //   488: invokespecial 58	java/lang/StringBuilder:<init>	()V
    //   491: aload_1
    //   492: invokevirtual 44	net/lingala/zip4j/model/ZipModel:getZipFile	()Ljava/lang/String;
    //   495: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   498: lload 12
    //   500: ldc2_w 414
    //   503: lrem
    //   504: invokevirtual 418	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   507: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   510: astore 20
    //   512: aload 20
    //   514: astore 22
    //   516: aload 20
    //   518: astore 23
    //   520: aload 20
    //   522: astore 21
    //   524: new 46	java/io/File
    //   527: dup
    //   528: aload 20
    //   530: invokespecial 49	java/io/File:<init>	(Ljava/lang/String;)V
    //   533: astore 24
    //   535: goto -100 -> 435
    //   538: aload 20
    //   540: astore 22
    //   542: aload 20
    //   544: astore 23
    //   546: aload 20
    //   548: astore 21
    //   550: new 423	net/lingala/zip4j/io/SplitOutputStream
    //   553: dup
    //   554: new 46	java/io/File
    //   557: dup
    //   558: aload 20
    //   560: invokespecial 49	java/io/File:<init>	(Ljava/lang/String;)V
    //   563: invokespecial 424	net/lingala/zip4j/io/SplitOutputStream:<init>	(Ljava/io/File;)V
    //   566: astore 24
    //   568: aload 35
    //   570: astore 21
    //   572: aload 24
    //   574: astore 22
    //   576: aload 20
    //   578: astore 23
    //   580: aload 34
    //   582: astore 25
    //   584: new 46	java/io/File
    //   587: dup
    //   588: aload_1
    //   589: invokevirtual 44	net/lingala/zip4j/model/ZipModel:getZipFile	()Ljava/lang/String;
    //   592: invokespecial 49	java/io/File:<init>	(Ljava/lang/String;)V
    //   595: astore 26
    //   597: aload 33
    //   599: astore 21
    //   601: iload 8
    //   603: istore 4
    //   605: aload 32
    //   607: astore 22
    //   609: iload 9
    //   611: istore 5
    //   613: aload 31
    //   615: astore 23
    //   617: iload 10
    //   619: istore 6
    //   621: aload_0
    //   622: aload_1
    //   623: ldc -86
    //   625: invokespecial 426	net/lingala/zip4j/util/ArchiveMaintainer:createFileHandler	(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Ljava/io/RandomAccessFile;
    //   628: astore 25
    //   630: aload 25
    //   632: astore 21
    //   634: iload 8
    //   636: istore 4
    //   638: aload 25
    //   640: astore 22
    //   642: iload 9
    //   644: istore 5
    //   646: aload 25
    //   648: astore 23
    //   650: iload 10
    //   652: istore 6
    //   654: new 428	net/lingala/zip4j/core/HeaderReader
    //   657: dup
    //   658: aload 25
    //   660: invokespecial 431	net/lingala/zip4j/core/HeaderReader:<init>	(Ljava/io/RandomAccessFile;)V
    //   663: aload_2
    //   664: invokevirtual 435	net/lingala/zip4j/core/HeaderReader:readLocalFileHeader	(Lnet/lingala/zip4j/model/FileHeader;)Lnet/lingala/zip4j/model/LocalFileHeader;
    //   667: ifnonnull +90 -> 757
    //   670: aload 25
    //   672: astore 21
    //   674: iload 8
    //   676: istore 4
    //   678: aload 25
    //   680: astore 22
    //   682: iload 9
    //   684: istore 5
    //   686: aload 25
    //   688: astore 23
    //   690: iload 10
    //   692: istore 6
    //   694: new 17	net/lingala/zip4j/exception/ZipException
    //   697: dup
    //   698: ldc_w 437
    //   701: invokespecial 101	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   704: athrow
    //   705: astore_2
    //   706: aload 21
    //   708: astore_1
    //   709: iload 4
    //   711: istore 5
    //   713: goto -560 -> 153
    //   716: astore_1
    //   717: aload 20
    //   719: astore 22
    //   721: aload 20
    //   723: astore 23
    //   725: aload 20
    //   727: astore 21
    //   729: new 17	net/lingala/zip4j/exception/ZipException
    //   732: dup
    //   733: aload_1
    //   734: invokespecial 143	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   737: athrow
    //   738: astore_1
    //   739: aconst_null
    //   740: astore 22
    //   742: aload 26
    //   744: astore 23
    //   746: iload 11
    //   748: istore 4
    //   750: aload 21
    //   752: astore 20
    //   754: goto -547 -> 207
    //   757: aload 25
    //   759: astore 21
    //   761: iload 8
    //   763: istore 4
    //   765: aload 25
    //   767: astore 22
    //   769: iload 9
    //   771: istore 5
    //   773: aload 25
    //   775: astore 23
    //   777: iload 10
    //   779: istore 6
    //   781: aload_2
    //   782: invokevirtual 300	net/lingala/zip4j/model/FileHeader:getOffsetLocalHeader	()J
    //   785: lstore 12
    //   787: aload 25
    //   789: astore 21
    //   791: iload 8
    //   793: istore 4
    //   795: lload 12
    //   797: lstore 14
    //   799: aload 25
    //   801: astore 22
    //   803: iload 9
    //   805: istore 5
    //   807: aload 25
    //   809: astore 23
    //   811: iload 10
    //   813: istore 6
    //   815: aload_2
    //   816: invokevirtual 441	net/lingala/zip4j/model/FileHeader:getZip64ExtendedInfo	()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
    //   819: ifnull +78 -> 897
    //   822: aload 25
    //   824: astore 21
    //   826: iload 8
    //   828: istore 4
    //   830: lload 12
    //   832: lstore 14
    //   834: aload 25
    //   836: astore 22
    //   838: iload 9
    //   840: istore 5
    //   842: aload 25
    //   844: astore 23
    //   846: iload 10
    //   848: istore 6
    //   850: aload_2
    //   851: invokevirtual 441	net/lingala/zip4j/model/FileHeader:getZip64ExtendedInfo	()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
    //   854: invokevirtual 444	net/lingala/zip4j/model/Zip64ExtendedInfo:getOffsetLocalHeader	()J
    //   857: ldc2_w 445
    //   860: lcmp
    //   861: ifeq +36 -> 897
    //   864: aload 25
    //   866: astore 21
    //   868: iload 8
    //   870: istore 4
    //   872: aload 25
    //   874: astore 22
    //   876: iload 9
    //   878: istore 5
    //   880: aload 25
    //   882: astore 23
    //   884: iload 10
    //   886: istore 6
    //   888: aload_2
    //   889: invokevirtual 441	net/lingala/zip4j/model/FileHeader:getZip64ExtendedInfo	()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
    //   892: invokevirtual 444	net/lingala/zip4j/model/Zip64ExtendedInfo:getOffsetLocalHeader	()J
    //   895: lstore 14
    //   897: ldc2_w 445
    //   900: lstore 12
    //   902: aload 25
    //   904: astore 21
    //   906: iload 8
    //   908: istore 4
    //   910: aload 25
    //   912: astore 22
    //   914: iload 9
    //   916: istore 5
    //   918: aload 25
    //   920: astore 23
    //   922: iload 10
    //   924: istore 6
    //   926: aload_1
    //   927: invokevirtual 34	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   930: invokevirtual 223	net/lingala/zip4j/model/EndCentralDirRecord:getOffsetOfStartOfCentralDir	()J
    //   933: lstore 18
    //   935: aload 25
    //   937: astore 21
    //   939: iload 8
    //   941: istore 4
    //   943: lload 18
    //   945: lstore 16
    //   947: aload 25
    //   949: astore 22
    //   951: iload 9
    //   953: istore 5
    //   955: aload 25
    //   957: astore 23
    //   959: iload 10
    //   961: istore 6
    //   963: aload_1
    //   964: invokevirtual 363	net/lingala/zip4j/model/ZipModel:isZip64Format	()Z
    //   967: ifeq +71 -> 1038
    //   970: aload 25
    //   972: astore 21
    //   974: iload 8
    //   976: istore 4
    //   978: lload 18
    //   980: lstore 16
    //   982: aload 25
    //   984: astore 22
    //   986: iload 9
    //   988: istore 5
    //   990: aload 25
    //   992: astore 23
    //   994: iload 10
    //   996: istore 6
    //   998: aload_1
    //   999: invokevirtual 335	net/lingala/zip4j/model/ZipModel:getZip64EndCentralDirRecord	()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
    //   1002: ifnull +36 -> 1038
    //   1005: aload 25
    //   1007: astore 21
    //   1009: iload 8
    //   1011: istore 4
    //   1013: aload 25
    //   1015: astore 22
    //   1017: iload 9
    //   1019: istore 5
    //   1021: aload 25
    //   1023: astore 23
    //   1025: iload 10
    //   1027: istore 6
    //   1029: aload_1
    //   1030: invokevirtual 335	net/lingala/zip4j/model/ZipModel:getZip64EndCentralDirRecord	()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
    //   1033: invokevirtual 347	net/lingala/zip4j/model/Zip64EndCentralDirRecord:getOffsetStartCenDirWRTStartDiskNo	()J
    //   1036: lstore 16
    //   1038: aload 25
    //   1040: astore 21
    //   1042: iload 8
    //   1044: istore 4
    //   1046: aload 25
    //   1048: astore 22
    //   1050: iload 9
    //   1052: istore 5
    //   1054: aload 25
    //   1056: astore 23
    //   1058: iload 10
    //   1060: istore 6
    //   1062: aload_1
    //   1063: invokevirtual 203	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   1066: invokevirtual 209	net/lingala/zip4j/model/CentralDirectory:getFileHeaders	()Ljava/util/ArrayList;
    //   1069: astore_2
    //   1070: aload 25
    //   1072: astore 21
    //   1074: iload 8
    //   1076: istore 4
    //   1078: aload 25
    //   1080: astore 22
    //   1082: iload 9
    //   1084: istore 5
    //   1086: aload 25
    //   1088: astore 23
    //   1090: iload 10
    //   1092: istore 6
    //   1094: iload 7
    //   1096: aload_2
    //   1097: invokevirtual 212	java/util/ArrayList:size	()I
    //   1100: iconst_1
    //   1101: isub
    //   1102: if_icmpne +47 -> 1149
    //   1105: lload 16
    //   1107: lconst_1
    //   1108: lsub
    //   1109: lstore 12
    //   1111: goto +1261 -> 2372
    //   1114: aload 25
    //   1116: astore 21
    //   1118: iload 8
    //   1120: istore 4
    //   1122: aload 25
    //   1124: astore 22
    //   1126: iload 9
    //   1128: istore 5
    //   1130: aload 25
    //   1132: astore 23
    //   1134: iload 10
    //   1136: istore 6
    //   1138: new 17	net/lingala/zip4j/exception/ZipException
    //   1141: dup
    //   1142: ldc_w 448
    //   1145: invokespecial 101	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   1148: athrow
    //   1149: aload 25
    //   1151: astore 21
    //   1153: iload 8
    //   1155: istore 4
    //   1157: aload 25
    //   1159: astore 22
    //   1161: iload 9
    //   1163: istore 5
    //   1165: aload 25
    //   1167: astore 23
    //   1169: iload 10
    //   1171: istore 6
    //   1173: aload_2
    //   1174: iload 7
    //   1176: iconst_1
    //   1177: iadd
    //   1178: invokevirtual 294	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   1181: checkcast 88	net/lingala/zip4j/model/FileHeader
    //   1184: astore 27
    //   1186: aload 27
    //   1188: ifnull +1184 -> 2372
    //   1191: aload 25
    //   1193: astore 21
    //   1195: iload 8
    //   1197: istore 4
    //   1199: aload 25
    //   1201: astore 22
    //   1203: iload 9
    //   1205: istore 5
    //   1207: aload 25
    //   1209: astore 23
    //   1211: iload 10
    //   1213: istore 6
    //   1215: aload 27
    //   1217: invokevirtual 300	net/lingala/zip4j/model/FileHeader:getOffsetLocalHeader	()J
    //   1220: lconst_1
    //   1221: lsub
    //   1222: lstore 18
    //   1224: aload 25
    //   1226: astore 21
    //   1228: iload 8
    //   1230: istore 4
    //   1232: lload 18
    //   1234: lstore 12
    //   1236: aload 25
    //   1238: astore 22
    //   1240: iload 9
    //   1242: istore 5
    //   1244: aload 25
    //   1246: astore 23
    //   1248: iload 10
    //   1250: istore 6
    //   1252: aload 27
    //   1254: invokevirtual 441	net/lingala/zip4j/model/FileHeader:getZip64ExtendedInfo	()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
    //   1257: ifnull +1115 -> 2372
    //   1260: aload 25
    //   1262: astore 21
    //   1264: iload 8
    //   1266: istore 4
    //   1268: lload 18
    //   1270: lstore 12
    //   1272: aload 25
    //   1274: astore 22
    //   1276: iload 9
    //   1278: istore 5
    //   1280: aload 25
    //   1282: astore 23
    //   1284: iload 10
    //   1286: istore 6
    //   1288: aload 27
    //   1290: invokevirtual 441	net/lingala/zip4j/model/FileHeader:getZip64ExtendedInfo	()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
    //   1293: invokevirtual 444	net/lingala/zip4j/model/Zip64ExtendedInfo:getOffsetLocalHeader	()J
    //   1296: ldc2_w 445
    //   1299: lcmp
    //   1300: ifeq +1072 -> 2372
    //   1303: aload 25
    //   1305: astore 21
    //   1307: iload 8
    //   1309: istore 4
    //   1311: aload 25
    //   1313: astore 22
    //   1315: iload 9
    //   1317: istore 5
    //   1319: aload 25
    //   1321: astore 23
    //   1323: iload 10
    //   1325: istore 6
    //   1327: aload 27
    //   1329: invokevirtual 441	net/lingala/zip4j/model/FileHeader:getZip64ExtendedInfo	()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
    //   1332: invokevirtual 444	net/lingala/zip4j/model/Zip64ExtendedInfo:getOffsetLocalHeader	()J
    //   1335: lconst_1
    //   1336: lsub
    //   1337: lstore 12
    //   1339: goto +1033 -> 2372
    //   1342: iload 7
    //   1344: ifne +203 -> 1547
    //   1347: aload 25
    //   1349: astore 21
    //   1351: iload 8
    //   1353: istore 4
    //   1355: aload 25
    //   1357: astore 22
    //   1359: iload 9
    //   1361: istore 5
    //   1363: aload 25
    //   1365: astore 23
    //   1367: iload 10
    //   1369: istore 6
    //   1371: aload_1
    //   1372: invokevirtual 203	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   1375: invokevirtual 209	net/lingala/zip4j/model/CentralDirectory:getFileHeaders	()Ljava/util/ArrayList;
    //   1378: invokevirtual 212	java/util/ArrayList:size	()I
    //   1381: iconst_1
    //   1382: if_icmple +42 -> 1424
    //   1385: aload 25
    //   1387: astore 21
    //   1389: iload 8
    //   1391: istore 4
    //   1393: aload 25
    //   1395: astore 22
    //   1397: iload 9
    //   1399: istore 5
    //   1401: aload 25
    //   1403: astore 23
    //   1405: iload 10
    //   1407: istore 6
    //   1409: aload_0
    //   1410: aload 25
    //   1412: aload 24
    //   1414: lconst_1
    //   1415: lload 12
    //   1417: ladd
    //   1418: lload 16
    //   1420: aload_3
    //   1421: invokespecial 228	net/lingala/zip4j/util/ArchiveMaintainer:copyFile	(Ljava/io/RandomAccessFile;Ljava/io/OutputStream;JJLnet/lingala/zip4j/progress/ProgressMonitor;)V
    //   1424: aload 25
    //   1426: astore 21
    //   1428: iload 8
    //   1430: istore 4
    //   1432: aload 25
    //   1434: astore 22
    //   1436: iload 9
    //   1438: istore 5
    //   1440: aload 25
    //   1442: astore 23
    //   1444: iload 10
    //   1446: istore 6
    //   1448: aload_3
    //   1449: invokevirtual 112	net/lingala/zip4j/progress/ProgressMonitor:isCancelAllTasks	()Z
    //   1452: ifeq +274 -> 1726
    //   1455: aload 25
    //   1457: astore 21
    //   1459: iload 8
    //   1461: istore 4
    //   1463: aload 25
    //   1465: astore 22
    //   1467: iload 9
    //   1469: istore 5
    //   1471: aload 25
    //   1473: astore 23
    //   1475: iload 10
    //   1477: istore 6
    //   1479: aload_3
    //   1480: iconst_3
    //   1481: invokevirtual 116	net/lingala/zip4j/progress/ProgressMonitor:setResult	(I)V
    //   1484: aload 25
    //   1486: astore 21
    //   1488: iload 8
    //   1490: istore 4
    //   1492: aload 25
    //   1494: astore 22
    //   1496: iload 9
    //   1498: istore 5
    //   1500: aload 25
    //   1502: astore 23
    //   1504: iload 10
    //   1506: istore 6
    //   1508: aload_3
    //   1509: iconst_0
    //   1510: invokevirtual 119	net/lingala/zip4j/progress/ProgressMonitor:setState	(I)V
    //   1513: aload 25
    //   1515: ifnull +8 -> 1523
    //   1518: aload 25
    //   1520: invokevirtual 186	java/io/RandomAccessFile:close	()V
    //   1523: aload 24
    //   1525: ifnull +8 -> 1533
    //   1528: aload 24
    //   1530: invokevirtual 185	java/io/OutputStream:close	()V
    //   1533: iconst_0
    //   1534: ifeq +177 -> 1711
    //   1537: aload_0
    //   1538: aload 26
    //   1540: aload 20
    //   1542: invokespecial 406	net/lingala/zip4j/util/ArchiveMaintainer:restoreFileName	(Ljava/io/File;Ljava/lang/String;)V
    //   1545: aconst_null
    //   1546: areturn
    //   1547: aload 25
    //   1549: astore 21
    //   1551: iload 8
    //   1553: istore 4
    //   1555: aload 25
    //   1557: astore 22
    //   1559: iload 9
    //   1561: istore 5
    //   1563: aload 25
    //   1565: astore 23
    //   1567: iload 10
    //   1569: istore 6
    //   1571: iload 7
    //   1573: aload_2
    //   1574: invokevirtual 212	java/util/ArrayList:size	()I
    //   1577: iconst_1
    //   1578: isub
    //   1579: if_icmpne +42 -> 1621
    //   1582: aload 25
    //   1584: astore 21
    //   1586: iload 8
    //   1588: istore 4
    //   1590: aload 25
    //   1592: astore 22
    //   1594: iload 9
    //   1596: istore 5
    //   1598: aload 25
    //   1600: astore 23
    //   1602: iload 10
    //   1604: istore 6
    //   1606: aload_0
    //   1607: aload 25
    //   1609: aload 24
    //   1611: lconst_0
    //   1612: lload 14
    //   1614: aload_3
    //   1615: invokespecial 228	net/lingala/zip4j/util/ArchiveMaintainer:copyFile	(Ljava/io/RandomAccessFile;Ljava/io/OutputStream;JJLnet/lingala/zip4j/progress/ProgressMonitor;)V
    //   1618: goto -194 -> 1424
    //   1621: aload 25
    //   1623: astore 21
    //   1625: iload 8
    //   1627: istore 4
    //   1629: aload 25
    //   1631: astore 22
    //   1633: iload 9
    //   1635: istore 5
    //   1637: aload 25
    //   1639: astore 23
    //   1641: iload 10
    //   1643: istore 6
    //   1645: aload_0
    //   1646: aload 25
    //   1648: aload 24
    //   1650: lconst_0
    //   1651: lload 14
    //   1653: aload_3
    //   1654: invokespecial 228	net/lingala/zip4j/util/ArchiveMaintainer:copyFile	(Ljava/io/RandomAccessFile;Ljava/io/OutputStream;JJLnet/lingala/zip4j/progress/ProgressMonitor;)V
    //   1657: aload 25
    //   1659: astore 21
    //   1661: iload 8
    //   1663: istore 4
    //   1665: aload 25
    //   1667: astore 22
    //   1669: iload 9
    //   1671: istore 5
    //   1673: aload 25
    //   1675: astore 23
    //   1677: iload 10
    //   1679: istore 6
    //   1681: aload_0
    //   1682: aload 25
    //   1684: aload 24
    //   1686: lconst_1
    //   1687: lload 12
    //   1689: ladd
    //   1690: lload 16
    //   1692: aload_3
    //   1693: invokespecial 228	net/lingala/zip4j/util/ArchiveMaintainer:copyFile	(Ljava/io/RandomAccessFile;Ljava/io/OutputStream;JJLnet/lingala/zip4j/progress/ProgressMonitor;)V
    //   1696: goto -272 -> 1424
    //   1699: astore_1
    //   1700: new 17	net/lingala/zip4j/exception/ZipException
    //   1703: dup
    //   1704: ldc_w 450
    //   1707: invokespecial 101	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   1710: athrow
    //   1711: new 46	java/io/File
    //   1714: dup
    //   1715: aload 20
    //   1717: invokespecial 49	java/io/File:<init>	(Ljava/lang/String;)V
    //   1720: invokevirtual 264	java/io/File:delete	()Z
    //   1723: pop
    //   1724: aconst_null
    //   1725: areturn
    //   1726: aload 25
    //   1728: astore 21
    //   1730: iload 8
    //   1732: istore 4
    //   1734: aload 25
    //   1736: astore 22
    //   1738: iload 9
    //   1740: istore 5
    //   1742: aload 25
    //   1744: astore 23
    //   1746: iload 10
    //   1748: istore 6
    //   1750: aload_1
    //   1751: invokevirtual 34	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   1754: aload 24
    //   1756: checkcast 423	net/lingala/zip4j/io/SplitOutputStream
    //   1759: invokevirtual 453	net/lingala/zip4j/io/SplitOutputStream:getFilePointer	()J
    //   1762: invokevirtual 239	net/lingala/zip4j/model/EndCentralDirRecord:setOffsetOfStartOfCentralDir	(J)V
    //   1765: aload 25
    //   1767: astore 21
    //   1769: iload 8
    //   1771: istore 4
    //   1773: aload 25
    //   1775: astore 22
    //   1777: iload 9
    //   1779: istore 5
    //   1781: aload 25
    //   1783: astore 23
    //   1785: iload 10
    //   1787: istore 6
    //   1789: aload_1
    //   1790: invokevirtual 34	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   1793: aload_1
    //   1794: invokevirtual 34	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   1797: invokevirtual 342	net/lingala/zip4j/model/EndCentralDirRecord:getTotNoOfEntriesInCentralDir	()I
    //   1800: iconst_1
    //   1801: isub
    //   1802: invokevirtual 286	net/lingala/zip4j/model/EndCentralDirRecord:setTotNoOfEntriesInCentralDir	(I)V
    //   1805: aload 25
    //   1807: astore 21
    //   1809: iload 8
    //   1811: istore 4
    //   1813: aload 25
    //   1815: astore 22
    //   1817: iload 9
    //   1819: istore 5
    //   1821: aload 25
    //   1823: astore 23
    //   1825: iload 10
    //   1827: istore 6
    //   1829: aload_1
    //   1830: invokevirtual 34	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   1833: aload_1
    //   1834: invokevirtual 34	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   1837: invokevirtual 456	net/lingala/zip4j/model/EndCentralDirRecord:getTotNoOfEntriesInCentralDirOnThisDisk	()I
    //   1840: iconst_1
    //   1841: isub
    //   1842: invokevirtual 289	net/lingala/zip4j/model/EndCentralDirRecord:setTotNoOfEntriesInCentralDirOnThisDisk	(I)V
    //   1845: aload 25
    //   1847: astore 21
    //   1849: iload 8
    //   1851: istore 4
    //   1853: aload 25
    //   1855: astore 22
    //   1857: iload 9
    //   1859: istore 5
    //   1861: aload 25
    //   1863: astore 23
    //   1865: iload 10
    //   1867: istore 6
    //   1869: aload_1
    //   1870: invokevirtual 203	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   1873: invokevirtual 209	net/lingala/zip4j/model/CentralDirectory:getFileHeaders	()Ljava/util/ArrayList;
    //   1876: iload 7
    //   1878: invokevirtual 459	java/util/ArrayList:remove	(I)Ljava/lang/Object;
    //   1881: pop
    //   1882: aload 25
    //   1884: astore 21
    //   1886: iload 8
    //   1888: istore 4
    //   1890: aload 25
    //   1892: astore 22
    //   1894: iload 9
    //   1896: istore 5
    //   1898: aload 25
    //   1900: astore 23
    //   1902: iload 10
    //   1904: istore 6
    //   1906: iload 7
    //   1908: aload_1
    //   1909: invokevirtual 203	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   1912: invokevirtual 209	net/lingala/zip4j/model/CentralDirectory:getFileHeaders	()Ljava/util/ArrayList;
    //   1915: invokevirtual 212	java/util/ArrayList:size	()I
    //   1918: if_icmpge +260 -> 2178
    //   1921: aload 25
    //   1923: astore 21
    //   1925: iload 8
    //   1927: istore 4
    //   1929: aload 25
    //   1931: astore 22
    //   1933: iload 9
    //   1935: istore 5
    //   1937: aload 25
    //   1939: astore 23
    //   1941: iload 10
    //   1943: istore 6
    //   1945: aload_1
    //   1946: invokevirtual 203	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   1949: invokevirtual 209	net/lingala/zip4j/model/CentralDirectory:getFileHeaders	()Ljava/util/ArrayList;
    //   1952: iload 7
    //   1954: invokevirtual 294	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   1957: checkcast 88	net/lingala/zip4j/model/FileHeader
    //   1960: invokevirtual 300	net/lingala/zip4j/model/FileHeader:getOffsetLocalHeader	()J
    //   1963: lstore 18
    //   1965: aload 25
    //   1967: astore 21
    //   1969: iload 8
    //   1971: istore 4
    //   1973: lload 18
    //   1975: lstore 16
    //   1977: aload 25
    //   1979: astore 22
    //   1981: iload 9
    //   1983: istore 5
    //   1985: aload 25
    //   1987: astore 23
    //   1989: iload 10
    //   1991: istore 6
    //   1993: aload_1
    //   1994: invokevirtual 203	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   1997: invokevirtual 209	net/lingala/zip4j/model/CentralDirectory:getFileHeaders	()Ljava/util/ArrayList;
    //   2000: iload 7
    //   2002: invokevirtual 294	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   2005: checkcast 88	net/lingala/zip4j/model/FileHeader
    //   2008: invokevirtual 441	net/lingala/zip4j/model/FileHeader:getZip64ExtendedInfo	()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
    //   2011: ifnull +106 -> 2117
    //   2014: aload 25
    //   2016: astore 21
    //   2018: iload 8
    //   2020: istore 4
    //   2022: lload 18
    //   2024: lstore 16
    //   2026: aload 25
    //   2028: astore 22
    //   2030: iload 9
    //   2032: istore 5
    //   2034: aload 25
    //   2036: astore 23
    //   2038: iload 10
    //   2040: istore 6
    //   2042: aload_1
    //   2043: invokevirtual 203	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   2046: invokevirtual 209	net/lingala/zip4j/model/CentralDirectory:getFileHeaders	()Ljava/util/ArrayList;
    //   2049: iload 7
    //   2051: invokevirtual 294	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   2054: checkcast 88	net/lingala/zip4j/model/FileHeader
    //   2057: invokevirtual 441	net/lingala/zip4j/model/FileHeader:getZip64ExtendedInfo	()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
    //   2060: invokevirtual 444	net/lingala/zip4j/model/Zip64ExtendedInfo:getOffsetLocalHeader	()J
    //   2063: ldc2_w 445
    //   2066: lcmp
    //   2067: ifeq +50 -> 2117
    //   2070: aload 25
    //   2072: astore 21
    //   2074: iload 8
    //   2076: istore 4
    //   2078: aload 25
    //   2080: astore 22
    //   2082: iload 9
    //   2084: istore 5
    //   2086: aload 25
    //   2088: astore 23
    //   2090: iload 10
    //   2092: istore 6
    //   2094: aload_1
    //   2095: invokevirtual 203	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   2098: invokevirtual 209	net/lingala/zip4j/model/CentralDirectory:getFileHeaders	()Ljava/util/ArrayList;
    //   2101: iload 7
    //   2103: invokevirtual 294	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   2106: checkcast 88	net/lingala/zip4j/model/FileHeader
    //   2109: invokevirtual 441	net/lingala/zip4j/model/FileHeader:getZip64ExtendedInfo	()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
    //   2112: invokevirtual 444	net/lingala/zip4j/model/Zip64ExtendedInfo:getOffsetLocalHeader	()J
    //   2115: lstore 16
    //   2117: aload 25
    //   2119: astore 21
    //   2121: iload 8
    //   2123: istore 4
    //   2125: aload 25
    //   2127: astore 22
    //   2129: iload 9
    //   2131: istore 5
    //   2133: aload 25
    //   2135: astore 23
    //   2137: iload 10
    //   2139: istore 6
    //   2141: aload_1
    //   2142: invokevirtual 203	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   2145: invokevirtual 209	net/lingala/zip4j/model/CentralDirectory:getFileHeaders	()Ljava/util/ArrayList;
    //   2148: iload 7
    //   2150: invokevirtual 294	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   2153: checkcast 88	net/lingala/zip4j/model/FileHeader
    //   2156: lload 16
    //   2158: lload 12
    //   2160: lload 14
    //   2162: lsub
    //   2163: lsub
    //   2164: lconst_1
    //   2165: lsub
    //   2166: invokevirtual 303	net/lingala/zip4j/model/FileHeader:setOffsetLocalHeader	(J)V
    //   2169: iload 7
    //   2171: iconst_1
    //   2172: iadd
    //   2173: istore 7
    //   2175: goto -293 -> 1882
    //   2178: aload 25
    //   2180: astore 21
    //   2182: iload 8
    //   2184: istore 4
    //   2186: aload 25
    //   2188: astore 22
    //   2190: iload 9
    //   2192: istore 5
    //   2194: aload 25
    //   2196: astore 23
    //   2198: iload 10
    //   2200: istore 6
    //   2202: new 245	net/lingala/zip4j/core/HeaderWriter
    //   2205: dup
    //   2206: invokespecial 246	net/lingala/zip4j/core/HeaderWriter:<init>	()V
    //   2209: aload_1
    //   2210: aload 24
    //   2212: invokevirtual 462	net/lingala/zip4j/core/HeaderWriter:finalizeZipFile	(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;)V
    //   2215: iconst_1
    //   2216: istore 5
    //   2218: iconst_1
    //   2219: istore 6
    //   2221: iconst_1
    //   2222: istore 4
    //   2224: aload 25
    //   2226: astore 21
    //   2228: aload 25
    //   2230: astore 22
    //   2232: aload 25
    //   2234: astore 23
    //   2236: aload 37
    //   2238: ldc_w 464
    //   2241: aload_1
    //   2242: invokevirtual 34	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   2245: invokevirtual 223	net/lingala/zip4j/model/EndCentralDirRecord:getOffsetOfStartOfCentralDir	()J
    //   2248: invokestatic 467	java/lang/Long:toString	(J)Ljava/lang/String;
    //   2251: invokevirtual 471	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   2254: pop
    //   2255: aload 25
    //   2257: ifnull +8 -> 2265
    //   2260: aload 25
    //   2262: invokevirtual 186	java/io/RandomAccessFile:close	()V
    //   2265: aload 24
    //   2267: ifnull +8 -> 2275
    //   2270: aload 24
    //   2272: invokevirtual 185	java/io/OutputStream:close	()V
    //   2275: iconst_1
    //   2276: ifeq +26 -> 2302
    //   2279: aload_0
    //   2280: aload 26
    //   2282: aload 20
    //   2284: invokespecial 406	net/lingala/zip4j/util/ArchiveMaintainer:restoreFileName	(Ljava/io/File;Ljava/lang/String;)V
    //   2287: aload 37
    //   2289: areturn
    //   2290: astore_1
    //   2291: new 17	net/lingala/zip4j/exception/ZipException
    //   2294: dup
    //   2295: ldc_w 450
    //   2298: invokespecial 101	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   2301: athrow
    //   2302: new 46	java/io/File
    //   2305: dup
    //   2306: aload 20
    //   2308: invokespecial 49	java/io/File:<init>	(Ljava/lang/String;)V
    //   2311: invokevirtual 264	java/io/File:delete	()Z
    //   2314: pop
    //   2315: aload 37
    //   2317: areturn
    //   2318: astore_1
    //   2319: new 17	net/lingala/zip4j/exception/ZipException
    //   2322: dup
    //   2323: ldc_w 450
    //   2326: invokespecial 101	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   2329: athrow
    //   2330: new 46	java/io/File
    //   2333: dup
    //   2334: aload 20
    //   2336: invokespecial 49	java/io/File:<init>	(Ljava/lang/String;)V
    //   2339: invokevirtual 264	java/io/File:delete	()Z
    //   2342: pop
    //   2343: goto -2103 -> 240
    //   2346: astore_2
    //   2347: aload 30
    //   2349: astore_1
    //   2350: iload 6
    //   2352: istore 5
    //   2354: aload 29
    //   2356: astore 26
    //   2358: goto -2055 -> 303
    //   2361: astore_2
    //   2362: aload 28
    //   2364: astore_1
    //   2365: aload 27
    //   2367: astore 26
    //   2369: goto -2216 -> 153
    //   2372: lload 14
    //   2374: lconst_0
    //   2375: lcmp
    //   2376: iflt -1262 -> 1114
    //   2379: lload 12
    //   2381: lconst_0
    //   2382: lcmp
    //   2383: ifge -1041 -> 1342
    //   2386: goto -1272 -> 1114
    //   2389: astore_2
    //   2390: aload 22
    //   2392: astore_1
    //   2393: goto -2090 -> 303
    //   2396: astore_1
    //   2397: aload 26
    //   2399: astore 25
    //   2401: aload 24
    //   2403: astore 22
    //   2405: iload 6
    //   2407: istore 4
    //   2409: goto -2202 -> 207
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	2412	0	this	ArchiveMaintainer
    //   0	2412	1	paramZipModel	ZipModel
    //   0	2412	2	paramFileHeader	FileHeader
    //   0	2412	3	paramProgressMonitor	ProgressMonitor
    //   53	2355	4	i	int
    //   71	2282	5	j	int
    //   59	2347	6	k	int
    //   108	2066	7	m	int
    //   62	2121	8	n	int
    //   65	2126	9	i1	int
    //   68	2131	10	i2	int
    //   56	691	11	i3	int
    //   370	2010	12	l1	long
    //   797	1576	14	l2	long
    //   945	1212	16	l3	long
    //   933	1090	18	l4	long
    //   80	2255	20	localObject1	Object
    //   101	2126	21	localObject2	Object
    //   93	2311	22	localObject3	Object
    //   97	2138	23	localObject4	Object
    //   74	2328	24	localObject5	Object
    //   23	2377	25	localObject6	Object
    //   35	2363	26	localObject7	Object
    //   29	2337	27	localFileHeader	FileHeader
    //   50	2313	28	localObject8	Object
    //   26	2329	29	localObject9	Object
    //   38	2310	30	localObject10	Object
    //   47	567	31	localObject11	Object
    //   44	562	32	localObject12	Object
    //   41	557	33	localObject13	Object
    //   20	561	34	localObject14	Object
    //   32	537	35	localObject15	Object
    //   77	304	36	localObject16	Object
    //   89	2227	37	localHashMap	HashMap
    // Exception table:
    //   from	to	target	type
    //   103	110	138	net/lingala/zip4j/exception/ZipException
    //   127	138	138	net/lingala/zip4j/exception/ZipException
    //   254	261	138	net/lingala/zip4j/exception/ZipException
    //   273	284	138	net/lingala/zip4j/exception/ZipException
    //   367	372	138	net/lingala/zip4j/exception/ZipException
    //   384	412	138	net/lingala/zip4j/exception/ZipException
    //   424	435	138	net/lingala/zip4j/exception/ZipException
    //   447	455	138	net/lingala/zip4j/exception/ZipException
    //   467	472	138	net/lingala/zip4j/exception/ZipException
    //   484	512	138	net/lingala/zip4j/exception/ZipException
    //   524	535	138	net/lingala/zip4j/exception/ZipException
    //   550	568	138	net/lingala/zip4j/exception/ZipException
    //   729	738	138	net/lingala/zip4j/exception/ZipException
    //   172	177	198	finally
    //   196	198	198	finally
    //   322	327	198	finally
    //   346	355	198	finally
    //   584	597	198	finally
    //   103	110	284	java/lang/Exception
    //   127	138	284	java/lang/Exception
    //   254	261	284	java/lang/Exception
    //   273	284	284	java/lang/Exception
    //   367	372	284	java/lang/Exception
    //   384	412	284	java/lang/Exception
    //   424	435	284	java/lang/Exception
    //   447	455	284	java/lang/Exception
    //   467	472	284	java/lang/Exception
    //   484	512	284	java/lang/Exception
    //   524	535	284	java/lang/Exception
    //   550	568	284	java/lang/Exception
    //   729	738	284	java/lang/Exception
    //   621	630	705	net/lingala/zip4j/exception/ZipException
    //   654	670	705	net/lingala/zip4j/exception/ZipException
    //   694	705	705	net/lingala/zip4j/exception/ZipException
    //   781	787	705	net/lingala/zip4j/exception/ZipException
    //   815	822	705	net/lingala/zip4j/exception/ZipException
    //   850	864	705	net/lingala/zip4j/exception/ZipException
    //   888	897	705	net/lingala/zip4j/exception/ZipException
    //   926	935	705	net/lingala/zip4j/exception/ZipException
    //   963	970	705	net/lingala/zip4j/exception/ZipException
    //   998	1005	705	net/lingala/zip4j/exception/ZipException
    //   1029	1038	705	net/lingala/zip4j/exception/ZipException
    //   1062	1070	705	net/lingala/zip4j/exception/ZipException
    //   1094	1105	705	net/lingala/zip4j/exception/ZipException
    //   1138	1149	705	net/lingala/zip4j/exception/ZipException
    //   1173	1186	705	net/lingala/zip4j/exception/ZipException
    //   1215	1224	705	net/lingala/zip4j/exception/ZipException
    //   1252	1260	705	net/lingala/zip4j/exception/ZipException
    //   1288	1303	705	net/lingala/zip4j/exception/ZipException
    //   1327	1339	705	net/lingala/zip4j/exception/ZipException
    //   1371	1385	705	net/lingala/zip4j/exception/ZipException
    //   1409	1424	705	net/lingala/zip4j/exception/ZipException
    //   1448	1455	705	net/lingala/zip4j/exception/ZipException
    //   1479	1484	705	net/lingala/zip4j/exception/ZipException
    //   1508	1513	705	net/lingala/zip4j/exception/ZipException
    //   1571	1582	705	net/lingala/zip4j/exception/ZipException
    //   1606	1618	705	net/lingala/zip4j/exception/ZipException
    //   1645	1657	705	net/lingala/zip4j/exception/ZipException
    //   1681	1696	705	net/lingala/zip4j/exception/ZipException
    //   1750	1765	705	net/lingala/zip4j/exception/ZipException
    //   1789	1805	705	net/lingala/zip4j/exception/ZipException
    //   1829	1845	705	net/lingala/zip4j/exception/ZipException
    //   1869	1882	705	net/lingala/zip4j/exception/ZipException
    //   1906	1921	705	net/lingala/zip4j/exception/ZipException
    //   1945	1965	705	net/lingala/zip4j/exception/ZipException
    //   1993	2014	705	net/lingala/zip4j/exception/ZipException
    //   2042	2070	705	net/lingala/zip4j/exception/ZipException
    //   2094	2117	705	net/lingala/zip4j/exception/ZipException
    //   2141	2169	705	net/lingala/zip4j/exception/ZipException
    //   2202	2215	705	net/lingala/zip4j/exception/ZipException
    //   2236	2255	705	net/lingala/zip4j/exception/ZipException
    //   550	568	716	java/io/FileNotFoundException
    //   103	110	738	finally
    //   127	138	738	finally
    //   254	261	738	finally
    //   273	284	738	finally
    //   367	372	738	finally
    //   384	412	738	finally
    //   424	435	738	finally
    //   447	455	738	finally
    //   467	472	738	finally
    //   484	512	738	finally
    //   524	535	738	finally
    //   550	568	738	finally
    //   729	738	738	finally
    //   1518	1523	1699	java/io/IOException
    //   1528	1533	1699	java/io/IOException
    //   2260	2265	2290	java/io/IOException
    //   2270	2275	2290	java/io/IOException
    //   212	217	2318	java/io/IOException
    //   222	227	2318	java/io/IOException
    //   584	597	2346	java/lang/Exception
    //   584	597	2361	net/lingala/zip4j/exception/ZipException
    //   621	630	2389	java/lang/Exception
    //   654	670	2389	java/lang/Exception
    //   694	705	2389	java/lang/Exception
    //   781	787	2389	java/lang/Exception
    //   815	822	2389	java/lang/Exception
    //   850	864	2389	java/lang/Exception
    //   888	897	2389	java/lang/Exception
    //   926	935	2389	java/lang/Exception
    //   963	970	2389	java/lang/Exception
    //   998	1005	2389	java/lang/Exception
    //   1029	1038	2389	java/lang/Exception
    //   1062	1070	2389	java/lang/Exception
    //   1094	1105	2389	java/lang/Exception
    //   1138	1149	2389	java/lang/Exception
    //   1173	1186	2389	java/lang/Exception
    //   1215	1224	2389	java/lang/Exception
    //   1252	1260	2389	java/lang/Exception
    //   1288	1303	2389	java/lang/Exception
    //   1327	1339	2389	java/lang/Exception
    //   1371	1385	2389	java/lang/Exception
    //   1409	1424	2389	java/lang/Exception
    //   1448	1455	2389	java/lang/Exception
    //   1479	1484	2389	java/lang/Exception
    //   1508	1513	2389	java/lang/Exception
    //   1571	1582	2389	java/lang/Exception
    //   1606	1618	2389	java/lang/Exception
    //   1645	1657	2389	java/lang/Exception
    //   1681	1696	2389	java/lang/Exception
    //   1750	1765	2389	java/lang/Exception
    //   1789	1805	2389	java/lang/Exception
    //   1829	1845	2389	java/lang/Exception
    //   1869	1882	2389	java/lang/Exception
    //   1906	1921	2389	java/lang/Exception
    //   1945	1965	2389	java/lang/Exception
    //   1993	2014	2389	java/lang/Exception
    //   2042	2070	2389	java/lang/Exception
    //   2094	2117	2389	java/lang/Exception
    //   2141	2169	2389	java/lang/Exception
    //   2202	2215	2389	java/lang/Exception
    //   2236	2255	2389	java/lang/Exception
    //   621	630	2396	finally
    //   654	670	2396	finally
    //   694	705	2396	finally
    //   781	787	2396	finally
    //   815	822	2396	finally
    //   850	864	2396	finally
    //   888	897	2396	finally
    //   926	935	2396	finally
    //   963	970	2396	finally
    //   998	1005	2396	finally
    //   1029	1038	2396	finally
    //   1062	1070	2396	finally
    //   1094	1105	2396	finally
    //   1138	1149	2396	finally
    //   1173	1186	2396	finally
    //   1215	1224	2396	finally
    //   1252	1260	2396	finally
    //   1288	1303	2396	finally
    //   1327	1339	2396	finally
    //   1371	1385	2396	finally
    //   1409	1424	2396	finally
    //   1448	1455	2396	finally
    //   1479	1484	2396	finally
    //   1508	1513	2396	finally
    //   1571	1582	2396	finally
    //   1606	1618	2396	finally
    //   1645	1657	2396	finally
    //   1681	1696	2396	finally
    //   1750	1765	2396	finally
    //   1789	1805	2396	finally
    //   1829	1845	2396	finally
    //   1869	1882	2396	finally
    //   1906	1921	2396	finally
    //   1945	1965	2396	finally
    //   1993	2014	2396	finally
    //   2042	2070	2396	finally
    //   2094	2117	2396	finally
    //   2141	2169	2396	finally
    //   2202	2215	2396	finally
    //   2236	2255	2396	finally
  }
  
  public void mergeSplitZipFiles(final ZipModel paramZipModel, final File paramFile, final ProgressMonitor paramProgressMonitor, boolean paramBoolean)
    throws ZipException
  {
    if (paramBoolean)
    {
      new Thread("Zip4j")
      {
        public void run()
        {
          try
          {
            ArchiveMaintainer.this.initMergeSplitZipFile(paramZipModel, paramFile, paramProgressMonitor);
            return;
          }
          catch (ZipException localZipException) {}
        }
      }.start();
      return;
    }
    initMergeSplitZipFile(paramZipModel, paramFile, paramProgressMonitor);
  }
  
  public HashMap removeZipFile(final ZipModel paramZipModel, final FileHeader paramFileHeader, final ProgressMonitor paramProgressMonitor, boolean paramBoolean)
    throws ZipException
  {
    if (paramBoolean)
    {
      new Thread("Zip4j")
      {
        public void run()
        {
          try
          {
            ArchiveMaintainer.this.initRemoveZipFile(paramZipModel, paramFileHeader, paramProgressMonitor);
            paramProgressMonitor.endProgressMonitorSuccess();
            return;
          }
          catch (ZipException localZipException) {}
        }
      }.start();
      return null;
    }
    paramZipModel = initRemoveZipFile(paramZipModel, paramFileHeader, paramProgressMonitor);
    paramProgressMonitor.endProgressMonitorSuccess();
    return paramZipModel;
  }
  
  /* Error */
  public void setComment(ZipModel paramZipModel, String paramString)
    throws ZipException
  {
    // Byte code:
    //   0: aload_2
    //   1: ifnonnull +14 -> 15
    //   4: new 17	net/lingala/zip4j/exception/ZipException
    //   7: dup
    //   8: ldc_w 496
    //   11: invokespecial 101	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   14: athrow
    //   15: aload_1
    //   16: ifnonnull +14 -> 30
    //   19: new 17	net/lingala/zip4j/exception/ZipException
    //   22: dup
    //   23: ldc_w 498
    //   26: invokespecial 101	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   29: athrow
    //   30: aload_2
    //   31: astore 5
    //   33: aload_2
    //   34: invokevirtual 502	java/lang/String:getBytes	()[B
    //   37: astore 4
    //   39: aload_2
    //   40: invokevirtual 504	java/lang/String:length	()I
    //   43: istore_3
    //   44: ldc_w 506
    //   47: invokestatic 509	net/lingala/zip4j/util/Zip4jUtil:isSupportedCharset	(Ljava/lang/String;)Z
    //   50: ifeq +38 -> 88
    //   53: new 62	java/lang/String
    //   56: dup
    //   57: aload_2
    //   58: ldc_w 506
    //   61: invokevirtual 512	java/lang/String:getBytes	(Ljava/lang/String;)[B
    //   64: ldc_w 506
    //   67: invokespecial 515	java/lang/String:<init>	([BLjava/lang/String;)V
    //   70: astore 5
    //   72: aload 5
    //   74: ldc_w 506
    //   77: invokevirtual 512	java/lang/String:getBytes	(Ljava/lang/String;)[B
    //   80: astore 4
    //   82: aload 5
    //   84: invokevirtual 504	java/lang/String:length	()I
    //   87: istore_3
    //   88: iload_3
    //   89: ldc_w 516
    //   92: if_icmple +33 -> 125
    //   95: new 17	net/lingala/zip4j/exception/ZipException
    //   98: dup
    //   99: ldc_w 518
    //   102: invokespecial 101	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   105: athrow
    //   106: astore 4
    //   108: aload_2
    //   109: astore 5
    //   111: aload_2
    //   112: invokevirtual 502	java/lang/String:getBytes	()[B
    //   115: astore 4
    //   117: aload_2
    //   118: invokevirtual 504	java/lang/String:length	()I
    //   121: istore_3
    //   122: goto -34 -> 88
    //   125: aload_1
    //   126: invokevirtual 34	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   129: aload 5
    //   131: invokevirtual 520	net/lingala/zip4j/model/EndCentralDirRecord:setComment	(Ljava/lang/String;)V
    //   134: aload_1
    //   135: invokevirtual 34	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   138: aload 4
    //   140: invokevirtual 524	net/lingala/zip4j/model/EndCentralDirRecord:setCommentBytes	([B)V
    //   143: aload_1
    //   144: invokevirtual 34	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   147: iload_3
    //   148: invokevirtual 527	net/lingala/zip4j/model/EndCentralDirRecord:setCommentLength	(I)V
    //   151: aconst_null
    //   152: astore 4
    //   154: aconst_null
    //   155: astore 6
    //   157: aconst_null
    //   158: astore 5
    //   160: aload 4
    //   162: astore_2
    //   163: new 245	net/lingala/zip4j/core/HeaderWriter
    //   166: dup
    //   167: invokespecial 246	net/lingala/zip4j/core/HeaderWriter:<init>	()V
    //   170: astore 7
    //   172: aload 4
    //   174: astore_2
    //   175: new 423	net/lingala/zip4j/io/SplitOutputStream
    //   178: dup
    //   179: aload_1
    //   180: invokevirtual 44	net/lingala/zip4j/model/ZipModel:getZipFile	()Ljava/lang/String;
    //   183: invokespecial 528	net/lingala/zip4j/io/SplitOutputStream:<init>	(Ljava/lang/String;)V
    //   186: astore 4
    //   188: aload_1
    //   189: invokevirtual 363	net/lingala/zip4j/model/ZipModel:isZip64Format	()Z
    //   192: ifeq +34 -> 226
    //   195: aload 4
    //   197: aload_1
    //   198: invokevirtual 335	net/lingala/zip4j/model/ZipModel:getZip64EndCentralDirRecord	()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
    //   201: invokevirtual 347	net/lingala/zip4j/model/Zip64EndCentralDirRecord:getOffsetStartCenDirWRTStartDiskNo	()J
    //   204: invokevirtual 529	net/lingala/zip4j/io/SplitOutputStream:seek	(J)V
    //   207: aload 7
    //   209: aload_1
    //   210: aload 4
    //   212: invokevirtual 250	net/lingala/zip4j/core/HeaderWriter:finalizeZipFileWithoutValidations	(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;)V
    //   215: aload 4
    //   217: ifnull +8 -> 225
    //   220: aload 4
    //   222: invokevirtual 530	net/lingala/zip4j/io/SplitOutputStream:close	()V
    //   225: return
    //   226: aload 4
    //   228: aload_1
    //   229: invokevirtual 34	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   232: invokevirtual 223	net/lingala/zip4j/model/EndCentralDirRecord:getOffsetOfStartOfCentralDir	()J
    //   235: invokevirtual 529	net/lingala/zip4j/io/SplitOutputStream:seek	(J)V
    //   238: goto -31 -> 207
    //   241: astore_1
    //   242: aload 4
    //   244: astore_2
    //   245: new 17	net/lingala/zip4j/exception/ZipException
    //   248: dup
    //   249: aload_1
    //   250: invokespecial 143	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   253: athrow
    //   254: astore_1
    //   255: aload_2
    //   256: ifnull +7 -> 263
    //   259: aload_2
    //   260: invokevirtual 530	net/lingala/zip4j/io/SplitOutputStream:close	()V
    //   263: aload_1
    //   264: athrow
    //   265: astore_1
    //   266: aload 6
    //   268: astore_2
    //   269: new 17	net/lingala/zip4j/exception/ZipException
    //   272: dup
    //   273: aload_1
    //   274: invokespecial 143	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   277: athrow
    //   278: astore_1
    //   279: return
    //   280: astore_2
    //   281: goto -18 -> 263
    //   284: astore_1
    //   285: aload 4
    //   287: astore_2
    //   288: goto -33 -> 255
    //   291: astore_1
    //   292: aload 4
    //   294: astore_2
    //   295: goto -26 -> 269
    //   298: astore_1
    //   299: aload 5
    //   301: astore_2
    //   302: goto -57 -> 245
    //   305: astore 4
    //   307: goto -199 -> 108
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	310	0	this	ArchiveMaintainer
    //   0	310	1	paramZipModel	ZipModel
    //   0	310	2	paramString	String
    //   43	105	3	i	int
    //   37	44	4	arrayOfByte	byte[]
    //   106	1	4	localUnsupportedEncodingException1	java.io.UnsupportedEncodingException
    //   115	178	4	localObject1	Object
    //   305	1	4	localUnsupportedEncodingException2	java.io.UnsupportedEncodingException
    //   31	269	5	str	String
    //   155	112	6	localObject2	Object
    //   170	38	7	localHeaderWriter	net.lingala.zip4j.core.HeaderWriter
    // Exception table:
    //   from	to	target	type
    //   53	72	106	java/io/UnsupportedEncodingException
    //   188	207	241	java/io/FileNotFoundException
    //   207	215	241	java/io/FileNotFoundException
    //   226	238	241	java/io/FileNotFoundException
    //   163	172	254	finally
    //   175	188	254	finally
    //   245	254	254	finally
    //   269	278	254	finally
    //   163	172	265	java/io/IOException
    //   175	188	265	java/io/IOException
    //   220	225	278	java/io/IOException
    //   259	263	280	java/io/IOException
    //   188	207	284	finally
    //   207	215	284	finally
    //   226	238	284	finally
    //   188	207	291	java/io/IOException
    //   207	215	291	java/io/IOException
    //   226	238	291	java/io/IOException
    //   163	172	298	java/io/FileNotFoundException
    //   175	188	298	java/io/FileNotFoundException
    //   72	88	305	java/io/UnsupportedEncodingException
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/util/ArchiveMaintainer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */