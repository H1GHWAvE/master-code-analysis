package net.lingala.zip4j.zip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.CentralDirectory;
import net.lingala.zip4j.model.EndCentralDirRecord;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.progress.ProgressMonitor;
import net.lingala.zip4j.util.Zip4jUtil;

public class ZipEngine
{
  private ZipModel zipModel;
  
  public ZipEngine(ZipModel paramZipModel)
    throws ZipException
  {
    if (paramZipModel == null) {
      throw new ZipException("zip model is null in ZipEngine constructor");
    }
    this.zipModel = paramZipModel;
  }
  
  private long calculateTotalWork(ArrayList paramArrayList, ZipParameters paramZipParameters)
    throws ZipException
  {
    if (paramArrayList == null) {
      throw new ZipException("file list is null, cannot calculate total work");
    }
    long l2 = 0L;
    int i = 0;
    if (i < paramArrayList.size())
    {
      long l1 = l2;
      if ((paramArrayList.get(i) instanceof File))
      {
        l1 = l2;
        if (((File)paramArrayList.get(i)).exists()) {
          if ((!paramZipParameters.isEncryptFiles()) || (paramZipParameters.getEncryptionMethod() != 0)) {
            break label228;
          }
        }
      }
      label228:
      for (l2 += Zip4jUtil.getFileLengh((File)paramArrayList.get(i)) * 2L;; l2 += Zip4jUtil.getFileLengh((File)paramArrayList.get(i)))
      {
        l1 = l2;
        if (this.zipModel.getCentralDirectory() != null)
        {
          l1 = l2;
          if (this.zipModel.getCentralDirectory().getFileHeaders() != null)
          {
            l1 = l2;
            if (this.zipModel.getCentralDirectory().getFileHeaders().size() > 0)
            {
              Object localObject = Zip4jUtil.getRelativeFileName(((File)paramArrayList.get(i)).getAbsolutePath(), paramZipParameters.getRootFolderInZip(), paramZipParameters.getDefaultFolderPath());
              localObject = Zip4jUtil.getFileHeader(this.zipModel, (String)localObject);
              l1 = l2;
              if (localObject != null) {
                l1 = l2 + (Zip4jUtil.getFileLengh(new File(this.zipModel.getZipFile())) - ((FileHeader)localObject).getCompressedSize());
              }
            }
          }
        }
        i += 1;
        l2 = l1;
        break;
      }
    }
    return l2;
  }
  
  private void checkParameters(ZipParameters paramZipParameters)
    throws ZipException
  {
    if (paramZipParameters == null) {
      throw new ZipException("cannot validate zip parameters");
    }
    if ((paramZipParameters.getCompressionMethod() != 0) && (paramZipParameters.getCompressionMethod() != 8)) {
      throw new ZipException("unsupported compression type");
    }
    if ((paramZipParameters.getCompressionMethod() == 8) && (paramZipParameters.getCompressionLevel() < 0) && (paramZipParameters.getCompressionLevel() > 9)) {
      throw new ZipException("invalid compression level. compression level dor deflate should be in the range of 0-9");
    }
    if (paramZipParameters.isEncryptFiles())
    {
      if ((paramZipParameters.getEncryptionMethod() != 0) && (paramZipParameters.getEncryptionMethod() != 99)) {
        throw new ZipException("unsupported encryption method");
      }
      if ((paramZipParameters.getPassword() == null) || (paramZipParameters.getPassword().length <= 0)) {
        throw new ZipException("input password is empty or null");
      }
    }
    else
    {
      paramZipParameters.setAesKeyStrength(-1);
      paramZipParameters.setEncryptionMethod(-1);
    }
  }
  
  private EndCentralDirRecord createEndOfCentralDirectoryRecord()
  {
    EndCentralDirRecord localEndCentralDirRecord = new EndCentralDirRecord();
    localEndCentralDirRecord.setSignature(101010256L);
    localEndCentralDirRecord.setNoOfThisDisk(0);
    localEndCentralDirRecord.setTotNoOfEntriesInCentralDir(0);
    localEndCentralDirRecord.setTotNoOfEntriesInCentralDirOnThisDisk(0);
    localEndCentralDirRecord.setOffsetOfStartOfCentralDir(0L);
    return localEndCentralDirRecord;
  }
  
  /* Error */
  private void initAddFiles(ArrayList paramArrayList, ZipParameters paramZipParameters, ProgressMonitor paramProgressMonitor)
    throws ZipException
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnull +7 -> 8
    //   4: aload_2
    //   5: ifnonnull +13 -> 18
    //   8: new 12	net/lingala/zip4j/exception/ZipException
    //   11: dup
    //   12: ldc -92
    //   14: invokespecial 20	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   17: athrow
    //   18: aload_1
    //   19: invokevirtual 40	java/util/ArrayList:size	()I
    //   22: ifgt +13 -> 35
    //   25: new 12	net/lingala/zip4j/exception/ZipException
    //   28: dup
    //   29: ldc -90
    //   31: invokespecial 20	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   34: athrow
    //   35: aload_0
    //   36: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   39: invokevirtual 169	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   42: ifnonnull +14 -> 56
    //   45: aload_0
    //   46: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   49: aload_0
    //   50: invokespecial 171	net/lingala/zip4j/zip/ZipEngine:createEndOfCentralDirectoryRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   53: invokevirtual 175	net/lingala/zip4j/model/ZipModel:setEndCentralDirRecord	(Lnet/lingala/zip4j/model/EndCentralDirRecord;)V
    //   56: aconst_null
    //   57: astore 8
    //   59: aconst_null
    //   60: astore 11
    //   62: aconst_null
    //   63: astore 10
    //   65: aconst_null
    //   66: astore 15
    //   68: aconst_null
    //   69: astore 17
    //   71: aconst_null
    //   72: astore 13
    //   74: aconst_null
    //   75: astore 16
    //   77: aconst_null
    //   78: astore 14
    //   80: aconst_null
    //   81: astore 12
    //   83: aload 17
    //   85: astore 7
    //   87: aload 8
    //   89: astore 9
    //   91: aload_0
    //   92: aload_2
    //   93: invokespecial 177	net/lingala/zip4j/zip/ZipEngine:checkParameters	(Lnet/lingala/zip4j/model/ZipParameters;)V
    //   96: aload 17
    //   98: astore 7
    //   100: aload 8
    //   102: astore 9
    //   104: aload_0
    //   105: aload_1
    //   106: aload_2
    //   107: aload_3
    //   108: invokespecial 180	net/lingala/zip4j/zip/ZipEngine:removeFilesIfExists	(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
    //   111: aload 17
    //   113: astore 7
    //   115: aload 8
    //   117: astore 9
    //   119: aload_0
    //   120: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   123: invokevirtual 99	net/lingala/zip4j/model/ZipModel:getZipFile	()Ljava/lang/String;
    //   126: invokestatic 184	net/lingala/zip4j/util/Zip4jUtil:checkFileExists	(Ljava/lang/String;)Z
    //   129: istore 6
    //   131: aload 17
    //   133: astore 7
    //   135: aload 8
    //   137: astore 9
    //   139: new 186	net/lingala/zip4j/io/SplitOutputStream
    //   142: dup
    //   143: new 46	java/io/File
    //   146: dup
    //   147: aload_0
    //   148: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   151: invokevirtual 99	net/lingala/zip4j/model/ZipModel:getZipFile	()Ljava/lang/String;
    //   154: invokespecial 100	java/io/File:<init>	(Ljava/lang/String;)V
    //   157: aload_0
    //   158: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   161: invokevirtual 189	net/lingala/zip4j/model/ZipModel:getSplitLength	()J
    //   164: invokespecial 192	net/lingala/zip4j/io/SplitOutputStream:<init>	(Ljava/io/File;J)V
    //   167: astore 18
    //   169: aload 17
    //   171: astore 7
    //   173: aload 8
    //   175: astore 9
    //   177: new 194	net/lingala/zip4j/io/ZipOutputStream
    //   180: dup
    //   181: aload 18
    //   183: aload_0
    //   184: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   187: invokespecial 197	net/lingala/zip4j/io/ZipOutputStream:<init>	(Ljava/io/OutputStream;Lnet/lingala/zip4j/model/ZipModel;)V
    //   190: astore 8
    //   192: iload 6
    //   194: ifeq +130 -> 324
    //   197: aload 12
    //   199: astore 7
    //   201: aload 13
    //   203: astore 10
    //   205: aload 14
    //   207: astore 11
    //   209: aload_0
    //   210: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   213: invokevirtual 169	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   216: ifnonnull +81 -> 297
    //   219: aload 12
    //   221: astore 7
    //   223: aload 13
    //   225: astore 10
    //   227: aload 14
    //   229: astore 11
    //   231: new 12	net/lingala/zip4j/exception/ZipException
    //   234: dup
    //   235: ldc -57
    //   237: invokespecial 20	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   240: athrow
    //   241: astore 9
    //   243: aload 8
    //   245: astore_2
    //   246: aload 7
    //   248: astore_1
    //   249: aload 9
    //   251: astore 8
    //   253: aload_1
    //   254: astore 7
    //   256: aload_2
    //   257: astore 9
    //   259: aload_3
    //   260: aload 8
    //   262: invokevirtual 205	net/lingala/zip4j/progress/ProgressMonitor:endProgressMonitorError	(Ljava/lang/Throwable;)V
    //   265: aload_1
    //   266: astore 7
    //   268: aload_2
    //   269: astore 9
    //   271: aload 8
    //   273: athrow
    //   274: astore_1
    //   275: aload 7
    //   277: ifnull +8 -> 285
    //   280: aload 7
    //   282: invokevirtual 210	java/io/InputStream:close	()V
    //   285: aload 9
    //   287: ifnull +8 -> 295
    //   290: aload 9
    //   292: invokevirtual 211	net/lingala/zip4j/io/ZipOutputStream:close	()V
    //   295: aload_1
    //   296: athrow
    //   297: aload 12
    //   299: astore 7
    //   301: aload 13
    //   303: astore 10
    //   305: aload 14
    //   307: astore 11
    //   309: aload 18
    //   311: aload_0
    //   312: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   315: invokevirtual 169	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   318: invokevirtual 214	net/lingala/zip4j/model/EndCentralDirRecord:getOffsetOfStartOfCentralDir	()J
    //   321: invokevirtual 217	net/lingala/zip4j/io/SplitOutputStream:seek	(J)V
    //   324: aload 12
    //   326: astore 7
    //   328: aload 13
    //   330: astore 10
    //   332: aload 14
    //   334: astore 11
    //   336: sipush 4096
    //   339: newarray <illegal type>
    //   341: astore 12
    //   343: iconst_0
    //   344: istore 4
    //   346: aconst_null
    //   347: astore 7
    //   349: iload 4
    //   351: aload_1
    //   352: invokevirtual 40	java/util/ArrayList:size	()I
    //   355: if_icmpge +544 -> 899
    //   358: aload_3
    //   359: invokevirtual 220	net/lingala/zip4j/progress/ProgressMonitor:isCancelAllTasks	()Z
    //   362: ifeq +34 -> 396
    //   365: aload_3
    //   366: iconst_3
    //   367: invokevirtual 223	net/lingala/zip4j/progress/ProgressMonitor:setResult	(I)V
    //   370: aload_3
    //   371: iconst_0
    //   372: invokevirtual 226	net/lingala/zip4j/progress/ProgressMonitor:setState	(I)V
    //   375: aload 7
    //   377: ifnull +8 -> 385
    //   380: aload 7
    //   382: invokevirtual 210	java/io/InputStream:close	()V
    //   385: aload 8
    //   387: ifnull +8 -> 395
    //   390: aload 8
    //   392: invokevirtual 211	net/lingala/zip4j/io/ZipOutputStream:close	()V
    //   395: return
    //   396: aload_2
    //   397: invokevirtual 230	net/lingala/zip4j/model/ZipParameters:clone	()Ljava/lang/Object;
    //   400: checkcast 52	net/lingala/zip4j/model/ZipParameters
    //   403: astore 9
    //   405: aload_3
    //   406: aload_1
    //   407: iload 4
    //   409: invokevirtual 44	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   412: checkcast 46	java/io/File
    //   415: invokevirtual 82	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   418: invokevirtual 233	net/lingala/zip4j/progress/ProgressMonitor:setFileName	(Ljava/lang/String;)V
    //   421: aload_1
    //   422: iload 4
    //   424: invokevirtual 44	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   427: checkcast 46	java/io/File
    //   430: invokevirtual 236	java/io/File:isDirectory	()Z
    //   433: ifne +112 -> 545
    //   436: aload 9
    //   438: invokevirtual 55	net/lingala/zip4j/model/ZipParameters:isEncryptFiles	()Z
    //   441: ifeq +81 -> 522
    //   444: aload 9
    //   446: invokevirtual 58	net/lingala/zip4j/model/ZipParameters:getEncryptionMethod	()I
    //   449: ifne +73 -> 522
    //   452: aload_3
    //   453: iconst_3
    //   454: invokevirtual 239	net/lingala/zip4j/progress/ProgressMonitor:setCurrentOperation	(I)V
    //   457: aload 9
    //   459: aload_1
    //   460: iload 4
    //   462: invokevirtual 44	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   465: checkcast 46	java/io/File
    //   468: invokevirtual 82	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   471: aload_3
    //   472: invokestatic 245	net/lingala/zip4j/util/CRCUtil:computeFileCRC	(Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;)J
    //   475: l2i
    //   476: invokevirtual 248	net/lingala/zip4j/model/ZipParameters:setSourceFileCRC	(I)V
    //   479: aload_3
    //   480: iconst_0
    //   481: invokevirtual 239	net/lingala/zip4j/progress/ProgressMonitor:setCurrentOperation	(I)V
    //   484: aload_3
    //   485: invokevirtual 220	net/lingala/zip4j/progress/ProgressMonitor:isCancelAllTasks	()Z
    //   488: ifeq +34 -> 522
    //   491: aload_3
    //   492: iconst_3
    //   493: invokevirtual 223	net/lingala/zip4j/progress/ProgressMonitor:setResult	(I)V
    //   496: aload_3
    //   497: iconst_0
    //   498: invokevirtual 226	net/lingala/zip4j/progress/ProgressMonitor:setState	(I)V
    //   501: aload 7
    //   503: ifnull +8 -> 511
    //   506: aload 7
    //   508: invokevirtual 210	java/io/InputStream:close	()V
    //   511: aload 8
    //   513: ifnull +8 -> 521
    //   516: aload 8
    //   518: invokevirtual 211	net/lingala/zip4j/io/ZipOutputStream:close	()V
    //   521: return
    //   522: aload_1
    //   523: iload 4
    //   525: invokevirtual 44	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   528: checkcast 46	java/io/File
    //   531: invokestatic 64	net/lingala/zip4j/util/Zip4jUtil:getFileLengh	(Ljava/io/File;)J
    //   534: lconst_0
    //   535: lcmp
    //   536: ifne +9 -> 545
    //   539: aload 9
    //   541: iconst_0
    //   542: invokevirtual 251	net/lingala/zip4j/model/ZipParameters:setCompressionMethod	(I)V
    //   545: aload_1
    //   546: iload 4
    //   548: invokevirtual 44	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   551: checkcast 46	java/io/File
    //   554: astore 10
    //   556: aload 10
    //   558: invokevirtual 254	java/io/File:isFile	()Z
    //   561: ifne +14 -> 575
    //   564: aload 10
    //   566: invokevirtual 236	java/io/File:isDirectory	()Z
    //   569: ifne +6 -> 575
    //   572: goto +453 -> 1025
    //   575: aload 8
    //   577: aload_1
    //   578: iload 4
    //   580: invokevirtual 44	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   583: checkcast 46	java/io/File
    //   586: aload 9
    //   588: invokevirtual 258	net/lingala/zip4j/io/ZipOutputStream:putNextEntry	(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    //   591: aload_1
    //   592: iload 4
    //   594: invokevirtual 44	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   597: checkcast 46	java/io/File
    //   600: invokevirtual 236	java/io/File:isDirectory	()Z
    //   603: ifeq +11 -> 614
    //   606: aload 8
    //   608: invokevirtual 261	net/lingala/zip4j/io/ZipOutputStream:closeEntry	()V
    //   611: goto +414 -> 1025
    //   614: getstatic 267	java/lang/System:out	Ljava/io/PrintStream;
    //   617: aload_1
    //   618: iload 4
    //   620: invokevirtual 44	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   623: checkcast 46	java/io/File
    //   626: invokevirtual 82	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   629: invokevirtual 272	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   632: new 274	java/io/FileInputStream
    //   635: dup
    //   636: aload_1
    //   637: iload 4
    //   639: invokevirtual 44	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   642: checkcast 46	java/io/File
    //   645: invokespecial 277	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   648: astore 9
    //   650: aload 9
    //   652: astore 7
    //   654: aload 9
    //   656: astore 10
    //   658: aload 9
    //   660: astore 11
    //   662: aload 9
    //   664: aload 12
    //   666: invokevirtual 281	java/io/InputStream:read	([B)I
    //   669: istore 5
    //   671: iload 5
    //   673: iconst_m1
    //   674: if_icmpeq +163 -> 837
    //   677: aload 9
    //   679: astore 7
    //   681: aload 9
    //   683: astore 10
    //   685: aload 9
    //   687: astore 11
    //   689: aload_3
    //   690: invokevirtual 220	net/lingala/zip4j/progress/ProgressMonitor:isCancelAllTasks	()Z
    //   693: ifeq +60 -> 753
    //   696: aload 9
    //   698: astore 7
    //   700: aload 9
    //   702: astore 10
    //   704: aload 9
    //   706: astore 11
    //   708: aload_3
    //   709: iconst_3
    //   710: invokevirtual 223	net/lingala/zip4j/progress/ProgressMonitor:setResult	(I)V
    //   713: aload 9
    //   715: astore 7
    //   717: aload 9
    //   719: astore 10
    //   721: aload 9
    //   723: astore 11
    //   725: aload_3
    //   726: iconst_0
    //   727: invokevirtual 226	net/lingala/zip4j/progress/ProgressMonitor:setState	(I)V
    //   730: aload 9
    //   732: ifnull +8 -> 740
    //   735: aload 9
    //   737: invokevirtual 210	java/io/InputStream:close	()V
    //   740: aload 8
    //   742: ifnull -347 -> 395
    //   745: aload 8
    //   747: invokevirtual 211	net/lingala/zip4j/io/ZipOutputStream:close	()V
    //   750: return
    //   751: astore_1
    //   752: return
    //   753: aload 9
    //   755: astore 7
    //   757: aload 9
    //   759: astore 10
    //   761: aload 9
    //   763: astore 11
    //   765: aload 8
    //   767: aload 12
    //   769: iconst_0
    //   770: iload 5
    //   772: invokevirtual 285	net/lingala/zip4j/io/ZipOutputStream:write	([BII)V
    //   775: aload 9
    //   777: astore 7
    //   779: aload 9
    //   781: astore 10
    //   783: aload 9
    //   785: astore 11
    //   787: aload_3
    //   788: iload 5
    //   790: i2l
    //   791: invokevirtual 288	net/lingala/zip4j/progress/ProgressMonitor:updateWorkCompleted	(J)V
    //   794: goto -144 -> 650
    //   797: astore 7
    //   799: aload 8
    //   801: astore_2
    //   802: aload 10
    //   804: astore_1
    //   805: aload 7
    //   807: astore 8
    //   809: aload_1
    //   810: astore 7
    //   812: aload_2
    //   813: astore 9
    //   815: aload_3
    //   816: aload 8
    //   818: invokevirtual 205	net/lingala/zip4j/progress/ProgressMonitor:endProgressMonitorError	(Ljava/lang/Throwable;)V
    //   821: aload_1
    //   822: astore 7
    //   824: aload_2
    //   825: astore 9
    //   827: new 12	net/lingala/zip4j/exception/ZipException
    //   830: dup
    //   831: aload 8
    //   833: invokespecial 290	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   836: athrow
    //   837: aload 9
    //   839: astore 7
    //   841: aload 9
    //   843: astore 10
    //   845: aload 9
    //   847: astore 11
    //   849: aload 8
    //   851: invokevirtual 261	net/lingala/zip4j/io/ZipOutputStream:closeEntry	()V
    //   854: aload 9
    //   856: astore 7
    //   858: aload 9
    //   860: ifnull +165 -> 1025
    //   863: aload 9
    //   865: astore 7
    //   867: aload 9
    //   869: astore 10
    //   871: aload 9
    //   873: astore 11
    //   875: aload 9
    //   877: invokevirtual 210	java/io/InputStream:close	()V
    //   880: aload 9
    //   882: astore 7
    //   884: goto +141 -> 1025
    //   887: astore_1
    //   888: aload 11
    //   890: astore 7
    //   892: aload 8
    //   894: astore 9
    //   896: goto -621 -> 275
    //   899: aload 8
    //   901: invokevirtual 293	net/lingala/zip4j/io/ZipOutputStream:finish	()V
    //   904: aload_3
    //   905: invokevirtual 296	net/lingala/zip4j/progress/ProgressMonitor:endProgressMonitorSuccess	()V
    //   908: aload 7
    //   910: ifnull +8 -> 918
    //   913: aload 7
    //   915: invokevirtual 210	java/io/InputStream:close	()V
    //   918: aload 8
    //   920: ifnull +8 -> 928
    //   923: aload 8
    //   925: invokevirtual 211	net/lingala/zip4j/io/ZipOutputStream:close	()V
    //   928: return
    //   929: astore_1
    //   930: goto -545 -> 385
    //   933: astore_1
    //   934: goto -539 -> 395
    //   937: astore_1
    //   938: goto -427 -> 511
    //   941: astore_1
    //   942: goto -421 -> 521
    //   945: astore_1
    //   946: goto -206 -> 740
    //   949: astore_1
    //   950: goto -32 -> 918
    //   953: astore_1
    //   954: goto -26 -> 928
    //   957: astore_2
    //   958: goto -673 -> 285
    //   961: astore_2
    //   962: goto -667 -> 295
    //   965: astore_1
    //   966: aload 8
    //   968: astore 9
    //   970: goto -695 -> 275
    //   973: astore 8
    //   975: aload 16
    //   977: astore_1
    //   978: aload 11
    //   980: astore_2
    //   981: goto -172 -> 809
    //   984: astore 9
    //   986: aload 7
    //   988: astore_1
    //   989: aload 8
    //   991: astore_2
    //   992: aload 9
    //   994: astore 8
    //   996: goto -187 -> 809
    //   999: astore 8
    //   1001: aload 15
    //   1003: astore_1
    //   1004: aload 10
    //   1006: astore_2
    //   1007: goto -754 -> 253
    //   1010: astore 9
    //   1012: aload 7
    //   1014: astore_1
    //   1015: aload 8
    //   1017: astore_2
    //   1018: aload 9
    //   1020: astore 8
    //   1022: goto -769 -> 253
    //   1025: iload 4
    //   1027: iconst_1
    //   1028: iadd
    //   1029: istore 4
    //   1031: goto -682 -> 349
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1034	0	this	ZipEngine
    //   0	1034	1	paramArrayList	ArrayList
    //   0	1034	2	paramZipParameters	ZipParameters
    //   0	1034	3	paramProgressMonitor	ProgressMonitor
    //   344	686	4	i	int
    //   669	120	5	j	int
    //   129	64	6	bool	boolean
    //   85	693	7	localObject1	Object
    //   797	9	7	localException1	Exception
    //   810	203	7	localObject2	Object
    //   57	910	8	localObject3	Object
    //   973	17	8	localException2	Exception
    //   994	1	8	localObject4	Object
    //   999	17	8	localZipException1	ZipException
    //   1020	1	8	localObject5	Object
    //   89	87	9	localObject6	Object
    //   241	9	9	localZipException2	ZipException
    //   257	712	9	localObject7	Object
    //   984	9	9	localException3	Exception
    //   1010	9	9	localZipException3	ZipException
    //   63	942	10	localObject8	Object
    //   60	919	11	localObject9	Object
    //   81	687	12	arrayOfByte	byte[]
    //   72	257	13	localObject10	Object
    //   78	255	14	localObject11	Object
    //   66	936	15	localObject12	Object
    //   75	901	16	localObject13	Object
    //   69	101	17	localObject14	Object
    //   167	143	18	localSplitOutputStream	net.lingala.zip4j.io.SplitOutputStream
    // Exception table:
    //   from	to	target	type
    //   209	219	241	net/lingala/zip4j/exception/ZipException
    //   231	241	241	net/lingala/zip4j/exception/ZipException
    //   309	324	241	net/lingala/zip4j/exception/ZipException
    //   336	343	241	net/lingala/zip4j/exception/ZipException
    //   662	671	241	net/lingala/zip4j/exception/ZipException
    //   689	696	241	net/lingala/zip4j/exception/ZipException
    //   708	713	241	net/lingala/zip4j/exception/ZipException
    //   725	730	241	net/lingala/zip4j/exception/ZipException
    //   765	775	241	net/lingala/zip4j/exception/ZipException
    //   787	794	241	net/lingala/zip4j/exception/ZipException
    //   849	854	241	net/lingala/zip4j/exception/ZipException
    //   875	880	241	net/lingala/zip4j/exception/ZipException
    //   91	96	274	finally
    //   104	111	274	finally
    //   119	131	274	finally
    //   139	169	274	finally
    //   177	192	274	finally
    //   259	265	274	finally
    //   271	274	274	finally
    //   815	821	274	finally
    //   827	837	274	finally
    //   745	750	751	java/io/IOException
    //   209	219	797	java/lang/Exception
    //   231	241	797	java/lang/Exception
    //   309	324	797	java/lang/Exception
    //   336	343	797	java/lang/Exception
    //   662	671	797	java/lang/Exception
    //   689	696	797	java/lang/Exception
    //   708	713	797	java/lang/Exception
    //   725	730	797	java/lang/Exception
    //   765	775	797	java/lang/Exception
    //   787	794	797	java/lang/Exception
    //   849	854	797	java/lang/Exception
    //   875	880	797	java/lang/Exception
    //   209	219	887	finally
    //   231	241	887	finally
    //   309	324	887	finally
    //   336	343	887	finally
    //   662	671	887	finally
    //   689	696	887	finally
    //   708	713	887	finally
    //   725	730	887	finally
    //   765	775	887	finally
    //   787	794	887	finally
    //   849	854	887	finally
    //   875	880	887	finally
    //   380	385	929	java/io/IOException
    //   390	395	933	java/io/IOException
    //   506	511	937	java/io/IOException
    //   516	521	941	java/io/IOException
    //   735	740	945	java/io/IOException
    //   913	918	949	java/io/IOException
    //   923	928	953	java/io/IOException
    //   280	285	957	java/io/IOException
    //   290	295	961	java/io/IOException
    //   349	375	965	finally
    //   396	501	965	finally
    //   522	545	965	finally
    //   545	572	965	finally
    //   575	611	965	finally
    //   614	650	965	finally
    //   899	908	965	finally
    //   91	96	973	java/lang/Exception
    //   104	111	973	java/lang/Exception
    //   119	131	973	java/lang/Exception
    //   139	169	973	java/lang/Exception
    //   177	192	973	java/lang/Exception
    //   349	375	984	java/lang/Exception
    //   396	501	984	java/lang/Exception
    //   522	545	984	java/lang/Exception
    //   545	572	984	java/lang/Exception
    //   575	611	984	java/lang/Exception
    //   614	650	984	java/lang/Exception
    //   899	908	984	java/lang/Exception
    //   91	96	999	net/lingala/zip4j/exception/ZipException
    //   104	111	999	net/lingala/zip4j/exception/ZipException
    //   119	131	999	net/lingala/zip4j/exception/ZipException
    //   139	169	999	net/lingala/zip4j/exception/ZipException
    //   177	192	999	net/lingala/zip4j/exception/ZipException
    //   349	375	1010	net/lingala/zip4j/exception/ZipException
    //   396	501	1010	net/lingala/zip4j/exception/ZipException
    //   522	545	1010	net/lingala/zip4j/exception/ZipException
    //   545	572	1010	net/lingala/zip4j/exception/ZipException
    //   575	611	1010	net/lingala/zip4j/exception/ZipException
    //   614	650	1010	net/lingala/zip4j/exception/ZipException
    //   899	908	1010	net/lingala/zip4j/exception/ZipException
  }
  
  private RandomAccessFile prepareFileOutputStream()
    throws ZipException
  {
    Object localObject = this.zipModel.getZipFile();
    if (!Zip4jUtil.isStringNotNullAndNotEmpty((String)localObject)) {
      throw new ZipException("invalid output path");
    }
    try
    {
      localObject = new File((String)localObject);
      if (!((File)localObject).getParentFile().exists()) {
        ((File)localObject).getParentFile().mkdirs();
      }
      localObject = new RandomAccessFile((File)localObject, "rw");
      return (RandomAccessFile)localObject;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      throw new ZipException(localFileNotFoundException);
    }
  }
  
  /* Error */
  private void removeFilesIfExists(ArrayList paramArrayList, ZipParameters paramZipParameters, ProgressMonitor paramProgressMonitor)
    throws ZipException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   4: ifnull +42 -> 46
    //   7: aload_0
    //   8: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   11: invokevirtual 72	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   14: ifnull +32 -> 46
    //   17: aload_0
    //   18: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   21: invokevirtual 72	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   24: invokevirtual 78	net/lingala/zip4j/model/CentralDirectory:getFileHeaders	()Ljava/util/ArrayList;
    //   27: ifnull +19 -> 46
    //   30: aload_0
    //   31: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   34: invokevirtual 72	net/lingala/zip4j/model/ZipModel:getCentralDirectory	()Lnet/lingala/zip4j/model/CentralDirectory;
    //   37: invokevirtual 78	net/lingala/zip4j/model/CentralDirectory:getFileHeaders	()Ljava/util/ArrayList;
    //   40: invokevirtual 40	java/util/ArrayList:size	()I
    //   43: ifgt +4 -> 47
    //   46: return
    //   47: aconst_null
    //   48: astore 8
    //   50: iconst_0
    //   51: istore 4
    //   53: aload 8
    //   55: astore 10
    //   57: aload 8
    //   59: astore 9
    //   61: iload 4
    //   63: aload_1
    //   64: invokevirtual 40	java/util/ArrayList:size	()I
    //   67: if_icmpge +391 -> 458
    //   70: aload 8
    //   72: astore 10
    //   74: aload 8
    //   76: astore 9
    //   78: aload_1
    //   79: iload 4
    //   81: invokevirtual 44	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   84: checkcast 46	java/io/File
    //   87: invokevirtual 82	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   90: aload_2
    //   91: invokevirtual 85	net/lingala/zip4j/model/ZipParameters:getRootFolderInZip	()Ljava/lang/String;
    //   94: aload_2
    //   95: invokevirtual 88	net/lingala/zip4j/model/ZipParameters:getDefaultFolderPath	()Ljava/lang/String;
    //   98: invokestatic 92	net/lingala/zip4j/util/Zip4jUtil:getRelativeFileName	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   101: astore 7
    //   103: aload 8
    //   105: astore 10
    //   107: aload 8
    //   109: astore 9
    //   111: aload_0
    //   112: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   115: aload 7
    //   117: invokestatic 96	net/lingala/zip4j/util/Zip4jUtil:getFileHeader	(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Lnet/lingala/zip4j/model/FileHeader;
    //   120: astore 11
    //   122: aload 8
    //   124: astore 9
    //   126: aload 11
    //   128: ifnull +347 -> 475
    //   131: aload 8
    //   133: astore 7
    //   135: aload 8
    //   137: ifnull +19 -> 156
    //   140: aload 8
    //   142: astore 10
    //   144: aload 8
    //   146: astore 9
    //   148: aload 8
    //   150: invokevirtual 322	java/io/RandomAccessFile:close	()V
    //   153: aconst_null
    //   154: astore 7
    //   156: aload 7
    //   158: astore 10
    //   160: aload 7
    //   162: astore 9
    //   164: new 324	net/lingala/zip4j/util/ArchiveMaintainer
    //   167: dup
    //   168: invokespecial 325	net/lingala/zip4j/util/ArchiveMaintainer:<init>	()V
    //   171: astore 8
    //   173: aload 7
    //   175: astore 10
    //   177: aload 7
    //   179: astore 9
    //   181: aload_3
    //   182: iconst_2
    //   183: invokevirtual 239	net/lingala/zip4j/progress/ProgressMonitor:setCurrentOperation	(I)V
    //   186: aload 7
    //   188: astore 10
    //   190: aload 7
    //   192: astore 9
    //   194: aload 8
    //   196: aload_0
    //   197: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   200: aload 11
    //   202: aload_3
    //   203: invokevirtual 329	net/lingala/zip4j/util/ArchiveMaintainer:initRemoveZipFile	(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;Lnet/lingala/zip4j/progress/ProgressMonitor;)Ljava/util/HashMap;
    //   206: astore 8
    //   208: aload 7
    //   210: astore 10
    //   212: aload 7
    //   214: astore 9
    //   216: aload_3
    //   217: invokevirtual 220	net/lingala/zip4j/progress/ProgressMonitor:isCancelAllTasks	()Z
    //   220: ifeq +42 -> 262
    //   223: aload 7
    //   225: astore 10
    //   227: aload 7
    //   229: astore 9
    //   231: aload_3
    //   232: iconst_3
    //   233: invokevirtual 223	net/lingala/zip4j/progress/ProgressMonitor:setResult	(I)V
    //   236: aload 7
    //   238: astore 10
    //   240: aload 7
    //   242: astore 9
    //   244: aload_3
    //   245: iconst_0
    //   246: invokevirtual 226	net/lingala/zip4j/progress/ProgressMonitor:setState	(I)V
    //   249: aload 7
    //   251: ifnull -205 -> 46
    //   254: aload 7
    //   256: invokevirtual 322	java/io/RandomAccessFile:close	()V
    //   259: return
    //   260: astore_1
    //   261: return
    //   262: aload 7
    //   264: astore 10
    //   266: aload 7
    //   268: astore 9
    //   270: aload_3
    //   271: iconst_0
    //   272: invokevirtual 239	net/lingala/zip4j/progress/ProgressMonitor:setCurrentOperation	(I)V
    //   275: aload 7
    //   277: astore 9
    //   279: aload 7
    //   281: ifnonnull +194 -> 475
    //   284: aload 7
    //   286: astore 10
    //   288: aload 7
    //   290: astore 9
    //   292: aload_0
    //   293: invokespecial 331	net/lingala/zip4j/zip/ZipEngine:prepareFileOutputStream	()Ljava/io/RandomAccessFile;
    //   296: astore 7
    //   298: aload 7
    //   300: astore 9
    //   302: aload 8
    //   304: ifnull +171 -> 475
    //   307: aload 7
    //   309: astore 10
    //   311: aload 7
    //   313: astore 9
    //   315: aload 8
    //   317: ldc_w 333
    //   320: invokevirtual 338	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   323: astore 11
    //   325: aload 7
    //   327: astore 9
    //   329: aload 11
    //   331: ifnull +144 -> 475
    //   334: aload 7
    //   336: astore 10
    //   338: aload 7
    //   340: astore 9
    //   342: aload 8
    //   344: ldc_w 333
    //   347: invokevirtual 338	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   350: checkcast 340	java/lang/String
    //   353: invokestatic 346	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   356: lstore 5
    //   358: aload 7
    //   360: astore 9
    //   362: lload 5
    //   364: lconst_0
    //   365: lcmp
    //   366: iflt +109 -> 475
    //   369: aload 7
    //   371: astore 10
    //   373: aload 7
    //   375: astore 9
    //   377: aload 7
    //   379: lload 5
    //   381: invokevirtual 347	java/io/RandomAccessFile:seek	(J)V
    //   384: aload 7
    //   386: astore 9
    //   388: goto +87 -> 475
    //   391: astore_1
    //   392: aload 7
    //   394: astore 10
    //   396: aload 7
    //   398: astore 9
    //   400: new 12	net/lingala/zip4j/exception/ZipException
    //   403: dup
    //   404: ldc_w 349
    //   407: invokespecial 20	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   410: athrow
    //   411: astore_1
    //   412: aload 10
    //   414: astore 9
    //   416: new 12	net/lingala/zip4j/exception/ZipException
    //   419: dup
    //   420: aload_1
    //   421: invokespecial 290	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   424: athrow
    //   425: astore_1
    //   426: aload 9
    //   428: ifnull +8 -> 436
    //   431: aload 9
    //   433: invokevirtual 322	java/io/RandomAccessFile:close	()V
    //   436: aload_1
    //   437: athrow
    //   438: astore_1
    //   439: aload 7
    //   441: astore 10
    //   443: aload 7
    //   445: astore 9
    //   447: new 12	net/lingala/zip4j/exception/ZipException
    //   450: dup
    //   451: ldc_w 351
    //   454: invokespecial 20	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   457: athrow
    //   458: aload 8
    //   460: ifnull -414 -> 46
    //   463: aload 8
    //   465: invokevirtual 322	java/io/RandomAccessFile:close	()V
    //   468: return
    //   469: astore_1
    //   470: return
    //   471: astore_2
    //   472: goto -36 -> 436
    //   475: iload 4
    //   477: iconst_1
    //   478: iadd
    //   479: istore 4
    //   481: aload 9
    //   483: astore 8
    //   485: goto -432 -> 53
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	488	0	this	ZipEngine
    //   0	488	1	paramArrayList	ArrayList
    //   0	488	2	paramZipParameters	ZipParameters
    //   0	488	3	paramProgressMonitor	ProgressMonitor
    //   51	429	4	i	int
    //   356	24	5	l	long
    //   101	343	7	localObject1	Object
    //   48	436	8	localObject2	Object
    //   59	423	9	localObject3	Object
    //   55	387	10	localObject4	Object
    //   120	210	11	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   254	259	260	java/io/IOException
    //   342	358	391	java/lang/NumberFormatException
    //   61	70	411	java/io/IOException
    //   78	103	411	java/io/IOException
    //   111	122	411	java/io/IOException
    //   148	153	411	java/io/IOException
    //   164	173	411	java/io/IOException
    //   181	186	411	java/io/IOException
    //   194	208	411	java/io/IOException
    //   216	223	411	java/io/IOException
    //   231	236	411	java/io/IOException
    //   244	249	411	java/io/IOException
    //   270	275	411	java/io/IOException
    //   292	298	411	java/io/IOException
    //   315	325	411	java/io/IOException
    //   342	358	411	java/io/IOException
    //   377	384	411	java/io/IOException
    //   400	411	411	java/io/IOException
    //   447	458	411	java/io/IOException
    //   61	70	425	finally
    //   78	103	425	finally
    //   111	122	425	finally
    //   148	153	425	finally
    //   164	173	425	finally
    //   181	186	425	finally
    //   194	208	425	finally
    //   216	223	425	finally
    //   231	236	425	finally
    //   244	249	425	finally
    //   270	275	425	finally
    //   292	298	425	finally
    //   315	325	425	finally
    //   342	358	425	finally
    //   377	384	425	finally
    //   400	411	425	finally
    //   416	425	425	finally
    //   447	458	425	finally
    //   342	358	438	java/lang/Exception
    //   463	468	469	java/io/IOException
    //   431	436	471	java/io/IOException
  }
  
  public void addFiles(final ArrayList paramArrayList, final ZipParameters paramZipParameters, final ProgressMonitor paramProgressMonitor, boolean paramBoolean)
    throws ZipException
  {
    if ((paramArrayList == null) || (paramZipParameters == null)) {
      throw new ZipException("one of the input parameters is null when adding files");
    }
    if (paramArrayList.size() <= 0) {
      throw new ZipException("no files to add");
    }
    paramProgressMonitor.setCurrentOperation(0);
    paramProgressMonitor.setState(1);
    paramProgressMonitor.setResult(1);
    if (paramBoolean)
    {
      paramProgressMonitor.setTotalWork(calculateTotalWork(paramArrayList, paramZipParameters));
      paramProgressMonitor.setFileName(((File)paramArrayList.get(0)).getAbsolutePath());
      new Thread("Zip4j")
      {
        public void run()
        {
          try
          {
            ZipEngine.this.initAddFiles(paramArrayList, paramZipParameters, paramProgressMonitor);
            return;
          }
          catch (ZipException localZipException) {}
        }
      }.start();
      return;
    }
    initAddFiles(paramArrayList, paramZipParameters, paramProgressMonitor);
  }
  
  public void addFolderToZip(File paramFile, ZipParameters paramZipParameters, ProgressMonitor paramProgressMonitor, boolean paramBoolean)
    throws ZipException
  {
    if ((paramFile == null) || (paramZipParameters == null)) {
      throw new ZipException("one of the input parameters is null, cannot add folder to zip");
    }
    if (!Zip4jUtil.checkFileExists(paramFile.getAbsolutePath())) {
      throw new ZipException("input folder does not exist");
    }
    if (!paramFile.isDirectory()) {
      throw new ZipException("input file is not a folder, user addFileToZip method to add files");
    }
    if (!Zip4jUtil.checkFileReadAccess(paramFile.getAbsolutePath())) {
      throw new ZipException("cannot read folder: " + paramFile.getAbsolutePath());
    }
    Object localObject;
    if (paramZipParameters.isIncludeRootFolder()) {
      if (paramFile.getAbsolutePath() != null) {
        if (paramFile.getAbsoluteFile().getParentFile() != null) {
          localObject = paramFile.getAbsoluteFile().getParentFile().getAbsolutePath();
        }
      }
    }
    for (;;)
    {
      paramZipParameters.setDefaultFolderPath((String)localObject);
      ArrayList localArrayList = Zip4jUtil.getFilesInDirectoryRec(paramFile, paramZipParameters.isReadHiddenFiles());
      localObject = localArrayList;
      if (paramZipParameters.isIncludeRootFolder())
      {
        localObject = localArrayList;
        if (localArrayList == null) {
          localObject = new ArrayList();
        }
        ((ArrayList)localObject).add(paramFile);
      }
      addFiles((ArrayList)localObject, paramZipParameters, paramProgressMonitor, paramBoolean);
      return;
      localObject = "";
      continue;
      if (paramFile.getParentFile() != null) {}
      for (localObject = paramFile.getParentFile().getAbsolutePath();; localObject = "") {
        break;
      }
      localObject = paramFile.getAbsolutePath();
    }
  }
  
  /* Error */
  public void addStreamToZip(java.io.InputStream paramInputStream, ZipParameters paramZipParameters)
    throws ZipException
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnull +7 -> 8
    //   4: aload_2
    //   5: ifnonnull +14 -> 19
    //   8: new 12	net/lingala/zip4j/exception/ZipException
    //   11: dup
    //   12: ldc_w 420
    //   15: invokespecial 20	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   18: athrow
    //   19: aconst_null
    //   20: astore 6
    //   22: aconst_null
    //   23: astore 8
    //   25: aconst_null
    //   26: astore 7
    //   28: aload 6
    //   30: astore 5
    //   32: aload_0
    //   33: aload_2
    //   34: invokespecial 177	net/lingala/zip4j/zip/ZipEngine:checkParameters	(Lnet/lingala/zip4j/model/ZipParameters;)V
    //   37: aload 6
    //   39: astore 5
    //   41: aload_0
    //   42: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   45: invokevirtual 99	net/lingala/zip4j/model/ZipModel:getZipFile	()Ljava/lang/String;
    //   48: invokestatic 184	net/lingala/zip4j/util/Zip4jUtil:checkFileExists	(Ljava/lang/String;)Z
    //   51: istore 4
    //   53: aload 6
    //   55: astore 5
    //   57: new 186	net/lingala/zip4j/io/SplitOutputStream
    //   60: dup
    //   61: new 46	java/io/File
    //   64: dup
    //   65: aload_0
    //   66: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   69: invokevirtual 99	net/lingala/zip4j/model/ZipModel:getZipFile	()Ljava/lang/String;
    //   72: invokespecial 100	java/io/File:<init>	(Ljava/lang/String;)V
    //   75: aload_0
    //   76: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   79: invokevirtual 189	net/lingala/zip4j/model/ZipModel:getSplitLength	()J
    //   82: invokespecial 192	net/lingala/zip4j/io/SplitOutputStream:<init>	(Ljava/io/File;J)V
    //   85: astore 9
    //   87: aload 6
    //   89: astore 5
    //   91: new 194	net/lingala/zip4j/io/ZipOutputStream
    //   94: dup
    //   95: aload 9
    //   97: aload_0
    //   98: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   101: invokespecial 197	net/lingala/zip4j/io/ZipOutputStream:<init>	(Ljava/io/OutputStream;Lnet/lingala/zip4j/model/ZipModel;)V
    //   104: astore 6
    //   106: iload 4
    //   108: ifeq +58 -> 166
    //   111: aload_0
    //   112: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   115: invokevirtual 169	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   118: ifnonnull +33 -> 151
    //   121: new 12	net/lingala/zip4j/exception/ZipException
    //   124: dup
    //   125: ldc -57
    //   127: invokespecial 20	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/String;)V
    //   130: athrow
    //   131: astore_1
    //   132: aload 6
    //   134: astore 5
    //   136: aload_1
    //   137: athrow
    //   138: astore_1
    //   139: aload 5
    //   141: ifnull +8 -> 149
    //   144: aload 5
    //   146: invokevirtual 211	net/lingala/zip4j/io/ZipOutputStream:close	()V
    //   149: aload_1
    //   150: athrow
    //   151: aload 9
    //   153: aload_0
    //   154: getfield 22	net/lingala/zip4j/zip/ZipEngine:zipModel	Lnet/lingala/zip4j/model/ZipModel;
    //   157: invokevirtual 169	net/lingala/zip4j/model/ZipModel:getEndCentralDirRecord	()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    //   160: invokevirtual 214	net/lingala/zip4j/model/EndCentralDirRecord:getOffsetOfStartOfCentralDir	()J
    //   163: invokevirtual 217	net/lingala/zip4j/io/SplitOutputStream:seek	(J)V
    //   166: sipush 4096
    //   169: newarray <illegal type>
    //   171: astore 5
    //   173: aload 6
    //   175: aconst_null
    //   176: aload_2
    //   177: invokevirtual 258	net/lingala/zip4j/io/ZipOutputStream:putNextEntry	(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    //   180: aload_2
    //   181: invokevirtual 423	net/lingala/zip4j/model/ZipParameters:getFileNameInZip	()Ljava/lang/String;
    //   184: ldc_w 425
    //   187: invokevirtual 428	java/lang/String:endsWith	(Ljava/lang/String;)Z
    //   190: ifne +54 -> 244
    //   193: aload_2
    //   194: invokevirtual 423	net/lingala/zip4j/model/ZipParameters:getFileNameInZip	()Ljava/lang/String;
    //   197: ldc_w 430
    //   200: invokevirtual 428	java/lang/String:endsWith	(Ljava/lang/String;)Z
    //   203: ifne +41 -> 244
    //   206: aload_1
    //   207: aload 5
    //   209: invokevirtual 281	java/io/InputStream:read	([B)I
    //   212: istore_3
    //   213: iload_3
    //   214: iconst_m1
    //   215: if_icmpeq +29 -> 244
    //   218: aload 6
    //   220: aload 5
    //   222: iconst_0
    //   223: iload_3
    //   224: invokevirtual 285	net/lingala/zip4j/io/ZipOutputStream:write	([BII)V
    //   227: goto -21 -> 206
    //   230: astore_1
    //   231: aload 6
    //   233: astore 5
    //   235: new 12	net/lingala/zip4j/exception/ZipException
    //   238: dup
    //   239: aload_1
    //   240: invokespecial 290	net/lingala/zip4j/exception/ZipException:<init>	(Ljava/lang/Throwable;)V
    //   243: athrow
    //   244: aload 6
    //   246: invokevirtual 261	net/lingala/zip4j/io/ZipOutputStream:closeEntry	()V
    //   249: aload 6
    //   251: invokevirtual 293	net/lingala/zip4j/io/ZipOutputStream:finish	()V
    //   254: aload 6
    //   256: ifnull +8 -> 264
    //   259: aload 6
    //   261: invokevirtual 211	net/lingala/zip4j/io/ZipOutputStream:close	()V
    //   264: return
    //   265: astore_1
    //   266: return
    //   267: astore_2
    //   268: goto -119 -> 149
    //   271: astore_1
    //   272: aload 6
    //   274: astore 5
    //   276: goto -137 -> 139
    //   279: astore_1
    //   280: aload 8
    //   282: astore 5
    //   284: goto -49 -> 235
    //   287: astore_1
    //   288: aload 7
    //   290: astore 5
    //   292: goto -156 -> 136
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	295	0	this	ZipEngine
    //   0	295	1	paramInputStream	java.io.InputStream
    //   0	295	2	paramZipParameters	ZipParameters
    //   212	12	3	i	int
    //   51	56	4	bool	boolean
    //   30	261	5	localObject1	Object
    //   20	253	6	localZipOutputStream	net.lingala.zip4j.io.ZipOutputStream
    //   26	263	7	localObject2	Object
    //   23	258	8	localObject3	Object
    //   85	67	9	localSplitOutputStream	net.lingala.zip4j.io.SplitOutputStream
    // Exception table:
    //   from	to	target	type
    //   111	131	131	net/lingala/zip4j/exception/ZipException
    //   151	166	131	net/lingala/zip4j/exception/ZipException
    //   166	206	131	net/lingala/zip4j/exception/ZipException
    //   206	213	131	net/lingala/zip4j/exception/ZipException
    //   218	227	131	net/lingala/zip4j/exception/ZipException
    //   244	254	131	net/lingala/zip4j/exception/ZipException
    //   32	37	138	finally
    //   41	53	138	finally
    //   57	87	138	finally
    //   91	106	138	finally
    //   136	138	138	finally
    //   235	244	138	finally
    //   111	131	230	java/lang/Exception
    //   151	166	230	java/lang/Exception
    //   166	206	230	java/lang/Exception
    //   206	213	230	java/lang/Exception
    //   218	227	230	java/lang/Exception
    //   244	254	230	java/lang/Exception
    //   259	264	265	java/io/IOException
    //   144	149	267	java/io/IOException
    //   111	131	271	finally
    //   151	166	271	finally
    //   166	206	271	finally
    //   206	213	271	finally
    //   218	227	271	finally
    //   244	254	271	finally
    //   32	37	279	java/lang/Exception
    //   41	53	279	java/lang/Exception
    //   57	87	279	java/lang/Exception
    //   91	106	279	java/lang/Exception
    //   32	37	287	net/lingala/zip4j/exception/ZipException
    //   41	53	287	net/lingala/zip4j/exception/ZipException
    //   57	87	287	net/lingala/zip4j/exception/ZipException
    //   91	106	287	net/lingala/zip4j/exception/ZipException
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/zip/ZipEngine.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */