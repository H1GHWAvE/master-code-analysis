package net.lingala.zip4j.io;

import java.io.IOException;
import java.io.RandomAccessFile;
import net.lingala.zip4j.crypto.AESDecrypter;
import net.lingala.zip4j.crypto.IDecrypter;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipModel;
import net.lingala.zip4j.unzip.UnzipEngine;

public class PartInputStream
  extends BaseInputStream
{
  private byte[] aesBlockByte = new byte[16];
  private int aesBytesReturned = 0;
  private long bytesRead;
  private int count = -1;
  private IDecrypter decrypter;
  private boolean isAESEncryptedFile = false;
  private long length;
  private byte[] oneByteBuff = new byte[1];
  private RandomAccessFile raf;
  private UnzipEngine unzipEngine;
  
  public PartInputStream(RandomAccessFile paramRandomAccessFile, long paramLong1, long paramLong2, UnzipEngine paramUnzipEngine)
  {
    this.raf = paramRandomAccessFile;
    this.unzipEngine = paramUnzipEngine;
    this.decrypter = paramUnzipEngine.getDecrypter();
    this.bytesRead = 0L;
    this.length = paramLong2;
    if ((paramUnzipEngine.getFileHeader().isEncrypted()) && (paramUnzipEngine.getFileHeader().getEncryptionMethod() == 99)) {}
    for (;;)
    {
      this.isAESEncryptedFile = bool;
      return;
      bool = false;
    }
  }
  
  public int available()
  {
    long l = this.length - this.bytesRead;
    if (l > 2147483647L) {
      return Integer.MAX_VALUE;
    }
    return (int)l;
  }
  
  protected void checkAndReadAESMacBytes()
    throws IOException
  {
    if ((!this.isAESEncryptedFile) || (this.decrypter == null) || (!(this.decrypter instanceof AESDecrypter)) || (((AESDecrypter)this.decrypter).getStoredMac() != null)) {
      return;
    }
    byte[] arrayOfByte = new byte[10];
    int i = this.raf.read(arrayOfByte);
    if (i != 10)
    {
      if (this.unzipEngine.getZipModel().isSplitArchive())
      {
        this.raf.close();
        this.raf = this.unzipEngine.startNextSplitFile();
        this.raf.read(arrayOfByte, i, 10 - i);
      }
    }
    else
    {
      ((AESDecrypter)this.unzipEngine.getDecrypter()).setStoredMac(arrayOfByte);
      return;
    }
    throw new IOException("Error occured while reading stored AES authentication bytes");
  }
  
  public void close()
    throws IOException
  {
    this.raf.close();
  }
  
  public UnzipEngine getUnzipEngine()
  {
    return this.unzipEngine;
  }
  
  public int read()
    throws IOException
  {
    if (this.bytesRead >= this.length) {}
    label79:
    do
    {
      do
      {
        return -1;
        if (!this.isAESEncryptedFile) {
          break label79;
        }
        if ((this.aesBytesReturned != 0) && (this.aesBytesReturned != 16)) {
          break;
        }
      } while (read(this.aesBlockByte) == -1);
      this.aesBytesReturned = 0;
      byte[] arrayOfByte = this.aesBlockByte;
      int i = this.aesBytesReturned;
      this.aesBytesReturned = (i + 1);
      return arrayOfByte[i] & 0xFF;
    } while (read(this.oneByteBuff, 0, 1) == -1);
    return this.oneByteBuff[0] & 0xFF;
  }
  
  public int read(byte[] paramArrayOfByte)
    throws IOException
  {
    return read(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    int i = paramInt2;
    if (paramInt2 > this.length - this.bytesRead)
    {
      paramInt2 = (int)(this.length - this.bytesRead);
      i = paramInt2;
      if (paramInt2 == 0)
      {
        checkAndReadAESMacBytes();
        return -1;
      }
    }
    paramInt2 = i;
    if ((this.unzipEngine.getDecrypter() instanceof AESDecrypter))
    {
      paramInt2 = i;
      if (this.bytesRead + i < this.length)
      {
        paramInt2 = i;
        if (i % 16 != 0) {
          paramInt2 = i - i % 16;
        }
      }
    }
    synchronized (this.raf)
    {
      this.count = this.raf.read(paramArrayOfByte, paramInt1, paramInt2);
      if ((this.count < paramInt2) && (this.unzipEngine.getZipModel().isSplitArchive()))
      {
        this.raf.close();
        this.raf = this.unzipEngine.startNextSplitFile();
        if (this.count < 0) {
          this.count = 0;
        }
        paramInt2 = this.raf.read(paramArrayOfByte, this.count, paramInt2 - this.count);
        if (paramInt2 > 0) {
          this.count += paramInt2;
        }
      }
      if (this.count > 0) {
        if (this.decrypter == null) {}
      }
    }
  }
  
  public void seek(long paramLong)
    throws IOException
  {
    this.raf.seek(paramLong);
  }
  
  public long skip(long paramLong)
    throws IOException
  {
    if (paramLong < 0L) {
      throw new IllegalArgumentException();
    }
    long l = paramLong;
    if (paramLong > this.length - this.bytesRead) {
      l = this.length - this.bytesRead;
    }
    this.bytesRead += l;
    return l;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/net/lingala/zip4j/io/PartInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */