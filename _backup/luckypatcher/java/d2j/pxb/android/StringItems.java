package pxb.android;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class StringItems
  extends ArrayList<StringItem>
{
  static final int UTF8_FLAG = 256;
  byte[] stringData;
  private boolean useUTF8 = true;
  
  public static String[] read(ByteBuffer paramByteBuffer)
    throws IOException
  {
    int k = paramByteBuffer.position() - 8;
    int j = paramByteBuffer.getInt();
    paramByteBuffer.getInt();
    int m = paramByteBuffer.getInt();
    int n = paramByteBuffer.getInt();
    int i1 = paramByteBuffer.getInt();
    int[] arrayOfInt = new int[j];
    String[] arrayOfString = new String[j];
    int i = 0;
    while (i < j)
    {
      arrayOfInt[i] = paramByteBuffer.getInt();
      i += 1;
    }
    if (i1 != 0) {
      System.err.println("ignore style offset at 0x" + Integer.toHexString(k));
    }
    i = 0;
    if (i < arrayOfInt.length)
    {
      paramByteBuffer.position(arrayOfInt[i] + (k + n));
      if ((m & 0x100) != 0)
      {
        u8length(paramByteBuffer);
        j = u8length(paramByteBuffer);
        i1 = paramByteBuffer.position();
        while (paramByteBuffer.get(i1 + j) != 0) {
          j += 1;
        }
      }
      for (String str = new String(paramByteBuffer.array(), i1, j, "UTF-8");; str = new String(paramByteBuffer.array(), paramByteBuffer.position(), j * 2, "UTF-16LE"))
      {
        arrayOfString[i] = str;
        i += 1;
        break;
        j = u16length(paramByteBuffer);
      }
    }
    return arrayOfString;
  }
  
  static int u16length(ByteBuffer paramByteBuffer)
  {
    int j = paramByteBuffer.getShort() & 0xFFFF;
    int i = j;
    if (j > 32767) {
      i = (j & 0x7FFF) << 8 | paramByteBuffer.getShort() & 0xFFFF;
    }
    return i;
  }
  
  static int u8length(ByteBuffer paramByteBuffer)
  {
    int j = paramByteBuffer.get() & 0xFF;
    int i = j;
    if ((j & 0x80) != 0) {
      i = (j & 0x7F) << 8 | paramByteBuffer.get() & 0xFF;
    }
    return i;
  }
  
  public int getSize()
  {
    return size() * 4 + 20 + this.stringData.length + 0;
  }
  
  public void prepare()
    throws IOException
  {
    Object localObject1 = iterator();
    while (((Iterator)localObject1).hasNext()) {
      if (((StringItem)((Iterator)localObject1).next()).data.length() > 32767) {
        this.useUTF8 = false;
      }
    }
    localObject1 = new ByteArrayOutputStream();
    int k = 0;
    int i = 0;
    ((ByteArrayOutputStream)localObject1).reset();
    HashMap localHashMap = new HashMap();
    Iterator localIterator = iterator();
    if (localIterator.hasNext())
    {
      Object localObject2 = (StringItem)localIterator.next();
      ((StringItem)localObject2).index = k;
      String str = ((StringItem)localObject2).data;
      Integer localInteger = (Integer)localHashMap.get(str);
      if (localInteger != null) {
        ((StringItem)localObject2).dataOffset = localInteger.intValue();
      }
      for (;;)
      {
        k += 1;
        break;
        ((StringItem)localObject2).dataOffset = i;
        localHashMap.put(str, Integer.valueOf(i));
        int m;
        int j;
        if (this.useUTF8)
        {
          int n = str.length();
          localObject2 = str.getBytes("UTF-8");
          m = localObject2.length;
          j = i;
          if (n > 127)
          {
            j = i + 1;
            ((ByteArrayOutputStream)localObject1).write(n >> 8 | 0x80);
          }
          ((ByteArrayOutputStream)localObject1).write(n);
          i = j;
          if (m > 127)
          {
            i = j + 1;
            ((ByteArrayOutputStream)localObject1).write(m >> 8 | 0x80);
          }
          ((ByteArrayOutputStream)localObject1).write(m);
          ((ByteArrayOutputStream)localObject1).write((byte[])localObject2);
          ((ByteArrayOutputStream)localObject1).write(0);
          i += m + 3;
        }
        else
        {
          m = str.length();
          localObject2 = str.getBytes("UTF-16LE");
          j = i;
          if (m > 32767)
          {
            j = m >> 16 | 0x8000;
            ((ByteArrayOutputStream)localObject1).write(j);
            ((ByteArrayOutputStream)localObject1).write(j >> 8);
            j = i + 2;
          }
          ((ByteArrayOutputStream)localObject1).write(m);
          ((ByteArrayOutputStream)localObject1).write(m >> 8);
          ((ByteArrayOutputStream)localObject1).write((byte[])localObject2);
          ((ByteArrayOutputStream)localObject1).write(0);
          ((ByteArrayOutputStream)localObject1).write(0);
          i = j + (localObject2.length + 4);
        }
      }
    }
    this.stringData = ((ByteArrayOutputStream)localObject1).toByteArray();
  }
  
  public void write(ByteBuffer paramByteBuffer)
    throws IOException
  {
    paramByteBuffer.putInt(size());
    paramByteBuffer.putInt(0);
    if (this.useUTF8) {}
    for (int i = 256;; i = 0)
    {
      paramByteBuffer.putInt(i);
      paramByteBuffer.putInt(size() * 4 + 28);
      paramByteBuffer.putInt(0);
      Iterator localIterator = iterator();
      while (localIterator.hasNext()) {
        paramByteBuffer.putInt(((StringItem)localIterator.next()).dataOffset);
      }
    }
    paramByteBuffer.put(this.stringData);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/StringItems.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */