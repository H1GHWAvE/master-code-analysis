package pxb.android.axmlLP;

public class EmptyAdapter
  extends AxmlVisitor
{
  public AxmlVisitor.NodeVisitor first(String paramString1, String paramString2)
  {
    return new EmptyNode();
  }
  
  public static class EmptyNode
    extends AxmlVisitor.NodeVisitor
  {
    public AxmlVisitor.NodeVisitor child(String paramString1, String paramString2)
    {
      return new EmptyNode();
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axmlLP/EmptyAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */