package pxb.android.axmlLP;

import com.googlecode.dex2jar.reader.io.ArrayDataIn;
import com.googlecode.dex2jar.reader.io.DataIn;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class AxmlReader
{
  static final int CHUNK_AXML_FILE = 524291;
  static final int CHUNK_RESOURCEIDS = 524672;
  static final int CHUNK_STRINGS = 1835009;
  static final int CHUNK_XML_END_NAMESPACE = 1048833;
  static final int CHUNK_XML_END_TAG = 1048835;
  static final int CHUNK_XML_START_NAMESPACE = 1048832;
  static final int CHUNK_XML_START_TAG = 1048834;
  static final int CHUNK_XML_TEXT = 1048836;
  public static final AxmlVisitor.NodeVisitor EMPTY_VISITOR = new AxmlVisitor.NodeVisitor()
  {
    public AxmlVisitor.NodeVisitor child(String paramAnonymousString1, String paramAnonymousString2)
    {
      return AxmlReader.EMPTY_VISITOR;
    }
  };
  static final int UTF8_FLAG = 256;
  private DataIn in;
  private List<Integer> resourceIds = new ArrayList();
  private StringItems stringItems = new StringItems();
  
  public AxmlReader(DataIn paramDataIn)
  {
    this.in = paramDataIn;
  }
  
  public AxmlReader(byte[] paramArrayOfByte)
  {
    this(ArrayDataIn.le(paramArrayOfByte));
  }
  
  public void accept(final AxmlVisitor paramAxmlVisitor)
    throws IOException
  {
    DataIn localDataIn = this.in;
    if (localDataIn.readIntx() != 524291) {
      throw new RuntimeException();
    }
    int m = localDataIn.readIntx();
    if (paramAxmlVisitor == null) {}
    Stack localStack;
    int i;
    int n;
    for (Object localObject1 = EMPTY_VISITOR;; localObject1 = new AxmlVisitor.NodeVisitor()
        {
          public AxmlVisitor.NodeVisitor child(String paramAnonymousString1, String paramAnonymousString2)
          {
            return paramAxmlVisitor.first(paramAnonymousString1, paramAnonymousString2);
          }
        })
    {
      localObject2 = localObject1;
      localStack = new Stack();
      localStack.push(localObject1);
      i = localDataIn.getCurrentPosition();
      localObject1 = localObject2;
      if (i >= m) {
        break;
      }
      j = localDataIn.readIntx();
      n = localDataIn.readIntx();
      switch (j)
      {
      default: 
        throw new RuntimeException();
      }
    }
    int j = localDataIn.readIntx();
    localDataIn.skip(4);
    int k = localDataIn.readIntx();
    int i1 = localDataIn.readIntx();
    if (localDataIn.readIntx() != 1310740) {
      throw new RuntimeException();
    }
    String str1 = ((StringItem)this.stringItems.get(i1)).data;
    label350:
    int i3;
    int i2;
    int i4;
    String str2;
    if (k >= 0)
    {
      localObject2 = ((StringItem)this.stringItems.get(k)).data;
      localObject2 = ((AxmlVisitor.NodeVisitor)localObject1).child((String)localObject2, str1);
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = EMPTY_VISITOR;
      }
      localStack.push(localObject1);
      ((AxmlVisitor.NodeVisitor)localObject1).line(j);
      i1 = localDataIn.readUShortx();
      localDataIn.skip(6);
      if (localObject1 == EMPTY_VISITOR) {
        break label600;
      }
      j = 0;
      localObject2 = localObject1;
      if (j >= i1) {
        break label613;
      }
      k = localDataIn.readIntx();
      i3 = localDataIn.readIntx();
      localDataIn.skip(4);
      i2 = localDataIn.readIntx() >>> 24;
      i4 = localDataIn.readIntx();
      str2 = ((StringItem)this.stringItems.get(i3)).data;
      if (k < 0) {
        break label544;
      }
      str1 = ((StringItem)this.stringItems.get(k)).data;
      switch (i2)
      {
      default: 
        label446:
        localObject2 = Integer.valueOf(i4);
        label483:
        if (i3 >= this.resourceIds.size()) {
          break;
        }
      }
    }
    for (k = ((Integer)this.resourceIds.get(i3)).intValue();; k = -1)
    {
      ((AxmlVisitor.NodeVisitor)localObject1).attr(str1, str2, k, i2, localObject2);
      j += 1;
      break label350;
      localObject2 = null;
      break;
      label544:
      str1 = null;
      break label446;
      localObject2 = ((StringItem)this.stringItems.get(i4)).data;
      break label483;
      if (i4 != 0) {}
      for (boolean bool = true;; bool = false)
      {
        localObject2 = Boolean.valueOf(bool);
        break;
      }
    }
    label600:
    localDataIn.skip(20);
    Object localObject2 = localObject1;
    for (;;)
    {
      label613:
      localDataIn.move(i + n);
      i = localDataIn.getCurrentPosition();
      localObject1 = localObject2;
      break;
      localDataIn.skip(n - 8);
      ((AxmlVisitor.NodeVisitor)localObject1).end();
      localStack.pop();
      localObject2 = (AxmlVisitor.NodeVisitor)localStack.peek();
      continue;
      if (paramAxmlVisitor == null)
      {
        localDataIn.skip(16);
        localObject2 = localObject1;
      }
      else
      {
        j = localDataIn.readIntx();
        localDataIn.skip(4);
        k = localDataIn.readIntx();
        i1 = localDataIn.readIntx();
        paramAxmlVisitor.ns(((StringItem)this.stringItems.get(k)).data, ((StringItem)this.stringItems.get(i1)).data, j);
        localObject2 = localObject1;
        continue;
        localDataIn.skip(n - 8);
        localObject2 = localObject1;
        continue;
        this.stringItems.read(localDataIn, n);
        localObject2 = localObject1;
        continue;
        k = n / 4;
        j = 0;
        for (;;)
        {
          localObject2 = localObject1;
          if (j >= k - 2) {
            break;
          }
          this.resourceIds.add(Integer.valueOf(localDataIn.readIntx()));
          j += 1;
        }
        if (localObject1 == EMPTY_VISITOR)
        {
          localDataIn.skip(20);
          localObject2 = localObject1;
        }
        else
        {
          j = localDataIn.readIntx();
          localDataIn.skip(4);
          k = localDataIn.readIntx();
          localDataIn.skip(8);
          ((AxmlVisitor.NodeVisitor)localObject1).text(j, ((StringItem)this.stringItems.get(k)).data);
          localObject2 = localObject1;
        }
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axmlLP/AxmlReader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */