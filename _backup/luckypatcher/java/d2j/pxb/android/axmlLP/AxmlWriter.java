package pxb.android.axmlLP;

import com.googlecode.dex2jar.reader.io.DataOut;
import com.googlecode.dex2jar.reader.io.LeDataOut;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

public class AxmlWriter
  extends AxmlVisitor
{
  private List<NodeImpl> firsts = new ArrayList(3);
  private Map<String, Ns> nses = new HashMap();
  private List<StringItem> otherString = new ArrayList();
  private Map<Integer, StringItem> resourceId2Str = new HashMap();
  private List<Integer> resourceIds = new ArrayList();
  private List<StringItem> resourceString = new ArrayList();
  private StringItems stringItems = new StringItems();
  
  private int prepare()
    throws IOException
  {
    int i = this.nses.size() * 24 * 2;
    Object localObject = this.firsts.iterator();
    while (((Iterator)localObject).hasNext()) {
      i += ((NodeImpl)((Iterator)localObject).next()).prepare(this);
    }
    int j = 0;
    Iterator localIterator = this.nses.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      Ns localNs = (Ns)localEntry.getValue();
      k = j;
      localObject = localNs;
      if (localNs == null)
      {
        localObject = new Ns(new StringItem(String.format("axml_auto_%02d", new Object[] { Integer.valueOf(j) })), new StringItem((String)localEntry.getKey()), 0);
        localEntry.setValue(localObject);
        k = j + 1;
      }
      ((Ns)localObject).prefix = update(((Ns)localObject).prefix);
      ((Ns)localObject).uri = update(((Ns)localObject).uri);
      j = k;
    }
    this.stringItems.addAll(this.resourceString);
    this.resourceString = null;
    this.stringItems.addAll(this.otherString);
    this.otherString = null;
    this.stringItems.prepare();
    int k = this.stringItems.getSize();
    j = k;
    if (k % 4 != 0) {
      j = k + (4 - k % 4);
    }
    return i + (j + 8) + (this.resourceIds.size() * 4 + 8);
  }
  
  public void end() {}
  
  public AxmlVisitor.NodeVisitor first(String paramString1, String paramString2)
  {
    paramString1 = new NodeImpl(paramString1, paramString2);
    this.firsts.add(paramString1);
    return paramString1;
  }
  
  public void ns(String paramString1, String paramString2, int paramInt)
  {
    this.nses.put(paramString2, new Ns(new StringItem(paramString1), new StringItem(paramString2), paramInt));
  }
  
  public byte[] toByteArray()
    throws IOException
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    LeDataOut localLeDataOut = new LeDataOut(localByteArrayOutputStream);
    int i = prepare();
    localLeDataOut.writeInt(524291);
    localLeDataOut.writeInt(i + 8);
    int j = this.stringItems.getSize();
    i = 0;
    if (j % 4 != 0) {
      i = 4 - j % 4;
    }
    localLeDataOut.writeInt(1835009);
    localLeDataOut.writeInt(j + i + 8);
    this.stringItems.write(localLeDataOut);
    localLeDataOut.writeBytes(new byte[i]);
    localLeDataOut.writeInt(524672);
    localLeDataOut.writeInt(this.resourceIds.size() * 4 + 8);
    Object localObject1 = this.resourceIds.iterator();
    while (((Iterator)localObject1).hasNext()) {
      localLeDataOut.writeInt(((Integer)((Iterator)localObject1).next()).intValue());
    }
    localObject1 = new Stack();
    Object localObject2 = this.nses.entrySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      Ns localNs = (Ns)((Map.Entry)((Iterator)localObject2).next()).getValue();
      ((Stack)localObject1).push(localNs);
      localLeDataOut.writeInt(1048832);
      localLeDataOut.writeInt(24);
      localLeDataOut.writeInt(-1);
      localLeDataOut.writeInt(-1);
      localLeDataOut.writeInt(localNs.prefix.index);
      localLeDataOut.writeInt(localNs.uri.index);
    }
    localObject2 = this.firsts.iterator();
    while (((Iterator)localObject2).hasNext()) {
      ((NodeImpl)((Iterator)localObject2).next()).write(localLeDataOut);
    }
    while (((Stack)localObject1).size() > 0)
    {
      localObject2 = (Ns)((Stack)localObject1).pop();
      localLeDataOut.writeInt(1048833);
      localLeDataOut.writeInt(24);
      localLeDataOut.writeInt(((Ns)localObject2).ln);
      localLeDataOut.writeInt(-1);
      localLeDataOut.writeInt(((Ns)localObject2).prefix.index);
      localLeDataOut.writeInt(((Ns)localObject2).uri.index);
    }
    return localByteArrayOutputStream.toByteArray();
  }
  
  StringItem update(StringItem paramStringItem)
  {
    if (paramStringItem == null) {
      return null;
    }
    int i = this.otherString.indexOf(paramStringItem);
    if (i < 0)
    {
      paramStringItem = new StringItem(paramStringItem.data);
      this.otherString.add(paramStringItem);
      return paramStringItem;
    }
    return (StringItem)this.otherString.get(i);
  }
  
  StringItem updateNs(StringItem paramStringItem)
  {
    if (paramStringItem == null) {
      return null;
    }
    String str = paramStringItem.data;
    if (!this.nses.containsKey(str)) {
      this.nses.put(str, null);
    }
    return update(paramStringItem);
  }
  
  StringItem updateWithResourceId(StringItem paramStringItem, int paramInt)
  {
    StringItem localStringItem = (StringItem)this.resourceId2Str.get(Integer.valueOf(paramInt));
    if (localStringItem != null) {
      return localStringItem;
    }
    paramStringItem = new StringItem(paramStringItem.data);
    this.resourceIds.add(Integer.valueOf(paramInt));
    this.resourceString.add(paramStringItem);
    this.resourceId2Str.put(Integer.valueOf(paramInt), paramStringItem);
    return paramStringItem;
  }
  
  static class Attr
  {
    public StringItem name;
    public StringItem ns;
    public int resourceId;
    public int type;
    public Object value;
    
    public Attr(StringItem paramStringItem1, StringItem paramStringItem2, int paramInt1, int paramInt2, Object paramObject)
    {
      this.ns = paramStringItem1;
      this.name = paramStringItem2;
      this.resourceId = paramInt1;
      this.type = paramInt2;
      this.value = paramObject;
    }
    
    public void prepare(AxmlWriter paramAxmlWriter)
    {
      this.ns = paramAxmlWriter.updateNs(this.ns);
      if (this.name != null) {
        if (this.resourceId == -1) {
          break label69;
        }
      }
      label69:
      for (this.name = paramAxmlWriter.updateWithResourceId(this.name, this.resourceId);; this.name = paramAxmlWriter.update(this.name))
      {
        if ((this.value instanceof StringItem)) {
          this.value = paramAxmlWriter.update((StringItem)this.value);
        }
        return;
      }
    }
  }
  
  static class NodeImpl
    extends AxmlVisitor.NodeVisitor
  {
    private Map<String, AxmlWriter.Attr> attrs = new HashMap();
    private List<NodeImpl> children = new ArrayList();
    private int line;
    private StringItem name;
    private StringItem ns;
    private StringItem text;
    private int textLineNumber;
    
    public NodeImpl(String paramString1, String paramString2)
    {
      super();
      if (paramString1 == null)
      {
        paramString1 = null;
        this.ns = paramString1;
        if (paramString2 != null) {
          break label64;
        }
      }
      label64:
      for (paramString1 = (String)localObject;; paramString1 = new StringItem(paramString2))
      {
        this.name = paramString1;
        return;
        paramString1 = new StringItem(paramString1);
        break;
      }
    }
    
    public void attr(String paramString1, String paramString2, int paramInt1, int paramInt2, Object paramObject)
    {
      if (paramString2 == null) {
        throw new RuntimeException("name can't be null");
      }
      Map localMap = this.attrs;
      Object localObject = new StringBuilder();
      String str;
      if (paramString1 == null)
      {
        str = "zzz";
        str = str + "." + paramString2;
        if (paramString1 != null) {
          break label124;
        }
        paramString1 = null;
        label64:
        localObject = new StringItem(paramString2);
        if (paramInt2 != 3) {
          break label136;
        }
      }
      label124:
      label136:
      for (paramString2 = new StringItem((String)paramObject);; paramString2 = (String)paramObject)
      {
        localMap.put(str, new AxmlWriter.Attr(paramString1, (StringItem)localObject, paramInt1, paramInt2, paramString2));
        return;
        str = paramString1;
        break;
        paramString1 = new StringItem(paramString1);
        break label64;
      }
    }
    
    public AxmlVisitor.NodeVisitor child(String paramString1, String paramString2)
    {
      paramString1 = new NodeImpl(paramString1, paramString2);
      this.children.add(paramString1);
      return paramString1;
    }
    
    public void end() {}
    
    public void line(int paramInt)
    {
      this.line = paramInt;
    }
    
    public int prepare(AxmlWriter paramAxmlWriter)
    {
      this.ns = paramAxmlWriter.updateNs(this.ns);
      this.name = paramAxmlWriter.update(this.name);
      Iterator localIterator = sortedAttrs().iterator();
      while (localIterator.hasNext()) {
        ((AxmlWriter.Attr)localIterator.next()).prepare(paramAxmlWriter);
      }
      this.text = paramAxmlWriter.update(this.text);
      int i = this.attrs.size() * 20 + 60;
      localIterator = this.children.iterator();
      while (localIterator.hasNext()) {
        i += ((NodeImpl)localIterator.next()).prepare(paramAxmlWriter);
      }
      int j = i;
      if (this.text != null) {
        j = i + 28;
      }
      return j;
    }
    
    List<AxmlWriter.Attr> sortedAttrs()
    {
      ArrayList localArrayList = new ArrayList(this.attrs.values());
      Collections.sort(localArrayList, new Comparator()
      {
        public int compare(AxmlWriter.Attr paramAnonymousAttr1, AxmlWriter.Attr paramAnonymousAttr2)
        {
          int i;
          if (paramAnonymousAttr1.ns == null) {
            if (paramAnonymousAttr2.ns == null) {
              i = paramAnonymousAttr2.name.data.compareTo(paramAnonymousAttr1.name.data);
            }
          }
          int j;
          do
          {
            do
            {
              return i;
              return 1;
              if (paramAnonymousAttr2.ns == null) {
                return -1;
              }
              j = paramAnonymousAttr1.ns.data.compareTo(paramAnonymousAttr2.ns.data);
              i = j;
            } while (j != 0);
            j = paramAnonymousAttr1.resourceId - paramAnonymousAttr2.resourceId;
            i = j;
          } while (j != 0);
          return paramAnonymousAttr1.name.data.compareTo(paramAnonymousAttr2.name.data);
        }
      });
      return localArrayList;
    }
    
    public void text(int paramInt, String paramString)
    {
      this.text = new StringItem(paramString);
      this.textLineNumber = paramInt;
    }
    
    void write(DataOut paramDataOut)
      throws IOException
    {
      int j = -1;
      paramDataOut.writeInt(1048834);
      paramDataOut.writeInt(this.attrs.size() * 20 + 36);
      paramDataOut.writeInt(this.line);
      paramDataOut.writeInt(-1);
      if (this.ns != null)
      {
        i = this.ns.index;
        paramDataOut.writeInt(i);
        paramDataOut.writeInt(this.name.index);
        paramDataOut.writeInt(1310740);
        paramDataOut.writeShort(this.attrs.size());
        paramDataOut.writeShort(0);
        paramDataOut.writeShort(0);
        paramDataOut.writeShort(0);
        localIterator = sortedAttrs().iterator();
      }
      for (;;)
      {
        label138:
        if (!localIterator.hasNext()) {
          break label351;
        }
        AxmlWriter.Attr localAttr = (AxmlWriter.Attr)localIterator.next();
        if (localAttr.ns == null)
        {
          i = -1;
          label170:
          paramDataOut.writeInt(i);
          paramDataOut.writeInt(localAttr.name.index);
          if (!(localAttr.value instanceof StringItem)) {
            break label290;
          }
        }
        Object localObject;
        label290:
        for (i = ((StringItem)localAttr.value).index;; i = -1)
        {
          paramDataOut.writeInt(i);
          paramDataOut.writeInt(localAttr.type << 24 | 0x8);
          localObject = localAttr.value;
          if (!(localObject instanceof StringItem)) {
            break label295;
          }
          paramDataOut.writeInt(((StringItem)localAttr.value).index);
          break label138;
          i = -1;
          break;
          i = localAttr.ns.index;
          break label170;
        }
        label295:
        if ((localObject instanceof Boolean))
        {
          if (Boolean.TRUE.equals(localObject)) {}
          for (i = -1;; i = 0)
          {
            paramDataOut.writeInt(i);
            break;
          }
        }
        paramDataOut.writeInt(((Integer)localAttr.value).intValue());
      }
      label351:
      if (this.text != null)
      {
        paramDataOut.writeInt(1048836);
        paramDataOut.writeInt(28);
        paramDataOut.writeInt(this.textLineNumber);
        paramDataOut.writeInt(-1);
        paramDataOut.writeInt(this.text.index);
        paramDataOut.writeInt(8);
        paramDataOut.writeInt(0);
      }
      Iterator localIterator = this.children.iterator();
      while (localIterator.hasNext()) {
        ((NodeImpl)localIterator.next()).write(paramDataOut);
      }
      paramDataOut.writeInt(1048835);
      paramDataOut.writeInt(24);
      paramDataOut.writeInt(-1);
      paramDataOut.writeInt(-1);
      int i = j;
      if (this.ns != null) {
        i = this.ns.index;
      }
      paramDataOut.writeInt(i);
      paramDataOut.writeInt(this.name.index);
    }
  }
  
  static class Ns
  {
    int ln;
    StringItem prefix;
    StringItem uri;
    
    public Ns(StringItem paramStringItem1, StringItem paramStringItem2, int paramInt)
    {
      this.prefix = paramStringItem1;
      this.uri = paramStringItem2;
      this.ln = paramInt;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axmlLP/AxmlWriter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */