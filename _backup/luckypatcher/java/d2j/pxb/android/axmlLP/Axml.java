package pxb.android.axmlLP;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Axml
  extends AxmlVisitor
{
  public List<Node> firsts = new ArrayList();
  public List<Ns> nses = new ArrayList();
  
  public void accept(final AxmlVisitor paramAxmlVisitor)
  {
    Iterator localIterator = this.nses.iterator();
    while (localIterator.hasNext()) {
      ((Ns)localIterator.next()).accept(paramAxmlVisitor);
    }
    localIterator = this.firsts.iterator();
    while (localIterator.hasNext()) {
      ((Node)localIterator.next()).accept(new AxmlVisitor.NodeVisitor(null)
      {
        public AxmlVisitor.NodeVisitor child(String paramAnonymousString1, String paramAnonymousString2)
        {
          return paramAxmlVisitor.first(paramAnonymousString1, paramAnonymousString2);
        }
      });
    }
  }
  
  public AxmlVisitor.NodeVisitor first(String paramString1, String paramString2)
  {
    Node localNode = new Node();
    localNode.name = paramString2;
    localNode.ns = paramString1;
    this.firsts.add(localNode);
    return localNode;
  }
  
  public void ns(String paramString1, String paramString2, int paramInt)
  {
    Ns localNs = new Ns();
    localNs.prefix = paramString1;
    localNs.uri = paramString2;
    localNs.ln = paramInt;
    this.nses.add(localNs);
  }
  
  public static class Node
    extends AxmlVisitor.NodeVisitor
  {
    public List<Attr> attrs = new ArrayList();
    public List<Node> children = new ArrayList();
    public Integer ln;
    public String name;
    public String ns;
    public Text text;
    
    public void accept(AxmlVisitor.NodeVisitor paramNodeVisitor)
    {
      paramNodeVisitor = paramNodeVisitor.child(this.ns, this.name);
      acceptB(paramNodeVisitor);
      paramNodeVisitor.end();
    }
    
    public void acceptB(AxmlVisitor.NodeVisitor paramNodeVisitor)
    {
      if (this.text != null) {
        this.text.accept(paramNodeVisitor);
      }
      Iterator localIterator = this.attrs.iterator();
      while (localIterator.hasNext()) {
        ((Attr)localIterator.next()).accept(paramNodeVisitor);
      }
      if (this.ln != null) {
        paramNodeVisitor.line(this.ln.intValue());
      }
      localIterator = this.children.iterator();
      while (localIterator.hasNext()) {
        ((Node)localIterator.next()).accept(paramNodeVisitor);
      }
    }
    
    public void attr(String paramString1, String paramString2, int paramInt1, int paramInt2, Object paramObject)
    {
      Attr localAttr = new Attr();
      localAttr.name = paramString2;
      localAttr.ns = paramString1;
      localAttr.resourceId = paramInt1;
      localAttr.type = paramInt2;
      localAttr.value = paramObject;
      this.attrs.add(localAttr);
    }
    
    public AxmlVisitor.NodeVisitor child(String paramString1, String paramString2)
    {
      Node localNode = new Node();
      localNode.name = paramString2;
      localNode.ns = paramString1;
      this.children.add(localNode);
      return localNode;
    }
    
    public void line(int paramInt)
    {
      this.ln = Integer.valueOf(paramInt);
    }
    
    public void text(int paramInt, String paramString)
    {
      Text localText = new Text();
      localText.ln = paramInt;
      localText.text = paramString;
      this.text = localText;
    }
    
    public static class Attr
    {
      public String name;
      public String ns;
      public int resourceId;
      public int type;
      public Object value;
      
      public void accept(AxmlVisitor.NodeVisitor paramNodeVisitor)
      {
        paramNodeVisitor.attr(this.ns, this.name, this.resourceId, this.type, this.value);
      }
    }
    
    public static class Text
    {
      public int ln;
      public String text;
      
      public void accept(AxmlVisitor.NodeVisitor paramNodeVisitor)
      {
        paramNodeVisitor.text(this.ln, this.text);
      }
    }
  }
  
  public static class Ns
  {
    public int ln;
    public String prefix;
    public String uri;
    
    public void accept(AxmlVisitor paramAxmlVisitor)
    {
      paramAxmlVisitor.ns(this.prefix, this.uri, this.ln);
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axmlLP/Axml.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */