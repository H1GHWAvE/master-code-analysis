package pxb.android.axmlLP;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

public class EnableDebugger
{
  public static void main(String... paramVarArgs)
    throws Exception
  {
    if (paramVarArgs.length < 2)
    {
      System.err.println("test5 in out");
      return;
    }
    new EnableDebugger().a(new File(paramVarArgs[0]), new File(paramVarArgs[1]));
  }
  
  void a(File paramFile1, File paramFile2)
    throws Exception
  {
    paramFile1 = new FileInputStream(paramFile1);
    Object localObject = new byte[paramFile1.available()];
    paramFile1.read((byte[])localObject);
    paramFile1.close();
    paramFile1 = new AxmlReader((byte[])localObject);
    localObject = new AxmlWriter();
    paramFile1.accept(new AxmlVisitor((AxmlVisitor)localObject)
    {
      public AxmlVisitor.NodeVisitor first(String paramAnonymousString1, String paramAnonymousString2)
      {
        new AxmlVisitor.NodeVisitor(super.first(paramAnonymousString1, paramAnonymousString2))
        {
          public AxmlVisitor.NodeVisitor child(String paramAnonymous2String1, String paramAnonymous2String2)
          {
            new AxmlVisitor.NodeVisitor(super.child(paramAnonymous2String1, paramAnonymous2String2))
            {
              public void attr(String paramAnonymous3String1, String paramAnonymous3String2, int paramAnonymous3Int1, int paramAnonymous3Int2, Object paramAnonymous3Object)
              {
                if (("http://schemas.android.com/apk/res/android".equals(paramAnonymous3String1)) && ("debuggable".equals(paramAnonymous3String2))) {
                  return;
                }
                super.attr(paramAnonymous3String1, paramAnonymous3String2, paramAnonymous3Int1, paramAnonymous3Int2, paramAnonymous3Object);
              }
              
              public void end()
              {
                super.attr("http://schemas.android.com/apk/res/android", "debuggable", 16842767, 18, Integer.valueOf(-1));
                super.end();
              }
            };
          }
        };
      }
    });
    paramFile1 = ((AxmlWriter)localObject).toByteArray();
    paramFile2 = new FileOutputStream(paramFile2);
    paramFile2.write(paramFile1);
    paramFile2.close();
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axmlLP/EnableDebugger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */