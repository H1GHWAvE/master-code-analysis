package pxb.android.axmlLP;

public class AxmlVisitor
{
  public static final int TYPE_FIRST_INT = 16;
  public static final int TYPE_INT_BOOLEAN = 18;
  public static final int TYPE_INT_HEX = 17;
  public static final int TYPE_REFERENCE = 1;
  public static final int TYPE_STRING = 3;
  protected AxmlVisitor av;
  
  public AxmlVisitor() {}
  
  public AxmlVisitor(AxmlVisitor paramAxmlVisitor)
  {
    this.av = paramAxmlVisitor;
  }
  
  public void end()
  {
    if (this.av != null) {
      this.av.end();
    }
  }
  
  public NodeVisitor first(String paramString1, String paramString2)
  {
    if (this.av != null) {
      return this.av.first(paramString1, paramString2);
    }
    return null;
  }
  
  public void ns(String paramString1, String paramString2, int paramInt)
  {
    if (this.av != null) {
      this.av.ns(paramString1, paramString2, paramInt);
    }
  }
  
  public static abstract class NodeVisitor
  {
    protected NodeVisitor nv;
    
    public NodeVisitor() {}
    
    public NodeVisitor(NodeVisitor paramNodeVisitor)
    {
      this.nv = paramNodeVisitor;
    }
    
    public void attr(String paramString1, String paramString2, int paramInt1, int paramInt2, Object paramObject)
    {
      if (this.nv != null) {
        this.nv.attr(paramString1, paramString2, paramInt1, paramInt2, paramObject);
      }
    }
    
    public NodeVisitor child(String paramString1, String paramString2)
    {
      if (this.nv != null) {
        return this.nv.child(paramString1, paramString2);
      }
      return null;
    }
    
    public void end()
    {
      if (this.nv != null) {
        this.nv.end();
      }
    }
    
    public void line(int paramInt)
    {
      if (this.nv != null) {
        this.nv.line(paramInt);
      }
    }
    
    public void text(int paramInt, String paramString)
    {
      if (this.nv != null) {
        this.nv.text(paramInt, paramString);
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axmlLP/AxmlVisitor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */