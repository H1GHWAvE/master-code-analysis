package pxb.android.axmlLP;

class StringItem
{
  public String data;
  public int dataOffset;
  public int index;
  
  public StringItem() {}
  
  public StringItem(String paramString)
  {
    this.data = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    return ((StringItem)paramObject).data.equals(this.data);
  }
  
  public int hashCode()
  {
    return this.data.hashCode();
  }
  
  public String toString()
  {
    return String.format("S%04d %s", new Object[] { Integer.valueOf(this.index), this.data });
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axmlLP/StringItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */