package pxb.android.axmlLP;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

public class DumpAdapter
  extends AxmlVisitor
{
  private Map<String, String> nses = new HashMap();
  
  public DumpAdapter() {}
  
  public DumpAdapter(AxmlVisitor paramAxmlVisitor)
  {
    super(paramAxmlVisitor);
  }
  
  public void end()
  {
    super.end();
  }
  
  public AxmlVisitor.NodeVisitor first(String paramString1, String paramString2)
  {
    System.out.print("<");
    if (paramString1 != null) {
      System.out.println((String)this.nses.get(paramString1) + ":");
    }
    System.out.println(paramString2);
    paramString1 = super.first(paramString1, paramString2);
    if (paramString1 != null) {
      return new DumpNodeAdapter(paramString1, 1, this.nses);
    }
    return null;
  }
  
  public void ns(String paramString1, String paramString2, int paramInt)
  {
    System.out.println(paramString1 + "=" + paramString2);
    this.nses.put(paramString2, paramString1);
    super.ns(paramString1, paramString2, paramInt);
  }
  
  public static class DumpNodeAdapter
    extends AxmlVisitor.NodeVisitor
  {
    protected int deep;
    protected Map<String, String> nses;
    
    public DumpNodeAdapter(AxmlVisitor.NodeVisitor paramNodeVisitor)
    {
      super();
      this.deep = 0;
      this.nses = null;
    }
    
    public DumpNodeAdapter(AxmlVisitor.NodeVisitor paramNodeVisitor, int paramInt, Map<String, String> paramMap)
    {
      super();
      this.deep = paramInt;
      this.nses = paramMap;
    }
    
    public void attr(String paramString1, String paramString2, int paramInt1, int paramInt2, Object paramObject)
    {
      int i = 0;
      while (i < this.deep)
      {
        System.out.print("  ");
        i += 1;
      }
      if (paramString1 != null) {
        System.out.print(String.format("%s:", new Object[] { getPrefix(paramString1) }));
      }
      System.out.print(paramString2);
      if (paramInt1 != -1) {
        System.out.print(String.format("(%08x)", new Object[] { Integer.valueOf(paramInt1) }));
      }
      if ((paramObject instanceof String)) {
        System.out.print(String.format("=[%08x]\"%s\"", new Object[] { Integer.valueOf(paramInt2), paramObject }));
      }
      for (;;)
      {
        System.out.println();
        super.attr(paramString1, paramString2, paramInt1, paramInt2, paramObject);
        return;
        if ((paramObject instanceof Boolean)) {
          System.out.print(String.format("=[%08x]\"%b\"", new Object[] { Integer.valueOf(paramInt2), paramObject }));
        } else {
          System.out.print(String.format("=[%08x]%08x", new Object[] { Integer.valueOf(paramInt2), paramObject }));
        }
      }
    }
    
    public AxmlVisitor.NodeVisitor child(String paramString1, String paramString2)
    {
      int i = 0;
      while (i < this.deep)
      {
        System.out.print("  ");
        i += 1;
      }
      System.out.print("<");
      if (paramString1 != null) {
        System.out.println(getPrefix(paramString1) + ":");
      }
      System.out.println(paramString2);
      paramString1 = super.child(paramString1, paramString2);
      if (paramString1 != null) {
        return new DumpNodeAdapter(paramString1, this.deep + 1, this.nses);
      }
      return null;
    }
    
    protected String getPrefix(String paramString)
    {
      if (this.nses != null)
      {
        String str = (String)this.nses.get(paramString);
        if (str != null) {
          return str;
        }
      }
      return paramString;
    }
    
    public void text(int paramInt, String paramString)
    {
      int i = 0;
      while (i < this.deep + 1)
      {
        System.out.print("  ");
        i += 1;
      }
      System.out.println(paramString);
      super.text(paramInt, paramString);
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axmlLP/DumpAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */