package pxb.android.axmlLP;

import com.googlecode.dex2jar.reader.io.DataIn;
import com.googlecode.dex2jar.reader.io.DataOut;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

class StringItems
  extends ArrayList<StringItem>
{
  byte[] stringData;
  
  public int getSize()
  {
    return size() * 4 + 20 + this.stringData.length + 0;
  }
  
  public void prepare()
    throws IOException
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    int i = 0;
    int j = 0;
    localByteArrayOutputStream.reset();
    HashMap localHashMap = new HashMap();
    Iterator localIterator = iterator();
    if (localIterator.hasNext())
    {
      Object localObject = (StringItem)localIterator.next();
      ((StringItem)localObject).index = i;
      String str = ((StringItem)localObject).data;
      Integer localInteger = (Integer)localHashMap.get(str);
      if (localInteger != null) {
        ((StringItem)localObject).dataOffset = localInteger.intValue();
      }
      for (;;)
      {
        i += 1;
        break;
        ((StringItem)localObject).dataOffset = j;
        localHashMap.put(str, Integer.valueOf(j));
        int k = str.length();
        localObject = str.getBytes("UTF-16LE");
        localByteArrayOutputStream.write(k);
        localByteArrayOutputStream.write(k >> 8);
        localByteArrayOutputStream.write((byte[])localObject);
        localByteArrayOutputStream.write(0);
        localByteArrayOutputStream.write(0);
        j += localObject.length + 4;
      }
    }
    this.stringData = localByteArrayOutputStream.toByteArray();
  }
  
  public void read(DataIn paramDataIn, int paramInt)
    throws IOException
  {
    paramDataIn.getCurrentPosition();
    int k = paramDataIn.readIntx();
    int n = paramDataIn.readIntx();
    int j = paramDataIn.readIntx();
    paramDataIn.readIntx();
    int m = paramDataIn.readIntx();
    int i = 0;
    while (i < k)
    {
      localObject1 = new StringItem();
      ((StringItem)localObject1).index = i;
      ((StringItem)localObject1).dataOffset = paramDataIn.readIntx();
      add(localObject1);
      i += 1;
    }
    Object localObject1 = new TreeMap();
    if (n != 0) {
      throw new RuntimeException();
    }
    Object localObject2;
    if (m == 0)
    {
      i = paramDataIn.getCurrentPosition();
      if ((j & 0x100) == 0) {}
    }
    else
    {
      for (j = i;; j = paramDataIn.getCurrentPosition())
      {
        if (j >= paramInt) {
          break label311;
        }
        localObject2 = new ByteArrayOutputStream((int)paramDataIn.readLeb128() + 10);
        k = paramDataIn.readByte();
        for (;;)
        {
          if (k != 0)
          {
            ((ByteArrayOutputStream)localObject2).write(k);
            k = paramDataIn.readByte();
            continue;
            paramInt = m;
            break;
          }
        }
        ((Map)localObject1).put(Integer.valueOf(j - i), new String(((ByteArrayOutputStream)localObject2).toByteArray(), "UTF-8"));
      }
    }
    for (j = i; j < paramInt; j = paramDataIn.getCurrentPosition())
    {
      localObject2 = paramDataIn.readBytes(paramDataIn.readShortx() * 2);
      paramDataIn.skip(2);
      ((Map)localObject1).put(Integer.valueOf(j - i), new String((byte[])localObject2, "UTF-16LE"));
    }
    label311:
    if (m != 0) {}
    paramDataIn = iterator();
    while (paramDataIn.hasNext())
    {
      localObject2 = (StringItem)paramDataIn.next();
      ((StringItem)localObject2).data = ((String)((Map)localObject1).get(Integer.valueOf(((StringItem)localObject2).dataOffset)));
    }
  }
  
  public void write(DataOut paramDataOut)
    throws IOException
  {
    paramDataOut.writeInt(size());
    paramDataOut.writeInt(0);
    paramDataOut.writeInt(0);
    paramDataOut.writeInt(size() * 4 + 28);
    paramDataOut.writeInt(0);
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      paramDataOut.writeInt(((StringItem)localIterator.next()).dataOffset);
    }
    paramDataOut.writeBytes(this.stringData);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axmlLP/StringItems.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */