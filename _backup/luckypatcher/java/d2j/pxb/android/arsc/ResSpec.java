package pxb.android.arsc;

public class ResSpec
{
  public int flags;
  public final int id;
  public String name;
  
  public ResSpec(int paramInt)
  {
    this.id = paramInt;
  }
  
  public void updateName(String paramString)
  {
    String str = this.name;
    if (str == null) {
      this.name = paramString;
    }
    while (paramString.equals(str)) {
      return;
    }
    throw new RuntimeException();
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/arsc/ResSpec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */