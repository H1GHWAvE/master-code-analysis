package pxb.android.arsc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

public class BagValue
{
  public List<Map.Entry<Integer, Value>> map = new ArrayList();
  public final int parent;
  
  public BagValue(int paramInt)
  {
    this.parent = paramInt;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (paramObject == null) {
        return false;
      }
      if (!(paramObject instanceof BagValue)) {
        return false;
      }
      paramObject = (BagValue)paramObject;
      if (this.map == null)
      {
        if (((BagValue)paramObject).map != null) {
          return false;
        }
      }
      else if (!this.map.equals(((BagValue)paramObject).map)) {
        return false;
      }
    } while (this.parent == ((BagValue)paramObject).parent);
    return false;
  }
  
  public int hashCode()
  {
    if (this.map == null) {}
    for (int i = 0;; i = this.map.hashCode()) {
      return (i + 31) * 31 + this.parent;
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(String.format("{bag%08x", new Object[] { Integer.valueOf(this.parent) }));
    Iterator localIterator = this.map.iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localStringBuilder.append(",").append(String.format("0x%08x", new Object[] { localEntry.getKey() }));
      localStringBuilder.append("=");
      localStringBuilder.append(localEntry.getValue());
    }
    return "}";
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/arsc/BagValue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */