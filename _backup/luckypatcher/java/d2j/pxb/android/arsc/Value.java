package pxb.android.arsc;

public class Value
{
  public final int data;
  public String raw;
  public final int type;
  
  public Value(int paramInt1, int paramInt2, String paramString)
  {
    this.type = paramInt1;
    this.data = paramInt2;
    this.raw = paramString;
  }
  
  public String toString()
  {
    if (this.type == 3) {
      return this.raw;
    }
    return String.format("{t=0x%02x d=0x%08x}", new Object[] { Integer.valueOf(this.type), Integer.valueOf(this.data) });
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/arsc/Value.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */