package pxb.android.arsc;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import pxb.android.axml.Util;

public class ArscDumper
{
  public static void dump(List<Pkg> paramList)
  {
    int i = 0;
    while (i < paramList.size())
    {
      Pkg localPkg = (Pkg)paramList.get(i);
      System.out.println(String.format("  Package %d id=%d name=%s typeCount=%d", new Object[] { Integer.valueOf(i), Integer.valueOf(localPkg.id), localPkg.name, Integer.valueOf(localPkg.types.size()) }));
      Iterator localIterator = localPkg.types.values().iterator();
      while (localIterator.hasNext())
      {
        Type localType = (Type)localIterator.next();
        System.out.println(String.format("    type %d %s", new Object[] { Integer.valueOf(localType.id - 1), localType.name }));
        int m = localPkg.id << 24 | localType.id << 16;
        int j = 0;
        Object localObject;
        while (j < localType.specs.length)
        {
          localObject = localType.getSpec(j);
          System.out.println(String.format("      spec 0x%08x 0x%08x %s", new Object[] { Integer.valueOf(((ResSpec)localObject).id | m), Integer.valueOf(((ResSpec)localObject).flags), ((ResSpec)localObject).name }));
          j += 1;
        }
        j = 0;
        while (j < localType.configs.size())
        {
          localObject = (Config)localType.configs.get(j);
          System.out.println("      config");
          localObject = new ArrayList(((Config)localObject).resources.values());
          int k = 0;
          while (k < ((List)localObject).size())
          {
            ResEntry localResEntry = (ResEntry)((List)localObject).get(k);
            System.out.println(String.format("        resource 0x%08x %-20s: %s", new Object[] { Integer.valueOf(localResEntry.spec.id | m), localResEntry.spec.name, localResEntry.value }));
            k += 1;
          }
          j += 1;
        }
      }
      i += 1;
    }
  }
  
  public static void main(String... paramVarArgs)
    throws IOException
  {
    if (paramVarArgs.length == 0)
    {
      System.err.println("asrc-dump file.arsc");
      return;
    }
    dump(new ArscParser(Util.readFile(new File(paramVarArgs[0]))).parse());
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/arsc/ArscDumper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */