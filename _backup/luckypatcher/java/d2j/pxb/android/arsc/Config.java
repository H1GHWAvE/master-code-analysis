package pxb.android.arsc;

import java.util.Map;
import java.util.TreeMap;

public class Config
{
  public final int entryCount;
  public final byte[] id;
  public Map<Integer, ResEntry> resources = new TreeMap();
  int wChunkSize;
  int wEntryStart;
  int wPosition;
  
  public Config(byte[] paramArrayOfByte, int paramInt)
  {
    this.id = paramArrayOfByte;
    this.entryCount = paramInt;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/arsc/Config.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */