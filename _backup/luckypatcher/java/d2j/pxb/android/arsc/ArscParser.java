package pxb.android.arsc;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pxb.android.ResConst;
import pxb.android.StringItems;

public class ArscParser
  implements ResConst
{
  private static final boolean DEBUG = false;
  static final int ENGRY_FLAG_PUBLIC = 2;
  static final short ENTRY_FLAG_COMPLEX = 1;
  public static final int TYPE_STRING = 3;
  private int fileSize = -1;
  private ByteBuffer in;
  private String[] keyNamesX;
  private Pkg pkg;
  private List<Pkg> pkgs = new ArrayList();
  private String[] strings;
  private String[] typeNamesX;
  
  public ArscParser(byte[] paramArrayOfByte)
  {
    this.in = ByteBuffer.wrap(paramArrayOfByte).order(ByteOrder.LITTLE_ENDIAN);
  }
  
  private static void D(String paramString, Object... paramVarArgs) {}
  
  private void readEntry(Config paramConfig, ResSpec paramResSpec)
  {
    D("[%08x]read ResTable_entry", new Object[] { Integer.valueOf(this.in.position()) });
    D("ResTable_entry %d", new Object[] { Integer.valueOf(this.in.getShort()) });
    int i = this.in.getShort();
    int j = this.in.getInt();
    paramResSpec.updateName(this.keyNamesX[j]);
    ResEntry localResEntry = new ResEntry(i, paramResSpec);
    BagValue localBagValue;
    if ((i & 0x1) != 0)
    {
      i = this.in.getInt();
      j = this.in.getInt();
      localBagValue = new BagValue(i);
      i = 0;
      while (i < j)
      {
        AbstractMap.SimpleEntry localSimpleEntry = new AbstractMap.SimpleEntry(Integer.valueOf(this.in.getInt()), readValue());
        localBagValue.map.add(localSimpleEntry);
        i += 1;
      }
    }
    for (localResEntry.value = localBagValue;; localResEntry.value = readValue())
    {
      paramConfig.resources.put(Integer.valueOf(paramResSpec.id), localResEntry);
      return;
    }
  }
  
  private void readPackage(ByteBuffer paramByteBuffer)
    throws IOException
  {
    int j = paramByteBuffer.getInt();
    int k = paramByteBuffer.position();
    Object localObject1 = new StringBuilder(32);
    int i = 0;
    for (;;)
    {
      int m;
      if (i < 128)
      {
        m = paramByteBuffer.getShort();
        if (m != 0) {}
      }
      else
      {
        localObject1 = ((StringBuilder)localObject1).toString();
        paramByteBuffer.position(k + 256);
        this.pkg = new Pkg(j % 255, (String)localObject1);
        this.pkgs.add(this.pkg);
        paramByteBuffer.getInt();
        paramByteBuffer.getInt();
        paramByteBuffer.getInt();
        paramByteBuffer.getInt();
        localObject1 = new Chunk();
        if (((Chunk)localObject1).type == 1) {
          break;
        }
        throw new RuntimeException();
      }
      ((StringBuilder)localObject1).append((char)m);
      i += 1;
    }
    this.typeNamesX = StringItems.read(paramByteBuffer);
    paramByteBuffer.position(((Chunk)localObject1).location + ((Chunk)localObject1).size);
    localObject1 = new Chunk();
    if (((Chunk)localObject1).type != 1) {
      throw new RuntimeException();
    }
    this.keyNamesX = StringItems.read(paramByteBuffer);
    paramByteBuffer.position(((Chunk)localObject1).location + ((Chunk)localObject1).size);
    for (;;)
    {
      if (paramByteBuffer.hasRemaining()) {
        localObject1 = new Chunk();
      }
      Type localType;
      switch (((Chunk)localObject1).type)
      {
      default: 
        return;
      case 514: 
        D("[%08x]read spec", new Object[] { Integer.valueOf(paramByteBuffer.position() - 8) });
        i = paramByteBuffer.get() & 0xFF;
        paramByteBuffer.get();
        paramByteBuffer.getShort();
        j = paramByteBuffer.getInt();
        localType = this.pkg.getType(i, this.typeNamesX[(i - 1)], j);
        i = 0;
      }
      while (i < j)
      {
        localType.getSpec(i).flags = paramByteBuffer.getInt();
        i += 1;
        continue;
        D("[%08x]read config", new Object[] { Integer.valueOf(paramByteBuffer.position() - 8) });
        i = paramByteBuffer.get() & 0xFF;
        paramByteBuffer.get();
        paramByteBuffer.getShort();
        k = paramByteBuffer.getInt();
        localType = this.pkg.getType(i, this.typeNamesX[(i - 1)], k);
        j = paramByteBuffer.getInt();
        D("[%08x]read config id", new Object[] { Integer.valueOf(paramByteBuffer.position()) });
        i = paramByteBuffer.position();
        Object localObject2 = new byte[paramByteBuffer.getInt()];
        paramByteBuffer.position(i);
        paramByteBuffer.get((byte[])localObject2);
        localObject2 = new Config((byte[])localObject2, k);
        paramByteBuffer.position(((Chunk)localObject1).location + ((Chunk)localObject1).headSize);
        D("[%08x]read config entry offset", new Object[] { Integer.valueOf(paramByteBuffer.position()) });
        int[] arrayOfInt = new int[k];
        i = 0;
        while (i < k)
        {
          arrayOfInt[i] = paramByteBuffer.getInt();
          i += 1;
        }
        D("[%08x]read config entrys", new Object[] { Integer.valueOf(paramByteBuffer.position()) });
        i = 0;
        while (i < arrayOfInt.length)
        {
          if (arrayOfInt[i] != -1)
          {
            paramByteBuffer.position(((Chunk)localObject1).location + j + arrayOfInt[i]);
            readEntry((Config)localObject2, localType.getSpec(i));
          }
          i += 1;
        }
        localType.addConfig((Config)localObject2);
      }
      paramByteBuffer.position(((Chunk)localObject1).location + ((Chunk)localObject1).size);
    }
  }
  
  private Object readValue()
  {
    this.in.getShort();
    this.in.get();
    int i = this.in.get() & 0xFF;
    int j = this.in.getInt();
    String str = null;
    if (i == 3) {
      str = this.strings[j];
    }
    return new Value(i, j, str);
  }
  
  public List<Pkg> parse()
    throws IOException
  {
    Chunk localChunk;
    if (this.fileSize < 0)
    {
      localChunk = new Chunk();
      if (localChunk.type != 2) {
        throw new RuntimeException();
      }
      this.fileSize = localChunk.size;
      this.in.getInt();
    }
    if (this.in.hasRemaining())
    {
      localChunk = new Chunk();
      switch (localChunk.type)
      {
      }
      for (;;)
      {
        this.in.position(localChunk.location + localChunk.size);
        break;
        this.strings = StringItems.read(this.in);
        continue;
        readPackage(this.in);
      }
    }
    return this.pkgs;
  }
  
  class Chunk
  {
    public final int headSize = ArscParser.this.in.getShort() & 0xFFFF;
    public final int location = ArscParser.this.in.position();
    public final int size = ArscParser.this.in.getInt();
    public final int type = ArscParser.this.in.getShort() & 0xFFFF;
    
    public Chunk()
    {
      ArscParser.D("[%08x]type: %04x, headsize: %04x, size:%08x", new Object[] { Integer.valueOf(this.location), Integer.valueOf(this.type), Integer.valueOf(this.headSize), Integer.valueOf(this.size) });
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/arsc/ArscParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */