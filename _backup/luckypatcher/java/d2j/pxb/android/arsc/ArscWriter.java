package pxb.android.arsc;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import pxb.android.ResConst;
import pxb.android.StringItem;
import pxb.android.StringItems;
import pxb.android.axml.Util;

public class ArscWriter
  implements ResConst
{
  private List<PkgCtx> ctxs = new ArrayList(5);
  private List<Pkg> pkgs;
  private Map<String, StringItem> strTable = new TreeMap();
  private StringItems strTable0 = new StringItems();
  
  public ArscWriter(List<Pkg> paramList)
  {
    this.pkgs = paramList;
  }
  
  private static void D(String paramString, Object... paramVarArgs) {}
  
  private void addString(String paramString)
  {
    if (this.strTable.containsKey(paramString)) {
      return;
    }
    StringItem localStringItem = new StringItem(paramString);
    this.strTable.put(paramString, localStringItem);
    this.strTable0.add(localStringItem);
  }
  
  private int count()
  {
    int j = this.strTable0.getSize();
    int i = j;
    if (j % 4 != 0) {
      i = j + (4 - j % 4);
    }
    j = i + 8 + 12;
    Iterator localIterator1 = this.ctxs.iterator();
    while (localIterator1.hasNext())
    {
      PkgCtx localPkgCtx = (PkgCtx)localIterator1.next();
      localPkgCtx.offset = j;
      localPkgCtx.typeStringOff = (0 + 'Č' + 16);
      int k = localPkgCtx.typeNames0.getSize();
      i = k;
      if (k % 4 != 0) {
        i = k + (4 - k % 4);
      }
      int m = i + 8 + 284;
      localPkgCtx.keyStringOff = m;
      k = localPkgCtx.keyNames0.getSize();
      i = k;
      if (k % 4 != 0) {
        i = k + (4 - k % 4);
      }
      i = m + (i + 8);
      Iterator localIterator2 = localPkgCtx.pkg.types.values().iterator();
      if (localIterator2.hasNext())
      {
        Object localObject = (Type)localIterator2.next();
        ((Type)localObject).wPosition = (j + i);
        i += ((Type)localObject).specs.length * 4 + 16;
        localObject = ((Type)localObject).configs.iterator();
        for (;;)
        {
          k = i;
          i = k;
          if (!((Iterator)localObject).hasNext()) {
            break;
          }
          Config localConfig = (Config)((Iterator)localObject).next();
          localConfig.wPosition = (k + j);
          m = localConfig.id.length;
          i = m;
          if (m % 4 != 0) {
            i = m + (4 - m % 4);
          }
          if (k + 20 + i - k > 56) {
            throw new RuntimeException("config id  too big");
          }
          m = k + 56 + localConfig.entryCount * 4;
          localConfig.wEntryStart = (m - k);
          Iterator localIterator3 = localConfig.resources.values().iterator();
          i = m;
          while (localIterator3.hasNext())
          {
            ResEntry localResEntry = (ResEntry)localIterator3.next();
            localResEntry.wOffset = (i - m);
            i += 8;
            if ((localResEntry.value instanceof BagValue)) {
              i += ((BagValue)localResEntry.value).map.size() * 12 + 8;
            } else {
              i += 8;
            }
          }
          localConfig.wChunkSize = (i - k);
        }
      }
      localPkgCtx.pkgSize = i;
      j += i;
    }
    return j;
  }
  
  public static void main(String... paramVarArgs)
    throws IOException
  {
    if (paramVarArgs.length < 2)
    {
      System.err.println("asrc-write-test in.arsc out.arsc");
      return;
    }
    Util.writeFile(new ArscWriter(new ArscParser(Util.readFile(new File(paramVarArgs[0]))).parse()).toByteArray(), new File(paramVarArgs[1]));
  }
  
  private List<PkgCtx> prepare()
    throws IOException
  {
    Iterator localIterator = this.pkgs.iterator();
    while (localIterator.hasNext())
    {
      Object localObject1 = (Pkg)localIterator.next();
      PkgCtx localPkgCtx = new PkgCtx(null);
      localPkgCtx.pkg = ((Pkg)localObject1);
      this.ctxs.add(localPkgCtx);
      localObject1 = ((Pkg)localObject1).types.values().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        Object localObject2 = (Type)((Iterator)localObject1).next();
        localPkgCtx.addTypeName(((Type)localObject2).id - 1, ((Type)localObject2).name);
        Object localObject3 = ((Type)localObject2).specs;
        int j = localObject3.length;
        int i = 0;
        while (i < j)
        {
          localPkgCtx.addKeyName(localObject3[i].name);
          i += 1;
        }
        localObject2 = ((Type)localObject2).configs.iterator();
        while (((Iterator)localObject2).hasNext())
        {
          localObject3 = ((Config)((Iterator)localObject2).next()).resources.values().iterator();
          while (((Iterator)localObject3).hasNext())
          {
            Object localObject4 = ((ResEntry)((Iterator)localObject3).next()).value;
            if ((localObject4 instanceof BagValue)) {
              travelBagValue((BagValue)localObject4);
            } else {
              travelValue((Value)localObject4);
            }
          }
        }
      }
      localPkgCtx.keyNames0.prepare();
      localPkgCtx.typeNames0.addAll(localPkgCtx.typeNames);
      localPkgCtx.typeNames0.prepare();
    }
    this.strTable0.prepare();
    return this.ctxs;
  }
  
  private void travelBagValue(BagValue paramBagValue)
  {
    paramBagValue = paramBagValue.map.iterator();
    while (paramBagValue.hasNext()) {
      travelValue((Value)((Map.Entry)paramBagValue.next()).getValue());
    }
  }
  
  private void travelValue(Value paramValue)
  {
    if (paramValue.raw != null) {
      addString(paramValue.raw);
    }
  }
  
  private void write(ByteBuffer paramByteBuffer, int paramInt)
    throws IOException
  {
    paramByteBuffer.putInt(786434);
    paramByteBuffer.putInt(paramInt);
    paramByteBuffer.putInt(this.ctxs.size());
    int i = this.strTable0.getSize();
    paramInt = 0;
    if (i % 4 != 0) {
      paramInt = 4 - i % 4;
    }
    paramByteBuffer.putInt(1835009);
    paramByteBuffer.putInt(i + paramInt + 8);
    this.strTable0.write(paramByteBuffer);
    paramByteBuffer.put(new byte[paramInt]);
    Iterator localIterator1 = this.ctxs.iterator();
    while (localIterator1.hasNext())
    {
      PkgCtx localPkgCtx = (PkgCtx)localIterator1.next();
      if (paramByteBuffer.position() != localPkgCtx.offset) {
        throw new RuntimeException();
      }
      i = paramByteBuffer.position();
      paramByteBuffer.putInt(18612736);
      paramByteBuffer.putInt(localPkgCtx.pkgSize);
      paramByteBuffer.putInt(localPkgCtx.pkg.id);
      paramInt = paramByteBuffer.position();
      paramByteBuffer.put(localPkgCtx.pkg.name.getBytes("UTF-16LE"));
      paramByteBuffer.position(paramInt + 256);
      paramByteBuffer.putInt(localPkgCtx.typeStringOff);
      paramByteBuffer.putInt(localPkgCtx.typeNames0.size());
      paramByteBuffer.putInt(localPkgCtx.keyStringOff);
      paramByteBuffer.putInt(localPkgCtx.keyNames0.size());
      if (paramByteBuffer.position() - i != localPkgCtx.typeStringOff) {
        throw new RuntimeException();
      }
      int j = localPkgCtx.typeNames0.getSize();
      paramInt = 0;
      if (j % 4 != 0) {
        paramInt = 4 - j % 4;
      }
      paramByteBuffer.putInt(1835009);
      paramByteBuffer.putInt(j + paramInt + 8);
      localPkgCtx.typeNames0.write(paramByteBuffer);
      paramByteBuffer.put(new byte[paramInt]);
      if (paramByteBuffer.position() - i != localPkgCtx.keyStringOff) {
        throw new RuntimeException();
      }
      i = localPkgCtx.keyNames0.getSize();
      paramInt = 0;
      if (i % 4 != 0) {
        paramInt = 4 - i % 4;
      }
      paramByteBuffer.putInt(1835009);
      paramByteBuffer.putInt(i + paramInt + 8);
      localPkgCtx.keyNames0.write(paramByteBuffer);
      paramByteBuffer.put(new byte[paramInt]);
      Iterator localIterator2 = localPkgCtx.pkg.types.values().iterator();
      if (localIterator2.hasNext())
      {
        Type localType = (Type)localIterator2.next();
        D("[%08x]write spec", new Object[] { Integer.valueOf(paramByteBuffer.position()), localType.name });
        if (localType.wPosition != paramByteBuffer.position()) {
          throw new RuntimeException();
        }
        paramByteBuffer.putInt(1049090);
        paramByteBuffer.putInt(localType.specs.length * 4 + 16);
        paramByteBuffer.putInt(localType.id);
        paramByteBuffer.putInt(localType.specs.length);
        Object localObject1 = localType.specs;
        i = localObject1.length;
        paramInt = 0;
        while (paramInt < i)
        {
          paramByteBuffer.putInt(localObject1[paramInt].flags);
          paramInt += 1;
        }
        localObject1 = localType.configs.iterator();
        while (((Iterator)localObject1).hasNext())
        {
          Object localObject2 = (Config)((Iterator)localObject1).next();
          D("[%08x]write config", new Object[] { Integer.valueOf(paramByteBuffer.position()) });
          i = paramByteBuffer.position();
          if (((Config)localObject2).wPosition != i) {
            throw new RuntimeException();
          }
          paramByteBuffer.putInt(3670529);
          paramByteBuffer.putInt(((Config)localObject2).wChunkSize);
          paramByteBuffer.putInt(localType.id);
          paramByteBuffer.putInt(localType.specs.length);
          paramByteBuffer.putInt(((Config)localObject2).wEntryStart);
          D("[%08x]write config ids", new Object[] { Integer.valueOf(paramByteBuffer.position()) });
          paramByteBuffer.put(((Config)localObject2).id);
          j = ((Config)localObject2).id.length;
          paramInt = 0;
          if (j % 4 != 0) {
            paramInt = 4 - j % 4;
          }
          paramByteBuffer.put(new byte[paramInt]);
          paramByteBuffer.position(i + 56);
          D("[%08x]write config entry offsets", new Object[] { Integer.valueOf(paramByteBuffer.position()) });
          paramInt = 0;
          Object localObject3;
          if (paramInt < ((Config)localObject2).entryCount)
          {
            localObject3 = (ResEntry)((Config)localObject2).resources.get(Integer.valueOf(paramInt));
            if (localObject3 == null) {
              paramByteBuffer.putInt(-1);
            }
            for (;;)
            {
              paramInt += 1;
              break;
              paramByteBuffer.putInt(((ResEntry)localObject3).wOffset);
            }
          }
          if (paramByteBuffer.position() - i != ((Config)localObject2).wEntryStart) {
            throw new RuntimeException();
          }
          D("[%08x]write config entrys", new Object[] { Integer.valueOf(paramByteBuffer.position()) });
          localObject2 = ((Config)localObject2).resources.values().iterator();
          while (((Iterator)localObject2).hasNext())
          {
            localObject3 = (ResEntry)((Iterator)localObject2).next();
            D("[%08x]ResTable_entry", new Object[] { Integer.valueOf(paramByteBuffer.position()) });
            boolean bool = ((ResEntry)localObject3).value instanceof BagValue;
            if (bool)
            {
              paramInt = 16;
              label986:
              paramByteBuffer.putShort((short)paramInt);
              paramInt = ((ResEntry)localObject3).flag;
              if (!bool) {
                break label1160;
              }
              paramInt |= 0x1;
            }
            for (;;)
            {
              paramByteBuffer.putShort((short)paramInt);
              paramByteBuffer.putInt(((StringItem)localPkgCtx.keyNames.get(((ResEntry)localObject3).spec.name)).index);
              if (!bool) {
                break label1168;
              }
              localObject3 = (BagValue)((ResEntry)localObject3).value;
              paramByteBuffer.putInt(((BagValue)localObject3).parent);
              paramByteBuffer.putInt(((BagValue)localObject3).map.size());
              localObject3 = ((BagValue)localObject3).map.iterator();
              while (((Iterator)localObject3).hasNext())
              {
                Map.Entry localEntry = (Map.Entry)((Iterator)localObject3).next();
                paramByteBuffer.putInt(((Integer)localEntry.getKey()).intValue());
                writeValue((Value)localEntry.getValue(), paramByteBuffer);
              }
              break;
              paramInt = 8;
              break label986;
              label1160:
              paramInt &= 0xFFFFFFFE;
            }
            label1168:
            writeValue((Value)((ResEntry)localObject3).value, paramByteBuffer);
          }
        }
      }
    }
  }
  
  private void writeValue(Value paramValue, ByteBuffer paramByteBuffer)
  {
    paramByteBuffer.putShort((short)8);
    paramByteBuffer.put((byte)0);
    paramByteBuffer.put((byte)paramValue.type);
    if (paramValue.type == 3)
    {
      paramByteBuffer.putInt(((StringItem)this.strTable.get(paramValue.raw)).index);
      return;
    }
    paramByteBuffer.putInt(paramValue.data);
  }
  
  public byte[] toByteArray()
    throws IOException
  {
    prepare();
    int i = count();
    ByteBuffer localByteBuffer = ByteBuffer.allocate(i).order(ByteOrder.LITTLE_ENDIAN);
    write(localByteBuffer, i);
    return localByteBuffer.array();
  }
  
  private static class PkgCtx
  {
    Map<String, StringItem> keyNames = new HashMap();
    StringItems keyNames0 = new StringItems();
    public int keyStringOff;
    int offset;
    Pkg pkg;
    int pkgSize;
    List<StringItem> typeNames = new ArrayList();
    StringItems typeNames0 = new StringItems();
    int typeStringOff;
    
    public void addKeyName(String paramString)
    {
      if (this.keyNames.containsKey(paramString)) {
        return;
      }
      StringItem localStringItem = new StringItem(paramString);
      this.keyNames.put(paramString, localStringItem);
      this.keyNames0.add(localStringItem);
    }
    
    public void addTypeName(int paramInt, String paramString)
    {
      while (this.typeNames.size() <= paramInt) {
        this.typeNames.add(null);
      }
      if ((StringItem)this.typeNames.get(paramInt) == null)
      {
        this.typeNames.set(paramInt, new StringItem(paramString));
        return;
      }
      throw new RuntimeException();
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/arsc/ArscWriter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */