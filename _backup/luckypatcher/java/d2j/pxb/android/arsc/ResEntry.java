package pxb.android.arsc;

public class ResEntry
{
  public final int flag;
  public final ResSpec spec;
  public Object value;
  int wOffset;
  
  public ResEntry(int paramInt, ResSpec paramResSpec)
  {
    this.flag = paramInt;
    this.spec = paramResSpec;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/arsc/ResEntry.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */