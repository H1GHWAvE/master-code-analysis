package pxb.android.arsc;

import java.util.TreeMap;

public class Pkg
{
  public final int id;
  public String name;
  public TreeMap<Integer, Type> types = new TreeMap();
  
  public Pkg(int paramInt, String paramString)
  {
    this.id = paramInt;
    this.name = paramString;
  }
  
  public Type getType(int paramInt1, String paramString, int paramInt2)
  {
    Type localType2 = (Type)this.types.get(Integer.valueOf(paramInt1));
    Type localType1;
    if (localType2 != null)
    {
      localType1 = localType2;
      if (paramString != null)
      {
        if (localType2.name == null) {
          localType2.name = paramString;
        }
        while (paramString.endsWith(localType2.name))
        {
          localType1 = localType2;
          if (localType2.specs.length == paramInt2) {
            break;
          }
          throw new RuntimeException();
        }
        throw new RuntimeException();
      }
    }
    else
    {
      localType1 = new Type();
      localType1.id = paramInt1;
      localType1.name = paramString;
      localType1.specs = new ResSpec[paramInt2];
      this.types.put(Integer.valueOf(paramInt1), localType1);
    }
    return localType1;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/arsc/Pkg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */