package pxb.android.arsc;

import java.util.ArrayList;
import java.util.List;

public class Type
{
  public List<Config> configs = new ArrayList();
  public int id;
  public String name;
  public ResSpec[] specs;
  int wPosition;
  
  public void addConfig(Config paramConfig)
  {
    if (paramConfig.entryCount != this.specs.length) {
      throw new RuntimeException();
    }
    this.configs.add(paramConfig);
  }
  
  public ResSpec getSpec(int paramInt)
  {
    ResSpec localResSpec2 = this.specs[paramInt];
    ResSpec localResSpec1 = localResSpec2;
    if (localResSpec2 == null)
    {
      localResSpec1 = new ResSpec(paramInt);
      this.specs[paramInt] = localResSpec1;
    }
    return localResSpec1;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/arsc/Type.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */