package pxb.android.axml;

public class AxmlVisitor
  extends NodeVisitor
{
  public AxmlVisitor() {}
  
  public AxmlVisitor(NodeVisitor paramNodeVisitor)
  {
    super(paramNodeVisitor);
  }
  
  public void ns(String paramString1, String paramString2, int paramInt)
  {
    if ((this.nv != null) && ((this.nv instanceof AxmlVisitor))) {
      ((AxmlVisitor)this.nv).ns(paramString1, paramString2, paramInt);
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axml/AxmlVisitor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */