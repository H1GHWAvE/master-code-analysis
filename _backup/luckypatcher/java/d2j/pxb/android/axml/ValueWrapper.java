package pxb.android.axml;

public class ValueWrapper
{
  public static final int CLASS = 3;
  public static final int ID = 1;
  public static final int STYLE = 2;
  public final String raw;
  public final int ref;
  public final int type;
  
  private ValueWrapper(int paramInt1, int paramInt2, String paramString)
  {
    this.type = paramInt1;
    this.raw = paramString;
    this.ref = paramInt2;
  }
  
  public static ValueWrapper wrapClass(int paramInt, String paramString)
  {
    return new ValueWrapper(3, paramInt, paramString);
  }
  
  public static ValueWrapper wrapId(int paramInt, String paramString)
  {
    return new ValueWrapper(1, paramInt, paramString);
  }
  
  public static ValueWrapper wrapStyle(int paramInt, String paramString)
  {
    return new ValueWrapper(2, paramInt, paramString);
  }
  
  public ValueWrapper replaceRaw(String paramString)
  {
    return new ValueWrapper(this.type, this.ref, paramString);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axml/ValueWrapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */