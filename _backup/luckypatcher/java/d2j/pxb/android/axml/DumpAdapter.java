package pxb.android.axml;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

public class DumpAdapter
  extends AxmlVisitor
{
  protected int deep;
  protected Map<String, String> nses;
  
  public DumpAdapter()
  {
    this(null);
  }
  
  public DumpAdapter(NodeVisitor paramNodeVisitor)
  {
    this(paramNodeVisitor, 0, new HashMap());
  }
  
  public DumpAdapter(NodeVisitor paramNodeVisitor, int paramInt, Map<String, String> paramMap)
  {
    super(paramNodeVisitor);
    this.deep = paramInt;
    this.nses = paramMap;
  }
  
  public void attr(String paramString1, String paramString2, int paramInt1, int paramInt2, Object paramObject)
  {
    int i = 0;
    while (i < this.deep)
    {
      System.out.print("  ");
      i += 1;
    }
    if (paramString1 != null) {
      System.out.print(String.format("%s:", new Object[] { getPrefix(paramString1) }));
    }
    System.out.print(paramString2);
    if (paramInt1 != -1) {
      System.out.print(String.format("(%08x)", new Object[] { Integer.valueOf(paramInt1) }));
    }
    if ((paramObject instanceof String)) {
      System.out.print(String.format("=[%08x]\"%s\"", new Object[] { Integer.valueOf(paramInt2), paramObject }));
    }
    for (;;)
    {
      System.out.println();
      super.attr(paramString1, paramString2, paramInt1, paramInt2, paramObject);
      return;
      if ((paramObject instanceof Boolean))
      {
        System.out.print(String.format("=[%08x]\"%b\"", new Object[] { Integer.valueOf(paramInt2), paramObject }));
      }
      else if ((paramObject instanceof ValueWrapper))
      {
        ValueWrapper localValueWrapper = (ValueWrapper)paramObject;
        System.out.print(String.format("=[%08x]@%08x, raw: \"%s\"", new Object[] { Integer.valueOf(paramInt2), Integer.valueOf(localValueWrapper.ref), localValueWrapper.raw }));
      }
      else if (paramInt2 == 1)
      {
        System.out.print(String.format("=[%08x]@%08x", new Object[] { Integer.valueOf(paramInt2), paramObject }));
      }
      else
      {
        System.out.print(String.format("=[%08x]%08x", new Object[] { Integer.valueOf(paramInt2), paramObject }));
      }
    }
  }
  
  public NodeVisitor child(String paramString1, String paramString2)
  {
    int i = 0;
    while (i < this.deep)
    {
      System.out.print("  ");
      i += 1;
    }
    System.out.print("<");
    if (paramString1 != null) {
      System.out.print(getPrefix(paramString1) + ":");
    }
    System.out.println(paramString2);
    paramString1 = super.child(paramString1, paramString2);
    if (paramString1 != null) {
      return new DumpAdapter(paramString1, this.deep + 1, this.nses);
    }
    return null;
  }
  
  protected String getPrefix(String paramString)
  {
    if (this.nses != null)
    {
      String str = (String)this.nses.get(paramString);
      if (str != null) {
        return str;
      }
    }
    return paramString;
  }
  
  public void ns(String paramString1, String paramString2, int paramInt)
  {
    System.out.println(paramString1 + "=" + paramString2);
    this.nses.put(paramString2, paramString1);
    super.ns(paramString1, paramString2, paramInt);
  }
  
  public void text(int paramInt, String paramString)
  {
    int i = 0;
    while (i < this.deep + 1)
    {
      System.out.print("  ");
      i += 1;
    }
    System.out.print("T: ");
    System.out.println(paramString);
    super.text(paramInt, paramString);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axml/DumpAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */