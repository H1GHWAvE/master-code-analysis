package pxb.android.axml;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;
import pxb.android.StringItem;
import pxb.android.StringItems;

public class AxmlWriter
  extends AxmlVisitor
{
  static final Comparator<Attr> ATTR_CMP = new Comparator()
  {
    public int compare(AxmlWriter.Attr paramAnonymousAttr1, AxmlWriter.Attr paramAnonymousAttr2)
    {
      int j = paramAnonymousAttr1.resourceId - paramAnonymousAttr2.resourceId;
      int i = j;
      if (j == 0)
      {
        j = paramAnonymousAttr1.name.data.compareTo(paramAnonymousAttr2.name.data);
        i = j;
        if (j == 0)
        {
          if (paramAnonymousAttr1.ns != null) {
            break label78;
          }
          i = 1;
          if (paramAnonymousAttr2.ns != null) {
            break label83;
          }
          j = 1;
        }
      }
      for (;;)
      {
        if (i != 0)
        {
          if (j != 0)
          {
            i = 0;
            return i;
            label78:
            i = 0;
            break;
            label83:
            j = 0;
            continue;
          }
          return -1;
        }
      }
      if (j != 0) {
        return 1;
      }
      return paramAnonymousAttr1.ns.data.compareTo(paramAnonymousAttr2.ns.data);
    }
  };
  private List<NodeImpl> firsts = new ArrayList(3);
  private Map<String, Ns> nses = new HashMap();
  private List<StringItem> otherString = new ArrayList();
  private Map<String, StringItem> resourceId2Str = new HashMap();
  private List<Integer> resourceIds = new ArrayList();
  private List<StringItem> resourceString = new ArrayList();
  private StringItems stringItems = new StringItems();
  
  private int prepare()
    throws IOException
  {
    int i = 0;
    Object localObject = this.firsts.iterator();
    while (((Iterator)localObject).hasNext()) {
      i += ((NodeImpl)((Iterator)localObject).next()).prepare(this);
    }
    int j = 0;
    Iterator localIterator = this.nses.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      Ns localNs = (Ns)localEntry.getValue();
      localObject = localNs;
      if (localNs == null)
      {
        localObject = new Ns(null, new StringItem((String)localEntry.getKey()), 0);
        localEntry.setValue(localObject);
      }
      k = j;
      if (((Ns)localObject).prefix == null)
      {
        ((Ns)localObject).prefix = new StringItem(String.format("axml_auto_%02d", new Object[] { Integer.valueOf(j) }));
        k = j + 1;
      }
      ((Ns)localObject).prefix = update(((Ns)localObject).prefix);
      ((Ns)localObject).uri = update(((Ns)localObject).uri);
      j = k;
    }
    int m = this.nses.size();
    this.stringItems.addAll(this.resourceString);
    this.resourceString = null;
    this.stringItems.addAll(this.otherString);
    this.otherString = null;
    this.stringItems.prepare();
    int k = this.stringItems.getSize();
    j = k;
    if (k % 4 != 0) {
      j = k + (4 - k % 4);
    }
    return i + m * 24 * 2 + (j + 8) + (this.resourceIds.size() * 4 + 8);
  }
  
  public NodeVisitor child(String paramString1, String paramString2)
  {
    paramString1 = new NodeImpl(paramString1, paramString2);
    this.firsts.add(paramString1);
    return paramString1;
  }
  
  public void end() {}
  
  public void ns(String paramString1, String paramString2, int paramInt)
  {
    Map localMap = this.nses;
    if (paramString1 == null) {}
    for (paramString1 = null;; paramString1 = new StringItem(paramString1))
    {
      localMap.put(paramString2, new Ns(paramString1, new StringItem(paramString2), paramInt));
      return;
    }
  }
  
  public byte[] toByteArray()
    throws IOException
  {
    int i = prepare() + 8;
    ByteBuffer localByteBuffer = ByteBuffer.allocate(i).order(ByteOrder.LITTLE_ENDIAN);
    localByteBuffer.putInt(524291);
    localByteBuffer.putInt(i);
    int j = this.stringItems.getSize();
    i = 0;
    if (j % 4 != 0) {
      i = 4 - j % 4;
    }
    localByteBuffer.putInt(1835009);
    localByteBuffer.putInt(j + i + 8);
    this.stringItems.write(localByteBuffer);
    localByteBuffer.put(new byte[i]);
    localByteBuffer.putInt(524672);
    localByteBuffer.putInt(this.resourceIds.size() * 4 + 8);
    Object localObject1 = this.resourceIds.iterator();
    while (((Iterator)localObject1).hasNext()) {
      localByteBuffer.putInt(((Integer)((Iterator)localObject1).next()).intValue());
    }
    localObject1 = new Stack();
    Object localObject2 = this.nses.entrySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      Ns localNs = (Ns)((Map.Entry)((Iterator)localObject2).next()).getValue();
      ((Stack)localObject1).push(localNs);
      localByteBuffer.putInt(1048832);
      localByteBuffer.putInt(24);
      localByteBuffer.putInt(-1);
      localByteBuffer.putInt(-1);
      localByteBuffer.putInt(localNs.prefix.index);
      localByteBuffer.putInt(localNs.uri.index);
    }
    localObject2 = this.firsts.iterator();
    while (((Iterator)localObject2).hasNext()) {
      ((NodeImpl)((Iterator)localObject2).next()).write(localByteBuffer);
    }
    while (((Stack)localObject1).size() > 0)
    {
      localObject2 = (Ns)((Stack)localObject1).pop();
      localByteBuffer.putInt(1048833);
      localByteBuffer.putInt(24);
      localByteBuffer.putInt(((Ns)localObject2).ln);
      localByteBuffer.putInt(-1);
      localByteBuffer.putInt(((Ns)localObject2).prefix.index);
      localByteBuffer.putInt(((Ns)localObject2).uri.index);
    }
    return localByteBuffer.array();
  }
  
  StringItem update(StringItem paramStringItem)
  {
    if (paramStringItem == null) {
      return null;
    }
    int i = this.otherString.indexOf(paramStringItem);
    if (i < 0)
    {
      paramStringItem = new StringItem(paramStringItem.data);
      this.otherString.add(paramStringItem);
      return paramStringItem;
    }
    return (StringItem)this.otherString.get(i);
  }
  
  StringItem updateNs(StringItem paramStringItem)
  {
    if (paramStringItem == null) {
      return null;
    }
    String str = paramStringItem.data;
    if (!this.nses.containsKey(str)) {
      this.nses.put(str, null);
    }
    return update(paramStringItem);
  }
  
  StringItem updateWithResourceId(StringItem paramStringItem, int paramInt)
  {
    String str = paramStringItem.data + paramInt;
    StringItem localStringItem = (StringItem)this.resourceId2Str.get(str);
    if (localStringItem != null) {
      return localStringItem;
    }
    paramStringItem = new StringItem(paramStringItem.data);
    this.resourceIds.add(Integer.valueOf(paramInt));
    this.resourceString.add(paramStringItem);
    this.resourceId2Str.put(str, paramStringItem);
    return paramStringItem;
  }
  
  static class Attr
  {
    public int index;
    public StringItem name;
    public StringItem ns;
    public StringItem raw;
    public int resourceId;
    public int type;
    public Object value;
    
    public Attr(StringItem paramStringItem1, StringItem paramStringItem2, int paramInt)
    {
      this.ns = paramStringItem1;
      this.name = paramStringItem2;
      this.resourceId = paramInt;
    }
    
    public void prepare(AxmlWriter paramAxmlWriter)
    {
      this.ns = paramAxmlWriter.updateNs(this.ns);
      if (this.name != null) {
        if (this.resourceId == -1) {
          break label88;
        }
      }
      label88:
      for (this.name = paramAxmlWriter.updateWithResourceId(this.name, this.resourceId);; this.name = paramAxmlWriter.update(this.name))
      {
        if ((this.value instanceof StringItem)) {
          this.value = paramAxmlWriter.update((StringItem)this.value);
        }
        if (this.raw != null) {
          this.raw = paramAxmlWriter.update(this.raw);
        }
        return;
      }
    }
  }
  
  static class NodeImpl
    extends NodeVisitor
  {
    private Set<AxmlWriter.Attr> attrs = new TreeSet(AxmlWriter.ATTR_CMP);
    private List<NodeImpl> children = new ArrayList();
    AxmlWriter.Attr clz;
    AxmlWriter.Attr id;
    private int line;
    private StringItem name;
    private StringItem ns;
    AxmlWriter.Attr style;
    private StringItem text;
    private int textLineNumber;
    
    public NodeImpl(String paramString1, String paramString2)
    {
      super();
      if (paramString1 == null)
      {
        paramString1 = null;
        this.ns = paramString1;
        if (paramString2 != null) {
          break label67;
        }
      }
      label67:
      for (paramString1 = (String)localObject;; paramString1 = new StringItem(paramString2))
      {
        this.name = paramString1;
        return;
        paramString1 = new StringItem(paramString1);
        break;
      }
    }
    
    public void attr(String paramString1, String paramString2, int paramInt1, int paramInt2, Object paramObject)
    {
      if (paramString2 == null) {
        throw new RuntimeException("name can't be null");
      }
      if (paramString1 == null)
      {
        paramString1 = null;
        paramString1 = new AxmlWriter.Attr(paramString1, new StringItem(paramString2), paramInt1);
        paramString1.type = paramInt2;
        if (!(paramObject instanceof ValueWrapper)) {
          break label168;
        }
        paramString2 = (ValueWrapper)paramObject;
        if (paramString2.raw != null) {
          paramString1.raw = new StringItem(paramString2.raw);
        }
        paramString1.value = Integer.valueOf(paramString2.ref);
        switch (paramString2.type)
        {
        }
      }
      for (;;)
      {
        this.attrs.add(paramString1);
        return;
        paramString1 = new StringItem(paramString1);
        break;
        this.clz = paramString1;
        continue;
        this.id = paramString1;
        continue;
        this.style = paramString1;
        continue;
        label168:
        if (paramInt2 == 3)
        {
          paramString2 = new StringItem((String)paramObject);
          paramString1.raw = paramString2;
          paramString1.value = paramString2;
        }
        else
        {
          paramString1.raw = null;
          paramString1.value = paramObject;
        }
      }
    }
    
    public NodeVisitor child(String paramString1, String paramString2)
    {
      paramString1 = new NodeImpl(paramString1, paramString2);
      this.children.add(paramString1);
      return paramString1;
    }
    
    public void end() {}
    
    public void line(int paramInt)
    {
      this.line = paramInt;
    }
    
    public int prepare(AxmlWriter paramAxmlWriter)
    {
      this.ns = paramAxmlWriter.updateNs(this.ns);
      this.name = paramAxmlWriter.update(this.name);
      int i = 0;
      Iterator localIterator = this.attrs.iterator();
      while (localIterator.hasNext())
      {
        AxmlWriter.Attr localAttr = (AxmlWriter.Attr)localIterator.next();
        localAttr.index = i;
        localAttr.prepare(paramAxmlWriter);
        i += 1;
      }
      this.text = paramAxmlWriter.update(this.text);
      i = this.attrs.size() * 20 + 60;
      localIterator = this.children.iterator();
      while (localIterator.hasNext()) {
        i += ((NodeImpl)localIterator.next()).prepare(paramAxmlWriter);
      }
      int j = i;
      if (this.text != null) {
        j = i + 28;
      }
      return j;
    }
    
    public void text(int paramInt, String paramString)
    {
      this.text = new StringItem(paramString);
      this.textLineNumber = paramInt;
    }
    
    void write(ByteBuffer paramByteBuffer)
      throws IOException
    {
      int j = -1;
      paramByteBuffer.putInt(1048834);
      paramByteBuffer.putInt(this.attrs.size() * 20 + 36);
      paramByteBuffer.putInt(this.line);
      paramByteBuffer.putInt(-1);
      if (this.ns != null)
      {
        i = this.ns.index;
        paramByteBuffer.putInt(i);
        paramByteBuffer.putInt(this.name.index);
        paramByteBuffer.putInt(1310740);
        paramByteBuffer.putShort((short)this.attrs.size());
        if (this.id != null) {
          break label287;
        }
        i = 0;
        label108:
        paramByteBuffer.putShort((short)i);
        if (this.clz != null) {
          break label300;
        }
        i = 0;
        label124:
        paramByteBuffer.putShort((short)i);
        if (this.style != null) {
          break label313;
        }
        i = 0;
        label140:
        paramByteBuffer.putShort((short)i);
        localIterator = this.attrs.iterator();
      }
      for (;;)
      {
        label158:
        if (!localIterator.hasNext()) {
          break label397;
        }
        AxmlWriter.Attr localAttr = (AxmlWriter.Attr)localIterator.next();
        if (localAttr.ns == null)
        {
          i = -1;
          label190:
          paramByteBuffer.putInt(i);
          paramByteBuffer.putInt(localAttr.name.index);
          if (localAttr.raw == null) {
            break label338;
          }
        }
        Object localObject;
        label287:
        label300:
        label313:
        label338:
        for (i = localAttr.raw.index;; i = -1)
        {
          paramByteBuffer.putInt(i);
          paramByteBuffer.putInt(localAttr.type << 24 | 0x8);
          localObject = localAttr.value;
          if (!(localObject instanceof StringItem)) {
            break label343;
          }
          paramByteBuffer.putInt(((StringItem)localAttr.value).index);
          break label158;
          i = -1;
          break;
          i = this.id.index + 1;
          break label108;
          i = this.clz.index + 1;
          break label124;
          i = this.style.index + 1;
          break label140;
          i = localAttr.ns.index;
          break label190;
        }
        label343:
        if ((localObject instanceof Boolean))
        {
          if (Boolean.TRUE.equals(localObject)) {}
          for (i = -1;; i = 0)
          {
            paramByteBuffer.putInt(i);
            break;
          }
        }
        paramByteBuffer.putInt(((Integer)localAttr.value).intValue());
      }
      label397:
      if (this.text != null)
      {
        paramByteBuffer.putInt(1048836);
        paramByteBuffer.putInt(28);
        paramByteBuffer.putInt(this.textLineNumber);
        paramByteBuffer.putInt(-1);
        paramByteBuffer.putInt(this.text.index);
        paramByteBuffer.putInt(8);
        paramByteBuffer.putInt(0);
      }
      Iterator localIterator = this.children.iterator();
      while (localIterator.hasNext()) {
        ((NodeImpl)localIterator.next()).write(paramByteBuffer);
      }
      paramByteBuffer.putInt(1048835);
      paramByteBuffer.putInt(24);
      paramByteBuffer.putInt(-1);
      paramByteBuffer.putInt(-1);
      int i = j;
      if (this.ns != null) {
        i = this.ns.index;
      }
      paramByteBuffer.putInt(i);
      paramByteBuffer.putInt(this.name.index);
    }
  }
  
  static class Ns
  {
    int ln;
    StringItem prefix;
    StringItem uri;
    
    public Ns(StringItem paramStringItem1, StringItem paramStringItem2, int paramInt)
    {
      this.prefix = paramStringItem1;
      this.uri = paramStringItem2;
      this.ln = paramInt;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axml/AxmlWriter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */