package pxb.android.axml;

import java.io.IOException;
import java.util.Stack;

public class AxmlReader
{
  public static final NodeVisitor EMPTY_VISITOR = new NodeVisitor()
  {
    public NodeVisitor child(String paramAnonymousString1, String paramAnonymousString2)
    {
      return this;
    }
  };
  final AxmlParser parser;
  
  public AxmlReader(byte[] paramArrayOfByte)
  {
    this.parser = new AxmlParser(paramArrayOfByte);
  }
  
  public void accept(AxmlVisitor paramAxmlVisitor)
    throws IOException
  {
    Stack localStack = new Stack();
    Object localObject = paramAxmlVisitor;
    for (;;)
    {
      switch (this.parser.next())
      {
      case 5: 
      default: 
        break;
      case 2: 
        localStack.push(localObject);
        NodeVisitor localNodeVisitor = ((NodeVisitor)localObject).child(this.parser.getNamespaceUri(), this.parser.getName());
        if (localNodeVisitor != null)
        {
          localObject = localNodeVisitor;
          if (localNodeVisitor != EMPTY_VISITOR)
          {
            localNodeVisitor.line(this.parser.getLineNumber());
            int i = 0;
            for (;;)
            {
              localObject = localNodeVisitor;
              if (i >= this.parser.getAttrCount()) {
                break;
              }
              localNodeVisitor.attr(this.parser.getAttrNs(i), this.parser.getAttrName(i), this.parser.getAttrResId(i), this.parser.getAttrType(i), this.parser.getAttrValue(i));
              i += 1;
            }
          }
        }
        else
        {
          localObject = EMPTY_VISITOR;
        }
        break;
      case 3: 
        ((NodeVisitor)localObject).end();
        localObject = (NodeVisitor)localStack.pop();
        break;
      case 4: 
        paramAxmlVisitor.ns(this.parser.getNamespacePrefix(), this.parser.getNamespaceUri(), this.parser.getLineNumber());
        break;
      case 6: 
        ((NodeVisitor)localObject).text(this.parser.getLineNumber(), this.parser.getText());
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axml/AxmlReader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */