package pxb.android.axml;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class Util
{
  public static void copy(InputStream paramInputStream, OutputStream paramOutputStream)
    throws IOException
  {
    byte[] arrayOfByte = new byte['⠀'];
    for (int i = paramInputStream.read(arrayOfByte); i > 0; i = paramInputStream.read(arrayOfByte)) {
      paramOutputStream.write(arrayOfByte, 0, i);
    }
  }
  
  public static byte[] readFile(File paramFile)
    throws IOException
  {
    paramFile = new FileInputStream(paramFile);
    byte[] arrayOfByte = new byte[paramFile.available()];
    paramFile.read(arrayOfByte);
    paramFile.close();
    return arrayOfByte;
  }
  
  public static byte[] readIs(InputStream paramInputStream)
    throws IOException
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    copy(paramInputStream, localByteArrayOutputStream);
    return localByteArrayOutputStream.toByteArray();
  }
  
  public static Map<String, String> readProguardConfig(File paramFile)
    throws IOException
  {
    HashMap localHashMap = new HashMap();
    BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(paramFile), "utf8"));
    try
    {
      paramFile = localBufferedReader.readLine();
      if (paramFile != null)
      {
        if ((paramFile.startsWith("#")) || (paramFile.startsWith(" "))) {}
        for (;;)
        {
          paramFile = localBufferedReader.readLine();
          break;
          int i = paramFile.indexOf("->");
          if (i > 0) {
            localHashMap.put(paramFile.substring(0, i).trim(), paramFile.substring(i + 2, paramFile.length() - 1).trim());
          }
        }
      }
    }
    finally
    {
      localBufferedReader.close();
    }
    return localHashMap;
  }
  
  public static void writeFile(byte[] paramArrayOfByte, File paramFile)
    throws IOException
  {
    paramFile = new FileOutputStream(paramFile);
    paramFile.write(paramArrayOfByte);
    paramFile.close();
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axml/Util.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */