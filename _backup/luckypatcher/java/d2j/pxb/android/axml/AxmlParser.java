package pxb.android.axml;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import pxb.android.ResConst;
import pxb.android.StringItems;

public class AxmlParser
  implements ResConst
{
  public static final int END_FILE = 7;
  public static final int END_NS = 5;
  public static final int END_TAG = 3;
  public static final int START_FILE = 1;
  public static final int START_NS = 4;
  public static final int START_TAG = 2;
  public static final int TEXT = 6;
  private int attributeCount;
  private IntBuffer attrs;
  private int classAttribute;
  private int fileSize = -1;
  private int idAttribute;
  private ByteBuffer in;
  private int lineNumber;
  private int nameIdx;
  private int nsIdx;
  private int prefixIdx;
  private int[] resourceIds;
  private String[] strings;
  private int styleAttribute;
  private int textIdx;
  
  public AxmlParser(ByteBuffer paramByteBuffer)
  {
    this.in = paramByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
  }
  
  public AxmlParser(byte[] paramArrayOfByte)
  {
    this(ByteBuffer.wrap(paramArrayOfByte));
  }
  
  public int getAttrCount()
  {
    return this.attributeCount;
  }
  
  public String getAttrName(int paramInt)
  {
    paramInt = this.attrs.get(paramInt * 5 + 1);
    return this.strings[paramInt];
  }
  
  public String getAttrNs(int paramInt)
  {
    paramInt = this.attrs.get(paramInt * 5 + 0);
    if (paramInt >= 0) {
      return this.strings[paramInt];
    }
    return null;
  }
  
  String getAttrRawString(int paramInt)
  {
    paramInt = this.attrs.get(paramInt * 5 + 2);
    if (paramInt >= 0) {
      return this.strings[paramInt];
    }
    return null;
  }
  
  public int getAttrResId(int paramInt)
  {
    if (this.resourceIds != null)
    {
      paramInt = this.attrs.get(paramInt * 5 + 1);
      if ((paramInt >= 0) && (paramInt < this.resourceIds.length)) {
        return this.resourceIds[paramInt];
      }
    }
    return -1;
  }
  
  public int getAttrType(int paramInt)
  {
    return this.attrs.get(paramInt * 5 + 3) >> 24;
  }
  
  public Object getAttrValue(int paramInt)
  {
    int i = this.attrs.get(paramInt * 5 + 4);
    if (paramInt == this.idAttribute) {
      return ValueWrapper.wrapId(i, getAttrRawString(paramInt));
    }
    if (paramInt == this.styleAttribute) {
      return ValueWrapper.wrapStyle(i, getAttrRawString(paramInt));
    }
    if (paramInt == this.classAttribute) {
      return ValueWrapper.wrapClass(i, getAttrRawString(paramInt));
    }
    switch (getAttrType(paramInt))
    {
    default: 
      return Integer.valueOf(i);
    case 3: 
      return this.strings[i];
    }
    if (i != 0) {}
    for (boolean bool = true;; bool = false) {
      return Boolean.valueOf(bool);
    }
  }
  
  public int getAttributeCount()
  {
    return this.attributeCount;
  }
  
  public int getLineNumber()
  {
    return this.lineNumber;
  }
  
  public String getName()
  {
    return this.strings[this.nameIdx];
  }
  
  public String getNamespacePrefix()
  {
    return this.strings[this.prefixIdx];
  }
  
  public String getNamespaceUri()
  {
    if (this.nsIdx >= 0) {
      return this.strings[this.nsIdx];
    }
    return null;
  }
  
  public String getText()
  {
    return this.strings[this.textIdx];
  }
  
  public int next()
    throws IOException
  {
    if (this.fileSize < 0)
    {
      if ((this.in.getInt() & 0xFFFF) != 3) {
        throw new RuntimeException();
      }
      this.fileSize = this.in.getInt();
      return 1;
    }
    int j = this.in.position();
    if (j < this.fileSize)
    {
      int i = this.in.getInt();
      int k = this.in.getInt();
      switch (i & 0xFFFF)
      {
      default: 
        throw new RuntimeException();
      case 258: 
        this.lineNumber = this.in.getInt();
        this.in.getInt();
        this.nsIdx = this.in.getInt();
        this.nameIdx = this.in.getInt();
        if (this.in.getInt() != 1310740) {
          throw new RuntimeException();
        }
        this.attributeCount = (this.in.getShort() & 0xFFFF);
        this.idAttribute = ((this.in.getShort() & 0xFFFF) - 1);
        this.classAttribute = ((this.in.getShort() & 0xFFFF) - 1);
        this.styleAttribute = ((this.in.getShort() & 0xFFFF) - 1);
        this.attrs = this.in.asIntBuffer();
        i = 2;
      }
      for (;;)
      {
        this.in.position(j + k);
        return i;
        this.in.position(j + k);
        i = 3;
        continue;
        this.lineNumber = this.in.getInt();
        this.in.getInt();
        this.prefixIdx = this.in.getInt();
        this.nsIdx = this.in.getInt();
        i = 4;
        continue;
        this.in.position(j + k);
        i = 5;
        continue;
        this.strings = StringItems.read(this.in);
        this.in.position(j + k);
        for (;;)
        {
          j = this.in.position();
          break;
          int m = k / 4 - 2;
          this.resourceIds = new int[m];
          i = 0;
          while (i < m)
          {
            this.resourceIds[i] = this.in.getInt();
            i += 1;
          }
          this.in.position(j + k);
        }
        this.lineNumber = this.in.getInt();
        this.in.getInt();
        this.textIdx = this.in.getInt();
        this.in.getInt();
        this.in.getInt();
        i = 6;
      }
    }
    return 7;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/axml/AxmlParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */