package pxb.android;

public class StringItem
{
  public String data;
  public int dataOffset;
  public int index;
  
  public StringItem() {}
  
  public StringItem(String paramString)
  {
    this.data = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      do
      {
        return true;
        if (paramObject == null) {
          return false;
        }
        if (getClass() != paramObject.getClass()) {
          return false;
        }
        paramObject = (StringItem)paramObject;
        if (this.data != null) {
          break;
        }
      } while (((StringItem)paramObject).data == null);
      return false;
    } while (this.data.equals(((StringItem)paramObject).data));
    return false;
  }
  
  public int hashCode()
  {
    if (this.data == null) {}
    for (int i = 0;; i = this.data.hashCode()) {
      return i + 31;
    }
  }
  
  public String toString()
  {
    return String.format("S%04d %s", new Object[] { Integer.valueOf(this.index), this.data });
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/StringItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */