package pxb.android;

public abstract interface ResConst
{
  public static final int RES_STRING_POOL_TYPE = 1;
  public static final int RES_TABLE_PACKAGE_TYPE = 512;
  public static final int RES_TABLE_TYPE = 2;
  public static final int RES_TABLE_TYPE_SPEC_TYPE = 514;
  public static final int RES_TABLE_TYPE_TYPE = 513;
  public static final int RES_XML_CDATA_TYPE = 260;
  public static final int RES_XML_END_ELEMENT_TYPE = 259;
  public static final int RES_XML_END_NAMESPACE_TYPE = 257;
  public static final int RES_XML_RESOURCE_MAP_TYPE = 384;
  public static final int RES_XML_START_ELEMENT_TYPE = 258;
  public static final int RES_XML_START_NAMESPACE_TYPE = 256;
  public static final int RES_XML_TYPE = 3;
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/pxb/android/ResConst.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */