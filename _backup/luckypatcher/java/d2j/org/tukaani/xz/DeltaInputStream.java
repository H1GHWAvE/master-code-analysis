package org.tukaani.xz;

import java.io.IOException;
import java.io.InputStream;
import org.tukaani.xz.delta.DeltaDecoder;

public class DeltaInputStream
  extends InputStream
{
  public static final int DISTANCE_MAX = 256;
  public static final int DISTANCE_MIN = 1;
  private final DeltaDecoder delta;
  private IOException exception = null;
  private InputStream in;
  private final byte[] tempBuf = new byte[1];
  
  public DeltaInputStream(InputStream paramInputStream, int paramInt)
  {
    if (paramInputStream == null) {
      throw new NullPointerException();
    }
    this.in = paramInputStream;
    this.delta = new DeltaDecoder(paramInt);
  }
  
  public int available()
    throws IOException
  {
    if (this.in == null) {
      throw new XZIOException("Stream closed");
    }
    if (this.exception != null) {
      throw this.exception;
    }
    return this.in.available();
  }
  
  public void close()
    throws IOException
  {
    if (this.in != null) {}
    try
    {
      this.in.close();
      return;
    }
    finally
    {
      this.in = null;
    }
  }
  
  public int read()
    throws IOException
  {
    if (read(this.tempBuf, 0, 1) == -1) {
      return -1;
    }
    return this.tempBuf[0] & 0xFF;
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (paramInt2 == 0) {
      return 0;
    }
    if (this.in == null) {
      throw new XZIOException("Stream closed");
    }
    if (this.exception != null) {
      throw this.exception;
    }
    try
    {
      paramInt2 = this.in.read(paramArrayOfByte, paramInt1, paramInt2);
      if (paramInt2 == -1) {
        return -1;
      }
    }
    catch (IOException paramArrayOfByte)
    {
      this.exception = paramArrayOfByte;
      throw paramArrayOfByte;
    }
    this.delta.decode(paramArrayOfByte, paramInt1, paramInt2);
    return paramInt2;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/DeltaInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */