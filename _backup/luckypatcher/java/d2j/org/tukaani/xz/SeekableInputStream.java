package org.tukaani.xz;

import java.io.IOException;
import java.io.InputStream;

public abstract class SeekableInputStream
  extends InputStream
{
  public abstract long length()
    throws IOException;
  
  public abstract long position()
    throws IOException;
  
  public abstract void seek(long paramLong)
    throws IOException;
  
  public long skip(long paramLong)
    throws IOException
  {
    if (paramLong <= 0L) {}
    long l3;
    long l2;
    do
    {
      return 0L;
      l3 = length();
      l2 = position();
    } while (l2 >= l3);
    long l1 = paramLong;
    if (l3 - l2 < paramLong) {
      l1 = l3 - l2;
    }
    seek(l2 + l1);
    return l1;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/SeekableInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */