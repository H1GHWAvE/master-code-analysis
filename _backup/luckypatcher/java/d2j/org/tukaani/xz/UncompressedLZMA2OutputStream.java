package org.tukaani.xz;

import java.io.DataOutputStream;
import java.io.IOException;

class UncompressedLZMA2OutputStream
  extends FinishableOutputStream
{
  private boolean dictResetNeeded = true;
  private IOException exception = null;
  private boolean finished = false;
  private FinishableOutputStream out;
  private final DataOutputStream outData;
  private final byte[] tempBuf = new byte[1];
  private final byte[] uncompBuf = new byte[65536];
  private int uncompPos = 0;
  
  UncompressedLZMA2OutputStream(FinishableOutputStream paramFinishableOutputStream)
  {
    if (paramFinishableOutputStream == null) {
      throw new NullPointerException();
    }
    this.out = paramFinishableOutputStream;
    this.outData = new DataOutputStream(paramFinishableOutputStream);
  }
  
  static int getMemoryUsage()
  {
    return 70;
  }
  
  private void writeChunk()
    throws IOException
  {
    DataOutputStream localDataOutputStream = this.outData;
    if (this.dictResetNeeded) {}
    for (int i = 1;; i = 2)
    {
      localDataOutputStream.writeByte(i);
      this.outData.writeShort(this.uncompPos - 1);
      this.outData.write(this.uncompBuf, 0, this.uncompPos);
      this.uncompPos = 0;
      this.dictResetNeeded = false;
      return;
    }
  }
  
  private void writeEndMarker()
    throws IOException
  {
    if (this.exception != null) {
      throw this.exception;
    }
    if (this.finished) {
      throw new XZIOException("Stream finished or closed");
    }
    try
    {
      if (this.uncompPos > 0) {
        writeChunk();
      }
      this.out.write(0);
      return;
    }
    catch (IOException localIOException)
    {
      this.exception = localIOException;
      throw localIOException;
    }
  }
  
  public void close()
    throws IOException
  {
    if ((this.out == null) || (!this.finished)) {}
    try
    {
      writeEndMarker();
      try
      {
        this.out.close();
        this.out = null;
        if (this.exception != null) {
          throw this.exception;
        }
      }
      catch (IOException localIOException1)
      {
        for (;;)
        {
          if (this.exception == null) {
            this.exception = localIOException1;
          }
        }
      }
    }
    catch (IOException localIOException2)
    {
      for (;;) {}
    }
  }
  
  public void finish()
    throws IOException
  {
    if (!this.finished) {
      writeEndMarker();
    }
    try
    {
      this.out.finish();
      this.finished = true;
      return;
    }
    catch (IOException localIOException)
    {
      this.exception = localIOException;
      throw localIOException;
    }
  }
  
  public void flush()
    throws IOException
  {
    if (this.exception != null) {
      throw this.exception;
    }
    if (this.finished) {
      throw new XZIOException("Stream finished or closed");
    }
    try
    {
      if (this.uncompPos > 0) {
        writeChunk();
      }
      this.out.flush();
      return;
    }
    catch (IOException localIOException)
    {
      this.exception = localIOException;
      throw localIOException;
    }
  }
  
  public void write(int paramInt)
    throws IOException
  {
    this.tempBuf[0] = ((byte)paramInt);
    write(this.tempBuf, 0, 1);
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length)) {
      throw new IndexOutOfBoundsException();
    }
    if (this.exception != null) {
      throw this.exception;
    }
    if (this.finished) {
      throw new XZIOException("Stream finished or closed");
    }
    while (paramInt2 > 0) {
      try
      {
        int j = Math.min(this.uncompBuf.length - this.uncompPos, paramInt2);
        System.arraycopy(paramArrayOfByte, paramInt1, this.uncompBuf, this.uncompPos, j);
        int i = paramInt2 - j;
        this.uncompPos += j;
        paramInt2 = i;
        if (this.uncompPos == this.uncompBuf.length)
        {
          writeChunk();
          paramInt2 = i;
        }
      }
      catch (IOException paramArrayOfByte)
      {
        this.exception = paramArrayOfByte;
        throw paramArrayOfByte;
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/UncompressedLZMA2OutputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */