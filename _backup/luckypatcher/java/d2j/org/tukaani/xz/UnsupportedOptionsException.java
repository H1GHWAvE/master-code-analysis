package org.tukaani.xz;

public class UnsupportedOptionsException
  extends XZIOException
{
  private static final long serialVersionUID = 3L;
  
  public UnsupportedOptionsException() {}
  
  public UnsupportedOptionsException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/UnsupportedOptionsException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */