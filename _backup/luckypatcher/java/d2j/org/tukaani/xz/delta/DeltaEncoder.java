package org.tukaani.xz.delta;

public class DeltaEncoder
  extends DeltaCoder
{
  public DeltaEncoder(int paramInt)
  {
    super(paramInt);
  }
  
  public void encode(byte[] paramArrayOfByte1, int paramInt1, int paramInt2, byte[] paramArrayOfByte2)
  {
    int i = 0;
    while (i < paramInt2)
    {
      int j = this.history[(this.distance + this.pos & 0xFF)];
      byte[] arrayOfByte = this.history;
      int k = this.pos;
      this.pos = (k - 1);
      arrayOfByte[(k & 0xFF)] = paramArrayOfByte1[(paramInt1 + i)];
      paramArrayOfByte2[i] = ((byte)(paramArrayOfByte1[(paramInt1 + i)] - j));
      i += 1;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/delta/DeltaEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */