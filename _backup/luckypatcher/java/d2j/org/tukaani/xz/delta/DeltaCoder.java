package org.tukaani.xz.delta;

abstract class DeltaCoder
{
  static final int DISTANCE_MASK = 255;
  static final int DISTANCE_MAX = 256;
  static final int DISTANCE_MIN = 1;
  final int distance;
  final byte[] history = new byte['Ā'];
  int pos = 0;
  
  DeltaCoder(int paramInt)
  {
    if ((paramInt < 1) || (paramInt > 256)) {
      throw new IllegalArgumentException();
    }
    this.distance = paramInt;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/delta/DeltaCoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */