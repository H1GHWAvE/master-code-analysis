package org.tukaani.xz.delta;

public class DeltaDecoder
  extends DeltaCoder
{
  public DeltaDecoder(int paramInt)
  {
    super(paramInt);
  }
  
  public void decode(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = paramInt1;
    while (i < paramInt1 + paramInt2)
    {
      paramArrayOfByte[i] = ((byte)(paramArrayOfByte[i] + this.history[(this.distance + this.pos & 0xFF)]));
      byte[] arrayOfByte = this.history;
      int j = this.pos;
      this.pos = (j - 1);
      arrayOfByte[(j & 0xFF)] = paramArrayOfByte[i];
      i += 1;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/delta/DeltaDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */