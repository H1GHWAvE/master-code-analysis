package org.tukaani.xz;

import java.io.InputStream;
import org.tukaani.xz.simple.ARM;

public class ARMOptions
  extends BCJOptions
{
  private static final int ALIGNMENT = 4;
  
  public ARMOptions()
  {
    super(4);
  }
  
  FilterEncoder getFilterEncoder()
  {
    return new BCJEncoder(this, 7L);
  }
  
  public InputStream getInputStream(InputStream paramInputStream)
  {
    return new SimpleInputStream(paramInputStream, new ARM(false, this.startOffset));
  }
  
  public FinishableOutputStream getOutputStream(FinishableOutputStream paramFinishableOutputStream)
  {
    return new SimpleOutputStream(paramFinishableOutputStream, new ARM(true, this.startOffset));
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/ARMOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */