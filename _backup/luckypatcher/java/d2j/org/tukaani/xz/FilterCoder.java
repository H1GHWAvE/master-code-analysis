package org.tukaani.xz;

abstract interface FilterCoder
{
  public abstract boolean changesSize();
  
  public abstract boolean lastOK();
  
  public abstract boolean nonLastOK();
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/FilterCoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */