package org.tukaani.xz.common;

import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.CRC32;

public class EncoderUtil
  extends Util
{
  public static void encodeVLI(OutputStream paramOutputStream, long paramLong)
    throws IOException
  {
    while (paramLong >= 128L)
    {
      paramOutputStream.write((byte)(int)(paramLong | 0x80));
      paramLong >>>= 7;
    }
    paramOutputStream.write((byte)(int)paramLong);
  }
  
  public static void writeCRC32(OutputStream paramOutputStream, byte[] paramArrayOfByte)
    throws IOException
  {
    CRC32 localCRC32 = new CRC32();
    localCRC32.update(paramArrayOfByte);
    long l = localCRC32.getValue();
    int i = 0;
    while (i < 4)
    {
      paramOutputStream.write((byte)(int)(l >>> i * 8));
      i += 1;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/common/EncoderUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */