package org.tukaani.xz.common;

public class Util
{
  public static final long BACKWARD_SIZE_MAX = 17179869184L;
  public static final int BLOCK_HEADER_SIZE_MAX = 1024;
  public static final int STREAM_HEADER_SIZE = 12;
  public static final long VLI_MAX = Long.MAX_VALUE;
  public static final int VLI_SIZE_MAX = 9;
  
  public static int getVLISize(long paramLong)
  {
    int i = 0;
    int j;
    long l;
    do
    {
      j = i + 1;
      l = paramLong >> 7;
      i = j;
      paramLong = l;
    } while (l != 0L);
    return j;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/common/Util.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */