package org.tukaani.xz.common;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.CRC32;
import org.tukaani.xz.CorruptedInputException;
import org.tukaani.xz.UnsupportedOptionsException;
import org.tukaani.xz.XZ;
import org.tukaani.xz.XZFormatException;

public class DecoderUtil
  extends Util
{
  public static boolean areStreamFlagsEqual(StreamFlags paramStreamFlags1, StreamFlags paramStreamFlags2)
  {
    return paramStreamFlags1.checkType == paramStreamFlags2.checkType;
  }
  
  private static StreamFlags decodeStreamFlags(byte[] paramArrayOfByte, int paramInt)
    throws UnsupportedOptionsException
  {
    if ((paramArrayOfByte[paramInt] != 0) || ((paramArrayOfByte[(paramInt + 1)] & 0xFF) >= 16)) {
      throw new UnsupportedOptionsException();
    }
    StreamFlags localStreamFlags = new StreamFlags();
    localStreamFlags.checkType = paramArrayOfByte[(paramInt + 1)];
    return localStreamFlags;
  }
  
  public static StreamFlags decodeStreamFooter(byte[] paramArrayOfByte)
    throws IOException
  {
    if ((paramArrayOfByte[10] != XZ.FOOTER_MAGIC[0]) || (paramArrayOfByte[11] != XZ.FOOTER_MAGIC[1])) {
      throw new CorruptedInputException("XZ Stream Footer is corrupt");
    }
    if (!isCRC32Valid(paramArrayOfByte, 4, 6, 0)) {
      throw new CorruptedInputException("XZ Stream Footer is corrupt");
    }
    StreamFlags localStreamFlags;
    try
    {
      localStreamFlags = decodeStreamFlags(paramArrayOfByte, 8);
      localStreamFlags.backwardSize = 0L;
      int i = 0;
      while (i < 4)
      {
        localStreamFlags.backwardSize |= (paramArrayOfByte[(i + 4)] & 0xFF) << i * 8;
        i += 1;
      }
      localStreamFlags.backwardSize = ((localStreamFlags.backwardSize + 1L) * 4L);
    }
    catch (UnsupportedOptionsException paramArrayOfByte)
    {
      throw new UnsupportedOptionsException("Unsupported options in XZ Stream Footer");
    }
    return localStreamFlags;
  }
  
  public static StreamFlags decodeStreamHeader(byte[] paramArrayOfByte)
    throws IOException
  {
    int i = 0;
    while (i < XZ.HEADER_MAGIC.length)
    {
      if (paramArrayOfByte[i] != XZ.HEADER_MAGIC[i]) {
        throw new XZFormatException();
      }
      i += 1;
    }
    if (!isCRC32Valid(paramArrayOfByte, XZ.HEADER_MAGIC.length, 2, XZ.HEADER_MAGIC.length + 2)) {
      throw new CorruptedInputException("XZ Stream Header is corrupt");
    }
    try
    {
      paramArrayOfByte = decodeStreamFlags(paramArrayOfByte, XZ.HEADER_MAGIC.length);
      return paramArrayOfByte;
    }
    catch (UnsupportedOptionsException paramArrayOfByte)
    {
      throw new UnsupportedOptionsException("Unsupported options in XZ Stream Header");
    }
  }
  
  public static long decodeVLI(InputStream paramInputStream)
    throws IOException
  {
    int i = paramInputStream.read();
    if (i == -1) {
      throw new EOFException();
    }
    long l = i & 0x7F;
    int j = 0;
    while ((i & 0x80) != 0)
    {
      j += 1;
      if (j >= 9) {
        throw new CorruptedInputException();
      }
      i = paramInputStream.read();
      if (i == -1) {
        throw new EOFException();
      }
      if (i == 0) {
        throw new CorruptedInputException();
      }
      l |= (i & 0x7F) << j * 7;
    }
    return l;
  }
  
  public static boolean isCRC32Valid(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    CRC32 localCRC32 = new CRC32();
    localCRC32.update(paramArrayOfByte, paramInt1, paramInt2);
    long l = localCRC32.getValue();
    paramInt1 = 0;
    while (paramInt1 < 4)
    {
      if ((byte)(int)(l >>> paramInt1 * 8) != paramArrayOfByte[(paramInt3 + paramInt1)]) {
        return false;
      }
      paramInt1 += 1;
    }
    return true;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/common/DecoderUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */