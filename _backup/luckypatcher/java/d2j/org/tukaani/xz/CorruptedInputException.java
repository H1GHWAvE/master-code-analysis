package org.tukaani.xz;

public class CorruptedInputException
  extends XZIOException
{
  private static final long serialVersionUID = 3L;
  
  public CorruptedInputException()
  {
    super("Compressed data is corrupt");
  }
  
  public CorruptedInputException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/CorruptedInputException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */