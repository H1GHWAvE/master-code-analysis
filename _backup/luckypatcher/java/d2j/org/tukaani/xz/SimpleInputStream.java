package org.tukaani.xz;

import java.io.IOException;
import java.io.InputStream;
import org.tukaani.xz.simple.SimpleFilter;

class SimpleInputStream
  extends InputStream
{
  private static final int FILTER_BUF_SIZE = 4096;
  private boolean endReached = false;
  private IOException exception = null;
  private final byte[] filterBuf = new byte['က'];
  private int filtered = 0;
  private InputStream in;
  private int pos = 0;
  private final SimpleFilter simpleFilter;
  private final byte[] tempBuf = new byte[1];
  private int unfiltered = 0;
  
  static
  {
    if (!SimpleInputStream.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  SimpleInputStream(InputStream paramInputStream, SimpleFilter paramSimpleFilter)
  {
    if (paramInputStream == null) {
      throw new NullPointerException();
    }
    assert (paramSimpleFilter != null);
    this.in = paramInputStream;
    this.simpleFilter = paramSimpleFilter;
  }
  
  static int getMemoryUsage()
  {
    return 5;
  }
  
  public int available()
    throws IOException
  {
    if (this.in == null) {
      throw new XZIOException("Stream closed");
    }
    if (this.exception != null) {
      throw this.exception;
    }
    return this.filtered;
  }
  
  public void close()
    throws IOException
  {
    if (this.in != null) {}
    try
    {
      this.in.close();
      return;
    }
    finally
    {
      this.in = null;
    }
  }
  
  public int read()
    throws IOException
  {
    if (read(this.tempBuf, 0, 1) == -1) {
      return -1;
    }
    return this.tempBuf[0] & 0xFF;
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    int i = 0;
    if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length)) {
      throw new IndexOutOfBoundsException();
    }
    if (paramInt2 == 0) {
      paramInt2 = i;
    }
    do
    {
      return paramInt2;
      if (this.in == null) {
        throw new XZIOException("Stream closed");
      }
      if (this.exception != null) {
        throw this.exception;
      }
      int j = 0;
      i = paramInt1;
      paramInt1 = j;
      for (;;)
      {
        try
        {
          j = Math.min(this.filtered, paramInt2);
          System.arraycopy(this.filterBuf, this.pos, paramArrayOfByte, i, j);
          this.pos += j;
          this.filtered -= j;
          i += j;
          paramInt2 -= j;
          paramInt1 += j;
          if (this.pos + this.filtered + this.unfiltered == 4096)
          {
            System.arraycopy(this.filterBuf, this.pos, this.filterBuf, 0, this.filtered + this.unfiltered);
            this.pos = 0;
          }
          if ((paramInt2 == 0) || (this.endReached)) {
            break;
          }
          if ((!$assertionsDisabled) && (this.filtered != 0)) {
            throw new AssertionError();
          }
        }
        catch (IOException paramArrayOfByte)
        {
          this.exception = paramArrayOfByte;
          throw paramArrayOfByte;
        }
        j = this.pos;
        int k = this.filtered;
        int m = this.unfiltered;
        j = this.in.read(this.filterBuf, this.pos + this.filtered + this.unfiltered, 4096 - (j + k + m));
        if (j == -1)
        {
          this.endReached = true;
          this.filtered = this.unfiltered;
          this.unfiltered = 0;
        }
        else
        {
          this.unfiltered += j;
          this.filtered = this.simpleFilter.code(this.filterBuf, this.pos, this.unfiltered);
          assert (this.filtered <= this.unfiltered);
          this.unfiltered -= this.filtered;
        }
      }
      paramInt2 = paramInt1;
    } while (paramInt1 > 0);
    return -1;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/SimpleInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */