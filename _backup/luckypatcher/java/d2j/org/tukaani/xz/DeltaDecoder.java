package org.tukaani.xz;

import java.io.InputStream;

class DeltaDecoder
  extends DeltaCoder
  implements FilterDecoder
{
  private final int distance;
  
  DeltaDecoder(byte[] paramArrayOfByte)
    throws UnsupportedOptionsException
  {
    if (paramArrayOfByte.length != 1) {
      throw new UnsupportedOptionsException("Unsupported Delta filter properties");
    }
    this.distance = ((paramArrayOfByte[0] & 0xFF) + 1);
  }
  
  public InputStream getInputStream(InputStream paramInputStream)
  {
    return new DeltaInputStream(paramInputStream, this.distance);
  }
  
  public int getMemoryUsage()
  {
    return 1;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/DeltaDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */