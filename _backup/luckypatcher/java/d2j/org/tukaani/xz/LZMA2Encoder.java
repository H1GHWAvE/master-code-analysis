package org.tukaani.xz;

import org.tukaani.xz.lzma.LZMAEncoder;

class LZMA2Encoder
  extends LZMA2Coder
  implements FilterEncoder
{
  private final LZMA2Options options;
  private final byte[] props = new byte[1];
  
  LZMA2Encoder(LZMA2Options paramLZMA2Options)
  {
    if (paramLZMA2Options.getPresetDict() != null) {
      throw new IllegalArgumentException("XZ doesn't support a preset dictionary for now");
    }
    if (paramLZMA2Options.getMode() == 0) {
      this.props[0] = 0;
    }
    for (;;)
    {
      this.options = ((LZMA2Options)paramLZMA2Options.clone());
      return;
      int i = Math.max(paramLZMA2Options.getDictSize(), 4096);
      this.props[0] = ((byte)(LZMAEncoder.getDistSlot(i - 1) - 23));
    }
  }
  
  public long getFilterID()
  {
    return 33L;
  }
  
  public byte[] getFilterProps()
  {
    return this.props;
  }
  
  public FinishableOutputStream getOutputStream(FinishableOutputStream paramFinishableOutputStream)
  {
    return this.options.getOutputStream(paramFinishableOutputStream);
  }
  
  public boolean supportsFlushing()
  {
    return true;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/LZMA2Encoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */