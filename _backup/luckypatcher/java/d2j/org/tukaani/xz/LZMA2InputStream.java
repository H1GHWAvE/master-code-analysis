package org.tukaani.xz;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.tukaani.xz.lz.LZDecoder;
import org.tukaani.xz.lzma.LZMADecoder;
import org.tukaani.xz.rangecoder.RangeDecoderFromBuffer;

public class LZMA2InputStream
  extends InputStream
{
  private static final int COMPRESSED_SIZE_MAX = 65536;
  public static final int DICT_SIZE_MAX = 2147483632;
  public static final int DICT_SIZE_MIN = 4096;
  private boolean endReached = false;
  private IOException exception = null;
  private DataInputStream in;
  private boolean isLZMAChunk;
  private final LZDecoder lz;
  private LZMADecoder lzma;
  private boolean needDictReset = true;
  private boolean needProps = true;
  private final RangeDecoderFromBuffer rc = new RangeDecoderFromBuffer(65536);
  private final byte[] tempBuf = new byte[1];
  private int uncompressedSize = 0;
  
  public LZMA2InputStream(InputStream paramInputStream, int paramInt)
  {
    this(paramInputStream, paramInt, null);
  }
  
  public LZMA2InputStream(InputStream paramInputStream, int paramInt, byte[] paramArrayOfByte)
  {
    if (paramInputStream == null) {
      throw new NullPointerException();
    }
    this.in = new DataInputStream(paramInputStream);
    this.lz = new LZDecoder(getDictSize(paramInt), paramArrayOfByte);
    if ((paramArrayOfByte != null) && (paramArrayOfByte.length > 0)) {
      this.needDictReset = false;
    }
  }
  
  private void decodeChunkHeader()
    throws IOException
  {
    int i = this.in.readUnsignedByte();
    if (i == 0)
    {
      this.endReached = true;
      return;
    }
    int j;
    if ((i >= 224) || (i == 1))
    {
      this.needProps = true;
      this.needDictReset = false;
      this.lz.reset();
      if (i < 128) {
        break label174;
      }
      this.isLZMAChunk = true;
      this.uncompressedSize = ((i & 0x1F) << 16);
      this.uncompressedSize += this.in.readUnsignedShort() + 1;
      j = this.in.readUnsignedShort();
      if (i < 192) {
        break label142;
      }
      this.needProps = false;
      decodeProps();
    }
    for (;;)
    {
      this.rc.prepareInputBuffer(this.in, j + 1);
      return;
      if (!this.needDictReset) {
        break;
      }
      throw new CorruptedInputException();
      label142:
      if (this.needProps) {
        throw new CorruptedInputException();
      }
      if (i >= 160) {
        this.lzma.reset();
      }
    }
    label174:
    if (i > 2) {
      throw new CorruptedInputException();
    }
    this.isLZMAChunk = false;
    this.uncompressedSize = (this.in.readUnsignedShort() + 1);
  }
  
  private void decodeProps()
    throws IOException
  {
    int j = this.in.readUnsignedByte();
    if (j > 224) {
      throw new CorruptedInputException();
    }
    int i = j / 45;
    int k = j - i * 9 * 5;
    j = k / 9;
    k -= j * 9;
    if (k + j > 4) {
      throw new CorruptedInputException();
    }
    this.lzma = new LZMADecoder(this.lz, this.rc, k, j, i);
  }
  
  private static int getDictSize(int paramInt)
  {
    if ((paramInt < 4096) || (paramInt > 2147483632)) {
      throw new IllegalArgumentException("Unsupported dictionary size " + paramInt);
    }
    return paramInt + 15 & 0xFFFFFFF0;
  }
  
  public static int getMemoryUsage(int paramInt)
  {
    return getDictSize(paramInt) / 1024 + 104;
  }
  
  public int available()
    throws IOException
  {
    if (this.in == null) {
      throw new XZIOException("Stream closed");
    }
    if (this.exception != null) {
      throw this.exception;
    }
    return this.uncompressedSize;
  }
  
  public void close()
    throws IOException
  {
    if (this.in != null) {}
    try
    {
      this.in.close();
      return;
    }
    finally
    {
      this.in = null;
    }
  }
  
  public int read()
    throws IOException
  {
    if (read(this.tempBuf, 0, 1) == -1) {
      return -1;
    }
    return this.tempBuf[0] & 0xFF;
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length)) {
      throw new IndexOutOfBoundsException();
    }
    int j;
    if (paramInt2 == 0) {
      j = 0;
    }
    int i;
    label87:
    do
    {
      return j;
      if (this.in == null) {
        throw new XZIOException("Stream closed");
      }
      if (this.exception != null) {
        throw this.exception;
      }
      if (this.endReached) {
        return -1;
      }
      j = 0;
      i = paramInt1;
      paramInt1 = j;
      j = paramInt1;
    } while (paramInt2 <= 0);
    do
    {
      try
      {
        if (this.uncompressedSize == 0)
        {
          decodeChunkHeader();
          if (this.endReached)
          {
            j = paramInt1;
            if (paramInt1 != 0) {
              break;
            }
            return -1;
          }
        }
        j = Math.min(this.uncompressedSize, paramInt2);
        if (!this.isLZMAChunk)
        {
          this.lz.copyUncompressed(this.in, j);
          int n = this.lz.flush(paramArrayOfByte, i);
          j = i + n;
          int k = paramInt2 - n;
          int m = paramInt1 + n;
          this.uncompressedSize -= n;
          paramInt1 = m;
          i = j;
          paramInt2 = k;
          if (this.uncompressedSize != 0) {
            break label87;
          }
          if (this.rc.isFinished())
          {
            paramInt1 = m;
            i = j;
            paramInt2 = k;
            if (!this.lz.hasPending()) {
              break label87;
            }
          }
          throw new CorruptedInputException();
        }
      }
      catch (IOException paramArrayOfByte)
      {
        this.exception = paramArrayOfByte;
        throw paramArrayOfByte;
      }
      this.lz.setLimit(j);
      this.lzma.decode();
    } while (this.rc.isInBufferOK());
    throw new CorruptedInputException();
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/LZMA2InputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */