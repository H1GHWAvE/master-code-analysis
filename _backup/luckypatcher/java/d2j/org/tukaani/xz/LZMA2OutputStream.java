package org.tukaani.xz;

import java.io.DataOutputStream;
import java.io.IOException;
import org.tukaani.xz.lz.LZEncoder;
import org.tukaani.xz.lzma.LZMAEncoder;
import org.tukaani.xz.rangecoder.RangeEncoder;

class LZMA2OutputStream
  extends FinishableOutputStream
{
  static final int COMPRESSED_SIZE_MAX = 65536;
  private boolean dictResetNeeded = true;
  private IOException exception = null;
  private boolean finished = false;
  private final LZEncoder lz;
  private final LZMAEncoder lzma;
  private FinishableOutputStream out;
  private final DataOutputStream outData;
  private int pendingSize = 0;
  private final int props;
  private boolean propsNeeded = true;
  private final RangeEncoder rc;
  private boolean stateResetNeeded = true;
  private final byte[] tempBuf = new byte[1];
  
  static
  {
    if (!LZMA2OutputStream.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  LZMA2OutputStream(FinishableOutputStream paramFinishableOutputStream, LZMA2Options paramLZMA2Options)
  {
    if (paramFinishableOutputStream == null) {
      throw new NullPointerException();
    }
    this.out = paramFinishableOutputStream;
    this.outData = new DataOutputStream(paramFinishableOutputStream);
    this.rc = new RangeEncoder(65536);
    int i = paramLZMA2Options.getDictSize();
    int j = getExtraSizeBefore(i);
    this.lzma = LZMAEncoder.getInstance(this.rc, paramLZMA2Options.getLc(), paramLZMA2Options.getLp(), paramLZMA2Options.getPb(), paramLZMA2Options.getMode(), i, j, paramLZMA2Options.getNiceLen(), paramLZMA2Options.getMatchFinder(), paramLZMA2Options.getDepthLimit());
    this.lz = this.lzma.getLZEncoder();
    paramFinishableOutputStream = paramLZMA2Options.getPresetDict();
    if ((paramFinishableOutputStream != null) && (paramFinishableOutputStream.length > 0))
    {
      this.lz.setPresetDict(i, paramFinishableOutputStream);
      this.dictResetNeeded = false;
    }
    this.props = ((paramLZMA2Options.getPb() * 5 + paramLZMA2Options.getLp()) * 9 + paramLZMA2Options.getLc());
  }
  
  private static int getExtraSizeBefore(int paramInt)
  {
    if (65536 > paramInt) {
      return 65536 - paramInt;
    }
    return 0;
  }
  
  static int getMemoryUsage(LZMA2Options paramLZMA2Options)
  {
    int i = paramLZMA2Options.getDictSize();
    int j = getExtraSizeBefore(i);
    return LZMAEncoder.getMemoryUsage(paramLZMA2Options.getMode(), i, j, paramLZMA2Options.getMatchFinder()) + 70;
  }
  
  private void writeChunk()
    throws IOException
  {
    int j = this.rc.finish();
    int i = this.lzma.getUncompressedSize();
    assert (j > 0) : j;
    assert (i > 0) : i;
    if (j + 2 < i) {
      writeLZMA(i, j);
    }
    for (;;)
    {
      this.pendingSize -= i;
      this.lzma.resetUncompressedSize();
      this.rc.reset();
      return;
      this.lzma.reset();
      i = this.lzma.getUncompressedSize();
      assert (i > 0) : i;
      writeUncompressed(i);
    }
  }
  
  private void writeEndMarker()
    throws IOException
  {
    assert (!this.finished);
    if (this.exception != null) {
      throw this.exception;
    }
    this.lz.setFinishing();
    try
    {
      while (this.pendingSize > 0)
      {
        this.lzma.encodeForLZMA2();
        writeChunk();
      }
      this.out.write(0);
    }
    catch (IOException localIOException)
    {
      this.exception = localIOException;
      throw localIOException;
    }
    this.finished = true;
  }
  
  private void writeLZMA(int paramInt1, int paramInt2)
    throws IOException
  {
    int i;
    if (this.propsNeeded) {
      if (this.dictResetNeeded) {
        i = 224;
      }
    }
    for (;;)
    {
      this.outData.writeByte(i | paramInt1 - 1 >>> 16);
      this.outData.writeShort(paramInt1 - 1);
      this.outData.writeShort(paramInt2 - 1);
      if (this.propsNeeded) {
        this.outData.writeByte(this.props);
      }
      this.rc.write(this.out);
      this.propsNeeded = false;
      this.stateResetNeeded = false;
      this.dictResetNeeded = false;
      return;
      i = 192;
      continue;
      if (this.stateResetNeeded) {
        i = 160;
      } else {
        i = 128;
      }
    }
  }
  
  private void writeUncompressed(int paramInt)
    throws IOException
  {
    if (paramInt > 0)
    {
      int j = Math.min(paramInt, 65536);
      DataOutputStream localDataOutputStream = this.outData;
      if (this.dictResetNeeded) {}
      for (int i = 1;; i = 2)
      {
        localDataOutputStream.writeByte(i);
        this.outData.writeShort(j - 1);
        this.lz.copyUncompressed(this.out, paramInt, j);
        paramInt -= j;
        this.dictResetNeeded = false;
        break;
      }
    }
    this.stateResetNeeded = true;
  }
  
  public void close()
    throws IOException
  {
    if ((this.out == null) || (!this.finished)) {}
    try
    {
      writeEndMarker();
      try
      {
        this.out.close();
        this.out = null;
        if (this.exception != null) {
          throw this.exception;
        }
      }
      catch (IOException localIOException1)
      {
        for (;;)
        {
          if (this.exception == null) {
            this.exception = localIOException1;
          }
        }
      }
    }
    catch (IOException localIOException2)
    {
      for (;;) {}
    }
  }
  
  public void finish()
    throws IOException
  {
    if (!this.finished) {
      writeEndMarker();
    }
    try
    {
      this.out.finish();
      this.finished = true;
      return;
    }
    catch (IOException localIOException)
    {
      this.exception = localIOException;
      throw localIOException;
    }
  }
  
  public void flush()
    throws IOException
  {
    if (this.exception != null) {
      throw this.exception;
    }
    if (this.finished) {
      throw new XZIOException("Stream finished or closed");
    }
    try
    {
      this.lz.setFlushing();
      while (this.pendingSize > 0)
      {
        this.lzma.encodeForLZMA2();
        writeChunk();
      }
      this.out.flush();
    }
    catch (IOException localIOException)
    {
      this.exception = localIOException;
      throw localIOException;
    }
  }
  
  public void write(int paramInt)
    throws IOException
  {
    this.tempBuf[0] = ((byte)paramInt);
    write(this.tempBuf, 0, 1);
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length)) {
      throw new IndexOutOfBoundsException();
    }
    if (this.exception != null) {
      throw this.exception;
    }
    if (this.finished) {
      throw new XZIOException("Stream finished or closed");
    }
    while (paramInt2 > 0) {
      try
      {
        int k = this.lz.fillWindow(paramArrayOfByte, paramInt1, paramInt2);
        int i = paramInt1 + k;
        int j = paramInt2 - k;
        this.pendingSize += k;
        paramInt1 = i;
        paramInt2 = j;
        if (this.lzma.encodeForLZMA2())
        {
          writeChunk();
          paramInt1 = i;
          paramInt2 = j;
        }
      }
      catch (IOException paramArrayOfByte)
      {
        this.exception = paramArrayOfByte;
        throw paramArrayOfByte;
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/LZMA2OutputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */