package org.tukaani.xz.rangecoder;

import java.util.Arrays;

public abstract class RangeCoder
{
  static final int BIT_MODEL_TOTAL = 2048;
  static final int BIT_MODEL_TOTAL_BITS = 11;
  static final int MOVE_BITS = 5;
  static final short PROB_INIT = 1024;
  static final int SHIFT_BITS = 8;
  static final int TOP_MASK = -16777216;
  
  public static final void initProbs(short[] paramArrayOfShort)
  {
    Arrays.fill(paramArrayOfShort, (short)1024);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/rangecoder/RangeCoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */