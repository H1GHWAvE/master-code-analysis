package org.tukaani.xz.rangecoder;

import java.io.IOException;

public abstract class RangeDecoder
  extends RangeCoder
{
  int code = 0;
  int range = 0;
  
  public int decodeBit(short[] paramArrayOfShort, int paramInt)
    throws IOException
  {
    normalize();
    int i = paramArrayOfShort[paramInt];
    int j = (this.range >>> 11) * i;
    if ((this.code ^ 0x80000000) < (0x80000000 ^ j))
    {
      this.range = j;
      paramArrayOfShort[paramInt] = ((short)((2048 - i >>> 5) + i));
      return 0;
    }
    this.range -= j;
    this.code -= j;
    paramArrayOfShort[paramInt] = ((short)(i - (i >>> 5)));
    return 1;
  }
  
  public int decodeBitTree(short[] paramArrayOfShort)
    throws IOException
  {
    int i = 1;
    int j;
    do
    {
      j = i << 1 | decodeBit(paramArrayOfShort, i);
      i = j;
    } while (j < paramArrayOfShort.length);
    return j - paramArrayOfShort.length;
  }
  
  public int decodeDirectBits(int paramInt)
    throws IOException
  {
    int i = 0;
    int j;
    int k;
    do
    {
      normalize();
      this.range >>>= 1;
      j = this.code - this.range >>> 31;
      this.code -= (this.range & j - 1);
      j = i << 1 | 1 - j;
      k = paramInt - 1;
      i = j;
      paramInt = k;
    } while (k != 0);
    return j;
  }
  
  public int decodeReverseBitTree(short[] paramArrayOfShort)
    throws IOException
  {
    int k = 1;
    int i = 0;
    int j = 0;
    for (;;)
    {
      int m = decodeBit(paramArrayOfShort, k);
      k = k << 1 | m;
      j |= m << i;
      if (k >= paramArrayOfShort.length) {
        return j;
      }
      i += 1;
    }
  }
  
  public abstract void normalize()
    throws IOException;
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/rangecoder/RangeDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */