package org.tukaani.xz.rangecoder;

import java.io.DataInputStream;
import java.io.IOException;
import org.tukaani.xz.CorruptedInputException;

public final class RangeDecoderFromBuffer
  extends RangeDecoder
{
  private static final int INIT_SIZE = 5;
  private final byte[] buf;
  private int end = 0;
  private int pos = 0;
  
  public RangeDecoderFromBuffer(int paramInt)
  {
    this.buf = new byte[paramInt - 5];
  }
  
  public boolean isFinished()
  {
    return (this.pos == this.end) && (this.code == 0);
  }
  
  public boolean isInBufferOK()
  {
    return this.pos <= this.end;
  }
  
  public void normalize()
    throws IOException
  {
    if ((this.range & 0xFF000000) == 0) {}
    try
    {
      int i = this.code;
      byte[] arrayOfByte = this.buf;
      int j = this.pos;
      this.pos = (j + 1);
      this.code = (i << 8 | arrayOfByte[j] & 0xFF);
      this.range <<= 8;
      return;
    }
    catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
    {
      throw new CorruptedInputException();
    }
  }
  
  public void prepareInputBuffer(DataInputStream paramDataInputStream, int paramInt)
    throws IOException
  {
    if (paramInt < 5) {
      throw new CorruptedInputException();
    }
    if (paramDataInputStream.readUnsignedByte() != 0) {
      throw new CorruptedInputException();
    }
    this.code = paramDataInputStream.readInt();
    this.range = -1;
    this.pos = 0;
    this.end = (paramInt - 5);
    paramDataInputStream.readFully(this.buf, 0, this.end);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/rangecoder/RangeDecoderFromBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */