package org.tukaani.xz.rangecoder;

import java.io.IOException;
import java.io.OutputStream;

public final class RangeEncoder
  extends RangeCoder
{
  private static final int BIT_PRICE_SHIFT_BITS = 4;
  private static final int MOVE_REDUCING_BITS = 4;
  private static final int[] prices;
  private final byte[] buf;
  private int bufPos;
  private byte cache;
  private int cacheSize;
  private long low;
  private int range;
  
  static
  {
    boolean bool;
    int i;
    if (!RangeEncoder.class.desiredAssertionStatus())
    {
      bool = true;
      $assertionsDisabled = bool;
      prices = new int[''];
      i = 8;
    }
    for (;;)
    {
      if (i >= 2048) {
        return;
      }
      int j = i;
      int k = 0;
      int m = 0;
      for (;;)
      {
        if (m >= 4) {
          break label84;
        }
        j *= j;
        k <<= 1;
        for (;;)
        {
          if ((0xFFFF0000 & j) != 0)
          {
            j >>>= 1;
            k += 1;
            continue;
            bool = false;
            break;
          }
        }
        m += 1;
      }
      label84:
      prices[(i >> 4)] = (161 - k);
      i += 16;
    }
  }
  
  public RangeEncoder(int paramInt)
  {
    this.buf = new byte[paramInt];
    reset();
  }
  
  public static int getBitPrice(int paramInt1, int paramInt2)
  {
    assert ((paramInt2 == 0) || (paramInt2 == 1));
    return prices[((-paramInt2 & 0x7FF ^ paramInt1) >>> 4)];
  }
  
  public static int getBitTreePrice(short[] paramArrayOfShort, int paramInt)
  {
    int i = 0;
    paramInt |= paramArrayOfShort.length;
    int j;
    int k;
    do
    {
      j = paramInt >>> 1;
      k = i + getBitPrice(paramArrayOfShort[j], paramInt & 0x1);
      i = k;
      paramInt = j;
    } while (j != 1);
    return k;
  }
  
  public static int getDirectBitsPrice(int paramInt)
  {
    return paramInt << 4;
  }
  
  public static int getReverseBitTreePrice(short[] paramArrayOfShort, int paramInt)
  {
    int i = 0;
    int j = 1;
    paramInt |= paramArrayOfShort.length;
    int k;
    int m;
    do
    {
      int n = paramInt & 0x1;
      k = paramInt >>> 1;
      m = i + getBitPrice(paramArrayOfShort[j], n);
      j = j << 1 | n;
      i = m;
      paramInt = k;
    } while (k != 1);
    return m;
  }
  
  private void shiftLow()
  {
    int j = (int)(this.low >>> 32);
    if ((j != 0) || (this.low < 4278190080L))
    {
      int i = this.cache;
      int k;
      do
      {
        byte[] arrayOfByte = this.buf;
        k = this.bufPos;
        this.bufPos = (k + 1);
        arrayOfByte[k] = ((byte)(i + j));
        i = 255;
        k = this.cacheSize - 1;
        this.cacheSize = k;
      } while (k != 0);
      this.cache = ((byte)(int)(this.low >>> 24));
    }
    this.cacheSize += 1;
    this.low = ((this.low & 0xFFFFFF) << 8);
  }
  
  public void encodeBit(short[] paramArrayOfShort, int paramInt1, int paramInt2)
  {
    int i = paramArrayOfShort[paramInt1];
    int j = (this.range >>> 11) * i;
    if (paramInt2 == 0)
    {
      this.range = j;
      paramArrayOfShort[paramInt1] = ((short)((2048 - i >>> 5) + i));
    }
    for (;;)
    {
      if ((this.range & 0xFF000000) == 0)
      {
        this.range <<= 8;
        shiftLow();
      }
      return;
      this.low += (j & 0xFFFFFFFF);
      this.range -= j;
      paramArrayOfShort[paramInt1] = ((short)(i - (i >>> 5)));
    }
  }
  
  public void encodeBitTree(short[] paramArrayOfShort, int paramInt)
  {
    int i = 1;
    int j = paramArrayOfShort.length;
    int k;
    do
    {
      k = j >>> 1;
      int m = paramInt & k;
      encodeBit(paramArrayOfShort, i, m);
      j = i << 1;
      i = j;
      if (m != 0) {
        i = j | 0x1;
      }
      j = k;
    } while (k != 1);
  }
  
  public void encodeDirectBits(int paramInt1, int paramInt2)
  {
    int i;
    do
    {
      this.range >>>= 1;
      long l = this.low;
      int j = this.range;
      i = paramInt2 - 1;
      this.low = (l + (j & 0 - (paramInt1 >>> i & 0x1)));
      if ((this.range & 0xFF000000) == 0)
      {
        this.range <<= 8;
        shiftLow();
      }
      paramInt2 = i;
    } while (i != 0);
  }
  
  public void encodeReverseBitTree(short[] paramArrayOfShort, int paramInt)
  {
    int i = 1;
    paramInt |= paramArrayOfShort.length;
    int j;
    do
    {
      int k = paramInt & 0x1;
      j = paramInt >>> 1;
      encodeBit(paramArrayOfShort, i, k);
      i = i << 1 | k;
      paramInt = j;
    } while (j != 1);
  }
  
  public int finish()
  {
    int i = 0;
    while (i < 5)
    {
      shiftLow();
      i += 1;
    }
    return this.bufPos;
  }
  
  public int getPendingSize()
  {
    return this.bufPos + this.cacheSize + 5 - 1;
  }
  
  public void reset()
  {
    this.low = 0L;
    this.range = -1;
    this.cache = 0;
    this.cacheSize = 1;
    this.bufPos = 0;
  }
  
  public void write(OutputStream paramOutputStream)
    throws IOException
  {
    paramOutputStream.write(this.buf, 0, this.bufPos);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/rangecoder/RangeEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */