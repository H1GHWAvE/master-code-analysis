package org.tukaani.xz.lzma;

import java.lang.reflect.Array;
import org.tukaani.xz.lz.LZEncoder;
import org.tukaani.xz.lz.Matches;
import org.tukaani.xz.rangecoder.RangeEncoder;

public abstract class LZMAEncoder
  extends LZMACoder
{
  private static final int ALIGN_PRICE_UPDATE_INTERVAL = 16;
  private static final int DIST_PRICE_UPDATE_INTERVAL = 128;
  private static final int LZMA2_COMPRESSED_LIMIT = 65510;
  private static final int LZMA2_UNCOMPRESSED_LIMIT = 2096879;
  public static final int MODE_FAST = 1;
  public static final int MODE_NORMAL = 2;
  private int alignPriceCount = 0;
  private final int[] alignPrices = new int[16];
  int back = 0;
  private int distPriceCount = 0;
  private final int[][] distSlotPrices;
  private final int distSlotPricesSize;
  private final int[][] fullDistPrices = (int[][])Array.newInstance(Integer.TYPE, new int[] { 4, 128 });
  final LiteralEncoder literalEncoder;
  final LZEncoder lz;
  final LengthEncoder matchLenEncoder;
  final int niceLen;
  private final RangeEncoder rc;
  int readAhead = -1;
  final LengthEncoder repLenEncoder;
  private int uncompressedSize = 0;
  
  static
  {
    if (!LZMAEncoder.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  LZMAEncoder(RangeEncoder paramRangeEncoder, LZEncoder paramLZEncoder, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    super(paramInt3);
    this.rc = paramRangeEncoder;
    this.lz = paramLZEncoder;
    this.niceLen = paramInt5;
    this.literalEncoder = new LiteralEncoder(paramInt1, paramInt2);
    this.matchLenEncoder = new LengthEncoder(paramInt3, paramInt5);
    this.repLenEncoder = new LengthEncoder(paramInt3, paramInt5);
    this.distSlotPricesSize = (getDistSlot(paramInt4 - 1) + 1);
    paramInt1 = this.distSlotPricesSize;
    this.distSlotPrices = ((int[][])Array.newInstance(Integer.TYPE, new int[] { 4, paramInt1 }));
    reset();
  }
  
  private boolean encodeInit()
  {
    assert (this.readAhead == -1);
    if (!this.lz.hasEnoughData(0)) {
      return false;
    }
    skip(1);
    this.rc.encodeBit(this.isMatch[this.state.get()], 0, 0);
    this.literalEncoder.encodeInit();
    this.readAhead -= 1;
    assert (this.readAhead == -1);
    this.uncompressedSize += 1;
    assert (this.uncompressedSize == 1);
    return true;
  }
  
  private void encodeMatch(int paramInt1, int paramInt2, int paramInt3)
  {
    this.state.updateMatch();
    this.matchLenEncoder.encode(paramInt2, paramInt3);
    paramInt3 = getDistSlot(paramInt1);
    this.rc.encodeBitTree(this.distSlots[getDistState(paramInt2)], paramInt3);
    int i;
    if (paramInt3 >= 4)
    {
      paramInt2 = (paramInt3 >>> 1) - 1;
      i = paramInt1 - ((paramInt3 & 0x1 | 0x2) << paramInt2);
      if (paramInt3 >= 14) {
        break label137;
      }
      this.rc.encodeReverseBitTree(this.distSpecial[(paramInt3 - 4)], i);
    }
    for (;;)
    {
      this.reps[3] = this.reps[2];
      this.reps[2] = this.reps[1];
      this.reps[1] = this.reps[0];
      this.reps[0] = paramInt1;
      this.distPriceCount -= 1;
      return;
      label137:
      this.rc.encodeDirectBits(i >>> 4, paramInt2 - 4);
      this.rc.encodeReverseBitTree(this.distAlign, i & 0xF);
      this.alignPriceCount -= 1;
    }
  }
  
  private void encodeRepMatch(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = 0;
    if (paramInt1 == 0)
    {
      this.rc.encodeBit(this.isRep0, this.state.get(), 0);
      RangeEncoder localRangeEncoder = this.rc;
      short[] arrayOfShort = this.isRep0Long[this.state.get()];
      if (paramInt2 == 1) {}
      for (paramInt1 = i;; paramInt1 = 1)
      {
        localRangeEncoder.encodeBit(arrayOfShort, paramInt3, paramInt1);
        if (paramInt2 != 1) {
          break;
        }
        this.state.updateShortRep();
        return;
      }
    }
    i = this.reps[paramInt1];
    this.rc.encodeBit(this.isRep0, this.state.get(), 1);
    if (paramInt1 == 1) {
      this.rc.encodeBit(this.isRep1, this.state.get(), 0);
    }
    for (;;)
    {
      this.reps[1] = this.reps[0];
      this.reps[0] = i;
      break;
      this.rc.encodeBit(this.isRep1, this.state.get(), 1);
      this.rc.encodeBit(this.isRep2, this.state.get(), paramInt1 - 2);
      if (paramInt1 == 3) {
        this.reps[3] = this.reps[2];
      }
      this.reps[2] = this.reps[1];
    }
    this.repLenEncoder.encode(paramInt2, paramInt3);
    this.state.updateLongRep();
  }
  
  private boolean encodeSymbol()
  {
    if (!this.lz.hasEnoughData(this.readAhead + 1)) {
      return false;
    }
    int i = getNextSymbol();
    assert (this.readAhead >= 0);
    int j = this.lz.getPos() - this.readAhead & this.posMask;
    if (this.back == -1)
    {
      assert (i == 1);
      this.rc.encodeBit(this.isMatch[this.state.get()], j, 0);
      this.literalEncoder.encode();
    }
    for (;;)
    {
      this.readAhead -= i;
      this.uncompressedSize += i;
      return true;
      this.rc.encodeBit(this.isMatch[this.state.get()], j, 1);
      if (this.back < 4)
      {
        assert (this.lz.getMatchLen(-this.readAhead, this.reps[this.back], i) == i);
        this.rc.encodeBit(this.isRep, this.state.get(), 1);
        encodeRepMatch(this.back, i, j);
      }
      else
      {
        assert (this.lz.getMatchLen(-this.readAhead, this.back - 4, i) == i);
        this.rc.encodeBit(this.isRep, this.state.get(), 0);
        encodeMatch(this.back - 4, i, j);
      }
    }
  }
  
  public static int getDistSlot(int paramInt)
  {
    if (paramInt <= 4) {
      return paramInt;
    }
    int i = paramInt;
    int k = 31;
    int m = i;
    if ((0xFFFF0000 & i) == 0)
    {
      m = i << 16;
      k = 15;
    }
    i = k;
    int j = m;
    if ((0xFF000000 & m) == 0)
    {
      j = m << 8;
      i = k - 8;
    }
    k = i;
    m = j;
    if ((0xF0000000 & j) == 0)
    {
      m = j << 4;
      k = i - 4;
    }
    i = k;
    j = m;
    if ((0xC0000000 & m) == 0)
    {
      j = m << 2;
      i = k - 2;
    }
    k = i;
    if ((0x80000000 & j) == 0) {
      k = i - 1;
    }
    return (k << 1) + (paramInt >>> k - 1 & 0x1);
  }
  
  public static LZMAEncoder getInstance(RangeEncoder paramRangeEncoder, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9)
  {
    switch (paramInt4)
    {
    default: 
      throw new IllegalArgumentException();
    case 1: 
      return new LZMAEncoderFast(paramRangeEncoder, paramInt1, paramInt2, paramInt3, paramInt5, paramInt6, paramInt7, paramInt8, paramInt9);
    }
    return new LZMAEncoderNormal(paramRangeEncoder, paramInt1, paramInt2, paramInt3, paramInt5, paramInt6, paramInt7, paramInt8, paramInt9);
  }
  
  public static int getMemoryUsage(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    switch (paramInt1)
    {
    default: 
      throw new IllegalArgumentException();
    case 1: 
      return 80 + LZMAEncoderFast.getMemoryUsage(paramInt2, paramInt3, paramInt4);
    }
    return 80 + LZMAEncoderNormal.getMemoryUsage(paramInt2, paramInt3, paramInt4);
  }
  
  private void updateAlignPrices()
  {
    this.alignPriceCount = 16;
    int i = 0;
    while (i < 16)
    {
      this.alignPrices[i] = RangeEncoder.getReverseBitTreePrice(this.distAlign, i);
      i += 1;
    }
  }
  
  private void updateDistPrices()
  {
    this.distPriceCount = 128;
    int i = 0;
    while (i < 4)
    {
      j = 0;
      while (j < this.distSlotPricesSize)
      {
        this.distSlotPrices[i][j] = RangeEncoder.getBitTreePrice(this.distSlots[i], j);
        j += 1;
      }
      j = 14;
      while (j < this.distSlotPricesSize)
      {
        int[] arrayOfInt = this.distSlotPrices[i];
        arrayOfInt[j] += RangeEncoder.getDirectBitsPrice((j >>> 1) - 1 - 4);
        j += 1;
      }
      j = 0;
      while (j < 4)
      {
        this.fullDistPrices[i][j] = this.distSlotPrices[i][j];
        j += 1;
      }
      i += 1;
    }
    int j = 4;
    i = 4;
    while (i < 14)
    {
      int n = this.distSpecial[(i - 4)].length;
      int k = 0;
      while (k < n)
      {
        int i1 = RangeEncoder.getReverseBitTreePrice(this.distSpecial[(i - 4)], j - ((i & 0x1 | 0x2) << (i >>> 1) - 1));
        int m = 0;
        while (m < 4)
        {
          this.fullDistPrices[m][j] = (this.distSlotPrices[m][i] + i1);
          m += 1;
        }
        j += 1;
        k += 1;
      }
      i += 1;
    }
    assert (j == 128);
  }
  
  public boolean encodeForLZMA2()
  {
    if ((!this.lz.isStarted()) && (!encodeInit())) {
      return false;
    }
    while ((this.uncompressedSize <= 2096879) && (this.rc.getPendingSize() <= 65510)) {
      if (!encodeSymbol()) {
        return false;
      }
    }
    return true;
  }
  
  int getAnyMatchPrice(State paramState, int paramInt)
  {
    return RangeEncoder.getBitPrice(this.isMatch[paramState.get()][paramInt], 1);
  }
  
  int getAnyRepPrice(int paramInt, State paramState)
  {
    return RangeEncoder.getBitPrice(this.isRep[paramState.get()], 1) + paramInt;
  }
  
  public LZEncoder getLZEncoder()
  {
    return this.lz;
  }
  
  int getLongRepAndLenPrice(int paramInt1, int paramInt2, State paramState, int paramInt3)
  {
    paramInt1 = getLongRepPrice(getAnyRepPrice(getAnyMatchPrice(paramState, paramInt3), paramState), paramInt1, paramState, paramInt3);
    return this.repLenEncoder.getPrice(paramInt2, paramInt3) + paramInt1;
  }
  
  int getLongRepPrice(int paramInt1, int paramInt2, State paramState, int paramInt3)
  {
    if (paramInt2 == 0) {
      return paramInt1 + (RangeEncoder.getBitPrice(this.isRep0[paramState.get()], 0) + RangeEncoder.getBitPrice(this.isRep0Long[paramState.get()][paramInt3], 1));
    }
    paramInt1 += RangeEncoder.getBitPrice(this.isRep0[paramState.get()], 1);
    if (paramInt2 == 1) {
      return paramInt1 + RangeEncoder.getBitPrice(this.isRep1[paramState.get()], 0);
    }
    return paramInt1 + (RangeEncoder.getBitPrice(this.isRep1[paramState.get()], 1) + RangeEncoder.getBitPrice(this.isRep2[paramState.get()], paramInt2 - 2));
  }
  
  int getMatchAndLenPrice(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    paramInt1 += this.matchLenEncoder.getPrice(paramInt3, paramInt4);
    paramInt3 = getDistState(paramInt3);
    if (paramInt2 < 128) {
      return paramInt1 + this.fullDistPrices[paramInt3][paramInt2];
    }
    paramInt4 = getDistSlot(paramInt2);
    return paramInt1 + (this.distSlotPrices[paramInt3][paramInt4] + this.alignPrices[(paramInt2 & 0xF)]);
  }
  
  Matches getMatches()
  {
    this.readAhead += 1;
    Matches localMatches = this.lz.getMatches();
    assert (this.lz.verifyMatches(localMatches));
    return localMatches;
  }
  
  abstract int getNextSymbol();
  
  int getNormalMatchPrice(int paramInt, State paramState)
  {
    return RangeEncoder.getBitPrice(this.isRep[paramState.get()], 0) + paramInt;
  }
  
  int getShortRepPrice(int paramInt1, State paramState, int paramInt2)
  {
    return RangeEncoder.getBitPrice(this.isRep0[paramState.get()], 0) + paramInt1 + RangeEncoder.getBitPrice(this.isRep0Long[paramState.get()][paramInt2], 0);
  }
  
  public int getUncompressedSize()
  {
    return this.uncompressedSize;
  }
  
  public void reset()
  {
    super.reset();
    this.literalEncoder.reset();
    this.matchLenEncoder.reset();
    this.repLenEncoder.reset();
    this.distPriceCount = 0;
    this.alignPriceCount = 0;
    this.uncompressedSize += this.readAhead + 1;
    this.readAhead = -1;
  }
  
  public void resetUncompressedSize()
  {
    this.uncompressedSize = 0;
  }
  
  void skip(int paramInt)
  {
    this.readAhead += paramInt;
    this.lz.skip(paramInt);
  }
  
  void updatePrices()
  {
    if (this.distPriceCount <= 0) {
      updateDistPrices();
    }
    if (this.alignPriceCount <= 0) {
      updateAlignPrices();
    }
    this.matchLenEncoder.updatePrices();
    this.repLenEncoder.updatePrices();
  }
  
  class LengthEncoder
    extends LZMACoder.LengthCoder
  {
    private static final int PRICE_UPDATE_INTERVAL = 32;
    private final int[] counters;
    private final int[][] prices;
    
    LengthEncoder(int paramInt1, int paramInt2)
    {
      super();
      paramInt1 = 1 << paramInt1;
      this.counters = new int[paramInt1];
      paramInt2 = Math.max(paramInt2 - 2 + 1, 16);
      this.prices = ((int[][])Array.newInstance(Integer.TYPE, new int[] { paramInt1, paramInt2 }));
    }
    
    private void updatePrices(int paramInt)
    {
      int j = RangeEncoder.getBitPrice(this.choice[0], 0);
      int i = 0;
      while (i < 8)
      {
        this.prices[paramInt][i] = (RangeEncoder.getBitTreePrice(this.low[paramInt], i) + j);
        i += 1;
      }
      j = RangeEncoder.getBitPrice(this.choice[0], 1);
      int k = RangeEncoder.getBitPrice(this.choice[1], 0);
      while (i < 16)
      {
        this.prices[paramInt][i] = (j + k + RangeEncoder.getBitTreePrice(this.mid[paramInt], i - 8));
        i += 1;
      }
      k = RangeEncoder.getBitPrice(this.choice[1], 1);
      while (i < this.prices[paramInt].length)
      {
        this.prices[paramInt][i] = (j + k + RangeEncoder.getBitTreePrice(this.high, i - 8 - 8));
        i += 1;
      }
    }
    
    void encode(int paramInt1, int paramInt2)
    {
      paramInt1 -= 2;
      if (paramInt1 < 8)
      {
        LZMAEncoder.this.rc.encodeBit(this.choice, 0, 0);
        LZMAEncoder.this.rc.encodeBitTree(this.low[paramInt2], paramInt1);
      }
      for (;;)
      {
        int[] arrayOfInt = this.counters;
        arrayOfInt[paramInt2] -= 1;
        return;
        LZMAEncoder.this.rc.encodeBit(this.choice, 0, 1);
        paramInt1 -= 8;
        if (paramInt1 < 8)
        {
          LZMAEncoder.this.rc.encodeBit(this.choice, 1, 0);
          LZMAEncoder.this.rc.encodeBitTree(this.mid[paramInt2], paramInt1);
        }
        else
        {
          LZMAEncoder.this.rc.encodeBit(this.choice, 1, 1);
          LZMAEncoder.this.rc.encodeBitTree(this.high, paramInt1 - 8);
        }
      }
    }
    
    int getPrice(int paramInt1, int paramInt2)
    {
      return this.prices[paramInt2][(paramInt1 - 2)];
    }
    
    void reset()
    {
      super.reset();
      int i = 0;
      while (i < this.counters.length)
      {
        this.counters[i] = 0;
        i += 1;
      }
    }
    
    void updatePrices()
    {
      int i = 0;
      while (i < this.counters.length)
      {
        if (this.counters[i] <= 0)
        {
          this.counters[i] = 32;
          updatePrices(i);
        }
        i += 1;
      }
    }
  }
  
  class LiteralEncoder
    extends LZMACoder.LiteralCoder
  {
    LiteralSubencoder[] subencoders;
    
    static
    {
      if (!LZMAEncoder.class.desiredAssertionStatus()) {}
      for (boolean bool = true;; bool = false)
      {
        $assertionsDisabled = bool;
        return;
      }
    }
    
    LiteralEncoder(int paramInt1, int paramInt2)
    {
      super(paramInt1, paramInt2);
      this.subencoders = new LiteralSubencoder[1 << paramInt1 + paramInt2];
      paramInt1 = 0;
      while (paramInt1 < this.subencoders.length)
      {
        this.subencoders[paramInt1] = new LiteralSubencoder(null);
        paramInt1 += 1;
      }
    }
    
    void encode()
    {
      assert (LZMAEncoder.this.readAhead >= 0);
      int i = getSubcoderIndex(LZMAEncoder.this.lz.getByte(LZMAEncoder.this.readAhead + 1), LZMAEncoder.this.lz.getPos() - LZMAEncoder.this.readAhead);
      this.subencoders[i].encode();
    }
    
    void encodeInit()
    {
      assert (LZMAEncoder.this.readAhead >= 0);
      this.subencoders[0].encode();
    }
    
    int getPrice(int paramInt1, int paramInt2, int paramInt3, int paramInt4, State paramState)
    {
      int i = RangeEncoder.getBitPrice(LZMAEncoder.this.isMatch[paramState.get()][(LZMAEncoder.this.posMask & paramInt4)], 0);
      paramInt3 = getSubcoderIndex(paramInt3, paramInt4);
      if (paramState.isLiteral()) {}
      for (paramInt1 = this.subencoders[paramInt3].getNormalPrice(paramInt1);; paramInt1 = this.subencoders[paramInt3].getMatchedPrice(paramInt1, paramInt2)) {
        return i + paramInt1;
      }
    }
    
    void reset()
    {
      int i = 0;
      while (i < this.subencoders.length)
      {
        this.subencoders[i].reset();
        i += 1;
      }
    }
    
    private class LiteralSubencoder
      extends LZMACoder.LiteralCoder.LiteralSubcoder
    {
      private LiteralSubencoder()
      {
        super();
      }
      
      void encode()
      {
        int i = LZMAEncoder.this.lz.getByte(LZMAEncoder.this.readAhead) | 0x100;
        int j;
        if (LZMAEncoder.this.state.isLiteral()) {
          do
          {
            LZMAEncoder.this.rc.encodeBit(this.probs, i >>> 8, i >>> 7 & 0x1);
            j = i << 1;
            i = j;
          } while (j < 65536);
        }
        for (;;)
        {
          LZMAEncoder.this.state.updateLiteral();
          return;
          int k = LZMAEncoder.this.lz.getByte(LZMAEncoder.this.reps[0] + 1 + LZMAEncoder.this.readAhead);
          j = 256;
          int m;
          do
          {
            k <<= 1;
            LZMAEncoder.this.rc.encodeBit(this.probs, j + (k & j) + (i >>> 8), i >>> 7 & 0x1);
            m = i << 1;
            j &= (k ^ m ^ 0xFFFFFFFF);
            i = m;
          } while (m < 65536);
        }
      }
      
      int getMatchedPrice(int paramInt1, int paramInt2)
      {
        int i = 0;
        int j = 256;
        paramInt1 |= 0x100;
        int k;
        int m;
        do
        {
          paramInt2 <<= 1;
          k = i + RangeEncoder.getBitPrice(this.probs[(j + (paramInt2 & j) + (paramInt1 >>> 8))], paramInt1 >>> 7 & 0x1);
          m = paramInt1 << 1;
          j &= (paramInt2 ^ m ^ 0xFFFFFFFF);
          i = k;
          paramInt1 = m;
        } while (m < 65536);
        return k;
      }
      
      int getNormalPrice(int paramInt)
      {
        int i = 0;
        paramInt |= 0x100;
        int j;
        int k;
        do
        {
          j = i + RangeEncoder.getBitPrice(this.probs[(paramInt >>> 8)], paramInt >>> 7 & 0x1);
          k = paramInt << 1;
          i = j;
          paramInt = k;
        } while (k < 65536);
        return j;
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/lzma/LZMAEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */