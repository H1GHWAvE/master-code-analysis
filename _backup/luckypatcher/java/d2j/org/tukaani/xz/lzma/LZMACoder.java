package org.tukaani.xz.lzma;

import java.lang.reflect.Array;
import org.tukaani.xz.rangecoder.RangeCoder;

abstract class LZMACoder
{
  static final int ALIGN_BITS = 4;
  static final int ALIGN_MASK = 15;
  static final int ALIGN_SIZE = 16;
  static final int DIST_MODEL_END = 14;
  static final int DIST_MODEL_START = 4;
  static final int DIST_SLOTS = 64;
  static final int DIST_STATES = 4;
  static final int FULL_DISTANCES = 128;
  static final int MATCH_LEN_MAX = 273;
  static final int MATCH_LEN_MIN = 2;
  static final int POS_STATES_MAX = 16;
  static final int REPS = 4;
  final short[] distAlign = new short[16];
  final short[][] distSlots = (short[][])Array.newInstance(Short.TYPE, new int[] { 4, 64 });
  final short[][] distSpecial = { new short[2], new short[2], new short[4], new short[4], new short[8], new short[8], new short[16], new short[16], new short[32], new short[32] };
  final short[][] isMatch = (short[][])Array.newInstance(Short.TYPE, new int[] { 12, 16 });
  final short[] isRep = new short[12];
  final short[] isRep0 = new short[12];
  final short[][] isRep0Long = (short[][])Array.newInstance(Short.TYPE, new int[] { 12, 16 });
  final short[] isRep1 = new short[12];
  final short[] isRep2 = new short[12];
  final int posMask;
  final int[] reps = new int[4];
  final State state = new State();
  
  LZMACoder(int paramInt)
  {
    this.posMask = ((1 << paramInt) - 1);
  }
  
  static final int getDistState(int paramInt)
  {
    if (paramInt < 6) {
      return paramInt - 2;
    }
    return 3;
  }
  
  void reset()
  {
    this.reps[0] = 0;
    this.reps[1] = 0;
    this.reps[2] = 0;
    this.reps[3] = 0;
    this.state.reset();
    int i = 0;
    while (i < this.isMatch.length)
    {
      RangeCoder.initProbs(this.isMatch[i]);
      i += 1;
    }
    RangeCoder.initProbs(this.isRep);
    RangeCoder.initProbs(this.isRep0);
    RangeCoder.initProbs(this.isRep1);
    RangeCoder.initProbs(this.isRep2);
    i = 0;
    while (i < this.isRep0Long.length)
    {
      RangeCoder.initProbs(this.isRep0Long[i]);
      i += 1;
    }
    i = 0;
    while (i < this.distSlots.length)
    {
      RangeCoder.initProbs(this.distSlots[i]);
      i += 1;
    }
    i = 0;
    while (i < this.distSpecial.length)
    {
      RangeCoder.initProbs(this.distSpecial[i]);
      i += 1;
    }
    RangeCoder.initProbs(this.distAlign);
  }
  
  abstract class LengthCoder
  {
    static final int HIGH_SYMBOLS = 256;
    static final int LOW_SYMBOLS = 8;
    static final int MID_SYMBOLS = 8;
    final short[] choice = new short[2];
    final short[] high = new short['Ā'];
    final short[][] low = (short[][])Array.newInstance(Short.TYPE, new int[] { 16, 8 });
    final short[][] mid = (short[][])Array.newInstance(Short.TYPE, new int[] { 16, 8 });
    
    LengthCoder() {}
    
    void reset()
    {
      RangeCoder.initProbs(this.choice);
      int i = 0;
      while (i < this.low.length)
      {
        RangeCoder.initProbs(this.low[i]);
        i += 1;
      }
      i = 0;
      while (i < this.low.length)
      {
        RangeCoder.initProbs(this.mid[i]);
        i += 1;
      }
      RangeCoder.initProbs(this.high);
    }
  }
  
  abstract class LiteralCoder
  {
    private final int lc;
    private final int literalPosMask;
    
    LiteralCoder(int paramInt1, int paramInt2)
    {
      this.lc = paramInt1;
      this.literalPosMask = ((1 << paramInt2) - 1);
    }
    
    final int getSubcoderIndex(int paramInt1, int paramInt2)
    {
      return (paramInt1 >> 8 - this.lc) + ((this.literalPosMask & paramInt2) << this.lc);
    }
    
    abstract class LiteralSubcoder
    {
      final short[] probs = new short['̀'];
      
      LiteralSubcoder() {}
      
      void reset()
      {
        RangeCoder.initProbs(this.probs);
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/lzma/LZMACoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */