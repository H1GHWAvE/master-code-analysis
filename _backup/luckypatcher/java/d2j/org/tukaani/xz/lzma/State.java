package org.tukaani.xz.lzma;

final class State
{
  private static final int LIT_LIT = 0;
  private static final int LIT_LONGREP = 8;
  private static final int LIT_MATCH = 7;
  private static final int LIT_SHORTREP = 9;
  private static final int LIT_STATES = 7;
  private static final int MATCH_LIT = 4;
  private static final int MATCH_LIT_LIT = 1;
  private static final int NONLIT_MATCH = 10;
  private static final int NONLIT_REP = 11;
  private static final int REP_LIT = 5;
  private static final int REP_LIT_LIT = 2;
  private static final int SHORTREP_LIT = 6;
  private static final int SHORTREP_LIT_LIT = 3;
  static final int STATES = 12;
  private int state;
  
  State() {}
  
  State(State paramState)
  {
    this.state = paramState.state;
  }
  
  int get()
  {
    return this.state;
  }
  
  boolean isLiteral()
  {
    return this.state < 7;
  }
  
  void reset()
  {
    this.state = 0;
  }
  
  void set(State paramState)
  {
    this.state = paramState.state;
  }
  
  void updateLiteral()
  {
    if (this.state <= 3)
    {
      this.state = 0;
      return;
    }
    if (this.state <= 9)
    {
      this.state -= 3;
      return;
    }
    this.state -= 6;
  }
  
  void updateLongRep()
  {
    if (this.state < 7) {}
    for (int i = 8;; i = 11)
    {
      this.state = i;
      return;
    }
  }
  
  void updateMatch()
  {
    int i = 7;
    if (this.state < 7) {}
    for (;;)
    {
      this.state = i;
      return;
      i = 10;
    }
  }
  
  void updateShortRep()
  {
    if (this.state < 7) {}
    for (int i = 9;; i = 11)
    {
      this.state = i;
      return;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/lzma/State.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */