package org.tukaani.xz.lzma;

import org.tukaani.xz.lz.LZEncoder;
import org.tukaani.xz.lz.Matches;
import org.tukaani.xz.rangecoder.RangeEncoder;

final class LZMAEncoderFast
  extends LZMAEncoder
{
  private static int EXTRA_SIZE_AFTER = 272;
  private static int EXTRA_SIZE_BEFORE = 1;
  private Matches matches = null;
  
  LZMAEncoderFast(RangeEncoder paramRangeEncoder, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
  {
    super(paramRangeEncoder, LZEncoder.getInstance(paramInt4, Math.max(paramInt5, EXTRA_SIZE_BEFORE), EXTRA_SIZE_AFTER, paramInt6, 273, paramInt7, paramInt8), paramInt1, paramInt2, paramInt3, paramInt4, paramInt6);
  }
  
  private boolean changePair(int paramInt1, int paramInt2)
  {
    return paramInt1 < paramInt2 >>> 7;
  }
  
  static int getMemoryUsage(int paramInt1, int paramInt2, int paramInt3)
  {
    return LZEncoder.getMemoryUsage(paramInt1, Math.max(paramInt2, EXTRA_SIZE_BEFORE), EXTRA_SIZE_AFTER, 273, paramInt3);
  }
  
  int getNextSymbol()
  {
    if (this.readAhead == -1) {
      this.matches = getMatches();
    }
    this.back = -1;
    int i2 = Math.min(this.lz.getAvail(), 273);
    if (i2 < 2) {
      return 1;
    }
    int m = 0;
    int n = 0;
    int i = 0;
    if (i < 4)
    {
      k = this.lz.getMatchLen(this.reps[i], i2);
      if (k < 2) {
        j = m;
      }
      for (;;)
      {
        i += 1;
        m = j;
        break;
        if (k >= this.niceLen)
        {
          this.back = i;
          skip(k - 1);
          return k;
        }
        j = m;
        if (k > m)
        {
          n = i;
          j = k;
        }
      }
    }
    i = 0;
    int i1 = 0;
    if (this.matches.count > 0)
    {
      i = this.matches.len[(this.matches.count - 1)];
      i1 = this.matches.dist[(this.matches.count - 1)];
      j = i1;
      k = i;
      if (i >= this.niceLen)
      {
        this.back = (i1 + 4);
        skip(i - 1);
        return i;
      }
      while ((this.matches.count > 1) && (k == this.matches.len[(this.matches.count - 2)] + 1) && (changePair(this.matches.dist[(this.matches.count - 2)], j)))
      {
        Matches localMatches = this.matches;
        localMatches.count -= 1;
        k = this.matches.len[(this.matches.count - 1)];
        j = this.matches.dist[(this.matches.count - 1)];
      }
      i1 = j;
      i = k;
      if (k == 2)
      {
        i1 = j;
        i = k;
        if (j >= 128)
        {
          i = 1;
          i1 = j;
        }
      }
    }
    if ((m >= 2) && ((m + 1 >= i) || ((m + 2 >= i) && (i1 >= 512)) || ((m + 3 >= i) && (i1 >= 32768))))
    {
      this.back = n;
      skip(m - 1);
      return m;
    }
    if ((i < 2) || (i2 <= 2)) {
      return 1;
    }
    this.matches = getMatches();
    if (this.matches.count > 0)
    {
      j = this.matches.len[(this.matches.count - 1)];
      k = this.matches.dist[(this.matches.count - 1)];
      if (((j >= i) && (k < i1)) || ((j == i + 1) && (!changePair(i1, k))) || (j > i + 1) || ((j + 1 >= i) && (i >= 3) && (changePair(k, i1)))) {
        return 1;
      }
    }
    int k = Math.max(i - 1, 2);
    int j = 0;
    while (j < 4)
    {
      if (this.lz.getMatchLen(this.reps[j], k) == k) {
        return 1;
      }
      j += 1;
    }
    this.back = (i1 + 4);
    skip(i - 2);
    return i;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/lzma/LZMAEncoderFast.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */