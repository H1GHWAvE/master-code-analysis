package org.tukaani.xz.lzma;

import java.io.IOException;
import org.tukaani.xz.lz.LZDecoder;
import org.tukaani.xz.rangecoder.RangeDecoder;

public final class LZMADecoder
  extends LZMACoder
{
  private final LiteralDecoder literalDecoder;
  private final LZDecoder lz;
  private final LengthDecoder matchLenDecoder = new LengthDecoder(null);
  private final RangeDecoder rc;
  private final LengthDecoder repLenDecoder = new LengthDecoder(null);
  
  public LZMADecoder(LZDecoder paramLZDecoder, RangeDecoder paramRangeDecoder, int paramInt1, int paramInt2, int paramInt3)
  {
    super(paramInt3);
    this.lz = paramLZDecoder;
    this.rc = paramRangeDecoder;
    this.literalDecoder = new LiteralDecoder(paramInt1, paramInt2);
    reset();
  }
  
  private int decodeMatch(int paramInt)
    throws IOException
  {
    this.state.updateMatch();
    this.reps[3] = this.reps[2];
    this.reps[2] = this.reps[1];
    this.reps[1] = this.reps[0];
    paramInt = this.matchLenDecoder.decode(paramInt);
    int i = this.rc.decodeBitTree(this.distSlots[getDistState(paramInt)]);
    if (i < 4)
    {
      this.reps[0] = i;
      return paramInt;
    }
    int j = (i >> 1) - 1;
    this.reps[0] = ((i & 0x1 | 0x2) << j);
    if (i < 14)
    {
      arrayOfInt = this.reps;
      arrayOfInt[0] |= this.rc.decodeReverseBitTree(this.distSpecial[(i - 4)]);
      return paramInt;
    }
    int[] arrayOfInt = this.reps;
    arrayOfInt[0] |= this.rc.decodeDirectBits(j - 4) << 4;
    arrayOfInt = this.reps;
    arrayOfInt[0] |= this.rc.decodeReverseBitTree(this.distAlign);
    return paramInt;
  }
  
  private int decodeRepMatch(int paramInt)
    throws IOException
  {
    int i;
    if (this.rc.decodeBit(this.isRep0, this.state.get()) == 0)
    {
      if (this.rc.decodeBit(this.isRep0Long[this.state.get()], paramInt) == 0)
      {
        this.state.updateShortRep();
        return 1;
      }
    }
    else
    {
      if (this.rc.decodeBit(this.isRep1, this.state.get()) != 0) {
        break label116;
      }
      i = this.reps[1];
      this.reps[1] = this.reps[0];
      this.reps[0] = i;
    }
    this.state.updateLongRep();
    return this.repLenDecoder.decode(paramInt);
    label116:
    if (this.rc.decodeBit(this.isRep2, this.state.get()) == 0) {
      i = this.reps[2];
    }
    for (;;)
    {
      this.reps[2] = this.reps[1];
      break;
      i = this.reps[3];
      this.reps[3] = this.reps[2];
    }
  }
  
  public void decode()
    throws IOException
  {
    this.lz.repeatPending();
    while (this.lz.hasSpace())
    {
      int i = this.lz.getPos() & this.posMask;
      if (this.rc.decodeBit(this.isMatch[this.state.get()], i) == 0)
      {
        this.literalDecoder.decode();
      }
      else
      {
        if (this.rc.decodeBit(this.isRep, this.state.get()) == 0) {}
        for (i = decodeMatch(i);; i = decodeRepMatch(i))
        {
          this.lz.repeat(this.reps[0], i);
          break;
        }
      }
    }
    this.rc.normalize();
  }
  
  public boolean endMarkerDetected()
  {
    boolean bool = false;
    if (this.reps[0] == -1) {
      bool = true;
    }
    return bool;
  }
  
  public void reset()
  {
    super.reset();
    this.literalDecoder.reset();
    this.matchLenDecoder.reset();
    this.repLenDecoder.reset();
  }
  
  private class LengthDecoder
    extends LZMACoder.LengthCoder
  {
    private LengthDecoder()
    {
      super();
    }
    
    int decode(int paramInt)
      throws IOException
    {
      if (LZMADecoder.this.rc.decodeBit(this.choice, 0) == 0) {
        return LZMADecoder.this.rc.decodeBitTree(this.low[paramInt]) + 2;
      }
      if (LZMADecoder.this.rc.decodeBit(this.choice, 1) == 0) {
        return LZMADecoder.this.rc.decodeBitTree(this.mid[paramInt]) + 2 + 8;
      }
      return LZMADecoder.this.rc.decodeBitTree(this.high) + 2 + 8 + 8;
    }
  }
  
  private class LiteralDecoder
    extends LZMACoder.LiteralCoder
  {
    LiteralSubdecoder[] subdecoders;
    
    LiteralDecoder(int paramInt1, int paramInt2)
    {
      super(paramInt1, paramInt2);
      this.subdecoders = new LiteralSubdecoder[1 << paramInt1 + paramInt2];
      paramInt1 = 0;
      while (paramInt1 < this.subdecoders.length)
      {
        this.subdecoders[paramInt1] = new LiteralSubdecoder(null);
        paramInt1 += 1;
      }
    }
    
    void decode()
      throws IOException
    {
      int i = getSubcoderIndex(LZMADecoder.this.lz.getByte(0), LZMADecoder.this.lz.getPos());
      this.subdecoders[i].decode();
    }
    
    void reset()
    {
      int i = 0;
      while (i < this.subdecoders.length)
      {
        this.subdecoders[i].reset();
        i += 1;
      }
    }
    
    private class LiteralSubdecoder
      extends LZMACoder.LiteralCoder.LiteralSubcoder
    {
      private LiteralSubdecoder()
      {
        super();
      }
      
      void decode()
        throws IOException
      {
        int i = 1;
        int j = 1;
        if (LZMADecoder.this.state.isLiteral())
        {
          i = j;
          do
          {
            j = i << 1 | LZMADecoder.this.rc.decodeBit(this.probs, i);
            i = j;
          } while (j < 256);
        }
        for (;;)
        {
          LZMADecoder.this.lz.putByte((byte)j);
          LZMADecoder.this.state.updateLiteral();
          return;
          int m = LZMADecoder.this.lz.getByte(LZMADecoder.this.reps[0]);
          j = 256;
          int k;
          do
          {
            m <<= 1;
            int n = m & j;
            int i1 = LZMADecoder.this.rc.decodeBit(this.probs, j + n + i);
            k = i << 1 | i1;
            j &= (0 - i1 ^ n ^ 0xFFFFFFFF);
            i = k;
          } while (k < 256);
          j = k;
        }
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/lzma/LZMADecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */