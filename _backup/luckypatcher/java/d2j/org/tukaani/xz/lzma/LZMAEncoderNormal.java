package org.tukaani.xz.lzma;

import org.tukaani.xz.lz.LZEncoder;
import org.tukaani.xz.lz.Matches;
import org.tukaani.xz.rangecoder.RangeEncoder;

final class LZMAEncoderNormal
  extends LZMAEncoder
{
  private static int EXTRA_SIZE_AFTER = 0;
  private static int EXTRA_SIZE_BEFORE = 0;
  private static final int OPTS = 4096;
  private Matches matches;
  private final State nextState = new State();
  private int optCur = 0;
  private int optEnd = 0;
  private final Optimum[] opts = new Optimum['က'];
  private final int[] repLens = new int[4];
  
  static
  {
    if (!LZMAEncoderNormal.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      EXTRA_SIZE_BEFORE = 4096;
      EXTRA_SIZE_AFTER = 4096;
      return;
    }
  }
  
  LZMAEncoderNormal(RangeEncoder paramRangeEncoder, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
  {
    super(paramRangeEncoder, LZEncoder.getInstance(paramInt4, Math.max(paramInt5, EXTRA_SIZE_BEFORE), EXTRA_SIZE_AFTER, paramInt6, 273, paramInt7, paramInt8), paramInt1, paramInt2, paramInt3, paramInt4, paramInt6);
    paramInt1 = 0;
    while (paramInt1 < 4096)
    {
      this.opts[paramInt1] = new Optimum();
      paramInt1 += 1;
    }
  }
  
  private void calc1BytePrices(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = 0;
    int m = this.lz.getByte(0);
    int n = this.lz.getByte(this.opts[this.optCur].reps[0] + 1);
    int k = this.opts[this.optCur].price + this.literalEncoder.getPrice(m, n, this.lz.getByte(1), paramInt1, this.opts[this.optCur].state);
    if (k < this.opts[(this.optCur + 1)].price)
    {
      this.opts[(this.optCur + 1)].set1(k, this.optCur, -1);
      i = 1;
    }
    int j = i;
    if (n == m) {
      if (this.opts[(this.optCur + 1)].optPrev != this.optCur)
      {
        j = i;
        if (this.opts[(this.optCur + 1)].backPrev == 0) {}
      }
      else
      {
        paramInt2 = getShortRepPrice(paramInt4, this.opts[this.optCur].state, paramInt2);
        j = i;
        if (paramInt2 <= this.opts[(this.optCur + 1)].price)
        {
          this.opts[(this.optCur + 1)].set1(paramInt2, this.optCur, 0);
          j = 1;
        }
      }
    }
    if ((j == 0) && (n != m) && (paramInt3 > 2))
    {
      paramInt2 = Math.min(this.niceLen, paramInt3 - 1);
      paramInt2 = this.lz.getMatchLen(1, this.opts[this.optCur].reps[0], paramInt2);
      if (paramInt2 >= 2)
      {
        this.nextState.set(this.opts[this.optCur].state);
        this.nextState.updateLiteral();
        paramInt3 = this.posMask;
        paramInt1 = k + getLongRepAndLenPrice(0, paramInt2, this.nextState, paramInt1 + 1 & paramInt3);
        paramInt2 = this.optCur + 1 + paramInt2;
        while (this.optEnd < paramInt2)
        {
          Optimum[] arrayOfOptimum = this.opts;
          paramInt3 = this.optEnd + 1;
          this.optEnd = paramInt3;
          arrayOfOptimum[paramInt3].reset();
        }
        if (paramInt1 < this.opts[paramInt2].price) {
          this.opts[paramInt2].set2(paramInt1, this.optCur, 0);
        }
      }
    }
  }
  
  private int calcLongRepPrices(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = 2;
    int n = Math.min(paramInt3, this.niceLen);
    int j = 0;
    if (j < 4)
    {
      int m = this.lz.getMatchLen(this.opts[this.optCur].reps[j], n);
      int k;
      if (m < 2) {
        k = i;
      }
      for (;;)
      {
        j += 1;
        i = k;
        break;
        Optimum[] arrayOfOptimum;
        while (this.optEnd < this.optCur + m)
        {
          arrayOfOptimum = this.opts;
          k = this.optEnd + 1;
          this.optEnd = k;
          arrayOfOptimum[k].reset();
        }
        int i1 = getLongRepPrice(paramInt4, j, this.opts[this.optCur].state, paramInt2);
        k = m;
        while (k >= 2)
        {
          i2 = i1 + this.repLenEncoder.getPrice(k, paramInt2);
          if (i2 < this.opts[(this.optCur + k)].price) {
            this.opts[(this.optCur + k)].set1(i2, this.optCur, j);
          }
          k -= 1;
        }
        if (j == 0) {
          i = m + 1;
        }
        k = Math.min(this.niceLen, paramInt3 - m - 1);
        int i2 = this.lz.getMatchLen(m + 1, this.opts[this.optCur].reps[j], k);
        k = i;
        if (i2 >= 2)
        {
          k = this.repLenEncoder.getPrice(m, paramInt2);
          this.nextState.set(this.opts[this.optCur].state);
          this.nextState.updateLongRep();
          int i3 = this.lz.getByte(m, 0);
          int i4 = this.lz.getByte(0);
          int i5 = this.lz.getByte(m, 1);
          i3 = this.literalEncoder.getPrice(i3, i4, i5, paramInt1 + m, this.nextState);
          this.nextState.updateLiteral();
          i4 = this.posMask;
          i1 = i1 + k + i3 + getLongRepAndLenPrice(0, i2, this.nextState, paramInt1 + m + 1 & i4);
          i2 = this.optCur + m + 1 + i2;
          while (this.optEnd < i2)
          {
            arrayOfOptimum = this.opts;
            k = this.optEnd + 1;
            this.optEnd = k;
            arrayOfOptimum[k].reset();
          }
          k = i;
          if (i1 < this.opts[i2].price)
          {
            this.opts[i2].set3(i1, this.optCur, j, m, 0);
            k = i;
          }
        }
      }
    }
    return i;
  }
  
  private void calcNormalMatchPrices(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    Object localObject;
    if (this.matches.len[(this.matches.count - 1)] > paramInt3)
    {
      for (this.matches.count = 0; this.matches.len[this.matches.count] < paramInt3; ((Matches)localObject).count += 1) {
        localObject = this.matches;
      }
      localObject = this.matches.len;
      Matches localMatches = this.matches;
      i = localMatches.count;
      localMatches.count = (i + 1);
      localObject[i] = paramInt3;
    }
    if (this.matches.len[(this.matches.count - 1)] < paramInt5) {
      return;
    }
    while (this.optEnd < this.optCur + this.matches.len[(this.matches.count - 1)])
    {
      localObject = this.opts;
      i = this.optEnd + 1;
      this.optEnd = i;
      localObject[i].reset();
    }
    int j = getNormalMatchPrice(paramInt4, this.opts[this.optCur].state);
    paramInt4 = 0;
    while (paramInt5 > this.matches.len[paramInt4]) {
      paramInt4 += 1;
    }
    int i = paramInt5;
    paramInt5 = paramInt4;
    paramInt4 = i;
    i = this.matches.dist[paramInt5];
    int k = getMatchAndLenPrice(j, i, paramInt4, paramInt2);
    if (k < this.opts[(this.optCur + paramInt4)].price) {
      this.opts[(this.optCur + paramInt4)].set1(k, this.optCur, i + 4);
    }
    if (paramInt4 != this.matches.len[paramInt5]) {}
    do
    {
      paramInt4 += 1;
      break;
      int m = Math.min(this.niceLen, paramInt3 - paramInt4 - 1);
      m = this.lz.getMatchLen(paramInt4 + 1, i, m);
      if (m >= 2)
      {
        this.nextState.set(this.opts[this.optCur].state);
        this.nextState.updateMatch();
        int n = this.lz.getByte(paramInt4, 0);
        int i1 = this.lz.getByte(0);
        int i2 = this.lz.getByte(paramInt4, 1);
        n = this.literalEncoder.getPrice(n, i1, i2, paramInt1 + paramInt4, this.nextState);
        this.nextState.updateLiteral();
        i1 = this.posMask;
        k = k + n + getLongRepAndLenPrice(0, m, this.nextState, paramInt1 + paramInt4 + 1 & i1);
        m = this.optCur + paramInt4 + 1 + m;
        while (this.optEnd < m)
        {
          localObject = this.opts;
          n = this.optEnd + 1;
          this.optEnd = n;
          localObject[n].reset();
        }
        if (k < this.opts[m].price) {
          this.opts[m].set3(k, this.optCur, i + 4, paramInt4, 0);
        }
      }
      i = paramInt5 + 1;
      paramInt5 = i;
    } while (i != this.matches.count);
  }
  
  private int convertOpts()
  {
    this.optEnd = this.optCur;
    int i = this.opts[this.optCur].optPrev;
    Optimum localOptimum = this.opts[this.optCur];
    int j = i;
    if (localOptimum.prev1IsLiteral)
    {
      this.opts[i].optPrev = this.optCur;
      this.opts[i].backPrev = -1;
      j = i - 1;
      this.optCur = i;
      if (!localOptimum.hasPrev2) {
        break label183;
      }
      this.opts[j].optPrev = (j + 1);
      this.opts[j].backPrev = localOptimum.backPrev2;
      this.optCur = j;
      j = localOptimum.optPrev2;
    }
    label183:
    for (;;)
    {
      i = this.opts[j].optPrev;
      this.opts[j].optPrev = this.optCur;
      this.optCur = j;
      if (this.optCur > 0) {
        break;
      }
      this.optCur = this.opts[0].optPrev;
      this.back = this.opts[this.optCur].backPrev;
      return this.optCur;
    }
  }
  
  static int getMemoryUsage(int paramInt1, int paramInt2, int paramInt3)
  {
    return LZEncoder.getMemoryUsage(paramInt1, Math.max(paramInt2, EXTRA_SIZE_BEFORE), EXTRA_SIZE_AFTER, 273, paramInt3) + 256;
  }
  
  private void updateOptStateAndReps()
  {
    int i = this.opts[this.optCur].optPrev;
    assert (i < this.optCur);
    if (this.opts[this.optCur].prev1IsLiteral)
    {
      i -= 1;
      if (this.opts[this.optCur].hasPrev2)
      {
        this.opts[this.optCur].state.set(this.opts[this.opts[this.optCur].optPrev2].state);
        if (this.opts[this.optCur].backPrev2 < 4)
        {
          this.opts[this.optCur].state.updateLongRep();
          this.opts[this.optCur].state.updateLiteral();
        }
      }
    }
    for (;;)
    {
      if (i == this.optCur - 1)
      {
        if ((!$assertionsDisabled) && (this.opts[this.optCur].backPrev != 0) && (this.opts[this.optCur].backPrev != -1))
        {
          throw new AssertionError();
          this.opts[this.optCur].state.updateMatch();
          break;
          this.opts[this.optCur].state.set(this.opts[i].state);
          break;
          this.opts[this.optCur].state.set(this.opts[i].state);
          continue;
        }
        if (this.opts[this.optCur].backPrev == 0)
        {
          this.opts[this.optCur].state.updateShortRep();
          System.arraycopy(this.opts[i].reps, 0, this.opts[this.optCur].reps, 0, 4);
        }
      }
    }
    int j;
    int k;
    for (;;)
    {
      return;
      this.opts[this.optCur].state.updateLiteral();
      break;
      if ((this.opts[this.optCur].prev1IsLiteral) && (this.opts[this.optCur].hasPrev2))
      {
        j = this.opts[this.optCur].optPrev2;
        k = this.opts[this.optCur].backPrev2;
        this.opts[this.optCur].state.updateLongRep();
      }
      int m;
      for (;;)
      {
        if (k >= 4) {
          break label598;
        }
        this.opts[this.optCur].reps[0] = this.opts[j].reps[k];
        i = 1;
        for (;;)
        {
          m = i;
          if (i > k) {
            break;
          }
          this.opts[this.optCur].reps[i] = this.opts[j].reps[(i - 1)];
          i += 1;
        }
        k = this.opts[this.optCur].backPrev;
        if (k < 4)
        {
          this.opts[this.optCur].state.updateLongRep();
          j = i;
        }
        else
        {
          this.opts[this.optCur].state.updateMatch();
          j = i;
        }
      }
      while (m < 4)
      {
        this.opts[this.optCur].reps[m] = this.opts[j].reps[m];
        m += 1;
      }
    }
    label598:
    this.opts[this.optCur].reps[0] = (k - 4);
    System.arraycopy(this.opts[j].reps, 0, this.opts[this.optCur].reps, 1, 3);
  }
  
  int getNextSymbol()
  {
    if (this.optCur < this.optEnd)
    {
      i = this.opts[this.optCur].optPrev;
      j = this.optCur;
      this.optCur = this.opts[this.optCur].optPrev;
      this.back = this.opts[this.optCur].backPrev;
      return i - j;
    }
    assert (this.optCur == this.optEnd);
    this.optCur = 0;
    this.optEnd = 0;
    this.back = -1;
    if (this.readAhead == -1) {
      this.matches = getMatches();
    }
    int m = Math.min(this.lz.getAvail(), 273);
    if (m < 2) {
      return 1;
    }
    int j = 0;
    int i = 0;
    if (i < 4)
    {
      this.repLens[i] = this.lz.getMatchLen(this.reps[i], m);
      if (this.repLens[i] < 2)
      {
        this.repLens[i] = 0;
        k = j;
      }
      for (;;)
      {
        i += 1;
        j = k;
        break;
        k = j;
        if (this.repLens[i] > this.repLens[j]) {
          k = i;
        }
      }
    }
    if (this.repLens[j] >= this.niceLen)
    {
      this.back = j;
      skip(this.repLens[j] - 1);
      return this.repLens[j];
    }
    i = 0;
    if (this.matches.count > 0)
    {
      k = this.matches.len[(this.matches.count - 1)];
      m = this.matches.dist[(this.matches.count - 1)];
      i = k;
      if (k >= this.niceLen)
      {
        this.back = (m + 4);
        skip(k - 1);
        return k;
      }
    }
    int k = this.lz.getByte(0);
    int n = this.lz.getByte(this.reps[0] + 1);
    if ((i < 2) && (k != n) && (this.repLens[j] < 2)) {
      return 1;
    }
    m = this.lz.getPos();
    int i1 = m & this.posMask;
    int i2 = this.lz.getByte(1);
    i2 = this.literalEncoder.getPrice(k, n, i2, m, this.state);
    this.opts[1].set1(i2, 0, -1);
    i2 = getAnyMatchPrice(this.state, i1);
    int i3 = getAnyRepPrice(i2, this.state);
    if (n == k)
    {
      k = getShortRepPrice(i3, this.state, i1);
      if (k < this.opts[1].price) {
        this.opts[1].set1(k, 0, 0);
      }
    }
    this.optEnd = Math.max(i, this.repLens[j]);
    if (this.optEnd < 2)
    {
      assert (this.optEnd == 0) : this.optEnd;
      this.back = this.opts[1].backPrev;
      return 1;
    }
    updatePrices();
    this.opts[0].state.set(this.state);
    System.arraycopy(this.reps, 0, this.opts[0].reps, 0, 4);
    j = this.optEnd;
    while (j >= 2)
    {
      this.opts[j].reset();
      j -= 1;
    }
    j = 0;
    if (j < 4)
    {
      k = this.repLens[j];
      if (k < 2) {}
      for (;;)
      {
        j += 1;
        break;
        int i4 = getLongRepPrice(i3, j, this.state, i1);
        do
        {
          n = i4 + this.repLenEncoder.getPrice(k, i1);
          if (n < this.opts[k].price) {
            this.opts[k].set1(n, 0, j);
          }
          n = k - 1;
          k = n;
        } while (n >= 2);
      }
    }
    n = Math.max(this.repLens[0] + 1, 2);
    if (n <= i)
    {
      i2 = getNormalMatchPrice(i2, this.state);
      i = 0;
      for (;;)
      {
        k = i;
        j = n;
        if (n <= this.matches.len[i]) {
          break;
        }
        i += 1;
      }
      do
      {
        do
        {
          j += 1;
          k = i;
          i = this.matches.dist[k];
          n = getMatchAndLenPrice(i2, i, j, i1);
          if (n < this.opts[j].price) {
            this.opts[j].set1(n, 0, i + 4);
          }
          i = k;
        } while (j != this.matches.len[k]);
        k += 1;
        i = k;
      } while (k != this.matches.count);
    }
    i = Math.min(this.lz.getAvail(), 4095);
    j = m;
    for (;;)
    {
      k = this.optCur + 1;
      this.optCur = k;
      if (k < this.optEnd)
      {
        this.matches = getMatches();
        if ((this.matches.count <= 0) || (this.matches.len[(this.matches.count - 1)] < this.niceLen)) {}
      }
      else
      {
        return convertOpts();
      }
      k = i - 1;
      m = j + 1;
      n = m & this.posMask;
      updateOptStateAndReps();
      i1 = this.opts[this.optCur].price + getAnyMatchPrice(this.opts[this.optCur].state, n);
      i2 = getAnyRepPrice(i1, this.opts[this.optCur].state);
      calc1BytePrices(m, n, k, i2);
      j = m;
      i = k;
      if (k >= 2)
      {
        i2 = calcLongRepPrices(m, n, k, i2);
        j = m;
        i = k;
        if (this.matches.count > 0)
        {
          calcNormalMatchPrices(m, n, k, i1, i2);
          j = m;
          i = k;
        }
      }
    }
  }
  
  public void reset()
  {
    this.optCur = 0;
    this.optEnd = 0;
    super.reset();
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/lzma/LZMAEncoderNormal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */