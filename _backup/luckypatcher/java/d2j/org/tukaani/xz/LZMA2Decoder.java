package org.tukaani.xz;

import java.io.InputStream;

class LZMA2Decoder
  extends LZMA2Coder
  implements FilterDecoder
{
  private int dictSize;
  
  LZMA2Decoder(byte[] paramArrayOfByte)
    throws UnsupportedOptionsException
  {
    if ((paramArrayOfByte.length != 1) || ((paramArrayOfByte[0] & 0xFF) > 37)) {
      throw new UnsupportedOptionsException("Unsupported LZMA2 properties");
    }
    this.dictSize = (paramArrayOfByte[0] & 0x1 | 0x2);
    this.dictSize <<= (paramArrayOfByte[0] >>> 1) + 11;
  }
  
  public InputStream getInputStream(InputStream paramInputStream)
  {
    return new LZMA2InputStream(paramInputStream, this.dictSize);
  }
  
  public int getMemoryUsage()
  {
    return LZMA2InputStream.getMemoryUsage(this.dictSize);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/LZMA2Decoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */