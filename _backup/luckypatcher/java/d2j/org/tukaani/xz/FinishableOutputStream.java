package org.tukaani.xz;

import java.io.IOException;
import java.io.OutputStream;

public abstract class FinishableOutputStream
  extends OutputStream
{
  public void finish()
    throws IOException
  {}
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/FinishableOutputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */