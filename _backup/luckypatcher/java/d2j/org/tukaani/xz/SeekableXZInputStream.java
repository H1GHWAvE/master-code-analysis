package org.tukaani.xz;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import org.tukaani.xz.check.Check;
import org.tukaani.xz.common.DecoderUtil;
import org.tukaani.xz.common.StreamFlags;
import org.tukaani.xz.index.BlockInfo;
import org.tukaani.xz.index.IndexDecoder;

public class SeekableXZInputStream
  extends SeekableInputStream
{
  private int blockCount = 0;
  private BlockInputStream blockDecoder = null;
  private Check check;
  private int checkTypes = 0;
  private final BlockInfo curBlockInfo;
  private long curPos = 0L;
  private boolean endReached = false;
  private IOException exception = null;
  private SeekableInputStream in;
  private int indexMemoryUsage = 0;
  private long largestBlockSize = 0L;
  private final int memoryLimit;
  private final BlockInfo queriedBlockInfo;
  private boolean seekNeeded = false;
  private long seekPos;
  private final ArrayList streams = new ArrayList();
  private final byte[] tempBuf = new byte[1];
  private long uncompressedSize = 0L;
  
  static
  {
    if (!SeekableXZInputStream.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  public SeekableXZInputStream(SeekableInputStream paramSeekableInputStream)
    throws IOException
  {
    this(paramSeekableInputStream, -1);
  }
  
  public SeekableXZInputStream(SeekableInputStream paramSeekableInputStream, int paramInt)
    throws IOException
  {
    this.in = paramSeekableInputStream;
    Object localObject = new DataInputStream(paramSeekableInputStream);
    paramSeekableInputStream.seek(0L);
    byte[] arrayOfByte = new byte[XZ.HEADER_MAGIC.length];
    ((DataInputStream)localObject).readFully(arrayOfByte);
    if (!Arrays.equals(arrayOfByte, XZ.HEADER_MAGIC)) {
      throw new XZFormatException();
    }
    long l2 = paramSeekableInputStream.length();
    if ((0x3 & l2) != 0L) {
      throw new CorruptedInputException("XZ file size is not a multiple of 4 bytes");
    }
    arrayOfByte = new byte[12];
    long l1 = 0L;
    int i = paramInt;
    while (l2 > 0L)
    {
      if (l2 < 12L) {
        throw new CorruptedInputException();
      }
      paramSeekableInputStream.seek(l2 - 12L);
      ((DataInputStream)localObject).readFully(arrayOfByte);
      if ((arrayOfByte[8] == 0) && (arrayOfByte[9] == 0) && (arrayOfByte[10] == 0) && (arrayOfByte[11] == 0))
      {
        l1 += 4L;
        l2 -= 4L;
      }
      else
      {
        l2 -= 12L;
        StreamFlags localStreamFlags = DecoderUtil.decodeStreamFooter(arrayOfByte);
        if (localStreamFlags.backwardSize >= l2) {
          throw new CorruptedInputException("Backward Size in XZ Stream Footer is too big");
        }
        this.check = Check.getInstance(localStreamFlags.checkType);
        this.checkTypes |= 1 << localStreamFlags.checkType;
        paramSeekableInputStream.seek(l2 - localStreamFlags.backwardSize);
        IndexDecoder localIndexDecoder;
        try
        {
          localIndexDecoder = new IndexDecoder(paramSeekableInputStream, localStreamFlags, l1, i);
          this.indexMemoryUsage += localIndexDecoder.getMemoryUsage();
          paramInt = i;
          if (i >= 0)
          {
            i -= localIndexDecoder.getMemoryUsage();
            paramInt = i;
            if (!$assertionsDisabled)
            {
              paramInt = i;
              if (i < 0) {
                throw new AssertionError();
              }
            }
          }
        }
        catch (MemoryLimitException paramSeekableInputStream)
        {
          assert (i >= 0);
          throw new MemoryLimitException(paramSeekableInputStream.getMemoryNeeded() + this.indexMemoryUsage, this.indexMemoryUsage + i);
        }
        if (this.largestBlockSize < localIndexDecoder.getLargestBlockSize()) {
          this.largestBlockSize = localIndexDecoder.getLargestBlockSize();
        }
        l1 = localIndexDecoder.getStreamSize() - 12L;
        if (l2 < l1) {
          throw new CorruptedInputException("XZ Index indicates too big compressed size for the XZ Stream");
        }
        l2 -= l1;
        paramSeekableInputStream.seek(l2);
        ((DataInputStream)localObject).readFully(arrayOfByte);
        if (!DecoderUtil.areStreamFlagsEqual(DecoderUtil.decodeStreamHeader(arrayOfByte), localStreamFlags)) {
          throw new CorruptedInputException("XZ Stream Footer does not match Stream Header");
        }
        this.uncompressedSize += localIndexDecoder.getUncompressedSize();
        if (this.uncompressedSize < 0L) {
          throw new UnsupportedOptionsException("XZ file is too big");
        }
        this.blockCount += localIndexDecoder.getRecordCount();
        if (this.blockCount < 0) {
          throw new UnsupportedOptionsException("XZ file has over 2147483647 Blocks");
        }
        this.streams.add(localIndexDecoder);
        l1 = 0L;
        i = paramInt;
      }
    }
    assert (l2 == 0L);
    this.memoryLimit = i;
    paramSeekableInputStream = (IndexDecoder)this.streams.get(this.streams.size() - 1);
    paramInt = this.streams.size() - 2;
    while (paramInt >= 0)
    {
      localObject = (IndexDecoder)this.streams.get(paramInt);
      ((IndexDecoder)localObject).setOffsets(paramSeekableInputStream);
      paramSeekableInputStream = (SeekableInputStream)localObject;
      paramInt -= 1;
    }
    paramSeekableInputStream = (IndexDecoder)this.streams.get(this.streams.size() - 1);
    this.curBlockInfo = new BlockInfo(paramSeekableInputStream);
    this.queriedBlockInfo = new BlockInfo(paramSeekableInputStream);
  }
  
  private void initBlockDecoder()
    throws IOException
  {
    try
    {
      this.blockDecoder = null;
      this.blockDecoder = new BlockInputStream(this.in, this.check, this.memoryLimit, this.curBlockInfo.unpaddedSize, this.curBlockInfo.uncompressedSize);
      return;
    }
    catch (MemoryLimitException localMemoryLimitException)
    {
      assert (this.memoryLimit >= 0);
      throw new MemoryLimitException(localMemoryLimitException.getMemoryNeeded() + this.indexMemoryUsage, this.memoryLimit + this.indexMemoryUsage);
    }
    catch (IndexIndicatorException localIndexIndicatorException)
    {
      throw new CorruptedInputException();
    }
  }
  
  private void locateBlockByNumber(BlockInfo paramBlockInfo, int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= this.blockCount)) {
      throw new IndexOutOfBoundsException("Invalid XZ Block number: " + paramInt);
    }
    if (paramBlockInfo.blockNumber == paramInt) {
      return;
    }
    int i = 0;
    for (;;)
    {
      IndexDecoder localIndexDecoder = (IndexDecoder)this.streams.get(i);
      if (localIndexDecoder.hasRecord(paramInt))
      {
        localIndexDecoder.setBlockInfo(paramBlockInfo, paramInt);
        return;
      }
      i += 1;
    }
  }
  
  private void locateBlockByPos(BlockInfo paramBlockInfo, long paramLong)
  {
    if ((paramLong < 0L) || (paramLong >= this.uncompressedSize)) {
      throw new IndexOutOfBoundsException("Invalid uncompressed position: " + paramLong);
    }
    int i = 0;
    for (;;)
    {
      IndexDecoder localIndexDecoder = (IndexDecoder)this.streams.get(i);
      if (localIndexDecoder.hasUncompressedOffset(paramLong))
      {
        localIndexDecoder.locateBlock(paramBlockInfo, paramLong);
        if (($assertionsDisabled) || ((paramBlockInfo.compressedOffset & 0x3) == 0L)) {
          break;
        }
        throw new AssertionError();
      }
      i += 1;
    }
    assert (paramBlockInfo.uncompressedSize > 0L);
    assert (paramLong >= paramBlockInfo.uncompressedOffset);
    assert (paramLong < paramBlockInfo.uncompressedOffset + paramBlockInfo.uncompressedSize);
  }
  
  private void seek()
    throws IOException
  {
    if (!this.seekNeeded) {
      if (this.curBlockInfo.hasNext())
      {
        this.curBlockInfo.setNext();
        initBlockDecoder();
      }
    }
    do
    {
      return;
      this.seekPos = this.curPos;
      this.seekNeeded = false;
      if (this.seekPos >= this.uncompressedSize)
      {
        this.curPos = this.seekPos;
        this.blockDecoder = null;
        this.endReached = true;
        return;
      }
      this.endReached = false;
      locateBlockByPos(this.curBlockInfo, this.seekPos);
      if ((this.curPos <= this.curBlockInfo.uncompressedOffset) || (this.curPos > this.seekPos))
      {
        this.in.seek(this.curBlockInfo.compressedOffset);
        this.check = Check.getInstance(this.curBlockInfo.getCheckType());
        initBlockDecoder();
        this.curPos = this.curBlockInfo.uncompressedOffset;
      }
    } while (this.seekPos <= this.curPos);
    long l = this.seekPos - this.curPos;
    if (this.blockDecoder.skip(l) != l) {
      throw new CorruptedInputException();
    }
    this.curPos = this.seekPos;
  }
  
  public int available()
    throws IOException
  {
    if (this.in == null) {
      throw new XZIOException("Stream closed");
    }
    if (this.exception != null) {
      throw this.exception;
    }
    if ((this.endReached) || (this.seekNeeded) || (this.blockDecoder == null)) {
      return 0;
    }
    return this.blockDecoder.available();
  }
  
  public void close()
    throws IOException
  {
    if (this.in != null) {}
    try
    {
      this.in.close();
      return;
    }
    finally
    {
      this.in = null;
    }
  }
  
  public int getBlockCheckType(int paramInt)
  {
    locateBlockByNumber(this.queriedBlockInfo, paramInt);
    return this.queriedBlockInfo.getCheckType();
  }
  
  public long getBlockCompPos(int paramInt)
  {
    locateBlockByNumber(this.queriedBlockInfo, paramInt);
    return this.queriedBlockInfo.compressedOffset;
  }
  
  public long getBlockCompSize(int paramInt)
  {
    locateBlockByNumber(this.queriedBlockInfo, paramInt);
    return this.queriedBlockInfo.unpaddedSize + 3L & 0xFFFFFFFFFFFFFFFC;
  }
  
  public int getBlockCount()
  {
    return this.blockCount;
  }
  
  public int getBlockNumber(long paramLong)
  {
    locateBlockByPos(this.queriedBlockInfo, paramLong);
    return this.queriedBlockInfo.blockNumber;
  }
  
  public long getBlockPos(int paramInt)
  {
    locateBlockByNumber(this.queriedBlockInfo, paramInt);
    return this.queriedBlockInfo.uncompressedOffset;
  }
  
  public long getBlockSize(int paramInt)
  {
    locateBlockByNumber(this.queriedBlockInfo, paramInt);
    return this.queriedBlockInfo.uncompressedSize;
  }
  
  public int getCheckTypes()
  {
    return this.checkTypes;
  }
  
  public int getIndexMemoryUsage()
  {
    return this.indexMemoryUsage;
  }
  
  public long getLargestBlockSize()
  {
    return this.largestBlockSize;
  }
  
  public int getStreamCount()
  {
    return this.streams.size();
  }
  
  public long length()
  {
    return this.uncompressedSize;
  }
  
  public long position()
    throws IOException
  {
    if (this.in == null) {
      throw new XZIOException("Stream closed");
    }
    if (this.seekNeeded) {
      return this.seekPos;
    }
    return this.curPos;
  }
  
  public int read()
    throws IOException
  {
    if (read(this.tempBuf, 0, 1) == -1) {
      return -1;
    }
    return this.tempBuf[0] & 0xFF;
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length)) {
      throw new IndexOutOfBoundsException();
    }
    if (paramInt2 == 0) {
      paramInt1 = 0;
    }
    int j;
    for (;;)
    {
      return paramInt1;
      if (this.in == null) {
        throw new XZIOException("Stream closed");
      }
      if (this.exception != null) {
        throw this.exception;
      }
      int m = 0;
      int i = 0;
      j = m;
      try
      {
        if (this.seekNeeded)
        {
          j = m;
          seek();
        }
        int k = paramInt1;
        j = m;
        if (this.endReached)
        {
          return -1;
          label109:
          j = i;
          paramInt1 = this.blockDecoder.read(paramArrayOfByte, k, paramInt2);
          if (paramInt1 <= 0) {
            break label200;
          }
          j = i;
          this.curPos += paramInt1;
          i += paramInt1;
          k += paramInt1;
          paramInt2 -= paramInt1;
        }
        for (;;)
        {
          paramInt1 = i;
          if (paramInt2 <= 0) {
            break;
          }
          j = i;
          if (this.blockDecoder != null) {
            break label109;
          }
          j = i;
          seek();
          j = i;
          if (!this.endReached) {
            break label109;
          }
          return i;
          label200:
          if (paramInt1 == -1)
          {
            j = i;
            this.blockDecoder = null;
          }
        }
        if (j != 0) {}
      }
      catch (IOException localIOException)
      {
        paramArrayOfByte = localIOException;
        if ((localIOException instanceof EOFException)) {
          paramArrayOfByte = new CorruptedInputException();
        }
        this.exception = paramArrayOfByte;
        paramInt1 = j;
      }
    }
    throw paramArrayOfByte;
  }
  
  public void seek(long paramLong)
    throws IOException
  {
    if (this.in == null) {
      throw new XZIOException("Stream closed");
    }
    if (paramLong < 0L) {
      throw new XZIOException("Negative seek position: " + paramLong);
    }
    this.seekPos = paramLong;
    this.seekNeeded = true;
  }
  
  public void seekToBlock(int paramInt)
    throws IOException
  {
    if (this.in == null) {
      throw new XZIOException("Stream closed");
    }
    if ((paramInt < 0) || (paramInt >= this.blockCount)) {
      throw new XZIOException("Invalid XZ Block number: " + paramInt);
    }
    this.seekPos = getBlockPos(paramInt);
    this.seekNeeded = true;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/SeekableXZInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */