package org.tukaani.xz;

import java.io.InputStream;
import org.tukaani.xz.simple.PowerPC;

public class PowerPCOptions
  extends BCJOptions
{
  private static final int ALIGNMENT = 4;
  
  public PowerPCOptions()
  {
    super(4);
  }
  
  FilterEncoder getFilterEncoder()
  {
    return new BCJEncoder(this, 5L);
  }
  
  public InputStream getInputStream(InputStream paramInputStream)
  {
    return new SimpleInputStream(paramInputStream, new PowerPC(false, this.startOffset));
  }
  
  public FinishableOutputStream getOutputStream(FinishableOutputStream paramFinishableOutputStream)
  {
    return new SimpleOutputStream(paramFinishableOutputStream, new PowerPC(true, this.startOffset));
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/PowerPCOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */