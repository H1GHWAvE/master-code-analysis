package org.tukaani.xz;

abstract class BCJCoder
  implements FilterCoder
{
  public static final long ARMTHUMB_FILTER_ID = 8L;
  public static final long ARM_FILTER_ID = 7L;
  public static final long IA64_FILTER_ID = 6L;
  public static final long POWERPC_FILTER_ID = 5L;
  public static final long SPARC_FILTER_ID = 9L;
  public static final long X86_FILTER_ID = 4L;
  
  public static boolean isBCJFilterID(long paramLong)
  {
    return (paramLong >= 4L) && (paramLong <= 9L);
  }
  
  public boolean changesSize()
  {
    return false;
  }
  
  public boolean lastOK()
  {
    return false;
  }
  
  public boolean nonLastOK()
  {
    return true;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/BCJCoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */