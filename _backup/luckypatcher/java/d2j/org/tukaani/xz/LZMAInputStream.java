package org.tukaani.xz;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.tukaani.xz.lz.LZDecoder;
import org.tukaani.xz.lzma.LZMADecoder;
import org.tukaani.xz.rangecoder.RangeDecoderFromStream;

public class LZMAInputStream
  extends InputStream
{
  public static final int DICT_SIZE_MAX = 2147483632;
  private boolean endReached = false;
  private IOException exception = null;
  private InputStream in;
  private LZDecoder lz;
  private LZMADecoder lzma;
  private RangeDecoderFromStream rc;
  private long remainingSize;
  private final byte[] tempBuf = new byte[1];
  
  static
  {
    if (!LZMAInputStream.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  public LZMAInputStream(InputStream paramInputStream)
    throws IOException
  {
    this(paramInputStream, -1);
  }
  
  public LZMAInputStream(InputStream paramInputStream, int paramInt)
    throws IOException
  {
    DataInputStream localDataInputStream = new DataInputStream(paramInputStream);
    byte b = localDataInputStream.readByte();
    int i = 0;
    int j = 0;
    while (j < 4)
    {
      i |= localDataInputStream.readUnsignedByte() << j * 8;
      j += 1;
    }
    long l = 0L;
    j = 0;
    while (j < 8)
    {
      l |= localDataInputStream.readUnsignedByte() << j * 8;
      j += 1;
    }
    j = getMemoryUsage(i, b);
    if ((paramInt != -1) && (j > paramInt)) {
      throw new MemoryLimitException(j, paramInt);
    }
    initialize(paramInputStream, l, b, i, null);
  }
  
  public LZMAInputStream(InputStream paramInputStream, long paramLong, byte paramByte, int paramInt)
    throws IOException
  {
    initialize(paramInputStream, paramLong, paramByte, paramInt, null);
  }
  
  public LZMAInputStream(InputStream paramInputStream, long paramLong, byte paramByte, int paramInt, byte[] paramArrayOfByte)
    throws IOException
  {
    initialize(paramInputStream, paramLong, paramByte, paramInt, paramArrayOfByte);
  }
  
  public LZMAInputStream(InputStream paramInputStream, long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfByte)
    throws IOException
  {
    initialize(paramInputStream, paramLong, paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfByte);
  }
  
  private static int getDictSize(int paramInt)
  {
    if ((paramInt < 0) || (paramInt > 2147483632)) {
      throw new IllegalArgumentException("LZMA dictionary is too big for this implementation");
    }
    int i = paramInt;
    if (paramInt < 4096) {
      i = 4096;
    }
    return i + 15 & 0xFFFFFFF0;
  }
  
  public static int getMemoryUsage(int paramInt, byte paramByte)
    throws UnsupportedOptionsException, CorruptedInputException
  {
    if ((paramInt < 0) || (paramInt > 2147483632)) {
      throw new UnsupportedOptionsException("LZMA dictionary is too big for this implementation");
    }
    paramByte &= 0xFF;
    if (paramByte > 224) {
      throw new CorruptedInputException("Invalid LZMA properties byte");
    }
    paramByte %= 45;
    int i = paramByte / 9;
    return getMemoryUsage(paramInt, paramByte - i * 9, i);
  }
  
  public static int getMemoryUsage(int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramInt2 < 0) || (paramInt2 > 8) || (paramInt3 < 0) || (paramInt3 > 4)) {
      throw new IllegalArgumentException("Invalid lc or lp");
    }
    return getDictSize(paramInt1) / 1024 + 10 + (1536 << paramInt2 + paramInt3) / 1024;
  }
  
  private void initialize(InputStream paramInputStream, long paramLong, byte paramByte, int paramInt, byte[] paramArrayOfByte)
    throws IOException
  {
    if (paramLong < -1L) {
      throw new UnsupportedOptionsException("Uncompressed size is too big");
    }
    int i = paramByte & 0xFF;
    if (i > 224) {
      throw new CorruptedInputException("Invalid LZMA properties byte");
    }
    paramByte = i / 45;
    i -= paramByte * 9 * 5;
    int j = i / 9;
    if ((paramInt < 0) || (paramInt > 2147483632)) {
      throw new UnsupportedOptionsException("LZMA dictionary is too big for this implementation");
    }
    initialize(paramInputStream, paramLong, i - j * 9, j, paramByte, paramInt, paramArrayOfByte);
  }
  
  private void initialize(InputStream paramInputStream, long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfByte)
    throws IOException
  {
    if ((paramLong < -1L) || (paramInt1 < 0) || (paramInt1 > 8) || (paramInt2 < 0) || (paramInt2 > 4) || (paramInt3 < 0) || (paramInt3 > 4)) {
      throw new IllegalArgumentException();
    }
    this.in = paramInputStream;
    int i = getDictSize(paramInt4);
    paramInt4 = i;
    if (paramLong >= 0L)
    {
      paramInt4 = i;
      if (i > paramLong) {
        paramInt4 = getDictSize((int)paramLong);
      }
    }
    this.lz = new LZDecoder(getDictSize(paramInt4), paramArrayOfByte);
    this.rc = new RangeDecoderFromStream(paramInputStream);
    this.lzma = new LZMADecoder(this.lz, this.rc, paramInt1, paramInt2, paramInt3);
    this.remainingSize = paramLong;
  }
  
  public void close()
    throws IOException
  {
    if (this.in != null) {}
    try
    {
      this.in.close();
      return;
    }
    finally
    {
      this.in = null;
    }
  }
  
  public int read()
    throws IOException
  {
    if (read(this.tempBuf, 0, 1) == -1) {
      return -1;
    }
    return this.tempBuf[0] & 0xFF;
  }
  
  /* Error */
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    // Byte code:
    //   0: iload_2
    //   1: iflt +21 -> 22
    //   4: iload_3
    //   5: iflt +17 -> 22
    //   8: iload_2
    //   9: iload_3
    //   10: iadd
    //   11: iflt +11 -> 22
    //   14: iload_2
    //   15: iload_3
    //   16: iadd
    //   17: aload_1
    //   18: arraylength
    //   19: if_icmple +11 -> 30
    //   22: new 141	java/lang/IndexOutOfBoundsException
    //   25: dup
    //   26: invokespecial 142	java/lang/IndexOutOfBoundsException:<init>	()V
    //   29: athrow
    //   30: iload_3
    //   31: ifne +9 -> 40
    //   34: iconst_0
    //   35: istore 4
    //   37: iload 4
    //   39: ireturn
    //   40: aload_0
    //   41: getfield 111	org/tukaani/xz/LZMAInputStream:in	Ljava/io/InputStream;
    //   44: ifnonnull +13 -> 57
    //   47: new 144	org/tukaani/xz/XZIOException
    //   50: dup
    //   51: ldc -110
    //   53: invokespecial 147	org/tukaani/xz/XZIOException:<init>	(Ljava/lang/String;)V
    //   56: athrow
    //   57: aload_0
    //   58: getfield 51	org/tukaani/xz/LZMAInputStream:exception	Ljava/io/IOException;
    //   61: ifnull +8 -> 69
    //   64: aload_0
    //   65: getfield 51	org/tukaani/xz/LZMAInputStream:exception	Ljava/io/IOException;
    //   68: athrow
    //   69: aload_0
    //   70: getfield 47	org/tukaani/xz/LZMAInputStream:endReached	Z
    //   73: ifeq +5 -> 78
    //   76: iconst_m1
    //   77: ireturn
    //   78: iconst_0
    //   79: istore 4
    //   81: iload_2
    //   82: istore 5
    //   84: iload 4
    //   86: istore_2
    //   87: iload_2
    //   88: istore 4
    //   90: iload_3
    //   91: ifle -54 -> 37
    //   94: iload_3
    //   95: istore 4
    //   97: iload 4
    //   99: istore 6
    //   101: aload_0
    //   102: getfield 132	org/tukaani/xz/LZMAInputStream:remainingSize	J
    //   105: lconst_0
    //   106: lcmp
    //   107: iflt +24 -> 131
    //   110: iload 4
    //   112: istore 6
    //   114: aload_0
    //   115: getfield 132	org/tukaani/xz/LZMAInputStream:remainingSize	J
    //   118: iload_3
    //   119: i2l
    //   120: lcmp
    //   121: ifge +10 -> 131
    //   124: aload_0
    //   125: getfield 132	org/tukaani/xz/LZMAInputStream:remainingSize	J
    //   128: l2i
    //   129: istore 6
    //   131: aload_0
    //   132: getfield 118	org/tukaani/xz/LZMAInputStream:lz	Lorg/tukaani/xz/lz/LZDecoder;
    //   135: iload 6
    //   137: invokevirtual 151	org/tukaani/xz/lz/LZDecoder:setLimit	(I)V
    //   140: aload_0
    //   141: getfield 130	org/tukaani/xz/LZMAInputStream:lzma	Lorg/tukaani/xz/lzma/LZMADecoder;
    //   144: invokevirtual 154	org/tukaani/xz/lzma/LZMADecoder:decode	()V
    //   147: aload_0
    //   148: getfield 118	org/tukaani/xz/LZMAInputStream:lz	Lorg/tukaani/xz/lz/LZDecoder;
    //   151: aload_1
    //   152: iload 5
    //   154: invokevirtual 158	org/tukaani/xz/lz/LZDecoder:flush	([BI)I
    //   157: istore 4
    //   159: iload 5
    //   161: iload 4
    //   163: iadd
    //   164: istore 5
    //   166: iload_3
    //   167: iload 4
    //   169: isub
    //   170: istore_3
    //   171: iload_2
    //   172: iload 4
    //   174: iadd
    //   175: istore 6
    //   177: aload_0
    //   178: getfield 132	org/tukaani/xz/LZMAInputStream:remainingSize	J
    //   181: lconst_0
    //   182: lcmp
    //   183: iflt +101 -> 284
    //   186: aload_0
    //   187: aload_0
    //   188: getfield 132	org/tukaani/xz/LZMAInputStream:remainingSize	J
    //   191: iload 4
    //   193: i2l
    //   194: lsub
    //   195: putfield 132	org/tukaani/xz/LZMAInputStream:remainingSize	J
    //   198: getstatic 34	org/tukaani/xz/LZMAInputStream:$assertionsDisabled	Z
    //   201: ifne +69 -> 270
    //   204: aload_0
    //   205: getfield 132	org/tukaani/xz/LZMAInputStream:remainingSize	J
    //   208: lconst_0
    //   209: lcmp
    //   210: ifge +60 -> 270
    //   213: new 160	java/lang/AssertionError
    //   216: dup
    //   217: invokespecial 161	java/lang/AssertionError:<init>	()V
    //   220: athrow
    //   221: astore_1
    //   222: aload_0
    //   223: aload_1
    //   224: putfield 51	org/tukaani/xz/LZMAInputStream:exception	Ljava/io/IOException;
    //   227: aload_1
    //   228: athrow
    //   229: astore 7
    //   231: aload_0
    //   232: getfield 132	org/tukaani/xz/LZMAInputStream:remainingSize	J
    //   235: ldc2_w 105
    //   238: lcmp
    //   239: ifne +13 -> 252
    //   242: aload_0
    //   243: getfield 130	org/tukaani/xz/LZMAInputStream:lzma	Lorg/tukaani/xz/lzma/LZMADecoder;
    //   246: invokevirtual 164	org/tukaani/xz/lzma/LZMADecoder:endMarkerDetected	()Z
    //   249: ifne +6 -> 255
    //   252: aload 7
    //   254: athrow
    //   255: aload_0
    //   256: iconst_1
    //   257: putfield 47	org/tukaani/xz/LZMAInputStream:endReached	Z
    //   260: aload_0
    //   261: getfield 123	org/tukaani/xz/LZMAInputStream:rc	Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;
    //   264: invokevirtual 167	org/tukaani/xz/rangecoder/RangeDecoderFromStream:normalize	()V
    //   267: goto -120 -> 147
    //   270: aload_0
    //   271: getfield 132	org/tukaani/xz/LZMAInputStream:remainingSize	J
    //   274: lconst_0
    //   275: lcmp
    //   276: ifne +8 -> 284
    //   279: aload_0
    //   280: iconst_1
    //   281: putfield 47	org/tukaani/xz/LZMAInputStream:endReached	Z
    //   284: iload 6
    //   286: istore_2
    //   287: aload_0
    //   288: getfield 47	org/tukaani/xz/LZMAInputStream:endReached	Z
    //   291: ifeq -204 -> 87
    //   294: aload_0
    //   295: getfield 123	org/tukaani/xz/LZMAInputStream:rc	Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;
    //   298: invokevirtual 170	org/tukaani/xz/rangecoder/RangeDecoderFromStream:isFinished	()Z
    //   301: ifeq +13 -> 314
    //   304: aload_0
    //   305: getfield 118	org/tukaani/xz/LZMAInputStream:lz	Lorg/tukaani/xz/lz/LZDecoder;
    //   308: invokevirtual 173	org/tukaani/xz/lz/LZDecoder:hasPending	()Z
    //   311: ifeq +11 -> 322
    //   314: new 93	org/tukaani/xz/CorruptedInputException
    //   317: dup
    //   318: invokespecial 174	org/tukaani/xz/CorruptedInputException:<init>	()V
    //   321: athrow
    //   322: iload 6
    //   324: istore 4
    //   326: iload 6
    //   328: ifne -291 -> 37
    //   331: iconst_m1
    //   332: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	333	0	this	LZMAInputStream
    //   0	333	1	paramArrayOfByte	byte[]
    //   0	333	2	paramInt1	int
    //   0	333	3	paramInt2	int
    //   35	290	4	i	int
    //   82	83	5	j	int
    //   99	228	6	k	int
    //   229	24	7	localCorruptedInputException	CorruptedInputException
    // Exception table:
    //   from	to	target	type
    //   101	110	221	java/io/IOException
    //   114	131	221	java/io/IOException
    //   131	140	221	java/io/IOException
    //   140	147	221	java/io/IOException
    //   147	159	221	java/io/IOException
    //   177	221	221	java/io/IOException
    //   231	252	221	java/io/IOException
    //   252	255	221	java/io/IOException
    //   255	267	221	java/io/IOException
    //   270	284	221	java/io/IOException
    //   287	314	221	java/io/IOException
    //   314	322	221	java/io/IOException
    //   140	147	229	org/tukaani/xz/CorruptedInputException
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/LZMAInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */