package org.tukaani.xz;

import java.io.InputStream;
import org.tukaani.xz.simple.X86;

public class X86Options
  extends BCJOptions
{
  private static final int ALIGNMENT = 1;
  
  public X86Options()
  {
    super(1);
  }
  
  FilterEncoder getFilterEncoder()
  {
    return new BCJEncoder(this, 4L);
  }
  
  public InputStream getInputStream(InputStream paramInputStream)
  {
    return new SimpleInputStream(paramInputStream, new X86(false, this.startOffset));
  }
  
  public FinishableOutputStream getOutputStream(FinishableOutputStream paramFinishableOutputStream)
  {
    return new SimpleOutputStream(paramFinishableOutputStream, new X86(true, this.startOffset));
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/X86Options.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */