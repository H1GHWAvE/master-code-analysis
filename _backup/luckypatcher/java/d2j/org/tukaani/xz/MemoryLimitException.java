package org.tukaani.xz;

public class MemoryLimitException
  extends XZIOException
{
  private static final long serialVersionUID = 3L;
  private final int memoryLimit;
  private final int memoryNeeded;
  
  public MemoryLimitException(int paramInt1, int paramInt2)
  {
    super("" + paramInt1 + " KiB of memory would be needed; limit was " + paramInt2 + " KiB");
    this.memoryNeeded = paramInt1;
    this.memoryLimit = paramInt2;
  }
  
  public int getMemoryLimit()
  {
    return this.memoryLimit;
  }
  
  public int getMemoryNeeded()
  {
    return this.memoryNeeded;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/MemoryLimitException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */