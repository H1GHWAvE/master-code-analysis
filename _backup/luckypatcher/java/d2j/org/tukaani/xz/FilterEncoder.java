package org.tukaani.xz;

abstract interface FilterEncoder
  extends FilterCoder
{
  public abstract long getFilterID();
  
  public abstract byte[] getFilterProps();
  
  public abstract FinishableOutputStream getOutputStream(FinishableOutputStream paramFinishableOutputStream);
  
  public abstract boolean supportsFlushing();
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/FilterEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */