package org.tukaani.xz.lz;

final class Hash234
  extends CRC32Hash
{
  private static final int HASH_2_MASK = 1023;
  private static final int HASH_2_SIZE = 1024;
  private static final int HASH_3_MASK = 65535;
  private static final int HASH_3_SIZE = 65536;
  private final int[] hash2Table = new int['Ѐ'];
  private int hash2Value = 0;
  private final int[] hash3Table = new int[65536];
  private int hash3Value = 0;
  private final int hash4Mask;
  private final int[] hash4Table;
  private int hash4Value = 0;
  
  Hash234(int paramInt)
  {
    this.hash4Table = new int[getHash4Size(paramInt)];
    this.hash4Mask = (this.hash4Table.length - 1);
  }
  
  static int getHash4Size(int paramInt)
  {
    paramInt -= 1;
    paramInt |= paramInt >>> 1;
    paramInt |= paramInt >>> 2;
    paramInt |= paramInt >>> 4;
    int i = (paramInt | paramInt >>> 8) >>> 1 | 0xFFFF;
    paramInt = i;
    if (i > 16777216) {
      paramInt = i >>> 1;
    }
    return paramInt + 1;
  }
  
  static int getMemoryUsage(int paramInt)
  {
    return (66560 + getHash4Size(paramInt)) / 256 + 4;
  }
  
  void calcHashes(byte[] paramArrayOfByte, int paramInt)
  {
    int i = crcTable[(paramArrayOfByte[paramInt] & 0xFF)] ^ paramArrayOfByte[(paramInt + 1)] & 0xFF;
    this.hash2Value = (i & 0x3FF);
    i ^= (paramArrayOfByte[(paramInt + 2)] & 0xFF) << 8;
    this.hash3Value = (0xFFFF & i);
    paramInt = crcTable[(paramArrayOfByte[(paramInt + 3)] & 0xFF)];
    this.hash4Value = (this.hash4Mask & (i ^ paramInt << 5));
  }
  
  int getHash2Pos()
  {
    return this.hash2Table[this.hash2Value];
  }
  
  int getHash3Pos()
  {
    return this.hash3Table[this.hash3Value];
  }
  
  int getHash4Pos()
  {
    return this.hash4Table[this.hash4Value];
  }
  
  void normalize(int paramInt)
  {
    LZEncoder.normalize(this.hash2Table, paramInt);
    LZEncoder.normalize(this.hash3Table, paramInt);
    LZEncoder.normalize(this.hash4Table, paramInt);
  }
  
  void updateTables(int paramInt)
  {
    this.hash2Table[this.hash2Value] = paramInt;
    this.hash3Table[this.hash3Value] = paramInt;
    this.hash4Table[this.hash4Value] = paramInt;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/lz/Hash234.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */