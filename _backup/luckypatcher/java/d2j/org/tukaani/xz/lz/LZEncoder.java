package org.tukaani.xz.lz;

import java.io.IOException;
import java.io.OutputStream;

public abstract class LZEncoder
{
  public static final int MF_BT4 = 20;
  public static final int MF_HC4 = 4;
  final byte[] buf;
  private boolean finishing = false;
  private final int keepSizeAfter;
  private final int keepSizeBefore;
  final int matchLenMax;
  final int niceLen;
  private int pendingSize = 0;
  private int readLimit = -1;
  int readPos = -1;
  private int writePos = 0;
  
  static
  {
    if (!LZEncoder.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  LZEncoder(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    this.buf = new byte[getBufSize(paramInt1, paramInt2, paramInt3, paramInt5)];
    this.keepSizeBefore = (paramInt2 + paramInt1);
    this.keepSizeAfter = (paramInt3 + paramInt5);
    this.matchLenMax = paramInt5;
    this.niceLen = paramInt4;
  }
  
  private static int getBufSize(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return paramInt2 + paramInt1 + (paramInt3 + paramInt4) + Math.min(paramInt1 / 2 + 262144, 536870912);
  }
  
  public static LZEncoder getInstance(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7)
  {
    switch (paramInt6)
    {
    default: 
      throw new IllegalArgumentException();
    case 4: 
      return new HC4(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt7);
    }
    return new BT4(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt7);
  }
  
  public static int getMemoryUsage(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    paramInt2 = getBufSize(paramInt1, paramInt2, paramInt3, paramInt4) / 1024 + 10;
    switch (paramInt5)
    {
    default: 
      throw new IllegalArgumentException();
    case 4: 
      return paramInt2 + HC4.getMemoryUsage(paramInt1);
    }
    return paramInt2 + BT4.getMemoryUsage(paramInt1);
  }
  
  private void moveWindow()
  {
    int i = this.readPos + 1 - this.keepSizeBefore & 0xFFFFFFF0;
    int j = this.writePos;
    System.arraycopy(this.buf, i, this.buf, 0, j - i);
    this.readPos -= i;
    this.readLimit -= i;
    this.writePos -= i;
  }
  
  static void normalize(int[] paramArrayOfInt, int paramInt)
  {
    int i = 0;
    if (i < paramArrayOfInt.length)
    {
      if (paramArrayOfInt[i] <= paramInt) {
        paramArrayOfInt[i] = 0;
      }
      for (;;)
      {
        i += 1;
        break;
        paramArrayOfInt[i] -= paramInt;
      }
    }
  }
  
  private void processPendingBytes()
  {
    if ((this.pendingSize > 0) && (this.readPos < this.readLimit))
    {
      this.readPos -= this.pendingSize;
      int i = this.pendingSize;
      this.pendingSize = 0;
      skip(i);
      assert (this.pendingSize < i);
    }
  }
  
  public void copyUncompressed(OutputStream paramOutputStream, int paramInt1, int paramInt2)
    throws IOException
  {
    paramOutputStream.write(this.buf, this.readPos + 1 - paramInt1, paramInt2);
  }
  
  public int fillWindow(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    assert (!this.finishing);
    if (this.readPos >= this.buf.length - this.keepSizeAfter) {
      moveWindow();
    }
    int i = paramInt2;
    if (paramInt2 > this.buf.length - this.writePos) {
      i = this.buf.length - this.writePos;
    }
    System.arraycopy(paramArrayOfByte, paramInt1, this.buf, this.writePos, i);
    this.writePos += i;
    if (this.writePos >= this.keepSizeAfter) {
      this.readLimit = (this.writePos - this.keepSizeAfter);
    }
    processPendingBytes();
    return i;
  }
  
  public int getAvail()
  {
    assert (isStarted());
    return this.writePos - this.readPos;
  }
  
  public int getByte(int paramInt)
  {
    return this.buf[(this.readPos - paramInt)] & 0xFF;
  }
  
  public int getByte(int paramInt1, int paramInt2)
  {
    return this.buf[(this.readPos + paramInt1 - paramInt2)] & 0xFF;
  }
  
  public int getMatchLen(int paramInt1, int paramInt2)
  {
    int j = this.readPos;
    int i = 0;
    while ((i < paramInt2) && (this.buf[(this.readPos + i)] == this.buf[(j - paramInt1 - 1 + i)])) {
      i += 1;
    }
    return i;
  }
  
  public int getMatchLen(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = this.readPos + paramInt1;
    paramInt1 = 0;
    while ((paramInt1 < paramInt3) && (this.buf[(i + paramInt1)] == this.buf[(i - paramInt2 - 1 + paramInt1)])) {
      paramInt1 += 1;
    }
    return paramInt1;
  }
  
  public abstract Matches getMatches();
  
  public int getPos()
  {
    return this.readPos;
  }
  
  public boolean hasEnoughData(int paramInt)
  {
    return this.readPos - paramInt < this.readLimit;
  }
  
  public boolean isStarted()
  {
    return this.readPos != -1;
  }
  
  int movePos(int paramInt1, int paramInt2)
  {
    assert (paramInt1 >= paramInt2);
    this.readPos += 1;
    int j = this.writePos - this.readPos;
    int i = j;
    if (j < paramInt1) {
      if (j >= paramInt2)
      {
        i = j;
        if (this.finishing) {}
      }
      else
      {
        this.pendingSize += 1;
        i = 0;
      }
    }
    return i;
  }
  
  public void setFinishing()
  {
    this.readLimit = (this.writePos - 1);
    this.finishing = true;
    processPendingBytes();
  }
  
  public void setFlushing()
  {
    this.readLimit = (this.writePos - 1);
    processPendingBytes();
  }
  
  public void setPresetDict(int paramInt, byte[] paramArrayOfByte)
  {
    assert (!isStarted());
    assert (this.writePos == 0);
    if (paramArrayOfByte != null)
    {
      paramInt = Math.min(paramArrayOfByte.length, paramInt);
      System.arraycopy(paramArrayOfByte, paramArrayOfByte.length - paramInt, this.buf, 0, paramInt);
      this.writePos += paramInt;
      skip(paramInt);
    }
  }
  
  public abstract void skip(int paramInt);
  
  public boolean verifyMatches(Matches paramMatches)
  {
    int j = Math.min(getAvail(), this.matchLenMax);
    int i = 0;
    while (i < paramMatches.count)
    {
      if (getMatchLen(paramMatches.dist[i], j) != paramMatches.len[i]) {
        return false;
      }
      i += 1;
    }
    return true;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/lz/LZEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */