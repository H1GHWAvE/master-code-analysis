package org.tukaani.xz.lz;

import java.io.DataInputStream;
import java.io.IOException;
import org.tukaani.xz.CorruptedInputException;

public final class LZDecoder
{
  private final byte[] buf;
  private int full = 0;
  private int limit = 0;
  private int pendingDist = 0;
  private int pendingLen = 0;
  private int pos = 0;
  private int start = 0;
  
  public LZDecoder(int paramInt, byte[] paramArrayOfByte)
  {
    this.buf = new byte[paramInt];
    if (paramArrayOfByte != null)
    {
      this.pos = Math.min(paramArrayOfByte.length, paramInt);
      this.full = this.pos;
      this.start = this.pos;
      System.arraycopy(paramArrayOfByte, paramArrayOfByte.length - this.pos, this.buf, 0, this.pos);
    }
  }
  
  public void copyUncompressed(DataInputStream paramDataInputStream, int paramInt)
    throws IOException
  {
    paramInt = Math.min(this.buf.length - this.pos, paramInt);
    paramDataInputStream.readFully(this.buf, this.pos, paramInt);
    this.pos += paramInt;
    if (this.full < this.pos) {
      this.full = this.pos;
    }
  }
  
  public int flush(byte[] paramArrayOfByte, int paramInt)
  {
    int i = this.pos - this.start;
    if (this.pos == this.buf.length) {
      this.pos = 0;
    }
    System.arraycopy(this.buf, this.start, paramArrayOfByte, paramInt, i);
    this.start = this.pos;
    return i;
  }
  
  public int getByte(int paramInt)
  {
    int j = this.pos - paramInt - 1;
    int i = j;
    if (paramInt >= this.pos) {
      i = j + this.buf.length;
    }
    return this.buf[i] & 0xFF;
  }
  
  public int getPos()
  {
    return this.pos;
  }
  
  public boolean hasPending()
  {
    return this.pendingLen > 0;
  }
  
  public boolean hasSpace()
  {
    return this.pos < this.limit;
  }
  
  public void putByte(byte paramByte)
  {
    byte[] arrayOfByte = this.buf;
    int i = this.pos;
    this.pos = (i + 1);
    arrayOfByte[i] = paramByte;
    if (this.full < this.pos) {
      this.full = this.pos;
    }
  }
  
  public void repeat(int paramInt1, int paramInt2)
    throws IOException
  {
    if ((paramInt1 < 0) || (paramInt1 >= this.full)) {
      throw new CorruptedInputException();
    }
    int j = Math.min(this.limit - this.pos, paramInt2);
    this.pendingLen = (paramInt2 - j);
    this.pendingDist = paramInt1;
    int k = this.pos - paramInt1 - 1;
    paramInt2 = k;
    int i = j;
    if (paramInt1 >= this.pos)
    {
      paramInt2 = k + this.buf.length;
      i = j;
    }
    byte[] arrayOfByte1 = this.buf;
    j = this.pos;
    this.pos = (j + 1);
    byte[] arrayOfByte2 = this.buf;
    paramInt1 = paramInt2 + 1;
    arrayOfByte1[j] = arrayOfByte2[paramInt2];
    if (paramInt1 == this.buf.length) {}
    for (paramInt2 = 0;; paramInt2 = paramInt1)
    {
      paramInt1 = i - 1;
      i = paramInt1;
      if (paramInt1 > 0) {
        break;
      }
      if (this.full < this.pos) {
        this.full = this.pos;
      }
      return;
    }
  }
  
  public void repeatPending()
    throws IOException
  {
    if (this.pendingLen > 0) {
      repeat(this.pendingDist, this.pendingLen);
    }
  }
  
  public void reset()
  {
    this.start = 0;
    this.pos = 0;
    this.full = 0;
    this.limit = 0;
    this.buf[(this.buf.length - 1)] = 0;
  }
  
  public void setLimit(int paramInt)
  {
    if (this.buf.length - this.pos <= paramInt)
    {
      this.limit = this.buf.length;
      return;
    }
    this.limit = (this.pos + paramInt);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/lz/LZDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */