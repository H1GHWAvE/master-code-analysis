package org.tukaani.xz.lz;

final class BT4
  extends LZEncoder
{
  private int cyclicPos = -1;
  private final int cyclicSize;
  private final int depthLimit;
  private final Hash234 hash;
  private int lzPos;
  private final Matches matches;
  private final int[] tree;
  
  BT4(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
  {
    super(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    this.cyclicSize = (paramInt1 + 1);
    this.lzPos = this.cyclicSize;
    this.hash = new Hash234(paramInt1);
    this.tree = new int[this.cyclicSize * 2];
    this.matches = new Matches(paramInt4 - 1);
    if (paramInt6 > 0) {}
    for (;;)
    {
      this.depthLimit = paramInt6;
      return;
      paramInt6 = paramInt4 / 2 + 16;
    }
  }
  
  static int getMemoryUsage(int paramInt)
  {
    return Hash234.getMemoryUsage(paramInt) + paramInt / 128 + 10;
  }
  
  private int movePos()
  {
    int i = movePos(this.niceLen, 4);
    if (i != 0)
    {
      int j = this.lzPos + 1;
      this.lzPos = j;
      if (j == Integer.MAX_VALUE)
      {
        j = Integer.MAX_VALUE - this.cyclicSize;
        this.hash.normalize(j);
        normalize(this.tree, j);
        this.lzPos -= j;
      }
      j = this.cyclicPos + 1;
      this.cyclicPos = j;
      if (j == this.cyclicSize) {
        this.cyclicPos = 0;
      }
    }
    return i;
  }
  
  private void skip(int paramInt1, int paramInt2)
  {
    int i = this.depthLimit;
    int n = (this.cyclicPos << 1) + 1;
    int j = this.cyclicPos << 1;
    int m = 0;
    int k = 0;
    int i1 = paramInt2;
    int i4 = this.lzPos - i1;
    if ((i == 0) || (i4 >= this.cyclicSize))
    {
      this.tree[n] = 0;
      this.tree[j] = 0;
      return;
    }
    int i2 = this.cyclicPos;
    int i3;
    if (i4 > this.cyclicPos)
    {
      paramInt2 = this.cyclicSize;
      i3 = paramInt2 + (i2 - i4) << 1;
      i2 = Math.min(m, k);
      paramInt2 = i2;
      if (this.buf[(this.readPos + i2 - i4)] != this.buf[(this.readPos + i2)]) {}
    }
    else
    {
      do
      {
        paramInt2 = i2 + 1;
        if (paramInt2 == paramInt1)
        {
          this.tree[j] = this.tree[i3];
          this.tree[n] = this.tree[(i3 + 1)];
          return;
          paramInt2 = 0;
          break;
        }
        i2 = paramInt2;
      } while (this.buf[(this.readPos + paramInt2 - i4)] == this.buf[(this.readPos + paramInt2)]);
    }
    if ((this.buf[(this.readPos + paramInt2 - i4)] & 0xFF) < (this.buf[(this.readPos + paramInt2)] & 0xFF))
    {
      this.tree[j] = i1;
      j = i3 + 1;
      i1 = this.tree[j];
      k = paramInt2;
    }
    for (paramInt2 = i1;; paramInt2 = i1)
    {
      i -= 1;
      i1 = paramInt2;
      break;
      this.tree[n] = i1;
      n = i3;
      i1 = this.tree[n];
      m = paramInt2;
    }
  }
  
  public Matches getMatches()
  {
    this.matches.count = 0;
    int j = this.matchLenMax;
    int n = this.niceLen;
    int i = movePos();
    int k = j;
    int m = n;
    if (i < j)
    {
      if (i == 0) {
        return this.matches;
      }
      j = i;
      k = j;
      m = n;
      if (n > i)
      {
        m = i;
        k = j;
      }
    }
    this.hash.calcHashes(this.buf, this.readPos);
    int i2 = this.lzPos - this.hash.getHash2Pos();
    int i1 = this.lzPos - this.hash.getHash3Pos();
    int i6 = this.hash.getHash4Pos();
    this.hash.updateTables(this.lzPos);
    i = 0;
    j = i;
    if (i2 < this.cyclicSize)
    {
      j = i;
      if (this.buf[(this.readPos - i2)] == this.buf[this.readPos])
      {
        j = 2;
        this.matches.len[0] = 2;
        this.matches.dist[0] = (i2 - 1);
        this.matches.count = 1;
      }
    }
    n = i2;
    i = j;
    Object localObject;
    if (i2 != i1)
    {
      n = i2;
      i = j;
      if (i1 < this.cyclicSize)
      {
        n = i2;
        i = j;
        if (this.buf[(this.readPos - i1)] == this.buf[this.readPos])
        {
          i = 3;
          localObject = this.matches.dist;
          Matches localMatches = this.matches;
          j = localMatches.count;
          localMatches.count = (j + 1);
          localObject[j] = (i1 - 1);
          n = i1;
        }
      }
    }
    j = i;
    if (this.matches.count > 0)
    {
      while ((i < k) && (this.buf[(this.readPos + i - n)] == this.buf[(this.readPos + i)])) {
        i += 1;
      }
      this.matches.len[(this.matches.count - 1)] = i;
      j = i;
      if (i >= m)
      {
        skip(m, i6);
        return this.matches;
      }
    }
    i = j;
    if (j < 3) {
      i = 3;
    }
    i1 = this.depthLimit;
    int i3 = (this.cyclicPos << 1) + 1;
    n = this.cyclicPos << 1;
    int i4 = 0;
    i2 = 0;
    int i7 = i;
    int i9 = this.lzPos - i6;
    if ((i1 == 0) || (i9 >= this.cyclicSize))
    {
      this.tree[i3] = 0;
      this.tree[n] = 0;
      return this.matches;
    }
    j = this.cyclicPos;
    if (i9 > this.cyclicPos) {}
    int i8;
    int i5;
    for (i = this.cyclicSize;; i = 0)
    {
      i8 = i + (j - i9) << 1;
      j = Math.min(i4, i2);
      i = j;
      i5 = i7;
      if (this.buf[(this.readPos + j - i9)] != this.buf[(this.readPos + j)]) {
        break;
      }
      i = j;
      do
      {
        j = i + 1;
        if (j >= k) {
          break;
        }
        i = j;
      } while (this.buf[(this.readPos + j - i9)] == this.buf[(this.readPos + j)]);
      i = j;
      i5 = i7;
      if (j <= i7) {
        break;
      }
      i5 = j;
      this.matches.len[this.matches.count] = j;
      this.matches.dist[this.matches.count] = (i9 - 1);
      localObject = this.matches;
      ((Matches)localObject).count += 1;
      i = j;
      if (j < m) {
        break;
      }
      this.tree[n] = this.tree[i8];
      this.tree[i3] = this.tree[(i8 + 1)];
      return this.matches;
    }
    if ((this.buf[(this.readPos + i - i9)] & 0xFF) < (this.buf[(this.readPos + i)] & 0xFF))
    {
      this.tree[n] = i6;
      n = i8 + 1;
      j = this.tree[n];
      i2 = i;
    }
    for (i = j;; i = j)
    {
      i1 -= 1;
      i6 = i;
      i7 = i5;
      break;
      this.tree[i3] = i6;
      i3 = i8;
      j = this.tree[i3];
      i4 = i;
    }
  }
  
  public void skip(int paramInt)
  {
    for (;;)
    {
      int i = paramInt - 1;
      if (paramInt <= 0) {
        break;
      }
      int j = this.niceLen;
      int k = movePos();
      paramInt = j;
      if (k < j)
      {
        if (k == 0) {
          paramInt = i;
        } else {
          paramInt = k;
        }
      }
      else
      {
        this.hash.calcHashes(this.buf, this.readPos);
        j = this.hash.getHash4Pos();
        this.hash.updateTables(this.lzPos);
        skip(paramInt, j);
        paramInt = i;
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/lz/BT4.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */