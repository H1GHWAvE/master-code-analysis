package org.tukaani.xz.lz;

class CRC32Hash
{
  private static final int CRC32_POLY = -306674912;
  static final int[] crcTable = new int['Ā'];
  
  static
  {
    int i = 0;
    while (i < 256)
    {
      int j = i;
      int k = 0;
      if (k < 8)
      {
        if ((j & 0x1) != 0) {
          j = j >>> 1 ^ 0xEDB88320;
        }
        for (;;)
        {
          k += 1;
          break;
          j >>>= 1;
        }
      }
      crcTable[i] = j;
      i += 1;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/lz/CRC32Hash.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */