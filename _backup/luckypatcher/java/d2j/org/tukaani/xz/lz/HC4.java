package org.tukaani.xz.lz;

final class HC4
  extends LZEncoder
{
  private final int[] chain;
  private int cyclicPos = -1;
  private final int cyclicSize;
  private final int depthLimit;
  private final Hash234 hash;
  private int lzPos;
  private final Matches matches;
  
  static
  {
    if (!HC4.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  HC4(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
  {
    super(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    this.hash = new Hash234(paramInt1);
    this.cyclicSize = (paramInt1 + 1);
    this.chain = new int[this.cyclicSize];
    this.lzPos = this.cyclicSize;
    this.matches = new Matches(paramInt4 - 1);
    if (paramInt6 > 0) {}
    for (;;)
    {
      this.depthLimit = paramInt6;
      return;
      paramInt6 = paramInt4 / 4 + 4;
    }
  }
  
  static int getMemoryUsage(int paramInt)
  {
    return Hash234.getMemoryUsage(paramInt) + paramInt / 256 + 10;
  }
  
  private int movePos()
  {
    int i = movePos(4, 4);
    if (i != 0)
    {
      int j = this.lzPos + 1;
      this.lzPos = j;
      if (j == Integer.MAX_VALUE)
      {
        j = Integer.MAX_VALUE - this.cyclicSize;
        this.hash.normalize(j);
        normalize(this.chain, j);
        this.lzPos -= j;
      }
      j = this.cyclicPos + 1;
      this.cyclicPos = j;
      if (j == this.cyclicSize) {
        this.cyclicPos = 0;
      }
    }
    return i;
  }
  
  public Matches getMatches()
  {
    this.matches.count = 0;
    int j = this.matchLenMax;
    int n = this.niceLen;
    int i = movePos();
    int k = j;
    int m = n;
    if (i < j)
    {
      if (i == 0) {
        return this.matches;
      }
      j = i;
      k = j;
      m = n;
      if (n > i)
      {
        m = i;
        k = j;
      }
    }
    this.hash.calcHashes(this.buf, this.readPos);
    int i3 = this.lzPos - this.hash.getHash2Pos();
    int i2 = this.lzPos - this.hash.getHash3Pos();
    int i1 = this.hash.getHash4Pos();
    this.hash.updateTables(this.lzPos);
    this.chain[this.cyclicPos] = i1;
    i = 0;
    j = i;
    if (i3 < this.cyclicSize)
    {
      j = i;
      if (this.buf[(this.readPos - i3)] == this.buf[this.readPos])
      {
        j = 2;
        this.matches.len[0] = 2;
        this.matches.dist[0] = (i3 - 1);
        this.matches.count = 1;
      }
    }
    n = i3;
    i = j;
    Object localObject;
    if (i3 != i2)
    {
      n = i3;
      i = j;
      if (i2 < this.cyclicSize)
      {
        n = i3;
        i = j;
        if (this.buf[(this.readPos - i2)] == this.buf[this.readPos])
        {
          i = 3;
          localObject = this.matches.dist;
          Matches localMatches = this.matches;
          j = localMatches.count;
          localMatches.count = (j + 1);
          localObject[j] = (i2 - 1);
          n = i2;
        }
      }
    }
    j = i;
    if (this.matches.count > 0)
    {
      while ((i < k) && (this.buf[(this.readPos + i - n)] == this.buf[(this.readPos + i)])) {
        i += 1;
      }
      this.matches.len[(this.matches.count - 1)] = i;
      j = i;
      if (i >= m) {
        return this.matches;
      }
    }
    i = j;
    if (j < 3) {
      i = 3;
    }
    n = this.depthLimit;
    j = i;
    i = n;
    n = i1;
    for (;;)
    {
      i3 = this.lzPos - n;
      if ((i == 0) || (i3 >= this.cyclicSize)) {
        return this.matches;
      }
      localObject = this.chain;
      i1 = this.cyclicPos;
      if (i3 > this.cyclicPos) {}
      for (n = this.cyclicSize;; n = 0)
      {
        i2 = localObject[(n + (i1 - i3))];
        i1 = j;
        if (this.buf[(this.readPos + j - i3)] != this.buf[(this.readPos + j)]) {
          break;
        }
        i1 = j;
        if (this.buf[(this.readPos - i3)] != this.buf[this.readPos]) {
          break;
        }
        i1 = 0;
        do
        {
          n = i1 + 1;
          if (n >= k) {
            break;
          }
          i1 = n;
        } while (this.buf[(this.readPos + n - i3)] == this.buf[(this.readPos + n)]);
        i1 = j;
        if (n <= j) {
          break;
        }
        i1 = n;
        this.matches.len[this.matches.count] = n;
        this.matches.dist[this.matches.count] = (i3 - 1);
        localObject = this.matches;
        ((Matches)localObject).count += 1;
        if (n < m) {
          break;
        }
        return this.matches;
      }
      i -= 1;
      n = i2;
      j = i1;
    }
  }
  
  public void skip(int paramInt)
  {
    int i = paramInt;
    if (!$assertionsDisabled)
    {
      i = paramInt;
      if (paramInt < 0) {
        throw new AssertionError();
      }
    }
    for (;;)
    {
      paramInt = i - 1;
      if (i > 0)
      {
        i = paramInt;
        if (movePos() != 0)
        {
          this.hash.calcHashes(this.buf, this.readPos);
          this.chain[this.cyclicPos] = this.hash.getHash4Pos();
          this.hash.updateTables(this.lzPos);
          i = paramInt;
        }
      }
      else
      {
        return;
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/lz/HC4.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */