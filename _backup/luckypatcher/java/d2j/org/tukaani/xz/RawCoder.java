package org.tukaani.xz;

class RawCoder
{
  static void validate(FilterCoder[] paramArrayOfFilterCoder)
    throws UnsupportedOptionsException
  {
    int i = 0;
    while (i < paramArrayOfFilterCoder.length - 1)
    {
      if (!paramArrayOfFilterCoder[i].nonLastOK()) {
        throw new UnsupportedOptionsException("Unsupported XZ filter chain");
      }
      i += 1;
    }
    if (!paramArrayOfFilterCoder[(paramArrayOfFilterCoder.length - 1)].lastOK()) {
      throw new UnsupportedOptionsException("Unsupported XZ filter chain");
    }
    int j = 0;
    i = 0;
    while (i < paramArrayOfFilterCoder.length)
    {
      int k = j;
      if (paramArrayOfFilterCoder[i].changesSize()) {
        k = j + 1;
      }
      i += 1;
      j = k;
    }
    if (j > 3) {
      throw new UnsupportedOptionsException("Unsupported XZ filter chain");
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/RawCoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */