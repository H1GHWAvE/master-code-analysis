package org.tukaani.xz;

import java.io.IOException;

public class XZIOException
  extends IOException
{
  private static final long serialVersionUID = 3L;
  
  public XZIOException() {}
  
  public XZIOException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/XZIOException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */