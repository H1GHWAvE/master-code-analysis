package org.tukaani.xz;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class XZInputStream
  extends InputStream
{
  private boolean endReached = false;
  private IOException exception = null;
  private InputStream in;
  private final int memoryLimit;
  private final byte[] tempBuf = new byte[1];
  private SingleXZInputStream xzIn;
  
  public XZInputStream(InputStream paramInputStream)
    throws IOException
  {
    this(paramInputStream, -1);
  }
  
  public XZInputStream(InputStream paramInputStream, int paramInt)
    throws IOException
  {
    this.in = paramInputStream;
    this.memoryLimit = paramInt;
    this.xzIn = new SingleXZInputStream(paramInputStream, paramInt);
  }
  
  private void prepareNextStream()
    throws IOException
  {
    DataInputStream localDataInputStream = new DataInputStream(this.in);
    byte[] arrayOfByte = new byte[12];
    do
    {
      if (localDataInputStream.read(arrayOfByte, 0, 1) == -1)
      {
        this.endReached = true;
        return;
      }
      localDataInputStream.readFully(arrayOfByte, 1, 3);
    } while ((arrayOfByte[0] == 0) && (arrayOfByte[1] == 0) && (arrayOfByte[2] == 0) && (arrayOfByte[3] == 0));
    localDataInputStream.readFully(arrayOfByte, 4, 8);
    try
    {
      this.xzIn = new SingleXZInputStream(this.in, this.memoryLimit, arrayOfByte);
      return;
    }
    catch (XZFormatException localXZFormatException)
    {
      throw new CorruptedInputException("Garbage after a valid XZ Stream");
    }
  }
  
  public int available()
    throws IOException
  {
    if (this.in == null) {
      throw new XZIOException("Stream closed");
    }
    if (this.exception != null) {
      throw this.exception;
    }
    if (this.xzIn == null) {
      return 0;
    }
    return this.xzIn.available();
  }
  
  public void close()
    throws IOException
  {
    if (this.in != null) {}
    try
    {
      this.in.close();
      return;
    }
    finally
    {
      this.in = null;
    }
  }
  
  public int read()
    throws IOException
  {
    if (read(this.tempBuf, 0, 1) == -1) {
      return -1;
    }
    return this.tempBuf[0] & 0xFF;
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length)) {
      throw new IndexOutOfBoundsException();
    }
    int j;
    if (paramInt2 == 0) {
      j = 0;
    }
    label87:
    do
    {
      for (;;)
      {
        return j;
        if (this.in == null) {
          throw new XZIOException("Stream closed");
        }
        if (this.exception != null) {
          throw this.exception;
        }
        if (this.endReached) {
          return -1;
        }
        j = 0;
        int i = paramInt1;
        paramInt1 = j;
        j = paramInt1;
        if (paramInt2 > 0) {
          try
          {
            if (this.xzIn == null)
            {
              prepareNextStream();
              if (this.endReached)
              {
                j = paramInt1;
                if (paramInt1 != 0) {
                  continue;
                }
                return -1;
              }
            }
            j = this.xzIn.read(paramArrayOfByte, i, paramInt2);
            if (j > 0)
            {
              paramInt1 += j;
              i += j;
              paramInt2 -= j;
              break label87;
            }
            if (j != -1) {
              break label87;
            }
            this.xzIn = null;
          }
          catch (IOException paramArrayOfByte)
          {
            this.exception = paramArrayOfByte;
            j = paramInt1;
          }
        }
      }
    } while (paramInt1 != 0);
    throw paramArrayOfByte;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/XZInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */