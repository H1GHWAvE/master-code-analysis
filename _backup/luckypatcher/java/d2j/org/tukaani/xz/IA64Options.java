package org.tukaani.xz;

import java.io.InputStream;
import org.tukaani.xz.simple.IA64;

public class IA64Options
  extends BCJOptions
{
  private static final int ALIGNMENT = 16;
  
  public IA64Options()
  {
    super(16);
  }
  
  FilterEncoder getFilterEncoder()
  {
    return new BCJEncoder(this, 6L);
  }
  
  public InputStream getInputStream(InputStream paramInputStream)
  {
    return new SimpleInputStream(paramInputStream, new IA64(false, this.startOffset));
  }
  
  public FinishableOutputStream getOutputStream(FinishableOutputStream paramFinishableOutputStream)
  {
    return new SimpleOutputStream(paramFinishableOutputStream, new IA64(true, this.startOffset));
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/IA64Options.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */