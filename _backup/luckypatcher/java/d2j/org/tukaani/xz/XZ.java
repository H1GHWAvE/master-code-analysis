package org.tukaani.xz;

public class XZ
{
  public static final int CHECK_CRC32 = 1;
  public static final int CHECK_CRC64 = 4;
  public static final int CHECK_NONE = 0;
  public static final int CHECK_SHA256 = 10;
  public static final byte[] FOOTER_MAGIC = { 89, 90 };
  public static final byte[] HEADER_MAGIC = { -3, 55, 122, 88, 90, 0 };
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/XZ.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */