package org.tukaani.xz.check;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA256
  extends Check
{
  private final MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
  
  public SHA256()
    throws NoSuchAlgorithmException
  {
    this.size = 32;
    this.name = "SHA-256";
  }
  
  public byte[] finish()
  {
    byte[] arrayOfByte = this.sha256.digest();
    this.sha256.reset();
    return arrayOfByte;
  }
  
  public void update(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    this.sha256.update(paramArrayOfByte, paramInt1, paramInt2);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/check/SHA256.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */