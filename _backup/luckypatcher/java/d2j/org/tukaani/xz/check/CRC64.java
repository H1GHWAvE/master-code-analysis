package org.tukaani.xz.check;

public class CRC64
  extends Check
{
  private static final long[] crcTable = new long['Ā'];
  private static final long poly = -3932672073523589310L;
  private long crc = -1L;
  
  static
  {
    int i = 0;
    while (i < crcTable.length)
    {
      long l = i;
      int j = 0;
      if (j < 8)
      {
        if ((l & 1L) == 1L) {}
        for (l = l >>> 1 ^ 0xC96C5795D7870F42;; l >>>= 1)
        {
          j += 1;
          break;
        }
      }
      crcTable[i] = l;
      i += 1;
    }
  }
  
  public CRC64()
  {
    this.size = 8;
    this.name = "CRC64";
  }
  
  public byte[] finish()
  {
    long l = this.crc;
    this.crc = -1L;
    byte[] arrayOfByte = new byte[8];
    int i = 0;
    while (i < arrayOfByte.length)
    {
      arrayOfByte[i] = ((byte)(int)((l ^ 0xFFFFFFFFFFFFFFFF) >> i * 8));
      i += 1;
    }
    return arrayOfByte;
  }
  
  public void update(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = paramInt1;
    while (i < paramInt1 + paramInt2)
    {
      this.crc = (crcTable[((paramArrayOfByte[i] ^ (int)this.crc) & 0xFF)] ^ this.crc >>> 8);
      i += 1;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/check/CRC64.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */