package org.tukaani.xz.check;

import java.security.NoSuchAlgorithmException;
import org.tukaani.xz.UnsupportedOptionsException;

public abstract class Check
{
  String name;
  int size;
  
  public static Check getInstance(int paramInt)
    throws UnsupportedOptionsException
  {
    switch (paramInt)
    {
    }
    for (;;)
    {
      throw new UnsupportedOptionsException("Unsupported Check ID " + paramInt);
      return new None();
      return new CRC32();
      return new CRC64();
      try
      {
        SHA256 localSHA256 = new SHA256();
        return localSHA256;
      }
      catch (NoSuchAlgorithmException localNoSuchAlgorithmException) {}
    }
  }
  
  public abstract byte[] finish();
  
  public String getName()
  {
    return this.name;
  }
  
  public int getSize()
  {
    return this.size;
  }
  
  public void update(byte[] paramArrayOfByte)
  {
    update(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public abstract void update(byte[] paramArrayOfByte, int paramInt1, int paramInt2);
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/check/Check.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */