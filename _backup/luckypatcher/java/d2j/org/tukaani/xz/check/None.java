package org.tukaani.xz.check;

public class None
  extends Check
{
  public None()
  {
    this.size = 0;
    this.name = "None";
  }
  
  public byte[] finish()
  {
    return new byte[0];
  }
  
  public void update(byte[] paramArrayOfByte, int paramInt1, int paramInt2) {}
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/check/None.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */