package org.tukaani.xz.check;

public class CRC32
  extends Check
{
  private final java.util.zip.CRC32 state = new java.util.zip.CRC32();
  
  public CRC32()
  {
    this.size = 4;
    this.name = "CRC32";
  }
  
  public byte[] finish()
  {
    long l = this.state.getValue();
    int i = (byte)(int)l;
    int j = (byte)(int)(l >>> 8);
    int k = (byte)(int)(l >>> 16);
    int m = (byte)(int)(l >>> 24);
    this.state.reset();
    return new byte[] { i, j, k, m };
  }
  
  public void update(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    this.state.update(paramArrayOfByte, paramInt1, paramInt2);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/check/CRC32.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */