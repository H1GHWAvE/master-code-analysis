package org.tukaani.xz;

abstract class LZMA2Coder
  implements FilterCoder
{
  public static final long FILTER_ID = 33L;
  
  public boolean changesSize()
  {
    return true;
  }
  
  public boolean lastOK()
  {
    return true;
  }
  
  public boolean nonLastOK()
  {
    return false;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/LZMA2Coder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */