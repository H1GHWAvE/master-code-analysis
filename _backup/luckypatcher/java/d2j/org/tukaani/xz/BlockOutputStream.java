package org.tukaani.xz;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.tukaani.xz.check.Check;
import org.tukaani.xz.common.EncoderUtil;

class BlockOutputStream
  extends FinishableOutputStream
{
  private final Check check;
  private final long compressedSizeLimit;
  private FinishableOutputStream filterChain;
  private final int headerSize;
  private final OutputStream out;
  private final CountingOutputStream outCounted;
  private final byte[] tempBuf = new byte[1];
  private long uncompressedSize = 0L;
  
  public BlockOutputStream(OutputStream paramOutputStream, FilterEncoder[] paramArrayOfFilterEncoder, Check paramCheck)
    throws IOException
  {
    this.out = paramOutputStream;
    this.check = paramCheck;
    this.outCounted = new CountingOutputStream(paramOutputStream);
    this.filterChain = this.outCounted;
    int i = paramArrayOfFilterEncoder.length - 1;
    while (i >= 0)
    {
      this.filterChain = paramArrayOfFilterEncoder[i].getOutputStream(this.filterChain);
      i -= 1;
    }
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    localByteArrayOutputStream.write(0);
    localByteArrayOutputStream.write(paramArrayOfFilterEncoder.length - 1);
    i = 0;
    while (i < paramArrayOfFilterEncoder.length)
    {
      EncoderUtil.encodeVLI(localByteArrayOutputStream, paramArrayOfFilterEncoder[i].getFilterID());
      byte[] arrayOfByte = paramArrayOfFilterEncoder[i].getFilterProps();
      EncoderUtil.encodeVLI(localByteArrayOutputStream, arrayOfByte.length);
      localByteArrayOutputStream.write(arrayOfByte);
      i += 1;
    }
    while ((localByteArrayOutputStream.size() & 0x3) != 0) {
      localByteArrayOutputStream.write(0);
    }
    paramArrayOfFilterEncoder = localByteArrayOutputStream.toByteArray();
    this.headerSize = (paramArrayOfFilterEncoder.length + 4);
    if (this.headerSize > 1024) {
      throw new UnsupportedOptionsException();
    }
    paramArrayOfFilterEncoder[0] = ((byte)(paramArrayOfFilterEncoder.length / 4));
    paramOutputStream.write(paramArrayOfFilterEncoder);
    EncoderUtil.writeCRC32(paramOutputStream, paramArrayOfFilterEncoder);
    this.compressedSizeLimit = (9223372036854775804L - this.headerSize - paramCheck.getSize());
  }
  
  private void validate()
    throws IOException
  {
    long l = this.outCounted.getSize();
    if ((l < 0L) || (l > this.compressedSizeLimit) || (this.uncompressedSize < 0L)) {
      throw new XZIOException("XZ Stream has grown too big");
    }
  }
  
  public void finish()
    throws IOException
  {
    this.filterChain.finish();
    validate();
    for (long l = this.outCounted.getSize(); (0x3 & l) != 0L; l += 1L) {
      this.out.write(0);
    }
    this.out.write(this.check.finish());
  }
  
  public void flush()
    throws IOException
  {
    this.filterChain.flush();
    validate();
  }
  
  public long getUncompressedSize()
  {
    return this.uncompressedSize;
  }
  
  public long getUnpaddedSize()
  {
    return this.headerSize + this.outCounted.getSize() + this.check.getSize();
  }
  
  public void write(int paramInt)
    throws IOException
  {
    this.tempBuf[0] = ((byte)paramInt);
    write(this.tempBuf, 0, 1);
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    this.filterChain.write(paramArrayOfByte, paramInt1, paramInt2);
    this.check.update(paramArrayOfByte, paramInt1, paramInt2);
    this.uncompressedSize += paramInt2;
    validate();
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/BlockOutputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */