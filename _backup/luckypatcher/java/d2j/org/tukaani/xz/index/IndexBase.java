package org.tukaani.xz.index;

import org.tukaani.xz.XZIOException;
import org.tukaani.xz.common.Util;

abstract class IndexBase
{
  long blocksSum = 0L;
  long indexListSize = 0L;
  private final XZIOException invalidIndexException;
  long recordCount = 0L;
  long uncompressedSum = 0L;
  
  IndexBase(XZIOException paramXZIOException)
  {
    this.invalidIndexException = paramXZIOException;
  }
  
  private long getUnpaddedIndexSize()
  {
    return Util.getVLISize(this.recordCount) + 1 + this.indexListSize + 4L;
  }
  
  void add(long paramLong1, long paramLong2)
    throws XZIOException
  {
    this.blocksSum += (3L + paramLong1 & 0xFFFFFFFFFFFFFFFC);
    this.uncompressedSum += paramLong2;
    this.indexListSize += Util.getVLISize(paramLong1) + Util.getVLISize(paramLong2);
    this.recordCount += 1L;
    if ((this.blocksSum < 0L) || (this.uncompressedSum < 0L) || (getIndexSize() > 17179869184L) || (getStreamSize() < 0L)) {
      throw this.invalidIndexException;
    }
  }
  
  int getIndexPaddingSize()
  {
    return (int)(4L - getUnpaddedIndexSize() & 0x3);
  }
  
  public long getIndexSize()
  {
    return getUnpaddedIndexSize() + 3L & 0xFFFFFFFFFFFFFFFC;
  }
  
  public long getStreamSize()
  {
    return this.blocksSum + 12L + getIndexSize() + 12L;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/index/IndexBase.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */