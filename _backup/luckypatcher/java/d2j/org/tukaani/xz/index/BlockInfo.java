package org.tukaani.xz.index;

import org.tukaani.xz.common.StreamFlags;

public class BlockInfo
{
  public int blockNumber = -1;
  public long compressedOffset = -1L;
  IndexDecoder index;
  public long uncompressedOffset = -1L;
  public long uncompressedSize = -1L;
  public long unpaddedSize = -1L;
  
  public BlockInfo(IndexDecoder paramIndexDecoder)
  {
    this.index = paramIndexDecoder;
  }
  
  public int getCheckType()
  {
    return this.index.getStreamFlags().checkType;
  }
  
  public boolean hasNext()
  {
    return this.index.hasRecord(this.blockNumber + 1);
  }
  
  public void setNext()
  {
    this.index.setBlockInfo(this, this.blockNumber + 1);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/index/BlockInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */