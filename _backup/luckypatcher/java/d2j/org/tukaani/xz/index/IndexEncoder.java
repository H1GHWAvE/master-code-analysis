package org.tukaani.xz.index;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import org.tukaani.xz.XZIOException;
import org.tukaani.xz.common.EncoderUtil;

public class IndexEncoder
  extends IndexBase
{
  private final ArrayList records = new ArrayList();
  
  public IndexEncoder()
  {
    super(new XZIOException("XZ Stream or its Index has grown too big"));
  }
  
  public void add(long paramLong1, long paramLong2)
    throws XZIOException
  {
    super.add(paramLong1, paramLong2);
    this.records.add(new IndexRecord(paramLong1, paramLong2));
  }
  
  public void encode(OutputStream paramOutputStream)
    throws IOException
  {
    CRC32 localCRC32 = new CRC32();
    CheckedOutputStream localCheckedOutputStream = new CheckedOutputStream(paramOutputStream, localCRC32);
    localCheckedOutputStream.write(0);
    EncoderUtil.encodeVLI(localCheckedOutputStream, this.recordCount);
    Iterator localIterator = this.records.iterator();
    while (localIterator.hasNext())
    {
      IndexRecord localIndexRecord = (IndexRecord)localIterator.next();
      EncoderUtil.encodeVLI(localCheckedOutputStream, localIndexRecord.unpadded);
      EncoderUtil.encodeVLI(localCheckedOutputStream, localIndexRecord.uncompressed);
    }
    int i = getIndexPaddingSize();
    while (i > 0)
    {
      localCheckedOutputStream.write(0);
      i -= 1;
    }
    long l = localCRC32.getValue();
    i = 0;
    while (i < 4)
    {
      paramOutputStream.write((byte)(int)(l >>> i * 8));
      i += 1;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/index/IndexEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */