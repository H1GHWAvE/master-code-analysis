package org.tukaani.xz.index;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import org.tukaani.xz.CorruptedInputException;
import org.tukaani.xz.MemoryLimitException;
import org.tukaani.xz.SeekableInputStream;
import org.tukaani.xz.UnsupportedOptionsException;
import org.tukaani.xz.common.DecoderUtil;
import org.tukaani.xz.common.StreamFlags;

public class IndexDecoder
  extends IndexBase
{
  private long compressedOffset = 0L;
  private long largestBlockSize = 0L;
  private final int memoryUsage;
  private int recordOffset = 0;
  private final StreamFlags streamFlags;
  private final long streamPadding;
  private final long[] uncompressed;
  private long uncompressedOffset = 0L;
  private final long[] unpadded;
  
  static
  {
    if (!IndexDecoder.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  public IndexDecoder(SeekableInputStream paramSeekableInputStream, StreamFlags paramStreamFlags, long paramLong, int paramInt)
    throws IOException
  {
    super(new CorruptedInputException("XZ Index is corrupt"));
    this.streamFlags = paramStreamFlags;
    this.streamPadding = paramLong;
    paramLong = paramSeekableInputStream.position() + paramStreamFlags.backwardSize - 4L;
    CRC32 localCRC32 = new CRC32();
    CheckedInputStream localCheckedInputStream = new CheckedInputStream(paramSeekableInputStream, localCRC32);
    if (localCheckedInputStream.read() != 0) {
      throw new CorruptedInputException("XZ Index is corrupt");
    }
    long l1;
    try
    {
      l1 = DecoderUtil.decodeVLI(localCheckedInputStream);
      if (l1 >= paramStreamFlags.backwardSize / 2L) {
        throw new CorruptedInputException("XZ Index is corrupt");
      }
    }
    catch (EOFException paramSeekableInputStream)
    {
      throw new CorruptedInputException("XZ Index is corrupt");
    }
    if (l1 > 2147483647L) {
      throw new UnsupportedOptionsException("XZ Index has over 2147483647 Records");
    }
    this.memoryUsage = ((int)((16L * l1 + 1023L) / 1024L) + 1);
    if ((paramInt >= 0) && (this.memoryUsage > paramInt)) {
      throw new MemoryLimitException(this.memoryUsage, paramInt);
    }
    this.unpadded = new long[(int)l1];
    this.uncompressed = new long[(int)l1];
    int i = 0;
    paramInt = (int)l1;
    while (paramInt > 0)
    {
      l1 = DecoderUtil.decodeVLI(localCheckedInputStream);
      long l2 = DecoderUtil.decodeVLI(localCheckedInputStream);
      if (paramSeekableInputStream.position() > paramLong) {
        throw new CorruptedInputException("XZ Index is corrupt");
      }
      this.unpadded[i] = (this.blocksSum + l1);
      this.uncompressed[i] = (this.uncompressedSum + l2);
      i += 1;
      super.add(l1, l2);
      assert (i == this.recordCount);
      if (this.largestBlockSize < l2) {
        this.largestBlockSize = l2;
      }
      paramInt -= 1;
    }
    i = getIndexPaddingSize();
    paramInt = i;
    if (paramSeekableInputStream.position() + i != paramLong) {
      throw new CorruptedInputException("XZ Index is corrupt");
    }
    do
    {
      i = paramInt - 1;
      if (paramInt <= 0) {
        break;
      }
      paramInt = i;
    } while (localCheckedInputStream.read() == 0);
    throw new CorruptedInputException("XZ Index is corrupt");
    paramLong = localCRC32.getValue();
    paramInt = 0;
    while (paramInt < 4)
    {
      if ((paramLong >>> paramInt * 8 & 0xFF) != paramSeekableInputStream.read()) {
        throw new CorruptedInputException("XZ Index is corrupt");
      }
      paramInt += 1;
    }
  }
  
  public long getLargestBlockSize()
  {
    return this.largestBlockSize;
  }
  
  public int getMemoryUsage()
  {
    return this.memoryUsage;
  }
  
  public int getRecordCount()
  {
    return (int)this.recordCount;
  }
  
  public StreamFlags getStreamFlags()
  {
    return this.streamFlags;
  }
  
  public long getUncompressedSize()
  {
    return this.uncompressedSum;
  }
  
  public boolean hasRecord(int paramInt)
  {
    return (paramInt >= this.recordOffset) && (paramInt < this.recordOffset + this.recordCount);
  }
  
  public boolean hasUncompressedOffset(long paramLong)
  {
    return (paramLong >= this.uncompressedOffset) && (paramLong < this.uncompressedOffset + this.uncompressedSum);
  }
  
  public void locateBlock(BlockInfo paramBlockInfo, long paramLong)
  {
    assert (paramLong >= this.uncompressedOffset);
    paramLong -= this.uncompressedOffset;
    assert (paramLong < this.uncompressedSum);
    int j = 0;
    int i = this.unpadded.length - 1;
    while (j < i)
    {
      int k = j + (i - j) / 2;
      if (this.uncompressed[k] <= paramLong) {
        j = k + 1;
      } else {
        i = k;
      }
    }
    setBlockInfo(paramBlockInfo, this.recordOffset + j);
  }
  
  public void setBlockInfo(BlockInfo paramBlockInfo, int paramInt)
  {
    assert (paramInt >= this.recordOffset);
    assert (paramInt - this.recordOffset < this.recordCount);
    paramBlockInfo.index = this;
    paramBlockInfo.blockNumber = paramInt;
    paramInt -= this.recordOffset;
    if (paramInt == 0) {
      paramBlockInfo.compressedOffset = 0L;
    }
    for (paramBlockInfo.uncompressedOffset = 0L;; paramBlockInfo.uncompressedOffset = this.uncompressed[(paramInt - 1)])
    {
      paramBlockInfo.unpaddedSize = (this.unpadded[paramInt] - paramBlockInfo.compressedOffset);
      paramBlockInfo.uncompressedSize = (this.uncompressed[paramInt] - paramBlockInfo.uncompressedOffset);
      paramBlockInfo.compressedOffset += this.compressedOffset + 12L;
      paramBlockInfo.uncompressedOffset += this.uncompressedOffset;
      return;
      paramBlockInfo.compressedOffset = (this.unpadded[(paramInt - 1)] + 3L & 0xFFFFFFFFFFFFFFFC);
    }
  }
  
  public void setOffsets(IndexDecoder paramIndexDecoder)
  {
    paramIndexDecoder.recordOffset += (int)paramIndexDecoder.recordCount;
    this.compressedOffset = (paramIndexDecoder.compressedOffset + paramIndexDecoder.getStreamSize() + paramIndexDecoder.streamPadding);
    assert ((this.compressedOffset & 0x3) == 0L);
    paramIndexDecoder.uncompressedOffset += paramIndexDecoder.uncompressedSum;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/index/IndexDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */