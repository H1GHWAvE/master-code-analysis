package org.tukaani.xz.index;

class IndexRecord
{
  final long uncompressed;
  final long unpadded;
  
  IndexRecord(long paramLong1, long paramLong2)
  {
    this.unpadded = paramLong1;
    this.uncompressed = paramLong2;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/index/IndexRecord.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */