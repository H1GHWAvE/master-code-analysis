package org.tukaani.xz.index;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.zip.CheckedInputStream;
import org.tukaani.xz.CorruptedInputException;
import org.tukaani.xz.XZIOException;
import org.tukaani.xz.check.Check;
import org.tukaani.xz.check.SHA256;
import org.tukaani.xz.common.DecoderUtil;

public class IndexHash
  extends IndexBase
{
  private Check hash;
  
  public IndexHash()
  {
    super(new CorruptedInputException());
    try
    {
      this.hash = new SHA256();
      return;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      this.hash = new org.tukaani.xz.check.CRC32();
    }
  }
  
  public void add(long paramLong1, long paramLong2)
    throws XZIOException
  {
    super.add(paramLong1, paramLong2);
    ByteBuffer localByteBuffer = ByteBuffer.allocate(16);
    localByteBuffer.putLong(paramLong1);
    localByteBuffer.putLong(paramLong2);
    this.hash.update(localByteBuffer.array());
  }
  
  public void validate(InputStream paramInputStream)
    throws IOException
  {
    java.util.zip.CRC32 localCRC32 = new java.util.zip.CRC32();
    localCRC32.update(0);
    paramInputStream = new CheckedInputStream(paramInputStream, localCRC32);
    if (DecoderUtil.decodeVLI(paramInputStream) != this.recordCount) {
      throw new CorruptedInputException("XZ Index is corrupt");
    }
    IndexHash localIndexHash = new IndexHash();
    for (long l1 = 0L; l1 < this.recordCount; l1 += 1L)
    {
      long l2 = DecoderUtil.decodeVLI(paramInputStream);
      long l3 = DecoderUtil.decodeVLI(paramInputStream);
      try
      {
        localIndexHash.add(l2, l3);
        if ((localIndexHash.blocksSum > this.blocksSum) || (localIndexHash.uncompressedSum > this.uncompressedSum) || (localIndexHash.indexListSize > this.indexListSize)) {
          throw new CorruptedInputException("XZ Index is corrupt");
        }
      }
      catch (XZIOException paramInputStream)
      {
        throw new CorruptedInputException("XZ Index is corrupt");
      }
    }
    if ((localIndexHash.blocksSum != this.blocksSum) || (localIndexHash.uncompressedSum != this.uncompressedSum) || (localIndexHash.indexListSize != this.indexListSize) || (!Arrays.equals(localIndexHash.hash.finish(), this.hash.finish()))) {
      throw new CorruptedInputException("XZ Index is corrupt");
    }
    paramInputStream = new DataInputStream(paramInputStream);
    int i = getIndexPaddingSize();
    while (i > 0)
    {
      if (paramInputStream.readUnsignedByte() != 0) {
        throw new CorruptedInputException("XZ Index is corrupt");
      }
      i -= 1;
    }
    l1 = localCRC32.getValue();
    i = 0;
    while (i < 4)
    {
      if ((l1 >>> i * 8 & 0xFF) != paramInputStream.readUnsignedByte()) {
        throw new CorruptedInputException("XZ Index is corrupt");
      }
      i += 1;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/index/IndexHash.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */