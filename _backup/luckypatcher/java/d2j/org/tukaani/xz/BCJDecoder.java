package org.tukaani.xz;

import java.io.InputStream;
import org.tukaani.xz.simple.ARM;
import org.tukaani.xz.simple.ARMThumb;
import org.tukaani.xz.simple.IA64;
import org.tukaani.xz.simple.PowerPC;
import org.tukaani.xz.simple.SPARC;
import org.tukaani.xz.simple.SimpleFilter;
import org.tukaani.xz.simple.X86;

class BCJDecoder
  extends BCJCoder
  implements FilterDecoder
{
  private final long filterID;
  private final int startOffset;
  
  static
  {
    if (!BCJDecoder.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  BCJDecoder(long paramLong, byte[] paramArrayOfByte)
    throws UnsupportedOptionsException
  {
    assert (isBCJFilterID(paramLong));
    this.filterID = paramLong;
    if (paramArrayOfByte.length == 0)
    {
      this.startOffset = 0;
      return;
    }
    if (paramArrayOfByte.length == 4)
    {
      int j = 0;
      int i = 0;
      while (i < 4)
      {
        j |= (paramArrayOfByte[i] & 0xFF) << i * 8;
        i += 1;
      }
      this.startOffset = j;
      return;
    }
    throw new UnsupportedOptionsException("Unsupported BCJ filter properties");
  }
  
  public InputStream getInputStream(InputStream paramInputStream)
  {
    Object localObject = null;
    if (this.filterID == 4L) {
      localObject = new X86(false, this.startOffset);
    }
    do
    {
      for (;;)
      {
        return new SimpleInputStream(paramInputStream, (SimpleFilter)localObject);
        if (this.filterID == 5L)
        {
          localObject = new PowerPC(false, this.startOffset);
        }
        else if (this.filterID == 6L)
        {
          localObject = new IA64(false, this.startOffset);
        }
        else if (this.filterID == 7L)
        {
          localObject = new ARM(false, this.startOffset);
        }
        else if (this.filterID == 8L)
        {
          localObject = new ARMThumb(false, this.startOffset);
        }
        else
        {
          if (this.filterID != 9L) {
            break;
          }
          localObject = new SPARC(false, this.startOffset);
        }
      }
    } while ($assertionsDisabled);
    throw new AssertionError();
  }
  
  public int getMemoryUsage()
  {
    return SimpleInputStream.getMemoryUsage();
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/BCJDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */