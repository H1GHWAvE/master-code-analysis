package org.tukaani.xz;

import java.io.IOException;
import java.io.OutputStream;
import org.tukaani.xz.check.Check;
import org.tukaani.xz.common.EncoderUtil;
import org.tukaani.xz.common.StreamFlags;
import org.tukaani.xz.index.IndexEncoder;

public class XZOutputStream
  extends FinishableOutputStream
{
  private BlockOutputStream blockEncoder = null;
  private final Check check;
  private IOException exception = null;
  private FilterEncoder[] filters;
  private boolean filtersSupportFlushing;
  private boolean finished = false;
  private final IndexEncoder index = new IndexEncoder();
  private OutputStream out;
  private final StreamFlags streamFlags = new StreamFlags();
  private final byte[] tempBuf = new byte[1];
  
  public XZOutputStream(OutputStream paramOutputStream, FilterOptions paramFilterOptions)
    throws IOException
  {
    this(paramOutputStream, paramFilterOptions, 4);
  }
  
  public XZOutputStream(OutputStream paramOutputStream, FilterOptions paramFilterOptions, int paramInt)
    throws IOException
  {
    this(paramOutputStream, new FilterOptions[] { paramFilterOptions }, paramInt);
  }
  
  public XZOutputStream(OutputStream paramOutputStream, FilterOptions[] paramArrayOfFilterOptions)
    throws IOException
  {
    this(paramOutputStream, paramArrayOfFilterOptions, 4);
  }
  
  public XZOutputStream(OutputStream paramOutputStream, FilterOptions[] paramArrayOfFilterOptions, int paramInt)
    throws IOException
  {
    this.out = paramOutputStream;
    updateFilters(paramArrayOfFilterOptions);
    this.streamFlags.checkType = paramInt;
    this.check = Check.getInstance(paramInt);
    encodeStreamHeader();
  }
  
  private void encodeStreamFlags(byte[] paramArrayOfByte, int paramInt)
  {
    paramArrayOfByte[paramInt] = 0;
    paramArrayOfByte[(paramInt + 1)] = ((byte)this.streamFlags.checkType);
  }
  
  private void encodeStreamFooter()
    throws IOException
  {
    byte[] arrayOfByte = new byte[6];
    long l = this.index.getIndexSize() / 4L;
    int i = 0;
    while (i < 4)
    {
      arrayOfByte[i] = ((byte)(int)(l - 1L >>> i * 8));
      i += 1;
    }
    encodeStreamFlags(arrayOfByte, 4);
    EncoderUtil.writeCRC32(this.out, arrayOfByte);
    this.out.write(arrayOfByte);
    this.out.write(XZ.FOOTER_MAGIC);
  }
  
  private void encodeStreamHeader()
    throws IOException
  {
    this.out.write(XZ.HEADER_MAGIC);
    byte[] arrayOfByte = new byte[2];
    encodeStreamFlags(arrayOfByte, 0);
    this.out.write(arrayOfByte);
    EncoderUtil.writeCRC32(this.out, arrayOfByte);
  }
  
  public void close()
    throws IOException
  {
    if (this.out != null) {}
    try
    {
      finish();
      try
      {
        this.out.close();
        this.out = null;
        if (this.exception != null) {
          throw this.exception;
        }
      }
      catch (IOException localIOException1)
      {
        for (;;)
        {
          if (this.exception == null) {
            this.exception = localIOException1;
          }
        }
      }
    }
    catch (IOException localIOException2)
    {
      for (;;) {}
    }
  }
  
  public void endBlock()
    throws IOException
  {
    if (this.exception != null) {
      throw this.exception;
    }
    if (this.finished) {
      throw new XZIOException("Stream finished or closed");
    }
    if (this.blockEncoder != null) {}
    try
    {
      this.blockEncoder.finish();
      this.index.add(this.blockEncoder.getUnpaddedSize(), this.blockEncoder.getUncompressedSize());
      this.blockEncoder = null;
      return;
    }
    catch (IOException localIOException)
    {
      this.exception = localIOException;
      throw localIOException;
    }
  }
  
  public void finish()
    throws IOException
  {
    if (!this.finished) {
      endBlock();
    }
    try
    {
      this.index.encode(this.out);
      encodeStreamFooter();
      this.finished = true;
      return;
    }
    catch (IOException localIOException)
    {
      this.exception = localIOException;
      throw localIOException;
    }
  }
  
  public void flush()
    throws IOException
  {
    if (this.exception != null) {
      throw this.exception;
    }
    if (this.finished) {
      throw new XZIOException("Stream finished or closed");
    }
    try
    {
      if (this.blockEncoder != null)
      {
        if (this.filtersSupportFlushing)
        {
          this.blockEncoder.flush();
          return;
        }
        endBlock();
        this.out.flush();
        return;
      }
    }
    catch (IOException localIOException)
    {
      this.exception = localIOException;
      throw localIOException;
    }
    this.out.flush();
  }
  
  public void updateFilters(FilterOptions paramFilterOptions)
    throws XZIOException
  {
    updateFilters(new FilterOptions[] { paramFilterOptions });
  }
  
  public void updateFilters(FilterOptions[] paramArrayOfFilterOptions)
    throws XZIOException
  {
    if (this.blockEncoder != null) {
      throw new UnsupportedOptionsException("Changing filter options in the middle of a XZ Block not implemented");
    }
    if ((paramArrayOfFilterOptions.length < 1) || (paramArrayOfFilterOptions.length > 4)) {
      throw new UnsupportedOptionsException("XZ filter chain must be 1-4 filters");
    }
    this.filtersSupportFlushing = true;
    FilterEncoder[] arrayOfFilterEncoder = new FilterEncoder[paramArrayOfFilterOptions.length];
    int i = 0;
    while (i < paramArrayOfFilterOptions.length)
    {
      arrayOfFilterEncoder[i] = paramArrayOfFilterOptions[i].getFilterEncoder();
      this.filtersSupportFlushing &= arrayOfFilterEncoder[i].supportsFlushing();
      i += 1;
    }
    RawCoder.validate(arrayOfFilterEncoder);
    this.filters = arrayOfFilterEncoder;
  }
  
  public void write(int paramInt)
    throws IOException
  {
    this.tempBuf[0] = ((byte)paramInt);
    write(this.tempBuf, 0, 1);
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length)) {
      throw new IndexOutOfBoundsException();
    }
    if (this.exception != null) {
      throw this.exception;
    }
    if (this.finished) {
      throw new XZIOException("Stream finished or closed");
    }
    try
    {
      if (this.blockEncoder == null) {
        this.blockEncoder = new BlockOutputStream(this.out, this.filters, this.check);
      }
      this.blockEncoder.write(paramArrayOfByte, paramInt1, paramInt2);
      return;
    }
    catch (IOException paramArrayOfByte)
    {
      this.exception = paramArrayOfByte;
      throw paramArrayOfByte;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/XZOutputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */