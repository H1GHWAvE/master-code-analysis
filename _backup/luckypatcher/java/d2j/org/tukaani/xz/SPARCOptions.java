package org.tukaani.xz;

import java.io.InputStream;
import org.tukaani.xz.simple.SPARC;

public class SPARCOptions
  extends BCJOptions
{
  private static final int ALIGNMENT = 4;
  
  public SPARCOptions()
  {
    super(4);
  }
  
  FilterEncoder getFilterEncoder()
  {
    return new BCJEncoder(this, 9L);
  }
  
  public InputStream getInputStream(InputStream paramInputStream)
  {
    return new SimpleInputStream(paramInputStream, new SPARC(false, this.startOffset));
  }
  
  public FinishableOutputStream getOutputStream(FinishableOutputStream paramFinishableOutputStream)
  {
    return new SimpleOutputStream(paramFinishableOutputStream, new SPARC(true, this.startOffset));
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/SPARCOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */