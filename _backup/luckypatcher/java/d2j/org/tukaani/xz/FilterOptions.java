package org.tukaani.xz;

import java.io.IOException;
import java.io.InputStream;

public abstract class FilterOptions
  implements Cloneable
{
  public static int getDecoderMemoryUsage(FilterOptions[] paramArrayOfFilterOptions)
  {
    int j = 0;
    int i = 0;
    while (i < paramArrayOfFilterOptions.length)
    {
      j += paramArrayOfFilterOptions[i].getDecoderMemoryUsage();
      i += 1;
    }
    return j;
  }
  
  public static int getEncoderMemoryUsage(FilterOptions[] paramArrayOfFilterOptions)
  {
    int j = 0;
    int i = 0;
    while (i < paramArrayOfFilterOptions.length)
    {
      j += paramArrayOfFilterOptions[i].getEncoderMemoryUsage();
      i += 1;
    }
    return j;
  }
  
  public abstract int getDecoderMemoryUsage();
  
  public abstract int getEncoderMemoryUsage();
  
  abstract FilterEncoder getFilterEncoder();
  
  public abstract InputStream getInputStream(InputStream paramInputStream)
    throws IOException;
  
  public abstract FinishableOutputStream getOutputStream(FinishableOutputStream paramFinishableOutputStream);
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/FilterOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */