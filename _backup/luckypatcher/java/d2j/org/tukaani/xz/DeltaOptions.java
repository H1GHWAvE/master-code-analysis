package org.tukaani.xz;

import java.io.InputStream;

public class DeltaOptions
  extends FilterOptions
{
  public static final int DISTANCE_MAX = 256;
  public static final int DISTANCE_MIN = 1;
  private int distance = 1;
  
  static
  {
    if (!DeltaOptions.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  public DeltaOptions() {}
  
  public DeltaOptions(int paramInt)
    throws UnsupportedOptionsException
  {
    setDistance(paramInt);
  }
  
  public Object clone()
  {
    try
    {
      Object localObject = super.clone();
      return localObject;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      if (!$assertionsDisabled) {
        throw new AssertionError();
      }
      throw new RuntimeException();
    }
  }
  
  public int getDecoderMemoryUsage()
  {
    return 1;
  }
  
  public int getDistance()
  {
    return this.distance;
  }
  
  public int getEncoderMemoryUsage()
  {
    return DeltaOutputStream.getMemoryUsage();
  }
  
  FilterEncoder getFilterEncoder()
  {
    return new DeltaEncoder(this);
  }
  
  public InputStream getInputStream(InputStream paramInputStream)
  {
    return new DeltaInputStream(paramInputStream, this.distance);
  }
  
  public FinishableOutputStream getOutputStream(FinishableOutputStream paramFinishableOutputStream)
  {
    return new DeltaOutputStream(paramFinishableOutputStream, this);
  }
  
  public void setDistance(int paramInt)
    throws UnsupportedOptionsException
  {
    if ((paramInt < 1) || (paramInt > 256)) {
      throw new UnsupportedOptionsException("Delta distance must be in the range [1, 256]: " + paramInt);
    }
    this.distance = paramInt;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/DeltaOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */