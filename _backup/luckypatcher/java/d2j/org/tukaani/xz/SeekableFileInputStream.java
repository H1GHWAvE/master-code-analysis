package org.tukaani.xz;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class SeekableFileInputStream
  extends SeekableInputStream
{
  protected RandomAccessFile randomAccessFile;
  
  public SeekableFileInputStream(File paramFile)
    throws FileNotFoundException
  {
    this.randomAccessFile = new RandomAccessFile(paramFile, "r");
  }
  
  public SeekableFileInputStream(RandomAccessFile paramRandomAccessFile)
  {
    this.randomAccessFile = paramRandomAccessFile;
  }
  
  public SeekableFileInputStream(String paramString)
    throws FileNotFoundException
  {
    this.randomAccessFile = new RandomAccessFile(paramString, "r");
  }
  
  public void close()
    throws IOException
  {
    this.randomAccessFile.close();
  }
  
  public long length()
    throws IOException
  {
    return this.randomAccessFile.length();
  }
  
  public long position()
    throws IOException
  {
    return this.randomAccessFile.getFilePointer();
  }
  
  public int read()
    throws IOException
  {
    return this.randomAccessFile.read();
  }
  
  public int read(byte[] paramArrayOfByte)
    throws IOException
  {
    return this.randomAccessFile.read(paramArrayOfByte);
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    return this.randomAccessFile.read(paramArrayOfByte, paramInt1, paramInt2);
  }
  
  public void seek(long paramLong)
    throws IOException
  {
    this.randomAccessFile.seek(paramLong);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/SeekableFileInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */