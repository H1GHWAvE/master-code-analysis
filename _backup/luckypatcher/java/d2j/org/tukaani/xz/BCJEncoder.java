package org.tukaani.xz;

class BCJEncoder
  extends BCJCoder
  implements FilterEncoder
{
  private final long filterID;
  private final BCJOptions options;
  private final byte[] props;
  
  static
  {
    if (!BCJEncoder.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  BCJEncoder(BCJOptions paramBCJOptions, long paramLong)
  {
    assert (isBCJFilterID(paramLong));
    int j = paramBCJOptions.getStartOffset();
    if (j == 0) {
      this.props = new byte[0];
    }
    for (;;)
    {
      this.filterID = paramLong;
      this.options = ((BCJOptions)paramBCJOptions.clone());
      return;
      this.props = new byte[4];
      int i = 0;
      while (i < 4)
      {
        this.props[i] = ((byte)(j >>> i * 8));
        i += 1;
      }
    }
  }
  
  public long getFilterID()
  {
    return this.filterID;
  }
  
  public byte[] getFilterProps()
  {
    return this.props;
  }
  
  public FinishableOutputStream getOutputStream(FinishableOutputStream paramFinishableOutputStream)
  {
    return this.options.getOutputStream(paramFinishableOutputStream);
  }
  
  public boolean supportsFlushing()
  {
    return false;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/BCJEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */