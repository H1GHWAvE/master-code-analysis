package org.tukaani.xz.simple;

public final class PowerPC
  implements SimpleFilter
{
  private final boolean isEncoder;
  private int pos;
  
  public PowerPC(boolean paramBoolean, int paramInt)
  {
    this.isEncoder = paramBoolean;
    this.pos = paramInt;
  }
  
  public int code(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = paramInt1;
    if (i <= paramInt1 + paramInt2 - 4)
    {
      int j;
      if (((paramArrayOfByte[i] & 0xFC) == 72) && ((paramArrayOfByte[(i + 3)] & 0x3) == 1))
      {
        j = (paramArrayOfByte[i] & 0x3) << 24 | (paramArrayOfByte[(i + 1)] & 0xFF) << 16 | (paramArrayOfByte[(i + 2)] & 0xFF) << 8 | paramArrayOfByte[(i + 3)] & 0xFC;
        if (!this.isEncoder) {
          break label175;
        }
        j += this.pos + i - paramInt1;
      }
      for (;;)
      {
        paramArrayOfByte[i] = ((byte)(j >>> 24 & 0x3 | 0x48));
        paramArrayOfByte[(i + 1)] = ((byte)(j >>> 16));
        paramArrayOfByte[(i + 2)] = ((byte)(j >>> 8));
        paramArrayOfByte[(i + 3)] = ((byte)(paramArrayOfByte[(i + 3)] & 0x3 | j));
        i += 4;
        break;
        label175:
        j -= this.pos + i - paramInt1;
      }
    }
    paramInt1 = i - paramInt1;
    this.pos += paramInt1;
    return paramInt1;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/simple/PowerPC.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */