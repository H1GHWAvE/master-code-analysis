package org.tukaani.xz.simple;

public final class ARMThumb
  implements SimpleFilter
{
  private final boolean isEncoder;
  private int pos;
  
  public ARMThumb(boolean paramBoolean, int paramInt)
  {
    this.isEncoder = paramBoolean;
    this.pos = (paramInt + 4);
  }
  
  public int code(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = paramInt1;
    if (i <= paramInt1 + paramInt2 - 4)
    {
      int j = i;
      if ((paramArrayOfByte[(i + 1)] & 0xF8) == 240)
      {
        j = i;
        if ((paramArrayOfByte[(i + 3)] & 0xF8) == 248)
        {
          j = ((paramArrayOfByte[(i + 1)] & 0x7) << 19 | (paramArrayOfByte[i] & 0xFF) << 11 | (paramArrayOfByte[(i + 3)] & 0x7) << 8 | paramArrayOfByte[(i + 2)] & 0xFF) << 1;
          if (!this.isEncoder) {
            break label204;
          }
          j += this.pos + i - paramInt1;
        }
      }
      for (;;)
      {
        j >>>= 1;
        paramArrayOfByte[(i + 1)] = ((byte)(j >>> 19 & 0x7 | 0xF0));
        paramArrayOfByte[i] = ((byte)(j >>> 11));
        paramArrayOfByte[(i + 3)] = ((byte)(j >>> 8 & 0x7 | 0xF8));
        paramArrayOfByte[(i + 2)] = ((byte)j);
        j = i + 2;
        i = j + 2;
        break;
        label204:
        j -= this.pos + i - paramInt1;
      }
    }
    paramInt1 = i - paramInt1;
    this.pos += paramInt1;
    return paramInt1;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/simple/ARMThumb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */