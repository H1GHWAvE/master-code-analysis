package org.tukaani.xz.simple;

public final class SPARC
  implements SimpleFilter
{
  private final boolean isEncoder;
  private int pos;
  
  public SPARC(boolean paramBoolean, int paramInt)
  {
    this.isEncoder = paramBoolean;
    this.pos = paramInt;
  }
  
  public int code(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = paramInt1;
    if (i <= paramInt1 + paramInt2 - 4)
    {
      int j;
      if (((paramArrayOfByte[i] == 64) && ((paramArrayOfByte[(i + 1)] & 0xC0) == 0)) || ((paramArrayOfByte[i] == Byte.MAX_VALUE) && ((paramArrayOfByte[(i + 1)] & 0xC0) == 192)))
      {
        j = ((paramArrayOfByte[i] & 0xFF) << 24 | (paramArrayOfByte[(i + 1)] & 0xFF) << 16 | (paramArrayOfByte[(i + 2)] & 0xFF) << 8 | paramArrayOfByte[(i + 3)] & 0xFF) << 2;
        if (!this.isEncoder) {
          break label219;
        }
        j += this.pos + i - paramInt1;
      }
      for (;;)
      {
        j >>>= 2;
        j = 0 - (j >>> 22 & 0x1) << 22 & 0x3FFFFFFF | 0x3FFFFF & j | 0x40000000;
        paramArrayOfByte[i] = ((byte)(j >>> 24));
        paramArrayOfByte[(i + 1)] = ((byte)(j >>> 16));
        paramArrayOfByte[(i + 2)] = ((byte)(j >>> 8));
        paramArrayOfByte[(i + 3)] = ((byte)j);
        i += 4;
        break;
        label219:
        j -= this.pos + i - paramInt1;
      }
    }
    paramInt1 = i - paramInt1;
    this.pos += paramInt1;
    return paramInt1;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/simple/SPARC.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */