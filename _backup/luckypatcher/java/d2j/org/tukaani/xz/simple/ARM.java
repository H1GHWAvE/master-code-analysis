package org.tukaani.xz.simple;

public final class ARM
  implements SimpleFilter
{
  private final boolean isEncoder;
  private int pos;
  
  public ARM(boolean paramBoolean, int paramInt)
  {
    this.isEncoder = paramBoolean;
    this.pos = (paramInt + 8);
  }
  
  public int code(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = paramInt1;
    if (i <= paramInt1 + paramInt2 - 4)
    {
      int j;
      if ((paramArrayOfByte[(i + 3)] & 0xFF) == 235)
      {
        j = ((paramArrayOfByte[(i + 2)] & 0xFF) << 16 | (paramArrayOfByte[(i + 1)] & 0xFF) << 8 | paramArrayOfByte[i] & 0xFF) << 2;
        if (!this.isEncoder) {
          break label136;
        }
        j += this.pos + i - paramInt1;
      }
      for (;;)
      {
        j >>>= 2;
        paramArrayOfByte[(i + 2)] = ((byte)(j >>> 16));
        paramArrayOfByte[(i + 1)] = ((byte)(j >>> 8));
        paramArrayOfByte[i] = ((byte)j);
        i += 4;
        break;
        label136:
        j -= this.pos + i - paramInt1;
      }
    }
    paramInt1 = i - paramInt1;
    this.pos += paramInt1;
    return paramInt1;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/simple/ARM.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */