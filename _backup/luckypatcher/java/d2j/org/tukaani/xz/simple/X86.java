package org.tukaani.xz.simple;

public final class X86
  implements SimpleFilter
{
  private static final boolean[] MASK_TO_ALLOWED_STATUS = { 1, 1, 1, 0, 1, 0, 0, 0 };
  private static final int[] MASK_TO_BIT_NUMBER = { 0, 1, 2, 2, 3, 3, 3, 3 };
  private final boolean isEncoder;
  private int pos;
  private int prevMask = 0;
  
  public X86(boolean paramBoolean, int paramInt)
  {
    this.isEncoder = paramBoolean;
    this.pos = (paramInt + 5);
  }
  
  private static boolean test86MSByte(byte paramByte)
  {
    paramByte &= 0xFF;
    return (paramByte == 0) || (paramByte == 255);
  }
  
  public int code(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int m = 0;
    int j = paramInt1 - 1;
    int i = paramInt1;
    if (i <= paramInt1 + paramInt2 - 5)
    {
      if ((paramArrayOfByte[i] & 0xFE) != 232) {}
      for (;;)
      {
        i += 1;
        break;
        j = i - j;
        label64:
        int k;
        if ((j & 0xFFFFFFFC) != 0)
        {
          this.prevMask = 0;
          j = i;
          if (test86MSByte(paramArrayOfByte[(i + 4)])) {
            k = paramArrayOfByte[(i + 1)] & 0xFF | (paramArrayOfByte[(i + 2)] & 0xFF) << 8 | (paramArrayOfByte[(i + 3)] & 0xFF) << 16 | (paramArrayOfByte[(i + 4)] & 0xFF) << 24;
          }
        }
        else
        {
          for (;;)
          {
            if (this.isEncoder)
            {
              k += this.pos + i - paramInt1;
              label155:
              if (this.prevMask != 0) {
                break label313;
              }
            }
            label313:
            int n;
            do
            {
              paramArrayOfByte[(i + 1)] = ((byte)k);
              paramArrayOfByte[(i + 2)] = ((byte)(k >>> 8));
              paramArrayOfByte[(i + 3)] = ((byte)(k >>> 16));
              paramArrayOfByte[(i + 4)] = ((byte)((k >>> 24 & 0x1) - 1 ^ 0xFFFFFFFF));
              i += 4;
              break;
              this.prevMask = (this.prevMask << j - 1 & 0x7);
              if ((this.prevMask == 0) || ((MASK_TO_ALLOWED_STATUS[this.prevMask] != 0) && (!test86MSByte(paramArrayOfByte[(i + 4 - MASK_TO_BIT_NUMBER[this.prevMask])])))) {
                break label64;
              }
              j = i;
              this.prevMask = (this.prevMask << 1 | 0x1);
              break;
              k -= this.pos + i - paramInt1;
              break label155;
              n = MASK_TO_BIT_NUMBER[this.prevMask] * 8;
            } while (!test86MSByte((byte)(k >>> 24 - n)));
            k ^= (1 << 32 - n) - 1;
          }
        }
        this.prevMask = (this.prevMask << 1 | 0x1);
      }
    }
    paramInt2 = i - j;
    if ((paramInt2 & 0xFFFFFFFC) != 0) {}
    for (paramInt2 = m;; paramInt2 = this.prevMask << paramInt2 - 1)
    {
      this.prevMask = paramInt2;
      paramInt1 = i - paramInt1;
      this.pos += paramInt1;
      return paramInt1;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/simple/X86.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */