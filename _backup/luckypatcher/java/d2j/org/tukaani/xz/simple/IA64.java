package org.tukaani.xz.simple;

public final class IA64
  implements SimpleFilter
{
  private static final int[] BRANCH_TABLE = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 6, 6, 0, 0, 7, 7, 4, 4, 0, 0, 4, 4, 0, 0 };
  private final boolean isEncoder;
  private int pos;
  
  public IA64(boolean paramBoolean, int paramInt)
  {
    this.isEncoder = paramBoolean;
    this.pos = paramInt;
  }
  
  public int code(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = paramInt1;
    while (i <= paramInt1 + paramInt2 - 16)
    {
      int j = paramArrayOfByte[i];
      int n = BRANCH_TABLE[(j & 0x1F)];
      int k = 0;
      j = 5;
      if (k < 3)
      {
        if ((n >>> k & 0x1) == 0) {}
        int i1;
        int i2;
        long l1;
        long l2;
        do
        {
          k += 1;
          j += 41;
          break;
          i1 = j >>> 3;
          i2 = j & 0x7;
          l1 = 0L;
          m = 0;
          while (m < 6)
          {
            l1 |= (paramArrayOfByte[(i + i1 + m)] & 0xFF) << m * 8;
            m += 1;
          }
          l2 = l1 >>> i2;
        } while (((l2 >>> 37 & 0xF) != 5L) || ((l2 >>> 9 & 0x7) != 0L));
        int m = ((int)(l2 >>> 13 & 0xFFFFF) | ((int)(l2 >>> 36) & 0x1) << 20) << 4;
        if (this.isEncoder) {
          m += this.pos + i - paramInt1;
        }
        for (;;)
        {
          m >>>= 4;
          long l3 = m;
          long l4 = m;
          long l5 = (1 << i2) - 1;
          m = 0;
          while (m < 6)
          {
            paramArrayOfByte[(i + i1 + m)] = ((byte)(int)((l1 & l5 | (l2 & 0xFFFFFFEE00001FFF | (l3 & 0xFFFFF) << 13 | (l4 & 0x100000) << 16) << i2) >>> m * 8));
            m += 1;
          }
          break;
          m -= this.pos + i - paramInt1;
        }
      }
      i += 16;
    }
    paramInt1 = i - paramInt1;
    this.pos += paramInt1;
    return paramInt1;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/simple/IA64.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */