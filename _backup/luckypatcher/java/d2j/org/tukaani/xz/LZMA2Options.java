package org.tukaani.xz;

import java.io.IOException;
import java.io.InputStream;

public class LZMA2Options
  extends FilterOptions
{
  public static final int DICT_SIZE_DEFAULT = 8388608;
  public static final int DICT_SIZE_MAX = 805306368;
  public static final int DICT_SIZE_MIN = 4096;
  public static final int LC_DEFAULT = 3;
  public static final int LC_LP_MAX = 4;
  public static final int LP_DEFAULT = 0;
  public static final int MF_BT4 = 20;
  public static final int MF_HC4 = 4;
  public static final int MODE_FAST = 1;
  public static final int MODE_NORMAL = 2;
  public static final int MODE_UNCOMPRESSED = 0;
  public static final int NICE_LEN_MAX = 273;
  public static final int NICE_LEN_MIN = 8;
  public static final int PB_DEFAULT = 2;
  public static final int PB_MAX = 4;
  public static final int PRESET_DEFAULT = 6;
  public static final int PRESET_MAX = 9;
  public static final int PRESET_MIN = 0;
  private static final int[] presetToDepthLimit;
  private static final int[] presetToDictSize;
  private int depthLimit;
  private int dictSize;
  private int lc;
  private int lp;
  private int mf;
  private int mode;
  private int niceLen;
  private int pb;
  private byte[] presetDict = null;
  
  static
  {
    if (!LZMA2Options.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      presetToDictSize = new int[] { 262144, 1048576, 2097152, 4194304, 4194304, 8388608, 8388608, 16777216, 33554432, 67108864 };
      presetToDepthLimit = new int[] { 4, 8, 24, 48 };
      return;
    }
  }
  
  public LZMA2Options()
  {
    try
    {
      setPreset(6);
      return;
    }
    catch (UnsupportedOptionsException localUnsupportedOptionsException)
    {
      if (!$assertionsDisabled) {
        throw new AssertionError();
      }
      throw new RuntimeException();
    }
  }
  
  public LZMA2Options(int paramInt)
    throws UnsupportedOptionsException
  {
    setPreset(paramInt);
  }
  
  public LZMA2Options(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
    throws UnsupportedOptionsException
  {
    setDictSize(paramInt1);
    setLcLp(paramInt2, paramInt3);
    setPb(paramInt4);
    setMode(paramInt5);
    setNiceLen(paramInt6);
    setMatchFinder(paramInt7);
    setDepthLimit(paramInt8);
  }
  
  public Object clone()
  {
    try
    {
      Object localObject = super.clone();
      return localObject;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      if (!$assertionsDisabled) {
        throw new AssertionError();
      }
      throw new RuntimeException();
    }
  }
  
  public int getDecoderMemoryUsage()
  {
    int i = this.dictSize - 1;
    i |= i >>> 2;
    i |= i >>> 3;
    i |= i >>> 4;
    i |= i >>> 8;
    return LZMA2InputStream.getMemoryUsage((i | i >>> 16) + 1);
  }
  
  public int getDepthLimit()
  {
    return this.depthLimit;
  }
  
  public int getDictSize()
  {
    return this.dictSize;
  }
  
  public int getEncoderMemoryUsage()
  {
    if (this.mode == 0) {
      return UncompressedLZMA2OutputStream.getMemoryUsage();
    }
    return LZMA2OutputStream.getMemoryUsage(this);
  }
  
  FilterEncoder getFilterEncoder()
  {
    return new LZMA2Encoder(this);
  }
  
  public InputStream getInputStream(InputStream paramInputStream)
    throws IOException
  {
    return new LZMA2InputStream(paramInputStream, this.dictSize);
  }
  
  public int getLc()
  {
    return this.lc;
  }
  
  public int getLp()
  {
    return this.lp;
  }
  
  public int getMatchFinder()
  {
    return this.mf;
  }
  
  public int getMode()
  {
    return this.mode;
  }
  
  public int getNiceLen()
  {
    return this.niceLen;
  }
  
  public FinishableOutputStream getOutputStream(FinishableOutputStream paramFinishableOutputStream)
  {
    if (this.mode == 0) {
      return new UncompressedLZMA2OutputStream(paramFinishableOutputStream);
    }
    return new LZMA2OutputStream(paramFinishableOutputStream, this);
  }
  
  public int getPb()
  {
    return this.pb;
  }
  
  public byte[] getPresetDict()
  {
    return this.presetDict;
  }
  
  public void setDepthLimit(int paramInt)
    throws UnsupportedOptionsException
  {
    if (paramInt < 0) {
      throw new UnsupportedOptionsException("Depth limit cannot be negative: " + paramInt);
    }
    this.depthLimit = paramInt;
  }
  
  public void setDictSize(int paramInt)
    throws UnsupportedOptionsException
  {
    if (paramInt < 4096) {
      throw new UnsupportedOptionsException("LZMA2 dictionary size must be at least 4 KiB: " + paramInt + " B");
    }
    if (paramInt > 805306368) {
      throw new UnsupportedOptionsException("LZMA2 dictionary size must not exceed 768 MiB: " + paramInt + " B");
    }
    this.dictSize = paramInt;
  }
  
  public void setLc(int paramInt)
    throws UnsupportedOptionsException
  {
    setLcLp(paramInt, this.lp);
  }
  
  public void setLcLp(int paramInt1, int paramInt2)
    throws UnsupportedOptionsException
  {
    if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 > 4) || (paramInt2 > 4) || (paramInt1 + paramInt2 > 4)) {
      throw new UnsupportedOptionsException("lc + lp must not exceed 4: " + paramInt1 + " + " + paramInt2);
    }
    this.lc = paramInt1;
    this.lp = paramInt2;
  }
  
  public void setLp(int paramInt)
    throws UnsupportedOptionsException
  {
    setLcLp(this.lc, paramInt);
  }
  
  public void setMatchFinder(int paramInt)
    throws UnsupportedOptionsException
  {
    if ((paramInt != 4) && (paramInt != 20)) {
      throw new UnsupportedOptionsException("Unsupported match finder: " + paramInt);
    }
    this.mf = paramInt;
  }
  
  public void setMode(int paramInt)
    throws UnsupportedOptionsException
  {
    if ((paramInt < 0) || (paramInt > 2)) {
      throw new UnsupportedOptionsException("Unsupported compression mode: " + paramInt);
    }
    this.mode = paramInt;
  }
  
  public void setNiceLen(int paramInt)
    throws UnsupportedOptionsException
  {
    if (paramInt < 8) {
      throw new UnsupportedOptionsException("Minimum nice length of matches is 8 bytes: " + paramInt);
    }
    if (paramInt > 273) {
      throw new UnsupportedOptionsException("Maximum nice length of matches is 273: " + paramInt);
    }
    this.niceLen = paramInt;
  }
  
  public void setPb(int paramInt)
    throws UnsupportedOptionsException
  {
    if ((paramInt < 0) || (paramInt > 4)) {
      throw new UnsupportedOptionsException("pb must not exceed 4: " + paramInt);
    }
    this.pb = paramInt;
  }
  
  public void setPreset(int paramInt)
    throws UnsupportedOptionsException
  {
    if ((paramInt < 0) || (paramInt > 9)) {
      throw new UnsupportedOptionsException("Unsupported preset: " + paramInt);
    }
    this.lc = 3;
    this.lp = 0;
    this.pb = 2;
    this.dictSize = presetToDictSize[paramInt];
    if (paramInt <= 3)
    {
      this.mode = 1;
      this.mf = 4;
      if (paramInt <= 1) {}
      for (int i = 128;; i = 273)
      {
        this.niceLen = i;
        this.depthLimit = presetToDepthLimit[paramInt];
        return;
      }
    }
    this.mode = 2;
    this.mf = 20;
    if (paramInt == 4) {
      paramInt = 16;
    }
    for (;;)
    {
      this.niceLen = paramInt;
      this.depthLimit = 0;
      return;
      if (paramInt == 5) {
        paramInt = 32;
      } else {
        paramInt = 64;
      }
    }
  }
  
  public void setPresetDict(byte[] paramArrayOfByte)
  {
    this.presetDict = paramArrayOfByte;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/LZMA2Options.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */