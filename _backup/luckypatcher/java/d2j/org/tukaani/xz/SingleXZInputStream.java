package org.tukaani.xz;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.tukaani.xz.check.Check;
import org.tukaani.xz.common.DecoderUtil;
import org.tukaani.xz.common.StreamFlags;
import org.tukaani.xz.index.IndexHash;

public class SingleXZInputStream
  extends InputStream
{
  private BlockInputStream blockDecoder = null;
  private Check check;
  private boolean endReached = false;
  private IOException exception = null;
  private InputStream in;
  private final IndexHash indexHash = new IndexHash();
  private int memoryLimit;
  private StreamFlags streamHeaderFlags;
  private final byte[] tempBuf = new byte[1];
  
  public SingleXZInputStream(InputStream paramInputStream)
    throws IOException
  {
    initialize(paramInputStream, -1);
  }
  
  public SingleXZInputStream(InputStream paramInputStream, int paramInt)
    throws IOException
  {
    initialize(paramInputStream, paramInt);
  }
  
  SingleXZInputStream(InputStream paramInputStream, int paramInt, byte[] paramArrayOfByte)
    throws IOException
  {
    initialize(paramInputStream, paramInt, paramArrayOfByte);
  }
  
  private void initialize(InputStream paramInputStream, int paramInt)
    throws IOException
  {
    byte[] arrayOfByte = new byte[12];
    new DataInputStream(paramInputStream).readFully(arrayOfByte);
    initialize(paramInputStream, paramInt, arrayOfByte);
  }
  
  private void initialize(InputStream paramInputStream, int paramInt, byte[] paramArrayOfByte)
    throws IOException
  {
    this.in = paramInputStream;
    this.memoryLimit = paramInt;
    this.streamHeaderFlags = DecoderUtil.decodeStreamHeader(paramArrayOfByte);
    this.check = Check.getInstance(this.streamHeaderFlags.checkType);
  }
  
  private void validateStreamFooter()
    throws IOException
  {
    Object localObject = new byte[12];
    new DataInputStream(this.in).readFully((byte[])localObject);
    localObject = DecoderUtil.decodeStreamFooter((byte[])localObject);
    if ((!DecoderUtil.areStreamFlagsEqual(this.streamHeaderFlags, (StreamFlags)localObject)) || (this.indexHash.getIndexSize() != ((StreamFlags)localObject).backwardSize)) {
      throw new CorruptedInputException("XZ Stream Footer does not match Stream Header");
    }
  }
  
  public int available()
    throws IOException
  {
    if (this.in == null) {
      throw new XZIOException("Stream closed");
    }
    if (this.exception != null) {
      throw this.exception;
    }
    if (this.blockDecoder == null) {
      return 0;
    }
    return this.blockDecoder.available();
  }
  
  public void close()
    throws IOException
  {
    if (this.in != null) {}
    try
    {
      this.in.close();
      return;
    }
    finally
    {
      this.in = null;
    }
  }
  
  public String getCheckName()
  {
    return this.check.getName();
  }
  
  public int getCheckType()
  {
    return this.streamHeaderFlags.checkType;
  }
  
  public int read()
    throws IOException
  {
    if (read(this.tempBuf, 0, 1) == -1) {
      return -1;
    }
    return this.tempBuf[0] & 0xFF;
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length)) {
      throw new IndexOutOfBoundsException();
    }
    int j;
    if (paramInt2 == 0) {
      j = 0;
    }
    label202:
    do
    {
      return j;
      if (this.in == null) {
        throw new XZIOException("Stream closed");
      }
      if (this.exception != null) {
        throw this.exception;
      }
      if (this.endReached) {
        return -1;
      }
      j = 0;
      int i = paramInt1;
      paramInt1 = j;
      for (;;)
      {
        j = paramInt1;
        if (paramInt2 <= 0) {
          break;
        }
        try
        {
          BlockInputStream localBlockInputStream = this.blockDecoder;
          if (localBlockInputStream == null) {}
          try
          {
            this.blockDecoder = new BlockInputStream(this.in, this.check, this.memoryLimit, -1L, -1L);
            j = this.blockDecoder.read(paramArrayOfByte, i, paramInt2);
            if (j <= 0) {
              break label202;
            }
            paramInt1 += j;
            i += j;
            paramInt2 -= j;
          }
          catch (IndexIndicatorException paramArrayOfByte)
          {
            this.indexHash.validate(this.in);
            validateStreamFooter();
            this.endReached = true;
            j = paramInt1;
          }
          if (paramInt1 > 0) {
            break;
          }
          return -1;
        }
        catch (IOException paramArrayOfByte)
        {
          this.exception = paramArrayOfByte;
          j = paramInt1;
        }
        if (j == -1)
        {
          this.indexHash.add(this.blockDecoder.getUnpaddedSize(), this.blockDecoder.getUncompressedSize());
          this.blockDecoder = null;
        }
      }
    } while (paramInt1 != 0);
    throw paramArrayOfByte;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/SingleXZInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */