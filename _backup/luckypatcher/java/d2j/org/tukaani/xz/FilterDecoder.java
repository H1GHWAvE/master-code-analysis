package org.tukaani.xz;

import java.io.InputStream;

abstract interface FilterDecoder
  extends FilterCoder
{
  public abstract InputStream getInputStream(InputStream paramInputStream);
  
  public abstract int getMemoryUsage();
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/FilterDecoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */