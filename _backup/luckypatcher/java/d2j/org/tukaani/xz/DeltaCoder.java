package org.tukaani.xz;

abstract class DeltaCoder
  implements FilterCoder
{
  public static final long FILTER_ID = 3L;
  
  public boolean changesSize()
  {
    return false;
  }
  
  public boolean lastOK()
  {
    return false;
  }
  
  public boolean nonLastOK()
  {
    return true;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/DeltaCoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */