package org.tukaani.xz;

import java.io.IOException;
import org.tukaani.xz.simple.SimpleFilter;

class SimpleOutputStream
  extends FinishableOutputStream
{
  private static final int FILTER_BUF_SIZE = 4096;
  private IOException exception = null;
  private final byte[] filterBuf = new byte['က'];
  private boolean finished = false;
  private FinishableOutputStream out;
  private int pos = 0;
  private final SimpleFilter simpleFilter;
  private final byte[] tempBuf = new byte[1];
  private int unfiltered = 0;
  
  static
  {
    if (!SimpleOutputStream.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  SimpleOutputStream(FinishableOutputStream paramFinishableOutputStream, SimpleFilter paramSimpleFilter)
  {
    if (paramFinishableOutputStream == null) {
      throw new NullPointerException();
    }
    this.out = paramFinishableOutputStream;
    this.simpleFilter = paramSimpleFilter;
  }
  
  static int getMemoryUsage()
  {
    return 5;
  }
  
  private void writePending()
    throws IOException
  {
    assert (!this.finished);
    if (this.exception != null) {
      throw this.exception;
    }
    try
    {
      this.out.write(this.filterBuf, this.pos, this.unfiltered);
      this.finished = true;
      return;
    }
    catch (IOException localIOException)
    {
      this.exception = localIOException;
      throw localIOException;
    }
  }
  
  public void close()
    throws IOException
  {
    if ((this.out == null) || (!this.finished)) {}
    try
    {
      writePending();
      try
      {
        this.out.close();
        this.out = null;
        if (this.exception != null) {
          throw this.exception;
        }
      }
      catch (IOException localIOException1)
      {
        for (;;)
        {
          if (this.exception == null) {
            this.exception = localIOException1;
          }
        }
      }
    }
    catch (IOException localIOException2)
    {
      for (;;) {}
    }
  }
  
  public void finish()
    throws IOException
  {
    if (!this.finished) {
      writePending();
    }
    try
    {
      this.out.finish();
      return;
    }
    catch (IOException localIOException)
    {
      this.exception = localIOException;
      throw localIOException;
    }
  }
  
  public void flush()
    throws IOException
  {
    throw new UnsupportedOptionsException("Flushing is not supported");
  }
  
  public void write(int paramInt)
    throws IOException
  {
    this.tempBuf[0] = ((byte)paramInt);
    write(this.tempBuf, 0, 1);
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length)) {
      throw new IndexOutOfBoundsException();
    }
    if (this.exception != null) {
      throw this.exception;
    }
    if (this.finished) {
      throw new XZIOException("Stream finished or closed");
    }
    for (;;)
    {
      this.unfiltered -= paramInt1;
      try
      {
        this.out.write(this.filterBuf, this.pos, paramInt1);
        this.pos += paramInt1;
        int i;
        paramInt1 = i;
        int j;
        paramInt2 = j;
        if (this.pos + this.unfiltered == 4096)
        {
          System.arraycopy(this.filterBuf, this.pos, this.filterBuf, 0, this.unfiltered);
          this.pos = 0;
          paramInt2 = j;
          paramInt1 = i;
        }
        if (paramInt2 > 0)
        {
          int k = Math.min(paramInt2, 4096 - (this.pos + this.unfiltered));
          System.arraycopy(paramArrayOfByte, paramInt1, this.filterBuf, this.pos + this.unfiltered, k);
          i = paramInt1 + k;
          j = paramInt2 - k;
          this.unfiltered += k;
          paramInt1 = this.simpleFilter.code(this.filterBuf, this.pos, this.unfiltered);
          if ((!$assertionsDisabled) && (paramInt1 > this.unfiltered)) {
            throw new AssertionError();
          }
        }
      }
      catch (IOException paramArrayOfByte)
      {
        this.exception = paramArrayOfByte;
        throw paramArrayOfByte;
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/SimpleOutputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */