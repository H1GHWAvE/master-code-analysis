package org.tukaani.xz;

abstract class BCJOptions
  extends FilterOptions
{
  private final int alignment;
  int startOffset = 0;
  
  static
  {
    if (!BCJOptions.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  BCJOptions(int paramInt)
  {
    this.alignment = paramInt;
  }
  
  public Object clone()
  {
    try
    {
      Object localObject = super.clone();
      return localObject;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      if (!$assertionsDisabled) {
        throw new AssertionError();
      }
      throw new RuntimeException();
    }
  }
  
  public int getDecoderMemoryUsage()
  {
    return SimpleInputStream.getMemoryUsage();
  }
  
  public int getEncoderMemoryUsage()
  {
    return SimpleOutputStream.getMemoryUsage();
  }
  
  public int getStartOffset()
  {
    return this.startOffset;
  }
  
  public void setStartOffset(int paramInt)
    throws UnsupportedOptionsException
  {
    if ((this.alignment - 1 & paramInt) != 0) {
      throw new UnsupportedOptionsException("Start offset must be a multiple of " + this.alignment);
    }
    this.startOffset = paramInt;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/BCJOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */