package org.tukaani.xz;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

class CountingInputStream
  extends FilterInputStream
{
  private long size = 0L;
  
  public CountingInputStream(InputStream paramInputStream)
  {
    super(paramInputStream);
  }
  
  public long getSize()
  {
    return this.size;
  }
  
  public int read()
    throws IOException
  {
    int i = this.in.read();
    if ((i != -1) && (this.size >= 0L)) {
      this.size += 1L;
    }
    return i;
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    paramInt1 = this.in.read(paramArrayOfByte, paramInt1, paramInt2);
    if ((paramInt1 > 0) && (this.size >= 0L)) {
      this.size += paramInt1;
    }
    return paramInt1;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/CountingInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */