package org.tukaani.xz;

class DeltaEncoder
  extends DeltaCoder
  implements FilterEncoder
{
  private final DeltaOptions options;
  private final byte[] props = new byte[1];
  
  DeltaEncoder(DeltaOptions paramDeltaOptions)
  {
    this.props[0] = ((byte)(paramDeltaOptions.getDistance() - 1));
    this.options = ((DeltaOptions)paramDeltaOptions.clone());
  }
  
  public long getFilterID()
  {
    return 3L;
  }
  
  public byte[] getFilterProps()
  {
    return this.props;
  }
  
  public FinishableOutputStream getOutputStream(FinishableOutputStream paramFinishableOutputStream)
  {
    return this.options.getOutputStream(paramFinishableOutputStream);
  }
  
  public boolean supportsFlushing()
  {
    return true;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/DeltaEncoder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */