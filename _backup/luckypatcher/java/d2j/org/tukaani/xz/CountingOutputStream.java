package org.tukaani.xz;

import java.io.IOException;
import java.io.OutputStream;

class CountingOutputStream
  extends FinishableOutputStream
{
  private final OutputStream out;
  private long size = 0L;
  
  public CountingOutputStream(OutputStream paramOutputStream)
  {
    this.out = paramOutputStream;
  }
  
  public void close()
    throws IOException
  {
    this.out.close();
  }
  
  public void flush()
    throws IOException
  {
    this.out.flush();
  }
  
  public long getSize()
  {
    return this.size;
  }
  
  public void write(int paramInt)
    throws IOException
  {
    this.out.write(paramInt);
    if (this.size >= 0L) {
      this.size += 1L;
    }
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    this.out.write(paramArrayOfByte, paramInt1, paramInt2);
    if (this.size >= 0L) {
      this.size += paramInt2;
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/CountingOutputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */