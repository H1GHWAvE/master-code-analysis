package org.tukaani.xz;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import org.tukaani.xz.check.Check;
import org.tukaani.xz.common.DecoderUtil;

class BlockInputStream
  extends InputStream
{
  private final Check check;
  private long compressedSizeInHeader = -1L;
  private long compressedSizeLimit;
  private boolean endReached = false;
  private InputStream filterChain;
  private final int headerSize;
  private final CountingInputStream inCounted;
  private final DataInputStream inData;
  private final byte[] tempBuf = new byte[1];
  private long uncompressedSize = 0L;
  private long uncompressedSizeInHeader = -1L;
  
  public BlockInputStream(InputStream paramInputStream, Check paramCheck, int paramInt, long paramLong1, long paramLong2)
    throws IOException, IndexIndicatorException
  {
    this.check = paramCheck;
    this.inData = new DataInputStream(paramInputStream);
    byte[] arrayOfByte1 = new byte['Ѐ'];
    this.inData.readFully(arrayOfByte1, 0, 1);
    if (arrayOfByte1[0] == 0) {
      throw new IndexIndicatorException();
    }
    this.headerSize = (((arrayOfByte1[0] & 0xFF) + 1) * 4);
    this.inData.readFully(arrayOfByte1, 1, this.headerSize - 1);
    if (!DecoderUtil.isCRC32Valid(arrayOfByte1, 0, this.headerSize - 4, this.headerSize - 4)) {
      throw new CorruptedInputException("XZ Block Header is corrupt");
    }
    if ((arrayOfByte1[1] & 0x3C) != 0) {
      throw new UnsupportedOptionsException("Unsupported options in XZ Block Header");
    }
    int j = (arrayOfByte1[1] & 0x3) + 1;
    long[] arrayOfLong = new long[j];
    byte[][] arrayOfByte = new byte[j][];
    ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(arrayOfByte1, 2, this.headerSize - 6);
    try
    {
      this.compressedSizeLimit = (9223372036854775804L - this.headerSize - paramCheck.getSize());
      if ((arrayOfByte1[1] & 0x40) == 0) {
        break label296;
      }
      this.compressedSizeInHeader = DecoderUtil.decodeVLI(localByteArrayInputStream);
      if ((this.compressedSizeInHeader == 0L) || (this.compressedSizeInHeader > this.compressedSizeLimit)) {
        throw new CorruptedInputException();
      }
    }
    catch (IOException paramInputStream)
    {
      throw new CorruptedInputException("XZ Block Header is corrupt");
    }
    this.compressedSizeLimit = this.compressedSizeInHeader;
    label296:
    if ((arrayOfByte1[1] & 0x80) != 0) {
      this.uncompressedSizeInHeader = DecoderUtil.decodeVLI(localByteArrayInputStream);
    }
    for (;;)
    {
      int i;
      if (i < j)
      {
        arrayOfLong[i] = DecoderUtil.decodeVLI(localByteArrayInputStream);
        long l = DecoderUtil.decodeVLI(localByteArrayInputStream);
        if (l > localByteArrayInputStream.available()) {
          throw new CorruptedInputException();
        }
        arrayOfByte[i] = new byte[(int)l];
        localByteArrayInputStream.read(arrayOfByte[i]);
        i += 1;
      }
      else
      {
        i = localByteArrayInputStream.available();
        while (i > 0)
        {
          if (localByteArrayInputStream.read() != 0) {
            throw new UnsupportedOptionsException("Unsupported options in XZ Block Header");
          }
          i -= 1;
        }
        if (paramLong1 != -1L)
        {
          i = this.headerSize + paramCheck.getSize();
          if (i >= paramLong1) {
            throw new CorruptedInputException("XZ Index does not match a Block Header");
          }
          paramLong1 -= i;
          if ((paramLong1 > this.compressedSizeLimit) || ((this.compressedSizeInHeader != -1L) && (this.compressedSizeInHeader != paramLong1))) {
            throw new CorruptedInputException("XZ Index does not match a Block Header");
          }
          if ((this.uncompressedSizeInHeader != -1L) && (this.uncompressedSizeInHeader != paramLong2)) {
            throw new CorruptedInputException("XZ Index does not match a Block Header");
          }
          this.compressedSizeLimit = paramLong1;
          this.compressedSizeInHeader = paramLong1;
          this.uncompressedSizeInHeader = paramLong2;
        }
        paramCheck = new FilterDecoder[arrayOfLong.length];
        i = 0;
        if (i < paramCheck.length)
        {
          if (arrayOfLong[i] == 33L) {
            paramCheck[i] = new LZMA2Decoder(arrayOfByte[i]);
          }
          for (;;)
          {
            i += 1;
            break;
            if (arrayOfLong[i] == 3L)
            {
              paramCheck[i] = new DeltaDecoder(arrayOfByte[i]);
            }
            else
            {
              if (!BCJDecoder.isBCJFilterID(arrayOfLong[i])) {
                break label689;
              }
              paramCheck[i] = new BCJDecoder(arrayOfLong[i], arrayOfByte[i]);
            }
          }
          label689:
          throw new UnsupportedOptionsException("Unknown Filter ID " + arrayOfLong[i]);
        }
        RawCoder.validate(paramCheck);
        if (paramInt >= 0)
        {
          j = 0;
          i = 0;
          while (i < paramCheck.length)
          {
            j += paramCheck[i].getMemoryUsage();
            i += 1;
          }
          if (j > paramInt) {
            throw new MemoryLimitException(j, paramInt);
          }
        }
        this.inCounted = new CountingInputStream(paramInputStream);
        this.filterChain = this.inCounted;
        paramInt = paramCheck.length - 1;
        while (paramInt >= 0)
        {
          this.filterChain = paramCheck[paramInt].getInputStream(this.filterChain);
          paramInt -= 1;
        }
        return;
        i = 0;
      }
    }
  }
  
  private void validate()
    throws IOException
  {
    long l2 = this.inCounted.getSize();
    long l1;
    if ((this.compressedSizeInHeader == -1L) || (this.compressedSizeInHeader == l2))
    {
      l1 = l2;
      if (this.uncompressedSizeInHeader != -1L)
      {
        l1 = l2;
        if (this.uncompressedSizeInHeader == this.uncompressedSize) {}
      }
    }
    else
    {
      throw new CorruptedInputException();
    }
    do
    {
      l2 = l1 + 1L;
      if ((0x3 & l1) == 0L) {
        break;
      }
      l1 = l2;
    } while (this.inData.readUnsignedByte() == 0);
    throw new CorruptedInputException();
    byte[] arrayOfByte = new byte[this.check.getSize()];
    this.inData.readFully(arrayOfByte);
    if (!Arrays.equals(this.check.finish(), arrayOfByte)) {
      throw new CorruptedInputException("Integrity check (" + this.check.getName() + ") does not match");
    }
  }
  
  public int available()
    throws IOException
  {
    return this.filterChain.available();
  }
  
  public long getUncompressedSize()
  {
    return this.uncompressedSize;
  }
  
  public long getUnpaddedSize()
  {
    return this.headerSize + this.inCounted.getSize() + this.check.getSize();
  }
  
  public int read()
    throws IOException
  {
    if (read(this.tempBuf, 0, 1) == -1) {
      return -1;
    }
    return this.tempBuf[0] & 0xFF;
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (this.endReached) {
      paramInt1 = -1;
    }
    int i;
    label169:
    do
    {
      do
      {
        return paramInt1;
        i = this.filterChain.read(paramArrayOfByte, paramInt1, paramInt2);
        if (i <= 0) {
          break label169;
        }
        this.check.update(paramArrayOfByte, paramInt1, i);
        this.uncompressedSize += i;
        long l = this.inCounted.getSize();
        if ((l < 0L) || (l > this.compressedSizeLimit) || (this.uncompressedSize < 0L) || ((this.uncompressedSizeInHeader != -1L) && (this.uncompressedSize > this.uncompressedSizeInHeader))) {
          throw new CorruptedInputException();
        }
        if (i < paramInt2) {
          break;
        }
        paramInt1 = i;
      } while (this.uncompressedSize != this.uncompressedSizeInHeader);
      if (this.filterChain.read() != -1) {
        throw new CorruptedInputException();
      }
      validate();
      this.endReached = true;
      return i;
      paramInt1 = i;
    } while (i != -1);
    validate();
    this.endReached = true;
    return i;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/BlockInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */