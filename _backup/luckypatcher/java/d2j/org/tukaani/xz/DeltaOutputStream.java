package org.tukaani.xz;

import java.io.IOException;
import org.tukaani.xz.delta.DeltaEncoder;

class DeltaOutputStream
  extends FinishableOutputStream
{
  private static final int FILTER_BUF_SIZE = 4096;
  private final DeltaEncoder delta;
  private IOException exception = null;
  private final byte[] filterBuf = new byte['က'];
  private boolean finished = false;
  private FinishableOutputStream out;
  private final byte[] tempBuf = new byte[1];
  
  DeltaOutputStream(FinishableOutputStream paramFinishableOutputStream, DeltaOptions paramDeltaOptions)
  {
    this.out = paramFinishableOutputStream;
    this.delta = new DeltaEncoder(paramDeltaOptions.getDistance());
  }
  
  static int getMemoryUsage()
  {
    return 5;
  }
  
  public void close()
    throws IOException
  {
    if (this.out != null) {}
    try
    {
      this.out.close();
      this.out = null;
      if (this.exception != null) {
        throw this.exception;
      }
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        if (this.exception == null) {
          this.exception = localIOException;
        }
      }
    }
  }
  
  public void finish()
    throws IOException
  {
    if (!this.finished) {
      if (this.exception != null) {
        throw this.exception;
      }
    }
    try
    {
      this.out.finish();
      this.finished = true;
      return;
    }
    catch (IOException localIOException)
    {
      this.exception = localIOException;
      throw localIOException;
    }
  }
  
  public void flush()
    throws IOException
  {
    if (this.exception != null) {
      throw this.exception;
    }
    if (this.finished) {
      throw new XZIOException("Stream finished or closed");
    }
    try
    {
      this.out.flush();
      return;
    }
    catch (IOException localIOException)
    {
      this.exception = localIOException;
      throw localIOException;
    }
  }
  
  public void write(int paramInt)
    throws IOException
  {
    this.tempBuf[0] = ((byte)paramInt);
    write(this.tempBuf, 0, 1);
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length)) {
      throw new IndexOutOfBoundsException();
    }
    if (this.exception != null) {
      throw this.exception;
    }
    if (this.finished) {
      throw new XZIOException("Stream finished");
    }
    for (;;)
    {
      if (paramInt2 > 4096) {}
      try
      {
        this.delta.encode(paramArrayOfByte, paramInt1, 4096, this.filterBuf);
        this.out.write(this.filterBuf);
        paramInt1 += 4096;
        paramInt2 -= 4096;
      }
      catch (IOException paramArrayOfByte)
      {
        this.exception = paramArrayOfByte;
        throw paramArrayOfByte;
      }
    }
    this.delta.encode(paramArrayOfByte, paramInt1, paramInt2, this.filterBuf);
    this.out.write(this.filterBuf, 0, paramInt2);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/DeltaOutputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */