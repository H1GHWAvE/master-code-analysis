package org.tukaani.xz;

import java.io.InputStream;
import org.tukaani.xz.simple.ARMThumb;

public class ARMThumbOptions
  extends BCJOptions
{
  private static final int ALIGNMENT = 2;
  
  public ARMThumbOptions()
  {
    super(2);
  }
  
  FilterEncoder getFilterEncoder()
  {
    return new BCJEncoder(this, 8L);
  }
  
  public InputStream getInputStream(InputStream paramInputStream)
  {
    return new SimpleInputStream(paramInputStream, new ARMThumb(false, this.startOffset));
  }
  
  public FinishableOutputStream getOutputStream(FinishableOutputStream paramFinishableOutputStream)
  {
    return new SimpleOutputStream(paramFinishableOutputStream, new ARMThumb(true, this.startOffset));
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/ARMThumbOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */