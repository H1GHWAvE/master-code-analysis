package org.tukaani.xz;

public class XZFormatException
  extends XZIOException
{
  private static final long serialVersionUID = 3L;
  
  public XZFormatException()
  {
    super("Input is not in the XZ format");
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/org/tukaani/xz/XZFormatException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */