package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.PkgListItem;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.android.vending.billing.InAppBillingService.LUCK.patchActivity;
import com.chelpus.Utils;
import java.io.PrintStream;

public class Custom_Patch_Dialog
{
  Dialog dialog = null;
  
  public void dismiss()
  {
    if (this.dialog != null)
    {
      this.dialog.dismiss();
      this.dialog = null;
    }
  }
  
  public Dialog onCreateDialog()
  {
    System.out.println("Custom Dialog create.");
    if ((listAppsFragment.frag == null) || (listAppsFragment.frag.getContext() == null)) {
      dismiss();
    }
    LinearLayout localLinearLayout = (LinearLayout)View.inflate(listAppsFragment.frag.getContext(), 2130968612, null);
    try
    {
      localObject = (LinearLayout)localLinearLayout.findViewById(2131558539).findViewById(2131558540);
      if (listAppsFragment.str == null) {
        listAppsFragment.str = " ";
      }
      if (!listAppsFragment.str.contains("SU Java-Code Running!")) {
        listAppsFragment.str = "Root not stable. Try again or update your root.";
      }
      if ((!listAppsFragment.str.contains("Error LP:")) && (!listAppsFragment.str.contains("Object not found!"))) {
        ((TextView)((LinearLayout)localObject).findViewById(2131558541)).append(Utils.getColoredText(listAppsFragment.str, "#ff00ff73", "bold"));
      }
      if ((listAppsFragment.str.contains("Error LP:")) || (listAppsFragment.str.contains("Object not found!"))) {
        ((TextView)((LinearLayout)localObject).findViewById(2131558541)).append(Utils.getColoredText(listAppsFragment.str, "#ffff0055", "bold"));
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject;
        localException.printStackTrace();
        dismiss();
      }
    }
    localObject = new AlertDlg(listAppsFragment.frag.getContext());
    ((AlertDlg)localObject).setIcon(2130837548);
    ((AlertDlg)localObject).setTitle(Utils.getText(2131165186));
    ((AlertDlg)localObject).setCancelable(true);
    ((AlertDlg)localObject).setPositiveButton(Utils.getText(17039370), null);
    if (listAppsFragment.func != 0) {
      ((AlertDlg)localObject).setNeutralButton(2131165510, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          new Thread(new Runnable()
          {
            public void run()
            {
              try
              {
                Utils.kill(listAppsFragment.pli.pkgName);
                Utils.run_all("killall " + listAppsFragment.pli.pkgName);
                this.val$handler.post(new Runnable()
                {
                  public void run()
                  {
                    Object localObject = listAppsFragment.getPkgMng().getLaunchIntentForPackage(listAppsFragment.pli.pkgName);
                    try
                    {
                      listAppsFragment.patchAct.startActivity((Intent)localObject);
                      localObject = listAppsFragment.frag;
                      listAppsFragment.removeDialogLP(4);
                      return;
                    }
                    catch (Exception localException)
                    {
                      localException.printStackTrace();
                    }
                  }
                });
                return;
              }
              catch (Exception localException)
              {
                while (listAppsFragment.frag == null) {}
                listAppsFragment.frag.runToMain(new Runnable()
                {
                  public void run()
                  {
                    Toast.makeText(listAppsFragment.frag.getContext(), Utils.getText(2131165437), 1).show();
                  }
                });
              }
            }
          }).start();
        }
      });
    }
    ((AlertDlg)localObject).setView(localLinearLayout);
    return ((AlertDlg)localObject).create();
  }
  
  public void showDialog()
  {
    if (this.dialog == null) {
      this.dialog = onCreateDialog();
    }
    if (this.dialog != null) {
      this.dialog.show();
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */