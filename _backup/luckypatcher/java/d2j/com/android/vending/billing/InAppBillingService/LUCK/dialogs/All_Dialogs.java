package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.BootListItemAdapter;
import com.android.vending.billing.InAppBillingService.LUCK.Components;
import com.android.vending.billing.InAppBillingService.LUCK.CoreItem;
import com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem;
import com.android.vending.billing.InAppBillingService.LUCK.LogCollector;
import com.android.vending.billing.InAppBillingService.LUCK.Patterns;
import com.android.vending.billing.InAppBillingService.LUCK.Perm;
import com.android.vending.billing.InAppBillingService.LUCK.PkgListItem;
import com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.android.vending.billing.InAppBillingService.LUCK.patchActivity;
import com.chelpus.Utils;
import com.google.android.finsky.billing.iab.google.util.Base64;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Comparator;
import org.json.JSONObject;

public class All_Dialogs
{
  public static final int ADD_BOOT = 2;
  public static final int CREATE_APK = 0;
  public static final int CUSTOM_PATCH = 1;
  Dialog dialog = null;
  public int dialog_int = 255;
  
  public void dismiss()
  {
    if (this.dialog != null)
    {
      this.dialog.dismiss();
      this.dialog = null;
    }
  }
  
  public boolean isVisible()
  {
    if (this.dialog != null) {
      return this.dialog.isShowing();
    }
    return false;
  }
  
  public Dialog onCreateDialog()
  {
    System.out.println("Create dialog");
    this.dialog_int = listAppsFragment.dialog_int;
    if ((listAppsFragment.frag == null) || (listAppsFragment.frag.getContext() == null)) {
      dismiss();
    }
    try
    {
      System.out.println("All Dialog create.");
      switch (listAppsFragment.dialog_int)
      {
      case 21: 
        localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
        if (listAppsFragment.perm_adapt != null)
        {
          listAppsFragment.perm_adapt.setNotifyOnChange(true);
          ((AlertDlg)localObject1).setAdapterNotClose(true);
          ((AlertDlg)localObject1).setAdapter(listAppsFragment.perm_adapt, new AdapterView.OnItemClickListener()
          {
            public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
            {
              paramAnonymousAdapterView = (Perm)listAppsFragment.perm_adapt.getItem(paramAnonymousInt);
              if (paramAnonymousAdapterView.Status) {}
              for (paramAnonymousAdapterView.Status = false;; paramAnonymousAdapterView.Status = true)
              {
                listAppsFragment.perm_adapt.notifyDataSetChanged();
                return;
              }
            }
          });
          ((AlertDlg)localObject1).setPositiveButton(Utils.getText(2131165382), new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
              paramAnonymousDialogInterface = new ArrayList();
              ArrayList localArrayList = new ArrayList();
              paramAnonymousInt = 0;
              while (paramAnonymousInt < listAppsFragment.perm_adapt.getCount())
              {
                Perm localPerm = (Perm)listAppsFragment.perm_adapt.getItem(paramAnonymousInt);
                if ((!localPerm.Status) && (!localPerm.Name.contains("chelpus_"))) {
                  paramAnonymousDialogInterface.add(localPerm.Name);
                }
                if ((localPerm.Status) && (localPerm.Name.startsWith("chelpa_per_")))
                {
                  paramAnonymousDialogInterface.add(localPerm.Name.replace("chelpa_per_", ""));
                  System.out.println(localPerm.Name.replace("chelpa_per_", ""));
                }
                if ((!localPerm.Status) && (localPerm.Name.contains("chelpus_")) && (!localPerm.Name.contains("disabled_")))
                {
                  localArrayList.add(localPerm.Name.replace("chelpus_", ""));
                  System.out.println("" + localPerm.Name.replace("chelpus_", ""));
                }
                if ((localPerm.Status) && (localPerm.Name.contains("chelpus_disabled_")))
                {
                  localArrayList.add(localPerm.Name.replace("chelpus_", ""));
                  System.out.println("" + localPerm.Name.replace("chelpus_", ""));
                }
                paramAnonymousInt += 1;
              }
              if (listAppsFragment.rebuldApk.equals(""))
              {
                listAppsFragment.frag.createapkpermissions(paramAnonymousDialogInterface, localArrayList);
                return;
              }
              listAppsFragment.frag.toolbar_createapkpermissions(paramAnonymousDialogInterface, localArrayList);
            }
          });
        }
        return ((AlertDlg)localObject1).create();
      }
    }
    catch (Exception localException1)
    {
      Object localObject1;
      System.out.println("LuckyPatcher (Create Dialog): " + localException1);
      localException1.printStackTrace();
      localObject2 = new AlertDlg(listAppsFragment.frag.getContext());
      ((AlertDlg)localObject2).setTitle("Error").setMessage("Sorry...\nShow Dialog - Error...").setNegativeButton("OK", null);
      return ((AlertDlg)localObject2).create();
    }
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
    if (listAppsFragment.perm_adapt != null)
    {
      listAppsFragment.perm_adapt.setNotifyOnChange(true);
      ((AlertDlg)localObject1).setAdapterNotClose(true);
      ((AlertDlg)localObject1).setAdapter(listAppsFragment.perm_adapt, new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          paramAnonymousAdapterView = (Perm)listAppsFragment.perm_adapt.getItem(paramAnonymousInt);
          if (paramAnonymousAdapterView.Status) {}
          for (paramAnonymousAdapterView.Status = false;; paramAnonymousAdapterView.Status = true)
          {
            listAppsFragment.perm_adapt.notifyDataSetChanged();
            return;
          }
        }
      });
      ((AlertDlg)localObject1).setPositiveButton(Utils.getText(2131165648), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          paramAnonymousDialogInterface = new ArrayList();
          ArrayList localArrayList = new ArrayList();
          paramAnonymousInt = 0;
          while (paramAnonymousInt < listAppsFragment.perm_adapt.getCount())
          {
            Perm localPerm = (Perm)listAppsFragment.perm_adapt.getItem(paramAnonymousInt);
            if ((!localPerm.Status) && (!localPerm.Name.contains("chelpus_"))) {
              paramAnonymousDialogInterface.add(localPerm.Name);
            }
            if ((localPerm.Status) && (localPerm.Name.startsWith("chelpa_per_")))
            {
              paramAnonymousDialogInterface.add(localPerm.Name.replace("chelpa_per_", ""));
              System.out.println(localPerm.Name.replace("chelpa_per_", ""));
            }
            if ((!localPerm.Status) && (localPerm.Name.contains("chelpus_")) && (!localPerm.Name.contains("disabled_")))
            {
              localArrayList.add(localPerm.Name.replace("chelpus_", ""));
              System.out.println("" + localPerm.Name.replace("chelpus_", ""));
            }
            if ((localPerm.Status) && (localPerm.Name.contains("chelpus_disabled_")))
            {
              localArrayList.add(localPerm.Name.replace("chelpus_", ""));
              System.out.println("" + localPerm.Name.replace("chelpus_", ""));
            }
            paramAnonymousInt += 1;
          }
          listAppsFragment.frag.apkpermissions(listAppsFragment.pli, paramAnonymousDialogInterface, localArrayList);
        }
      });
    }
    return ((AlertDlg)localObject1).create();
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
    if (listAppsFragment.perm_adapt != null)
    {
      listAppsFragment.perm_adapt.setNotifyOnChange(true);
      ((AlertDlg)localObject1).setAdapterNotClose(true);
      ((AlertDlg)localObject1).setAdapter(listAppsFragment.perm_adapt, new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          paramAnonymousAdapterView = (Perm)listAppsFragment.perm_adapt.getItem(paramAnonymousInt);
          if (!paramAnonymousAdapterView.Status)
          {
            new Utils("").cmdRoot(new String[] { "pm enable '" + listAppsFragment.pli.pkgName + "/" + paramAnonymousAdapterView.Name + "'" });
            if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(listAppsFragment.pli.pkgName, paramAnonymousAdapterView.Name)) != 2) {
              break label224;
            }
          }
          label224:
          for (paramAnonymousAdapterView.Status = false;; paramAnonymousAdapterView.Status = true)
          {
            listAppsFragment.perm_adapt.notifyDataSetChanged();
            listAppsFragment.pli.modified = true;
            listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.pli);
            listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.pli.pkgName, true).commit();
            return;
            new Utils("").cmdRoot(new String[] { "pm disable '" + listAppsFragment.pli.pkgName + "/" + paramAnonymousAdapterView.Name + "'" });
            break;
          }
        }
      });
      ((AlertDlg)localObject1).setPositiveButton(Utils.getText(2131165510), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          try
          {
            paramAnonymousDialogInterface = listAppsFragment.getPkgMng().getLaunchIntentForPackage(listAppsFragment.pli.pkgName);
            if (listAppsFragment.patchAct != null) {
              listAppsFragment.patchAct.startActivity(paramAnonymousDialogInterface);
            }
            listAppsFragment.removeDialogLP(29);
            return;
          }
          catch (Exception paramAnonymousDialogInterface)
          {
            for (;;)
            {
              Toast.makeText(listAppsFragment.frag.getContext(), Utils.getText(2131165437), 1).show();
            }
          }
        }
      });
    }
    return ((AlertDlg)localObject1).create();
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
    if (listAppsFragment.component_adapt != null)
    {
      listAppsFragment.component_adapt.setNotifyOnChange(true);
      ((AlertDlg)localObject1).setAdapterNotClose(true);
      ((AlertDlg)localObject1).setAdapter(listAppsFragment.component_adapt, new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          paramAnonymousAdapterView = (Components)listAppsFragment.component_adapt.getItem(paramAnonymousInt);
          if (!paramAnonymousAdapterView.header)
          {
            if (paramAnonymousAdapterView.Status) {
              break label167;
            }
            new Utils("").cmdRoot(new String[] { "pm enable '" + listAppsFragment.pli.pkgName + "/" + paramAnonymousAdapterView.Name + "'" });
            if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(listAppsFragment.pli.pkgName, paramAnonymousAdapterView.Name)) != 2) {
              break label231;
            }
          }
          label167:
          label231:
          for (paramAnonymousAdapterView.Status = false;; paramAnonymousAdapterView.Status = true)
          {
            listAppsFragment.component_adapt.notifyDataSetChanged();
            listAppsFragment.pli.modified = true;
            listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.pli);
            listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.pli.pkgName, true).commit();
            return;
            new Utils("").cmdRoot(new String[] { "pm disable '" + listAppsFragment.pli.pkgName + "/" + paramAnonymousAdapterView.Name + "'" });
            break;
          }
        }
      });
      ((AlertDlg)localObject1).setPositiveButton(Utils.getText(2131165510), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          try
          {
            paramAnonymousDialogInterface = listAppsFragment.getPkgMng().getLaunchIntentForPackage(listAppsFragment.pli.pkgName);
            if (listAppsFragment.patchAct != null) {
              listAppsFragment.patchAct.startActivity(paramAnonymousDialogInterface);
            }
            paramAnonymousDialogInterface = listAppsFragment.frag;
            listAppsFragment.removeDialogLP(31);
            return;
          }
          catch (Exception paramAnonymousDialogInterface)
          {
            for (;;)
            {
              Toast.makeText(listAppsFragment.frag.getContext(), Utils.getText(2131165437), 1).show();
            }
          }
        }
      });
    }
    return ((AlertDlg)localObject1).create();
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
    if (listAppsFragment.perm_adapt != null)
    {
      listAppsFragment.perm_adapt.setNotifyOnChange(true);
      ((AlertDlg)localObject1).setAdapterNotClose(true);
      ((AlertDlg)localObject1).setAdapter(listAppsFragment.perm_adapt, new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          paramAnonymousAdapterView = (Perm)listAppsFragment.perm_adapt.getItem(paramAnonymousInt);
          if (paramAnonymousAdapterView.Status) {}
          for (paramAnonymousAdapterView.Status = false;; paramAnonymousAdapterView.Status = true)
          {
            listAppsFragment.perm_adapt.notifyDataSetChanged();
            return;
          }
        }
      });
      ((AlertDlg)localObject1).setPositiveButton(Utils.getText(2131165648), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          paramAnonymousDialogInterface = new ArrayList();
          ArrayList localArrayList = new ArrayList();
          paramAnonymousInt = 0;
          while (paramAnonymousInt < listAppsFragment.perm_adapt.getCount())
          {
            Perm localPerm = (Perm)listAppsFragment.perm_adapt.getItem(paramAnonymousInt);
            if ((!localPerm.Status) && (!localPerm.Name.contains("chelpus_"))) {
              paramAnonymousDialogInterface.add(localPerm.Name);
            }
            if ((localPerm.Status) && (localPerm.Name.startsWith("chelpa_per_")))
            {
              paramAnonymousDialogInterface.add(localPerm.Name.replace("chelpa_per_", ""));
              System.out.println(localPerm.Name.replace("chelpa_per_", ""));
            }
            if ((!localPerm.Status) && (localPerm.Name.contains("chelpus_")) && (!localPerm.Name.contains("disabled_")))
            {
              localArrayList.add(localPerm.Name.replace("chelpus_", ""));
              System.out.println("" + localPerm.Name.replace("chelpus_", ""));
            }
            if ((localPerm.Status) && (localPerm.Name.contains("chelpus_disabled_")))
            {
              localArrayList.add(localPerm.Name.replace("chelpus_", ""));
              System.out.println("" + localPerm.Name.replace("chelpus_", ""));
            }
            paramAnonymousInt += 1;
          }
          listAppsFragment.frag.apkpermissions_safe(listAppsFragment.pli, paramAnonymousDialogInterface, localArrayList);
        }
      });
    }
    return ((AlertDlg)localObject1).create();
    listAppsFragment.changedPermissions = new ArrayList();
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
    if (listAppsFragment.perm_adapt != null)
    {
      listAppsFragment.perm_adapt.setNotifyOnChange(true);
      ((AlertDlg)localObject1).setAdapterNotClose(true);
      ((AlertDlg)localObject1).setAdapter(listAppsFragment.perm_adapt, new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, final int paramAnonymousInt, long paramAnonymousLong)
        {
          listAppsFragment.frag.runWithWait(new Runnable()
          {
            public void run()
            {
              Perm localPerm = (Perm)listAppsFragment.perm_adapt.getItem(paramAnonymousInt);
              listAppsFragment.frag.disable_permission(listAppsFragment.pli, localPerm);
              listAppsFragment.frag.contextpermissions();
              listAppsFragment.frag.runToMain(new Runnable()
              {
                public void run()
                {
                  listAppsFragment locallistAppsFragment = listAppsFragment.frag;
                  listAppsFragment.removeDialogLP(10);
                  locallistAppsFragment = listAppsFragment.frag;
                  listAppsFragment.showDialogLP(10);
                }
              });
            }
          });
        }
      });
      ((AlertDlg)localObject1).setPositiveButton(Utils.getText(2131165623), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          listAppsFragment.frag.runWithWait(new Runnable()
          {
            public void run()
            {
              localObject3 = null;
              Object localObject1 = localObject3;
              try
              {
                if (Utils.exists("/data/system/packages.xml")) {
                  localObject1 = "/data/system/packages.xml";
                }
              }
              catch (Exception localException1)
              {
                for (;;)
                {
                  System.out.println("LuckyPatcher (Get packages.xml Error): " + localException1);
                  localObject2 = localObject3;
                }
              }
              localObject3 = localObject1;
              try
              {
                if (Utils.exists("/dbdata/system/packages.xml")) {
                  localObject3 = "/dbdata/system/packages.xml";
                }
              }
              catch (Exception localException3)
              {
                for (;;)
                {
                  System.out.println("LuckyPatcher (Get packages.xml Error): " + localException3);
                  localObject4 = localObject2;
                }
              }
              localObject1 = Utils.getPermissions((String)localObject3);
              try
              {
                boolean bool = ((String)localObject1).equals("");
                if (bool) {}
              }
              catch (Exception localException2)
              {
                Object localObject2;
                Object localObject4;
                for (;;) {}
              }
              Utils.copyFile(listAppsFragment.frag.getContext().getDir("packages", 0).getAbsolutePath() + "/packages.xml", (String)localObject3, false, false);
              Utils.run_all("chmod 777 " + (String)localObject3);
              Utils.run_all("chown 1000.1000 " + (String)localObject3);
              Utils.run_all("chown 1000:1000 " + (String)localObject3);
              listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).modified = true;
              listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).pkgName, true).commit();
              Utils.reboot();
              Utils.run_all("killall -9 zygote");
            }
          });
        }
      });
    }
    return ((AlertDlg)localObject1).create();
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
    if (listAppsFragment.patch_adapt != null)
    {
      listAppsFragment.patch_adapt.setNotifyOnChange(true);
      ((AlertDlg)localObject1).setAdapterNotClose(true);
      ((AlertDlg)localObject1).setAdapter(listAppsFragment.patch_adapt, new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          paramAnonymousView = (Patterns)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
          if (paramAnonymousView.Status) {}
          for (paramAnonymousView.Status = false;; paramAnonymousView.Status = true)
          {
            paramAnonymousAdapterView = paramAnonymousView;
            if (paramAnonymousInt == 4)
            {
              paramAnonymousAdapterView = paramAnonymousView;
              if (paramAnonymousView.Status)
              {
                paramAnonymousAdapterView = (Patterns)listAppsFragment.patch_adapt.getItem(5);
                paramAnonymousAdapterView.Status = false;
              }
            }
            paramAnonymousView = paramAnonymousAdapterView;
            if (paramAnonymousInt == 5)
            {
              paramAnonymousView = paramAnonymousAdapterView;
              if (paramAnonymousAdapterView.Status)
              {
                paramAnonymousView = (Patterns)listAppsFragment.patch_adapt.getItem(4);
                paramAnonymousView.Status = false;
              }
            }
            paramAnonymousAdapterView = paramAnonymousView;
            if (paramAnonymousInt == 6)
            {
              paramAnonymousAdapterView = paramAnonymousView;
              if (paramAnonymousView.Status)
              {
                ((Patterns)listAppsFragment.patch_adapt.getItem(7)).Status = true;
                paramAnonymousAdapterView = (Patterns)listAppsFragment.patch_adapt.getItem(8);
                paramAnonymousAdapterView.Status = false;
              }
            }
            if ((paramAnonymousInt == 8) && (paramAnonymousAdapterView.Status))
            {
              ((Patterns)listAppsFragment.patch_adapt.getItem(7)).Status = true;
              ((Patterns)listAppsFragment.patch_adapt.getItem(6)).Status = false;
            }
            listAppsFragment.patch_adapt.notifyDataSetChanged();
            return;
          }
        }
      });
      ((AlertDlg)localObject1).setPositiveButton(Utils.getText(2131165592), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Object localObject = "1";
          int i = listAppsFragment.patch_adapt.getCount();
          paramAnonymousInt = 0;
          while (paramAnonymousInt < i)
          {
            Patterns localPatterns = (Patterns)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (localPatterns.Status)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (paramAnonymousInt < 6) {
                paramAnonymousDialogInterface = (String)localObject + "pattern" + (paramAnonymousInt + 1) + "_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 6)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = paramAnonymousDialogInterface + "copyDC" + "_";
              }
            }
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (paramAnonymousInt == 7)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (localPatterns.Status) {
                paramAnonymousDialogInterface = (String)localObject + "backup" + "_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 8)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = paramAnonymousDialogInterface + "deleteDC" + "_";
              }
            }
            paramAnonymousInt += 1;
          }
          listAppsFragment.frag.addIgnoreOdex(listAppsFragment.pli, (String)localObject);
        }
      });
    }
    return ((AlertDlg)localObject1).create();
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
    if (listAppsFragment.adapt != null)
    {
      listAppsFragment.adapt.setNotifyOnChange(true);
      ((AlertDlg)localObject1).setAdapter(listAppsFragment.adapt, new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          switch (listAppsFragment.func)
          {
          default: 
            return;
          case 1: 
            paramAnonymousAdapterView = listAppsFragment.frag;
            listAppsFragment.removeDialogLP(16);
            listAppsFragment.customselect = (File)listAppsFragment.adapt.getItem(paramAnonymousInt);
            paramAnonymousAdapterView = listAppsFragment.frag;
            listAppsFragment.showDialogLP(15);
            return;
          case 2: 
            paramAnonymousAdapterView = listAppsFragment.frag;
            listAppsFragment.removeDialogLP(16);
            listAppsFragment.customselect = (File)listAppsFragment.adapt.getItem(paramAnonymousInt);
            listAppsFragment.frag.bootadd(listAppsFragment.pli, "custom");
            return;
          }
          paramAnonymousAdapterView = listAppsFragment.frag;
          listAppsFragment.removeDialogLP(16);
          listAppsFragment.customselect = (File)listAppsFragment.adapt.getItem(paramAnonymousInt);
          paramAnonymousAdapterView = listAppsFragment.frag;
          listAppsFragment.showDialogLP(15);
        }
      });
    }
    return ((AlertDlg)localObject1).create();
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
    if (listAppsFragment.adapt != null)
    {
      listAppsFragment.adapt.setNotifyOnChange(true);
      ((AlertDlg)localObject1).setAdapter(listAppsFragment.adapt, new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          paramAnonymousAdapterView = listAppsFragment.frag;
          listAppsFragment.removeDialogLP(26);
          paramAnonymousAdapterView = (File)listAppsFragment.adapt.getItem(paramAnonymousInt);
          listAppsFragment.frag.restore(listAppsFragment.pli, paramAnonymousAdapterView);
        }
      });
    }
    return ((AlertDlg)localObject1).create();
    if (listAppsFragment.adapt != null) {
      listAppsFragment.adapt.setNotifyOnChange(true);
    }
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext(), true);
    ((AlertDlg)localObject1).setAdapter(listAppsFragment.adapt, new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        paramAnonymousAdapterView = listAppsFragment.frag;
        listAppsFragment.removeDialogLP(28);
        paramAnonymousAdapterView = (FileApkListItem)listAppsFragment.adapt.getItem(paramAnonymousInt);
        listAppsFragment.frag.toolbar_restore(paramAnonymousAdapterView, false);
      }
    });
    ((AlertDlg)localObject1).setCancelable(true);
    ((AlertDlg)localObject1).setOnCancelListener(new DialogInterface.OnCancelListener()
    {
      public void onCancel(DialogInterface paramAnonymousDialogInterface) {}
    });
    return ((AlertDlg)localObject1).create();
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
    if (listAppsFragment.patch_adapt != null)
    {
      listAppsFragment.patch_adapt.setNotifyOnChange(true);
      ((AlertDlg)localObject1).setAdapterNotClose(true);
      ((AlertDlg)localObject1).setAdapter(listAppsFragment.patch_adapt, new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          paramAnonymousView = (Patterns)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
          if (paramAnonymousView.Status) {}
          for (paramAnonymousView.Status = false;; paramAnonymousView.Status = true)
          {
            paramAnonymousAdapterView = paramAnonymousView;
            if (paramAnonymousInt == 1)
            {
              paramAnonymousAdapterView = paramAnonymousView;
              if (paramAnonymousView.Status)
              {
                paramAnonymousAdapterView = (Patterns)listAppsFragment.patch_adapt.getItem(2);
                paramAnonymousAdapterView.Status = false;
              }
            }
            paramAnonymousView = paramAnonymousAdapterView;
            if (paramAnonymousInt == 2)
            {
              paramAnonymousView = paramAnonymousAdapterView;
              if (paramAnonymousAdapterView.Status)
              {
                paramAnonymousView = (Patterns)listAppsFragment.patch_adapt.getItem(1);
                paramAnonymousView.Status = false;
              }
            }
            paramAnonymousAdapterView = paramAnonymousView;
            if (paramAnonymousInt == 9)
            {
              paramAnonymousAdapterView = paramAnonymousView;
              if (paramAnonymousView.Status)
              {
                ((Patterns)listAppsFragment.patch_adapt.getItem(10)).Status = true;
                paramAnonymousAdapterView = (Patterns)listAppsFragment.patch_adapt.getItem(11);
                paramAnonymousAdapterView.Status = false;
              }
            }
            if ((paramAnonymousInt == 11) && (paramAnonymousAdapterView.Status))
            {
              ((Patterns)listAppsFragment.patch_adapt.getItem(10)).Status = true;
              ((Patterns)listAppsFragment.patch_adapt.getItem(9)).Status = false;
            }
            listAppsFragment.patch_adapt.notifyDataSetChanged();
            return;
          }
        }
      });
      ((AlertDlg)localObject1).setPositiveButton(Utils.getText(2131165592), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Object localObject = "1";
          int i = listAppsFragment.patch_adapt.getCount();
          paramAnonymousInt = 0;
          while (paramAnonymousInt < i)
          {
            Patterns localPatterns = (Patterns)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (localPatterns.Status)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (paramAnonymousInt < 7) {
                paramAnonymousDialogInterface = (String)localObject + "pattern" + paramAnonymousInt + "_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 7)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = paramAnonymousDialogInterface + "dependencies" + "_";
              }
            }
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (paramAnonymousInt == 8)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (localPatterns.Status) {
                paramAnonymousDialogInterface = (String)localObject + "fulloffline" + "_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 9)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = paramAnonymousDialogInterface + "copyDC" + "_";
              }
            }
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (paramAnonymousInt == 10)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (localPatterns.Status) {
                paramAnonymousDialogInterface = (String)localObject + "backup" + "_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 11)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = paramAnonymousDialogInterface + "deleteDC" + "_";
              }
            }
            paramAnonymousInt += 1;
          }
          listAppsFragment.frag.ads(listAppsFragment.pli, (String)localObject);
        }
      });
    }
    return ((AlertDlg)localObject1).create();
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
    if (listAppsFragment.patch_adapt != null)
    {
      listAppsFragment.patch_adapt.setNotifyOnChange(true);
      ((AlertDlg)localObject1).setAdapterNotClose(true);
      ((AlertDlg)localObject1).setAdapter(listAppsFragment.patch_adapt, new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          paramAnonymousView = (Patterns)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
          if (paramAnonymousView.Status) {}
          for (paramAnonymousView.Status = false;; paramAnonymousView.Status = true)
          {
            paramAnonymousAdapterView = paramAnonymousView;
            if (paramAnonymousInt == 1)
            {
              paramAnonymousAdapterView = paramAnonymousView;
              if (paramAnonymousView.Status)
              {
                paramAnonymousAdapterView = (Patterns)listAppsFragment.patch_adapt.getItem(2);
                paramAnonymousAdapterView.Status = false;
              }
            }
            if ((paramAnonymousInt == 2) && (paramAnonymousAdapterView.Status)) {
              ((Patterns)listAppsFragment.patch_adapt.getItem(1)).Status = false;
            }
            listAppsFragment.patch_adapt.notifyDataSetChanged();
            return;
          }
        }
      });
      ((AlertDlg)localObject1).setPositiveButton(Utils.getText(2131165382), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Object localObject = "1";
          int i = listAppsFragment.patch_adapt.getCount();
          paramAnonymousInt = 0;
          while (paramAnonymousInt < i)
          {
            Patterns localPatterns = (Patterns)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (localPatterns.Status)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (paramAnonymousInt < 7) {
                paramAnonymousDialogInterface = (String)localObject + "pattern" + paramAnonymousInt + "_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 7)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = paramAnonymousDialogInterface + "dependencies" + "_";
              }
            }
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (paramAnonymousInt == 8)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (localPatterns.Status) {
                paramAnonymousDialogInterface = (String)localObject + "fulloffline" + "_";
              }
            }
            paramAnonymousInt += 1;
            localObject = paramAnonymousDialogInterface;
          }
          if (listAppsFragment.rebuldApk.equals(""))
          {
            listAppsFragment.frag.createapkads((String)localObject);
            return;
          }
          listAppsFragment.frag.toolbar_createapkads((String)localObject);
        }
      });
    }
    return ((AlertDlg)localObject1).create();
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
    if (listAppsFragment.patch_adapt != null)
    {
      listAppsFragment.patch_adapt.setNotifyOnChange(true);
      ((AlertDlg)localObject1).setAdapterNotClose(true);
      ((AlertDlg)localObject1).setAdapter(listAppsFragment.patch_adapt, new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          paramAnonymousView = (Patterns)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
          if (paramAnonymousView.Status) {}
          for (paramAnonymousView.Status = false;; paramAnonymousView.Status = true)
          {
            paramAnonymousAdapterView = paramAnonymousView;
            if (paramAnonymousInt == 3)
            {
              paramAnonymousAdapterView = paramAnonymousView;
              if (paramAnonymousView.Status)
              {
                ((Patterns)listAppsFragment.patch_adapt.getItem(4)).Status = true;
                paramAnonymousAdapterView = (Patterns)listAppsFragment.patch_adapt.getItem(5);
                paramAnonymousAdapterView.Status = false;
              }
            }
            if ((paramAnonymousInt == 5) && (paramAnonymousAdapterView.Status))
            {
              ((Patterns)listAppsFragment.patch_adapt.getItem(4)).Status = true;
              ((Patterns)listAppsFragment.patch_adapt.getItem(3)).Status = false;
            }
            listAppsFragment.patch_adapt.notifyDataSetChanged();
            return;
          }
        }
      });
      ((AlertDlg)localObject1).setPositiveButton(Utils.getText(2131165592), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Object localObject = "1";
          int i = listAppsFragment.patch_adapt.getCount();
          paramAnonymousInt = 0;
          while (paramAnonymousInt < i)
          {
            Patterns localPatterns = (Patterns)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (localPatterns.Status)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (paramAnonymousInt < 3) {
                paramAnonymousDialogInterface = (String)localObject + "pattern" + paramAnonymousInt + "_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 3)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = paramAnonymousDialogInterface + "copyDC" + "_";
              }
            }
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (paramAnonymousInt == 4)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (localPatterns.Status) {
                paramAnonymousDialogInterface = (String)localObject + "backup" + "_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 5)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = paramAnonymousDialogInterface + "deleteDC" + "_";
              }
            }
            paramAnonymousInt += 1;
          }
          listAppsFragment.frag.support(listAppsFragment.pli, (String)localObject);
        }
      });
    }
    return ((AlertDlg)localObject1).create();
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
    if (listAppsFragment.patch_adapt != null)
    {
      listAppsFragment.patch_adapt.setNotifyOnChange(true);
      ((AlertDlg)localObject1).setAdapterNotClose(true);
      ((AlertDlg)localObject1).setAdapter(listAppsFragment.patch_adapt, new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          paramAnonymousAdapterView = (Patterns)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
          if (paramAnonymousAdapterView.Status) {}
          for (paramAnonymousAdapterView.Status = false;; paramAnonymousAdapterView.Status = true)
          {
            listAppsFragment.patch_adapt.notifyDataSetChanged();
            return;
          }
        }
      });
      ((AlertDlg)localObject1).setPositiveButton(Utils.getText(2131165382), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          paramAnonymousDialogInterface = "1";
          int i = listAppsFragment.patch_adapt.getCount();
          paramAnonymousInt = 0;
          while (paramAnonymousInt < i)
          {
            Object localObject = paramAnonymousDialogInterface;
            if (((Patterns)listAppsFragment.patch_adapt.getItem(paramAnonymousInt)).Status)
            {
              localObject = paramAnonymousDialogInterface;
              if (paramAnonymousInt < 3) {
                localObject = paramAnonymousDialogInterface + "pattern" + paramAnonymousInt + "_";
              }
            }
            paramAnonymousInt += 1;
            paramAnonymousDialogInterface = (DialogInterface)localObject;
          }
          if (listAppsFragment.rebuldApk.equals(""))
          {
            listAppsFragment.frag.createapksupport(paramAnonymousDialogInterface);
            return;
          }
          listAppsFragment.frag.toolbar_createapksupport(paramAnonymousDialogInterface);
        }
      });
    }
    return ((AlertDlg)localObject1).create();
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
    if (listAppsFragment.patch_adapt != null)
    {
      listAppsFragment.patch_adapt.setNotifyOnChange(true);
      ((AlertDlg)localObject1).setAdapterNotClose(true);
      ((AlertDlg)localObject1).setAdapter(listAppsFragment.patch_adapt, new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          paramAnonymousView = (Patterns)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
          if (paramAnonymousView.Status) {}
          for (paramAnonymousView.Status = false;; paramAnonymousView.Status = true)
          {
            paramAnonymousAdapterView = paramAnonymousView;
            if (paramAnonymousInt == 0)
            {
              paramAnonymousAdapterView = paramAnonymousView;
              if (paramAnonymousView.Status)
              {
                paramAnonymousAdapterView = (Patterns)listAppsFragment.patch_adapt.getItem(1);
                paramAnonymousAdapterView.Status = false;
              }
            }
            paramAnonymousView = paramAnonymousAdapterView;
            if (paramAnonymousInt == 1)
            {
              paramAnonymousView = paramAnonymousAdapterView;
              if (paramAnonymousAdapterView.Status)
              {
                paramAnonymousView = (Patterns)listAppsFragment.patch_adapt.getItem(0);
                paramAnonymousView.Status = false;
              }
            }
            paramAnonymousAdapterView = paramAnonymousView;
            if (paramAnonymousInt == 3)
            {
              paramAnonymousAdapterView = paramAnonymousView;
              if (paramAnonymousView.Status)
              {
                ((Patterns)listAppsFragment.patch_adapt.getItem(0)).Status = false;
                ((Patterns)listAppsFragment.patch_adapt.getItem(1)).Status = false;
                ((Patterns)listAppsFragment.patch_adapt.getItem(2)).Status = false;
                paramAnonymousAdapterView = (Patterns)listAppsFragment.patch_adapt.getItem(4);
                paramAnonymousAdapterView.Status = false;
              }
            }
            if ((paramAnonymousInt == 4) && (paramAnonymousAdapterView.Status))
            {
              ((Patterns)listAppsFragment.patch_adapt.getItem(0)).Status = false;
              ((Patterns)listAppsFragment.patch_adapt.getItem(1)).Status = false;
              ((Patterns)listAppsFragment.patch_adapt.getItem(2)).Status = false;
              ((Patterns)listAppsFragment.patch_adapt.getItem(3)).Status = false;
            }
            listAppsFragment.patch_adapt.notifyDataSetChanged();
            return;
          }
        }
      });
      ((AlertDlg)localObject1).setPositiveButton(Utils.getText(2131165382), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Object localObject = "1";
          int i = listAppsFragment.patch_adapt.getCount();
          paramAnonymousInt = 0;
          while (paramAnonymousInt < i)
          {
            Patterns localPatterns = (Patterns)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (paramAnonymousInt == 0)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (localPatterns.Status) {
                paramAnonymousDialogInterface = "pattern1_pattern2_pattern3_pattern5_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 1)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = "pattern1_pattern2_pattern3_pattern6_";
              }
            }
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (paramAnonymousInt == 2)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (localPatterns.Status) {
                paramAnonymousDialogInterface = (String)localObject + "pattern4" + "_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 3)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = paramAnonymousDialogInterface + "amazon" + "_";
              }
            }
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (paramAnonymousInt == 4)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (localPatterns.Status) {
                paramAnonymousDialogInterface = (String)localObject + "samsung" + "_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 5)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = paramAnonymousDialogInterface + "dependencies" + "_";
              }
            }
            paramAnonymousInt += 1;
          }
          if (listAppsFragment.rebuldApk.equals(""))
          {
            listAppsFragment.frag.createapklvl((String)localObject);
            return;
          }
          listAppsFragment.frag.toolbar_createapklvl((String)localObject);
        }
      });
    }
    return ((AlertDlg)localObject1).create();
    localObject1 = new AlertDlg(listAppsFragment.frag.getContext());
    if (listAppsFragment.patch_adapt != null)
    {
      listAppsFragment.patch_adapt.setNotifyOnChange(true);
      ((AlertDlg)localObject1).setAdapterNotClose(true);
      ((AlertDlg)localObject1).setAdapter(listAppsFragment.patch_adapt, new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          paramAnonymousView = (Patterns)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
          if (paramAnonymousView.Status) {}
          for (paramAnonymousView.Status = false;; paramAnonymousView.Status = true)
          {
            paramAnonymousAdapterView = paramAnonymousView;
            if (paramAnonymousInt == 0)
            {
              paramAnonymousAdapterView = paramAnonymousView;
              if (paramAnonymousView.Status)
              {
                paramAnonymousAdapterView = (Patterns)listAppsFragment.patch_adapt.getItem(1);
                paramAnonymousAdapterView.Status = false;
              }
            }
            paramAnonymousView = paramAnonymousAdapterView;
            if (paramAnonymousInt == 1)
            {
              paramAnonymousView = paramAnonymousAdapterView;
              if (paramAnonymousAdapterView.Status)
              {
                paramAnonymousView = (Patterns)listAppsFragment.patch_adapt.getItem(0);
                paramAnonymousView.Status = false;
              }
            }
            paramAnonymousAdapterView = paramAnonymousView;
            if (paramAnonymousInt == 3)
            {
              paramAnonymousAdapterView = paramAnonymousView;
              if (paramAnonymousView.Status)
              {
                ((Patterns)listAppsFragment.patch_adapt.getItem(0)).Status = false;
                ((Patterns)listAppsFragment.patch_adapt.getItem(1)).Status = false;
                ((Patterns)listAppsFragment.patch_adapt.getItem(2)).Status = false;
                paramAnonymousAdapterView = (Patterns)listAppsFragment.patch_adapt.getItem(4);
                paramAnonymousAdapterView.Status = false;
              }
            }
            paramAnonymousView = paramAnonymousAdapterView;
            if (paramAnonymousInt == 4)
            {
              paramAnonymousView = paramAnonymousAdapterView;
              if (paramAnonymousAdapterView.Status)
              {
                ((Patterns)listAppsFragment.patch_adapt.getItem(0)).Status = false;
                ((Patterns)listAppsFragment.patch_adapt.getItem(1)).Status = false;
                ((Patterns)listAppsFragment.patch_adapt.getItem(2)).Status = false;
                paramAnonymousView = (Patterns)listAppsFragment.patch_adapt.getItem(3);
                paramAnonymousView.Status = false;
              }
            }
            paramAnonymousAdapterView = paramAnonymousView;
            if (paramAnonymousInt == 6)
            {
              paramAnonymousAdapterView = paramAnonymousView;
              if (paramAnonymousView.Status)
              {
                ((Patterns)listAppsFragment.patch_adapt.getItem(7)).Status = true;
                paramAnonymousAdapterView = (Patterns)listAppsFragment.patch_adapt.getItem(8);
                paramAnonymousAdapterView.Status = false;
              }
            }
            if ((paramAnonymousInt == 8) && (paramAnonymousAdapterView.Status))
            {
              ((Patterns)listAppsFragment.patch_adapt.getItem(7)).Status = true;
              ((Patterns)listAppsFragment.patch_adapt.getItem(6)).Status = false;
            }
            listAppsFragment.patch_adapt.notifyDataSetChanged();
            return;
          }
        }
      });
      ((AlertDlg)localObject1).setPositiveButton(Utils.getText(2131165592), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Object localObject = "1";
          int i = listAppsFragment.patch_adapt.getCount();
          paramAnonymousInt = 0;
          while (paramAnonymousInt < i)
          {
            Patterns localPatterns = (Patterns)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (paramAnonymousInt == 0)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (localPatterns.Status) {
                paramAnonymousDialogInterface = "pattern1_pattern2_pattern3_pattern5_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 1)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = "pattern1_pattern2_pattern3_pattern6_";
              }
            }
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (paramAnonymousInt == 2)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (localPatterns.Status) {
                paramAnonymousDialogInterface = (String)localObject + "pattern4" + "_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 3)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = paramAnonymousDialogInterface + "amazon" + "_";
              }
            }
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (paramAnonymousInt == 4)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (localPatterns.Status) {
                paramAnonymousDialogInterface = (String)localObject + "samsung" + "_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 5)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = paramAnonymousDialogInterface + "dependencies" + "_";
              }
            }
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (paramAnonymousInt == 6)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (localPatterns.Status) {
                paramAnonymousDialogInterface = (String)localObject + "copyDC" + "_";
              }
            }
            localObject = paramAnonymousDialogInterface;
            if (paramAnonymousInt == 7)
            {
              localObject = paramAnonymousDialogInterface;
              if (localPatterns.Status) {
                localObject = paramAnonymousDialogInterface + "backup" + "_";
              }
            }
            paramAnonymousDialogInterface = (DialogInterface)localObject;
            if (paramAnonymousInt == 8)
            {
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (localPatterns.Status) {
                paramAnonymousDialogInterface = (String)localObject + "deleteDC" + "_";
              }
            }
            paramAnonymousInt += 1;
            localObject = paramAnonymousDialogInterface;
          }
          listAppsFragment.frag.addIgnoreOdex(listAppsFragment.pli, (String)localObject);
        }
      });
    }
    return ((AlertDlg)localObject1).create();
    localObject1 = (LinearLayout)View.inflate(listAppsFragment.frag.getContext(), 2130968608, null);
    final Object localObject3 = (ListView)((LinearLayout)localObject1).findViewById(2131558530);
    final Object localObject4 = (CheckBox)((LinearLayout)localObject1).findViewById(2131558531);
    ((CheckBox)localObject4).setChecked(true);
    ((CheckBox)localObject4).setText(Utils.getText(2131165371));
    ((CheckBox)localObject4).setMaxLines(1);
    if ((Utils.getFileDalvikCache("/system/framework/core.jar") != null) && (!Utils.getCurrentRuntimeValue().equals("ART")))
    {
      ((CheckBox)localObject4).setChecked(true);
      listAppsFragment.patchOnlyDalvikCore = true;
    }
    for (;;)
    {
      ((CheckBox)localObject4).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
      {
        public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
        {
          if (!Utils.getCurrentRuntimeValue().equals("ART"))
          {
            listAppsFragment.patchOnlyDalvikCore = paramAnonymousBoolean;
            return;
          }
          localObject4.setChecked(false);
          localObject4.setEnabled(false);
          listAppsFragment.patchOnlyDalvikCore = false;
        }
      });
      localObject4 = new AlertDlg(listAppsFragment.frag.getContext());
      if (listAppsFragment.patch_adapt != null)
      {
        listAppsFragment.patch_adapt.setNotifyOnChange(true);
        ((ListView)localObject3).setAdapter(listAppsFragment.patch_adapt);
        ((ListView)localObject3).setDivider(new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] { -1715492012, -4215980, -1715492012 }));
        ((ListView)localObject3).setDividerHeight(1);
        ((ListView)localObject3).setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
          public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
          {
            paramAnonymousAdapterView = (CoreItem)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
            if (paramAnonymousAdapterView.Status) {}
            for (paramAnonymousAdapterView.Status = false;; paramAnonymousAdapterView.Status = true)
            {
              CoreItem localCoreItem;
              do
              {
                paramAnonymousView = (CoreItem)listAppsFragment.patch_adapt.getItem(2);
                localCoreItem = (CoreItem)listAppsFragment.patch_adapt.getItem(3);
                if (((paramAnonymousInt == 0) || (paramAnonymousInt == 1)) && (paramAnonymousAdapterView.Status) && ((paramAnonymousView.Status) || (localCoreItem.Status)) && ((!Utils.exists("/system/framework/core.odex")) || (Utils.getCurrentRuntimeValue().contains("ART"))))
                {
                  localCoreItem.Status = false;
                  paramAnonymousView.Status = false;
                  listAppsFragment.frag.showMessage(Utils.getText(2131165748), Utils.getText(2131165602));
                }
                if (((paramAnonymousInt == 2) || (paramAnonymousInt == 3)) && (paramAnonymousAdapterView.Status) && ((!Utils.exists("/system/framework/core.odex")) || (Utils.getCurrentRuntimeValue().contains("ART"))) && ((((CoreItem)listAppsFragment.patch_adapt.getItem(0)).Status) || (((CoreItem)listAppsFragment.patch_adapt.getItem(1)).Status)))
                {
                  ((CoreItem)listAppsFragment.patch_adapt.getItem(0)).Status = false;
                  ((CoreItem)listAppsFragment.patch_adapt.getItem(1)).Status = false;
                  listAppsFragment.frag.showMessage(Utils.getText(2131165748), Utils.getText(2131165602));
                }
                if ((paramAnonymousInt == 4) && (paramAnonymousAdapterView.Status))
                {
                  ((CoreItem)listAppsFragment.patch_adapt.getItem(0)).Status = false;
                  ((CoreItem)listAppsFragment.patch_adapt.getItem(1)).Status = false;
                  ((CoreItem)listAppsFragment.patch_adapt.getItem(2)).Status = false;
                  ((CoreItem)listAppsFragment.patch_adapt.getItem(3)).Status = false;
                }
                if ((paramAnonymousInt == 5) && (paramAnonymousAdapterView.Status))
                {
                  ((CoreItem)listAppsFragment.patch_adapt.getItem(0)).Status = false;
                  ((CoreItem)listAppsFragment.patch_adapt.getItem(1)).Status = false;
                  ((CoreItem)listAppsFragment.patch_adapt.getItem(2)).Status = false;
                  ((CoreItem)listAppsFragment.patch_adapt.getItem(3)).Status = false;
                }
                listAppsFragment.patch_adapt.notifyDataSetChanged();
                return;
                paramAnonymousView = (CoreItem)listAppsFragment.patch_adapt.getItem(4);
                localCoreItem = (CoreItem)listAppsFragment.patch_adapt.getItem(5);
              } while ((paramAnonymousView.Status) || (localCoreItem.Status) || (paramAnonymousAdapterView.disabled));
            }
          }
        });
        ((AlertDlg)localObject4).setView((View)localObject1);
        ((AlertDlg)localObject4).setPositiveButton(Utils.getText(2131165592), new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            Object localObject = "patch";
            int i2 = listAppsFragment.patch_adapt.getCount();
            int i1 = 0;
            int j = 0;
            int i = 0;
            paramAnonymousInt = 0;
            int k = 0;
            if (k < i2)
            {
              CoreItem localCoreItem = (CoreItem)listAppsFragment.patch_adapt.getItem(k);
              int m = i1;
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (k == 0)
              {
                m = i1;
                paramAnonymousDialogInterface = (DialogInterface)localObject;
                if (localCoreItem.Status)
                {
                  paramAnonymousDialogInterface = (String)localObject + "_patch1";
                  m = 1;
                }
              }
              int n = j;
              localObject = paramAnonymousDialogInterface;
              if (k == 1)
              {
                n = j;
                localObject = paramAnonymousDialogInterface;
                if (localCoreItem.Status)
                {
                  localObject = paramAnonymousDialogInterface + "_patch2";
                  n = 1;
                }
              }
              j = i;
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (k == 2)
              {
                j = i;
                paramAnonymousDialogInterface = (DialogInterface)localObject;
                if (localCoreItem.Status)
                {
                  paramAnonymousDialogInterface = (String)localObject + "_patch3";
                  j = 1;
                }
              }
              i = j;
              localObject = paramAnonymousDialogInterface;
              if (k == 3)
              {
                i = j;
                localObject = paramAnonymousDialogInterface;
                if (localCoreItem.Status)
                {
                  localObject = paramAnonymousDialogInterface + "_patch4";
                  i = 1;
                }
              }
              j = paramAnonymousInt;
              paramAnonymousDialogInterface = (DialogInterface)localObject;
              if (k == 4)
              {
                j = paramAnonymousInt;
                paramAnonymousDialogInterface = (DialogInterface)localObject;
                if (localCoreItem.Status)
                {
                  paramAnonymousDialogInterface = "restoreCore";
                  j = 1;
                }
              }
              paramAnonymousInt = j;
              localObject = paramAnonymousDialogInterface;
              if (k == 5)
              {
                paramAnonymousInt = j;
                localObject = paramAnonymousDialogInterface;
                if (localCoreItem.Status) {
                  if (!paramAnonymousDialogInterface.contains("restoreCore")) {
                    break label354;
                  }
                }
              }
              label354:
              for (localObject = paramAnonymousDialogInterface + "_restoreServices";; localObject = "restoreServices")
              {
                paramAnonymousInt = 1;
                k += 1;
                i1 = m;
                j = n;
                break;
              }
            }
            if ((i1 != 0) || (j != 0) || (i != 0) || (paramAnonymousInt != 0)) {
              listAppsFragment.frag.corepatch((String)localObject);
            }
          }
        });
      }
      return ((AlertDlg)localObject4).create();
      listAppsFragment.patchOnlyDalvikCore = false;
      ((CheckBox)localObject4).setChecked(false);
      ((CheckBox)localObject4).setEnabled(false);
    }
    Object localObject2 = (LinearLayout)View.inflate(listAppsFragment.frag.getContext(), 2130968608, null);
    localObject3 = (CheckBox)((LinearLayout)localObject2).findViewById(2131558531);
    ((CheckBox)localObject3).setChecked(Utils.readXposedParamBoolean().getBoolean("module_on"));
    if (Utils.isXposedEnabled())
    {
      ((CheckBox)localObject3).setEnabled(true);
      ((CheckBox)localObject3).setText(Utils.getText(2131165750));
      ((CheckBox)localObject3).setMaxLines(2);
      localObject4 = new AlertDlg(listAppsFragment.frag.getContext());
      if (listAppsFragment.patch_adapt != null)
      {
        listAppsFragment.patch_adapt.setNotifyOnChange(true);
        localObject5 = (ListView)((LinearLayout)localObject2).findViewById(2131558530);
        ((ListView)localObject5).setAdapter(listAppsFragment.patch_adapt);
        ((ListView)localObject5).setDivider(new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] { -1715492012, -4215980, -1715492012 }));
        ((ListView)localObject5).setDividerHeight(1);
        if (Utils.isXposedEnabled()) {
          ((ListView)localObject5).setOnItemClickListener(new AdapterView.OnItemClickListener()
          {
            public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
            {
              paramAnonymousAdapterView = (CoreItem)listAppsFragment.patch_adapt.getItem(paramAnonymousInt);
              System.out.println("" + paramAnonymousAdapterView.Status);
              if (paramAnonymousAdapterView.Status) {}
              for (paramAnonymousAdapterView.Status = false;; paramAnonymousAdapterView.Status = true)
              {
                System.out.println("" + ((CoreItem)listAppsFragment.patch_adapt.getItem(paramAnonymousInt)).Status);
                listAppsFragment.patch_adapt.notifyDataSetChanged();
                return;
              }
            }
          });
        }
        ((AlertDlg)localObject4).setView((View)localObject2);
        if (Utils.isXposedEnabled()) {
          break label1907;
        }
        ((AlertDlg)localObject4).setPositiveButton(Utils.getText(2131165587), null);
      }
    }
    for (;;)
    {
      return ((AlertDlg)localObject4).create();
      ((CheckBox)localObject3).setEnabled(false);
      ((CheckBox)localObject3).setChecked(false);
      ((CheckBox)localObject3).setText(Utils.getText(2131165752));
      break;
      label1907:
      ((AlertDlg)localObject4).setPositiveButton(Utils.getText(2131165736), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          new Thread(new Runnable()
          {
            /* Error */
            public void run()
            {
              // Byte code:
              //   0: getstatic 34	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:patch_adapt	Landroid/widget/ArrayAdapter;
              //   3: invokevirtual 40	android/widget/ArrayAdapter:getCount	()I
              //   6: istore_2
              //   7: new 42	org/json/JSONObject
              //   10: dup
              //   11: invokespecial 43	org/json/JSONObject:<init>	()V
              //   14: astore 7
              //   16: iconst_0
              //   17: istore_1
              //   18: iload_1
              //   19: iload_2
              //   20: if_icmpge +363 -> 383
              //   23: getstatic 34	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:patch_adapt	Landroid/widget/ArrayAdapter;
              //   26: iload_1
              //   27: invokevirtual 47	android/widget/ArrayAdapter:getItem	(I)Ljava/lang/Object;
              //   30: checkcast 49	com/android/vending/billing/InAppBillingService/LUCK/CoreItem
              //   33: astore 8
              //   35: getstatic 55	java/lang/System:out	Ljava/io/PrintStream;
              //   38: new 57	java/lang/StringBuilder
              //   41: dup
              //   42: invokespecial 58	java/lang/StringBuilder:<init>	()V
              //   45: ldc 60
              //   47: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
              //   50: aload 8
              //   52: getfield 68	com/android/vending/billing/InAppBillingService/LUCK/CoreItem:Status	Z
              //   55: invokevirtual 71	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
              //   58: invokevirtual 75	java/lang/StringBuilder:toString	()Ljava/lang/String;
              //   61: invokevirtual 81	java/io/PrintStream:println	(Ljava/lang/String;)V
              //   64: iload_1
              //   65: ifne +16 -> 81
              //   68: aload 7
              //   70: ldc 83
              //   72: aload 8
              //   74: getfield 68	com/android/vending/billing/InAppBillingService/LUCK/CoreItem:Status	Z
              //   77: invokevirtual 87	org/json/JSONObject:put	(Ljava/lang/String;Z)Lorg/json/JSONObject;
              //   80: pop
              //   81: iload_1
              //   82: iconst_1
              //   83: if_icmpne +16 -> 99
              //   86: aload 7
              //   88: ldc 89
              //   90: aload 8
              //   92: getfield 68	com/android/vending/billing/InAppBillingService/LUCK/CoreItem:Status	Z
              //   95: invokevirtual 87	org/json/JSONObject:put	(Ljava/lang/String;Z)Lorg/json/JSONObject;
              //   98: pop
              //   99: iload_1
              //   100: iconst_2
              //   101: if_icmpne +16 -> 117
              //   104: aload 7
              //   106: ldc 91
              //   108: aload 8
              //   110: getfield 68	com/android/vending/billing/InAppBillingService/LUCK/CoreItem:Status	Z
              //   113: invokevirtual 87	org/json/JSONObject:put	(Ljava/lang/String;Z)Lorg/json/JSONObject;
              //   116: pop
              //   117: iload_1
              //   118: iconst_3
              //   119: if_icmpne +76 -> 195
              //   122: ldc 93
              //   124: iconst_0
              //   125: invokestatic 99	com/chelpus/Utils:getPkgInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
              //   128: astore 9
              //   130: aload 9
              //   132: ifnull +50 -> 182
              //   135: new 101	java/io/File
              //   138: dup
              //   139: aload 9
              //   141: getfield 107	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
              //   144: getfield 113	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
              //   147: iconst_0
              //   148: invokestatic 117	com/chelpus/Utils:getPlaceForOdex	(Ljava/lang/String;Z)Ljava/lang/String;
              //   151: invokespecial 119	java/io/File:<init>	(Ljava/lang/String;)V
              //   154: astore 10
              //   156: lconst_0
              //   157: lstore_3
              //   158: aload 10
              //   160: invokevirtual 123	java/io/File:length	()J
              //   163: lstore 5
              //   165: lload 5
              //   167: lstore_3
              //   168: lload_3
              //   169: ldc2_w 124
              //   172: lcmp
              //   173: ifgt +9 -> 182
              //   176: lload_3
              //   177: lconst_0
              //   178: lcmp
              //   179: ifne +71 -> 250
              //   182: aload 7
              //   184: ldc 127
              //   186: aload 8
              //   188: getfield 68	com/android/vending/billing/InAppBillingService/LUCK/CoreItem:Status	Z
              //   191: invokevirtual 87	org/json/JSONObject:put	(Ljava/lang/String;Z)Lorg/json/JSONObject;
              //   194: pop
              //   195: iload_1
              //   196: iconst_4
              //   197: if_icmpne +16 -> 213
              //   200: aload 7
              //   202: ldc -127
              //   204: aload 8
              //   206: getfield 68	com/android/vending/billing/InAppBillingService/LUCK/CoreItem:Status	Z
              //   209: invokevirtual 87	org/json/JSONObject:put	(Ljava/lang/String;Z)Lorg/json/JSONObject;
              //   212: pop
              //   213: iload_1
              //   214: iconst_1
              //   215: iadd
              //   216: istore_1
              //   217: goto -199 -> 18
              //   220: astore 9
              //   222: aload 9
              //   224: invokevirtual 132	org/json/JSONException:printStackTrace	()V
              //   227: goto -146 -> 81
              //   230: astore 9
              //   232: aload 9
              //   234: invokevirtual 132	org/json/JSONException:printStackTrace	()V
              //   237: goto -138 -> 99
              //   240: astore 9
              //   242: aload 9
              //   244: invokevirtual 132	org/json/JSONException:printStackTrace	()V
              //   247: goto -130 -> 117
              //   250: new 95	com/chelpus/Utils
              //   253: dup
              //   254: ldc 60
              //   256: invokespecial 133	com/chelpus/Utils:<init>	(Ljava/lang/String;)V
              //   259: iconst_1
              //   260: anewarray 135	java/lang/String
              //   263: dup
              //   264: iconst_0
              //   265: new 57	java/lang/StringBuilder
              //   268: dup
              //   269: invokespecial 58	java/lang/StringBuilder:<init>	()V
              //   272: getstatic 138	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:dalvikruncommand	Ljava/lang/String;
              //   275: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
              //   278: ldc -116
              //   280: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
              //   283: aload 9
              //   285: getfield 107	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
              //   288: getfield 113	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
              //   291: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
              //   294: ldc -114
              //   296: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
              //   299: getstatic 145	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:toolfilesdir	Ljava/lang/String;
              //   302: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
              //   305: ldc -114
              //   307: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
              //   310: aload 9
              //   312: getfield 107	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
              //   315: getfield 149	android/content/pm/ApplicationInfo:uid	I
              //   318: invokestatic 153	java/lang/String:valueOf	(I)Ljava/lang/String;
              //   321: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
              //   324: ldc -101
              //   326: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
              //   329: invokevirtual 75	java/lang/StringBuilder:toString	()Ljava/lang/String;
              //   332: aastore
              //   333: invokevirtual 159	com/chelpus/Utils:cmdRoot	([Ljava/lang/String;)Ljava/lang/String;
              //   336: pop
              //   337: iconst_1
              //   338: invokestatic 163	com/chelpus/Utils:market_billing_services	(Z)V
              //   341: iconst_1
              //   342: invokestatic 166	com/chelpus/Utils:market_licensing_services	(Z)V
              //   345: ldc 93
              //   347: invokestatic 169	com/chelpus/Utils:kill	(Ljava/lang/String;)V
              //   350: goto -168 -> 182
              //   353: astore 9
              //   355: aload 9
              //   357: invokevirtual 170	java/lang/Exception:printStackTrace	()V
              //   360: goto -178 -> 182
              //   363: astore 9
              //   365: aload 9
              //   367: invokevirtual 132	org/json/JSONException:printStackTrace	()V
              //   370: goto -175 -> 195
              //   373: astore 8
              //   375: aload 8
              //   377: invokevirtual 132	org/json/JSONException:printStackTrace	()V
              //   380: goto -167 -> 213
              //   383: aload 7
              //   385: ldc -84
              //   387: aload_0
              //   388: getfield 19	com/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1:this$1	Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;
              //   391: getfield 176	com/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35:val$chk_x	Landroid/widget/CheckBox;
              //   394: invokevirtual 182	android/widget/CheckBox:isChecked	()Z
              //   397: invokevirtual 87	org/json/JSONObject:put	(Ljava/lang/String;Z)Lorg/json/JSONObject;
              //   400: pop
              //   401: new 184	java/lang/Thread
              //   404: dup
              //   405: new 13	com/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1$1
              //   408: dup
              //   409: aload_0
              //   410: aload 7
              //   412: invokespecial 187	com/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1$1:<init>	(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1;Lorg/json/JSONObject;)V
              //   415: invokespecial 190	java/lang/Thread:<init>	(Ljava/lang/Runnable;)V
              //   418: invokevirtual 193	java/lang/Thread:start	()V
              //   421: return
              //   422: astore 8
              //   424: aload 8
              //   426: invokevirtual 132	org/json/JSONException:printStackTrace	()V
              //   429: goto -28 -> 401
              //   432: astore 10
              //   434: goto -266 -> 168
              // Local variable table:
              //   start	length	slot	name	signature
              //   0	437	0	this	1
              //   17	200	1	i	int
              //   6	15	2	j	int
              //   157	20	3	l1	long
              //   163	3	5	l2	long
              //   14	397	7	localJSONObject	JSONObject
              //   33	172	8	localCoreItem	CoreItem
              //   373	3	8	localJSONException1	org.json.JSONException
              //   422	3	8	localJSONException2	org.json.JSONException
              //   128	12	9	localPackageInfo	PackageInfo
              //   220	3	9	localJSONException3	org.json.JSONException
              //   230	3	9	localJSONException4	org.json.JSONException
              //   240	71	9	localJSONException5	org.json.JSONException
              //   353	3	9	localException1	Exception
              //   363	3	9	localJSONException6	org.json.JSONException
              //   154	5	10	localFile	File
              //   432	1	10	localException2	Exception
              // Exception table:
              //   from	to	target	type
              //   68	81	220	org/json/JSONException
              //   86	99	230	org/json/JSONException
              //   104	117	240	org/json/JSONException
              //   250	350	353	java/lang/Exception
              //   122	130	363	org/json/JSONException
              //   135	156	363	org/json/JSONException
              //   158	165	363	org/json/JSONException
              //   182	195	363	org/json/JSONException
              //   250	350	363	org/json/JSONException
              //   355	360	363	org/json/JSONException
              //   200	213	373	org/json/JSONException
              //   383	401	422	org/json/JSONException
              //   158	165	432	java/lang/Exception
            }
          }).start();
        }
      });
    }
    localObject3 = (LinearLayout)View.inflate(listAppsFragment.frag.getContext(), 2130968606, null);
    Object localObject5 = (TextView)((LinearLayout)localObject3).findViewById(2131558527);
    localObject4 = (CheckBox)((LinearLayout)localObject3).findViewById(2131558528);
    ((TextView)localObject5).setText(Utils.getText(2131165768));
    int i;
    int j;
    if (listAppsFragment.result_core_patch.contains("_patch1"))
    {
      i = 0;
      j = 0;
      if ((!listAppsFragment.str.contains("Core patched!")) && (!listAppsFragment.str.contains("Core 2 patched!"))) {
        break label2752;
      }
      localObject2 = Utils.getText(2131165372);
      if (listAppsFragment.str.contains("Core patched!")) {
        j = 0 + 1;
      }
      i = j;
      if (!listAppsFragment.str.contains("Core 2 patched!")) {
        break label3157;
      }
      i = j + 1;
      break label3157;
    }
    for (;;)
    {
      ((TextView)localObject5).append(Utils.getText(2131165300) + " " + i + "/2 ");
      ((TextView)localObject5).append(Utils.getColoredText((String)localObject2 + "\n", j, ""));
      if (listAppsFragment.result_core_patch.contains("_patch2"))
      {
        if (listAppsFragment.str.contains("Core unsigned install patched!"))
        {
          localObject2 = Utils.getText(2131165372);
          i = -16711936;
          label2178:
          ((TextView)localObject5).append(Utils.getText(2131165302) + " ");
          ((TextView)localObject5).append(Utils.getColoredText((String)localObject2 + "\n", i, ""));
        }
      }
      else
      {
        if (listAppsFragment.result_core_patch.contains("_patch3"))
        {
          if (!listAppsFragment.str.contains("Core4 patched!")) {
            break label2780;
          }
          localObject2 = Utils.getText(2131165372);
          i = -16711936;
          label2275:
          ((TextView)localObject5).append(Utils.getText(2131165304) + " ");
          ((TextView)localObject5).append(Utils.getColoredText((String)localObject2 + "\n", i, ""));
        }
        if (listAppsFragment.result_core_patch.contains("restore"))
        {
          ((CheckBox)localObject4).setChecked(false);
          ((CheckBox)localObject4).setEnabled(false);
          Utils.turn_off_patch_on_boot_all();
          if ((listAppsFragment.str.contains("Core restored!")) || (listAppsFragment.str.contains("Core 2 restored!")))
          {
            localObject2 = Utils.getText(2131165781);
            ((TextView)localObject5).append(Utils.getText(2131165300) + " ");
            ((TextView)localObject5).append(Utils.getColoredText((String)localObject2 + "\n", -16711936, ""));
          }
          if (listAppsFragment.str.contains("Core unsigned install restored!"))
          {
            localObject2 = Utils.getText(2131165781);
            ((TextView)localObject5).append(Utils.getText(2131165302) + " ");
            ((TextView)localObject5).append(Utils.getColoredText((String)localObject2 + "\n", -16711936, ""));
          }
          if (listAppsFragment.str.contains("Core4 restored!"))
          {
            localObject2 = Utils.getText(2131165781);
            ((TextView)localObject5).append(Utils.getText(2131165304) + " ");
            ((TextView)localObject5).append(Utils.getColoredText((String)localObject2 + "\n", -16711936, ""));
          }
        }
        ((TextView)localObject5).append("\n" + Utils.getText(2131165767));
        ((CheckBox)localObject4).setChecked(false);
        ((CheckBox)localObject4).setText(Utils.getText(2131165769));
        if (!((CheckBox)localObject4).isChecked()) {
          break label2794;
        }
        Utils.turn_on_patch_on_boot(listAppsFragment.result_core_patch);
      }
      for (;;)
      {
        ((CheckBox)localObject4).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
          public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
          {
            if (paramAnonymousBoolean)
            {
              Utils.turn_on_patch_on_boot(listAppsFragment.result_core_patch);
              return;
            }
            Utils.turn_off_patch_on_boot(listAppsFragment.result_core_patch);
          }
        });
        localObject2 = new AlertDlg(listAppsFragment.frag.getContext());
        ((AlertDlg)localObject2).setView((View)localObject3);
        ((AlertDlg)localObject2).setPositiveButton(Utils.getText(2131165587), new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            if (localObject4.isChecked())
            {
              Utils.turn_on_patch_on_boot(listAppsFragment.result_core_patch);
              return;
            }
            Utils.turn_off_patch_on_boot(listAppsFragment.result_core_patch);
          }
        }).setTitle(2131165495);
        return ((AlertDlg)localObject2).create();
        label2752:
        localObject2 = Utils.getText(2131165373);
        j = -65536;
        break;
        localObject2 = Utils.getText(2131165373);
        i = -65536;
        break label2178;
        label2780:
        localObject2 = Utils.getText(2131165372);
        i = -65536;
        break label2275;
        label2794:
        Utils.turn_off_patch_on_boot(listAppsFragment.result_core_patch);
      }
      localObject2 = new AlertDlg(listAppsFragment.frag.getContext());
      listAppsFragment.frag.refresh_boot();
      localObject3 = listAppsFragment.frag.getContext();
      localObject4 = listAppsFragment.getConfig();
      localObject5 = listAppsFragment.frag;
      listAppsFragment.adapter_boot = new BootListItemAdapter((Context)localObject3, 2130968593, ((SharedPreferences)localObject4).getInt("viewsize", 0), listAppsFragment.boot_pat);
      listAppsFragment.adapter_boot.sorter = new byPkgName();
      try
      {
        listAppsFragment.plia.notifyDataSetChanged();
        listAppsFragment.adapter_boot.notifyDataSetChanged();
        ((AlertDlg)localObject2).setTitle(Utils.getText(2131165407));
        ((AlertDlg)localObject2).setAdapter(listAppsFragment.adapter_boot, new AdapterView.OnItemClickListener()
        {
          public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
          {
            paramAnonymousAdapterView = (PkgListItem)listAppsFragment.adapter_boot.getItem(paramAnonymousInt);
            if (paramAnonymousAdapterView.pkgName.equals(Utils.getText(2131165754))) {
              Utils.turn_off_patch_on_boot_all();
            }
            for (;;)
            {
              listAppsFragment.plia.notifyDataSetChanged();
              System.out.println("asd1");
              paramAnonymousAdapterView = listAppsFragment.frag;
              listAppsFragment.showDialogLP(3);
              return;
              try
              {
                listAppsFragment.getPkgMng().getPackageInfo(paramAnonymousAdapterView.pkgName, 0);
                paramAnonymousInt = 0;
                while (paramAnonymousInt < listAppsFragment.plia.data.length)
                {
                  if (listAppsFragment.plia.data[paramAnonymousInt].pkgName.equals(paramAnonymousAdapterView.pkgName))
                  {
                    listAppsFragment.plia.data[paramAnonymousInt].boot_ads = false;
                    listAppsFragment.plia.data[paramAnonymousInt].boot_lvl = false;
                    if (listAppsFragment.plia.data[paramAnonymousInt].boot_custom)
                    {
                      if (new File(listAppsFragment.frag.getContext().getDir("bootlist", 0) + "/" + listAppsFragment.plia.data[paramAnonymousInt].pkgName).exists()) {
                        new File(listAppsFragment.frag.getContext().getDir("bootlist", 0) + "/" + listAppsFragment.plia.data[paramAnonymousInt].pkgName).delete();
                      }
                      listAppsFragment.plia.data[paramAnonymousInt].boot_custom = false;
                    }
                    listAppsFragment.plia.data[paramAnonymousInt].boot_manual = false;
                    listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.data[paramAnonymousInt]);
                  }
                  paramAnonymousInt += 1;
                }
              }
              catch (PackageManager.NameNotFoundException paramAnonymousView)
              {
                if (new File(listAppsFragment.frag.getContext().getDir("bootlist", 0) + "/" + paramAnonymousAdapterView.pkgName).exists()) {
                  new File(listAppsFragment.frag.getContext().getDir("bootlist", 0) + "/" + paramAnonymousAdapterView.pkgName).delete();
                }
                new StringBuilder();
                paramAnonymousAdapterView = new StringBuilder();
                paramAnonymousInt = 0;
                while (paramAnonymousInt < listAppsFragment.plia.data.length)
                {
                  if ((listAppsFragment.plia.data[paramAnonymousInt].boot_ads) || (listAppsFragment.plia.data[paramAnonymousInt].boot_custom) || (listAppsFragment.plia.data[paramAnonymousInt].boot_lvl) || (listAppsFragment.plia.data[paramAnonymousInt].boot_manual))
                  {
                    paramAnonymousAdapterView.append(listAppsFragment.plia.data[paramAnonymousInt].pkgName);
                    if (listAppsFragment.plia.data[paramAnonymousInt].boot_ads) {
                      paramAnonymousAdapterView.append("%ads");
                    }
                    if (listAppsFragment.plia.data[paramAnonymousInt].boot_lvl) {
                      paramAnonymousAdapterView.append("%lvl");
                    }
                    if (listAppsFragment.plia.data[paramAnonymousInt].boot_custom) {
                      paramAnonymousAdapterView.append("%custom");
                    }
                    if (listAppsFragment.plia.data[paramAnonymousInt].boot_manual) {
                      paramAnonymousAdapterView.append("%object");
                    }
                    paramAnonymousAdapterView.append(",");
                  }
                  paramAnonymousInt += 1;
                }
                paramAnonymousAdapterView.append("\n");
                try
                {
                  paramAnonymousView = new FileOutputStream(new File(listAppsFragment.frag.getContext().getDir("bootlist", 0) + "/bootlist"));
                  paramAnonymousView.write(paramAnonymousAdapterView.toString().getBytes());
                  paramAnonymousView.close();
                }
                catch (FileNotFoundException paramAnonymousAdapterView) {}catch (Exception paramAnonymousAdapterView) {}
              }
            }
          }
        });
        System.out.println("asd2");
        return ((AlertDlg)localObject2).create();
      }
      catch (Exception localException2)
      {
        for (;;)
        {
          System.out.println("LuckyPatcher(Bootlist dialog): " + localException2);
        }
      }
      AlertDlg localAlertDlg = new AlertDlg(listAppsFragment.frag.getContext());
      if (listAppsFragment.dialog_int == 345350) {}
      for (localObject2 = Utils.getText(2131165656);; localObject2 = Utils.getText(2131165435))
      {
        localAlertDlg.setTitle(Utils.getText(2131165748)).setIcon(2130837550).setMessage((String)localObject2).setPositiveButton(Utils.getText(2131165187), new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            new AsyncTask()
            {
              protected Boolean doInBackground(Void... paramAnonymous2VarArgs)
              {
                return Boolean.valueOf(listAppsFragment.mLogCollector.collect(listAppsFragment.frag.getContext(), true));
              }
              
              protected void onPostExecute(Boolean paramAnonymous2Boolean)
              {
                listAppsFragment locallistAppsFragment = listAppsFragment.frag;
                listAppsFragment.removeDialogLP(3255);
                if (paramAnonymous2Boolean.booleanValue()) {
                  try
                  {
                    listAppsFragment.mLogCollector.sendLog(listAppsFragment.frag.getContext(), "lp.chelpus@gmail.com", "Error Log", "Lucky Patcher " + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.frag.getContext().getPackageName(), 0).versionName);
                    return;
                  }
                  catch (PackageManager.NameNotFoundException paramAnonymous2Boolean)
                  {
                    paramAnonymous2Boolean.printStackTrace();
                    return;
                  }
                }
                paramAnonymous2Boolean = listAppsFragment.frag;
                listAppsFragment.showDialogLP(3535122);
              }
              
              protected void onPreExecute()
              {
                listAppsFragment locallistAppsFragment = listAppsFragment.frag;
                listAppsFragment.showDialogLP(3255);
              }
            }.execute(new Void[0]);
          }
        }).setNegativeButton(Utils.getText(2131165563), new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {}
        });
        return localAlertDlg.create();
      }
      localObject2 = new ProgressDialog(listAppsFragment.frag.getContext());
      ((ProgressDialog)localObject2).setTitle("Progress");
      ((ProgressDialog)localObject2).setMessage(Utils.getText(2131165255));
      ((ProgressDialog)localObject2).setIndeterminate(true);
      return (Dialog)localObject2;
      localObject2 = new AlertDlg(listAppsFragment.frag.getContext());
      ((AlertDlg)localObject2).setTitle("Error").setMessage(Utils.getText(2131165433)).setNegativeButton("OK", null);
      localObject2 = ((AlertDlg)localObject2).create();
      return (Dialog)localObject2;
      return null;
      label3157:
      j = -16711936;
    }
  }
  
  public void showDialog()
  {
    if (this.dialog == null) {
      this.dialog = onCreateDialog();
    }
    if (this.dialog != null) {
      this.dialog.show();
    }
  }
  
  public class byPkgName
    implements Comparator<PkgListItem>
  {
    public byPkgName() {}
    
    public int compare(PkgListItem paramPkgListItem1, PkgListItem paramPkgListItem2)
    {
      if ((paramPkgListItem1 == null) || (paramPkgListItem2 == null)) {
        throw new ClassCastException();
      }
      if ((paramPkgListItem1.stored == 0) || (paramPkgListItem2.stored == 0))
      {
        int i = Integer.valueOf(paramPkgListItem1.stored).compareTo(Integer.valueOf(paramPkgListItem2.stored));
        if (i == 0) {
          return paramPkgListItem1.toString().compareToIgnoreCase(paramPkgListItem2.toString());
        }
        return i;
      }
      return paramPkgListItem1.toString().compareToIgnoreCase(paramPkgListItem2.toString());
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */