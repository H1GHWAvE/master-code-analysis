package com.android.vending.billing.InAppBillingService.LUCK;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

public class DatabaseHelper
  extends SQLiteOpenHelper
{
  static final String ads = "ads";
  static final String billing = "billing";
  static final String boot_ads = "boot_ads";
  static final String boot_custom = "boot_custom";
  static final String boot_lvl = "boot_lvl";
  static final String boot_manual = "boot_manual";
  public static Context contextdb = null;
  static final String custom = "custom";
  public static SQLiteDatabase db = null;
  static final String dbName = "PackagesDB";
  public static boolean getPackage = false;
  static final String hidden = "hidden";
  static final String icon = "icon";
  static final String lvl = "lvl";
  static final String modified = "modified";
  static final String odex = "odex";
  static final String packagesTable = "Packages";
  static final String pkgLabel = "pkgLabel";
  static final String pkgName = "pkgName";
  public static boolean savePackage = false;
  static final String statusi = "statusi";
  static final String stored = "stored";
  static final String storepref = "storepref";
  static final String system = "system";
  static final String updatetime = "updatetime";
  
  public DatabaseHelper(Context paramContext)
  {
    super(paramContext, "PackagesDB", null, 42);
    contextdb = paramContext;
    try
    {
      db = getWritableDatabase();
      System.out.println("SQLite base version is " + db.getVersion());
      if (db.getVersion() != 42)
      {
        System.out.println("SQL delete and recreate.");
        db.execSQL("DROP TABLE IF EXISTS Packages");
        onCreate(db);
      }
      return;
    }
    catch (SQLiteException paramContext)
    {
      paramContext.printStackTrace();
    }
  }
  
  public void deletePackage(PkgListItem paramPkgListItem)
  {
    try
    {
      db.delete("Packages", "pkgName = '" + paramPkgListItem.pkgName + "'", null);
      return;
    }
    catch (Exception paramPkgListItem)
    {
      System.out.println("LuckyPatcher-Error: deletePackage " + paramPkgListItem);
    }
  }
  
  public void deletePackage(String paramString)
  {
    try
    {
      db.delete("Packages", "pkgName = '" + paramString + "'", null);
      return;
    }
    catch (Exception paramString)
    {
      System.out.println("LuckyPatcher-Error: deletePackage " + paramString);
    }
  }
  
  /* Error */
  public java.util.ArrayList<PkgListItem> getPackage(boolean paramBoolean1, boolean paramBoolean2)
  {
    // Byte code:
    //   0: new 158	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 159	java/util/ArrayList:<init>	()V
    //   7: astore 22
    //   9: aload 22
    //   11: invokevirtual 162	java/util/ArrayList:clear	()V
    //   14: invokestatic 168	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getPkgMng	()Landroid/content/pm/PackageManager;
    //   17: astore 24
    //   19: iconst_1
    //   20: putstatic 65	com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper:getPackage	Z
    //   23: getstatic 63	com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper:db	Landroid/database/sqlite/SQLiteDatabase;
    //   26: ldc 42
    //   28: bipush 19
    //   30: anewarray 170	java/lang/String
    //   33: dup
    //   34: iconst_0
    //   35: ldc 46
    //   37: aastore
    //   38: dup
    //   39: iconst_1
    //   40: ldc 44
    //   42: aastore
    //   43: dup
    //   44: iconst_2
    //   45: ldc 51
    //   47: aastore
    //   48: dup
    //   49: iconst_3
    //   50: ldc 53
    //   52: aastore
    //   53: dup
    //   54: iconst_4
    //   55: ldc 31
    //   57: aastore
    //   58: dup
    //   59: iconst_5
    //   60: ldc 49
    //   62: aastore
    //   63: dup
    //   64: bipush 6
    //   66: ldc 11
    //   68: aastore
    //   69: dup
    //   70: bipush 7
    //   72: ldc 15
    //   74: aastore
    //   75: dup
    //   76: bipush 8
    //   78: ldc 13
    //   80: aastore
    //   81: dup
    //   82: bipush 9
    //   84: ldc 17
    //   86: aastore
    //   87: dup
    //   88: bipush 10
    //   90: ldc 21
    //   92: aastore
    //   93: dup
    //   94: bipush 11
    //   96: ldc 35
    //   98: aastore
    //   99: dup
    //   100: bipush 12
    //   102: ldc 7
    //   104: aastore
    //   105: dup
    //   106: bipush 13
    //   108: ldc 37
    //   110: aastore
    //   111: dup
    //   112: bipush 14
    //   114: ldc 55
    //   116: aastore
    //   117: dup
    //   118: bipush 15
    //   120: ldc 39
    //   122: aastore
    //   123: dup
    //   124: bipush 16
    //   126: ldc 33
    //   128: aastore
    //   129: dup
    //   130: bipush 17
    //   132: ldc 57
    //   134: aastore
    //   135: dup
    //   136: bipush 18
    //   138: ldc 9
    //   140: aastore
    //   141: aconst_null
    //   142: aconst_null
    //   143: aconst_null
    //   144: aconst_null
    //   145: aconst_null
    //   146: invokevirtual 174	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   149: astore 23
    //   151: aload 23
    //   153: invokeinterface 180 1 0
    //   158: pop
    //   159: aload 23
    //   161: aload 23
    //   163: ldc 46
    //   165: invokeinterface 184 2 0
    //   170: invokeinterface 188 2 0
    //   175: astore 25
    //   177: aload 24
    //   179: aload 25
    //   181: iconst_0
    //   182: invokevirtual 194	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    //   185: getfield 200	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
    //   188: astore 26
    //   190: aload 26
    //   192: getfield 205	android/content/pm/ApplicationInfo:enabled	Z
    //   195: istore 19
    //   197: aload 26
    //   199: getfield 209	android/content/pm/ApplicationInfo:flags	I
    //   202: ldc -46
    //   204: iand
    //   205: ldc -46
    //   207: if_icmpne +559 -> 766
    //   210: iconst_1
    //   211: istore 18
    //   213: aload 23
    //   215: aload 23
    //   217: ldc 44
    //   219: invokeinterface 213 2 0
    //   224: invokeinterface 188 2 0
    //   229: astore 27
    //   231: aload 23
    //   233: aload 23
    //   235: ldc 51
    //   237: invokeinterface 213 2 0
    //   242: invokeinterface 217 2 0
    //   247: istore_3
    //   248: aload 23
    //   250: aload 23
    //   252: ldc 53
    //   254: invokeinterface 213 2 0
    //   259: invokeinterface 217 2 0
    //   264: istore 5
    //   266: aload 23
    //   268: aload 23
    //   270: ldc 31
    //   272: invokeinterface 213 2 0
    //   277: invokeinterface 217 2 0
    //   282: istore 6
    //   284: aload 23
    //   286: aload 23
    //   288: ldc 49
    //   290: invokeinterface 213 2 0
    //   295: invokeinterface 188 2 0
    //   300: astore 28
    //   302: aload 23
    //   304: aload 23
    //   306: ldc 11
    //   308: invokeinterface 213 2 0
    //   313: invokeinterface 217 2 0
    //   318: istore 7
    //   320: aload 23
    //   322: aload 23
    //   324: ldc 15
    //   326: invokeinterface 213 2 0
    //   331: invokeinterface 217 2 0
    //   336: istore 8
    //   338: aload 23
    //   340: aload 23
    //   342: ldc 13
    //   344: invokeinterface 213 2 0
    //   349: invokeinterface 217 2 0
    //   354: istore 9
    //   356: aload 23
    //   358: aload 23
    //   360: ldc 17
    //   362: invokeinterface 213 2 0
    //   367: invokeinterface 217 2 0
    //   372: istore 10
    //   374: aload 23
    //   376: aload 23
    //   378: ldc 21
    //   380: invokeinterface 213 2 0
    //   385: invokeinterface 217 2 0
    //   390: istore 11
    //   392: aload 23
    //   394: aload 23
    //   396: ldc 35
    //   398: invokeinterface 213 2 0
    //   403: invokeinterface 217 2 0
    //   408: istore 12
    //   410: aload 23
    //   412: aload 23
    //   414: ldc 7
    //   416: invokeinterface 213 2 0
    //   421: invokeinterface 217 2 0
    //   426: istore 13
    //   428: aload 23
    //   430: aload 23
    //   432: ldc 37
    //   434: invokeinterface 213 2 0
    //   439: invokeinterface 217 2 0
    //   444: istore 14
    //   446: aload 23
    //   448: aload 23
    //   450: ldc 55
    //   452: invokeinterface 213 2 0
    //   457: invokeinterface 217 2 0
    //   462: istore 15
    //   464: aload 23
    //   466: aload 23
    //   468: ldc 39
    //   470: invokeinterface 213 2 0
    //   475: invokeinterface 217 2 0
    //   480: istore 16
    //   482: aload 23
    //   484: aload 23
    //   486: ldc 9
    //   488: invokeinterface 213 2 0
    //   493: invokeinterface 217 2 0
    //   498: istore 17
    //   500: aconst_null
    //   501: astore 21
    //   503: aload 21
    //   505: astore 20
    //   507: iload_2
    //   508: ifne +44 -> 552
    //   511: aload 23
    //   513: aload 23
    //   515: ldc 33
    //   517: invokeinterface 184 2 0
    //   522: invokeinterface 221 2 0
    //   527: astore 29
    //   529: aload 21
    //   531: astore 20
    //   533: aload 29
    //   535: ifnull +17 -> 552
    //   538: new 223	java/io/ByteArrayInputStream
    //   541: dup
    //   542: aload 29
    //   544: invokespecial 226	java/io/ByteArrayInputStream:<init>	([B)V
    //   547: invokestatic 232	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    //   550: astore 20
    //   552: aload 23
    //   554: aload 23
    //   556: ldc 57
    //   558: invokeinterface 213 2 0
    //   563: invokeinterface 217 2 0
    //   568: istore 4
    //   570: iload_3
    //   571: ifeq +215 -> 786
    //   574: invokestatic 236	java/lang/System:currentTimeMillis	()J
    //   577: ldc2_w 237
    //   580: ldiv
    //   581: l2i
    //   582: iload 4
    //   584: isub
    //   585: invokestatic 243	java/lang/Math:abs	(I)I
    //   588: ldc -12
    //   590: getstatic 247	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:days	I
    //   593: imul
    //   594: if_icmpge +36 -> 630
    //   597: iconst_0
    //   598: istore_3
    //   599: getstatic 63	com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper:db	Landroid/database/sqlite/SQLiteDatabase;
    //   602: new 87	java/lang/StringBuilder
    //   605: dup
    //   606: invokespecial 89	java/lang/StringBuilder:<init>	()V
    //   609: ldc -7
    //   611: invokevirtual 95	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   614: aload 25
    //   616: invokevirtual 95	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   619: ldc -116
    //   621: invokevirtual 95	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   624: invokevirtual 108	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   627: invokevirtual 121	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
    //   630: new 136	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem
    //   633: dup
    //   634: invokestatic 253	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getInstance	()Landroid/content/Context;
    //   637: aload 25
    //   639: aload 27
    //   641: iload_3
    //   642: iload 5
    //   644: iload 6
    //   646: aload 28
    //   648: iload 7
    //   650: iload 8
    //   652: iload 9
    //   654: iload 10
    //   656: iload 11
    //   658: iload 12
    //   660: iload 13
    //   662: iload 14
    //   664: iload 15
    //   666: iload 16
    //   668: aload 20
    //   670: iload 4
    //   672: iload 17
    //   674: iload 19
    //   676: iload 18
    //   678: iload_2
    //   679: invokespecial 256	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:<init>	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;IIIIIIIIIILandroid/graphics/Bitmap;IIZZZ)V
    //   682: astore 20
    //   684: invokestatic 260	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getConfig	()Landroid/content/SharedPreferences;
    //   687: ldc_w 262
    //   690: iconst_0
    //   691: invokeinterface 268 3 0
    //   696: istore 18
    //   698: iload_1
    //   699: ifne +678 -> 1377
    //   702: aload 25
    //   704: ldc_w 270
    //   707: invokevirtual 274	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   710: ifne +19 -> 729
    //   713: aload 25
    //   715: ldc -92
    //   717: invokevirtual 279	java/lang/Class:getPackage	()Ljava/lang/Package;
    //   720: invokevirtual 284	java/lang/Package:getName	()Ljava/lang/String;
    //   723: invokevirtual 274	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   726: ifeq +167 -> 893
    //   729: new 154	java/lang/IllegalArgumentException
    //   732: dup
    //   733: ldc_w 286
    //   736: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   739: athrow
    //   740: astore 20
    //   742: aload 23
    //   744: invokeinterface 291 1 0
    //   749: ifne -590 -> 159
    //   752: aload 23
    //   754: invokeinterface 294 1 0
    //   759: iconst_0
    //   760: putstatic 65	com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper:getPackage	Z
    //   763: aload 22
    //   765: areturn
    //   766: iconst_0
    //   767: istore 18
    //   769: goto -556 -> 213
    //   772: astore 20
    //   774: aload 20
    //   776: invokevirtual 295	java/lang/Exception:printStackTrace	()V
    //   779: aload 21
    //   781: astore 20
    //   783: goto -231 -> 552
    //   786: invokestatic 236	java/lang/System:currentTimeMillis	()J
    //   789: ldc2_w 237
    //   792: ldiv
    //   793: l2i
    //   794: iload 4
    //   796: isub
    //   797: invokestatic 243	java/lang/Math:abs	(I)I
    //   800: ldc -12
    //   802: getstatic 247	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:days	I
    //   805: imul
    //   806: if_icmple +30 -> 836
    //   809: new 136	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem
    //   812: dup
    //   813: invokestatic 253	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getInstance	()Landroid/content/Context;
    //   816: aload 25
    //   818: getstatic 247	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:days	I
    //   821: iload_2
    //   822: invokespecial 298	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:<init>	(Landroid/content/Context;Ljava/lang/String;IZ)V
    //   825: astore 20
    //   827: aload_0
    //   828: aload 20
    //   830: invokevirtual 300	com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper:savePackage	(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V
    //   833: goto -149 -> 684
    //   836: new 136	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem
    //   839: dup
    //   840: invokestatic 253	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getInstance	()Landroid/content/Context;
    //   843: aload 25
    //   845: aload 27
    //   847: iload_3
    //   848: iload 5
    //   850: iload 6
    //   852: aload 28
    //   854: iload 7
    //   856: iload 8
    //   858: iload 9
    //   860: iload 10
    //   862: iload 11
    //   864: iload 12
    //   866: iload 13
    //   868: iload 14
    //   870: iload 15
    //   872: iload 16
    //   874: aload 20
    //   876: iload 4
    //   878: iload 17
    //   880: iload 19
    //   882: iload 18
    //   884: iload_2
    //   885: invokespecial 256	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:<init>	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;IIIIIIIIIILandroid/graphics/Bitmap;IIZZZ)V
    //   888: astore 20
    //   890: goto -206 -> 684
    //   893: aload 20
    //   895: getfield 302	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:system	Z
    //   898: ifeq +27 -> 925
    //   901: iload 18
    //   903: ifne +22 -> 925
    //   906: aload 20
    //   908: getfield 304	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:custom	Z
    //   911: ifne +14 -> 925
    //   914: new 154	java/lang/IllegalArgumentException
    //   917: dup
    //   918: ldc_w 306
    //   921: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   924: athrow
    //   925: invokestatic 260	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getConfig	()Landroid/content/SharedPreferences;
    //   928: ldc_w 308
    //   931: iconst_1
    //   932: invokeinterface 268 3 0
    //   937: ifne +54 -> 991
    //   940: aload 20
    //   942: getfield 310	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:lvl	Z
    //   945: ifeq +46 -> 991
    //   948: aload 20
    //   950: getfield 312	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:ads	Z
    //   953: ifne +38 -> 991
    //   956: aload 20
    //   958: getfield 304	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:custom	Z
    //   961: ifne +30 -> 991
    //   964: aload 20
    //   966: getfield 314	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:odex	Z
    //   969: ifne +22 -> 991
    //   972: aload 20
    //   974: getfield 316	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:modified	Z
    //   977: ifne +14 -> 991
    //   980: new 154	java/lang/IllegalArgumentException
    //   983: dup
    //   984: ldc_w 286
    //   987: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   990: athrow
    //   991: invokestatic 260	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getConfig	()Landroid/content/SharedPreferences;
    //   994: ldc_w 318
    //   997: iconst_1
    //   998: invokeinterface 268 3 0
    //   1003: ifne +54 -> 1057
    //   1006: aload 20
    //   1008: getfield 312	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:ads	Z
    //   1011: ifeq +46 -> 1057
    //   1014: aload 20
    //   1016: getfield 310	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:lvl	Z
    //   1019: ifne +38 -> 1057
    //   1022: aload 20
    //   1024: getfield 304	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:custom	Z
    //   1027: ifne +30 -> 1057
    //   1030: aload 20
    //   1032: getfield 314	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:odex	Z
    //   1035: ifne +22 -> 1057
    //   1038: aload 20
    //   1040: getfield 316	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:modified	Z
    //   1043: ifne +14 -> 1057
    //   1046: new 154	java/lang/IllegalArgumentException
    //   1049: dup
    //   1050: ldc_w 286
    //   1053: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1056: athrow
    //   1057: invokestatic 260	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getConfig	()Landroid/content/SharedPreferences;
    //   1060: ldc_w 308
    //   1063: iconst_1
    //   1064: invokeinterface 268 3 0
    //   1069: ifne +69 -> 1138
    //   1072: invokestatic 260	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getConfig	()Landroid/content/SharedPreferences;
    //   1075: ldc_w 318
    //   1078: iconst_1
    //   1079: invokeinterface 268 3 0
    //   1084: ifne +54 -> 1138
    //   1087: aload 20
    //   1089: getfield 310	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:lvl	Z
    //   1092: ifeq +46 -> 1138
    //   1095: aload 20
    //   1097: getfield 312	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:ads	Z
    //   1100: ifeq +38 -> 1138
    //   1103: aload 20
    //   1105: getfield 304	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:custom	Z
    //   1108: ifne +30 -> 1138
    //   1111: aload 20
    //   1113: getfield 314	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:odex	Z
    //   1116: ifne +22 -> 1138
    //   1119: aload 20
    //   1121: getfield 316	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:modified	Z
    //   1124: ifne +14 -> 1138
    //   1127: new 154	java/lang/IllegalArgumentException
    //   1130: dup
    //   1131: ldc_w 286
    //   1134: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1137: athrow
    //   1138: invokestatic 260	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getConfig	()Landroid/content/SharedPreferences;
    //   1141: ldc_w 320
    //   1144: iconst_1
    //   1145: invokeinterface 268 3 0
    //   1150: ifne +36 -> 1186
    //   1153: iload_3
    //   1154: bipush 6
    //   1156: if_icmpne +30 -> 1186
    //   1159: aload 20
    //   1161: getfield 314	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:odex	Z
    //   1164: ifne +22 -> 1186
    //   1167: aload 20
    //   1169: getfield 316	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:modified	Z
    //   1172: ifne +14 -> 1186
    //   1175: new 154	java/lang/IllegalArgumentException
    //   1178: dup
    //   1179: ldc_w 286
    //   1182: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1185: athrow
    //   1186: invokestatic 260	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getConfig	()Landroid/content/SharedPreferences;
    //   1189: ldc_w 322
    //   1192: iconst_1
    //   1193: invokeinterface 268 3 0
    //   1198: ifne +38 -> 1236
    //   1201: aload 20
    //   1203: getfield 304	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:custom	Z
    //   1206: ifeq +30 -> 1236
    //   1209: aload 20
    //   1211: getfield 314	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:odex	Z
    //   1214: ifne +22 -> 1236
    //   1217: aload 20
    //   1219: getfield 316	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:modified	Z
    //   1222: ifne +14 -> 1236
    //   1225: new 154	java/lang/IllegalArgumentException
    //   1228: dup
    //   1229: ldc_w 286
    //   1232: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1235: athrow
    //   1236: invokestatic 260	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getConfig	()Landroid/content/SharedPreferences;
    //   1239: ldc_w 324
    //   1242: iconst_1
    //   1243: invokeinterface 268 3 0
    //   1248: ifne +30 -> 1278
    //   1251: aload 20
    //   1253: getfield 314	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:odex	Z
    //   1256: ifeq +22 -> 1278
    //   1259: aload 20
    //   1261: getfield 316	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:modified	Z
    //   1264: ifne +14 -> 1278
    //   1267: new 154	java/lang/IllegalArgumentException
    //   1270: dup
    //   1271: ldc_w 286
    //   1274: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1277: athrow
    //   1278: invokestatic 260	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getConfig	()Landroid/content/SharedPreferences;
    //   1281: ldc_w 326
    //   1284: iconst_1
    //   1285: invokeinterface 268 3 0
    //   1290: ifne +30 -> 1320
    //   1293: aload 20
    //   1295: getfield 314	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:odex	Z
    //   1298: ifne +22 -> 1320
    //   1301: aload 20
    //   1303: getfield 316	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:modified	Z
    //   1306: ifeq +14 -> 1320
    //   1309: new 154	java/lang/IllegalArgumentException
    //   1312: dup
    //   1313: ldc_w 286
    //   1316: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1319: athrow
    //   1320: invokestatic 260	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getConfig	()Landroid/content/SharedPreferences;
    //   1323: ldc_w 324
    //   1326: iconst_1
    //   1327: invokeinterface 268 3 0
    //   1332: ifne +244 -> 1576
    //   1335: invokestatic 260	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getConfig	()Landroid/content/SharedPreferences;
    //   1338: ldc_w 326
    //   1341: iconst_1
    //   1342: invokeinterface 268 3 0
    //   1347: ifne +229 -> 1576
    //   1350: aload 20
    //   1352: getfield 314	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:odex	Z
    //   1355: ifne +11 -> 1366
    //   1358: aload 20
    //   1360: getfield 316	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:modified	Z
    //   1363: ifeq +213 -> 1576
    //   1366: new 154	java/lang/IllegalArgumentException
    //   1369: dup
    //   1370: ldc_w 286
    //   1373: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1376: athrow
    //   1377: aload 25
    //   1379: ldc_w 270
    //   1382: invokevirtual 274	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1385: ifne +17 -> 1402
    //   1388: aload 25
    //   1390: getstatic 61	com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper:contextdb	Landroid/content/Context;
    //   1393: invokevirtual 331	android/content/Context:getPackageName	()Ljava/lang/String;
    //   1396: invokevirtual 274	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1399: ifeq +14 -> 1413
    //   1402: new 154	java/lang/IllegalArgumentException
    //   1405: dup
    //   1406: ldc_w 286
    //   1409: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1412: athrow
    //   1413: getstatic 334	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:advancedFilter	I
    //   1416: ifeq +160 -> 1576
    //   1419: getstatic 334	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:advancedFilter	I
    //   1422: lookupswitch	default:+813->2235, 1929:+366->1788, 1930:+462->1884, 1931:+481->1903, 1932:+544->1966, 2131558415:+165->1587, 2131558416:+172->1594, 2131558417:+202->1624, 2131558418:+221->1643, 2131558419:+240->1662, 2131558420:+259->1681, 2131558421:+278->1700, 2131558422:+297->1719, 2131558423:+316->1738, 2131558424:+341->1763, 2131558425:+728->2150, 2131558426:+396->1818, 2131558427:+415->1837, 2131558428:+630->2052
    //   1576: aload 22
    //   1578: aload 20
    //   1580: invokevirtual 337	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1583: pop
    //   1584: goto -842 -> 742
    //   1587: iconst_0
    //   1588: putstatic 334	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:advancedFilter	I
    //   1591: goto -15 -> 1576
    //   1594: invokestatic 236	java/lang/System:currentTimeMillis	()J
    //   1597: ldc2_w 237
    //   1600: ldiv
    //   1601: l2i
    //   1602: iload 4
    //   1604: isub
    //   1605: invokestatic 243	java/lang/Math:abs	(I)I
    //   1608: ldc -12
    //   1610: if_icmple -34 -> 1576
    //   1613: new 154	java/lang/IllegalArgumentException
    //   1616: dup
    //   1617: ldc_w 286
    //   1620: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1623: athrow
    //   1624: aload 20
    //   1626: getfield 310	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:lvl	Z
    //   1629: ifne -53 -> 1576
    //   1632: new 154	java/lang/IllegalArgumentException
    //   1635: dup
    //   1636: ldc_w 286
    //   1639: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1642: athrow
    //   1643: aload 20
    //   1645: getfield 312	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:ads	Z
    //   1648: ifne -72 -> 1576
    //   1651: new 154	java/lang/IllegalArgumentException
    //   1654: dup
    //   1655: ldc_w 286
    //   1658: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1661: athrow
    //   1662: aload 20
    //   1664: getfield 304	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:custom	Z
    //   1667: ifne -91 -> 1576
    //   1670: new 154	java/lang/IllegalArgumentException
    //   1673: dup
    //   1674: ldc_w 286
    //   1677: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1680: athrow
    //   1681: aload 20
    //   1683: getfield 316	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:modified	Z
    //   1686: ifne -110 -> 1576
    //   1689: new 154	java/lang/IllegalArgumentException
    //   1692: dup
    //   1693: ldc_w 286
    //   1696: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1699: athrow
    //   1700: aload 20
    //   1702: getfield 314	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:odex	Z
    //   1705: ifne -129 -> 1576
    //   1708: new 154	java/lang/IllegalArgumentException
    //   1711: dup
    //   1712: ldc_w 286
    //   1715: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1718: athrow
    //   1719: aload 20
    //   1721: getfield 339	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:billing	Z
    //   1724: ifne -148 -> 1576
    //   1727: new 154	java/lang/IllegalArgumentException
    //   1730: dup
    //   1731: ldc_w 286
    //   1734: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1737: athrow
    //   1738: aload 26
    //   1740: getfield 342	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
    //   1743: ldc_w 344
    //   1746: invokevirtual 348	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   1749: ifne -173 -> 1576
    //   1752: new 154	java/lang/IllegalArgumentException
    //   1755: dup
    //   1756: ldc_w 286
    //   1759: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1762: athrow
    //   1763: aload 26
    //   1765: getfield 342	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
    //   1768: ldc_w 350
    //   1771: invokevirtual 348	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   1774: ifne -198 -> 1576
    //   1777: new 154	java/lang/IllegalArgumentException
    //   1780: dup
    //   1781: ldc_w 286
    //   1784: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1787: athrow
    //   1788: aload 20
    //   1790: getfield 302	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:system	Z
    //   1793: ifne +14 -> 1807
    //   1796: aload 20
    //   1798: getfield 138	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
    //   1801: invokestatic 355	com/chelpus/Utils:isInstalledOnSdCard	(Ljava/lang/String;)Z
    //   1804: ifeq -228 -> 1576
    //   1807: new 154	java/lang/IllegalArgumentException
    //   1810: dup
    //   1811: ldc_w 286
    //   1814: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1817: athrow
    //   1818: aload 20
    //   1820: getfield 302	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:system	Z
    //   1823: ifne -247 -> 1576
    //   1826: new 154	java/lang/IllegalArgumentException
    //   1829: dup
    //   1830: ldc_w 286
    //   1833: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1836: athrow
    //   1837: aload 20
    //   1839: getfield 302	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:system	Z
    //   1842: ifeq +31 -> 1873
    //   1845: aload 26
    //   1847: getfield 342	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
    //   1850: ldc_w 344
    //   1853: invokevirtual 348	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   1856: ifne -280 -> 1576
    //   1859: aload 26
    //   1861: getfield 342	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
    //   1864: ldc_w 350
    //   1867: invokevirtual 348	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   1870: ifne -294 -> 1576
    //   1873: new 154	java/lang/IllegalArgumentException
    //   1876: dup
    //   1877: ldc_w 286
    //   1880: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1883: athrow
    //   1884: aload 20
    //   1886: getfield 302	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:system	Z
    //   1889: ifeq -313 -> 1576
    //   1892: new 154	java/lang/IllegalArgumentException
    //   1895: dup
    //   1896: ldc_w 286
    //   1899: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1902: athrow
    //   1903: aload 24
    //   1905: ldc_w 357
    //   1908: aload 20
    //   1910: getfield 138	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
    //   1913: invokevirtual 361	android/content/pm/PackageManager:checkPermission	(Ljava/lang/String;Ljava/lang/String;)I
    //   1916: iconst_m1
    //   1917: if_icmpne +14 -> 1931
    //   1920: new 154	java/lang/IllegalArgumentException
    //   1923: dup
    //   1924: ldc_w 286
    //   1927: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1930: athrow
    //   1931: aload 24
    //   1933: new 363	android/content/ComponentName
    //   1936: dup
    //   1937: aload 20
    //   1939: getfield 138	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
    //   1942: ldc_w 357
    //   1945: invokespecial 366	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   1948: invokevirtual 370	android/content/pm/PackageManager:getComponentEnabledSetting	(Landroid/content/ComponentName;)I
    //   1951: iconst_2
    //   1952: if_icmpne -376 -> 1576
    //   1955: new 154	java/lang/IllegalArgumentException
    //   1958: dup
    //   1959: ldc_w 286
    //   1962: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1965: athrow
    //   1966: aload 24
    //   1968: ldc_w 357
    //   1971: aload 20
    //   1973: getfield 138	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
    //   1976: invokevirtual 361	android/content/pm/PackageManager:checkPermission	(Ljava/lang/String;Ljava/lang/String;)I
    //   1979: iconst_m1
    //   1980: if_icmpne +14 -> 1994
    //   1983: new 154	java/lang/IllegalArgumentException
    //   1986: dup
    //   1987: ldc_w 286
    //   1990: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   1993: athrow
    //   1994: aload 24
    //   1996: new 363	android/content/ComponentName
    //   1999: dup
    //   2000: aload 20
    //   2002: getfield 138	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
    //   2005: ldc_w 357
    //   2008: invokespecial 366	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   2011: invokevirtual 370	android/content/pm/PackageManager:getComponentEnabledSetting	(Landroid/content/ComponentName;)I
    //   2014: iconst_1
    //   2015: if_icmpeq +26 -> 2041
    //   2018: aload 24
    //   2020: new 363	android/content/ComponentName
    //   2023: dup
    //   2024: aload 20
    //   2026: getfield 138	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
    //   2029: ldc_w 357
    //   2032: invokespecial 366	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   2035: invokevirtual 370	android/content/pm/PackageManager:getComponentEnabledSetting	(Landroid/content/ComponentName;)I
    //   2038: ifne -462 -> 1576
    //   2041: new 154	java/lang/IllegalArgumentException
    //   2044: dup
    //   2045: ldc_w 286
    //   2048: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   2051: athrow
    //   2052: invokestatic 168	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getPkgMng	()Landroid/content/pm/PackageManager;
    //   2055: aload 20
    //   2057: getfield 138	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
    //   2060: sipush 4096
    //   2063: invokevirtual 194	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    //   2066: getfield 374	android/content/pm/PackageInfo:requestedPermissions	[Ljava/lang/String;
    //   2069: astore 21
    //   2071: iconst_0
    //   2072: istore 5
    //   2074: iconst_0
    //   2075: istore_3
    //   2076: aload 21
    //   2078: ifnull +56 -> 2134
    //   2081: aload 21
    //   2083: arraylength
    //   2084: istore 6
    //   2086: iconst_0
    //   2087: istore 4
    //   2089: iload_3
    //   2090: istore 5
    //   2092: iload 4
    //   2094: iload 6
    //   2096: if_icmpge +38 -> 2134
    //   2099: aload 21
    //   2101: iload 4
    //   2103: aaload
    //   2104: astore 25
    //   2106: aload 25
    //   2108: invokevirtual 377	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   2111: ldc_w 379
    //   2114: invokevirtual 383	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2117: ifne +121 -> 2238
    //   2120: aload 25
    //   2122: ldc_w 385
    //   2125: invokevirtual 383	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2128: ifeq +112 -> 2240
    //   2131: goto +107 -> 2238
    //   2134: iload 5
    //   2136: ifne -560 -> 1576
    //   2139: new 154	java/lang/IllegalArgumentException
    //   2142: dup
    //   2143: ldc_w 286
    //   2146: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   2149: athrow
    //   2150: aload 20
    //   2152: getfield 388	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:enable	Z
    //   2155: ifeq -579 -> 1576
    //   2158: new 154	java/lang/IllegalArgumentException
    //   2161: dup
    //   2162: ldc_w 286
    //   2165: invokespecial 288	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   2168: athrow
    //   2169: astore 20
    //   2171: aload 23
    //   2173: invokeinterface 294 1 0
    //   2178: goto -1419 -> 759
    //   2181: astore 20
    //   2183: iconst_0
    //   2184: putstatic 65	com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper:getPackage	Z
    //   2187: getstatic 85	java/lang/System:out	Ljava/io/PrintStream;
    //   2190: new 87	java/lang/StringBuilder
    //   2193: dup
    //   2194: invokespecial 89	java/lang/StringBuilder:<init>	()V
    //   2197: ldc_w 390
    //   2200: invokevirtual 95	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2203: aload 20
    //   2205: invokevirtual 149	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   2208: invokevirtual 108	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2211: invokevirtual 114	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2214: aload 22
    //   2216: areturn
    //   2217: astore 20
    //   2219: goto -1477 -> 742
    //   2222: astore 20
    //   2224: aconst_null
    //   2225: astore 20
    //   2227: goto -1675 -> 552
    //   2230: astore 20
    //   2232: goto -1490 -> 742
    //   2235: goto -659 -> 1576
    //   2238: iconst_1
    //   2239: istore_3
    //   2240: iload 4
    //   2242: iconst_1
    //   2243: iadd
    //   2244: istore 4
    //   2246: goto -157 -> 2089
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	2249	0	this	DatabaseHelper
    //   0	2249	1	paramBoolean1	boolean
    //   0	2249	2	paramBoolean2	boolean
    //   247	1993	3	i	int
    //   568	1677	4	j	int
    //   264	1871	5	k	int
    //   282	1815	6	m	int
    //   318	537	7	n	int
    //   336	521	8	i1	int
    //   354	505	9	i2	int
    //   372	489	10	i3	int
    //   390	473	11	i4	int
    //   408	457	12	i5	int
    //   426	441	13	i6	int
    //   444	425	14	i7	int
    //   462	409	15	i8	int
    //   480	393	16	i9	int
    //   498	381	17	i10	int
    //   211	691	18	bool1	boolean
    //   195	686	19	bool2	boolean
    //   505	178	20	localObject1	Object
    //   740	1	20	localNameNotFoundException	android.content.pm.PackageManager.NameNotFoundException
    //   772	3	20	localException1	Exception
    //   781	1370	20	localObject2	Object
    //   2169	1	20	localException2	Exception
    //   2181	23	20	localException3	Exception
    //   2217	1	20	localIllegalArgumentException	IllegalArgumentException
    //   2222	1	20	localOutOfMemoryError	OutOfMemoryError
    //   2225	1	20	localObject3	Object
    //   2230	1	20	localException4	Exception
    //   501	1599	21	arrayOfString	String[]
    //   7	2208	22	localArrayList	java.util.ArrayList
    //   149	2023	23	localCursor	android.database.Cursor
    //   17	2002	24	localPackageManager	PackageManager
    //   175	1946	25	str1	String
    //   188	1672	26	localApplicationInfo	ApplicationInfo
    //   229	617	27	str2	String
    //   300	553	28	str3	String
    //   527	16	29	arrayOfByte	byte[]
    // Exception table:
    //   from	to	target	type
    //   177	210	740	android/content/pm/PackageManager$NameNotFoundException
    //   213	500	740	android/content/pm/PackageManager$NameNotFoundException
    //   511	529	740	android/content/pm/PackageManager$NameNotFoundException
    //   538	552	740	android/content/pm/PackageManager$NameNotFoundException
    //   552	570	740	android/content/pm/PackageManager$NameNotFoundException
    //   574	597	740	android/content/pm/PackageManager$NameNotFoundException
    //   599	630	740	android/content/pm/PackageManager$NameNotFoundException
    //   630	684	740	android/content/pm/PackageManager$NameNotFoundException
    //   684	698	740	android/content/pm/PackageManager$NameNotFoundException
    //   702	729	740	android/content/pm/PackageManager$NameNotFoundException
    //   729	740	740	android/content/pm/PackageManager$NameNotFoundException
    //   774	779	740	android/content/pm/PackageManager$NameNotFoundException
    //   786	833	740	android/content/pm/PackageManager$NameNotFoundException
    //   836	890	740	android/content/pm/PackageManager$NameNotFoundException
    //   893	901	740	android/content/pm/PackageManager$NameNotFoundException
    //   906	925	740	android/content/pm/PackageManager$NameNotFoundException
    //   925	991	740	android/content/pm/PackageManager$NameNotFoundException
    //   991	1057	740	android/content/pm/PackageManager$NameNotFoundException
    //   1057	1138	740	android/content/pm/PackageManager$NameNotFoundException
    //   1138	1153	740	android/content/pm/PackageManager$NameNotFoundException
    //   1159	1186	740	android/content/pm/PackageManager$NameNotFoundException
    //   1186	1236	740	android/content/pm/PackageManager$NameNotFoundException
    //   1236	1278	740	android/content/pm/PackageManager$NameNotFoundException
    //   1278	1320	740	android/content/pm/PackageManager$NameNotFoundException
    //   1320	1366	740	android/content/pm/PackageManager$NameNotFoundException
    //   1366	1377	740	android/content/pm/PackageManager$NameNotFoundException
    //   1377	1402	740	android/content/pm/PackageManager$NameNotFoundException
    //   1402	1413	740	android/content/pm/PackageManager$NameNotFoundException
    //   1413	1576	740	android/content/pm/PackageManager$NameNotFoundException
    //   1576	1584	740	android/content/pm/PackageManager$NameNotFoundException
    //   1587	1591	740	android/content/pm/PackageManager$NameNotFoundException
    //   1594	1624	740	android/content/pm/PackageManager$NameNotFoundException
    //   1624	1643	740	android/content/pm/PackageManager$NameNotFoundException
    //   1643	1662	740	android/content/pm/PackageManager$NameNotFoundException
    //   1662	1681	740	android/content/pm/PackageManager$NameNotFoundException
    //   1681	1700	740	android/content/pm/PackageManager$NameNotFoundException
    //   1700	1719	740	android/content/pm/PackageManager$NameNotFoundException
    //   1719	1738	740	android/content/pm/PackageManager$NameNotFoundException
    //   1738	1763	740	android/content/pm/PackageManager$NameNotFoundException
    //   1763	1788	740	android/content/pm/PackageManager$NameNotFoundException
    //   1788	1807	740	android/content/pm/PackageManager$NameNotFoundException
    //   1807	1818	740	android/content/pm/PackageManager$NameNotFoundException
    //   1818	1837	740	android/content/pm/PackageManager$NameNotFoundException
    //   1837	1873	740	android/content/pm/PackageManager$NameNotFoundException
    //   1873	1884	740	android/content/pm/PackageManager$NameNotFoundException
    //   1884	1903	740	android/content/pm/PackageManager$NameNotFoundException
    //   1903	1931	740	android/content/pm/PackageManager$NameNotFoundException
    //   1931	1966	740	android/content/pm/PackageManager$NameNotFoundException
    //   1966	1994	740	android/content/pm/PackageManager$NameNotFoundException
    //   1994	2041	740	android/content/pm/PackageManager$NameNotFoundException
    //   2041	2052	740	android/content/pm/PackageManager$NameNotFoundException
    //   2052	2071	740	android/content/pm/PackageManager$NameNotFoundException
    //   2081	2086	740	android/content/pm/PackageManager$NameNotFoundException
    //   2106	2120	740	android/content/pm/PackageManager$NameNotFoundException
    //   2120	2131	740	android/content/pm/PackageManager$NameNotFoundException
    //   2139	2150	740	android/content/pm/PackageManager$NameNotFoundException
    //   2150	2169	740	android/content/pm/PackageManager$NameNotFoundException
    //   511	529	772	java/lang/Exception
    //   538	552	772	java/lang/Exception
    //   742	759	2169	java/lang/Exception
    //   23	159	2181	java/lang/Exception
    //   759	763	2181	java/lang/Exception
    //   2171	2178	2181	java/lang/Exception
    //   177	210	2217	java/lang/IllegalArgumentException
    //   213	500	2217	java/lang/IllegalArgumentException
    //   511	529	2217	java/lang/IllegalArgumentException
    //   538	552	2217	java/lang/IllegalArgumentException
    //   552	570	2217	java/lang/IllegalArgumentException
    //   574	597	2217	java/lang/IllegalArgumentException
    //   599	630	2217	java/lang/IllegalArgumentException
    //   630	684	2217	java/lang/IllegalArgumentException
    //   684	698	2217	java/lang/IllegalArgumentException
    //   702	729	2217	java/lang/IllegalArgumentException
    //   729	740	2217	java/lang/IllegalArgumentException
    //   774	779	2217	java/lang/IllegalArgumentException
    //   786	833	2217	java/lang/IllegalArgumentException
    //   836	890	2217	java/lang/IllegalArgumentException
    //   893	901	2217	java/lang/IllegalArgumentException
    //   906	925	2217	java/lang/IllegalArgumentException
    //   925	991	2217	java/lang/IllegalArgumentException
    //   991	1057	2217	java/lang/IllegalArgumentException
    //   1057	1138	2217	java/lang/IllegalArgumentException
    //   1138	1153	2217	java/lang/IllegalArgumentException
    //   1159	1186	2217	java/lang/IllegalArgumentException
    //   1186	1236	2217	java/lang/IllegalArgumentException
    //   1236	1278	2217	java/lang/IllegalArgumentException
    //   1278	1320	2217	java/lang/IllegalArgumentException
    //   1320	1366	2217	java/lang/IllegalArgumentException
    //   1366	1377	2217	java/lang/IllegalArgumentException
    //   1377	1402	2217	java/lang/IllegalArgumentException
    //   1402	1413	2217	java/lang/IllegalArgumentException
    //   1413	1576	2217	java/lang/IllegalArgumentException
    //   1576	1584	2217	java/lang/IllegalArgumentException
    //   1587	1591	2217	java/lang/IllegalArgumentException
    //   1594	1624	2217	java/lang/IllegalArgumentException
    //   1624	1643	2217	java/lang/IllegalArgumentException
    //   1643	1662	2217	java/lang/IllegalArgumentException
    //   1662	1681	2217	java/lang/IllegalArgumentException
    //   1681	1700	2217	java/lang/IllegalArgumentException
    //   1700	1719	2217	java/lang/IllegalArgumentException
    //   1719	1738	2217	java/lang/IllegalArgumentException
    //   1738	1763	2217	java/lang/IllegalArgumentException
    //   1763	1788	2217	java/lang/IllegalArgumentException
    //   1788	1807	2217	java/lang/IllegalArgumentException
    //   1807	1818	2217	java/lang/IllegalArgumentException
    //   1818	1837	2217	java/lang/IllegalArgumentException
    //   1837	1873	2217	java/lang/IllegalArgumentException
    //   1873	1884	2217	java/lang/IllegalArgumentException
    //   1884	1903	2217	java/lang/IllegalArgumentException
    //   1903	1931	2217	java/lang/IllegalArgumentException
    //   1931	1966	2217	java/lang/IllegalArgumentException
    //   1966	1994	2217	java/lang/IllegalArgumentException
    //   1994	2041	2217	java/lang/IllegalArgumentException
    //   2041	2052	2217	java/lang/IllegalArgumentException
    //   2052	2071	2217	java/lang/IllegalArgumentException
    //   2081	2086	2217	java/lang/IllegalArgumentException
    //   2106	2120	2217	java/lang/IllegalArgumentException
    //   2120	2131	2217	java/lang/IllegalArgumentException
    //   2139	2150	2217	java/lang/IllegalArgumentException
    //   2150	2169	2217	java/lang/IllegalArgumentException
    //   511	529	2222	java/lang/OutOfMemoryError
    //   538	552	2222	java/lang/OutOfMemoryError
    //   159	177	2230	java/lang/Exception
    //   177	210	2230	java/lang/Exception
    //   213	500	2230	java/lang/Exception
    //   552	570	2230	java/lang/Exception
    //   574	597	2230	java/lang/Exception
    //   599	630	2230	java/lang/Exception
    //   630	684	2230	java/lang/Exception
    //   684	698	2230	java/lang/Exception
    //   702	729	2230	java/lang/Exception
    //   729	740	2230	java/lang/Exception
    //   774	779	2230	java/lang/Exception
    //   786	833	2230	java/lang/Exception
    //   836	890	2230	java/lang/Exception
    //   893	901	2230	java/lang/Exception
    //   906	925	2230	java/lang/Exception
    //   925	991	2230	java/lang/Exception
    //   991	1057	2230	java/lang/Exception
    //   1057	1138	2230	java/lang/Exception
    //   1138	1153	2230	java/lang/Exception
    //   1159	1186	2230	java/lang/Exception
    //   1186	1236	2230	java/lang/Exception
    //   1236	1278	2230	java/lang/Exception
    //   1278	1320	2230	java/lang/Exception
    //   1320	1366	2230	java/lang/Exception
    //   1366	1377	2230	java/lang/Exception
    //   1377	1402	2230	java/lang/Exception
    //   1402	1413	2230	java/lang/Exception
    //   1413	1576	2230	java/lang/Exception
    //   1576	1584	2230	java/lang/Exception
    //   1587	1591	2230	java/lang/Exception
    //   1594	1624	2230	java/lang/Exception
    //   1624	1643	2230	java/lang/Exception
    //   1643	1662	2230	java/lang/Exception
    //   1662	1681	2230	java/lang/Exception
    //   1681	1700	2230	java/lang/Exception
    //   1700	1719	2230	java/lang/Exception
    //   1719	1738	2230	java/lang/Exception
    //   1738	1763	2230	java/lang/Exception
    //   1763	1788	2230	java/lang/Exception
    //   1788	1807	2230	java/lang/Exception
    //   1807	1818	2230	java/lang/Exception
    //   1818	1837	2230	java/lang/Exception
    //   1837	1873	2230	java/lang/Exception
    //   1873	1884	2230	java/lang/Exception
    //   1884	1903	2230	java/lang/Exception
    //   1903	1931	2230	java/lang/Exception
    //   1931	1966	2230	java/lang/Exception
    //   1966	1994	2230	java/lang/Exception
    //   1994	2041	2230	java/lang/Exception
    //   2041	2052	2230	java/lang/Exception
    //   2052	2071	2230	java/lang/Exception
    //   2081	2086	2230	java/lang/Exception
    //   2106	2120	2230	java/lang/Exception
    //   2120	2131	2230	java/lang/Exception
    //   2139	2150	2230	java/lang/Exception
    //   2150	2169	2230	java/lang/Exception
  }
  
  public boolean isOpen()
  {
    return db.isOpen();
  }
  
  public void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.execSQL("CREATE TABLE Packages (pkgName TEXT PRIMARY KEY, pkgLabel TEXT, stored Integer, storepref Integer, hidden Integer, statusi TEXT, boot_ads Integer, boot_lvl Integer, boot_custom Integer, boot_manual Integer, custom Integer, lvl Integer, ads Integer, modified Integer, system Integer, odex Integer, icon BLOB, updatetime Integer, billing Integer );");
  }
  
  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS Packages");
    onCreate(paramSQLiteDatabase);
  }
  
  public void savePackage(PkgListItem paramPkgListItem)
    throws SQLiteException
  {
    for (;;)
    {
      try
      {
        savePackage = true;
        localContentValues = new ContentValues();
        localContentValues.put("pkgName", paramPkgListItem.pkgName);
        localContentValues.put("pkgLabel", paramPkgListItem.name);
        localContentValues.put("stored", Integer.valueOf(paramPkgListItem.stored));
        localContentValues.put("storepref", Integer.valueOf(paramPkgListItem.storepref));
        localContentValues.put("hidden", Boolean.valueOf(paramPkgListItem.hidden));
        localContentValues.put("statusi", paramPkgListItem.statusi);
        localContentValues.put("boot_ads", Boolean.valueOf(paramPkgListItem.boot_ads));
        localContentValues.put("boot_lvl", Boolean.valueOf(paramPkgListItem.boot_lvl));
        localContentValues.put("boot_custom", Boolean.valueOf(paramPkgListItem.boot_custom));
        localContentValues.put("boot_manual", Boolean.valueOf(paramPkgListItem.boot_manual));
        localContentValues.put("custom", Boolean.valueOf(paramPkgListItem.custom));
        localContentValues.put("lvl", Boolean.valueOf(paramPkgListItem.lvl));
        localContentValues.put("ads", Boolean.valueOf(paramPkgListItem.ads));
        localContentValues.put("modified", Boolean.valueOf(paramPkgListItem.modified));
        localContentValues.put("system", Boolean.valueOf(paramPkgListItem.system));
        localContentValues.put("odex", Boolean.valueOf(paramPkgListItem.odex));
        localContentValues.put("updatetime", Integer.valueOf(paramPkgListItem.updatetime));
        localContentValues.put("billing", Boolean.valueOf(paramPkgListItem.billing));
        localObject = listAppsFragment.getPkgMng().getApplicationInfo(paramPkgListItem.pkgName, 0).sourceDir;
        localObject = null;
      }
      catch (Exception paramPkgListItem)
      {
        ContentValues localContentValues;
        Object localObject;
        savePackage = false;
        System.out.println("LuckyPatcher-Error: savePackage " + paramPkgListItem);
        return;
      }
      try
      {
        if (paramPkgListItem.icon != null) {
          localObject = ((BitmapDrawable)paramPkgListItem.icon).getBitmap();
        }
        if (localObject != null)
        {
          paramPkgListItem = new ByteArrayOutputStream();
          ((Bitmap)localObject).compress(Bitmap.CompressFormat.PNG, 100, paramPkgListItem);
          localContentValues.put("icon", paramPkgListItem.toByteArray());
        }
      }
      catch (OutOfMemoryError paramPkgListItem)
      {
        continue;
      }
      try
      {
        db.insertOrThrow("Packages", "pkgName", localContentValues);
        savePackage = false;
        savePackage = false;
        return;
      }
      catch (Exception paramPkgListItem)
      {
        db.replace("Packages", null, localContentValues);
      }
    }
  }
  
  public void updatePackage(List<PkgListItem> paramList) {}
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */