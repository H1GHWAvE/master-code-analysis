package com.android.vending.billing.InAppBillingService.LUCK;

public class StringItem
{
  public boolean bits32 = false;
  public byte[] offset = null;
  public String str = "";
  
  public StringItem(String paramString, byte[] paramArrayOfByte, boolean paramBoolean)
  {
    this.offset = paramArrayOfByte;
    this.str = paramString;
    this.bits32 = paramBoolean;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/android/vending/billing/InAppBillingService/LUCK/StringItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */