package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import java.io.PrintStream;

public class LuckyApp
  extends Application
{
  static Context instance = null;
  
  public static Context getInstance()
  {
    return instance;
  }
  
  public void onCreate()
  {
    super.onCreate();
    instance = getApplicationContext();
    Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
    {
      public void uncaughtException(Thread paramAnonymousThread, Throwable paramAnonymousThrowable)
      {
        System.out.println("FATAL Exception LP " + paramAnonymousThrowable.toString());
        try
        {
          listAppsFragment.init();
          paramAnonymousThrowable.printStackTrace();
          int i = 0;
          if (LuckyApp.this.getSharedPreferences("config", 4).getBoolean("force_close", false)) {
            i = 1;
          }
          LuckyApp.this.getSharedPreferences("config", 4).edit().putBoolean("force_close", true).commit();
          try
          {
            new LogCollector().collect(LuckyApp.instance, true);
            if (i == 0)
            {
              paramAnonymousThread = listAppsFragment.getPkgMng().getLaunchIntentForPackage(LuckyApp.this.getPackageName());
              LuckyApp.this.startActivity(paramAnonymousThread);
            }
          }
          catch (RuntimeException paramAnonymousThread)
          {
            for (;;)
            {
              paramAnonymousThread.printStackTrace();
            }
          }
          catch (Exception paramAnonymousThread)
          {
            for (;;)
            {
              System.exit(0);
              paramAnonymousThread.printStackTrace();
            }
          }
          System.exit(0);
          return;
        }
        catch (Exception paramAnonymousThread)
        {
          for (;;)
          {
            paramAnonymousThread.printStackTrace();
          }
        }
      }
    });
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/android/vending/billing/InAppBillingService/LUCK/LuckyApp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */