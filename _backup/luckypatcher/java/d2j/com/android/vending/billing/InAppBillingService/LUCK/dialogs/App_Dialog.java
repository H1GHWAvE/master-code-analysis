package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.PkgListItem;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import java.io.PrintStream;

public class App_Dialog
{
  Dialog dialog = null;
  
  public void dismiss()
  {
    if (this.dialog != null)
    {
      this.dialog.dismiss();
      this.dialog = null;
    }
  }
  
  public Dialog onCreateDialog()
  {
    System.out.println("App Dialog create.");
    if ((listAppsFragment.frag == null) || (listAppsFragment.frag.getContext() == null)) {
      dismiss();
    }
    final Object localObject1 = new SpannableStringBuilder();
    final SpannableStringBuilder localSpannableStringBuilder1 = new SpannableStringBuilder();
    final SpannableStringBuilder localSpannableStringBuilder2 = new SpannableStringBuilder();
    LinearLayout localLinearLayout = (LinearLayout)View.inflate(listAppsFragment.frag.getContext(), 2130968584, null);
    final Object localObject2 = (LinearLayout)localLinearLayout.findViewById(2131558447).findViewById(2131558412);
    ImageView localImageView = (ImageView)((LinearLayout)localObject2).findViewById(2131558448);
    final TextView localTextView1 = (TextView)((LinearLayout)localObject2).findViewById(2131558449);
    final TextView localTextView2 = (TextView)((LinearLayout)localObject2).findViewById(2131558413);
    final TextView localTextView3 = (TextView)((LinearLayout)localObject2).findViewById(2131558452);
    final TextView localTextView4 = (TextView)((LinearLayout)localObject2).findViewById(2131558455);
    localObject2 = (ProgressBar)((LinearLayout)localObject2).findViewById(2131558451);
    try
    {
      localImageView.setImageDrawable(listAppsFragment.getPkgMng().getApplicationIcon(listAppsFragment.pli.pkgName));
      new Thread(new Runnable()
      {
        /* Error */
        public void run()
        {
          // Byte code:
          //   0: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   3: getfield 75	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:name	Ljava/lang/String;
          //   6: ldc 77
          //   8: ldc 79
          //   10: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   13: astore 7
          //   15: getstatic 89	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:handler	Landroid/os/Handler;
          //   18: new 13	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$1
          //   21: dup
          //   22: aload_0
          //   23: aload 7
          //   25: invokespecial 92	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$1:<init>	(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;Landroid/text/SpannableString;)V
          //   28: invokevirtual 98	android/os/Handler:post	(Ljava/lang/Runnable;)Z
          //   31: pop
          //   32: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   35: getfield 102	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:enable	Z
          //   38: ifeq +600 -> 638
          //   41: new 104	java/lang/StringBuilder
          //   44: dup
          //   45: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   48: ldc 106
          //   50: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   53: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   56: ldc 116
          //   58: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   61: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   64: astore 7
          //   66: aload_0
          //   67: getfield 42	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder	Landroid/text/SpannableStringBuilder;
          //   70: aload 7
          //   72: ldc 122
          //   74: ldc 122
          //   76: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   79: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   82: pop
          //   83: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   86: getfield 130	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:custom	Z
          //   89: ifeq +45 -> 134
          //   92: new 104	java/lang/StringBuilder
          //   95: dup
          //   96: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   99: ldc -125
          //   101: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   104: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   107: ldc 116
          //   109: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   112: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   115: astore 7
          //   117: aload_0
          //   118: getfield 42	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder	Landroid/text/SpannableStringBuilder;
          //   121: aload 7
          //   123: ldc -124
          //   125: ldc 122
          //   127: invokestatic 135	com/chelpus/Utils:getColoredText	(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;
          //   130: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   133: pop
          //   134: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   137: getfield 138	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:lvl	Z
          //   140: ifeq +45 -> 185
          //   143: new 104	java/lang/StringBuilder
          //   146: dup
          //   147: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   150: ldc -117
          //   152: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   155: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   158: ldc 116
          //   160: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   163: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   166: astore 7
          //   168: aload_0
          //   169: getfield 42	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder	Landroid/text/SpannableStringBuilder;
          //   172: aload 7
          //   174: ldc -116
          //   176: ldc 122
          //   178: invokestatic 135	com/chelpus/Utils:getColoredText	(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;
          //   181: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   184: pop
          //   185: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   188: getfield 143	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:ads	Z
          //   191: ifeq +45 -> 236
          //   194: new 104	java/lang/StringBuilder
          //   197: dup
          //   198: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   201: ldc -112
          //   203: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   206: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   209: ldc 116
          //   211: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   214: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   217: astore 7
          //   219: aload_0
          //   220: getfield 42	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder	Landroid/text/SpannableStringBuilder;
          //   223: aload 7
          //   225: ldc -124
          //   227: ldc 122
          //   229: invokestatic 135	com/chelpus/Utils:getColoredText	(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;
          //   232: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   235: pop
          //   236: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   239: getfield 143	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:ads	Z
          //   242: ifne +63 -> 305
          //   245: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   248: getfield 138	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:lvl	Z
          //   251: ifne +54 -> 305
          //   254: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   257: getfield 130	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:custom	Z
          //   260: ifne +45 -> 305
          //   263: new 104	java/lang/StringBuilder
          //   266: dup
          //   267: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   270: ldc -111
          //   272: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   275: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   278: ldc 116
          //   280: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   283: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   286: astore 7
          //   288: aload_0
          //   289: getfield 42	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder	Landroid/text/SpannableStringBuilder;
          //   292: aload 7
          //   294: ldc -110
          //   296: ldc 122
          //   298: invokestatic 135	com/chelpus/Utils:getColoredText	(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;
          //   301: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   304: pop
          //   305: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   308: getfield 149	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:modified	Z
          //   311: ifeq +380 -> 691
          //   314: new 104	java/lang/StringBuilder
          //   317: dup
          //   318: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   321: ldc -106
          //   323: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   326: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   329: ldc 116
          //   331: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   334: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   337: astore 7
          //   339: aload_0
          //   340: getfield 42	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder	Landroid/text/SpannableStringBuilder;
          //   343: aload 7
          //   345: ldc -116
          //   347: ldc 122
          //   349: invokestatic 135	com/chelpus/Utils:getColoredText	(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;
          //   352: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   355: pop
          //   356: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   359: getfield 153	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:odex	Z
          //   362: ifeq +374 -> 736
          //   365: new 104	java/lang/StringBuilder
          //   368: dup
          //   369: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   372: ldc -102
          //   374: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   377: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   380: ldc 116
          //   382: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   385: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   388: astore 7
          //   390: aload_0
          //   391: getfield 42	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder	Landroid/text/SpannableStringBuilder;
          //   394: aload 7
          //   396: ldc -124
          //   398: ldc 122
          //   400: invokestatic 135	com/chelpus/Utils:getColoredText	(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;
          //   403: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   406: pop
          //   407: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   410: getfield 157	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:system	Z
          //   413: ifeq +368 -> 781
          //   416: ldc -98
          //   418: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   421: astore 7
          //   423: aload_0
          //   424: getfield 42	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder	Landroid/text/SpannableStringBuilder;
          //   427: aload 7
          //   429: ldc -97
          //   431: ldc 122
          //   433: invokestatic 135	com/chelpus/Utils:getColoredText	(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;
          //   436: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   439: pop
          //   440: getstatic 89	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:handler	Landroid/os/Handler;
          //   443: new 15	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$2
          //   446: dup
          //   447: aload_0
          //   448: invokespecial 162	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$2:<init>	(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;)V
          //   451: invokevirtual 98	android/os/Handler:post	(Ljava/lang/Runnable;)Z
          //   454: pop
          //   455: invokestatic 166	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getPkgMng	()Landroid/content/pm/PackageManager;
          //   458: astore 9
          //   460: aload_0
          //   461: getfield 44	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder3	Landroid/text/SpannableStringBuilder;
          //   464: invokevirtual 169	android/text/SpannableStringBuilder:clear	()V
          //   467: aload 9
          //   469: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   472: getfield 172	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
          //   475: sipush 4096
          //   478: invokevirtual 178	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
          //   481: getfield 184	android/content/pm/PackageInfo:requestedPermissions	[Ljava/lang/String;
          //   484: astore 10
          //   486: aload 10
          //   488: ifnull +360 -> 848
          //   491: aload 10
          //   493: arraylength
          //   494: ifle +354 -> 848
          //   497: aload 10
          //   499: arraylength
          //   500: anewarray 186	java/lang/String
          //   503: astore 7
          //   505: iconst_0
          //   506: istore_1
          //   507: iload_1
          //   508: aload 10
          //   510: arraylength
          //   511: if_icmpge +337 -> 848
          //   514: new 104	java/lang/StringBuilder
          //   517: dup
          //   518: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   521: aload 10
          //   523: iload_1
          //   524: aaload
          //   525: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   528: ldc 116
          //   530: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   533: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   536: astore 7
          //   538: aload_0
          //   539: getfield 44	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder3	Landroid/text/SpannableStringBuilder;
          //   542: aload 7
          //   544: ldc -68
          //   546: ldc 79
          //   548: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   551: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   554: pop
          //   555: aconst_null
          //   556: astore 7
          //   558: aload 9
          //   560: aload 10
          //   562: iload_1
          //   563: aaload
          //   564: iconst_0
          //   565: invokevirtual 192	android/content/pm/PackageManager:getPermissionInfo	(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;
          //   568: aload 9
          //   570: invokevirtual 198	android/content/pm/PermissionInfo:loadDescription	(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
          //   573: invokeinterface 201 1 0
          //   578: astore 8
          //   580: aload 8
          //   582: astore 7
          //   584: aload 7
          //   586: ifnonnull +230 -> 816
          //   589: new 104	java/lang/StringBuilder
          //   592: dup
          //   593: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   596: ldc -54
          //   598: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   601: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   604: ldc -52
          //   606: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   609: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   612: astore 7
          //   614: aload_0
          //   615: getfield 44	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder3	Landroid/text/SpannableStringBuilder;
          //   618: aload 7
          //   620: ldc 122
          //   622: ldc 122
          //   624: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   627: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   630: pop
          //   631: iload_1
          //   632: iconst_1
          //   633: iadd
          //   634: istore_1
          //   635: goto -128 -> 507
          //   638: new 104	java/lang/StringBuilder
          //   641: dup
          //   642: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   645: ldc -51
          //   647: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   650: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   653: ldc 116
          //   655: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   658: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   661: astore 7
          //   663: aload_0
          //   664: getfield 42	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder	Landroid/text/SpannableStringBuilder;
          //   667: aload 7
          //   669: ldc 122
          //   671: ldc 122
          //   673: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   676: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   679: pop
          //   680: goto -597 -> 83
          //   683: astore 7
          //   685: aload 7
          //   687: invokevirtual 208	java/lang/NullPointerException:printStackTrace	()V
          //   690: return
          //   691: new 104	java/lang/StringBuilder
          //   694: dup
          //   695: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   698: ldc -47
          //   700: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   703: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   706: ldc 116
          //   708: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   711: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   714: astore 7
          //   716: aload_0
          //   717: getfield 42	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder	Landroid/text/SpannableStringBuilder;
          //   720: aload 7
          //   722: ldc 122
          //   724: ldc 122
          //   726: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   729: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   732: pop
          //   733: goto -377 -> 356
          //   736: new 104	java/lang/StringBuilder
          //   739: dup
          //   740: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   743: ldc -46
          //   745: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   748: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   751: ldc 116
          //   753: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   756: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   759: astore 7
          //   761: aload_0
          //   762: getfield 42	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder	Landroid/text/SpannableStringBuilder;
          //   765: aload 7
          //   767: ldc 122
          //   769: ldc 122
          //   771: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   774: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   777: pop
          //   778: goto -371 -> 407
          //   781: ldc -45
          //   783: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   786: astore 7
          //   788: aload_0
          //   789: getfield 42	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder	Landroid/text/SpannableStringBuilder;
          //   792: aload 7
          //   794: ldc -116
          //   796: ldc 122
          //   798: invokestatic 135	com/chelpus/Utils:getColoredText	(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;
          //   801: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   804: pop
          //   805: goto -365 -> 440
          //   808: astore 7
          //   810: aconst_null
          //   811: astore 7
          //   813: goto -229 -> 584
          //   816: new 104	java/lang/StringBuilder
          //   819: dup
          //   820: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   823: aload 7
          //   825: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   828: ldc -52
          //   830: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   833: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   836: astore 7
          //   838: goto -224 -> 614
          //   841: astore 7
          //   843: aload 7
          //   845: invokevirtual 212	android/content/pm/PackageManager$NameNotFoundException:printStackTrace	()V
          //   848: getstatic 89	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:handler	Landroid/os/Handler;
          //   851: new 17	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$3
          //   854: dup
          //   855: aload_0
          //   856: invokespecial 213	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$3:<init>	(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;)V
          //   859: invokevirtual 98	android/os/Handler:post	(Ljava/lang/Runnable;)Z
          //   862: pop
          //   863: aload_0
          //   864: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   867: invokevirtual 169	android/text/SpannableStringBuilder:clear	()V
          //   870: aload_0
          //   871: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   874: ldc -41
          //   876: ldc -68
          //   878: ldc 79
          //   880: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   883: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   886: pop
          //   887: new 104	java/lang/StringBuilder
          //   890: dup
          //   891: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   894: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   897: getfield 172	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
          //   900: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   903: ldc 116
          //   905: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   908: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   911: astore 7
          //   913: aload_0
          //   914: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   917: aload 7
          //   919: ldc 122
          //   921: ldc 122
          //   923: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   926: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   929: pop
          //   930: new 104	java/lang/StringBuilder
          //   933: dup
          //   934: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   937: ldc -40
          //   939: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   942: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   945: ldc 116
          //   947: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   950: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   953: astore 7
          //   955: aload_0
          //   956: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   959: aload 7
          //   961: ldc -68
          //   963: ldc 79
          //   965: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   968: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   971: pop
          //   972: new 104	java/lang/StringBuilder
          //   975: dup
          //   976: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   979: invokestatic 166	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getPkgMng	()Landroid/content/pm/PackageManager;
          //   982: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   985: getfield 172	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
          //   988: invokevirtual 220	android/content/pm/PackageManager:getLaunchIntentForPackage	(Ljava/lang/String;)Landroid/content/Intent;
          //   991: invokevirtual 226	android/content/Intent:getComponent	()Landroid/content/ComponentName;
          //   994: invokevirtual 231	android/content/ComponentName:getClassName	()Ljava/lang/String;
          //   997: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1000: ldc 116
          //   1002: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1005: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1008: astore 7
          //   1010: aload_0
          //   1011: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1014: aload 7
          //   1016: ldc 122
          //   1018: ldc 122
          //   1020: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1023: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1026: pop
          //   1027: new 104	java/lang/StringBuilder
          //   1030: dup
          //   1031: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1034: ldc -24
          //   1036: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   1039: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1042: ldc 116
          //   1044: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1047: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1050: astore 7
          //   1052: aload_0
          //   1053: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1056: aload 7
          //   1058: ldc -68
          //   1060: ldc 79
          //   1062: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1065: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1068: pop
          //   1069: invokestatic 166	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getPkgMng	()Landroid/content/pm/PackageManager;
          //   1072: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   1075: getfield 172	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
          //   1078: iconst_0
          //   1079: invokevirtual 178	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
          //   1082: getfield 236	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
          //   1085: getfield 241	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
          //   1088: astore 7
          //   1090: aload_0
          //   1091: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1094: aload 7
          //   1096: ldc 122
          //   1098: ldc 122
          //   1100: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1103: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1106: pop
          //   1107: new 104	java/lang/StringBuilder
          //   1110: dup
          //   1111: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1114: ldc 116
          //   1116: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1119: ldc -14
          //   1121: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   1124: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1127: ldc 116
          //   1129: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1132: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1135: astore 7
          //   1137: aload_0
          //   1138: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1141: aload 7
          //   1143: ldc -68
          //   1145: ldc 79
          //   1147: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1150: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1153: pop
          //   1154: new 104	java/lang/StringBuilder
          //   1157: dup
          //   1158: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1161: invokestatic 166	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getPkgMng	()Landroid/content/pm/PackageManager;
          //   1164: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   1167: getfield 172	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
          //   1170: iconst_0
          //   1171: invokevirtual 178	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
          //   1174: getfield 236	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
          //   1177: getfield 245	android/content/pm/ApplicationInfo:dataDir	Ljava/lang/String;
          //   1180: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1183: ldc -9
          //   1185: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1188: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1191: astore 7
          //   1193: aload_0
          //   1194: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1197: aload 7
          //   1199: ldc 122
          //   1201: ldc 122
          //   1203: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1206: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1209: pop
          //   1210: new 104	java/lang/StringBuilder
          //   1213: dup
          //   1214: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1217: ldc 116
          //   1219: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1222: ldc -8
          //   1224: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   1227: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1230: ldc -6
          //   1232: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1235: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1238: astore 7
          //   1240: aload_0
          //   1241: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1244: aload 7
          //   1246: ldc -68
          //   1248: ldc 79
          //   1250: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1253: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1256: pop
          //   1257: invokestatic 166	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getPkgMng	()Landroid/content/pm/PackageManager;
          //   1260: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   1263: getfield 172	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
          //   1266: iconst_0
          //   1267: invokevirtual 178	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
          //   1270: getfield 253	android/content/pm/PackageInfo:versionName	Ljava/lang/String;
          //   1273: astore 7
          //   1275: aload_0
          //   1276: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1279: aload 7
          //   1281: ldc 122
          //   1283: ldc 122
          //   1285: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1288: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1291: pop
          //   1292: new 104	java/lang/StringBuilder
          //   1295: dup
          //   1296: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1299: ldc 116
          //   1301: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1304: ldc -2
          //   1306: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   1309: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1312: ldc -6
          //   1314: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1317: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1320: astore 7
          //   1322: aload_0
          //   1323: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1326: aload 7
          //   1328: ldc -68
          //   1330: ldc 79
          //   1332: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1335: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1338: pop
          //   1339: new 104	java/lang/StringBuilder
          //   1342: dup
          //   1343: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1346: ldc 122
          //   1348: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1351: invokestatic 166	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getPkgMng	()Landroid/content/pm/PackageManager;
          //   1354: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   1357: getfield 172	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
          //   1360: iconst_0
          //   1361: invokevirtual 178	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
          //   1364: getfield 258	android/content/pm/PackageInfo:versionCode	I
          //   1367: invokevirtual 261	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
          //   1370: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1373: astore 7
          //   1375: aload_0
          //   1376: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1379: aload 7
          //   1381: ldc 122
          //   1383: ldc 122
          //   1385: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1388: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1391: pop
          //   1392: new 104	java/lang/StringBuilder
          //   1395: dup
          //   1396: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1399: ldc 116
          //   1401: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1404: ldc_w 262
          //   1407: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   1410: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1413: ldc -6
          //   1415: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1418: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1421: astore 7
          //   1423: aload_0
          //   1424: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1427: aload 7
          //   1429: ldc -68
          //   1431: ldc 79
          //   1433: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1436: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1439: pop
          //   1440: new 104	java/lang/StringBuilder
          //   1443: dup
          //   1444: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1447: ldc 122
          //   1449: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1452: invokestatic 166	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getPkgMng	()Landroid/content/pm/PackageManager;
          //   1455: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   1458: getfield 172	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
          //   1461: iconst_0
          //   1462: invokevirtual 178	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
          //   1465: getfield 236	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
          //   1468: getfield 265	android/content/pm/ApplicationInfo:uid	I
          //   1471: invokevirtual 261	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
          //   1474: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1477: astore 7
          //   1479: aload_0
          //   1480: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1483: aload 7
          //   1485: ldc 122
          //   1487: ldc 122
          //   1489: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1492: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1495: pop
          //   1496: new 104	java/lang/StringBuilder
          //   1499: dup
          //   1500: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1503: ldc 116
          //   1505: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1508: ldc_w 266
          //   1511: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   1514: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1517: ldc -6
          //   1519: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1522: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1525: astore 7
          //   1527: aload_0
          //   1528: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1531: aload 7
          //   1533: ldc -68
          //   1535: ldc 79
          //   1537: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1540: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1543: pop
          //   1544: new 268	java/text/SimpleDateFormat
          //   1547: dup
          //   1548: ldc_w 270
          //   1551: invokespecial 273	java/text/SimpleDateFormat:<init>	(Ljava/lang/String;)V
          //   1554: astore 7
          //   1556: new 104	java/lang/StringBuilder
          //   1559: dup
          //   1560: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1563: ldc 122
          //   1565: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1568: aload 7
          //   1570: new 275	java/util/Date
          //   1573: dup
          //   1574: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   1577: getfield 278	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:updatetime	I
          //   1580: i2l
          //   1581: ldc2_w 279
          //   1584: lmul
          //   1585: invokespecial 283	java/util/Date:<init>	(J)V
          //   1588: invokevirtual 287	java/text/SimpleDateFormat:format	(Ljava/util/Date;)Ljava/lang/String;
          //   1591: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1594: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1597: astore 7
          //   1599: getstatic 293	java/lang/System:out	Ljava/io/PrintStream;
          //   1602: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   1605: getfield 278	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:updatetime	I
          //   1608: invokevirtual 299	java/io/PrintStream:println	(I)V
          //   1611: getstatic 293	java/lang/System:out	Ljava/io/PrintStream;
          //   1614: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   1617: getfield 278	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:updatetime	I
          //   1620: i2l
          //   1621: ldc2_w 279
          //   1624: lmul
          //   1625: invokevirtual 301	java/io/PrintStream:println	(J)V
          //   1628: aload_0
          //   1629: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1632: aload 7
          //   1634: ldc 122
          //   1636: ldc 122
          //   1638: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1641: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1644: pop
          //   1645: new 104	java/lang/StringBuilder
          //   1648: dup
          //   1649: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1652: ldc -52
          //   1654: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1657: ldc_w 302
          //   1660: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   1663: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1666: ldc -6
          //   1668: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1671: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1674: astore 7
          //   1676: aload_0
          //   1677: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1680: aload 7
          //   1682: ldc -68
          //   1684: ldc 79
          //   1686: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1689: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1692: pop
          //   1693: new 104	java/lang/StringBuilder
          //   1696: dup
          //   1697: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1700: ldc_w 304
          //   1703: iconst_1
          //   1704: anewarray 4	java/lang/Object
          //   1707: dup
          //   1708: iconst_0
          //   1709: new 306	java/io/File
          //   1712: dup
          //   1713: invokestatic 166	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getPkgMng	()Landroid/content/pm/PackageManager;
          //   1716: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   1719: getfield 172	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
          //   1722: iconst_0
          //   1723: invokevirtual 178	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
          //   1726: getfield 236	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
          //   1729: getfield 241	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
          //   1732: invokespecial 307	java/io/File:<init>	(Ljava/lang/String;)V
          //   1735: invokevirtual 311	java/io/File:length	()J
          //   1738: l2f
          //   1739: ldc_w 312
          //   1742: fdiv
          //   1743: invokestatic 318	java/lang/Float:valueOf	(F)Ljava/lang/Float;
          //   1746: aastore
          //   1747: invokestatic 321	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
          //   1750: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1753: ldc_w 323
          //   1756: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1759: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1762: astore 7
          //   1764: aload_0
          //   1765: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1768: aload 7
          //   1770: ldc 122
          //   1772: ldc 122
          //   1774: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1777: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1780: pop
          //   1781: new 104	java/lang/StringBuilder
          //   1784: dup
          //   1785: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1788: ldc 116
          //   1790: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1793: ldc_w 324
          //   1796: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   1799: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1802: ldc -6
          //   1804: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1807: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1810: astore 7
          //   1812: aload_0
          //   1813: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1816: aload 7
          //   1818: ldc -68
          //   1820: ldc 79
          //   1822: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1825: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1828: pop
          //   1829: lconst_0
          //   1830: lstore_2
          //   1831: invokestatic 166	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getPkgMng	()Landroid/content/pm/PackageManager;
          //   1834: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   1837: getfield 172	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
          //   1840: iconst_0
          //   1841: invokevirtual 178	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
          //   1844: getfield 236	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
          //   1847: getfield 241	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
          //   1850: invokestatic 328	com/chelpus/Utils:getFileDalvikCache	(Ljava/lang/String;)Ljava/io/File;
          //   1853: invokevirtual 311	java/io/File:length	()J
          //   1856: lstore 4
          //   1858: lload 4
          //   1860: lstore_2
          //   1861: new 104	java/lang/StringBuilder
          //   1864: dup
          //   1865: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1868: ldc_w 304
          //   1871: iconst_1
          //   1872: anewarray 4	java/lang/Object
          //   1875: dup
          //   1876: iconst_0
          //   1877: lload_2
          //   1878: l2f
          //   1879: ldc_w 312
          //   1882: fdiv
          //   1883: invokestatic 318	java/lang/Float:valueOf	(F)Ljava/lang/Float;
          //   1886: aastore
          //   1887: invokestatic 321	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
          //   1890: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1893: ldc_w 323
          //   1896: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1899: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1902: astore 7
          //   1904: aload_0
          //   1905: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1908: aload 7
          //   1910: ldc 122
          //   1912: ldc 122
          //   1914: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1917: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1920: pop
          //   1921: getstatic 331	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:su	Z
          //   1924: istore 6
          //   1926: iload 6
          //   1928: ifeq +195 -> 2123
          //   1931: new 104	java/lang/StringBuilder
          //   1934: dup
          //   1935: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   1938: ldc 116
          //   1940: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1943: ldc_w 332
          //   1946: invokestatic 110	com/chelpus/Utils:getText	(I)Ljava/lang/String;
          //   1949: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1952: ldc -6
          //   1954: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   1957: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   1960: astore 7
          //   1962: aload_0
          //   1963: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   1966: aload 7
          //   1968: ldc -68
          //   1970: ldc 79
          //   1972: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   1975: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   1978: pop
          //   1979: ldc 122
          //   1981: astore 7
          //   1983: new 81	com/chelpus/Utils
          //   1986: dup
          //   1987: ldc 122
          //   1989: invokespecial 333	com/chelpus/Utils:<init>	(Ljava/lang/String;)V
          //   1992: iconst_1
          //   1993: anewarray 186	java/lang/String
          //   1996: dup
          //   1997: iconst_0
          //   1998: new 104	java/lang/StringBuilder
          //   2001: dup
          //   2002: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   2005: getstatic 336	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:dalvikruncommand	Ljava/lang/String;
          //   2008: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   2011: ldc_w 338
          //   2014: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   2017: invokestatic 166	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:getPkgMng	()Landroid/content/pm/PackageManager;
          //   2020: getstatic 69	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:pli	Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
          //   2023: getfield 172	com/android/vending/billing/InAppBillingService/LUCK/PkgListItem:pkgName	Ljava/lang/String;
          //   2026: iconst_0
          //   2027: invokevirtual 178	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
          //   2030: getfield 236	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
          //   2033: getfield 245	android/content/pm/ApplicationInfo:dataDir	Ljava/lang/String;
          //   2036: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   2039: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   2042: aastore
          //   2043: invokevirtual 342	com/chelpus/Utils:cmdRoot	([Ljava/lang/String;)Ljava/lang/String;
          //   2046: astore 8
          //   2048: aload 8
          //   2050: astore 7
          //   2052: aload 8
          //   2054: ldc_w 344
          //   2057: ldc 122
          //   2059: invokevirtual 348	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
          //   2062: astore 8
          //   2064: aload 8
          //   2066: astore 7
          //   2068: new 104	java/lang/StringBuilder
          //   2071: dup
          //   2072: invokespecial 105	java/lang/StringBuilder:<init>	()V
          //   2075: aload 7
          //   2077: ldc 116
          //   2079: ldc 122
          //   2081: invokevirtual 348	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
          //   2084: ldc_w 350
          //   2087: ldc 122
          //   2089: invokevirtual 348	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
          //   2092: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   2095: ldc_w 323
          //   2098: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
          //   2101: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
          //   2104: astore 7
          //   2106: aload_0
          //   2107: getfield 48	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1:val$builder2	Landroid/text/SpannableStringBuilder;
          //   2110: aload 7
          //   2112: ldc 122
          //   2114: ldc 122
          //   2116: invokestatic 85	com/chelpus/Utils:getColoredText	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
          //   2119: invokevirtual 127	android/text/SpannableStringBuilder:append	(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
          //   2122: pop
          //   2123: getstatic 89	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:handler	Landroid/os/Handler;
          //   2126: new 19	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$4
          //   2129: dup
          //   2130: aload_0
          //   2131: invokespecial 351	com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$4:<init>	(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;)V
          //   2134: invokevirtual 98	android/os/Handler:post	(Ljava/lang/Runnable;)Z
          //   2137: pop
          //   2138: return
          //   2139: astore 7
          //   2141: aload 7
          //   2143: invokevirtual 352	java/lang/Exception:printStackTrace	()V
          //   2146: goto -1119 -> 1027
          //   2149: astore 7
          //   2151: aload 7
          //   2153: invokevirtual 212	android/content/pm/PackageManager$NameNotFoundException:printStackTrace	()V
          //   2156: goto -375 -> 1781
          //   2159: astore 7
          //   2161: goto -38 -> 2123
          //   2164: astore 8
          //   2166: goto -98 -> 2068
          //   2169: astore 7
          //   2171: goto -250 -> 1921
          //   2174: astore 7
          //   2176: goto -315 -> 1861
          //   2179: astore 8
          //   2181: goto -1597 -> 584
          // Local variable table:
          //   start	length	slot	name	signature
          //   0	2184	0	this	1
          //   506	129	1	i	int
          //   1830	48	2	l1	long
          //   1856	3	4	l2	long
          //   1924	3	6	bool	boolean
          //   13	655	7	localObject1	Object
          //   683	3	7	localNullPointerException	NullPointerException
          //   714	79	7	str1	String
          //   808	1	7	localNameNotFoundException1	PackageManager.NameNotFoundException
          //   811	26	7	str2	String
          //   841	3	7	localNameNotFoundException2	PackageManager.NameNotFoundException
          //   911	1200	7	localObject2	Object
          //   2139	3	7	localException1	Exception
          //   2149	3	7	localNameNotFoundException3	PackageManager.NameNotFoundException
          //   2159	1	7	localException2	Exception
          //   2169	1	7	localException3	Exception
          //   2174	1	7	localException4	Exception
          //   578	1487	8	str3	String
          //   2164	1	8	localException5	Exception
          //   2179	1	8	localException6	Exception
          //   458	111	9	localPackageManager	PackageManager
          //   484	77	10	arrayOfString	String[]
          // Exception table:
          //   from	to	target	type
          //   0	83	683	java/lang/NullPointerException
          //   83	134	683	java/lang/NullPointerException
          //   134	185	683	java/lang/NullPointerException
          //   185	236	683	java/lang/NullPointerException
          //   236	305	683	java/lang/NullPointerException
          //   305	356	683	java/lang/NullPointerException
          //   356	407	683	java/lang/NullPointerException
          //   407	440	683	java/lang/NullPointerException
          //   440	467	683	java/lang/NullPointerException
          //   467	486	683	java/lang/NullPointerException
          //   491	505	683	java/lang/NullPointerException
          //   507	555	683	java/lang/NullPointerException
          //   558	580	683	java/lang/NullPointerException
          //   589	614	683	java/lang/NullPointerException
          //   614	631	683	java/lang/NullPointerException
          //   638	680	683	java/lang/NullPointerException
          //   691	733	683	java/lang/NullPointerException
          //   736	778	683	java/lang/NullPointerException
          //   781	805	683	java/lang/NullPointerException
          //   816	838	683	java/lang/NullPointerException
          //   843	848	683	java/lang/NullPointerException
          //   848	972	683	java/lang/NullPointerException
          //   972	1027	683	java/lang/NullPointerException
          //   1027	1069	683	java/lang/NullPointerException
          //   1069	1781	683	java/lang/NullPointerException
          //   1781	1829	683	java/lang/NullPointerException
          //   1831	1858	683	java/lang/NullPointerException
          //   1861	1921	683	java/lang/NullPointerException
          //   1921	1926	683	java/lang/NullPointerException
          //   1931	1979	683	java/lang/NullPointerException
          //   1983	2048	683	java/lang/NullPointerException
          //   2052	2064	683	java/lang/NullPointerException
          //   2068	2123	683	java/lang/NullPointerException
          //   2123	2138	683	java/lang/NullPointerException
          //   2141	2146	683	java/lang/NullPointerException
          //   2151	2156	683	java/lang/NullPointerException
          //   558	580	808	android/content/pm/PackageManager$NameNotFoundException
          //   467	486	841	android/content/pm/PackageManager$NameNotFoundException
          //   491	505	841	android/content/pm/PackageManager$NameNotFoundException
          //   507	555	841	android/content/pm/PackageManager$NameNotFoundException
          //   589	614	841	android/content/pm/PackageManager$NameNotFoundException
          //   614	631	841	android/content/pm/PackageManager$NameNotFoundException
          //   816	838	841	android/content/pm/PackageManager$NameNotFoundException
          //   972	1027	2139	java/lang/Exception
          //   1069	1781	2149	android/content/pm/PackageManager$NameNotFoundException
          //   1931	1979	2159	java/lang/Exception
          //   2068	2123	2159	java/lang/Exception
          //   1983	2048	2164	java/lang/Exception
          //   2052	2064	2164	java/lang/Exception
          //   1781	1829	2169	java/lang/Exception
          //   1861	1921	2169	java/lang/Exception
          //   1831	1858	2174	java/lang/Exception
          //   558	580	2179	java/lang/Exception
        }
      }).start();
      localObject1 = new AlertDlg(listAppsFragment.frag.getContext(), true).setView(localLinearLayout).create();
      ((Dialog)localObject1).setCancelable(true);
      ((Dialog)localObject1).setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramAnonymousDialogInterface) {}
      });
      return (Dialog)localObject1;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        localNameNotFoundException.printStackTrace();
      }
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      for (;;)
      {
        localOutOfMemoryError.printStackTrace();
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void showDialog()
  {
    if (this.dialog == null) {
      this.dialog = onCreateDialog();
    }
    if (this.dialog != null) {
      this.dialog.show();
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */