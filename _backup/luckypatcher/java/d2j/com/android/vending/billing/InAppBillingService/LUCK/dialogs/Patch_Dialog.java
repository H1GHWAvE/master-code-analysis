package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.PkgListItem;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.android.vending.billing.InAppBillingService.LUCK.patchActivity;
import com.chelpus.Utils;
import java.io.PrintStream;

public class Patch_Dialog
{
  Dialog dialog = null;
  
  public void dismiss()
  {
    if (this.dialog != null)
    {
      this.dialog.dismiss();
      this.dialog = null;
    }
  }
  
  public Dialog onCreateDialog()
  {
    System.out.println("Patch Dialog create.");
    if ((listAppsFragment.frag == null) || (listAppsFragment.frag.getContext() == null)) {
      dismiss();
    }
    LinearLayout localLinearLayout = (LinearLayout)View.inflate(listAppsFragment.frag.getContext(), 2130968625, null);
    Object localObject = (TextView)((LinearLayout)localLinearLayout.findViewById(2131558596).findViewById(2131558597)).findViewById(2131558600);
    try
    {
      listAppsFragment locallistAppsFragment = listAppsFragment.frag;
      listAppsFragment.patch_dialog_text_builder((TextView)localObject, false);
      localObject = new AlertDlg(listAppsFragment.frag.getContext());
      ((AlertDlg)localObject).setIcon(2130837548);
      ((AlertDlg)localObject).setTitle(Utils.getText(2131165186));
      ((AlertDlg)localObject).setCancelable(true).setPositiveButton(Utils.getText(17039370), null).setNeutralButton(2131165510, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          new Thread(new Runnable()
          {
            public void run()
            {
              try
              {
                Utils.kill(listAppsFragment.pli.pkgName);
                Utils.run_all("killall " + listAppsFragment.pli.pkgName);
                listAppsFragment.runToMainStatic(new Runnable()
                {
                  public void run()
                  {
                    Intent localIntent = listAppsFragment.getPkgMng().getLaunchIntentForPackage(listAppsFragment.pli.pkgName);
                    if (listAppsFragment.patchAct != null) {
                      listAppsFragment.patchAct.startActivity(localIntent);
                    }
                  }
                });
                return;
              }
              catch (Exception localException)
              {
                Toast.makeText(listAppsFragment.getInstance(), Utils.getText(2131165437), 1).show();
              }
            }
          }).start();
          Patch_Dialog.this.dismiss();
        }
      }).setView(localLinearLayout).create();
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void showDialog()
  {
    if (this.dialog == null) {
      this.dialog = onCreateDialog();
    }
    if (this.dialog != null) {
      this.dialog.show();
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */