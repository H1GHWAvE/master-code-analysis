package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;

public class Progress_Dialog
{
  public static ProgressDlg dialog;
  Dialog dialog2 = null;
  FragmentManager fm = null;
  String message = "";
  String title = "";
  
  public static Progress_Dialog newInstance()
  {
    return new Progress_Dialog();
  }
  
  public void dismiss()
  {
    if (this.dialog2 != null)
    {
      this.dialog2.dismiss();
      this.dialog2 = null;
    }
  }
  
  public boolean isShowing()
  {
    return (this.dialog2 != null) && (this.dialog2.isShowing());
  }
  
  public Dialog onCreateDialog()
  {
    dialog = new ProgressDlg(listAppsFragment.frag.getContext());
    if (this.title.equals("")) {
      this.title = Utils.getText(2131165747);
    }
    dialog.setTitle(this.title);
    if (this.message.equals("")) {
      this.message = Utils.getText(2131165515);
    }
    dialog.setMessage(this.message);
    dialog.setCancelable(true);
    dialog.setOnCancelListener(new DialogInterface.OnCancelListener()
    {
      public void onCancel(DialogInterface paramAnonymousDialogInterface)
      {
        if (listAppsFragment.su) {
          Utils.exitRoot();
        }
      }
    });
    return dialog.create();
  }
  
  public void setCancelable(boolean paramBoolean)
  {
    if (this.dialog2 != null) {
      this.dialog2.setCancelable(paramBoolean);
    }
  }
  
  public void setIndeterminate(boolean paramBoolean)
  {
    if (dialog == null) {
      onCreateDialog();
    }
    if (!paramBoolean) {
      dialog.setIncrementStyle();
    }
    for (;;)
    {
      listAppsFragment.frag.getContext().runOnUiThread(new Runnable()
      {
        public void run()
        {
          listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
        }
      });
      return;
      dialog.setDefaultStyle();
    }
  }
  
  public void setMax(int paramInt)
  {
    if (dialog == null) {
      onCreateDialog();
    }
    dialog.setMax(paramInt);
    listAppsFragment.frag.getContext().runOnUiThread(new Runnable()
    {
      public void run()
      {
        listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
      }
    });
  }
  
  public void setMessage(String paramString)
  {
    if (dialog == null) {
      onCreateDialog();
    }
    this.message = paramString;
    dialog.setMessage(this.message);
    listAppsFragment.frag.getContext().runOnUiThread(new Runnable()
    {
      public void run()
      {
        listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
      }
    });
  }
  
  public void setProgress(int paramInt)
  {
    if (dialog == null) {
      onCreateDialog();
    }
    dialog.setProgress(paramInt);
    listAppsFragment.frag.getContext().runOnUiThread(new Runnable()
    {
      public void run()
      {
        listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
      }
    });
  }
  
  public void setTitle(String paramString)
  {
    if (dialog == null) {
      onCreateDialog();
    }
    this.title = paramString;
    dialog.setTitle(this.title);
    listAppsFragment.frag.getContext().runOnUiThread(new Runnable()
    {
      public void run()
      {
        listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
      }
    });
  }
  
  public void showDialog()
  {
    if (this.dialog2 == null) {
      this.dialog2 = onCreateDialog();
    }
    if (this.dialog2 != null) {
      this.dialog2.show();
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */