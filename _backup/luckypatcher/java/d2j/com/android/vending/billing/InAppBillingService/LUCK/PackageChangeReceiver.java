package com.android.vending.billing.InAppBillingService.LUCK;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget;
import com.android.vending.licensing.ILicensingService;
import com.chelpus.Utils;
import com.google.android.finsky.billing.iab.InAppBillingService;
import com.google.android.finsky.billing.iab.MarketBillingService;
import com.google.android.finsky.billing.iab.google.util.IInAppBillingService;
import com.google.android.finsky.services.LicensingService;
import com.google.android.vending.licensing.util.Base64;
import com.google.android.vending.licensing.util.Base64DecoderException;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PackageChangeReceiver
  extends BroadcastReceiver
{
  static ServiceConnection mServiceConn;
  static ServiceConnection mServiceConnL;
  boolean hackedBilling = false;
  Handler handler = null;
  IInAppBillingService mService;
  ILicensingService mServiceL;
  boolean mSetupDone = false;
  int responseCode = 255;
  
  private void cleanupService()
  {
    if (this.mServiceL != null) {}
    try
    {
      listAppsFragment.getInstance().unbindService(mServiceConnL);
      this.mServiceL = null;
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;) {}
    }
  }
  
  public String backup(String paramString)
  {
    if (testSD())
    {
      new File(listAppsFragment.basepath + "/Backup/").mkdirs();
      try
      {
        String str;
        if (listAppsFragment.getConfig().getInt("apkname", 0) != 0) {
          str = listAppsFragment.basepath + "/Backup/" + paramString + ".ver." + listAppsFragment.getPkgMng().getPackageInfo(paramString, 0).versionName.replaceAll(" ", ".") + ".build." + listAppsFragment.getPkgMng().getPackageInfo(paramString, 0).versionCode + ".apk";
        }
        for (;;)
        {
          if (new File(str).exists()) {
            new File(str).delete();
          }
          try
          {
            Utils.copyFile(new File(listAppsFragment.getPkgMng().getPackageInfo(paramString, 0).applicationInfo.sourceDir), new File(str));
            if (!new File(str).exists())
            {
              new Utils("").cmdRoot(new String[] { "dd if=" + listAppsFragment.getPkgMng().getPackageInfo(paramString, 0).applicationInfo.sourceDir + " of=" + str });
              Utils.run_all("chmod 777 " + str);
            }
            if (new File(str).exists())
            {
              this.handler.post(new Runnable()
              {
                public void run()
                {
                  Toast.makeText(listAppsFragment.getInstance(), Utils.getText(2131165217) + " " + listAppsFragment.basepath + "/Backup/", 1).show();
                }
              });
              return str;
              str = listAppsFragment.basepath + "/Backup/" + listAppsFragment.getPkgMng().getPackageInfo(paramString, 0).applicationInfo.loadLabel(listAppsFragment.getPkgMng()).toString() + ".ver." + listAppsFragment.getPkgMng().getPackageInfo(paramString, 0).versionName.replaceAll(" ", ".") + ".build." + listAppsFragment.getPkgMng().getPackageInfo(paramString, 0).versionCode + ".apk";
            }
          }
          catch (Exception localException)
          {
            for (;;)
            {
              Utils.copyFile(listAppsFragment.getPkgMng().getPackageInfo(paramString, 0).applicationInfo.sourceDir, str, false, false);
              localException.printStackTrace();
            }
          }
        }
        return "";
      }
      catch (PackageManager.NameNotFoundException paramString)
      {
        paramString.printStackTrace();
        return "";
        this.handler.post(new Runnable()
        {
          public void run()
          {
            Toast.makeText(listAppsFragment.getInstance(), Utils.getText(2131165748) + ":" + Utils.getText(2131165432), 1).show();
          }
        });
        return "";
      }
      catch (Exception paramString)
      {
        paramString.printStackTrace();
      }
    }
  }
  
  public void connectToBilling()
  {
    Intent localIntent;
    if (this.mSetupDone) {
      localIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
    }
    try
    {
      if (mServiceConn != null) {
        listAppsFragment.getInstance().unbindService(mServiceConn);
      }
      this.mSetupDone = false;
      listAppsFragment.getInstance().bindService(localIntent, mServiceConn, 1);
      mServiceConn = new ServiceConnection()
      {
        public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
        {
          System.out.println("Billing service try to connect.");
          if (!paramAnonymousComponentName.getPackageName().equals(listAppsFragment.class.getPackage().getName()))
          {
            System.out.println("Firmware not support hacked billing");
            if (!listAppsFragment.su)
            {
              Utils.showSystemWindow(Utils.getText(2131165748), Utils.getText(2131165787), new View.OnClickListener()new View.OnClickListener
              {
                public void onClick(View paramAnonymous2View)
                {
                  Intent localIntent;
                  if (Build.VERSION.SDK_INT >= 9)
                  {
                    localIntent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", Uri.parse("package:com.android.vending"));
                    localIntent.setFlags(131072);
                    localIntent.setFlags(268435456);
                    listAppsFragment.getInstance().startActivity(localIntent);
                  }
                  for (;;)
                  {
                    ((WindowManager)listAppsFragment.getInstance().getSystemService("window")).removeView(paramAnonymous2View.getRootView());
                    return;
                    localIntent = new Intent("android.intent.action.VIEW");
                    localIntent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
                    localIntent.putExtra("com.android.settings.ApplicationPkgName", "com.android.vending");
                    localIntent.putExtra("pkg", "com.android.vending");
                    localIntent.setFlags(131072);
                    localIntent.setFlags(268435456);
                    listAppsFragment.getInstance().startActivity(localIntent);
                  }
                }
              }, new View.OnClickListener()
              {
                public void onClick(View paramAnonymous2View)
                {
                  ((WindowManager)listAppsFragment.getInstance().getSystemService("window")).removeView(paramAnonymous2View.getRootView());
                }
              });
              listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 2, 1);
              listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 2, 1);
            }
            for (;;)
            {
              paramAnonymousComponentName = new Intent(listAppsFragment.getInstance(), inapp_widget.class);
              paramAnonymousComponentName.setAction(inapp_widget.ACTION_WIDGET_RECEIVER_Updater);
              listAppsFragment.getInstance().sendBroadcast(paramAnonymousComponentName);
              try
              {
                listAppsFragment.getInstance().unbindService(PackageChangeReceiver.mServiceConn);
                return;
              }
              catch (Exception paramAnonymousComponentName)
              {
                paramAnonymousComponentName.printStackTrace();
                return;
              }
              new Thread(new Runnable()
              {
                public void run()
                {
                  Utils.market_billing_services(false);
                }
              }).start();
            }
          }
          System.out.println("Billing service connected.");
          try
          {
            listAppsFragment.getInstance().unbindService(PackageChangeReceiver.mServiceConn);
            return;
          }
          catch (Exception paramAnonymousComponentName)
          {
            paramAnonymousComponentName.printStackTrace();
          }
        }
        
        public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
        {
          System.out.println("Billing service disconnected.");
          PackageChangeReceiver.this.mSetupDone = false;
          PackageChangeReceiver.this.mService = null;
        }
      };
      localIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
      if (!listAppsFragment.getPkgMng().queryIntentServices(localIntent, 0).isEmpty())
      {
        listAppsFragment.getInstance().bindService(localIntent, mServiceConn, 1);
        return;
      }
    }
    catch (Exception localException)
    {
      do
      {
        for (;;)
        {
          localException.printStackTrace();
        }
        System.out.println("Billing service unavailable on device.");
      } while (!listAppsFragment.su);
      Utils.market_billing_services(true);
      listAppsFragment.getInstance().bindService(localIntent, mServiceConn, 1);
      localIntent = new Intent(listAppsFragment.getInstance(), inapp_widget.class);
      localIntent.setAction(inapp_widget.ACTION_WIDGET_RECEIVER_Updater);
      listAppsFragment.getInstance().sendBroadcast(localIntent);
    }
  }
  
  public void connectToLicensing()
  {
    mServiceConnL = new ServiceConnection()
    {
      public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
      {
        System.out.println("Licensing service try to connect.");
        if (!paramAnonymousComponentName.getPackageName().equals(listAppsFragment.class.getPackage().getName()))
        {
          if (!listAppsFragment.su) {
            break label68;
          }
          new Thread(new Runnable()
          {
            public void run()
            {
              Utils.market_licensing_services(false);
            }
          }).start();
        }
        for (;;)
        {
          System.out.println("Firmware not support lvl emulation");
          try
          {
            listAppsFragment.getInstance().unbindService(PackageChangeReceiver.mServiceConnL);
            return;
          }
          catch (Exception paramAnonymousComponentName)
          {
            label68:
            paramAnonymousComponentName.printStackTrace();
          }
          Utils.showSystemWindow(Utils.getText(2131165748), Utils.getText(2131165787), new View.OnClickListener()new View.OnClickListener
          {
            public void onClick(View paramAnonymous2View)
            {
              Intent localIntent;
              if (Build.VERSION.SDK_INT >= 9)
              {
                localIntent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", Uri.parse("package:com.android.vending"));
                localIntent.setFlags(131072);
                localIntent.setFlags(268435456);
                listAppsFragment.getInstance().startActivity(localIntent);
              }
              for (;;)
              {
                ((WindowManager)listAppsFragment.getInstance().getSystemService("window")).removeView(paramAnonymous2View.getRootView());
                return;
                localIntent = new Intent("android.intent.action.VIEW");
                localIntent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
                localIntent.putExtra("com.android.settings.ApplicationPkgName", "com.android.vending");
                localIntent.putExtra("pkg", "com.android.vending");
                localIntent.setFlags(131072);
                localIntent.setFlags(268435456);
                listAppsFragment.getInstance().startActivity(localIntent);
              }
            }
          }, new View.OnClickListener()
          {
            public void onClick(View paramAnonymous2View)
            {
              ((WindowManager)listAppsFragment.getInstance().getSystemService("window")).removeView(paramAnonymous2View.getRootView());
            }
          });
          listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 2, 1);
        }
      }
      
      public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
      {
        System.out.println("Licensing service disconnected.");
        PackageChangeReceiver.this.mServiceL = null;
      }
    };
    if (this.mServiceL == null) {
      try
      {
        Intent localIntent = new Intent(new String(Base64.decode("Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U=")));
        if (listAppsFragment.getInstance().bindService(localIntent, mServiceConnL, 1)) {
          return;
        }
        cleanupService();
        return;
      }
      catch (SecurityException localSecurityException)
      {
        cleanupService();
        return;
      }
      catch (Base64DecoderException localBase64DecoderException)
      {
        localBase64DecoderException.printStackTrace();
        cleanupService();
      }
    }
  }
  
  String getPackageName(Intent paramIntent)
  {
    paramIntent = paramIntent.getData();
    if (paramIntent != null) {
      return paramIntent.getSchemeSpecificPart();
    }
    return null;
  }
  
  public void onReceive(final Context paramContext, final Intent paramIntent)
  {
    this.handler = new Handler();
    new Thread(new Runnable()
    {
      public void run()
      {
        System.out.println(paramIntent.getAction());
        Object localObject1;
        if (paramIntent.getAction().equals("android.intent.action.PACKAGE_CHANGED"))
        {
          localObject1 = new Intent(paramContext, AppDisablerWidget.class);
          ((Intent)localObject1).setAction(AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater);
          paramContext.sendBroadcast((Intent)localObject1);
          listAppsFragment.init();
        }
        for (;;)
        {
          Object localObject3;
          int i2;
          int j;
          int i1;
          int k;
          int m;
          int n;
          Object localObject4;
          int i3;
          int i4;
          try
          {
            System.out.println(paramIntent.getData().toString());
            if (PackageChangeReceiver.this.getPackageName(paramIntent).equals(listAppsFragment.class.getPackage().getName()))
            {
              localObject1 = paramIntent.getStringArrayExtra("android.intent.extra.changed_component_name_list");
              if ((localObject1 != null) && (localObject1.length > 0) && (listAppsFragment.frag != null))
              {
                System.out.println("update adapt " + localObject1[0]);
                boolean bool = localObject1[0].contains("com.android.vending.billing.InAppBillingService.LUCK.MainActivity");
                if (bool) {
                  try
                  {
                    if (!listAppsFragment.su) {
                      continue;
                    }
                    localObject1 = new Intent("android.intent.action.MAIN");
                    ((Intent)localObject1).addCategory("android.intent.category.HOME");
                    ((Intent)localObject1).addCategory("android.intent.category.DEFAULT");
                    localObject1 = listAppsFragment.getPkgMng().queryIntentActivities((Intent)localObject1, 0).iterator();
                    if (!((Iterator)localObject1).hasNext()) {
                      continue;
                    }
                    localObject3 = (ResolveInfo)((Iterator)localObject1).next();
                    if (((ResolveInfo)localObject3).activityInfo == null) {
                      continue;
                    }
                    new Thread(new Runnable()
                    {
                      public void run()
                      {
                        Utils.kill(this.val$pn);
                        Utils.kill(this.val$pkg);
                      }
                    }).start();
                    continue;
                    if (PackageChangeReceiver.this.getPackageName(paramIntent).equals("com.android.vending")) {
                      continue;
                    }
                  }
                  catch (Exception localException1)
                  {
                    localException1.printStackTrace();
                  }
                }
              }
            }
            if (PackageChangeReceiver.this.getPackageName(paramIntent).equals("com.google.android.gms"))
            {
              arrayOfString = paramIntent.getStringArrayExtra("android.intent.extra.changed_component_name_list");
              if ((arrayOfString != null) && (arrayOfString.length > 0))
              {
                if ((PackageChangeReceiver.this.getPackageName(paramIntent).equals("com.google.android.gms")) && (listAppsFragment.frag != null))
                {
                  System.out.println("update adapt " + arrayOfString[0]);
                  listAppsFragment.frag.runToMain(new Runnable()
                  {
                    public void run()
                    {
                      listAppsFragment.removeDialogLP(11);
                    }
                  });
                }
                if ((listAppsFragment.adapt != null) && (listAppsFragment.frag != null))
                {
                  System.out.println("update adapt " + arrayOfString[0]);
                  listAppsFragment.frag.runToMain(new Runnable()
                  {
                    public void run()
                    {
                      listAppsFragment.removeDialogLP(11);
                      listAppsFragment.adapt.notifyDataSetChanged();
                    }
                  });
                }
              }
            }
            if (!PackageChangeReceiver.this.getPackageName(paramIntent).equals(listAppsFragment.getInstance().getPackageName())) {
              break label3696;
            }
            String[] arrayOfString = paramIntent.getStringArrayExtra("android.intent.extra.changed_component_name_list");
            if ((arrayOfString == null) || (arrayOfString.length <= 0)) {
              break label3696;
            }
            i2 = 0;
            j = 0;
            i1 = 0;
            k = 0;
            int i5 = arrayOfString.length;
            int i = 0;
            if (i >= i5) {
              break label3628;
            }
            localObject3 = arrayOfString[i];
            if (!((String)localObject3).equals("com.google.android.finsky.billing.iab.MarketBillingService"))
            {
              m = i2;
              n = i1;
              if (!((String)localObject3).equals("com.google.android.finsky.billing.iab.InAppBillingService")) {}
            }
            else
            {
              System.out.println((String)localObject3);
              localObject4 = new Intent(listAppsFragment.getInstance(), inapp_widget.class);
              ((Intent)localObject4).setPackage(listAppsFragment.getInstance().getPackageName());
              ((Intent)localObject4).setAction(inapp_widget.ACTION_WIDGET_RECEIVER_Updater);
              listAppsFragment.getInstance().sendBroadcast((Intent)localObject4);
              if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class)) != 1) {
                break label3609;
              }
              m = 1;
              n = i1;
            }
            i3 = k;
            i4 = j;
            if (((String)localObject3).equals("com.licensinghack.LicensingService"))
            {
              System.out.println((String)localObject3);
              localObject3 = new Intent(listAppsFragment.getInstance(), lvl_widget.class);
              ((Intent)localObject3).setPackage(listAppsFragment.getInstance().getPackageName());
              ((Intent)localObject3).setAction(lvl_widget.ACTION_WIDGET_RECEIVER_Updater);
              listAppsFragment.getInstance().sendBroadcast((Intent)localObject3);
              i1 = listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class));
              if (i1 != 1) {
                break label3619;
              }
              i4 = 1;
              i3 = k;
            }
            label766:
            i += 1;
            i2 = m;
            i1 = n;
            k = i3;
            j = i4;
            continue;
            listAppsFragment.frag.runToMain(new Runnable()
            {
              public void run()
              {
                listAppsFragment.removeDialogLP(11);
              }
            });
            continue;
            if (!paramIntent.getAction().equals("android.intent.action.PACKAGE_REPLACED")) {
              break label1432;
            }
          }
          catch (RuntimeException localRuntimeException1)
          {
            localRuntimeException1.printStackTrace();
          }
          label811:
          final Object localObject2 = new Intent(paramContext, AppDisablerWidget.class);
          ((Intent)localObject2).setAction(AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater);
          paramContext.sendBroadcast((Intent)localObject2);
          listAppsFragment.init();
          try
          {
            System.out.println(paramIntent.getData().toString());
            localObject2 = PackageChangeReceiver.this.getPackageName(paramIntent);
            listAppsFragment.getConfig().edit().putBoolean((String)localObject2, false).commit();
            if (listAppsFragment.su)
            {
              if (new File("/data/app/" + (String)localObject2 + "-1.odex").exists()) {
                Utils.run_all("rm /data/app/" + (String)localObject2 + "-1.odex");
              }
              if (new File("/data/app/" + (String)localObject2 + "-2.odex").exists()) {
                Utils.run_all("rm /data/app/" + (String)localObject2 + "-2.odex");
              }
              if (new File("/data/app/" + (String)localObject2 + ".odex").exists()) {
                Utils.run_all("rm /data/app/" + (String)localObject2 + ".odex");
              }
            }
            if ((((String)localObject2).contains("com.android.vending")) || (((String)localObject2).contains(paramContext.getPackageName())))
            {
              if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(paramContext, LicensingService.class)) == 2)
              {
                Utils.market_licensing_services(true);
                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 2, 1);
                label1204:
                if ((listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(paramContext, MarketBillingService.class)) != 2) && (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(paramContext, InAppBillingService.class)) != 2)) {
                  break label3867;
                }
                Utils.market_billing_services(true);
                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 2, 1);
                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 2, 1);
              }
            }
            else {
              label1298:
              if (listAppsFragment.getConfig().getBoolean("manual_path", false)) {
                listAppsFragment.days = listAppsFragment.getConfig().getInt("days_on_up", 1);
              }
            }
          }
          catch (RuntimeException localRuntimeException3)
          {
            try
            {
              localObject2 = new PkgListItem(listAppsFragment.getInstance(), PackageChangeReceiver.this.getPackageName(paramIntent), listAppsFragment.days, false);
              if (listAppsFragment.database == null) {
                listAppsFragment.database = new DatabaseHelper(listAppsFragment.getInstance());
              }
              listAppsFragment.database.savePackage((PkgListItem)localObject2);
              if ((listAppsFragment.desktop_launch) && (listAppsFragment.plia != null) && (listAppsFragment.frag != null) && (listAppsFragment.frag.getActivity() != null)) {
                listAppsFragment.frag.getActivity().runOnUiThread(new Runnable()
                {
                  public void run()
                  {
                    if (listAppsFragment.plia.checkItem(localObject2.pkgName)) {
                      listAppsFragment.plia.updateItem(localObject2.pkgName);
                    }
                    for (;;)
                    {
                      listAppsFragment.plia.notifyDataSetChanged();
                      listAppsFragment.plia.sort();
                      return;
                      listAppsFragment.plia.add(localObject2);
                    }
                  }
                });
              }
              label1428:
              listAppsFragment.refresh = true;
              label1432:
              if (paramIntent.getAction().equals("android.intent.action.PACKAGE_ADDED"))
              {
                localObject2 = new Intent(paramContext, AppDisablerWidget.class);
                ((Intent)localObject2).setAction(AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater);
                paramContext.sendBroadcast((Intent)localObject2);
                listAppsFragment.init();
              }
            }
            catch (Exception localRuntimeException3)
            {
              try
              {
                localObject4 = PackageChangeReceiver.this.getPackageName(paramIntent);
                listAppsFragment.getConfig().edit().putBoolean((String)localObject4, false).commit();
                if (listAppsFragment.getConfig().getBoolean("switch_auto_backup_apk", false))
                {
                  System.out.println("Backup app on update");
                  if (!listAppsFragment.getConfig().getBoolean("switch_auto_backup_apk_only_gp", false)) {
                    PackageChangeReceiver.this.backup((String)localObject4);
                  }
                }
                if (listAppsFragment.getConfig().getBoolean("switch_auto_backup_apk_only_gp", false)) {
                  System.out.println("Backup app on update");
                }
              }
              catch (RuntimeException localRuntimeException3)
              {
                try
                {
                  if (listAppsFragment.getInstance().getPackageManager().getInstallerPackageName((String)localObject4).equals("com.android.vending")) {
                    PackageChangeReceiver.this.backup((String)localObject4);
                  }
                  if (listAppsFragment.handlerToast == null) {
                    listAppsFragment.handlerToast = PackageChangeReceiver.this.handler;
                  }
                  listAppsFragment.days = listAppsFragment.getConfig().getInt("days_on_up", 1);
                  final PkgListItem localPkgListItem = new PkgListItem(listAppsFragment.getInstance(), PackageChangeReceiver.this.getPackageName(paramIntent), listAppsFragment.days, false);
                  localObject2 = "";
                  try
                  {
                    localObject3 = listAppsFragment.getInstance().getPackageManager().getPackageInfo(localPkgListItem.pkgName, 0).applicationInfo.sourceDir;
                    localObject2 = localObject3;
                  }
                  catch (PackageManager.NameNotFoundException localNameNotFoundException)
                  {
                    for (;;)
                    {
                      label2223:
                      label2317:
                      label2424:
                      label3609:
                      label3619:
                      label3628:
                      label3696:
                      label3867:
                      localNameNotFoundException.printStackTrace();
                    }
                  }
                  if ((localPkgListItem.system) && (((String)localObject2).startsWith("/data")) && (listAppsFragment.getConfig().getBoolean("switch_auto_integrate_update", false)) && (listAppsFragment.su))
                  {
                    System.out.println("Integrate update to system on update app");
                    localObject3 = new ArrayList();
                    ((ArrayList)localObject3).add(localPkgListItem);
                    listAppsFragment.integrate_to_system((ArrayList)localObject3, false, true);
                  }
                  if ((!localPkgListItem.system) && (((String)localObject2).startsWith("/data/")) && (listAppsFragment.getConfig().getBoolean("switch_auto_move_to_sd", false)) && (listAppsFragment.su)) {
                    new Utils("").cmdRoot(new String[] { "pm install -r -s -i com.android.vending " + (String)localObject2 });
                  }
                  if ((!localPkgListItem.system) && (((String)localObject2).startsWith("/mnt/")) && (listAppsFragment.getConfig().getBoolean("switch_auto_move_to_internal", false)) && (listAppsFragment.su)) {
                    new Utils("").cmdRoot(new String[] { "pm install -r -f -i com.android.vending " + (String)localObject2 });
                  }
                  if (listAppsFragment.su)
                  {
                    if (new File("/data/app/" + (String)localObject4 + "-1.odex").exists()) {
                      Utils.run_all("rm /data/app/" + (String)localObject4 + "-1.odex");
                    }
                    if (new File("/data/app/" + (String)localObject4 + "-2.odex").exists()) {
                      Utils.run_all("rm /data/app/" + (String)localObject4 + "-2.odex");
                    }
                    if (new File("/data/app/" + (String)localObject4 + ".odex").exists()) {
                      Utils.run_all("rm /data/app/" + (String)localObject4 + ".odex");
                    }
                  }
                  if (((String)localObject4).contains("com.android.vending"))
                  {
                    if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(paramContext, LicensingService.class)) == 2)
                    {
                      Utils.market_licensing_services(true);
                      listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 2, 1);
                      if ((listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(paramContext, MarketBillingService.class)) != 2) && (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(paramContext, InAppBillingService.class)) != 2)) {
                        break label3975;
                      }
                      Utils.market_billing_services(true);
                      listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 2, 1);
                      listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 2, 1);
                    }
                  }
                  else
                  {
                    if (listAppsFragment.getConfig().getBoolean("manual_path", false))
                    {
                      listAppsFragment.days = listAppsFragment.getConfig().getInt("days_on_up", 1);
                      if (listAppsFragment.database == null) {
                        listAppsFragment.database = new DatabaseHelper(listAppsFragment.getInstance());
                      }
                      listAppsFragment.database.savePackage(localPkgListItem);
                      if ((listAppsFragment.desktop_launch) && (listAppsFragment.plia != null) && (listAppsFragment.frag != null) && (listAppsFragment.frag.getActivity() != null)) {
                        listAppsFragment.frag.getActivity().runOnUiThread(new Runnable()
                        {
                          public void run()
                          {
                            if (listAppsFragment.plia.checkItem(localPkgListItem.pkgName)) {
                              listAppsFragment.plia.updateItem(localPkgListItem.pkgName);
                            }
                            for (;;)
                            {
                              listAppsFragment.plia.notifyDataSetChanged();
                              listAppsFragment.plia.sort();
                              return;
                              listAppsFragment.plia.add(localPkgListItem);
                            }
                          }
                        });
                      }
                    }
                    listAppsFragment.refresh = true;
                    if (paramIntent.getAction().equals("android.intent.action.PACKAGE_REMOVED"))
                    {
                      localObject2 = new Intent(paramContext, AppDisablerWidget.class);
                      ((Intent)localObject2).setAction(AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater);
                      paramContext.sendBroadcast((Intent)localObject2);
                      System.out.println("delete trigger " + paramIntent.getBooleanExtra("android.intent.extra.REPLACING", false));
                      if (!paramIntent.getBooleanExtra("android.intent.extra.REPLACING", false)) {
                        listAppsFragment.init();
                      }
                    }
                  }
                  for (;;)
                  {
                    try
                    {
                      localObject2 = PackageChangeReceiver.this.getPackageName(paramIntent);
                      listAppsFragment.getConfig().edit().remove((String)localObject2).commit();
                      if (listAppsFragment.su)
                      {
                        if (listAppsFragment.api >= 21)
                        {
                          if (new File("/data/app/" + (String)localObject2 + "-1/arm").exists()) {
                            Utils.run_all("rm -rf /data/app/" + (String)localObject2 + "-1");
                          }
                          if (new File("/data/app/" + (String)localObject2 + "-2/arm").exists()) {
                            Utils.run_all("rm -rf /data/app/" + (String)localObject2 + "-2");
                          }
                          if (new File("/data/app/" + (String)localObject2 + "-3/arm").exists()) {
                            Utils.run_all("rm -rf /data/app/" + (String)localObject2 + "-3");
                          }
                          if (new File("/data/app/" + (String)localObject2 + "-4/arm").exists()) {
                            Utils.run_all("rm -rf /data/app/" + (String)localObject2 + "-4");
                          }
                          if (new File("/data/app/" + (String)localObject2 + "-1/x86").exists()) {
                            Utils.run_all("rm -rf /data/app/" + (String)localObject2 + "-1");
                          }
                          if (new File("/data/app/" + (String)localObject2 + "-2/x86").exists()) {
                            Utils.run_all("rm -rf /data/app/" + (String)localObject2 + "-2");
                          }
                          if (new File("/data/app/" + (String)localObject2 + "-3/x86").exists()) {
                            Utils.run_all("rm -rf /data/app/" + (String)localObject2 + "-3");
                          }
                          if (new File("/data/app/" + (String)localObject2 + "-4/x86").exists()) {
                            Utils.run_all("rm -rf /data/app/" + (String)localObject2 + "-4");
                          }
                        }
                        if (new File("/data/app/" + (String)localObject2 + "-1.odex").exists()) {
                          Utils.run_all("rm /data/app/" + (String)localObject2 + "-1.odex");
                        }
                        if (new File("/data/app/" + (String)localObject2 + "-2.odex").exists()) {
                          Utils.run_all("rm /data/app/" + (String)localObject2 + "-2.odex");
                        }
                        if (new File("/data/app/" + (String)localObject2 + ".odex").exists()) {
                          Utils.run_all("rm /data/app/" + (String)localObject2 + ".odex");
                        }
                      }
                      if (((String)localObject2).contains("com.android.vending"))
                      {
                        if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(paramContext, LicensingService.class)) != 2) {
                          continue;
                        }
                        Utils.market_licensing_services(true);
                        listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 2, 1);
                        if ((listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(paramContext, MarketBillingService.class)) != 2) && (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(paramContext, InAppBillingService.class)) != 2)) {
                          continue;
                        }
                        Utils.market_billing_services(true);
                        listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 2, 1);
                        listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 2, 1);
                      }
                    }
                    catch (RuntimeException localRuntimeException4)
                    {
                      label3975:
                      localRuntimeException4.printStackTrace();
                      continue;
                      listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 1, 1);
                      listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 1, 1);
                      continue;
                    }
                    listAppsFragment.refresh = true;
                    localObject2 = PackageChangeReceiver.this.getPackageName(paramIntent);
                    if (listAppsFragment.database == null) {
                      listAppsFragment.database = new DatabaseHelper(listAppsFragment.getInstance());
                    }
                    listAppsFragment.database.deletePackage((String)localObject2);
                    if ((listAppsFragment.desktop_launch) && (listAppsFragment.plia != null) && (listAppsFragment.frag != null) && (listAppsFragment.frag.getActivity() != null)) {
                      listAppsFragment.frag.getActivity().runOnUiThread(new Runnable()
                      {
                        public void run()
                        {
                          if (listAppsFragment.plia.checkItem(localObject2)) {
                            listAppsFragment.plia.remove(localObject2);
                          }
                          listAppsFragment.plia.notifyDataSetChanged();
                          listAppsFragment.plia.sort();
                        }
                      });
                    }
                    if ((!listAppsFragment.patchOnBoot) && (!listAppsFragment.desktop_launch)) {
                      System.out.println("LP - exit.");
                    }
                    return;
                    n = 1;
                    m = i2;
                    break;
                    i3 = 1;
                    i4 = j;
                    break label766;
                    if (i2 != 0) {
                      PackageChangeReceiver.this.connectToBilling();
                    }
                    if (j != 0) {
                      PackageChangeReceiver.this.connectToLicensing();
                    }
                    if (i1 != 0) {
                      new Thread(new Runnable()
                      {
                        public void run()
                        {
                          if (listAppsFragment.su) {
                            Utils.market_billing_services(true);
                          }
                        }
                      }).start();
                    }
                    if (k != 0) {
                      new Thread(new Runnable()
                      {
                        public void run()
                        {
                          if (listAppsFragment.su) {
                            Utils.market_licensing_services(true);
                          }
                        }
                      }).start();
                    }
                    listAppsFragment.days = listAppsFragment.getConfig().getInt("days_on_up", 1);
                    try
                    {
                      localObject2 = new PkgListItem(listAppsFragment.getInstance(), PackageChangeReceiver.this.getPackageName(paramIntent), listAppsFragment.days, false);
                      if (listAppsFragment.database == null) {
                        listAppsFragment.database = new DatabaseHelper(listAppsFragment.getInstance());
                      }
                      listAppsFragment.database.savePackage((PkgListItem)localObject2);
                      if ((!listAppsFragment.desktop_launch) || (listAppsFragment.plia == null) || (listAppsFragment.frag == null) || (listAppsFragment.frag.getActivity() == null)) {
                        break label811;
                      }
                      listAppsFragment.frag.getActivity().runOnUiThread(new Runnable()
                      {
                        public void run()
                        {
                          if (listAppsFragment.plia.checkItem(localObject2.pkgName))
                          {
                            listAppsFragment.plia.updateItem(localObject2.pkgName);
                            listAppsFragment.plia.sort();
                          }
                          for (;;)
                          {
                            listAppsFragment.plia.notifyDataSetChanged();
                            listAppsFragment.plia.sort();
                            return;
                            listAppsFragment.plia.add(localObject2);
                          }
                        }
                      });
                    }
                    catch (Exception localException2)
                    {
                      localException2.printStackTrace();
                      System.out.println("Item dont create. And dont add to database.");
                    }
                    break label811;
                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 1, 1);
                    break label1204;
                    localRuntimeException2 = localRuntimeException2;
                    localRuntimeException2.printStackTrace();
                    break label1432;
                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 1, 1);
                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 1, 1);
                    break label1298;
                    localException3 = localException3;
                    localException3.printStackTrace();
                    System.out.println("Item dont create. And dont add to database.");
                    break label1428;
                    localRuntimeException3 = localRuntimeException3;
                    localRuntimeException3.printStackTrace();
                    break label2424;
                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 1, 1);
                    break label2223;
                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 1, 1);
                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 1, 1);
                    break label2317;
                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 1, 1);
                  }
                }
                catch (Exception localException4)
                {
                  for (;;) {}
                }
              }
            }
          }
        }
      }
    }).start();
  }
  
  public boolean testSD()
  {
    boolean bool = false;
    try
    {
      if (listAppsFragment.basepath.startsWith(listAppsFragment.getInstance().getDir("sdcard", 0).getAbsolutePath()))
      {
        System.out.println("LuckyAppManager (sdcard to internal memory): " + listAppsFragment.basepath);
        return false;
      }
      if (!new File(listAppsFragment.basepath).exists()) {
        new File(listAppsFragment.basepath).mkdirs();
      }
      if (!new File(listAppsFragment.basepath).exists())
      {
        System.out.println("LuckyAppManager (sdcard directory not found and not created): " + listAppsFragment.basepath);
        return false;
      }
      new File(listAppsFragment.basepath + "/tmp.txt").delete();
      System.out.println("LuckyAppManager (sdcard test create file): " + listAppsFragment.basepath);
      if (new File(listAppsFragment.basepath + "/tmp.txt").createNewFile())
      {
        System.out.println("LuckyAppManager (sdcard test create file true): " + listAppsFragment.basepath);
        new File(listAppsFragment.basepath + "/tmp.txt").delete();
        bool = true;
      }
      return bool;
    }
    catch (IOException localIOException) {}
    return false;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */