package com.google.android.vending.licensing;

public abstract interface DeviceLimiter
{
  public abstract int isDeviceAllowed(String paramString);
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/google/android/vending/licensing/DeviceLimiter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */