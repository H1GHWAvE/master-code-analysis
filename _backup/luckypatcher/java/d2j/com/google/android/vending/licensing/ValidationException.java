package com.google.android.vending.licensing;

public class ValidationException
  extends Exception
{
  private static final long serialVersionUID = 1L;
  
  public ValidationException() {}
  
  public ValidationException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/google/android/vending/licensing/ValidationException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */