package com.google.android.vending.licensing;

public class StrictPolicy
  implements Policy
{
  private int mLastResponse = 291;
  
  public boolean allowAccess()
  {
    return this.mLastResponse == 256;
  }
  
  public void processServerResponse(int paramInt, ResponseData paramResponseData)
  {
    this.mLastResponse = paramInt;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/google/android/vending/licensing/StrictPolicy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */