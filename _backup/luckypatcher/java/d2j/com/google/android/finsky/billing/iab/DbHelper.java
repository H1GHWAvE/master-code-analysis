package com.google.android.finsky.billing.iab;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import com.android.vending.billing.InAppBillingService.LUCK.PkgListItem;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import java.io.PrintStream;
import java.util.List;

public class DbHelper
  extends SQLiteOpenHelper
{
  public static Context contextdb = null;
  static final String dbName = "BillingRestoreTransactions";
  public static boolean getPackage = false;
  static final String itemID = "itemID";
  static final String pData = "Data";
  static final String pSignature = "Signature";
  static String packageTable = "Packages";
  public static boolean savePackage = false;
  
  public DbHelper(Context paramContext)
  {
    super(paramContext, "BillingRestoreTransactions", null, 41);
    contextdb = paramContext;
    packageTable = packageTable;
    try
    {
      if (listAppsFragment.billing_db == null) {}
      for (listAppsFragment.billing_db = getWritableDatabase();; listAppsFragment.billing_db = getWritableDatabase())
      {
        System.out.println("SQLite base version is " + listAppsFragment.billing_db.getVersion());
        if (listAppsFragment.billing_db.getVersion() != 41)
        {
          System.out.println("SQL delete and recreate.");
          listAppsFragment.billing_db.execSQL("DROP TABLE IF EXISTS " + packageTable);
          onCreate(listAppsFragment.billing_db);
        }
        onCreate(listAppsFragment.billing_db);
        return;
        listAppsFragment.billing_db.close();
      }
      return;
    }
    catch (SQLiteException paramContext)
    {
      paramContext.printStackTrace();
    }
  }
  
  public DbHelper(Context paramContext, String paramString)
  {
    super(paramContext, "BillingRestoreTransactions", null, 41);
    contextdb = paramContext;
    packageTable = paramString;
    try
    {
      if (listAppsFragment.billing_db == null) {}
      for (listAppsFragment.billing_db = getWritableDatabase();; listAppsFragment.billing_db = getWritableDatabase())
      {
        listAppsFragment.billing_db = listAppsFragment.billing_db;
        if (listAppsFragment.billing_db.getVersion() != 41)
        {
          System.out.println("SQL delete and recreate.");
          listAppsFragment.billing_db.execSQL("DROP TABLE IF EXISTS " + paramString);
          onCreate(listAppsFragment.billing_db);
        }
        onCreate(listAppsFragment.billing_db);
        return;
        listAppsFragment.billing_db.close();
      }
      return;
    }
    catch (SQLiteException paramContext)
    {
      paramContext.printStackTrace();
    }
  }
  
  public void deleteAll() {}
  
  public void deleteItem(ItemsListItem paramItemsListItem)
  {
    try
    {
      listAppsFragment.billing_db.delete("'" + packageTable + "'", "itemID = '" + paramItemsListItem.itemID + "'", null);
      return;
    }
    catch (Exception paramItemsListItem)
    {
      System.out.println("LuckyPatcher-Error: deletePackage " + paramItemsListItem);
    }
  }
  
  public void deleteItem(String paramString)
  {
    try
    {
      listAppsFragment.billing_db.delete("'" + packageTable + "'", "itemID = '" + paramString + "'", null);
      return;
    }
    catch (Exception paramString)
    {
      System.out.println("LuckyPatcher-Error: deletePackage " + paramString);
    }
  }
  
  /* Error */
  public java.util.ArrayList<ItemsListItem> getItems()
  {
    // Byte code:
    //   0: new 134	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 135	java/util/ArrayList:<init>	()V
    //   7: astore_1
    //   8: aload_1
    //   9: invokevirtual 138	java/util/ArrayList:clear	()V
    //   12: getstatic 49	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:billing_db	Landroid/database/sqlite/SQLiteDatabase;
    //   15: new 61	java/lang/StringBuilder
    //   18: dup
    //   19: invokespecial 63	java/lang/StringBuilder:<init>	()V
    //   22: ldc 113
    //   24: invokevirtual 69	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   27: getstatic 29	com/google/android/finsky/billing/iab/DbHelper:packageTable	Ljava/lang/String;
    //   30: invokevirtual 69	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   33: ldc 113
    //   35: invokevirtual 69	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   38: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   41: iconst_3
    //   42: anewarray 140	java/lang/String
    //   45: dup
    //   46: iconst_0
    //   47: ldc 15
    //   49: aastore
    //   50: dup
    //   51: iconst_1
    //   52: ldc 18
    //   54: aastore
    //   55: dup
    //   56: iconst_2
    //   57: ldc 21
    //   59: aastore
    //   60: aconst_null
    //   61: aconst_null
    //   62: aconst_null
    //   63: aconst_null
    //   64: aconst_null
    //   65: invokevirtual 144	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   68: astore_2
    //   69: aload_2
    //   70: invokeinterface 150 1 0
    //   75: pop
    //   76: aload_2
    //   77: aload_2
    //   78: ldc 15
    //   80: invokeinterface 154 2 0
    //   85: invokeinterface 158 2 0
    //   90: astore_3
    //   91: aload_1
    //   92: new 117	com/google/android/finsky/billing/iab/ItemsListItem
    //   95: dup
    //   96: aload_3
    //   97: aload_2
    //   98: aload_2
    //   99: ldc 18
    //   101: invokeinterface 161 2 0
    //   106: invokeinterface 158 2 0
    //   111: aload_2
    //   112: aload_2
    //   113: ldc 21
    //   115: invokeinterface 161 2 0
    //   120: invokeinterface 158 2 0
    //   125: invokespecial 164	com/google/android/finsky/billing/iab/ItemsListItem:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   128: invokevirtual 168	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   131: pop
    //   132: aload_2
    //   133: invokeinterface 171 1 0
    //   138: ifne -62 -> 76
    //   141: aload_2
    //   142: invokeinterface 172 1 0
    //   147: iconst_0
    //   148: putstatic 33	com/google/android/finsky/billing/iab/DbHelper:getPackage	Z
    //   151: aload_1
    //   152: areturn
    //   153: astore_3
    //   154: aload_2
    //   155: invokeinterface 172 1 0
    //   160: goto -13 -> 147
    //   163: astore_2
    //   164: iconst_0
    //   165: putstatic 33	com/google/android/finsky/billing/iab/DbHelper:getPackage	Z
    //   168: getstatic 59	java/lang/System:out	Ljava/io/PrintStream;
    //   171: new 61	java/lang/StringBuilder
    //   174: dup
    //   175: invokespecial 63	java/lang/StringBuilder:<init>	()V
    //   178: ldc -82
    //   180: invokevirtual 69	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   183: aload_2
    //   184: invokevirtual 128	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   187: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   190: invokevirtual 88	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   193: aload_1
    //   194: areturn
    //   195: astore_3
    //   196: goto -64 -> 132
    //   199: astore_3
    //   200: goto -68 -> 132
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	203	0	this	DbHelper
    //   7	187	1	localArrayList	java.util.ArrayList
    //   68	87	2	localCursor	android.database.Cursor
    //   163	21	2	localException1	Exception
    //   90	7	3	str	String
    //   153	1	3	localException2	Exception
    //   195	1	3	localException3	Exception
    //   199	1	3	localIllegalArgumentException	IllegalArgumentException
    // Exception table:
    //   from	to	target	type
    //   132	147	153	java/lang/Exception
    //   12	76	163	java/lang/Exception
    //   147	151	163	java/lang/Exception
    //   154	160	163	java/lang/Exception
    //   76	91	195	java/lang/Exception
    //   91	132	195	java/lang/Exception
    //   91	132	199	java/lang/IllegalArgumentException
  }
  
  public boolean isOpen()
  {
    return listAppsFragment.billing_db.isOpen();
  }
  
  public void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS '" + packageTable + "' (" + "itemID" + " TEXT PRIMARY KEY, " + "Data" + " TEXT, " + "Signature" + " TEXT" + ");");
  }
  
  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + packageTable + "'");
    onCreate(paramSQLiteDatabase);
  }
  
  public void saveItem(ItemsListItem paramItemsListItem)
    throws SQLiteException
  {
    try
    {
      savePackage = true;
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("itemID", paramItemsListItem.itemID);
      localContentValues.put("Data", paramItemsListItem.pData);
      localContentValues.put("Signature", paramItemsListItem.pSignature);
      try
      {
        listAppsFragment.billing_db.insertOrThrow("'" + packageTable + "'", "itemID", localContentValues);
        savePackage = false;
        savePackage = false;
        return;
      }
      catch (Exception paramItemsListItem)
      {
        for (;;)
        {
          listAppsFragment.billing_db.replace("'" + packageTable + "'", null, localContentValues);
        }
      }
      return;
    }
    catch (Exception paramItemsListItem)
    {
      savePackage = false;
      System.out.println("LuckyPatcher-Error: savePackage " + paramItemsListItem);
    }
  }
  
  public void updatePackage(List<PkgListItem> paramList) {}
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/google/android/finsky/billing/iab/DbHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */