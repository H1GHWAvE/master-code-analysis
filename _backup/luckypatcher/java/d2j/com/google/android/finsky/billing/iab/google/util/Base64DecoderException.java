package com.google.android.finsky.billing.iab.google.util;

public class Base64DecoderException
  extends Exception
{
  private static final long serialVersionUID = 1L;
  
  public Base64DecoderException() {}
  
  public Base64DecoderException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/google/android/finsky/billing/iab/google/util/Base64DecoderException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */