package com.google.android.finsky.billing.iab;

public class ItemsListItem
{
  public String itemID;
  public String pData;
  public String pSignature;
  
  public ItemsListItem(String paramString1, String paramString2, String paramString3)
  {
    this.itemID = paramString1;
    this.pData = paramString2;
    this.pSignature = paramString3;
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/google/android/finsky/billing/iab/ItemsListItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */