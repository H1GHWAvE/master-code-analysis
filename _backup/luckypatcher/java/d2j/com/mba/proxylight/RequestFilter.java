package com.mba.proxylight;

public abstract interface RequestFilter
{
  public abstract boolean filter(Request paramRequest);
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/mba/proxylight/RequestFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */