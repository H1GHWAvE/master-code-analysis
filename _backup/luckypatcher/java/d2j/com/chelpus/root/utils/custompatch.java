package com.chelpus.root.utils;

import com.android.vending.billing.InAppBillingService.LUCK.PatchesItem;
import com.android.vending.billing.InAppBillingService.LUCK.SearchItem;
import com.chelpus.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.nio.BufferUnderflowException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

public class custompatch
{
  public static boolean ART = false;
  static final int BUFFER = 2048;
  public static final byte[] MAGIC = { 100, 101, 121, 10, 48, 51, 53, 0 };
  public static boolean OdexPatch = false;
  public static int adler = 0;
  public static boolean armv7 = false;
  public static ArrayList<File> arrayFile2 = new ArrayList();
  private static final int beginTag = 0;
  public static ArrayList<File> classesFiles;
  private static final int classesTag = 1;
  public static boolean convert = false;
  private static String dataBase;
  private static boolean dataBaseExist = false;
  public static String dir;
  public static String dir2;
  public static String dirapp;
  private static final int endTag = 4;
  private static final int fileInApkTag = 14;
  public static boolean fixunpack = false;
  public static boolean goodResult = false;
  private static String group;
  private static final int libTagALL = 2;
  private static final int libTagARMEABI = 6;
  private static final int libTagARMEABIV7A = 7;
  private static final int libTagMIPS = 8;
  private static final int libTagx86 = 9;
  public static File localFile2;
  public static String log;
  public static boolean manualpatch = false;
  public static boolean multidex = false;
  public static boolean multilib_patch = false;
  public static boolean odex = false;
  private static final int odexTag = 10;
  public static boolean odexpatch = false;
  private static final int odexpatchTag = 11;
  private static final int otherfilesTag = 3;
  private static final int packageTag = 5;
  private static ArrayList<PatchesItem> pat = null;
  public static boolean patchteil = false;
  public static String pkgName;
  public static String sddir;
  private static ArrayList<Byte> search;
  private static String searchStr;
  private static ArrayList<SearchItem> ser = null;
  private static final int set_copy_file_Tag = 15;
  private static final int set_permissions_Tag = 13;
  private static final int sqlTag = 12;
  public static boolean system;
  public static int tag;
  public static String uid;
  public static boolean unpack;
  private static boolean withFramework;
  
  static
  {
    search = null;
    patchteil = false;
    unpack = false;
    fixunpack = false;
    manualpatch = false;
    odex = false;
    dir = "/sdcard/";
    dir2 = "/sdcard/";
    dirapp = "/data/app/";
    sddir = "/data/app/";
    uid = "";
    system = false;
    odexpatch = false;
    OdexPatch = false;
    armv7 = false;
    ART = false;
    convert = false;
    dataBaseExist = false;
    dataBase = "";
    searchStr = "";
    group = "";
    withFramework = true;
    pkgName = "";
    log = "";
    classesFiles = new ArrayList();
    multidex = false;
  }
  
  public static void addToLog(String paramString)
  {
    log = log + paramString + "\n";
  }
  
  public static void clearTemp()
  {
    File localFile1;
    do
    {
      try
      {
        Object localObject = new File(dir + "/AndroidManifest.xml");
        if (((File)localObject).exists()) {
          ((File)localObject).delete();
        }
        if ((classesFiles != null) && (classesFiles.size() > 0))
        {
          localObject = classesFiles.iterator();
          while (((Iterator)localObject).hasNext())
          {
            File localFile3 = (File)((Iterator)localObject).next();
            localFile3 = new File(dir + "/" + localFile3.getName());
            if (localFile3.exists()) {
              localFile3.delete();
            }
          }
        }
        localFile1 = new File(dir + "/classes.dex");
      }
      catch (Exception localException)
      {
        addToLog("" + localException.toString());
        return;
      }
      if (localFile1.exists()) {
        localFile1.delete();
      }
      localFile1 = new File(dir + "/classes1.dex");
      if (localFile1.exists()) {
        localFile1.delete();
      }
      localFile1 = new File(dir + "/classes2.dex");
      if (localFile1.exists()) {
        localFile1.delete();
      }
      localFile1 = new File(dir + "/classes3.dex");
      if (localFile1.exists()) {
        localFile1.delete();
      }
      localFile1 = new File(dir + "/classes4.dex");
      if (localFile1.exists()) {
        localFile1.delete();
      }
      localFile1 = new File(dir + "/classes5.dex");
      if (localFile1.exists()) {
        localFile1.delete();
      }
      localFile1 = new File(dir + "/classes6.dex");
      if (localFile1.exists()) {
        localFile1.delete();
      }
      localFile1 = new File(dir + "/classes.dex.apk");
      if (localFile1.exists()) {
        localFile1.delete();
      }
      localFile1 = new File(dir + "/classes.dex.dex");
    } while (!localFile1.exists());
    localFile1.delete();
  }
  
  /* Error */
  public static void main(String[] paramArrayOfString)
  {
    // Byte code:
    //   0: new 174	java/lang/StringBuilder
    //   3: dup
    //   4: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   7: ldc_w 256
    //   10: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13: new 6	com/chelpus/root/utils/custompatch$1
    //   16: dup
    //   17: invokespecial 257	com/chelpus/root/utils/custompatch$1:<init>	()V
    //   20: invokevirtual 261	java/lang/Object:getClass	()Ljava/lang/Class;
    //   23: invokevirtual 266	java/lang/Class:getEnclosingClass	()Ljava/lang/Class;
    //   26: invokevirtual 267	java/lang/Class:getName	()Ljava/lang/String;
    //   29: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   32: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   35: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   38: aconst_null
    //   39: invokestatic 273	com/chelpus/Utils:startRootJava	(Ljava/lang/Object;)V
    //   42: aload_0
    //   43: iconst_0
    //   44: aaload
    //   45: putstatic 158	com/chelpus/root/utils/custompatch:pkgName	Ljava/lang/String;
    //   48: getstatic 158	com/chelpus/root/utils/custompatch:pkgName	Ljava/lang/String;
    //   51: invokestatic 276	com/chelpus/Utils:kill	(Ljava/lang/String;)V
    //   54: aload_0
    //   55: iconst_2
    //   56: aaload
    //   57: ldc_w 278
    //   60: invokestatic 282	com/chelpus/Utils:remount	(Ljava/lang/String;Ljava/lang/String;)Z
    //   63: pop
    //   64: ldc -124
    //   66: ldc -124
    //   68: invokestatic 288	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   71: pop
    //   72: aload_0
    //   73: bipush 9
    //   75: aaload
    //   76: ifnull +20 -> 96
    //   79: aload_0
    //   80: bipush 9
    //   82: aaload
    //   83: ldc_w 289
    //   86: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   89: ifeq +7 -> 96
    //   92: iconst_1
    //   93: putstatic 144	com/chelpus/root/utils/custompatch:ART	Z
    //   96: aload_0
    //   97: bipush 10
    //   99: aaload
    //   100: ifnull +10 -> 110
    //   103: aload_0
    //   104: bipush 10
    //   106: aaload
    //   107: putstatic 134	com/chelpus/root/utils/custompatch:uid	Ljava/lang/String;
    //   110: new 190	java/io/File
    //   113: dup
    //   114: aload_0
    //   115: iconst_4
    //   116: aaload
    //   117: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   120: invokevirtual 299	java/io/File:listFiles	()[Ljava/io/File;
    //   123: astore 19
    //   125: aload 19
    //   127: arraylength
    //   128: istore_2
    //   129: iconst_0
    //   130: istore_1
    //   131: iload_1
    //   132: iload_2
    //   133: if_icmpge +137 -> 270
    //   136: aload 19
    //   138: iload_1
    //   139: aaload
    //   140: astore 20
    //   142: aload 20
    //   144: invokevirtual 302	java/io/File:isFile	()Z
    //   147: ifeq +51 -> 198
    //   150: aload 20
    //   152: invokevirtual 223	java/io/File:getName	()Ljava/lang/String;
    //   155: ldc_w 304
    //   158: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   161: ifne +37 -> 198
    //   164: aload 20
    //   166: invokevirtual 223	java/io/File:getName	()Ljava/lang/String;
    //   169: ldc_w 310
    //   172: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   175: ifne +23 -> 198
    //   178: aload 20
    //   180: invokevirtual 223	java/io/File:getName	()Ljava/lang/String;
    //   183: ldc_w 312
    //   186: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   189: ifne +9 -> 198
    //   192: aload 20
    //   194: invokevirtual 201	java/io/File:delete	()Z
    //   197: pop
    //   198: iload_1
    //   199: iconst_1
    //   200: iadd
    //   201: istore_1
    //   202: goto -71 -> 131
    //   205: astore 19
    //   207: aload 19
    //   209: invokevirtual 315	java/lang/Exception:printStackTrace	()V
    //   212: goto -148 -> 64
    //   215: astore_0
    //   216: aload_0
    //   217: invokevirtual 315	java/lang/Exception:printStackTrace	()V
    //   220: getstatic 160	com/chelpus/root/utils/custompatch:log	Ljava/lang/String;
    //   223: invokestatic 319	com/chelpus/Utils:sendFromRootCP	(Ljava/lang/String;)Z
    //   226: pop
    //   227: iconst_0
    //   228: invokestatic 325	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   231: putstatic 331	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:startUnderRoot	Ljava/lang/Boolean;
    //   234: invokestatic 334	com/chelpus/Utils:exitFromRootJava	()V
    //   237: return
    //   238: astore 19
    //   240: ldc_w 336
    //   243: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   246: iconst_0
    //   247: putstatic 156	com/chelpus/root/utils/custompatch:withFramework	Z
    //   250: goto -178 -> 72
    //   253: astore 19
    //   255: aload 19
    //   257: invokevirtual 315	java/lang/Exception:printStackTrace	()V
    //   260: goto -150 -> 110
    //   263: astore 19
    //   265: aload 19
    //   267: invokevirtual 315	java/lang/Exception:printStackTrace	()V
    //   270: aload_0
    //   271: iconst_3
    //   272: aaload
    //   273: putstatic 130	com/chelpus/root/utils/custompatch:sddir	Ljava/lang/String;
    //   276: aload_0
    //   277: iconst_4
    //   278: aaload
    //   279: putstatic 122	com/chelpus/root/utils/custompatch:dir	Ljava/lang/String;
    //   282: aload_0
    //   283: iconst_4
    //   284: aaload
    //   285: putstatic 124	com/chelpus/root/utils/custompatch:dir2	Ljava/lang/String;
    //   288: aload_0
    //   289: iconst_2
    //   290: aaload
    //   291: putstatic 128	com/chelpus/root/utils/custompatch:dirapp	Ljava/lang/String;
    //   294: invokestatic 338	com/chelpus/root/utils/custompatch:clearTemp	()V
    //   297: ldc -124
    //   299: astore 19
    //   301: ldc -124
    //   303: astore 28
    //   305: ldc -124
    //   307: astore 34
    //   309: iconst_0
    //   310: istore_2
    //   311: iconst_0
    //   312: istore 13
    //   314: iconst_0
    //   315: istore_1
    //   316: aload_0
    //   317: bipush 6
    //   319: aaload
    //   320: ldc_w 340
    //   323: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   326: ifeq +7 -> 333
    //   329: iconst_0
    //   330: putstatic 136	com/chelpus/root/utils/custompatch:system	Z
    //   333: aload_0
    //   334: bipush 6
    //   336: aaload
    //   337: ldc_w 341
    //   340: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   343: ifeq +7 -> 350
    //   346: iconst_1
    //   347: putstatic 136	com/chelpus/root/utils/custompatch:system	Z
    //   350: getstatic 136	com/chelpus/root/utils/custompatch:system	Z
    //   353: ifeq +70 -> 423
    //   356: new 190	java/io/File
    //   359: dup
    //   360: getstatic 128	com/chelpus/root/utils/custompatch:dirapp	Ljava/lang/String;
    //   363: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   366: astore 20
    //   368: new 190	java/io/File
    //   371: dup
    //   372: getstatic 128	com/chelpus/root/utils/custompatch:dirapp	Ljava/lang/String;
    //   375: iconst_1
    //   376: invokestatic 345	com/chelpus/Utils:getPlaceForOdex	(Ljava/lang/String;Z)Ljava/lang/String;
    //   379: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   382: astore 21
    //   384: aload 20
    //   386: invokevirtual 198	java/io/File:exists	()Z
    //   389: ifeq +34 -> 423
    //   392: aload 21
    //   394: invokevirtual 198	java/io/File:exists	()Z
    //   397: ifeq +26 -> 423
    //   400: aload 20
    //   402: invokestatic 349	com/chelpus/Utils:classes_test	(Ljava/io/File;)Z
    //   405: ifne +18 -> 423
    //   408: iconst_1
    //   409: putstatic 138	com/chelpus/root/utils/custompatch:odexpatch	Z
    //   412: aload 21
    //   414: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   417: ldc_w 353
    //   420: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   423: iconst_0
    //   424: putstatic 140	com/chelpus/root/utils/custompatch:OdexPatch	Z
    //   427: new 355	java/io/FileInputStream
    //   430: dup
    //   431: aload_0
    //   432: iconst_1
    //   433: aaload
    //   434: invokespecial 356	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   437: astore 20
    //   439: new 358	java/io/BufferedReader
    //   442: dup
    //   443: new 360	java/io/InputStreamReader
    //   446: dup
    //   447: aload 20
    //   449: ldc_w 362
    //   452: invokespecial 365	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/lang/String;)V
    //   455: invokespecial 368	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   458: astore 21
    //   460: aload 21
    //   462: invokevirtual 371	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   465: astore 22
    //   467: aload 22
    //   469: ifnull +3135 -> 3604
    //   472: aload 22
    //   474: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   477: ldc_w 376
    //   480: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   483: ifeq +7 -> 490
    //   486: iconst_1
    //   487: putstatic 140	com/chelpus/root/utils/custompatch:OdexPatch	Z
    //   490: aload 22
    //   492: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   495: ldc_w 378
    //   498: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   501: ifeq -41 -> 460
    //   504: iconst_1
    //   505: putstatic 142	com/chelpus/root/utils/custompatch:armv7	Z
    //   508: goto -48 -> 460
    //   511: astore 20
    //   513: aload_0
    //   514: iconst_2
    //   515: aaload
    //   516: invokestatic 381	com/chelpus/root/utils/custompatch:searchDalvik	(Ljava/lang/String;)V
    //   519: new 355	java/io/FileInputStream
    //   522: dup
    //   523: aload_0
    //   524: iconst_1
    //   525: aaload
    //   526: invokespecial 356	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   529: astore 35
    //   531: new 360	java/io/InputStreamReader
    //   534: dup
    //   535: aload 35
    //   537: ldc_w 362
    //   540: invokespecial 365	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/lang/String;)V
    //   543: astore 36
    //   545: new 358	java/io/BufferedReader
    //   548: dup
    //   549: aload 36
    //   551: invokespecial 368	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   554: astore 37
    //   556: sipush 2000
    //   559: anewarray 291	java/lang/String
    //   562: astore 38
    //   564: aconst_null
    //   565: astore 25
    //   567: aconst_null
    //   568: astore 26
    //   570: ldc -124
    //   572: astore 20
    //   574: ldc -124
    //   576: astore 33
    //   578: ldc -124
    //   580: astore 27
    //   582: iconst_1
    //   583: istore 15
    //   585: iconst_1
    //   586: istore 16
    //   588: iconst_0
    //   589: istore 7
    //   591: iconst_0
    //   592: istore 4
    //   594: iconst_0
    //   595: istore_3
    //   596: iconst_0
    //   597: istore 6
    //   599: iconst_0
    //   600: istore 5
    //   602: iconst_0
    //   603: istore 8
    //   605: ldc -124
    //   607: astore 21
    //   609: ldc -124
    //   611: astore 23
    //   613: ldc -124
    //   615: astore 24
    //   617: new 97	java/util/ArrayList
    //   620: dup
    //   621: invokespecial 100	java/util/ArrayList:<init>	()V
    //   624: putstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   627: new 97	java/util/ArrayList
    //   630: dup
    //   631: invokespecial 100	java/util/ArrayList:<init>	()V
    //   634: putstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   637: new 97	java/util/ArrayList
    //   640: dup
    //   641: invokespecial 100	java/util/ArrayList:<init>	()V
    //   644: putstatic 108	com/chelpus/root/utils/custompatch:search	Ljava/util/ArrayList;
    //   647: iconst_0
    //   648: istore 12
    //   650: aload 37
    //   652: invokevirtual 371	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   655: astore 22
    //   657: aload 22
    //   659: ifnull +8807 -> 9466
    //   662: aload 22
    //   664: astore 29
    //   666: aload 22
    //   668: ldc -124
    //   670: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   673: ifne +13 -> 686
    //   676: aload 22
    //   678: getstatic 158	com/chelpus/root/utils/custompatch:pkgName	Ljava/lang/String;
    //   681: invokestatic 385	com/chelpus/Utils:apply_TAGS	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   684: astore 29
    //   686: aload 38
    //   688: iload 12
    //   690: aload 29
    //   692: aastore
    //   693: aload 38
    //   695: iload 12
    //   697: aaload
    //   698: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   701: ldc_w 387
    //   704: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   707: ifeq +268 -> 975
    //   710: iconst_5
    //   711: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   714: iconst_1
    //   715: putstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   718: new 190	java/io/File
    //   721: dup
    //   722: aload_0
    //   723: iconst_2
    //   724: aaload
    //   725: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   728: astore 30
    //   730: aload 30
    //   732: astore 22
    //   734: aload 30
    //   736: invokevirtual 198	java/io/File:exists	()Z
    //   739: ifne +55 -> 794
    //   742: new 190	java/io/File
    //   745: dup
    //   746: aload_0
    //   747: iconst_2
    //   748: aaload
    //   749: ldc_w 391
    //   752: ldc_w 393
    //   755: invokevirtual 397	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   758: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   761: astore 30
    //   763: aload 30
    //   765: astore 22
    //   767: aload 30
    //   769: invokevirtual 198	java/io/File:exists	()Z
    //   772: ifeq +22 -> 794
    //   775: aload_0
    //   776: iconst_2
    //   777: aload_0
    //   778: iconst_2
    //   779: aaload
    //   780: ldc_w 391
    //   783: ldc_w 393
    //   786: invokevirtual 397	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   789: aastore
    //   790: aload 30
    //   792: astore 22
    //   794: aload 22
    //   796: astore 30
    //   798: aload 22
    //   800: invokevirtual 198	java/io/File:exists	()Z
    //   803: ifne +53 -> 856
    //   806: new 190	java/io/File
    //   809: dup
    //   810: aload_0
    //   811: iconst_2
    //   812: aaload
    //   813: ldc_w 391
    //   816: ldc -36
    //   818: invokevirtual 397	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   821: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   824: astore 22
    //   826: aload 22
    //   828: astore 30
    //   830: aload 22
    //   832: invokevirtual 198	java/io/File:exists	()Z
    //   835: ifeq +21 -> 856
    //   838: aload_0
    //   839: iconst_2
    //   840: aload_0
    //   841: iconst_2
    //   842: aaload
    //   843: ldc_w 391
    //   846: ldc -124
    //   848: invokevirtual 397	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   851: aastore
    //   852: aload 22
    //   854: astore 30
    //   856: aload 30
    //   858: invokestatic 401	com/chelpus/root/utils/custompatch:unzip	(Ljava/io/File;)V
    //   861: getstatic 138	com/chelpus/root/utils/custompatch:odexpatch	Z
    //   864: ifne +111 -> 975
    //   867: getstatic 140	com/chelpus/root/utils/custompatch:OdexPatch	Z
    //   870: ifne +105 -> 975
    //   873: aload_0
    //   874: iconst_2
    //   875: aaload
    //   876: iconst_1
    //   877: invokestatic 345	com/chelpus/Utils:getPlaceForOdex	(Ljava/lang/String;Z)Ljava/lang/String;
    //   880: astore 22
    //   882: new 190	java/io/File
    //   885: dup
    //   886: aload 22
    //   888: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   891: astore 30
    //   893: aload 30
    //   895: invokevirtual 198	java/io/File:exists	()Z
    //   898: ifeq +9 -> 907
    //   901: aload 30
    //   903: invokevirtual 201	java/io/File:delete	()Z
    //   906: pop
    //   907: new 190	java/io/File
    //   910: dup
    //   911: aload 22
    //   913: ldc_w 403
    //   916: ldc_w 405
    //   919: invokevirtual 397	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   922: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   925: astore 30
    //   927: aload 30
    //   929: invokevirtual 198	java/io/File:exists	()Z
    //   932: ifeq +9 -> 941
    //   935: aload 30
    //   937: invokevirtual 201	java/io/File:delete	()Z
    //   940: pop
    //   941: new 190	java/io/File
    //   944: dup
    //   945: aload 22
    //   947: ldc_w 405
    //   950: ldc_w 403
    //   953: invokevirtual 397	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   956: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   959: astore 22
    //   961: aload 22
    //   963: invokevirtual 198	java/io/File:exists	()Z
    //   966: ifeq +9 -> 975
    //   969: aload 22
    //   971: invokevirtual 201	java/io/File:delete	()Z
    //   974: pop
    //   975: iload 4
    //   977: istore 11
    //   979: iload 4
    //   981: ifeq +80 -> 1061
    //   984: aload 38
    //   986: iload 12
    //   988: aaload
    //   989: ldc_w 407
    //   992: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   995: ifne +35 -> 1030
    //   998: aload 38
    //   1000: iload 12
    //   1002: aaload
    //   1003: ldc_w 409
    //   1006: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1009: ifne +21 -> 1030
    //   1012: iload 4
    //   1014: istore 11
    //   1016: aload 38
    //   1018: iload 12
    //   1020: aaload
    //   1021: ldc_w 411
    //   1024: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1027: ifeq +34 -> 1061
    //   1030: new 174	java/lang/StringBuilder
    //   1033: dup
    //   1034: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   1037: ldc -124
    //   1039: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1042: aload 34
    //   1044: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1047: ldc -75
    //   1049: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1052: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1055: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   1058: iconst_0
    //   1059: istore 11
    //   1061: aload 34
    //   1063: astore 30
    //   1065: iload 11
    //   1067: ifeq +33 -> 1100
    //   1070: new 174	java/lang/StringBuilder
    //   1073: dup
    //   1074: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   1077: aload 34
    //   1079: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1082: ldc -75
    //   1084: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1087: aload 38
    //   1089: iload 12
    //   1091: aaload
    //   1092: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1095: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1098: astore 30
    //   1100: iload 8
    //   1102: istore 10
    //   1104: iload_1
    //   1105: istore 14
    //   1107: aload 19
    //   1109: astore 31
    //   1111: iload 5
    //   1113: istore 9
    //   1115: iload 15
    //   1117: istore 18
    //   1119: iload 16
    //   1121: istore 17
    //   1123: aload 38
    //   1125: iload 12
    //   1127: aaload
    //   1128: ldc_w 407
    //   1131: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1134: ifeq +142 -> 1276
    //   1137: iload 8
    //   1139: istore 10
    //   1141: iload_1
    //   1142: istore 14
    //   1144: aload 19
    //   1146: astore 31
    //   1148: iload 5
    //   1150: istore 9
    //   1152: iload 15
    //   1154: istore 18
    //   1156: iload 16
    //   1158: istore 17
    //   1160: aload 38
    //   1162: iload 12
    //   1164: aaload
    //   1165: ldc_w 409
    //   1168: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1171: ifeq +105 -> 1276
    //   1174: iload 8
    //   1176: istore 10
    //   1178: iload_1
    //   1179: istore 14
    //   1181: aload 19
    //   1183: astore 31
    //   1185: iload 5
    //   1187: istore 9
    //   1189: iload 15
    //   1191: istore 18
    //   1193: iload 16
    //   1195: istore 17
    //   1197: getstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   1200: tableswitch	default:+8464->9664, 1:+2417->3617, 2:+3288->4488, 3:+4468->5668, 4:+76->1276, 5:+76->1276, 6:+3524->4724, 7:+3760->4960, 8:+3996->5196, 9:+4232->5432, 10:+3088->4288, 11:+4547->5747, 12:+4736->5936, 13:+4789->5989, 14:+5320->6520, 15:+4942->6142
    //   1276: iload 11
    //   1278: istore 4
    //   1280: aload 38
    //   1282: iload 12
    //   1284: aaload
    //   1285: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   1288: ldc_w 413
    //   1291: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1294: ifeq +10 -> 1304
    //   1297: iconst_0
    //   1298: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   1301: iconst_1
    //   1302: istore 4
    //   1304: aload 38
    //   1306: iload 12
    //   1308: aaload
    //   1309: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   1312: ldc_w 415
    //   1315: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1318: ifeq +50 -> 1368
    //   1321: iconst_1
    //   1322: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   1325: getstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   1328: ifeq +40 -> 1368
    //   1331: new 190	java/io/File
    //   1334: dup
    //   1335: new 174	java/lang/StringBuilder
    //   1338: dup
    //   1339: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   1342: getstatic 122	com/chelpus/root/utils/custompatch:dir	Ljava/lang/String;
    //   1345: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1348: ldc -36
    //   1350: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1353: ldc_w 417
    //   1356: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1359: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1362: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   1365: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   1368: aload 38
    //   1370: iload 12
    //   1372: aaload
    //   1373: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   1376: ldc_w 419
    //   1379: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1382: ifeq +8 -> 1390
    //   1385: bipush 10
    //   1387: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   1390: aload 38
    //   1392: iload 12
    //   1394: aaload
    //   1395: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   1398: ldc_w 421
    //   1401: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1404: ifeq +8 -> 1412
    //   1407: bipush 12
    //   1409: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   1412: aload 21
    //   1414: astore 19
    //   1416: getstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   1419: bipush 12
    //   1421: if_icmpne +526 -> 1947
    //   1424: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   1427: invokevirtual 424	java/util/ArrayList:clear	()V
    //   1430: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   1433: invokevirtual 424	java/util/ArrayList:clear	()V
    //   1436: aload 38
    //   1438: iload 12
    //   1440: aaload
    //   1441: ldc_w 426
    //   1444: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1447: istore 15
    //   1449: aload 21
    //   1451: astore 22
    //   1453: iload 15
    //   1455: ifeq +352 -> 1807
    //   1458: new 428	org/json/JSONObject
    //   1461: dup
    //   1462: aload 38
    //   1464: iload 12
    //   1466: aaload
    //   1467: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   1470: ldc_w 426
    //   1473: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   1476: astore 22
    //   1478: aload 22
    //   1480: astore 21
    //   1482: new 190	java/io/File
    //   1485: dup
    //   1486: aload 22
    //   1488: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   1491: astore 19
    //   1493: aload 22
    //   1495: astore 21
    //   1497: iconst_3
    //   1498: anewarray 291	java/lang/String
    //   1501: dup
    //   1502: iconst_0
    //   1503: ldc_w 435
    //   1506: aastore
    //   1507: dup
    //   1508: iconst_1
    //   1509: ldc_w 437
    //   1512: aastore
    //   1513: dup
    //   1514: iconst_2
    //   1515: aload 22
    //   1517: aastore
    //   1518: invokestatic 440	com/chelpus/Utils:run_all_no_root	([Ljava/lang/String;)V
    //   1521: aload 22
    //   1523: astore 21
    //   1525: getstatic 156	com/chelpus/root/utils/custompatch:withFramework	Z
    //   1528: ifne +48 -> 1576
    //   1531: aload 22
    //   1533: astore 21
    //   1535: aload 19
    //   1537: new 190	java/io/File
    //   1540: dup
    //   1541: new 174	java/lang/StringBuilder
    //   1544: dup
    //   1545: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   1548: getstatic 122	com/chelpus/root/utils/custompatch:dir	Ljava/lang/String;
    //   1551: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1554: ldc -36
    //   1556: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1559: aload 19
    //   1561: invokevirtual 223	java/io/File:getName	()Ljava/lang/String;
    //   1564: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1567: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1570: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   1573: invokestatic 444	com/chelpus/Utils:copyFile	(Ljava/io/File;Ljava/io/File;)V
    //   1576: aload 22
    //   1578: astore 21
    //   1580: aload 19
    //   1582: invokevirtual 198	java/io/File:exists	()Z
    //   1585: ifne +4993 -> 6578
    //   1588: aload 22
    //   1590: astore 21
    //   1592: aconst_null
    //   1593: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   1596: aload 22
    //   1598: astore 21
    //   1600: aload_0
    //   1601: iconst_0
    //   1602: aaload
    //   1603: aload 19
    //   1605: invokevirtual 223	java/io/File:getName	()Ljava/lang/String;
    //   1608: invokestatic 448	com/chelpus/root/utils/custompatch:searchfile	(Ljava/lang/String;Ljava/lang/String;)V
    //   1611: aload 22
    //   1613: astore 21
    //   1615: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   1618: ifnull +4952 -> 6570
    //   1621: aload 22
    //   1623: astore 21
    //   1625: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   1628: invokevirtual 198	java/io/File:exists	()Z
    //   1631: ifeq +4939 -> 6570
    //   1634: aload 22
    //   1636: astore 21
    //   1638: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   1641: astore 19
    //   1643: aload 22
    //   1645: astore 21
    //   1647: iconst_3
    //   1648: anewarray 291	java/lang/String
    //   1651: dup
    //   1652: iconst_0
    //   1653: ldc_w 435
    //   1656: aastore
    //   1657: dup
    //   1658: iconst_1
    //   1659: ldc_w 437
    //   1662: aastore
    //   1663: dup
    //   1664: iconst_2
    //   1665: aload 22
    //   1667: aastore
    //   1668: invokestatic 440	com/chelpus/Utils:run_all_no_root	([Ljava/lang/String;)V
    //   1671: aload 22
    //   1673: astore 21
    //   1675: getstatic 156	com/chelpus/root/utils/custompatch:withFramework	Z
    //   1678: ifne +48 -> 1726
    //   1681: aload 22
    //   1683: astore 21
    //   1685: aload 19
    //   1687: new 190	java/io/File
    //   1690: dup
    //   1691: new 174	java/lang/StringBuilder
    //   1694: dup
    //   1695: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   1698: getstatic 122	com/chelpus/root/utils/custompatch:dir	Ljava/lang/String;
    //   1701: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1704: ldc -36
    //   1706: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1709: aload 19
    //   1711: invokevirtual 223	java/io/File:getName	()Ljava/lang/String;
    //   1714: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1717: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1720: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   1723: invokestatic 444	com/chelpus/Utils:copyFile	(Ljava/io/File;Ljava/io/File;)V
    //   1726: aload 22
    //   1728: astore 21
    //   1730: iconst_1
    //   1731: putstatic 148	com/chelpus/root/utils/custompatch:dataBaseExist	Z
    //   1734: aload 22
    //   1736: astore 21
    //   1738: getstatic 148	com/chelpus/root/utils/custompatch:dataBaseExist	Z
    //   1741: ifeq +15 -> 1756
    //   1744: aload 22
    //   1746: astore 21
    //   1748: aload 19
    //   1750: invokevirtual 451	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   1753: putstatic 150	com/chelpus/root/utils/custompatch:dataBase	Ljava/lang/String;
    //   1756: aload 22
    //   1758: astore 21
    //   1760: ldc_w 453
    //   1763: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   1766: aload 22
    //   1768: astore 21
    //   1770: new 174	java/lang/StringBuilder
    //   1773: dup
    //   1774: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   1777: ldc_w 455
    //   1780: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1783: aload 19
    //   1785: invokevirtual 451	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   1788: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1791: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1794: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   1797: aload 22
    //   1799: astore 21
    //   1801: ldc_w 457
    //   1804: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   1807: aload 22
    //   1809: astore 19
    //   1811: aload 38
    //   1813: iload 12
    //   1815: aaload
    //   1816: ldc_w 459
    //   1819: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1822: ifeq +125 -> 1947
    //   1825: getstatic 148	com/chelpus/root/utils/custompatch:dataBaseExist	Z
    //   1828: istore 15
    //   1830: aload 22
    //   1832: astore 19
    //   1834: iload 15
    //   1836: ifeq +111 -> 1947
    //   1839: aload 22
    //   1841: astore 19
    //   1843: new 428	org/json/JSONObject
    //   1846: dup
    //   1847: aload 38
    //   1849: iload 12
    //   1851: aaload
    //   1852: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   1855: ldc_w 459
    //   1858: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   1861: astore 21
    //   1863: aload 21
    //   1865: astore 19
    //   1867: new 174	java/lang/StringBuilder
    //   1870: dup
    //   1871: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   1874: ldc_w 461
    //   1877: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1880: aload 21
    //   1882: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1885: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1888: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   1891: aload 21
    //   1893: astore 19
    //   1895: getstatic 156	com/chelpus/root/utils/custompatch:withFramework	Z
    //   1898: istore 15
    //   1900: aload 21
    //   1902: astore 19
    //   1904: iload 15
    //   1906: ifeq +41 -> 1947
    //   1909: aload 21
    //   1911: astore 19
    //   1913: getstatic 150	com/chelpus/root/utils/custompatch:dataBase	Ljava/lang/String;
    //   1916: aconst_null
    //   1917: iconst_0
    //   1918: invokestatic 467	android/database/sqlite/SQLiteDatabase:openDatabase	(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;
    //   1921: astore 22
    //   1923: aload 21
    //   1925: astore 19
    //   1927: aload 22
    //   1929: aload 21
    //   1931: invokevirtual 470	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
    //   1934: aload 21
    //   1936: astore 19
    //   1938: aload 22
    //   1940: invokevirtual 473	android/database/sqlite/SQLiteDatabase:close	()V
    //   1943: aload 21
    //   1945: astore 19
    //   1947: iload 6
    //   1949: istore 11
    //   1951: aload 19
    //   1953: astore 21
    //   1955: iload 6
    //   1957: ifeq +108 -> 2065
    //   1960: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   1963: invokevirtual 424	java/util/ArrayList:clear	()V
    //   1966: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   1969: invokevirtual 424	java/util/ArrayList:clear	()V
    //   1972: new 428	org/json/JSONObject
    //   1975: dup
    //   1976: aload 38
    //   1978: iload 12
    //   1980: aaload
    //   1981: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   1984: ldc_w 475
    //   1987: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   1990: astore 21
    //   1992: aload 21
    //   1994: astore 19
    //   1996: ldc_w 453
    //   1999: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   2002: aload 21
    //   2004: astore 19
    //   2006: new 174	java/lang/StringBuilder
    //   2009: dup
    //   2010: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   2013: ldc_w 477
    //   2016: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2019: aload 21
    //   2021: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2024: ldc_w 479
    //   2027: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2030: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2033: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   2036: aload 21
    //   2038: astore 19
    //   2040: ldc_w 457
    //   2043: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   2046: aload 21
    //   2048: astore 19
    //   2050: aload_0
    //   2051: iconst_0
    //   2052: aaload
    //   2053: aload 19
    //   2055: invokestatic 448	com/chelpus/root/utils/custompatch:searchfile	(Ljava/lang/String;Ljava/lang/String;)V
    //   2058: iconst_0
    //   2059: istore 11
    //   2061: aload 19
    //   2063: astore 21
    //   2065: aload 20
    //   2067: astore 19
    //   2069: aload 33
    //   2071: astore 32
    //   2073: iload 9
    //   2075: ifeq +177 -> 2252
    //   2078: aload 38
    //   2080: iload 12
    //   2082: aaload
    //   2083: ldc_w 481
    //   2086: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2089: istore 15
    //   2091: aload 20
    //   2093: astore 22
    //   2095: iload 15
    //   2097: ifeq +39 -> 2136
    //   2100: new 428	org/json/JSONObject
    //   2103: dup
    //   2104: aload 38
    //   2106: iload 12
    //   2108: aaload
    //   2109: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   2112: ldc_w 481
    //   2115: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   2118: astore 19
    //   2120: aload 19
    //   2122: astore 20
    //   2124: aload_0
    //   2125: iconst_0
    //   2126: aaload
    //   2127: aload 20
    //   2129: invokestatic 448	com/chelpus/root/utils/custompatch:searchfile	(Ljava/lang/String;Ljava/lang/String;)V
    //   2132: aload 20
    //   2134: astore 22
    //   2136: aload 22
    //   2138: astore 19
    //   2140: aload 33
    //   2142: astore 32
    //   2144: aload 38
    //   2146: iload 12
    //   2148: aaload
    //   2149: ldc_w 483
    //   2152: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2155: ifeq +97 -> 2252
    //   2158: aload 22
    //   2160: astore 19
    //   2162: aload 33
    //   2164: astore 32
    //   2166: aload 38
    //   2168: iload 12
    //   2170: aaload
    //   2171: ldc_w 411
    //   2174: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2177: ifeq +75 -> 2252
    //   2180: aload 22
    //   2182: astore 19
    //   2184: aload 33
    //   2186: astore 32
    //   2188: aload 38
    //   2190: iload 12
    //   2192: aaload
    //   2193: ldc_w 407
    //   2196: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2199: ifne +53 -> 2252
    //   2202: aload 38
    //   2204: iload 12
    //   2206: aaload
    //   2207: ldc_w 409
    //   2210: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2213: istore 15
    //   2215: aload 22
    //   2217: astore 19
    //   2219: aload 33
    //   2221: astore 32
    //   2223: iload 15
    //   2225: ifne +27 -> 2252
    //   2228: new 428	org/json/JSONObject
    //   2231: dup
    //   2232: aload 38
    //   2234: iload 12
    //   2236: aaload
    //   2237: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   2240: ldc_w 483
    //   2243: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   2246: astore 32
    //   2248: aload 22
    //   2250: astore 19
    //   2252: aload 19
    //   2254: astore 22
    //   2256: aload 27
    //   2258: astore 33
    //   2260: iload 10
    //   2262: ifeq +161 -> 2423
    //   2265: aload 38
    //   2267: iload 12
    //   2269: aaload
    //   2270: ldc_w 481
    //   2273: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2276: istore 15
    //   2278: aload 19
    //   2280: astore 20
    //   2282: iload 15
    //   2284: ifeq +23 -> 2307
    //   2287: new 428	org/json/JSONObject
    //   2290: dup
    //   2291: aload 38
    //   2293: iload 12
    //   2295: aaload
    //   2296: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   2299: ldc_w 481
    //   2302: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   2305: astore 20
    //   2307: aload 20
    //   2309: astore 22
    //   2311: aload 27
    //   2313: astore 33
    //   2315: aload 38
    //   2317: iload 12
    //   2319: aaload
    //   2320: ldc_w 485
    //   2323: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2326: ifeq +97 -> 2423
    //   2329: aload 20
    //   2331: astore 22
    //   2333: aload 27
    //   2335: astore 33
    //   2337: aload 38
    //   2339: iload 12
    //   2341: aaload
    //   2342: ldc_w 411
    //   2345: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2348: ifeq +75 -> 2423
    //   2351: aload 20
    //   2353: astore 22
    //   2355: aload 27
    //   2357: astore 33
    //   2359: aload 38
    //   2361: iload 12
    //   2363: aaload
    //   2364: ldc_w 407
    //   2367: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2370: ifne +53 -> 2423
    //   2373: aload 38
    //   2375: iload 12
    //   2377: aaload
    //   2378: ldc_w 409
    //   2381: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2384: istore 15
    //   2386: aload 20
    //   2388: astore 22
    //   2390: aload 27
    //   2392: astore 33
    //   2394: iload 15
    //   2396: ifne +27 -> 2423
    //   2399: new 428	org/json/JSONObject
    //   2402: dup
    //   2403: aload 38
    //   2405: iload 12
    //   2407: aaload
    //   2408: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   2411: ldc_w 487
    //   2414: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   2417: astore 33
    //   2419: aload 20
    //   2421: astore 22
    //   2423: iload 7
    //   2425: istore_1
    //   2426: aload 21
    //   2428: astore 19
    //   2430: iload 7
    //   2432: ifeq +57 -> 2489
    //   2435: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   2438: invokevirtual 424	java/util/ArrayList:clear	()V
    //   2441: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   2444: invokevirtual 424	java/util/ArrayList:clear	()V
    //   2447: new 428	org/json/JSONObject
    //   2450: dup
    //   2451: aload 38
    //   2453: iload 12
    //   2455: aaload
    //   2456: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   2459: ldc_w 475
    //   2462: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   2465: astore 19
    //   2467: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   2470: invokevirtual 424	java/util/ArrayList:clear	()V
    //   2473: aload_0
    //   2474: iconst_0
    //   2475: aaload
    //   2476: aload 19
    //   2478: aload_0
    //   2479: iconst_2
    //   2480: aaload
    //   2481: invokestatic 491	com/chelpus/root/utils/custompatch:searchlib	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    //   2484: putstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   2487: iconst_0
    //   2488: istore_1
    //   2489: iload 10
    //   2491: istore 5
    //   2493: iload_1
    //   2494: istore 7
    //   2496: aload 38
    //   2498: iload 12
    //   2500: aaload
    //   2501: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   2504: ldc_w 493
    //   2507: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2510: ifeq +27 -> 2537
    //   2513: iconst_2
    //   2514: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   2517: iconst_1
    //   2518: istore 7
    //   2520: iconst_0
    //   2521: istore 11
    //   2523: iconst_0
    //   2524: istore 9
    //   2526: iconst_0
    //   2527: putstatic 118	com/chelpus/root/utils/custompatch:odex	Z
    //   2530: iconst_0
    //   2531: putstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   2534: iconst_0
    //   2535: istore 5
    //   2537: iload 5
    //   2539: istore 10
    //   2541: iload 7
    //   2543: istore 8
    //   2545: iload 11
    //   2547: istore 6
    //   2549: iload 9
    //   2551: istore_1
    //   2552: aload 38
    //   2554: iload 12
    //   2556: aaload
    //   2557: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   2560: ldc_w 495
    //   2563: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2566: ifeq +65 -> 2631
    //   2569: aload_0
    //   2570: bipush 7
    //   2572: aaload
    //   2573: invokevirtual 498	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   2576: ldc_w 500
    //   2579: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   2582: ifne +25 -> 2607
    //   2585: aload_0
    //   2586: bipush 7
    //   2588: aaload
    //   2589: invokevirtual 498	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   2592: ldc_w 502
    //   2595: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   2598: ifeq +4157 -> 6755
    //   2601: getstatic 142	com/chelpus/root/utils/custompatch:armv7	Z
    //   2604: ifne +4151 -> 6755
    //   2607: bipush 6
    //   2609: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   2612: iconst_1
    //   2613: istore 8
    //   2615: iconst_0
    //   2616: istore 6
    //   2618: iconst_0
    //   2619: istore_1
    //   2620: iconst_0
    //   2621: putstatic 118	com/chelpus/root/utils/custompatch:odex	Z
    //   2624: iconst_0
    //   2625: putstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   2628: iconst_0
    //   2629: istore 10
    //   2631: iload 10
    //   2633: istore 11
    //   2635: iload 8
    //   2637: istore 9
    //   2639: iload 6
    //   2641: istore 7
    //   2643: iload_1
    //   2644: istore 5
    //   2646: aload 38
    //   2648: iload 12
    //   2650: aaload
    //   2651: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   2654: ldc_w 378
    //   2657: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2660: ifeq +44 -> 2704
    //   2663: aload_0
    //   2664: bipush 7
    //   2666: aaload
    //   2667: invokevirtual 498	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   2670: ldc_w 502
    //   2673: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   2676: ifeq +4103 -> 6779
    //   2679: bipush 7
    //   2681: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   2684: iconst_1
    //   2685: istore 9
    //   2687: iconst_0
    //   2688: istore 7
    //   2690: iconst_0
    //   2691: istore 5
    //   2693: iconst_0
    //   2694: putstatic 118	com/chelpus/root/utils/custompatch:odex	Z
    //   2697: iconst_0
    //   2698: putstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   2701: iconst_0
    //   2702: istore 11
    //   2704: iload 11
    //   2706: istore 10
    //   2708: iload 9
    //   2710: istore 8
    //   2712: iload 7
    //   2714: istore 6
    //   2716: iload 5
    //   2718: istore_1
    //   2719: aload 38
    //   2721: iload 12
    //   2723: aaload
    //   2724: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   2727: ldc_w 504
    //   2730: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2733: ifeq +43 -> 2776
    //   2736: aload_0
    //   2737: bipush 7
    //   2739: aaload
    //   2740: invokevirtual 498	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   2743: ldc_w 506
    //   2746: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   2749: ifeq +4054 -> 6803
    //   2752: bipush 8
    //   2754: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   2757: iconst_1
    //   2758: istore 8
    //   2760: iconst_0
    //   2761: istore 6
    //   2763: iconst_0
    //   2764: istore_1
    //   2765: iconst_0
    //   2766: putstatic 118	com/chelpus/root/utils/custompatch:odex	Z
    //   2769: iconst_0
    //   2770: putstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   2773: iconst_0
    //   2774: istore 10
    //   2776: iload 10
    //   2778: istore 11
    //   2780: iload 8
    //   2782: istore 9
    //   2784: iload 6
    //   2786: istore 7
    //   2788: iload_1
    //   2789: istore 5
    //   2791: aload 38
    //   2793: iload 12
    //   2795: aaload
    //   2796: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   2799: ldc_w 508
    //   2802: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2805: ifeq +44 -> 2849
    //   2808: aload_0
    //   2809: bipush 7
    //   2811: aaload
    //   2812: invokevirtual 498	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   2815: ldc_w 510
    //   2818: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   2821: ifeq +4006 -> 6827
    //   2824: bipush 9
    //   2826: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   2829: iconst_1
    //   2830: istore 9
    //   2832: iconst_0
    //   2833: istore 7
    //   2835: iconst_0
    //   2836: istore 5
    //   2838: iconst_0
    //   2839: putstatic 118	com/chelpus/root/utils/custompatch:odex	Z
    //   2842: iconst_0
    //   2843: putstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   2846: iconst_0
    //   2847: istore 11
    //   2849: aload 38
    //   2851: iload 12
    //   2853: aaload
    //   2854: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   2857: ldc_w 512
    //   2860: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2863: ifeq +27 -> 2890
    //   2866: iconst_3
    //   2867: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   2870: iconst_0
    //   2871: istore 9
    //   2873: iconst_1
    //   2874: istore 7
    //   2876: iconst_0
    //   2877: istore 5
    //   2879: iconst_0
    //   2880: putstatic 118	com/chelpus/root/utils/custompatch:odex	Z
    //   2883: iconst_0
    //   2884: putstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   2887: iconst_0
    //   2888: istore 11
    //   2890: aload 38
    //   2892: iload 12
    //   2894: aaload
    //   2895: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   2898: ldc_w 514
    //   2901: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2904: ifeq +28 -> 2932
    //   2907: bipush 13
    //   2909: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   2912: iconst_0
    //   2913: istore 9
    //   2915: iconst_0
    //   2916: istore 7
    //   2918: iconst_1
    //   2919: istore 5
    //   2921: iconst_0
    //   2922: putstatic 118	com/chelpus/root/utils/custompatch:odex	Z
    //   2925: iconst_0
    //   2926: putstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   2929: iconst_0
    //   2930: istore 11
    //   2932: aload 38
    //   2934: iload 12
    //   2936: aaload
    //   2937: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   2940: ldc_w 516
    //   2943: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2946: ifeq +28 -> 2974
    //   2949: bipush 15
    //   2951: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   2954: iconst_0
    //   2955: istore 9
    //   2957: iconst_0
    //   2958: istore 7
    //   2960: iconst_0
    //   2961: istore 5
    //   2963: iconst_0
    //   2964: putstatic 118	com/chelpus/root/utils/custompatch:odex	Z
    //   2967: iconst_0
    //   2968: putstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   2971: iconst_1
    //   2972: istore 11
    //   2974: iload 11
    //   2976: istore 8
    //   2978: iload 7
    //   2980: istore 6
    //   2982: aload 38
    //   2984: iload 12
    //   2986: aaload
    //   2987: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   2990: ldc_w 376
    //   2993: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2996: ifeq +28 -> 3024
    //   2999: bipush 11
    //   3001: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   3004: iconst_0
    //   3005: putstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   3008: iconst_0
    //   3009: istore 8
    //   3011: iconst_0
    //   3012: istore 9
    //   3014: iconst_0
    //   3015: istore 6
    //   3017: iconst_0
    //   3018: istore 5
    //   3020: iconst_1
    //   3021: putstatic 118	com/chelpus/root/utils/custompatch:odex	Z
    //   3024: iload 9
    //   3026: istore 7
    //   3028: aload 38
    //   3030: iload 12
    //   3032: aaload
    //   3033: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   3036: ldc_w 518
    //   3039: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3042: ifeq +28 -> 3070
    //   3045: bipush 14
    //   3047: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   3050: iconst_0
    //   3051: putstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   3054: iconst_0
    //   3055: istore 7
    //   3057: iconst_0
    //   3058: istore 6
    //   3060: iconst_0
    //   3061: istore 5
    //   3063: iconst_0
    //   3064: putstatic 118	com/chelpus/root/utils/custompatch:odex	Z
    //   3067: iconst_0
    //   3068: istore 8
    //   3070: aload 38
    //   3072: iload 12
    //   3074: aaload
    //   3075: ldc_w 519
    //   3078: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3081: ifeq +56 -> 3137
    //   3084: aload 38
    //   3086: iload 12
    //   3088: aaload
    //   3089: ldc_w 411
    //   3092: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3095: ifeq +42 -> 3137
    //   3098: aload 38
    //   3100: iload 12
    //   3102: aaload
    //   3103: ldc_w 521
    //   3106: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3109: istore 15
    //   3111: iload 15
    //   3113: ifeq +24 -> 3137
    //   3116: new 428	org/json/JSONObject
    //   3119: dup
    //   3120: aload 38
    //   3122: iload 12
    //   3124: aaload
    //   3125: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   3128: ldc_w 519
    //   3131: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   3134: putstatic 154	com/chelpus/root/utils/custompatch:group	Ljava/lang/String;
    //   3137: aload 25
    //   3139: astore 20
    //   3141: aload 26
    //   3143: astore 27
    //   3145: iload_2
    //   3146: istore_1
    //   3147: iload_3
    //   3148: istore 9
    //   3150: iload 17
    //   3152: istore 15
    //   3154: aload 19
    //   3156: astore 21
    //   3158: aload 38
    //   3160: iload 12
    //   3162: aaload
    //   3163: ldc_w 523
    //   3166: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3169: ifeq +3766 -> 6935
    //   3172: aload 25
    //   3174: astore 20
    //   3176: aload 26
    //   3178: astore 27
    //   3180: iload_2
    //   3181: istore_1
    //   3182: iload_3
    //   3183: istore 9
    //   3185: iload 17
    //   3187: istore 15
    //   3189: aload 19
    //   3191: astore 21
    //   3193: aload 38
    //   3195: iload 12
    //   3197: aaload
    //   3198: ldc_w 411
    //   3201: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3204: ifeq +3731 -> 6935
    //   3207: aload 25
    //   3209: astore 20
    //   3211: aload 26
    //   3213: astore 27
    //   3215: iload_2
    //   3216: istore_1
    //   3217: iload_3
    //   3218: istore 9
    //   3220: iload 17
    //   3222: istore 15
    //   3224: aload 19
    //   3226: astore 21
    //   3228: aload 38
    //   3230: iload 12
    //   3232: aaload
    //   3233: ldc_w 521
    //   3236: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3239: ifeq +3696 -> 6935
    //   3242: iload_3
    //   3243: istore 10
    //   3245: iload_3
    //   3246: ifeq +14 -> 3260
    //   3249: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   3252: invokestatic 527	com/chelpus/root/utils/custompatch:searchProcess	(Ljava/util/ArrayList;)Z
    //   3255: istore 17
    //   3257: iconst_0
    //   3258: istore 10
    //   3260: new 428	org/json/JSONObject
    //   3263: dup
    //   3264: aload 38
    //   3266: iload 12
    //   3268: aaload
    //   3269: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   3272: ldc_w 523
    //   3275: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   3278: astore 20
    //   3280: aload 20
    //   3282: astore 19
    //   3284: aload 19
    //   3286: invokevirtual 530	java/lang/String:trim	()Ljava/lang/String;
    //   3289: astore 20
    //   3291: aload 20
    //   3293: astore 19
    //   3295: getstatic 146	com/chelpus/root/utils/custompatch:convert	Z
    //   3298: ifeq +10 -> 3308
    //   3301: aload 20
    //   3303: invokestatic 533	com/chelpus/Utils:rework	(Ljava/lang/String;)Ljava/lang/String;
    //   3306: astore 19
    //   3308: aload 19
    //   3310: ldc_w 535
    //   3313: invokevirtual 539	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   3316: arraylength
    //   3317: anewarray 291	java/lang/String
    //   3320: astore 20
    //   3322: aload 19
    //   3324: ldc_w 535
    //   3327: invokevirtual 539	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   3330: astore 34
    //   3332: aload 34
    //   3334: arraylength
    //   3335: newarray <illegal type>
    //   3337: astore 25
    //   3339: aload 34
    //   3341: arraylength
    //   3342: newarray <illegal type>
    //   3344: astore 26
    //   3346: iconst_0
    //   3347: istore 11
    //   3349: iload_2
    //   3350: istore_3
    //   3351: aload 26
    //   3353: astore 20
    //   3355: aload 25
    //   3357: astore 27
    //   3359: iload_2
    //   3360: istore_1
    //   3361: iload 10
    //   3363: istore 9
    //   3365: iload 17
    //   3367: istore 15
    //   3369: aload 19
    //   3371: astore 21
    //   3373: iload 11
    //   3375: aload 34
    //   3377: arraylength
    //   3378: if_icmpge +3557 -> 6935
    //   3381: iload_2
    //   3382: istore_1
    //   3383: iload_2
    //   3384: istore_3
    //   3385: aload 34
    //   3387: iload 11
    //   3389: aaload
    //   3390: ldc_w 541
    //   3393: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3396: ifeq +31 -> 3427
    //   3399: iload_2
    //   3400: istore_1
    //   3401: iload_2
    //   3402: istore_3
    //   3403: aload 34
    //   3405: iload 11
    //   3407: aaload
    //   3408: ldc_w 543
    //   3411: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3414: ifne +13 -> 3427
    //   3417: iconst_1
    //   3418: istore_1
    //   3419: aload 34
    //   3421: iload 11
    //   3423: ldc_w 545
    //   3426: aastore
    //   3427: iload_1
    //   3428: istore_3
    //   3429: aload 34
    //   3431: iload 11
    //   3433: aaload
    //   3434: ldc_w 543
    //   3437: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3440: ifne +6250 -> 9690
    //   3443: iload_1
    //   3444: istore_3
    //   3445: aload 34
    //   3447: iload 11
    //   3449: aaload
    //   3450: ldc_w 547
    //   3453: invokevirtual 550	java/lang/String:matches	(Ljava/lang/String;)Z
    //   3456: ifeq +3422 -> 6878
    //   3459: goto +6231 -> 9690
    //   3462: iload_1
    //   3463: istore_3
    //   3464: aload 34
    //   3466: iload 11
    //   3468: aaload
    //   3469: ldc_w 552
    //   3472: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3475: ifne +51 -> 3526
    //   3478: iload_1
    //   3479: istore_3
    //   3480: aload 34
    //   3482: iload 11
    //   3484: aaload
    //   3485: ldc_w 554
    //   3488: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3491: ifne +35 -> 3526
    //   3494: iload_1
    //   3495: istore_3
    //   3496: aload 34
    //   3498: iload 11
    //   3500: aaload
    //   3501: ldc_w 556
    //   3504: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3507: ifne +19 -> 3526
    //   3510: iload_1
    //   3511: istore_3
    //   3512: aload 34
    //   3514: iload 11
    //   3516: aaload
    //   3517: ldc_w 558
    //   3520: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3523: ifeq +50 -> 3573
    //   3526: iload_1
    //   3527: istore_3
    //   3528: aload 25
    //   3530: iload 11
    //   3532: aload 34
    //   3534: iload 11
    //   3536: aaload
    //   3537: invokevirtual 498	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   3540: ldc_w 554
    //   3543: ldc -124
    //   3545: invokevirtual 397	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   3548: ldc_w 558
    //   3551: ldc -124
    //   3553: invokevirtual 397	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   3556: invokestatic 563	java/lang/Integer:valueOf	(Ljava/lang/String;)Ljava/lang/Integer;
    //   3559: invokevirtual 566	java/lang/Integer:intValue	()I
    //   3562: iconst_2
    //   3563: iadd
    //   3564: iastore
    //   3565: aload 34
    //   3567: iload 11
    //   3569: ldc_w 545
    //   3572: aastore
    //   3573: iload_1
    //   3574: istore_3
    //   3575: aload 26
    //   3577: iload 11
    //   3579: aload 34
    //   3581: iload 11
    //   3583: aaload
    //   3584: bipush 16
    //   3586: invokestatic 569	java/lang/Integer:valueOf	(Ljava/lang/String;I)Ljava/lang/Integer;
    //   3589: invokevirtual 573	java/lang/Integer:byteValue	()B
    //   3592: bastore
    //   3593: iload 11
    //   3595: iconst_1
    //   3596: iadd
    //   3597: istore 11
    //   3599: iload_1
    //   3600: istore_2
    //   3601: goto -252 -> 3349
    //   3604: aload 21
    //   3606: invokevirtual 574	java/io/BufferedReader:close	()V
    //   3609: aload 20
    //   3611: invokevirtual 575	java/io/FileInputStream:close	()V
    //   3614: goto -3101 -> 513
    //   3617: getstatic 138	com/chelpus/root/utils/custompatch:odexpatch	Z
    //   3620: ifeq +163 -> 3783
    //   3623: new 190	java/io/File
    //   3626: dup
    //   3627: getstatic 128	com/chelpus/root/utils/custompatch:dirapp	Ljava/lang/String;
    //   3630: iconst_1
    //   3631: invokestatic 345	com/chelpus/Utils:getPlaceForOdex	(Ljava/lang/String;Z)Ljava/lang/String;
    //   3634: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   3637: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   3640: iload 8
    //   3642: istore 10
    //   3644: iload_1
    //   3645: istore 14
    //   3647: aload 19
    //   3649: astore 31
    //   3651: iload 5
    //   3653: istore 9
    //   3655: iload 15
    //   3657: istore 18
    //   3659: iload 16
    //   3661: istore 17
    //   3663: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   3666: invokevirtual 205	java/util/ArrayList:size	()I
    //   3669: ifle -2393 -> 1276
    //   3672: ldc_w 577
    //   3675: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   3678: ldc_w 579
    //   3681: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   3684: ldc_w 581
    //   3687: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   3690: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   3693: ldc -124
    //   3695: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   3698: ifne +9 -> 3707
    //   3701: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   3704: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   3707: getstatic 116	com/chelpus/root/utils/custompatch:manualpatch	Z
    //   3710: ifne +5997 -> 9707
    //   3713: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   3716: invokestatic 584	com/chelpus/root/utils/custompatch:patchProcess	(Ljava/util/ArrayList;)Z
    //   3719: istore 15
    //   3721: goto +5986 -> 9707
    //   3724: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   3727: invokevirtual 424	java/util/ArrayList:clear	()V
    //   3730: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   3733: invokevirtual 424	java/util/ArrayList:clear	()V
    //   3736: sipush 200
    //   3739: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   3742: ldc -124
    //   3744: putstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   3747: iload 8
    //   3749: istore 10
    //   3751: iload_1
    //   3752: istore 14
    //   3754: aload 19
    //   3756: astore 31
    //   3758: iload 5
    //   3760: istore 9
    //   3762: iload 15
    //   3764: istore 18
    //   3766: iload 16
    //   3768: istore 17
    //   3770: goto -2494 -> 1276
    //   3773: astore_0
    //   3774: ldc_w 586
    //   3777: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   3780: goto -3560 -> 220
    //   3783: getstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   3786: ifne +243 -> 4029
    //   3789: aload_0
    //   3790: iconst_2
    //   3791: aaload
    //   3792: invokestatic 381	com/chelpus/root/utils/custompatch:searchDalvik	(Ljava/lang/String;)V
    //   3795: new 190	java/io/File
    //   3798: dup
    //   3799: getstatic 128	com/chelpus/root/utils/custompatch:dirapp	Ljava/lang/String;
    //   3802: iconst_1
    //   3803: invokestatic 345	com/chelpus/Utils:getPlaceForOdex	(Ljava/lang/String;Z)Ljava/lang/String;
    //   3806: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   3809: invokevirtual 198	java/io/File:exists	()Z
    //   3812: ifeq +54 -> 3866
    //   3815: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   3818: invokevirtual 223	java/io/File:getName	()Ljava/lang/String;
    //   3821: ldc_w 588
    //   3824: invokevirtual 591	java/lang/String:endsWith	(Ljava/lang/String;)Z
    //   3827: ifne +39 -> 3866
    //   3830: new 190	java/io/File
    //   3833: dup
    //   3834: getstatic 128	com/chelpus/root/utils/custompatch:dirapp	Ljava/lang/String;
    //   3837: iconst_1
    //   3838: invokestatic 345	com/chelpus/Utils:getPlaceForOdex	(Ljava/lang/String;Z)Ljava/lang/String;
    //   3841: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   3844: invokevirtual 201	java/io/File:delete	()Z
    //   3847: pop
    //   3848: ldc_w 577
    //   3851: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   3854: ldc_w 593
    //   3857: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   3860: ldc_w 581
    //   3863: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   3866: iload 8
    //   3868: istore 10
    //   3870: iload_1
    //   3871: istore 14
    //   3873: aload 19
    //   3875: astore 31
    //   3877: iload 5
    //   3879: istore 9
    //   3881: iload 15
    //   3883: istore 18
    //   3885: iload 16
    //   3887: istore 17
    //   3889: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   3892: invokevirtual 205	java/util/ArrayList:size	()I
    //   3895: ifle -2619 -> 1276
    //   3898: ldc_w 577
    //   3901: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   3904: ldc_w 595
    //   3907: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   3910: ldc_w 581
    //   3913: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   3916: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   3919: ldc -124
    //   3921: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   3924: ifne +9 -> 3933
    //   3927: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   3930: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   3933: getstatic 116	com/chelpus/root/utils/custompatch:manualpatch	Z
    //   3936: ifne +5782 -> 9718
    //   3939: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   3942: invokestatic 584	com/chelpus/root/utils/custompatch:patchProcess	(Ljava/util/ArrayList;)Z
    //   3945: istore 15
    //   3947: goto +5771 -> 9718
    //   3950: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   3953: invokevirtual 424	java/util/ArrayList:clear	()V
    //   3956: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   3959: invokevirtual 424	java/util/ArrayList:clear	()V
    //   3962: sipush 200
    //   3965: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   3968: ldc -124
    //   3970: putstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   3973: iload 8
    //   3975: istore 10
    //   3977: iload_1
    //   3978: istore 14
    //   3980: aload 19
    //   3982: astore 31
    //   3984: iload 5
    //   3986: istore 9
    //   3988: iload 15
    //   3990: istore 18
    //   3992: iload 16
    //   3994: istore 17
    //   3996: goto -2720 -> 1276
    //   3999: astore_0
    //   4000: aload_0
    //   4001: invokevirtual 315	java/lang/Exception:printStackTrace	()V
    //   4004: new 174	java/lang/StringBuilder
    //   4007: dup
    //   4008: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   4011: ldc -124
    //   4013: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4016: aload_0
    //   4017: invokevirtual 598	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   4020: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   4023: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4026: goto -3806 -> 220
    //   4029: iload 8
    //   4031: istore 10
    //   4033: iload_1
    //   4034: istore 14
    //   4036: aload 19
    //   4038: astore 31
    //   4040: iload 5
    //   4042: istore 9
    //   4044: iload 15
    //   4046: istore 18
    //   4048: iload 16
    //   4050: istore 17
    //   4052: getstatic 162	com/chelpus/root/utils/custompatch:classesFiles	Ljava/util/ArrayList;
    //   4055: ifnull -2779 -> 1276
    //   4058: iload 8
    //   4060: istore 10
    //   4062: iload_1
    //   4063: istore 14
    //   4065: aload 19
    //   4067: astore 31
    //   4069: iload 5
    //   4071: istore 9
    //   4073: iload 15
    //   4075: istore 18
    //   4077: iload 16
    //   4079: istore 17
    //   4081: getstatic 162	com/chelpus/root/utils/custompatch:classesFiles	Ljava/util/ArrayList;
    //   4084: invokevirtual 205	java/util/ArrayList:size	()I
    //   4087: ifle -2811 -> 1276
    //   4090: getstatic 162	com/chelpus/root/utils/custompatch:classesFiles	Ljava/util/ArrayList;
    //   4093: invokevirtual 205	java/util/ArrayList:size	()I
    //   4096: iconst_1
    //   4097: if_icmple +7 -> 4104
    //   4100: iconst_1
    //   4101: putstatic 164	com/chelpus/root/utils/custompatch:multidex	Z
    //   4104: getstatic 162	com/chelpus/root/utils/custompatch:classesFiles	Ljava/util/ArrayList;
    //   4107: invokevirtual 209	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   4110: astore 22
    //   4112: aload 22
    //   4114: invokeinterface 214 1 0
    //   4119: ifeq +112 -> 4231
    //   4122: aload 22
    //   4124: invokeinterface 218 1 0
    //   4129: checkcast 190	java/io/File
    //   4132: astore 31
    //   4134: aload 31
    //   4136: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   4139: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   4142: invokevirtual 205	java/util/ArrayList:size	()I
    //   4145: ifle -33 -> 4112
    //   4148: ldc_w 577
    //   4151: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4154: new 174	java/lang/StringBuilder
    //   4157: dup
    //   4158: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   4161: ldc_w 600
    //   4164: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4167: aload 31
    //   4169: invokevirtual 223	java/io/File:getName	()Ljava/lang/String;
    //   4172: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4175: ldc_w 479
    //   4178: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4181: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   4184: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4187: ldc_w 581
    //   4190: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4193: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   4196: ldc -124
    //   4198: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4201: ifne +9 -> 4210
    //   4204: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   4207: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4210: iload 15
    //   4212: istore 17
    //   4214: getstatic 116	com/chelpus/root/utils/custompatch:manualpatch	Z
    //   4217: ifne +5512 -> 9729
    //   4220: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   4223: invokestatic 584	com/chelpus/root/utils/custompatch:patchProcess	(Ljava/util/ArrayList;)Z
    //   4226: istore 17
    //   4228: goto +5501 -> 9729
    //   4231: iconst_0
    //   4232: putstatic 164	com/chelpus/root/utils/custompatch:multidex	Z
    //   4235: iconst_0
    //   4236: putstatic 166	com/chelpus/root/utils/custompatch:goodResult	Z
    //   4239: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   4242: invokevirtual 424	java/util/ArrayList:clear	()V
    //   4245: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   4248: invokevirtual 424	java/util/ArrayList:clear	()V
    //   4251: sipush 200
    //   4254: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   4257: ldc -124
    //   4259: putstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   4262: iload 8
    //   4264: istore 10
    //   4266: iload_1
    //   4267: istore 14
    //   4269: aload 19
    //   4271: astore 31
    //   4273: iload 5
    //   4275: istore 9
    //   4277: iload 15
    //   4279: istore 18
    //   4281: iload 16
    //   4283: istore 17
    //   4285: goto -3009 -> 1276
    //   4288: iconst_0
    //   4289: putstatic 146	com/chelpus/root/utils/custompatch:convert	Z
    //   4292: iload 16
    //   4294: istore 17
    //   4296: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   4299: invokevirtual 205	java/util/ArrayList:size	()I
    //   4302: ifle +66 -> 4368
    //   4305: ldc_w 577
    //   4308: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4311: ldc_w 602
    //   4314: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4317: ldc_w 581
    //   4320: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4323: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   4326: ldc -124
    //   4328: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4331: ifne +9 -> 4340
    //   4334: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   4337: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4340: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   4343: invokestatic 584	com/chelpus/root/utils/custompatch:patchProcess	(Ljava/util/ArrayList;)Z
    //   4346: istore 18
    //   4348: iload 18
    //   4350: istore 15
    //   4352: iload 16
    //   4354: istore 17
    //   4356: iload 18
    //   4358: ifne +10 -> 4368
    //   4361: iconst_0
    //   4362: istore 17
    //   4364: iload 18
    //   4366: istore 15
    //   4368: ldc_w 577
    //   4371: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4374: ldc_w 604
    //   4377: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4380: ldc_w 581
    //   4383: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4386: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   4389: ldc -124
    //   4391: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4394: ifne +9 -> 4403
    //   4397: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   4400: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4403: getstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   4406: ifne +9 -> 4415
    //   4409: aload_0
    //   4410: iconst_2
    //   4411: aaload
    //   4412: invokestatic 381	com/chelpus/root/utils/custompatch:searchDalvik	(Ljava/lang/String;)V
    //   4415: aload_0
    //   4416: iconst_0
    //   4417: aaload
    //   4418: aload_0
    //   4419: iconst_2
    //   4420: aaload
    //   4421: invokestatic 607	com/chelpus/root/utils/custompatch:searchDalvikOdex	(Ljava/lang/String;Ljava/lang/String;)V
    //   4424: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   4427: invokevirtual 198	java/io/File:exists	()Z
    //   4430: ifeq +13 -> 4443
    //   4433: iconst_1
    //   4434: istore_1
    //   4435: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   4438: invokevirtual 451	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   4441: astore 19
    //   4443: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   4446: invokevirtual 424	java/util/ArrayList:clear	()V
    //   4449: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   4452: invokevirtual 424	java/util/ArrayList:clear	()V
    //   4455: sipush 200
    //   4458: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   4461: ldc -124
    //   4463: putstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   4466: iload 8
    //   4468: istore 10
    //   4470: iload_1
    //   4471: istore 14
    //   4473: aload 19
    //   4475: astore 31
    //   4477: iload 5
    //   4479: istore 9
    //   4481: iload 15
    //   4483: istore 18
    //   4485: goto -3209 -> 1276
    //   4488: iconst_0
    //   4489: putstatic 146	com/chelpus/root/utils/custompatch:convert	Z
    //   4492: iload 15
    //   4494: istore 18
    //   4496: iload 16
    //   4498: istore 17
    //   4500: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   4503: invokevirtual 205	java/util/ArrayList:size	()I
    //   4506: ifle +177 -> 4683
    //   4509: iload 15
    //   4511: istore 18
    //   4513: iload 16
    //   4515: istore 17
    //   4517: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   4520: ifnull +163 -> 4683
    //   4523: iload 15
    //   4525: istore 18
    //   4527: iload 16
    //   4529: istore 17
    //   4531: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   4534: invokevirtual 205	java/util/ArrayList:size	()I
    //   4537: ifle +146 -> 4683
    //   4540: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   4543: invokevirtual 209	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   4546: astore 22
    //   4548: aload 22
    //   4550: invokeinterface 214 1 0
    //   4555: ifeq +106 -> 4661
    //   4558: aload 22
    //   4560: invokeinterface 218 1 0
    //   4565: checkcast 190	java/io/File
    //   4568: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   4571: ldc_w 453
    //   4574: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4577: new 174	java/lang/StringBuilder
    //   4580: dup
    //   4581: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   4584: ldc_w 609
    //   4587: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4590: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   4593: invokevirtual 451	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   4596: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4599: ldc_w 479
    //   4602: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4605: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   4608: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4611: ldc_w 457
    //   4614: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4617: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   4620: ldc -124
    //   4622: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4625: ifne +9 -> 4634
    //   4628: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   4631: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4634: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   4637: invokestatic 584	com/chelpus/root/utils/custompatch:patchProcess	(Ljava/util/ArrayList;)Z
    //   4640: istore 17
    //   4642: iload 17
    //   4644: istore 15
    //   4646: iload 17
    //   4648: ifne -100 -> 4548
    //   4651: iconst_0
    //   4652: istore 16
    //   4654: iload 17
    //   4656: istore 15
    //   4658: goto -110 -> 4548
    //   4661: iconst_0
    //   4662: putstatic 168	com/chelpus/root/utils/custompatch:multilib_patch	Z
    //   4665: iconst_0
    //   4666: putstatic 166	com/chelpus/root/utils/custompatch:goodResult	Z
    //   4669: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   4672: invokevirtual 424	java/util/ArrayList:clear	()V
    //   4675: iload 16
    //   4677: istore 17
    //   4679: iload 15
    //   4681: istore 18
    //   4683: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   4686: invokevirtual 424	java/util/ArrayList:clear	()V
    //   4689: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   4692: invokevirtual 424	java/util/ArrayList:clear	()V
    //   4695: sipush 200
    //   4698: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   4701: ldc -124
    //   4703: putstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   4706: iload 8
    //   4708: istore 10
    //   4710: iload_1
    //   4711: istore 14
    //   4713: aload 19
    //   4715: astore 31
    //   4717: iload 5
    //   4719: istore 9
    //   4721: goto -3445 -> 1276
    //   4724: iconst_0
    //   4725: putstatic 146	com/chelpus/root/utils/custompatch:convert	Z
    //   4728: iload 15
    //   4730: istore 18
    //   4732: iload 16
    //   4734: istore 17
    //   4736: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   4739: invokevirtual 205	java/util/ArrayList:size	()I
    //   4742: ifle +177 -> 4919
    //   4745: iload 15
    //   4747: istore 18
    //   4749: iload 16
    //   4751: istore 17
    //   4753: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   4756: ifnull +163 -> 4919
    //   4759: iload 15
    //   4761: istore 18
    //   4763: iload 16
    //   4765: istore 17
    //   4767: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   4770: invokevirtual 205	java/util/ArrayList:size	()I
    //   4773: ifle +146 -> 4919
    //   4776: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   4779: invokevirtual 209	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   4782: astore 22
    //   4784: aload 22
    //   4786: invokeinterface 214 1 0
    //   4791: ifeq +106 -> 4897
    //   4794: aload 22
    //   4796: invokeinterface 218 1 0
    //   4801: checkcast 190	java/io/File
    //   4804: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   4807: ldc_w 611
    //   4810: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4813: new 174	java/lang/StringBuilder
    //   4816: dup
    //   4817: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   4820: ldc_w 613
    //   4823: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4826: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   4829: invokevirtual 223	java/io/File:getName	()Ljava/lang/String;
    //   4832: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4835: ldc_w 479
    //   4838: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4841: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   4844: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4847: ldc_w 615
    //   4850: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4853: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   4856: ldc -124
    //   4858: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4861: ifne +9 -> 4870
    //   4864: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   4867: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   4870: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   4873: invokestatic 584	com/chelpus/root/utils/custompatch:patchProcess	(Ljava/util/ArrayList;)Z
    //   4876: istore 17
    //   4878: iload 17
    //   4880: istore 15
    //   4882: iload 17
    //   4884: ifne -100 -> 4784
    //   4887: iconst_0
    //   4888: istore 16
    //   4890: iload 17
    //   4892: istore 15
    //   4894: goto -110 -> 4784
    //   4897: iconst_0
    //   4898: putstatic 168	com/chelpus/root/utils/custompatch:multilib_patch	Z
    //   4901: iconst_0
    //   4902: putstatic 166	com/chelpus/root/utils/custompatch:goodResult	Z
    //   4905: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   4908: invokevirtual 424	java/util/ArrayList:clear	()V
    //   4911: iload 16
    //   4913: istore 17
    //   4915: iload 15
    //   4917: istore 18
    //   4919: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   4922: invokevirtual 424	java/util/ArrayList:clear	()V
    //   4925: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   4928: invokevirtual 424	java/util/ArrayList:clear	()V
    //   4931: sipush 200
    //   4934: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   4937: ldc -124
    //   4939: putstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   4942: iload 8
    //   4944: istore 10
    //   4946: iload_1
    //   4947: istore 14
    //   4949: aload 19
    //   4951: astore 31
    //   4953: iload 5
    //   4955: istore 9
    //   4957: goto -3681 -> 1276
    //   4960: iconst_0
    //   4961: putstatic 146	com/chelpus/root/utils/custompatch:convert	Z
    //   4964: iload 15
    //   4966: istore 18
    //   4968: iload 16
    //   4970: istore 17
    //   4972: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   4975: invokevirtual 205	java/util/ArrayList:size	()I
    //   4978: ifle +177 -> 5155
    //   4981: iload 15
    //   4983: istore 18
    //   4985: iload 16
    //   4987: istore 17
    //   4989: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   4992: ifnull +163 -> 5155
    //   4995: iload 15
    //   4997: istore 18
    //   4999: iload 16
    //   5001: istore 17
    //   5003: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   5006: invokevirtual 205	java/util/ArrayList:size	()I
    //   5009: ifle +146 -> 5155
    //   5012: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   5015: invokevirtual 209	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   5018: astore 22
    //   5020: aload 22
    //   5022: invokeinterface 214 1 0
    //   5027: ifeq +106 -> 5133
    //   5030: aload 22
    //   5032: invokeinterface 218 1 0
    //   5037: checkcast 190	java/io/File
    //   5040: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   5043: ldc_w 617
    //   5046: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5049: new 174	java/lang/StringBuilder
    //   5052: dup
    //   5053: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   5056: ldc_w 619
    //   5059: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5062: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   5065: invokevirtual 223	java/io/File:getName	()Ljava/lang/String;
    //   5068: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5071: ldc_w 479
    //   5074: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5077: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   5080: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5083: ldc_w 621
    //   5086: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5089: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   5092: ldc -124
    //   5094: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   5097: ifne +9 -> 5106
    //   5100: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   5103: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5106: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5109: invokestatic 584	com/chelpus/root/utils/custompatch:patchProcess	(Ljava/util/ArrayList;)Z
    //   5112: istore 17
    //   5114: iload 17
    //   5116: istore 15
    //   5118: iload 17
    //   5120: ifne -100 -> 5020
    //   5123: iconst_0
    //   5124: istore 16
    //   5126: iload 17
    //   5128: istore 15
    //   5130: goto -110 -> 5020
    //   5133: iconst_0
    //   5134: putstatic 168	com/chelpus/root/utils/custompatch:multilib_patch	Z
    //   5137: iconst_0
    //   5138: putstatic 166	com/chelpus/root/utils/custompatch:goodResult	Z
    //   5141: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   5144: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5147: iload 16
    //   5149: istore 17
    //   5151: iload 15
    //   5153: istore 18
    //   5155: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   5158: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5161: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5164: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5167: sipush 200
    //   5170: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   5173: ldc -124
    //   5175: putstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   5178: iload 8
    //   5180: istore 10
    //   5182: iload_1
    //   5183: istore 14
    //   5185: aload 19
    //   5187: astore 31
    //   5189: iload 5
    //   5191: istore 9
    //   5193: goto -3917 -> 1276
    //   5196: iconst_0
    //   5197: putstatic 146	com/chelpus/root/utils/custompatch:convert	Z
    //   5200: iload 15
    //   5202: istore 18
    //   5204: iload 16
    //   5206: istore 17
    //   5208: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5211: invokevirtual 205	java/util/ArrayList:size	()I
    //   5214: ifle +177 -> 5391
    //   5217: iload 15
    //   5219: istore 18
    //   5221: iload 16
    //   5223: istore 17
    //   5225: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   5228: ifnull +163 -> 5391
    //   5231: iload 15
    //   5233: istore 18
    //   5235: iload 16
    //   5237: istore 17
    //   5239: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   5242: invokevirtual 205	java/util/ArrayList:size	()I
    //   5245: ifle +146 -> 5391
    //   5248: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   5251: invokevirtual 209	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   5254: astore 22
    //   5256: aload 22
    //   5258: invokeinterface 214 1 0
    //   5263: ifeq +106 -> 5369
    //   5266: aload 22
    //   5268: invokeinterface 218 1 0
    //   5273: checkcast 190	java/io/File
    //   5276: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   5279: ldc_w 453
    //   5282: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5285: new 174	java/lang/StringBuilder
    //   5288: dup
    //   5289: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   5292: ldc_w 623
    //   5295: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5298: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   5301: invokevirtual 223	java/io/File:getName	()Ljava/lang/String;
    //   5304: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5307: ldc_w 479
    //   5310: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5313: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   5316: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5319: ldc_w 457
    //   5322: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5325: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   5328: ldc -124
    //   5330: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   5333: ifne +9 -> 5342
    //   5336: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   5339: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5342: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5345: invokestatic 584	com/chelpus/root/utils/custompatch:patchProcess	(Ljava/util/ArrayList;)Z
    //   5348: istore 17
    //   5350: iload 17
    //   5352: istore 15
    //   5354: iload 17
    //   5356: ifne -100 -> 5256
    //   5359: iconst_0
    //   5360: istore 16
    //   5362: iload 17
    //   5364: istore 15
    //   5366: goto -110 -> 5256
    //   5369: iconst_0
    //   5370: putstatic 168	com/chelpus/root/utils/custompatch:multilib_patch	Z
    //   5373: iconst_0
    //   5374: putstatic 166	com/chelpus/root/utils/custompatch:goodResult	Z
    //   5377: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   5380: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5383: iload 16
    //   5385: istore 17
    //   5387: iload 15
    //   5389: istore 18
    //   5391: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   5394: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5397: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5400: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5403: sipush 200
    //   5406: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   5409: ldc -124
    //   5411: putstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   5414: iload 8
    //   5416: istore 10
    //   5418: iload_1
    //   5419: istore 14
    //   5421: aload 19
    //   5423: astore 31
    //   5425: iload 5
    //   5427: istore 9
    //   5429: goto -4153 -> 1276
    //   5432: iconst_0
    //   5433: putstatic 146	com/chelpus/root/utils/custompatch:convert	Z
    //   5436: iload 15
    //   5438: istore 18
    //   5440: iload 16
    //   5442: istore 17
    //   5444: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5447: invokevirtual 205	java/util/ArrayList:size	()I
    //   5450: ifle +177 -> 5627
    //   5453: iload 15
    //   5455: istore 18
    //   5457: iload 16
    //   5459: istore 17
    //   5461: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   5464: ifnull +163 -> 5627
    //   5467: iload 15
    //   5469: istore 18
    //   5471: iload 16
    //   5473: istore 17
    //   5475: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   5478: invokevirtual 205	java/util/ArrayList:size	()I
    //   5481: ifle +146 -> 5627
    //   5484: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   5487: invokevirtual 209	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   5490: astore 22
    //   5492: aload 22
    //   5494: invokeinterface 214 1 0
    //   5499: ifeq +106 -> 5605
    //   5502: aload 22
    //   5504: invokeinterface 218 1 0
    //   5509: checkcast 190	java/io/File
    //   5512: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   5515: ldc_w 453
    //   5518: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5521: new 174	java/lang/StringBuilder
    //   5524: dup
    //   5525: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   5528: ldc_w 625
    //   5531: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5534: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   5537: invokevirtual 223	java/io/File:getName	()Ljava/lang/String;
    //   5540: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5543: ldc_w 479
    //   5546: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5549: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   5552: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5555: ldc_w 457
    //   5558: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5561: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   5564: ldc -124
    //   5566: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   5569: ifne +9 -> 5578
    //   5572: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   5575: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5578: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5581: invokestatic 584	com/chelpus/root/utils/custompatch:patchProcess	(Ljava/util/ArrayList;)Z
    //   5584: istore 17
    //   5586: iload 17
    //   5588: istore 15
    //   5590: iload 17
    //   5592: ifne -100 -> 5492
    //   5595: iconst_0
    //   5596: istore 16
    //   5598: iload 17
    //   5600: istore 15
    //   5602: goto -110 -> 5492
    //   5605: iconst_0
    //   5606: putstatic 168	com/chelpus/root/utils/custompatch:multilib_patch	Z
    //   5609: iconst_0
    //   5610: putstatic 166	com/chelpus/root/utils/custompatch:goodResult	Z
    //   5613: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   5616: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5619: iload 16
    //   5621: istore 17
    //   5623: iload 15
    //   5625: istore 18
    //   5627: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   5630: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5633: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5636: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5639: sipush 200
    //   5642: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   5645: ldc -124
    //   5647: putstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   5650: iload 8
    //   5652: istore 10
    //   5654: iload_1
    //   5655: istore 14
    //   5657: aload 19
    //   5659: astore 31
    //   5661: iload 5
    //   5663: istore 9
    //   5665: goto -4389 -> 1276
    //   5668: iconst_0
    //   5669: putstatic 146	com/chelpus/root/utils/custompatch:convert	Z
    //   5672: iload 15
    //   5674: istore 18
    //   5676: iload 16
    //   5678: istore 17
    //   5680: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5683: invokevirtual 205	java/util/ArrayList:size	()I
    //   5686: ifle +20 -> 5706
    //   5689: getstatic 116	com/chelpus/root/utils/custompatch:manualpatch	Z
    //   5692: ifne +4056 -> 9748
    //   5695: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5698: invokestatic 584	com/chelpus/root/utils/custompatch:patchProcess	(Ljava/util/ArrayList;)Z
    //   5701: istore 15
    //   5703: goto +4045 -> 9748
    //   5706: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   5709: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5712: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5715: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5718: sipush 200
    //   5721: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   5724: ldc -124
    //   5726: putstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   5729: iload 8
    //   5731: istore 10
    //   5733: iload_1
    //   5734: istore 14
    //   5736: aload 19
    //   5738: astore 31
    //   5740: iload 5
    //   5742: istore 9
    //   5744: goto -4468 -> 1276
    //   5747: iconst_0
    //   5748: putstatic 146	com/chelpus/root/utils/custompatch:convert	Z
    //   5751: ldc_w 453
    //   5754: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5757: ldc_w 627
    //   5760: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5763: ldc_w 457
    //   5766: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5769: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   5772: ldc -124
    //   5774: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   5777: ifne +9 -> 5786
    //   5780: getstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   5783: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5786: new 190	java/io/File
    //   5789: dup
    //   5790: getstatic 128	com/chelpus/root/utils/custompatch:dirapp	Ljava/lang/String;
    //   5793: iconst_1
    //   5794: invokestatic 345	com/chelpus/Utils:getPlaceForOdex	(Ljava/lang/String;Z)Ljava/lang/String;
    //   5797: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   5800: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   5803: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   5806: invokevirtual 198	java/io/File:exists	()Z
    //   5809: ifne +9 -> 5818
    //   5812: ldc_w 629
    //   5815: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5818: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   5821: invokevirtual 633	java/io/File:length	()J
    //   5824: lconst_0
    //   5825: lcmp
    //   5826: ifne +16 -> 5842
    //   5829: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   5832: invokevirtual 201	java/io/File:delete	()Z
    //   5835: pop
    //   5836: ldc_w 629
    //   5839: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   5842: iconst_0
    //   5843: putstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   5846: iconst_0
    //   5847: putstatic 118	com/chelpus/root/utils/custompatch:odex	Z
    //   5850: iload 16
    //   5852: istore 17
    //   5854: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5857: invokevirtual 205	java/util/ArrayList:size	()I
    //   5860: ifle +31 -> 5891
    //   5863: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5866: invokestatic 584	com/chelpus/root/utils/custompatch:patchProcess	(Ljava/util/ArrayList;)Z
    //   5869: istore 18
    //   5871: iload 18
    //   5873: istore 15
    //   5875: iload 16
    //   5877: istore 17
    //   5879: iload 18
    //   5881: ifne +10 -> 5891
    //   5884: iconst_0
    //   5885: istore 17
    //   5887: iload 18
    //   5889: istore 15
    //   5891: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   5894: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5897: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5900: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5903: sipush 200
    //   5906: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   5909: ldc -124
    //   5911: putstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   5914: iload 8
    //   5916: istore 10
    //   5918: iload_1
    //   5919: istore 14
    //   5921: aload 19
    //   5923: astore 31
    //   5925: iload 5
    //   5927: istore 9
    //   5929: iload 15
    //   5931: istore 18
    //   5933: goto -4657 -> 1276
    //   5936: iconst_0
    //   5937: putstatic 146	com/chelpus/root/utils/custompatch:convert	Z
    //   5940: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   5943: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5946: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   5949: invokevirtual 424	java/util/ArrayList:clear	()V
    //   5952: sipush 200
    //   5955: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   5958: ldc -124
    //   5960: putstatic 152	com/chelpus/root/utils/custompatch:searchStr	Ljava/lang/String;
    //   5963: iload 8
    //   5965: istore 10
    //   5967: iload_1
    //   5968: istore 14
    //   5970: aload 19
    //   5972: astore 31
    //   5974: iload 5
    //   5976: istore 9
    //   5978: iload 15
    //   5980: istore 18
    //   5982: iload 16
    //   5984: istore 17
    //   5986: goto -4710 -> 1276
    //   5989: iconst_0
    //   5990: istore 4
    //   5992: iload 8
    //   5994: istore 10
    //   5996: iload_1
    //   5997: istore 14
    //   5999: aload 19
    //   6001: astore 31
    //   6003: iload 4
    //   6005: istore 9
    //   6007: iload 15
    //   6009: istore 18
    //   6011: iload 16
    //   6013: istore 17
    //   6015: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   6018: invokevirtual 198	java/io/File:exists	()Z
    //   6021: ifeq -4745 -> 1276
    //   6024: ldc_w 453
    //   6027: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6030: new 174	java/lang/StringBuilder
    //   6033: dup
    //   6034: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   6037: ldc_w 635
    //   6040: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6043: aload 33
    //   6045: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6048: ldc_w 637
    //   6051: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6054: aload 20
    //   6056: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6059: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   6062: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6065: ldc_w 457
    //   6068: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6071: iconst_3
    //   6072: anewarray 291	java/lang/String
    //   6075: dup
    //   6076: iconst_0
    //   6077: ldc_w 435
    //   6080: aastore
    //   6081: dup
    //   6082: iconst_1
    //   6083: new 174	java/lang/StringBuilder
    //   6086: dup
    //   6087: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   6090: ldc -124
    //   6092: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6095: aload 33
    //   6097: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6100: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   6103: aastore
    //   6104: dup
    //   6105: iconst_2
    //   6106: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   6109: invokevirtual 451	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   6112: aastore
    //   6113: invokestatic 440	com/chelpus/Utils:run_all_no_root	([Ljava/lang/String;)V
    //   6116: iload 8
    //   6118: istore 10
    //   6120: iload_1
    //   6121: istore 14
    //   6123: aload 19
    //   6125: astore 31
    //   6127: iload 4
    //   6129: istore 9
    //   6131: iload 15
    //   6133: istore 18
    //   6135: iload 16
    //   6137: istore 17
    //   6139: goto -4863 -> 1276
    //   6142: iconst_0
    //   6143: istore 10
    //   6145: new 190	java/io/File
    //   6148: dup
    //   6149: aload 27
    //   6151: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   6154: invokestatic 641	com/chelpus/Utils:getDirs	(Ljava/io/File;)Ljava/io/File;
    //   6157: astore 22
    //   6159: aload 22
    //   6161: invokevirtual 644	java/io/File:mkdirs	()Z
    //   6164: pop
    //   6165: iconst_3
    //   6166: anewarray 291	java/lang/String
    //   6169: dup
    //   6170: iconst_0
    //   6171: ldc_w 435
    //   6174: aastore
    //   6175: dup
    //   6176: iconst_1
    //   6177: ldc_w 437
    //   6180: aastore
    //   6181: dup
    //   6182: iconst_2
    //   6183: aload 22
    //   6185: invokevirtual 451	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   6188: aastore
    //   6189: invokestatic 440	com/chelpus/Utils:run_all_no_root	([Ljava/lang/String;)V
    //   6192: aload 22
    //   6194: invokevirtual 451	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   6197: pop
    //   6198: ldc_w 453
    //   6201: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6204: new 174	java/lang/StringBuilder
    //   6207: dup
    //   6208: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   6211: ldc_w 646
    //   6214: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6217: aload 20
    //   6219: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6222: ldc_w 648
    //   6225: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6228: aload 27
    //   6230: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6233: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   6236: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6239: ldc_w 457
    //   6242: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6245: new 190	java/io/File
    //   6248: dup
    //   6249: new 174	java/lang/StringBuilder
    //   6252: dup
    //   6253: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   6256: getstatic 130	com/chelpus/root/utils/custompatch:sddir	Ljava/lang/String;
    //   6259: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6262: ldc -36
    //   6264: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6267: aload 20
    //   6269: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6272: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   6275: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   6278: invokevirtual 198	java/io/File:exists	()Z
    //   6281: ifne +66 -> 6347
    //   6284: new 174	java/lang/StringBuilder
    //   6287: dup
    //   6288: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   6291: ldc_w 650
    //   6294: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6297: aload 20
    //   6299: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6302: ldc_w 652
    //   6305: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6308: getstatic 130	com/chelpus/root/utils/custompatch:sddir	Ljava/lang/String;
    //   6311: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6314: ldc -36
    //   6316: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6319: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   6322: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6325: iload_1
    //   6326: istore 14
    //   6328: aload 19
    //   6330: astore 31
    //   6332: iload 5
    //   6334: istore 9
    //   6336: iload 15
    //   6338: istore 18
    //   6340: iload 16
    //   6342: istore 17
    //   6344: goto -5068 -> 1276
    //   6347: new 190	java/io/File
    //   6350: dup
    //   6351: new 174	java/lang/StringBuilder
    //   6354: dup
    //   6355: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   6358: getstatic 130	com/chelpus/root/utils/custompatch:sddir	Ljava/lang/String;
    //   6361: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6364: ldc -36
    //   6366: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6369: aload 20
    //   6371: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6374: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   6377: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   6380: new 190	java/io/File
    //   6383: dup
    //   6384: aload 27
    //   6386: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   6389: invokestatic 444	com/chelpus/Utils:copyFile	(Ljava/io/File;Ljava/io/File;)V
    //   6392: new 190	java/io/File
    //   6395: dup
    //   6396: aload 27
    //   6398: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   6401: invokevirtual 198	java/io/File:exists	()Z
    //   6404: ifeq +107 -> 6511
    //   6407: new 190	java/io/File
    //   6410: dup
    //   6411: aload 27
    //   6413: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   6416: invokevirtual 633	java/io/File:length	()J
    //   6419: new 190	java/io/File
    //   6422: dup
    //   6423: new 174	java/lang/StringBuilder
    //   6426: dup
    //   6427: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   6430: getstatic 130	com/chelpus/root/utils/custompatch:sddir	Ljava/lang/String;
    //   6433: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6436: ldc -36
    //   6438: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6441: aload 20
    //   6443: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6446: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   6449: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   6452: invokevirtual 633	java/io/File:length	()J
    //   6455: lcmp
    //   6456: ifne +55 -> 6511
    //   6459: ldc_w 654
    //   6462: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6465: iconst_3
    //   6466: anewarray 291	java/lang/String
    //   6469: dup
    //   6470: iconst_0
    //   6471: ldc_w 435
    //   6474: aastore
    //   6475: dup
    //   6476: iconst_1
    //   6477: ldc_w 437
    //   6480: aastore
    //   6481: dup
    //   6482: iconst_2
    //   6483: aload 27
    //   6485: aastore
    //   6486: invokestatic 440	com/chelpus/Utils:run_all_no_root	([Ljava/lang/String;)V
    //   6489: iload_1
    //   6490: istore 14
    //   6492: aload 19
    //   6494: astore 31
    //   6496: iload 5
    //   6498: istore 9
    //   6500: iload 15
    //   6502: istore 18
    //   6504: iload 16
    //   6506: istore 17
    //   6508: goto -5232 -> 1276
    //   6511: ldc_w 656
    //   6514: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6517: goto -52 -> 6465
    //   6520: ldc_w 453
    //   6523: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6526: ldc_w 658
    //   6529: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6532: ldc_w 457
    //   6535: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6538: ldc_w 660
    //   6541: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6544: iload 8
    //   6546: istore 10
    //   6548: iload_1
    //   6549: istore 14
    //   6551: aload 19
    //   6553: astore 31
    //   6555: iload 5
    //   6557: istore 9
    //   6559: iload 15
    //   6561: istore 18
    //   6563: iload 16
    //   6565: istore 17
    //   6567: goto -5291 -> 1276
    //   6570: ldc_w 662
    //   6573: astore 22
    //   6575: goto -4841 -> 1734
    //   6578: aload 22
    //   6580: astore 21
    //   6582: iconst_1
    //   6583: putstatic 148	com/chelpus/root/utils/custompatch:dataBaseExist	Z
    //   6586: goto -4852 -> 1734
    //   6589: astore 19
    //   6591: ldc_w 664
    //   6594: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6597: aload 21
    //   6599: astore 22
    //   6601: goto -4794 -> 1807
    //   6604: astore 22
    //   6606: aload 21
    //   6608: astore 19
    //   6610: getstatic 670	java/lang/System:out	Ljava/io/PrintStream;
    //   6613: new 174	java/lang/StringBuilder
    //   6616: dup
    //   6617: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   6620: ldc_w 672
    //   6623: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6626: aload 22
    //   6628: invokevirtual 598	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   6631: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   6634: invokevirtual 677	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   6637: aload 21
    //   6639: astore 19
    //   6641: goto -4694 -> 1947
    //   6644: astore 21
    //   6646: ldc_w 679
    //   6649: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6652: goto -4705 -> 1947
    //   6655: astore 21
    //   6657: ldc_w 681
    //   6660: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6663: goto -4613 -> 2050
    //   6666: astore 19
    //   6668: aload 19
    //   6670: invokevirtual 682	org/json/JSONException:printStackTrace	()V
    //   6673: ldc_w 681
    //   6676: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6679: goto -4555 -> 2124
    //   6682: astore 19
    //   6684: ldc_w 684
    //   6687: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6690: aload 22
    //   6692: astore 19
    //   6694: aload 33
    //   6696: astore 32
    //   6698: goto -4446 -> 2252
    //   6701: astore 20
    //   6703: aload 20
    //   6705: invokevirtual 682	org/json/JSONException:printStackTrace	()V
    //   6708: ldc_w 681
    //   6711: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6714: aload 19
    //   6716: astore 20
    //   6718: goto -4411 -> 2307
    //   6721: astore 19
    //   6723: ldc_w 686
    //   6726: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6729: aload 20
    //   6731: astore 22
    //   6733: aload 27
    //   6735: astore 33
    //   6737: goto -4314 -> 2423
    //   6740: astore 19
    //   6742: ldc_w 688
    //   6745: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6748: aload 21
    //   6750: astore 19
    //   6752: goto -4285 -> 2467
    //   6755: sipush 200
    //   6758: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   6761: iload 5
    //   6763: istore 10
    //   6765: iload 7
    //   6767: istore 8
    //   6769: iload 11
    //   6771: istore 6
    //   6773: iload 9
    //   6775: istore_1
    //   6776: goto -4145 -> 2631
    //   6779: sipush 200
    //   6782: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   6785: iload 10
    //   6787: istore 11
    //   6789: iload 8
    //   6791: istore 9
    //   6793: iload 6
    //   6795: istore 7
    //   6797: iload_1
    //   6798: istore 5
    //   6800: goto -4096 -> 2704
    //   6803: sipush 200
    //   6806: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   6809: iload 11
    //   6811: istore 10
    //   6813: iload 9
    //   6815: istore 8
    //   6817: iload 7
    //   6819: istore 6
    //   6821: iload 5
    //   6823: istore_1
    //   6824: goto -4048 -> 2776
    //   6827: sipush 200
    //   6830: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   6833: iload 10
    //   6835: istore 11
    //   6837: iload 8
    //   6839: istore 9
    //   6841: iload 6
    //   6843: istore 7
    //   6845: iload_1
    //   6846: istore 5
    //   6848: goto -3999 -> 2849
    //   6851: astore 20
    //   6853: ldc_w 690
    //   6856: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6859: ldc -124
    //   6861: putstatic 154	com/chelpus/root/utils/custompatch:group	Ljava/lang/String;
    //   6864: goto -3727 -> 3137
    //   6867: astore 20
    //   6869: ldc_w 690
    //   6872: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6875: goto -3591 -> 3284
    //   6878: aload 25
    //   6880: iload 11
    //   6882: iconst_0
    //   6883: iastore
    //   6884: goto -3422 -> 3462
    //   6887: astore 20
    //   6889: new 174	java/lang/StringBuilder
    //   6892: dup
    //   6893: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   6896: ldc_w 692
    //   6899: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6902: aload 20
    //   6904: invokevirtual 598	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   6907: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   6910: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   6913: aload 19
    //   6915: astore 21
    //   6917: iload 17
    //   6919: istore 15
    //   6921: iload 10
    //   6923: istore 9
    //   6925: iload_3
    //   6926: istore_1
    //   6927: aload 25
    //   6929: astore 27
    //   6931: aload 26
    //   6933: astore 20
    //   6935: iload 15
    //   6937: istore 17
    //   6939: aload 24
    //   6941: astore 19
    //   6943: aload 38
    //   6945: iload 12
    //   6947: aaload
    //   6948: ldc_w 694
    //   6951: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   6954: ifeq +229 -> 7183
    //   6957: iload 15
    //   6959: istore 17
    //   6961: aload 24
    //   6963: astore 19
    //   6965: aload 38
    //   6967: iload 12
    //   6969: aaload
    //   6970: ldc_w 411
    //   6973: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   6976: ifeq +207 -> 7183
    //   6979: aload 38
    //   6981: iload 12
    //   6983: aaload
    //   6984: ldc_w 521
    //   6987: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   6990: istore 16
    //   6992: iload 15
    //   6994: istore 17
    //   6996: aload 24
    //   6998: astore 19
    //   7000: iload 16
    //   7002: ifeq +181 -> 7183
    //   7005: new 428	org/json/JSONObject
    //   7008: dup
    //   7009: aload 38
    //   7011: iload 12
    //   7013: aaload
    //   7014: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   7017: ldc_w 696
    //   7020: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   7023: astore 19
    //   7025: new 174	java/lang/StringBuilder
    //   7028: dup
    //   7029: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   7032: ldc_w 698
    //   7035: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7038: aload_0
    //   7039: iconst_5
    //   7040: aaload
    //   7041: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7044: ldc_w 692
    //   7047: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7050: aload_0
    //   7051: bipush 8
    //   7053: aaload
    //   7054: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7057: ldc_w 700
    //   7060: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7063: aload_0
    //   7064: iconst_0
    //   7065: aaload
    //   7066: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7069: ldc_w 692
    //   7072: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7075: ldc_w 696
    //   7078: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7081: aload 19
    //   7083: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7086: ldc -75
    //   7088: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7091: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   7094: astore 24
    //   7096: invokestatic 706	java/lang/Runtime:getRuntime	()Ljava/lang/Runtime;
    //   7099: aload 24
    //   7101: invokevirtual 710	java/lang/Runtime:exec	(Ljava/lang/String;)Ljava/lang/Process;
    //   7104: astore 24
    //   7106: aload 24
    //   7108: invokevirtual 715	java/lang/Process:waitFor	()I
    //   7111: pop
    //   7112: new 717	java/io/DataInputStream
    //   7115: dup
    //   7116: aload 24
    //   7118: invokevirtual 721	java/lang/Process:getInputStream	()Ljava/io/InputStream;
    //   7121: invokespecial 724	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   7124: astore 25
    //   7126: aload 25
    //   7128: invokevirtual 727	java/io/DataInputStream:available	()I
    //   7131: newarray <illegal type>
    //   7133: astore 26
    //   7135: aload 25
    //   7137: aload 26
    //   7139: invokevirtual 731	java/io/DataInputStream:read	([B)I
    //   7142: pop
    //   7143: new 291	java/lang/String
    //   7146: dup
    //   7147: aload 26
    //   7149: invokespecial 734	java/lang/String:<init>	([B)V
    //   7152: astore 25
    //   7154: aload 24
    //   7156: invokevirtual 737	java/lang/Process:destroy	()V
    //   7159: aload 25
    //   7161: ldc_w 739
    //   7164: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7167: ifeq +408 -> 7575
    //   7170: ldc_w 741
    //   7173: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   7176: iconst_1
    //   7177: istore 17
    //   7179: iconst_1
    //   7180: putstatic 116	com/chelpus/root/utils/custompatch:manualpatch	Z
    //   7183: aload 20
    //   7185: astore 25
    //   7187: aload 27
    //   7189: astore 26
    //   7191: iload_1
    //   7192: istore_2
    //   7193: iload 9
    //   7195: istore_3
    //   7196: iload 18
    //   7198: istore 16
    //   7200: aload 19
    //   7202: astore 24
    //   7204: aload 38
    //   7206: iload 12
    //   7208: aaload
    //   7209: ldc_w 742
    //   7212: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7215: ifeq +436 -> 7651
    //   7218: aload 20
    //   7220: astore 25
    //   7222: aload 27
    //   7224: astore 26
    //   7226: iload_1
    //   7227: istore_2
    //   7228: iload 9
    //   7230: istore_3
    //   7231: iload 18
    //   7233: istore 16
    //   7235: aload 19
    //   7237: astore 24
    //   7239: aload 38
    //   7241: iload 12
    //   7243: aaload
    //   7244: ldc_w 411
    //   7247: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7250: ifeq +401 -> 7651
    //   7253: aload 38
    //   7255: iload 12
    //   7257: aaload
    //   7258: ldc_w 521
    //   7261: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7264: istore 15
    //   7266: aload 20
    //   7268: astore 25
    //   7270: aload 27
    //   7272: astore 26
    //   7274: iload_1
    //   7275: istore_2
    //   7276: iload 9
    //   7278: istore_3
    //   7279: iload 18
    //   7281: istore 16
    //   7283: aload 19
    //   7285: astore 24
    //   7287: iload 15
    //   7289: ifeq +362 -> 7651
    //   7292: new 428	org/json/JSONObject
    //   7295: dup
    //   7296: aload 38
    //   7298: iload 12
    //   7300: aaload
    //   7301: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   7304: ldc_w 742
    //   7307: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   7310: astore 20
    //   7312: aload 20
    //   7314: astore 19
    //   7316: aload 19
    //   7318: invokevirtual 530	java/lang/String:trim	()Ljava/lang/String;
    //   7321: astore 19
    //   7323: aload 19
    //   7325: astore 24
    //   7327: getstatic 146	com/chelpus/root/utils/custompatch:convert	Z
    //   7330: ifeq +10 -> 7340
    //   7333: aload 19
    //   7335: invokestatic 533	com/chelpus/Utils:rework	(Ljava/lang/String;)Ljava/lang/String;
    //   7338: astore 24
    //   7340: aload 24
    //   7342: ldc_w 535
    //   7345: invokevirtual 539	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   7348: arraylength
    //   7349: anewarray 291	java/lang/String
    //   7352: astore 19
    //   7354: aload 24
    //   7356: ldc_w 535
    //   7359: invokevirtual 539	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   7362: astore 19
    //   7364: aload 19
    //   7366: arraylength
    //   7367: newarray <illegal type>
    //   7369: astore 26
    //   7371: aload 19
    //   7373: arraylength
    //   7374: newarray <illegal type>
    //   7376: astore 25
    //   7378: iconst_0
    //   7379: istore 10
    //   7381: iload_1
    //   7382: istore_3
    //   7383: iload_1
    //   7384: istore_2
    //   7385: iload 10
    //   7387: aload 19
    //   7389: arraylength
    //   7390: if_icmpge +245 -> 7635
    //   7393: iload_1
    //   7394: istore_2
    //   7395: iload_1
    //   7396: istore_3
    //   7397: aload 19
    //   7399: iload 10
    //   7401: aaload
    //   7402: ldc_w 541
    //   7405: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7408: ifeq +31 -> 7439
    //   7411: iload_1
    //   7412: istore_2
    //   7413: iload_1
    //   7414: istore_3
    //   7415: aload 19
    //   7417: iload 10
    //   7419: aaload
    //   7420: ldc_w 543
    //   7423: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7426: ifne +13 -> 7439
    //   7429: iconst_1
    //   7430: istore_2
    //   7431: aload 19
    //   7433: iload 10
    //   7435: ldc_w 545
    //   7438: aastore
    //   7439: iload_2
    //   7440: istore_3
    //   7441: aload 19
    //   7443: iload 10
    //   7445: aaload
    //   7446: ldc_w 543
    //   7449: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7452: ifne +2319 -> 9771
    //   7455: iload_2
    //   7456: istore_3
    //   7457: aload 19
    //   7459: iload 10
    //   7461: aaload
    //   7462: ldc_w 547
    //   7465: invokevirtual 550	java/lang/String:matches	(Ljava/lang/String;)Z
    //   7468: ifeq +130 -> 7598
    //   7471: goto +2300 -> 9771
    //   7474: iload_2
    //   7475: istore_3
    //   7476: aload 19
    //   7478: iload 10
    //   7480: aaload
    //   7481: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   7484: ldc_w 556
    //   7487: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7490: ifeq +39 -> 7529
    //   7493: iload_2
    //   7494: istore_3
    //   7495: aload 26
    //   7497: iload 10
    //   7499: aload 19
    //   7501: iload 10
    //   7503: aaload
    //   7504: ldc_w 556
    //   7507: ldc -124
    //   7509: invokevirtual 397	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   7512: invokestatic 563	java/lang/Integer:valueOf	(Ljava/lang/String;)Ljava/lang/Integer;
    //   7515: invokevirtual 566	java/lang/Integer:intValue	()I
    //   7518: iconst_2
    //   7519: iadd
    //   7520: iastore
    //   7521: aload 19
    //   7523: iload 10
    //   7525: ldc_w 545
    //   7528: aastore
    //   7529: iload_2
    //   7530: istore_3
    //   7531: aload 25
    //   7533: iload 10
    //   7535: aload 19
    //   7537: iload 10
    //   7539: aaload
    //   7540: bipush 16
    //   7542: invokestatic 569	java/lang/Integer:valueOf	(Ljava/lang/String;I)Ljava/lang/Integer;
    //   7545: invokevirtual 573	java/lang/Integer:byteValue	()B
    //   7548: bastore
    //   7549: iload 10
    //   7551: iconst_1
    //   7552: iadd
    //   7553: istore 10
    //   7555: iload_2
    //   7556: istore_1
    //   7557: goto -176 -> 7381
    //   7560: astore 19
    //   7562: ldc_w 744
    //   7565: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   7568: aload 24
    //   7570: astore 19
    //   7572: goto -547 -> 7025
    //   7575: ldc_w 746
    //   7578: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   7581: iconst_0
    //   7582: istore 17
    //   7584: goto -405 -> 7179
    //   7587: astore 20
    //   7589: ldc_w 748
    //   7592: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   7595: goto -279 -> 7316
    //   7598: aload 26
    //   7600: iload 10
    //   7602: iconst_0
    //   7603: iastore
    //   7604: goto -130 -> 7474
    //   7607: astore 19
    //   7609: new 174	java/lang/StringBuilder
    //   7612: dup
    //   7613: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   7616: ldc_w 692
    //   7619: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7622: aload 19
    //   7624: invokevirtual 598	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   7627: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   7630: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   7633: iload_3
    //   7634: istore_2
    //   7635: iload_2
    //   7636: ifeq +532 -> 8168
    //   7639: iconst_0
    //   7640: istore 16
    //   7642: ldc_w 750
    //   7645: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   7648: iload 9
    //   7650: istore_3
    //   7651: iload_2
    //   7652: istore_1
    //   7653: iload 16
    //   7655: istore 15
    //   7657: aload 23
    //   7659: astore 19
    //   7661: aload 38
    //   7663: iload 12
    //   7665: aaload
    //   7666: ldc_w 752
    //   7669: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7672: ifeq +745 -> 8417
    //   7675: iload_2
    //   7676: istore_1
    //   7677: iload 16
    //   7679: istore 15
    //   7681: aload 23
    //   7683: astore 19
    //   7685: aload 38
    //   7687: iload 12
    //   7689: aaload
    //   7690: ldc_w 411
    //   7693: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7696: ifeq +721 -> 8417
    //   7699: aload 38
    //   7701: iload 12
    //   7703: aaload
    //   7704: ldc_w 521
    //   7707: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7710: istore 18
    //   7712: iload_2
    //   7713: istore_1
    //   7714: iload 16
    //   7716: istore 15
    //   7718: aload 23
    //   7720: astore 19
    //   7722: iload 18
    //   7724: ifeq +693 -> 8417
    //   7727: new 428	org/json/JSONObject
    //   7730: dup
    //   7731: aload 38
    //   7733: iload 12
    //   7735: aaload
    //   7736: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   7739: ldc_w 752
    //   7742: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   7745: astore 19
    //   7747: aload 19
    //   7749: invokevirtual 530	java/lang/String:trim	()Ljava/lang/String;
    //   7752: astore 19
    //   7754: aload 19
    //   7756: astore 20
    //   7758: getstatic 146	com/chelpus/root/utils/custompatch:convert	Z
    //   7761: ifeq +10 -> 7771
    //   7764: aload 19
    //   7766: invokestatic 533	com/chelpus/Utils:rework	(Ljava/lang/String;)Ljava/lang/String;
    //   7769: astore 20
    //   7771: aload 20
    //   7773: ldc_w 535
    //   7776: invokevirtual 539	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   7779: arraylength
    //   7780: anewarray 291	java/lang/String
    //   7783: astore 19
    //   7785: aload 20
    //   7787: ldc_w 535
    //   7790: invokevirtual 539	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   7793: astore 19
    //   7795: aload 19
    //   7797: arraylength
    //   7798: newarray <illegal type>
    //   7800: astore 23
    //   7802: aload 19
    //   7804: arraylength
    //   7805: newarray <illegal type>
    //   7807: astore 27
    //   7809: iconst_0
    //   7810: istore 10
    //   7812: iload_2
    //   7813: istore 9
    //   7815: iload_2
    //   7816: istore_1
    //   7817: iload 10
    //   7819: aload 19
    //   7821: arraylength
    //   7822: if_icmpge +478 -> 8300
    //   7825: iload_2
    //   7826: istore_1
    //   7827: iload_2
    //   7828: istore 9
    //   7830: aload 19
    //   7832: iload 10
    //   7834: aaload
    //   7835: ldc_w 541
    //   7838: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7841: ifeq +32 -> 7873
    //   7844: iload_2
    //   7845: istore_1
    //   7846: iload_2
    //   7847: istore 9
    //   7849: aload 19
    //   7851: iload 10
    //   7853: aaload
    //   7854: ldc_w 543
    //   7857: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7860: ifne +13 -> 7873
    //   7863: iconst_1
    //   7864: istore_1
    //   7865: aload 19
    //   7867: iload 10
    //   7869: ldc_w 545
    //   7872: aastore
    //   7873: iload_1
    //   7874: istore 9
    //   7876: aload 19
    //   7878: iload 10
    //   7880: aaload
    //   7881: ldc_w 543
    //   7884: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7887: ifne +1901 -> 9788
    //   7890: iload_1
    //   7891: istore 9
    //   7893: aload 19
    //   7895: iload 10
    //   7897: aaload
    //   7898: ldc_w 547
    //   7901: invokevirtual 550	java/lang/String:matches	(Ljava/lang/String;)Z
    //   7904: ifeq +358 -> 8262
    //   7907: goto +1881 -> 9788
    //   7910: iload_1
    //   7911: istore 9
    //   7913: aload 19
    //   7915: iload 10
    //   7917: aaload
    //   7918: invokevirtual 498	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   7921: ldc_w 754
    //   7924: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7927: ifeq +19 -> 7946
    //   7930: aload 19
    //   7932: iload 10
    //   7934: ldc_w 545
    //   7937: aastore
    //   7938: aload 23
    //   7940: iload 10
    //   7942: sipush 253
    //   7945: iastore
    //   7946: iload_1
    //   7947: istore 9
    //   7949: aload 19
    //   7951: iload 10
    //   7953: aaload
    //   7954: ldc_w 756
    //   7957: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7960: ifne +1845 -> 9805
    //   7963: iload_1
    //   7964: istore 9
    //   7966: aload 19
    //   7968: iload 10
    //   7970: aaload
    //   7971: ldc_w 758
    //   7974: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7977: ifeq +6 -> 7983
    //   7980: goto +1825 -> 9805
    //   7983: iload_1
    //   7984: istore 9
    //   7986: aload 19
    //   7988: iload 10
    //   7990: aaload
    //   7991: ldc_w 760
    //   7994: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   7997: ifne +1827 -> 9824
    //   8000: iload_1
    //   8001: istore 9
    //   8003: aload 19
    //   8005: iload 10
    //   8007: aaload
    //   8008: ldc_w 762
    //   8011: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8014: ifeq +6 -> 8020
    //   8017: goto +1807 -> 9824
    //   8020: iload_1
    //   8021: istore 9
    //   8023: aload 19
    //   8025: iload 10
    //   8027: aaload
    //   8028: ldc_w 552
    //   8031: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8034: ifne +54 -> 8088
    //   8037: iload_1
    //   8038: istore 9
    //   8040: aload 19
    //   8042: iload 10
    //   8044: aaload
    //   8045: ldc_w 554
    //   8048: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8051: ifne +37 -> 8088
    //   8054: iload_1
    //   8055: istore 9
    //   8057: aload 19
    //   8059: iload 10
    //   8061: aaload
    //   8062: ldc_w 556
    //   8065: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8068: ifne +20 -> 8088
    //   8071: iload_1
    //   8072: istore 9
    //   8074: aload 19
    //   8076: iload 10
    //   8078: aaload
    //   8079: ldc_w 556
    //   8082: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8085: ifeq +51 -> 8136
    //   8088: iload_1
    //   8089: istore 9
    //   8091: aload 23
    //   8093: iload 10
    //   8095: aload 19
    //   8097: iload 10
    //   8099: aaload
    //   8100: invokevirtual 498	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   8103: ldc_w 554
    //   8106: ldc -124
    //   8108: invokevirtual 397	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   8111: ldc_w 558
    //   8114: ldc -124
    //   8116: invokevirtual 397	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   8119: invokestatic 563	java/lang/Integer:valueOf	(Ljava/lang/String;)Ljava/lang/Integer;
    //   8122: invokevirtual 566	java/lang/Integer:intValue	()I
    //   8125: iconst_2
    //   8126: iadd
    //   8127: iastore
    //   8128: aload 19
    //   8130: iload 10
    //   8132: ldc_w 545
    //   8135: aastore
    //   8136: iload_1
    //   8137: istore 9
    //   8139: aload 27
    //   8141: iload 10
    //   8143: aload 19
    //   8145: iload 10
    //   8147: aaload
    //   8148: bipush 16
    //   8150: invokestatic 569	java/lang/Integer:valueOf	(Ljava/lang/String;I)Ljava/lang/Integer;
    //   8153: invokevirtual 573	java/lang/Integer:byteValue	()B
    //   8156: bastore
    //   8157: iload 10
    //   8159: iconst_1
    //   8160: iadd
    //   8161: istore 10
    //   8163: iload_1
    //   8164: istore_2
    //   8165: goto -353 -> 7812
    //   8168: iconst_1
    //   8169: istore_3
    //   8170: new 764	com/android/vending/billing/InAppBillingService/LUCK/SearchItem
    //   8173: dup
    //   8174: aload 25
    //   8176: aload 26
    //   8178: invokespecial 767	com/android/vending/billing/InAppBillingService/LUCK/SearchItem:<init>	([B[I)V
    //   8181: astore 19
    //   8183: aload 19
    //   8185: aload 25
    //   8187: arraylength
    //   8188: newarray <illegal type>
    //   8190: putfield 770	com/android/vending/billing/InAppBillingService/LUCK/SearchItem:repByte	[B
    //   8193: getstatic 106	com/chelpus/root/utils/custompatch:ser	Ljava/util/ArrayList;
    //   8196: aload 19
    //   8198: invokevirtual 773	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   8201: pop
    //   8202: iload 18
    //   8204: istore 16
    //   8206: goto -555 -> 7651
    //   8209: astore 19
    //   8211: aload 19
    //   8213: invokevirtual 315	java/lang/Exception:printStackTrace	()V
    //   8216: new 174	java/lang/StringBuilder
    //   8219: dup
    //   8220: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   8223: ldc_w 692
    //   8226: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8229: aload 19
    //   8231: invokevirtual 598	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   8234: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   8237: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   8240: iload 18
    //   8242: istore 16
    //   8244: goto -593 -> 7651
    //   8247: astore 19
    //   8249: ldc_w 775
    //   8252: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   8255: aload 23
    //   8257: astore 19
    //   8259: goto -512 -> 7747
    //   8262: aload 23
    //   8264: iload 10
    //   8266: iconst_1
    //   8267: iastore
    //   8268: goto -358 -> 7910
    //   8271: astore 19
    //   8273: new 174	java/lang/StringBuilder
    //   8276: dup
    //   8277: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   8280: ldc_w 692
    //   8283: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8286: aload 19
    //   8288: invokevirtual 598	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   8291: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   8294: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   8297: iload 9
    //   8299: istore_1
    //   8300: aload 23
    //   8302: arraylength
    //   8303: aload 26
    //   8305: arraylength
    //   8306: if_icmpne +1537 -> 9843
    //   8309: aload 25
    //   8311: arraylength
    //   8312: aload 27
    //   8314: arraylength
    //   8315: if_icmpne +1528 -> 9843
    //   8318: aload 27
    //   8320: arraylength
    //   8321: iconst_4
    //   8322: if_icmplt +1521 -> 9843
    //   8325: iload_1
    //   8326: istore_2
    //   8327: aload 25
    //   8329: arraylength
    //   8330: iconst_4
    //   8331: if_icmpge +6 -> 8337
    //   8334: goto +1509 -> 9843
    //   8337: iload_2
    //   8338: ifeq +12 -> 8350
    //   8341: iconst_0
    //   8342: istore 16
    //   8344: ldc_w 777
    //   8347: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   8350: iload_2
    //   8351: istore_1
    //   8352: iload 16
    //   8354: istore 15
    //   8356: aload 20
    //   8358: astore 19
    //   8360: iload_2
    //   8361: ifne +56 -> 8417
    //   8364: getstatic 168	com/chelpus/root/utils/custompatch:multilib_patch	Z
    //   8367: ifne +9 -> 8376
    //   8370: getstatic 164	com/chelpus/root/utils/custompatch:multidex	Z
    //   8373: ifeq +565 -> 8938
    //   8376: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   8379: new 779	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
    //   8382: dup
    //   8383: aload 25
    //   8385: aload 26
    //   8387: aload 27
    //   8389: aload 23
    //   8391: ldc_w 781
    //   8394: iconst_0
    //   8395: invokespecial 784	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:<init>	([B[I[B[ILjava/lang/String;Z)V
    //   8398: invokevirtual 773	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   8401: pop
    //   8402: ldc -124
    //   8404: putstatic 154	com/chelpus/root/utils/custompatch:group	Ljava/lang/String;
    //   8407: aload 20
    //   8409: astore 19
    //   8411: iload 16
    //   8413: istore 15
    //   8415: iload_2
    //   8416: istore_1
    //   8417: iload_1
    //   8418: istore_2
    //   8419: iload 15
    //   8421: istore 16
    //   8423: aload 19
    //   8425: astore 20
    //   8427: aload 38
    //   8429: iload 12
    //   8431: aaload
    //   8432: ldc_w 786
    //   8435: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8438: ifeq +665 -> 9103
    //   8441: iload_1
    //   8442: istore_2
    //   8443: iload 15
    //   8445: istore 16
    //   8447: aload 19
    //   8449: astore 20
    //   8451: aload 38
    //   8453: iload 12
    //   8455: aaload
    //   8456: ldc_w 411
    //   8459: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8462: ifeq +641 -> 9103
    //   8465: aload 38
    //   8467: iload 12
    //   8469: aaload
    //   8470: ldc_w 521
    //   8473: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8476: istore 18
    //   8478: iload_1
    //   8479: istore_2
    //   8480: iload 15
    //   8482: istore 16
    //   8484: aload 19
    //   8486: astore 20
    //   8488: iload 18
    //   8490: ifeq +613 -> 9103
    //   8493: new 428	org/json/JSONObject
    //   8496: dup
    //   8497: aload 38
    //   8499: iload 12
    //   8501: aaload
    //   8502: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   8505: ldc_w 786
    //   8508: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   8511: astore 20
    //   8513: aload 20
    //   8515: astore 19
    //   8517: aload 19
    //   8519: invokevirtual 530	java/lang/String:trim	()Ljava/lang/String;
    //   8522: astore 20
    //   8524: aload 20
    //   8526: astore 19
    //   8528: getstatic 146	com/chelpus/root/utils/custompatch:convert	Z
    //   8531: ifeq +10 -> 8541
    //   8534: aload 20
    //   8536: invokestatic 533	com/chelpus/Utils:rework	(Ljava/lang/String;)Ljava/lang/String;
    //   8539: astore 19
    //   8541: aload 19
    //   8543: ldc_w 535
    //   8546: invokevirtual 539	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   8549: arraylength
    //   8550: anewarray 291	java/lang/String
    //   8553: astore 20
    //   8555: aload 19
    //   8557: ldc_w 535
    //   8560: invokevirtual 539	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   8563: astore 20
    //   8565: aload 20
    //   8567: arraylength
    //   8568: newarray <illegal type>
    //   8570: astore 23
    //   8572: aload 20
    //   8574: arraylength
    //   8575: newarray <illegal type>
    //   8577: astore 27
    //   8579: iconst_0
    //   8580: istore 10
    //   8582: iload_1
    //   8583: istore 9
    //   8585: iload_1
    //   8586: istore_2
    //   8587: iload 10
    //   8589: aload 20
    //   8591: arraylength
    //   8592: if_icmpge +424 -> 9016
    //   8595: iload_1
    //   8596: istore_2
    //   8597: iload_1
    //   8598: istore 9
    //   8600: aload 20
    //   8602: iload 10
    //   8604: aaload
    //   8605: ldc_w 541
    //   8608: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8611: ifeq +32 -> 8643
    //   8614: iload_1
    //   8615: istore_2
    //   8616: iload_1
    //   8617: istore 9
    //   8619: aload 20
    //   8621: iload 10
    //   8623: aaload
    //   8624: ldc_w 543
    //   8627: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8630: ifne +13 -> 8643
    //   8633: iconst_1
    //   8634: istore_2
    //   8635: aload 20
    //   8637: iload 10
    //   8639: ldc_w 545
    //   8642: aastore
    //   8643: iload_2
    //   8644: istore 9
    //   8646: aload 20
    //   8648: iload 10
    //   8650: aaload
    //   8651: ldc_w 543
    //   8654: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8657: ifne +1191 -> 9848
    //   8660: iload_2
    //   8661: istore 9
    //   8663: aload 20
    //   8665: iload 10
    //   8667: aaload
    //   8668: ldc_w 547
    //   8671: invokevirtual 550	java/lang/String:matches	(Ljava/lang/String;)Z
    //   8674: ifeq +304 -> 8978
    //   8677: goto +1171 -> 9848
    //   8680: iload_2
    //   8681: istore 9
    //   8683: aload 20
    //   8685: iload 10
    //   8687: aaload
    //   8688: invokevirtual 498	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   8691: ldc_w 754
    //   8694: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8697: ifeq +19 -> 8716
    //   8700: aload 20
    //   8702: iload 10
    //   8704: ldc_w 545
    //   8707: aastore
    //   8708: aload 23
    //   8710: iload 10
    //   8712: sipush 253
    //   8715: iastore
    //   8716: iload_2
    //   8717: istore 9
    //   8719: aload 20
    //   8721: iload 10
    //   8723: aaload
    //   8724: ldc_w 756
    //   8727: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8730: ifne +1135 -> 9865
    //   8733: iload_2
    //   8734: istore 9
    //   8736: aload 20
    //   8738: iload 10
    //   8740: aaload
    //   8741: ldc_w 758
    //   8744: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8747: ifeq +6 -> 8753
    //   8750: goto +1115 -> 9865
    //   8753: iload_2
    //   8754: istore 9
    //   8756: aload 20
    //   8758: iload 10
    //   8760: aaload
    //   8761: ldc_w 760
    //   8764: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8767: ifne +1117 -> 9884
    //   8770: iload_2
    //   8771: istore 9
    //   8773: aload 20
    //   8775: iload 10
    //   8777: aaload
    //   8778: ldc_w 762
    //   8781: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8784: ifeq +6 -> 8790
    //   8787: goto +1097 -> 9884
    //   8790: iload_2
    //   8791: istore 9
    //   8793: aload 20
    //   8795: iload 10
    //   8797: aaload
    //   8798: ldc_w 552
    //   8801: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8804: ifne +54 -> 8858
    //   8807: iload_2
    //   8808: istore 9
    //   8810: aload 20
    //   8812: iload 10
    //   8814: aaload
    //   8815: ldc_w 554
    //   8818: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8821: ifne +37 -> 8858
    //   8824: iload_2
    //   8825: istore 9
    //   8827: aload 20
    //   8829: iload 10
    //   8831: aaload
    //   8832: ldc_w 556
    //   8835: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8838: ifne +20 -> 8858
    //   8841: iload_2
    //   8842: istore 9
    //   8844: aload 20
    //   8846: iload 10
    //   8848: aaload
    //   8849: ldc_w 556
    //   8852: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   8855: ifeq +51 -> 8906
    //   8858: iload_2
    //   8859: istore 9
    //   8861: aload 23
    //   8863: iload 10
    //   8865: aload 20
    //   8867: iload 10
    //   8869: aaload
    //   8870: invokevirtual 498	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   8873: ldc_w 554
    //   8876: ldc -124
    //   8878: invokevirtual 397	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   8881: ldc_w 558
    //   8884: ldc -124
    //   8886: invokevirtual 397	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   8889: invokestatic 563	java/lang/Integer:valueOf	(Ljava/lang/String;)Ljava/lang/Integer;
    //   8892: invokevirtual 566	java/lang/Integer:intValue	()I
    //   8895: iconst_2
    //   8896: iadd
    //   8897: iastore
    //   8898: aload 20
    //   8900: iload 10
    //   8902: ldc_w 545
    //   8905: aastore
    //   8906: iload_2
    //   8907: istore 9
    //   8909: aload 27
    //   8911: iload 10
    //   8913: aload 20
    //   8915: iload 10
    //   8917: aaload
    //   8918: bipush 16
    //   8920: invokestatic 569	java/lang/Integer:valueOf	(Ljava/lang/String;I)Ljava/lang/Integer;
    //   8923: invokevirtual 573	java/lang/Integer:byteValue	()B
    //   8926: bastore
    //   8927: iload 10
    //   8929: iconst_1
    //   8930: iadd
    //   8931: istore 10
    //   8933: iload_2
    //   8934: istore_1
    //   8935: goto -353 -> 8582
    //   8938: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   8941: new 779	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
    //   8944: dup
    //   8945: aload 25
    //   8947: aload 26
    //   8949: aload 27
    //   8951: aload 23
    //   8953: getstatic 154	com/chelpus/root/utils/custompatch:group	Ljava/lang/String;
    //   8956: iconst_0
    //   8957: invokespecial 784	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:<init>	([B[I[B[ILjava/lang/String;Z)V
    //   8960: invokevirtual 773	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   8963: pop
    //   8964: goto -562 -> 8402
    //   8967: astore 20
    //   8969: ldc_w 788
    //   8972: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   8975: goto -458 -> 8517
    //   8978: aload 23
    //   8980: iload 10
    //   8982: iconst_1
    //   8983: iastore
    //   8984: goto -304 -> 8680
    //   8987: astore 20
    //   8989: new 174	java/lang/StringBuilder
    //   8992: dup
    //   8993: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   8996: ldc_w 692
    //   8999: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9002: aload 20
    //   9004: invokevirtual 598	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   9007: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   9010: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   9013: iload 9
    //   9015: istore_2
    //   9016: aload 27
    //   9018: arraylength
    //   9019: iconst_4
    //   9020: if_icmplt +883 -> 9903
    //   9023: iload_2
    //   9024: istore_1
    //   9025: aload 25
    //   9027: arraylength
    //   9028: iconst_4
    //   9029: if_icmpge +6 -> 9035
    //   9032: goto +871 -> 9903
    //   9035: iload_1
    //   9036: ifeq +12 -> 9048
    //   9039: iconst_0
    //   9040: istore 15
    //   9042: ldc_w 790
    //   9045: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   9048: iload_1
    //   9049: istore_2
    //   9050: iload 15
    //   9052: istore 16
    //   9054: aload 19
    //   9056: astore 20
    //   9058: iload_1
    //   9059: ifne +44 -> 9103
    //   9062: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   9065: new 779	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
    //   9068: dup
    //   9069: aload 25
    //   9071: aload 26
    //   9073: aload 27
    //   9075: aload 23
    //   9077: getstatic 154	com/chelpus/root/utils/custompatch:group	Ljava/lang/String;
    //   9080: iconst_1
    //   9081: invokespecial 784	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:<init>	([B[I[B[ILjava/lang/String;Z)V
    //   9084: invokevirtual 773	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   9087: pop
    //   9088: ldc -124
    //   9090: putstatic 154	com/chelpus/root/utils/custompatch:group	Ljava/lang/String;
    //   9093: aload 19
    //   9095: astore 20
    //   9097: iload 15
    //   9099: istore 16
    //   9101: iload_1
    //   9102: istore_2
    //   9103: iload 16
    //   9105: istore 15
    //   9107: aload 20
    //   9109: astore 23
    //   9111: aload 38
    //   9113: iload 12
    //   9115: aaload
    //   9116: ldc_w 792
    //   9119: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   9122: ifeq +236 -> 9358
    //   9125: iload 16
    //   9127: istore 15
    //   9129: aload 20
    //   9131: astore 23
    //   9133: aload 38
    //   9135: iload 12
    //   9137: aaload
    //   9138: ldc_w 411
    //   9141: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   9144: ifeq +214 -> 9358
    //   9147: aload 38
    //   9149: iload 12
    //   9151: aaload
    //   9152: ldc_w 521
    //   9155: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   9158: istore 18
    //   9160: iload 16
    //   9162: istore 15
    //   9164: aload 20
    //   9166: astore 23
    //   9168: iload 18
    //   9170: ifeq +188 -> 9358
    //   9173: new 428	org/json/JSONObject
    //   9176: dup
    //   9177: aload 38
    //   9179: iload 12
    //   9181: aaload
    //   9182: invokespecial 429	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   9185: ldc_w 792
    //   9188: invokevirtual 433	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   9191: astore 19
    //   9193: aload 19
    //   9195: invokevirtual 530	java/lang/String:trim	()Ljava/lang/String;
    //   9198: astore 19
    //   9200: new 190	java/io/File
    //   9203: dup
    //   9204: new 174	java/lang/StringBuilder
    //   9207: dup
    //   9208: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   9211: new 190	java/io/File
    //   9214: dup
    //   9215: aload_0
    //   9216: iconst_1
    //   9217: aaload
    //   9218: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   9221: invokestatic 641	com/chelpus/Utils:getDirs	(Ljava/io/File;)Ljava/io/File;
    //   9224: invokevirtual 598	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   9227: ldc -36
    //   9229: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9232: aload 19
    //   9234: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9237: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   9240: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   9243: astore 23
    //   9245: aload 23
    //   9247: invokevirtual 633	java/io/File:length	()J
    //   9250: l2i
    //   9251: istore_1
    //   9252: iload_1
    //   9253: newarray <illegal type>
    //   9255: astore 20
    //   9257: new 355	java/io/FileInputStream
    //   9260: dup
    //   9261: aload 23
    //   9263: invokespecial 794	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   9266: astore 23
    //   9268: aload 23
    //   9270: aload 20
    //   9272: invokevirtual 795	java/io/FileInputStream:read	([B)I
    //   9275: ifgt -7 -> 9268
    //   9278: aload 23
    //   9280: invokevirtual 575	java/io/FileInputStream:close	()V
    //   9283: iload_1
    //   9284: newarray <illegal type>
    //   9286: astore 27
    //   9288: aload 27
    //   9290: iconst_1
    //   9291: invokestatic 801	java/util/Arrays:fill	([II)V
    //   9294: iload_2
    //   9295: ifeq +12 -> 9307
    //   9298: iconst_0
    //   9299: istore 16
    //   9301: ldc_w 777
    //   9304: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   9307: iload 16
    //   9309: istore 15
    //   9311: aload 19
    //   9313: astore 23
    //   9315: iload_2
    //   9316: ifne +42 -> 9358
    //   9319: getstatic 104	com/chelpus/root/utils/custompatch:pat	Ljava/util/ArrayList;
    //   9322: new 779	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
    //   9325: dup
    //   9326: aload 25
    //   9328: aload 26
    //   9330: aload 20
    //   9332: aload 27
    //   9334: getstatic 154	com/chelpus/root/utils/custompatch:group	Ljava/lang/String;
    //   9337: iconst_0
    //   9338: invokespecial 784	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:<init>	([B[I[B[ILjava/lang/String;Z)V
    //   9341: invokevirtual 773	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   9344: pop
    //   9345: ldc -124
    //   9347: putstatic 154	com/chelpus/root/utils/custompatch:group	Ljava/lang/String;
    //   9350: aload 19
    //   9352: astore 23
    //   9354: iload 16
    //   9356: istore 15
    //   9358: aload 38
    //   9360: iload 12
    //   9362: aaload
    //   9363: invokevirtual 374	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   9366: ldc_w 803
    //   9369: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   9372: ifeq +9 -> 9381
    //   9375: ldc_w 805
    //   9378: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   9381: aload 28
    //   9383: astore 27
    //   9385: iload 13
    //   9387: ifeq +33 -> 9420
    //   9390: new 174	java/lang/StringBuilder
    //   9393: dup
    //   9394: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   9397: aload 28
    //   9399: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9402: ldc -75
    //   9404: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9407: aload 38
    //   9409: iload 12
    //   9411: aaload
    //   9412: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9415: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   9418: astore 27
    //   9420: aload 29
    //   9422: ldc_w 807
    //   9425: invokevirtual 295	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   9428: ifeq +480 -> 9908
    //   9431: iconst_4
    //   9432: putstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   9435: iconst_1
    //   9436: istore 13
    //   9438: goto +470 -> 9908
    //   9441: astore 19
    //   9443: ldc_w 775
    //   9446: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   9449: aload 20
    //   9451: astore 19
    //   9453: goto -260 -> 9193
    //   9456: astore 23
    //   9458: aload 23
    //   9460: invokevirtual 315	java/lang/Exception:printStackTrace	()V
    //   9463: goto -180 -> 9283
    //   9466: aload 37
    //   9468: invokevirtual 574	java/io/BufferedReader:close	()V
    //   9471: iload 16
    //   9473: ifeq +26 -> 9499
    //   9476: new 174	java/lang/StringBuilder
    //   9479: dup
    //   9480: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   9483: ldc -124
    //   9485: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9488: aload 28
    //   9490: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9493: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   9496: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   9499: iload 16
    //   9501: ifne +15 -> 9516
    //   9504: getstatic 110	com/chelpus/root/utils/custompatch:patchteil	Z
    //   9507: ifeq +148 -> 9655
    //   9510: ldc_w 809
    //   9513: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   9516: iload_1
    //   9517: ifeq +27 -> 9544
    //   9520: new 174	java/lang/StringBuilder
    //   9523: dup
    //   9524: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   9527: ldc_w 811
    //   9530: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9533: aload 19
    //   9535: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9538: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   9541: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   9544: getstatic 114	com/chelpus/root/utils/custompatch:fixunpack	Z
    //   9547: ifeq +95 -> 9642
    //   9550: ldc_w 813
    //   9553: invokestatic 816	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   9556: pop
    //   9557: getstatic 122	com/chelpus/root/utils/custompatch:dir	Ljava/lang/String;
    //   9560: getstatic 162	com/chelpus/root/utils/custompatch:classesFiles	Ljava/util/ArrayList;
    //   9563: getstatic 128	com/chelpus/root/utils/custompatch:dirapp	Ljava/lang/String;
    //   9566: getstatic 134	com/chelpus/root/utils/custompatch:uid	Ljava/lang/String;
    //   9569: getstatic 128	com/chelpus/root/utils/custompatch:dirapp	Ljava/lang/String;
    //   9572: getstatic 134	com/chelpus/root/utils/custompatch:uid	Ljava/lang/String;
    //   9575: invokestatic 819	com/chelpus/Utils:getOdexForCreate	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   9578: invokestatic 823	com/chelpus/Utils:create_ODEX_root	(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    //   9581: istore_1
    //   9582: iload_1
    //   9583: ifeq +26 -> 9609
    //   9586: new 174	java/lang/StringBuilder
    //   9589: dup
    //   9590: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   9593: ldc_w 825
    //   9596: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9599: iload_1
    //   9600: invokevirtual 828	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   9603: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   9606: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   9609: iload_1
    //   9610: ifne +32 -> 9642
    //   9613: new 174	java/lang/StringBuilder
    //   9616: dup
    //   9617: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   9620: ldc_w 830
    //   9623: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9626: aload_0
    //   9627: iconst_2
    //   9628: aaload
    //   9629: iconst_1
    //   9630: invokestatic 345	com/chelpus/Utils:getPlaceForOdex	(Ljava/lang/String;Z)Ljava/lang/String;
    //   9633: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9636: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   9639: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   9642: aload 36
    //   9644: invokevirtual 831	java/io/InputStreamReader:close	()V
    //   9647: aload 35
    //   9649: invokevirtual 575	java/io/FileInputStream:close	()V
    //   9652: goto -9432 -> 220
    //   9655: ldc_w 833
    //   9658: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   9661: goto -145 -> 9516
    //   9664: iload 8
    //   9666: istore 10
    //   9668: iload_1
    //   9669: istore 14
    //   9671: aload 19
    //   9673: astore 31
    //   9675: iload 5
    //   9677: istore 9
    //   9679: iload 15
    //   9681: istore 18
    //   9683: iload 16
    //   9685: istore 17
    //   9687: goto -8411 -> 1276
    //   9690: aload 34
    //   9692: iload 11
    //   9694: ldc_w 545
    //   9697: aastore
    //   9698: aload 25
    //   9700: iload 11
    //   9702: iconst_1
    //   9703: iastore
    //   9704: goto -6242 -> 3462
    //   9707: iload 15
    //   9709: ifne -5985 -> 3724
    //   9712: iconst_0
    //   9713: istore 16
    //   9715: goto -5991 -> 3724
    //   9718: iload 15
    //   9720: ifne -5770 -> 3950
    //   9723: iconst_0
    //   9724: istore 16
    //   9726: goto -5776 -> 3950
    //   9729: iload 17
    //   9731: istore 15
    //   9733: iload 17
    //   9735: ifne -5623 -> 4112
    //   9738: iconst_0
    //   9739: istore 16
    //   9741: iload 17
    //   9743: istore 15
    //   9745: goto -5633 -> 4112
    //   9748: iload 15
    //   9750: istore 18
    //   9752: iload 16
    //   9754: istore 17
    //   9756: iload 15
    //   9758: ifne -4052 -> 5706
    //   9761: iconst_0
    //   9762: istore 17
    //   9764: iload 15
    //   9766: istore 18
    //   9768: goto -4062 -> 5706
    //   9771: aload 19
    //   9773: iload 10
    //   9775: ldc_w 545
    //   9778: aastore
    //   9779: aload 26
    //   9781: iload 10
    //   9783: iconst_1
    //   9784: iastore
    //   9785: goto -2311 -> 7474
    //   9788: aload 19
    //   9790: iload 10
    //   9792: ldc_w 545
    //   9795: aastore
    //   9796: aload 23
    //   9798: iload 10
    //   9800: iconst_0
    //   9801: iastore
    //   9802: goto -1892 -> 7910
    //   9805: aload 19
    //   9807: iload 10
    //   9809: ldc_w 545
    //   9812: aastore
    //   9813: aload 23
    //   9815: iload 10
    //   9817: sipush 254
    //   9820: iastore
    //   9821: goto -1838 -> 7983
    //   9824: aload 19
    //   9826: iload 10
    //   9828: ldc_w 545
    //   9831: aastore
    //   9832: aload 23
    //   9834: iload 10
    //   9836: sipush 255
    //   9839: iastore
    //   9840: goto -1820 -> 8020
    //   9843: iconst_1
    //   9844: istore_2
    //   9845: goto -1508 -> 8337
    //   9848: aload 20
    //   9850: iload 10
    //   9852: ldc_w 545
    //   9855: aastore
    //   9856: aload 23
    //   9858: iload 10
    //   9860: iconst_0
    //   9861: iastore
    //   9862: goto -1182 -> 8680
    //   9865: aload 20
    //   9867: iload 10
    //   9869: ldc_w 545
    //   9872: aastore
    //   9873: aload 23
    //   9875: iload 10
    //   9877: sipush 254
    //   9880: iastore
    //   9881: goto -1128 -> 8753
    //   9884: aload 20
    //   9886: iload 10
    //   9888: ldc_w 545
    //   9891: aastore
    //   9892: aload 23
    //   9894: iload 10
    //   9896: sipush 255
    //   9899: iastore
    //   9900: goto -1110 -> 8790
    //   9903: iconst_1
    //   9904: istore_1
    //   9905: goto -870 -> 9035
    //   9908: iload 12
    //   9910: iconst_1
    //   9911: iadd
    //   9912: istore 12
    //   9914: aload 30
    //   9916: astore 34
    //   9918: iload 14
    //   9920: istore_1
    //   9921: aload 31
    //   9923: astore 19
    //   9925: aload 22
    //   9927: astore 20
    //   9929: aload 27
    //   9931: astore 28
    //   9933: aload 33
    //   9935: astore 27
    //   9937: aload 32
    //   9939: astore 33
    //   9941: iload 17
    //   9943: istore 16
    //   9945: goto -9295 -> 650
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	9948	0	paramArrayOfString	String[]
    //   130	9791	1	i	int
    //   128	9717	2	j	int
    //   595	7575	3	k	int
    //   592	5536	4	m	int
    //   600	9076	5	n	int
    //   597	6245	6	i1	int
    //   589	6255	7	i2	int
    //   603	9062	8	i3	int
    //   1113	8565	9	i4	int
    //   1102	8793	10	i5	int
    //   977	8724	11	i6	int
    //   648	9265	12	i7	int
    //   312	9125	13	i8	int
    //   1105	8814	14	i9	int
    //   583	9182	15	bool1	boolean
    //   586	9358	16	bool2	boolean
    //   1121	8821	17	bool3	boolean
    //   1117	8650	18	bool4	boolean
    //   123	14	19	arrayOfFile	File[]
    //   205	3	19	localException1	Exception
    //   238	1	19	localUnsatisfiedLinkError	UnsatisfiedLinkError
    //   253	3	19	localException2	Exception
    //   263	3	19	localException3	Exception
    //   299	6253	19	localObject1	Object
    //   6589	1	19	localJSONException1	org.json.JSONException
    //   6608	32	19	localObject2	Object
    //   6666	3	19	localJSONException2	org.json.JSONException
    //   6682	1	19	localJSONException3	org.json.JSONException
    //   6692	23	19	localObject3	Object
    //   6721	1	19	localJSONException4	org.json.JSONException
    //   6740	1	19	localJSONException5	org.json.JSONException
    //   6750	786	19	localObject4	Object
    //   7560	1	19	localJSONException6	org.json.JSONException
    //   7570	1	19	localObject5	Object
    //   7607	16	19	localException4	Exception
    //   7659	538	19	localObject6	Object
    //   8209	21	19	localException5	Exception
    //   8247	1	19	localJSONException7	org.json.JSONException
    //   8257	1	19	localObject7	Object
    //   8271	16	19	localException6	Exception
    //   8358	993	19	localObject8	Object
    //   9441	1	19	localJSONException8	org.json.JSONException
    //   9451	473	19	localObject9	Object
    //   140	308	20	localObject10	Object
    //   511	1	20	localIOException	java.io.IOException
    //   572	5870	20	localObject11	Object
    //   6701	3	20	localJSONException9	org.json.JSONException
    //   6716	14	20	localObject12	Object
    //   6851	1	20	localJSONException10	org.json.JSONException
    //   6867	1	20	localJSONException11	org.json.JSONException
    //   6887	16	20	localException7	Exception
    //   6933	380	20	localObject13	Object
    //   7587	1	20	localJSONException12	org.json.JSONException
    //   7756	1158	20	localObject14	Object
    //   8967	1	20	localJSONException13	org.json.JSONException
    //   8987	16	20	localException8	Exception
    //   9056	872	20	localObject15	Object
    //   382	6256	21	localObject16	Object
    //   6644	1	21	localJSONException14	org.json.JSONException
    //   6655	94	21	localJSONException15	org.json.JSONException
    //   6915	1	21	localObject17	Object
    //   465	6135	22	localObject18	Object
    //   6604	87	22	localException9	Exception
    //   6731	3195	22	localObject19	Object
    //   611	8742	23	localObject20	Object
    //   9456	437	23	localException10	Exception
    //   615	6954	24	localObject21	Object
    //   565	9134	25	localObject22	Object
    //   568	9212	26	localObject23	Object
    //   580	9356	27	localObject24	Object
    //   303	9629	28	localObject25	Object
    //   664	8757	29	localObject26	Object
    //   728	9187	30	localObject27	Object
    //   1109	8813	31	localObject28	Object
    //   2071	7867	32	localObject29	Object
    //   576	9364	33	localObject30	Object
    //   307	9610	34	localObject31	Object
    //   529	9119	35	localFileInputStream	FileInputStream
    //   543	9100	36	localInputStreamReader	java.io.InputStreamReader
    //   554	8913	37	localBufferedReader	java.io.BufferedReader
    //   562	8846	38	arrayOfString	String[]
    // Exception table:
    //   from	to	target	type
    //   48	64	205	java/lang/Exception
    //   0	48	215	java/lang/Exception
    //   64	72	215	java/lang/Exception
    //   207	212	215	java/lang/Exception
    //   240	250	215	java/lang/Exception
    //   255	260	215	java/lang/Exception
    //   265	270	215	java/lang/Exception
    //   270	297	215	java/lang/Exception
    //   316	333	215	java/lang/Exception
    //   333	350	215	java/lang/Exception
    //   350	423	215	java/lang/Exception
    //   423	427	215	java/lang/Exception
    //   427	460	215	java/lang/Exception
    //   460	467	215	java/lang/Exception
    //   472	490	215	java/lang/Exception
    //   490	508	215	java/lang/Exception
    //   513	519	215	java/lang/Exception
    //   3604	3614	215	java/lang/Exception
    //   3774	3780	215	java/lang/Exception
    //   4000	4026	215	java/lang/Exception
    //   64	72	238	java/lang/UnsatisfiedLinkError
    //   79	96	253	java/lang/Exception
    //   103	110	253	java/lang/Exception
    //   110	129	263	java/lang/Exception
    //   142	198	263	java/lang/Exception
    //   427	460	511	java/io/IOException
    //   460	467	511	java/io/IOException
    //   472	490	511	java/io/IOException
    //   490	508	511	java/io/IOException
    //   3604	3614	511	java/io/IOException
    //   519	564	3773	java/io/FileNotFoundException
    //   617	647	3773	java/io/FileNotFoundException
    //   650	657	3773	java/io/FileNotFoundException
    //   666	686	3773	java/io/FileNotFoundException
    //   693	730	3773	java/io/FileNotFoundException
    //   734	763	3773	java/io/FileNotFoundException
    //   767	790	3773	java/io/FileNotFoundException
    //   798	826	3773	java/io/FileNotFoundException
    //   830	852	3773	java/io/FileNotFoundException
    //   856	907	3773	java/io/FileNotFoundException
    //   907	941	3773	java/io/FileNotFoundException
    //   941	975	3773	java/io/FileNotFoundException
    //   984	1012	3773	java/io/FileNotFoundException
    //   1016	1030	3773	java/io/FileNotFoundException
    //   1030	1058	3773	java/io/FileNotFoundException
    //   1070	1100	3773	java/io/FileNotFoundException
    //   1123	1137	3773	java/io/FileNotFoundException
    //   1160	1174	3773	java/io/FileNotFoundException
    //   1197	1276	3773	java/io/FileNotFoundException
    //   1280	1301	3773	java/io/FileNotFoundException
    //   1304	1368	3773	java/io/FileNotFoundException
    //   1368	1390	3773	java/io/FileNotFoundException
    //   1390	1412	3773	java/io/FileNotFoundException
    //   1416	1449	3773	java/io/FileNotFoundException
    //   1458	1478	3773	java/io/FileNotFoundException
    //   1482	1493	3773	java/io/FileNotFoundException
    //   1497	1521	3773	java/io/FileNotFoundException
    //   1525	1531	3773	java/io/FileNotFoundException
    //   1535	1576	3773	java/io/FileNotFoundException
    //   1580	1588	3773	java/io/FileNotFoundException
    //   1592	1596	3773	java/io/FileNotFoundException
    //   1600	1611	3773	java/io/FileNotFoundException
    //   1615	1621	3773	java/io/FileNotFoundException
    //   1625	1634	3773	java/io/FileNotFoundException
    //   1638	1643	3773	java/io/FileNotFoundException
    //   1647	1671	3773	java/io/FileNotFoundException
    //   1675	1681	3773	java/io/FileNotFoundException
    //   1685	1726	3773	java/io/FileNotFoundException
    //   1730	1734	3773	java/io/FileNotFoundException
    //   1738	1744	3773	java/io/FileNotFoundException
    //   1748	1756	3773	java/io/FileNotFoundException
    //   1760	1766	3773	java/io/FileNotFoundException
    //   1770	1797	3773	java/io/FileNotFoundException
    //   1801	1807	3773	java/io/FileNotFoundException
    //   1811	1830	3773	java/io/FileNotFoundException
    //   1843	1863	3773	java/io/FileNotFoundException
    //   1867	1891	3773	java/io/FileNotFoundException
    //   1895	1900	3773	java/io/FileNotFoundException
    //   1913	1923	3773	java/io/FileNotFoundException
    //   1927	1934	3773	java/io/FileNotFoundException
    //   1938	1943	3773	java/io/FileNotFoundException
    //   1960	1972	3773	java/io/FileNotFoundException
    //   1972	1992	3773	java/io/FileNotFoundException
    //   1996	2002	3773	java/io/FileNotFoundException
    //   2006	2036	3773	java/io/FileNotFoundException
    //   2040	2046	3773	java/io/FileNotFoundException
    //   2050	2058	3773	java/io/FileNotFoundException
    //   2078	2091	3773	java/io/FileNotFoundException
    //   2100	2120	3773	java/io/FileNotFoundException
    //   2124	2132	3773	java/io/FileNotFoundException
    //   2144	2158	3773	java/io/FileNotFoundException
    //   2166	2180	3773	java/io/FileNotFoundException
    //   2188	2215	3773	java/io/FileNotFoundException
    //   2228	2248	3773	java/io/FileNotFoundException
    //   2265	2278	3773	java/io/FileNotFoundException
    //   2287	2307	3773	java/io/FileNotFoundException
    //   2315	2329	3773	java/io/FileNotFoundException
    //   2337	2351	3773	java/io/FileNotFoundException
    //   2359	2386	3773	java/io/FileNotFoundException
    //   2399	2419	3773	java/io/FileNotFoundException
    //   2435	2447	3773	java/io/FileNotFoundException
    //   2447	2467	3773	java/io/FileNotFoundException
    //   2467	2487	3773	java/io/FileNotFoundException
    //   2496	2517	3773	java/io/FileNotFoundException
    //   2526	2534	3773	java/io/FileNotFoundException
    //   2552	2607	3773	java/io/FileNotFoundException
    //   2607	2612	3773	java/io/FileNotFoundException
    //   2620	2628	3773	java/io/FileNotFoundException
    //   2646	2684	3773	java/io/FileNotFoundException
    //   2693	2701	3773	java/io/FileNotFoundException
    //   2719	2757	3773	java/io/FileNotFoundException
    //   2765	2773	3773	java/io/FileNotFoundException
    //   2791	2829	3773	java/io/FileNotFoundException
    //   2838	2846	3773	java/io/FileNotFoundException
    //   2849	2870	3773	java/io/FileNotFoundException
    //   2879	2887	3773	java/io/FileNotFoundException
    //   2890	2912	3773	java/io/FileNotFoundException
    //   2921	2929	3773	java/io/FileNotFoundException
    //   2932	2954	3773	java/io/FileNotFoundException
    //   2963	2971	3773	java/io/FileNotFoundException
    //   2982	3008	3773	java/io/FileNotFoundException
    //   3020	3024	3773	java/io/FileNotFoundException
    //   3028	3054	3773	java/io/FileNotFoundException
    //   3063	3067	3773	java/io/FileNotFoundException
    //   3070	3111	3773	java/io/FileNotFoundException
    //   3116	3137	3773	java/io/FileNotFoundException
    //   3158	3172	3773	java/io/FileNotFoundException
    //   3193	3207	3773	java/io/FileNotFoundException
    //   3228	3242	3773	java/io/FileNotFoundException
    //   3249	3257	3773	java/io/FileNotFoundException
    //   3260	3280	3773	java/io/FileNotFoundException
    //   3284	3291	3773	java/io/FileNotFoundException
    //   3295	3308	3773	java/io/FileNotFoundException
    //   3308	3346	3773	java/io/FileNotFoundException
    //   3373	3381	3773	java/io/FileNotFoundException
    //   3385	3399	3773	java/io/FileNotFoundException
    //   3403	3417	3773	java/io/FileNotFoundException
    //   3429	3443	3773	java/io/FileNotFoundException
    //   3445	3459	3773	java/io/FileNotFoundException
    //   3464	3478	3773	java/io/FileNotFoundException
    //   3480	3494	3773	java/io/FileNotFoundException
    //   3496	3510	3773	java/io/FileNotFoundException
    //   3512	3526	3773	java/io/FileNotFoundException
    //   3528	3565	3773	java/io/FileNotFoundException
    //   3575	3593	3773	java/io/FileNotFoundException
    //   3617	3640	3773	java/io/FileNotFoundException
    //   3663	3707	3773	java/io/FileNotFoundException
    //   3707	3721	3773	java/io/FileNotFoundException
    //   3724	3747	3773	java/io/FileNotFoundException
    //   3783	3866	3773	java/io/FileNotFoundException
    //   3889	3933	3773	java/io/FileNotFoundException
    //   3933	3947	3773	java/io/FileNotFoundException
    //   3950	3973	3773	java/io/FileNotFoundException
    //   4052	4058	3773	java/io/FileNotFoundException
    //   4081	4104	3773	java/io/FileNotFoundException
    //   4104	4112	3773	java/io/FileNotFoundException
    //   4112	4210	3773	java/io/FileNotFoundException
    //   4214	4228	3773	java/io/FileNotFoundException
    //   4231	4262	3773	java/io/FileNotFoundException
    //   4288	4292	3773	java/io/FileNotFoundException
    //   4296	4340	3773	java/io/FileNotFoundException
    //   4340	4348	3773	java/io/FileNotFoundException
    //   4368	4403	3773	java/io/FileNotFoundException
    //   4403	4415	3773	java/io/FileNotFoundException
    //   4415	4424	3773	java/io/FileNotFoundException
    //   4424	4433	3773	java/io/FileNotFoundException
    //   4435	4443	3773	java/io/FileNotFoundException
    //   4443	4466	3773	java/io/FileNotFoundException
    //   4488	4492	3773	java/io/FileNotFoundException
    //   4500	4509	3773	java/io/FileNotFoundException
    //   4517	4523	3773	java/io/FileNotFoundException
    //   4531	4548	3773	java/io/FileNotFoundException
    //   4548	4634	3773	java/io/FileNotFoundException
    //   4634	4642	3773	java/io/FileNotFoundException
    //   4661	4675	3773	java/io/FileNotFoundException
    //   4683	4706	3773	java/io/FileNotFoundException
    //   4724	4728	3773	java/io/FileNotFoundException
    //   4736	4745	3773	java/io/FileNotFoundException
    //   4753	4759	3773	java/io/FileNotFoundException
    //   4767	4784	3773	java/io/FileNotFoundException
    //   4784	4870	3773	java/io/FileNotFoundException
    //   4870	4878	3773	java/io/FileNotFoundException
    //   4897	4911	3773	java/io/FileNotFoundException
    //   4919	4942	3773	java/io/FileNotFoundException
    //   4960	4964	3773	java/io/FileNotFoundException
    //   4972	4981	3773	java/io/FileNotFoundException
    //   4989	4995	3773	java/io/FileNotFoundException
    //   5003	5020	3773	java/io/FileNotFoundException
    //   5020	5106	3773	java/io/FileNotFoundException
    //   5106	5114	3773	java/io/FileNotFoundException
    //   5133	5147	3773	java/io/FileNotFoundException
    //   5155	5178	3773	java/io/FileNotFoundException
    //   5196	5200	3773	java/io/FileNotFoundException
    //   5208	5217	3773	java/io/FileNotFoundException
    //   5225	5231	3773	java/io/FileNotFoundException
    //   5239	5256	3773	java/io/FileNotFoundException
    //   5256	5342	3773	java/io/FileNotFoundException
    //   5342	5350	3773	java/io/FileNotFoundException
    //   5369	5383	3773	java/io/FileNotFoundException
    //   5391	5414	3773	java/io/FileNotFoundException
    //   5432	5436	3773	java/io/FileNotFoundException
    //   5444	5453	3773	java/io/FileNotFoundException
    //   5461	5467	3773	java/io/FileNotFoundException
    //   5475	5492	3773	java/io/FileNotFoundException
    //   5492	5578	3773	java/io/FileNotFoundException
    //   5578	5586	3773	java/io/FileNotFoundException
    //   5605	5619	3773	java/io/FileNotFoundException
    //   5627	5650	3773	java/io/FileNotFoundException
    //   5668	5672	3773	java/io/FileNotFoundException
    //   5680	5689	3773	java/io/FileNotFoundException
    //   5689	5703	3773	java/io/FileNotFoundException
    //   5706	5729	3773	java/io/FileNotFoundException
    //   5747	5786	3773	java/io/FileNotFoundException
    //   5786	5818	3773	java/io/FileNotFoundException
    //   5818	5842	3773	java/io/FileNotFoundException
    //   5842	5850	3773	java/io/FileNotFoundException
    //   5854	5871	3773	java/io/FileNotFoundException
    //   5891	5914	3773	java/io/FileNotFoundException
    //   5936	5963	3773	java/io/FileNotFoundException
    //   6015	6116	3773	java/io/FileNotFoundException
    //   6145	6325	3773	java/io/FileNotFoundException
    //   6347	6465	3773	java/io/FileNotFoundException
    //   6465	6489	3773	java/io/FileNotFoundException
    //   6511	6517	3773	java/io/FileNotFoundException
    //   6520	6544	3773	java/io/FileNotFoundException
    //   6582	6586	3773	java/io/FileNotFoundException
    //   6591	6597	3773	java/io/FileNotFoundException
    //   6610	6637	3773	java/io/FileNotFoundException
    //   6646	6652	3773	java/io/FileNotFoundException
    //   6657	6663	3773	java/io/FileNotFoundException
    //   6668	6679	3773	java/io/FileNotFoundException
    //   6684	6690	3773	java/io/FileNotFoundException
    //   6703	6714	3773	java/io/FileNotFoundException
    //   6723	6729	3773	java/io/FileNotFoundException
    //   6742	6748	3773	java/io/FileNotFoundException
    //   6755	6761	3773	java/io/FileNotFoundException
    //   6779	6785	3773	java/io/FileNotFoundException
    //   6803	6809	3773	java/io/FileNotFoundException
    //   6827	6833	3773	java/io/FileNotFoundException
    //   6853	6864	3773	java/io/FileNotFoundException
    //   6869	6875	3773	java/io/FileNotFoundException
    //   6889	6913	3773	java/io/FileNotFoundException
    //   6943	6957	3773	java/io/FileNotFoundException
    //   6965	6992	3773	java/io/FileNotFoundException
    //   7005	7025	3773	java/io/FileNotFoundException
    //   7025	7176	3773	java/io/FileNotFoundException
    //   7179	7183	3773	java/io/FileNotFoundException
    //   7204	7218	3773	java/io/FileNotFoundException
    //   7239	7266	3773	java/io/FileNotFoundException
    //   7292	7312	3773	java/io/FileNotFoundException
    //   7316	7323	3773	java/io/FileNotFoundException
    //   7327	7340	3773	java/io/FileNotFoundException
    //   7340	7378	3773	java/io/FileNotFoundException
    //   7385	7393	3773	java/io/FileNotFoundException
    //   7397	7411	3773	java/io/FileNotFoundException
    //   7415	7429	3773	java/io/FileNotFoundException
    //   7441	7455	3773	java/io/FileNotFoundException
    //   7457	7471	3773	java/io/FileNotFoundException
    //   7476	7493	3773	java/io/FileNotFoundException
    //   7495	7521	3773	java/io/FileNotFoundException
    //   7531	7549	3773	java/io/FileNotFoundException
    //   7562	7568	3773	java/io/FileNotFoundException
    //   7575	7581	3773	java/io/FileNotFoundException
    //   7589	7595	3773	java/io/FileNotFoundException
    //   7609	7633	3773	java/io/FileNotFoundException
    //   7642	7648	3773	java/io/FileNotFoundException
    //   7661	7675	3773	java/io/FileNotFoundException
    //   7685	7712	3773	java/io/FileNotFoundException
    //   7727	7747	3773	java/io/FileNotFoundException
    //   7747	7754	3773	java/io/FileNotFoundException
    //   7758	7771	3773	java/io/FileNotFoundException
    //   7771	7809	3773	java/io/FileNotFoundException
    //   7817	7825	3773	java/io/FileNotFoundException
    //   7830	7844	3773	java/io/FileNotFoundException
    //   7849	7863	3773	java/io/FileNotFoundException
    //   7876	7890	3773	java/io/FileNotFoundException
    //   7893	7907	3773	java/io/FileNotFoundException
    //   7913	7930	3773	java/io/FileNotFoundException
    //   7949	7963	3773	java/io/FileNotFoundException
    //   7966	7980	3773	java/io/FileNotFoundException
    //   7986	8000	3773	java/io/FileNotFoundException
    //   8003	8017	3773	java/io/FileNotFoundException
    //   8023	8037	3773	java/io/FileNotFoundException
    //   8040	8054	3773	java/io/FileNotFoundException
    //   8057	8071	3773	java/io/FileNotFoundException
    //   8074	8088	3773	java/io/FileNotFoundException
    //   8091	8128	3773	java/io/FileNotFoundException
    //   8139	8157	3773	java/io/FileNotFoundException
    //   8170	8202	3773	java/io/FileNotFoundException
    //   8211	8240	3773	java/io/FileNotFoundException
    //   8249	8255	3773	java/io/FileNotFoundException
    //   8273	8297	3773	java/io/FileNotFoundException
    //   8300	8325	3773	java/io/FileNotFoundException
    //   8327	8334	3773	java/io/FileNotFoundException
    //   8344	8350	3773	java/io/FileNotFoundException
    //   8364	8376	3773	java/io/FileNotFoundException
    //   8376	8402	3773	java/io/FileNotFoundException
    //   8402	8407	3773	java/io/FileNotFoundException
    //   8427	8441	3773	java/io/FileNotFoundException
    //   8451	8478	3773	java/io/FileNotFoundException
    //   8493	8513	3773	java/io/FileNotFoundException
    //   8517	8524	3773	java/io/FileNotFoundException
    //   8528	8541	3773	java/io/FileNotFoundException
    //   8541	8579	3773	java/io/FileNotFoundException
    //   8587	8595	3773	java/io/FileNotFoundException
    //   8600	8614	3773	java/io/FileNotFoundException
    //   8619	8633	3773	java/io/FileNotFoundException
    //   8646	8660	3773	java/io/FileNotFoundException
    //   8663	8677	3773	java/io/FileNotFoundException
    //   8683	8700	3773	java/io/FileNotFoundException
    //   8719	8733	3773	java/io/FileNotFoundException
    //   8736	8750	3773	java/io/FileNotFoundException
    //   8756	8770	3773	java/io/FileNotFoundException
    //   8773	8787	3773	java/io/FileNotFoundException
    //   8793	8807	3773	java/io/FileNotFoundException
    //   8810	8824	3773	java/io/FileNotFoundException
    //   8827	8841	3773	java/io/FileNotFoundException
    //   8844	8858	3773	java/io/FileNotFoundException
    //   8861	8898	3773	java/io/FileNotFoundException
    //   8909	8927	3773	java/io/FileNotFoundException
    //   8938	8964	3773	java/io/FileNotFoundException
    //   8969	8975	3773	java/io/FileNotFoundException
    //   8989	9013	3773	java/io/FileNotFoundException
    //   9016	9023	3773	java/io/FileNotFoundException
    //   9025	9032	3773	java/io/FileNotFoundException
    //   9042	9048	3773	java/io/FileNotFoundException
    //   9062	9093	3773	java/io/FileNotFoundException
    //   9111	9125	3773	java/io/FileNotFoundException
    //   9133	9160	3773	java/io/FileNotFoundException
    //   9173	9193	3773	java/io/FileNotFoundException
    //   9193	9257	3773	java/io/FileNotFoundException
    //   9257	9268	3773	java/io/FileNotFoundException
    //   9268	9283	3773	java/io/FileNotFoundException
    //   9283	9294	3773	java/io/FileNotFoundException
    //   9301	9307	3773	java/io/FileNotFoundException
    //   9319	9350	3773	java/io/FileNotFoundException
    //   9358	9381	3773	java/io/FileNotFoundException
    //   9390	9420	3773	java/io/FileNotFoundException
    //   9420	9435	3773	java/io/FileNotFoundException
    //   9443	9449	3773	java/io/FileNotFoundException
    //   9458	9463	3773	java/io/FileNotFoundException
    //   9466	9471	3773	java/io/FileNotFoundException
    //   9476	9499	3773	java/io/FileNotFoundException
    //   9504	9516	3773	java/io/FileNotFoundException
    //   9520	9544	3773	java/io/FileNotFoundException
    //   9544	9582	3773	java/io/FileNotFoundException
    //   9586	9609	3773	java/io/FileNotFoundException
    //   9613	9642	3773	java/io/FileNotFoundException
    //   9642	9652	3773	java/io/FileNotFoundException
    //   9655	9661	3773	java/io/FileNotFoundException
    //   519	564	3999	java/lang/Exception
    //   617	647	3999	java/lang/Exception
    //   650	657	3999	java/lang/Exception
    //   666	686	3999	java/lang/Exception
    //   693	730	3999	java/lang/Exception
    //   734	763	3999	java/lang/Exception
    //   767	790	3999	java/lang/Exception
    //   798	826	3999	java/lang/Exception
    //   830	852	3999	java/lang/Exception
    //   856	907	3999	java/lang/Exception
    //   907	941	3999	java/lang/Exception
    //   941	975	3999	java/lang/Exception
    //   984	1012	3999	java/lang/Exception
    //   1016	1030	3999	java/lang/Exception
    //   1030	1058	3999	java/lang/Exception
    //   1070	1100	3999	java/lang/Exception
    //   1123	1137	3999	java/lang/Exception
    //   1160	1174	3999	java/lang/Exception
    //   1197	1276	3999	java/lang/Exception
    //   1280	1301	3999	java/lang/Exception
    //   1304	1368	3999	java/lang/Exception
    //   1368	1390	3999	java/lang/Exception
    //   1390	1412	3999	java/lang/Exception
    //   1416	1449	3999	java/lang/Exception
    //   1458	1478	3999	java/lang/Exception
    //   1482	1493	3999	java/lang/Exception
    //   1497	1521	3999	java/lang/Exception
    //   1525	1531	3999	java/lang/Exception
    //   1535	1576	3999	java/lang/Exception
    //   1580	1588	3999	java/lang/Exception
    //   1592	1596	3999	java/lang/Exception
    //   1600	1611	3999	java/lang/Exception
    //   1615	1621	3999	java/lang/Exception
    //   1625	1634	3999	java/lang/Exception
    //   1638	1643	3999	java/lang/Exception
    //   1647	1671	3999	java/lang/Exception
    //   1675	1681	3999	java/lang/Exception
    //   1685	1726	3999	java/lang/Exception
    //   1730	1734	3999	java/lang/Exception
    //   1738	1744	3999	java/lang/Exception
    //   1748	1756	3999	java/lang/Exception
    //   1760	1766	3999	java/lang/Exception
    //   1770	1797	3999	java/lang/Exception
    //   1801	1807	3999	java/lang/Exception
    //   1811	1830	3999	java/lang/Exception
    //   1843	1863	3999	java/lang/Exception
    //   1867	1891	3999	java/lang/Exception
    //   1895	1900	3999	java/lang/Exception
    //   1960	1972	3999	java/lang/Exception
    //   1972	1992	3999	java/lang/Exception
    //   1996	2002	3999	java/lang/Exception
    //   2006	2036	3999	java/lang/Exception
    //   2040	2046	3999	java/lang/Exception
    //   2050	2058	3999	java/lang/Exception
    //   2078	2091	3999	java/lang/Exception
    //   2100	2120	3999	java/lang/Exception
    //   2124	2132	3999	java/lang/Exception
    //   2144	2158	3999	java/lang/Exception
    //   2166	2180	3999	java/lang/Exception
    //   2188	2215	3999	java/lang/Exception
    //   2228	2248	3999	java/lang/Exception
    //   2265	2278	3999	java/lang/Exception
    //   2287	2307	3999	java/lang/Exception
    //   2315	2329	3999	java/lang/Exception
    //   2337	2351	3999	java/lang/Exception
    //   2359	2386	3999	java/lang/Exception
    //   2399	2419	3999	java/lang/Exception
    //   2435	2447	3999	java/lang/Exception
    //   2447	2467	3999	java/lang/Exception
    //   2467	2487	3999	java/lang/Exception
    //   2496	2517	3999	java/lang/Exception
    //   2526	2534	3999	java/lang/Exception
    //   2552	2607	3999	java/lang/Exception
    //   2607	2612	3999	java/lang/Exception
    //   2620	2628	3999	java/lang/Exception
    //   2646	2684	3999	java/lang/Exception
    //   2693	2701	3999	java/lang/Exception
    //   2719	2757	3999	java/lang/Exception
    //   2765	2773	3999	java/lang/Exception
    //   2791	2829	3999	java/lang/Exception
    //   2838	2846	3999	java/lang/Exception
    //   2849	2870	3999	java/lang/Exception
    //   2879	2887	3999	java/lang/Exception
    //   2890	2912	3999	java/lang/Exception
    //   2921	2929	3999	java/lang/Exception
    //   2932	2954	3999	java/lang/Exception
    //   2963	2971	3999	java/lang/Exception
    //   2982	3008	3999	java/lang/Exception
    //   3020	3024	3999	java/lang/Exception
    //   3028	3054	3999	java/lang/Exception
    //   3063	3067	3999	java/lang/Exception
    //   3070	3111	3999	java/lang/Exception
    //   3116	3137	3999	java/lang/Exception
    //   3158	3172	3999	java/lang/Exception
    //   3193	3207	3999	java/lang/Exception
    //   3228	3242	3999	java/lang/Exception
    //   3249	3257	3999	java/lang/Exception
    //   3260	3280	3999	java/lang/Exception
    //   3284	3291	3999	java/lang/Exception
    //   3295	3308	3999	java/lang/Exception
    //   3308	3346	3999	java/lang/Exception
    //   3617	3640	3999	java/lang/Exception
    //   3663	3707	3999	java/lang/Exception
    //   3707	3721	3999	java/lang/Exception
    //   3724	3747	3999	java/lang/Exception
    //   3783	3866	3999	java/lang/Exception
    //   3889	3933	3999	java/lang/Exception
    //   3933	3947	3999	java/lang/Exception
    //   3950	3973	3999	java/lang/Exception
    //   4052	4058	3999	java/lang/Exception
    //   4081	4104	3999	java/lang/Exception
    //   4104	4112	3999	java/lang/Exception
    //   4112	4210	3999	java/lang/Exception
    //   4214	4228	3999	java/lang/Exception
    //   4231	4262	3999	java/lang/Exception
    //   4288	4292	3999	java/lang/Exception
    //   4296	4340	3999	java/lang/Exception
    //   4340	4348	3999	java/lang/Exception
    //   4368	4403	3999	java/lang/Exception
    //   4403	4415	3999	java/lang/Exception
    //   4415	4424	3999	java/lang/Exception
    //   4424	4433	3999	java/lang/Exception
    //   4435	4443	3999	java/lang/Exception
    //   4443	4466	3999	java/lang/Exception
    //   4488	4492	3999	java/lang/Exception
    //   4500	4509	3999	java/lang/Exception
    //   4517	4523	3999	java/lang/Exception
    //   4531	4548	3999	java/lang/Exception
    //   4548	4634	3999	java/lang/Exception
    //   4634	4642	3999	java/lang/Exception
    //   4661	4675	3999	java/lang/Exception
    //   4683	4706	3999	java/lang/Exception
    //   4724	4728	3999	java/lang/Exception
    //   4736	4745	3999	java/lang/Exception
    //   4753	4759	3999	java/lang/Exception
    //   4767	4784	3999	java/lang/Exception
    //   4784	4870	3999	java/lang/Exception
    //   4870	4878	3999	java/lang/Exception
    //   4897	4911	3999	java/lang/Exception
    //   4919	4942	3999	java/lang/Exception
    //   4960	4964	3999	java/lang/Exception
    //   4972	4981	3999	java/lang/Exception
    //   4989	4995	3999	java/lang/Exception
    //   5003	5020	3999	java/lang/Exception
    //   5020	5106	3999	java/lang/Exception
    //   5106	5114	3999	java/lang/Exception
    //   5133	5147	3999	java/lang/Exception
    //   5155	5178	3999	java/lang/Exception
    //   5196	5200	3999	java/lang/Exception
    //   5208	5217	3999	java/lang/Exception
    //   5225	5231	3999	java/lang/Exception
    //   5239	5256	3999	java/lang/Exception
    //   5256	5342	3999	java/lang/Exception
    //   5342	5350	3999	java/lang/Exception
    //   5369	5383	3999	java/lang/Exception
    //   5391	5414	3999	java/lang/Exception
    //   5432	5436	3999	java/lang/Exception
    //   5444	5453	3999	java/lang/Exception
    //   5461	5467	3999	java/lang/Exception
    //   5475	5492	3999	java/lang/Exception
    //   5492	5578	3999	java/lang/Exception
    //   5578	5586	3999	java/lang/Exception
    //   5605	5619	3999	java/lang/Exception
    //   5627	5650	3999	java/lang/Exception
    //   5668	5672	3999	java/lang/Exception
    //   5680	5689	3999	java/lang/Exception
    //   5689	5703	3999	java/lang/Exception
    //   5706	5729	3999	java/lang/Exception
    //   5747	5786	3999	java/lang/Exception
    //   5786	5818	3999	java/lang/Exception
    //   5818	5842	3999	java/lang/Exception
    //   5842	5850	3999	java/lang/Exception
    //   5854	5871	3999	java/lang/Exception
    //   5891	5914	3999	java/lang/Exception
    //   5936	5963	3999	java/lang/Exception
    //   6015	6116	3999	java/lang/Exception
    //   6145	6325	3999	java/lang/Exception
    //   6347	6465	3999	java/lang/Exception
    //   6465	6489	3999	java/lang/Exception
    //   6511	6517	3999	java/lang/Exception
    //   6520	6544	3999	java/lang/Exception
    //   6582	6586	3999	java/lang/Exception
    //   6591	6597	3999	java/lang/Exception
    //   6610	6637	3999	java/lang/Exception
    //   6646	6652	3999	java/lang/Exception
    //   6657	6663	3999	java/lang/Exception
    //   6668	6679	3999	java/lang/Exception
    //   6684	6690	3999	java/lang/Exception
    //   6703	6714	3999	java/lang/Exception
    //   6723	6729	3999	java/lang/Exception
    //   6742	6748	3999	java/lang/Exception
    //   6755	6761	3999	java/lang/Exception
    //   6779	6785	3999	java/lang/Exception
    //   6803	6809	3999	java/lang/Exception
    //   6827	6833	3999	java/lang/Exception
    //   6853	6864	3999	java/lang/Exception
    //   6869	6875	3999	java/lang/Exception
    //   6889	6913	3999	java/lang/Exception
    //   6943	6957	3999	java/lang/Exception
    //   6965	6992	3999	java/lang/Exception
    //   7005	7025	3999	java/lang/Exception
    //   7025	7176	3999	java/lang/Exception
    //   7179	7183	3999	java/lang/Exception
    //   7204	7218	3999	java/lang/Exception
    //   7239	7266	3999	java/lang/Exception
    //   7292	7312	3999	java/lang/Exception
    //   7316	7323	3999	java/lang/Exception
    //   7327	7340	3999	java/lang/Exception
    //   7340	7378	3999	java/lang/Exception
    //   7562	7568	3999	java/lang/Exception
    //   7575	7581	3999	java/lang/Exception
    //   7589	7595	3999	java/lang/Exception
    //   7609	7633	3999	java/lang/Exception
    //   7642	7648	3999	java/lang/Exception
    //   7661	7675	3999	java/lang/Exception
    //   7685	7712	3999	java/lang/Exception
    //   7727	7747	3999	java/lang/Exception
    //   7747	7754	3999	java/lang/Exception
    //   7758	7771	3999	java/lang/Exception
    //   7771	7809	3999	java/lang/Exception
    //   8211	8240	3999	java/lang/Exception
    //   8249	8255	3999	java/lang/Exception
    //   8273	8297	3999	java/lang/Exception
    //   8300	8325	3999	java/lang/Exception
    //   8327	8334	3999	java/lang/Exception
    //   8344	8350	3999	java/lang/Exception
    //   8364	8376	3999	java/lang/Exception
    //   8376	8402	3999	java/lang/Exception
    //   8402	8407	3999	java/lang/Exception
    //   8427	8441	3999	java/lang/Exception
    //   8451	8478	3999	java/lang/Exception
    //   8493	8513	3999	java/lang/Exception
    //   8517	8524	3999	java/lang/Exception
    //   8528	8541	3999	java/lang/Exception
    //   8541	8579	3999	java/lang/Exception
    //   8938	8964	3999	java/lang/Exception
    //   8969	8975	3999	java/lang/Exception
    //   8989	9013	3999	java/lang/Exception
    //   9016	9023	3999	java/lang/Exception
    //   9025	9032	3999	java/lang/Exception
    //   9042	9048	3999	java/lang/Exception
    //   9062	9093	3999	java/lang/Exception
    //   9111	9125	3999	java/lang/Exception
    //   9133	9160	3999	java/lang/Exception
    //   9173	9193	3999	java/lang/Exception
    //   9193	9257	3999	java/lang/Exception
    //   9283	9294	3999	java/lang/Exception
    //   9301	9307	3999	java/lang/Exception
    //   9319	9350	3999	java/lang/Exception
    //   9358	9381	3999	java/lang/Exception
    //   9390	9420	3999	java/lang/Exception
    //   9420	9435	3999	java/lang/Exception
    //   9443	9449	3999	java/lang/Exception
    //   9458	9463	3999	java/lang/Exception
    //   9466	9471	3999	java/lang/Exception
    //   9476	9499	3999	java/lang/Exception
    //   9504	9516	3999	java/lang/Exception
    //   9520	9544	3999	java/lang/Exception
    //   9544	9582	3999	java/lang/Exception
    //   9586	9609	3999	java/lang/Exception
    //   9613	9642	3999	java/lang/Exception
    //   9642	9652	3999	java/lang/Exception
    //   9655	9661	3999	java/lang/Exception
    //   1458	1478	6589	org/json/JSONException
    //   1482	1493	6589	org/json/JSONException
    //   1497	1521	6589	org/json/JSONException
    //   1525	1531	6589	org/json/JSONException
    //   1535	1576	6589	org/json/JSONException
    //   1580	1588	6589	org/json/JSONException
    //   1592	1596	6589	org/json/JSONException
    //   1600	1611	6589	org/json/JSONException
    //   1615	1621	6589	org/json/JSONException
    //   1625	1634	6589	org/json/JSONException
    //   1638	1643	6589	org/json/JSONException
    //   1647	1671	6589	org/json/JSONException
    //   1675	1681	6589	org/json/JSONException
    //   1685	1726	6589	org/json/JSONException
    //   1730	1734	6589	org/json/JSONException
    //   1738	1744	6589	org/json/JSONException
    //   1748	1756	6589	org/json/JSONException
    //   1760	1766	6589	org/json/JSONException
    //   1770	1797	6589	org/json/JSONException
    //   1801	1807	6589	org/json/JSONException
    //   6582	6586	6589	org/json/JSONException
    //   1913	1923	6604	java/lang/Exception
    //   1927	1934	6604	java/lang/Exception
    //   1938	1943	6604	java/lang/Exception
    //   1843	1863	6644	org/json/JSONException
    //   1867	1891	6644	org/json/JSONException
    //   1895	1900	6644	org/json/JSONException
    //   1913	1923	6644	org/json/JSONException
    //   1927	1934	6644	org/json/JSONException
    //   1938	1943	6644	org/json/JSONException
    //   6610	6637	6644	org/json/JSONException
    //   1972	1992	6655	org/json/JSONException
    //   1996	2002	6655	org/json/JSONException
    //   2006	2036	6655	org/json/JSONException
    //   2040	2046	6655	org/json/JSONException
    //   2100	2120	6666	org/json/JSONException
    //   2228	2248	6682	org/json/JSONException
    //   2287	2307	6701	org/json/JSONException
    //   2399	2419	6721	org/json/JSONException
    //   2447	2467	6740	org/json/JSONException
    //   3116	3137	6851	org/json/JSONException
    //   3260	3280	6867	org/json/JSONException
    //   3373	3381	6887	java/lang/Exception
    //   3385	3399	6887	java/lang/Exception
    //   3403	3417	6887	java/lang/Exception
    //   3429	3443	6887	java/lang/Exception
    //   3445	3459	6887	java/lang/Exception
    //   3464	3478	6887	java/lang/Exception
    //   3480	3494	6887	java/lang/Exception
    //   3496	3510	6887	java/lang/Exception
    //   3512	3526	6887	java/lang/Exception
    //   3528	3565	6887	java/lang/Exception
    //   3575	3593	6887	java/lang/Exception
    //   7005	7025	7560	org/json/JSONException
    //   7292	7312	7587	org/json/JSONException
    //   7385	7393	7607	java/lang/Exception
    //   7397	7411	7607	java/lang/Exception
    //   7415	7429	7607	java/lang/Exception
    //   7441	7455	7607	java/lang/Exception
    //   7457	7471	7607	java/lang/Exception
    //   7476	7493	7607	java/lang/Exception
    //   7495	7521	7607	java/lang/Exception
    //   7531	7549	7607	java/lang/Exception
    //   8170	8202	8209	java/lang/Exception
    //   7727	7747	8247	org/json/JSONException
    //   7817	7825	8271	java/lang/Exception
    //   7830	7844	8271	java/lang/Exception
    //   7849	7863	8271	java/lang/Exception
    //   7876	7890	8271	java/lang/Exception
    //   7893	7907	8271	java/lang/Exception
    //   7913	7930	8271	java/lang/Exception
    //   7949	7963	8271	java/lang/Exception
    //   7966	7980	8271	java/lang/Exception
    //   7986	8000	8271	java/lang/Exception
    //   8003	8017	8271	java/lang/Exception
    //   8023	8037	8271	java/lang/Exception
    //   8040	8054	8271	java/lang/Exception
    //   8057	8071	8271	java/lang/Exception
    //   8074	8088	8271	java/lang/Exception
    //   8091	8128	8271	java/lang/Exception
    //   8139	8157	8271	java/lang/Exception
    //   8493	8513	8967	org/json/JSONException
    //   8587	8595	8987	java/lang/Exception
    //   8600	8614	8987	java/lang/Exception
    //   8619	8633	8987	java/lang/Exception
    //   8646	8660	8987	java/lang/Exception
    //   8663	8677	8987	java/lang/Exception
    //   8683	8700	8987	java/lang/Exception
    //   8719	8733	8987	java/lang/Exception
    //   8736	8750	8987	java/lang/Exception
    //   8756	8770	8987	java/lang/Exception
    //   8773	8787	8987	java/lang/Exception
    //   8793	8807	8987	java/lang/Exception
    //   8810	8824	8987	java/lang/Exception
    //   8827	8841	8987	java/lang/Exception
    //   8844	8858	8987	java/lang/Exception
    //   8861	8898	8987	java/lang/Exception
    //   8909	8927	8987	java/lang/Exception
    //   9173	9193	9441	org/json/JSONException
    //   9257	9268	9456	java/lang/Exception
    //   9268	9283	9456	java/lang/Exception
  }
  
  /* Error */
  public static boolean patchProcess(ArrayList<PatchesItem> paramArrayList)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore 12
    //   3: iconst_1
    //   4: istore 16
    //   6: iconst_1
    //   7: istore 17
    //   9: iconst_1
    //   10: istore 11
    //   12: aload_0
    //   13: ifnull +43 -> 56
    //   16: aload_0
    //   17: invokevirtual 205	java/util/ArrayList:size	()I
    //   20: ifle +36 -> 56
    //   23: aload_0
    //   24: invokevirtual 209	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   27: astore 18
    //   29: aload 18
    //   31: invokeinterface 214 1 0
    //   36: ifeq +20 -> 56
    //   39: aload 18
    //   41: invokeinterface 218 1 0
    //   46: checkcast 779	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
    //   49: iconst_0
    //   50: putfield 840	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:result	Z
    //   53: goto -24 -> 29
    //   56: iconst_3
    //   57: anewarray 291	java/lang/String
    //   60: dup
    //   61: iconst_0
    //   62: ldc_w 435
    //   65: aastore
    //   66: dup
    //   67: iconst_1
    //   68: ldc_w 437
    //   71: aastore
    //   72: dup
    //   73: iconst_2
    //   74: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   77: invokevirtual 451	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   80: aastore
    //   81: invokestatic 440	com/chelpus/Utils:run_all_no_root	([Ljava/lang/String;)V
    //   84: iload 12
    //   86: istore 13
    //   88: iload 16
    //   90: istore 14
    //   92: iload 17
    //   94: istore 15
    //   96: new 842	java/io/RandomAccessFile
    //   99: dup
    //   100: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   103: ldc_w 278
    //   106: invokespecial 845	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   109: invokevirtual 849	java/io/RandomAccessFile:getChannel	()Ljava/nio/channels/FileChannel;
    //   112: astore 19
    //   114: iload 12
    //   116: istore 13
    //   118: iload 16
    //   120: istore 14
    //   122: iload 17
    //   124: istore 15
    //   126: new 174	java/lang/StringBuilder
    //   129: dup
    //   130: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   133: ldc_w 851
    //   136: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   139: aload 19
    //   141: invokevirtual 855	java/nio/channels/FileChannel:size	()J
    //   144: invokevirtual 858	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   147: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   150: invokestatic 319	com/chelpus/Utils:sendFromRootCP	(Ljava/lang/String;)Z
    //   153: pop
    //   154: iload 12
    //   156: istore 13
    //   158: iload 16
    //   160: istore 14
    //   162: iload 17
    //   164: istore 15
    //   166: aload 19
    //   168: getstatic 864	java/nio/channels/FileChannel$MapMode:READ_WRITE	Ljava/nio/channels/FileChannel$MapMode;
    //   171: lconst_0
    //   172: aload 19
    //   174: invokevirtual 855	java/nio/channels/FileChannel:size	()J
    //   177: l2i
    //   178: i2l
    //   179: invokevirtual 868	java/nio/channels/FileChannel:map	(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    //   182: astore 18
    //   184: iload 12
    //   186: istore 13
    //   188: iload 16
    //   190: istore 14
    //   192: iload 17
    //   194: istore 15
    //   196: aload_0
    //   197: invokevirtual 872	java/util/ArrayList:toArray	()[Ljava/lang/Object;
    //   200: arraylength
    //   201: anewarray 779	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
    //   204: astore 20
    //   206: iload 12
    //   208: istore 13
    //   210: iload 16
    //   212: istore 14
    //   214: iload 17
    //   216: istore 15
    //   218: aload_0
    //   219: aload_0
    //   220: invokevirtual 205	java/util/ArrayList:size	()I
    //   223: anewarray 779	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
    //   226: invokevirtual 875	java/util/ArrayList:toArray	([Ljava/lang/Object;)[Ljava/lang/Object;
    //   229: checkcast 877	[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    //   232: checkcast 877	[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    //   235: astore 20
    //   237: iconst_m1
    //   238: istore 7
    //   240: iconst_0
    //   241: istore 8
    //   243: iconst_0
    //   244: istore 6
    //   246: iload 12
    //   248: istore 13
    //   250: aload 18
    //   252: invokevirtual 882	java/nio/MappedByteBuffer:hasRemaining	()Z
    //   255: ifeq +1302 -> 1557
    //   258: iload 6
    //   260: ifne +1297 -> 1557
    //   263: iload 8
    //   265: istore 4
    //   267: iload 12
    //   269: istore 13
    //   271: aload 18
    //   273: invokevirtual 885	java/nio/MappedByteBuffer:position	()I
    //   276: iload 8
    //   278: isub
    //   279: ldc_w 886
    //   282: if_icmple +46 -> 328
    //   285: iload 12
    //   287: istore 13
    //   289: new 174	java/lang/StringBuilder
    //   292: dup
    //   293: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   296: ldc_w 888
    //   299: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   302: aload 18
    //   304: invokevirtual 885	java/nio/MappedByteBuffer:position	()I
    //   307: invokevirtual 828	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   310: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   313: invokestatic 319	com/chelpus/Utils:sendFromRootCP	(Ljava/lang/String;)Z
    //   316: pop
    //   317: iload 12
    //   319: istore 13
    //   321: aload 18
    //   323: invokevirtual 885	java/nio/MappedByteBuffer:position	()I
    //   326: istore 4
    //   328: iload 12
    //   330: istore 13
    //   332: aload 18
    //   334: iload 7
    //   336: iconst_1
    //   337: iadd
    //   338: invokevirtual 891	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   341: pop
    //   342: iload 12
    //   344: istore 13
    //   346: aload 18
    //   348: invokevirtual 885	java/nio/MappedByteBuffer:position	()I
    //   351: istore 9
    //   353: iload 12
    //   355: istore 13
    //   357: aload 18
    //   359: invokevirtual 894	java/nio/MappedByteBuffer:get	()B
    //   362: istore_2
    //   363: iconst_0
    //   364: istore 5
    //   366: aload 18
    //   368: astore_0
    //   369: iload 6
    //   371: istore_3
    //   372: iload_3
    //   373: istore 6
    //   375: iload 9
    //   377: istore 7
    //   379: aload_0
    //   380: astore 18
    //   382: iload 4
    //   384: istore 8
    //   386: iload 12
    //   388: istore 13
    //   390: iload 5
    //   392: aload 20
    //   394: arraylength
    //   395: if_icmpge -149 -> 246
    //   398: iload 12
    //   400: istore 13
    //   402: aload_0
    //   403: iload 9
    //   405: invokevirtual 891	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   408: pop
    //   409: iload 12
    //   411: istore 13
    //   413: iload_2
    //   414: aload 20
    //   416: iload 5
    //   418: aaload
    //   419: getfield 897	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origByte	[B
    //   422: iconst_0
    //   423: baload
    //   424: if_icmpeq +83 -> 507
    //   427: iload 12
    //   429: istore 13
    //   431: aload 20
    //   433: iload 5
    //   435: aaload
    //   436: getfield 901	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origMask	[I
    //   439: iconst_0
    //   440: iaload
    //   441: iconst_1
    //   442: if_icmpeq +65 -> 507
    //   445: iload 12
    //   447: istore 13
    //   449: iload_3
    //   450: istore 7
    //   452: aload_0
    //   453: astore 18
    //   455: aload 20
    //   457: iload 5
    //   459: aaload
    //   460: getfield 901	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origMask	[I
    //   463: iconst_0
    //   464: iaload
    //   465: iconst_1
    //   466: if_icmple +2039 -> 2505
    //   469: iload 12
    //   471: istore 13
    //   473: iload_3
    //   474: istore 7
    //   476: aload_0
    //   477: astore 18
    //   479: iload_2
    //   480: getstatic 108	com/chelpus/root/utils/custompatch:search	Ljava/util/ArrayList;
    //   483: aload 20
    //   485: iload 5
    //   487: aaload
    //   488: getfield 901	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origMask	[I
    //   491: iconst_0
    //   492: iaload
    //   493: iconst_2
    //   494: isub
    //   495: invokevirtual 904	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   498: checkcast 906	java/lang/Byte
    //   501: invokevirtual 907	java/lang/Byte:byteValue	()B
    //   504: if_icmpne +2001 -> 2505
    //   507: iload 12
    //   509: istore 13
    //   511: aload 20
    //   513: iload 5
    //   515: aaload
    //   516: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   519: iconst_0
    //   520: iaload
    //   521: ifne +18 -> 539
    //   524: iload 12
    //   526: istore 13
    //   528: aload 20
    //   530: iload 5
    //   532: aaload
    //   533: getfield 911	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   536: iconst_0
    //   537: iload_2
    //   538: bastore
    //   539: iload 12
    //   541: istore 13
    //   543: aload 20
    //   545: iload 5
    //   547: aaload
    //   548: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   551: iconst_0
    //   552: iaload
    //   553: iconst_1
    //   554: if_icmple +61 -> 615
    //   557: iload 12
    //   559: istore 13
    //   561: aload 20
    //   563: iload 5
    //   565: aaload
    //   566: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   569: iconst_0
    //   570: iaload
    //   571: sipush 253
    //   574: if_icmpge +41 -> 615
    //   577: iload 12
    //   579: istore 13
    //   581: aload 20
    //   583: iload 5
    //   585: aaload
    //   586: getfield 911	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   589: iconst_0
    //   590: getstatic 108	com/chelpus/root/utils/custompatch:search	Ljava/util/ArrayList;
    //   593: aload 20
    //   595: iload 5
    //   597: aaload
    //   598: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   601: iconst_0
    //   602: iaload
    //   603: iconst_2
    //   604: isub
    //   605: invokevirtual 904	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   608: checkcast 906	java/lang/Byte
    //   611: invokevirtual 907	java/lang/Byte:byteValue	()B
    //   614: bastore
    //   615: iload 12
    //   617: istore 13
    //   619: aload 20
    //   621: iload 5
    //   623: aaload
    //   624: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   627: iconst_0
    //   628: iaload
    //   629: sipush 253
    //   632: if_icmpne +30 -> 662
    //   635: iload 12
    //   637: istore 13
    //   639: aload 20
    //   641: iload 5
    //   643: aaload
    //   644: getfield 911	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   647: iconst_0
    //   648: iload_2
    //   649: bipush 15
    //   651: iand
    //   652: iload_2
    //   653: bipush 15
    //   655: iand
    //   656: bipush 16
    //   658: imul
    //   659: iadd
    //   660: i2b
    //   661: bastore
    //   662: iload 12
    //   664: istore 13
    //   666: aload 20
    //   668: iload 5
    //   670: aaload
    //   671: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   674: iconst_0
    //   675: iaload
    //   676: sipush 254
    //   679: if_icmpne +25 -> 704
    //   682: iload 12
    //   684: istore 13
    //   686: aload 20
    //   688: iload 5
    //   690: aaload
    //   691: getfield 911	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   694: iconst_0
    //   695: iload_2
    //   696: bipush 15
    //   698: iand
    //   699: bipush 16
    //   701: iadd
    //   702: i2b
    //   703: bastore
    //   704: iload 12
    //   706: istore 13
    //   708: aload 20
    //   710: iload 5
    //   712: aaload
    //   713: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   716: iconst_0
    //   717: iaload
    //   718: sipush 255
    //   721: if_icmpne +22 -> 743
    //   724: iload 12
    //   726: istore 13
    //   728: aload 20
    //   730: iload 5
    //   732: aaload
    //   733: getfield 911	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   736: iconst_0
    //   737: iload_2
    //   738: bipush 15
    //   740: iand
    //   741: i2b
    //   742: bastore
    //   743: iconst_1
    //   744: istore 6
    //   746: iload 12
    //   748: istore 13
    //   750: aload_0
    //   751: iload 9
    //   753: iconst_1
    //   754: iadd
    //   755: invokevirtual 891	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   758: pop
    //   759: iload 12
    //   761: istore 13
    //   763: aload_0
    //   764: invokevirtual 894	java/nio/MappedByteBuffer:get	()B
    //   767: istore_1
    //   768: iload 12
    //   770: istore 13
    //   772: iload_1
    //   773: aload 20
    //   775: iload 5
    //   777: aaload
    //   778: getfield 897	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origByte	[B
    //   781: iload 6
    //   783: baload
    //   784: if_icmpeq +84 -> 868
    //   787: iload 12
    //   789: istore 13
    //   791: aload 20
    //   793: iload 5
    //   795: aaload
    //   796: getfield 901	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origMask	[I
    //   799: iload 6
    //   801: iaload
    //   802: iconst_1
    //   803: if_icmple +36 -> 839
    //   806: iload 12
    //   808: istore 13
    //   810: iload_1
    //   811: getstatic 108	com/chelpus/root/utils/custompatch:search	Ljava/util/ArrayList;
    //   814: aload 20
    //   816: iload 5
    //   818: aaload
    //   819: getfield 901	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origMask	[I
    //   822: iload 6
    //   824: iaload
    //   825: iconst_2
    //   826: isub
    //   827: invokevirtual 904	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   830: checkcast 906	java/lang/Byte
    //   833: invokevirtual 907	java/lang/Byte:byteValue	()B
    //   836: if_icmpeq +32 -> 868
    //   839: iload 12
    //   841: istore 13
    //   843: aload 20
    //   845: iload 5
    //   847: aaload
    //   848: getfield 901	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origMask	[I
    //   851: iload 6
    //   853: iaload
    //   854: istore 8
    //   856: iload_3
    //   857: istore 7
    //   859: aload_0
    //   860: astore 18
    //   862: iload 8
    //   864: iconst_1
    //   865: if_icmpne +1640 -> 2505
    //   868: iload 12
    //   870: istore 13
    //   872: aload 20
    //   874: iload 5
    //   876: aaload
    //   877: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   880: iload 6
    //   882: iaload
    //   883: ifne +19 -> 902
    //   886: iload 12
    //   888: istore 13
    //   890: aload 20
    //   892: iload 5
    //   894: aaload
    //   895: getfield 911	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   898: iload 6
    //   900: iload_1
    //   901: bastore
    //   902: iload 12
    //   904: istore 13
    //   906: aload 20
    //   908: iload 5
    //   910: aaload
    //   911: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   914: iload 6
    //   916: iaload
    //   917: iconst_1
    //   918: if_icmple +72 -> 990
    //   921: iload 12
    //   923: istore 13
    //   925: aload 20
    //   927: iload 5
    //   929: aaload
    //   930: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   933: iload 6
    //   935: iaload
    //   936: sipush 253
    //   939: if_icmpge +51 -> 990
    //   942: iload 12
    //   944: istore 13
    //   946: aload 20
    //   948: iload 5
    //   950: aaload
    //   951: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   954: iload 6
    //   956: iaload
    //   957: istore 7
    //   959: iload 12
    //   961: istore 13
    //   963: aload 20
    //   965: iload 5
    //   967: aaload
    //   968: getfield 911	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   971: iload 6
    //   973: getstatic 108	com/chelpus/root/utils/custompatch:search	Ljava/util/ArrayList;
    //   976: iload 7
    //   978: iconst_2
    //   979: isub
    //   980: invokevirtual 904	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   983: checkcast 906	java/lang/Byte
    //   986: invokevirtual 907	java/lang/Byte:byteValue	()B
    //   989: bastore
    //   990: iload 12
    //   992: istore 13
    //   994: aload 20
    //   996: iload 5
    //   998: aaload
    //   999: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   1002: iconst_0
    //   1003: iaload
    //   1004: sipush 253
    //   1007: if_icmpne +30 -> 1037
    //   1010: iload 12
    //   1012: istore 13
    //   1014: aload 20
    //   1016: iload 5
    //   1018: aaload
    //   1019: getfield 911	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   1022: iconst_0
    //   1023: iload_2
    //   1024: bipush 15
    //   1026: iand
    //   1027: iload_2
    //   1028: bipush 15
    //   1030: iand
    //   1031: bipush 16
    //   1033: imul
    //   1034: iadd
    //   1035: i2b
    //   1036: bastore
    //   1037: iload 12
    //   1039: istore 13
    //   1041: aload 20
    //   1043: iload 5
    //   1045: aaload
    //   1046: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   1049: iload 6
    //   1051: iaload
    //   1052: sipush 254
    //   1055: if_icmpne +26 -> 1081
    //   1058: iload 12
    //   1060: istore 13
    //   1062: aload 20
    //   1064: iload 5
    //   1066: aaload
    //   1067: getfield 911	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   1070: iload 6
    //   1072: iload_1
    //   1073: bipush 15
    //   1075: iand
    //   1076: bipush 16
    //   1078: iadd
    //   1079: i2b
    //   1080: bastore
    //   1081: iload 12
    //   1083: istore 13
    //   1085: aload 20
    //   1087: iload 5
    //   1089: aaload
    //   1090: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   1093: iload 6
    //   1095: iaload
    //   1096: sipush 255
    //   1099: if_icmpne +23 -> 1122
    //   1102: iload 12
    //   1104: istore 13
    //   1106: aload 20
    //   1108: iload 5
    //   1110: aaload
    //   1111: getfield 911	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   1114: iload 6
    //   1116: iload_1
    //   1117: bipush 15
    //   1119: iand
    //   1120: i2b
    //   1121: bastore
    //   1122: iload 6
    //   1124: iconst_1
    //   1125: iadd
    //   1126: istore 6
    //   1128: iload 12
    //   1130: istore 13
    //   1132: iload 6
    //   1134: aload 20
    //   1136: iload 5
    //   1138: aaload
    //   1139: getfield 897	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origByte	[B
    //   1142: arraylength
    //   1143: if_icmpne +760 -> 1903
    //   1146: iconst_0
    //   1147: istore 8
    //   1149: iload 12
    //   1151: istore 13
    //   1153: aload 20
    //   1155: iload 5
    //   1157: aaload
    //   1158: getfield 901	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origMask	[I
    //   1161: astore 18
    //   1163: iload 12
    //   1165: istore 13
    //   1167: aload 18
    //   1169: arraylength
    //   1170: istore 10
    //   1172: iconst_0
    //   1173: istore 7
    //   1175: goto +1298 -> 2473
    //   1178: aload_0
    //   1179: astore 18
    //   1181: iload 12
    //   1183: istore 13
    //   1185: aload 20
    //   1187: iload 5
    //   1189: aaload
    //   1190: getfield 913	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:insert	Z
    //   1193: ifeq +140 -> 1333
    //   1196: iload 12
    //   1198: istore 13
    //   1200: aload_0
    //   1201: invokevirtual 885	java/nio/MappedByteBuffer:position	()I
    //   1204: istore 6
    //   1206: iload 12
    //   1208: istore 13
    //   1210: aload 19
    //   1212: invokevirtual 855	java/nio/channels/FileChannel:size	()J
    //   1215: l2i
    //   1216: iload 6
    //   1218: isub
    //   1219: istore 7
    //   1221: iload 12
    //   1223: istore 13
    //   1225: iload 7
    //   1227: newarray <illegal type>
    //   1229: astore 18
    //   1231: iload 12
    //   1233: istore 13
    //   1235: aload_0
    //   1236: aload 18
    //   1238: iconst_0
    //   1239: iload 7
    //   1241: invokevirtual 916	java/nio/MappedByteBuffer:get	([BII)Ljava/nio/ByteBuffer;
    //   1244: pop
    //   1245: iload 12
    //   1247: istore 13
    //   1249: aload 18
    //   1251: invokestatic 922	java/nio/ByteBuffer:wrap	([B)Ljava/nio/ByteBuffer;
    //   1254: astore_0
    //   1255: iload 12
    //   1257: istore 13
    //   1259: aload 19
    //   1261: aload 20
    //   1263: iload 5
    //   1265: aaload
    //   1266: getfield 911	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   1269: arraylength
    //   1270: aload 20
    //   1272: iload 5
    //   1274: aaload
    //   1275: getfield 897	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origByte	[B
    //   1278: arraylength
    //   1279: isub
    //   1280: iload 6
    //   1282: iadd
    //   1283: i2l
    //   1284: invokevirtual 925	java/nio/channels/FileChannel:position	(J)Ljava/nio/channels/FileChannel;
    //   1287: pop
    //   1288: iload 12
    //   1290: istore 13
    //   1292: aload 19
    //   1294: aload_0
    //   1295: invokevirtual 929	java/nio/channels/FileChannel:write	(Ljava/nio/ByteBuffer;)I
    //   1298: pop
    //   1299: iload 12
    //   1301: istore 13
    //   1303: aload 19
    //   1305: getstatic 864	java/nio/channels/FileChannel$MapMode:READ_WRITE	Ljava/nio/channels/FileChannel$MapMode;
    //   1308: lconst_0
    //   1309: aload 19
    //   1311: invokevirtual 855	java/nio/channels/FileChannel:size	()J
    //   1314: l2i
    //   1315: i2l
    //   1316: invokevirtual 868	java/nio/channels/FileChannel:map	(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    //   1319: astore 18
    //   1321: iload 12
    //   1323: istore 13
    //   1325: aload 18
    //   1327: iload 6
    //   1329: invokevirtual 891	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   1332: pop
    //   1333: iload 12
    //   1335: istore 13
    //   1337: aload 19
    //   1339: iload 9
    //   1341: i2l
    //   1342: invokevirtual 925	java/nio/channels/FileChannel:position	(J)Ljava/nio/channels/FileChannel;
    //   1345: pop
    //   1346: iload 12
    //   1348: istore 13
    //   1350: aload 19
    //   1352: aload 20
    //   1354: iload 5
    //   1356: aaload
    //   1357: getfield 911	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   1360: invokestatic 922	java/nio/ByteBuffer:wrap	([B)Ljava/nio/ByteBuffer;
    //   1363: invokevirtual 929	java/nio/channels/FileChannel:write	(Ljava/nio/ByteBuffer;)I
    //   1366: pop
    //   1367: iload 12
    //   1369: istore 13
    //   1371: aload 18
    //   1373: invokevirtual 933	java/nio/MappedByteBuffer:force	()Ljava/nio/MappedByteBuffer;
    //   1376: pop
    //   1377: iload 12
    //   1379: istore 13
    //   1381: new 174	java/lang/StringBuilder
    //   1384: dup
    //   1385: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   1388: ldc_w 935
    //   1391: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1394: iload 5
    //   1396: iconst_1
    //   1397: iadd
    //   1398: invokevirtual 828	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1401: ldc_w 937
    //   1404: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1407: iload 9
    //   1409: invokestatic 941	java/lang/Integer:toHexString	(I)Ljava/lang/String;
    //   1412: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1415: ldc_w 943
    //   1418: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1421: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1424: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   1427: iload 12
    //   1429: istore 13
    //   1431: aload 20
    //   1433: iload 5
    //   1435: aaload
    //   1436: iconst_1
    //   1437: putfield 840	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:result	Z
    //   1440: iload 12
    //   1442: istore 13
    //   1444: iconst_1
    //   1445: putstatic 110	com/chelpus/root/utils/custompatch:patchteil	Z
    //   1448: iload_3
    //   1449: istore 7
    //   1451: goto +1054 -> 2505
    //   1454: astore 18
    //   1456: iload 12
    //   1458: istore 13
    //   1460: aload 20
    //   1462: iload 5
    //   1464: aaload
    //   1465: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   1468: iconst_0
    //   1469: iaload
    //   1470: iconst_2
    //   1471: isub
    //   1472: istore 6
    //   1474: iload 12
    //   1476: istore 13
    //   1478: new 174	java/lang/StringBuilder
    //   1481: dup
    //   1482: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   1485: ldc_w 945
    //   1488: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1491: iload 6
    //   1493: invokevirtual 828	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1496: ldc_w 947
    //   1499: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1502: iload 6
    //   1504: invokevirtual 828	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1507: ldc_w 949
    //   1510: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1513: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1516: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   1519: goto -904 -> 615
    //   1522: astore_0
    //   1523: iload 12
    //   1525: istore 13
    //   1527: iload 16
    //   1529: istore 14
    //   1531: iload 17
    //   1533: istore 15
    //   1535: aload_0
    //   1536: invokevirtual 950	java/lang/IndexOutOfBoundsException:printStackTrace	()V
    //   1539: iload 12
    //   1541: istore 13
    //   1543: iload 16
    //   1545: istore 14
    //   1547: iload 17
    //   1549: istore 15
    //   1551: ldc_w 952
    //   1554: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   1557: iload 12
    //   1559: istore 13
    //   1561: iload 16
    //   1563: istore 14
    //   1565: iload 17
    //   1567: istore 15
    //   1569: aload 19
    //   1571: invokevirtual 953	java/nio/channels/FileChannel:close	()V
    //   1574: iload 12
    //   1576: istore 13
    //   1578: iload 16
    //   1580: istore 14
    //   1582: iload 17
    //   1584: istore 15
    //   1586: ldc_w 813
    //   1589: invokestatic 319	com/chelpus/Utils:sendFromRootCP	(Ljava/lang/String;)Z
    //   1592: pop
    //   1593: iconst_0
    //   1594: istore 4
    //   1596: iload 11
    //   1598: istore 13
    //   1600: iload 11
    //   1602: istore 12
    //   1604: iload 11
    //   1606: istore 14
    //   1608: iload 11
    //   1610: istore 15
    //   1612: iload 4
    //   1614: aload 20
    //   1616: arraylength
    //   1617: if_icmpge +743 -> 2360
    //   1620: iload 11
    //   1622: istore 13
    //   1624: iload 11
    //   1626: istore 14
    //   1628: iload 11
    //   1630: istore 15
    //   1632: aload 20
    //   1634: iload 4
    //   1636: aaload
    //   1637: getfield 840	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:result	Z
    //   1640: ifne +614 -> 2254
    //   1643: iconst_0
    //   1644: istore 6
    //   1646: iconst_0
    //   1647: istore_3
    //   1648: iload 11
    //   1650: istore 13
    //   1652: iload 11
    //   1654: istore 14
    //   1656: iload 11
    //   1658: istore 15
    //   1660: aload 20
    //   1662: iload 4
    //   1664: aaload
    //   1665: getfield 954	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:group	Ljava/lang/String;
    //   1668: ldc -124
    //   1670: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1673: ifne +242 -> 1915
    //   1676: iload 11
    //   1678: istore 13
    //   1680: iload 11
    //   1682: istore 14
    //   1684: iload 11
    //   1686: istore 15
    //   1688: aload 20
    //   1690: arraylength
    //   1691: istore 7
    //   1693: iconst_0
    //   1694: istore 5
    //   1696: iload_3
    //   1697: istore 6
    //   1699: iload 5
    //   1701: iload 7
    //   1703: if_icmpge +212 -> 1915
    //   1706: aload 20
    //   1708: iload 5
    //   1710: aaload
    //   1711: astore_0
    //   1712: iload_3
    //   1713: istore 6
    //   1715: iload 11
    //   1717: istore 13
    //   1719: iload 11
    //   1721: istore 14
    //   1723: iload 11
    //   1725: istore 15
    //   1727: aload 20
    //   1729: iload 4
    //   1731: aaload
    //   1732: getfield 954	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:group	Ljava/lang/String;
    //   1735: aload_0
    //   1736: getfield 954	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:group	Ljava/lang/String;
    //   1739: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1742: ifeq +80 -> 1822
    //   1745: iload_3
    //   1746: istore 6
    //   1748: iload 11
    //   1750: istore 13
    //   1752: iload 11
    //   1754: istore 14
    //   1756: iload 11
    //   1758: istore 15
    //   1760: aload_0
    //   1761: getfield 840	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:result	Z
    //   1764: ifeq +58 -> 1822
    //   1767: iload 11
    //   1769: istore 13
    //   1771: iload 11
    //   1773: istore 14
    //   1775: iload 11
    //   1777: istore 15
    //   1779: getstatic 164	com/chelpus/root/utils/custompatch:multidex	Z
    //   1782: ifne +21 -> 1803
    //   1785: iload 11
    //   1787: istore 13
    //   1789: iload 11
    //   1791: istore 14
    //   1793: iload 11
    //   1795: istore 15
    //   1797: getstatic 168	com/chelpus/root/utils/custompatch:multilib_patch	Z
    //   1800: ifeq +19 -> 1819
    //   1803: iload 11
    //   1805: istore 13
    //   1807: iload 11
    //   1809: istore 14
    //   1811: iload 11
    //   1813: istore 15
    //   1815: iconst_1
    //   1816: putstatic 166	com/chelpus/root/utils/custompatch:goodResult	Z
    //   1819: iconst_1
    //   1820: istore 6
    //   1822: iload 5
    //   1824: iconst_1
    //   1825: iadd
    //   1826: istore 5
    //   1828: iload 6
    //   1830: istore_3
    //   1831: goto -135 -> 1696
    //   1834: astore 18
    //   1836: iload 12
    //   1838: istore 13
    //   1840: aload 20
    //   1842: iload 5
    //   1844: aaload
    //   1845: getfield 910	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   1848: iload 6
    //   1850: iaload
    //   1851: iconst_2
    //   1852: isub
    //   1853: istore 7
    //   1855: iload 12
    //   1857: istore 13
    //   1859: new 174	java/lang/StringBuilder
    //   1862: dup
    //   1863: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   1866: ldc_w 945
    //   1869: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1872: iload 7
    //   1874: invokevirtual 828	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1877: ldc_w 947
    //   1880: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1883: iload 7
    //   1885: invokevirtual 828	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1888: ldc_w 949
    //   1891: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1894: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1897: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   1900: goto -910 -> 990
    //   1903: iload 12
    //   1905: istore 13
    //   1907: aload_0
    //   1908: invokevirtual 894	java/nio/MappedByteBuffer:get	()B
    //   1911: istore_1
    //   1912: goto -1144 -> 768
    //   1915: iload 11
    //   1917: istore 12
    //   1919: iload 6
    //   1921: ifne +612 -> 2533
    //   1924: iload 11
    //   1926: istore 13
    //   1928: iload 11
    //   1930: istore 14
    //   1932: iload 11
    //   1934: istore 15
    //   1936: iload 11
    //   1938: istore 12
    //   1940: getstatic 166	com/chelpus/root/utils/custompatch:goodResult	Z
    //   1943: ifne +590 -> 2533
    //   1946: iload 11
    //   1948: istore 13
    //   1950: iload 11
    //   1952: istore 14
    //   1954: iload 11
    //   1956: istore 15
    //   1958: getstatic 164	com/chelpus/root/utils/custompatch:multidex	Z
    //   1961: ifeq +114 -> 2075
    //   1964: iload 11
    //   1966: istore 13
    //   1968: iload 11
    //   1970: istore 14
    //   1972: iload 11
    //   1974: istore 15
    //   1976: iload 11
    //   1978: istore 12
    //   1980: getstatic 166	com/chelpus/root/utils/custompatch:goodResult	Z
    //   1983: ifne +550 -> 2533
    //   1986: iload 11
    //   1988: istore 13
    //   1990: iload 11
    //   1992: istore 14
    //   1994: iload 11
    //   1996: istore 15
    //   1998: iload 11
    //   2000: istore 12
    //   2002: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   2005: getstatic 162	com/chelpus/root/utils/custompatch:classesFiles	Ljava/util/ArrayList;
    //   2008: getstatic 162	com/chelpus/root/utils/custompatch:classesFiles	Ljava/util/ArrayList;
    //   2011: invokevirtual 205	java/util/ArrayList:size	()I
    //   2014: iconst_1
    //   2015: isub
    //   2016: invokevirtual 904	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   2019: invokevirtual 955	java/io/File:equals	(Ljava/lang/Object;)Z
    //   2022: ifeq +511 -> 2533
    //   2025: iload 11
    //   2027: istore 13
    //   2029: iload 11
    //   2031: istore 14
    //   2033: iload 11
    //   2035: istore 15
    //   2037: new 174	java/lang/StringBuilder
    //   2040: dup
    //   2041: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   2044: ldc_w 935
    //   2047: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2050: iload 4
    //   2052: iconst_1
    //   2053: iadd
    //   2054: invokevirtual 828	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   2057: ldc_w 957
    //   2060: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2063: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2066: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   2069: iconst_0
    //   2070: istore 12
    //   2072: goto +461 -> 2533
    //   2075: iload 11
    //   2077: istore 13
    //   2079: iload 11
    //   2081: istore 14
    //   2083: iload 11
    //   2085: istore 15
    //   2087: getstatic 168	com/chelpus/root/utils/custompatch:multilib_patch	Z
    //   2090: ifeq +114 -> 2204
    //   2093: iload 11
    //   2095: istore 13
    //   2097: iload 11
    //   2099: istore 14
    //   2101: iload 11
    //   2103: istore 15
    //   2105: iload 11
    //   2107: istore 12
    //   2109: getstatic 166	com/chelpus/root/utils/custompatch:goodResult	Z
    //   2112: ifne +421 -> 2533
    //   2115: iload 11
    //   2117: istore 13
    //   2119: iload 11
    //   2121: istore 14
    //   2123: iload 11
    //   2125: istore 15
    //   2127: iload 11
    //   2129: istore 12
    //   2131: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   2134: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   2137: getstatic 102	com/chelpus/root/utils/custompatch:arrayFile2	Ljava/util/ArrayList;
    //   2140: invokevirtual 205	java/util/ArrayList:size	()I
    //   2143: iconst_1
    //   2144: isub
    //   2145: invokevirtual 904	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   2148: invokevirtual 955	java/io/File:equals	(Ljava/lang/Object;)Z
    //   2151: ifeq +382 -> 2533
    //   2154: iload 11
    //   2156: istore 13
    //   2158: iload 11
    //   2160: istore 14
    //   2162: iload 11
    //   2164: istore 15
    //   2166: new 174	java/lang/StringBuilder
    //   2169: dup
    //   2170: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   2173: ldc_w 935
    //   2176: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2179: iload 4
    //   2181: iconst_1
    //   2182: iadd
    //   2183: invokevirtual 828	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   2186: ldc_w 957
    //   2189: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2192: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2195: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   2198: iconst_0
    //   2199: istore 12
    //   2201: goto +332 -> 2533
    //   2204: iload 11
    //   2206: istore 13
    //   2208: iload 11
    //   2210: istore 14
    //   2212: iload 11
    //   2214: istore 15
    //   2216: new 174	java/lang/StringBuilder
    //   2219: dup
    //   2220: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   2223: ldc_w 935
    //   2226: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2229: iload 4
    //   2231: iconst_1
    //   2232: iadd
    //   2233: invokevirtual 828	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   2236: ldc_w 957
    //   2239: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2242: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2245: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   2248: iconst_0
    //   2249: istore 12
    //   2251: goto +282 -> 2533
    //   2254: iload 11
    //   2256: istore 13
    //   2258: iload 11
    //   2260: istore 14
    //   2262: iload 11
    //   2264: istore 15
    //   2266: getstatic 164	com/chelpus/root/utils/custompatch:multidex	Z
    //   2269: ifne +25 -> 2294
    //   2272: iload 11
    //   2274: istore 13
    //   2276: iload 11
    //   2278: istore 14
    //   2280: iload 11
    //   2282: istore 15
    //   2284: iload 11
    //   2286: istore 12
    //   2288: getstatic 168	com/chelpus/root/utils/custompatch:multilib_patch	Z
    //   2291: ifeq +242 -> 2533
    //   2294: iload 11
    //   2296: istore 13
    //   2298: iload 11
    //   2300: istore 14
    //   2302: iload 11
    //   2304: istore 15
    //   2306: iload 11
    //   2308: istore 12
    //   2310: aload 20
    //   2312: iload 4
    //   2314: aaload
    //   2315: getfield 954	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:group	Ljava/lang/String;
    //   2318: ldc -124
    //   2320: invokevirtual 308	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   2323: ifne +210 -> 2533
    //   2326: iload 11
    //   2328: istore 13
    //   2330: iload 11
    //   2332: istore 14
    //   2334: iload 11
    //   2336: istore 15
    //   2338: iconst_1
    //   2339: putstatic 166	com/chelpus/root/utils/custompatch:goodResult	Z
    //   2342: iload 11
    //   2344: istore 12
    //   2346: goto +187 -> 2533
    //   2349: astore_0
    //   2350: ldc_w 959
    //   2353: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   2356: iload 13
    //   2358: istore 12
    //   2360: getstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   2363: iconst_1
    //   2364: if_icmpne +9 -> 2373
    //   2367: getstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   2370: ifeq +11 -> 2381
    //   2373: getstatic 389	com/chelpus/root/utils/custompatch:tag	I
    //   2376: bipush 10
    //   2378: if_icmpne +18 -> 2396
    //   2381: getstatic 144	com/chelpus/root/utils/custompatch:ART	Z
    //   2384: ifne +12 -> 2396
    //   2387: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   2390: getstatic 128	com/chelpus/root/utils/custompatch:dirapp	Ljava/lang/String;
    //   2393: invokestatic 962	com/chelpus/Utils:fixadlerOdex	(Ljava/io/File;Ljava/lang/String;)V
    //   2396: getstatic 112	com/chelpus/root/utils/custompatch:unpack	Z
    //   2399: ifeq +25 -> 2424
    //   2402: getstatic 138	com/chelpus/root/utils/custompatch:odexpatch	Z
    //   2405: ifne +19 -> 2424
    //   2408: getstatic 140	com/chelpus/root/utils/custompatch:OdexPatch	Z
    //   2411: ifne +13 -> 2424
    //   2414: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   2417: invokestatic 965	com/chelpus/Utils:fixadler	(Ljava/io/File;)V
    //   2420: iconst_1
    //   2421: putstatic 114	com/chelpus/root/utils/custompatch:fixunpack	Z
    //   2424: iload 12
    //   2426: ireturn
    //   2427: astore_0
    //   2428: new 174	java/lang/StringBuilder
    //   2431: dup
    //   2432: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   2435: ldc_w 967
    //   2438: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2441: aload_0
    //   2442: invokevirtual 224	java/lang/Exception:toString	()Ljava/lang/String;
    //   2445: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2448: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2451: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   2454: iload 14
    //   2456: istore 12
    //   2458: goto -98 -> 2360
    //   2461: astore_0
    //   2462: iload 15
    //   2464: istore 12
    //   2466: goto -106 -> 2360
    //   2469: astore_0
    //   2470: goto -913 -> 1557
    //   2473: iload 8
    //   2475: istore 6
    //   2477: iload 7
    //   2479: iload 10
    //   2481: if_icmpge +14 -> 2495
    //   2484: aload 18
    //   2486: iload 7
    //   2488: iaload
    //   2489: ifne +35 -> 2524
    //   2492: iconst_1
    //   2493: istore 6
    //   2495: iload 6
    //   2497: ifne -1319 -> 1178
    //   2500: iconst_1
    //   2501: istore_3
    //   2502: goto -1324 -> 1178
    //   2505: iload 5
    //   2507: iconst_1
    //   2508: iadd
    //   2509: istore 5
    //   2511: iload 7
    //   2513: istore_3
    //   2514: aload 18
    //   2516: astore_0
    //   2517: goto -2145 -> 372
    //   2520: astore_0
    //   2521: goto -964 -> 1557
    //   2524: iload 7
    //   2526: iconst_1
    //   2527: iadd
    //   2528: istore 7
    //   2530: goto -57 -> 2473
    //   2533: iload 4
    //   2535: iconst_1
    //   2536: iadd
    //   2537: istore 4
    //   2539: iload 12
    //   2541: istore 11
    //   2543: goto -947 -> 1596
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	2546	0	paramArrayList	ArrayList<PatchesItem>
    //   767	1145	1	i	int
    //   362	669	2	j	int
    //   371	2143	3	k	int
    //   265	2273	4	m	int
    //   364	2146	5	n	int
    //   244	2252	6	i1	int
    //   238	2291	7	i2	int
    //   241	2233	8	i3	int
    //   351	1057	9	i4	int
    //   1170	1312	10	i5	int
    //   10	2532	11	bool1	boolean
    //   1	2539	12	bool2	boolean
    //   86	2271	13	bool3	boolean
    //   90	2365	14	bool4	boolean
    //   94	2369	15	bool5	boolean
    //   4	1575	16	bool6	boolean
    //   7	1576	17	bool7	boolean
    //   27	1345	18	localObject	Object
    //   1454	1	18	localException1	Exception
    //   1834	681	18	localException2	Exception
    //   112	1458	19	localFileChannel	FileChannel
    //   204	2107	20	arrayOfPatchesItem	PatchesItem[]
    // Exception table:
    //   from	to	target	type
    //   543	557	1454	java/lang/Exception
    //   561	577	1454	java/lang/Exception
    //   581	615	1454	java/lang/Exception
    //   250	258	1522	java/lang/IndexOutOfBoundsException
    //   271	285	1522	java/lang/IndexOutOfBoundsException
    //   289	317	1522	java/lang/IndexOutOfBoundsException
    //   321	328	1522	java/lang/IndexOutOfBoundsException
    //   332	342	1522	java/lang/IndexOutOfBoundsException
    //   346	353	1522	java/lang/IndexOutOfBoundsException
    //   357	363	1522	java/lang/IndexOutOfBoundsException
    //   390	398	1522	java/lang/IndexOutOfBoundsException
    //   402	409	1522	java/lang/IndexOutOfBoundsException
    //   413	427	1522	java/lang/IndexOutOfBoundsException
    //   431	445	1522	java/lang/IndexOutOfBoundsException
    //   455	469	1522	java/lang/IndexOutOfBoundsException
    //   479	507	1522	java/lang/IndexOutOfBoundsException
    //   511	524	1522	java/lang/IndexOutOfBoundsException
    //   528	539	1522	java/lang/IndexOutOfBoundsException
    //   543	557	1522	java/lang/IndexOutOfBoundsException
    //   561	577	1522	java/lang/IndexOutOfBoundsException
    //   581	615	1522	java/lang/IndexOutOfBoundsException
    //   619	635	1522	java/lang/IndexOutOfBoundsException
    //   639	662	1522	java/lang/IndexOutOfBoundsException
    //   666	682	1522	java/lang/IndexOutOfBoundsException
    //   686	704	1522	java/lang/IndexOutOfBoundsException
    //   708	724	1522	java/lang/IndexOutOfBoundsException
    //   728	743	1522	java/lang/IndexOutOfBoundsException
    //   750	759	1522	java/lang/IndexOutOfBoundsException
    //   763	768	1522	java/lang/IndexOutOfBoundsException
    //   772	787	1522	java/lang/IndexOutOfBoundsException
    //   791	806	1522	java/lang/IndexOutOfBoundsException
    //   810	839	1522	java/lang/IndexOutOfBoundsException
    //   843	856	1522	java/lang/IndexOutOfBoundsException
    //   872	886	1522	java/lang/IndexOutOfBoundsException
    //   890	902	1522	java/lang/IndexOutOfBoundsException
    //   906	921	1522	java/lang/IndexOutOfBoundsException
    //   925	942	1522	java/lang/IndexOutOfBoundsException
    //   946	959	1522	java/lang/IndexOutOfBoundsException
    //   963	990	1522	java/lang/IndexOutOfBoundsException
    //   994	1010	1522	java/lang/IndexOutOfBoundsException
    //   1014	1037	1522	java/lang/IndexOutOfBoundsException
    //   1041	1058	1522	java/lang/IndexOutOfBoundsException
    //   1062	1081	1522	java/lang/IndexOutOfBoundsException
    //   1085	1102	1522	java/lang/IndexOutOfBoundsException
    //   1106	1122	1522	java/lang/IndexOutOfBoundsException
    //   1132	1146	1522	java/lang/IndexOutOfBoundsException
    //   1153	1163	1522	java/lang/IndexOutOfBoundsException
    //   1167	1172	1522	java/lang/IndexOutOfBoundsException
    //   1185	1196	1522	java/lang/IndexOutOfBoundsException
    //   1200	1206	1522	java/lang/IndexOutOfBoundsException
    //   1210	1221	1522	java/lang/IndexOutOfBoundsException
    //   1225	1231	1522	java/lang/IndexOutOfBoundsException
    //   1235	1245	1522	java/lang/IndexOutOfBoundsException
    //   1249	1255	1522	java/lang/IndexOutOfBoundsException
    //   1259	1288	1522	java/lang/IndexOutOfBoundsException
    //   1292	1299	1522	java/lang/IndexOutOfBoundsException
    //   1303	1321	1522	java/lang/IndexOutOfBoundsException
    //   1325	1333	1522	java/lang/IndexOutOfBoundsException
    //   1337	1346	1522	java/lang/IndexOutOfBoundsException
    //   1350	1367	1522	java/lang/IndexOutOfBoundsException
    //   1371	1377	1522	java/lang/IndexOutOfBoundsException
    //   1381	1427	1522	java/lang/IndexOutOfBoundsException
    //   1431	1440	1522	java/lang/IndexOutOfBoundsException
    //   1444	1448	1522	java/lang/IndexOutOfBoundsException
    //   1460	1474	1522	java/lang/IndexOutOfBoundsException
    //   1478	1519	1522	java/lang/IndexOutOfBoundsException
    //   1840	1855	1522	java/lang/IndexOutOfBoundsException
    //   1859	1900	1522	java/lang/IndexOutOfBoundsException
    //   1907	1912	1522	java/lang/IndexOutOfBoundsException
    //   872	886	1834	java/lang/Exception
    //   890	902	1834	java/lang/Exception
    //   906	921	1834	java/lang/Exception
    //   925	942	1834	java/lang/Exception
    //   946	959	1834	java/lang/Exception
    //   963	990	1834	java/lang/Exception
    //   96	114	2349	java/io/FileNotFoundException
    //   126	154	2349	java/io/FileNotFoundException
    //   166	184	2349	java/io/FileNotFoundException
    //   196	206	2349	java/io/FileNotFoundException
    //   218	237	2349	java/io/FileNotFoundException
    //   250	258	2349	java/io/FileNotFoundException
    //   271	285	2349	java/io/FileNotFoundException
    //   289	317	2349	java/io/FileNotFoundException
    //   321	328	2349	java/io/FileNotFoundException
    //   332	342	2349	java/io/FileNotFoundException
    //   346	353	2349	java/io/FileNotFoundException
    //   357	363	2349	java/io/FileNotFoundException
    //   390	398	2349	java/io/FileNotFoundException
    //   402	409	2349	java/io/FileNotFoundException
    //   413	427	2349	java/io/FileNotFoundException
    //   431	445	2349	java/io/FileNotFoundException
    //   455	469	2349	java/io/FileNotFoundException
    //   479	507	2349	java/io/FileNotFoundException
    //   511	524	2349	java/io/FileNotFoundException
    //   528	539	2349	java/io/FileNotFoundException
    //   543	557	2349	java/io/FileNotFoundException
    //   561	577	2349	java/io/FileNotFoundException
    //   581	615	2349	java/io/FileNotFoundException
    //   619	635	2349	java/io/FileNotFoundException
    //   639	662	2349	java/io/FileNotFoundException
    //   666	682	2349	java/io/FileNotFoundException
    //   686	704	2349	java/io/FileNotFoundException
    //   708	724	2349	java/io/FileNotFoundException
    //   728	743	2349	java/io/FileNotFoundException
    //   750	759	2349	java/io/FileNotFoundException
    //   763	768	2349	java/io/FileNotFoundException
    //   772	787	2349	java/io/FileNotFoundException
    //   791	806	2349	java/io/FileNotFoundException
    //   810	839	2349	java/io/FileNotFoundException
    //   843	856	2349	java/io/FileNotFoundException
    //   872	886	2349	java/io/FileNotFoundException
    //   890	902	2349	java/io/FileNotFoundException
    //   906	921	2349	java/io/FileNotFoundException
    //   925	942	2349	java/io/FileNotFoundException
    //   946	959	2349	java/io/FileNotFoundException
    //   963	990	2349	java/io/FileNotFoundException
    //   994	1010	2349	java/io/FileNotFoundException
    //   1014	1037	2349	java/io/FileNotFoundException
    //   1041	1058	2349	java/io/FileNotFoundException
    //   1062	1081	2349	java/io/FileNotFoundException
    //   1085	1102	2349	java/io/FileNotFoundException
    //   1106	1122	2349	java/io/FileNotFoundException
    //   1132	1146	2349	java/io/FileNotFoundException
    //   1153	1163	2349	java/io/FileNotFoundException
    //   1167	1172	2349	java/io/FileNotFoundException
    //   1185	1196	2349	java/io/FileNotFoundException
    //   1200	1206	2349	java/io/FileNotFoundException
    //   1210	1221	2349	java/io/FileNotFoundException
    //   1225	1231	2349	java/io/FileNotFoundException
    //   1235	1245	2349	java/io/FileNotFoundException
    //   1249	1255	2349	java/io/FileNotFoundException
    //   1259	1288	2349	java/io/FileNotFoundException
    //   1292	1299	2349	java/io/FileNotFoundException
    //   1303	1321	2349	java/io/FileNotFoundException
    //   1325	1333	2349	java/io/FileNotFoundException
    //   1337	1346	2349	java/io/FileNotFoundException
    //   1350	1367	2349	java/io/FileNotFoundException
    //   1371	1377	2349	java/io/FileNotFoundException
    //   1381	1427	2349	java/io/FileNotFoundException
    //   1431	1440	2349	java/io/FileNotFoundException
    //   1444	1448	2349	java/io/FileNotFoundException
    //   1460	1474	2349	java/io/FileNotFoundException
    //   1478	1519	2349	java/io/FileNotFoundException
    //   1535	1539	2349	java/io/FileNotFoundException
    //   1551	1557	2349	java/io/FileNotFoundException
    //   1569	1574	2349	java/io/FileNotFoundException
    //   1586	1593	2349	java/io/FileNotFoundException
    //   1612	1620	2349	java/io/FileNotFoundException
    //   1632	1643	2349	java/io/FileNotFoundException
    //   1660	1676	2349	java/io/FileNotFoundException
    //   1688	1693	2349	java/io/FileNotFoundException
    //   1727	1745	2349	java/io/FileNotFoundException
    //   1760	1767	2349	java/io/FileNotFoundException
    //   1779	1785	2349	java/io/FileNotFoundException
    //   1797	1803	2349	java/io/FileNotFoundException
    //   1815	1819	2349	java/io/FileNotFoundException
    //   1840	1855	2349	java/io/FileNotFoundException
    //   1859	1900	2349	java/io/FileNotFoundException
    //   1907	1912	2349	java/io/FileNotFoundException
    //   1940	1946	2349	java/io/FileNotFoundException
    //   1958	1964	2349	java/io/FileNotFoundException
    //   1980	1986	2349	java/io/FileNotFoundException
    //   2002	2025	2349	java/io/FileNotFoundException
    //   2037	2069	2349	java/io/FileNotFoundException
    //   2087	2093	2349	java/io/FileNotFoundException
    //   2109	2115	2349	java/io/FileNotFoundException
    //   2131	2154	2349	java/io/FileNotFoundException
    //   2166	2198	2349	java/io/FileNotFoundException
    //   2216	2248	2349	java/io/FileNotFoundException
    //   2266	2272	2349	java/io/FileNotFoundException
    //   2288	2294	2349	java/io/FileNotFoundException
    //   2310	2326	2349	java/io/FileNotFoundException
    //   2338	2342	2349	java/io/FileNotFoundException
    //   96	114	2427	java/lang/Exception
    //   126	154	2427	java/lang/Exception
    //   166	184	2427	java/lang/Exception
    //   196	206	2427	java/lang/Exception
    //   218	237	2427	java/lang/Exception
    //   1535	1539	2427	java/lang/Exception
    //   1551	1557	2427	java/lang/Exception
    //   1569	1574	2427	java/lang/Exception
    //   1586	1593	2427	java/lang/Exception
    //   1612	1620	2427	java/lang/Exception
    //   1632	1643	2427	java/lang/Exception
    //   1660	1676	2427	java/lang/Exception
    //   1688	1693	2427	java/lang/Exception
    //   1727	1745	2427	java/lang/Exception
    //   1760	1767	2427	java/lang/Exception
    //   1779	1785	2427	java/lang/Exception
    //   1797	1803	2427	java/lang/Exception
    //   1815	1819	2427	java/lang/Exception
    //   1940	1946	2427	java/lang/Exception
    //   1958	1964	2427	java/lang/Exception
    //   1980	1986	2427	java/lang/Exception
    //   2002	2025	2427	java/lang/Exception
    //   2037	2069	2427	java/lang/Exception
    //   2087	2093	2427	java/lang/Exception
    //   2109	2115	2427	java/lang/Exception
    //   2131	2154	2427	java/lang/Exception
    //   2166	2198	2427	java/lang/Exception
    //   2216	2248	2427	java/lang/Exception
    //   2266	2272	2427	java/lang/Exception
    //   2288	2294	2427	java/lang/Exception
    //   2310	2326	2427	java/lang/Exception
    //   2338	2342	2427	java/lang/Exception
    //   96	114	2461	java/nio/BufferUnderflowException
    //   126	154	2461	java/nio/BufferUnderflowException
    //   166	184	2461	java/nio/BufferUnderflowException
    //   196	206	2461	java/nio/BufferUnderflowException
    //   218	237	2461	java/nio/BufferUnderflowException
    //   1535	1539	2461	java/nio/BufferUnderflowException
    //   1551	1557	2461	java/nio/BufferUnderflowException
    //   1569	1574	2461	java/nio/BufferUnderflowException
    //   1586	1593	2461	java/nio/BufferUnderflowException
    //   1612	1620	2461	java/nio/BufferUnderflowException
    //   1632	1643	2461	java/nio/BufferUnderflowException
    //   1660	1676	2461	java/nio/BufferUnderflowException
    //   1688	1693	2461	java/nio/BufferUnderflowException
    //   1727	1745	2461	java/nio/BufferUnderflowException
    //   1760	1767	2461	java/nio/BufferUnderflowException
    //   1779	1785	2461	java/nio/BufferUnderflowException
    //   1797	1803	2461	java/nio/BufferUnderflowException
    //   1815	1819	2461	java/nio/BufferUnderflowException
    //   1940	1946	2461	java/nio/BufferUnderflowException
    //   1958	1964	2461	java/nio/BufferUnderflowException
    //   1980	1986	2461	java/nio/BufferUnderflowException
    //   2002	2025	2461	java/nio/BufferUnderflowException
    //   2037	2069	2461	java/nio/BufferUnderflowException
    //   2087	2093	2461	java/nio/BufferUnderflowException
    //   2109	2115	2461	java/nio/BufferUnderflowException
    //   2131	2154	2461	java/nio/BufferUnderflowException
    //   2166	2198	2461	java/nio/BufferUnderflowException
    //   2216	2248	2461	java/nio/BufferUnderflowException
    //   2266	2272	2461	java/nio/BufferUnderflowException
    //   2288	2294	2461	java/nio/BufferUnderflowException
    //   2310	2326	2461	java/nio/BufferUnderflowException
    //   2338	2342	2461	java/nio/BufferUnderflowException
    //   250	258	2469	java/lang/Exception
    //   271	285	2469	java/lang/Exception
    //   289	317	2469	java/lang/Exception
    //   321	328	2469	java/lang/Exception
    //   332	342	2469	java/lang/Exception
    //   346	353	2469	java/lang/Exception
    //   357	363	2469	java/lang/Exception
    //   390	398	2469	java/lang/Exception
    //   402	409	2469	java/lang/Exception
    //   413	427	2469	java/lang/Exception
    //   431	445	2469	java/lang/Exception
    //   455	469	2469	java/lang/Exception
    //   479	507	2469	java/lang/Exception
    //   511	524	2469	java/lang/Exception
    //   528	539	2469	java/lang/Exception
    //   619	635	2469	java/lang/Exception
    //   639	662	2469	java/lang/Exception
    //   666	682	2469	java/lang/Exception
    //   686	704	2469	java/lang/Exception
    //   708	724	2469	java/lang/Exception
    //   728	743	2469	java/lang/Exception
    //   750	759	2469	java/lang/Exception
    //   763	768	2469	java/lang/Exception
    //   772	787	2469	java/lang/Exception
    //   791	806	2469	java/lang/Exception
    //   810	839	2469	java/lang/Exception
    //   843	856	2469	java/lang/Exception
    //   994	1010	2469	java/lang/Exception
    //   1014	1037	2469	java/lang/Exception
    //   1041	1058	2469	java/lang/Exception
    //   1062	1081	2469	java/lang/Exception
    //   1085	1102	2469	java/lang/Exception
    //   1106	1122	2469	java/lang/Exception
    //   1132	1146	2469	java/lang/Exception
    //   1153	1163	2469	java/lang/Exception
    //   1167	1172	2469	java/lang/Exception
    //   1185	1196	2469	java/lang/Exception
    //   1200	1206	2469	java/lang/Exception
    //   1210	1221	2469	java/lang/Exception
    //   1225	1231	2469	java/lang/Exception
    //   1235	1245	2469	java/lang/Exception
    //   1249	1255	2469	java/lang/Exception
    //   1259	1288	2469	java/lang/Exception
    //   1292	1299	2469	java/lang/Exception
    //   1303	1321	2469	java/lang/Exception
    //   1325	1333	2469	java/lang/Exception
    //   1337	1346	2469	java/lang/Exception
    //   1350	1367	2469	java/lang/Exception
    //   1371	1377	2469	java/lang/Exception
    //   1381	1427	2469	java/lang/Exception
    //   1431	1440	2469	java/lang/Exception
    //   1444	1448	2469	java/lang/Exception
    //   1460	1474	2469	java/lang/Exception
    //   1478	1519	2469	java/lang/Exception
    //   1840	1855	2469	java/lang/Exception
    //   1859	1900	2469	java/lang/Exception
    //   1907	1912	2469	java/lang/Exception
    //   250	258	2520	java/nio/BufferUnderflowException
    //   271	285	2520	java/nio/BufferUnderflowException
    //   289	317	2520	java/nio/BufferUnderflowException
    //   321	328	2520	java/nio/BufferUnderflowException
    //   332	342	2520	java/nio/BufferUnderflowException
    //   346	353	2520	java/nio/BufferUnderflowException
    //   357	363	2520	java/nio/BufferUnderflowException
    //   390	398	2520	java/nio/BufferUnderflowException
    //   402	409	2520	java/nio/BufferUnderflowException
    //   413	427	2520	java/nio/BufferUnderflowException
    //   431	445	2520	java/nio/BufferUnderflowException
    //   455	469	2520	java/nio/BufferUnderflowException
    //   479	507	2520	java/nio/BufferUnderflowException
    //   511	524	2520	java/nio/BufferUnderflowException
    //   528	539	2520	java/nio/BufferUnderflowException
    //   543	557	2520	java/nio/BufferUnderflowException
    //   561	577	2520	java/nio/BufferUnderflowException
    //   581	615	2520	java/nio/BufferUnderflowException
    //   619	635	2520	java/nio/BufferUnderflowException
    //   639	662	2520	java/nio/BufferUnderflowException
    //   666	682	2520	java/nio/BufferUnderflowException
    //   686	704	2520	java/nio/BufferUnderflowException
    //   708	724	2520	java/nio/BufferUnderflowException
    //   728	743	2520	java/nio/BufferUnderflowException
    //   750	759	2520	java/nio/BufferUnderflowException
    //   763	768	2520	java/nio/BufferUnderflowException
    //   772	787	2520	java/nio/BufferUnderflowException
    //   791	806	2520	java/nio/BufferUnderflowException
    //   810	839	2520	java/nio/BufferUnderflowException
    //   843	856	2520	java/nio/BufferUnderflowException
    //   872	886	2520	java/nio/BufferUnderflowException
    //   890	902	2520	java/nio/BufferUnderflowException
    //   906	921	2520	java/nio/BufferUnderflowException
    //   925	942	2520	java/nio/BufferUnderflowException
    //   946	959	2520	java/nio/BufferUnderflowException
    //   963	990	2520	java/nio/BufferUnderflowException
    //   994	1010	2520	java/nio/BufferUnderflowException
    //   1014	1037	2520	java/nio/BufferUnderflowException
    //   1041	1058	2520	java/nio/BufferUnderflowException
    //   1062	1081	2520	java/nio/BufferUnderflowException
    //   1085	1102	2520	java/nio/BufferUnderflowException
    //   1106	1122	2520	java/nio/BufferUnderflowException
    //   1132	1146	2520	java/nio/BufferUnderflowException
    //   1153	1163	2520	java/nio/BufferUnderflowException
    //   1167	1172	2520	java/nio/BufferUnderflowException
    //   1185	1196	2520	java/nio/BufferUnderflowException
    //   1200	1206	2520	java/nio/BufferUnderflowException
    //   1210	1221	2520	java/nio/BufferUnderflowException
    //   1225	1231	2520	java/nio/BufferUnderflowException
    //   1235	1245	2520	java/nio/BufferUnderflowException
    //   1249	1255	2520	java/nio/BufferUnderflowException
    //   1259	1288	2520	java/nio/BufferUnderflowException
    //   1292	1299	2520	java/nio/BufferUnderflowException
    //   1303	1321	2520	java/nio/BufferUnderflowException
    //   1325	1333	2520	java/nio/BufferUnderflowException
    //   1337	1346	2520	java/nio/BufferUnderflowException
    //   1350	1367	2520	java/nio/BufferUnderflowException
    //   1371	1377	2520	java/nio/BufferUnderflowException
    //   1381	1427	2520	java/nio/BufferUnderflowException
    //   1431	1440	2520	java/nio/BufferUnderflowException
    //   1444	1448	2520	java/nio/BufferUnderflowException
    //   1460	1474	2520	java/nio/BufferUnderflowException
    //   1478	1519	2520	java/nio/BufferUnderflowException
    //   1840	1855	2520	java/nio/BufferUnderflowException
    //   1859	1900	2520	java/nio/BufferUnderflowException
    //   1907	1912	2520	java/nio/BufferUnderflowException
  }
  
  /* Error */
  public static void searchDalvik(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic 973	com/chelpus/Utils:getFileDalvikCache	(Ljava/lang/String;)Ljava/io/File;
    //   4: astore_0
    //   5: aload_0
    //   6: ifnull +7 -> 13
    //   9: aload_0
    //   10: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   13: getstatic 138	com/chelpus/root/utils/custompatch:odexpatch	Z
    //   16: ifeq +20 -> 36
    //   19: new 190	java/io/File
    //   22: dup
    //   23: getstatic 128	com/chelpus/root/utils/custompatch:dirapp	Ljava/lang/String;
    //   26: iconst_1
    //   27: invokestatic 345	com/chelpus/Utils:getPlaceForOdex	(Ljava/lang/String;Z)Ljava/lang/String;
    //   30: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   33: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   36: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   39: invokevirtual 198	java/io/File:exists	()Z
    //   42: ifne +20 -> 62
    //   45: new 190	java/io/File
    //   48: dup
    //   49: getstatic 128	com/chelpus/root/utils/custompatch:dirapp	Ljava/lang/String;
    //   52: iconst_1
    //   53: invokestatic 345	com/chelpus/Utils:getPlaceForOdex	(Ljava/lang/String;Z)Ljava/lang/String;
    //   56: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   59: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   62: getstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   65: invokevirtual 198	java/io/File:exists	()Z
    //   68: ifne +24 -> 92
    //   71: new 252	java/io/FileNotFoundException
    //   74: dup
    //   75: invokespecial 974	java/io/FileNotFoundException:<init>	()V
    //   78: athrow
    //   79: astore_0
    //   80: getstatic 136	com/chelpus/root/utils/custompatch:system	Z
    //   83: ifne +9 -> 92
    //   86: ldc_w 976
    //   89: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   92: return
    //   93: astore_0
    //   94: new 190	java/io/File
    //   97: dup
    //   98: getstatic 128	com/chelpus/root/utils/custompatch:dirapp	Ljava/lang/String;
    //   101: iconst_1
    //   102: invokestatic 345	com/chelpus/Utils:getPlaceForOdex	(Ljava/lang/String;Z)Ljava/lang/String;
    //   105: invokespecial 194	java/io/File:<init>	(Ljava/lang/String;)V
    //   108: putstatic 351	com/chelpus/root/utils/custompatch:localFile2	Ljava/io/File;
    //   111: goto -49 -> 62
    //   114: astore_0
    //   115: new 174	java/lang/StringBuilder
    //   118: dup
    //   119: invokespecial 175	java/lang/StringBuilder:<init>	()V
    //   122: ldc -124
    //   124: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   127: aload_0
    //   128: invokevirtual 598	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   131: invokevirtual 185	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   134: invokestatic 226	com/chelpus/root/utils/custompatch:addToLog	(Ljava/lang/String;)V
    //   137: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	138	0	paramString	String
    // Exception table:
    //   from	to	target	type
    //   9	13	79	java/io/FileNotFoundException
    //   13	36	79	java/io/FileNotFoundException
    //   36	62	79	java/io/FileNotFoundException
    //   62	79	79	java/io/FileNotFoundException
    //   94	111	79	java/io/FileNotFoundException
    //   36	62	93	java/lang/Exception
    //   9	13	114	java/lang/Exception
    //   13	36	114	java/lang/Exception
    //   62	79	114	java/lang/Exception
    //   94	111	114	java/lang/Exception
  }
  
  public static void searchDalvikOdex(String paramString1, String paramString2)
    throws FileNotFoundException
  {
    searchDalvik(paramString1);
    try
    {
      if (!odexpatch)
      {
        paramString1 = Utils.getOdexForCreate(paramString2, uid);
        paramString2 = new File(paramString1);
        if (paramString2.exists()) {
          paramString2.delete();
        }
        paramString2 = new File(paramString1.replace("-2", "-1"));
        if (paramString2.exists()) {
          paramString2.delete();
        }
        paramString2 = new File(paramString1.replace("-1", "-2"));
        if (paramString2.exists()) {
          paramString2.delete();
        }
        paramString2 = new File(paramString1.replace("-2", ""));
        if (paramString2.exists()) {
          paramString2.delete();
        }
        paramString2 = new File(paramString1.replace("-1", ""));
        if (paramString2.exists()) {
          paramString2.delete();
        }
        paramString1 = new File(paramString1);
        Utils.copyFile(localFile2, paramString1);
        if ((paramString1.exists()) && (paramString1.length() == localFile2.length()))
        {
          Utils.run_all_no_root(new String[] { "chmod", "644", paramString1.getAbsolutePath() });
          Utils.run_all_no_root(new String[] { "chown", "1000." + uid, paramString1.getAbsolutePath() });
          Utils.run_all_no_root(new String[] { "chown", "1000:" + uid, paramString1.getAbsolutePath() });
          localFile2 = paramString1;
        }
      }
      else
      {
        localFile2 = new File(Utils.getPlaceForOdex(dirapp, true));
        return;
      }
    }
    catch (Exception paramString1)
    {
      paramString1.printStackTrace();
      addToLog("Exception e" + paramString1.toString());
    }
  }
  
  public static boolean searchProcess(ArrayList<SearchItem> paramArrayList)
  {
    boolean bool5 = true;
    boolean bool6 = true;
    boolean bool7 = true;
    boolean bool1 = true;
    searchStr = "";
    Object localObject;
    if ((paramArrayList != null) && (paramArrayList.size() > 0))
    {
      localObject = paramArrayList.iterator();
      while (((Iterator)localObject).hasNext()) {
        ((SearchItem)((Iterator)localObject).next()).result = false;
      }
    }
    boolean bool2 = bool5;
    boolean bool3 = bool6;
    boolean bool4 = bool7;
    int k;
    int m;
    try
    {
      localObject = new RandomAccessFile(localFile2, "rw").getChannel();
      bool2 = bool5;
      bool3 = bool6;
      bool4 = bool7;
      MappedByteBuffer localMappedByteBuffer = ((FileChannel)localObject).map(FileChannel.MapMode.READ_WRITE, 0L, (int)((FileChannel)localObject).size());
      bool2 = bool5;
      bool3 = bool6;
      bool4 = bool7;
      SearchItem[] arrayOfSearchItem = new SearchItem[paramArrayList.toArray().length];
      bool2 = bool5;
      bool3 = bool6;
      bool4 = bool7;
      paramArrayList = (SearchItem[])paramArrayList.toArray(new SearchItem[paramArrayList.size()]);
      long l = 0L;
      for (;;)
      {
        bool2 = bool5;
        bool3 = bool6;
        try
        {
          if (localMappedByteBuffer.hasRemaining())
          {
            bool2 = bool5;
            bool3 = bool6;
            n = localMappedByteBuffer.position();
            bool2 = bool5;
            bool3 = bool6;
            int j = localMappedByteBuffer.get();
            k = 0;
            bool2 = bool5;
            bool3 = bool6;
            if (k < paramArrayList.length)
            {
              bool2 = bool5;
              bool3 = bool6;
              localMappedByteBuffer.position(n);
              bool2 = bool5;
              bool3 = bool6;
              if (paramArrayList[k].result) {
                break label1218;
              }
              bool2 = bool5;
              bool3 = bool6;
              if (j != paramArrayList[k].origByte[0])
              {
                bool2 = bool5;
                bool3 = bool6;
                if (paramArrayList[k].origMask[0] == 0) {
                  break label1218;
                }
              }
              bool2 = bool5;
              bool3 = bool6;
              if (paramArrayList[k].origMask[0] != 0)
              {
                bool2 = bool5;
                bool3 = bool6;
                paramArrayList[k].repByte[0] = j;
              }
              m = 1;
              bool2 = bool5;
              bool3 = bool6;
              localMappedByteBuffer.position(n + 1);
              bool2 = bool5;
              bool3 = bool6;
              for (i = localMappedByteBuffer.get();; i = localMappedByteBuffer.get())
              {
                bool2 = bool5;
                bool3 = bool6;
                if (!paramArrayList[k].result)
                {
                  bool2 = bool5;
                  bool3 = bool6;
                  if (i == paramArrayList[k].origByte[m]) {}
                }
                else
                {
                  bool2 = bool5;
                  bool3 = bool6;
                  if (paramArrayList[k].origMask[m] == 0) {
                    break;
                  }
                }
                bool2 = bool5;
                bool3 = bool6;
                if (paramArrayList[k].origMask[m] > 0)
                {
                  bool2 = bool5;
                  bool3 = bool6;
                  paramArrayList[k].repByte[m] = i;
                }
                m += 1;
                bool2 = bool5;
                bool3 = bool6;
                if (m == paramArrayList[k].origByte.length)
                {
                  bool2 = bool5;
                  bool3 = bool6;
                  paramArrayList[k].result = true;
                  bool2 = bool5;
                  bool3 = bool6;
                  patchteil = true;
                  break;
                }
                bool2 = bool5;
                bool3 = bool6;
              }
            }
            bool2 = bool5;
            bool3 = bool6;
            localMappedByteBuffer.position(n + 1);
            l += 1L;
          }
        }
        catch (Exception localException2)
        {
          bool2 = bool5;
          bool3 = bool6;
          bool4 = bool7;
          addToLog("Search byte error: " + localException2);
          bool2 = bool5;
          bool3 = bool6;
          bool4 = bool7;
          ((FileChannel)localObject).close();
          k = 0;
        }
      }
      label633:
      bool2 = bool1;
      bool3 = bool1;
      bool4 = bool1;
      if (k >= paramArrayList.length) {
        break label1236;
      }
      bool2 = bool1;
      bool3 = bool1;
      bool4 = bool1;
      bool5 = bool1;
      if (paramArrayList[k].result) {
        break label1225;
      }
      bool2 = bool1;
      bool3 = bool1;
      bool4 = bool1;
      searchStr = searchStr + "Bytes by serach N" + (k + 1) + ":\nError LP: Bytes not found!" + "\n";
      bool5 = false;
    }
    catch (FileNotFoundException paramArrayList)
    {
      for (;;)
      {
        int n;
        int i;
        addToLog("Error LP: Program files are not found!\nMove Program to internal storage.");
        bool5 = bool2;
        return bool5;
        bool2 = bool1;
        bool3 = bool1;
        bool4 = bool1;
        searchStr += "\n";
        k += 1;
      }
    }
    catch (BufferUnderflowException paramArrayList)
    {
      addToLog("Exception e" + paramArrayList.toString());
      return bool3;
    }
    catch (Exception paramArrayList)
    {
      label736:
      addToLog("Exception e" + paramArrayList.toString());
      return bool4;
    }
    bool2 = bool1;
    bool5 = bool1;
    bool3 = bool1;
    bool4 = bool1;
    if (k < paramArrayList.length)
    {
      bool2 = bool1;
      bool3 = bool1;
      bool4 = bool1;
      if (!paramArrayList[k].result) {
        break label1241;
      }
      bool2 = bool1;
      bool3 = bool1;
      bool4 = bool1;
      searchStr = searchStr + "\nBytes by search N" + (k + 1) + ":" + "\n";
    }
    for (;;)
    {
      bool2 = bool1;
      bool3 = bool1;
      bool4 = bool1;
      if (m < paramArrayList[k].origMask.length)
      {
        bool2 = bool1;
        bool3 = bool1;
        bool4 = bool1;
        if (paramArrayList[k].origMask[m] > 1)
        {
          bool2 = bool1;
          bool3 = bool1;
          bool4 = bool1;
          n = paramArrayList[k].origMask[m];
          n -= 2;
          bool2 = bool1;
          bool3 = bool1;
          try
          {
            search.set(n, Byte.valueOf(paramArrayList[k].repByte[m]));
            bool2 = bool1;
            bool3 = bool1;
            bool4 = bool1;
            if (!paramArrayList[k].result) {
              break label1247;
            }
            bool2 = bool1;
            bool3 = bool1;
            bool4 = bool1;
            i = ((Byte)search.get(n)).byteValue();
            bool2 = bool1;
            bool3 = bool1;
            bool4 = bool1;
            searchStr = searchStr + "R" + n + "=" + Utils.bytesToHex(new byte[] { i }).toUpperCase() + " ";
          }
          catch (Exception localException1)
          {
            for (;;)
            {
              bool2 = bool1;
              bool3 = bool1;
              bool4 = bool1;
              search.add(n, Byte.valueOf(paramArrayList[k].repByte[m]));
            }
          }
        }
      }
      else
      {
        label1218:
        k += 1;
        break;
        label1225:
        k += 1;
        bool1 = bool5;
        break label633;
        label1236:
        k = 0;
        break label736;
        label1241:
        m = 0;
        continue;
      }
      label1247:
      m += 1;
    }
  }
  
  public static void searchfile(String paramString1, String paramString2)
  {
    do
    {
      try
      {
        if ((new File(paramString2).exists()) && (!paramString2.startsWith("/mnt/sdcard")))
        {
          localFile2 = new File(paramString2);
          return;
        }
        if ((!paramString2.startsWith("/mnt/sdcard")) && (!paramString2.startsWith("/sdcard"))) {
          break label380;
        }
        paramString1 = new ArrayList();
        paramString1.add("/mnt/sdcard/");
        paramString1.add("/storage/emulated/legacy/");
        paramString1.add("/storage/emulated/0/");
        paramString1.add("/storage/sdcard0/");
        paramString1.add("/storage/sdcard/");
        paramString1.add("/storage/sdcard1/");
        paramString1.add("/sdcard/");
        paramString1 = paramString1.iterator();
      }
      catch (FileNotFoundException paramString1)
      {
        for (;;)
        {
          try
          {
            String str;
            boolean bool;
            new File(str + "test.tmp").createNewFile();
            if (new File(str + "test.tmp").exists())
            {
              new File(str + "test.tmp").delete();
              if (localFile2.exists()) {
                break;
              }
              throw new FileNotFoundException();
              paramString1 = paramString1;
              addToLog("Error LP: " + paramString2 + " are not found!\n\nRun the application file to appear in the folder with the data.!\n");
              return;
            }
            localFile2 = new File("/figjvaja_papka");
          }
          catch (Exception localException)
          {
            localException.printStackTrace();
            localFile2 = new File("/figjvaja_papka");
          }
        }
      }
      catch (Exception paramString1)
      {
        addToLog("Exception e" + paramString1.toString());
        return;
      }
      if (!paramString1.hasNext()) {
        break;
      }
      str = (String)paramString1.next();
      localFile2 = new File(paramString2.replace("/mnt/sdcard/", str));
      bool = localFile2.exists();
    } while (!bool);
    label380:
    if ((paramString2.startsWith("/data/data/" + pkgName + "/shared_prefs/")) || (paramString2.startsWith("/dbdata/databases/" + pkgName + "/shared_prefs/")) || (paramString2.startsWith("/shared_prefs/")))
    {
      paramString1 = "";
      if (paramString2.startsWith("/data/data/" + pkgName + "/shared_prefs/")) {
        paramString1 = "/data/data/" + pkgName + "/shared_prefs/";
      }
      if (paramString2.startsWith("/dbdata/databases/" + pkgName + "/shared_prefs/")) {
        paramString1 = "/dbdata/databases/" + pkgName + "/shared_prefs/";
      }
      if (paramString2.startsWith("/shared_prefs/")) {
        paramString1 = "/shared_prefs/";
      }
      localFile2 = new File(paramString2.replace(paramString1, "/data/data/" + pkgName + "/shared_prefs/"));
      if (!localFile2.exists()) {
        localFile2 = new File(paramString2.replace(paramString1, "/dbdata/databases/" + pkgName + "/shared_prefs/"));
      }
      if (!localFile2.exists()) {
        throw new FileNotFoundException();
      }
    }
    else
    {
      localFile2 = new File("/data/data/" + paramString1 + paramString2);
      if (!localFile2.exists()) {
        localFile2 = new File("/mnt/asec/" + paramString1 + "-1" + paramString2);
      }
      if (!localFile2.exists()) {
        localFile2 = new File("/mnt/asec/" + paramString1 + "-2" + paramString2);
      }
      if (!localFile2.exists()) {
        localFile2 = new File("/mnt/asec/" + paramString1 + paramString2);
      }
      if (!localFile2.exists()) {
        localFile2 = new File("/sd-ext/data/" + paramString1 + paramString2);
      }
      if (!localFile2.exists()) {
        localFile2 = new File("/data/sdext2/" + paramString1 + paramString2);
      }
      if (!localFile2.exists()) {
        localFile2 = new File("/data/sdext1/" + paramString1 + paramString2);
      }
      if (!localFile2.exists()) {
        localFile2 = new File("/data/sdext/" + paramString1 + paramString2);
      }
      if (!localFile2.exists())
      {
        paramString1 = new Utils("fgh").findFile(new File("/data/data/" + paramString1), paramString2);
        if (!paramString1.equals("")) {
          localFile2 = new File(paramString1);
        }
      }
      if (!localFile2.exists()) {
        throw new FileNotFoundException();
      }
    }
  }
  
  public static ArrayList<File> searchlib(String paramString1, String paramString2, String paramString3)
  {
    ArrayList localArrayList = new ArrayList();
    for (;;)
    {
      Object localObject2;
      int i;
      try
      {
        localObject2 = new ArrayList();
        ((ArrayList)localObject2).add("/data/data/");
        ((ArrayList)localObject2).add("/mnt/asec/");
        ((ArrayList)localObject2).add("/sd-ext/data/");
        ((ArrayList)localObject2).add("/data/sdext2/");
        ((ArrayList)localObject2).add("/data/sdext1/");
        ((ArrayList)localObject2).add("/data/sdext/");
        localObject1 = new ArrayList();
        ((ArrayList)localObject1).add(paramString1);
        ((ArrayList)localObject1).add(paramString1 + "-1");
        ((ArrayList)localObject1).add(paramString1 + "-2");
        if (!paramString2.trim().equals("*")) {
          break label793;
        }
        multilib_patch = true;
        paramString2 = new File("/data/data/" + paramString1 + "/lib/").listFiles();
        int j;
        if ((paramString2 != null) && (paramString2.length > 0))
        {
          j = paramString2.length;
          i = 0;
          if (i < j)
          {
            localObject1 = paramString2[i];
            if ((((File)localObject1).length() <= 0L) || (!((File)localObject1).getName().endsWith(".so"))) {
              break label1416;
            }
            Utils.addFileToList((File)localObject1, localArrayList);
            break label1416;
          }
        }
        paramString2 = new File(paramString3).getName().replace(paramString1, "").replace(".apk", "");
        if (new File("/data/app-lib").exists())
        {
          paramString3 = new File("/data/app-lib/" + paramString1 + paramString2 + "/").listFiles();
          if ((paramString3 != null) && (paramString3.length > 0))
          {
            j = paramString3.length;
            i = 0;
            if (i < j)
            {
              localObject1 = paramString3[i];
              if ((((File)localObject1).length() <= 0L) || (!((File)localObject1).getName().endsWith(".so"))) {
                break label1423;
              }
              Utils.addFileToList((File)localObject1, localArrayList);
              break label1423;
            }
          }
        }
        localObject1 = ((ArrayList)localObject2).iterator();
      }
      catch (FileNotFoundException paramString1)
      {
        addToLog("Error LP: " + localFile2 + " are not found!\n\nCheck the location libraries to solve problems!\n");
        paramString3 = null;
        return paramString3;
        localObject2 = new File(paramString3 + paramString1 + "/");
        if (!((File)localObject2).exists()) {
          continue;
        }
        localObject3 = new Utils("sdf");
        paramString3 = new ArrayList();
        localObject2 = ((Utils)localObject3).findFileEndText((File)localObject2, ".so", paramString3);
        if ((((String)localObject2).equals("")) || (paramString3.size() <= 0)) {
          continue;
        }
        paramString3 = paramString3.iterator();
        if (!paramString3.hasNext()) {
          continue;
        }
        Utils.addFileToList((File)paramString3.next(), localArrayList);
        addToLog("Found lib:" + (String)localObject2);
        continue;
      }
      catch (Exception paramString1)
      {
        addToLog("Exception e" + paramString1.toString());
        return null;
      }
      paramString3 = localArrayList;
      Object localObject3;
      Object localObject4;
      if (((Iterator)localObject1).hasNext())
      {
        paramString3 = (String)((Iterator)localObject1).next();
        localObject3 = new File(paramString3 + paramString1 + paramString2 + "/");
        if (!((File)localObject3).exists()) {
          continue;
        }
        localObject4 = new Utils("sdf");
        localObject2 = new ArrayList();
        localObject3 = ((Utils)localObject4).findFileEndText((File)localObject3, ".so", (ArrayList)localObject2);
        if ((((String)localObject3).equals("")) || (((ArrayList)localObject2).size() <= 0)) {
          continue;
        }
        localObject2 = ((ArrayList)localObject2).iterator();
        if (!((Iterator)localObject2).hasNext()) {
          continue;
        }
        Utils.addFileToList((File)((Iterator)localObject2).next(), localArrayList);
        addToLog("Found lib:" + (String)localObject3);
        continue;
      }
      label793:
      localFile2 = new File("/data/data/" + paramString1 + "/lib/" + paramString2);
      if (!localFile2.exists())
      {
        localObject2 = ((ArrayList)localObject2).iterator();
        while (((Iterator)localObject2).hasNext())
        {
          localObject3 = (String)((Iterator)localObject2).next();
          localObject4 = ((ArrayList)localObject1).iterator();
          while (((Iterator)localObject4).hasNext())
          {
            Object localObject5 = (String)((Iterator)localObject4).next();
            localObject5 = new File((String)localObject3 + (String)localObject5);
            if (((File)localObject5).exists())
            {
              localObject5 = new Utils("sdf").findFile((File)localObject5, paramString2);
              if (!((String)localObject5).equals(""))
              {
                localFile2 = new File((String)localObject5);
                Utils.addFileToList(localFile2, localArrayList);
                addToLog("Found lib:" + (String)localObject5);
              }
            }
          }
        }
      }
      Utils.addFileToList(localFile2, localArrayList);
      Object localObject1 = Utils.getDirs(new File(paramString3));
      if (new File(((File)localObject1).getAbsoluteFile() + "/lib").exists())
      {
        localFile2 = new File(((File)localObject1).getAbsoluteFile() + "/lib/arm/" + paramString2);
        if (localFile2.exists()) {
          Utils.addFileToList(localFile2, localArrayList);
        }
        localFile2 = new File(((File)localObject1).getAbsoluteFile() + "/lib/x86/" + paramString2);
        if (localFile2.exists()) {
          Utils.addFileToList(localFile2, localArrayList);
        }
        localFile2 = new File(((File)localObject1).getAbsoluteFile() + "/lib/mips/" + paramString2);
        if (localFile2.exists()) {
          Utils.addFileToList(localFile2, localArrayList);
        }
      }
      if (!localFile2.exists()) {
        localFile2 = new File("/system/lib/" + paramString2);
      }
      if (localArrayList.size() == 0) {
        throw new FileNotFoundException();
      }
      localObject1 = new File(paramString3).getName().replace(paramString1, "").replace(".apk", "");
      paramString3 = localArrayList;
      if (new File("/data/app-lib/" + paramString1 + (String)localObject1 + "/" + paramString2).exists())
      {
        Utils.addFileToList(new File("/data/app-lib/" + paramString1 + (String)localObject1 + "/" + paramString2), localArrayList);
        return localArrayList;
        label1416:
        i += 1;
        continue;
        label1423:
        i += 1;
      }
    }
  }
  
  public static void unzip(File paramFile)
  {
    classesFiles.clear();
    i = 0;
    try
    {
      FileInputStream localFileInputStream = new FileInputStream(paramFile);
      ZipInputStream localZipInputStream = new ZipInputStream(localFileInputStream);
      Object localObject1 = localZipInputStream.getNextEntry();
      for (;;)
      {
        Object localObject3;
        if ((localObject1 != null) && (1 != 0))
        {
          localObject1 = ((ZipEntry)localObject1).getName();
          if ((((String)localObject1).toLowerCase().startsWith("classes")) && (((String)localObject1).endsWith(".dex")) && (!((String)localObject1).contains("/")))
          {
            localObject3 = new FileOutputStream(dir + "/" + (String)localObject1);
            byte[] arrayOfByte = new byte['ࠀ'];
            for (;;)
            {
              int j = localZipInputStream.read(arrayOfByte);
              if (j == -1) {
                break;
              }
              ((FileOutputStream)localObject3).write(arrayOfByte, 0, j);
            }
          }
        }
        try
        {
          paramFile = new ZipFile(paramFile);
          paramFile.extractFile("classes.dex", dir);
          Utils.run_all_no_root(new String[] { "chmod", "777", dir + "/" + "classes.dex" });
          classesFiles.add(new File(dir + "/" + "classes.dex"));
          paramFile.extractFile("AndroidManifest.xml", dir);
          Utils.run_all_no_root(new String[] { "chmod", "777", dir + "/" + "AndroidManifest.xml" });
          addToLog("Exception e" + localException.toString());
          return;
          localZipInputStream.closeEntry();
          ((FileOutputStream)localObject3).close();
          classesFiles.add(new File(dir + "/" + localException));
          Utils.run_all_no_root(new String[] { "chmod", "777", dir + "/" + localException });
          if (localException.equals("AndroidManifest.xml"))
          {
            localObject2 = new FileOutputStream(dir + "/" + "AndroidManifest.xml");
            localObject3 = new byte['ࠀ'];
            for (;;)
            {
              i = localZipInputStream.read((byte[])localObject3);
              if (i == -1) {
                break;
              }
              ((FileOutputStream)localObject2).write((byte[])localObject3, 0, i);
            }
            localZipInputStream.closeEntry();
            ((FileOutputStream)localObject2).close();
            Utils.run_all_no_root(new String[] { "chmod", "777", dir + "/" + "AndroidManifest.xml" });
            i = 1;
            break label715;
            localZipInputStream.close();
            localFileInputStream.close();
            return;
            localObject2 = localZipInputStream.getNextEntry();
          }
        }
        catch (ZipException paramFile)
        {
          for (;;)
          {
            paramFile.printStackTrace();
            addToLog("Error LP: Error classes.dex decompress! " + paramFile);
            addToLog("Exception e1" + ((Exception)localObject2).toString());
          }
        }
        catch (Exception paramFile)
        {
          for (;;)
          {
            Object localObject2;
            paramFile.printStackTrace();
            addToLog("Error LP: Error classes.dex decompress! " + paramFile);
            addToLog("Exception e1" + ((Exception)localObject2).toString());
            continue;
            if ((0 == 0) || (i == 0)) {}
          }
        }
      }
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/chelpus/root/utils/custompatch.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */