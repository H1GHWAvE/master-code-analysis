package com.chelpus.root.utils;

import com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem;
import com.android.vending.billing.InAppBillingService.LUCK.PatchesItem;
import com.android.vending.billing.InAppBillingService.LUCK.SearchItem;
import com.chelpus.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.nio.BufferUnderflowException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.Adler32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;

public class createapkcustom
{
  static final int BUFFER = 2048;
  private static final int all = 4;
  public static String appdir;
  private static final int armeabi = 0;
  private static final int armeabiv7a = 1;
  private static final int beginTag = 0;
  public static ArrayList<File> classesFiles;
  private static final int classesTag = 1;
  public static File crkapk;
  public static String dir;
  public static String dir2;
  private static final int endTag = 4;
  private static final int fileInApkTag = 10;
  public static boolean goodResult = false;
  private static String group;
  private static final int libTagALL = 2;
  private static final int libTagARMEABI = 6;
  private static final int libTagARMEABIV7A = 7;
  private static final int libTagMIPS = 8;
  private static final int libTagx86 = 9;
  private static ArrayList<String> libs;
  public static File localFile2;
  public static boolean manualpatch = false;
  private static final int mips = 2;
  public static boolean multidex = false;
  public static boolean multilib_patch = false;
  public static String packageName;
  private static final int packageTag = 5;
  private static ArrayList<PatchesItem> pat = null;
  private static ArrayList<String> patchedLibs;
  public static boolean patchteil = false;
  private static PrintStream print;
  public static String sddir;
  private static ArrayList<Byte> search;
  private static ArrayList<SearchItem> ser = null;
  public static int tag = 0;
  public static String tooldir;
  public static boolean unpack = false;
  private static final int x86 = 3;
  
  static
  {
    search = null;
    patchteil = false;
    unpack = false;
    manualpatch = false;
    dir = "/sdcard/";
    dir2 = "/sdcard/";
    sddir = "/sdcard/";
    appdir = "/sdcard/";
    tooldir = "/sdcard/";
    packageName = "";
    libs = new ArrayList();
    patchedLibs = new ArrayList();
    group = "";
    classesFiles = new ArrayList();
    multidex = false;
  }
  
  private static final void calcChecksum(byte[] paramArrayOfByte, int paramInt)
  {
    Adler32 localAdler32 = new Adler32();
    localAdler32.update(paramArrayOfByte, 12, paramArrayOfByte.length - (paramInt + 12));
    int i = (int)localAdler32.getValue();
    paramArrayOfByte[(paramInt + 8)] = ((byte)i);
    paramArrayOfByte[(paramInt + 9)] = ((byte)(i >> 8));
    paramArrayOfByte[(paramInt + 10)] = ((byte)(i >> 16));
    paramArrayOfByte[(paramInt + 11)] = ((byte)(i >> 24));
  }
  
  private static final void calcSignature(byte[] paramArrayOfByte, int paramInt)
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-1");
      localMessageDigest.update(paramArrayOfByte, 32, paramArrayOfByte.length - (paramInt + 32));
      try
      {
        paramInt = localMessageDigest.digest(paramArrayOfByte, paramInt + 12, 20);
        if (paramInt != 20) {
          throw new RuntimeException("unexpected digest write:" + paramInt + "bytes");
        }
      }
      catch (DigestException paramArrayOfByte)
      {
        throw new RuntimeException(paramArrayOfByte);
      }
      return;
    }
    catch (NoSuchAlgorithmException paramArrayOfByte)
    {
      throw new RuntimeException(paramArrayOfByte);
    }
  }
  
  public static void clearTemp()
  {
    try
    {
      File localFile = new File(sddir + "/Modified/classes.dex.apk");
      if (localFile.exists()) {
        localFile.delete();
      }
      localFile = new File(sddir + "/tmp/");
      new Utils("createcustompatch").deleteFolder(localFile);
      return;
    }
    catch (Exception localException)
    {
      print.println("" + localException.toString());
    }
  }
  
  public static String extractFile(File paramFile, String paramString)
  {
    return new Decompress(paramFile.getAbsolutePath(), sddir + "/tmp/").unzip(paramString);
  }
  
  public static void extractLibs(File paramFile)
  {
    paramFile = paramFile.getAbsolutePath();
    String str = sddir + "/tmp/";
    if (!new File(sddir + "/tmp/lib/").exists()) {
      new Decompress(paramFile, str).unzip();
    }
  }
  
  public static void fixadler(File paramFile)
  {
    try
    {
      FileInputStream localFileInputStream = new FileInputStream(paramFile);
      byte[] arrayOfByte = new byte[localFileInputStream.available()];
      localFileInputStream.read(arrayOfByte);
      calcSignature(arrayOfByte, 0);
      calcChecksum(arrayOfByte, 0);
      localFileInputStream.close();
      paramFile = new FileOutputStream(paramFile);
      paramFile.write(arrayOfByte);
      paramFile.close();
      return;
    }
    catch (Exception paramFile)
    {
      paramFile.printStackTrace();
    }
  }
  
  public static void getClassesDex()
  {
    try
    {
      File localFile = new File(appdir);
      crkapk = new File(sddir + "/Modified/" + packageName + ".apk");
      Utils.copyFile(localFile, crkapk);
      unzip(crkapk);
      if ((classesFiles == null) || (classesFiles.size() == 0)) {
        throw new FileNotFoundException();
      }
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      print.println("Error LP: unzip classes.dex fault!\n\n");
      Iterator localIterator;
      do
      {
        do
        {
          return;
          while ((classesFiles == null) || (classesFiles.size() <= 0)) {}
          localIterator = classesFiles.iterator();
        } while (!localIterator.hasNext());
      } while (((File)localIterator.next()).exists());
      throw new FileNotFoundException();
    }
    catch (Exception localException)
    {
      print.println("Extract classes.dex error: " + localException.toString());
    }
  }
  
  public static String getFileFromApk(String paramString)
  {
    try
    {
      File localFile = new File(appdir);
      crkapk = new File(sddir + "/Modified/" + packageName + ".apk");
      if (!crkapk.exists()) {
        Utils.copyFile(localFile, crkapk);
      }
      paramString = extractFile(crkapk, paramString);
      return paramString;
    }
    catch (Exception paramString)
    {
      print.println("Lib select error: " + paramString.toString());
    }
    return "";
  }
  
  /* Error */
  public static String main(String[] paramArrayOfString)
  {
    // Byte code:
    //   0: new 312	com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream
    //   3: dup
    //   4: ldc_w 314
    //   7: invokespecial 315	com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream:<init>	(Ljava/lang/String;)V
    //   10: astore 27
    //   12: new 210	java/io/PrintStream
    //   15: dup
    //   16: aload 27
    //   18: invokespecial 318	java/io/PrintStream:<init>	(Ljava/io/OutputStream;)V
    //   21: putstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   24: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   27: ldc_w 320
    //   30: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   33: getstatic 108	com/chelpus/root/utils/createapkcustom:patchedLibs	Ljava/util/ArrayList;
    //   36: invokevirtual 323	java/util/ArrayList:clear	()V
    //   39: aload_0
    //   40: iconst_0
    //   41: aaload
    //   42: putstatic 99	com/chelpus/root/utils/createapkcustom:packageName	Ljava/lang/String;
    //   45: aload_0
    //   46: iconst_2
    //   47: aaload
    //   48: putstatic 93	com/chelpus/root/utils/createapkcustom:appdir	Ljava/lang/String;
    //   51: aload_0
    //   52: iconst_3
    //   53: aaload
    //   54: putstatic 91	com/chelpus/root/utils/createapkcustom:sddir	Ljava/lang/String;
    //   57: aload_0
    //   58: iconst_4
    //   59: aaload
    //   60: putstatic 95	com/chelpus/root/utils/createapkcustom:tooldir	Ljava/lang/String;
    //   63: invokestatic 325	com/chelpus/root/utils/createapkcustom:clearTemp	()V
    //   66: iconst_0
    //   67: putstatic 83	com/chelpus/root/utils/createapkcustom:manualpatch	Z
    //   70: ldc 97
    //   72: astore 23
    //   74: ldc 97
    //   76: astore 26
    //   78: iconst_0
    //   79: istore_2
    //   80: iconst_0
    //   81: istore 10
    //   83: invokestatic 327	com/chelpus/root/utils/createapkcustom:getClassesDex	()V
    //   86: new 233	java/io/FileInputStream
    //   89: dup
    //   90: aload_0
    //   91: iconst_1
    //   92: aaload
    //   93: invokespecial 328	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   96: astore 28
    //   98: new 330	java/io/BufferedReader
    //   101: dup
    //   102: new 332	java/io/InputStreamReader
    //   105: dup
    //   106: aload 28
    //   108: ldc_w 334
    //   111: invokespecial 337	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/lang/String;)V
    //   114: invokespecial 340	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   117: astore 29
    //   119: sipush 1000
    //   122: anewarray 342	java/lang/String
    //   125: astore 30
    //   127: aconst_null
    //   128: astore 21
    //   130: aconst_null
    //   131: astore 22
    //   133: iconst_1
    //   134: istore 12
    //   136: iconst_1
    //   137: istore 13
    //   139: iconst_0
    //   140: istore 5
    //   142: iconst_0
    //   143: istore 4
    //   145: iconst_0
    //   146: istore_3
    //   147: iconst_0
    //   148: istore 6
    //   150: ldc 97
    //   152: astore 18
    //   154: ldc 97
    //   156: astore 16
    //   158: ldc 97
    //   160: astore 17
    //   162: new 101	java/util/ArrayList
    //   165: dup
    //   166: invokespecial 104	java/util/ArrayList:<init>	()V
    //   169: putstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   172: new 101	java/util/ArrayList
    //   175: dup
    //   176: invokespecial 104	java/util/ArrayList:<init>	()V
    //   179: putstatic 75	com/chelpus/root/utils/createapkcustom:ser	Ljava/util/ArrayList;
    //   182: new 101	java/util/ArrayList
    //   185: dup
    //   186: invokespecial 104	java/util/ArrayList:<init>	()V
    //   189: putstatic 77	com/chelpus/root/utils/createapkcustom:search	Ljava/util/ArrayList;
    //   192: iconst_0
    //   193: istore 9
    //   195: aload 29
    //   197: invokevirtual 345	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   200: astore 19
    //   202: aload 19
    //   204: ifnull +4967 -> 5171
    //   207: aload 19
    //   209: astore 24
    //   211: aload 19
    //   213: ldc 97
    //   215: invokevirtual 349	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   218: ifne +13 -> 231
    //   221: aload 19
    //   223: getstatic 99	com/chelpus/root/utils/createapkcustom:packageName	Ljava/lang/String;
    //   226: invokestatic 353	com/chelpus/Utils:apply_TAGS	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   229: astore 24
    //   231: aload 30
    //   233: iload 9
    //   235: aload 24
    //   237: aastore
    //   238: iload 4
    //   240: istore_1
    //   241: iload 4
    //   243: ifeq +82 -> 325
    //   246: aload 30
    //   248: iload 9
    //   250: aaload
    //   251: ldc_w 355
    //   254: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   257: ifne +34 -> 291
    //   260: aload 30
    //   262: iload 9
    //   264: aaload
    //   265: ldc_w 361
    //   268: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   271: ifne +20 -> 291
    //   274: iload 4
    //   276: istore_1
    //   277: aload 30
    //   279: iload 9
    //   281: aaload
    //   282: ldc_w 363
    //   285: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   288: ifeq +37 -> 325
    //   291: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   294: new 159	java/lang/StringBuilder
    //   297: dup
    //   298: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   301: ldc 97
    //   303: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   306: aload 26
    //   308: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   311: ldc_w 365
    //   314: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   317: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   320: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   323: iconst_0
    //   324: istore_1
    //   325: aload 26
    //   327: astore 25
    //   329: iload_1
    //   330: ifeq +34 -> 364
    //   333: new 159	java/lang/StringBuilder
    //   336: dup
    //   337: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   340: aload 26
    //   342: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   345: ldc_w 365
    //   348: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   351: aload 30
    //   353: iload 9
    //   355: aaload
    //   356: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   359: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   362: astore 25
    //   364: iload 12
    //   366: istore 15
    //   368: iload 13
    //   370: istore 14
    //   372: aload 30
    //   374: iload 9
    //   376: aaload
    //   377: ldc_w 355
    //   380: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   383: ifeq +89 -> 472
    //   386: iload 12
    //   388: istore 15
    //   390: iload 13
    //   392: istore 14
    //   394: aload 30
    //   396: iload 9
    //   398: aaload
    //   399: ldc_w 361
    //   402: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   405: ifeq +67 -> 472
    //   408: iload 12
    //   410: istore 15
    //   412: iload 13
    //   414: istore 14
    //   416: getstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   419: tableswitch	default:+4850->5269, 1:+893->1312, 2:+1302->1721, 3:+53->472, 4:+53->472, 5:+53->472, 6:+1500->1919, 7:+1690->2109, 8:+1880->2299, 9:+2070->2489, 10:+1131->1550
    //   472: iload_1
    //   473: istore 4
    //   475: aload 30
    //   477: iload 9
    //   479: aaload
    //   480: ldc_w 369
    //   483: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   486: ifeq +10 -> 496
    //   489: iconst_0
    //   490: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   493: iconst_1
    //   494: istore 4
    //   496: aload 30
    //   498: iload 9
    //   500: aaload
    //   501: ldc_w 371
    //   504: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   507: ifne +17 -> 524
    //   510: aload 30
    //   512: iload 9
    //   514: aaload
    //   515: ldc_w 373
    //   518: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   521: ifeq +7 -> 528
    //   524: iconst_1
    //   525: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   528: aload 30
    //   530: iload 9
    //   532: aaload
    //   533: ldc_w 375
    //   536: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   539: ifeq +7 -> 546
    //   542: iconst_5
    //   543: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   546: iload 5
    //   548: istore_1
    //   549: aload 18
    //   551: astore 19
    //   553: iload 5
    //   555: ifeq +89 -> 644
    //   558: getstatic 75	com/chelpus/root/utils/createapkcustom:ser	Ljava/util/ArrayList;
    //   561: invokevirtual 323	java/util/ArrayList:clear	()V
    //   564: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   567: invokevirtual 323	java/util/ArrayList:clear	()V
    //   570: new 377	org/json/JSONObject
    //   573: dup
    //   574: aload 30
    //   576: iload 9
    //   578: aaload
    //   579: invokespecial 378	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   582: ldc_w 380
    //   585: invokevirtual 383	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   588: astore 19
    //   590: aload 19
    //   592: astore 18
    //   594: getstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   597: tableswitch	default:+4683->5280, 2:+2096->2693, 3:+4683->5280, 4:+4683->5280, 5:+4683->5280, 6:+2114->2711, 7:+2132->2729, 8:+2150->2747, 9:+2168->2765
    //   644: iload 6
    //   646: istore 5
    //   648: aload 19
    //   650: astore 18
    //   652: iload 6
    //   654: ifeq +72 -> 726
    //   657: getstatic 75	com/chelpus/root/utils/createapkcustom:ser	Ljava/util/ArrayList;
    //   660: invokevirtual 323	java/util/ArrayList:clear	()V
    //   663: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   666: invokevirtual 323	java/util/ArrayList:clear	()V
    //   669: new 377	org/json/JSONObject
    //   672: dup
    //   673: aload 30
    //   675: iload 9
    //   677: aaload
    //   678: invokespecial 378	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   681: ldc_w 380
    //   684: invokevirtual 383	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   687: astore 18
    //   689: aload 18
    //   691: invokestatic 385	com/chelpus/root/utils/createapkcustom:getFileFromApk	(Ljava/lang/String;)Ljava/lang/String;
    //   694: astore 19
    //   696: new 186	java/io/File
    //   699: dup
    //   700: aload 19
    //   702: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   705: invokevirtual 193	java/io/File:exists	()Z
    //   708: ifeq +2093 -> 2801
    //   711: new 186	java/io/File
    //   714: dup
    //   715: aload 19
    //   717: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   720: putstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   723: goto +4566 -> 5289
    //   726: aload 30
    //   728: iload 9
    //   730: aaload
    //   731: ldc_w 389
    //   734: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   737: ifeq +17 -> 754
    //   740: bipush 6
    //   742: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   745: iconst_0
    //   746: putstatic 81	com/chelpus/root/utils/createapkcustom:unpack	Z
    //   749: iconst_0
    //   750: istore 5
    //   752: iconst_1
    //   753: istore_1
    //   754: aload 30
    //   756: iload 9
    //   758: aaload
    //   759: ldc_w 391
    //   762: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   765: ifeq +17 -> 782
    //   768: bipush 7
    //   770: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   773: iconst_0
    //   774: putstatic 81	com/chelpus/root/utils/createapkcustom:unpack	Z
    //   777: iconst_0
    //   778: istore 5
    //   780: iconst_1
    //   781: istore_1
    //   782: aload 30
    //   784: iload 9
    //   786: aaload
    //   787: ldc_w 393
    //   790: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   793: ifeq +17 -> 810
    //   796: bipush 8
    //   798: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   801: iconst_0
    //   802: putstatic 81	com/chelpus/root/utils/createapkcustom:unpack	Z
    //   805: iconst_0
    //   806: istore 5
    //   808: iconst_1
    //   809: istore_1
    //   810: aload 30
    //   812: iload 9
    //   814: aaload
    //   815: ldc_w 395
    //   818: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   821: ifeq +17 -> 838
    //   824: bipush 9
    //   826: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   829: iconst_0
    //   830: putstatic 81	com/chelpus/root/utils/createapkcustom:unpack	Z
    //   833: iconst_0
    //   834: istore 5
    //   836: iconst_1
    //   837: istore_1
    //   838: aload 30
    //   840: iload 9
    //   842: aaload
    //   843: ldc_w 397
    //   846: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   849: ifeq +16 -> 865
    //   852: iconst_2
    //   853: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   856: iconst_0
    //   857: putstatic 81	com/chelpus/root/utils/createapkcustom:unpack	Z
    //   860: iconst_0
    //   861: istore 5
    //   863: iconst_1
    //   864: istore_1
    //   865: iload 5
    //   867: istore 6
    //   869: iload_1
    //   870: istore 5
    //   872: aload 30
    //   874: iload 9
    //   876: aaload
    //   877: ldc_w 399
    //   880: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   883: ifeq +18 -> 901
    //   886: bipush 10
    //   888: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   891: iconst_0
    //   892: putstatic 81	com/chelpus/root/utils/createapkcustom:unpack	Z
    //   895: iconst_0
    //   896: istore 5
    //   898: iconst_1
    //   899: istore 6
    //   901: aload 30
    //   903: iload 9
    //   905: aaload
    //   906: ldc_w 400
    //   909: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   912: istore 12
    //   914: iload 12
    //   916: ifeq +24 -> 940
    //   919: new 377	org/json/JSONObject
    //   922: dup
    //   923: aload 30
    //   925: iload 9
    //   927: aaload
    //   928: invokespecial 378	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   931: ldc_w 400
    //   934: invokevirtual 383	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   937: putstatic 110	com/chelpus/root/utils/createapkcustom:group	Ljava/lang/String;
    //   940: iload_2
    //   941: istore_1
    //   942: iload_3
    //   943: istore 7
    //   945: iload 14
    //   947: istore 12
    //   949: aload 18
    //   951: astore 19
    //   953: aload 30
    //   955: iload 9
    //   957: aaload
    //   958: ldc_w 402
    //   961: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   964: ifeq +1942 -> 2906
    //   967: iload_3
    //   968: istore 8
    //   970: iload_3
    //   971: ifeq +14 -> 985
    //   974: getstatic 75	com/chelpus/root/utils/createapkcustom:ser	Ljava/util/ArrayList;
    //   977: invokestatic 406	com/chelpus/root/utils/createapkcustom:searchProcess	(Ljava/util/ArrayList;)Z
    //   980: istore 14
    //   982: iconst_0
    //   983: istore 8
    //   985: new 377	org/json/JSONObject
    //   988: dup
    //   989: aload 30
    //   991: iload 9
    //   993: aaload
    //   994: invokespecial 378	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   997: ldc_w 402
    //   1000: invokevirtual 383	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   1003: astore 19
    //   1005: aload 19
    //   1007: astore 18
    //   1009: aload 18
    //   1011: invokevirtual 409	java/lang/String:trim	()Ljava/lang/String;
    //   1014: astore 18
    //   1016: aload 18
    //   1018: ldc_w 411
    //   1021: invokevirtual 415	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   1024: arraylength
    //   1025: anewarray 342	java/lang/String
    //   1028: astore 19
    //   1030: aload 18
    //   1032: ldc_w 411
    //   1035: invokevirtual 415	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   1038: astore 31
    //   1040: aload 31
    //   1042: arraylength
    //   1043: newarray <illegal type>
    //   1045: astore 20
    //   1047: aload 31
    //   1049: arraylength
    //   1050: newarray <illegal type>
    //   1052: astore 26
    //   1054: iconst_0
    //   1055: istore 11
    //   1057: iload_2
    //   1058: istore_3
    //   1059: aload 26
    //   1061: astore 21
    //   1063: aload 20
    //   1065: astore 22
    //   1067: iload_2
    //   1068: istore_1
    //   1069: iload 8
    //   1071: istore 7
    //   1073: iload 14
    //   1075: istore 12
    //   1077: aload 18
    //   1079: astore 19
    //   1081: iload 11
    //   1083: aload 31
    //   1085: arraylength
    //   1086: if_icmpge +1820 -> 2906
    //   1089: iload_2
    //   1090: istore_1
    //   1091: iload_2
    //   1092: istore_3
    //   1093: aload 31
    //   1095: iload 11
    //   1097: aaload
    //   1098: ldc_w 417
    //   1101: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1104: ifeq +31 -> 1135
    //   1107: iload_2
    //   1108: istore_1
    //   1109: iload_2
    //   1110: istore_3
    //   1111: aload 31
    //   1113: iload 11
    //   1115: aaload
    //   1116: ldc_w 419
    //   1119: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1122: ifne +13 -> 1135
    //   1125: iconst_1
    //   1126: istore_1
    //   1127: aload 31
    //   1129: iload 11
    //   1131: ldc_w 421
    //   1134: aastore
    //   1135: iload_1
    //   1136: istore_3
    //   1137: aload 31
    //   1139: iload 11
    //   1141: aaload
    //   1142: ldc_w 419
    //   1145: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1148: ifne +4147 -> 5295
    //   1151: iload_1
    //   1152: istore_3
    //   1153: aload 31
    //   1155: iload 11
    //   1157: aaload
    //   1158: ldc_w 423
    //   1161: invokevirtual 427	java/lang/String:matches	(Ljava/lang/String;)Z
    //   1164: ifeq +1682 -> 2846
    //   1167: goto +4128 -> 5295
    //   1170: iload_1
    //   1171: istore_3
    //   1172: aload 31
    //   1174: iload 11
    //   1176: aaload
    //   1177: ldc_w 429
    //   1180: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1183: ifne +51 -> 1234
    //   1186: iload_1
    //   1187: istore_3
    //   1188: aload 31
    //   1190: iload 11
    //   1192: aaload
    //   1193: ldc_w 431
    //   1196: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1199: ifne +35 -> 1234
    //   1202: iload_1
    //   1203: istore_3
    //   1204: aload 31
    //   1206: iload 11
    //   1208: aaload
    //   1209: ldc_w 433
    //   1212: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1215: ifne +19 -> 1234
    //   1218: iload_1
    //   1219: istore_3
    //   1220: aload 31
    //   1222: iload 11
    //   1224: aaload
    //   1225: ldc_w 435
    //   1228: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   1231: ifeq +50 -> 1281
    //   1234: iload_1
    //   1235: istore_3
    //   1236: aload 20
    //   1238: iload 11
    //   1240: aload 31
    //   1242: iload 11
    //   1244: aaload
    //   1245: invokevirtual 438	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   1248: ldc_w 431
    //   1251: ldc 97
    //   1253: invokevirtual 442	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   1256: ldc_w 435
    //   1259: ldc 97
    //   1261: invokevirtual 442	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   1264: invokestatic 448	java/lang/Integer:valueOf	(Ljava/lang/String;)Ljava/lang/Integer;
    //   1267: invokevirtual 451	java/lang/Integer:intValue	()I
    //   1270: iconst_2
    //   1271: iadd
    //   1272: iastore
    //   1273: aload 31
    //   1275: iload 11
    //   1277: ldc_w 421
    //   1280: aastore
    //   1281: iload_1
    //   1282: istore_3
    //   1283: aload 26
    //   1285: iload 11
    //   1287: aload 31
    //   1289: iload 11
    //   1291: aaload
    //   1292: bipush 16
    //   1294: invokestatic 454	java/lang/Integer:valueOf	(Ljava/lang/String;I)Ljava/lang/Integer;
    //   1297: invokevirtual 458	java/lang/Integer:byteValue	()B
    //   1300: bastore
    //   1301: iload 11
    //   1303: iconst_1
    //   1304: iadd
    //   1305: istore 11
    //   1307: iload_1
    //   1308: istore_2
    //   1309: goto -252 -> 1057
    //   1312: iload 12
    //   1314: istore 15
    //   1316: iload 13
    //   1318: istore 14
    //   1320: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   1323: invokevirtual 279	java/util/ArrayList:size	()I
    //   1326: ifle -854 -> 472
    //   1329: invokestatic 327	com/chelpus/root/utils/createapkcustom:getClassesDex	()V
    //   1332: iload 12
    //   1334: istore 15
    //   1336: iload 13
    //   1338: istore 14
    //   1340: getstatic 112	com/chelpus/root/utils/createapkcustom:classesFiles	Ljava/util/ArrayList;
    //   1343: ifnull +152 -> 1495
    //   1346: iload 12
    //   1348: istore 15
    //   1350: iload 13
    //   1352: istore 14
    //   1354: getstatic 112	com/chelpus/root/utils/createapkcustom:classesFiles	Ljava/util/ArrayList;
    //   1357: invokevirtual 279	java/util/ArrayList:size	()I
    //   1360: ifle +135 -> 1495
    //   1363: getstatic 112	com/chelpus/root/utils/createapkcustom:classesFiles	Ljava/util/ArrayList;
    //   1366: invokevirtual 279	java/util/ArrayList:size	()I
    //   1369: iconst_1
    //   1370: if_icmple +7 -> 1377
    //   1373: iconst_1
    //   1374: putstatic 114	com/chelpus/root/utils/createapkcustom:multidex	Z
    //   1377: getstatic 112	com/chelpus/root/utils/createapkcustom:classesFiles	Ljava/util/ArrayList;
    //   1380: invokevirtual 286	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   1383: astore 19
    //   1385: iload 12
    //   1387: istore 15
    //   1389: iload 13
    //   1391: istore 14
    //   1393: aload 19
    //   1395: invokeinterface 291 1 0
    //   1400: ifeq +95 -> 1495
    //   1403: aload 19
    //   1405: invokeinterface 295 1 0
    //   1410: checkcast 186	java/io/File
    //   1413: astore 20
    //   1415: aload 20
    //   1417: putstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   1420: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1423: ldc_w 460
    //   1426: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1429: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1432: new 159	java/lang/StringBuilder
    //   1435: dup
    //   1436: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   1439: ldc_w 462
    //   1442: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1445: aload 20
    //   1447: invokevirtual 465	java/io/File:getName	()Ljava/lang/String;
    //   1450: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1453: ldc_w 467
    //   1456: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1459: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1462: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1465: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1468: ldc_w 469
    //   1471: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1474: iload 12
    //   1476: istore 14
    //   1478: getstatic 83	com/chelpus/root/utils/createapkcustom:manualpatch	Z
    //   1481: ifne +3831 -> 5312
    //   1484: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   1487: invokestatic 472	com/chelpus/root/utils/createapkcustom:patchProcess	(Ljava/util/ArrayList;)Z
    //   1490: istore 14
    //   1492: goto +3820 -> 5312
    //   1495: iconst_0
    //   1496: putstatic 114	com/chelpus/root/utils/createapkcustom:multidex	Z
    //   1499: iconst_0
    //   1500: putstatic 116	com/chelpus/root/utils/createapkcustom:goodResult	Z
    //   1503: getstatic 75	com/chelpus/root/utils/createapkcustom:ser	Ljava/util/ArrayList;
    //   1506: invokevirtual 323	java/util/ArrayList:clear	()V
    //   1509: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   1512: invokevirtual 323	java/util/ArrayList:clear	()V
    //   1515: sipush 200
    //   1518: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   1521: goto -1049 -> 472
    //   1524: astore_0
    //   1525: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1528: ldc_w 474
    //   1531: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1534: invokestatic 325	com/chelpus/root/utils/createapkcustom:clearTemp	()V
    //   1537: aload 27
    //   1539: getfield 477	com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream:allresult	Ljava/lang/String;
    //   1542: astore_0
    //   1543: aload 27
    //   1545: invokevirtual 478	com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream:close	()V
    //   1548: aload_0
    //   1549: areturn
    //   1550: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1553: ldc_w 480
    //   1556: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1559: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1562: new 159	java/lang/StringBuilder
    //   1565: dup
    //   1566: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   1569: ldc_w 482
    //   1572: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1575: getstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   1578: invokevirtual 485	java/io/File:getPath	()Ljava/lang/String;
    //   1581: new 159	java/lang/StringBuilder
    //   1584: dup
    //   1585: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   1588: getstatic 91	com/chelpus/root/utils/createapkcustom:sddir	Ljava/lang/String;
    //   1591: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1594: ldc_w 487
    //   1597: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1600: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1603: ldc 97
    //   1605: invokevirtual 442	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   1608: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1611: ldc_w 467
    //   1614: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1617: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1620: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1623: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1626: ldc_w 489
    //   1629: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1632: getstatic 83	com/chelpus/root/utils/createapkcustom:manualpatch	Z
    //   1635: ifne +3696 -> 5331
    //   1638: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   1641: invokestatic 472	com/chelpus/root/utils/createapkcustom:patchProcess	(Ljava/util/ArrayList;)Z
    //   1644: istore 12
    //   1646: goto +3685 -> 5331
    //   1649: getstatic 108	com/chelpus/root/utils/createapkcustom:patchedLibs	Ljava/util/ArrayList;
    //   1652: getstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   1655: invokevirtual 218	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   1658: invokevirtual 492	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1661: pop
    //   1662: getstatic 75	com/chelpus/root/utils/createapkcustom:ser	Ljava/util/ArrayList;
    //   1665: invokevirtual 323	java/util/ArrayList:clear	()V
    //   1668: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   1671: invokevirtual 323	java/util/ArrayList:clear	()V
    //   1674: sipush 200
    //   1677: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   1680: iload 12
    //   1682: istore 15
    //   1684: iload 13
    //   1686: istore 14
    //   1688: goto -1216 -> 472
    //   1691: astore_0
    //   1692: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1695: new 159	java/lang/StringBuilder
    //   1698: dup
    //   1699: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   1702: ldc_w 494
    //   1705: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1708: aload_0
    //   1709: invokevirtual 497	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1712: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1715: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1718: goto -184 -> 1534
    //   1721: getstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   1724: invokevirtual 286	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   1727: astore 19
    //   1729: aload 19
    //   1731: invokeinterface 291 1 0
    //   1736: ifeq +146 -> 1882
    //   1739: aload 19
    //   1741: invokeinterface 295 1 0
    //   1746: checkcast 342	java/lang/String
    //   1749: astore 20
    //   1751: new 186	java/io/File
    //   1754: dup
    //   1755: aload 20
    //   1757: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   1760: putstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   1763: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1766: ldc_w 480
    //   1769: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1772: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1775: new 159	java/lang/StringBuilder
    //   1778: dup
    //   1779: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   1782: ldc_w 499
    //   1785: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1788: getstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   1791: invokevirtual 485	java/io/File:getPath	()Ljava/lang/String;
    //   1794: new 159	java/lang/StringBuilder
    //   1797: dup
    //   1798: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   1801: getstatic 91	com/chelpus/root/utils/createapkcustom:sddir	Ljava/lang/String;
    //   1804: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1807: ldc_w 487
    //   1810: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1813: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1816: ldc 97
    //   1818: invokevirtual 442	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   1821: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1824: ldc_w 467
    //   1827: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1830: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1833: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1836: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1839: ldc_w 489
    //   1842: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1845: getstatic 83	com/chelpus/root/utils/createapkcustom:manualpatch	Z
    //   1848: ifne +3494 -> 5342
    //   1851: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   1854: invokestatic 472	com/chelpus/root/utils/createapkcustom:patchProcess	(Ljava/util/ArrayList;)Z
    //   1857: istore 12
    //   1859: goto +3483 -> 5342
    //   1862: getstatic 108	com/chelpus/root/utils/createapkcustom:patchedLibs	Ljava/util/ArrayList;
    //   1865: aload 20
    //   1867: invokevirtual 492	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1870: pop
    //   1871: goto -142 -> 1729
    //   1874: astore_0
    //   1875: aload_0
    //   1876: invokevirtual 500	java/lang/InterruptedException:printStackTrace	()V
    //   1879: goto -345 -> 1534
    //   1882: iconst_0
    //   1883: putstatic 118	com/chelpus/root/utils/createapkcustom:multilib_patch	Z
    //   1886: iconst_0
    //   1887: putstatic 116	com/chelpus/root/utils/createapkcustom:goodResult	Z
    //   1890: getstatic 75	com/chelpus/root/utils/createapkcustom:ser	Ljava/util/ArrayList;
    //   1893: invokevirtual 323	java/util/ArrayList:clear	()V
    //   1896: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   1899: invokevirtual 323	java/util/ArrayList:clear	()V
    //   1902: sipush 200
    //   1905: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   1908: iload 12
    //   1910: istore 15
    //   1912: iload 13
    //   1914: istore 14
    //   1916: goto -1444 -> 472
    //   1919: getstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   1922: invokevirtual 286	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   1925: astore 19
    //   1927: aload 19
    //   1929: invokeinterface 291 1 0
    //   1934: ifeq +138 -> 2072
    //   1937: aload 19
    //   1939: invokeinterface 295 1 0
    //   1944: checkcast 342	java/lang/String
    //   1947: astore 20
    //   1949: new 186	java/io/File
    //   1952: dup
    //   1953: aload 20
    //   1955: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   1958: putstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   1961: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1964: ldc_w 502
    //   1967: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1970: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1973: new 159	java/lang/StringBuilder
    //   1976: dup
    //   1977: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   1980: ldc_w 504
    //   1983: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1986: getstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   1989: invokevirtual 485	java/io/File:getPath	()Ljava/lang/String;
    //   1992: new 159	java/lang/StringBuilder
    //   1995: dup
    //   1996: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   1999: getstatic 91	com/chelpus/root/utils/createapkcustom:sddir	Ljava/lang/String;
    //   2002: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2005: ldc_w 487
    //   2008: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2011: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2014: ldc 97
    //   2016: invokevirtual 442	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   2019: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2022: ldc_w 467
    //   2025: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2028: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2031: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2034: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2037: ldc_w 506
    //   2040: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2043: getstatic 83	com/chelpus/root/utils/createapkcustom:manualpatch	Z
    //   2046: ifne +3307 -> 5353
    //   2049: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   2052: invokestatic 472	com/chelpus/root/utils/createapkcustom:patchProcess	(Ljava/util/ArrayList;)Z
    //   2055: istore 12
    //   2057: goto +3296 -> 5353
    //   2060: getstatic 108	com/chelpus/root/utils/createapkcustom:patchedLibs	Ljava/util/ArrayList;
    //   2063: aload 20
    //   2065: invokevirtual 492	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2068: pop
    //   2069: goto -142 -> 1927
    //   2072: iconst_0
    //   2073: putstatic 118	com/chelpus/root/utils/createapkcustom:multilib_patch	Z
    //   2076: iconst_0
    //   2077: putstatic 116	com/chelpus/root/utils/createapkcustom:goodResult	Z
    //   2080: getstatic 75	com/chelpus/root/utils/createapkcustom:ser	Ljava/util/ArrayList;
    //   2083: invokevirtual 323	java/util/ArrayList:clear	()V
    //   2086: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   2089: invokevirtual 323	java/util/ArrayList:clear	()V
    //   2092: sipush 200
    //   2095: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   2098: iload 12
    //   2100: istore 15
    //   2102: iload 13
    //   2104: istore 14
    //   2106: goto -1634 -> 472
    //   2109: getstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   2112: invokevirtual 286	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   2115: astore 19
    //   2117: aload 19
    //   2119: invokeinterface 291 1 0
    //   2124: ifeq +138 -> 2262
    //   2127: aload 19
    //   2129: invokeinterface 295 1 0
    //   2134: checkcast 342	java/lang/String
    //   2137: astore 20
    //   2139: new 186	java/io/File
    //   2142: dup
    //   2143: aload 20
    //   2145: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   2148: putstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   2151: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2154: ldc_w 508
    //   2157: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2160: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2163: new 159	java/lang/StringBuilder
    //   2166: dup
    //   2167: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   2170: ldc_w 510
    //   2173: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2176: getstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   2179: invokevirtual 485	java/io/File:getPath	()Ljava/lang/String;
    //   2182: new 159	java/lang/StringBuilder
    //   2185: dup
    //   2186: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   2189: getstatic 91	com/chelpus/root/utils/createapkcustom:sddir	Ljava/lang/String;
    //   2192: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2195: ldc_w 487
    //   2198: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2201: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2204: ldc 97
    //   2206: invokevirtual 442	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   2209: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2212: ldc_w 467
    //   2215: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2218: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2221: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2224: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2227: ldc_w 512
    //   2230: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2233: getstatic 83	com/chelpus/root/utils/createapkcustom:manualpatch	Z
    //   2236: ifne +3128 -> 5364
    //   2239: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   2242: invokestatic 472	com/chelpus/root/utils/createapkcustom:patchProcess	(Ljava/util/ArrayList;)Z
    //   2245: istore 12
    //   2247: goto +3117 -> 5364
    //   2250: getstatic 108	com/chelpus/root/utils/createapkcustom:patchedLibs	Ljava/util/ArrayList;
    //   2253: aload 20
    //   2255: invokevirtual 492	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2258: pop
    //   2259: goto -142 -> 2117
    //   2262: iconst_0
    //   2263: putstatic 118	com/chelpus/root/utils/createapkcustom:multilib_patch	Z
    //   2266: iconst_0
    //   2267: putstatic 116	com/chelpus/root/utils/createapkcustom:goodResult	Z
    //   2270: getstatic 75	com/chelpus/root/utils/createapkcustom:ser	Ljava/util/ArrayList;
    //   2273: invokevirtual 323	java/util/ArrayList:clear	()V
    //   2276: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   2279: invokevirtual 323	java/util/ArrayList:clear	()V
    //   2282: sipush 200
    //   2285: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   2288: iload 12
    //   2290: istore 15
    //   2292: iload 13
    //   2294: istore 14
    //   2296: goto -1824 -> 472
    //   2299: getstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   2302: invokevirtual 286	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   2305: astore 19
    //   2307: aload 19
    //   2309: invokeinterface 291 1 0
    //   2314: ifeq +138 -> 2452
    //   2317: aload 19
    //   2319: invokeinterface 295 1 0
    //   2324: checkcast 342	java/lang/String
    //   2327: astore 20
    //   2329: new 186	java/io/File
    //   2332: dup
    //   2333: aload 20
    //   2335: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   2338: putstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   2341: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2344: ldc_w 480
    //   2347: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2350: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2353: new 159	java/lang/StringBuilder
    //   2356: dup
    //   2357: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   2360: ldc_w 514
    //   2363: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2366: getstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   2369: invokevirtual 485	java/io/File:getPath	()Ljava/lang/String;
    //   2372: new 159	java/lang/StringBuilder
    //   2375: dup
    //   2376: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   2379: getstatic 91	com/chelpus/root/utils/createapkcustom:sddir	Ljava/lang/String;
    //   2382: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2385: ldc_w 487
    //   2388: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2391: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2394: ldc 97
    //   2396: invokevirtual 442	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   2399: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2402: ldc_w 467
    //   2405: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2408: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2411: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2414: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2417: ldc_w 489
    //   2420: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2423: getstatic 83	com/chelpus/root/utils/createapkcustom:manualpatch	Z
    //   2426: ifne +2949 -> 5375
    //   2429: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   2432: invokestatic 472	com/chelpus/root/utils/createapkcustom:patchProcess	(Ljava/util/ArrayList;)Z
    //   2435: istore 12
    //   2437: goto +2938 -> 5375
    //   2440: getstatic 108	com/chelpus/root/utils/createapkcustom:patchedLibs	Ljava/util/ArrayList;
    //   2443: aload 20
    //   2445: invokevirtual 492	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2448: pop
    //   2449: goto -142 -> 2307
    //   2452: iconst_0
    //   2453: putstatic 118	com/chelpus/root/utils/createapkcustom:multilib_patch	Z
    //   2456: iconst_0
    //   2457: putstatic 116	com/chelpus/root/utils/createapkcustom:goodResult	Z
    //   2460: getstatic 75	com/chelpus/root/utils/createapkcustom:ser	Ljava/util/ArrayList;
    //   2463: invokevirtual 323	java/util/ArrayList:clear	()V
    //   2466: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   2469: invokevirtual 323	java/util/ArrayList:clear	()V
    //   2472: sipush 200
    //   2475: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   2478: iload 12
    //   2480: istore 15
    //   2482: iload 13
    //   2484: istore 14
    //   2486: goto -2014 -> 472
    //   2489: getstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   2492: invokevirtual 286	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   2495: astore 19
    //   2497: aload 19
    //   2499: invokeinterface 291 1 0
    //   2504: ifeq +138 -> 2642
    //   2507: aload 19
    //   2509: invokeinterface 295 1 0
    //   2514: checkcast 342	java/lang/String
    //   2517: astore 20
    //   2519: new 186	java/io/File
    //   2522: dup
    //   2523: aload 20
    //   2525: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   2528: putstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   2531: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2534: ldc_w 480
    //   2537: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2540: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2543: new 159	java/lang/StringBuilder
    //   2546: dup
    //   2547: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   2550: ldc_w 516
    //   2553: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2556: getstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   2559: invokevirtual 485	java/io/File:getPath	()Ljava/lang/String;
    //   2562: new 159	java/lang/StringBuilder
    //   2565: dup
    //   2566: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   2569: getstatic 91	com/chelpus/root/utils/createapkcustom:sddir	Ljava/lang/String;
    //   2572: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2575: ldc_w 487
    //   2578: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2581: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2584: ldc 97
    //   2586: invokevirtual 442	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   2589: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2592: ldc_w 467
    //   2595: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2598: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2601: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2604: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2607: ldc_w 489
    //   2610: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2613: getstatic 83	com/chelpus/root/utils/createapkcustom:manualpatch	Z
    //   2616: ifne +2770 -> 5386
    //   2619: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   2622: invokestatic 472	com/chelpus/root/utils/createapkcustom:patchProcess	(Ljava/util/ArrayList;)Z
    //   2625: istore 12
    //   2627: goto +2759 -> 5386
    //   2630: getstatic 108	com/chelpus/root/utils/createapkcustom:patchedLibs	Ljava/util/ArrayList;
    //   2633: aload 20
    //   2635: invokevirtual 492	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2638: pop
    //   2639: goto -142 -> 2497
    //   2642: iconst_0
    //   2643: putstatic 118	com/chelpus/root/utils/createapkcustom:multilib_patch	Z
    //   2646: iconst_0
    //   2647: putstatic 116	com/chelpus/root/utils/createapkcustom:goodResult	Z
    //   2650: getstatic 75	com/chelpus/root/utils/createapkcustom:ser	Ljava/util/ArrayList;
    //   2653: invokevirtual 323	java/util/ArrayList:clear	()V
    //   2656: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   2659: invokevirtual 323	java/util/ArrayList:clear	()V
    //   2662: sipush 200
    //   2665: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   2668: iload 12
    //   2670: istore 15
    //   2672: iload 13
    //   2674: istore 14
    //   2676: goto -2204 -> 472
    //   2679: astore 19
    //   2681: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2684: ldc_w 518
    //   2687: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2690: goto -2096 -> 594
    //   2693: getstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   2696: invokevirtual 323	java/util/ArrayList:clear	()V
    //   2699: iconst_4
    //   2700: aload 18
    //   2702: invokestatic 522	com/chelpus/root/utils/createapkcustom:searchlib	(ILjava/lang/String;)Ljava/util/ArrayList;
    //   2705: putstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   2708: goto +2572 -> 5280
    //   2711: getstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   2714: invokevirtual 323	java/util/ArrayList:clear	()V
    //   2717: iconst_0
    //   2718: aload 18
    //   2720: invokestatic 522	com/chelpus/root/utils/createapkcustom:searchlib	(ILjava/lang/String;)Ljava/util/ArrayList;
    //   2723: putstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   2726: goto +2554 -> 5280
    //   2729: getstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   2732: invokevirtual 323	java/util/ArrayList:clear	()V
    //   2735: iconst_1
    //   2736: aload 18
    //   2738: invokestatic 522	com/chelpus/root/utils/createapkcustom:searchlib	(ILjava/lang/String;)Ljava/util/ArrayList;
    //   2741: putstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   2744: goto +2536 -> 5280
    //   2747: getstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   2750: invokevirtual 323	java/util/ArrayList:clear	()V
    //   2753: iconst_2
    //   2754: aload 18
    //   2756: invokestatic 522	com/chelpus/root/utils/createapkcustom:searchlib	(ILjava/lang/String;)Ljava/util/ArrayList;
    //   2759: putstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   2762: goto +2518 -> 5280
    //   2765: getstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   2768: invokevirtual 323	java/util/ArrayList:clear	()V
    //   2771: iconst_3
    //   2772: aload 18
    //   2774: invokestatic 522	com/chelpus/root/utils/createapkcustom:searchlib	(ILjava/lang/String;)Ljava/util/ArrayList;
    //   2777: putstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   2780: goto +2500 -> 5280
    //   2783: astore 18
    //   2785: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2788: ldc_w 524
    //   2791: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2794: aload 19
    //   2796: astore 18
    //   2798: goto -2109 -> 689
    //   2801: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2804: ldc_w 526
    //   2807: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2810: goto +2479 -> 5289
    //   2813: astore 19
    //   2815: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2818: ldc_w 528
    //   2821: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2824: ldc 97
    //   2826: putstatic 110	com/chelpus/root/utils/createapkcustom:group	Ljava/lang/String;
    //   2829: goto -1889 -> 940
    //   2832: astore 19
    //   2834: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2837: ldc_w 528
    //   2840: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2843: goto -1834 -> 1009
    //   2846: aload 20
    //   2848: iload 11
    //   2850: iconst_0
    //   2851: iastore
    //   2852: goto -1682 -> 1170
    //   2855: astore 19
    //   2857: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   2860: new 159	java/lang/StringBuilder
    //   2863: dup
    //   2864: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   2867: ldc_w 530
    //   2870: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2873: aload 19
    //   2875: invokevirtual 497	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   2878: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2881: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   2884: aload 18
    //   2886: astore 19
    //   2888: iload 14
    //   2890: istore 12
    //   2892: iload 8
    //   2894: istore 7
    //   2896: iload_3
    //   2897: istore_1
    //   2898: aload 20
    //   2900: astore 22
    //   2902: aload 26
    //   2904: astore 21
    //   2906: aload 30
    //   2908: iload 9
    //   2910: aaload
    //   2911: ldc_w 532
    //   2914: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   2917: istore 13
    //   2919: iload 12
    //   2921: istore 14
    //   2923: aload 17
    //   2925: astore 18
    //   2927: iload 13
    //   2929: ifeq +223 -> 3152
    //   2932: new 377	org/json/JSONObject
    //   2935: dup
    //   2936: aload 30
    //   2938: iload 9
    //   2940: aaload
    //   2941: invokespecial 378	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   2944: ldc_w 534
    //   2947: invokevirtual 383	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   2950: astore 18
    //   2952: aload 18
    //   2954: astore 17
    //   2956: new 159	java/lang/StringBuilder
    //   2959: dup
    //   2960: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   2963: ldc_w 536
    //   2966: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2969: aload_0
    //   2970: iconst_5
    //   2971: aaload
    //   2972: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2975: ldc_w 530
    //   2978: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2981: aload_0
    //   2982: bipush 6
    //   2984: aaload
    //   2985: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2988: ldc_w 538
    //   2991: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2994: aload_0
    //   2995: iconst_0
    //   2996: aaload
    //   2997: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3000: ldc_w 530
    //   3003: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3006: ldc_w 534
    //   3009: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3012: aload 17
    //   3014: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3017: ldc_w 530
    //   3020: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3023: getstatic 91	com/chelpus/root/utils/createapkcustom:sddir	Ljava/lang/String;
    //   3026: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3029: ldc_w 530
    //   3032: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3035: getstatic 95	com/chelpus/root/utils/createapkcustom:tooldir	Ljava/lang/String;
    //   3038: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3041: ldc_w 365
    //   3044: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3047: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   3050: astore 18
    //   3052: invokestatic 544	java/lang/Runtime:getRuntime	()Ljava/lang/Runtime;
    //   3055: aload 18
    //   3057: invokevirtual 548	java/lang/Runtime:exec	(Ljava/lang/String;)Ljava/lang/Process;
    //   3060: astore 18
    //   3062: aload 18
    //   3064: invokevirtual 553	java/lang/Process:waitFor	()I
    //   3067: pop
    //   3068: new 555	java/io/DataInputStream
    //   3071: dup
    //   3072: aload 18
    //   3074: invokevirtual 559	java/lang/Process:getInputStream	()Ljava/io/InputStream;
    //   3077: invokespecial 562	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   3080: astore 20
    //   3082: aload 20
    //   3084: invokevirtual 563	java/io/DataInputStream:available	()I
    //   3087: newarray <illegal type>
    //   3089: astore 26
    //   3091: aload 20
    //   3093: aload 26
    //   3095: invokevirtual 564	java/io/DataInputStream:read	([B)I
    //   3098: pop
    //   3099: new 342	java/lang/String
    //   3102: dup
    //   3103: aload 26
    //   3105: invokespecial 566	java/lang/String:<init>	([B)V
    //   3108: astore 20
    //   3110: aload 18
    //   3112: invokevirtual 569	java/lang/Process:destroy	()V
    //   3115: aload 20
    //   3117: ldc_w 571
    //   3120: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3123: ifeq +321 -> 3444
    //   3126: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   3129: ldc_w 573
    //   3132: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   3135: iconst_1
    //   3136: istore 14
    //   3138: getstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   3141: invokestatic 575	com/chelpus/root/utils/createapkcustom:fixadler	(Ljava/io/File;)V
    //   3144: iconst_1
    //   3145: putstatic 83	com/chelpus/root/utils/createapkcustom:manualpatch	Z
    //   3148: aload 17
    //   3150: astore 18
    //   3152: aload 30
    //   3154: iload 9
    //   3156: aaload
    //   3157: ldc_w 576
    //   3160: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3163: istore 12
    //   3165: iload_1
    //   3166: istore_2
    //   3167: iload 7
    //   3169: istore_3
    //   3170: iload 15
    //   3172: istore 13
    //   3174: aload 18
    //   3176: astore 20
    //   3178: iload 12
    //   3180: ifeq +356 -> 3536
    //   3183: new 377	org/json/JSONObject
    //   3186: dup
    //   3187: aload 30
    //   3189: iload 9
    //   3191: aaload
    //   3192: invokespecial 378	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   3195: ldc_w 576
    //   3198: invokevirtual 383	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   3201: astore 17
    //   3203: aload 17
    //   3205: invokevirtual 409	java/lang/String:trim	()Ljava/lang/String;
    //   3208: astore 20
    //   3210: aload 20
    //   3212: ldc_w 411
    //   3215: invokevirtual 415	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   3218: arraylength
    //   3219: anewarray 342	java/lang/String
    //   3222: astore 17
    //   3224: aload 20
    //   3226: ldc_w 411
    //   3229: invokevirtual 415	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   3232: astore 17
    //   3234: aload 17
    //   3236: arraylength
    //   3237: newarray <illegal type>
    //   3239: astore 22
    //   3241: aload 17
    //   3243: arraylength
    //   3244: newarray <illegal type>
    //   3246: astore 21
    //   3248: iconst_0
    //   3249: istore 8
    //   3251: iload_1
    //   3252: istore_3
    //   3253: iload_1
    //   3254: istore_2
    //   3255: iload 8
    //   3257: aload 17
    //   3259: arraylength
    //   3260: if_icmpge +257 -> 3517
    //   3263: iload_1
    //   3264: istore_2
    //   3265: iload_1
    //   3266: istore_3
    //   3267: aload 17
    //   3269: iload 8
    //   3271: aaload
    //   3272: ldc_w 417
    //   3275: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3278: ifeq +31 -> 3309
    //   3281: iload_1
    //   3282: istore_2
    //   3283: iload_1
    //   3284: istore_3
    //   3285: aload 17
    //   3287: iload 8
    //   3289: aaload
    //   3290: ldc_w 419
    //   3293: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3296: ifne +13 -> 3309
    //   3299: iconst_1
    //   3300: istore_2
    //   3301: aload 17
    //   3303: iload 8
    //   3305: ldc_w 421
    //   3308: aastore
    //   3309: iload_2
    //   3310: istore_3
    //   3311: aload 17
    //   3313: iload 8
    //   3315: aaload
    //   3316: ldc_w 419
    //   3319: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3322: ifne +2075 -> 5397
    //   3325: iload_2
    //   3326: istore_3
    //   3327: aload 17
    //   3329: iload 8
    //   3331: aaload
    //   3332: ldc_w 423
    //   3335: invokevirtual 427	java/lang/String:matches	(Ljava/lang/String;)Z
    //   3338: ifeq +139 -> 3477
    //   3341: goto +2056 -> 5397
    //   3344: iload_2
    //   3345: istore_3
    //   3346: aload 17
    //   3348: iload 8
    //   3350: aaload
    //   3351: invokevirtual 579	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   3354: ldc_w 433
    //   3357: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3360: ifeq +39 -> 3399
    //   3363: iload_2
    //   3364: istore_3
    //   3365: aload 22
    //   3367: iload 8
    //   3369: aload 17
    //   3371: iload 8
    //   3373: aaload
    //   3374: ldc_w 433
    //   3377: ldc 97
    //   3379: invokevirtual 442	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   3382: invokestatic 448	java/lang/Integer:valueOf	(Ljava/lang/String;)Ljava/lang/Integer;
    //   3385: invokevirtual 451	java/lang/Integer:intValue	()I
    //   3388: iconst_2
    //   3389: iadd
    //   3390: iastore
    //   3391: aload 17
    //   3393: iload 8
    //   3395: ldc_w 421
    //   3398: aastore
    //   3399: iload_2
    //   3400: istore_3
    //   3401: aload 21
    //   3403: iload 8
    //   3405: aload 17
    //   3407: iload 8
    //   3409: aaload
    //   3410: bipush 16
    //   3412: invokestatic 454	java/lang/Integer:valueOf	(Ljava/lang/String;I)Ljava/lang/Integer;
    //   3415: invokevirtual 458	java/lang/Integer:byteValue	()B
    //   3418: bastore
    //   3419: iload 8
    //   3421: iconst_1
    //   3422: iadd
    //   3423: istore 8
    //   3425: iload_2
    //   3426: istore_1
    //   3427: goto -176 -> 3251
    //   3430: astore 18
    //   3432: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   3435: ldc_w 581
    //   3438: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   3441: goto -485 -> 2956
    //   3444: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   3447: ldc_w 583
    //   3450: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   3453: iconst_0
    //   3454: istore 14
    //   3456: goto -318 -> 3138
    //   3459: astore 17
    //   3461: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   3464: ldc_w 585
    //   3467: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   3470: aload 18
    //   3472: astore 17
    //   3474: goto -271 -> 3203
    //   3477: aload 22
    //   3479: iload 8
    //   3481: iconst_0
    //   3482: iastore
    //   3483: goto -139 -> 3344
    //   3486: astore 17
    //   3488: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   3491: new 159	java/lang/StringBuilder
    //   3494: dup
    //   3495: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   3498: ldc_w 587
    //   3501: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3504: aload 17
    //   3506: invokevirtual 497	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   3509: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   3512: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   3515: iload_3
    //   3516: istore_2
    //   3517: iload_2
    //   3518: ifeq +474 -> 3992
    //   3521: iconst_0
    //   3522: istore 13
    //   3524: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   3527: ldc_w 589
    //   3530: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   3533: iload 7
    //   3535: istore_3
    //   3536: aload 30
    //   3538: iload 9
    //   3540: aaload
    //   3541: ldc_w 591
    //   3544: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3547: istore 15
    //   3549: iload_2
    //   3550: istore_1
    //   3551: iload 13
    //   3553: istore 12
    //   3555: aload 16
    //   3557: astore 17
    //   3559: iload 15
    //   3561: ifeq +671 -> 4232
    //   3564: new 377	org/json/JSONObject
    //   3567: dup
    //   3568: aload 30
    //   3570: iload 9
    //   3572: aaload
    //   3573: invokespecial 378	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   3576: ldc_w 591
    //   3579: invokevirtual 383	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   3582: astore 17
    //   3584: aload 17
    //   3586: astore 16
    //   3588: aload 16
    //   3590: invokevirtual 409	java/lang/String:trim	()Ljava/lang/String;
    //   3593: astore 16
    //   3595: aload 16
    //   3597: ldc_w 411
    //   3600: invokevirtual 415	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   3603: arraylength
    //   3604: anewarray 342	java/lang/String
    //   3607: astore 17
    //   3609: aload 16
    //   3611: ldc_w 411
    //   3614: invokevirtual 415	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   3617: astore 17
    //   3619: aload 17
    //   3621: arraylength
    //   3622: newarray <illegal type>
    //   3624: astore 18
    //   3626: aload 17
    //   3628: arraylength
    //   3629: newarray <illegal type>
    //   3631: astore 26
    //   3633: iconst_0
    //   3634: istore 8
    //   3636: iload_2
    //   3637: istore 7
    //   3639: iload_2
    //   3640: istore_1
    //   3641: iload 8
    //   3643: aload 17
    //   3645: arraylength
    //   3646: if_icmpge +478 -> 4124
    //   3649: iload_2
    //   3650: istore_1
    //   3651: iload_2
    //   3652: istore 7
    //   3654: aload 17
    //   3656: iload 8
    //   3658: aaload
    //   3659: ldc_w 417
    //   3662: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3665: ifeq +32 -> 3697
    //   3668: iload_2
    //   3669: istore_1
    //   3670: iload_2
    //   3671: istore 7
    //   3673: aload 17
    //   3675: iload 8
    //   3677: aaload
    //   3678: ldc_w 419
    //   3681: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3684: ifne +13 -> 3697
    //   3687: iconst_1
    //   3688: istore_1
    //   3689: aload 17
    //   3691: iload 8
    //   3693: ldc_w 421
    //   3696: aastore
    //   3697: iload_1
    //   3698: istore 7
    //   3700: aload 17
    //   3702: iload 8
    //   3704: aaload
    //   3705: ldc_w 419
    //   3708: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3711: ifne +1703 -> 5414
    //   3714: iload_1
    //   3715: istore 7
    //   3717: aload 17
    //   3719: iload 8
    //   3721: aaload
    //   3722: ldc_w 423
    //   3725: invokevirtual 427	java/lang/String:matches	(Ljava/lang/String;)Z
    //   3728: ifeq +355 -> 4083
    //   3731: goto +1683 -> 5414
    //   3734: iload_1
    //   3735: istore 7
    //   3737: aload 17
    //   3739: iload 8
    //   3741: aaload
    //   3742: invokevirtual 438	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   3745: ldc_w 593
    //   3748: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3751: ifeq +19 -> 3770
    //   3754: aload 17
    //   3756: iload 8
    //   3758: ldc_w 421
    //   3761: aastore
    //   3762: aload 18
    //   3764: iload 8
    //   3766: sipush 253
    //   3769: iastore
    //   3770: iload_1
    //   3771: istore 7
    //   3773: aload 17
    //   3775: iload 8
    //   3777: aaload
    //   3778: ldc_w 595
    //   3781: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3784: ifne +1647 -> 5431
    //   3787: iload_1
    //   3788: istore 7
    //   3790: aload 17
    //   3792: iload 8
    //   3794: aaload
    //   3795: ldc_w 597
    //   3798: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3801: ifeq +6 -> 3807
    //   3804: goto +1627 -> 5431
    //   3807: iload_1
    //   3808: istore 7
    //   3810: aload 17
    //   3812: iload 8
    //   3814: aaload
    //   3815: ldc_w 599
    //   3818: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3821: ifne +1629 -> 5450
    //   3824: iload_1
    //   3825: istore 7
    //   3827: aload 17
    //   3829: iload 8
    //   3831: aaload
    //   3832: ldc_w 601
    //   3835: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3838: ifeq +6 -> 3844
    //   3841: goto +1609 -> 5450
    //   3844: iload_1
    //   3845: istore 7
    //   3847: aload 17
    //   3849: iload 8
    //   3851: aaload
    //   3852: ldc_w 429
    //   3855: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3858: ifne +54 -> 3912
    //   3861: iload_1
    //   3862: istore 7
    //   3864: aload 17
    //   3866: iload 8
    //   3868: aaload
    //   3869: ldc_w 431
    //   3872: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3875: ifne +37 -> 3912
    //   3878: iload_1
    //   3879: istore 7
    //   3881: aload 17
    //   3883: iload 8
    //   3885: aaload
    //   3886: ldc_w 433
    //   3889: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3892: ifne +20 -> 3912
    //   3895: iload_1
    //   3896: istore 7
    //   3898: aload 17
    //   3900: iload 8
    //   3902: aaload
    //   3903: ldc_w 433
    //   3906: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3909: ifeq +51 -> 3960
    //   3912: iload_1
    //   3913: istore 7
    //   3915: aload 18
    //   3917: iload 8
    //   3919: aload 17
    //   3921: iload 8
    //   3923: aaload
    //   3924: invokevirtual 438	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   3927: ldc_w 431
    //   3930: ldc 97
    //   3932: invokevirtual 442	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   3935: ldc_w 435
    //   3938: ldc 97
    //   3940: invokevirtual 442	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   3943: invokestatic 448	java/lang/Integer:valueOf	(Ljava/lang/String;)Ljava/lang/Integer;
    //   3946: invokevirtual 451	java/lang/Integer:intValue	()I
    //   3949: iconst_2
    //   3950: iadd
    //   3951: iastore
    //   3952: aload 17
    //   3954: iload 8
    //   3956: ldc_w 421
    //   3959: aastore
    //   3960: iload_1
    //   3961: istore 7
    //   3963: aload 26
    //   3965: iload 8
    //   3967: aload 17
    //   3969: iload 8
    //   3971: aaload
    //   3972: bipush 16
    //   3974: invokestatic 454	java/lang/Integer:valueOf	(Ljava/lang/String;I)Ljava/lang/Integer;
    //   3977: invokevirtual 458	java/lang/Integer:byteValue	()B
    //   3980: bastore
    //   3981: iload 8
    //   3983: iconst_1
    //   3984: iadd
    //   3985: istore 8
    //   3987: iload_1
    //   3988: istore_2
    //   3989: goto -353 -> 3636
    //   3992: iconst_1
    //   3993: istore_3
    //   3994: new 603	com/android/vending/billing/InAppBillingService/LUCK/SearchItem
    //   3997: dup
    //   3998: aload 21
    //   4000: aload 22
    //   4002: invokespecial 606	com/android/vending/billing/InAppBillingService/LUCK/SearchItem:<init>	([B[I)V
    //   4005: astore 17
    //   4007: aload 17
    //   4009: aload 21
    //   4011: arraylength
    //   4012: newarray <illegal type>
    //   4014: putfield 610	com/android/vending/billing/InAppBillingService/LUCK/SearchItem:repByte	[B
    //   4017: getstatic 75	com/chelpus/root/utils/createapkcustom:ser	Ljava/util/ArrayList;
    //   4020: aload 17
    //   4022: invokevirtual 492	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   4025: pop
    //   4026: iload 15
    //   4028: istore 13
    //   4030: goto -494 -> 3536
    //   4033: astore 17
    //   4035: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   4038: new 159	java/lang/StringBuilder
    //   4041: dup
    //   4042: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   4045: ldc_w 530
    //   4048: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4051: aload 17
    //   4053: invokevirtual 497	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   4056: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   4059: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   4062: iload 15
    //   4064: istore 13
    //   4066: goto -530 -> 3536
    //   4069: astore 17
    //   4071: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   4074: ldc_w 612
    //   4077: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   4080: goto -492 -> 3588
    //   4083: aload 18
    //   4085: iload 8
    //   4087: iconst_1
    //   4088: iastore
    //   4089: goto -355 -> 3734
    //   4092: astore 17
    //   4094: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   4097: new 159	java/lang/StringBuilder
    //   4100: dup
    //   4101: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   4104: ldc_w 530
    //   4107: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4110: aload 17
    //   4112: invokevirtual 497	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   4115: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   4118: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   4121: iload 7
    //   4123: istore_1
    //   4124: aload 18
    //   4126: arraylength
    //   4127: aload 22
    //   4129: arraylength
    //   4130: if_icmpne +1339 -> 5469
    //   4133: aload 21
    //   4135: arraylength
    //   4136: aload 26
    //   4138: arraylength
    //   4139: if_icmpne +1330 -> 5469
    //   4142: aload 26
    //   4144: arraylength
    //   4145: iconst_4
    //   4146: if_icmplt +1323 -> 5469
    //   4149: iload_1
    //   4150: istore_2
    //   4151: aload 21
    //   4153: arraylength
    //   4154: iconst_4
    //   4155: if_icmpge +6 -> 4161
    //   4158: goto +1311 -> 5469
    //   4161: iload_2
    //   4162: ifeq +15 -> 4177
    //   4165: iconst_0
    //   4166: istore 13
    //   4168: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   4171: ldc_w 614
    //   4174: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   4177: iload_2
    //   4178: istore_1
    //   4179: iload 13
    //   4181: istore 12
    //   4183: aload 16
    //   4185: astore 17
    //   4187: iload_2
    //   4188: ifne +44 -> 4232
    //   4191: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   4194: new 616	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
    //   4197: dup
    //   4198: aload 21
    //   4200: aload 22
    //   4202: aload 26
    //   4204: aload 18
    //   4206: getstatic 110	com/chelpus/root/utils/createapkcustom:group	Ljava/lang/String;
    //   4209: iconst_0
    //   4210: invokespecial 619	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:<init>	([B[I[B[ILjava/lang/String;Z)V
    //   4213: invokevirtual 492	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   4216: pop
    //   4217: ldc 97
    //   4219: putstatic 110	com/chelpus/root/utils/createapkcustom:group	Ljava/lang/String;
    //   4222: aload 16
    //   4224: astore 17
    //   4226: iload 13
    //   4228: istore 12
    //   4230: iload_2
    //   4231: istore_1
    //   4232: aload 30
    //   4234: iload 9
    //   4236: aaload
    //   4237: ldc_w 621
    //   4240: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   4243: istore 15
    //   4245: iload_1
    //   4246: istore_2
    //   4247: iload 12
    //   4249: istore 13
    //   4251: aload 17
    //   4253: astore 16
    //   4255: iload 15
    //   4257: ifeq +582 -> 4839
    //   4260: new 377	org/json/JSONObject
    //   4263: dup
    //   4264: aload 30
    //   4266: iload 9
    //   4268: aaload
    //   4269: invokespecial 378	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   4272: ldc_w 621
    //   4275: invokevirtual 383	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   4278: astore 16
    //   4280: aload 16
    //   4282: invokevirtual 409	java/lang/String:trim	()Ljava/lang/String;
    //   4285: astore 17
    //   4287: aload 17
    //   4289: ldc_w 411
    //   4292: invokevirtual 415	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   4295: arraylength
    //   4296: anewarray 342	java/lang/String
    //   4299: astore 16
    //   4301: aload 17
    //   4303: ldc_w 411
    //   4306: invokevirtual 415	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   4309: astore 16
    //   4311: aload 16
    //   4313: arraylength
    //   4314: newarray <illegal type>
    //   4316: astore 18
    //   4318: aload 16
    //   4320: arraylength
    //   4321: newarray <illegal type>
    //   4323: astore 26
    //   4325: iconst_0
    //   4326: istore 8
    //   4328: iload_1
    //   4329: istore 7
    //   4331: iload_1
    //   4332: istore_2
    //   4333: iload 8
    //   4335: aload 16
    //   4337: arraylength
    //   4338: if_icmpge +401 -> 4739
    //   4341: iload_1
    //   4342: istore_2
    //   4343: iload_1
    //   4344: istore 7
    //   4346: aload 16
    //   4348: iload 8
    //   4350: aaload
    //   4351: ldc_w 417
    //   4354: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   4357: ifeq +32 -> 4389
    //   4360: iload_1
    //   4361: istore_2
    //   4362: iload_1
    //   4363: istore 7
    //   4365: aload 16
    //   4367: iload 8
    //   4369: aaload
    //   4370: ldc_w 419
    //   4373: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   4376: ifne +13 -> 4389
    //   4379: iconst_1
    //   4380: istore_2
    //   4381: aload 16
    //   4383: iload 8
    //   4385: ldc_w 421
    //   4388: aastore
    //   4389: iload_2
    //   4390: istore 7
    //   4392: aload 16
    //   4394: iload 8
    //   4396: aaload
    //   4397: ldc_w 419
    //   4400: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   4403: ifne +1071 -> 5474
    //   4406: iload_2
    //   4407: istore 7
    //   4409: aload 16
    //   4411: iload 8
    //   4413: aaload
    //   4414: ldc_w 423
    //   4417: invokevirtual 427	java/lang/String:matches	(Ljava/lang/String;)Z
    //   4420: ifeq +280 -> 4700
    //   4423: goto +1051 -> 5474
    //   4426: iload_2
    //   4427: istore 7
    //   4429: aload 16
    //   4431: iload 8
    //   4433: aaload
    //   4434: invokevirtual 438	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   4437: ldc_w 593
    //   4440: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   4443: ifeq +19 -> 4462
    //   4446: aload 16
    //   4448: iload 8
    //   4450: ldc_w 421
    //   4453: aastore
    //   4454: aload 18
    //   4456: iload 8
    //   4458: sipush 253
    //   4461: iastore
    //   4462: iload_2
    //   4463: istore 7
    //   4465: aload 16
    //   4467: iload 8
    //   4469: aaload
    //   4470: ldc_w 595
    //   4473: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   4476: ifne +1015 -> 5491
    //   4479: iload_2
    //   4480: istore 7
    //   4482: aload 16
    //   4484: iload 8
    //   4486: aaload
    //   4487: ldc_w 597
    //   4490: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   4493: ifeq +6 -> 4499
    //   4496: goto +995 -> 5491
    //   4499: iload_2
    //   4500: istore 7
    //   4502: aload 16
    //   4504: iload 8
    //   4506: aaload
    //   4507: ldc_w 599
    //   4510: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   4513: ifne +997 -> 5510
    //   4516: iload_2
    //   4517: istore 7
    //   4519: aload 16
    //   4521: iload 8
    //   4523: aaload
    //   4524: ldc_w 601
    //   4527: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   4530: ifeq +6 -> 4536
    //   4533: goto +977 -> 5510
    //   4536: iload_2
    //   4537: istore 7
    //   4539: aload 16
    //   4541: iload 8
    //   4543: aaload
    //   4544: ldc_w 429
    //   4547: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   4550: ifne +54 -> 4604
    //   4553: iload_2
    //   4554: istore 7
    //   4556: aload 16
    //   4558: iload 8
    //   4560: aaload
    //   4561: ldc_w 431
    //   4564: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   4567: ifne +37 -> 4604
    //   4570: iload_2
    //   4571: istore 7
    //   4573: aload 16
    //   4575: iload 8
    //   4577: aaload
    //   4578: ldc_w 433
    //   4581: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   4584: ifne +20 -> 4604
    //   4587: iload_2
    //   4588: istore 7
    //   4590: aload 16
    //   4592: iload 8
    //   4594: aaload
    //   4595: ldc_w 433
    //   4598: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   4601: ifeq +51 -> 4652
    //   4604: iload_2
    //   4605: istore 7
    //   4607: aload 18
    //   4609: iload 8
    //   4611: aload 16
    //   4613: iload 8
    //   4615: aaload
    //   4616: invokevirtual 438	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   4619: ldc_w 431
    //   4622: ldc 97
    //   4624: invokevirtual 442	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   4627: ldc_w 435
    //   4630: ldc 97
    //   4632: invokevirtual 442	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   4635: invokestatic 448	java/lang/Integer:valueOf	(Ljava/lang/String;)Ljava/lang/Integer;
    //   4638: invokevirtual 451	java/lang/Integer:intValue	()I
    //   4641: iconst_2
    //   4642: iadd
    //   4643: iastore
    //   4644: aload 16
    //   4646: iload 8
    //   4648: ldc_w 421
    //   4651: aastore
    //   4652: iload_2
    //   4653: istore 7
    //   4655: aload 26
    //   4657: iload 8
    //   4659: aload 16
    //   4661: iload 8
    //   4663: aaload
    //   4664: bipush 16
    //   4666: invokestatic 454	java/lang/Integer:valueOf	(Ljava/lang/String;I)Ljava/lang/Integer;
    //   4669: invokevirtual 458	java/lang/Integer:byteValue	()B
    //   4672: bastore
    //   4673: iload 8
    //   4675: iconst_1
    //   4676: iadd
    //   4677: istore 8
    //   4679: iload_2
    //   4680: istore_1
    //   4681: goto -353 -> 4328
    //   4684: astore 16
    //   4686: ldc_w 623
    //   4689: invokestatic 626	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   4692: pop
    //   4693: aload 17
    //   4695: astore 16
    //   4697: goto -417 -> 4280
    //   4700: aload 18
    //   4702: iload 8
    //   4704: iconst_1
    //   4705: iastore
    //   4706: goto -280 -> 4426
    //   4709: astore 16
    //   4711: new 159	java/lang/StringBuilder
    //   4714: dup
    //   4715: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   4718: ldc_w 530
    //   4721: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4724: aload 16
    //   4726: invokevirtual 497	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   4729: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   4732: invokestatic 626	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   4735: pop
    //   4736: iload 7
    //   4738: istore_2
    //   4739: aload 26
    //   4741: arraylength
    //   4742: iconst_4
    //   4743: if_icmplt +786 -> 5529
    //   4746: iload_2
    //   4747: istore_1
    //   4748: aload 21
    //   4750: arraylength
    //   4751: iconst_4
    //   4752: if_icmpge +6 -> 4758
    //   4755: goto +774 -> 5529
    //   4758: iload_1
    //   4759: ifeq +13 -> 4772
    //   4762: iconst_0
    //   4763: istore 12
    //   4765: ldc_w 628
    //   4768: invokestatic 626	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   4771: pop
    //   4772: iload_1
    //   4773: istore_2
    //   4774: iload 12
    //   4776: istore 13
    //   4778: aload 17
    //   4780: astore 16
    //   4782: iload_1
    //   4783: ifne +56 -> 4839
    //   4786: getstatic 118	com/chelpus/root/utils/createapkcustom:multilib_patch	Z
    //   4789: ifne +9 -> 4798
    //   4792: getstatic 114	com/chelpus/root/utils/createapkcustom:multidex	Z
    //   4795: ifeq +323 -> 5118
    //   4798: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   4801: new 616	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
    //   4804: dup
    //   4805: aload 21
    //   4807: aload 22
    //   4809: aload 26
    //   4811: aload 18
    //   4813: ldc_w 630
    //   4816: iconst_1
    //   4817: invokespecial 619	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:<init>	([B[I[B[ILjava/lang/String;Z)V
    //   4820: invokevirtual 492	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   4823: pop
    //   4824: ldc 97
    //   4826: putstatic 110	com/chelpus/root/utils/createapkcustom:group	Ljava/lang/String;
    //   4829: aload 17
    //   4831: astore 16
    //   4833: iload 12
    //   4835: istore 13
    //   4837: iload_1
    //   4838: istore_2
    //   4839: aload 30
    //   4841: iload 9
    //   4843: aaload
    //   4844: ldc_w 632
    //   4847: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   4850: istore 15
    //   4852: iload 13
    //   4854: istore 12
    //   4856: aload 16
    //   4858: astore 17
    //   4860: iload 15
    //   4862: ifeq +195 -> 5057
    //   4865: new 377	org/json/JSONObject
    //   4868: dup
    //   4869: aload 30
    //   4871: iload 9
    //   4873: aaload
    //   4874: invokespecial 378	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   4877: ldc_w 632
    //   4880: invokevirtual 383	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   4883: astore 17
    //   4885: aload 17
    //   4887: astore 16
    //   4889: aload 16
    //   4891: invokevirtual 409	java/lang/String:trim	()Ljava/lang/String;
    //   4894: astore 16
    //   4896: new 186	java/io/File
    //   4899: dup
    //   4900: new 159	java/lang/StringBuilder
    //   4903: dup
    //   4904: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   4907: new 186	java/io/File
    //   4910: dup
    //   4911: aload_0
    //   4912: iconst_1
    //   4913: aaload
    //   4914: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   4917: invokestatic 636	com/chelpus/Utils:getDirs	(Ljava/io/File;)Ljava/io/File;
    //   4920: invokevirtual 497	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   4923: ldc_w 638
    //   4926: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4929: aload 16
    //   4931: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4934: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   4937: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   4940: astore 17
    //   4942: aload 17
    //   4944: invokevirtual 641	java/io/File:length	()J
    //   4947: l2i
    //   4948: istore_1
    //   4949: iload_1
    //   4950: newarray <illegal type>
    //   4952: astore 18
    //   4954: new 233	java/io/FileInputStream
    //   4957: dup
    //   4958: aload 17
    //   4960: invokespecial 235	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   4963: astore 17
    //   4965: aload 17
    //   4967: aload 18
    //   4969: invokevirtual 243	java/io/FileInputStream:read	([B)I
    //   4972: istore 7
    //   4974: iload 7
    //   4976: ifgt -11 -> 4965
    //   4979: iload_1
    //   4980: newarray <illegal type>
    //   4982: astore 26
    //   4984: aload 26
    //   4986: iconst_1
    //   4987: invokestatic 647	java/util/Arrays:fill	([II)V
    //   4990: iload_2
    //   4991: ifeq +15 -> 5006
    //   4994: iconst_0
    //   4995: istore 13
    //   4997: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   5000: ldc_w 614
    //   5003: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   5006: iload 13
    //   5008: istore 12
    //   5010: aload 16
    //   5012: astore 17
    //   5014: iload_2
    //   5015: ifne +42 -> 5057
    //   5018: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   5021: new 616	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
    //   5024: dup
    //   5025: aload 21
    //   5027: aload 22
    //   5029: aload 18
    //   5031: aload 26
    //   5033: getstatic 110	com/chelpus/root/utils/createapkcustom:group	Ljava/lang/String;
    //   5036: iconst_0
    //   5037: invokespecial 619	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:<init>	([B[I[B[ILjava/lang/String;Z)V
    //   5040: invokevirtual 492	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   5043: pop
    //   5044: ldc 97
    //   5046: putstatic 110	com/chelpus/root/utils/createapkcustom:group	Ljava/lang/String;
    //   5049: aload 16
    //   5051: astore 17
    //   5053: iload 13
    //   5055: istore 12
    //   5057: aload 23
    //   5059: astore 16
    //   5061: iload 10
    //   5063: ifeq +34 -> 5097
    //   5066: new 159	java/lang/StringBuilder
    //   5069: dup
    //   5070: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   5073: aload 23
    //   5075: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5078: ldc_w 365
    //   5081: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5084: aload 30
    //   5086: iload 9
    //   5088: aaload
    //   5089: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5092: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   5095: astore 16
    //   5097: aload 24
    //   5099: ldc_w 649
    //   5102: invokevirtual 359	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   5105: ifeq +429 -> 5534
    //   5108: iconst_4
    //   5109: putstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   5112: iconst_1
    //   5113: istore 10
    //   5115: goto +419 -> 5534
    //   5118: getstatic 73	com/chelpus/root/utils/createapkcustom:pat	Ljava/util/ArrayList;
    //   5121: new 616	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
    //   5124: dup
    //   5125: aload 21
    //   5127: aload 22
    //   5129: aload 26
    //   5131: aload 18
    //   5133: getstatic 110	com/chelpus/root/utils/createapkcustom:group	Ljava/lang/String;
    //   5136: iconst_1
    //   5137: invokespecial 619	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:<init>	([B[I[B[ILjava/lang/String;Z)V
    //   5140: invokevirtual 492	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   5143: pop
    //   5144: goto -320 -> 4824
    //   5147: astore 17
    //   5149: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   5152: ldc_w 612
    //   5155: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   5158: goto -269 -> 4889
    //   5161: astore 17
    //   5163: aload 17
    //   5165: invokevirtual 261	java/lang/Exception:printStackTrace	()V
    //   5168: goto -189 -> 4979
    //   5171: getstatic 108	com/chelpus/root/utils/createapkcustom:patchedLibs	Ljava/util/ArrayList;
    //   5174: invokevirtual 279	java/util/ArrayList:size	()I
    //   5177: ifle +9 -> 5186
    //   5180: getstatic 108	com/chelpus/root/utils/createapkcustom:patchedLibs	Ljava/util/ArrayList;
    //   5183: invokestatic 653	com/chelpus/root/utils/createapkcustom:zipLib	(Ljava/util/ArrayList;)V
    //   5186: iload 13
    //   5188: ifeq +29 -> 5217
    //   5191: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   5194: new 159	java/lang/StringBuilder
    //   5197: dup
    //   5198: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   5201: ldc 97
    //   5203: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5206: aload 23
    //   5208: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5211: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   5214: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   5217: iload 13
    //   5219: ifne +18 -> 5237
    //   5222: getstatic 79	com/chelpus/root/utils/createapkcustom:patchteil	Z
    //   5225: ifeq +23 -> 5248
    //   5228: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   5231: ldc_w 655
    //   5234: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   5237: invokestatic 325	com/chelpus/root/utils/createapkcustom:clearTemp	()V
    //   5240: aload 28
    //   5242: invokevirtual 250	java/io/FileInputStream:close	()V
    //   5245: goto -3711 -> 1534
    //   5248: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   5251: ldc_w 657
    //   5254: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   5257: goto -20 -> 5237
    //   5260: astore 16
    //   5262: aload 16
    //   5264: invokevirtual 658	java/io/IOException:printStackTrace	()V
    //   5267: aload_0
    //   5268: areturn
    //   5269: iload 12
    //   5271: istore 15
    //   5273: iload 13
    //   5275: istore 14
    //   5277: goto -4805 -> 472
    //   5280: iconst_0
    //   5281: istore_1
    //   5282: aload 18
    //   5284: astore 19
    //   5286: goto -4642 -> 644
    //   5289: iconst_0
    //   5290: istore 5
    //   5292: goto -4566 -> 726
    //   5295: aload 31
    //   5297: iload 11
    //   5299: ldc_w 421
    //   5302: aastore
    //   5303: aload 20
    //   5305: iload 11
    //   5307: iconst_1
    //   5308: iastore
    //   5309: goto -4139 -> 1170
    //   5312: iload 14
    //   5314: istore 12
    //   5316: iload 14
    //   5318: ifne -3933 -> 1385
    //   5321: iconst_0
    //   5322: istore 13
    //   5324: iload 14
    //   5326: istore 12
    //   5328: goto -3943 -> 1385
    //   5331: iload 12
    //   5333: ifne -3684 -> 1649
    //   5336: iconst_0
    //   5337: istore 13
    //   5339: goto -3690 -> 1649
    //   5342: iload 12
    //   5344: ifne -3482 -> 1862
    //   5347: iconst_0
    //   5348: istore 13
    //   5350: goto -3488 -> 1862
    //   5353: iload 12
    //   5355: ifne -3295 -> 2060
    //   5358: iconst_0
    //   5359: istore 13
    //   5361: goto -3301 -> 2060
    //   5364: iload 12
    //   5366: ifne -3116 -> 2250
    //   5369: iconst_0
    //   5370: istore 13
    //   5372: goto -3122 -> 2250
    //   5375: iload 12
    //   5377: ifne -2937 -> 2440
    //   5380: iconst_0
    //   5381: istore 13
    //   5383: goto -2943 -> 2440
    //   5386: iload 12
    //   5388: ifne -2758 -> 2630
    //   5391: iconst_0
    //   5392: istore 13
    //   5394: goto -2764 -> 2630
    //   5397: aload 17
    //   5399: iload 8
    //   5401: ldc_w 421
    //   5404: aastore
    //   5405: aload 22
    //   5407: iload 8
    //   5409: iconst_1
    //   5410: iastore
    //   5411: goto -2067 -> 3344
    //   5414: aload 17
    //   5416: iload 8
    //   5418: ldc_w 421
    //   5421: aastore
    //   5422: aload 18
    //   5424: iload 8
    //   5426: iconst_0
    //   5427: iastore
    //   5428: goto -1694 -> 3734
    //   5431: aload 17
    //   5433: iload 8
    //   5435: ldc_w 421
    //   5438: aastore
    //   5439: aload 18
    //   5441: iload 8
    //   5443: sipush 254
    //   5446: iastore
    //   5447: goto -1640 -> 3807
    //   5450: aload 17
    //   5452: iload 8
    //   5454: ldc_w 421
    //   5457: aastore
    //   5458: aload 18
    //   5460: iload 8
    //   5462: sipush 255
    //   5465: iastore
    //   5466: goto -1622 -> 3844
    //   5469: iconst_1
    //   5470: istore_2
    //   5471: goto -1310 -> 4161
    //   5474: aload 16
    //   5476: iload 8
    //   5478: ldc_w 421
    //   5481: aastore
    //   5482: aload 18
    //   5484: iload 8
    //   5486: iconst_0
    //   5487: iastore
    //   5488: goto -1062 -> 4426
    //   5491: aload 16
    //   5493: iload 8
    //   5495: ldc_w 421
    //   5498: aastore
    //   5499: aload 18
    //   5501: iload 8
    //   5503: sipush 254
    //   5506: iastore
    //   5507: goto -1008 -> 4499
    //   5510: aload 16
    //   5512: iload 8
    //   5514: ldc_w 421
    //   5517: aastore
    //   5518: aload 18
    //   5520: iload 8
    //   5522: sipush 255
    //   5525: iastore
    //   5526: goto -990 -> 4536
    //   5529: iconst_1
    //   5530: istore_1
    //   5531: goto -773 -> 4758
    //   5534: iload 9
    //   5536: iconst_1
    //   5537: iadd
    //   5538: istore 9
    //   5540: aload 25
    //   5542: astore 26
    //   5544: aload 16
    //   5546: astore 23
    //   5548: iload 14
    //   5550: istore 13
    //   5552: aload 19
    //   5554: astore 18
    //   5556: aload 17
    //   5558: astore 16
    //   5560: aload 20
    //   5562: astore 17
    //   5564: goto -5369 -> 195
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	5567	0	paramArrayOfString	String[]
    //   240	5291	1	i	int
    //   79	5392	2	j	int
    //   146	3848	3	k	int
    //   143	352	4	m	int
    //   140	5151	5	n	int
    //   148	752	6	i1	int
    //   943	4032	7	i2	int
    //   968	4553	8	i3	int
    //   193	5346	9	i4	int
    //   81	5033	10	i5	int
    //   1055	4251	11	i6	int
    //   134	5253	12	bool1	boolean
    //   137	5414	13	bool2	boolean
    //   370	5179	14	bool3	boolean
    //   366	4906	15	bool4	boolean
    //   156	4504	16	localObject1	Object
    //   4684	1	16	localJSONException1	org.json.JSONException
    //   4695	1	16	localObject2	Object
    //   4709	16	16	localException1	Exception
    //   4780	316	16	localObject3	Object
    //   5260	285	16	localIOException	java.io.IOException
    //   5558	1	16	localObject4	Object
    //   160	3246	17	localObject5	Object
    //   3459	1	17	localJSONException2	org.json.JSONException
    //   3472	1	17	localJSONException3	org.json.JSONException
    //   3486	19	17	localException2	Exception
    //   3557	464	17	localObject6	Object
    //   4033	19	17	localException3	Exception
    //   4069	1	17	localJSONException4	org.json.JSONException
    //   4092	19	17	localException4	Exception
    //   4185	867	17	localObject7	Object
    //   5147	1	17	localJSONException5	org.json.JSONException
    //   5161	396	17	localException5	Exception
    //   5562	1	17	localObject8	Object
    //   152	2621	18	localObject9	Object
    //   2783	1	18	localJSONException6	org.json.JSONException
    //   2796	379	18	localObject10	Object
    //   3430	41	18	localJSONException7	org.json.JSONException
    //   3624	1931	18	localObject11	Object
    //   200	2308	19	localObject12	Object
    //   2679	116	19	localJSONException8	org.json.JSONException
    //   2813	1	19	localJSONException9	org.json.JSONException
    //   2832	1	19	localJSONException10	org.json.JSONException
    //   2855	19	19	localException6	Exception
    //   2886	2667	19	localObject13	Object
    //   1045	4516	20	localObject14	Object
    //   128	4998	21	localObject15	Object
    //   131	5275	22	localObject16	Object
    //   72	5475	23	localObject17	Object
    //   209	4889	24	localObject18	Object
    //   327	5214	25	localObject19	Object
    //   76	5467	26	localObject20	Object
    //   10	1534	27	localLogOutputStream	com.android.vending.billing.InAppBillingService.LUCK.LogOutputStream
    //   96	5145	28	localFileInputStream	FileInputStream
    //   117	79	29	localBufferedReader	java.io.BufferedReader
    //   125	4960	30	arrayOfString1	String[]
    //   1038	4258	31	arrayOfString2	String[]
    // Exception table:
    //   from	to	target	type
    //   86	127	1524	java/io/FileNotFoundException
    //   162	192	1524	java/io/FileNotFoundException
    //   195	202	1524	java/io/FileNotFoundException
    //   211	231	1524	java/io/FileNotFoundException
    //   246	274	1524	java/io/FileNotFoundException
    //   277	291	1524	java/io/FileNotFoundException
    //   291	323	1524	java/io/FileNotFoundException
    //   333	364	1524	java/io/FileNotFoundException
    //   372	386	1524	java/io/FileNotFoundException
    //   394	408	1524	java/io/FileNotFoundException
    //   416	472	1524	java/io/FileNotFoundException
    //   475	493	1524	java/io/FileNotFoundException
    //   496	524	1524	java/io/FileNotFoundException
    //   524	528	1524	java/io/FileNotFoundException
    //   528	546	1524	java/io/FileNotFoundException
    //   558	570	1524	java/io/FileNotFoundException
    //   570	590	1524	java/io/FileNotFoundException
    //   594	644	1524	java/io/FileNotFoundException
    //   657	669	1524	java/io/FileNotFoundException
    //   669	689	1524	java/io/FileNotFoundException
    //   689	723	1524	java/io/FileNotFoundException
    //   726	749	1524	java/io/FileNotFoundException
    //   754	777	1524	java/io/FileNotFoundException
    //   782	805	1524	java/io/FileNotFoundException
    //   810	833	1524	java/io/FileNotFoundException
    //   838	860	1524	java/io/FileNotFoundException
    //   872	895	1524	java/io/FileNotFoundException
    //   901	914	1524	java/io/FileNotFoundException
    //   919	940	1524	java/io/FileNotFoundException
    //   953	967	1524	java/io/FileNotFoundException
    //   974	982	1524	java/io/FileNotFoundException
    //   985	1005	1524	java/io/FileNotFoundException
    //   1009	1054	1524	java/io/FileNotFoundException
    //   1081	1089	1524	java/io/FileNotFoundException
    //   1093	1107	1524	java/io/FileNotFoundException
    //   1111	1125	1524	java/io/FileNotFoundException
    //   1137	1151	1524	java/io/FileNotFoundException
    //   1153	1167	1524	java/io/FileNotFoundException
    //   1172	1186	1524	java/io/FileNotFoundException
    //   1188	1202	1524	java/io/FileNotFoundException
    //   1204	1218	1524	java/io/FileNotFoundException
    //   1220	1234	1524	java/io/FileNotFoundException
    //   1236	1273	1524	java/io/FileNotFoundException
    //   1283	1301	1524	java/io/FileNotFoundException
    //   1320	1332	1524	java/io/FileNotFoundException
    //   1340	1346	1524	java/io/FileNotFoundException
    //   1354	1377	1524	java/io/FileNotFoundException
    //   1377	1385	1524	java/io/FileNotFoundException
    //   1393	1474	1524	java/io/FileNotFoundException
    //   1478	1492	1524	java/io/FileNotFoundException
    //   1495	1521	1524	java/io/FileNotFoundException
    //   1550	1632	1524	java/io/FileNotFoundException
    //   1632	1646	1524	java/io/FileNotFoundException
    //   1649	1680	1524	java/io/FileNotFoundException
    //   1721	1729	1524	java/io/FileNotFoundException
    //   1729	1845	1524	java/io/FileNotFoundException
    //   1845	1859	1524	java/io/FileNotFoundException
    //   1862	1871	1524	java/io/FileNotFoundException
    //   1882	1908	1524	java/io/FileNotFoundException
    //   1919	1927	1524	java/io/FileNotFoundException
    //   1927	2043	1524	java/io/FileNotFoundException
    //   2043	2057	1524	java/io/FileNotFoundException
    //   2060	2069	1524	java/io/FileNotFoundException
    //   2072	2098	1524	java/io/FileNotFoundException
    //   2109	2117	1524	java/io/FileNotFoundException
    //   2117	2233	1524	java/io/FileNotFoundException
    //   2233	2247	1524	java/io/FileNotFoundException
    //   2250	2259	1524	java/io/FileNotFoundException
    //   2262	2288	1524	java/io/FileNotFoundException
    //   2299	2307	1524	java/io/FileNotFoundException
    //   2307	2423	1524	java/io/FileNotFoundException
    //   2423	2437	1524	java/io/FileNotFoundException
    //   2440	2449	1524	java/io/FileNotFoundException
    //   2452	2478	1524	java/io/FileNotFoundException
    //   2489	2497	1524	java/io/FileNotFoundException
    //   2497	2613	1524	java/io/FileNotFoundException
    //   2613	2627	1524	java/io/FileNotFoundException
    //   2630	2639	1524	java/io/FileNotFoundException
    //   2642	2668	1524	java/io/FileNotFoundException
    //   2681	2690	1524	java/io/FileNotFoundException
    //   2693	2708	1524	java/io/FileNotFoundException
    //   2711	2726	1524	java/io/FileNotFoundException
    //   2729	2744	1524	java/io/FileNotFoundException
    //   2747	2762	1524	java/io/FileNotFoundException
    //   2765	2780	1524	java/io/FileNotFoundException
    //   2785	2794	1524	java/io/FileNotFoundException
    //   2801	2810	1524	java/io/FileNotFoundException
    //   2815	2829	1524	java/io/FileNotFoundException
    //   2834	2843	1524	java/io/FileNotFoundException
    //   2857	2884	1524	java/io/FileNotFoundException
    //   2906	2919	1524	java/io/FileNotFoundException
    //   2932	2952	1524	java/io/FileNotFoundException
    //   2956	3135	1524	java/io/FileNotFoundException
    //   3138	3148	1524	java/io/FileNotFoundException
    //   3152	3165	1524	java/io/FileNotFoundException
    //   3183	3203	1524	java/io/FileNotFoundException
    //   3203	3248	1524	java/io/FileNotFoundException
    //   3255	3263	1524	java/io/FileNotFoundException
    //   3267	3281	1524	java/io/FileNotFoundException
    //   3285	3299	1524	java/io/FileNotFoundException
    //   3311	3325	1524	java/io/FileNotFoundException
    //   3327	3341	1524	java/io/FileNotFoundException
    //   3346	3363	1524	java/io/FileNotFoundException
    //   3365	3391	1524	java/io/FileNotFoundException
    //   3401	3419	1524	java/io/FileNotFoundException
    //   3432	3441	1524	java/io/FileNotFoundException
    //   3444	3453	1524	java/io/FileNotFoundException
    //   3461	3470	1524	java/io/FileNotFoundException
    //   3488	3515	1524	java/io/FileNotFoundException
    //   3524	3533	1524	java/io/FileNotFoundException
    //   3536	3549	1524	java/io/FileNotFoundException
    //   3564	3584	1524	java/io/FileNotFoundException
    //   3588	3633	1524	java/io/FileNotFoundException
    //   3641	3649	1524	java/io/FileNotFoundException
    //   3654	3668	1524	java/io/FileNotFoundException
    //   3673	3687	1524	java/io/FileNotFoundException
    //   3700	3714	1524	java/io/FileNotFoundException
    //   3717	3731	1524	java/io/FileNotFoundException
    //   3737	3754	1524	java/io/FileNotFoundException
    //   3773	3787	1524	java/io/FileNotFoundException
    //   3790	3804	1524	java/io/FileNotFoundException
    //   3810	3824	1524	java/io/FileNotFoundException
    //   3827	3841	1524	java/io/FileNotFoundException
    //   3847	3861	1524	java/io/FileNotFoundException
    //   3864	3878	1524	java/io/FileNotFoundException
    //   3881	3895	1524	java/io/FileNotFoundException
    //   3898	3912	1524	java/io/FileNotFoundException
    //   3915	3952	1524	java/io/FileNotFoundException
    //   3963	3981	1524	java/io/FileNotFoundException
    //   3994	4026	1524	java/io/FileNotFoundException
    //   4035	4062	1524	java/io/FileNotFoundException
    //   4071	4080	1524	java/io/FileNotFoundException
    //   4094	4121	1524	java/io/FileNotFoundException
    //   4124	4149	1524	java/io/FileNotFoundException
    //   4151	4158	1524	java/io/FileNotFoundException
    //   4168	4177	1524	java/io/FileNotFoundException
    //   4191	4222	1524	java/io/FileNotFoundException
    //   4232	4245	1524	java/io/FileNotFoundException
    //   4260	4280	1524	java/io/FileNotFoundException
    //   4280	4325	1524	java/io/FileNotFoundException
    //   4333	4341	1524	java/io/FileNotFoundException
    //   4346	4360	1524	java/io/FileNotFoundException
    //   4365	4379	1524	java/io/FileNotFoundException
    //   4392	4406	1524	java/io/FileNotFoundException
    //   4409	4423	1524	java/io/FileNotFoundException
    //   4429	4446	1524	java/io/FileNotFoundException
    //   4465	4479	1524	java/io/FileNotFoundException
    //   4482	4496	1524	java/io/FileNotFoundException
    //   4502	4516	1524	java/io/FileNotFoundException
    //   4519	4533	1524	java/io/FileNotFoundException
    //   4539	4553	1524	java/io/FileNotFoundException
    //   4556	4570	1524	java/io/FileNotFoundException
    //   4573	4587	1524	java/io/FileNotFoundException
    //   4590	4604	1524	java/io/FileNotFoundException
    //   4607	4644	1524	java/io/FileNotFoundException
    //   4655	4673	1524	java/io/FileNotFoundException
    //   4686	4693	1524	java/io/FileNotFoundException
    //   4711	4736	1524	java/io/FileNotFoundException
    //   4739	4746	1524	java/io/FileNotFoundException
    //   4748	4755	1524	java/io/FileNotFoundException
    //   4765	4772	1524	java/io/FileNotFoundException
    //   4786	4798	1524	java/io/FileNotFoundException
    //   4798	4824	1524	java/io/FileNotFoundException
    //   4824	4829	1524	java/io/FileNotFoundException
    //   4839	4852	1524	java/io/FileNotFoundException
    //   4865	4885	1524	java/io/FileNotFoundException
    //   4889	4954	1524	java/io/FileNotFoundException
    //   4954	4965	1524	java/io/FileNotFoundException
    //   4965	4974	1524	java/io/FileNotFoundException
    //   4979	4990	1524	java/io/FileNotFoundException
    //   4997	5006	1524	java/io/FileNotFoundException
    //   5018	5049	1524	java/io/FileNotFoundException
    //   5066	5097	1524	java/io/FileNotFoundException
    //   5097	5112	1524	java/io/FileNotFoundException
    //   5118	5144	1524	java/io/FileNotFoundException
    //   5149	5158	1524	java/io/FileNotFoundException
    //   5163	5168	1524	java/io/FileNotFoundException
    //   5171	5186	1524	java/io/FileNotFoundException
    //   5191	5217	1524	java/io/FileNotFoundException
    //   5222	5237	1524	java/io/FileNotFoundException
    //   5237	5245	1524	java/io/FileNotFoundException
    //   5248	5257	1524	java/io/FileNotFoundException
    //   86	127	1691	java/io/IOException
    //   162	192	1691	java/io/IOException
    //   195	202	1691	java/io/IOException
    //   211	231	1691	java/io/IOException
    //   246	274	1691	java/io/IOException
    //   277	291	1691	java/io/IOException
    //   291	323	1691	java/io/IOException
    //   333	364	1691	java/io/IOException
    //   372	386	1691	java/io/IOException
    //   394	408	1691	java/io/IOException
    //   416	472	1691	java/io/IOException
    //   475	493	1691	java/io/IOException
    //   496	524	1691	java/io/IOException
    //   524	528	1691	java/io/IOException
    //   528	546	1691	java/io/IOException
    //   558	570	1691	java/io/IOException
    //   570	590	1691	java/io/IOException
    //   594	644	1691	java/io/IOException
    //   657	669	1691	java/io/IOException
    //   669	689	1691	java/io/IOException
    //   689	723	1691	java/io/IOException
    //   726	749	1691	java/io/IOException
    //   754	777	1691	java/io/IOException
    //   782	805	1691	java/io/IOException
    //   810	833	1691	java/io/IOException
    //   838	860	1691	java/io/IOException
    //   872	895	1691	java/io/IOException
    //   901	914	1691	java/io/IOException
    //   919	940	1691	java/io/IOException
    //   953	967	1691	java/io/IOException
    //   974	982	1691	java/io/IOException
    //   985	1005	1691	java/io/IOException
    //   1009	1054	1691	java/io/IOException
    //   1081	1089	1691	java/io/IOException
    //   1093	1107	1691	java/io/IOException
    //   1111	1125	1691	java/io/IOException
    //   1137	1151	1691	java/io/IOException
    //   1153	1167	1691	java/io/IOException
    //   1172	1186	1691	java/io/IOException
    //   1188	1202	1691	java/io/IOException
    //   1204	1218	1691	java/io/IOException
    //   1220	1234	1691	java/io/IOException
    //   1236	1273	1691	java/io/IOException
    //   1283	1301	1691	java/io/IOException
    //   1320	1332	1691	java/io/IOException
    //   1340	1346	1691	java/io/IOException
    //   1354	1377	1691	java/io/IOException
    //   1377	1385	1691	java/io/IOException
    //   1393	1474	1691	java/io/IOException
    //   1478	1492	1691	java/io/IOException
    //   1495	1521	1691	java/io/IOException
    //   1550	1632	1691	java/io/IOException
    //   1632	1646	1691	java/io/IOException
    //   1649	1680	1691	java/io/IOException
    //   1721	1729	1691	java/io/IOException
    //   1729	1845	1691	java/io/IOException
    //   1845	1859	1691	java/io/IOException
    //   1862	1871	1691	java/io/IOException
    //   1882	1908	1691	java/io/IOException
    //   1919	1927	1691	java/io/IOException
    //   1927	2043	1691	java/io/IOException
    //   2043	2057	1691	java/io/IOException
    //   2060	2069	1691	java/io/IOException
    //   2072	2098	1691	java/io/IOException
    //   2109	2117	1691	java/io/IOException
    //   2117	2233	1691	java/io/IOException
    //   2233	2247	1691	java/io/IOException
    //   2250	2259	1691	java/io/IOException
    //   2262	2288	1691	java/io/IOException
    //   2299	2307	1691	java/io/IOException
    //   2307	2423	1691	java/io/IOException
    //   2423	2437	1691	java/io/IOException
    //   2440	2449	1691	java/io/IOException
    //   2452	2478	1691	java/io/IOException
    //   2489	2497	1691	java/io/IOException
    //   2497	2613	1691	java/io/IOException
    //   2613	2627	1691	java/io/IOException
    //   2630	2639	1691	java/io/IOException
    //   2642	2668	1691	java/io/IOException
    //   2681	2690	1691	java/io/IOException
    //   2693	2708	1691	java/io/IOException
    //   2711	2726	1691	java/io/IOException
    //   2729	2744	1691	java/io/IOException
    //   2747	2762	1691	java/io/IOException
    //   2765	2780	1691	java/io/IOException
    //   2785	2794	1691	java/io/IOException
    //   2801	2810	1691	java/io/IOException
    //   2815	2829	1691	java/io/IOException
    //   2834	2843	1691	java/io/IOException
    //   2857	2884	1691	java/io/IOException
    //   2906	2919	1691	java/io/IOException
    //   2932	2952	1691	java/io/IOException
    //   2956	3135	1691	java/io/IOException
    //   3138	3148	1691	java/io/IOException
    //   3152	3165	1691	java/io/IOException
    //   3183	3203	1691	java/io/IOException
    //   3203	3248	1691	java/io/IOException
    //   3255	3263	1691	java/io/IOException
    //   3267	3281	1691	java/io/IOException
    //   3285	3299	1691	java/io/IOException
    //   3311	3325	1691	java/io/IOException
    //   3327	3341	1691	java/io/IOException
    //   3346	3363	1691	java/io/IOException
    //   3365	3391	1691	java/io/IOException
    //   3401	3419	1691	java/io/IOException
    //   3432	3441	1691	java/io/IOException
    //   3444	3453	1691	java/io/IOException
    //   3461	3470	1691	java/io/IOException
    //   3488	3515	1691	java/io/IOException
    //   3524	3533	1691	java/io/IOException
    //   3536	3549	1691	java/io/IOException
    //   3564	3584	1691	java/io/IOException
    //   3588	3633	1691	java/io/IOException
    //   3641	3649	1691	java/io/IOException
    //   3654	3668	1691	java/io/IOException
    //   3673	3687	1691	java/io/IOException
    //   3700	3714	1691	java/io/IOException
    //   3717	3731	1691	java/io/IOException
    //   3737	3754	1691	java/io/IOException
    //   3773	3787	1691	java/io/IOException
    //   3790	3804	1691	java/io/IOException
    //   3810	3824	1691	java/io/IOException
    //   3827	3841	1691	java/io/IOException
    //   3847	3861	1691	java/io/IOException
    //   3864	3878	1691	java/io/IOException
    //   3881	3895	1691	java/io/IOException
    //   3898	3912	1691	java/io/IOException
    //   3915	3952	1691	java/io/IOException
    //   3963	3981	1691	java/io/IOException
    //   3994	4026	1691	java/io/IOException
    //   4035	4062	1691	java/io/IOException
    //   4071	4080	1691	java/io/IOException
    //   4094	4121	1691	java/io/IOException
    //   4124	4149	1691	java/io/IOException
    //   4151	4158	1691	java/io/IOException
    //   4168	4177	1691	java/io/IOException
    //   4191	4222	1691	java/io/IOException
    //   4232	4245	1691	java/io/IOException
    //   4260	4280	1691	java/io/IOException
    //   4280	4325	1691	java/io/IOException
    //   4333	4341	1691	java/io/IOException
    //   4346	4360	1691	java/io/IOException
    //   4365	4379	1691	java/io/IOException
    //   4392	4406	1691	java/io/IOException
    //   4409	4423	1691	java/io/IOException
    //   4429	4446	1691	java/io/IOException
    //   4465	4479	1691	java/io/IOException
    //   4482	4496	1691	java/io/IOException
    //   4502	4516	1691	java/io/IOException
    //   4519	4533	1691	java/io/IOException
    //   4539	4553	1691	java/io/IOException
    //   4556	4570	1691	java/io/IOException
    //   4573	4587	1691	java/io/IOException
    //   4590	4604	1691	java/io/IOException
    //   4607	4644	1691	java/io/IOException
    //   4655	4673	1691	java/io/IOException
    //   4686	4693	1691	java/io/IOException
    //   4711	4736	1691	java/io/IOException
    //   4739	4746	1691	java/io/IOException
    //   4748	4755	1691	java/io/IOException
    //   4765	4772	1691	java/io/IOException
    //   4786	4798	1691	java/io/IOException
    //   4798	4824	1691	java/io/IOException
    //   4824	4829	1691	java/io/IOException
    //   4839	4852	1691	java/io/IOException
    //   4865	4885	1691	java/io/IOException
    //   4889	4954	1691	java/io/IOException
    //   4954	4965	1691	java/io/IOException
    //   4965	4974	1691	java/io/IOException
    //   4979	4990	1691	java/io/IOException
    //   4997	5006	1691	java/io/IOException
    //   5018	5049	1691	java/io/IOException
    //   5066	5097	1691	java/io/IOException
    //   5097	5112	1691	java/io/IOException
    //   5118	5144	1691	java/io/IOException
    //   5149	5158	1691	java/io/IOException
    //   5163	5168	1691	java/io/IOException
    //   5171	5186	1691	java/io/IOException
    //   5191	5217	1691	java/io/IOException
    //   5222	5237	1691	java/io/IOException
    //   5237	5245	1691	java/io/IOException
    //   5248	5257	1691	java/io/IOException
    //   86	127	1874	java/lang/InterruptedException
    //   162	192	1874	java/lang/InterruptedException
    //   195	202	1874	java/lang/InterruptedException
    //   211	231	1874	java/lang/InterruptedException
    //   246	274	1874	java/lang/InterruptedException
    //   277	291	1874	java/lang/InterruptedException
    //   291	323	1874	java/lang/InterruptedException
    //   333	364	1874	java/lang/InterruptedException
    //   372	386	1874	java/lang/InterruptedException
    //   394	408	1874	java/lang/InterruptedException
    //   416	472	1874	java/lang/InterruptedException
    //   475	493	1874	java/lang/InterruptedException
    //   496	524	1874	java/lang/InterruptedException
    //   524	528	1874	java/lang/InterruptedException
    //   528	546	1874	java/lang/InterruptedException
    //   558	570	1874	java/lang/InterruptedException
    //   570	590	1874	java/lang/InterruptedException
    //   594	644	1874	java/lang/InterruptedException
    //   657	669	1874	java/lang/InterruptedException
    //   669	689	1874	java/lang/InterruptedException
    //   689	723	1874	java/lang/InterruptedException
    //   726	749	1874	java/lang/InterruptedException
    //   754	777	1874	java/lang/InterruptedException
    //   782	805	1874	java/lang/InterruptedException
    //   810	833	1874	java/lang/InterruptedException
    //   838	860	1874	java/lang/InterruptedException
    //   872	895	1874	java/lang/InterruptedException
    //   901	914	1874	java/lang/InterruptedException
    //   919	940	1874	java/lang/InterruptedException
    //   953	967	1874	java/lang/InterruptedException
    //   974	982	1874	java/lang/InterruptedException
    //   985	1005	1874	java/lang/InterruptedException
    //   1009	1054	1874	java/lang/InterruptedException
    //   1081	1089	1874	java/lang/InterruptedException
    //   1093	1107	1874	java/lang/InterruptedException
    //   1111	1125	1874	java/lang/InterruptedException
    //   1137	1151	1874	java/lang/InterruptedException
    //   1153	1167	1874	java/lang/InterruptedException
    //   1172	1186	1874	java/lang/InterruptedException
    //   1188	1202	1874	java/lang/InterruptedException
    //   1204	1218	1874	java/lang/InterruptedException
    //   1220	1234	1874	java/lang/InterruptedException
    //   1236	1273	1874	java/lang/InterruptedException
    //   1283	1301	1874	java/lang/InterruptedException
    //   1320	1332	1874	java/lang/InterruptedException
    //   1340	1346	1874	java/lang/InterruptedException
    //   1354	1377	1874	java/lang/InterruptedException
    //   1377	1385	1874	java/lang/InterruptedException
    //   1393	1474	1874	java/lang/InterruptedException
    //   1478	1492	1874	java/lang/InterruptedException
    //   1495	1521	1874	java/lang/InterruptedException
    //   1550	1632	1874	java/lang/InterruptedException
    //   1632	1646	1874	java/lang/InterruptedException
    //   1649	1680	1874	java/lang/InterruptedException
    //   1721	1729	1874	java/lang/InterruptedException
    //   1729	1845	1874	java/lang/InterruptedException
    //   1845	1859	1874	java/lang/InterruptedException
    //   1862	1871	1874	java/lang/InterruptedException
    //   1882	1908	1874	java/lang/InterruptedException
    //   1919	1927	1874	java/lang/InterruptedException
    //   1927	2043	1874	java/lang/InterruptedException
    //   2043	2057	1874	java/lang/InterruptedException
    //   2060	2069	1874	java/lang/InterruptedException
    //   2072	2098	1874	java/lang/InterruptedException
    //   2109	2117	1874	java/lang/InterruptedException
    //   2117	2233	1874	java/lang/InterruptedException
    //   2233	2247	1874	java/lang/InterruptedException
    //   2250	2259	1874	java/lang/InterruptedException
    //   2262	2288	1874	java/lang/InterruptedException
    //   2299	2307	1874	java/lang/InterruptedException
    //   2307	2423	1874	java/lang/InterruptedException
    //   2423	2437	1874	java/lang/InterruptedException
    //   2440	2449	1874	java/lang/InterruptedException
    //   2452	2478	1874	java/lang/InterruptedException
    //   2489	2497	1874	java/lang/InterruptedException
    //   2497	2613	1874	java/lang/InterruptedException
    //   2613	2627	1874	java/lang/InterruptedException
    //   2630	2639	1874	java/lang/InterruptedException
    //   2642	2668	1874	java/lang/InterruptedException
    //   2681	2690	1874	java/lang/InterruptedException
    //   2693	2708	1874	java/lang/InterruptedException
    //   2711	2726	1874	java/lang/InterruptedException
    //   2729	2744	1874	java/lang/InterruptedException
    //   2747	2762	1874	java/lang/InterruptedException
    //   2765	2780	1874	java/lang/InterruptedException
    //   2785	2794	1874	java/lang/InterruptedException
    //   2801	2810	1874	java/lang/InterruptedException
    //   2815	2829	1874	java/lang/InterruptedException
    //   2834	2843	1874	java/lang/InterruptedException
    //   2857	2884	1874	java/lang/InterruptedException
    //   2906	2919	1874	java/lang/InterruptedException
    //   2932	2952	1874	java/lang/InterruptedException
    //   2956	3135	1874	java/lang/InterruptedException
    //   3138	3148	1874	java/lang/InterruptedException
    //   3152	3165	1874	java/lang/InterruptedException
    //   3183	3203	1874	java/lang/InterruptedException
    //   3203	3248	1874	java/lang/InterruptedException
    //   3255	3263	1874	java/lang/InterruptedException
    //   3267	3281	1874	java/lang/InterruptedException
    //   3285	3299	1874	java/lang/InterruptedException
    //   3311	3325	1874	java/lang/InterruptedException
    //   3327	3341	1874	java/lang/InterruptedException
    //   3346	3363	1874	java/lang/InterruptedException
    //   3365	3391	1874	java/lang/InterruptedException
    //   3401	3419	1874	java/lang/InterruptedException
    //   3432	3441	1874	java/lang/InterruptedException
    //   3444	3453	1874	java/lang/InterruptedException
    //   3461	3470	1874	java/lang/InterruptedException
    //   3488	3515	1874	java/lang/InterruptedException
    //   3524	3533	1874	java/lang/InterruptedException
    //   3536	3549	1874	java/lang/InterruptedException
    //   3564	3584	1874	java/lang/InterruptedException
    //   3588	3633	1874	java/lang/InterruptedException
    //   3641	3649	1874	java/lang/InterruptedException
    //   3654	3668	1874	java/lang/InterruptedException
    //   3673	3687	1874	java/lang/InterruptedException
    //   3700	3714	1874	java/lang/InterruptedException
    //   3717	3731	1874	java/lang/InterruptedException
    //   3737	3754	1874	java/lang/InterruptedException
    //   3773	3787	1874	java/lang/InterruptedException
    //   3790	3804	1874	java/lang/InterruptedException
    //   3810	3824	1874	java/lang/InterruptedException
    //   3827	3841	1874	java/lang/InterruptedException
    //   3847	3861	1874	java/lang/InterruptedException
    //   3864	3878	1874	java/lang/InterruptedException
    //   3881	3895	1874	java/lang/InterruptedException
    //   3898	3912	1874	java/lang/InterruptedException
    //   3915	3952	1874	java/lang/InterruptedException
    //   3963	3981	1874	java/lang/InterruptedException
    //   3994	4026	1874	java/lang/InterruptedException
    //   4035	4062	1874	java/lang/InterruptedException
    //   4071	4080	1874	java/lang/InterruptedException
    //   4094	4121	1874	java/lang/InterruptedException
    //   4124	4149	1874	java/lang/InterruptedException
    //   4151	4158	1874	java/lang/InterruptedException
    //   4168	4177	1874	java/lang/InterruptedException
    //   4191	4222	1874	java/lang/InterruptedException
    //   4232	4245	1874	java/lang/InterruptedException
    //   4260	4280	1874	java/lang/InterruptedException
    //   4280	4325	1874	java/lang/InterruptedException
    //   4333	4341	1874	java/lang/InterruptedException
    //   4346	4360	1874	java/lang/InterruptedException
    //   4365	4379	1874	java/lang/InterruptedException
    //   4392	4406	1874	java/lang/InterruptedException
    //   4409	4423	1874	java/lang/InterruptedException
    //   4429	4446	1874	java/lang/InterruptedException
    //   4465	4479	1874	java/lang/InterruptedException
    //   4482	4496	1874	java/lang/InterruptedException
    //   4502	4516	1874	java/lang/InterruptedException
    //   4519	4533	1874	java/lang/InterruptedException
    //   4539	4553	1874	java/lang/InterruptedException
    //   4556	4570	1874	java/lang/InterruptedException
    //   4573	4587	1874	java/lang/InterruptedException
    //   4590	4604	1874	java/lang/InterruptedException
    //   4607	4644	1874	java/lang/InterruptedException
    //   4655	4673	1874	java/lang/InterruptedException
    //   4686	4693	1874	java/lang/InterruptedException
    //   4711	4736	1874	java/lang/InterruptedException
    //   4739	4746	1874	java/lang/InterruptedException
    //   4748	4755	1874	java/lang/InterruptedException
    //   4765	4772	1874	java/lang/InterruptedException
    //   4786	4798	1874	java/lang/InterruptedException
    //   4798	4824	1874	java/lang/InterruptedException
    //   4824	4829	1874	java/lang/InterruptedException
    //   4839	4852	1874	java/lang/InterruptedException
    //   4865	4885	1874	java/lang/InterruptedException
    //   4889	4954	1874	java/lang/InterruptedException
    //   4954	4965	1874	java/lang/InterruptedException
    //   4965	4974	1874	java/lang/InterruptedException
    //   4979	4990	1874	java/lang/InterruptedException
    //   4997	5006	1874	java/lang/InterruptedException
    //   5018	5049	1874	java/lang/InterruptedException
    //   5066	5097	1874	java/lang/InterruptedException
    //   5097	5112	1874	java/lang/InterruptedException
    //   5118	5144	1874	java/lang/InterruptedException
    //   5149	5158	1874	java/lang/InterruptedException
    //   5163	5168	1874	java/lang/InterruptedException
    //   5171	5186	1874	java/lang/InterruptedException
    //   5191	5217	1874	java/lang/InterruptedException
    //   5222	5237	1874	java/lang/InterruptedException
    //   5237	5245	1874	java/lang/InterruptedException
    //   5248	5257	1874	java/lang/InterruptedException
    //   570	590	2679	org/json/JSONException
    //   669	689	2783	org/json/JSONException
    //   919	940	2813	org/json/JSONException
    //   985	1005	2832	org/json/JSONException
    //   1081	1089	2855	java/lang/Exception
    //   1093	1107	2855	java/lang/Exception
    //   1111	1125	2855	java/lang/Exception
    //   1137	1151	2855	java/lang/Exception
    //   1153	1167	2855	java/lang/Exception
    //   1172	1186	2855	java/lang/Exception
    //   1188	1202	2855	java/lang/Exception
    //   1204	1218	2855	java/lang/Exception
    //   1220	1234	2855	java/lang/Exception
    //   1236	1273	2855	java/lang/Exception
    //   1283	1301	2855	java/lang/Exception
    //   2932	2952	3430	org/json/JSONException
    //   3183	3203	3459	org/json/JSONException
    //   3255	3263	3486	java/lang/Exception
    //   3267	3281	3486	java/lang/Exception
    //   3285	3299	3486	java/lang/Exception
    //   3311	3325	3486	java/lang/Exception
    //   3327	3341	3486	java/lang/Exception
    //   3346	3363	3486	java/lang/Exception
    //   3365	3391	3486	java/lang/Exception
    //   3401	3419	3486	java/lang/Exception
    //   3994	4026	4033	java/lang/Exception
    //   3564	3584	4069	org/json/JSONException
    //   3641	3649	4092	java/lang/Exception
    //   3654	3668	4092	java/lang/Exception
    //   3673	3687	4092	java/lang/Exception
    //   3700	3714	4092	java/lang/Exception
    //   3717	3731	4092	java/lang/Exception
    //   3737	3754	4092	java/lang/Exception
    //   3773	3787	4092	java/lang/Exception
    //   3790	3804	4092	java/lang/Exception
    //   3810	3824	4092	java/lang/Exception
    //   3827	3841	4092	java/lang/Exception
    //   3847	3861	4092	java/lang/Exception
    //   3864	3878	4092	java/lang/Exception
    //   3881	3895	4092	java/lang/Exception
    //   3898	3912	4092	java/lang/Exception
    //   3915	3952	4092	java/lang/Exception
    //   3963	3981	4092	java/lang/Exception
    //   4260	4280	4684	org/json/JSONException
    //   4333	4341	4709	java/lang/Exception
    //   4346	4360	4709	java/lang/Exception
    //   4365	4379	4709	java/lang/Exception
    //   4392	4406	4709	java/lang/Exception
    //   4409	4423	4709	java/lang/Exception
    //   4429	4446	4709	java/lang/Exception
    //   4465	4479	4709	java/lang/Exception
    //   4482	4496	4709	java/lang/Exception
    //   4502	4516	4709	java/lang/Exception
    //   4519	4533	4709	java/lang/Exception
    //   4539	4553	4709	java/lang/Exception
    //   4556	4570	4709	java/lang/Exception
    //   4573	4587	4709	java/lang/Exception
    //   4590	4604	4709	java/lang/Exception
    //   4607	4644	4709	java/lang/Exception
    //   4655	4673	4709	java/lang/Exception
    //   4865	4885	5147	org/json/JSONException
    //   4954	4965	5161	java/lang/Exception
    //   4965	4974	5161	java/lang/Exception
    //   1543	1548	5260	java/io/IOException
  }
  
  /* Error */
  public static boolean patchProcess(ArrayList<PatchesItem> paramArrayList)
  {
    // Byte code:
    //   0: new 664	java/io/RandomAccessFile
    //   3: dup
    //   4: getstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   7: ldc_w 666
    //   10: invokespecial 669	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   13: invokevirtual 673	java/io/RandomAccessFile:getChannel	()Ljava/nio/channels/FileChannel;
    //   16: astore 8
    //   18: aload 8
    //   20: getstatic 679	java/nio/channels/FileChannel$MapMode:READ_WRITE	Ljava/nio/channels/FileChannel$MapMode;
    //   23: lconst_0
    //   24: aload 8
    //   26: invokevirtual 683	java/nio/channels/FileChannel:size	()J
    //   29: l2i
    //   30: i2l
    //   31: invokevirtual 687	java/nio/channels/FileChannel:map	(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    //   34: astore 7
    //   36: aload_0
    //   37: invokevirtual 691	java/util/ArrayList:toArray	()[Ljava/lang/Object;
    //   40: arraylength
    //   41: anewarray 616	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
    //   44: astore 9
    //   46: aload_0
    //   47: aload_0
    //   48: invokevirtual 279	java/util/ArrayList:size	()I
    //   51: anewarray 616	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
    //   54: invokevirtual 694	java/util/ArrayList:toArray	([Ljava/lang/Object;)[Ljava/lang/Object;
    //   57: checkcast 696	[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    //   60: checkcast 696	[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    //   63: astore 9
    //   65: iconst_m1
    //   66: istore 4
    //   68: aload 7
    //   70: invokevirtual 701	java/nio/MappedByteBuffer:hasRemaining	()Z
    //   73: ifeq +889 -> 962
    //   76: aload 7
    //   78: iload 4
    //   80: iconst_1
    //   81: iadd
    //   82: invokevirtual 705	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   85: pop
    //   86: aload 7
    //   88: invokevirtual 707	java/nio/MappedByteBuffer:position	()I
    //   91: istore 5
    //   93: aload 7
    //   95: invokevirtual 710	java/nio/MappedByteBuffer:get	()B
    //   98: istore_2
    //   99: iconst_0
    //   100: istore_3
    //   101: aload 7
    //   103: astore_0
    //   104: iload 5
    //   106: istore 4
    //   108: aload_0
    //   109: astore 7
    //   111: iload_3
    //   112: aload 9
    //   114: arraylength
    //   115: if_icmpge -47 -> 68
    //   118: aload_0
    //   119: iload 5
    //   121: invokevirtual 705	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   124: pop
    //   125: iload_2
    //   126: aload 9
    //   128: iload_3
    //   129: aaload
    //   130: getfield 713	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origByte	[B
    //   133: iconst_0
    //   134: baload
    //   135: if_icmpeq +62 -> 197
    //   138: aload 9
    //   140: iload_3
    //   141: aaload
    //   142: getfield 717	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origMask	[I
    //   145: iconst_0
    //   146: iaload
    //   147: iconst_1
    //   148: if_icmpeq +49 -> 197
    //   151: aload_0
    //   152: astore 7
    //   154: aload 9
    //   156: iload_3
    //   157: aaload
    //   158: getfield 717	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origMask	[I
    //   161: iconst_0
    //   162: iaload
    //   163: iconst_1
    //   164: if_icmple +1307 -> 1471
    //   167: aload_0
    //   168: astore 7
    //   170: iload_2
    //   171: getstatic 77	com/chelpus/root/utils/createapkcustom:search	Ljava/util/ArrayList;
    //   174: aload 9
    //   176: iload_3
    //   177: aaload
    //   178: getfield 717	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origMask	[I
    //   181: iconst_0
    //   182: iaload
    //   183: iconst_2
    //   184: isub
    //   185: invokevirtual 720	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   188: checkcast 722	java/lang/Byte
    //   191: invokevirtual 723	java/lang/Byte:byteValue	()B
    //   194: if_icmpne +1277 -> 1471
    //   197: aload 9
    //   199: iload_3
    //   200: aaload
    //   201: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   204: iconst_0
    //   205: iaload
    //   206: ifne +13 -> 219
    //   209: aload 9
    //   211: iload_3
    //   212: aaload
    //   213: getfield 727	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   216: iconst_0
    //   217: iload_2
    //   218: bastore
    //   219: aload 9
    //   221: iload_3
    //   222: aaload
    //   223: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   226: iconst_0
    //   227: iaload
    //   228: iconst_1
    //   229: if_icmple +50 -> 279
    //   232: aload 9
    //   234: iload_3
    //   235: aaload
    //   236: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   239: iconst_0
    //   240: iaload
    //   241: sipush 253
    //   244: if_icmpge +35 -> 279
    //   247: aload 9
    //   249: iload_3
    //   250: aaload
    //   251: getfield 727	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   254: iconst_0
    //   255: getstatic 77	com/chelpus/root/utils/createapkcustom:search	Ljava/util/ArrayList;
    //   258: aload 9
    //   260: iload_3
    //   261: aaload
    //   262: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   265: iconst_0
    //   266: iaload
    //   267: iconst_2
    //   268: isub
    //   269: invokevirtual 720	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   272: checkcast 722	java/lang/Byte
    //   275: invokevirtual 723	java/lang/Byte:byteValue	()B
    //   278: bastore
    //   279: aload 9
    //   281: iload_3
    //   282: aaload
    //   283: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   286: iconst_0
    //   287: iaload
    //   288: sipush 253
    //   291: if_icmpne +25 -> 316
    //   294: aload 9
    //   296: iload_3
    //   297: aaload
    //   298: getfield 727	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   301: iconst_0
    //   302: iload_2
    //   303: bipush 15
    //   305: iand
    //   306: iload_2
    //   307: bipush 15
    //   309: iand
    //   310: bipush 16
    //   312: imul
    //   313: iadd
    //   314: i2b
    //   315: bastore
    //   316: aload 9
    //   318: iload_3
    //   319: aaload
    //   320: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   323: iconst_0
    //   324: iaload
    //   325: sipush 254
    //   328: if_icmpne +20 -> 348
    //   331: aload 9
    //   333: iload_3
    //   334: aaload
    //   335: getfield 727	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   338: iconst_0
    //   339: iload_2
    //   340: bipush 15
    //   342: iand
    //   343: bipush 16
    //   345: iadd
    //   346: i2b
    //   347: bastore
    //   348: aload 9
    //   350: iload_3
    //   351: aaload
    //   352: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   355: iconst_0
    //   356: iaload
    //   357: sipush 255
    //   360: if_icmpne +17 -> 377
    //   363: aload 9
    //   365: iload_3
    //   366: aaload
    //   367: getfield 727	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   370: iconst_0
    //   371: iload_2
    //   372: bipush 15
    //   374: iand
    //   375: i2b
    //   376: bastore
    //   377: iconst_1
    //   378: istore 4
    //   380: aload_0
    //   381: iload 5
    //   383: iconst_1
    //   384: iadd
    //   385: invokevirtual 705	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   388: pop
    //   389: aload_0
    //   390: invokevirtual 710	java/nio/MappedByteBuffer:get	()B
    //   393: istore_1
    //   394: iload_1
    //   395: aload 9
    //   397: iload_3
    //   398: aaload
    //   399: getfield 713	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origByte	[B
    //   402: iload 4
    //   404: baload
    //   405: if_icmpeq +66 -> 471
    //   408: aload 9
    //   410: iload_3
    //   411: aaload
    //   412: getfield 717	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origMask	[I
    //   415: iload 4
    //   417: iaload
    //   418: iconst_1
    //   419: if_icmple +31 -> 450
    //   422: iload_1
    //   423: getstatic 77	com/chelpus/root/utils/createapkcustom:search	Ljava/util/ArrayList;
    //   426: aload 9
    //   428: iload_3
    //   429: aaload
    //   430: getfield 717	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origMask	[I
    //   433: iload 4
    //   435: iaload
    //   436: iconst_2
    //   437: isub
    //   438: invokevirtual 720	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   441: checkcast 722	java/lang/Byte
    //   444: invokevirtual 723	java/lang/Byte:byteValue	()B
    //   447: if_icmpeq +24 -> 471
    //   450: aload 9
    //   452: iload_3
    //   453: aaload
    //   454: getfield 717	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origMask	[I
    //   457: iload 4
    //   459: iaload
    //   460: istore 6
    //   462: aload_0
    //   463: astore 7
    //   465: iload 6
    //   467: iconst_1
    //   468: if_icmpne +1003 -> 1471
    //   471: aload 9
    //   473: iload_3
    //   474: aaload
    //   475: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   478: iload 4
    //   480: iaload
    //   481: ifne +14 -> 495
    //   484: aload 9
    //   486: iload_3
    //   487: aaload
    //   488: getfield 727	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   491: iload 4
    //   493: iload_1
    //   494: bastore
    //   495: aload 9
    //   497: iload_3
    //   498: aaload
    //   499: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   502: iload 4
    //   504: iaload
    //   505: iconst_1
    //   506: if_icmple +57 -> 563
    //   509: aload 9
    //   511: iload_3
    //   512: aaload
    //   513: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   516: iload 4
    //   518: iaload
    //   519: sipush 253
    //   522: if_icmpge +41 -> 563
    //   525: aload 9
    //   527: iload_3
    //   528: aaload
    //   529: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   532: iload 4
    //   534: iaload
    //   535: istore 6
    //   537: aload 9
    //   539: iload_3
    //   540: aaload
    //   541: getfield 727	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   544: iload 4
    //   546: getstatic 77	com/chelpus/root/utils/createapkcustom:search	Ljava/util/ArrayList;
    //   549: iload 6
    //   551: iconst_2
    //   552: isub
    //   553: invokevirtual 720	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   556: checkcast 722	java/lang/Byte
    //   559: invokevirtual 723	java/lang/Byte:byteValue	()B
    //   562: bastore
    //   563: aload 9
    //   565: iload_3
    //   566: aaload
    //   567: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   570: iconst_0
    //   571: iaload
    //   572: sipush 253
    //   575: if_icmpne +25 -> 600
    //   578: aload 9
    //   580: iload_3
    //   581: aaload
    //   582: getfield 727	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   585: iconst_0
    //   586: iload_2
    //   587: bipush 15
    //   589: iand
    //   590: iload_2
    //   591: bipush 15
    //   593: iand
    //   594: bipush 16
    //   596: imul
    //   597: iadd
    //   598: i2b
    //   599: bastore
    //   600: aload 9
    //   602: iload_3
    //   603: aaload
    //   604: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   607: iload 4
    //   609: iaload
    //   610: sipush 254
    //   613: if_icmpne +21 -> 634
    //   616: aload 9
    //   618: iload_3
    //   619: aaload
    //   620: getfield 727	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   623: iload 4
    //   625: iload_1
    //   626: bipush 15
    //   628: iand
    //   629: bipush 16
    //   631: iadd
    //   632: i2b
    //   633: bastore
    //   634: aload 9
    //   636: iload_3
    //   637: aaload
    //   638: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   641: iload 4
    //   643: iaload
    //   644: sipush 255
    //   647: if_icmpne +18 -> 665
    //   650: aload 9
    //   652: iload_3
    //   653: aaload
    //   654: getfield 727	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   657: iload 4
    //   659: iload_1
    //   660: bipush 15
    //   662: iand
    //   663: i2b
    //   664: bastore
    //   665: iload 4
    //   667: iconst_1
    //   668: iadd
    //   669: istore 4
    //   671: iload 4
    //   673: aload 9
    //   675: iload_3
    //   676: aaload
    //   677: getfield 713	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origByte	[B
    //   680: arraylength
    //   681: if_icmpne +476 -> 1157
    //   684: aload_0
    //   685: astore 7
    //   687: aload 9
    //   689: iload_3
    //   690: aaload
    //   691: getfield 729	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:insert	Z
    //   694: ifeq +102 -> 796
    //   697: aload_0
    //   698: invokevirtual 707	java/nio/MappedByteBuffer:position	()I
    //   701: istore 4
    //   703: aload 8
    //   705: invokevirtual 683	java/nio/channels/FileChannel:size	()J
    //   708: l2i
    //   709: iload 4
    //   711: isub
    //   712: istore 6
    //   714: iload 6
    //   716: newarray <illegal type>
    //   718: astore 7
    //   720: aload_0
    //   721: aload 7
    //   723: iconst_0
    //   724: iload 6
    //   726: invokevirtual 732	java/nio/MappedByteBuffer:get	([BII)Ljava/nio/ByteBuffer;
    //   729: pop
    //   730: aload 7
    //   732: invokestatic 738	java/nio/ByteBuffer:wrap	([B)Ljava/nio/ByteBuffer;
    //   735: astore_0
    //   736: aload 8
    //   738: aload 9
    //   740: iload_3
    //   741: aaload
    //   742: getfield 727	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   745: arraylength
    //   746: aload 9
    //   748: iload_3
    //   749: aaload
    //   750: getfield 713	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:origByte	[B
    //   753: arraylength
    //   754: isub
    //   755: iload 4
    //   757: iadd
    //   758: i2l
    //   759: invokevirtual 741	java/nio/channels/FileChannel:position	(J)Ljava/nio/channels/FileChannel;
    //   762: pop
    //   763: aload 8
    //   765: aload_0
    //   766: invokevirtual 744	java/nio/channels/FileChannel:write	(Ljava/nio/ByteBuffer;)I
    //   769: pop
    //   770: aload 8
    //   772: getstatic 679	java/nio/channels/FileChannel$MapMode:READ_WRITE	Ljava/nio/channels/FileChannel$MapMode;
    //   775: lconst_0
    //   776: aload 8
    //   778: invokevirtual 683	java/nio/channels/FileChannel:size	()J
    //   781: l2i
    //   782: i2l
    //   783: invokevirtual 687	java/nio/channels/FileChannel:map	(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    //   786: astore 7
    //   788: aload 7
    //   790: iload 4
    //   792: invokevirtual 705	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   795: pop
    //   796: aload 8
    //   798: iload 5
    //   800: i2l
    //   801: invokevirtual 741	java/nio/channels/FileChannel:position	(J)Ljava/nio/channels/FileChannel;
    //   804: pop
    //   805: aload 8
    //   807: aload 9
    //   809: iload_3
    //   810: aaload
    //   811: getfield 727	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repByte	[B
    //   814: invokestatic 738	java/nio/ByteBuffer:wrap	([B)Ljava/nio/ByteBuffer;
    //   817: invokevirtual 744	java/nio/channels/FileChannel:write	(Ljava/nio/ByteBuffer;)I
    //   820: pop
    //   821: aload 7
    //   823: invokevirtual 748	java/nio/MappedByteBuffer:force	()Ljava/nio/MappedByteBuffer;
    //   826: pop
    //   827: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   830: new 159	java/lang/StringBuilder
    //   833: dup
    //   834: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   837: ldc_w 750
    //   840: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   843: iload_3
    //   844: iconst_1
    //   845: iadd
    //   846: invokevirtual 169	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   849: ldc_w 752
    //   852: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   855: iload 5
    //   857: invokestatic 756	java/lang/Integer:toHexString	(I)Ljava/lang/String;
    //   860: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   863: ldc_w 758
    //   866: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   869: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   872: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   875: aload 9
    //   877: iload_3
    //   878: aaload
    //   879: iconst_1
    //   880: putfield 761	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:result	Z
    //   883: iconst_1
    //   884: putstatic 79	com/chelpus/root/utils/createapkcustom:patchteil	Z
    //   887: goto +584 -> 1471
    //   890: astore 7
    //   892: aload 9
    //   894: iload_3
    //   895: aaload
    //   896: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   899: iconst_0
    //   900: iaload
    //   901: iconst_2
    //   902: isub
    //   903: istore 4
    //   905: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   908: new 159	java/lang/StringBuilder
    //   911: dup
    //   912: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   915: ldc_w 763
    //   918: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   921: iload 4
    //   923: invokevirtual 169	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   926: ldc_w 765
    //   929: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   932: iload 4
    //   934: invokevirtual 169	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   937: ldc_w 767
    //   940: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   943: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   946: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   949: goto -670 -> 279
    //   952: astore_0
    //   953: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   956: ldc_w 769
    //   959: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   962: aload 8
    //   964: invokevirtual 770	java/nio/channels/FileChannel:close	()V
    //   967: iconst_0
    //   968: istore 4
    //   970: iload 4
    //   972: aload 9
    //   974: arraylength
    //   975: if_icmpge +440 -> 1415
    //   978: aload 9
    //   980: iload 4
    //   982: aaload
    //   983: getfield 761	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:result	Z
    //   986: ifne +384 -> 1370
    //   989: iconst_0
    //   990: istore 6
    //   992: iconst_0
    //   993: istore_3
    //   994: aload 9
    //   996: iload 4
    //   998: aaload
    //   999: getfield 771	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:group	Ljava/lang/String;
    //   1002: ldc 97
    //   1004: invokevirtual 349	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1007: ifne +158 -> 1165
    //   1010: iconst_0
    //   1011: istore 5
    //   1013: iload_3
    //   1014: istore 6
    //   1016: iload 5
    //   1018: aload 9
    //   1020: arraylength
    //   1021: if_icmpge +144 -> 1165
    //   1024: iload_3
    //   1025: istore 6
    //   1027: aload 9
    //   1029: iload 4
    //   1031: aaload
    //   1032: getfield 771	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:group	Ljava/lang/String;
    //   1035: aload 9
    //   1037: iload 5
    //   1039: aaload
    //   1040: getfield 771	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:group	Ljava/lang/String;
    //   1043: invokevirtual 349	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1046: ifeq +36 -> 1082
    //   1049: iload_3
    //   1050: istore 6
    //   1052: aload 9
    //   1054: iload 5
    //   1056: aaload
    //   1057: getfield 761	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:result	Z
    //   1060: ifeq +22 -> 1082
    //   1063: getstatic 114	com/chelpus/root/utils/createapkcustom:multidex	Z
    //   1066: ifne +9 -> 1075
    //   1069: getstatic 118	com/chelpus/root/utils/createapkcustom:multilib_patch	Z
    //   1072: ifeq +7 -> 1079
    //   1075: iconst_1
    //   1076: putstatic 116	com/chelpus/root/utils/createapkcustom:goodResult	Z
    //   1079: iconst_1
    //   1080: istore 6
    //   1082: iload 5
    //   1084: iconst_1
    //   1085: iadd
    //   1086: istore 5
    //   1088: iload 6
    //   1090: istore_3
    //   1091: goto -78 -> 1013
    //   1094: astore 7
    //   1096: aload 9
    //   1098: iload_3
    //   1099: aaload
    //   1100: getfield 726	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:repMask	[I
    //   1103: iload 4
    //   1105: iaload
    //   1106: iconst_2
    //   1107: isub
    //   1108: istore 6
    //   1110: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1113: new 159	java/lang/StringBuilder
    //   1116: dup
    //   1117: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   1120: ldc_w 763
    //   1123: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1126: iload 6
    //   1128: invokevirtual 169	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1131: ldc_w 765
    //   1134: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1137: iload 6
    //   1139: invokevirtual 169	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1142: ldc_w 767
    //   1145: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1148: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1151: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1154: goto -591 -> 563
    //   1157: aload_0
    //   1158: invokevirtual 710	java/nio/MappedByteBuffer:get	()B
    //   1161: istore_1
    //   1162: goto -768 -> 394
    //   1165: iload 6
    //   1167: ifne +318 -> 1485
    //   1170: getstatic 116	com/chelpus/root/utils/createapkcustom:goodResult	Z
    //   1173: ifne +312 -> 1485
    //   1176: getstatic 114	com/chelpus/root/utils/createapkcustom:multidex	Z
    //   1179: ifeq +70 -> 1249
    //   1182: getstatic 116	com/chelpus/root/utils/createapkcustom:goodResult	Z
    //   1185: ifne +300 -> 1485
    //   1188: getstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   1191: getstatic 112	com/chelpus/root/utils/createapkcustom:classesFiles	Ljava/util/ArrayList;
    //   1194: getstatic 112	com/chelpus/root/utils/createapkcustom:classesFiles	Ljava/util/ArrayList;
    //   1197: invokevirtual 279	java/util/ArrayList:size	()I
    //   1200: iconst_1
    //   1201: isub
    //   1202: invokevirtual 720	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   1205: invokevirtual 772	java/io/File:equals	(Ljava/lang/Object;)Z
    //   1208: ifeq +277 -> 1485
    //   1211: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1214: new 159	java/lang/StringBuilder
    //   1217: dup
    //   1218: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   1221: ldc_w 750
    //   1224: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1227: iload 4
    //   1229: iconst_1
    //   1230: iadd
    //   1231: invokevirtual 169	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1234: ldc_w 774
    //   1237: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1240: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1243: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1246: goto +239 -> 1485
    //   1249: getstatic 118	com/chelpus/root/utils/createapkcustom:multilib_patch	Z
    //   1252: ifeq +80 -> 1332
    //   1255: getstatic 116	com/chelpus/root/utils/createapkcustom:goodResult	Z
    //   1258: ifne +227 -> 1485
    //   1261: getstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   1264: new 186	java/io/File
    //   1267: dup
    //   1268: getstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   1271: getstatic 106	com/chelpus/root/utils/createapkcustom:libs	Ljava/util/ArrayList;
    //   1274: invokevirtual 279	java/util/ArrayList:size	()I
    //   1277: iconst_1
    //   1278: isub
    //   1279: invokevirtual 720	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   1282: checkcast 342	java/lang/String
    //   1285: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   1288: invokevirtual 772	java/io/File:equals	(Ljava/lang/Object;)Z
    //   1291: ifeq +194 -> 1485
    //   1294: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1297: new 159	java/lang/StringBuilder
    //   1300: dup
    //   1301: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   1304: ldc_w 750
    //   1307: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1310: iload 4
    //   1312: iconst_1
    //   1313: iadd
    //   1314: invokevirtual 169	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1317: ldc_w 774
    //   1320: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1323: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1326: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1329: goto +156 -> 1485
    //   1332: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1335: new 159	java/lang/StringBuilder
    //   1338: dup
    //   1339: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   1342: ldc_w 750
    //   1345: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1348: iload 4
    //   1350: iconst_1
    //   1351: iadd
    //   1352: invokevirtual 169	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1355: ldc_w 774
    //   1358: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1361: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1364: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1367: goto +118 -> 1485
    //   1370: getstatic 114	com/chelpus/root/utils/createapkcustom:multidex	Z
    //   1373: ifne +9 -> 1382
    //   1376: getstatic 118	com/chelpus/root/utils/createapkcustom:multilib_patch	Z
    //   1379: ifeq +106 -> 1485
    //   1382: aload 9
    //   1384: iload 4
    //   1386: aaload
    //   1387: getfield 771	com/android/vending/billing/InAppBillingService/LUCK/PatchesItem:group	Ljava/lang/String;
    //   1390: ldc 97
    //   1392: invokevirtual 349	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1395: ifne +90 -> 1485
    //   1398: iconst_1
    //   1399: putstatic 116	com/chelpus/root/utils/createapkcustom:goodResult	Z
    //   1402: goto +83 -> 1485
    //   1405: astore_0
    //   1406: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1409: ldc_w 776
    //   1412: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1415: getstatic 367	com/chelpus/root/utils/createapkcustom:tag	I
    //   1418: iconst_1
    //   1419: if_icmpne +9 -> 1428
    //   1422: getstatic 387	com/chelpus/root/utils/createapkcustom:localFile2	Ljava/io/File;
    //   1425: invokestatic 575	com/chelpus/root/utils/createapkcustom:fixadler	(Ljava/io/File;)V
    //   1428: iconst_1
    //   1429: ireturn
    //   1430: astore_0
    //   1431: getstatic 124	com/chelpus/root/utils/createapkcustom:print	Ljava/io/PrintStream;
    //   1434: new 159	java/lang/StringBuilder
    //   1437: dup
    //   1438: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   1441: ldc_w 778
    //   1444: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1447: aload_0
    //   1448: invokevirtual 208	java/lang/Exception:toString	()Ljava/lang/String;
    //   1451: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1454: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1457: invokevirtual 213	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   1460: goto -45 -> 1415
    //   1463: astore_0
    //   1464: goto -49 -> 1415
    //   1467: astore_0
    //   1468: goto -506 -> 962
    //   1471: iload_3
    //   1472: iconst_1
    //   1473: iadd
    //   1474: istore_3
    //   1475: aload 7
    //   1477: astore_0
    //   1478: goto -1374 -> 104
    //   1481: astore_0
    //   1482: goto -520 -> 962
    //   1485: iload 4
    //   1487: iconst_1
    //   1488: iadd
    //   1489: istore 4
    //   1491: goto -521 -> 970
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1494	0	paramArrayList	ArrayList<PatchesItem>
    //   393	769	1	i	int
    //   98	496	2	j	int
    //   100	1375	3	k	int
    //   66	1424	4	m	int
    //   91	996	5	n	int
    //   460	706	6	i1	int
    //   34	788	7	localObject	Object
    //   890	1	7	localException1	Exception
    //   1094	382	7	localException2	Exception
    //   16	947	8	localFileChannel	FileChannel
    //   44	1339	9	arrayOfPatchesItem	PatchesItem[]
    // Exception table:
    //   from	to	target	type
    //   219	279	890	java/lang/Exception
    //   68	99	952	java/lang/IndexOutOfBoundsException
    //   111	151	952	java/lang/IndexOutOfBoundsException
    //   154	167	952	java/lang/IndexOutOfBoundsException
    //   170	197	952	java/lang/IndexOutOfBoundsException
    //   197	219	952	java/lang/IndexOutOfBoundsException
    //   219	279	952	java/lang/IndexOutOfBoundsException
    //   279	316	952	java/lang/IndexOutOfBoundsException
    //   316	348	952	java/lang/IndexOutOfBoundsException
    //   348	377	952	java/lang/IndexOutOfBoundsException
    //   380	394	952	java/lang/IndexOutOfBoundsException
    //   394	450	952	java/lang/IndexOutOfBoundsException
    //   450	462	952	java/lang/IndexOutOfBoundsException
    //   471	495	952	java/lang/IndexOutOfBoundsException
    //   495	563	952	java/lang/IndexOutOfBoundsException
    //   563	600	952	java/lang/IndexOutOfBoundsException
    //   600	634	952	java/lang/IndexOutOfBoundsException
    //   634	665	952	java/lang/IndexOutOfBoundsException
    //   671	684	952	java/lang/IndexOutOfBoundsException
    //   687	796	952	java/lang/IndexOutOfBoundsException
    //   796	887	952	java/lang/IndexOutOfBoundsException
    //   892	949	952	java/lang/IndexOutOfBoundsException
    //   1096	1154	952	java/lang/IndexOutOfBoundsException
    //   1157	1162	952	java/lang/IndexOutOfBoundsException
    //   471	495	1094	java/lang/Exception
    //   495	563	1094	java/lang/Exception
    //   0	65	1405	java/io/FileNotFoundException
    //   68	99	1405	java/io/FileNotFoundException
    //   111	151	1405	java/io/FileNotFoundException
    //   154	167	1405	java/io/FileNotFoundException
    //   170	197	1405	java/io/FileNotFoundException
    //   197	219	1405	java/io/FileNotFoundException
    //   219	279	1405	java/io/FileNotFoundException
    //   279	316	1405	java/io/FileNotFoundException
    //   316	348	1405	java/io/FileNotFoundException
    //   348	377	1405	java/io/FileNotFoundException
    //   380	394	1405	java/io/FileNotFoundException
    //   394	450	1405	java/io/FileNotFoundException
    //   450	462	1405	java/io/FileNotFoundException
    //   471	495	1405	java/io/FileNotFoundException
    //   495	563	1405	java/io/FileNotFoundException
    //   563	600	1405	java/io/FileNotFoundException
    //   600	634	1405	java/io/FileNotFoundException
    //   634	665	1405	java/io/FileNotFoundException
    //   671	684	1405	java/io/FileNotFoundException
    //   687	796	1405	java/io/FileNotFoundException
    //   796	887	1405	java/io/FileNotFoundException
    //   892	949	1405	java/io/FileNotFoundException
    //   953	962	1405	java/io/FileNotFoundException
    //   962	967	1405	java/io/FileNotFoundException
    //   970	989	1405	java/io/FileNotFoundException
    //   994	1010	1405	java/io/FileNotFoundException
    //   1016	1024	1405	java/io/FileNotFoundException
    //   1027	1049	1405	java/io/FileNotFoundException
    //   1052	1075	1405	java/io/FileNotFoundException
    //   1075	1079	1405	java/io/FileNotFoundException
    //   1096	1154	1405	java/io/FileNotFoundException
    //   1157	1162	1405	java/io/FileNotFoundException
    //   1170	1246	1405	java/io/FileNotFoundException
    //   1249	1329	1405	java/io/FileNotFoundException
    //   1332	1367	1405	java/io/FileNotFoundException
    //   1370	1382	1405	java/io/FileNotFoundException
    //   1382	1402	1405	java/io/FileNotFoundException
    //   0	65	1430	java/lang/Exception
    //   953	962	1430	java/lang/Exception
    //   962	967	1430	java/lang/Exception
    //   970	989	1430	java/lang/Exception
    //   994	1010	1430	java/lang/Exception
    //   1016	1024	1430	java/lang/Exception
    //   1027	1049	1430	java/lang/Exception
    //   1052	1075	1430	java/lang/Exception
    //   1075	1079	1430	java/lang/Exception
    //   1170	1246	1430	java/lang/Exception
    //   1249	1329	1430	java/lang/Exception
    //   1332	1367	1430	java/lang/Exception
    //   1370	1382	1430	java/lang/Exception
    //   1382	1402	1430	java/lang/Exception
    //   0	65	1463	java/nio/BufferUnderflowException
    //   953	962	1463	java/nio/BufferUnderflowException
    //   962	967	1463	java/nio/BufferUnderflowException
    //   970	989	1463	java/nio/BufferUnderflowException
    //   994	1010	1463	java/nio/BufferUnderflowException
    //   1016	1024	1463	java/nio/BufferUnderflowException
    //   1027	1049	1463	java/nio/BufferUnderflowException
    //   1052	1075	1463	java/nio/BufferUnderflowException
    //   1075	1079	1463	java/nio/BufferUnderflowException
    //   1170	1246	1463	java/nio/BufferUnderflowException
    //   1249	1329	1463	java/nio/BufferUnderflowException
    //   1332	1367	1463	java/nio/BufferUnderflowException
    //   1370	1382	1463	java/nio/BufferUnderflowException
    //   1382	1402	1463	java/nio/BufferUnderflowException
    //   68	99	1467	java/lang/Exception
    //   111	151	1467	java/lang/Exception
    //   154	167	1467	java/lang/Exception
    //   170	197	1467	java/lang/Exception
    //   197	219	1467	java/lang/Exception
    //   279	316	1467	java/lang/Exception
    //   316	348	1467	java/lang/Exception
    //   348	377	1467	java/lang/Exception
    //   380	394	1467	java/lang/Exception
    //   394	450	1467	java/lang/Exception
    //   450	462	1467	java/lang/Exception
    //   563	600	1467	java/lang/Exception
    //   600	634	1467	java/lang/Exception
    //   634	665	1467	java/lang/Exception
    //   671	684	1467	java/lang/Exception
    //   687	796	1467	java/lang/Exception
    //   796	887	1467	java/lang/Exception
    //   892	949	1467	java/lang/Exception
    //   1096	1154	1467	java/lang/Exception
    //   1157	1162	1467	java/lang/Exception
    //   68	99	1481	java/nio/BufferUnderflowException
    //   111	151	1481	java/nio/BufferUnderflowException
    //   154	167	1481	java/nio/BufferUnderflowException
    //   170	197	1481	java/nio/BufferUnderflowException
    //   197	219	1481	java/nio/BufferUnderflowException
    //   219	279	1481	java/nio/BufferUnderflowException
    //   279	316	1481	java/nio/BufferUnderflowException
    //   316	348	1481	java/nio/BufferUnderflowException
    //   348	377	1481	java/nio/BufferUnderflowException
    //   380	394	1481	java/nio/BufferUnderflowException
    //   394	450	1481	java/nio/BufferUnderflowException
    //   450	462	1481	java/nio/BufferUnderflowException
    //   471	495	1481	java/nio/BufferUnderflowException
    //   495	563	1481	java/nio/BufferUnderflowException
    //   563	600	1481	java/nio/BufferUnderflowException
    //   600	634	1481	java/nio/BufferUnderflowException
    //   634	665	1481	java/nio/BufferUnderflowException
    //   671	684	1481	java/nio/BufferUnderflowException
    //   687	796	1481	java/nio/BufferUnderflowException
    //   796	887	1481	java/nio/BufferUnderflowException
    //   892	949	1481	java/nio/BufferUnderflowException
    //   1096	1154	1481	java/nio/BufferUnderflowException
    //   1157	1162	1481	java/nio/BufferUnderflowException
  }
  
  public static boolean searchProcess(ArrayList<SearchItem> paramArrayList)
  {
    boolean bool5 = true;
    boolean bool6 = true;
    boolean bool7 = true;
    boolean bool1 = true;
    boolean bool2 = bool5;
    boolean bool3 = bool6;
    boolean bool4 = bool7;
    int k;
    int m;
    try
    {
      FileChannel localFileChannel = new RandomAccessFile(localFile2, "rw").getChannel();
      bool2 = bool5;
      bool3 = bool6;
      bool4 = bool7;
      MappedByteBuffer localMappedByteBuffer = localFileChannel.map(FileChannel.MapMode.READ_WRITE, 0L, (int)localFileChannel.size());
      bool2 = bool5;
      bool3 = bool6;
      bool4 = bool7;
      SearchItem[] arrayOfSearchItem = new SearchItem[paramArrayList.toArray().length];
      bool2 = bool5;
      bool3 = bool6;
      bool4 = bool7;
      paramArrayList = (SearchItem[])paramArrayList.toArray(new SearchItem[paramArrayList.size()]);
      long l = 0L;
      for (;;)
      {
        bool2 = bool5;
        bool3 = bool6;
        try
        {
          if (localMappedByteBuffer.hasRemaining())
          {
            bool2 = bool5;
            bool3 = bool6;
            n = localMappedByteBuffer.position();
            bool2 = bool5;
            bool3 = bool6;
            int j = localMappedByteBuffer.get();
            k = 0;
            bool2 = bool5;
            bool3 = bool6;
            if (k < paramArrayList.length)
            {
              bool2 = bool5;
              bool3 = bool6;
              localMappedByteBuffer.position(n);
              bool2 = bool5;
              bool3 = bool6;
              if (paramArrayList[k].result) {
                break label1125;
              }
              bool2 = bool5;
              bool3 = bool6;
              if (j != paramArrayList[k].origByte[0])
              {
                bool2 = bool5;
                bool3 = bool6;
                if (paramArrayList[k].origMask[0] == 0) {
                  break label1125;
                }
              }
              bool2 = bool5;
              bool3 = bool6;
              if (paramArrayList[k].origMask[0] != 0)
              {
                bool2 = bool5;
                bool3 = bool6;
                paramArrayList[k].repByte[0] = j;
              }
              m = 1;
              bool2 = bool5;
              bool3 = bool6;
              localMappedByteBuffer.position(n + 1);
              bool2 = bool5;
              bool3 = bool6;
              for (int i = localMappedByteBuffer.get();; i = localMappedByteBuffer.get())
              {
                bool2 = bool5;
                bool3 = bool6;
                if (!paramArrayList[k].result)
                {
                  bool2 = bool5;
                  bool3 = bool6;
                  if (i == paramArrayList[k].origByte[m]) {}
                }
                else
                {
                  bool2 = bool5;
                  bool3 = bool6;
                  if (paramArrayList[k].origMask[m] == 0) {
                    break;
                  }
                }
                bool2 = bool5;
                bool3 = bool6;
                if (paramArrayList[k].origMask[m] > 0)
                {
                  bool2 = bool5;
                  bool3 = bool6;
                  paramArrayList[k].repByte[m] = i;
                }
                m += 1;
                bool2 = bool5;
                bool3 = bool6;
                if (m == paramArrayList[k].origByte.length)
                {
                  bool2 = bool5;
                  bool3 = bool6;
                  paramArrayList[k].result = true;
                  bool2 = bool5;
                  bool3 = bool6;
                  patchteil = true;
                  break;
                }
                bool2 = bool5;
                bool3 = bool6;
              }
            }
            bool2 = bool5;
            bool3 = bool6;
            localMappedByteBuffer.position(n + 1);
            l += 1L;
          }
        }
        catch (Exception localException2)
        {
          bool2 = bool5;
          bool3 = bool6;
          bool4 = bool7;
          print.println("" + localException2);
          bool2 = bool5;
          bool3 = bool6;
          bool4 = bool7;
          localFileChannel.close();
          k = 0;
        }
      }
      label586:
      bool2 = bool1;
      bool3 = bool1;
      bool4 = bool1;
      if (k >= paramArrayList.length) {
        break label1143;
      }
      bool2 = bool1;
      bool3 = bool1;
      bool4 = bool1;
      bool5 = bool1;
      if (paramArrayList[k].result) {
        break label1132;
      }
      bool2 = bool1;
      bool3 = bool1;
      bool4 = bool1;
      print.println("Bytes by serach N" + (k + 1) + ":\nError LP: Bytes not found!");
      bool5 = false;
    }
    catch (FileNotFoundException paramArrayList)
    {
      for (;;)
      {
        int n;
        print.println("Error LP: Program files are not found!\nMove Program to internal storage.");
        bool5 = bool2;
        return bool5;
        bool2 = bool1;
        bool3 = bool1;
        bool4 = bool1;
        print.println("");
        k += 1;
      }
    }
    catch (BufferUnderflowException paramArrayList)
    {
      print.println("Exception e" + paramArrayList.toString());
      return bool3;
    }
    catch (Exception paramArrayList)
    {
      label681:
      print.println("Exception e" + paramArrayList.toString());
      return bool4;
    }
    bool2 = bool1;
    bool5 = bool1;
    bool3 = bool1;
    bool4 = bool1;
    if (k < paramArrayList.length)
    {
      bool2 = bool1;
      bool3 = bool1;
      bool4 = bool1;
      if (!paramArrayList[k].result) {
        break label1148;
      }
      bool2 = bool1;
      bool3 = bool1;
      bool4 = bool1;
      print.println("\nBytes by search N" + (k + 1) + ":");
    }
    for (;;)
    {
      bool2 = bool1;
      bool3 = bool1;
      bool4 = bool1;
      if (m < paramArrayList[k].origMask.length)
      {
        bool2 = bool1;
        bool3 = bool1;
        bool4 = bool1;
        if (paramArrayList[k].origMask[m] > 1)
        {
          bool2 = bool1;
          bool3 = bool1;
          bool4 = bool1;
          n = paramArrayList[k].origMask[m];
          n -= 2;
          bool2 = bool1;
          bool3 = bool1;
          try
          {
            search.set(n, Byte.valueOf(paramArrayList[k].repByte[m]));
            bool2 = bool1;
            bool3 = bool1;
            bool4 = bool1;
            if (!paramArrayList[k].result) {
              break label1154;
            }
            bool2 = bool1;
            bool3 = bool1;
            bool4 = bool1;
            print.print("R" + n + "=" + Integer.toHexString(((Byte)search.get(n)).byteValue()).toUpperCase() + " ");
          }
          catch (Exception localException1)
          {
            for (;;)
            {
              bool2 = bool1;
              bool3 = bool1;
              bool4 = bool1;
              search.add(n, Byte.valueOf(paramArrayList[k].repByte[m]));
            }
          }
        }
      }
      else
      {
        label1125:
        k += 1;
        break;
        label1132:
        k += 1;
        bool1 = bool5;
        break label586;
        label1143:
        k = 0;
        break label681;
        label1148:
        m = 0;
        continue;
      }
      label1154:
      m += 1;
    }
  }
  
  public static ArrayList<String> searchlib(int paramInt, String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    do
    {
      try
      {
        localObject = new File(appdir);
        crkapk = new File(sddir + "/Modified/" + packageName + ".apk");
        if (!crkapk.exists()) {
          Utils.copyFile((File)localObject, crkapk);
        }
        extractLibs(crkapk);
        if (paramString.trim().equals("*"))
        {
          multilib_patch = true;
          paramString = new ArrayList();
          new Utils("").findFileEndText(new File(sddir + "/tmp/lib/"), ".so", paramString);
          if (paramString.size() > 0)
          {
            paramString = paramString.iterator();
            while (paramString.hasNext())
            {
              localObject = (File)paramString.next();
              if (((File)localObject).length() > 0L) {
                localArrayList.add(((File)localObject).getAbsolutePath());
              }
            }
          }
        }
        throw new FileNotFoundException();
      }
      catch (FileNotFoundException paramString)
      {
        print.println("Lib not found!" + paramString.toString());
        return localArrayList;
        switch (paramInt)
        {
        default: 
          return localArrayList;
        case 0: 
          localArrayList.clear();
          localObject = sddir + "/tmp/lib/armeabi/" + paramString;
          if (new File(sddir + "/tmp/lib/armeabi/" + paramString).exists())
          {
            localArrayList.add(localObject);
            return localArrayList;
          }
          break;
        }
      }
      catch (Exception paramString)
      {
        print.println("Lib select error: " + paramString.toString());
        return localArrayList;
      }
      localArrayList.clear();
      Object localObject = sddir + "/tmp/lib/armeabi-v7a/" + paramString;
      if (new File(sddir + "/tmp/lib/armeabi-v7a/" + paramString).exists())
      {
        localArrayList.add(localObject);
        return localArrayList;
      }
      throw new FileNotFoundException();
      localArrayList.clear();
      localObject = sddir + "/tmp/lib/mips/" + paramString;
      if (new File(sddir + "/tmp/lib/mips/" + paramString).exists())
      {
        localArrayList.add(localObject);
        return localArrayList;
      }
      throw new FileNotFoundException();
      localArrayList.clear();
      localObject = sddir + "/tmp/lib/x86/" + paramString;
      if (new File(sddir + "/tmp/lib/x86/" + paramString).exists())
      {
        localArrayList.add(localObject);
        return localArrayList;
      }
      throw new FileNotFoundException();
      localObject = sddir + "/tmp/lib/armeabi/" + paramString;
      if (new File((String)localObject).exists()) {
        localArrayList.add(localObject);
      }
      localObject = sddir + "/tmp/lib/armeabi-v7a/" + paramString;
      if (new File((String)localObject).exists()) {
        localArrayList.add(localObject);
      }
      localObject = sddir + "/tmp/lib/mips/" + paramString;
      if (new File((String)localObject).exists()) {
        localArrayList.add(localObject);
      }
      paramString = sddir + "/tmp/lib/x86/" + paramString;
    } while (!new File(paramString).exists());
    localArrayList.add(paramString);
    return localArrayList;
  }
  
  public static void unzip(File paramFile)
  {
    classesFiles.clear();
    try
    {
      FileInputStream localFileInputStream = new FileInputStream(paramFile);
      localZipInputStream = new ZipInputStream(localFileInputStream);
      do
      {
        localZipEntry = localZipInputStream.getNextEntry();
        if (localZipEntry == null) {
          break;
        }
      } while ((!localZipEntry.getName().toLowerCase().startsWith("classes")) || (!localZipEntry.getName().endsWith(".dex")) || (localZipEntry.getName().contains("/")));
      localFileOutputStream = new FileOutputStream(sddir + "/Modified/" + localZipEntry.getName());
      byte[] arrayOfByte = new byte['Ѐ'];
      for (;;)
      {
        int i = localZipInputStream.read(arrayOfByte);
        if (i == -1) {
          break;
        }
        localFileOutputStream.write(arrayOfByte, 0, i);
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        try
        {
          new ZipFile(paramFile).extractFile("classes.dex", sddir + "/Modified/");
          classesFiles.add(new File(sddir + "/Modified/" + "classes.dex"));
          return;
        }
        catch (ZipException paramFile)
        {
          ZipInputStream localZipInputStream;
          ZipEntry localZipEntry;
          FileOutputStream localFileOutputStream;
          print.println("Error LP: Error classes.dex decompress! " + paramFile);
          print.println("Exception e1" + localException.toString());
          return;
        }
        catch (Exception paramFile)
        {
          print.println("Error LP: Error classes.dex decompress! " + paramFile);
          print.println("Exception e1" + localException.toString());
        }
        classesFiles.add(new File(sddir + "/Modified/" + localZipEntry.getName()));
        localZipInputStream.closeEntry();
        localFileOutputStream.close();
        continue;
        localZipInputStream.close();
        localException.close();
        return;
      }
    }
  }
  
  public static void zipLib(ArrayList<String> paramArrayList)
  {
    do
    {
      try
      {
        ArrayList localArrayList = new ArrayList();
        paramArrayList = paramArrayList.iterator();
        while (paramArrayList.hasNext()) {
          localArrayList.add(new AddFilesItem((String)paramArrayList.next(), sddir + "/tmp/"));
        }
        Utils.addFilesToZip(crkapk.getAbsolutePath(), crkapk.getAbsolutePath() + "checlpis.zip", localArrayList);
      }
      catch (Exception paramArrayList)
      {
        print.println("Error LP: Error libs compress! " + paramArrayList);
        return;
      }
      crkapk.delete();
    } while (crkapk.exists());
    new File(crkapk.getAbsolutePath() + "checlpis.zip").renameTo(crkapk);
  }
  
  public static class Decompress
  {
    private String _location;
    private String _zipFile;
    
    public Decompress(String paramString1, String paramString2)
    {
      this._zipFile = paramString1;
      this._location = paramString2;
      _dirChecker("");
    }
    
    private void _dirChecker(String paramString)
    {
      paramString = new File(this._location + paramString);
      if (paramString.isFile()) {
        paramString.delete();
      }
      if (!paramString.exists()) {
        paramString.mkdirs();
      }
    }
    
    public String unzip(String paramString)
    {
      Object localObject1 = paramString;
      try
      {
        FileInputStream localFileInputStream = new FileInputStream(this._zipFile);
        localObject1 = paramString;
        ZipInputStream localZipInputStream = new ZipInputStream(localFileInputStream);
        Object localObject2 = paramString;
        ZipEntry localZipEntry;
        for (;;)
        {
          localObject1 = localObject2;
          localZipEntry = localZipInputStream.getNextEntry();
          if (localZipEntry == null) {
            break label473;
          }
          localObject1 = localObject2;
          if (!localZipEntry.isDirectory()) {
            break;
          }
          localObject1 = localObject2;
          _dirChecker(localZipEntry.getName());
        }
        try
        {
          paramString = new ZipFile(this._zipFile);
          localObject2 = paramString.getFileHeaders();
          i = 0;
          if (i < ((List)localObject2).size())
          {
            localObject3 = (FileHeader)((List)localObject2).get(i);
            if (!((FileHeader)localObject3).getFileName().equals(localObject1)) {
              break label492;
            }
            createapkcustom.print.println(((FileHeader)localObject3).getFileName());
            paramString.extractFile(((FileHeader)localObject3).getFileName(), this._location);
            paramString = this._location + ((FileHeader)localObject3).getFileName();
            return paramString;
          }
        }
        catch (ZipException paramString)
        {
          for (;;)
          {
            paramString.printStackTrace();
          }
        }
        catch (Exception paramString)
        {
          for (;;)
          {
            int i;
            Object localObject3;
            paramString.printStackTrace();
            continue;
            i += 1;
            localObject2 = localObject3;
          }
        }
      }
      catch (Exception paramString)
      {
        createapkcustom.print.println("Decompressunzip " + paramString);
      }
      for (;;)
      {
        localObject1 = localObject2;
        paramString = (String)localObject2;
        if (((String)localObject2).startsWith("/"))
        {
          localObject1 = localObject2;
          paramString = ((String)localObject2).replaceFirst("/", "");
        }
        localObject2 = paramString;
        localObject1 = paramString;
        if (!localZipEntry.getName().equals(paramString)) {
          break;
        }
        localObject1 = paramString;
        String[] arrayOfString = localZipEntry.getName().split("\\/+");
        localObject2 = "";
        i = 0;
        localObject1 = paramString;
        if (i < arrayOfString.length - 1)
        {
          localObject1 = paramString;
          localObject3 = localObject2;
          if (arrayOfString[i].equals("")) {
            break label515;
          }
          localObject1 = paramString;
          localObject3 = (String)localObject2 + "/" + arrayOfString[i];
          break label515;
        }
        localObject1 = paramString;
        _dirChecker((String)localObject2);
        localObject1 = paramString;
        localObject2 = new FileOutputStream(this._location + localZipEntry.getName());
        localObject1 = paramString;
        localObject3 = new byte['Ѐ'];
        for (;;)
        {
          localObject1 = paramString;
          i = localZipInputStream.read((byte[])localObject3);
          if (i == -1) {
            break;
          }
          localObject1 = paramString;
          ((FileOutputStream)localObject2).write((byte[])localObject3, 0, i);
        }
        localObject1 = paramString;
        localZipInputStream.closeEntry();
        localObject1 = paramString;
        ((FileOutputStream)localObject2).close();
        localObject1 = paramString;
        localZipInputStream.close();
        localObject1 = paramString;
        localFileInputStream.close();
        localObject1 = paramString;
        return this._location + localZipEntry.getName();
        label473:
        localObject1 = localObject2;
        localZipInputStream.close();
        localObject1 = localObject2;
        localFileInputStream.close();
        return "";
        label492:
        i += 1;
      }
    }
    
    public void unzip()
    {
      try
      {
        Object localObject4 = new FileInputStream(this._zipFile);
        ZipInputStream localZipInputStream = new ZipInputStream((InputStream)localObject4);
        for (;;)
        {
          ZipEntry localZipEntry = localZipInputStream.getNextEntry();
          if (localZipEntry != null) {
            if (localZipEntry.isDirectory())
            {
              _dirChecker(localZipEntry.getName());
              continue;
            }
          }
          try
          {
            localObject1 = new ZipFile(this._zipFile);
            localObject3 = ((ZipFile)localObject1).getFileHeaders();
            i = 0;
            for (;;)
            {
              if (i < ((List)localObject3).size())
              {
                localObject4 = (FileHeader)((List)localObject3).get(i);
                if (((FileHeader)localObject4).getFileName().endsWith(".so"))
                {
                  createapkcustom.print.println(((FileHeader)localObject4).getFileName());
                  ((ZipFile)localObject1).extractFile(((FileHeader)localObject4).getFileName(), this._location);
                }
                i += 1;
                continue;
                if (!localZipEntry.getName().endsWith(".so")) {
                  break;
                }
                arrayOfString = localZipEntry.getName().split("\\/+");
                localObject1 = "";
                i = 0;
                if (i < arrayOfString.length - 1)
                {
                  localObject3 = localObject1;
                  if (arrayOfString[i].equals("")) {
                    break label351;
                  }
                  localObject3 = (String)localObject1 + "/" + arrayOfString[i];
                  break label351;
                }
                _dirChecker((String)localObject1);
                localObject1 = new FileOutputStream(this._location + localZipEntry.getName());
                localObject3 = new byte['Ѐ'];
                for (;;)
                {
                  i = localZipInputStream.read((byte[])localObject3);
                  if (i == -1) {
                    break;
                  }
                  ((FileOutputStream)localObject1).write((byte[])localObject3, 0, i);
                }
                localZipInputStream.closeEntry();
                ((FileOutputStream)localObject1).close();
                break;
                localZipInputStream.close();
                ((FileInputStream)localObject4).close();
              }
            }
            return;
          }
          catch (ZipException localZipException)
          {
            localZipException.printStackTrace();
            return;
          }
          catch (Exception localException2)
          {
            localException2.printStackTrace();
            return;
          }
        }
      }
      catch (Exception localException1)
      {
        createapkcustom.print.println("Decompressunzip " + localException1);
      }
      for (;;)
      {
        Object localObject1;
        Object localObject3;
        int i;
        String[] arrayOfString;
        label351:
        i += 1;
        Object localObject2 = localObject3;
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/chelpus/root/utils/createapkcustom.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */