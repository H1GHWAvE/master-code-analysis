package com.chelpus.root.utils;

import com.chelpus.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.MappedByteBuffer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

public class corepatch
{
  public static final byte[] MAGIC = { 100, 101, 121, 10, 48, 51, 53, 0 };
  public static int adler;
  public static MappedByteBuffer fileBytes;
  public static byte[] lastByteReplace = null;
  public static int lastPatchPosition;
  public static boolean not_found_bytes_for_patch;
  public static boolean onlyDalvik;
  public static String toolfilesdir = "";
  
  static
  {
    onlyDalvik = false;
    not_found_bytes_for_patch = false;
    fileBytes = null;
    lastPatchPosition = 0;
  }
  
  private static boolean applyPatch(int paramInt, byte paramByte, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3, byte[] paramArrayOfByte4, boolean paramBoolean)
  {
    if ((paramArrayOfByte1 != null) && (paramByte == paramArrayOfByte1[0]) && (paramBoolean))
    {
      if (paramArrayOfByte4[0] == 0) {
        paramArrayOfByte3[0] = paramByte;
      }
      int i = 1;
      fileBytes.position(paramInt + 1);
      for (paramByte = fileBytes.get(); (paramByte == paramArrayOfByte1[i]) || (paramArrayOfByte2[i] == 1); paramByte = fileBytes.get())
      {
        if (paramArrayOfByte4[i] == 0) {
          paramArrayOfByte3[i] = paramByte;
        }
        if (paramArrayOfByte4[i] == 3) {
          paramArrayOfByte3[i] = ((byte)(paramByte & 0xF));
        }
        if (paramArrayOfByte4[i] == 2) {
          paramArrayOfByte3[i] = ((byte)((paramByte & 0xF) + (paramByte & 0xF) * 16));
        }
        i += 1;
        if (i == paramArrayOfByte1.length)
        {
          fileBytes.position(paramInt);
          fileBytes.put(paramArrayOfByte3);
          fileBytes.force();
          return true;
        }
      }
      fileBytes.position(paramInt + 1);
    }
    return false;
  }
  
  private static boolean applyPatchCounter(int paramInt1, byte paramByte, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3, byte[] paramArrayOfByte4, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    if ((paramArrayOfByte1 != null) && (paramByte == paramArrayOfByte1[0]) && (paramBoolean))
    {
      if (paramArrayOfByte4[0] == 0) {
        paramArrayOfByte3[0] = paramByte;
      }
      int i = 1;
      fileBytes.position(paramInt1 + 1);
      for (paramByte = fileBytes.get(); (paramByte == paramArrayOfByte1[i]) || (paramArrayOfByte2[i] == 1); paramByte = fileBytes.get())
      {
        if (paramArrayOfByte4[i] == 0) {
          paramArrayOfByte3[i] = paramByte;
        }
        if (paramArrayOfByte4[i] == 3) {
          paramArrayOfByte3[i] = ((byte)(paramByte & 0xF));
        }
        if (paramArrayOfByte4[i] == 2) {
          paramArrayOfByte3[i] = ((byte)((paramByte & 0xF) + (paramByte & 0xF) * 16));
        }
        i += 1;
        if (i == paramArrayOfByte1.length)
        {
          if (paramInt2 >= paramInt3)
          {
            lastPatchPosition = paramInt1;
            lastByteReplace = new byte[paramArrayOfByte3.length];
            System.arraycopy(paramArrayOfByte3, 0, lastByteReplace, 0, paramArrayOfByte3.length);
          }
          return true;
        }
      }
      fileBytes.position(paramInt1 + 1);
    }
    return false;
  }
  
  public static void main(String[] paramArrayOfString)
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: Method code too large!\n\tat org.objectweb.asm.MethodWriter.getSize(MethodWriter.java:1872)\n\tat org.objectweb.asm.AsmBridge.sizeOfMethodWriter(AsmBridge.java:28)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:55)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  public static void unzip(File paramFile, String paramString)
  {
    int i = 0;
    try
    {
      FileInputStream localFileInputStream = new FileInputStream(paramFile);
      ZipInputStream localZipInputStream = new ZipInputStream(localFileInputStream);
      Object localObject = localZipInputStream.getNextEntry();
      for (;;)
      {
        if ((localObject != null) && (1 != 0))
        {
          if (((ZipEntry)localObject).getName().equals("classes.dex"))
          {
            localObject = new FileOutputStream(paramString + "/" + "classes.dex");
            byte[] arrayOfByte = new byte['ࠀ'];
            for (;;)
            {
              i = localZipInputStream.read(arrayOfByte);
              if (i == -1) {
                break;
              }
              ((FileOutputStream)localObject).write(arrayOfByte, 0, i);
            }
          }
        }
        else {
          try
          {
            new ZipFile(paramFile).extractFile("classes.dex", paramString);
            System.out.println("Exception e" + localException.toString());
            return;
            Utils.run_all_no_root(new String[] { "chmod", "777", paramString + "/" + "classes.dex" });
            Utils.run_all_no_root(new String[] { "chown", "0.0", paramString + "/" + "classes.dex" });
            Utils.run_all_no_root(new String[] { "chown", "0:0", paramString + "/" + "classes.dex" });
            localZipInputStream.closeEntry();
            localException.close();
            i = 1;
            break label457;
            localZipInputStream.close();
            localFileInputStream.close();
            return;
            localZipEntry = localZipInputStream.getNextEntry();
          }
          catch (ZipException paramFile)
          {
            for (;;)
            {
              System.out.println("Error classes.dex decompress! " + paramFile);
              System.out.println("Exception e1" + localZipEntry.toString());
            }
          }
          catch (Exception paramFile)
          {
            for (;;)
            {
              System.out.println("Error classes.dex decompress! " + paramFile);
              System.out.println("Exception e1" + localZipEntry.toString());
            }
          }
        }
      }
    }
    catch (Exception localException) {}
    for (;;)
    {
      ZipEntry localZipEntry;
      label457:
      if (i != 0) {}
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/chelpus/root/utils/corepatch.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */