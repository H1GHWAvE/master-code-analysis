package com.chelpus.root.utils;

import com.chelpus.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.MappedByteBuffer;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.Adler32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

public class runpatchads
{
  private static boolean ART;
  public static String AdsBlockFile = "";
  public static String appdir;
  public static ArrayList<File> classesFiles = new ArrayList();
  private static boolean copyDC;
  private static boolean createAPK = false;
  public static File crkapk;
  private static boolean dependencies;
  public static String dir;
  public static String dir2;
  public static String dirapp;
  private static boolean fileblock = true;
  public static ArrayList<File> filestopatch;
  private static boolean full_offline;
  private static boolean pattern1 = true;
  private static boolean pattern2 = true;
  private static boolean pattern3 = true;
  private static boolean pattern4 = true;
  private static boolean pattern5 = true;
  private static boolean pattern6 = true;
  public static PrintStream print;
  public static String result;
  public static String sddir;
  public static byte[][] sites;
  public static boolean system;
  public static String uid;
  
  static
  {
    dependencies = true;
    full_offline = true;
    copyDC = false;
    ART = false;
    dirapp = "/data/app/";
    system = false;
    uid = "";
    dir = "/sdcard/";
    dir2 = "/sdcard/";
    filestopatch = null;
    sddir = "/sdcard/";
    appdir = "/sdcard/";
  }
  
  public static boolean byteverify(MappedByteBuffer paramMappedByteBuffer, int paramInt, byte paramByte, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3, byte[] paramArrayOfByte4, String paramString, boolean paramBoolean)
  {
    if ((paramByte == paramArrayOfByte1[0]) && (paramBoolean))
    {
      if (paramArrayOfByte4[0] == 0) {
        paramArrayOfByte3[0] = paramByte;
      }
      int i = 1;
      paramMappedByteBuffer.position(paramInt + 1);
      for (paramByte = paramMappedByteBuffer.get(); (paramByte == paramArrayOfByte1[i]) || (paramArrayOfByte2[i] == 1); paramByte = paramMappedByteBuffer.get())
      {
        if (paramArrayOfByte4[i] == 0) {
          paramArrayOfByte3[i] = paramByte;
        }
        i += 1;
        if (i == paramArrayOfByte1.length)
        {
          paramMappedByteBuffer.position(paramInt);
          paramMappedByteBuffer.put(paramArrayOfByte3);
          paramMappedByteBuffer.force();
          Utils.sendFromRoot(paramString);
          return true;
        }
      }
      paramMappedByteBuffer.position(paramInt + 1);
    }
    return false;
  }
  
  private static final void calcChecksum(byte[] paramArrayOfByte, int paramInt)
  {
    Adler32 localAdler32 = new Adler32();
    localAdler32.update(paramArrayOfByte, 12, paramArrayOfByte.length - (paramInt + 12));
    int i = (int)localAdler32.getValue();
    paramArrayOfByte[(paramInt + 8)] = ((byte)i);
    paramArrayOfByte[(paramInt + 9)] = ((byte)(i >> 8));
    paramArrayOfByte[(paramInt + 10)] = ((byte)(i >> 16));
    paramArrayOfByte[(paramInt + 11)] = ((byte)(i >> 24));
  }
  
  private static final void calcSignature(byte[] paramArrayOfByte, int paramInt)
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-1");
      localMessageDigest.update(paramArrayOfByte, 32, paramArrayOfByte.length - (paramInt + 32));
      try
      {
        paramInt = localMessageDigest.digest(paramArrayOfByte, paramInt + 12, 20);
        if (paramInt != 20) {
          throw new RuntimeException("unexpected digest write:" + paramInt + "bytes");
        }
      }
      catch (DigestException paramArrayOfByte)
      {
        throw new RuntimeException(paramArrayOfByte);
      }
      return;
    }
    catch (NoSuchAlgorithmException paramArrayOfByte)
    {
      throw new RuntimeException(paramArrayOfByte);
    }
  }
  
  public static void clearTemp()
  {
    File localFile1;
    do
    {
      try
      {
        Object localObject = new File(dir + "/AndroidManifest.xml");
        if (((File)localObject).exists()) {
          ((File)localObject).delete();
        }
        if ((classesFiles != null) && (classesFiles.size() > 0))
        {
          localObject = classesFiles.iterator();
          while (((Iterator)localObject).hasNext())
          {
            File localFile2 = (File)((Iterator)localObject).next();
            if (localFile2.exists()) {
              localFile2.delete();
            }
          }
        }
        localFile1 = new File(dir + "/classes.dex");
      }
      catch (Exception localException)
      {
        Utils.sendFromRoot("" + localException.toString());
        return;
      }
      if (localFile1.exists()) {
        localFile1.delete();
      }
      localFile1 = new File(dir + "/classes.dex.apk");
    } while (!localFile1.exists());
    localFile1.delete();
  }
  
  public static void clearTempSD()
  {
    try
    {
      File localFile = new File(sddir + "/Modified/classes.dex.apk");
      if (localFile.exists()) {
        localFile.delete();
      }
      return;
    }
    catch (Exception localException)
    {
      Utils.sendFromRoot("" + localException.toString());
    }
  }
  
  public static void fixadler(File paramFile)
  {
    try
    {
      FileInputStream localFileInputStream = new FileInputStream(paramFile);
      byte[] arrayOfByte = new byte[localFileInputStream.available()];
      localFileInputStream.read(arrayOfByte);
      calcSignature(arrayOfByte, 0);
      calcChecksum(arrayOfByte, 0);
      localFileInputStream.close();
      paramFile = new FileOutputStream(paramFile);
      paramFile.write(arrayOfByte);
      paramFile.close();
      return;
    }
    catch (Exception paramFile)
    {
      paramFile.printStackTrace();
    }
  }
  
  /* Error */
  public static void main(String[] paramArrayOfString)
  {
    // Byte code:
    //   0: new 260	com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream
    //   3: dup
    //   4: ldc_w 262
    //   7: invokespecial 263	com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream:<init>	(Ljava/lang/String;)V
    //   10: astore 11
    //   12: new 265	java/io/PrintStream
    //   15: dup
    //   16: aload 11
    //   18: invokespecial 268	java/io/PrintStream:<init>	(Ljava/io/OutputStream;)V
    //   21: putstatic 270	com/chelpus/root/utils/runpatchads:print	Ljava/io/PrintStream;
    //   24: getstatic 270	com/chelpus/root/utils/runpatchads:print	Ljava/io/PrintStream;
    //   27: ldc_w 272
    //   30: invokevirtual 275	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   33: new 6	com/chelpus/root/utils/runpatchads$1
    //   36: dup
    //   37: invokespecial 276	com/chelpus/root/utils/runpatchads$1:<init>	()V
    //   40: invokestatic 280	com/chelpus/Utils:startRootJava	(Ljava/lang/Object;)V
    //   43: ldc_w 272
    //   46: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   49: pop
    //   50: aload_0
    //   51: iconst_0
    //   52: aaload
    //   53: invokestatic 283	com/chelpus/Utils:kill	(Ljava/lang/String;)V
    //   56: new 91	java/util/ArrayList
    //   59: dup
    //   60: invokespecial 94	java/util/ArrayList:<init>	()V
    //   63: astore 12
    //   65: iconst_1
    //   66: putstatic 45	com/chelpus/root/utils/runpatchads:fileblock	Z
    //   69: iconst_1
    //   70: putstatic 47	com/chelpus/root/utils/runpatchads:pattern1	Z
    //   73: iconst_1
    //   74: putstatic 49	com/chelpus/root/utils/runpatchads:pattern2	Z
    //   77: iconst_1
    //   78: putstatic 51	com/chelpus/root/utils/runpatchads:pattern3	Z
    //   81: iconst_1
    //   82: putstatic 53	com/chelpus/root/utils/runpatchads:pattern4	Z
    //   85: iconst_1
    //   86: putstatic 55	com/chelpus/root/utils/runpatchads:pattern5	Z
    //   89: iconst_1
    //   90: putstatic 57	com/chelpus/root/utils/runpatchads:pattern6	Z
    //   93: iconst_1
    //   94: putstatic 59	com/chelpus/root/utils/runpatchads:dependencies	Z
    //   97: iconst_1
    //   98: putstatic 61	com/chelpus/root/utils/runpatchads:full_offline	Z
    //   101: new 91	java/util/ArrayList
    //   104: dup
    //   105: invokespecial 94	java/util/ArrayList:<init>	()V
    //   108: putstatic 83	com/chelpus/root/utils/runpatchads:filestopatch	Ljava/util/ArrayList;
    //   111: new 186	java/io/File
    //   114: dup
    //   115: aload_0
    //   116: iconst_3
    //   117: aaload
    //   118: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   121: invokevirtual 287	java/io/File:listFiles	()[Ljava/io/File;
    //   124: astore 13
    //   126: aload 13
    //   128: arraylength
    //   129: istore 4
    //   131: iconst_0
    //   132: istore_3
    //   133: iload_3
    //   134: iload 4
    //   136: if_icmpge +79 -> 215
    //   139: aload 13
    //   141: iload_3
    //   142: aaload
    //   143: astore 14
    //   145: aload 14
    //   147: invokevirtual 290	java/io/File:isFile	()Z
    //   150: ifeq +51 -> 201
    //   153: aload 14
    //   155: invokevirtual 293	java/io/File:getName	()Ljava/lang/String;
    //   158: ldc_w 295
    //   161: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   164: ifne +37 -> 201
    //   167: aload 14
    //   169: invokevirtual 293	java/io/File:getName	()Ljava/lang/String;
    //   172: ldc_w 303
    //   175: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   178: ifne +23 -> 201
    //   181: aload 14
    //   183: invokevirtual 293	java/io/File:getName	()Ljava/lang/String;
    //   186: ldc_w 305
    //   189: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   192: ifne +9 -> 201
    //   195: aload 14
    //   197: invokevirtual 196	java/io/File:delete	()Z
    //   200: pop
    //   201: iload_3
    //   202: iconst_1
    //   203: iadd
    //   204: istore_3
    //   205: goto -72 -> 133
    //   208: astore 13
    //   210: aload 13
    //   212: invokevirtual 252	java/lang/Exception:printStackTrace	()V
    //   215: aload_0
    //   216: iconst_1
    //   217: aaload
    //   218: ldc_w 307
    //   221: invokevirtual 311	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   224: ifne +7 -> 231
    //   227: iconst_0
    //   228: putstatic 45	com/chelpus/root/utils/runpatchads:fileblock	Z
    //   231: aload_0
    //   232: iconst_1
    //   233: aaload
    //   234: ldc_w 312
    //   237: invokevirtual 311	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   240: ifne +7 -> 247
    //   243: iconst_0
    //   244: putstatic 47	com/chelpus/root/utils/runpatchads:pattern1	Z
    //   247: aload_0
    //   248: iconst_1
    //   249: aaload
    //   250: ldc_w 313
    //   253: invokevirtual 311	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   256: ifne +7 -> 263
    //   259: iconst_0
    //   260: putstatic 49	com/chelpus/root/utils/runpatchads:pattern2	Z
    //   263: aload_0
    //   264: iconst_1
    //   265: aaload
    //   266: ldc_w 314
    //   269: invokevirtual 311	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   272: ifne +7 -> 279
    //   275: iconst_0
    //   276: putstatic 51	com/chelpus/root/utils/runpatchads:pattern3	Z
    //   279: aload_0
    //   280: iconst_1
    //   281: aaload
    //   282: ldc_w 315
    //   285: invokevirtual 311	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   288: ifne +7 -> 295
    //   291: iconst_0
    //   292: putstatic 53	com/chelpus/root/utils/runpatchads:pattern4	Z
    //   295: aload_0
    //   296: iconst_1
    //   297: aaload
    //   298: ldc_w 316
    //   301: invokevirtual 311	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   304: ifne +7 -> 311
    //   307: iconst_0
    //   308: putstatic 55	com/chelpus/root/utils/runpatchads:pattern5	Z
    //   311: aload_0
    //   312: iconst_1
    //   313: aaload
    //   314: ldc_w 317
    //   317: invokevirtual 311	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   320: ifne +7 -> 327
    //   323: iconst_0
    //   324: putstatic 57	com/chelpus/root/utils/runpatchads:pattern6	Z
    //   327: aload_0
    //   328: iconst_1
    //   329: aaload
    //   330: ldc_w 318
    //   333: invokevirtual 311	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   336: ifne +7 -> 343
    //   339: iconst_0
    //   340: putstatic 59	com/chelpus/root/utils/runpatchads:dependencies	Z
    //   343: aload_0
    //   344: iconst_1
    //   345: aaload
    //   346: ldc_w 320
    //   349: invokevirtual 311	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   352: ifne +7 -> 359
    //   355: iconst_0
    //   356: putstatic 61	com/chelpus/root/utils/runpatchads:full_offline	Z
    //   359: aload_0
    //   360: bipush 6
    //   362: aaload
    //   363: ifnull +20 -> 383
    //   366: aload_0
    //   367: bipush 6
    //   369: aaload
    //   370: ldc_w 321
    //   373: invokevirtual 311	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   376: ifeq +7 -> 383
    //   379: iconst_1
    //   380: putstatic 43	com/chelpus/root/utils/runpatchads:createAPK	Z
    //   383: aload_0
    //   384: bipush 6
    //   386: aaload
    //   387: ifnull +20 -> 407
    //   390: aload_0
    //   391: bipush 6
    //   393: aaload
    //   394: ldc_w 322
    //   397: invokevirtual 311	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   400: ifeq +7 -> 407
    //   403: iconst_1
    //   404: putstatic 65	com/chelpus/root/utils/runpatchads:ART	Z
    //   407: aload_0
    //   408: bipush 6
    //   410: aaload
    //   411: ifnull +11 -> 422
    //   414: aload_0
    //   415: bipush 6
    //   417: aaload
    //   418: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   421: pop
    //   422: aload_0
    //   423: bipush 7
    //   425: aaload
    //   426: ifnull +37 -> 463
    //   429: new 159	java/lang/StringBuilder
    //   432: dup
    //   433: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   436: ldc_w 324
    //   439: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   442: aload_0
    //   443: bipush 7
    //   445: aaload
    //   446: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   449: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   452: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   455: pop
    //   456: aload_0
    //   457: bipush 7
    //   459: aaload
    //   460: putstatic 89	com/chelpus/root/utils/runpatchads:AdsBlockFile	Ljava/lang/String;
    //   463: aload_0
    //   464: bipush 8
    //   466: aaload
    //   467: ifnull +3435 -> 3902
    //   470: aload_0
    //   471: bipush 8
    //   473: aaload
    //   474: putstatic 75	com/chelpus/root/utils/runpatchads:uid	Ljava/lang/String;
    //   477: aload_0
    //   478: iconst_5
    //   479: aaload
    //   480: ldc_w 325
    //   483: invokevirtual 311	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   486: ifeq +7 -> 493
    //   489: iconst_1
    //   490: putstatic 63	com/chelpus/root/utils/runpatchads:copyDC	Z
    //   493: getstatic 43	com/chelpus/root/utils/runpatchads:createAPK	Z
    //   496: ifeq +10 -> 506
    //   499: iconst_0
    //   500: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   503: putstatic 337	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:startUnderRoot	Ljava/lang/Boolean;
    //   506: new 91	java/util/ArrayList
    //   509: dup
    //   510: invokespecial 94	java/util/ArrayList:<init>	()V
    //   513: astore 13
    //   515: new 91	java/util/ArrayList
    //   518: dup
    //   519: invokespecial 94	java/util/ArrayList:<init>	()V
    //   522: astore 14
    //   524: new 91	java/util/ArrayList
    //   527: dup
    //   528: invokespecial 94	java/util/ArrayList:<init>	()V
    //   531: astore 15
    //   533: new 91	java/util/ArrayList
    //   536: dup
    //   537: invokespecial 94	java/util/ArrayList:<init>	()V
    //   540: astore 16
    //   542: new 91	java/util/ArrayList
    //   545: dup
    //   546: invokespecial 94	java/util/ArrayList:<init>	()V
    //   549: astore 17
    //   551: new 91	java/util/ArrayList
    //   554: dup
    //   555: invokespecial 94	java/util/ArrayList:<init>	()V
    //   558: astore 18
    //   560: new 91	java/util/ArrayList
    //   563: dup
    //   564: invokespecial 94	java/util/ArrayList:<init>	()V
    //   567: astore 19
    //   569: aload 14
    //   571: ldc_w 339
    //   574: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   577: pop
    //   578: aload 15
    //   580: ldc_w 344
    //   583: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   586: pop
    //   587: aload 16
    //   589: getstatic 55	com/chelpus/root/utils/runpatchads:pattern5	Z
    //   592: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   595: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   598: pop
    //   599: aload 17
    //   601: ldc_w 346
    //   604: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   607: pop
    //   608: aload 18
    //   610: ldc_w 348
    //   613: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   616: pop
    //   617: aload 19
    //   619: iconst_0
    //   620: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   623: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   626: pop
    //   627: aload 14
    //   629: ldc_w 350
    //   632: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   635: pop
    //   636: aload 15
    //   638: ldc_w 352
    //   641: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   644: pop
    //   645: aload 16
    //   647: getstatic 55	com/chelpus/root/utils/runpatchads:pattern5	Z
    //   650: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   653: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   656: pop
    //   657: aload 17
    //   659: ldc_w 346
    //   662: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   665: pop
    //   666: aload 18
    //   668: ldc_w 348
    //   671: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   674: pop
    //   675: aload 19
    //   677: iconst_0
    //   678: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   681: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   684: pop
    //   685: aload 14
    //   687: ldc_w 354
    //   690: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   693: pop
    //   694: aload 15
    //   696: ldc_w 356
    //   699: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   702: pop
    //   703: aload 16
    //   705: iconst_0
    //   706: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   709: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   712: pop
    //   713: aload 17
    //   715: ldc_w 358
    //   718: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   721: pop
    //   722: aload 18
    //   724: ldc_w 348
    //   727: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   730: pop
    //   731: aload 19
    //   733: iconst_0
    //   734: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   737: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   740: pop
    //   741: aload 14
    //   743: ldc_w 354
    //   746: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   749: pop
    //   750: aload 15
    //   752: ldc_w 356
    //   755: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   758: pop
    //   759: aload 16
    //   761: getstatic 61	com/chelpus/root/utils/runpatchads:full_offline	Z
    //   764: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   767: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   770: pop
    //   771: aload 17
    //   773: ldc_w 360
    //   776: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   779: pop
    //   780: aload 18
    //   782: ldc 73
    //   784: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   787: pop
    //   788: aload 19
    //   790: iconst_0
    //   791: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   794: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   797: pop
    //   798: aload 14
    //   800: ldc_w 362
    //   803: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   806: pop
    //   807: aload 15
    //   809: ldc_w 364
    //   812: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   815: pop
    //   816: aload 16
    //   818: getstatic 47	com/chelpus/root/utils/runpatchads:pattern1	Z
    //   821: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   824: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   827: pop
    //   828: aload 17
    //   830: ldc_w 366
    //   833: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   836: pop
    //   837: aload 18
    //   839: ldc 73
    //   841: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   844: pop
    //   845: aload 19
    //   847: iconst_1
    //   848: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   851: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   854: pop
    //   855: aload 14
    //   857: ldc_w 368
    //   860: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   863: pop
    //   864: aload 15
    //   866: ldc_w 370
    //   869: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   872: pop
    //   873: aload 16
    //   875: getstatic 47	com/chelpus/root/utils/runpatchads:pattern1	Z
    //   878: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   881: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   884: pop
    //   885: aload 17
    //   887: ldc_w 366
    //   890: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   893: pop
    //   894: aload 18
    //   896: ldc 73
    //   898: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   901: pop
    //   902: aload 19
    //   904: iconst_1
    //   905: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   908: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   911: pop
    //   912: aload 14
    //   914: ldc_w 372
    //   917: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   920: pop
    //   921: aload 15
    //   923: ldc_w 370
    //   926: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   929: pop
    //   930: aload 16
    //   932: getstatic 47	com/chelpus/root/utils/runpatchads:pattern1	Z
    //   935: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   938: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   941: pop
    //   942: aload 17
    //   944: ldc_w 366
    //   947: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   950: pop
    //   951: aload 18
    //   953: ldc 73
    //   955: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   958: pop
    //   959: aload 19
    //   961: iconst_1
    //   962: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   965: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   968: pop
    //   969: aload 14
    //   971: ldc_w 374
    //   974: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   977: pop
    //   978: aload 15
    //   980: ldc_w 370
    //   983: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   986: pop
    //   987: aload 16
    //   989: getstatic 47	com/chelpus/root/utils/runpatchads:pattern1	Z
    //   992: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   995: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   998: pop
    //   999: aload 17
    //   1001: ldc_w 366
    //   1004: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1007: pop
    //   1008: aload 18
    //   1010: ldc 73
    //   1012: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1015: pop
    //   1016: aload 19
    //   1018: iconst_1
    //   1019: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1022: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1025: pop
    //   1026: aload 14
    //   1028: ldc_w 376
    //   1031: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1034: pop
    //   1035: aload 15
    //   1037: ldc_w 370
    //   1040: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1043: pop
    //   1044: aload 16
    //   1046: getstatic 47	com/chelpus/root/utils/runpatchads:pattern1	Z
    //   1049: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1052: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1055: pop
    //   1056: aload 17
    //   1058: ldc_w 366
    //   1061: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1064: pop
    //   1065: aload 18
    //   1067: ldc 73
    //   1069: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1072: pop
    //   1073: aload 19
    //   1075: iconst_1
    //   1076: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1079: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1082: pop
    //   1083: aload 14
    //   1085: ldc_w 378
    //   1088: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1091: pop
    //   1092: aload 15
    //   1094: ldc_w 380
    //   1097: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1100: pop
    //   1101: aload 16
    //   1103: getstatic 47	com/chelpus/root/utils/runpatchads:pattern1	Z
    //   1106: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1109: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1112: pop
    //   1113: aload 17
    //   1115: ldc_w 366
    //   1118: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1121: pop
    //   1122: aload 18
    //   1124: ldc 73
    //   1126: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1129: pop
    //   1130: aload 19
    //   1132: iconst_1
    //   1133: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1136: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1139: pop
    //   1140: aload 14
    //   1142: ldc_w 382
    //   1145: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1148: pop
    //   1149: aload 15
    //   1151: ldc_w 370
    //   1154: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1157: pop
    //   1158: aload 16
    //   1160: getstatic 47	com/chelpus/root/utils/runpatchads:pattern1	Z
    //   1163: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1166: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1169: pop
    //   1170: aload 17
    //   1172: ldc_w 366
    //   1175: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1178: pop
    //   1179: aload 18
    //   1181: ldc 73
    //   1183: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1186: pop
    //   1187: aload 19
    //   1189: iconst_1
    //   1190: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1193: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1196: pop
    //   1197: aload 14
    //   1199: ldc_w 384
    //   1202: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1205: pop
    //   1206: aload 15
    //   1208: ldc_w 386
    //   1211: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1214: pop
    //   1215: aload 16
    //   1217: getstatic 47	com/chelpus/root/utils/runpatchads:pattern1	Z
    //   1220: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1223: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1226: pop
    //   1227: aload 17
    //   1229: ldc_w 366
    //   1232: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1235: pop
    //   1236: aload 18
    //   1238: ldc 73
    //   1240: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1243: pop
    //   1244: aload 19
    //   1246: iconst_0
    //   1247: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1250: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1253: pop
    //   1254: aload 14
    //   1256: ldc_w 388
    //   1259: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1262: pop
    //   1263: aload 15
    //   1265: ldc_w 390
    //   1268: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1271: pop
    //   1272: aload 16
    //   1274: getstatic 47	com/chelpus/root/utils/runpatchads:pattern1	Z
    //   1277: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1280: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1283: pop
    //   1284: aload 17
    //   1286: ldc_w 366
    //   1289: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1292: pop
    //   1293: aload 18
    //   1295: ldc 73
    //   1297: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1300: pop
    //   1301: aload 19
    //   1303: iconst_0
    //   1304: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1307: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1310: pop
    //   1311: aload 14
    //   1313: ldc_w 392
    //   1316: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1319: pop
    //   1320: aload 15
    //   1322: ldc_w 394
    //   1325: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1328: pop
    //   1329: aload 16
    //   1331: getstatic 47	com/chelpus/root/utils/runpatchads:pattern1	Z
    //   1334: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1337: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1340: pop
    //   1341: aload 17
    //   1343: ldc_w 366
    //   1346: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1349: pop
    //   1350: aload 18
    //   1352: ldc 73
    //   1354: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1357: pop
    //   1358: aload 19
    //   1360: iconst_0
    //   1361: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1364: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1367: pop
    //   1368: aload 14
    //   1370: ldc_w 396
    //   1373: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1376: pop
    //   1377: aload 15
    //   1379: ldc_w 398
    //   1382: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1385: pop
    //   1386: aload 16
    //   1388: getstatic 47	com/chelpus/root/utils/runpatchads:pattern1	Z
    //   1391: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1394: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1397: pop
    //   1398: aload 17
    //   1400: ldc_w 400
    //   1403: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1406: pop
    //   1407: aload 18
    //   1409: ldc 73
    //   1411: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1414: pop
    //   1415: aload 19
    //   1417: iconst_0
    //   1418: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1421: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1424: pop
    //   1425: aload 14
    //   1427: ldc_w 362
    //   1430: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1433: pop
    //   1434: aload 15
    //   1436: ldc_w 402
    //   1439: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1442: pop
    //   1443: aload 16
    //   1445: getstatic 47	com/chelpus/root/utils/runpatchads:pattern1	Z
    //   1448: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1451: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1454: pop
    //   1455: aload 17
    //   1457: ldc_w 366
    //   1460: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1463: pop
    //   1464: aload 18
    //   1466: ldc 73
    //   1468: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1471: pop
    //   1472: aload 19
    //   1474: iconst_1
    //   1475: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1478: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1481: pop
    //   1482: aload 14
    //   1484: ldc_w 372
    //   1487: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1490: pop
    //   1491: aload 15
    //   1493: ldc_w 404
    //   1496: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1499: pop
    //   1500: aload 16
    //   1502: getstatic 49	com/chelpus/root/utils/runpatchads:pattern2	Z
    //   1505: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1508: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1511: pop
    //   1512: aload 17
    //   1514: ldc_w 406
    //   1517: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1520: pop
    //   1521: aload 18
    //   1523: ldc 73
    //   1525: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1528: pop
    //   1529: aload 19
    //   1531: iconst_1
    //   1532: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1535: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1538: pop
    //   1539: aload 14
    //   1541: ldc_w 374
    //   1544: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1547: pop
    //   1548: aload 15
    //   1550: ldc_w 404
    //   1553: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1556: pop
    //   1557: aload 16
    //   1559: getstatic 49	com/chelpus/root/utils/runpatchads:pattern2	Z
    //   1562: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1565: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1568: pop
    //   1569: aload 17
    //   1571: ldc_w 406
    //   1574: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1577: pop
    //   1578: aload 18
    //   1580: ldc 73
    //   1582: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1585: pop
    //   1586: aload 19
    //   1588: iconst_1
    //   1589: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1592: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1595: pop
    //   1596: aload 14
    //   1598: ldc_w 376
    //   1601: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1604: pop
    //   1605: aload 15
    //   1607: ldc_w 404
    //   1610: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1613: pop
    //   1614: aload 16
    //   1616: getstatic 49	com/chelpus/root/utils/runpatchads:pattern2	Z
    //   1619: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1622: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1625: pop
    //   1626: aload 17
    //   1628: ldc_w 406
    //   1631: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1634: pop
    //   1635: aload 18
    //   1637: ldc 73
    //   1639: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1642: pop
    //   1643: aload 19
    //   1645: iconst_1
    //   1646: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1649: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1652: pop
    //   1653: aload 14
    //   1655: ldc_w 378
    //   1658: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1661: pop
    //   1662: aload 15
    //   1664: ldc_w 408
    //   1667: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1670: pop
    //   1671: aload 16
    //   1673: getstatic 49	com/chelpus/root/utils/runpatchads:pattern2	Z
    //   1676: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1679: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1682: pop
    //   1683: aload 17
    //   1685: ldc_w 406
    //   1688: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1691: pop
    //   1692: aload 18
    //   1694: ldc 73
    //   1696: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1699: pop
    //   1700: aload 19
    //   1702: iconst_1
    //   1703: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1706: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1709: pop
    //   1710: aload 14
    //   1712: ldc_w 368
    //   1715: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1718: pop
    //   1719: aload 15
    //   1721: ldc_w 404
    //   1724: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1727: pop
    //   1728: aload 16
    //   1730: getstatic 49	com/chelpus/root/utils/runpatchads:pattern2	Z
    //   1733: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1736: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1739: pop
    //   1740: aload 17
    //   1742: ldc_w 406
    //   1745: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1748: pop
    //   1749: aload 18
    //   1751: ldc 73
    //   1753: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1756: pop
    //   1757: aload 19
    //   1759: iconst_1
    //   1760: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1763: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1766: pop
    //   1767: aload 14
    //   1769: ldc_w 382
    //   1772: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1775: pop
    //   1776: aload 15
    //   1778: ldc_w 404
    //   1781: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1784: pop
    //   1785: aload 16
    //   1787: getstatic 49	com/chelpus/root/utils/runpatchads:pattern2	Z
    //   1790: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1793: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1796: pop
    //   1797: aload 17
    //   1799: ldc_w 406
    //   1802: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1805: pop
    //   1806: aload 18
    //   1808: ldc 73
    //   1810: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1813: pop
    //   1814: aload 19
    //   1816: iconst_1
    //   1817: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1820: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1823: pop
    //   1824: aload 14
    //   1826: ldc_w 384
    //   1829: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1832: pop
    //   1833: aload 15
    //   1835: ldc_w 386
    //   1838: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1841: pop
    //   1842: aload 16
    //   1844: getstatic 49	com/chelpus/root/utils/runpatchads:pattern2	Z
    //   1847: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1850: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1853: pop
    //   1854: aload 17
    //   1856: ldc_w 406
    //   1859: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1862: pop
    //   1863: aload 18
    //   1865: ldc 73
    //   1867: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1870: pop
    //   1871: aload 19
    //   1873: iconst_0
    //   1874: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1877: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1880: pop
    //   1881: aload 14
    //   1883: ldc_w 388
    //   1886: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1889: pop
    //   1890: aload 15
    //   1892: ldc_w 390
    //   1895: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1898: pop
    //   1899: aload 16
    //   1901: getstatic 49	com/chelpus/root/utils/runpatchads:pattern2	Z
    //   1904: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1907: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1910: pop
    //   1911: aload 17
    //   1913: ldc_w 406
    //   1916: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1919: pop
    //   1920: aload 18
    //   1922: ldc 73
    //   1924: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1927: pop
    //   1928: aload 19
    //   1930: iconst_0
    //   1931: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1934: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1937: pop
    //   1938: aload 14
    //   1940: ldc_w 392
    //   1943: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1946: pop
    //   1947: aload 15
    //   1949: ldc_w 394
    //   1952: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1955: pop
    //   1956: aload 16
    //   1958: getstatic 49	com/chelpus/root/utils/runpatchads:pattern2	Z
    //   1961: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1964: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1967: pop
    //   1968: aload 17
    //   1970: ldc_w 406
    //   1973: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1976: pop
    //   1977: aload 18
    //   1979: ldc 73
    //   1981: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1984: pop
    //   1985: aload 19
    //   1987: iconst_0
    //   1988: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   1991: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1994: pop
    //   1995: aload 14
    //   1997: ldc_w 396
    //   2000: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2003: pop
    //   2004: aload 15
    //   2006: ldc_w 398
    //   2009: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2012: pop
    //   2013: aload 16
    //   2015: getstatic 49	com/chelpus/root/utils/runpatchads:pattern2	Z
    //   2018: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2021: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2024: pop
    //   2025: aload 17
    //   2027: ldc_w 410
    //   2030: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2033: pop
    //   2034: aload 18
    //   2036: ldc 73
    //   2038: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2041: pop
    //   2042: aload 19
    //   2044: iconst_0
    //   2045: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2048: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2051: pop
    //   2052: aload 14
    //   2054: ldc_w 412
    //   2057: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2060: pop
    //   2061: aload 15
    //   2063: ldc_w 414
    //   2066: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2069: pop
    //   2070: aload 16
    //   2072: getstatic 51	com/chelpus/root/utils/runpatchads:pattern3	Z
    //   2075: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2078: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2081: pop
    //   2082: aload 17
    //   2084: ldc_w 416
    //   2087: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2090: pop
    //   2091: aload 18
    //   2093: ldc 73
    //   2095: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2098: pop
    //   2099: aload 19
    //   2101: iconst_0
    //   2102: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2105: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2108: pop
    //   2109: aload 14
    //   2111: ldc_w 418
    //   2114: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2117: pop
    //   2118: aload 15
    //   2120: ldc_w 420
    //   2123: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2126: pop
    //   2127: aload 16
    //   2129: getstatic 51	com/chelpus/root/utils/runpatchads:pattern3	Z
    //   2132: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2135: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2138: pop
    //   2139: aload 17
    //   2141: ldc_w 416
    //   2144: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2147: pop
    //   2148: aload 18
    //   2150: ldc 73
    //   2152: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2155: pop
    //   2156: aload 19
    //   2158: iconst_0
    //   2159: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2162: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2165: pop
    //   2166: aload 14
    //   2168: ldc_w 422
    //   2171: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2174: pop
    //   2175: aload 15
    //   2177: ldc_w 424
    //   2180: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2183: pop
    //   2184: aload 16
    //   2186: getstatic 51	com/chelpus/root/utils/runpatchads:pattern3	Z
    //   2189: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2192: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2195: pop
    //   2196: aload 17
    //   2198: ldc_w 416
    //   2201: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2204: pop
    //   2205: aload 18
    //   2207: ldc 73
    //   2209: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2212: pop
    //   2213: aload 19
    //   2215: iconst_0
    //   2216: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2219: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2222: pop
    //   2223: aload 14
    //   2225: ldc_w 426
    //   2228: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2231: pop
    //   2232: aload 15
    //   2234: ldc_w 428
    //   2237: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2240: pop
    //   2241: aload 16
    //   2243: getstatic 51	com/chelpus/root/utils/runpatchads:pattern3	Z
    //   2246: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2249: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2252: pop
    //   2253: aload 17
    //   2255: ldc_w 416
    //   2258: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2261: pop
    //   2262: aload 18
    //   2264: ldc 73
    //   2266: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2269: pop
    //   2270: aload 19
    //   2272: iconst_0
    //   2273: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2276: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2279: pop
    //   2280: aload 14
    //   2282: ldc_w 430
    //   2285: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2288: pop
    //   2289: aload 15
    //   2291: ldc_w 432
    //   2294: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2297: pop
    //   2298: aload 16
    //   2300: getstatic 53	com/chelpus/root/utils/runpatchads:pattern4	Z
    //   2303: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2306: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2309: pop
    //   2310: aload 17
    //   2312: ldc_w 434
    //   2315: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2318: pop
    //   2319: aload 18
    //   2321: ldc 73
    //   2323: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2326: pop
    //   2327: aload 19
    //   2329: iconst_0
    //   2330: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2333: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2336: pop
    //   2337: aload 14
    //   2339: ldc_w 436
    //   2342: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2345: pop
    //   2346: aload 15
    //   2348: ldc_w 438
    //   2351: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2354: pop
    //   2355: aload 16
    //   2357: getstatic 53	com/chelpus/root/utils/runpatchads:pattern4	Z
    //   2360: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2363: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2366: pop
    //   2367: aload 17
    //   2369: ldc_w 434
    //   2372: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2375: pop
    //   2376: aload 18
    //   2378: ldc 73
    //   2380: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2383: pop
    //   2384: aload 19
    //   2386: iconst_0
    //   2387: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2390: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2393: pop
    //   2394: aload 14
    //   2396: ldc_w 440
    //   2399: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2402: pop
    //   2403: aload 15
    //   2405: ldc_w 442
    //   2408: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2411: pop
    //   2412: aload 16
    //   2414: getstatic 53	com/chelpus/root/utils/runpatchads:pattern4	Z
    //   2417: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2420: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2423: pop
    //   2424: aload 17
    //   2426: ldc_w 434
    //   2429: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2432: pop
    //   2433: aload 18
    //   2435: ldc 73
    //   2437: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2440: pop
    //   2441: aload 19
    //   2443: iconst_0
    //   2444: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2447: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2450: pop
    //   2451: aload 14
    //   2453: ldc_w 444
    //   2456: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2459: pop
    //   2460: aload 15
    //   2462: ldc_w 446
    //   2465: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2468: pop
    //   2469: aload 16
    //   2471: getstatic 55	com/chelpus/root/utils/runpatchads:pattern5	Z
    //   2474: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2477: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2480: pop
    //   2481: aload 17
    //   2483: ldc_w 448
    //   2486: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2489: pop
    //   2490: aload 18
    //   2492: ldc 73
    //   2494: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2497: pop
    //   2498: aload 19
    //   2500: iconst_0
    //   2501: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2504: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2507: pop
    //   2508: aload 14
    //   2510: ldc_w 450
    //   2513: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2516: pop
    //   2517: aload 15
    //   2519: ldc_w 452
    //   2522: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2525: pop
    //   2526: aload 16
    //   2528: getstatic 55	com/chelpus/root/utils/runpatchads:pattern5	Z
    //   2531: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2534: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2537: pop
    //   2538: aload 17
    //   2540: ldc_w 448
    //   2543: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2546: pop
    //   2547: aload 18
    //   2549: ldc 73
    //   2551: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2554: pop
    //   2555: aload 19
    //   2557: iconst_0
    //   2558: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2561: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2564: pop
    //   2565: aload 14
    //   2567: ldc_w 454
    //   2570: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2573: pop
    //   2574: aload 15
    //   2576: ldc_w 456
    //   2579: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2582: pop
    //   2583: aload 16
    //   2585: getstatic 55	com/chelpus/root/utils/runpatchads:pattern5	Z
    //   2588: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2591: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2594: pop
    //   2595: aload 17
    //   2597: ldc_w 448
    //   2600: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2603: pop
    //   2604: aload 18
    //   2606: ldc 73
    //   2608: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2611: pop
    //   2612: aload 19
    //   2614: iconst_0
    //   2615: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2618: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2621: pop
    //   2622: aload 14
    //   2624: ldc_w 458
    //   2627: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2630: pop
    //   2631: aload 15
    //   2633: ldc_w 460
    //   2636: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2639: pop
    //   2640: aload 16
    //   2642: getstatic 55	com/chelpus/root/utils/runpatchads:pattern5	Z
    //   2645: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2648: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2651: pop
    //   2652: aload 17
    //   2654: ldc_w 448
    //   2657: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2660: pop
    //   2661: aload 18
    //   2663: ldc 73
    //   2665: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2668: pop
    //   2669: aload 19
    //   2671: iconst_0
    //   2672: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2675: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2678: pop
    //   2679: aload 14
    //   2681: ldc_w 462
    //   2684: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2687: pop
    //   2688: aload 15
    //   2690: ldc_w 464
    //   2693: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2696: pop
    //   2697: aload 16
    //   2699: getstatic 55	com/chelpus/root/utils/runpatchads:pattern5	Z
    //   2702: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2705: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2708: pop
    //   2709: aload 17
    //   2711: ldc_w 448
    //   2714: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2717: pop
    //   2718: aload 18
    //   2720: ldc 73
    //   2722: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2725: pop
    //   2726: aload 19
    //   2728: iconst_0
    //   2729: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2732: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2735: pop
    //   2736: aload 14
    //   2738: ldc_w 466
    //   2741: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2744: pop
    //   2745: aload 15
    //   2747: ldc_w 468
    //   2750: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2753: pop
    //   2754: aload 16
    //   2756: getstatic 55	com/chelpus/root/utils/runpatchads:pattern5	Z
    //   2759: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2762: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2765: pop
    //   2766: aload 17
    //   2768: ldc_w 448
    //   2771: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2774: pop
    //   2775: aload 18
    //   2777: ldc 73
    //   2779: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2782: pop
    //   2783: aload 19
    //   2785: iconst_0
    //   2786: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2789: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2792: pop
    //   2793: aload 14
    //   2795: ldc_w 470
    //   2798: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2801: pop
    //   2802: aload 15
    //   2804: ldc_w 472
    //   2807: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2810: pop
    //   2811: aload 16
    //   2813: getstatic 55	com/chelpus/root/utils/runpatchads:pattern5	Z
    //   2816: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2819: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2822: pop
    //   2823: aload 17
    //   2825: ldc_w 448
    //   2828: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2831: pop
    //   2832: aload 18
    //   2834: ldc 73
    //   2836: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2839: pop
    //   2840: aload 19
    //   2842: iconst_0
    //   2843: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2846: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2849: pop
    //   2850: aload 14
    //   2852: ldc_w 474
    //   2855: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2858: pop
    //   2859: aload 15
    //   2861: ldc_w 476
    //   2864: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2867: pop
    //   2868: aload 16
    //   2870: getstatic 55	com/chelpus/root/utils/runpatchads:pattern5	Z
    //   2873: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2876: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2879: pop
    //   2880: aload 17
    //   2882: ldc_w 448
    //   2885: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2888: pop
    //   2889: aload 18
    //   2891: ldc 73
    //   2893: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2896: pop
    //   2897: aload 19
    //   2899: iconst_0
    //   2900: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2903: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2906: pop
    //   2907: aload 14
    //   2909: ldc_w 478
    //   2912: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2915: pop
    //   2916: aload 15
    //   2918: ldc_w 480
    //   2921: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2924: pop
    //   2925: aload 16
    //   2927: getstatic 55	com/chelpus/root/utils/runpatchads:pattern5	Z
    //   2930: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2933: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2936: pop
    //   2937: aload 17
    //   2939: ldc_w 448
    //   2942: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2945: pop
    //   2946: aload 18
    //   2948: ldc 73
    //   2950: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2953: pop
    //   2954: aload 19
    //   2956: iconst_0
    //   2957: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2960: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2963: pop
    //   2964: aload 14
    //   2966: ldc_w 482
    //   2969: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2972: pop
    //   2973: aload 15
    //   2975: ldc_w 484
    //   2978: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2981: pop
    //   2982: aload 16
    //   2984: getstatic 57	com/chelpus/root/utils/runpatchads:pattern6	Z
    //   2987: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   2990: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   2993: pop
    //   2994: aload 17
    //   2996: ldc_w 486
    //   2999: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3002: pop
    //   3003: aload 18
    //   3005: ldc 73
    //   3007: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3010: pop
    //   3011: aload 19
    //   3013: iconst_0
    //   3014: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3017: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3020: pop
    //   3021: aload 14
    //   3023: ldc_w 488
    //   3026: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3029: pop
    //   3030: aload 15
    //   3032: ldc_w 490
    //   3035: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3038: pop
    //   3039: aload 16
    //   3041: getstatic 57	com/chelpus/root/utils/runpatchads:pattern6	Z
    //   3044: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3047: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3050: pop
    //   3051: aload 17
    //   3053: ldc_w 492
    //   3056: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3059: pop
    //   3060: aload 18
    //   3062: ldc 73
    //   3064: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3067: pop
    //   3068: aload 19
    //   3070: iconst_0
    //   3071: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3074: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3077: pop
    //   3078: aload_0
    //   3079: iconst_0
    //   3080: aaload
    //   3081: ldc_w 494
    //   3084: invokevirtual 311	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   3087: ifne +60 -> 3147
    //   3090: aload 14
    //   3092: ldc_w 496
    //   3095: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3098: pop
    //   3099: aload 15
    //   3101: ldc_w 498
    //   3104: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3107: pop
    //   3108: aload 16
    //   3110: getstatic 57	com/chelpus/root/utils/runpatchads:pattern6	Z
    //   3113: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3116: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3119: pop
    //   3120: aload 17
    //   3122: ldc_w 486
    //   3125: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3128: pop
    //   3129: aload 18
    //   3131: ldc 73
    //   3133: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3136: pop
    //   3137: aload 19
    //   3139: iconst_1
    //   3140: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3143: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3146: pop
    //   3147: aload 14
    //   3149: ldc_w 500
    //   3152: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3155: pop
    //   3156: aload 15
    //   3158: ldc_w 502
    //   3161: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3164: pop
    //   3165: aload 16
    //   3167: getstatic 57	com/chelpus/root/utils/runpatchads:pattern6	Z
    //   3170: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3173: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3176: pop
    //   3177: aload 17
    //   3179: ldc_w 486
    //   3182: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3185: pop
    //   3186: aload 18
    //   3188: ldc 73
    //   3190: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3193: pop
    //   3194: aload 19
    //   3196: iconst_1
    //   3197: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3200: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3203: pop
    //   3204: aload 14
    //   3206: ldc_w 504
    //   3209: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3212: pop
    //   3213: aload 15
    //   3215: ldc_w 506
    //   3218: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3221: pop
    //   3222: aload 16
    //   3224: getstatic 57	com/chelpus/root/utils/runpatchads:pattern6	Z
    //   3227: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3230: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3233: pop
    //   3234: aload 17
    //   3236: ldc_w 486
    //   3239: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3242: pop
    //   3243: aload 18
    //   3245: ldc 73
    //   3247: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3250: pop
    //   3251: aload 19
    //   3253: iconst_0
    //   3254: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3257: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3260: pop
    //   3261: aload 14
    //   3263: ldc_w 508
    //   3266: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3269: pop
    //   3270: aload 15
    //   3272: ldc_w 510
    //   3275: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3278: pop
    //   3279: aload 16
    //   3281: getstatic 57	com/chelpus/root/utils/runpatchads:pattern6	Z
    //   3284: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3287: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3290: pop
    //   3291: aload 17
    //   3293: ldc_w 486
    //   3296: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3299: pop
    //   3300: aload 18
    //   3302: ldc 73
    //   3304: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3307: pop
    //   3308: aload 19
    //   3310: iconst_0
    //   3311: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3314: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3317: pop
    //   3318: getstatic 65	com/chelpus/root/utils/runpatchads:ART	Z
    //   3321: ifne +596 -> 3917
    //   3324: aload 14
    //   3326: ldc_w 512
    //   3329: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3332: pop
    //   3333: aload 15
    //   3335: ldc_w 514
    //   3338: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3341: pop
    //   3342: aload 16
    //   3344: getstatic 59	com/chelpus/root/utils/runpatchads:dependencies	Z
    //   3347: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3350: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3353: pop
    //   3354: aload 17
    //   3356: ldc_w 516
    //   3359: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3362: pop
    //   3363: aload 18
    //   3365: ldc 73
    //   3367: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3370: pop
    //   3371: aload 19
    //   3373: iconst_1
    //   3374: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3377: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3380: pop
    //   3381: aload 14
    //   3383: ldc_w 518
    //   3386: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3389: pop
    //   3390: aload 15
    //   3392: ldc_w 520
    //   3395: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3398: pop
    //   3399: aload 16
    //   3401: getstatic 59	com/chelpus/root/utils/runpatchads:dependencies	Z
    //   3404: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3407: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3410: pop
    //   3411: aload 17
    //   3413: ldc_w 516
    //   3416: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3419: pop
    //   3420: aload 18
    //   3422: ldc 73
    //   3424: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3427: pop
    //   3428: aload 19
    //   3430: iconst_1
    //   3431: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3434: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3437: pop
    //   3438: aload 12
    //   3440: aload 14
    //   3442: aload 15
    //   3444: aload 16
    //   3446: aload 17
    //   3448: aload 18
    //   3450: aload 19
    //   3452: iconst_0
    //   3453: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3456: invokestatic 524	com/chelpus/Utils:convertToPatchItemAuto	(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Boolean;)V
    //   3459: getstatic 45	com/chelpus/root/utils/runpatchads:fileblock	Z
    //   3462: ifeq +227 -> 3689
    //   3465: getstatic 89	com/chelpus/root/utils/runpatchads:AdsBlockFile	Ljava/lang/String;
    //   3468: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   3471: pop
    //   3472: new 225	java/io/FileInputStream
    //   3475: dup
    //   3476: getstatic 89	com/chelpus/root/utils/runpatchads:AdsBlockFile	Ljava/lang/String;
    //   3479: invokespecial 525	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   3482: astore 14
    //   3484: new 527	java/io/BufferedReader
    //   3487: dup
    //   3488: new 529	java/io/InputStreamReader
    //   3491: dup
    //   3492: aload 14
    //   3494: invokespecial 532	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   3497: invokespecial 535	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   3500: astore 15
    //   3502: getstatic 45	com/chelpus/root/utils/runpatchads:fileblock	Z
    //   3505: ifeq +529 -> 4034
    //   3508: aload 15
    //   3510: invokevirtual 538	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   3513: astore 16
    //   3515: aload 16
    //   3517: ifnull +517 -> 4034
    //   3520: aload 16
    //   3522: invokevirtual 541	java/lang/String:trim	()Ljava/lang/String;
    //   3525: astore 16
    //   3527: aload 16
    //   3529: ldc 73
    //   3531: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   3534: ifne -26 -> 3508
    //   3537: aload 13
    //   3539: aload 16
    //   3541: ldc_w 543
    //   3544: invokevirtual 547	java/lang/String:getBytes	(Ljava/lang/String;)[B
    //   3547: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3550: pop
    //   3551: goto -43 -> 3508
    //   3554: astore 14
    //   3556: aload 14
    //   3558: invokevirtual 252	java/lang/Exception:printStackTrace	()V
    //   3561: new 225	java/io/FileInputStream
    //   3564: dup
    //   3565: getstatic 89	com/chelpus/root/utils/runpatchads:AdsBlockFile	Ljava/lang/String;
    //   3568: ldc_w 549
    //   3571: ldc_w 551
    //   3574: invokevirtual 555	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   3577: invokespecial 525	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   3580: astore 14
    //   3582: new 527	java/io/BufferedReader
    //   3585: dup
    //   3586: new 529	java/io/InputStreamReader
    //   3589: dup
    //   3590: aload 14
    //   3592: invokespecial 532	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   3595: invokespecial 535	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   3598: astore 15
    //   3600: aload 15
    //   3602: invokevirtual 538	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   3605: astore 16
    //   3607: aload 16
    //   3609: ifnull +433 -> 4042
    //   3612: aload 16
    //   3614: invokevirtual 541	java/lang/String:trim	()Ljava/lang/String;
    //   3617: astore 16
    //   3619: aload 16
    //   3621: ldc 73
    //   3623: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   3626: ifne -26 -> 3600
    //   3629: aload 13
    //   3631: aload 16
    //   3633: ldc_w 543
    //   3636: invokevirtual 547	java/lang/String:getBytes	(Ljava/lang/String;)[B
    //   3639: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3642: pop
    //   3643: goto -43 -> 3600
    //   3646: astore 14
    //   3648: aload 14
    //   3650: invokevirtual 252	java/lang/Exception:printStackTrace	()V
    //   3653: aload 13
    //   3655: invokevirtual 558	java/util/ArrayList:isEmpty	()Z
    //   3658: ifne +31 -> 3689
    //   3661: aload 13
    //   3663: invokevirtual 200	java/util/ArrayList:size	()I
    //   3666: anewarray 560	[B
    //   3669: putstatic 562	com/chelpus/root/utils/runpatchads:sites	[[B
    //   3672: aload 13
    //   3674: getstatic 562	com/chelpus/root/utils/runpatchads:sites	[[B
    //   3677: invokevirtual 566	java/util/ArrayList:toArray	([Ljava/lang/Object;)[Ljava/lang/Object;
    //   3680: checkcast 567	[[B
    //   3683: checkcast 567	[[B
    //   3686: putstatic 562	com/chelpus/root/utils/runpatchads:sites	[[B
    //   3689: getstatic 337	com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment:startUnderRoot	Ljava/lang/Boolean;
    //   3692: invokevirtual 570	java/lang/Boolean:booleanValue	()Z
    //   3695: ifeq +13 -> 3708
    //   3698: aload_0
    //   3699: iconst_2
    //   3700: aaload
    //   3701: ldc_w 572
    //   3704: invokestatic 576	com/chelpus/Utils:remount	(Ljava/lang/String;Ljava/lang/String;)Z
    //   3707: pop
    //   3708: getstatic 43	com/chelpus/root/utils/runpatchads:createAPK	Z
    //   3711: ifne +783 -> 4494
    //   3714: getstatic 65	com/chelpus/root/utils/runpatchads:ART	Z
    //   3717: ifne +777 -> 4494
    //   3720: aload_0
    //   3721: iconst_3
    //   3722: aaload
    //   3723: putstatic 79	com/chelpus/root/utils/runpatchads:dir	Ljava/lang/String;
    //   3726: aload_0
    //   3727: iconst_2
    //   3728: aaload
    //   3729: putstatic 69	com/chelpus/root/utils/runpatchads:dirapp	Ljava/lang/String;
    //   3732: invokestatic 578	com/chelpus/root/utils/runpatchads:clearTemp	()V
    //   3735: aload_0
    //   3736: iconst_4
    //   3737: aaload
    //   3738: ldc_w 580
    //   3741: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   3744: ifeq +7 -> 3751
    //   3747: iconst_0
    //   3748: putstatic 71	com/chelpus/root/utils/runpatchads:system	Z
    //   3751: aload_0
    //   3752: iconst_4
    //   3753: aaload
    //   3754: ldc_w 581
    //   3757: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   3760: ifeq +7 -> 3767
    //   3763: iconst_1
    //   3764: putstatic 71	com/chelpus/root/utils/runpatchads:system	Z
    //   3767: getstatic 83	com/chelpus/root/utils/runpatchads:filestopatch	Ljava/util/ArrayList;
    //   3770: invokevirtual 584	java/util/ArrayList:clear	()V
    //   3773: ldc_w 586
    //   3776: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   3779: pop
    //   3780: aload_0
    //   3781: iconst_0
    //   3782: aaload
    //   3783: astore 13
    //   3785: aload_0
    //   3786: iconst_2
    //   3787: aaload
    //   3788: putstatic 87	com/chelpus/root/utils/runpatchads:appdir	Ljava/lang/String;
    //   3791: aload_0
    //   3792: iconst_3
    //   3793: aaload
    //   3794: putstatic 85	com/chelpus/root/utils/runpatchads:sddir	Ljava/lang/String;
    //   3797: invokestatic 588	com/chelpus/root/utils/runpatchads:clearTempSD	()V
    //   3800: new 186	java/io/File
    //   3803: dup
    //   3804: getstatic 87	com/chelpus/root/utils/runpatchads:appdir	Ljava/lang/String;
    //   3807: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   3810: astore 13
    //   3812: ldc_w 590
    //   3815: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   3818: pop
    //   3819: getstatic 270	com/chelpus/root/utils/runpatchads:print	Ljava/io/PrintStream;
    //   3822: ldc_w 590
    //   3825: invokevirtual 275	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   3828: aload 13
    //   3830: invokestatic 593	com/chelpus/root/utils/runpatchads:unzipART	(Ljava/io/File;)V
    //   3833: getstatic 96	com/chelpus/root/utils/runpatchads:classesFiles	Ljava/util/ArrayList;
    //   3836: ifnull +12 -> 3848
    //   3839: getstatic 96	com/chelpus/root/utils/runpatchads:classesFiles	Ljava/util/ArrayList;
    //   3842: invokevirtual 200	java/util/ArrayList:size	()I
    //   3845: ifne +205 -> 4050
    //   3848: new 258	java/io/FileNotFoundException
    //   3851: dup
    //   3852: invokespecial 594	java/io/FileNotFoundException:<init>	()V
    //   3855: athrow
    //   3856: astore 12
    //   3858: ldc_w 596
    //   3861: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   3864: pop
    //   3865: getstatic 83	com/chelpus/root/utils/runpatchads:filestopatch	Ljava/util/ArrayList;
    //   3868: invokevirtual 204	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   3871: astore 12
    //   3873: aload 12
    //   3875: invokeinterface 209 1 0
    //   3880: ifeq +2694 -> 6574
    //   3883: aload 12
    //   3885: invokeinterface 213 1 0
    //   3890: checkcast 186	java/io/File
    //   3893: invokestatic 598	com/chelpus/Utils:fixadler	(Ljava/io/File;)V
    //   3896: invokestatic 588	com/chelpus/root/utils/runpatchads:clearTempSD	()V
    //   3899: goto -26 -> 3873
    //   3902: ldc_w 600
    //   3905: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   3908: pop
    //   3909: goto -3432 -> 477
    //   3912: astore 13
    //   3914: goto -3437 -> 477
    //   3917: aload 14
    //   3919: ldc_w 512
    //   3922: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3925: pop
    //   3926: aload 15
    //   3928: ldc_w 602
    //   3931: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3934: pop
    //   3935: aload 16
    //   3937: getstatic 59	com/chelpus/root/utils/runpatchads:dependencies	Z
    //   3940: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3943: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3946: pop
    //   3947: aload 17
    //   3949: ldc_w 516
    //   3952: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3955: pop
    //   3956: aload 18
    //   3958: ldc 73
    //   3960: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3963: pop
    //   3964: aload 19
    //   3966: iconst_1
    //   3967: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   3970: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3973: pop
    //   3974: aload 14
    //   3976: ldc_w 518
    //   3979: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3982: pop
    //   3983: aload 15
    //   3985: ldc_w 604
    //   3988: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   3991: pop
    //   3992: aload 16
    //   3994: getstatic 59	com/chelpus/root/utils/runpatchads:dependencies	Z
    //   3997: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   4000: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   4003: pop
    //   4004: aload 17
    //   4006: ldc_w 516
    //   4009: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   4012: pop
    //   4013: aload 18
    //   4015: ldc 73
    //   4017: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   4020: pop
    //   4021: aload 19
    //   4023: iconst_1
    //   4024: invokestatic 331	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   4027: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   4030: pop
    //   4031: goto -593 -> 3438
    //   4034: aload 14
    //   4036: invokevirtual 241	java/io/FileInputStream:close	()V
    //   4039: goto -478 -> 3561
    //   4042: aload 14
    //   4044: invokevirtual 241	java/io/FileInputStream:close	()V
    //   4047: goto -394 -> 3653
    //   4050: getstatic 83	com/chelpus/root/utils/runpatchads:filestopatch	Ljava/util/ArrayList;
    //   4053: invokevirtual 584	java/util/ArrayList:clear	()V
    //   4056: getstatic 96	com/chelpus/root/utils/runpatchads:classesFiles	Ljava/util/ArrayList;
    //   4059: invokevirtual 204	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   4062: astore 13
    //   4064: aload 13
    //   4066: invokeinterface 209 1 0
    //   4071: ifeq +76 -> 4147
    //   4074: aload 13
    //   4076: invokeinterface 213 1 0
    //   4081: checkcast 186	java/io/File
    //   4084: astore 14
    //   4086: aload 14
    //   4088: invokevirtual 193	java/io/File:exists	()Z
    //   4091: ifne +44 -> 4135
    //   4094: new 258	java/io/FileNotFoundException
    //   4097: dup
    //   4098: invokespecial 594	java/io/FileNotFoundException:<init>	()V
    //   4101: athrow
    //   4102: astore 12
    //   4104: new 159	java/lang/StringBuilder
    //   4107: dup
    //   4108: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   4111: ldc_w 606
    //   4114: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4117: aload 12
    //   4119: invokevirtual 214	java/lang/Exception:toString	()Ljava/lang/String;
    //   4122: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4125: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   4128: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   4131: pop
    //   4132: goto -267 -> 3865
    //   4135: getstatic 83	com/chelpus/root/utils/runpatchads:filestopatch	Ljava/util/ArrayList;
    //   4138: aload 14
    //   4140: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   4143: pop
    //   4144: goto -80 -> 4064
    //   4147: aload_0
    //   4148: iconst_2
    //   4149: aaload
    //   4150: iconst_1
    //   4151: invokestatic 610	com/chelpus/Utils:getPlaceForOdex	(Ljava/lang/String;Z)Ljava/lang/String;
    //   4154: astore 13
    //   4156: new 186	java/io/File
    //   4159: dup
    //   4160: aload 13
    //   4162: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   4165: astore 14
    //   4167: aload 14
    //   4169: invokevirtual 193	java/io/File:exists	()Z
    //   4172: ifeq +9 -> 4181
    //   4175: aload 14
    //   4177: invokevirtual 196	java/io/File:delete	()Z
    //   4180: pop
    //   4181: new 186	java/io/File
    //   4184: dup
    //   4185: aload 13
    //   4187: ldc_w 612
    //   4190: ldc_w 614
    //   4193: invokevirtual 555	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   4196: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   4199: astore 14
    //   4201: aload 14
    //   4203: invokevirtual 193	java/io/File:exists	()Z
    //   4206: ifeq +9 -> 4215
    //   4209: aload 14
    //   4211: invokevirtual 196	java/io/File:delete	()Z
    //   4214: pop
    //   4215: new 186	java/io/File
    //   4218: dup
    //   4219: aload 13
    //   4221: ldc_w 614
    //   4224: ldc_w 612
    //   4227: invokevirtual 555	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   4230: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   4233: astore 13
    //   4235: aload 13
    //   4237: invokevirtual 193	java/io/File:exists	()Z
    //   4240: ifeq +9 -> 4249
    //   4243: aload 13
    //   4245: invokevirtual 196	java/io/File:delete	()Z
    //   4248: pop
    //   4249: getstatic 83	com/chelpus/root/utils/runpatchads:filestopatch	Ljava/util/ArrayList;
    //   4252: invokevirtual 204	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   4255: astore 13
    //   4257: aload 13
    //   4259: invokeinterface 209 1 0
    //   4264: ifeq -399 -> 3865
    //   4267: aload 13
    //   4269: invokeinterface 213 1 0
    //   4274: checkcast 186	java/io/File
    //   4277: astore 14
    //   4279: ldc_w 616
    //   4282: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   4285: pop
    //   4286: new 91	java/util/ArrayList
    //   4289: dup
    //   4290: invokespecial 94	java/util/ArrayList:<init>	()V
    //   4293: astore 15
    //   4295: aload 15
    //   4297: ldc_w 618
    //   4300: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   4303: pop
    //   4304: aload 15
    //   4306: ldc_w 620
    //   4309: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   4312: pop
    //   4313: aload 15
    //   4315: ldc_w 622
    //   4318: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   4321: pop
    //   4322: ldc_w 624
    //   4325: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   4328: pop
    //   4329: getstatic 270	com/chelpus/root/utils/runpatchads:print	Ljava/io/PrintStream;
    //   4332: ldc_w 624
    //   4335: invokevirtual 275	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   4338: aload 14
    //   4340: invokevirtual 627	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   4343: aload 15
    //   4345: iconst_0
    //   4346: invokestatic 631	com/chelpus/Utils:getStringIds	(Ljava/lang/String;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
    //   4349: astore 16
    //   4351: iconst_0
    //   4352: istore_3
    //   4353: new 91	java/util/ArrayList
    //   4356: dup
    //   4357: invokespecial 94	java/util/ArrayList:<init>	()V
    //   4360: astore 15
    //   4362: aload 15
    //   4364: new 633	com/android/vending/billing/InAppBillingService/LUCK/CommandItem
    //   4367: dup
    //   4368: ldc_w 620
    //   4371: ldc_w 622
    //   4374: invokespecial 636	com/android/vending/billing/InAppBillingService/LUCK/CommandItem:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   4377: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   4380: pop
    //   4381: aload 16
    //   4383: invokevirtual 204	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   4386: astore 16
    //   4388: aload 16
    //   4390: invokeinterface 209 1 0
    //   4395: ifeq +593 -> 4988
    //   4398: aload 16
    //   4400: invokeinterface 213 1 0
    //   4405: checkcast 638	com/android/vending/billing/InAppBillingService/LUCK/StringItem
    //   4408: astore 17
    //   4410: aload 15
    //   4412: invokevirtual 204	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   4415: astore 18
    //   4417: aload 18
    //   4419: invokeinterface 209 1 0
    //   4424: ifeq +384 -> 4808
    //   4427: aload 18
    //   4429: invokeinterface 213 1 0
    //   4434: checkcast 633	com/android/vending/billing/InAppBillingService/LUCK/CommandItem
    //   4437: astore 19
    //   4439: aload 19
    //   4441: getfield 641	com/android/vending/billing/InAppBillingService/LUCK/CommandItem:object	Ljava/lang/String;
    //   4444: aload 17
    //   4446: getfield 644	com/android/vending/billing/InAppBillingService/LUCK/StringItem:str	Ljava/lang/String;
    //   4449: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4452: ifeq +13 -> 4465
    //   4455: aload 19
    //   4457: aload 17
    //   4459: getfield 647	com/android/vending/billing/InAppBillingService/LUCK/StringItem:offset	[B
    //   4462: putfield 650	com/android/vending/billing/InAppBillingService/LUCK/CommandItem:Object	[B
    //   4465: aload 19
    //   4467: getfield 653	com/android/vending/billing/InAppBillingService/LUCK/CommandItem:method	Ljava/lang/String;
    //   4470: aload 17
    //   4472: getfield 644	com/android/vending/billing/InAppBillingService/LUCK/StringItem:str	Ljava/lang/String;
    //   4475: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4478: ifeq -61 -> 4417
    //   4481: aload 19
    //   4483: aload 17
    //   4485: getfield 647	com/android/vending/billing/InAppBillingService/LUCK/StringItem:offset	[B
    //   4488: putfield 656	com/android/vending/billing/InAppBillingService/LUCK/CommandItem:Method	[B
    //   4491: goto -74 -> 4417
    //   4494: getstatic 43	com/chelpus/root/utils/runpatchads:createAPK	Z
    //   4497: ifeq +178 -> 4675
    //   4500: aload_0
    //   4501: iconst_0
    //   4502: aaload
    //   4503: astore 13
    //   4505: aload_0
    //   4506: iconst_2
    //   4507: aaload
    //   4508: putstatic 87	com/chelpus/root/utils/runpatchads:appdir	Ljava/lang/String;
    //   4511: aload_0
    //   4512: iconst_5
    //   4513: aaload
    //   4514: putstatic 85	com/chelpus/root/utils/runpatchads:sddir	Ljava/lang/String;
    //   4517: invokestatic 588	com/chelpus/root/utils/runpatchads:clearTempSD	()V
    //   4520: new 186	java/io/File
    //   4523: dup
    //   4524: getstatic 87	com/chelpus/root/utils/runpatchads:appdir	Ljava/lang/String;
    //   4527: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   4530: astore 14
    //   4532: aload 14
    //   4534: invokestatic 659	com/chelpus/root/utils/runpatchads:unzipSD	(Ljava/io/File;)V
    //   4537: new 186	java/io/File
    //   4540: dup
    //   4541: new 159	java/lang/StringBuilder
    //   4544: dup
    //   4545: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   4548: getstatic 85	com/chelpus/root/utils/runpatchads:sddir	Ljava/lang/String;
    //   4551: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4554: ldc_w 661
    //   4557: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4560: aload 13
    //   4562: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4565: ldc_w 663
    //   4568: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4571: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   4574: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   4577: putstatic 665	com/chelpus/root/utils/runpatchads:crkapk	Ljava/io/File;
    //   4580: aload 14
    //   4582: getstatic 665	com/chelpus/root/utils/runpatchads:crkapk	Ljava/io/File;
    //   4585: invokestatic 669	com/chelpus/Utils:copyFile	(Ljava/io/File;Ljava/io/File;)V
    //   4588: getstatic 96	com/chelpus/root/utils/runpatchads:classesFiles	Ljava/util/ArrayList;
    //   4591: ifnull +12 -> 4603
    //   4594: getstatic 96	com/chelpus/root/utils/runpatchads:classesFiles	Ljava/util/ArrayList;
    //   4597: invokevirtual 200	java/util/ArrayList:size	()I
    //   4600: ifne +11 -> 4611
    //   4603: new 258	java/io/FileNotFoundException
    //   4606: dup
    //   4607: invokespecial 594	java/io/FileNotFoundException:<init>	()V
    //   4610: athrow
    //   4611: getstatic 83	com/chelpus/root/utils/runpatchads:filestopatch	Ljava/util/ArrayList;
    //   4614: invokevirtual 584	java/util/ArrayList:clear	()V
    //   4617: getstatic 96	com/chelpus/root/utils/runpatchads:classesFiles	Ljava/util/ArrayList;
    //   4620: invokevirtual 204	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   4623: astore 13
    //   4625: aload 13
    //   4627: invokeinterface 209 1 0
    //   4632: ifeq +43 -> 4675
    //   4635: aload 13
    //   4637: invokeinterface 213 1 0
    //   4642: checkcast 186	java/io/File
    //   4645: astore 14
    //   4647: aload 14
    //   4649: invokevirtual 193	java/io/File:exists	()Z
    //   4652: ifne +11 -> 4663
    //   4655: new 258	java/io/FileNotFoundException
    //   4658: dup
    //   4659: invokespecial 594	java/io/FileNotFoundException:<init>	()V
    //   4662: athrow
    //   4663: getstatic 83	com/chelpus/root/utils/runpatchads:filestopatch	Ljava/util/ArrayList;
    //   4666: aload 14
    //   4668: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   4671: pop
    //   4672: goto -47 -> 4625
    //   4675: getstatic 65	com/chelpus/root/utils/runpatchads:ART	Z
    //   4678: ifeq -429 -> 4249
    //   4681: ldc_w 671
    //   4684: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   4687: pop
    //   4688: aload_0
    //   4689: iconst_0
    //   4690: aaload
    //   4691: astore 13
    //   4693: aload_0
    //   4694: iconst_2
    //   4695: aaload
    //   4696: putstatic 87	com/chelpus/root/utils/runpatchads:appdir	Ljava/lang/String;
    //   4699: aload_0
    //   4700: iconst_3
    //   4701: aaload
    //   4702: putstatic 85	com/chelpus/root/utils/runpatchads:sddir	Ljava/lang/String;
    //   4705: invokestatic 588	com/chelpus/root/utils/runpatchads:clearTempSD	()V
    //   4708: new 186	java/io/File
    //   4711: dup
    //   4712: getstatic 87	com/chelpus/root/utils/runpatchads:appdir	Ljava/lang/String;
    //   4715: invokespecial 189	java/io/File:<init>	(Ljava/lang/String;)V
    //   4718: invokestatic 593	com/chelpus/root/utils/runpatchads:unzipART	(Ljava/io/File;)V
    //   4721: getstatic 96	com/chelpus/root/utils/runpatchads:classesFiles	Ljava/util/ArrayList;
    //   4724: ifnull +12 -> 4736
    //   4727: getstatic 96	com/chelpus/root/utils/runpatchads:classesFiles	Ljava/util/ArrayList;
    //   4730: invokevirtual 200	java/util/ArrayList:size	()I
    //   4733: ifne +11 -> 4744
    //   4736: new 258	java/io/FileNotFoundException
    //   4739: dup
    //   4740: invokespecial 594	java/io/FileNotFoundException:<init>	()V
    //   4743: athrow
    //   4744: getstatic 83	com/chelpus/root/utils/runpatchads:filestopatch	Ljava/util/ArrayList;
    //   4747: invokevirtual 584	java/util/ArrayList:clear	()V
    //   4750: getstatic 96	com/chelpus/root/utils/runpatchads:classesFiles	Ljava/util/ArrayList;
    //   4753: invokevirtual 204	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   4756: astore 13
    //   4758: aload 13
    //   4760: invokeinterface 209 1 0
    //   4765: ifeq -516 -> 4249
    //   4768: aload 13
    //   4770: invokeinterface 213 1 0
    //   4775: checkcast 186	java/io/File
    //   4778: astore 14
    //   4780: aload 14
    //   4782: invokevirtual 193	java/io/File:exists	()Z
    //   4785: ifne +11 -> 4796
    //   4788: new 258	java/io/FileNotFoundException
    //   4791: dup
    //   4792: invokespecial 594	java/io/FileNotFoundException:<init>	()V
    //   4795: athrow
    //   4796: getstatic 83	com/chelpus/root/utils/runpatchads:filestopatch	Ljava/util/ArrayList;
    //   4799: aload 14
    //   4801: invokevirtual 342	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   4804: pop
    //   4805: goto -47 -> 4758
    //   4808: aload 17
    //   4810: getfield 644	com/android/vending/billing/InAppBillingService/LUCK/StringItem:str	Ljava/lang/String;
    //   4813: ldc_w 618
    //   4816: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   4819: ifeq -431 -> 4388
    //   4822: aload 17
    //   4824: getfield 674	com/android/vending/billing/InAppBillingService/LUCK/StringItem:bits32	Z
    //   4827: ifne +61 -> 4888
    //   4830: aload 12
    //   4832: iconst_0
    //   4833: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   4836: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   4839: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   4842: iconst_2
    //   4843: aload 17
    //   4845: getfield 647	com/android/vending/billing/InAppBillingService/LUCK/StringItem:offset	[B
    //   4848: iconst_0
    //   4849: baload
    //   4850: bastore
    //   4851: aload 12
    //   4853: iconst_0
    //   4854: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   4857: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   4860: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   4863: iconst_3
    //   4864: aload 17
    //   4866: getfield 647	com/android/vending/billing/InAppBillingService/LUCK/StringItem:offset	[B
    //   4869: iconst_1
    //   4870: baload
    //   4871: bastore
    //   4872: aload 12
    //   4874: iconst_1
    //   4875: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   4878: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   4881: iconst_0
    //   4882: putfield 685	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:pattern	Z
    //   4885: goto +1809 -> 6694
    //   4888: aload 12
    //   4890: iconst_0
    //   4891: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   4894: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   4897: iconst_0
    //   4898: putfield 685	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:pattern	Z
    //   4901: aload 12
    //   4903: iconst_1
    //   4904: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   4907: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   4910: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   4913: iconst_2
    //   4914: aload 17
    //   4916: getfield 647	com/android/vending/billing/InAppBillingService/LUCK/StringItem:offset	[B
    //   4919: iconst_0
    //   4920: baload
    //   4921: bastore
    //   4922: aload 12
    //   4924: iconst_1
    //   4925: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   4928: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   4931: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   4934: iconst_3
    //   4935: aload 17
    //   4937: getfield 647	com/android/vending/billing/InAppBillingService/LUCK/StringItem:offset	[B
    //   4940: iconst_1
    //   4941: baload
    //   4942: bastore
    //   4943: aload 12
    //   4945: iconst_1
    //   4946: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   4949: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   4952: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   4955: iconst_4
    //   4956: aload 17
    //   4958: getfield 647	com/android/vending/billing/InAppBillingService/LUCK/StringItem:offset	[B
    //   4961: iconst_2
    //   4962: baload
    //   4963: bastore
    //   4964: aload 12
    //   4966: iconst_1
    //   4967: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   4970: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   4973: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   4976: iconst_5
    //   4977: aload 17
    //   4979: getfield 647	com/android/vending/billing/InAppBillingService/LUCK/StringItem:offset	[B
    //   4982: iconst_3
    //   4983: baload
    //   4984: bastore
    //   4985: goto +1709 -> 6694
    //   4988: iload_3
    //   4989: ifne +9 -> 4998
    //   4992: getstatic 61	com/chelpus/root/utils/runpatchads:full_offline	Z
    //   4995: ifeq +296 -> 5291
    //   4998: ldc_w 687
    //   5001: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   5004: pop
    //   5005: getstatic 270	com/chelpus/root/utils/runpatchads:print	Ljava/io/PrintStream;
    //   5008: ldc_w 687
    //   5011: invokevirtual 275	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   5014: aload 14
    //   5016: invokevirtual 627	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   5019: aload 15
    //   5021: iconst_0
    //   5022: invokestatic 691	com/chelpus/Utils:getMethodsIds	(Ljava/lang/String;Ljava/util/ArrayList;Z)Z
    //   5025: pop
    //   5026: getstatic 696	java/lang/System:out	Ljava/io/PrintStream;
    //   5029: aload 15
    //   5031: iconst_0
    //   5032: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   5035: checkcast 633	com/android/vending/billing/InAppBillingService/LUCK/CommandItem
    //   5038: getfield 699	com/android/vending/billing/InAppBillingService/LUCK/CommandItem:index_command	[B
    //   5041: iconst_0
    //   5042: baload
    //   5043: invokestatic 705	java/lang/Integer:toHexString	(I)Ljava/lang/String;
    //   5046: invokevirtual 275	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   5049: getstatic 696	java/lang/System:out	Ljava/io/PrintStream;
    //   5052: aload 15
    //   5054: iconst_0
    //   5055: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   5058: checkcast 633	com/android/vending/billing/InAppBillingService/LUCK/CommandItem
    //   5061: getfield 699	com/android/vending/billing/InAppBillingService/LUCK/CommandItem:index_command	[B
    //   5064: iconst_1
    //   5065: baload
    //   5066: invokestatic 705	java/lang/Integer:toHexString	(I)Ljava/lang/String;
    //   5069: invokevirtual 275	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   5072: aload 15
    //   5074: invokevirtual 204	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   5077: astore 15
    //   5079: aload 15
    //   5081: invokeinterface 209 1 0
    //   5086: ifeq +162 -> 5248
    //   5089: aload 15
    //   5091: invokeinterface 213 1 0
    //   5096: checkcast 633	com/android/vending/billing/InAppBillingService/LUCK/CommandItem
    //   5099: astore 16
    //   5101: aload 16
    //   5103: getfield 641	com/android/vending/billing/InAppBillingService/LUCK/CommandItem:object	Ljava/lang/String;
    //   5106: ldc_w 620
    //   5109: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   5112: ifeq -33 -> 5079
    //   5115: aload 16
    //   5117: getfield 708	com/android/vending/billing/InAppBillingService/LUCK/CommandItem:found_index_command	Z
    //   5120: ifeq +99 -> 5219
    //   5123: getstatic 696	java/lang/System:out	Ljava/io/PrintStream;
    //   5126: ldc_w 710
    //   5129: invokevirtual 275	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   5132: aload 12
    //   5134: iconst_2
    //   5135: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   5138: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   5141: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   5144: iconst_2
    //   5145: aload 16
    //   5147: getfield 699	com/android/vending/billing/InAppBillingService/LUCK/CommandItem:index_command	[B
    //   5150: iconst_0
    //   5151: baload
    //   5152: bastore
    //   5153: aload 12
    //   5155: iconst_2
    //   5156: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   5159: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   5162: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   5165: iconst_3
    //   5166: aload 16
    //   5168: getfield 699	com/android/vending/billing/InAppBillingService/LUCK/CommandItem:index_command	[B
    //   5171: iconst_1
    //   5172: baload
    //   5173: bastore
    //   5174: aload 12
    //   5176: iconst_3
    //   5177: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   5180: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   5183: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   5186: iconst_2
    //   5187: aload 16
    //   5189: getfield 699	com/android/vending/billing/InAppBillingService/LUCK/CommandItem:index_command	[B
    //   5192: iconst_0
    //   5193: baload
    //   5194: bastore
    //   5195: aload 12
    //   5197: iconst_3
    //   5198: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   5201: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   5204: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   5207: iconst_3
    //   5208: aload 16
    //   5210: getfield 699	com/android/vending/billing/InAppBillingService/LUCK/CommandItem:index_command	[B
    //   5213: iconst_1
    //   5214: baload
    //   5215: bastore
    //   5216: goto -137 -> 5079
    //   5219: aload 12
    //   5221: iconst_2
    //   5222: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   5225: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   5228: iconst_0
    //   5229: putfield 685	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:pattern	Z
    //   5232: aload 12
    //   5234: iconst_3
    //   5235: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   5238: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   5241: iconst_0
    //   5242: putfield 685	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:pattern	Z
    //   5245: goto -166 -> 5079
    //   5248: iload_3
    //   5249: ifne +42 -> 5291
    //   5252: aload 12
    //   5254: iconst_0
    //   5255: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   5258: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   5261: iconst_0
    //   5262: putfield 685	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:pattern	Z
    //   5265: aload 12
    //   5267: iconst_1
    //   5268: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   5271: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   5274: iconst_0
    //   5275: putfield 685	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:pattern	Z
    //   5278: aload 12
    //   5280: iconst_2
    //   5281: invokevirtual 677	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   5284: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   5287: iconst_0
    //   5288: putfield 685	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:pattern	Z
    //   5291: aload 12
    //   5293: invokevirtual 200	java/util/ArrayList:size	()I
    //   5296: anewarray 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   5299: astore 15
    //   5301: iconst_0
    //   5302: istore_3
    //   5303: aload 12
    //   5305: invokevirtual 204	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   5308: astore 16
    //   5310: aload 16
    //   5312: invokeinterface 209 1 0
    //   5317: ifeq +24 -> 5341
    //   5320: aload 15
    //   5322: iload_3
    //   5323: aload 16
    //   5325: invokeinterface 213 1 0
    //   5330: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   5333: aastore
    //   5334: iload_3
    //   5335: iconst_1
    //   5336: iadd
    //   5337: istore_3
    //   5338: goto -28 -> 5310
    //   5341: iconst_0
    //   5342: istore_3
    //   5343: getstatic 45	com/chelpus/root/utils/runpatchads:fileblock	Z
    //   5346: ifeq +34 -> 5380
    //   5349: ldc_w 624
    //   5352: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   5355: pop
    //   5356: getstatic 270	com/chelpus/root/utils/runpatchads:print	Ljava/io/PrintStream;
    //   5359: ldc_w 624
    //   5362: invokevirtual 275	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   5365: aload 14
    //   5367: invokevirtual 627	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   5370: getstatic 562	com/chelpus/root/utils/runpatchads:sites	[[B
    //   5373: iconst_0
    //   5374: bipush 64
    //   5376: invokestatic 714	com/chelpus/Utils:setStringIds	(Ljava/lang/String;[[BZB)I
    //   5379: istore_3
    //   5380: iload_3
    //   5381: ifle +26 -> 5407
    //   5384: iload_3
    //   5385: iconst_1
    //   5386: isub
    //   5387: istore_3
    //   5388: ldc_w 716
    //   5391: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   5394: pop
    //   5395: getstatic 270	com/chelpus/root/utils/runpatchads:print	Ljava/io/PrintStream;
    //   5398: ldc_w 716
    //   5401: invokevirtual 275	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   5404: goto -24 -> 5380
    //   5407: new 159	java/lang/StringBuilder
    //   5410: dup
    //   5411: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   5414: ldc_w 718
    //   5417: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5420: aload 14
    //   5422: invokevirtual 627	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   5425: iconst_1
    //   5426: anewarray 297	java/lang/String
    //   5429: dup
    //   5430: iconst_0
    //   5431: ldc_w 720
    //   5434: aastore
    //   5435: iconst_0
    //   5436: iconst_1
    //   5437: anewarray 297	java/lang/String
    //   5440: dup
    //   5441: iconst_0
    //   5442: ldc_w 722
    //   5445: aastore
    //   5446: invokestatic 726	com/chelpus/Utils:replaceStringIds	(Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)I
    //   5449: invokevirtual 169	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   5452: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   5455: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   5458: pop
    //   5459: invokestatic 729	java/lang/System:currentTimeMillis	()J
    //   5462: lstore 9
    //   5464: new 731	java/io/RandomAccessFile
    //   5467: dup
    //   5468: aload 14
    //   5470: ldc_w 733
    //   5473: invokespecial 736	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   5476: invokevirtual 740	java/io/RandomAccessFile:getChannel	()Ljava/nio/channels/FileChannel;
    //   5479: astore 14
    //   5481: new 159	java/lang/StringBuilder
    //   5484: dup
    //   5485: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   5488: ldc_w 742
    //   5491: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5494: aload 14
    //   5496: invokevirtual 746	java/nio/channels/FileChannel:size	()J
    //   5499: invokevirtual 749	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   5502: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   5505: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   5508: pop
    //   5509: aload 14
    //   5511: getstatic 755	java/nio/channels/FileChannel$MapMode:READ_WRITE	Ljava/nio/channels/FileChannel$MapMode;
    //   5514: lconst_0
    //   5515: aload 14
    //   5517: invokevirtual 746	java/nio/channels/FileChannel:size	()J
    //   5520: l2i
    //   5521: i2l
    //   5522: invokevirtual 759	java/nio/channels/FileChannel:map	(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    //   5525: astore 16
    //   5527: iconst_0
    //   5528: istore_3
    //   5529: iconst_0
    //   5530: istore 5
    //   5532: aload 16
    //   5534: invokevirtual 762	java/nio/MappedByteBuffer:hasRemaining	()Z
    //   5537: ifeq +488 -> 6025
    //   5540: iload 5
    //   5542: istore 4
    //   5544: getstatic 43	com/chelpus/root/utils/runpatchads:createAPK	Z
    //   5547: ifne +56 -> 5603
    //   5550: iload 5
    //   5552: istore 4
    //   5554: aload 16
    //   5556: invokevirtual 764	java/nio/MappedByteBuffer:position	()I
    //   5559: iload 5
    //   5561: isub
    //   5562: ldc_w 765
    //   5565: if_icmple +38 -> 5603
    //   5568: new 159	java/lang/StringBuilder
    //   5571: dup
    //   5572: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   5575: ldc_w 767
    //   5578: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5581: aload 16
    //   5583: invokevirtual 764	java/nio/MappedByteBuffer:position	()I
    //   5586: invokevirtual 169	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   5589: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   5592: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   5595: pop
    //   5596: aload 16
    //   5598: invokevirtual 764	java/nio/MappedByteBuffer:position	()I
    //   5601: istore 4
    //   5603: aload 16
    //   5605: invokevirtual 764	java/nio/MappedByteBuffer:position	()I
    //   5608: istore 8
    //   5610: aload 16
    //   5612: invokevirtual 110	java/nio/MappedByteBuffer:get	()B
    //   5615: istore_2
    //   5616: iconst_0
    //   5617: istore 5
    //   5619: iload_3
    //   5620: istore 6
    //   5622: iload 5
    //   5624: aload 15
    //   5626: arraylength
    //   5627: if_icmpge +916 -> 6543
    //   5630: aload 15
    //   5632: iload 5
    //   5634: aaload
    //   5635: astore 17
    //   5637: aload 16
    //   5639: iload 8
    //   5641: invokevirtual 106	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   5644: pop
    //   5645: iload 6
    //   5647: istore_3
    //   5648: aload 17
    //   5650: getfield 770	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:markerTrig	Z
    //   5653: ifeq +441 -> 6094
    //   5656: iload 6
    //   5658: istore_3
    //   5659: iload 5
    //   5661: iconst_2
    //   5662: if_icmpne +432 -> 6094
    //   5665: iload 6
    //   5667: iconst_1
    //   5668: iadd
    //   5669: istore 7
    //   5671: iload 7
    //   5673: sipush 574
    //   5676: if_icmpge +770 -> 6446
    //   5679: iload 7
    //   5681: istore_3
    //   5682: iload_2
    //   5683: aload 17
    //   5685: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   5688: iconst_0
    //   5689: baload
    //   5690: if_icmpne +394 -> 6084
    //   5693: aload 17
    //   5695: getfield 774	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repMask	[I
    //   5698: iconst_0
    //   5699: iaload
    //   5700: ifne +11 -> 5711
    //   5703: aload 17
    //   5705: getfield 777	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repByte	[B
    //   5708: iconst_0
    //   5709: iload_2
    //   5710: bastore
    //   5711: iconst_1
    //   5712: istore 6
    //   5714: aload 16
    //   5716: iload 8
    //   5718: iconst_1
    //   5719: iadd
    //   5720: invokevirtual 106	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   5723: pop
    //   5724: aload 16
    //   5726: invokevirtual 110	java/nio/MappedByteBuffer:get	()B
    //   5729: istore_1
    //   5730: iload_1
    //   5731: aload 17
    //   5733: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   5736: iload 6
    //   5738: baload
    //   5739: if_icmpeq +57 -> 5796
    //   5742: aload 17
    //   5744: getfield 780	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origMask	[I
    //   5747: iload 6
    //   5749: iaload
    //   5750: iconst_1
    //   5751: if_icmpeq +45 -> 5796
    //   5754: aload 17
    //   5756: getfield 780	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origMask	[I
    //   5759: iload 6
    //   5761: iaload
    //   5762: bipush 20
    //   5764: if_icmpeq +32 -> 5796
    //   5767: aload 17
    //   5769: getfield 780	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origMask	[I
    //   5772: iload 6
    //   5774: iaload
    //   5775: bipush 21
    //   5777: if_icmpeq +19 -> 5796
    //   5780: iload 7
    //   5782: istore_3
    //   5783: aload 17
    //   5785: getfield 780	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origMask	[I
    //   5788: iload 6
    //   5790: iaload
    //   5791: bipush 23
    //   5793: if_icmpne +291 -> 6084
    //   5796: aload 17
    //   5798: getfield 774	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repMask	[I
    //   5801: iload 6
    //   5803: iaload
    //   5804: ifne +12 -> 5816
    //   5807: aload 17
    //   5809: getfield 777	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repByte	[B
    //   5812: iload 6
    //   5814: iload_1
    //   5815: bastore
    //   5816: aload 17
    //   5818: getfield 774	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repMask	[I
    //   5821: iload 6
    //   5823: iaload
    //   5824: bipush 20
    //   5826: if_icmpne +16 -> 5842
    //   5829: aload 17
    //   5831: getfield 777	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repByte	[B
    //   5834: iload 6
    //   5836: iload_1
    //   5837: bipush 15
    //   5839: iand
    //   5840: i2b
    //   5841: bastore
    //   5842: aload 17
    //   5844: getfield 774	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repMask	[I
    //   5847: iload 6
    //   5849: iaload
    //   5850: bipush 21
    //   5852: if_icmpne +19 -> 5871
    //   5855: aload 17
    //   5857: getfield 777	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repByte	[B
    //   5860: iload 6
    //   5862: iload_1
    //   5863: bipush 15
    //   5865: iand
    //   5866: bipush 16
    //   5868: iadd
    //   5869: i2b
    //   5870: bastore
    //   5871: iload 6
    //   5873: iconst_1
    //   5874: iadd
    //   5875: istore 6
    //   5877: iload 6
    //   5879: aload 17
    //   5881: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   5884: arraylength
    //   5885: if_icmpne +552 -> 6437
    //   5888: aload 16
    //   5890: iload 8
    //   5892: invokevirtual 106	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   5895: pop
    //   5896: aload 16
    //   5898: aload 17
    //   5900: getfield 777	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repByte	[B
    //   5903: invokevirtual 114	java/nio/MappedByteBuffer:put	([B)Ljava/nio/ByteBuffer;
    //   5906: pop
    //   5907: aload 16
    //   5909: invokevirtual 118	java/nio/MappedByteBuffer:force	()Ljava/nio/MappedByteBuffer;
    //   5912: pop
    //   5913: aload 17
    //   5915: getfield 783	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:resultText	Ljava/lang/String;
    //   5918: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   5921: pop
    //   5922: getstatic 270	com/chelpus/root/utils/runpatchads:print	Ljava/io/PrintStream;
    //   5925: aload 17
    //   5927: getfield 783	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:resultText	Ljava/lang/String;
    //   5930: invokevirtual 275	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   5933: aload 17
    //   5935: iconst_1
    //   5936: putfield 785	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:result	Z
    //   5939: aload 17
    //   5941: iconst_0
    //   5942: putfield 770	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:markerTrig	Z
    //   5945: aload 12
    //   5947: invokevirtual 204	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   5950: astore 18
    //   5952: aload 18
    //   5954: invokeinterface 209 1 0
    //   5959: ifeq +113 -> 6072
    //   5962: aload 18
    //   5964: invokeinterface 213 1 0
    //   5969: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   5972: astore 19
    //   5974: aload 19
    //   5976: getfield 788	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:marker	Ljava/lang/String;
    //   5979: aload 17
    //   5981: getfield 788	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:marker	Ljava/lang/String;
    //   5984: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   5987: ifeq -35 -> 5952
    //   5990: aload 19
    //   5992: iconst_0
    //   5993: putfield 770	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:markerTrig	Z
    //   5996: goto -44 -> 5952
    //   5999: astore 15
    //   6001: new 159	java/lang/StringBuilder
    //   6004: dup
    //   6005: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   6008: ldc 73
    //   6010: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6013: aload 15
    //   6015: invokevirtual 791	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   6018: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   6021: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   6024: pop
    //   6025: aload 14
    //   6027: invokevirtual 792	java/nio/channels/FileChannel:close	()V
    //   6030: new 159	java/lang/StringBuilder
    //   6033: dup
    //   6034: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   6037: ldc 73
    //   6039: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6042: invokestatic 729	java/lang/System:currentTimeMillis	()J
    //   6045: lload 9
    //   6047: lsub
    //   6048: ldc2_w 793
    //   6051: ldiv
    //   6052: invokevirtual 749	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   6055: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   6058: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   6061: pop
    //   6062: ldc_w 796
    //   6065: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   6068: pop
    //   6069: goto -1812 -> 4257
    //   6072: iconst_0
    //   6073: istore_3
    //   6074: aload 16
    //   6076: iload 8
    //   6078: iconst_1
    //   6079: iadd
    //   6080: invokevirtual 106	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   6083: pop
    //   6084: aload 16
    //   6086: iload 8
    //   6088: iconst_1
    //   6089: iadd
    //   6090: invokevirtual 106	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   6093: pop
    //   6094: aload 17
    //   6096: getfield 770	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:markerTrig	Z
    //   6099: ifne +600 -> 6699
    //   6102: iload_2
    //   6103: aload 17
    //   6105: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   6108: iconst_0
    //   6109: baload
    //   6110: if_icmpne +589 -> 6699
    //   6113: aload 17
    //   6115: getfield 685	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:pattern	Z
    //   6118: ifeq +581 -> 6699
    //   6121: aload 17
    //   6123: getfield 774	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repMask	[I
    //   6126: iconst_0
    //   6127: iaload
    //   6128: ifne +11 -> 6139
    //   6131: aload 17
    //   6133: getfield 777	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repByte	[B
    //   6136: iconst_0
    //   6137: iload_2
    //   6138: bastore
    //   6139: iconst_1
    //   6140: istore 6
    //   6142: aload 16
    //   6144: iload 8
    //   6146: iconst_1
    //   6147: iadd
    //   6148: invokevirtual 106	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   6151: pop
    //   6152: aload 16
    //   6154: invokevirtual 110	java/nio/MappedByteBuffer:get	()B
    //   6157: istore_1
    //   6158: iload_1
    //   6159: aload 17
    //   6161: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   6164: iload 6
    //   6166: baload
    //   6167: if_icmpeq +54 -> 6221
    //   6170: aload 17
    //   6172: getfield 780	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origMask	[I
    //   6175: iload 6
    //   6177: iaload
    //   6178: iconst_1
    //   6179: if_icmpeq +42 -> 6221
    //   6182: aload 17
    //   6184: getfield 780	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origMask	[I
    //   6187: iload 6
    //   6189: iaload
    //   6190: bipush 20
    //   6192: if_icmpeq +29 -> 6221
    //   6195: aload 17
    //   6197: getfield 780	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origMask	[I
    //   6200: iload 6
    //   6202: iaload
    //   6203: bipush 21
    //   6205: if_icmpeq +16 -> 6221
    //   6208: aload 17
    //   6210: getfield 780	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origMask	[I
    //   6213: iload 6
    //   6215: iaload
    //   6216: bipush 23
    //   6218: if_icmpne +312 -> 6530
    //   6221: aload 17
    //   6223: getfield 774	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repMask	[I
    //   6226: iload 6
    //   6228: iaload
    //   6229: ifne +12 -> 6241
    //   6232: aload 17
    //   6234: getfield 777	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repByte	[B
    //   6237: iload 6
    //   6239: iload_1
    //   6240: bastore
    //   6241: aload 17
    //   6243: getfield 774	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repMask	[I
    //   6246: iload 6
    //   6248: iaload
    //   6249: bipush 20
    //   6251: if_icmpne +16 -> 6267
    //   6254: aload 17
    //   6256: getfield 777	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repByte	[B
    //   6259: iload 6
    //   6261: iload_1
    //   6262: bipush 15
    //   6264: iand
    //   6265: i2b
    //   6266: bastore
    //   6267: aload 17
    //   6269: getfield 774	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repMask	[I
    //   6272: iload 6
    //   6274: iaload
    //   6275: bipush 21
    //   6277: if_icmpne +19 -> 6296
    //   6280: aload 17
    //   6282: getfield 777	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repByte	[B
    //   6285: iload 6
    //   6287: iload_1
    //   6288: bipush 15
    //   6290: iand
    //   6291: bipush 16
    //   6293: iadd
    //   6294: i2b
    //   6295: bastore
    //   6296: iload 6
    //   6298: iconst_1
    //   6299: iadd
    //   6300: istore 6
    //   6302: iload 6
    //   6304: aload 17
    //   6306: getfield 682	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:origByte	[B
    //   6309: arraylength
    //   6310: if_icmpne +211 -> 6521
    //   6313: aload 16
    //   6315: iload 8
    //   6317: invokevirtual 106	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   6320: pop
    //   6321: aload 16
    //   6323: aload 17
    //   6325: getfield 777	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:repByte	[B
    //   6328: invokevirtual 114	java/nio/MappedByteBuffer:put	([B)Ljava/nio/ByteBuffer;
    //   6331: pop
    //   6332: aload 16
    //   6334: invokevirtual 118	java/nio/MappedByteBuffer:force	()Ljava/nio/MappedByteBuffer;
    //   6337: pop
    //   6338: aload 17
    //   6340: getfield 783	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:resultText	Ljava/lang/String;
    //   6343: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   6346: pop
    //   6347: getstatic 270	com/chelpus/root/utils/runpatchads:print	Ljava/io/PrintStream;
    //   6350: aload 17
    //   6352: getfield 783	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:resultText	Ljava/lang/String;
    //   6355: invokevirtual 275	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   6358: aload 17
    //   6360: iconst_1
    //   6361: putfield 785	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:result	Z
    //   6364: aload 17
    //   6366: getfield 788	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:marker	Ljava/lang/String;
    //   6369: ldc 73
    //   6371: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   6374: ifne +156 -> 6530
    //   6377: aload 17
    //   6379: iconst_1
    //   6380: putfield 770	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:markerTrig	Z
    //   6383: aload 12
    //   6385: invokevirtual 204	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   6388: astore 18
    //   6390: aload 18
    //   6392: invokeinterface 209 1 0
    //   6397: ifeq +133 -> 6530
    //   6400: aload 18
    //   6402: invokeinterface 213 1 0
    //   6407: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   6410: astore 19
    //   6412: aload 19
    //   6414: getfield 788	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:marker	Ljava/lang/String;
    //   6417: aload 17
    //   6419: getfield 788	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:marker	Ljava/lang/String;
    //   6422: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   6425: ifeq -35 -> 6390
    //   6428: aload 19
    //   6430: iconst_1
    //   6431: putfield 770	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:markerTrig	Z
    //   6434: goto -44 -> 6390
    //   6437: aload 16
    //   6439: invokevirtual 110	java/nio/MappedByteBuffer:get	()B
    //   6442: istore_1
    //   6443: goto -713 -> 5730
    //   6446: aload 17
    //   6448: iconst_0
    //   6449: putfield 770	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:markerTrig	Z
    //   6452: aload 12
    //   6454: invokevirtual 204	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   6457: astore 18
    //   6459: aload 18
    //   6461: invokeinterface 209 1 0
    //   6466: ifeq +40 -> 6506
    //   6469: aload 18
    //   6471: invokeinterface 213 1 0
    //   6476: checkcast 679	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
    //   6479: astore 19
    //   6481: aload 19
    //   6483: getfield 788	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:marker	Ljava/lang/String;
    //   6486: aload 17
    //   6488: getfield 788	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:marker	Ljava/lang/String;
    //   6491: invokevirtual 301	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   6494: ifeq -35 -> 6459
    //   6497: aload 19
    //   6499: iconst_0
    //   6500: putfield 770	com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto:markerTrig	Z
    //   6503: goto -44 -> 6459
    //   6506: iconst_0
    //   6507: istore_3
    //   6508: aload 16
    //   6510: iload 8
    //   6512: iconst_1
    //   6513: iadd
    //   6514: invokevirtual 106	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   6517: pop
    //   6518: goto -424 -> 6094
    //   6521: aload 16
    //   6523: invokevirtual 110	java/nio/MappedByteBuffer:get	()B
    //   6526: istore_1
    //   6527: goto -369 -> 6158
    //   6530: aload 16
    //   6532: iload 8
    //   6534: iconst_1
    //   6535: iadd
    //   6536: invokevirtual 106	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   6539: pop
    //   6540: goto +159 -> 6699
    //   6543: iload 4
    //   6545: istore 5
    //   6547: iload 6
    //   6549: istore_3
    //   6550: iconst_0
    //   6551: ifne -1019 -> 5532
    //   6554: aload 16
    //   6556: iload 8
    //   6558: iconst_1
    //   6559: iadd
    //   6560: invokevirtual 106	java/nio/MappedByteBuffer:position	(I)Ljava/nio/Buffer;
    //   6563: pop
    //   6564: iload 4
    //   6566: istore 5
    //   6568: iload 6
    //   6570: istore_3
    //   6571: goto -1039 -> 5532
    //   6574: getstatic 43	com/chelpus/root/utils/runpatchads:createAPK	Z
    //   6577: ifne +84 -> 6661
    //   6580: aload_0
    //   6581: iconst_3
    //   6582: aaload
    //   6583: getstatic 96	com/chelpus/root/utils/runpatchads:classesFiles	Ljava/util/ArrayList;
    //   6586: aload_0
    //   6587: iconst_2
    //   6588: aaload
    //   6589: getstatic 75	com/chelpus/root/utils/runpatchads:uid	Ljava/lang/String;
    //   6592: aload_0
    //   6593: iconst_2
    //   6594: aaload
    //   6595: getstatic 75	com/chelpus/root/utils/runpatchads:uid	Ljava/lang/String;
    //   6598: invokestatic 800	com/chelpus/Utils:getOdexForCreate	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   6601: invokestatic 804	com/chelpus/Utils:create_ODEX_root	(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    //   6604: istore_3
    //   6605: new 159	java/lang/StringBuilder
    //   6608: dup
    //   6609: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   6612: ldc_w 806
    //   6615: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6618: iload_3
    //   6619: invokevirtual 169	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   6622: invokevirtual 175	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   6625: invokestatic 124	com/chelpus/Utils:sendFromRoot	(Ljava/lang/String;)Z
    //   6628: pop
    //   6629: iload_3
    //   6630: ifne +31 -> 6661
    //   6633: getstatic 65	com/chelpus/root/utils/runpatchads:ART	Z
    //   6636: ifne +25 -> 6661
    //   6639: aload_0
    //   6640: iconst_1
    //   6641: aaload
    //   6642: aload_0
    //   6643: iconst_2
    //   6644: aaload
    //   6645: aload_0
    //   6646: iconst_2
    //   6647: aaload
    //   6648: iconst_1
    //   6649: invokestatic 610	com/chelpus/Utils:getPlaceForOdex	(Ljava/lang/String;Z)Ljava/lang/String;
    //   6652: getstatic 75	com/chelpus/root/utils/runpatchads:uid	Ljava/lang/String;
    //   6655: aload_0
    //   6656: iconst_3
    //   6657: aaload
    //   6658: invokestatic 810	com/chelpus/Utils:afterPatch	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   6661: getstatic 43	com/chelpus/root/utils/runpatchads:createAPK	Z
    //   6664: ifne +6 -> 6670
    //   6667: invokestatic 813	com/chelpus/Utils:exitFromRootJava	()V
    //   6670: aload 11
    //   6672: getfield 816	com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream:allresult	Ljava/lang/String;
    //   6675: putstatic 818	com/chelpus/root/utils/runpatchads:result	Ljava/lang/String;
    //   6678: return
    //   6679: astore 13
    //   6681: goto -6188 -> 493
    //   6684: astore 13
    //   6686: goto -6193 -> 493
    //   6689: astore 13
    //   6691: goto -6214 -> 477
    //   6694: iconst_1
    //   6695: istore_3
    //   6696: goto -2308 -> 4388
    //   6699: iload 5
    //   6701: iconst_1
    //   6702: iadd
    //   6703: istore 5
    //   6705: iload_3
    //   6706: istore 6
    //   6708: goto -1086 -> 5622
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	6711	0	paramArrayOfString	String[]
    //   5729	798	1	i	int
    //   5615	523	2	j	int
    //   132	6574	3	k	int
    //   129	6436	4	m	int
    //   5530	1174	5	n	int
    //   5620	1087	6	i1	int
    //   5669	112	7	i2	int
    //   5608	952	8	i3	int
    //   5462	584	9	l	long
    //   10	6661	11	localLogOutputStream	com.android.vending.billing.InAppBillingService.LUCK.LogOutputStream
    //   63	3376	12	localArrayList	ArrayList
    //   3856	1	12	localFileNotFoundException	java.io.FileNotFoundException
    //   3871	13	12	localIterator	Iterator
    //   4102	2351	12	localException1	Exception
    //   124	16	13	arrayOfFile	File[]
    //   208	3	13	localException2	Exception
    //   513	3316	13	localObject1	Object
    //   3912	1	13	localNullPointerException1	NullPointerException
    //   4062	707	13	localObject2	Object
    //   6679	1	13	localException3	Exception
    //   6684	1	13	localNullPointerException2	NullPointerException
    //   6689	1	13	localException4	Exception
    //   143	3350	14	localObject3	Object
    //   3554	3	14	localException5	Exception
    //   3580	11	14	localFileInputStream	FileInputStream
    //   3646	397	14	localException6	Exception
    //   4084	1942	14	localObject4	Object
    //   531	5100	15	localObject5	Object
    //   5999	15	15	localException7	Exception
    //   540	6015	16	localObject6	Object
    //   549	5938	17	localObject7	Object
    //   558	5912	18	localObject8	Object
    //   567	5931	19	localObject9	Object
    // Exception table:
    //   from	to	target	type
    //   111	131	208	java/lang/Exception
    //   145	201	208	java/lang/Exception
    //   3465	3508	3554	java/lang/Exception
    //   3508	3515	3554	java/lang/Exception
    //   3520	3551	3554	java/lang/Exception
    //   4034	4039	3554	java/lang/Exception
    //   3561	3600	3646	java/lang/Exception
    //   3600	3607	3646	java/lang/Exception
    //   3612	3643	3646	java/lang/Exception
    //   4042	4047	3646	java/lang/Exception
    //   3689	3708	3856	java/io/FileNotFoundException
    //   3708	3751	3856	java/io/FileNotFoundException
    //   3751	3767	3856	java/io/FileNotFoundException
    //   3767	3780	3856	java/io/FileNotFoundException
    //   3785	3848	3856	java/io/FileNotFoundException
    //   3848	3856	3856	java/io/FileNotFoundException
    //   4050	4064	3856	java/io/FileNotFoundException
    //   4064	4102	3856	java/io/FileNotFoundException
    //   4135	4144	3856	java/io/FileNotFoundException
    //   4147	4181	3856	java/io/FileNotFoundException
    //   4181	4215	3856	java/io/FileNotFoundException
    //   4215	4249	3856	java/io/FileNotFoundException
    //   4249	4257	3856	java/io/FileNotFoundException
    //   4257	4351	3856	java/io/FileNotFoundException
    //   4353	4388	3856	java/io/FileNotFoundException
    //   4388	4417	3856	java/io/FileNotFoundException
    //   4417	4465	3856	java/io/FileNotFoundException
    //   4465	4491	3856	java/io/FileNotFoundException
    //   4494	4500	3856	java/io/FileNotFoundException
    //   4505	4603	3856	java/io/FileNotFoundException
    //   4603	4611	3856	java/io/FileNotFoundException
    //   4611	4625	3856	java/io/FileNotFoundException
    //   4625	4663	3856	java/io/FileNotFoundException
    //   4663	4672	3856	java/io/FileNotFoundException
    //   4675	4688	3856	java/io/FileNotFoundException
    //   4693	4736	3856	java/io/FileNotFoundException
    //   4736	4744	3856	java/io/FileNotFoundException
    //   4744	4758	3856	java/io/FileNotFoundException
    //   4758	4796	3856	java/io/FileNotFoundException
    //   4796	4805	3856	java/io/FileNotFoundException
    //   4808	4885	3856	java/io/FileNotFoundException
    //   4888	4985	3856	java/io/FileNotFoundException
    //   4992	4998	3856	java/io/FileNotFoundException
    //   4998	5079	3856	java/io/FileNotFoundException
    //   5079	5216	3856	java/io/FileNotFoundException
    //   5219	5245	3856	java/io/FileNotFoundException
    //   5252	5291	3856	java/io/FileNotFoundException
    //   5291	5301	3856	java/io/FileNotFoundException
    //   5303	5310	3856	java/io/FileNotFoundException
    //   5310	5334	3856	java/io/FileNotFoundException
    //   5343	5380	3856	java/io/FileNotFoundException
    //   5388	5404	3856	java/io/FileNotFoundException
    //   5407	5527	3856	java/io/FileNotFoundException
    //   5532	5540	3856	java/io/FileNotFoundException
    //   5544	5550	3856	java/io/FileNotFoundException
    //   5554	5603	3856	java/io/FileNotFoundException
    //   5603	5616	3856	java/io/FileNotFoundException
    //   5622	5630	3856	java/io/FileNotFoundException
    //   5637	5645	3856	java/io/FileNotFoundException
    //   5648	5656	3856	java/io/FileNotFoundException
    //   5682	5711	3856	java/io/FileNotFoundException
    //   5714	5730	3856	java/io/FileNotFoundException
    //   5730	5780	3856	java/io/FileNotFoundException
    //   5783	5796	3856	java/io/FileNotFoundException
    //   5796	5816	3856	java/io/FileNotFoundException
    //   5816	5842	3856	java/io/FileNotFoundException
    //   5842	5871	3856	java/io/FileNotFoundException
    //   5877	5952	3856	java/io/FileNotFoundException
    //   5952	5996	3856	java/io/FileNotFoundException
    //   6001	6025	3856	java/io/FileNotFoundException
    //   6025	6069	3856	java/io/FileNotFoundException
    //   6074	6084	3856	java/io/FileNotFoundException
    //   6084	6094	3856	java/io/FileNotFoundException
    //   6094	6139	3856	java/io/FileNotFoundException
    //   6142	6158	3856	java/io/FileNotFoundException
    //   6158	6221	3856	java/io/FileNotFoundException
    //   6221	6241	3856	java/io/FileNotFoundException
    //   6241	6267	3856	java/io/FileNotFoundException
    //   6267	6296	3856	java/io/FileNotFoundException
    //   6302	6390	3856	java/io/FileNotFoundException
    //   6390	6434	3856	java/io/FileNotFoundException
    //   6437	6443	3856	java/io/FileNotFoundException
    //   6446	6459	3856	java/io/FileNotFoundException
    //   6459	6503	3856	java/io/FileNotFoundException
    //   6508	6518	3856	java/io/FileNotFoundException
    //   6521	6527	3856	java/io/FileNotFoundException
    //   6530	6540	3856	java/io/FileNotFoundException
    //   6554	6564	3856	java/io/FileNotFoundException
    //   215	231	3912	java/lang/NullPointerException
    //   231	247	3912	java/lang/NullPointerException
    //   247	263	3912	java/lang/NullPointerException
    //   263	279	3912	java/lang/NullPointerException
    //   279	295	3912	java/lang/NullPointerException
    //   295	311	3912	java/lang/NullPointerException
    //   311	327	3912	java/lang/NullPointerException
    //   327	343	3912	java/lang/NullPointerException
    //   343	359	3912	java/lang/NullPointerException
    //   366	383	3912	java/lang/NullPointerException
    //   390	407	3912	java/lang/NullPointerException
    //   414	422	3912	java/lang/NullPointerException
    //   429	463	3912	java/lang/NullPointerException
    //   470	477	3912	java/lang/NullPointerException
    //   3902	3909	3912	java/lang/NullPointerException
    //   3689	3708	4102	java/lang/Exception
    //   3708	3751	4102	java/lang/Exception
    //   3751	3767	4102	java/lang/Exception
    //   3767	3780	4102	java/lang/Exception
    //   3785	3848	4102	java/lang/Exception
    //   3848	3856	4102	java/lang/Exception
    //   4050	4064	4102	java/lang/Exception
    //   4064	4102	4102	java/lang/Exception
    //   4135	4144	4102	java/lang/Exception
    //   4147	4181	4102	java/lang/Exception
    //   4181	4215	4102	java/lang/Exception
    //   4215	4249	4102	java/lang/Exception
    //   4249	4257	4102	java/lang/Exception
    //   4257	4351	4102	java/lang/Exception
    //   4353	4388	4102	java/lang/Exception
    //   4388	4417	4102	java/lang/Exception
    //   4417	4465	4102	java/lang/Exception
    //   4465	4491	4102	java/lang/Exception
    //   4494	4500	4102	java/lang/Exception
    //   4505	4603	4102	java/lang/Exception
    //   4603	4611	4102	java/lang/Exception
    //   4611	4625	4102	java/lang/Exception
    //   4625	4663	4102	java/lang/Exception
    //   4663	4672	4102	java/lang/Exception
    //   4675	4688	4102	java/lang/Exception
    //   4693	4736	4102	java/lang/Exception
    //   4736	4744	4102	java/lang/Exception
    //   4744	4758	4102	java/lang/Exception
    //   4758	4796	4102	java/lang/Exception
    //   4796	4805	4102	java/lang/Exception
    //   4808	4885	4102	java/lang/Exception
    //   4888	4985	4102	java/lang/Exception
    //   4992	4998	4102	java/lang/Exception
    //   4998	5079	4102	java/lang/Exception
    //   5079	5216	4102	java/lang/Exception
    //   5219	5245	4102	java/lang/Exception
    //   5252	5291	4102	java/lang/Exception
    //   5291	5301	4102	java/lang/Exception
    //   5303	5310	4102	java/lang/Exception
    //   5310	5334	4102	java/lang/Exception
    //   5343	5380	4102	java/lang/Exception
    //   5388	5404	4102	java/lang/Exception
    //   5407	5527	4102	java/lang/Exception
    //   6001	6025	4102	java/lang/Exception
    //   6025	6069	4102	java/lang/Exception
    //   5532	5540	5999	java/lang/Exception
    //   5544	5550	5999	java/lang/Exception
    //   5554	5603	5999	java/lang/Exception
    //   5603	5616	5999	java/lang/Exception
    //   5622	5630	5999	java/lang/Exception
    //   5637	5645	5999	java/lang/Exception
    //   5648	5656	5999	java/lang/Exception
    //   5682	5711	5999	java/lang/Exception
    //   5714	5730	5999	java/lang/Exception
    //   5730	5780	5999	java/lang/Exception
    //   5783	5796	5999	java/lang/Exception
    //   5796	5816	5999	java/lang/Exception
    //   5816	5842	5999	java/lang/Exception
    //   5842	5871	5999	java/lang/Exception
    //   5877	5952	5999	java/lang/Exception
    //   5952	5996	5999	java/lang/Exception
    //   6074	6084	5999	java/lang/Exception
    //   6084	6094	5999	java/lang/Exception
    //   6094	6139	5999	java/lang/Exception
    //   6142	6158	5999	java/lang/Exception
    //   6158	6221	5999	java/lang/Exception
    //   6221	6241	5999	java/lang/Exception
    //   6241	6267	5999	java/lang/Exception
    //   6267	6296	5999	java/lang/Exception
    //   6302	6390	5999	java/lang/Exception
    //   6390	6434	5999	java/lang/Exception
    //   6437	6443	5999	java/lang/Exception
    //   6446	6459	5999	java/lang/Exception
    //   6459	6503	5999	java/lang/Exception
    //   6508	6518	5999	java/lang/Exception
    //   6521	6527	5999	java/lang/Exception
    //   6530	6540	5999	java/lang/Exception
    //   6554	6564	5999	java/lang/Exception
    //   477	493	6679	java/lang/Exception
    //   477	493	6684	java/lang/NullPointerException
    //   215	231	6689	java/lang/Exception
    //   231	247	6689	java/lang/Exception
    //   247	263	6689	java/lang/Exception
    //   263	279	6689	java/lang/Exception
    //   279	295	6689	java/lang/Exception
    //   295	311	6689	java/lang/Exception
    //   311	327	6689	java/lang/Exception
    //   327	343	6689	java/lang/Exception
    //   343	359	6689	java/lang/Exception
    //   366	383	6689	java/lang/Exception
    //   390	407	6689	java/lang/Exception
    //   414	422	6689	java/lang/Exception
    //   429	463	6689	java/lang/Exception
    //   470	477	6689	java/lang/Exception
    //   3902	3909	6689	java/lang/Exception
  }
  
  public static void unzipART(File paramFile)
  {
    i = 0;
    try
    {
      FileInputStream localFileInputStream = new FileInputStream(paramFile);
      ZipInputStream localZipInputStream = new ZipInputStream(localFileInputStream);
      Object localObject1 = localZipInputStream.getNextEntry();
      for (;;)
      {
        Object localObject3;
        if ((localObject1 != null) && (1 != 0))
        {
          localObject1 = ((ZipEntry)localObject1).getName();
          if ((((String)localObject1).toLowerCase().startsWith("classes")) && (((String)localObject1).endsWith(".dex")) && (!((String)localObject1).contains("/")))
          {
            localObject3 = new FileOutputStream(sddir + "/" + (String)localObject1);
            byte[] arrayOfByte = new byte['ࠀ'];
            for (;;)
            {
              int j = localZipInputStream.read(arrayOfByte);
              if (j == -1) {
                break;
              }
              ((FileOutputStream)localObject3).write(arrayOfByte, 0, j);
            }
          }
        }
        try
        {
          paramFile = new ZipFile(paramFile);
          paramFile.extractFile("classes.dex", sddir);
          classesFiles.add(new File(sddir + "/" + "classes.dex"));
          Utils.cmdParam(new String[] { "chmod", "777", sddir + "/" + "classes.dex" });
          paramFile.extractFile("AndroidManifest.xml", sddir);
          Utils.cmdParam(new String[] { "chmod", "777", sddir + "/" + "AndroidManifest.xml" });
          Utils.sendFromRoot("Exception e" + localException.toString());
          return;
          localZipInputStream.closeEntry();
          ((FileOutputStream)localObject3).close();
          classesFiles.add(new File(sddir + "/" + localException));
          Utils.cmdParam(new String[] { "chmod", "777", sddir + "/" + localException });
          if (localException.equals("AndroidManifest.xml"))
          {
            localObject2 = new FileOutputStream(sddir + "/" + "AndroidManifest.xml");
            localObject3 = new byte['ࠀ'];
            for (;;)
            {
              i = localZipInputStream.read((byte[])localObject3);
              if (i == -1) {
                break;
              }
              ((FileOutputStream)localObject2).write((byte[])localObject3, 0, i);
            }
            localZipInputStream.closeEntry();
            ((FileOutputStream)localObject2).close();
            Utils.cmdParam(new String[] { "chmod", "777", sddir + "/" + "AndroidManifest.xml" });
            i = 1;
            break label715;
            localZipInputStream.close();
            localFileInputStream.close();
            return;
            localObject2 = localZipInputStream.getNextEntry();
          }
        }
        catch (ZipException paramFile)
        {
          for (;;)
          {
            Utils.sendFromRoot("Error classes.dex decompress! " + paramFile);
            Utils.sendFromRoot("Exception e1" + ((Exception)localObject2).toString());
          }
        }
        catch (Exception paramFile)
        {
          for (;;)
          {
            Object localObject2;
            Utils.sendFromRoot("Error classes.dex decompress! " + paramFile);
            Utils.sendFromRoot("Exception e1" + ((Exception)localObject2).toString());
            continue;
            if ((0 == 0) || (i == 0)) {}
          }
        }
      }
    }
    catch (Exception localException) {}
  }
  
  public static void unzipSD(File paramFile)
  {
    try
    {
      FileInputStream localFileInputStream = new FileInputStream(paramFile);
      localZipInputStream = new ZipInputStream(localFileInputStream);
      do
      {
        localZipEntry = localZipInputStream.getNextEntry();
        if (localZipEntry == null) {
          break;
        }
      } while ((!localZipEntry.getName().toLowerCase().startsWith("classes")) || (!localZipEntry.getName().endsWith(".dex")) || (localZipEntry.getName().contains("/")));
      localFileOutputStream = new FileOutputStream(sddir + "/Modified/" + localZipEntry.getName());
      byte[] arrayOfByte = new byte['Ѐ'];
      for (;;)
      {
        int i = localZipInputStream.read(arrayOfByte);
        if (i == -1) {
          break;
        }
        localFileOutputStream.write(arrayOfByte, 0, i);
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        try
        {
          new ZipFile(paramFile).extractFile("classes.dex", sddir + "/Modified/");
          classesFiles.add(new File(sddir + "/Modified/" + "classes.dex"));
          return;
        }
        catch (ZipException paramFile)
        {
          ZipInputStream localZipInputStream;
          ZipEntry localZipEntry;
          FileOutputStream localFileOutputStream;
          Utils.sendFromRoot("Error classes.dex decompress! " + paramFile);
          Utils.sendFromRoot("Exception e1" + localException.toString());
          return;
        }
        catch (Exception paramFile)
        {
          Utils.sendFromRoot("Error classes.dex decompress! " + paramFile);
          Utils.sendFromRoot("Exception e1" + localException.toString());
        }
        localZipInputStream.closeEntry();
        localFileOutputStream.close();
        classesFiles.add(new File(sddir + "/Modified/" + localZipEntry.getName()));
        continue;
        localZipInputStream.close();
        localException.close();
        return;
      }
    }
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/chelpus/root/utils/runpatchads.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */