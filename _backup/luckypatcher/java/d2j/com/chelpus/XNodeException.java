package com.chelpus;

public class XNodeException
  extends Exception
{
  private static final long serialVersionUID = 6194649056119743851L;
  
  public XNodeException() {}
  
  public XNodeException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/H1GHWAvE/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar!/com/chelpus/XNodeException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */