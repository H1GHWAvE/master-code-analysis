.class public Lpxb/android/StringItems;
.super Ljava/util/ArrayList;
.source "StringItems.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Lpxb/android/StringItem;",
        ">;"
    }
.end annotation


# static fields
.field static final UTF8_FLAG:I = 0x100


# instance fields
.field stringData:[B

.field private useUTF8:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 152
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpxb/android/StringItems;->useUTF8:Z

    return-void
.end method

.method public static read(Ljava/nio/ByteBuffer;)[Ljava/lang/String;
    .locals 23
    .param p0, "in"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v19

    add-int/lit8 v17, v19, -0x8

    .line 32
    .local v17, "trunkOffset":I
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v12

    .line 33
    .local v12, "stringCount":I
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v15

    .line 34
    .local v15, "styleOffsetCount":I
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    .line 35
    .local v6, "flags":I
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v13

    .line 36
    .local v13, "stringDataOffset":I
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v16

    .line 37
    .local v16, "stylesOffset":I
    new-array v9, v12, [I

    .line 38
    .local v9, "offsets":[I
    new-array v14, v12, [Ljava/lang/String;

    .line 39
    .local v14, "strings":[Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v12, :cond_0

    .line 40
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v19

    aput v19, v9, v7

    .line 39
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 43
    :cond_0
    if-eqz v16, :cond_1

    .line 44
    sget-object v19, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "ignore style offset at 0x"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 46
    :cond_1
    add-int v4, v17, v13

    .line 47
    .local v4, "base":I
    const/4 v7, 0x0

    :goto_1
    array-length v0, v9

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v7, v0, :cond_4

    .line 48
    aget v19, v9, v7

    add-int v19, v19, v4

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 51
    and-int/lit16 v0, v6, 0x100

    move/from16 v19, v0

    if-eqz v19, :cond_3

    .line 52
    invoke-static/range {p0 .. p0}, Lpxb/android/StringItems;->u8length(Ljava/nio/ByteBuffer;)I

    .line 53
    invoke-static/range {p0 .. p0}, Lpxb/android/StringItems;->u8length(Ljava/nio/ByteBuffer;)I

    move-result v18

    .line 54
    .local v18, "u8len":I
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v11

    .line 55
    .local v11, "start":I
    move/from16 v5, v18

    .line 56
    .local v5, "blength":I
    :goto_2
    add-int v19, v11, v5

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v19

    if-eqz v19, :cond_2

    .line 57
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 59
    :cond_2
    new-instance v10, Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v19

    const-string v20, "UTF-8"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v10, v0, v11, v5, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 64
    .end local v5    # "blength":I
    .end local v11    # "start":I
    .end local v18    # "u8len":I
    .local v10, "s":Ljava/lang/String;
    :goto_3
    aput-object v10, v14, v7

    .line 47
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 61
    .end local v10    # "s":Ljava/lang/String;
    :cond_3
    invoke-static/range {p0 .. p0}, Lpxb/android/StringItems;->u16length(Ljava/nio/ByteBuffer;)I

    move-result v8

    .line 62
    .local v8, "length":I
    new-instance v10, Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v20

    mul-int/lit8 v21, v8, 0x2

    const-string v22, "UTF-16LE"

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move-object/from16 v3, v22

    invoke-direct {v10, v0, v1, v2, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .restart local v10    # "s":Ljava/lang/String;
    goto :goto_3

    .line 66
    .end local v8    # "length":I
    .end local v10    # "s":Ljava/lang/String;
    :cond_4
    return-object v14
.end method

.method static u16length(Ljava/nio/ByteBuffer;)I
    .locals 4
    .param p0, "in"    # Ljava/nio/ByteBuffer;

    .prologue
    const v3, 0xffff

    .line 70
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    and-int v0, v1, v3

    .line 71
    .local v0, "length":I
    const/16 v1, 0x7fff

    if-le v0, v1, :cond_0

    .line 72
    and-int/lit16 v1, v0, 0x7fff

    shl-int/lit8 v1, v1, 0x8

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v2

    and-int/2addr v2, v3

    or-int v0, v1, v2

    .line 74
    :cond_0
    return v0
.end method

.method static u8length(Ljava/nio/ByteBuffer;)I
    .locals 3
    .param p0, "in"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 78
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    and-int/lit16 v0, v1, 0xff

    .line 79
    .local v0, "len":I
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_0

    .line 80
    and-int/lit8 v1, v0, 0x7f

    shl-int/lit8 v1, v1, 0x8

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    or-int v0, v1, v2

    .line 82
    :cond_0
    return v0
.end method


# virtual methods
.method public getSize()I
    .locals 2

    .prologue
    .line 88
    invoke-virtual {p0}, Lpxb/android/StringItems;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x14

    iget-object v1, p0, Lpxb/android/StringItems;->stringData:[B

    array-length v1, v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    return v0
.end method

.method public prepare()V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-virtual/range {p0 .. p0}, Lpxb/android/StringItems;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lpxb/android/StringItem;

    .line 93
    .local v11, "s":Lpxb/android/StringItem;
    iget-object v15, v11, Lpxb/android/StringItem;->data:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    const/16 v16, 0x7fff

    move/from16 v0, v16

    if-le v15, v0, :cond_0

    .line 94
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lpxb/android/StringItems;->useUTF8:Z

    goto :goto_0

    .line 97
    .end local v11    # "s":Lpxb/android/StringItem;
    :cond_1
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 98
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    const/4 v3, 0x0

    .line 99
    .local v3, "i":I
    const/4 v10, 0x0

    .line 100
    .local v10, "offset":I
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 101
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 102
    .local v8, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-virtual/range {p0 .. p0}, Lpxb/android/StringItems;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lpxb/android/StringItem;

    .line 103
    .local v6, "item":Lpxb/android/StringItem;
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    iput v3, v6, Lpxb/android/StringItem;->index:I

    .line 104
    iget-object v12, v6, Lpxb/android/StringItem;->data:Ljava/lang/String;

    .line 105
    .local v12, "stringData":Ljava/lang/String;
    invoke-interface {v8, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    .line 106
    .local v9, "of":Ljava/lang/Integer;
    if-eqz v9, :cond_2

    .line 107
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v15

    iput v15, v6, Lpxb/android/StringItem;->dataOffset:I

    :goto_2
    move v3, v4

    .line 147
    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_1

    .line 109
    .end local v3    # "i":I
    .restart local v4    # "i":I
    :cond_2
    iput v10, v6, Lpxb/android/StringItem;->dataOffset:I

    .line 110
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v8, v12, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lpxb/android/StringItems;->useUTF8:Z

    if-eqz v15, :cond_5

    .line 112
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v7

    .line 113
    .local v7, "length":I
    const-string v15, "UTF-8"

    invoke-virtual {v12, v15}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    .line 114
    .local v2, "data":[B
    array-length v13, v2

    .line 116
    .local v13, "u8lenght":I
    const/16 v15, 0x7f

    if-le v7, v15, :cond_3

    .line 117
    add-int/lit8 v10, v10, 0x1

    .line 118
    shr-int/lit8 v15, v7, 0x8

    or-int/lit16 v15, v15, 0x80

    invoke-virtual {v1, v15}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 120
    :cond_3
    invoke-virtual {v1, v7}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 122
    const/16 v15, 0x7f

    if-le v13, v15, :cond_4

    .line 123
    add-int/lit8 v10, v10, 0x1

    .line 124
    shr-int/lit8 v15, v13, 0x8

    or-int/lit16 v15, v15, 0x80

    invoke-virtual {v1, v15}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 126
    :cond_4
    invoke-virtual {v1, v13}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 127
    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 128
    const/4 v15, 0x0

    invoke-virtual {v1, v15}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 129
    add-int/lit8 v15, v13, 0x3

    add-int/2addr v10, v15

    .line 130
    goto :goto_2

    .line 131
    .end local v2    # "data":[B
    .end local v7    # "length":I
    .end local v13    # "u8lenght":I
    :cond_5
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v7

    .line 132
    .restart local v7    # "length":I
    const-string v15, "UTF-16LE"

    invoke-virtual {v12, v15}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    .line 133
    .restart local v2    # "data":[B
    const/16 v15, 0x7fff

    if-le v7, v15, :cond_6

    .line 134
    shr-int/lit8 v15, v7, 0x10

    const v16, 0x8000

    or-int v14, v15, v16

    .line 135
    .local v14, "x":I
    invoke-virtual {v1, v14}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 136
    shr-int/lit8 v15, v14, 0x8

    invoke-virtual {v1, v15}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 137
    add-int/lit8 v10, v10, 0x2

    .line 139
    .end local v14    # "x":I
    :cond_6
    invoke-virtual {v1, v7}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 140
    shr-int/lit8 v15, v7, 0x8

    invoke-virtual {v1, v15}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 141
    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 142
    const/4 v15, 0x0

    invoke-virtual {v1, v15}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 143
    const/4 v15, 0x0

    invoke-virtual {v1, v15}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 144
    array-length v15, v2

    add-int/lit8 v15, v15, 0x4

    add-int/2addr v10, v15

    goto :goto_2

    .line 149
    .end local v2    # "data":[B
    .end local v4    # "i":I
    .end local v6    # "item":Lpxb/android/StringItem;
    .end local v7    # "length":I
    .end local v9    # "of":Ljava/lang/Integer;
    .end local v12    # "stringData":Ljava/lang/String;
    .restart local v3    # "i":I
    :cond_7
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lpxb/android/StringItems;->stringData:[B

    .line 150
    return-void
.end method

.method public write(Ljava/nio/ByteBuffer;)V
    .locals 4
    .param p1, "out"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 155
    invoke-virtual {p0}, Lpxb/android/StringItems;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 156
    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 157
    iget-boolean v2, p0, Lpxb/android/StringItems;->useUTF8:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x100

    :goto_0
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 158
    invoke-virtual {p0}, Lpxb/android/StringItems;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    add-int/lit8 v2, v2, 0x1c

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 159
    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 160
    invoke-virtual {p0}, Lpxb/android/StringItems;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpxb/android/StringItem;

    .line 161
    .local v1, "item":Lpxb/android/StringItem;
    iget v2, v1, Lpxb/android/StringItem;->dataOffset:I

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lpxb/android/StringItem;
    :cond_0
    move v2, v3

    .line 157
    goto :goto_0

    .line 163
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v2, p0, Lpxb/android/StringItems;->stringData:[B

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 165
    return-void
.end method
