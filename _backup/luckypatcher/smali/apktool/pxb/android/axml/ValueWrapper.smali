.class public Lpxb/android/axml/ValueWrapper;
.super Ljava/lang/Object;
.source "ValueWrapper.java"


# static fields
.field public static final CLASS:I = 0x3

.field public static final ID:I = 0x1

.field public static final STYLE:I = 0x2


# instance fields
.field public final raw:Ljava/lang/String;

.field public final ref:I

.field public final type:I


# direct methods
.method private constructor <init>(IILjava/lang/String;)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "ref"    # I
    .param p3, "raw"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput p1, p0, Lpxb/android/axml/ValueWrapper;->type:I

    .line 15
    iput-object p3, p0, Lpxb/android/axml/ValueWrapper;->raw:Ljava/lang/String;

    .line 16
    iput p2, p0, Lpxb/android/axml/ValueWrapper;->ref:I

    .line 17
    return-void
.end method

.method public static wrapClass(ILjava/lang/String;)Lpxb/android/axml/ValueWrapper;
    .locals 2
    .param p0, "ref"    # I
    .param p1, "raw"    # Ljava/lang/String;

    .prologue
    .line 32
    new-instance v0, Lpxb/android/axml/ValueWrapper;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p0, p1}, Lpxb/android/axml/ValueWrapper;-><init>(IILjava/lang/String;)V

    return-object v0
.end method

.method public static wrapId(ILjava/lang/String;)Lpxb/android/axml/ValueWrapper;
    .locals 2
    .param p0, "ref"    # I
    .param p1, "raw"    # Ljava/lang/String;

    .prologue
    .line 24
    new-instance v0, Lpxb/android/axml/ValueWrapper;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0, p1}, Lpxb/android/axml/ValueWrapper;-><init>(IILjava/lang/String;)V

    return-object v0
.end method

.method public static wrapStyle(ILjava/lang/String;)Lpxb/android/axml/ValueWrapper;
    .locals 2
    .param p0, "ref"    # I
    .param p1, "raw"    # Ljava/lang/String;

    .prologue
    .line 28
    new-instance v0, Lpxb/android/axml/ValueWrapper;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0, p1}, Lpxb/android/axml/ValueWrapper;-><init>(IILjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public replaceRaw(Ljava/lang/String;)Lpxb/android/axml/ValueWrapper;
    .locals 3
    .param p1, "raw"    # Ljava/lang/String;

    .prologue
    .line 20
    new-instance v0, Lpxb/android/axml/ValueWrapper;

    iget v1, p0, Lpxb/android/axml/ValueWrapper;->type:I

    iget v2, p0, Lpxb/android/axml/ValueWrapper;->ref:I

    invoke-direct {v0, v1, v2, p1}, Lpxb/android/axml/ValueWrapper;-><init>(IILjava/lang/String;)V

    return-object v0
.end method
