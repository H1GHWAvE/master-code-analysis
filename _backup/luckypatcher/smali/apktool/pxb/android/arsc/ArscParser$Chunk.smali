.class Lpxb/android/arsc/ArscParser$Chunk;
.super Ljava/lang/Object;
.source "ArscParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpxb/android/arsc/ArscParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Chunk"
.end annotation


# instance fields
.field public final headSize:I

.field public final location:I

.field public final size:I

.field final synthetic this$0:Lpxb/android/arsc/ArscParser;

.field public final type:I


# direct methods
.method public constructor <init>(Lpxb/android/arsc/ArscParser;)V
    .locals 4

    .prologue
    const v1, 0xffff

    .line 75
    iput-object p1, p0, Lpxb/android/arsc/ArscParser$Chunk;->this$0:Lpxb/android/arsc/ArscParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    # getter for: Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;
    invoke-static {p1}, Lpxb/android/arsc/ArscParser;->access$000(Lpxb/android/arsc/ArscParser;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iput v0, p0, Lpxb/android/arsc/ArscParser$Chunk;->location:I

    .line 77
    # getter for: Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;
    invoke-static {p1}, Lpxb/android/arsc/ArscParser;->access$000(Lpxb/android/arsc/ArscParser;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    and-int/2addr v0, v1

    iput v0, p0, Lpxb/android/arsc/ArscParser$Chunk;->type:I

    .line 78
    # getter for: Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;
    invoke-static {p1}, Lpxb/android/arsc/ArscParser;->access$000(Lpxb/android/arsc/ArscParser;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    and-int/2addr v0, v1

    iput v0, p0, Lpxb/android/arsc/ArscParser$Chunk;->headSize:I

    .line 79
    # getter for: Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;
    invoke-static {p1}, Lpxb/android/arsc/ArscParser;->access$000(Lpxb/android/arsc/ArscParser;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lpxb/android/arsc/ArscParser$Chunk;->size:I

    .line 80
    const-string v0, "[%08x]type: %04x, headsize: %04x, size:%08x"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lpxb/android/arsc/ArscParser$Chunk;->location:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lpxb/android/arsc/ArscParser$Chunk;->type:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lpxb/android/arsc/ArscParser$Chunk;->headSize:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lpxb/android/arsc/ArscParser$Chunk;->size:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    # invokes: Lpxb/android/arsc/ArscParser;->D(Ljava/lang/String;[Ljava/lang/Object;)V
    invoke-static {v0, v1}, Lpxb/android/arsc/ArscParser;->access$100(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    return-void
.end method
