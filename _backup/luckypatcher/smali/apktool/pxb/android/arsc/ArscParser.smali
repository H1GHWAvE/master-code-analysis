.class public Lpxb/android/arsc/ArscParser;
.super Ljava/lang/Object;
.source "ArscParser.java"

# interfaces
.implements Lpxb/android/ResConst;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpxb/android/arsc/ArscParser$Chunk;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field static final ENGRY_FLAG_PUBLIC:I = 0x2

.field static final ENTRY_FLAG_COMPLEX:S = 0x1s

.field public static final TYPE_STRING:I = 0x3


# instance fields
.field private fileSize:I

.field private in:Ljava/nio/ByteBuffer;

.field private keyNamesX:[Ljava/lang/String;

.field private pkg:Lpxb/android/arsc/Pkg;

.field private pkgs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/arsc/Pkg;",
            ">;"
        }
    .end annotation
.end field

.field private strings:[Ljava/lang/String;

.field private typeNamesX:[Ljava/lang/String;


# direct methods
.method public constructor <init>([B)V
    .locals 2
    .param p1, "b"    # [B

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    const/4 v0, -0x1

    iput v0, p0, Lpxb/android/arsc/ArscParser;->fileSize:I

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/arsc/ArscParser;->pkgs:Ljava/util/List;

    .line 114
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    .line 115
    return-void
.end method

.method private static varargs D(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "fmt"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    .line 90
    return-void
.end method

.method static synthetic access$000(Lpxb/android/arsc/ArscParser;)Ljava/nio/ByteBuffer;
    .locals 1
    .param p0, "x0"    # Lpxb/android/arsc/ArscParser;

    .prologue
    .line 67
    iget-object v0, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 67
    invoke-static {p0, p1}, Lpxb/android/arsc/ArscParser;->D(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private readEntry(Lpxb/android/arsc/Config;Lpxb/android/arsc/ResSpec;)V
    .locals 13
    .param p1, "config"    # Lpxb/android/arsc/Config;
    .param p2, "spec"    # Lpxb/android/arsc/ResSpec;

    .prologue
    .line 196
    const-string v9, "[%08x]read ResTable_entry"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->position()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Lpxb/android/arsc/ArscParser;->D(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    iget-object v9, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v8

    .line 198
    .local v8, "size":I
    const-string v9, "ResTable_entry %d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Lpxb/android/arsc/ArscParser;->D(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    iget-object v9, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v3

    .line 201
    .local v3, "flags":I
    iget-object v9, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    .line 202
    .local v5, "keyStr":I
    iget-object v9, p0, Lpxb/android/arsc/ArscParser;->keyNamesX:[Ljava/lang/String;

    aget-object v9, v9, v5

    invoke-virtual {p2, v9}, Lpxb/android/arsc/ResSpec;->updateName(Ljava/lang/String;)V

    .line 204
    new-instance v7, Lpxb/android/arsc/ResEntry;

    invoke-direct {v7, v3, p2}, Lpxb/android/arsc/ResEntry;-><init>(ILpxb/android/arsc/ResSpec;)V

    .line 206
    .local v7, "resEntry":Lpxb/android/arsc/ResEntry;
    and-int/lit8 v9, v3, 0x1

    if-eqz v9, :cond_1

    .line 208
    iget-object v9, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    .line 209
    .local v6, "parent":I
    iget-object v9, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    .line 210
    .local v1, "count":I
    new-instance v0, Lpxb/android/arsc/BagValue;

    invoke-direct {v0, v6}, Lpxb/android/arsc/BagValue;-><init>(I)V

    .line 211
    .local v0, "bag":Lpxb/android/arsc/BagValue;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v1, :cond_0

    .line 212
    new-instance v2, Ljava/util/AbstractMap$SimpleEntry;

    iget-object v9, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {p0}, Lpxb/android/arsc/ArscParser;->readValue()Ljava/lang/Object;

    move-result-object v10

    invoke-direct {v2, v9, v10}, Ljava/util/AbstractMap$SimpleEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 213
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lpxb/android/arsc/Value;>;"
    iget-object v9, v0, Lpxb/android/arsc/BagValue;->map:Ljava/util/List;

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 215
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lpxb/android/arsc/Value;>;"
    :cond_0
    iput-object v0, v7, Lpxb/android/arsc/ResEntry;->value:Ljava/lang/Object;

    .line 219
    .end local v0    # "bag":Lpxb/android/arsc/BagValue;
    .end local v1    # "count":I
    .end local v4    # "i":I
    .end local v6    # "parent":I
    :goto_1
    iget-object v9, p1, Lpxb/android/arsc/Config;->resources:Ljava/util/Map;

    iget v10, p2, Lpxb/android/arsc/ResSpec;->id:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    return-void

    .line 217
    :cond_1
    invoke-direct {p0}, Lpxb/android/arsc/ArscParser;->readValue()Ljava/lang/Object;

    move-result-object v9

    iput-object v9, v7, Lpxb/android/arsc/ResEntry;->value:Ljava/lang/Object;

    goto :goto_1
.end method

.method private readPackage(Ljava/nio/ByteBuffer;)V
    .locals 28
    .param p1, "in"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 223
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v24

    move/from16 v0, v24

    rem-int/lit16 v14, v0, 0xff

    .line 227
    .local v14, "pid":I
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v24

    move/from16 v0, v24

    add-int/lit16 v12, v0, 0x100

    .line 228
    .local v12, "nextPisition":I
    new-instance v16, Ljava/lang/StringBuilder;

    const/16 v24, 0x20

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 229
    .local v16, "sb":Ljava/lang/StringBuilder;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    const/16 v24, 0x80

    move/from16 v0, v24

    if-ge v9, v0, :cond_0

    .line 230
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v15

    .line 231
    .local v15, "s":I
    if-nez v15, :cond_1

    .line 237
    .end local v15    # "s":I
    :cond_0
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 238
    .local v11, "name":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 241
    new-instance v24, Lpxb/android/arsc/Pkg;

    move-object/from16 v0, v24

    invoke-direct {v0, v14, v11}, Lpxb/android/arsc/Pkg;-><init>(ILjava/lang/String;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lpxb/android/arsc/ArscParser;->pkg:Lpxb/android/arsc/Pkg;

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/arsc/ArscParser;->pkgs:Ljava/util/List;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/arsc/ArscParser;->pkg:Lpxb/android/arsc/Pkg;

    move-object/from16 v25, v0

    invoke-interface/range {v24 .. v25}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v23

    .line 245
    .local v23, "typeStringOff":I
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v22

    .line 246
    .local v22, "typeNameCount":I
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    .line 247
    .local v10, "keyStringOff":I
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v19

    .line 250
    .local v19, "specNameCount":I
    new-instance v3, Lpxb/android/arsc/ArscParser$Chunk;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lpxb/android/arsc/ArscParser$Chunk;-><init>(Lpxb/android/arsc/ArscParser;)V

    .line 251
    .local v3, "chunk":Lpxb/android/arsc/ArscParser$Chunk;
    iget v0, v3, Lpxb/android/arsc/ArscParser$Chunk;->type:I

    move/from16 v24, v0

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_2

    .line 252
    new-instance v24, Ljava/lang/RuntimeException;

    invoke-direct/range {v24 .. v24}, Ljava/lang/RuntimeException;-><init>()V

    throw v24

    .line 234
    .end local v3    # "chunk":Lpxb/android/arsc/ArscParser$Chunk;
    .end local v10    # "keyStringOff":I
    .end local v11    # "name":Ljava/lang/String;
    .end local v19    # "specNameCount":I
    .end local v22    # "typeNameCount":I
    .end local v23    # "typeStringOff":I
    .restart local v15    # "s":I
    :cond_1
    int-to-char v0, v15

    move/from16 v24, v0

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 229
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 254
    .end local v15    # "s":I
    .restart local v3    # "chunk":Lpxb/android/arsc/ArscParser$Chunk;
    .restart local v10    # "keyStringOff":I
    .restart local v11    # "name":Ljava/lang/String;
    .restart local v19    # "specNameCount":I
    .restart local v22    # "typeNameCount":I
    .restart local v23    # "typeStringOff":I
    :cond_2
    invoke-static/range {p1 .. p1}, Lpxb/android/StringItems;->read(Ljava/nio/ByteBuffer;)[Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lpxb/android/arsc/ArscParser;->typeNamesX:[Ljava/lang/String;

    .line 255
    iget v0, v3, Lpxb/android/arsc/ArscParser$Chunk;->location:I

    move/from16 v24, v0

    iget v0, v3, Lpxb/android/arsc/ArscParser$Chunk;->size:I

    move/from16 v25, v0

    add-int v24, v24, v25

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 258
    new-instance v3, Lpxb/android/arsc/ArscParser$Chunk;

    .end local v3    # "chunk":Lpxb/android/arsc/ArscParser$Chunk;
    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lpxb/android/arsc/ArscParser$Chunk;-><init>(Lpxb/android/arsc/ArscParser;)V

    .line 259
    .restart local v3    # "chunk":Lpxb/android/arsc/ArscParser$Chunk;
    iget v0, v3, Lpxb/android/arsc/ArscParser$Chunk;->type:I

    move/from16 v24, v0

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_3

    .line 260
    new-instance v24, Ljava/lang/RuntimeException;

    invoke-direct/range {v24 .. v24}, Ljava/lang/RuntimeException;-><init>()V

    throw v24

    .line 262
    :cond_3
    invoke-static/range {p1 .. p1}, Lpxb/android/StringItems;->read(Ljava/nio/ByteBuffer;)[Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lpxb/android/arsc/ArscParser;->keyNamesX:[Ljava/lang/String;

    .line 268
    iget v0, v3, Lpxb/android/arsc/ArscParser$Chunk;->location:I

    move/from16 v24, v0

    iget v0, v3, Lpxb/android/arsc/ArscParser$Chunk;->size:I

    move/from16 v25, v0

    add-int v24, v24, v25

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 271
    :goto_1
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v24

    if-eqz v24, :cond_4

    .line 272
    new-instance v3, Lpxb/android/arsc/ArscParser$Chunk;

    .end local v3    # "chunk":Lpxb/android/arsc/ArscParser$Chunk;
    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lpxb/android/arsc/ArscParser$Chunk;-><init>(Lpxb/android/arsc/ArscParser;)V

    .line 273
    .restart local v3    # "chunk":Lpxb/android/arsc/ArscParser$Chunk;
    iget v0, v3, Lpxb/android/arsc/ArscParser$Chunk;->type:I

    move/from16 v24, v0

    packed-switch v24, :pswitch_data_0

    .line 331
    :cond_4
    return-void

    .line 275
    :pswitch_0
    const-string v24, "[%08x]read spec"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v27

    add-int/lit8 v27, v27, -0x8

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Lpxb/android/arsc/ArscParser;->D(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v21, v0

    .line 277
    .local v21, "tid":I
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->get()B

    .line 278
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getShort()S

    .line 279
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    .line 281
    .local v7, "entryCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/arsc/ArscParser;->pkg:Lpxb/android/arsc/Pkg;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/arsc/ArscParser;->typeNamesX:[Ljava/lang/String;

    move-object/from16 v25, v0

    add-int/lit8 v26, v21, -0x1

    aget-object v25, v25, v26

    move-object/from16 v0, v24

    move/from16 v1, v21

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2, v7}, Lpxb/android/arsc/Pkg;->getType(ILjava/lang/String;I)Lpxb/android/arsc/Type;

    move-result-object v20

    .line 282
    .local v20, "t":Lpxb/android/arsc/Type;
    const/4 v9, 0x0

    :goto_2
    if-ge v9, v7, :cond_8

    .line 283
    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Lpxb/android/arsc/Type;->getSpec(I)Lpxb/android/arsc/ResSpec;

    move-result-object v24

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, v24

    iput v0, v1, Lpxb/android/arsc/ResSpec;->flags:I

    .line 282
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 288
    .end local v7    # "entryCount":I
    .end local v20    # "t":Lpxb/android/arsc/Type;
    .end local v21    # "tid":I
    :pswitch_1
    const-string v24, "[%08x]read config"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v27

    add-int/lit8 v27, v27, -0x8

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Lpxb/android/arsc/ArscParser;->D(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 289
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v21, v0

    .line 290
    .restart local v21    # "tid":I
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->get()B

    .line 291
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getShort()S

    .line 292
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    .line 293
    .restart local v7    # "entryCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/arsc/ArscParser;->pkg:Lpxb/android/arsc/Pkg;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/arsc/ArscParser;->typeNamesX:[Ljava/lang/String;

    move-object/from16 v25, v0

    add-int/lit8 v26, v21, -0x1

    aget-object v25, v25, v26

    move-object/from16 v0, v24

    move/from16 v1, v21

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2, v7}, Lpxb/android/arsc/Pkg;->getType(ILjava/lang/String;I)Lpxb/android/arsc/Type;

    move-result-object v20

    .line 294
    .restart local v20    # "t":Lpxb/android/arsc/Type;
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    .line 296
    .local v6, "entriesStart":I
    const-string v24, "[%08x]read config id"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Lpxb/android/arsc/ArscParser;->D(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v13

    .line 299
    .local v13, "p":I
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v17

    .line 301
    .local v17, "size":I
    move/from16 v0, v17

    new-array v5, v0, [B

    .line 302
    .local v5, "data":[B
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 303
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 304
    new-instance v4, Lpxb/android/arsc/Config;

    invoke-direct {v4, v5, v7}, Lpxb/android/arsc/Config;-><init>([BI)V

    .line 306
    .local v4, "config":Lpxb/android/arsc/Config;
    iget v0, v3, Lpxb/android/arsc/ArscParser$Chunk;->location:I

    move/from16 v24, v0

    iget v0, v3, Lpxb/android/arsc/ArscParser$Chunk;->headSize:I

    move/from16 v25, v0

    add-int v24, v24, v25

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 308
    const-string v24, "[%08x]read config entry offset"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Lpxb/android/arsc/ArscParser;->D(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 310
    new-array v8, v7, [I

    .line 311
    .local v8, "entrys":[I
    const/4 v9, 0x0

    :goto_3
    if-ge v9, v7, :cond_5

    .line 312
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v24

    aput v24, v8, v9

    .line 311
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 314
    :cond_5
    const-string v24, "[%08x]read config entrys"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Lpxb/android/arsc/ArscParser;->D(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 315
    const/4 v9, 0x0

    :goto_4
    array-length v0, v8

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v9, v0, :cond_7

    .line 316
    aget v24, v8, v9

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_6

    .line 317
    iget v0, v3, Lpxb/android/arsc/ArscParser$Chunk;->location:I

    move/from16 v24, v0

    add-int v24, v24, v6

    aget v25, v8, v9

    add-int v24, v24, v25

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 318
    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Lpxb/android/arsc/Type;->getSpec(I)Lpxb/android/arsc/ResSpec;

    move-result-object v18

    .line 319
    .local v18, "spec":Lpxb/android/arsc/ResSpec;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v4, v1}, Lpxb/android/arsc/ArscParser;->readEntry(Lpxb/android/arsc/Config;Lpxb/android/arsc/ResSpec;)V

    .line 315
    .end local v18    # "spec":Lpxb/android/arsc/ResSpec;
    :cond_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 323
    :cond_7
    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lpxb/android/arsc/Type;->addConfig(Lpxb/android/arsc/Config;)V

    .line 329
    .end local v4    # "config":Lpxb/android/arsc/Config;
    .end local v5    # "data":[B
    .end local v6    # "entriesStart":I
    .end local v8    # "entrys":[I
    .end local v13    # "p":I
    .end local v17    # "size":I
    :cond_8
    iget v0, v3, Lpxb/android/arsc/ArscParser$Chunk;->location:I

    move/from16 v24, v0

    iget v0, v3, Lpxb/android/arsc/ArscParser$Chunk;->size:I

    move/from16 v25, v0

    add-int v24, v24, v25

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/16 :goto_1

    .line 273
    nop

    :pswitch_data_0
    .packed-switch 0x201
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private readValue()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 334
    iget-object v5, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v2

    .line 335
    .local v2, "size1":I
    iget-object v5, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    .line 336
    .local v4, "zero":I
    iget-object v5, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->get()B

    move-result v5

    and-int/lit16 v3, v5, 0xff

    .line 337
    .local v3, "type":I
    iget-object v5, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    .line 338
    .local v0, "data":I
    const/4 v1, 0x0

    .line 339
    .local v1, "raw":Ljava/lang/String;
    const/4 v5, 0x3

    if-ne v3, v5, :cond_0

    .line 340
    iget-object v5, p0, Lpxb/android/arsc/ArscParser;->strings:[Ljava/lang/String;

    aget-object v1, v5, v0

    .line 342
    :cond_0
    new-instance v5, Lpxb/android/arsc/Value;

    invoke-direct {v5, v3, v0, v1}, Lpxb/android/arsc/Value;-><init>(IILjava/lang/String;)V

    return-object v5
.end method


# virtual methods
.method public parse()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lpxb/android/arsc/Pkg;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    iget v2, p0, Lpxb/android/arsc/ArscParser;->fileSize:I

    if-gez v2, :cond_1

    .line 119
    new-instance v1, Lpxb/android/arsc/ArscParser$Chunk;

    invoke-direct {v1, p0}, Lpxb/android/arsc/ArscParser$Chunk;-><init>(Lpxb/android/arsc/ArscParser;)V

    .line 120
    .local v1, "head":Lpxb/android/arsc/ArscParser$Chunk;
    iget v2, v1, Lpxb/android/arsc/ArscParser$Chunk;->type:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 121
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2}, Ljava/lang/RuntimeException;-><init>()V

    throw v2

    .line 123
    :cond_0
    iget v2, v1, Lpxb/android/arsc/ArscParser$Chunk;->size:I

    iput v2, p0, Lpxb/android/arsc/ArscParser;->fileSize:I

    .line 124
    iget-object v2, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    .line 126
    .end local v1    # "head":Lpxb/android/arsc/ArscParser$Chunk;
    :cond_1
    :goto_0
    iget-object v2, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 127
    new-instance v0, Lpxb/android/arsc/ArscParser$Chunk;

    invoke-direct {v0, p0}, Lpxb/android/arsc/ArscParser$Chunk;-><init>(Lpxb/android/arsc/ArscParser;)V

    .line 128
    .local v0, "chunk":Lpxb/android/arsc/ArscParser$Chunk;
    iget v2, v0, Lpxb/android/arsc/ArscParser$Chunk;->type:I

    sparse-switch v2, :sswitch_data_0

    .line 140
    :goto_1
    iget-object v2, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    iget v3, v0, Lpxb/android/arsc/ArscParser$Chunk;->location:I

    iget v4, v0, Lpxb/android/arsc/ArscParser$Chunk;->size:I

    add-int/2addr v3, v4

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_0

    .line 130
    :sswitch_0
    iget-object v2, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-static {v2}, Lpxb/android/StringItems;->read(Ljava/nio/ByteBuffer;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lpxb/android/arsc/ArscParser;->strings:[Ljava/lang/String;

    goto :goto_1

    .line 138
    :sswitch_1
    iget-object v2, p0, Lpxb/android/arsc/ArscParser;->in:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v2}, Lpxb/android/arsc/ArscParser;->readPackage(Ljava/nio/ByteBuffer;)V

    goto :goto_1

    .line 142
    .end local v0    # "chunk":Lpxb/android/arsc/ArscParser$Chunk;
    :cond_2
    iget-object v2, p0, Lpxb/android/arsc/ArscParser;->pkgs:Ljava/util/List;

    return-object v2

    .line 128
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x200 -> :sswitch_1
    .end sparse-switch
.end method
