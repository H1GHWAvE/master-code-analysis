.class Lpxb/android/arsc/ArscWriter$PkgCtx;
.super Ljava/lang/Object;
.source "ArscWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpxb/android/arsc/ArscWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PkgCtx"
.end annotation


# instance fields
.field keyNames:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lpxb/android/StringItem;",
            ">;"
        }
    .end annotation
.end field

.field keyNames0:Lpxb/android/StringItems;

.field public keyStringOff:I

.field offset:I

.field pkg:Lpxb/android/arsc/Pkg;

.field pkgSize:I

.field typeNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/StringItem;",
            ">;"
        }
    .end annotation
.end field

.field typeNames0:Lpxb/android/StringItems;

.field typeStringOff:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lpxb/android/arsc/ArscWriter$PkgCtx;->keyNames:Ljava/util/Map;

    .line 43
    new-instance v0, Lpxb/android/StringItems;

    invoke-direct {v0}, Lpxb/android/StringItems;-><init>()V

    iput-object v0, p0, Lpxb/android/arsc/ArscWriter$PkgCtx;->keyNames0:Lpxb/android/StringItems;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeNames:Ljava/util/List;

    .line 50
    new-instance v0, Lpxb/android/StringItems;

    invoke-direct {v0}, Lpxb/android/StringItems;-><init>()V

    iput-object v0, p0, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeNames0:Lpxb/android/StringItems;

    return-void
.end method

.method synthetic constructor <init>(Lpxb/android/arsc/ArscWriter$1;)V
    .locals 0
    .param p1, "x0"    # Lpxb/android/arsc/ArscWriter$1;

    .prologue
    .line 41
    invoke-direct {p0}, Lpxb/android/arsc/ArscWriter$PkgCtx;-><init>()V

    return-void
.end method


# virtual methods
.method public addKeyName(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 54
    iget-object v1, p0, Lpxb/android/arsc/ArscWriter$PkgCtx;->keyNames:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    :goto_0
    return-void

    .line 57
    :cond_0
    new-instance v0, Lpxb/android/StringItem;

    invoke-direct {v0, p1}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    .line 58
    .local v0, "stringItem":Lpxb/android/StringItem;
    iget-object v1, p0, Lpxb/android/arsc/ArscWriter$PkgCtx;->keyNames:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    iget-object v1, p0, Lpxb/android/arsc/ArscWriter$PkgCtx;->keyNames0:Lpxb/android/StringItems;

    invoke-virtual {v1, v0}, Lpxb/android/StringItems;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addTypeName(ILjava/lang/String;)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    :goto_0
    iget-object v1, p0, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeNames:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, p1, :cond_0

    .line 64
    iget-object v1, p0, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeNames:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 67
    :cond_0
    iget-object v1, p0, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeNames:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpxb/android/StringItem;

    .line 68
    .local v0, "item":Lpxb/android/StringItem;
    if-nez v0, :cond_1

    .line 69
    iget-object v1, p0, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeNames:Ljava/util/List;

    new-instance v2, Lpxb/android/StringItem;

    invoke-direct {v2, p2}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, p1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 73
    return-void

    .line 71
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    throw v1
.end method
