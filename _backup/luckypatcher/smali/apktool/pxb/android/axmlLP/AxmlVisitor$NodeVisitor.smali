.class public abstract Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
.super Ljava/lang/Object;
.source "AxmlVisitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpxb/android/axmlLP/AxmlVisitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "NodeVisitor"
.end annotation


# instance fields
.field protected nv:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public constructor <init>(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;)V
    .locals 0
    .param p1, "nv"    # Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->nv:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    .line 36
    return-void
.end method


# virtual methods
.method public attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
    .locals 6
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "resourceId"    # I
    .param p4, "type"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    .line 50
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->nv:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->nv:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V

    .line 53
    :cond_0
    return-void
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    .locals 1
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->nv:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->nv:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    invoke-virtual {v0, p1, p2}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public end()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->nv:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->nv:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    invoke-virtual {v0}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->end()V

    .line 76
    :cond_0
    return-void
.end method

.method public line(I)V
    .locals 1
    .param p1, "ln"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->nv:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->nv:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    invoke-virtual {v0, p1}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->line(I)V

    .line 87
    :cond_0
    return-void
.end method

.method public text(ILjava/lang/String;)V
    .locals 1
    .param p1, "lineNumber"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 95
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->nv:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->nv:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    invoke-virtual {v0, p1, p2}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->text(ILjava/lang/String;)V

    .line 98
    :cond_0
    return-void
.end method
