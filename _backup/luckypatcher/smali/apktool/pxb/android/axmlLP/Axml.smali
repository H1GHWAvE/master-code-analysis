.class public Lpxb/android/axmlLP/Axml;
.super Lpxb/android/axmlLP/AxmlVisitor;
.source "Axml.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpxb/android/axmlLP/Axml$Ns;,
        Lpxb/android/axmlLP/Axml$Node;
    }
.end annotation


# instance fields
.field public firsts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/axmlLP/Axml$Node;",
            ">;"
        }
    .end annotation
.end field

.field public nses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/axmlLP/Axml$Ns;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lpxb/android/axmlLP/AxmlVisitor;-><init>()V

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/Axml;->firsts:Ljava/util/List;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/Axml;->nses:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public accept(Lpxb/android/axmlLP/AxmlVisitor;)V
    .locals 5
    .param p1, "visitor"    # Lpxb/android/axmlLP/AxmlVisitor;

    .prologue
    .line 117
    iget-object v2, p0, Lpxb/android/axmlLP/Axml;->nses:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpxb/android/axmlLP/Axml$Ns;

    .line 118
    .local v1, "ns":Lpxb/android/axmlLP/Axml$Ns;
    invoke-virtual {v1, p1}, Lpxb/android/axmlLP/Axml$Ns;->accept(Lpxb/android/axmlLP/AxmlVisitor;)V

    goto :goto_0

    .line 120
    .end local v1    # "ns":Lpxb/android/axmlLP/Axml$Ns;
    :cond_0
    iget-object v2, p0, Lpxb/android/axmlLP/Axml;->firsts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpxb/android/axmlLP/Axml$Node;

    .line 121
    .local v0, "first":Lpxb/android/axmlLP/Axml$Node;
    new-instance v3, Lpxb/android/axmlLP/Axml$1;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4, p1}, Lpxb/android/axmlLP/Axml$1;-><init>(Lpxb/android/axmlLP/Axml;Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;Lpxb/android/axmlLP/AxmlVisitor;)V

    invoke-virtual {v0, v3}, Lpxb/android/axmlLP/Axml$Node;->accept(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;)V

    goto :goto_1

    .line 129
    .end local v0    # "first":Lpxb/android/axmlLP/Axml$Node;
    :cond_1
    return-void
.end method

.method public first(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    .locals 2
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 133
    new-instance v0, Lpxb/android/axmlLP/Axml$Node;

    invoke-direct {v0}, Lpxb/android/axmlLP/Axml$Node;-><init>()V

    .line 134
    .local v0, "node":Lpxb/android/axmlLP/Axml$Node;
    iput-object p2, v0, Lpxb/android/axmlLP/Axml$Node;->name:Ljava/lang/String;

    .line 135
    iput-object p1, v0, Lpxb/android/axmlLP/Axml$Node;->ns:Ljava/lang/String;

    .line 136
    iget-object v1, p0, Lpxb/android/axmlLP/Axml;->firsts:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    return-object v0
.end method

.method public ns(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "ln"    # I

    .prologue
    .line 142
    new-instance v0, Lpxb/android/axmlLP/Axml$Ns;

    invoke-direct {v0}, Lpxb/android/axmlLP/Axml$Ns;-><init>()V

    .line 143
    .local v0, "ns":Lpxb/android/axmlLP/Axml$Ns;
    iput-object p1, v0, Lpxb/android/axmlLP/Axml$Ns;->prefix:Ljava/lang/String;

    .line 144
    iput-object p2, v0, Lpxb/android/axmlLP/Axml$Ns;->uri:Ljava/lang/String;

    .line 145
    iput p3, v0, Lpxb/android/axmlLP/Axml$Ns;->ln:I

    .line 146
    iget-object v1, p0, Lpxb/android/axmlLP/Axml;->nses:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    return-void
.end method
