.class public Lpxb/android/axmlLP/AxmlVisitor;
.super Ljava/lang/Object;
.source "AxmlVisitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    }
.end annotation


# static fields
.field public static final TYPE_FIRST_INT:I = 0x10

.field public static final TYPE_INT_BOOLEAN:I = 0x12

.field public static final TYPE_INT_HEX:I = 0x11

.field public static final TYPE_REFERENCE:I = 0x1

.field public static final TYPE_STRING:I = 0x3


# instance fields
.field protected av:Lpxb/android/axmlLP/AxmlVisitor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    return-void
.end method

.method public constructor <init>(Lpxb/android/axmlLP/AxmlVisitor;)V
    .locals 0
    .param p1, "av"    # Lpxb/android/axmlLP/AxmlVisitor;

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lpxb/android/axmlLP/AxmlVisitor;->av:Lpxb/android/axmlLP/AxmlVisitor;

    .line 117
    return-void
.end method


# virtual methods
.method public end()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor;->av:Lpxb/android/axmlLP/AxmlVisitor;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor;->av:Lpxb/android/axmlLP/AxmlVisitor;

    invoke-virtual {v0}, Lpxb/android/axmlLP/AxmlVisitor;->end()V

    .line 126
    :cond_0
    return-void
.end method

.method public first(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    .locals 1
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 136
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor;->av:Lpxb/android/axmlLP/AxmlVisitor;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor;->av:Lpxb/android/axmlLP/AxmlVisitor;

    invoke-virtual {v0, p1, p2}, Lpxb/android/axmlLP/AxmlVisitor;->first(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    move-result-object v0

    .line 139
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ns(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "ln"    # I

    .prologue
    .line 150
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor;->av:Lpxb/android/axmlLP/AxmlVisitor;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlVisitor;->av:Lpxb/android/axmlLP/AxmlVisitor;

    invoke-virtual {v0, p1, p2, p3}, Lpxb/android/axmlLP/AxmlVisitor;->ns(Ljava/lang/String;Ljava/lang/String;I)V

    .line 153
    :cond_0
    return-void
.end method
