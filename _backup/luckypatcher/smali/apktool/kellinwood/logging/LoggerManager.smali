.class public Lkellinwood/logging/LoggerManager;
.super Ljava/lang/Object;
.source "LoggerManager.java"


# static fields
.field static factory:Lkellinwood/logging/LoggerFactory;

.field static loggers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/logging/LoggerInterface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lkellinwood/logging/NullLoggerFactory;

    invoke-direct {v0}, Lkellinwood/logging/NullLoggerFactory;-><init>()V

    sput-object v0, Lkellinwood/logging/LoggerManager;->factory:Lkellinwood/logging/LoggerFactory;

    .line 25
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    sput-object v0, Lkellinwood/logging/LoggerManager;->loggers:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
    .locals 2
    .param p0, "category"    # Ljava/lang/String;

    .prologue
    .line 33
    sget-object v1, Lkellinwood/logging/LoggerManager;->loggers:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkellinwood/logging/LoggerInterface;

    .line 34
    .local v0, "logger":Lkellinwood/logging/LoggerInterface;
    if-nez v0, :cond_0

    .line 35
    sget-object v1, Lkellinwood/logging/LoggerManager;->factory:Lkellinwood/logging/LoggerFactory;

    invoke-interface {v1, p0}, Lkellinwood/logging/LoggerFactory;->getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    .line 36
    sget-object v1, Lkellinwood/logging/LoggerManager;->loggers:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    :cond_0
    return-object v0
.end method

.method public static setLoggerFactory(Lkellinwood/logging/LoggerFactory;)V
    .locals 0
    .param p0, "f"    # Lkellinwood/logging/LoggerFactory;

    .prologue
    .line 28
    sput-object p0, Lkellinwood/logging/LoggerManager;->factory:Lkellinwood/logging/LoggerFactory;

    .line 29
    return-void
.end method
