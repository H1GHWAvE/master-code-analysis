.class public Lkellinwood/logging/ConsoleLoggerFactory;
.super Ljava/lang/Object;
.source "ConsoleLoggerFactory.java"

# interfaces
.implements Lkellinwood/logging/LoggerFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
    .locals 2
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 21
    new-instance v0, Lkellinwood/logging/StreamLogger;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-direct {v0, p1, v1}, Lkellinwood/logging/StreamLogger;-><init>(Ljava/lang/String;Ljava/io/PrintStream;)V

    return-object v0
.end method
