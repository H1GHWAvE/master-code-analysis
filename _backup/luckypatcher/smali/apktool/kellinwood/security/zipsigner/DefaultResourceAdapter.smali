.class public Lkellinwood/security/zipsigner/DefaultResourceAdapter;
.super Ljava/lang/Object;
.source "DefaultResourceAdapter.java"

# interfaces
.implements Lkellinwood/security/zipsigner/ResourceAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkellinwood/security/zipsigner/DefaultResourceAdapter$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method


# virtual methods
.method public varargs getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 5
    .param p1, "item"    # Lkellinwood/security/zipsigner/ResourceAdapter$Item;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 11
    sget-object v0, Lkellinwood/security/zipsigner/DefaultResourceAdapter$1;->$SwitchMap$kellinwood$security$zipsigner$ResourceAdapter$Item:[I

    invoke-virtual {p1}, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 29
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown item "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 13
    :pswitch_0
    const-string v0, "Input and output files are the same.  Specify a different name for the output."

    .line 27
    :goto_0
    return-object v0

    .line 15
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to auto-select key for signing "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, p2, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 17
    :pswitch_2
    const-string v0, "Loading certificate and private key"

    goto :goto_0

    .line 19
    :pswitch_3
    const-string v0, "Parsing the input\'s central directory"

    goto :goto_0

    .line 21
    :pswitch_4
    const-string v0, "Generating manifest"

    goto :goto_0

    .line 23
    :pswitch_5
    const-string v0, "Generating signature file"

    goto :goto_0

    .line 25
    :pswitch_6
    const-string v0, "Generating signature block file"

    goto :goto_0

    .line 27
    :pswitch_7
    const-string v0, "Copying zip entry %d of %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aget-object v2, p2, v3

    aput-object v2, v1, v3

    aget-object v2, p2, v4

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 11
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
