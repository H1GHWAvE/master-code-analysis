.class public Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;
.super Ljava/lang/Object;
.source "KeyStoreFileManager.java"


# static fields
.field static logger:Lkellinwood/logging/LoggerInterface;

.field static provider:Ljava/security/Provider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lorg/spongycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v0}, Lorg/spongycastle/jce/provider/BouncyCastleProvider;-><init>()V

    sput-object v0, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->provider:Ljava/security/Provider;

    .line 28
    const-class v0, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkellinwood/logging/LoggerManager;->getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    sput-object v0, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->logger:Lkellinwood/logging/LoggerInterface;

    .line 33
    invoke-static {}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->getProvider()Ljava/security/Provider;

    move-result-object v0

    invoke-static {v0}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    .line 34
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static containsKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "keystorePath"    # Ljava/lang/String;
    .param p1, "storePass"    # Ljava/lang/String;
    .param p2, "keyName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 236
    invoke-static {p0, p1}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->loadKeyStore(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v0

    .line 237
    .local v0, "ks":Ljava/security/KeyStore;
    invoke-virtual {v0, p2}, Ljava/security/KeyStore;->containsAlias(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method static copyFile(Ljava/io/File;Ljava/io/File;Z)V
    .locals 10
    .param p0, "srcFile"    # Ljava/io/File;
    .param p1, "destFile"    # Ljava/io/File;
    .param p2, "preserveFileDate"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 143
    new-instance v6, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Destination \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\' exists but is a directory"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 146
    :cond_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 148
    .local v3, "input":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 150
    .local v5, "output":Ljava/io/FileOutputStream;
    const/16 v6, 0x1000

    :try_start_1
    new-array v0, v6, [B

    .line 151
    .local v0, "buffer":[B
    const-wide/16 v1, 0x0

    .line 152
    .local v1, "count":J
    const/4 v4, 0x0

    .line 153
    .local v4, "n":I
    :goto_0
    const/4 v6, -0x1

    invoke-virtual {v3, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    if-eq v6, v4, :cond_1

    .line 154
    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155
    int-to-long v6, v4

    add-long/2addr v1, v6

    goto :goto_0

    .line 158
    :cond_1
    :try_start_2
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 161
    :goto_1
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 164
    :goto_2
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-eqz v6, :cond_2

    .line 165
    new-instance v6, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to copy full contents from \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\' to \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 158
    .end local v0    # "buffer":[B
    .end local v1    # "count":J
    .end local v4    # "n":I
    :catchall_0
    move-exception v6

    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :goto_3
    :try_start_5
    throw v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 161
    .end local v5    # "output":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v6

    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :goto_4
    throw v6

    .line 168
    .restart local v0    # "buffer":[B
    .restart local v1    # "count":J
    .restart local v4    # "n":I
    .restart local v5    # "output":Ljava/io/FileOutputStream;
    :cond_2
    if-eqz p2, :cond_3

    .line 169
    invoke-virtual {p0}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    invoke-virtual {p1, v6, v7}, Ljava/io/File;->setLastModified(J)Z

    .line 171
    :cond_3
    return-void

    .line 158
    :catch_0
    move-exception v6

    goto :goto_1

    .end local v0    # "buffer":[B
    .end local v1    # "count":J
    .end local v4    # "n":I
    :catch_1
    move-exception v7

    goto :goto_3

    .line 161
    .restart local v0    # "buffer":[B
    .restart local v1    # "count":J
    .restart local v4    # "n":I
    :catch_2
    move-exception v6

    goto :goto_2

    .end local v0    # "buffer":[B
    .end local v1    # "count":J
    .end local v4    # "n":I
    .end local v5    # "output":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v7

    goto :goto_4
.end method

.method public static createKeyStore(Ljava/lang/String;[C)Ljava/security/KeyStore;
    .locals 3
    .param p0, "keystorePath"    # Ljava/lang/String;
    .param p1, "password"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 54
    .local v0, "ks":Ljava/security/KeyStore;
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".bks"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    const-string v1, "bks"

    new-instance v2, Lorg/spongycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v2}, Lorg/spongycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static {v1, v2}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyStore;

    move-result-object v0

    .line 58
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 60
    return-object v0

    .line 57
    :cond_0
    new-instance v0, Lkellinwood/security/zipsigner/optional/JksKeyStore;

    .end local v0    # "ks":Ljava/security/KeyStore;
    invoke-direct {v0}, Lkellinwood/security/zipsigner/optional/JksKeyStore;-><init>()V

    .restart local v0    # "ks":Ljava/security/KeyStore;
    goto :goto_0
.end method

.method public static deleteKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "storePath"    # Ljava/lang/String;
    .param p1, "storePass"    # Ljava/lang/String;
    .param p2, "keyName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 184
    invoke-static {p0, p1}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->loadKeyStore(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v0

    .line 185
    .local v0, "ks":Ljava/security/KeyStore;
    invoke-virtual {v0, p2}, Ljava/security/KeyStore;->deleteEntry(Ljava/lang/String;)V

    .line 186
    invoke-static {v0, p0, p1}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->writeKeyStore(Ljava/security/KeyStore;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    return-void
.end method

.method public static getKeyEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore$Entry;
    .locals 5
    .param p0, "keystorePath"    # Ljava/lang/String;
    .param p1, "storePass"    # Ljava/lang/String;
    .param p2, "keyName"    # Ljava/lang/String;
    .param p3, "keyPass"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 218
    const/4 v0, 0x0

    .line 219
    .local v0, "keyPw":[C
    const/4 v2, 0x0

    .line 222
    .local v2, "passwordProtection":Ljava/security/KeyStore$PasswordProtection;
    :try_start_0
    invoke-static {p0, p1}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->loadKeyStore(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1

    .line 223
    .local v1, "ks":Ljava/security/KeyStore;
    invoke-static {}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->getInstance()Lkellinwood/security/zipsigner/optional/PasswordObfuscator;

    move-result-object v4

    invoke-virtual {v4, p0, p2, p3}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->decodeAliasPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[C

    move-result-object v0

    .line 224
    new-instance v3, Ljava/security/KeyStore$PasswordProtection;

    invoke-direct {v3, v0}, Ljava/security/KeyStore$PasswordProtection;-><init>([C)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    .end local v2    # "passwordProtection":Ljava/security/KeyStore$PasswordProtection;
    .local v3, "passwordProtection":Ljava/security/KeyStore$PasswordProtection;
    :try_start_1
    invoke-virtual {v1, p2, v3}, Ljava/security/KeyStore;->getEntry(Ljava/lang/String;Ljava/security/KeyStore$ProtectionParameter;)Ljava/security/KeyStore$Entry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 228
    if-eqz v0, :cond_0

    invoke-static {v0}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->flush([C)V

    .line 229
    :cond_0
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/security/KeyStore$PasswordProtection;->destroy()V

    :cond_1
    return-object v4

    .line 228
    .end local v1    # "ks":Ljava/security/KeyStore;
    .end local v3    # "passwordProtection":Ljava/security/KeyStore$PasswordProtection;
    .restart local v2    # "passwordProtection":Ljava/security/KeyStore$PasswordProtection;
    :catchall_0
    move-exception v4

    :goto_0
    if-eqz v0, :cond_2

    invoke-static {v0}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->flush([C)V

    .line 229
    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/security/KeyStore$PasswordProtection;->destroy()V

    :cond_3
    throw v4

    .line 228
    .end local v2    # "passwordProtection":Ljava/security/KeyStore$PasswordProtection;
    .restart local v1    # "ks":Ljava/security/KeyStore;
    .restart local v3    # "passwordProtection":Ljava/security/KeyStore$PasswordProtection;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "passwordProtection":Ljava/security/KeyStore$PasswordProtection;
    .restart local v2    # "passwordProtection":Ljava/security/KeyStore$PasswordProtection;
    goto :goto_0
.end method

.method public static getProvider()Ljava/security/Provider;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->provider:Ljava/security/Provider;

    return-object v0
.end method

.method public static loadKeyStore(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;
    .locals 2
    .param p0, "keystorePath"    # Ljava/lang/String;
    .param p1, "encodedPassword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 39
    const/4 v0, 0x0

    .line 41
    .local v0, "password":[C
    if-eqz p1, :cond_0

    .line 42
    :try_start_0
    invoke-static {}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->getInstance()Lkellinwood/security/zipsigner/optional/PasswordObfuscator;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->decodeKeystorePassword(Ljava/lang/String;Ljava/lang/String;)[C

    move-result-object v0

    .line 44
    :cond_0
    invoke-static {p0, v0}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->loadKeyStore(Ljava/lang/String;[C)Ljava/security/KeyStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 46
    if-eqz v0, :cond_1

    invoke-static {v0}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->flush([C)V

    :cond_1
    return-object v1

    :catchall_0
    move-exception v1

    if-eqz v0, :cond_2

    invoke-static {v0}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->flush([C)V

    :cond_2
    throw v1
.end method

.method public static loadKeyStore(Ljava/lang/String;[C)Ljava/security/KeyStore;
    .locals 8
    .param p0, "keystorePath"    # Ljava/lang/String;
    .param p1, "password"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    const/4 v2, 0x0

    .line 68
    .local v2, "ks":Ljava/security/KeyStore;
    :try_start_0
    new-instance v3, Lkellinwood/security/zipsigner/optional/JksKeyStore;

    invoke-direct {v3}, Lkellinwood/security/zipsigner/optional/JksKeyStore;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    .end local v2    # "ks":Ljava/security/KeyStore;
    .local v3, "ks":Ljava/security/KeyStore;
    :try_start_1
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 70
    .local v1, "fis":Ljava/io/FileInputStream;
    invoke-virtual {v3, v1, p1}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 71
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-object v2, v3

    .line 80
    .end local v3    # "ks":Ljava/security/KeyStore;
    :goto_0
    return-object v3

    .line 73
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "ks":Ljava/security/KeyStore;
    :catch_0
    move-exception v4

    .line 76
    .local v4, "x":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    const-string v5, "bks"

    invoke-static {}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->getProvider()Ljava/security/Provider;

    move-result-object v6

    invoke-static {v5, v6}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyStore;

    move-result-object v2

    .line 77
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 78
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    invoke-virtual {v2, v1, p1}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 79
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-object v3, v2

    .line 80
    .local v3, "ks":Ljava/lang/Object;
    goto :goto_0

    .line 81
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v3    # "ks":Ljava/lang/Object;
    :catch_1
    move-exception v0

    .line 82
    .local v0, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to load keystore: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 73
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "ks":Ljava/security/KeyStore;
    .end local v4    # "x":Ljava/lang/Exception;
    .local v3, "ks":Ljava/security/KeyStore;
    :catch_2
    move-exception v4

    move-object v2, v3

    .end local v3    # "ks":Ljava/security/KeyStore;
    .restart local v2    # "ks":Ljava/security/KeyStore;
    goto :goto_1
.end method

.method public static renameKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "keystorePath"    # Ljava/lang/String;
    .param p1, "storePass"    # Ljava/lang/String;
    .param p2, "oldKeyName"    # Ljava/lang/String;
    .param p3, "newKeyName"    # Ljava/lang/String;
    .param p4, "keyPass"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 192
    const/4 v2, 0x0

    .line 195
    .local v2, "keyPw":[C
    :try_start_0
    invoke-static {p0, p1}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->loadKeyStore(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v3

    .line 196
    .local v3, "ks":Ljava/security/KeyStore;
    instance-of v4, v3, Lkellinwood/security/zipsigner/optional/JksKeyStore;

    if-eqz v4, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p3

    .line 198
    :cond_0
    invoke-virtual {v3, p3}, Ljava/security/KeyStore;->containsAlias(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v4, Lkellinwood/security/zipsigner/optional/KeyNameConflictException;

    invoke-direct {v4}, Lkellinwood/security/zipsigner/optional/KeyNameConflictException;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    .end local v3    # "ks":Ljava/security/KeyStore;
    :catchall_0
    move-exception v4

    invoke-static {v2}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->flush([C)V

    throw v4

    .line 200
    .restart local v3    # "ks":Ljava/security/KeyStore;
    :cond_1
    :try_start_1
    invoke-static {}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->getInstance()Lkellinwood/security/zipsigner/optional/PasswordObfuscator;

    move-result-object v4

    invoke-virtual {v4, p0, p2, p4}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->decodeAliasPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[C

    move-result-object v2

    .line 201
    invoke-virtual {v3, p2, v2}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object v1

    .line 202
    .local v1, "key":Ljava/security/Key;
    invoke-virtual {v3, p2}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v0

    .line 204
    .local v0, "cert":Ljava/security/cert/Certificate;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/security/cert/Certificate;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v3, p3, v1, v2, v4}, Ljava/security/KeyStore;->setKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V

    .line 205
    invoke-virtual {v3, p2}, Ljava/security/KeyStore;->deleteEntry(Ljava/lang/String;)V

    .line 207
    invoke-static {v3, p0, p1}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->writeKeyStore(Ljava/security/KeyStore;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    invoke-static {v2}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->flush([C)V

    return-object p3
.end method

.method public static renameTo(Ljava/io/File;Ljava/io/File;)V
    .locals 3
    .param p0, "fromFile"    # Ljava/io/File;
    .param p1, "toFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->copyFile(Ljava/io/File;Ljava/io/File;Z)V

    .line 178
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to delete "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_0
    return-void
.end method

.method public static setProvider(Ljava/security/Provider;)V
    .locals 1
    .param p0, "provider"    # Ljava/security/Provider;

    .prologue
    .line 23
    sget-object v0, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->provider:Ljava/security/Provider;

    if-eqz v0, :cond_0

    sget-object v0, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->provider:Ljava/security/Provider;

    invoke-virtual {v0}, Ljava/security/Provider;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/security/Security;->removeProvider(Ljava/lang/String;)V

    .line 24
    :cond_0
    sput-object p0, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->provider:Ljava/security/Provider;

    .line 25
    invoke-static {p0}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    .line 26
    return-void
.end method

.method public static validateKeyPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "keystorePath"    # Ljava/lang/String;
    .param p1, "keyName"    # Ljava/lang/String;
    .param p2, "encodedPassword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 269
    const/4 v1, 0x0

    .line 271
    .local v1, "password":[C
    const/4 v2, 0x0

    :try_start_0
    check-cast v2, [C

    invoke-static {p0, v2}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->loadKeyStore(Ljava/lang/String;[C)Ljava/security/KeyStore;

    move-result-object v0

    .line 272
    .local v0, "ks":Ljava/security/KeyStore;
    invoke-static {}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->getInstance()Lkellinwood/security/zipsigner/optional/PasswordObfuscator;

    move-result-object v2

    invoke-virtual {v2, p0, p1, p2}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->decodeAliasPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[C

    move-result-object v1

    .line 273
    invoke-virtual {v0, p1, v1}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    if-eqz v1, :cond_0

    invoke-static {v1}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->flush([C)V

    .line 278
    :cond_0
    return-void

    .line 275
    .end local v0    # "ks":Ljava/security/KeyStore;
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_1

    invoke-static {v1}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->flush([C)V

    :cond_1
    throw v2
.end method

.method public static validateKeystorePassword(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "keystorePath"    # Ljava/lang/String;
    .param p1, "encodedPassword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 250
    const/4 v0, 0x0

    .line 252
    .local v0, "password":[C
    :try_start_0
    invoke-static {p0, p1}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->loadKeyStore(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    if-eqz v0, :cond_0

    invoke-static {v0}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->flush([C)V

    .line 257
    :cond_0
    return-void

    .line 254
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->flush([C)V

    :cond_1
    throw v1
.end method

.method public static writeKeyStore(Ljava/security/KeyStore;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "ks"    # Ljava/security/KeyStore;
    .param p1, "keystorePath"    # Ljava/lang/String;
    .param p2, "encodedPassword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 90
    const/4 v0, 0x0

    .line 92
    .local v0, "password":[C
    :try_start_0
    invoke-static {}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->getInstance()Lkellinwood/security/zipsigner/optional/PasswordObfuscator;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->decodeKeystorePassword(Ljava/lang/String;Ljava/lang/String;)[C

    move-result-object v0

    .line 93
    invoke-static {p0, p1, v0}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->writeKeyStore(Ljava/security/KeyStore;Ljava/lang/String;[C)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    if-eqz v0, :cond_0

    invoke-static {v0}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->flush([C)V

    .line 97
    :cond_0
    return-void

    .line 95
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->flush([C)V

    :cond_1
    throw v1
.end method

.method public static writeKeyStore(Ljava/security/KeyStore;Ljava/lang/String;[C)V
    .locals 9
    .param p0, "ks"    # Ljava/security/KeyStore;
    .param p1, "keystorePath"    # Ljava/lang/String;
    .param p2, "password"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 103
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 105
    .local v1, "keystoreFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 108
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v8

    invoke-static {v6, v7, v8}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v4

    .line 109
    .local v4, "tmpFile":Ljava/io/File;
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 110
    .local v0, "fos":Ljava/io/FileOutputStream;
    invoke-virtual {p0, v0, p2}, Ljava/security/KeyStore;->store(Ljava/io/OutputStream;[C)V

    .line 111
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V

    .line 112
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 122
    invoke-static {v4, v1}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->renameTo(Ljava/io/File;Ljava/io/File;)V

    .line 138
    .end local v4    # "tmpFile":Ljava/io/File;
    :goto_0
    return-void

    .line 124
    .end local v0    # "fos":Ljava/io/FileOutputStream;
    :cond_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 125
    .restart local v0    # "fos":Ljava/io/FileOutputStream;
    invoke-virtual {p0, v0, p2}, Ljava/security/KeyStore;->store(Ljava/io/OutputStream;[C)V

    .line 126
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 128
    .end local v0    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v5

    .line 130
    .local v5, "x":Ljava/lang/Exception;
    :try_start_1
    const-string v6, "zipsigner-error"

    const-string v7, ".log"

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v8

    invoke-static {v6, v7, v8}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    .line 131
    .local v2, "logfile":Ljava/io/File;
    new-instance v3, Ljava/io/PrintWriter;

    new-instance v6, Ljava/io/FileWriter;

    invoke-direct {v6, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v6}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 132
    .local v3, "pw":Ljava/io/PrintWriter;
    invoke-virtual {v5, v3}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 133
    invoke-virtual {v3}, Ljava/io/PrintWriter;->flush()V

    .line 134
    invoke-virtual {v3}, Ljava/io/PrintWriter;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 136
    .end local v2    # "logfile":Ljava/io/File;
    .end local v3    # "pw":Ljava/io/PrintWriter;
    :goto_1
    throw v5

    .line 135
    :catch_1
    move-exception v6

    goto :goto_1
.end method
