.class public Lkellinwood/security/zipsigner/optional/Fingerprint;
.super Ljava/lang/Object;
.source "Fingerprint.java"


# static fields
.field static logger:Lkellinwood/logging/LoggerInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lkellinwood/security/zipsigner/optional/Fingerprint;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkellinwood/logging/LoggerManager;->getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    sput-object v0, Lkellinwood/security/zipsigner/optional/Fingerprint;->logger:Lkellinwood/logging/LoggerInterface;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static base64Fingerprint(Ljava/lang/String;[B)Ljava/lang/String;
    .locals 5
    .param p0, "algorithm"    # Ljava/lang/String;
    .param p1, "encodedCert"    # [B

    .prologue
    .line 59
    const/4 v1, 0x0

    .line 61
    .local v1, "result":Ljava/lang/String;
    :try_start_0
    invoke-static {p0, p1}, Lkellinwood/security/zipsigner/optional/Fingerprint;->calcDigest(Ljava/lang/String;[B)[B

    move-result-object v0

    .line 62
    .local v0, "digest":[B
    if-nez v0, :cond_0

    .line 67
    .end local v0    # "digest":[B
    .end local v1    # "result":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 63
    .restart local v0    # "digest":[B
    .restart local v1    # "result":Ljava/lang/String;
    :cond_0
    invoke-static {v0}, Lkellinwood/security/zipsigner/Base64;->encode([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 64
    .end local v0    # "digest":[B
    :catch_0
    move-exception v2

    .line 65
    .local v2, "x":Ljava/lang/Exception;
    sget-object v3, Lkellinwood/security/zipsigner/optional/Fingerprint;->logger:Lkellinwood/logging/LoggerInterface;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Lkellinwood/logging/LoggerInterface;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static calcDigest(Ljava/lang/String;[B)[B
    .locals 5
    .param p0, "algorithm"    # Ljava/lang/String;
    .param p1, "encodedCert"    # [B

    .prologue
    .line 19
    const/4 v1, 0x0

    .line 21
    .local v1, "result":[B
    :try_start_0
    invoke-static {p0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 22
    .local v0, "messageDigest":Ljava/security/MessageDigest;
    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update([B)V

    .line 23
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 27
    .end local v0    # "messageDigest":Ljava/security/MessageDigest;
    :goto_0
    return-object v1

    .line 24
    :catch_0
    move-exception v2

    .line 25
    .local v2, "x":Ljava/lang/Exception;
    sget-object v3, Lkellinwood/security/zipsigner/optional/Fingerprint;->logger:Lkellinwood/logging/LoggerInterface;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Lkellinwood/logging/LoggerInterface;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static hexFingerprint(Ljava/lang/String;[B)Ljava/lang/String;
    .locals 10
    .param p0, "algorithm"    # Ljava/lang/String;
    .param p1, "encodedCert"    # [B

    .prologue
    const/4 v9, 0x0

    .line 32
    :try_start_0
    invoke-static {p0, p1}, Lkellinwood/security/zipsigner/optional/Fingerprint;->calcDigest(Ljava/lang/String;[B)[B

    move-result-object v1

    .line 33
    .local v1, "digest":[B
    if-nez v1, :cond_0

    move-object v2, v9

    .line 47
    .end local v1    # "digest":[B
    :goto_0
    return-object v2

    .line 34
    .restart local v1    # "digest":[B
    :cond_0
    new-instance v0, Lorg/spongycastle/util/encoders/HexTranslator;

    invoke-direct {v0}, Lorg/spongycastle/util/encoders/HexTranslator;-><init>()V

    .line 35
    .local v0, "hexTranslator":Lorg/spongycastle/util/encoders/HexTranslator;
    array-length v2, v1

    mul-int/lit8 v2, v2, 0x2

    new-array v4, v2, [B

    .line 36
    .local v4, "hex":[B
    const/4 v2, 0x0

    array-length v3, v1

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lorg/spongycastle/util/encoders/HexTranslator;->encode([BII[BI)I

    .line 37
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    .local v6, "builder":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    array-length v2, v4

    if-ge v7, v2, :cond_2

    .line 39
    aget-byte v2, v4, v7

    int-to-char v2, v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 40
    add-int/lit8 v2, v7, 0x1

    aget-byte v2, v4, v2

    int-to-char v2, v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 41
    array-length v2, v4

    add-int/lit8 v2, v2, -0x2

    if-eq v7, v2, :cond_1

    const/16 v2, 0x3a

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 38
    :cond_1
    add-int/lit8 v7, v7, 0x2

    goto :goto_1

    .line 43
    :cond_2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 44
    .end local v0    # "hexTranslator":Lorg/spongycastle/util/encoders/HexTranslator;
    .end local v1    # "digest":[B
    .end local v4    # "hex":[B
    .end local v6    # "builder":Ljava/lang/StringBuilder;
    .end local v7    # "i":I
    :catch_0
    move-exception v8

    .line 45
    .local v8, "x":Ljava/lang/Exception;
    sget-object v2, Lkellinwood/security/zipsigner/optional/Fingerprint;->logger:Lkellinwood/logging/LoggerInterface;

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v8}, Lkellinwood/logging/LoggerInterface;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v2, v9

    .line 47
    goto :goto_0
.end method
