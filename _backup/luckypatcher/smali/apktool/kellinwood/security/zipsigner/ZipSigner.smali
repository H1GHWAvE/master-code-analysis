.class public Lkellinwood/security/zipsigner/ZipSigner;
.super Ljava/lang/Object;
.source "ZipSigner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkellinwood/security/zipsigner/ZipSigner$AutoKeyObservable;
    }
.end annotation


# static fields
.field private static final CERT_RSA_NAME:Ljava/lang/String; = "META-INF/CERT.RSA"

.field private static final CERT_SF_NAME:Ljava/lang/String; = "META-INF/CERT.SF"

.field public static final KEY_NONE:Ljava/lang/String; = "none"

.field public static final KEY_TESTKEY:Ljava/lang/String; = "testkey"

.field public static final MODE_AUTO:Ljava/lang/String; = "auto"

.field public static final MODE_AUTO_NONE:Ljava/lang/String; = "auto-none"

.field public static final MODE_AUTO_TESTKEY:Ljava/lang/String; = "auto-testkey"

.field public static final SUPPORTED_KEY_MODES:[Ljava/lang/String;

.field static log:Lkellinwood/logging/LoggerInterface;

.field private static stripPattern:Ljava/util/regex/Pattern;


# instance fields
.field autoKeyDetect:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field autoKeyObservable:Lkellinwood/security/zipsigner/ZipSigner$AutoKeyObservable;

.field private canceled:Z

.field keySet:Lkellinwood/security/zipsigner/KeySet;

.field keymode:Ljava/lang/String;

.field loadedKeys:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/security/zipsigner/KeySet;",
            ">;"
        }
    .end annotation
.end field

.field private progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

.field private resourceAdapter:Lkellinwood/security/zipsigner/ResourceAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 72
    const/4 v0, 0x0

    sput-object v0, Lkellinwood/security/zipsigner/ZipSigner;->log:Lkellinwood/logging/LoggerInterface;

    .line 78
    const-string v0, "^META-INF/(.*)[.](SF|RSA|DSA)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lkellinwood/security/zipsigner/ZipSigner;->stripPattern:Ljava/util/regex/Pattern;

    .line 96
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "auto-testkey"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "auto"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "auto-none"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "media"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "platform"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "shared"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "testkey"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "none"

    aput-object v2, v0, v1

    sput-object v0, Lkellinwood/security/zipsigner/ZipSigner;->SUPPORTED_KEY_MODES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    .line 69
    new-instance v0, Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-direct {v0}, Lkellinwood/security/zipsigner/ProgressHelper;-><init>()V

    iput-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    .line 70
    new-instance v0, Lkellinwood/security/zipsigner/DefaultResourceAdapter;

    invoke-direct {v0}, Lkellinwood/security/zipsigner/DefaultResourceAdapter;-><init>()V

    iput-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->resourceAdapter:Lkellinwood/security/zipsigner/ResourceAdapter;

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->loadedKeys:Ljava/util/Map;

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    .line 99
    const-string v0, "testkey"

    iput-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->keymode:Ljava/lang/String;

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->autoKeyDetect:Ljava/util/Map;

    .line 103
    new-instance v0, Lkellinwood/security/zipsigner/ZipSigner$AutoKeyObservable;

    invoke-direct {v0}, Lkellinwood/security/zipsigner/ZipSigner$AutoKeyObservable;-><init>()V

    iput-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->autoKeyObservable:Lkellinwood/security/zipsigner/ZipSigner$AutoKeyObservable;

    .line 108
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->autoKeyDetect:Ljava/util/Map;

    const-string v1, "aa9852bc5a53272ac8031d49b65e4b0e"

    const-string v2, "media"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->autoKeyDetect:Ljava/util/Map;

    const-string v1, "e60418c4b638f20d0721e115674ca11f"

    const-string v2, "platform"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->autoKeyDetect:Ljava/util/Map;

    const-string v1, "3e24e49741b60c215c010dc6048fca7d"

    const-string v2, "shared"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->autoKeyDetect:Ljava/util/Map;

    const-string v1, "dab2cead827ef5313f28e22b6fa8479f"

    const-string v2, "testkey"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    return-void
.end method

.method private addDigestsToManifest(Ljava/util/Map;)Ljava/util/jar/Manifest;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/zipio/ZioEntry;",
            ">;)",
            "Ljava/util/jar/Manifest;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 378
    .local p1, "entries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    const/4 v10, 0x0

    .line 379
    .local v10, "input":Ljava/util/jar/Manifest;
    const-string v17, "META-INF/MANIFEST.MF"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lkellinwood/zipio/ZioEntry;

    .line 380
    .local v12, "manifestEntry":Lkellinwood/zipio/ZioEntry;
    if-eqz v12, :cond_0

    .line 381
    new-instance v10, Ljava/util/jar/Manifest;

    .end local v10    # "input":Ljava/util/jar/Manifest;
    invoke-direct {v10}, Ljava/util/jar/Manifest;-><init>()V

    .line 382
    .restart local v10    # "input":Ljava/util/jar/Manifest;
    invoke-virtual {v12}, Lkellinwood/zipio/ZioEntry;->getInputStream()Ljava/io/InputStream;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/util/jar/Manifest;->read(Ljava/io/InputStream;)V

    .line 384
    :cond_0
    new-instance v16, Ljava/util/jar/Manifest;

    invoke-direct/range {v16 .. v16}, Ljava/util/jar/Manifest;-><init>()V

    .line 385
    .local v16, "output":Ljava/util/jar/Manifest;
    invoke-virtual/range {v16 .. v16}, Ljava/util/jar/Manifest;->getMainAttributes()Ljava/util/jar/Attributes;

    move-result-object v11

    .line 386
    .local v11, "main":Ljava/util/jar/Attributes;
    if-eqz v10, :cond_4

    .line 387
    invoke-virtual {v10}, Ljava/util/jar/Manifest;->getMainAttributes()Ljava/util/jar/Attributes;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/jar/Attributes;->putAll(Ljava/util/Map;)V

    .line 394
    :goto_0
    const-string v17, "SHA1"

    invoke-static/range {v17 .. v17}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v13

    .line 395
    .local v13, "md":Ljava/security/MessageDigest;
    const/16 v17, 0x200

    move/from16 v0, v17

    new-array v3, v0, [B

    .line 402
    .local v3, "buffer":[B
    new-instance v4, Ljava/util/TreeMap;

    invoke-direct {v4}, Ljava/util/TreeMap;-><init>()V

    .line 403
    .local v4, "byName":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/util/TreeMap;->putAll(Ljava/util/Map;)V

    .line 405
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v6

    .line 406
    .local v6, "debug":Z
    if-eqz v6, :cond_1

    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v17

    const-string v18, "Manifest entries:"

    invoke-interface/range {v17 .. v18}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 407
    :cond_1
    invoke-virtual {v4}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lkellinwood/zipio/ZioEntry;

    .line 408
    .local v7, "entry":Lkellinwood/zipio/ZioEntry;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    move/from16 v17, v0

    if-eqz v17, :cond_5

    .line 434
    .end local v7    # "entry":Lkellinwood/zipio/ZioEntry;
    :cond_3
    return-object v16

    .line 389
    .end local v3    # "buffer":[B
    .end local v4    # "byName":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .end local v6    # "debug":Z
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v13    # "md":Ljava/security/MessageDigest;
    :cond_4
    const-string v17, "Manifest-Version"

    const-string v18, "1.0"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Ljava/util/jar/Attributes;->putValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 390
    const-string v17, "Created-By"

    const-string v18, "1.0 (Android SignApk)"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Ljava/util/jar/Attributes;->putValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 409
    .restart local v3    # "buffer":[B
    .restart local v4    # "byName":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .restart local v6    # "debug":Z
    .restart local v7    # "entry":Lkellinwood/zipio/ZioEntry;
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v13    # "md":Ljava/security/MessageDigest;
    :cond_5
    invoke-virtual {v7}, Lkellinwood/zipio/ZioEntry;->getName()Ljava/lang/String;

    move-result-object v14

    .line 410
    .local v14, "name":Ljava/lang/String;
    if-eqz v6, :cond_6

    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 411
    :cond_6
    invoke-virtual {v7}, Lkellinwood/zipio/ZioEntry;->isDirectory()Z

    move-result v17

    if-nez v17, :cond_2

    const-string v17, "META-INF/MANIFEST.MF"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2

    const-string v17, "META-INF/CERT.SF"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2

    const-string v17, "META-INF/CERT.RSA"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2

    sget-object v17, Lkellinwood/security/zipsigner/ZipSigner;->stripPattern:Ljava/util/regex/Pattern;

    if-eqz v17, :cond_7

    sget-object v17, Lkellinwood/security/zipsigner/ZipSigner;->stripPattern:Ljava/util/regex/Pattern;

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/regex/Matcher;->matches()Z

    move-result v17

    if-nez v17, :cond_2

    .line 417
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lkellinwood/security/zipsigner/ZipSigner;->resourceAdapter:Lkellinwood/security/zipsigner/ResourceAdapter;

    move-object/from16 v19, v0

    sget-object v20, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->GENERATING_MANIFEST:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    invoke-interface/range {v19 .. v21}, Lkellinwood/security/zipsigner/ResourceAdapter;->getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Lkellinwood/security/zipsigner/ProgressHelper;->progress(ILjava/lang/String;)V

    .line 418
    invoke-virtual {v7}, Lkellinwood/zipio/ZioEntry;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 419
    .local v5, "data":Ljava/io/InputStream;
    :goto_2
    invoke-virtual {v5, v3}, Ljava/io/InputStream;->read([B)I

    move-result v15

    .local v15, "num":I
    if-lez v15, :cond_8

    .line 420
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v13, v3, v0, v15}, Ljava/security/MessageDigest;->update([BII)V

    goto :goto_2

    .line 423
    :cond_8
    const/4 v2, 0x0

    .line 424
    .local v2, "attr":Ljava/util/jar/Attributes;
    if-eqz v10, :cond_9

    .line 425
    invoke-virtual {v10, v14}, Ljava/util/jar/Manifest;->getAttributes(Ljava/lang/String;)Ljava/util/jar/Attributes;

    move-result-object v9

    .line 426
    .local v9, "inAttr":Ljava/util/jar/Attributes;
    if-eqz v9, :cond_9

    new-instance v2, Ljava/util/jar/Attributes;

    .end local v2    # "attr":Ljava/util/jar/Attributes;
    invoke-direct {v2, v9}, Ljava/util/jar/Attributes;-><init>(Ljava/util/jar/Attributes;)V

    .line 428
    .end local v9    # "inAttr":Ljava/util/jar/Attributes;
    .restart local v2    # "attr":Ljava/util/jar/Attributes;
    :cond_9
    if-nez v2, :cond_a

    new-instance v2, Ljava/util/jar/Attributes;

    .end local v2    # "attr":Ljava/util/jar/Attributes;
    invoke-direct {v2}, Ljava/util/jar/Attributes;-><init>()V

    .line 429
    .restart local v2    # "attr":Ljava/util/jar/Attributes;
    :cond_a
    const-string v17, "SHA1-Digest"

    invoke-virtual {v13}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lkellinwood/security/zipsigner/Base64;->encode([B)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Ljava/util/jar/Attributes;->putValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 430
    invoke-virtual/range {v16 .. v16}, Ljava/util/jar/Manifest;->getEntries()Ljava/util/Map;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v0, v14, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1
.end method

.method private copyFiles(Ljava/util/Map;Lkellinwood/zipio/ZipOutput;)V
    .locals 10
    .param p2, "output"    # Lkellinwood/zipio/ZipOutput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/zipio/ZioEntry;",
            ">;",
            "Lkellinwood/zipio/ZipOutput;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "input":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    const/4 v9, 0x0

    .line 554
    const/4 v0, 0x1

    .line 555
    .local v0, "i":I
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkellinwood/zipio/ZioEntry;

    .line 556
    .local v2, "inEntry":Lkellinwood/zipio/ZioEntry;
    iget-boolean v3, p0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    if-eqz v3, :cond_1

    .line 561
    .end local v2    # "inEntry":Lkellinwood/zipio/ZioEntry;
    :cond_0
    return-void

    .line 557
    .restart local v2    # "inEntry":Lkellinwood/zipio/ZioEntry;
    :cond_1
    iget-object v3, p0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    iget-object v4, p0, Lkellinwood/security/zipsigner/ZipSigner;->resourceAdapter:Lkellinwood/security/zipsigner/ResourceAdapter;

    sget-object v5, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->COPYING_ZIP_ENTRY:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    const/4 v7, 0x1

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-interface {v4, v5, v6}, Lkellinwood/security/zipsigner/ResourceAdapter;->getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Lkellinwood/security/zipsigner/ProgressHelper;->progress(ILjava/lang/String;)V

    .line 558
    add-int/lit8 v0, v0, 0x1

    .line 559
    invoke-virtual {p2, v2}, Lkellinwood/zipio/ZipOutput;->write(Lkellinwood/zipio/ZioEntry;)V

    goto :goto_0
.end method

.method private copyFiles(Ljava/util/jar/Manifest;Ljava/util/Map;Lkellinwood/zipio/ZipOutput;J)V
    .locals 15
    .param p1, "manifest"    # Ljava/util/jar/Manifest;
    .param p3, "output"    # Lkellinwood/zipio/ZipOutput;
    .param p4, "timestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/jar/Manifest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/zipio/ZioEntry;",
            ">;",
            "Lkellinwood/zipio/ZipOutput;",
            "J)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 533
    .local p2, "input":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/jar/Manifest;->getEntries()Ljava/util/Map;

    move-result-object v2

    .line 534
    .local v2, "entries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/jar/Attributes;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 535
    .local v7, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v7}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 536
    const/4 v3, 0x1

    .line 537
    .local v3, "i":I
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 538
    .local v6, "name":Ljava/lang/String;
    iget-boolean v8, p0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    if-eqz v8, :cond_1

    .line 546
    .end local v6    # "name":Ljava/lang/String;
    :cond_0
    return-void

    .line 539
    .restart local v6    # "name":Ljava/lang/String;
    :cond_1
    iget-object v8, p0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    const/4 v9, 0x0

    iget-object v10, p0, Lkellinwood/security/zipsigner/ZipSigner;->resourceAdapter:Lkellinwood/security/zipsigner/ResourceAdapter;

    sget-object v11, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->COPYING_ZIP_ENTRY:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-interface {v10, v11, v12}, Lkellinwood/security/zipsigner/ResourceAdapter;->getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lkellinwood/security/zipsigner/ProgressHelper;->progress(ILjava/lang/String;)V

    .line 540
    add-int/lit8 v3, v3, 0x1

    .line 541
    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lkellinwood/zipio/ZioEntry;

    .line 542
    .local v5, "inEntry":Lkellinwood/zipio/ZioEntry;
    move-wide/from16 v0, p4

    invoke-virtual {v5, v0, v1}, Lkellinwood/zipio/ZioEntry;->setTime(J)V

    .line 543
    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Lkellinwood/zipio/ZipOutput;->write(Lkellinwood/zipio/ZioEntry;)V

    goto :goto_0
.end method

.method private decryptPrivateKey([BLjava/lang/String;)Ljava/security/spec/KeySpec;
    .locals 8
    .param p1, "encryptedPrivateKey"    # [B
    .param p2, "keyPassword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 307
    :try_start_0
    new-instance v1, Ljavax/crypto/EncryptedPrivateKeyInfo;

    invoke-direct {v1, p1}, Ljavax/crypto/EncryptedPrivateKeyInfo;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    .local v1, "epkInfo":Ljavax/crypto/EncryptedPrivateKeyInfo;
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    .line 315
    .local v4, "keyPasswd":[C
    invoke-virtual {v1}, Ljavax/crypto/EncryptedPrivateKeyInfo;->getAlgName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v5

    .line 316
    .local v5, "skFactory":Ljavax/crypto/SecretKeyFactory;
    new-instance v6, Ljavax/crypto/spec/PBEKeySpec;

    invoke-direct {v6, v4}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C)V

    invoke-virtual {v5, v6}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    move-result-object v3

    .line 318
    .local v3, "key":Ljava/security/Key;
    invoke-virtual {v1}, Ljavax/crypto/EncryptedPrivateKeyInfo;->getAlgName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 319
    .local v0, "cipher":Ljavax/crypto/Cipher;
    const/4 v6, 0x2

    invoke-virtual {v1}, Ljavax/crypto/EncryptedPrivateKeyInfo;->getAlgParameters()Ljava/security/AlgorithmParameters;

    move-result-object v7

    invoke-virtual {v0, v6, v3, v7}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/AlgorithmParameters;)V

    .line 322
    :try_start_1
    invoke-virtual {v1, v0}, Ljavax/crypto/EncryptedPrivateKeyInfo;->getKeySpec(Ljavax/crypto/Cipher;)Ljava/security/spec/PKCS8EncodedKeySpec;
    :try_end_1
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    .end local v0    # "cipher":Ljavax/crypto/Cipher;
    .end local v1    # "epkInfo":Ljavax/crypto/EncryptedPrivateKeyInfo;
    .end local v3    # "key":Ljava/security/Key;
    .end local v4    # "keyPasswd":[C
    .end local v5    # "skFactory":Ljavax/crypto/SecretKeyFactory;
    :goto_0
    return-object v6

    .line 308
    :catch_0
    move-exception v2

    .line 310
    .local v2, "ex":Ljava/io/IOException;
    const/4 v6, 0x0

    goto :goto_0

    .line 323
    .end local v2    # "ex":Ljava/io/IOException;
    .restart local v0    # "cipher":Ljavax/crypto/Cipher;
    .restart local v1    # "epkInfo":Ljavax/crypto/EncryptedPrivateKeyInfo;
    .restart local v3    # "key":Ljava/security/Key;
    .restart local v4    # "keyPasswd":[C
    .restart local v5    # "skFactory":Ljavax/crypto/SecretKeyFactory;
    :catch_1
    move-exception v2

    .line 324
    .local v2, "ex":Ljava/security/spec/InvalidKeySpecException;
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v6

    const-string v7, "signapk: Password for private key may be bad."

    invoke-interface {v6, v7}, Lkellinwood/logging/LoggerInterface;->error(Ljava/lang/String;)V

    .line 325
    throw v2
.end method

.method private generateSignatureFile(Ljava/util/jar/Manifest;Ljava/io/OutputStream;)V
    .locals 13
    .param p1, "manifest"    # Ljava/util/jar/Manifest;
    .param p2, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 441
    const-string v8, "Signature-Version: 1.0\r\n"

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/io/OutputStream;->write([B)V

    .line 442
    const-string v8, "Created-By: 1.0 (Android SignApk)\r\n"

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/io/OutputStream;->write([B)V

    .line 446
    const-string v8, "SHA1"

    invoke-static {v8}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v5

    .line 447
    .local v5, "md":Ljava/security/MessageDigest;
    new-instance v7, Ljava/io/PrintStream;

    new-instance v8, Ljava/security/DigestOutputStream;

    new-instance v9, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v9}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-direct {v8, v9, v5}, Ljava/security/DigestOutputStream;-><init>(Ljava/io/OutputStream;Ljava/security/MessageDigest;)V

    const/4 v9, 0x1

    const-string v10, "UTF-8"

    invoke-direct {v7, v8, v9, v10}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;ZLjava/lang/String;)V

    .line 452
    .local v7, "print":Ljava/io/PrintStream;
    invoke-virtual {p1, v7}, Ljava/util/jar/Manifest;->write(Ljava/io/OutputStream;)V

    .line 453
    invoke-virtual {v7}, Ljava/io/PrintStream;->flush()V

    .line 455
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SHA1-Digest-Manifest: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v9

    invoke-static {v9}, Lkellinwood/security/zipsigner/Base64;->encode([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\r\n\r\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/io/OutputStream;->write([B)V

    .line 457
    invoke-virtual {p1}, Ljava/util/jar/Manifest;->getEntries()Ljava/util/Map;

    move-result-object v1

    .line 458
    .local v1, "entries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/jar/Attributes;>;"
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 459
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/jar/Attributes;>;"
    iget-boolean v8, p0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    if-eqz v8, :cond_1

    .line 474
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/jar/Attributes;>;"
    :cond_0
    return-void

    .line 460
    .restart local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/jar/Attributes;>;"
    :cond_1
    iget-object v8, p0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    iget-object v9, p0, Lkellinwood/security/zipsigner/ZipSigner;->resourceAdapter:Lkellinwood/security/zipsigner/ResourceAdapter;

    sget-object v10, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->GENERATING_SIGNATURE_FILE:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    new-array v11, v12, [Ljava/lang/Object;

    invoke-interface {v9, v10, v11}, Lkellinwood/security/zipsigner/ResourceAdapter;->getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v12, v9}, Lkellinwood/security/zipsigner/ProgressHelper;->progress(ILjava/lang/String;)V

    .line 462
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Name: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\r\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 463
    .local v6, "nameEntry":Ljava/lang/String;
    invoke-virtual {v7, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 464
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/jar/Attributes;

    invoke-virtual {v8}, Ljava/util/jar/Attributes;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 465
    .local v0, "att":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;Ljava/lang/Object;>;"
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\r\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_1

    .line 467
    .end local v0    # "att":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;Ljava/lang/Object;>;"
    :cond_2
    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 468
    invoke-virtual {v7}, Ljava/io/PrintStream;->flush()V

    .line 470
    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/io/OutputStream;->write([B)V

    .line 471
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SHA1-Digest: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v9

    invoke-static {v9}, Lkellinwood/security/zipsigner/Base64;->encode([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\r\n\r\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/io/OutputStream;->write([B)V

    goto/16 :goto_0
.end method

.method public static getLogger()Lkellinwood/logging/LoggerInterface;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lkellinwood/security/zipsigner/ZipSigner;->log:Lkellinwood/logging/LoggerInterface;

    if-nez v0, :cond_0

    const-class v0, Lkellinwood/security/zipsigner/ZipSigner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkellinwood/logging/LoggerManager;->getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    sput-object v0, Lkellinwood/security/zipsigner/ZipSigner;->log:Lkellinwood/logging/LoggerInterface;

    .line 86
    :cond_0
    sget-object v0, Lkellinwood/security/zipsigner/ZipSigner;->log:Lkellinwood/logging/LoggerInterface;

    return-object v0
.end method

.method public static getSupportedKeyModes()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lkellinwood/security/zipsigner/ZipSigner;->SUPPORTED_KEY_MODES:[Ljava/lang/String;

    return-object v0
.end method

.method private writeSignatureBlock(Lkellinwood/security/zipsigner/KeySet;[BLjava/io/OutputStream;)V
    .locals 15
    .param p1, "keySet"    # Lkellinwood/security/zipsigner/KeySet;
    .param p2, "signatureFileBytes"    # [B
    .param p3, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 481
    invoke-virtual/range {p1 .. p1}, Lkellinwood/security/zipsigner/KeySet;->getSigBlockTemplate()[B

    move-result-object v11

    if-eqz v11, :cond_1

    .line 486
    new-instance v7, Lkellinwood/security/zipsigner/ZipSignature;

    invoke-direct {v7}, Lkellinwood/security/zipsigner/ZipSignature;-><init>()V

    .line 487
    .local v7, "signature":Lkellinwood/security/zipsigner/ZipSignature;
    invoke-virtual/range {p1 .. p1}, Lkellinwood/security/zipsigner/KeySet;->getPrivateKey()Ljava/security/PrivateKey;

    move-result-object v11

    invoke-virtual {v7, v11}, Lkellinwood/security/zipsigner/ZipSignature;->initSign(Ljava/security/PrivateKey;)V

    .line 488
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lkellinwood/security/zipsigner/ZipSignature;->update([B)V

    .line 489
    invoke-virtual {v7}, Lkellinwood/security/zipsigner/ZipSignature;->sign()[B

    move-result-object v8

    .line 491
    .local v8, "signatureBytes":[B
    invoke-virtual/range {p1 .. p1}, Lkellinwood/security/zipsigner/KeySet;->getSigBlockTemplate()[B

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Ljava/io/OutputStream;->write([B)V

    .line 492
    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Ljava/io/OutputStream;->write([B)V

    .line 494
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v11

    invoke-interface {v11}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 496
    const-string v11, "SHA1"

    invoke-static {v11}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v4

    .line 497
    .local v4, "md":Ljava/security/MessageDigest;
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 498
    invoke-virtual {v4}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    .line 499
    .local v5, "sfDigest":[B
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Sig File SHA1: \n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v5}, Lkellinwood/security/zipsigner/HexDumpEncoder;->encode([B)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 501
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Signature: \n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v8}, Lkellinwood/security/zipsigner/HexDumpEncoder;->encode([B)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 503
    const-string v11, "RSA/ECB/PKCS1Padding"

    invoke-static {v11}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 504
    .local v1, "cipher":Ljavax/crypto/Cipher;
    const/4 v11, 0x2

    invoke-virtual/range {p1 .. p1}, Lkellinwood/security/zipsigner/KeySet;->getPublicKey()Ljava/security/cert/X509Certificate;

    move-result-object v12

    invoke-virtual {v1, v11, v12}, Ljavax/crypto/Cipher;->init(ILjava/security/cert/Certificate;)V

    .line 506
    invoke-virtual {v1, v8}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v9

    .line 507
    .local v9, "tmpData":[B
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Signature Decrypted: \n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v9}, Lkellinwood/security/zipsigner/HexDumpEncoder;->encode([B)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 522
    .end local v1    # "cipher":Ljavax/crypto/Cipher;
    .end local v4    # "md":Ljava/security/MessageDigest;
    .end local v5    # "sfDigest":[B
    .end local v7    # "signature":Lkellinwood/security/zipsigner/ZipSignature;
    .end local v8    # "signatureBytes":[B
    .end local v9    # "tmpData":[B
    :cond_0
    :goto_0
    return-void

    .line 512
    :cond_1
    const/4 v6, 0x0

    .line 514
    .local v6, "sigBlock":[B
    :try_start_0
    const-string v11, "kellinwood.security.zipsigner.optional.SignatureBlockGenerator"

    invoke-static {v11}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 515
    .local v2, "generatorClass":Ljava/lang/Class;
    const-string v11, "generate"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Class;

    const/4 v13, 0x0

    const-class v14, Lkellinwood/security/zipsigner/KeySet;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    const/4 v14, 0x1

    new-array v14, v14, [B

    invoke-virtual {v14}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v2, v11, v12}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 516
    .local v3, "generatorMethod":Ljava/lang/reflect/Method;
    const/4 v11, 0x0

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object p1, v12, v13

    const/4 v13, 0x1

    aput-object p2, v12, v13

    invoke-virtual {v3, v11, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [B

    move-object v0, v11

    check-cast v0, [B

    move-object v6, v0

    .line 517
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 518
    .end local v2    # "generatorClass":Ljava/lang/Class;
    .end local v3    # "generatorMethod":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v10

    .line 519
    .local v10, "x":Ljava/lang/Exception;
    new-instance v11, Ljava/lang/RuntimeException;

    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v11
.end method


# virtual methods
.method public addAutoKeyObserver(Ljava/util/Observer;)V
    .locals 1
    .param p1, "o"    # Ljava/util/Observer;

    .prologue
    .line 125
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->autoKeyObservable:Lkellinwood/security/zipsigner/ZipSigner$AutoKeyObservable;

    invoke-virtual {v0, p1}, Lkellinwood/security/zipsigner/ZipSigner$AutoKeyObservable;->addObserver(Ljava/util/Observer;)V

    .line 126
    return-void
.end method

.method public addProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
    .locals 1
    .param p1, "l"    # Lkellinwood/security/zipsigner/ProgressListener;

    .prologue
    .line 764
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-virtual {v0, p1}, Lkellinwood/security/zipsigner/ProgressHelper;->addProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V

    .line 765
    return-void
.end method

.method protected autoDetectKey(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 19
    .param p1, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/zipio/ZioEntry;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    .local p2, "zioEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v15

    invoke-interface {v15}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v4

    .line 155
    .local v4, "debug":Z
    const-string v15, "auto"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 207
    .end local p1    # "mode":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 159
    .restart local p1    # "mode":Ljava/lang/String;
    :cond_0
    const/4 v10, 0x0

    .line 161
    .local v10, "keyName":Ljava/lang/String;
    invoke-interface/range {p2 .. p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 162
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 163
    .local v7, "entryName":Ljava/lang/String;
    const-string v15, "META-INF/"

    invoke-virtual {v7, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    const-string v15, ".RSA"

    invoke-virtual {v7, v15}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 167
    const-string v15, "MD5"

    invoke-static {v15}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v12

    .line 168
    .local v12, "md5":Ljava/security/MessageDigest;
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lkellinwood/zipio/ZioEntry;

    invoke-virtual {v15}, Lkellinwood/zipio/ZioEntry;->getData()[B

    move-result-object v6

    .line 169
    .local v6, "entryData":[B
    array-length v15, v6

    const/16 v16, 0x5b2

    move/from16 v0, v16

    if-ge v15, v0, :cond_4

    .line 195
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .end local v6    # "entryData":[B
    .end local v7    # "entryName":Ljava/lang/String;
    .end local v12    # "md5":Ljava/security/MessageDigest;
    :cond_2
    const-string v15, "auto-testkey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 197
    if-eqz v4, :cond_3

    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v15

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Falling back to key="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 198
    :cond_3
    const-string p1, "testkey"

    goto :goto_0

    .line 170
    .restart local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .restart local v6    # "entryData":[B
    .restart local v7    # "entryName":Ljava/lang/String;
    .restart local v12    # "md5":Ljava/security/MessageDigest;
    :cond_4
    const/4 v15, 0x0

    const/16 v16, 0x5b2

    move/from16 v0, v16

    invoke-virtual {v12, v6, v15, v0}, Ljava/security/MessageDigest;->update([BII)V

    .line 171
    invoke-virtual {v12}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v14

    .line 174
    .local v14, "rawDigest":[B
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    .local v3, "builder":Ljava/lang/StringBuilder;
    move-object v1, v14

    .local v1, "arr$":[B
    array-length v11, v1

    .local v11, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_1
    if-ge v9, v11, :cond_5

    aget-byte v2, v1, v9

    .line 176
    .local v2, "b":B
    const-string v15, "%02x"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 179
    .end local v2    # "b":B
    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 181
    .local v13, "md5String":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lkellinwood/security/zipsigner/ZipSigner;->autoKeyDetect:Ljava/util/Map;

    invoke-interface {v15, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "keyName":Ljava/lang/String;
    check-cast v10, Ljava/lang/String;

    .line 184
    .restart local v10    # "keyName":Ljava/lang/String;
    if-eqz v4, :cond_6

    .line 185
    if-eqz v10, :cond_7

    .line 186
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v15

    const-string v16, "Auto-determined key=%s using md5=%s"

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v10, v17, v18

    const/16 v18, 0x1

    aput-object v13, v17, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 191
    :cond_6
    :goto_2
    if-eqz v10, :cond_1

    move-object/from16 p1, v10

    goto/16 :goto_0

    .line 188
    :cond_7
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v15

    const-string v16, "Auto key determination failed for md5=%s"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v13, v17, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    goto :goto_2

    .line 201
    .end local v1    # "arr$":[B
    .end local v3    # "builder":Ljava/lang/StringBuilder;
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .end local v6    # "entryData":[B
    .end local v7    # "entryName":Ljava/lang/String;
    .end local v9    # "i$":I
    .end local v11    # "len$":I
    .end local v12    # "md5":Ljava/security/MessageDigest;
    .end local v13    # "md5String":Ljava/lang/String;
    .end local v14    # "rawDigest":[B
    :cond_8
    const-string v15, "auto-none"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 203
    if-eqz v4, :cond_9

    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v15

    const-string v16, "Unable to determine key, returning: none"

    invoke-interface/range {v15 .. v16}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 204
    :cond_9
    const-string p1, "none"

    goto/16 :goto_0

    .line 207
    :cond_a
    const/16 p1, 0x0

    goto/16 :goto_0
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    .line 262
    return-void
.end method

.method public getKeySet()Lkellinwood/security/zipsigner/KeySet;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    return-object v0
.end method

.method public getKeymode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->keymode:Ljava/lang/String;

    return-object v0
.end method

.method public getResourceAdapter()Lkellinwood/security/zipsigner/ResourceAdapter;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->resourceAdapter:Lkellinwood/security/zipsigner/ResourceAdapter;

    return-object v0
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    return v0
.end method

.method public issueLoadingCertAndKeysProgressEvent()V
    .locals 5

    .prologue
    .line 211
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    const/4 v1, 0x1

    iget-object v2, p0, Lkellinwood/security/zipsigner/ZipSigner;->resourceAdapter:Lkellinwood/security/zipsigner/ResourceAdapter;

    sget-object v3, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->LOADING_CERTIFICATE_AND_KEY:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {v2, v3, v4}, Lkellinwood/security/zipsigner/ResourceAdapter;->getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lkellinwood/security/zipsigner/ProgressHelper;->progress(ILjava/lang/String;)V

    .line 212
    return-void
.end method

.method public loadKeys(Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 219
    iget-object v3, p0, Lkellinwood/security/zipsigner/ZipSigner;->loadedKeys:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkellinwood/security/zipsigner/KeySet;

    iput-object v3, p0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    .line 220
    iget-object v3, p0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    if-eqz v3, :cond_1

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    new-instance v3, Lkellinwood/security/zipsigner/KeySet;

    invoke-direct {v3}, Lkellinwood/security/zipsigner/KeySet;-><init>()V

    iput-object v3, p0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    .line 223
    iget-object v3, p0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual {v3, p1}, Lkellinwood/security/zipsigner/KeySet;->setName(Ljava/lang/String;)V

    .line 224
    iget-object v3, p0, Lkellinwood/security/zipsigner/ZipSigner;->loadedKeys:Ljava/util/Map;

    iget-object v4, p0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-interface {v3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    const-string v3, "none"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 228
    invoke-virtual {p0}, Lkellinwood/security/zipsigner/ZipSigner;->issueLoadingCertAndKeysProgressEvent()V

    .line 231
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/keys/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".pk8"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    .line 232
    .local v0, "privateKeyUrl":Ljava/net/URL;
    iget-object v3, p0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v4}, Lkellinwood/security/zipsigner/ZipSigner;->readPrivateKey(Ljava/net/URL;Ljava/lang/String;)Ljava/security/PrivateKey;

    move-result-object v4

    invoke-virtual {v3, v4}, Lkellinwood/security/zipsigner/KeySet;->setPrivateKey(Ljava/security/PrivateKey;)V

    .line 235
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/keys/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".x509.pem"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v1

    .line 236
    .local v1, "publicKeyUrl":Ljava/net/URL;
    iget-object v3, p0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual {p0, v1}, Lkellinwood/security/zipsigner/ZipSigner;->readPublicKey(Ljava/net/URL;)Ljava/security/cert/X509Certificate;

    move-result-object v4

    invoke-virtual {v3, v4}, Lkellinwood/security/zipsigner/KeySet;->setPublicKey(Ljava/security/cert/X509Certificate;)V

    .line 239
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/keys/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".sbt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v2

    .line 240
    .local v2, "sigBlockTemplateUrl":Ljava/net/URL;
    if-eqz v2, :cond_0

    .line 241
    iget-object v3, p0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual {p0, v2}, Lkellinwood/security/zipsigner/ZipSigner;->readContentAsBytes(Ljava/net/URL;)[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lkellinwood/security/zipsigner/KeySet;->setSigBlockTemplate([B)V

    goto/16 :goto_0
.end method

.method public loadProvider(Ljava/lang/String;)V
    .locals 3
    .param p1, "providerClassName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;
        }
    .end annotation

    .prologue
    .line 277
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 278
    .local v1, "providerClass":Ljava/lang/Class;
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/Provider;

    .line 279
    .local v0, "provider":Ljava/security/Provider;
    const/4 v2, 0x1

    invoke-static {v0, v2}, Ljava/security/Security;->insertProviderAt(Ljava/security/Provider;I)I

    .line 280
    return-void
.end method

.method public readContentAsBytes(Ljava/io/InputStream;)[B
    .locals 5
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 338
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 340
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    const/16 v4, 0x800

    new-array v1, v4, [B

    .line 342
    .local v1, "buffer":[B
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 343
    .local v3, "numRead":I
    :goto_0
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 344
    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 345
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    goto :goto_0

    .line 348
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 349
    .local v2, "bytes":[B
    return-object v2
.end method

.method public readContentAsBytes(Ljava/net/URL;)[B
    .locals 1
    .param p1, "contentUrl"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 332
    invoke-virtual {p1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkellinwood/security/zipsigner/ZipSigner;->readContentAsBytes(Ljava/io/InputStream;)[B

    move-result-object v0

    return-object v0
.end method

.method public readPrivateKey(Ljava/net/URL;Ljava/lang/String;)Ljava/security/PrivateKey;
    .locals 5
    .param p1, "privateKeyUrl"    # Ljava/net/URL;
    .param p2, "keyPassword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 355
    new-instance v2, Ljava/io/DataInputStream;

    invoke-virtual {p1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 357
    .local v2, "input":Ljava/io/DataInputStream;
    :try_start_0
    invoke-virtual {p0, v2}, Lkellinwood/security/zipsigner/ZipSigner;->readContentAsBytes(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 359
    .local v0, "bytes":[B
    invoke-direct {p0, v0, p2}, Lkellinwood/security/zipsigner/ZipSigner;->decryptPrivateKey([BLjava/lang/String;)Ljava/security/spec/KeySpec;

    move-result-object v3

    .line 360
    .local v3, "spec":Ljava/security/spec/KeySpec;
    if-nez v3, :cond_0

    .line 361
    new-instance v3, Ljava/security/spec/PKCS8EncodedKeySpec;

    .end local v3    # "spec":Ljava/security/spec/KeySpec;
    invoke-direct {v3, v0}, Ljava/security/spec/PKCS8EncodedKeySpec;-><init>([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 365
    .restart local v3    # "spec":Ljava/security/spec/KeySpec;
    :cond_0
    :try_start_1
    const-string v4, "RSA"

    invoke-static {v4}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
    :try_end_1
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 370
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V

    :goto_0
    return-object v4

    .line 366
    :catch_0
    move-exception v1

    .line 367
    .local v1, "ex":Ljava/security/spec/InvalidKeySpecException;
    :try_start_2
    const-string v4, "DSA"

    invoke-static {v4}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 370
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V

    goto :goto_0

    .end local v0    # "bytes":[B
    .end local v1    # "ex":Ljava/security/spec/InvalidKeySpecException;
    .end local v3    # "spec":Ljava/security/spec/KeySpec;
    :catchall_0
    move-exception v4

    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V

    throw v4
.end method

.method public readPublicKey(Ljava/net/URL;)Ljava/security/cert/X509Certificate;
    .locals 3
    .param p1, "publicKeyUrl"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 285
    invoke-virtual {p1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v1

    .line 287
    .local v1, "input":Ljava/io/InputStream;
    :try_start_0
    const-string v2, "X.509"

    invoke-static {v2}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v0

    .line 288
    .local v0, "cf":Ljava/security/cert/CertificateFactory;
    invoke-virtual {v0, v1}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v2

    check-cast v2, Ljava/security/cert/X509Certificate;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-object v2

    .end local v0    # "cf":Ljava/security/cert/CertificateFactory;
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v2
.end method

.method public declared-synchronized removeProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
    .locals 1
    .param p1, "l"    # Lkellinwood/security/zipsigner/ProgressListener;

    .prologue
    .line 769
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-virtual {v0, p1}, Lkellinwood/security/zipsigner/ProgressHelper;->removeProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 770
    monitor-exit p0

    return-void

    .line 769
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public resetCanceled()V
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    .line 267
    return-void
.end method

.method public setKeymode(Ljava/lang/String;)V
    .locals 3
    .param p1, "km"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 134
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    invoke-interface {v0}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setKeymode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 135
    :cond_0
    iput-object p1, p0, Lkellinwood/security/zipsigner/ZipSigner;->keymode:Ljava/lang/String;

    .line 136
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->keymode:Ljava/lang/String;

    const-string v1, "auto"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    .line 143
    :goto_0
    return-void

    .line 140
    :cond_1
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-virtual {v0}, Lkellinwood/security/zipsigner/ProgressHelper;->initProgress()V

    .line 141
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->keymode:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lkellinwood/security/zipsigner/ZipSigner;->loadKeys(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setKeys(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;Ljava/lang/String;[B)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "publicKey"    # Ljava/security/cert/X509Certificate;
    .param p3, "privateKey"    # Ljava/security/PrivateKey;
    .param p4, "signatureAlgorithm"    # Ljava/lang/String;
    .param p5, "signatureBlockTemplate"    # [B

    .prologue
    .line 252
    new-instance v0, Lkellinwood/security/zipsigner/KeySet;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lkellinwood/security/zipsigner/KeySet;-><init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;Ljava/lang/String;[B)V

    iput-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    .line 253
    return-void
.end method

.method public setKeys(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "publicKey"    # Ljava/security/cert/X509Certificate;
    .param p3, "privateKey"    # Ljava/security/PrivateKey;
    .param p4, "signatureBlockTemplate"    # [B

    .prologue
    .line 247
    new-instance v0, Lkellinwood/security/zipsigner/KeySet;

    invoke-direct {v0, p1, p2, p3, p4}, Lkellinwood/security/zipsigner/KeySet;-><init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V

    iput-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    .line 248
    return-void
.end method

.method public setResourceAdapter(Lkellinwood/security/zipsigner/ResourceAdapter;)V
    .locals 0
    .param p1, "resourceAdapter"    # Lkellinwood/security/zipsigner/ResourceAdapter;

    .prologue
    .line 120
    iput-object p1, p0, Lkellinwood/security/zipsigner/ZipSigner;->resourceAdapter:Lkellinwood/security/zipsigner/ResourceAdapter;

    .line 121
    return-void
.end method

.method public signZip(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "inputZipFilename"    # Ljava/lang/String;
    .param p2, "outputZipFilename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 636
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v0

    .line 637
    .local v0, "inFile":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v2

    .line 639
    .local v2, "outFile":Ljava/io/File;
    invoke-virtual {v0, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 640
    new-instance v3, Ljava/lang/IllegalArgumentException;

    iget-object v4, p0, Lkellinwood/security/zipsigner/ZipSigner;->resourceAdapter:Lkellinwood/security/zipsigner/ResourceAdapter;

    sget-object v5, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->INPUT_SAME_AS_OUTPUT_ERROR:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    new-array v6, v7, [Ljava/lang/Object;

    invoke-interface {v4, v5, v6}, Lkellinwood/security/zipsigner/ResourceAdapter;->getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 643
    :cond_0
    iget-object v3, p0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-virtual {v3}, Lkellinwood/security/zipsigner/ProgressHelper;->initProgress()V

    .line 644
    iget-object v3, p0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    const/4 v4, 0x1

    iget-object v5, p0, Lkellinwood/security/zipsigner/ZipSigner;->resourceAdapter:Lkellinwood/security/zipsigner/ResourceAdapter;

    sget-object v6, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->PARSING_CENTRAL_DIRECTORY:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    new-array v7, v7, [Ljava/lang/Object;

    invoke-interface {v5, v6, v7}, Lkellinwood/security/zipsigner/ResourceAdapter;->getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lkellinwood/security/zipsigner/ProgressHelper;->progress(ILjava/lang/String;)V

    .line 646
    invoke-static {p1}, Lkellinwood/zipio/ZipInput;->read(Ljava/lang/String;)Lkellinwood/zipio/ZipInput;

    move-result-object v1

    .line 647
    .local v1, "input":Lkellinwood/zipio/ZipInput;
    invoke-virtual {v1}, Lkellinwood/zipio/ZipInput;->getEntries()Ljava/util/Map;

    move-result-object v3

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3, v4, p2}, Lkellinwood/security/zipsigner/ZipSigner;->signZip(Ljava/util/Map;Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 648
    return-void
.end method

.method public signZip(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "keystoreURL"    # Ljava/net/URL;
    .param p2, "keystoreType"    # Ljava/lang/String;
    .param p3, "keystorePw"    # Ljava/lang/String;
    .param p4, "certAlias"    # Ljava/lang/String;
    .param p5, "certPw"    # Ljava/lang/String;
    .param p6, "inputZipFilename"    # Ljava/lang/String;
    .param p7, "outputZipFilename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;,
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 576
    invoke-virtual {p3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-virtual {p5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    const-string v6, "SHA1withRSA"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lkellinwood/security/zipsigner/ZipSigner;->signZip(Ljava/net/URL;Ljava/lang/String;[CLjava/lang/String;[CLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    return-void
.end method

.method public signZip(Ljava/net/URL;Ljava/lang/String;[CLjava/lang/String;[CLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1, "keystoreURL"    # Ljava/net/URL;
    .param p2, "keystoreType"    # Ljava/lang/String;
    .param p3, "keystorePw"    # [C
    .param p4, "certAlias"    # Ljava/lang/String;
    .param p5, "certPw"    # [C
    .param p6, "signatureAlgorithm"    # Ljava/lang/String;
    .param p7, "inputZipFilename"    # Ljava/lang/String;
    .param p8, "outputZipFilename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;,
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 590
    const/4 v11, 0x0

    .line 594
    .local v11, "keystoreStream":Ljava/io/InputStream;
    const/4 v10, 0x0

    .line 595
    .local v10, "keystore":Ljava/security/KeyStore;
    if-nez p2, :cond_0

    :try_start_0
    invoke-static {}, Ljava/security/KeyStore;->getDefaultType()Ljava/lang/String;

    move-result-object p2

    .line 596
    :cond_0
    invoke-static {p2}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v10

    .line 598
    invoke-virtual {p1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v11

    .line 599
    invoke-virtual {v10, v11, p3}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 600
    move-object/from16 v0, p4

    invoke-virtual {v10, v0}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v8

    .line 601
    .local v8, "cert":Ljava/security/cert/Certificate;
    move-object v0, v8

    check-cast v0, Ljava/security/cert/X509Certificate;

    move-object v4, v0

    .line 602
    .local v4, "publicKey":Ljava/security/cert/X509Certificate;
    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-virtual {v10, v0, v1}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object v9

    .line 603
    .local v9, "key":Ljava/security/Key;
    move-object v0, v9

    check-cast v0, Ljava/security/PrivateKey;

    move-object v5, v0

    .line 605
    .local v5, "privateKey":Ljava/security/PrivateKey;
    const-string v3, "custom"

    const/4 v7, 0x0

    move-object v2, p0

    move-object/from16 v6, p6

    invoke-virtual/range {v2 .. v7}, Lkellinwood/security/zipsigner/ZipSigner;->setKeys(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;Ljava/lang/String;[B)V

    .line 607
    move-object/from16 v0, p7

    move-object/from16 v1, p8

    invoke-virtual {p0, v0, v1}, Lkellinwood/security/zipsigner/ZipSigner;->signZip(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 610
    if-eqz v11, :cond_1

    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    .line 612
    :cond_1
    return-void

    .line 610
    .end local v4    # "publicKey":Ljava/security/cert/X509Certificate;
    .end local v5    # "privateKey":Ljava/security/PrivateKey;
    .end local v8    # "cert":Ljava/security/cert/Certificate;
    .end local v9    # "key":Ljava/security/Key;
    :catchall_0
    move-exception v2

    if-eqz v11, :cond_2

    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    :cond_2
    throw v2
.end method

.method public signZip(Ljava/util/Map;Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 23
    .param p2, "outputStream"    # Ljava/io/OutputStream;
    .param p3, "outputZipFilename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/zipio/ZioEntry;",
            ">;",
            "Ljava/io/OutputStream;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 658
    .local p1, "zioEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v2

    invoke-interface {v2}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v8

    .line 660
    .local v8, "debug":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-virtual {v2}, Lkellinwood/security/zipsigner/ProgressHelper;->initProgress()V

    .line 661
    move-object/from16 v0, p0

    iget-object v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    if-nez v2, :cond_2

    .line 662
    move-object/from16 v0, p0

    iget-object v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->keymode:Ljava/lang/String;

    const-string v4, "auto"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 663
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v4, "No keys configured for signing the file!"

    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 666
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->keymode:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Lkellinwood/security/zipsigner/ZipSigner;->autoDetectKey(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v11

    .line 667
    .local v11, "keyName":Ljava/lang/String;
    if-nez v11, :cond_1

    .line 668
    new-instance v2, Lkellinwood/security/zipsigner/AutoKeyException;

    move-object/from16 v0, p0

    iget-object v4, v0, Lkellinwood/security/zipsigner/ZipSigner;->resourceAdapter:Lkellinwood/security/zipsigner/ResourceAdapter;

    sget-object v19, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->AUTO_KEY_SELECTION_ERROR:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    new-instance v22, Ljava/io/File;

    move-object/from16 v0, v22

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v4, v0, v1}, Lkellinwood/security/zipsigner/ResourceAdapter;->getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Lkellinwood/security/zipsigner/AutoKeyException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 670
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->autoKeyObservable:Lkellinwood/security/zipsigner/ZipSigner$AutoKeyObservable;

    invoke-virtual {v2, v11}, Lkellinwood/security/zipsigner/ZipSigner$AutoKeyObservable;->notifyObservers(Ljava/lang/Object;)V

    .line 672
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lkellinwood/security/zipsigner/ZipSigner;->loadKeys(Ljava/lang/String;)V

    .line 678
    .end local v11    # "keyName":Ljava/lang/String;
    :cond_2
    const/16 v18, 0x0

    .line 683
    .local v18, "zipOutput":Lkellinwood/zipio/ZipOutput;
    :try_start_0
    new-instance v5, Lkellinwood/zipio/ZipOutput;

    move-object/from16 v0, p2

    invoke-direct {v5, v0}, Lkellinwood/zipio/ZipOutput;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 685
    .end local v18    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    .local v5, "zipOutput":Lkellinwood/zipio/ZipOutput;
    :try_start_1
    const-string v2, "none"

    move-object/from16 v0, p0

    iget-object v4, v0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual {v4}, Lkellinwood/security/zipsigner/KeySet;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 686
    move-object/from16 v0, p0

    iget-object v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Lkellinwood/security/zipsigner/ProgressHelper;->setProgressTotalItems(I)V

    .line 687
    move-object/from16 v0, p0

    iget-object v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lkellinwood/security/zipsigner/ProgressHelper;->setProgressCurrentItem(I)V

    .line 688
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lkellinwood/security/zipsigner/ZipSigner;->copyFiles(Ljava/util/Map;Lkellinwood/zipio/ZipOutput;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 750
    invoke-virtual {v5}, Lkellinwood/zipio/ZipOutput;->close()V

    .line 751
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    if-eqz v2, :cond_3

    .line 753
    if-eqz p3, :cond_3

    :try_start_2
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 760
    :cond_3
    :goto_0
    return-void

    .line 755
    :catch_0
    move-exception v16

    .line 756
    .local v16, "t":Ljava/lang/Throwable;
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v19, ":"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    goto :goto_0

    .line 693
    .end local v16    # "t":Ljava/lang/Throwable;
    :cond_4
    const/4 v14, 0x0

    .line 694
    .local v14, "progressTotalItems":I
    :try_start_3
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lkellinwood/zipio/ZioEntry;

    .line 695
    .local v9, "entry":Lkellinwood/zipio/ZioEntry;
    invoke-virtual {v9}, Lkellinwood/zipio/ZioEntry;->getName()Ljava/lang/String;

    move-result-object v12

    .line 696
    .local v12, "name":Ljava/lang/String;
    invoke-virtual {v9}, Lkellinwood/zipio/ZioEntry;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "META-INF/MANIFEST.MF"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "META-INF/CERT.SF"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "META-INF/CERT.RSA"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    sget-object v2, Lkellinwood/security/zipsigner/ZipSigner;->stripPattern:Ljava/util/regex/Pattern;

    if-eqz v2, :cond_6

    sget-object v2, Lkellinwood/security/zipsigner/ZipSigner;->stripPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v12}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_5

    .line 701
    :cond_6
    add-int/lit8 v14, v14, 0x3

    goto :goto_1

    .line 704
    .end local v9    # "entry":Lkellinwood/zipio/ZioEntry;
    .end local v12    # "name":Ljava/lang/String;
    :cond_7
    add-int/lit8 v14, v14, 0x1

    .line 705
    move-object/from16 v0, p0

    iget-object v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-virtual {v2, v14}, Lkellinwood/security/zipsigner/ProgressHelper;->setProgressTotalItems(I)V

    .line 706
    move-object/from16 v0, p0

    iget-object v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lkellinwood/security/zipsigner/ProgressHelper;->setProgressCurrentItem(I)V

    .line 709
    move-object/from16 v0, p0

    iget-object v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual {v2}, Lkellinwood/security/zipsigner/KeySet;->getPublicKey()Ljava/security/cert/X509Certificate;

    move-result-object v2

    invoke-virtual {v2}, Ljava/security/cert/X509Certificate;->getNotBefore()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v19

    const-wide/32 v21, 0x36ee80

    add-long v6, v19, v21

    .line 713
    .local v6, "timestamp":J
    invoke-direct/range {p0 .. p1}, Lkellinwood/security/zipsigner/ZipSigner;->addDigestsToManifest(Ljava/util/Map;)Ljava/util/jar/Manifest;

    move-result-object v3

    .line 714
    .local v3, "manifest":Ljava/util/jar/Manifest;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v2, :cond_8

    .line 750
    invoke-virtual {v5}, Lkellinwood/zipio/ZipOutput;->close()V

    .line 751
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    if-eqz v2, :cond_3

    .line 753
    if-eqz p3, :cond_3

    :try_start_4
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 755
    :catch_1
    move-exception v16

    .line 756
    .restart local v16    # "t":Ljava/lang/Throwable;
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v19, ":"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 715
    .end local v16    # "t":Ljava/lang/Throwable;
    :cond_8
    :try_start_5
    new-instance v17, Lkellinwood/zipio/ZioEntry;

    const-string v2, "META-INF/MANIFEST.MF"

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Lkellinwood/zipio/ZioEntry;-><init>(Ljava/lang/String;)V

    .line 716
    .local v17, "ze":Lkellinwood/zipio/ZioEntry;
    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lkellinwood/zipio/ZioEntry;->setTime(J)V

    .line 717
    invoke-virtual/range {v17 .. v17}, Lkellinwood/zipio/ZioEntry;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/jar/Manifest;->write(Ljava/io/OutputStream;)V

    .line 718
    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lkellinwood/zipio/ZipOutput;->write(Lkellinwood/zipio/ZioEntry;)V

    .line 722
    new-instance v17, Lkellinwood/zipio/ZioEntry;

    .end local v17    # "ze":Lkellinwood/zipio/ZioEntry;
    const-string v2, "META-INF/CERT.SF"

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Lkellinwood/zipio/ZioEntry;-><init>(Ljava/lang/String;)V

    .line 723
    .restart local v17    # "ze":Lkellinwood/zipio/ZioEntry;
    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lkellinwood/zipio/ZioEntry;->setTime(J)V

    .line 725
    new-instance v13, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v13}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 726
    .local v13, "out":Ljava/io/ByteArrayOutputStream;
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v13}, Lkellinwood/security/zipsigner/ZipSigner;->generateSignatureFile(Ljava/util/jar/Manifest;Ljava/io/OutputStream;)V

    .line 727
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v2, :cond_9

    .line 750
    invoke-virtual {v5}, Lkellinwood/zipio/ZipOutput;->close()V

    .line 751
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    if-eqz v2, :cond_3

    .line 753
    if-eqz p3, :cond_3

    :try_start_6
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_0

    .line 755
    :catch_2
    move-exception v16

    .line 756
    .restart local v16    # "t":Ljava/lang/Throwable;
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v19, ":"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 728
    .end local v16    # "t":Ljava/lang/Throwable;
    :cond_9
    :try_start_7
    invoke-virtual {v13}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v15

    .line 729
    .local v15, "sfBytes":[B
    if-eqz v8, :cond_a

    .line 730
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Signature File: \n"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v19, Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v0, v15}, Ljava/lang/String;-><init>([B)V

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v19, "\n"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v15}, Lkellinwood/security/zipsigner/HexDumpEncoder;->encode([B)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 733
    :cond_a
    invoke-virtual/range {v17 .. v17}, Lkellinwood/zipio/ZioEntry;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/io/OutputStream;->write([B)V

    .line 734
    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lkellinwood/zipio/ZipOutput;->write(Lkellinwood/zipio/ZioEntry;)V

    .line 737
    move-object/from16 v0, p0

    iget-object v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lkellinwood/security/zipsigner/ZipSigner;->resourceAdapter:Lkellinwood/security/zipsigner/ResourceAdapter;

    move-object/from16 v19, v0

    sget-object v20, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->GENERATING_SIGNATURE_BLOCK:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    invoke-interface/range {v19 .. v21}, Lkellinwood/security/zipsigner/ResourceAdapter;->getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v2, v4, v0}, Lkellinwood/security/zipsigner/ProgressHelper;->progress(ILjava/lang/String;)V

    .line 738
    new-instance v17, Lkellinwood/zipio/ZioEntry;

    .end local v17    # "ze":Lkellinwood/zipio/ZioEntry;
    const-string v2, "META-INF/CERT.RSA"

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Lkellinwood/zipio/ZioEntry;-><init>(Ljava/lang/String;)V

    .line 739
    .restart local v17    # "ze":Lkellinwood/zipio/ZioEntry;
    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lkellinwood/zipio/ZioEntry;->setTime(J)V

    .line 740
    move-object/from16 v0, p0

    iget-object v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual/range {v17 .. v17}, Lkellinwood/zipio/ZioEntry;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v15, v4}, Lkellinwood/security/zipsigner/ZipSigner;->writeSignatureBlock(Lkellinwood/security/zipsigner/KeySet;[BLjava/io/OutputStream;)V

    .line 741
    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lkellinwood/zipio/ZipOutput;->write(Lkellinwood/zipio/ZioEntry;)V

    .line 742
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v2, :cond_b

    .line 750
    invoke-virtual {v5}, Lkellinwood/zipio/ZipOutput;->close()V

    .line 751
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    if-eqz v2, :cond_3

    .line 753
    if-eqz p3, :cond_3

    :try_start_8
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_0

    .line 755
    :catch_3
    move-exception v16

    .line 756
    .restart local v16    # "t":Ljava/lang/Throwable;
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v19, ":"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v16    # "t":Ljava/lang/Throwable;
    :cond_b
    move-object/from16 v2, p0

    move-object/from16 v4, p1

    .line 745
    :try_start_9
    invoke-direct/range {v2 .. v7}, Lkellinwood/security/zipsigner/ZipSigner;->copyFiles(Ljava/util/jar/Manifest;Ljava/util/Map;Lkellinwood/zipio/ZipOutput;J)V

    .line 746
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    if-eqz v2, :cond_c

    .line 750
    invoke-virtual {v5}, Lkellinwood/zipio/ZipOutput;->close()V

    .line 751
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    if-eqz v2, :cond_3

    .line 753
    if-eqz p3, :cond_3

    :try_start_a
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_4

    goto/16 :goto_0

    .line 755
    :catch_4
    move-exception v16

    .line 756
    .restart local v16    # "t":Ljava/lang/Throwable;
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v19, ":"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 750
    .end local v16    # "t":Ljava/lang/Throwable;
    :cond_c
    invoke-virtual {v5}, Lkellinwood/zipio/ZipOutput;->close()V

    .line 751
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    if-eqz v2, :cond_3

    .line 753
    if-eqz p3, :cond_3

    :try_start_b
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_5

    goto/16 :goto_0

    .line 755
    :catch_5
    move-exception v16

    .line 756
    .restart local v16    # "t":Ljava/lang/Throwable;
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v19, ":"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 750
    .end local v3    # "manifest":Ljava/util/jar/Manifest;
    .end local v5    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    .end local v6    # "timestamp":J
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v13    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v14    # "progressTotalItems":I
    .end local v15    # "sfBytes":[B
    .end local v16    # "t":Ljava/lang/Throwable;
    .end local v17    # "ze":Lkellinwood/zipio/ZioEntry;
    .restart local v18    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    :catchall_0
    move-exception v2

    move-object/from16 v5, v18

    .end local v18    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    .restart local v5    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    :goto_2
    invoke-virtual {v5}, Lkellinwood/zipio/ZipOutput;->close()V

    .line 751
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lkellinwood/security/zipsigner/ZipSigner;->canceled:Z

    if-eqz v4, :cond_d

    .line 753
    if-eqz p3, :cond_d

    :try_start_c
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_6

    .line 757
    :cond_d
    :goto_3
    throw v2

    .line 755
    :catch_6
    move-exception v16

    .line 756
    .restart local v16    # "t":Ljava/lang/Throwable;
    invoke-static {}, Lkellinwood/security/zipsigner/ZipSigner;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ":"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-interface {v4, v0}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    goto :goto_3

    .line 750
    .end local v16    # "t":Ljava/lang/Throwable;
    :catchall_1
    move-exception v2

    goto :goto_2
.end method

.method public signZip(Ljava/util/Map;Ljava/lang/String;)V
    .locals 1
    .param p2, "outputZipFilename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/zipio/ZioEntry;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 623
    .local p1, "zioEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    iget-object v0, p0, Lkellinwood/security/zipsigner/ZipSigner;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-virtual {v0}, Lkellinwood/security/zipsigner/ProgressHelper;->initProgress()V

    .line 624
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0, p2}, Lkellinwood/security/zipsigner/ZipSigner;->signZip(Ljava/util/Map;Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 625
    return-void
.end method
