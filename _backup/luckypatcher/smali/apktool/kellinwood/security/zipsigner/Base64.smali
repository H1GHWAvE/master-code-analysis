.class public Lkellinwood/security/zipsigner/Base64;
.super Ljava/lang/Object;
.source "Base64.java"


# static fields
.field static aDecodeMethod:Ljava/lang/reflect/Method;

.field static aEncodeMethod:Ljava/lang/reflect/Method;

.field static bDecodeMethod:Ljava/lang/reflect/Method;

.field static bDecoder:Ljava/lang/Object;

.field static bEncodeMethod:Ljava/lang/reflect/Method;

.field static bEncoder:Ljava/lang/Object;

.field static logger:Lkellinwood/logging/LoggerInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 43
    sput-object v2, Lkellinwood/security/zipsigner/Base64;->aEncodeMethod:Ljava/lang/reflect/Method;

    .line 44
    sput-object v2, Lkellinwood/security/zipsigner/Base64;->aDecodeMethod:Ljava/lang/reflect/Method;

    .line 46
    sput-object v2, Lkellinwood/security/zipsigner/Base64;->bEncoder:Ljava/lang/Object;

    .line 47
    sput-object v2, Lkellinwood/security/zipsigner/Base64;->bEncodeMethod:Ljava/lang/reflect/Method;

    .line 49
    sput-object v2, Lkellinwood/security/zipsigner/Base64;->bDecoder:Ljava/lang/Object;

    .line 50
    sput-object v2, Lkellinwood/security/zipsigner/Base64;->bDecodeMethod:Ljava/lang/reflect/Method;

    .line 52
    sput-object v2, Lkellinwood/security/zipsigner/Base64;->logger:Lkellinwood/logging/LoggerInterface;

    .line 58
    const-class v2, Lkellinwood/security/zipsigner/Base64;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lkellinwood/logging/LoggerManager;->getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;

    move-result-object v2

    sput-object v2, Lkellinwood/security/zipsigner/Base64;->logger:Lkellinwood/logging/LoggerInterface;

    .line 61
    :try_start_0
    const-string v2, "android.util.Base64"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 63
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<Ljava/lang/Object;>;"
    const-string v2, "encode"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, [B

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lkellinwood/security/zipsigner/Base64;->aEncodeMethod:Ljava/lang/reflect/Method;

    .line 64
    const-string v2, "decode"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, [B

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lkellinwood/security/zipsigner/Base64;->aDecodeMethod:Ljava/lang/reflect/Method;

    .line 65
    sget-object v2, Lkellinwood/security/zipsigner/Base64;->logger:Lkellinwood/logging/LoggerInterface;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is available."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lkellinwood/logging/LoggerInterface;->info(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :goto_0
    :try_start_1
    const-string v2, "org.bouncycastle.util.encoders.Base64Encoder"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    sput-object v2, Lkellinwood/security/zipsigner/Base64;->bEncoder:Ljava/lang/Object;

    .line 76
    const-string v2, "encode"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, [B

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-class v5, Ljava/io/OutputStream;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lkellinwood/security/zipsigner/Base64;->bEncodeMethod:Ljava/lang/reflect/Method;

    .line 77
    sget-object v2, Lkellinwood/security/zipsigner/Base64;->logger:Lkellinwood/logging/LoggerInterface;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is available."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lkellinwood/logging/LoggerInterface;->info(Ljava/lang/String;)V

    .line 79
    const-string v2, "decode"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, [B

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-class v5, Ljava/io/OutputStream;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lkellinwood/security/zipsigner/Base64;->bDecodeMethod:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 87
    :goto_1
    sget-object v2, Lkellinwood/security/zipsigner/Base64;->aEncodeMethod:Ljava/lang/reflect/Method;

    if-nez v2, :cond_0

    sget-object v2, Lkellinwood/security/zipsigner/Base64;->bEncodeMethod:Ljava/lang/reflect/Method;

    if-nez v2, :cond_0

    .line 88
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "No base64 encoder implementation is available."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 68
    :catch_0
    move-exception v1

    .line 69
    .local v1, "x":Ljava/lang/Exception;
    sget-object v2, Lkellinwood/security/zipsigner/Base64;->logger:Lkellinwood/logging/LoggerInterface;

    const-string v3, "Failed to initialize use of android.util.Base64"

    invoke-interface {v2, v3, v1}, Lkellinwood/logging/LoggerInterface;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 83
    .end local v1    # "x":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 84
    .restart local v1    # "x":Ljava/lang/Exception;
    sget-object v2, Lkellinwood/security/zipsigner/Base64;->logger:Lkellinwood/logging/LoggerInterface;

    const-string v3, "Failed to initialize use of org.bouncycastle.util.encoders.Base64Encoder"

    invoke-interface {v2, v3, v1}, Lkellinwood/logging/LoggerInterface;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 89
    .end local v1    # "x":Ljava/lang/Exception;
    :cond_0
    return-void

    .line 82
    :catch_2
    move-exception v2

    goto :goto_1

    .line 67
    :catch_3
    move-exception v2

    goto/16 :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decode([B)[B
    .locals 9
    .param p0, "data"    # [B

    .prologue
    .line 115
    :try_start_0
    sget-object v4, Lkellinwood/security/zipsigner/Base64;->aDecodeMethod:Ljava/lang/reflect/Method;

    if-eqz v4, :cond_0

    .line 117
    sget-object v4, Lkellinwood/security/zipsigner/Base64;->aDecodeMethod:Ljava/lang/reflect/Method;

    const/4 v5, 0x0

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p0, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    move-object v0, v4

    check-cast v0, [B

    move-object v2, v0

    .line 123
    :goto_0
    return-object v2

    .line 120
    :cond_0
    sget-object v4, Lkellinwood/security/zipsigner/Base64;->bDecodeMethod:Ljava/lang/reflect/Method;

    if-eqz v4, :cond_1

    .line 121
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 122
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    sget-object v4, Lkellinwood/security/zipsigner/Base64;->bDecodeMethod:Ljava/lang/reflect/Method;

    sget-object v5, Lkellinwood/security/zipsigner/Base64;->bEncoder:Ljava/lang/Object;

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p0, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    array-length v8, p0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object v1, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 126
    .end local v1    # "baos":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v3

    .line 127
    .local v3, "x":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 131
    .end local v3    # "x":Ljava/lang/Exception;
    :cond_1
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "No base64 encoder implementation is available."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static encode([B)Ljava/lang/String;
    .locals 9
    .param p0, "data"    # [B

    .prologue
    .line 94
    :try_start_0
    sget-object v4, Lkellinwood/security/zipsigner/Base64;->aEncodeMethod:Ljava/lang/reflect/Method;

    if-eqz v4, :cond_0

    .line 96
    sget-object v4, Lkellinwood/security/zipsigner/Base64;->aEncodeMethod:Ljava/lang/reflect/Method;

    const/4 v5, 0x0

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p0, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    move-object v0, v4

    check-cast v0, [B

    move-object v2, v0

    .line 97
    .local v2, "encodedBytes":[B
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v2}, Ljava/lang/String;-><init>([B)V

    .line 102
    .end local v2    # "encodedBytes":[B
    :goto_0
    return-object v4

    .line 99
    :cond_0
    sget-object v4, Lkellinwood/security/zipsigner/Base64;->bEncodeMethod:Ljava/lang/reflect/Method;

    if-eqz v4, :cond_1

    .line 100
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 101
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    sget-object v4, Lkellinwood/security/zipsigner/Base64;->bEncodeMethod:Ljava/lang/reflect/Method;

    sget-object v5, Lkellinwood/security/zipsigner/Base64;->bEncoder:Ljava/lang/Object;

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p0, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    array-length v8, p0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object v1, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    new-instance v4, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 105
    .end local v1    # "baos":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v3

    .line 106
    .local v3, "x":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 110
    .end local v3    # "x":Ljava/lang/Exception;
    :cond_1
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "No base64 encoder implementation is available."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
.end method
