.class public Lkellinwood/zipio/ZioEntryInputStream;
.super Ljava/io/InputStream;
.source "ZioEntryInputStream.java"


# instance fields
.field debug:Z

.field log:Lkellinwood/logging/LoggerInterface;

.field monitor:Ljava/io/OutputStream;

.field offset:I

.field raf:Ljava/io/RandomAccessFile;

.field returnDummyByte:Z

.field size:I


# direct methods
.method public constructor <init>(Lkellinwood/zipio/ZioEntry;)V
    .locals 8
    .param p1, "entry"    # Lkellinwood/zipio/ZioEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 38
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 35
    iput-boolean v7, p0, Lkellinwood/zipio/ZioEntryInputStream;->returnDummyByte:Z

    .line 36
    const/4 v2, 0x0

    iput-object v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->monitor:Ljava/io/OutputStream;

    .line 40
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lkellinwood/logging/LoggerManager;->getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;

    move-result-object v2

    iput-object v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->log:Lkellinwood/logging/LoggerInterface;

    .line 41
    iget-object v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->log:Lkellinwood/logging/LoggerInterface;

    invoke-interface {v2}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v2

    iput-boolean v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->debug:Z

    .line 42
    iput v7, p0, Lkellinwood/zipio/ZioEntryInputStream;->offset:I

    .line 43
    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getCompressedSize()I

    move-result v2

    iput v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->size:I

    .line 44
    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getZipInput()Lkellinwood/zipio/ZipInput;

    move-result-object v2

    iget-object v2, v2, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    iput-object v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->raf:Ljava/io/RandomAccessFile;

    .line 45
    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getDataPosition()J

    move-result-wide v0

    .line 46
    .local v0, "dpos":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_1

    .line 47
    iget-boolean v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->debug:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->log:Lkellinwood/logging/LoggerInterface;

    const-string v3, "Seeking to %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getDataPosition()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 48
    :cond_0
    iget-object v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->raf:Ljava/io/RandomAccessFile;

    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getDataPosition()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 56
    :goto_0
    return-void

    .line 53
    :cond_1
    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->readLocalHeader()V

    goto :goto_0
.end method

.method private readBytes([BII)I
    .locals 8
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 109
    iget v3, p0, Lkellinwood/zipio/ZioEntryInputStream;->size:I

    iget v4, p0, Lkellinwood/zipio/ZioEntryInputStream;->offset:I

    sub-int/2addr v3, v4

    if-nez v3, :cond_2

    .line 110
    iget-boolean v3, p0, Lkellinwood/zipio/ZioEntryInputStream;->returnDummyByte:Z

    if-eqz v3, :cond_1

    .line 111
    iput-boolean v7, p0, Lkellinwood/zipio/ZioEntryInputStream;->returnDummyByte:Z

    .line 112
    aput-byte v7, p1, p2

    move v0, v2

    .line 124
    :cond_0
    :goto_0
    return v0

    .line 115
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 117
    :cond_2
    invoke-virtual {p0}, Lkellinwood/zipio/ZioEntryInputStream;->available()I

    move-result v3

    invoke-static {p3, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 118
    .local v1, "numToRead":I
    iget-object v3, p0, Lkellinwood/zipio/ZioEntryInputStream;->raf:Ljava/io/RandomAccessFile;

    invoke-virtual {v3, p1, p2, v1}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v0

    .line 119
    .local v0, "numRead":I
    if-lez v0, :cond_4

    .line 120
    iget-object v3, p0, Lkellinwood/zipio/ZioEntryInputStream;->monitor:Ljava/io/OutputStream;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lkellinwood/zipio/ZioEntryInputStream;->monitor:Ljava/io/OutputStream;

    invoke-virtual {v3, p1, p2, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 121
    :cond_3
    iget v3, p0, Lkellinwood/zipio/ZioEntryInputStream;->offset:I

    add-int/2addr v3, v0

    iput v3, p0, Lkellinwood/zipio/ZioEntryInputStream;->offset:I

    .line 123
    :cond_4
    iget-boolean v3, p0, Lkellinwood/zipio/ZioEntryInputStream;->debug:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lkellinwood/zipio/ZioEntryInputStream;->log:Lkellinwood/logging/LoggerInterface;

    const-string v4, "Read %d bytes for read(b,%d,%d)"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public available()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 78
    iget v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->size:I

    iget v3, p0, Lkellinwood/zipio/ZioEntryInputStream;->offset:I

    sub-int v0, v2, v3

    .line 79
    .local v0, "available":I
    iget-boolean v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->debug:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->log:Lkellinwood/logging/LoggerInterface;

    const-string v3, "Available = %d"

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 80
    :cond_0
    if-nez v0, :cond_1

    iget-boolean v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->returnDummyByte:Z

    if-eqz v2, :cond_1

    move v0, v1

    .line 81
    .end local v0    # "available":I
    :cond_1
    return v0
.end method

.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 86
    iget v1, p0, Lkellinwood/zipio/ZioEntryInputStream;->size:I

    iget v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->offset:I

    sub-int/2addr v1, v2

    if-nez v1, :cond_2

    .line 87
    iget-boolean v1, p0, Lkellinwood/zipio/ZioEntryInputStream;->returnDummyByte:Z

    if-eqz v1, :cond_1

    .line 88
    iput-boolean v0, p0, Lkellinwood/zipio/ZioEntryInputStream;->returnDummyByte:Z

    .line 100
    :cond_0
    :goto_0
    return v0

    .line 91
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 93
    :cond_2
    iget-object v1, p0, Lkellinwood/zipio/ZioEntryInputStream;->raf:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->read()I

    move-result v0

    .line 94
    .local v0, "b":I
    if-ltz v0, :cond_5

    .line 95
    iget-object v1, p0, Lkellinwood/zipio/ZioEntryInputStream;->monitor:Ljava/io/OutputStream;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lkellinwood/zipio/ZioEntryInputStream;->monitor:Ljava/io/OutputStream;

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 96
    :cond_3
    iget-boolean v1, p0, Lkellinwood/zipio/ZioEntryInputStream;->debug:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lkellinwood/zipio/ZioEntryInputStream;->log:Lkellinwood/logging/LoggerInterface;

    const-string v2, "Read 1 byte"

    invoke-interface {v1, v2}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 97
    :cond_4
    iget v1, p0, Lkellinwood/zipio/ZioEntryInputStream;->offset:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lkellinwood/zipio/ZioEntryInputStream;->offset:I

    goto :goto_0

    .line 99
    :cond_5
    iget-boolean v1, p0, Lkellinwood/zipio/ZioEntryInputStream;->debug:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lkellinwood/zipio/ZioEntryInputStream;->log:Lkellinwood/logging/LoggerInterface;

    const-string v2, "Read 0 bytes"

    invoke-interface {v1, v2}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public read([B)I
    .locals 2
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lkellinwood/zipio/ZioEntryInputStream;->readBytes([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 1
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0, p1, p2, p3}, Lkellinwood/zipio/ZioEntryInputStream;->readBytes([BII)I

    move-result v0

    return v0
.end method

.method public setMonitorStream(Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "monitorStream"    # Ljava/io/OutputStream;

    .prologue
    .line 64
    iput-object p1, p0, Lkellinwood/zipio/ZioEntryInputStream;->monitor:Ljava/io/OutputStream;

    .line 65
    return-void
.end method

.method public setReturnDummyByte(Z)V
    .locals 0
    .param p1, "returnExtraByte"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lkellinwood/zipio/ZioEntryInputStream;->returnDummyByte:Z

    .line 60
    return-void
.end method

.method public skip(J)J
    .locals 7
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    invoke-virtual {p0}, Lkellinwood/zipio/ZioEntryInputStream;->available()I

    move-result v2

    int-to-long v2, v2

    invoke-static {p1, p2, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 135
    .local v0, "numToSkip":J
    iget-object v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->raf:Ljava/io/RandomAccessFile;

    iget-object v3, p0, Lkellinwood/zipio/ZioEntryInputStream;->raf:Ljava/io/RandomAccessFile;

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v3

    add-long/2addr v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 136
    iget-boolean v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->debug:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lkellinwood/zipio/ZioEntryInputStream;->log:Lkellinwood/logging/LoggerInterface;

    const-string v3, "Skipped %d bytes"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 137
    :cond_0
    return-wide v0
.end method
