.class public Lkellinwood/zipio/ZioEntry;
.super Ljava/lang/Object;
.source "ZioEntry.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static alignBytes:[B

.field private static log:Lkellinwood/logging/LoggerInterface;


# instance fields
.field private compressedSize:I

.field private compression:S

.field private crc32:I

.field private data:[B

.field private dataPosition:J

.field private diskNumberStart:S

.field private entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

.field private externalAttributes:I

.field private extraData:[B

.field private fileComment:Ljava/lang/String;

.field private filename:Ljava/lang/String;

.field private generalPurposeBits:S

.field private internalAttributes:S

.field private localHeaderOffset:I

.field private modificationDate:S

.field private modificationTime:S

.field private numAlignBytes:S

.field private size:I

.field private versionMadeBy:S

.field private versionRequired:S

.field private zipInput:Lkellinwood/zipio/ZipInput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x4

    new-array v0, v0, [B

    sput-object v0, Lkellinwood/zipio/ZioEntry;->alignBytes:[B

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-short v2, p0, Lkellinwood/zipio/ZioEntry;->numAlignBytes:S

    .line 56
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lkellinwood/zipio/ZioEntry;->dataPosition:J

    .line 57
    iput-object v3, p0, Lkellinwood/zipio/ZioEntry;->data:[B

    .line 58
    iput-object v3, p0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    .line 75
    iput-object p1, p0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    .line 76
    const-string v0, ""

    iput-object v0, p0, Lkellinwood/zipio/ZioEntry;->fileComment:Ljava/lang/String;

    .line 77
    const/16 v0, 0x8

    iput-short v0, p0, Lkellinwood/zipio/ZioEntry;->compression:S

    .line 78
    new-array v0, v2, [B

    iput-object v0, p0, Lkellinwood/zipio/ZioEntry;->extraData:[B

    .line 79
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lkellinwood/zipio/ZioEntry;->setTime(J)V

    .line 80
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "sourceDataFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-short v9, p0, Lkellinwood/zipio/ZioEntry;->numAlignBytes:S

    .line 56
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lkellinwood/zipio/ZioEntry;->dataPosition:J

    .line 57
    iput-object v6, p0, Lkellinwood/zipio/ZioEntry;->data:[B

    .line 58
    iput-object v6, p0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    .line 86
    new-instance v4, Lkellinwood/zipio/ZipInput;

    invoke-direct {v4, p2}, Lkellinwood/zipio/ZipInput;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lkellinwood/zipio/ZioEntry;->zipInput:Lkellinwood/zipio/ZipInput;

    .line 87
    iput-object p1, p0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    .line 88
    const-string v4, ""

    iput-object v4, p0, Lkellinwood/zipio/ZioEntry;->fileComment:Ljava/lang/String;

    .line 89
    iput-short v9, p0, Lkellinwood/zipio/ZioEntry;->compression:S

    .line 90
    iget-object v4, p0, Lkellinwood/zipio/ZioEntry;->zipInput:Lkellinwood/zipio/ZipInput;

    invoke-virtual {v4}, Lkellinwood/zipio/ZipInput;->getFileLength()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, p0, Lkellinwood/zipio/ZioEntry;->size:I

    .line 91
    iget v4, p0, Lkellinwood/zipio/ZioEntry;->size:I

    iput v4, p0, Lkellinwood/zipio/ZioEntry;->compressedSize:I

    .line 93
    invoke-static {}, Lkellinwood/zipio/ZioEntry;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    invoke-interface {v4}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 94
    invoke-static {}, Lkellinwood/zipio/ZioEntry;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    const-string v5, "Computing CRC for %s, size=%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p2, v6, v9

    const/4 v7, 0x1

    iget v8, p0, Lkellinwood/zipio/ZioEntry;->size:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 97
    :cond_0
    new-instance v2, Ljava/util/zip/CRC32;

    invoke-direct {v2}, Ljava/util/zip/CRC32;-><init>()V

    .line 99
    .local v2, "crc":Ljava/util/zip/CRC32;
    const/16 v4, 0x1fa0

    new-array v0, v4, [B

    .line 101
    .local v0, "buffer":[B
    const/4 v3, 0x0

    .line 102
    .local v3, "numRead":I
    :cond_1
    :goto_0
    iget v4, p0, Lkellinwood/zipio/ZioEntry;->size:I

    if-eq v3, v4, :cond_2

    .line 103
    iget-object v4, p0, Lkellinwood/zipio/ZioEntry;->zipInput:Lkellinwood/zipio/ZipInput;

    array-length v5, v0

    iget v6, p0, Lkellinwood/zipio/ZioEntry;->size:I

    sub-int/2addr v6, v3

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-virtual {v4, v0, v9, v5}, Lkellinwood/zipio/ZipInput;->read([BII)I

    move-result v1

    .line 104
    .local v1, "count":I
    if-lez v1, :cond_1

    .line 105
    invoke-virtual {v2, v0, v9, v1}, Ljava/util/zip/CRC32;->update([BII)V

    .line 106
    add-int/2addr v3, v1

    goto :goto_0

    .line 110
    .end local v1    # "count":I
    :cond_2
    invoke-virtual {v2}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, p0, Lkellinwood/zipio/ZioEntry;->crc32:I

    .line 112
    iget-object v4, p0, Lkellinwood/zipio/ZioEntry;->zipInput:Lkellinwood/zipio/ZipInput;

    invoke-virtual {v4, v10, v11}, Lkellinwood/zipio/ZipInput;->seek(J)V

    .line 113
    iput-wide v10, p0, Lkellinwood/zipio/ZioEntry;->dataPosition:J

    .line 114
    new-array v4, v9, [B

    iput-object v4, p0, Lkellinwood/zipio/ZioEntry;->extraData:[B

    .line 115
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lkellinwood/zipio/ZioEntry;->setTime(J)V

    .line 116
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;SIII)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "sourceDataFile"    # Ljava/lang/String;
    .param p3, "compression"    # S
    .param p4, "crc32"    # I
    .param p5, "compressedSize"    # I
    .param p6, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-short v2, p0, Lkellinwood/zipio/ZioEntry;->numAlignBytes:S

    .line 56
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lkellinwood/zipio/ZioEntry;->dataPosition:J

    .line 57
    iput-object v3, p0, Lkellinwood/zipio/ZioEntry;->data:[B

    .line 58
    iput-object v3, p0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    .line 123
    new-instance v0, Lkellinwood/zipio/ZipInput;

    invoke-direct {v0, p2}, Lkellinwood/zipio/ZipInput;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lkellinwood/zipio/ZioEntry;->zipInput:Lkellinwood/zipio/ZipInput;

    .line 124
    iput-object p1, p0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    .line 125
    const-string v0, ""

    iput-object v0, p0, Lkellinwood/zipio/ZioEntry;->fileComment:Ljava/lang/String;

    .line 126
    iput-short p3, p0, Lkellinwood/zipio/ZioEntry;->compression:S

    .line 127
    iput p4, p0, Lkellinwood/zipio/ZioEntry;->crc32:I

    .line 128
    iput p5, p0, Lkellinwood/zipio/ZioEntry;->compressedSize:I

    .line 129
    iput p6, p0, Lkellinwood/zipio/ZioEntry;->size:I

    .line 130
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lkellinwood/zipio/ZioEntry;->dataPosition:J

    .line 131
    new-array v0, v2, [B

    iput-object v0, p0, Lkellinwood/zipio/ZioEntry;->extraData:[B

    .line 132
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lkellinwood/zipio/ZioEntry;->setTime(J)V

    .line 133
    return-void
.end method

.method public constructor <init>(Lkellinwood/zipio/ZipInput;)V
    .locals 3
    .param p1, "input"    # Lkellinwood/zipio/ZipInput;

    .prologue
    const/4 v2, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-short v0, p0, Lkellinwood/zipio/ZioEntry;->numAlignBytes:S

    .line 56
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lkellinwood/zipio/ZioEntry;->dataPosition:J

    .line 57
    iput-object v2, p0, Lkellinwood/zipio/ZioEntry;->data:[B

    .line 58
    iput-object v2, p0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    .line 66
    iput-object p1, p0, Lkellinwood/zipio/ZioEntry;->zipInput:Lkellinwood/zipio/ZipInput;

    .line 67
    return-void
.end method

.method private doRead(Lkellinwood/zipio/ZipInput;)V
    .locals 10
    .param p1, "input"    # Lkellinwood/zipio/ZipInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 340
    invoke-static {}, Lkellinwood/zipio/ZioEntry;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    invoke-interface {v4}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v0

    .line 343
    .local v0, "debug":Z
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v4

    iput-short v4, p0, Lkellinwood/zipio/ZioEntry;->versionMadeBy:S

    .line 344
    if-eqz v0, :cond_0

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "Version made by: 0x%04x"

    new-array v6, v8, [Ljava/lang/Object;

    iget-short v7, p0, Lkellinwood/zipio/ZioEntry;->versionMadeBy:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 347
    :cond_0
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v4

    iput-short v4, p0, Lkellinwood/zipio/ZioEntry;->versionRequired:S

    .line 348
    if-eqz v0, :cond_1

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "Version required: 0x%04x"

    new-array v6, v8, [Ljava/lang/Object;

    iget-short v7, p0, Lkellinwood/zipio/ZioEntry;->versionRequired:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 351
    :cond_1
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v4

    iput-short v4, p0, Lkellinwood/zipio/ZioEntry;->generalPurposeBits:S

    .line 352
    if-eqz v0, :cond_2

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "General purpose bits: 0x%04x"

    new-array v6, v8, [Ljava/lang/Object;

    iget-short v7, p0, Lkellinwood/zipio/ZioEntry;->generalPurposeBits:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 354
    :cond_2
    iget-short v4, p0, Lkellinwood/zipio/ZioEntry;->generalPurposeBits:S

    const v5, 0xf7f1

    and-int/2addr v4, v5

    if-eqz v4, :cond_3

    .line 355
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Can\'t handle general purpose bits == "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "0x%04x"

    new-array v7, v8, [Ljava/lang/Object;

    iget-short v8, p0, Lkellinwood/zipio/ZioEntry;->generalPurposeBits:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 359
    :cond_3
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v4

    iput-short v4, p0, Lkellinwood/zipio/ZioEntry;->compression:S

    .line 360
    if-eqz v0, :cond_4

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "Compression: 0x%04x"

    new-array v6, v8, [Ljava/lang/Object;

    iget-short v7, p0, Lkellinwood/zipio/ZioEntry;->compression:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 363
    :cond_4
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v4

    iput-short v4, p0, Lkellinwood/zipio/ZioEntry;->modificationTime:S

    .line 364
    if-eqz v0, :cond_5

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "Modification time: 0x%04x"

    new-array v6, v8, [Ljava/lang/Object;

    iget-short v7, p0, Lkellinwood/zipio/ZioEntry;->modificationTime:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 367
    :cond_5
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v4

    iput-short v4, p0, Lkellinwood/zipio/ZioEntry;->modificationDate:S

    .line 368
    if-eqz v0, :cond_6

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "Modification date: 0x%04x"

    new-array v6, v8, [Ljava/lang/Object;

    iget-short v7, p0, Lkellinwood/zipio/ZioEntry;->modificationDate:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 371
    :cond_6
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readInt()I

    move-result v4

    iput v4, p0, Lkellinwood/zipio/ZioEntry;->crc32:I

    .line 372
    if-eqz v0, :cond_7

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "CRC-32: 0x%04x"

    new-array v6, v8, [Ljava/lang/Object;

    iget v7, p0, Lkellinwood/zipio/ZioEntry;->crc32:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 375
    :cond_7
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readInt()I

    move-result v4

    iput v4, p0, Lkellinwood/zipio/ZioEntry;->compressedSize:I

    .line 376
    if-eqz v0, :cond_8

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "Compressed size: 0x%04x"

    new-array v6, v8, [Ljava/lang/Object;

    iget v7, p0, Lkellinwood/zipio/ZioEntry;->compressedSize:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 379
    :cond_8
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readInt()I

    move-result v4

    iput v4, p0, Lkellinwood/zipio/ZioEntry;->size:I

    .line 380
    if-eqz v0, :cond_9

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "Size: 0x%04x"

    new-array v6, v8, [Ljava/lang/Object;

    iget v7, p0, Lkellinwood/zipio/ZioEntry;->size:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 383
    :cond_9
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v3

    .line 384
    .local v3, "fileNameLen":S
    if-eqz v0, :cond_a

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "File name length: 0x%04x"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 387
    :cond_a
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v1

    .line 388
    .local v1, "extraLen":S
    if-eqz v0, :cond_b

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "Extra length: 0x%04x"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 390
    :cond_b
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v2

    .line 391
    .local v2, "fileCommentLen":S
    if-eqz v0, :cond_c

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "File comment length: 0x%04x"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 393
    :cond_c
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v4

    iput-short v4, p0, Lkellinwood/zipio/ZioEntry;->diskNumberStart:S

    .line 394
    if-eqz v0, :cond_d

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "Disk number start: 0x%04x"

    new-array v6, v8, [Ljava/lang/Object;

    iget-short v7, p0, Lkellinwood/zipio/ZioEntry;->diskNumberStart:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 396
    :cond_d
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v4

    iput-short v4, p0, Lkellinwood/zipio/ZioEntry;->internalAttributes:S

    .line 397
    if-eqz v0, :cond_e

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "Internal attributes: 0x%04x"

    new-array v6, v8, [Ljava/lang/Object;

    iget-short v7, p0, Lkellinwood/zipio/ZioEntry;->internalAttributes:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 399
    :cond_e
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readInt()I

    move-result v4

    iput v4, p0, Lkellinwood/zipio/ZioEntry;->externalAttributes:I

    .line 400
    if-eqz v0, :cond_f

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "External attributes: 0x%08x"

    new-array v6, v8, [Ljava/lang/Object;

    iget v7, p0, Lkellinwood/zipio/ZioEntry;->externalAttributes:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 402
    :cond_f
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readInt()I

    move-result v4

    iput v4, p0, Lkellinwood/zipio/ZioEntry;->localHeaderOffset:I

    .line 403
    if-eqz v0, :cond_10

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v5, "Local header offset: 0x%08x"

    new-array v6, v8, [Ljava/lang/Object;

    iget v7, p0, Lkellinwood/zipio/ZioEntry;->localHeaderOffset:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 406
    :cond_10
    invoke-virtual {p1, v3}, Lkellinwood/zipio/ZipInput;->readString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    .line 407
    if-eqz v0, :cond_11

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Filename: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 409
    :cond_11
    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipInput;->readBytes(I)[B

    move-result-object v4

    iput-object v4, p0, Lkellinwood/zipio/ZioEntry;->extraData:[B

    .line 411
    invoke-virtual {p1, v2}, Lkellinwood/zipio/ZipInput;->readString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lkellinwood/zipio/ZioEntry;->fileComment:Ljava/lang/String;

    .line 412
    if-eqz v0, :cond_12

    sget-object v4, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File comment: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lkellinwood/zipio/ZioEntry;->fileComment:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 414
    :cond_12
    iget-short v4, p0, Lkellinwood/zipio/ZioEntry;->generalPurposeBits:S

    and-int/lit16 v4, v4, 0x800

    int-to-short v4, v4

    iput-short v4, p0, Lkellinwood/zipio/ZioEntry;->generalPurposeBits:S

    .line 417
    iget v4, p0, Lkellinwood/zipio/ZioEntry;->size:I

    if-nez v4, :cond_13

    .line 418
    iput v9, p0, Lkellinwood/zipio/ZioEntry;->compressedSize:I

    .line 419
    iput-short v9, p0, Lkellinwood/zipio/ZioEntry;->compression:S

    .line 420
    iput v9, p0, Lkellinwood/zipio/ZioEntry;->crc32:I

    .line 423
    :cond_13
    return-void
.end method

.method public static getLogger()Lkellinwood/logging/LoggerInterface;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    if-nez v0, :cond_0

    const-class v0, Lkellinwood/zipio/ZioEntry;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkellinwood/logging/LoggerManager;->getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    sput-object v0, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    .line 71
    :cond_0
    sget-object v0, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    return-object v0
.end method

.method public static read(Lkellinwood/zipio/ZipInput;)Lkellinwood/zipio/ZioEntry;
    .locals 6
    .param p0, "input"    # Lkellinwood/zipio/ZipInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 324
    invoke-virtual {p0}, Lkellinwood/zipio/ZipInput;->readInt()I

    move-result v1

    .line 325
    .local v1, "signature":I
    const v2, 0x2014b50

    if-eq v1, v2, :cond_0

    .line 327
    invoke-virtual {p0}, Lkellinwood/zipio/ZipInput;->getFilePointer()J

    move-result-wide v2

    const-wide/16 v4, 0x4

    sub-long/2addr v2, v4

    invoke-virtual {p0, v2, v3}, Lkellinwood/zipio/ZipInput;->seek(J)V

    .line 328
    const/4 v0, 0x0

    .line 334
    :goto_0
    return-object v0

    .line 331
    :cond_0
    new-instance v0, Lkellinwood/zipio/ZioEntry;

    invoke-direct {v0, p0}, Lkellinwood/zipio/ZioEntry;-><init>(Lkellinwood/zipio/ZipInput;)V

    .line 333
    .local v0, "entry":Lkellinwood/zipio/ZioEntry;
    invoke-direct {v0, p0}, Lkellinwood/zipio/ZioEntry;->doRead(Lkellinwood/zipio/ZipInput;)V

    goto :goto_0
.end method


# virtual methods
.method public getClonedEntry(Ljava/lang/String;)Lkellinwood/zipio/ZioEntry;
    .locals 4
    .param p1, "newName"    # Ljava/lang/String;

    .prologue
    .line 141
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkellinwood/zipio/ZioEntry;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    .local v0, "clone":Lkellinwood/zipio/ZioEntry;
    invoke-virtual {v0, p1}, Lkellinwood/zipio/ZioEntry;->setName(Ljava/lang/String;)V

    .line 148
    return-object v0

    .line 143
    .end local v0    # "clone":Lkellinwood/zipio/ZioEntry;
    :catch_0
    move-exception v1

    .line 145
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "clone() failed!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getCompressedSize()I
    .locals 1

    .prologue
    .line 589
    iget v0, p0, Lkellinwood/zipio/ZioEntry;->compressedSize:I

    return v0
.end method

.method public getCompression()S
    .locals 1

    .prologue
    .line 581
    iget-short v0, p0, Lkellinwood/zipio/ZioEntry;->compression:S

    return v0
.end method

.method public getCrc32()I
    .locals 1

    .prologue
    .line 585
    iget v0, p0, Lkellinwood/zipio/ZioEntry;->crc32:I

    return v0
.end method

.method public getData()[B
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 428
    iget-object v4, p0, Lkellinwood/zipio/ZioEntry;->data:[B

    if-eqz v4, :cond_1

    iget-object v3, p0, Lkellinwood/zipio/ZioEntry;->data:[B

    .line 440
    :cond_0
    return-object v3

    .line 430
    :cond_1
    iget v4, p0, Lkellinwood/zipio/ZioEntry;->size:I

    new-array v3, v4, [B

    .line 432
    .local v3, "tmpdata":[B
    invoke-virtual {p0}, Lkellinwood/zipio/ZioEntry;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 433
    .local v1, "din":Ljava/io/InputStream;
    const/4 v0, 0x0

    .line 435
    .local v0, "count":I
    :goto_0
    iget v4, p0, Lkellinwood/zipio/ZioEntry;->size:I

    if-eq v0, v4, :cond_0

    .line 436
    iget v4, p0, Lkellinwood/zipio/ZioEntry;->size:I

    sub-int/2addr v4, v0

    invoke-virtual {v1, v3, v0, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 437
    .local v2, "numRead":I
    if-gez v2, :cond_2

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Read failed, expecting %d bytes, got %d instead"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p0, Lkellinwood/zipio/ZioEntry;->size:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 438
    :cond_2
    add-int/2addr v0, v2

    .line 439
    goto :goto_0
.end method

.method public getDataPosition()J
    .locals 2

    .prologue
    .line 621
    iget-wide v0, p0, Lkellinwood/zipio/ZioEntry;->dataPosition:J

    return-wide v0
.end method

.method public getDiskNumberStart()S
    .locals 1

    .prologue
    .line 605
    iget-short v0, p0, Lkellinwood/zipio/ZioEntry;->diskNumberStart:S

    return v0
.end method

.method public getEntryOut()Lkellinwood/zipio/ZioEntryOutputStream;
    .locals 1

    .prologue
    .line 625
    iget-object v0, p0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    return-object v0
.end method

.method public getExternalAttributes()I
    .locals 1

    .prologue
    .line 613
    iget v0, p0, Lkellinwood/zipio/ZioEntry;->externalAttributes:I

    return v0
.end method

.method public getExtraData()[B
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lkellinwood/zipio/ZioEntry;->extraData:[B

    return-object v0
.end method

.method public getFileComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lkellinwood/zipio/ZioEntry;->fileComment:Ljava/lang/String;

    return-object v0
.end method

.method public getGeneralPurposeBits()S
    .locals 1

    .prologue
    .line 577
    iget-short v0, p0, Lkellinwood/zipio/ZioEntry;->generalPurposeBits:S

    return v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 445
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lkellinwood/zipio/ZioEntry;->getInputStream(Ljava/io/OutputStream;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getInputStream(Ljava/io/OutputStream;)Ljava/io/InputStream;
    .locals 6
    .param p1, "monitorStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 451
    iget-object v2, p0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    if-eqz v2, :cond_1

    .line 452
    iget-object v2, p0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    invoke-virtual {v2}, Lkellinwood/zipio/ZioEntryOutputStream;->close()V

    .line 453
    iget-object v2, p0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    invoke-virtual {v2}, Lkellinwood/zipio/ZioEntryOutputStream;->getSize()I

    move-result v2

    iput v2, p0, Lkellinwood/zipio/ZioEntry;->size:I

    .line 454
    iget-object v2, p0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    invoke-virtual {v2}, Lkellinwood/zipio/ZioEntryOutputStream;->getWrappedStream()Ljava/io/OutputStream;

    move-result-object v2

    check-cast v2, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    iput-object v2, p0, Lkellinwood/zipio/ZioEntry;->data:[B

    .line 455
    iget-object v2, p0, Lkellinwood/zipio/ZioEntry;->data:[B

    array-length v2, v2

    iput v2, p0, Lkellinwood/zipio/ZioEntry;->compressedSize:I

    .line 456
    iget-object v2, p0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    invoke-virtual {v2}, Lkellinwood/zipio/ZioEntryOutputStream;->getCRC()I

    move-result v2

    iput v2, p0, Lkellinwood/zipio/ZioEntry;->crc32:I

    .line 457
    const/4 v2, 0x0

    iput-object v2, p0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    .line 458
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lkellinwood/zipio/ZioEntry;->data:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 459
    .local v1, "rawis":Ljava/io/InputStream;
    iget-short v2, p0, Lkellinwood/zipio/ZioEntry;->compression:S

    if-nez v2, :cond_0

    .line 477
    .end local v1    # "rawis":Ljava/io/InputStream;
    :goto_0
    return-object v1

    .line 463
    .restart local v1    # "rawis":Ljava/io/InputStream;
    :cond_0
    new-instance v0, Ljava/util/zip/InflaterInputStream;

    new-instance v2, Ljava/io/SequenceInputStream;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    new-array v4, v5, [B

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v1, v3}, Ljava/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    new-instance v3, Ljava/util/zip/Inflater;

    invoke-direct {v3, v5}, Ljava/util/zip/Inflater;-><init>(Z)V

    invoke-direct {v0, v2, v3}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V

    move-object v1, v0

    goto :goto_0

    .line 468
    .end local v1    # "rawis":Ljava/io/InputStream;
    :cond_1
    new-instance v0, Lkellinwood/zipio/ZioEntryInputStream;

    invoke-direct {v0, p0}, Lkellinwood/zipio/ZioEntryInputStream;-><init>(Lkellinwood/zipio/ZioEntry;)V

    .line 469
    .local v0, "dataStream":Lkellinwood/zipio/ZioEntryInputStream;
    if-eqz p1, :cond_2

    invoke-virtual {v0, p1}, Lkellinwood/zipio/ZioEntryInputStream;->setMonitorStream(Ljava/io/OutputStream;)V

    .line 470
    :cond_2
    iget-short v2, p0, Lkellinwood/zipio/ZioEntry;->compression:S

    if-eqz v2, :cond_3

    .line 474
    invoke-virtual {v0, v5}, Lkellinwood/zipio/ZioEntryInputStream;->setReturnDummyByte(Z)V

    .line 475
    new-instance v1, Ljava/util/zip/InflaterInputStream;

    new-instance v2, Ljava/util/zip/Inflater;

    invoke-direct {v2, v5}, Ljava/util/zip/Inflater;-><init>(Z)V

    invoke-direct {v1, v0, v2}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V

    goto :goto_0

    :cond_3
    move-object v1, v0

    .line 477
    goto :goto_0
.end method

.method public getInternalAttributes()S
    .locals 1

    .prologue
    .line 609
    iget-short v0, p0, Lkellinwood/zipio/ZioEntry;->internalAttributes:S

    return v0
.end method

.method public getLocalHeaderOffset()I
    .locals 1

    .prologue
    .line 617
    iget v0, p0, Lkellinwood/zipio/ZioEntry;->localHeaderOffset:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 3

    .prologue
    .line 483
    new-instance v0, Lkellinwood/zipio/ZioEntryOutputStream;

    iget-short v1, p0, Lkellinwood/zipio/ZioEntry;->compression:S

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-direct {v0, v1, v2}, Lkellinwood/zipio/ZioEntryOutputStream;-><init>(ILjava/io/OutputStream;)V

    iput-object v0, p0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    .line 484
    iget-object v0, p0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 593
    iget v0, p0, Lkellinwood/zipio/ZioEntry;->size:I

    return v0
.end method

.method public getTime()J
    .locals 9

    .prologue
    .line 521
    iget-short v7, p0, Lkellinwood/zipio/ZioEntry;->modificationDate:S

    shr-int/lit8 v7, v7, 0x9

    and-int/lit8 v7, v7, 0x7f

    add-int/lit8 v1, v7, 0x50

    .line 522
    .local v1, "year":I
    iget-short v7, p0, Lkellinwood/zipio/ZioEntry;->modificationDate:S

    shr-int/lit8 v7, v7, 0x5

    and-int/lit8 v7, v7, 0xf

    add-int/lit8 v2, v7, -0x1

    .line 523
    .local v2, "month":I
    iget-short v7, p0, Lkellinwood/zipio/ZioEntry;->modificationDate:S

    and-int/lit8 v3, v7, 0x1f

    .line 524
    .local v3, "day":I
    iget-short v7, p0, Lkellinwood/zipio/ZioEntry;->modificationTime:S

    shr-int/lit8 v7, v7, 0xb

    and-int/lit8 v4, v7, 0x1f

    .line 525
    .local v4, "hour":I
    iget-short v7, p0, Lkellinwood/zipio/ZioEntry;->modificationTime:S

    shr-int/lit8 v7, v7, 0x5

    and-int/lit8 v5, v7, 0x3f

    .line 526
    .local v5, "minute":I
    iget-short v7, p0, Lkellinwood/zipio/ZioEntry;->modificationTime:S

    shl-int/lit8 v7, v7, 0x1

    and-int/lit8 v6, v7, 0x3e

    .line 527
    .local v6, "seconds":I
    new-instance v0, Ljava/util/Date;

    invoke-direct/range {v0 .. v6}, Ljava/util/Date;-><init>(IIIIII)V

    .line 528
    .local v0, "d":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    return-wide v7
.end method

.method public getVersionMadeBy()S
    .locals 1

    .prologue
    .line 569
    iget-short v0, p0, Lkellinwood/zipio/ZioEntry;->versionMadeBy:S

    return v0
.end method

.method public getVersionRequired()S
    .locals 1

    .prologue
    .line 573
    iget-short v0, p0, Lkellinwood/zipio/ZioEntry;->versionRequired:S

    return v0
.end method

.method public getZipInput()Lkellinwood/zipio/ZipInput;
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lkellinwood/zipio/ZioEntry;->zipInput:Lkellinwood/zipio/ZipInput;

    return-object v0
.end method

.method public isDirectory()Z
    .locals 2

    .prologue
    .line 552
    iget-object v0, p0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public readLocalHeader()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    iget-object v5, p0, Lkellinwood/zipio/ZioEntry;->zipInput:Lkellinwood/zipio/ZipInput;

    .line 155
    .local v5, "input":Lkellinwood/zipio/ZipInput;
    invoke-static {}, Lkellinwood/zipio/ZioEntry;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v9

    invoke-interface {v9}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v0

    .line 157
    .local v0, "debug":Z
    iget v9, p0, Lkellinwood/zipio/ZioEntry;->localHeaderOffset:I

    int-to-long v9, v9

    invoke-virtual {v5, v9, v10}, Lkellinwood/zipio/ZipInput;->seek(J)V

    .line 159
    if-eqz v0, :cond_0

    invoke-static {}, Lkellinwood/zipio/ZioEntry;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v9

    const-string v10, "FILE POSITION: 0x%08x"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v5}, Lkellinwood/zipio/ZipInput;->getFilePointer()J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 162
    :cond_0
    invoke-virtual {v5}, Lkellinwood/zipio/ZipInput;->readInt()I

    move-result v6

    .line 163
    .local v6, "signature":I
    const v9, 0x4034b50

    if-eq v6, v9, :cond_1

    .line 164
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string v10, "Local header not found at pos=0x%08x, file=%s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v5}, Lkellinwood/zipio/ZipInput;->getFilePointer()J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    iget-object v13, p0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 177
    :cond_1
    invoke-virtual {v5}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v8

    .line 178
    .local v8, "tmpShort":S
    if-eqz v0, :cond_2

    sget-object v9, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v10, "Version required: 0x%04x"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 181
    :cond_2
    invoke-virtual {v5}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v8

    .line 182
    if-eqz v0, :cond_3

    sget-object v9, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v10, "General purpose bits: 0x%04x"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 185
    :cond_3
    invoke-virtual {v5}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v8

    .line 186
    if-eqz v0, :cond_4

    sget-object v9, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v10, "Compression: 0x%04x"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 189
    :cond_4
    invoke-virtual {v5}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v8

    .line 190
    if-eqz v0, :cond_5

    sget-object v9, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v10, "Modification time: 0x%04x"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 193
    :cond_5
    invoke-virtual {v5}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v8

    .line 194
    if-eqz v0, :cond_6

    sget-object v9, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v10, "Modification date: 0x%04x"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 197
    :cond_6
    invoke-virtual {v5}, Lkellinwood/zipio/ZipInput;->readInt()I

    move-result v7

    .line 198
    .local v7, "tmpInt":I
    if-eqz v0, :cond_7

    sget-object v9, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v10, "CRC-32: 0x%04x"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 201
    :cond_7
    invoke-virtual {v5}, Lkellinwood/zipio/ZipInput;->readInt()I

    move-result v7

    .line 202
    if-eqz v0, :cond_8

    sget-object v9, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v10, "Compressed size: 0x%04x"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 205
    :cond_8
    invoke-virtual {v5}, Lkellinwood/zipio/ZipInput;->readInt()I

    move-result v7

    .line 206
    if-eqz v0, :cond_9

    sget-object v9, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v10, "Size: 0x%04x"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 209
    :cond_9
    invoke-virtual {v5}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v3

    .line 210
    .local v3, "fileNameLen":S
    if-eqz v0, :cond_a

    sget-object v9, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v10, "File name length: 0x%04x"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v3}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 213
    :cond_a
    invoke-virtual {v5}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v2

    .line 214
    .local v2, "extraLen":S
    if-eqz v0, :cond_b

    sget-object v9, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v10, "Extra length: 0x%04x"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 217
    :cond_b
    invoke-virtual {v5, v3}, Lkellinwood/zipio/ZipInput;->readString(I)Ljava/lang/String;

    move-result-object v4

    .line 218
    .local v4, "filename":Ljava/lang/String;
    if-eqz v0, :cond_c

    sget-object v9, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Filename: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 221
    :cond_c
    invoke-virtual {v5, v2}, Lkellinwood/zipio/ZipInput;->readBytes(I)[B

    move-result-object v1

    .line 224
    .local v1, "extra":[B
    invoke-virtual {v5}, Lkellinwood/zipio/ZipInput;->getFilePointer()J

    move-result-wide v9

    iput-wide v9, p0, Lkellinwood/zipio/ZioEntry;->dataPosition:J

    .line 225
    if-eqz v0, :cond_d

    sget-object v9, Lkellinwood/zipio/ZioEntry;->log:Lkellinwood/logging/LoggerInterface;

    const-string v10, "Data position: 0x%08x"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-wide v13, p0, Lkellinwood/zipio/ZioEntry;->dataPosition:J

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 227
    :cond_d
    return-void
.end method

.method public setCompression(I)V
    .locals 1
    .param p1, "compression"    # I

    .prologue
    .line 565
    int-to-short v0, p1

    iput-short v0, p0, Lkellinwood/zipio/ZioEntry;->compression:S

    .line 566
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 560
    iput-object p1, p0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    .line 561
    return-void
.end method

.method public setTime(J)V
    .locals 6
    .param p1, "time"    # J

    .prologue
    .line 535
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 537
    .local v0, "d":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getYear()I

    move-result v4

    add-int/lit16 v3, v4, 0x76c

    .line 538
    .local v3, "year":I
    const/16 v4, 0x7bc

    if-ge v3, v4, :cond_0

    .line 539
    const-wide/32 v1, 0x210000

    .line 547
    .local v1, "dtime":J
    :goto_0
    const/16 v4, 0x10

    shr-long v4, v1, v4

    long-to-int v4, v4

    int-to-short v4, v4

    iput-short v4, p0, Lkellinwood/zipio/ZioEntry;->modificationDate:S

    .line 548
    const-wide/32 v4, 0xffff

    and-long/2addr v4, v1

    long-to-int v4, v4

    int-to-short v4, v4

    iput-short v4, p0, Lkellinwood/zipio/ZioEntry;->modificationTime:S

    .line 549
    return-void

    .line 542
    .end local v1    # "dtime":J
    :cond_0
    add-int/lit16 v4, v3, -0x7bc

    shl-int/lit8 v4, v4, 0x19

    invoke-virtual {v0}, Ljava/util/Date;->getMonth()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    shl-int/lit8 v5, v5, 0x15

    or-int/2addr v4, v5

    invoke-virtual {v0}, Ljava/util/Date;->getDate()I

    move-result v5

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v4, v5

    invoke-virtual {v0}, Ljava/util/Date;->getHours()I

    move-result v5

    shl-int/lit8 v5, v5, 0xb

    or-int/2addr v4, v5

    invoke-virtual {v0}, Ljava/util/Date;->getMinutes()I

    move-result v5

    shl-int/lit8 v5, v5, 0x5

    or-int/2addr v4, v5

    invoke-virtual {v0}, Ljava/util/Date;->getSeconds()I

    move-result v5

    shr-int/lit8 v5, v5, 0x1

    or-int/2addr v4, v5

    int-to-long v1, v4

    .restart local v1    # "dtime":J
    goto :goto_0
.end method

.method public write(Lkellinwood/zipio/ZipOutput;)V
    .locals 4
    .param p1, "output"    # Lkellinwood/zipio/ZipOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 489
    invoke-static {}, Lkellinwood/zipio/ZioEntry;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v1

    invoke-interface {v1}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v0

    .line 492
    .local v0, "debug":Z
    const v1, 0x2014b50

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeInt(I)V

    .line 493
    iget-short v1, p0, Lkellinwood/zipio/ZioEntry;->versionMadeBy:S

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 494
    iget-short v1, p0, Lkellinwood/zipio/ZioEntry;->versionRequired:S

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 495
    iget-short v1, p0, Lkellinwood/zipio/ZioEntry;->generalPurposeBits:S

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 496
    iget-short v1, p0, Lkellinwood/zipio/ZioEntry;->compression:S

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 497
    iget-short v1, p0, Lkellinwood/zipio/ZioEntry;->modificationTime:S

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 498
    iget-short v1, p0, Lkellinwood/zipio/ZioEntry;->modificationDate:S

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 499
    iget v1, p0, Lkellinwood/zipio/ZioEntry;->crc32:I

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeInt(I)V

    .line 500
    iget v1, p0, Lkellinwood/zipio/ZioEntry;->compressedSize:I

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeInt(I)V

    .line 501
    iget v1, p0, Lkellinwood/zipio/ZioEntry;->size:I

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeInt(I)V

    .line 502
    iget-object v1, p0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 503
    iget-object v1, p0, Lkellinwood/zipio/ZioEntry;->extraData:[B

    array-length v1, v1

    iget-short v2, p0, Lkellinwood/zipio/ZioEntry;->numAlignBytes:S

    add-int/2addr v1, v2

    int-to-short v1, v1

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 504
    iget-object v1, p0, Lkellinwood/zipio/ZioEntry;->fileComment:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 505
    iget-short v1, p0, Lkellinwood/zipio/ZioEntry;->diskNumberStart:S

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 506
    iget-short v1, p0, Lkellinwood/zipio/ZioEntry;->internalAttributes:S

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 507
    iget v1, p0, Lkellinwood/zipio/ZioEntry;->externalAttributes:I

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeInt(I)V

    .line 508
    iget v1, p0, Lkellinwood/zipio/ZioEntry;->localHeaderOffset:I

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeInt(I)V

    .line 510
    iget-object v1, p0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeString(Ljava/lang/String;)V

    .line 511
    iget-object v1, p0, Lkellinwood/zipio/ZioEntry;->extraData:[B

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeBytes([B)V

    .line 512
    iget-short v1, p0, Lkellinwood/zipio/ZioEntry;->numAlignBytes:S

    if-lez v1, :cond_0

    sget-object v1, Lkellinwood/zipio/ZioEntry;->alignBytes:[B

    const/4 v2, 0x0

    iget-short v3, p0, Lkellinwood/zipio/ZioEntry;->numAlignBytes:S

    invoke-virtual {p1, v1, v2, v3}, Lkellinwood/zipio/ZipOutput;->writeBytes([BII)V

    .line 513
    :cond_0
    iget-object v1, p0, Lkellinwood/zipio/ZioEntry;->fileComment:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeString(Ljava/lang/String;)V

    .line 515
    return-void
.end method

.method public writeLocalEntry(Lkellinwood/zipio/ZipOutput;)V
    .locals 16
    .param p1, "output"    # Lkellinwood/zipio/ZipOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 231
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->data:[B

    if-nez v10, :cond_0

    move-object/from16 v0, p0

    iget-wide v10, v0, Lkellinwood/zipio/ZioEntry;->dataPosition:J

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-gez v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->zipInput:Lkellinwood/zipio/ZipInput;

    if-eqz v10, :cond_0

    .line 232
    invoke-virtual/range {p0 .. p0}, Lkellinwood/zipio/ZioEntry;->readLocalHeader()V

    .line 235
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lkellinwood/zipio/ZipOutput;->getFilePointer()I

    move-result v10

    move-object/from16 v0, p0

    iput v10, v0, Lkellinwood/zipio/ZioEntry;->localHeaderOffset:I

    .line 237
    invoke-static {}, Lkellinwood/zipio/ZioEntry;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v10

    invoke-interface {v10}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v6

    .line 239
    .local v6, "debug":Z
    if-eqz v6, :cond_1

    .line 240
    invoke-static {}, Lkellinwood/zipio/ZioEntry;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v10

    const-string v11, "Writing local header at 0x%08x - %s"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget v14, v0, Lkellinwood/zipio/ZioEntry;->localHeaderOffset:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 243
    :cond_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    if-eqz v10, :cond_2

    .line 244
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    invoke-virtual {v10}, Lkellinwood/zipio/ZioEntryOutputStream;->close()V

    .line 245
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    invoke-virtual {v10}, Lkellinwood/zipio/ZioEntryOutputStream;->getSize()I

    move-result v10

    move-object/from16 v0, p0

    iput v10, v0, Lkellinwood/zipio/ZioEntry;->size:I

    .line 246
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    invoke-virtual {v10}, Lkellinwood/zipio/ZioEntryOutputStream;->getWrappedStream()Ljava/io/OutputStream;

    move-result-object v10

    check-cast v10, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lkellinwood/zipio/ZioEntry;->data:[B

    .line 247
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->data:[B

    array-length v10, v10

    move-object/from16 v0, p0

    iput v10, v0, Lkellinwood/zipio/ZioEntry;->compressedSize:I

    .line 248
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->entryOut:Lkellinwood/zipio/ZioEntryOutputStream;

    invoke-virtual {v10}, Lkellinwood/zipio/ZioEntryOutputStream;->getCRC()I

    move-result v10

    move-object/from16 v0, p0

    iput v10, v0, Lkellinwood/zipio/ZioEntry;->crc32:I

    .line 251
    :cond_2
    const v10, 0x4034b50

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lkellinwood/zipio/ZipOutput;->writeInt(I)V

    .line 252
    move-object/from16 v0, p0

    iget-short v10, v0, Lkellinwood/zipio/ZioEntry;->versionRequired:S

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 253
    move-object/from16 v0, p0

    iget-short v10, v0, Lkellinwood/zipio/ZioEntry;->generalPurposeBits:S

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 254
    move-object/from16 v0, p0

    iget-short v10, v0, Lkellinwood/zipio/ZioEntry;->compression:S

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 255
    move-object/from16 v0, p0

    iget-short v10, v0, Lkellinwood/zipio/ZioEntry;->modificationTime:S

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 256
    move-object/from16 v0, p0

    iget-short v10, v0, Lkellinwood/zipio/ZioEntry;->modificationDate:S

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 257
    move-object/from16 v0, p0

    iget v10, v0, Lkellinwood/zipio/ZioEntry;->crc32:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lkellinwood/zipio/ZipOutput;->writeInt(I)V

    .line 258
    move-object/from16 v0, p0

    iget v10, v0, Lkellinwood/zipio/ZioEntry;->compressedSize:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lkellinwood/zipio/ZipOutput;->writeInt(I)V

    .line 259
    move-object/from16 v0, p0

    iget v10, v0, Lkellinwood/zipio/ZioEntry;->size:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lkellinwood/zipio/ZipOutput;->writeInt(I)V

    .line 260
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    int-to-short v10, v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 262
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput-short v10, v0, Lkellinwood/zipio/ZioEntry;->numAlignBytes:S

    .line 265
    move-object/from16 v0, p0

    iget-short v10, v0, Lkellinwood/zipio/ZioEntry;->compression:S

    if-nez v10, :cond_3

    .line 267
    invoke-virtual/range {p1 .. p1}, Lkellinwood/zipio/ZipOutput;->getFilePointer()I

    move-result v10

    add-int/lit8 v10, v10, 0x2

    move-object/from16 v0, p0

    iget-object v11, v0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lkellinwood/zipio/ZioEntry;->extraData:[B

    array-length v11, v11

    add-int/2addr v10, v11

    int-to-long v3, v10

    .line 272
    .local v3, "dataPos":J
    const-wide/16 v10, 0x4

    rem-long v10, v3, v10

    long-to-int v10, v10

    int-to-short v5, v10

    .line 274
    .local v5, "dataPosMod4":S
    if-lez v5, :cond_3

    .line 275
    rsub-int/lit8 v10, v5, 0x4

    int-to-short v10, v10

    move-object/from16 v0, p0

    iput-short v10, v0, Lkellinwood/zipio/ZioEntry;->numAlignBytes:S

    .line 281
    .end local v3    # "dataPos":J
    .end local v5    # "dataPosMod4":S
    :cond_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->extraData:[B

    array-length v10, v10

    move-object/from16 v0, p0

    iget-short v11, v0, Lkellinwood/zipio/ZioEntry;->numAlignBytes:S

    add-int/2addr v10, v11

    int-to-short v10, v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 284
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lkellinwood/zipio/ZipOutput;->writeString(Ljava/lang/String;)V

    .line 287
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->extraData:[B

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lkellinwood/zipio/ZipOutput;->writeBytes([B)V

    .line 290
    move-object/from16 v0, p0

    iget-short v10, v0, Lkellinwood/zipio/ZioEntry;->numAlignBytes:S

    if-lez v10, :cond_4

    .line 291
    sget-object v10, Lkellinwood/zipio/ZioEntry;->alignBytes:[B

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-short v12, v0, Lkellinwood/zipio/ZioEntry;->numAlignBytes:S

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11, v12}, Lkellinwood/zipio/ZipOutput;->writeBytes([BII)V

    .line 294
    :cond_4
    if-eqz v6, :cond_5

    invoke-static {}, Lkellinwood/zipio/ZioEntry;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v10

    const-string v11, "Data position 0x%08x"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual/range {p1 .. p1}, Lkellinwood/zipio/ZipOutput;->getFilePointer()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 295
    :cond_5
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->data:[B

    if-eqz v10, :cond_7

    .line 296
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->data:[B

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lkellinwood/zipio/ZipOutput;->writeBytes([B)V

    .line 297
    if-eqz v6, :cond_6

    invoke-static {}, Lkellinwood/zipio/ZioEntry;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v10

    const-string v11, "Wrote %d bytes"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lkellinwood/zipio/ZioEntry;->data:[B

    array-length v14, v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 318
    :cond_6
    return-void

    .line 301
    :cond_7
    if-eqz v6, :cond_8

    invoke-static {}, Lkellinwood/zipio/ZioEntry;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v10

    const-string v11, "Seeking to position 0x%08x"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-wide v14, v0, Lkellinwood/zipio/ZioEntry;->dataPosition:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 302
    :cond_8
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->zipInput:Lkellinwood/zipio/ZipInput;

    move-object/from16 v0, p0

    iget-wide v11, v0, Lkellinwood/zipio/ZioEntry;->dataPosition:J

    invoke-virtual {v10, v11, v12}, Lkellinwood/zipio/ZipInput;->seek(J)V

    .line 304
    move-object/from16 v0, p0

    iget v10, v0, Lkellinwood/zipio/ZioEntry;->compressedSize:I

    const/16 v11, 0x1fa0

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 305
    .local v2, "bufferSize":I
    new-array v1, v2, [B

    .line 306
    .local v1, "buffer":[B
    const-wide/16 v8, 0x0

    .line 308
    .local v8, "totalCount":J
    :goto_0
    move-object/from16 v0, p0

    iget v10, v0, Lkellinwood/zipio/ZioEntry;->compressedSize:I

    int-to-long v10, v10

    cmp-long v10, v8, v10

    if-eqz v10, :cond_6

    .line 309
    move-object/from16 v0, p0

    iget-object v10, v0, Lkellinwood/zipio/ZioEntry;->zipInput:Lkellinwood/zipio/ZipInput;

    iget-object v10, v10, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v12, v0, Lkellinwood/zipio/ZioEntry;->compressedSize:I

    int-to-long v12, v12

    sub-long/2addr v12, v8

    int-to-long v14, v2

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v12

    long-to-int v12, v12

    invoke-virtual {v10, v1, v11, v12}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v7

    .line 310
    .local v7, "numRead":I
    if-lez v7, :cond_a

    .line 311
    const/4 v10, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10, v7}, Lkellinwood/zipio/ZipOutput;->writeBytes([BII)V

    .line 312
    if-eqz v6, :cond_9

    invoke-static {}, Lkellinwood/zipio/ZioEntry;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v10

    const-string v11, "Wrote %d bytes"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 313
    :cond_9
    int-to-long v10, v7

    add-long/2addr v8, v10

    goto :goto_0

    .line 315
    :cond_a
    new-instance v10, Ljava/lang/IllegalStateException;

    const-string v11, "EOF reached while copying %s with %d bytes left to go"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lkellinwood/zipio/ZioEntry;->filename:Ljava/lang/String;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget v14, v0, Lkellinwood/zipio/ZioEntry;->compressedSize:I

    int-to-long v14, v14

    sub-long/2addr v14, v8

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v10
.end method
