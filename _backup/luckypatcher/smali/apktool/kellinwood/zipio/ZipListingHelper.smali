.class public Lkellinwood/zipio/ZipListingHelper;
.super Ljava/lang/Object;
.source "ZipListingHelper.java"


# static fields
.field static dateFormat:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd-yy HH:mm"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkellinwood/zipio/ZipListingHelper;->dateFormat:Ljava/text/DateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static listEntry(Lkellinwood/logging/LoggerInterface;Lkellinwood/zipio/ZioEntry;)V
    .locals 8
    .param p0, "log"    # Lkellinwood/logging/LoggerInterface;
    .param p1, "entry"    # Lkellinwood/zipio/ZioEntry;

    .prologue
    .line 41
    const/4 v0, 0x0

    .line 42
    .local v0, "ratio":I
    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getSize()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getSize()I

    move-result v1

    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getCompressedSize()I

    move-result v2

    sub-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x64

    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getSize()I

    move-result v2

    div-int v0, v1, v2

    .line 43
    :cond_0
    const-string v2, "%8d  %6s %8d %4d%% %s  %08x  %s"

    const/4 v1, 0x7

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getSize()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x1

    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getCompression()S

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Stored"

    :goto_0
    aput-object v1, v3, v4

    const/4 v1, 0x2

    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getCompressedSize()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x4

    sget-object v4, Lkellinwood/zipio/ZipListingHelper;->dateFormat:Ljava/text/DateFormat;

    new-instance v5, Ljava/util/Date;

    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getTime()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x5

    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getCrc32()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x6

    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v1}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 51
    return-void

    .line 43
    :cond_1
    const-string v1, "Defl:N"

    goto :goto_0
.end method

.method public static listHeader(Lkellinwood/logging/LoggerInterface;)V
    .locals 1
    .param p0, "log"    # Lkellinwood/logging/LoggerInterface;

    .prologue
    .line 34
    const-string v0, " Length   Method    Size  Ratio   Date   Time   CRC-32    Name"

    invoke-interface {p0, v0}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 35
    const-string v0, "--------  ------  ------- -----   ----   ----   ------    ----"

    invoke-interface {p0, v0}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 37
    return-void
.end method
