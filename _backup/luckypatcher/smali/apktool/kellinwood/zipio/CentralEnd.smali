.class public Lkellinwood/zipio/CentralEnd;
.super Ljava/lang/Object;
.source "CentralEnd.java"


# static fields
.field private static log:Lkellinwood/logging/LoggerInterface;


# instance fields
.field public centralDirectorySize:I

.field public centralStartDisk:S

.field public centralStartOffset:I

.field public fileComment:Ljava/lang/String;

.field public numCentralEntries:S

.field public numberThisDisk:S

.field public signature:I

.field public totalCentralEntries:S


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const v0, 0x6054b50

    iput v0, p0, Lkellinwood/zipio/CentralEnd;->signature:I

    .line 26
    iput-short v1, p0, Lkellinwood/zipio/CentralEnd;->numberThisDisk:S

    .line 27
    iput-short v1, p0, Lkellinwood/zipio/CentralEnd;->centralStartDisk:S

    return-void
.end method

.method private doRead(Lkellinwood/zipio/ZipInput;)V
    .locals 8
    .param p1, "input"    # Lkellinwood/zipio/ZipInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 62
    invoke-static {}, Lkellinwood/zipio/CentralEnd;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v2

    invoke-interface {v2}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v0

    .line 64
    .local v0, "debug":Z
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v2

    iput-short v2, p0, Lkellinwood/zipio/CentralEnd;->numberThisDisk:S

    .line 65
    if-eqz v0, :cond_0

    sget-object v2, Lkellinwood/zipio/CentralEnd;->log:Lkellinwood/logging/LoggerInterface;

    const-string v3, "This disk number: 0x%04x"

    new-array v4, v7, [Ljava/lang/Object;

    iget-short v5, p0, Lkellinwood/zipio/CentralEnd;->numberThisDisk:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 67
    :cond_0
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v2

    iput-short v2, p0, Lkellinwood/zipio/CentralEnd;->centralStartDisk:S

    .line 68
    if-eqz v0, :cond_1

    sget-object v2, Lkellinwood/zipio/CentralEnd;->log:Lkellinwood/logging/LoggerInterface;

    const-string v3, "Central dir start disk number: 0x%04x"

    new-array v4, v7, [Ljava/lang/Object;

    iget-short v5, p0, Lkellinwood/zipio/CentralEnd;->centralStartDisk:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 70
    :cond_1
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v2

    iput-short v2, p0, Lkellinwood/zipio/CentralEnd;->numCentralEntries:S

    .line 71
    if-eqz v0, :cond_2

    sget-object v2, Lkellinwood/zipio/CentralEnd;->log:Lkellinwood/logging/LoggerInterface;

    const-string v3, "Central entries on this disk: 0x%04x"

    new-array v4, v7, [Ljava/lang/Object;

    iget-short v5, p0, Lkellinwood/zipio/CentralEnd;->numCentralEntries:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 73
    :cond_2
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v2

    iput-short v2, p0, Lkellinwood/zipio/CentralEnd;->totalCentralEntries:S

    .line 74
    if-eqz v0, :cond_3

    sget-object v2, Lkellinwood/zipio/CentralEnd;->log:Lkellinwood/logging/LoggerInterface;

    const-string v3, "Total number of central entries: 0x%04x"

    new-array v4, v7, [Ljava/lang/Object;

    iget-short v5, p0, Lkellinwood/zipio/CentralEnd;->totalCentralEntries:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 76
    :cond_3
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readInt()I

    move-result v2

    iput v2, p0, Lkellinwood/zipio/CentralEnd;->centralDirectorySize:I

    .line 77
    if-eqz v0, :cond_4

    sget-object v2, Lkellinwood/zipio/CentralEnd;->log:Lkellinwood/logging/LoggerInterface;

    const-string v3, "Central directory size: 0x%08x"

    new-array v4, v7, [Ljava/lang/Object;

    iget v5, p0, Lkellinwood/zipio/CentralEnd;->centralDirectorySize:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 79
    :cond_4
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readInt()I

    move-result v2

    iput v2, p0, Lkellinwood/zipio/CentralEnd;->centralStartOffset:I

    .line 80
    if-eqz v0, :cond_5

    sget-object v2, Lkellinwood/zipio/CentralEnd;->log:Lkellinwood/logging/LoggerInterface;

    const-string v3, "Central directory offset: 0x%08x"

    new-array v4, v7, [Ljava/lang/Object;

    iget v5, p0, Lkellinwood/zipio/CentralEnd;->centralStartOffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 82
    :cond_5
    invoke-virtual {p1}, Lkellinwood/zipio/ZipInput;->readShort()S

    move-result v1

    .line 83
    .local v1, "zipFileCommentLen":S
    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipInput;->readString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lkellinwood/zipio/CentralEnd;->fileComment:Ljava/lang/String;

    .line 84
    if-eqz v0, :cond_6

    sget-object v2, Lkellinwood/zipio/CentralEnd;->log:Lkellinwood/logging/LoggerInterface;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ".ZIP file comment: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lkellinwood/zipio/CentralEnd;->fileComment:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 87
    :cond_6
    return-void
.end method

.method public static getLogger()Lkellinwood/logging/LoggerInterface;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lkellinwood/zipio/CentralEnd;->log:Lkellinwood/logging/LoggerInterface;

    if-nez v0, :cond_0

    const-class v0, Lkellinwood/zipio/CentralEnd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkellinwood/logging/LoggerManager;->getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    sput-object v0, Lkellinwood/zipio/CentralEnd;->log:Lkellinwood/logging/LoggerInterface;

    .line 55
    :cond_0
    sget-object v0, Lkellinwood/zipio/CentralEnd;->log:Lkellinwood/logging/LoggerInterface;

    return-object v0
.end method

.method public static read(Lkellinwood/zipio/ZipInput;)Lkellinwood/zipio/CentralEnd;
    .locals 6
    .param p0, "input"    # Lkellinwood/zipio/ZipInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    invoke-virtual {p0}, Lkellinwood/zipio/ZipInput;->readInt()I

    move-result v1

    .line 41
    .local v1, "signature":I
    const v2, 0x6054b50

    if-eq v1, v2, :cond_0

    .line 43
    invoke-virtual {p0}, Lkellinwood/zipio/ZipInput;->getFilePointer()J

    move-result-wide v2

    const-wide/16 v4, 0x4

    sub-long/2addr v2, v4

    invoke-virtual {p0, v2, v3}, Lkellinwood/zipio/ZipInput;->seek(J)V

    .line 44
    const/4 v0, 0x0

    .line 50
    :goto_0
    return-object v0

    .line 47
    :cond_0
    new-instance v0, Lkellinwood/zipio/CentralEnd;

    invoke-direct {v0}, Lkellinwood/zipio/CentralEnd;-><init>()V

    .line 49
    .local v0, "entry":Lkellinwood/zipio/CentralEnd;
    invoke-direct {v0, p0}, Lkellinwood/zipio/CentralEnd;->doRead(Lkellinwood/zipio/ZipInput;)V

    goto :goto_0
.end method


# virtual methods
.method public write(Lkellinwood/zipio/ZipOutput;)V
    .locals 2
    .param p1, "output"    # Lkellinwood/zipio/ZipOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    invoke-static {}, Lkellinwood/zipio/CentralEnd;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v1

    invoke-interface {v1}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v0

    .line 95
    .local v0, "debug":Z
    iget v1, p0, Lkellinwood/zipio/CentralEnd;->signature:I

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeInt(I)V

    .line 96
    iget-short v1, p0, Lkellinwood/zipio/CentralEnd;->numberThisDisk:S

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 97
    iget-short v1, p0, Lkellinwood/zipio/CentralEnd;->centralStartDisk:S

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 98
    iget-short v1, p0, Lkellinwood/zipio/CentralEnd;->numCentralEntries:S

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 99
    iget-short v1, p0, Lkellinwood/zipio/CentralEnd;->totalCentralEntries:S

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 100
    iget v1, p0, Lkellinwood/zipio/CentralEnd;->centralDirectorySize:I

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeInt(I)V

    .line 101
    iget v1, p0, Lkellinwood/zipio/CentralEnd;->centralStartOffset:I

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeInt(I)V

    .line 102
    iget-object v1, p0, Lkellinwood/zipio/CentralEnd;->fileComment:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeShort(S)V

    .line 103
    iget-object v1, p0, Lkellinwood/zipio/CentralEnd;->fileComment:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lkellinwood/zipio/ZipOutput;->writeString(Ljava/lang/String;)V

    .line 106
    return-void
.end method
