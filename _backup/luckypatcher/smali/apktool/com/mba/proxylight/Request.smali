.class public Lcom/mba/proxylight/Request;
.super Ljava/lang/Object;
.source "Request.java"


# static fields
.field private static CONNECT_PATTERN:Ljava/util/regex/Pattern;

.field private static GETPOST_PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field private headers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private host:Ljava/lang/String;

.field private method:Ljava/lang/String;

.field private port:I

.field private protocol:Ljava/lang/String;

.field private statusline:Ljava/lang/String;

.field private url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-string v0, "(.*):([\\d]+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/mba/proxylight/Request;->CONNECT_PATTERN:Ljava/util/regex/Pattern;

    .line 49
    const-string v0, "(https?)://([^:/]+)(:[\\d]+])?/.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/mba/proxylight/Request;->GETPOST_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v1, p0, Lcom/mba/proxylight/Request;->statusline:Ljava/lang/String;

    iput-object v1, p0, Lcom/mba/proxylight/Request;->method:Ljava/lang/String;

    iput-object v1, p0, Lcom/mba/proxylight/Request;->url:Ljava/lang/String;

    iput-object v1, p0, Lcom/mba/proxylight/Request;->protocol:Ljava/lang/String;

    .line 11
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/mba/proxylight/Request;->headers:Ljava/util/Map;

    .line 50
    iput-object v1, p0, Lcom/mba/proxylight/Request;->host:Ljava/lang/String;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/mba/proxylight/Request;->port:I

    return-void
.end method


# virtual methods
.method public addHeader(Ljava/lang/String;)V
    .locals 6
    .param p1, "h"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x3a

    const/16 v4, 0x20

    .line 108
    const/4 v0, 0x1

    .line 109
    .local v0, "idx1":I
    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v3, v5, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v3, v4, :cond_0

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 114
    .local v1, "name":Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 115
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v3, v5, :cond_1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v3, v4, :cond_1

    .line 116
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 118
    .local v2, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/mba/proxylight/Request;->headers:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    return-void
.end method

.method public dump()V
    .locals 0

    .prologue
    .line 130
    return-void
.end method

.method public getHeaders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/mba/proxylight/Request;->headers:Ljava/util/Map;

    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x50

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 53
    iget-object v2, p0, Lcom/mba/proxylight/Request;->host:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 54
    iget-object v2, p0, Lcom/mba/proxylight/Request;->host:Ljava/lang/String;

    .line 89
    :goto_0
    return-object v2

    .line 56
    :cond_0
    invoke-virtual {p0}, Lcom/mba/proxylight/Request;->getUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 57
    const-string v2, "CONNECT"

    iget-object v3, p0, Lcom/mba/proxylight/Request;->method:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 58
    sget-object v2, Lcom/mba/proxylight/Request;->CONNECT_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Lcom/mba/proxylight/Request;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 59
    .local v1, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 60
    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mba/proxylight/Request;->host:Ljava/lang/String;

    .line 61
    invoke-virtual {v1, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/mba/proxylight/Request;->port:I

    .line 78
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/mba/proxylight/Request;->host:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 79
    invoke-virtual {p0}, Lcom/mba/proxylight/Request;->getHeaders()Ljava/util/Map;

    move-result-object v2

    const-string v3, "Host"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/mba/proxylight/Request;->host:Ljava/lang/String;

    .line 80
    iget-object v2, p0, Lcom/mba/proxylight/Request;->host:Ljava/lang/String;

    const/16 v3, 0x3a

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 81
    .local v0, "idx":I
    const/4 v2, -0x1

    if-le v0, v2, :cond_6

    .line 82
    iget-object v2, p0, Lcom/mba/proxylight/Request;->host:Ljava/lang/String;

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/mba/proxylight/Request;->port:I

    .line 83
    iget-object v2, p0, Lcom/mba/proxylight/Request;->host:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mba/proxylight/Request;->host:Ljava/lang/String;

    .line 89
    .end local v0    # "idx":I
    .end local v1    # "m":Ljava/util/regex/Matcher;
    :cond_2
    :goto_2
    iget-object v2, p0, Lcom/mba/proxylight/Request;->host:Ljava/lang/String;

    goto :goto_0

    .line 64
    :cond_3
    sget-object v2, Lcom/mba/proxylight/Request;->GETPOST_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Lcom/mba/proxylight/Request;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 65
    .restart local v1    # "m":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 66
    invoke-virtual {v1, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mba/proxylight/Request;->host:Ljava/lang/String;

    .line 67
    invoke-virtual {v1, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 68
    invoke-virtual {v1, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    goto :goto_1

    .line 70
    :cond_4
    const-string v2, "http"

    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 71
    iput v7, p0, Lcom/mba/proxylight/Request;->port:I

    goto :goto_1

    .line 73
    :cond_5
    const/16 v2, 0x1bb

    iput v2, p0, Lcom/mba/proxylight/Request;->port:I

    goto :goto_1

    .line 85
    .restart local v0    # "idx":I
    :cond_6
    iput v7, p0, Lcom/mba/proxylight/Request;->port:I

    goto :goto_2
.end method

.method public getMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/mba/proxylight/Request;->method:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/mba/proxylight/Request;->getHost()Ljava/lang/String;

    .line 93
    iget v0, p0, Lcom/mba/proxylight/Request;->port:I

    return v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/mba/proxylight/Request;->protocol:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusline()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/mba/proxylight/Request;->statusline:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/mba/proxylight/Request;->url:Ljava/lang/String;

    return-object v0
.end method

.method public setMethod(Ljava/lang/String;)V
    .locals 0
    .param p1, "method"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/mba/proxylight/Request;->method:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setProtocol(Ljava/lang/String;)V
    .locals 0
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/mba/proxylight/Request;->protocol:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public setStatusline(Ljava/lang/String;)V
    .locals 5
    .param p1, "statusline"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    const/16 v3, 0x20

    .line 16
    iput-object p1, p0, Lcom/mba/proxylight/Request;->statusline:Ljava/lang/String;

    .line 19
    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 20
    .local v0, "idx1":I
    if-eq v0, v4, :cond_0

    const/4 v2, 0x3

    if-ge v0, v2, :cond_1

    .line 21
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "statusline: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 23
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mba/proxylight/Request;->method:Ljava/lang/String;

    .line 26
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 27
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v2, v3, :cond_2

    .line 28
    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 29
    .local v1, "idx2":I
    if-ne v1, v4, :cond_3

    .line 30
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "statusline: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 32
    :cond_3
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mba/proxylight/Request;->url:Ljava/lang/String;

    .line 35
    :cond_4
    add-int/lit8 v1, v1, 0x1

    .line 36
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v2, v3, :cond_4

    .line 37
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mba/proxylight/Request;->protocol:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/mba/proxylight/Request;->url:Ljava/lang/String;

    .line 97
    return-void
.end method
