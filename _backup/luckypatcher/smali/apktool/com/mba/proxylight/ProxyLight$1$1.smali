.class Lcom/mba/proxylight/ProxyLight$1$1;
.super Lcom/mba/proxylight/RequestProcessor;
.source "ProxyLight.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mba/proxylight/ProxyLight$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/mba/proxylight/ProxyLight$1;


# direct methods
.method constructor <init>(Lcom/mba/proxylight/ProxyLight$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/mba/proxylight/ProxyLight$1;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/mba/proxylight/ProxyLight$1$1;->this$1:Lcom/mba/proxylight/ProxyLight$1;

    invoke-direct {p0}, Lcom/mba/proxylight/RequestProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public debug(Ljava/lang/String;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/mba/proxylight/ProxyLight$1$1;->this$1:Lcom/mba/proxylight/ProxyLight$1;

    iget-object v0, v0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    invoke-virtual {v0, p1}, Lcom/mba/proxylight/ProxyLight;->debug(Ljava/lang/String;)V

    .line 103
    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/mba/proxylight/ProxyLight$1$1;->this$1:Lcom/mba/proxylight/ProxyLight$1;

    iget-object v0, v0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    invoke-virtual {v0, p1, p2}, Lcom/mba/proxylight/ProxyLight;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 100
    return-void
.end method

.method public getRemoteProxyHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/mba/proxylight/ProxyLight$1$1;->this$1:Lcom/mba/proxylight/ProxyLight$1;

    iget-object v0, v0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    invoke-virtual {v0}, Lcom/mba/proxylight/ProxyLight;->getRemoteProxyHost()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteProxyPort()I
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/mba/proxylight/ProxyLight$1$1;->this$1:Lcom/mba/proxylight/ProxyLight$1;

    iget-object v0, v0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    invoke-virtual {v0}, Lcom/mba/proxylight/ProxyLight;->getRemoteProxyPort()I

    move-result v0

    return v0
.end method

.method public getRequestFilters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mba/proxylight/RequestFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcom/mba/proxylight/ProxyLight$1$1;->this$1:Lcom/mba/proxylight/ProxyLight$1;

    iget-object v0, v0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    invoke-virtual {v0}, Lcom/mba/proxylight/ProxyLight;->getRequestFilters()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/mba/proxylight/ProxyLight$1$1;->this$1:Lcom/mba/proxylight/ProxyLight$1;

    iget-object v0, v0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    invoke-virtual {v0, p0}, Lcom/mba/proxylight/ProxyLight;->recycle(Lcom/mba/proxylight/RequestProcessor;)V

    .line 108
    return-void
.end method

.method public resolve(Ljava/lang/String;)Ljava/net/InetAddress;
    .locals 1
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/mba/proxylight/ProxyLight$1$1;->this$1:Lcom/mba/proxylight/ProxyLight$1;

    iget-object v0, v0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    invoke-virtual {v0, p1}, Lcom/mba/proxylight/ProxyLight;->resolve(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method
