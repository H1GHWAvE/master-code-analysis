.class Lcom/chelpus/XSupport$35;
.super Lde/robv/android/xposed/XC_MethodHook;
.source "XSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/chelpus/XSupport;->handleLoadPackage(Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/chelpus/XSupport;


# direct methods
.method constructor <init>(Lcom/chelpus/XSupport;)V
    .locals 0
    .param p1, "this$0"    # Lcom/chelpus/XSupport;

    .prologue
    .line 1085
    iput-object p1, p0, Lcom/chelpus/XSupport$35;->this$0:Lcom/chelpus/XSupport;

    invoke-direct {p0}, Lde/robv/android/xposed/XC_MethodHook;-><init>()V

    return-void
.end method


# virtual methods
.method protected beforeHookedMethod(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)V
    .locals 3
    .param p1, "param"    # Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 1089
    iget-object v1, p0, Lcom/chelpus/XSupport$35;->this$0:Lcom/chelpus/XSupport;

    invoke-virtual {v1}, Lcom/chelpus/XSupport;->loadPrefs()V

    .line 1090
    iget-object v1, p0, Lcom/chelpus/XSupport$35;->this$0:Lcom/chelpus/XSupport;

    iget-object v1, v1, Lcom/chelpus/XSupport;->forHide:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/chelpus/XSupport$35;->this$0:Lcom/chelpus/XSupport;

    iget-object v1, v1, Lcom/chelpus/XSupport;->forHide:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/chelpus/XSupport;->enable:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/chelpus/XSupport;->hide:Z

    if-eqz v1, :cond_0

    .line 1092
    iget-object v1, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->args:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v1, v2

    check-cast v0, Ljava/lang/String;

    .line 1093
    .local v0, "pkg":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-class v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1096
    new-instance v1, Landroid/content/pm/PackageManager$NameNotFoundException;

    invoke-direct {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;-><init>()V

    invoke-virtual {p1, v1}, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->setThrowable(Ljava/lang/Throwable;)V

    .line 1102
    .end local v0    # "pkg":Ljava/lang/String;
    :cond_0
    return-void
.end method
