.class Lcom/chelpus/XSupport$39;
.super Lde/robv/android/xposed/XC_MethodHook;
.source "XSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/chelpus/XSupport;->handleLoadPackage(Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/chelpus/XSupport;


# direct methods
.method constructor <init>(Lcom/chelpus/XSupport;)V
    .locals 0
    .param p1, "this$0"    # Lcom/chelpus/XSupport;

    .prologue
    .line 1185
    iput-object p1, p0, Lcom/chelpus/XSupport$39;->this$0:Lcom/chelpus/XSupport;

    invoke-direct {p0}, Lde/robv/android/xposed/XC_MethodHook;-><init>()V

    return-void
.end method


# virtual methods
.method protected afterHookedMethod(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)V
    .locals 6
    .param p1, "param"    # Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 1189
    iget-object v3, p0, Lcom/chelpus/XSupport$39;->this$0:Lcom/chelpus/XSupport;

    invoke-virtual {v3}, Lcom/chelpus/XSupport;->loadPrefs()V

    .line 1190
    iget-object v3, p0, Lcom/chelpus/XSupport$39;->this$0:Lcom/chelpus/XSupport;

    iget-object v3, v3, Lcom/chelpus/XSupport;->forHide:Ljava/lang/Boolean;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/chelpus/XSupport$39;->this$0:Lcom/chelpus/XSupport;

    iget-object v3, v3, Lcom/chelpus/XSupport;->forHide:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    sget-boolean v3, Lcom/chelpus/XSupport;->enable:Z

    if-eqz v3, :cond_3

    sget-boolean v3, Lcom/chelpus/XSupport;->hide:Z

    if-eqz v3, :cond_3

    .line 1192
    iget-object v3, p0, Lcom/chelpus/XSupport$39;->this$0:Lcom/chelpus/XSupport;

    iget-object v3, v3, Lcom/chelpus/XSupport;->ctx:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 1196
    :cond_0
    invoke-virtual {p1}, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 1197
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    const/4 v1, 0x0

    .line 1198
    .local v1, "found":Landroid/content/pm/PackageInfo;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 1199
    .local v0, "app":Landroid/content/pm/PackageInfo;
    iget-object v4, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-class v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v5}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1200
    move-object v1, v0

    goto :goto_0

    .line 1203
    .end local v0    # "app":Landroid/content/pm/PackageInfo;
    :cond_2
    if-eqz v1, :cond_3

    .line 1205
    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1206
    invoke-virtual {p1, v2}, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->setResult(Ljava/lang/Object;)V

    .line 1209
    .end local v1    # "found":Landroid/content/pm/PackageInfo;
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_3
    return-void
.end method
