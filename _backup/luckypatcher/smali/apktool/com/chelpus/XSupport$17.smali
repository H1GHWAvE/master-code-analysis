.class Lcom/chelpus/XSupport$17;
.super Lde/robv/android/xposed/XC_MethodHook;
.source "XSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/chelpus/XSupport;->handleLoadPackage(Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/chelpus/XSupport;


# direct methods
.method constructor <init>(Lcom/chelpus/XSupport;)V
    .locals 0
    .param p1, "this$0"    # Lcom/chelpus/XSupport;

    .prologue
    .line 524
    iput-object p1, p0, Lcom/chelpus/XSupport$17;->this$0:Lcom/chelpus/XSupport;

    invoke-direct {p0}, Lde/robv/android/xposed/XC_MethodHook;-><init>()V

    return-void
.end method


# virtual methods
.method protected beforeHookedMethod(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)V
    .locals 12
    .param p1, "param"    # Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 529
    iget-object v7, p0, Lcom/chelpus/XSupport$17;->this$0:Lcom/chelpus/XSupport;

    invoke-virtual {v7}, Lcom/chelpus/XSupport;->loadPrefs()V

    .line 530
    sget-boolean v7, Lcom/chelpus/XSupport;->enable:Z

    if-eqz v7, :cond_1

    sget-boolean v7, Lcom/chelpus/XSupport;->patch3:Z

    if-eqz v7, :cond_1

    .line 531
    const-string v2, ""

    .line 533
    .local v2, "platform":Ljava/lang/String;
    :try_start_0
    iget-object v7, p0, Lcom/chelpus/XSupport$17;->this$0:Lcom/chelpus/XSupport;

    iget-object v7, v7, Lcom/chelpus/XSupport;->PMcontext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v9, "android"

    const/16 v10, 0x40

    invoke-virtual {v7, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    iget-object v7, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v9, 0x0

    aget-object v7, v7, v9

    invoke-virtual {v7}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/finsky/billing/iab/google/util/Base64;->encode([B)Ljava/lang/String;

    move-result-object v2

    .line 534
    const-string v7, "\n"

    const-string v9, ""

    invoke-virtual {v2, v7, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 541
    const/4 v6, 0x0

    .line 542
    .local v6, "skip":Z
    iget-object v7, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->args:[Ljava/lang/Object;

    aget-object v7, v7, v8

    check-cast v7, [Landroid/content/pm/Signature;

    move-object v4, v7

    check-cast v4, [Landroid/content/pm/Signature;

    .line 543
    .local v4, "sigs1":[Landroid/content/pm/Signature;
    iget-object v7, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->args:[Ljava/lang/Object;

    const/4 v9, 0x1

    aget-object v7, v7, v9

    check-cast v7, [Landroid/content/pm/Signature;

    move-object v5, v7

    check-cast v5, [Landroid/content/pm/Signature;

    .line 545
    .local v5, "sigs2":[Landroid/content/pm/Signature;
    if-eqz v4, :cond_2

    array-length v7, v4

    if-lez v7, :cond_2

    .line 546
    array-length v9, v4

    move v7, v8

    :goto_0
    if-ge v7, v9, :cond_2

    aget-object v3, v4, v7

    .line 547
    .local v3, "sig":Landroid/content/pm/Signature;
    invoke-virtual {v3}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/finsky/billing/iab/google/util/Base64;->encode([B)Ljava/lang/String;

    move-result-object v0

    .line 548
    .local v0, "char1":Ljava/lang/String;
    const-string v10, "\n"

    const-string v11, ""

    invoke-virtual {v0, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 550
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 551
    const/4 v6, 0x1

    .line 546
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 535
    .end local v0    # "char1":Ljava/lang/String;
    .end local v3    # "sig":Landroid/content/pm/Signature;
    .end local v4    # "sigs1":[Landroid/content/pm/Signature;
    .end local v5    # "sigs2":[Landroid/content/pm/Signature;
    .end local v6    # "skip":Z
    :catch_0
    move-exception v1

    .line 536
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 579
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "platform":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 556
    .restart local v2    # "platform":Ljava/lang/String;
    .restart local v4    # "sigs1":[Landroid/content/pm/Signature;
    .restart local v5    # "sigs2":[Landroid/content/pm/Signature;
    .restart local v6    # "skip":Z
    :cond_2
    if-eqz v5, :cond_4

    array-length v7, v5

    if-lez v7, :cond_4

    .line 558
    array-length v9, v5

    move v7, v8

    :goto_2
    if-ge v7, v9, :cond_4

    aget-object v3, v5, v7

    .line 560
    .restart local v3    # "sig":Landroid/content/pm/Signature;
    invoke-virtual {v3}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/finsky/billing/iab/google/util/Base64;->encode([B)Ljava/lang/String;

    move-result-object v0

    .line 561
    .restart local v0    # "char1":Ljava/lang/String;
    const-string v10, "\n"

    const-string v11, ""

    invoke-virtual {v0, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 562
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 563
    const/4 v6, 0x1

    .line 558
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 573
    .end local v0    # "char1":Ljava/lang/String;
    .end local v3    # "sig":Landroid/content/pm/Signature;
    :cond_4
    if-nez v6, :cond_1

    .line 574
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p1, v7}, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->setResult(Ljava/lang/Object;)V

    goto :goto_1
.end method
