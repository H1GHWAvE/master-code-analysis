.class public Lcom/chelpus/SimpleXPath;
.super Ljava/lang/Object;
.source "SimpleXPath.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chelpus/SimpleXPath$XNode;
    }
.end annotation


# static fields
.field public static final XPATH_SEPARATOR:Ljava/lang/String; = "/"


# instance fields
.field mDocument:Lorg/w3c/dom/Document;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 3
    .param p1, "xmlFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/parsers/ParserConfigurationException;,
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    .line 168
    .local v1, "dbFactory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 169
    .local v0, "dBuilder":Ljavax/xml/parsers/DocumentBuilder;
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, v2}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v2

    iput-object v2, p0, Lcom/chelpus/SimpleXPath;->mDocument:Lorg/w3c/dom/Document;

    .line 170
    return-void
.end method

.method private getNodes(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/util/List;)V
    .locals 8
    .param p1, "root"    # Lorg/w3c/dom/Node;
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/Node;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/chelpus/SimpleXPath$XNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/chelpus/SimpleXPath$XNode;>;"
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 235
    const-string v6, "/"

    const/4 v7, 0x2

    invoke-virtual {p2, v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 236
    .local v3, "node":[Ljava/lang/String;
    array-length v6, v3

    if-ne v6, v4, :cond_1

    move v1, v4

    .line 237
    .local v1, "isFinishing":Z
    :goto_0
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v2

    .line 239
    .local v2, "n":Lorg/w3c/dom/Node;
    :goto_1
    if-eqz v2, :cond_3

    .line 240
    aget-object v6, v3, v5

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 241
    if-eqz v1, :cond_2

    .line 242
    new-instance v6, Lcom/chelpus/SimpleXPath$XNode;

    invoke-direct {v6, p0, v2}, Lcom/chelpus/SimpleXPath$XNode;-><init>(Lcom/chelpus/SimpleXPath;Lorg/w3c/dom/Node;)V

    invoke-interface {p3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    :cond_0
    :goto_2
    :try_start_0
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_1

    .end local v1    # "isFinishing":Z
    .end local v2    # "n":Lorg/w3c/dom/Node;
    :cond_1
    move v1, v5

    .line 236
    goto :goto_0

    .line 244
    .restart local v1    # "isFinishing":Z
    .restart local v2    # "n":Lorg/w3c/dom/Node;
    :cond_2
    aget-object v6, v3, v4

    invoke-direct {p0, v2, v6, p3}, Lcom/chelpus/SimpleXPath;->getNodes(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_2

    .line 249
    :catch_0
    move-exception v0

    .line 250
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    .line 251
    goto :goto_1

    .line 253
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    return-void
.end method


# virtual methods
.method public getRoot()Lorg/w3c/dom/Node;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/chelpus/SimpleXPath;->mDocument:Lorg/w3c/dom/Document;

    return-object v0
.end method

.method public getXPathNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lcom/chelpus/SimpleXPath$XNode;
    .locals 9
    .param p1, "root"    # Lorg/w3c/dom/Node;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 188
    const/4 v4, 0x0

    .line 189
    .local v4, "result":Lcom/chelpus/SimpleXPath$XNode;
    const-string v7, "/"

    const/4 v8, 0x2

    invoke-virtual {p2, v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 190
    .local v3, "node":[Ljava/lang/String;
    array-length v7, v3

    if-ne v7, v5, :cond_1

    move v1, v5

    .line 191
    .local v1, "isFinishing":Z
    :goto_0
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v2

    .line 193
    .local v2, "n":Lorg/w3c/dom/Node;
    :goto_1
    if-eqz v2, :cond_0

    .line 194
    aget-object v7, v3, v6

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 195
    if-eqz v1, :cond_2

    .line 196
    new-instance v4, Lcom/chelpus/SimpleXPath$XNode;

    .end local v4    # "result":Lcom/chelpus/SimpleXPath$XNode;
    invoke-direct {v4, p0, v2}, Lcom/chelpus/SimpleXPath$XNode;-><init>(Lcom/chelpus/SimpleXPath;Lorg/w3c/dom/Node;)V

    .line 210
    .restart local v4    # "result":Lcom/chelpus/SimpleXPath$XNode;
    :cond_0
    :goto_2
    return-object v4

    .end local v1    # "isFinishing":Z
    .end local v2    # "n":Lorg/w3c/dom/Node;
    :cond_1
    move v1, v6

    .line 190
    goto :goto_0

    .line 198
    .restart local v1    # "isFinishing":Z
    .restart local v2    # "n":Lorg/w3c/dom/Node;
    :cond_2
    aget-object v5, v3, v5

    invoke-virtual {p0, v2, v5}, Lcom/chelpus/SimpleXPath;->getXPathNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lcom/chelpus/SimpleXPath$XNode;

    move-result-object v4

    .line 201
    goto :goto_2

    .line 204
    :cond_3
    :try_start_0
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_1

    .line 205
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    .line 207
    goto :goto_1
.end method

.method public getXPathNodes(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "root"    # Lorg/w3c/dom/Node;
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/Node;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/chelpus/SimpleXPath$XNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 222
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/chelpus/SimpleXPath$XNode;>;"
    invoke-direct {p0, p1, p2, v0}, Lcom/chelpus/SimpleXPath;->getNodes(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/util/List;)V

    .line 224
    return-object v0
.end method
