.class Lcom/chelpus/XSupport$34;
.super Lde/robv/android/xposed/XC_MethodHook;
.source "XSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/chelpus/XSupport;->handleLoadPackage(Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/chelpus/XSupport;


# direct methods
.method constructor <init>(Lcom/chelpus/XSupport;)V
    .locals 0
    .param p1, "this$0"    # Lcom/chelpus/XSupport;

    .prologue
    .line 1018
    iput-object p1, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    invoke-direct {p0}, Lde/robv/android/xposed/XC_MethodHook;-><init>()V

    return-void
.end method


# virtual methods
.method protected afterHookedMethod(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)V
    .locals 13
    .param p1, "param"    # Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1021
    iget-object v6, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    iget-object v5, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->args:[Ljava/lang/Object;

    aget-object v5, v5, v10

    check-cast v5, Landroid/content/Context;

    iput-object v5, v6, Lcom/chelpus/XSupport;->ctx:Landroid/content/Context;

    .line 1023
    iget-object v5, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    invoke-virtual {v5}, Lcom/chelpus/XSupport;->loadPrefs()V

    .line 1024
    sget-boolean v5, Lcom/chelpus/XSupport;->enable:Z

    if-eqz v5, :cond_0

    sget-boolean v5, Lcom/chelpus/XSupport;->hide:Z

    if-eqz v5, :cond_0

    .line 1025
    iget-object v5, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v5, Lcom/chelpus/XSupport;->forHide:Ljava/lang/Boolean;

    .line 1027
    iget-object v5, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    iget-object v5, v5, Lcom/chelpus/XSupport;->ctx:Landroid/content/Context;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    iget-object v5, v5, Lcom/chelpus/XSupport;->ctx:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-class v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v6}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    iget-object v5, v5, Lcom/chelpus/XSupport;->ctx:Landroid/content/Context;

    .line 1028
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "de.robv.android.xposed.installer"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    iget-object v5, v5, Lcom/chelpus/XSupport;->ctx:Landroid/content/Context;

    .line 1029
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "supersu"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    iget-object v5, v5, Lcom/chelpus/XSupport;->ctx:Landroid/content/Context;

    .line 1030
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "superuser"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    iget-object v5, v5, Lcom/chelpus/XSupport;->ctx:Landroid/content/Context;

    .line 1031
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "pro.burgerz.wsm.manager"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1032
    iget-object v5, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v5, Lcom/chelpus/XSupport;->forHide:Ljava/lang/Boolean;

    .line 1034
    const/4 v1, 0x0

    .line 1036
    .local v1, "info":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    iget-object v5, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->thisObject:Ljava/lang/Object;

    const-string v6, "getApplicationInfo"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    iget-object v9, v9, Lcom/chelpus/XSupport;->ctx:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Lde/robv/android/xposed/XposedHelpers;->callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "info":Landroid/content/pm/ApplicationInfo;
    check-cast v1, Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1043
    .restart local v1    # "info":Landroid/content/pm/ApplicationInfo;
    if-eqz v1, :cond_2

    .line 1044
    iget v5, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_1

    .line 1045
    iget-object v5, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v5, Lcom/chelpus/XSupport;->forHide:Ljava/lang/Boolean;

    .line 1068
    .end local v1    # "info":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    return-void

    .line 1037
    :catch_0
    move-exception v0

    .line 1038
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1039
    iget-object v5, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v5, Lcom/chelpus/XSupport;->forHide:Ljava/lang/Boolean;

    goto :goto_0

    .line 1048
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "info":Landroid/content/pm/ApplicationInfo;
    :cond_1
    iget-object v5, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v5, Lcom/chelpus/XSupport;->forHide:Ljava/lang/Boolean;

    .line 1052
    :cond_2
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1053
    .local v2, "intent_home":Landroid/content/Intent;
    const-string v5, "android.intent.category.HOME"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1054
    const-string v5, "android.intent.category.DEFAULT"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1055
    iget-object v5, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->thisObject:Ljava/lang/Object;

    const-string v6, "queryIntentActivities"

    new-array v7, v12, [Ljava/lang/Object;

    aput-object v2, v7, v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v5, v6, v7}, Lde/robv/android/xposed/XposedHelpers;->callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 1057
    .local v4, "launcherList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 1058
    .local v3, "launcher":Landroid/content/pm/ResolveInfo;
    iget-object v6, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v7, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    iget-object v7, v7, Lcom/chelpus/XSupport;->ctx:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1060
    iget-object v5, p0, Lcom/chelpus/XSupport$34;->this$0:Lcom/chelpus/XSupport;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v5, Lcom/chelpus/XSupport;->forHide:Ljava/lang/Boolean;

    goto :goto_0
.end method
