.class public Lcom/chelpus/root/utils/createapkcustom;
.super Ljava/lang/Object;
.source "createapkcustom.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chelpus/root/utils/createapkcustom$Decompress;
    }
.end annotation


# static fields
.field static final BUFFER:I = 0x800

.field private static final all:I = 0x4

.field public static appdir:Ljava/lang/String; = null

.field private static final armeabi:I = 0x0

.field private static final armeabiv7a:I = 0x1

.field private static final beginTag:I = 0x0

.field public static classesFiles:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static final classesTag:I = 0x1

.field public static crkapk:Ljava/io/File; = null

.field public static dir:Ljava/lang/String; = null

.field public static dir2:Ljava/lang/String; = null

.field private static final endTag:I = 0x4

.field private static final fileInApkTag:I = 0xa

.field public static goodResult:Z = false

.field private static group:Ljava/lang/String; = null

.field private static final libTagALL:I = 0x2

.field private static final libTagARMEABI:I = 0x6

.field private static final libTagARMEABIV7A:I = 0x7

.field private static final libTagMIPS:I = 0x8

.field private static final libTagx86:I = 0x9

.field private static libs:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static localFile2:Ljava/io/File; = null

.field public static manualpatch:Z = false

.field private static final mips:I = 0x2

.field public static multidex:Z = false

.field public static multilib_patch:Z = false

.field public static packageName:Ljava/lang/String; = null

.field private static final packageTag:I = 0x5

.field private static pat:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;",
            ">;"
        }
    .end annotation
.end field

.field private static patchedLibs:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static patchteil:Z = false

.field private static print:Ljava/io/PrintStream; = null

.field public static sddir:Ljava/lang/String; = null

.field private static search:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field private static ser:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;",
            ">;"
        }
    .end annotation
.end field

.field public static tag:I = 0x0

.field public static tooldir:Ljava/lang/String; = null

.field public static unpack:Z = false

.field private static final x86:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 43
    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    .line 44
    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    .line 45
    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    .line 46
    sput-boolean v1, Lcom/chelpus/root/utils/createapkcustom;->patchteil:Z

    .line 47
    sput-boolean v1, Lcom/chelpus/root/utils/createapkcustom;->unpack:Z

    .line 48
    sput-boolean v1, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    .line 49
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->dir:Ljava/lang/String;

    .line 50
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->dir2:Ljava/lang/String;

    .line 51
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    .line 52
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->appdir:Ljava/lang/String;

    .line 53
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->tooldir:Ljava/lang/String;

    .line 54
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->packageName:Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    .line 77
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    .line 79
    sput-boolean v1, Lcom/chelpus/root/utils/createapkcustom;->multidex:Z

    .line 80
    sput-boolean v1, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    .line 81
    sput-boolean v1, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1197
    return-void
.end method

.method static synthetic access$000()Ljava/io/PrintStream;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    return-object v0
.end method

.method private static final calcChecksum([BI)V
    .locals 5
    .param p0, "paramArrayOfByte"    # [B
    .param p1, "j"    # I

    .prologue
    .line 1013
    new-instance v1, Ljava/util/zip/Adler32;

    invoke-direct {v1}, Ljava/util/zip/Adler32;-><init>()V

    .line 1014
    .local v1, "localAdler32":Ljava/util/zip/Adler32;
    const/16 v2, 0xc

    array-length v3, p0

    add-int/lit8 v4, p1, 0xc

    sub-int/2addr v3, v4

    invoke-virtual {v1, p0, v2, v3}, Ljava/util/zip/Adler32;->update([BII)V

    .line 1015
    invoke-virtual {v1}, Ljava/util/zip/Adler32;->getValue()J

    move-result-wide v2

    long-to-int v0, v2

    .line 1016
    .local v0, "i":I
    add-int/lit8 v2, p1, 0x8

    int-to-byte v3, v0

    aput-byte v3, p0, v2

    .line 1017
    add-int/lit8 v2, p1, 0x9

    shr-int/lit8 v3, v0, 0x8

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 1018
    add-int/lit8 v2, p1, 0xa

    shr-int/lit8 v3, v0, 0x10

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 1019
    add-int/lit8 v2, p1, 0xb

    shr-int/lit8 v3, v0, 0x18

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 1020
    return-void
.end method

.method private static final calcSignature([BI)V
    .locals 8
    .param p0, "paramArrayOfByte"    # [B
    .param p1, "j"    # I

    .prologue
    const/16 v7, 0x14

    .line 1026
    :try_start_0
    const-string v4, "SHA-1"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 1033
    .local v2, "localMessageDigest":Ljava/security/MessageDigest;
    const/16 v4, 0x20

    array-length v5, p0

    add-int/lit8 v6, p1, 0x20

    sub-int/2addr v5, v6

    invoke-virtual {v2, p0, v4, v5}, Ljava/security/MessageDigest;->update([BII)V

    .line 1036
    add-int/lit8 v4, p1, 0xc

    const/16 v5, 0x14

    :try_start_1
    invoke-virtual {v2, p0, v4, v5}, Ljava/security/MessageDigest;->digest([BII)I

    move-result v0

    .line 1037
    .local v0, "i":I
    if-eq v0, v7, :cond_0

    .line 1038
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unexpected digest write:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "bytes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Ljava/security/DigestException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1040
    .end local v0    # "i":I
    :catch_0
    move-exception v1

    .line 1042
    .local v1, "localDigestException":Ljava/security/DigestException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 1028
    .end local v1    # "localDigestException":Ljava/security/DigestException;
    .end local v2    # "localMessageDigest":Ljava/security/MessageDigest;
    :catch_1
    move-exception v3

    .line 1030
    .local v3, "localNoSuchAlgorithmException":Ljava/security/NoSuchAlgorithmException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 1044
    .end local v3    # "localNoSuchAlgorithmException":Ljava/security/NoSuchAlgorithmException;
    .restart local v0    # "i":I
    .restart local v2    # "localMessageDigest":Ljava/security/MessageDigest;
    :cond_0
    return-void
.end method

.method public static clearTemp()V
    .locals 7

    .prologue
    .line 1127
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Modified/classes.dex.apk"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1131
    .local v2, "tmp":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1132
    .local v1, "tempdex":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1133
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/tmp/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1134
    new-instance v1, Ljava/io/File;

    .end local v1    # "tempdex":Ljava/io/File;
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1135
    .restart local v1    # "tempdex":Ljava/io/File;
    new-instance v3, Lcom/chelpus/Utils;

    const-string v4, "createcustompatch"

    invoke-direct {v3, v4}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    .line 1136
    .local v3, "ut":Lcom/chelpus/Utils;
    invoke-virtual {v3, v1}, Lcom/chelpus/Utils;->deleteFolder(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1140
    .end local v1    # "tempdex":Ljava/io/File;
    .end local v3    # "ut":Lcom/chelpus/Utils;
    :goto_0
    return-void

    .line 1137
    :catch_0
    move-exception v0

    .line 1138
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static extractFile(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "apk"    # Ljava/io/File;
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    .line 1155
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 1156
    .local v2, "zipFile":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/tmp/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1158
    .local v1, "unzipLocation":Ljava/lang/String;
    new-instance v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;

    invoke-direct {v0, v2, v1}, Lcom/chelpus/root/utils/createapkcustom$Decompress;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159
    .local v0, "d":Lcom/chelpus/root/utils/createapkcustom$Decompress;
    invoke-virtual {v0, p1}, Lcom/chelpus/root/utils/createapkcustom$Decompress;->unzip(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static extractLibs(Ljava/io/File;)V
    .locals 6
    .param p0, "apk"    # Ljava/io/File;

    .prologue
    .line 1143
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 1144
    .local v3, "zipFile":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/tmp/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1145
    .local v2, "unzipLocation":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/tmp/lib/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1146
    .local v1, "tmp":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1148
    new-instance v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;

    invoke-direct {v0, v3, v2}, Lcom/chelpus/root/utils/createapkcustom$Decompress;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1149
    .local v0, "d":Lcom/chelpus/root/utils/createapkcustom$Decompress;
    invoke-virtual {v0}, Lcom/chelpus/root/utils/createapkcustom$Decompress;->unzip()V

    .line 1152
    .end local v0    # "d":Lcom/chelpus/root/utils/createapkcustom$Decompress;
    :cond_0
    return-void
.end method

.method public static fixadler(Ljava/io/File;)V
    .locals 5
    .param p0, "destFile"    # Ljava/io/File;

    .prologue
    .line 996
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 997
    .local v2, "localFileInputStream":Ljava/io/FileInputStream;
    invoke-virtual {v2}, Ljava/io/FileInputStream;->available()I

    move-result v4

    new-array v0, v4, [B

    .line 998
    .local v0, "arrayOfByte":[B
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I

    .line 999
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/chelpus/root/utils/createapkcustom;->calcSignature([BI)V

    .line 1000
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/chelpus/root/utils/createapkcustom;->calcChecksum([BI)V

    .line 1001
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 1002
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1003
    .local v3, "localFileOutputStream":Ljava/io/FileOutputStream;
    invoke-virtual {v3, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 1004
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1010
    .end local v0    # "arrayOfByte":[B
    .end local v2    # "localFileInputStream":Ljava/io/FileInputStream;
    .end local v3    # "localFileOutputStream":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 1006
    :catch_0
    move-exception v1

    .line 1008
    .local v1, "localException":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getClassesDex()V
    .locals 7

    .prologue
    .line 882
    :try_start_0
    new-instance v0, Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->appdir:Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 883
    .local v0, "apk":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/Modified/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/chelpus/root/utils/createapkcustom;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".apk"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v4, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    .line 884
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-static {v0, v4}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 885
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-static {v4}, Lcom/chelpus/root/utils/createapkcustom;->unzip(Ljava/io/File;)V

    .line 886
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    new-instance v4, Ljava/io/FileNotFoundException;

    invoke-direct {v4}, Ljava/io/FileNotFoundException;-><init>()V

    throw v4
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 894
    :catch_0
    move-exception v3

    .line 895
    .local v3, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v5, "Error LP: unzip classes.dex fault!\n\n"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 901
    .end local v3    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_1
    :goto_0
    return-void

    .line 887
    :cond_2
    :try_start_1
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 888
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 889
    .local v1, "cl":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_3

    new-instance v4, Ljava/io/FileNotFoundException;

    invoke-direct {v4}, Ljava/io/FileNotFoundException;-><init>()V

    throw v4
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 897
    .end local v1    # "cl":Ljava/io/File;
    :catch_1
    move-exception v2

    .line 898
    .local v2, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Extract classes.dex error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getFileFromApk(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "file"    # Ljava/lang/String;

    .prologue
    .line 981
    :try_start_0
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->appdir:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 982
    .local v0, "apk":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Modified/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".apk"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    .line 983
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-static {v0, v2}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 985
    :cond_0
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-static {v2, p0}, Lcom/chelpus/root/utils/createapkcustom;->extractFile(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 990
    .end local v0    # "apk":Ljava/io/File;
    :goto_0
    return-object v2

    .line 987
    :catch_0
    move-exception v1

    .line 988
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Lib select error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 990
    const-string v2, ""

    goto :goto_0
.end method

.method public static main([Ljava/lang/String;)Ljava/lang/String;
    .locals 55
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 85
    new-instance v39, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;

    const-string v2, "System.out"

    move-object/from16 v0, v39

    invoke-direct {v0, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;-><init>(Ljava/lang/String;)V

    .line 86
    .local v39, "pout":Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;
    new-instance v2, Ljava/io/PrintStream;

    move-object/from16 v0, v39

    invoke-direct {v2, v0}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    .line 88
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "SU Java-Code Running!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 89
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 90
    const/4 v2, 0x0

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->packageName:Ljava/lang/String;

    .line 91
    const/4 v2, 0x2

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->appdir:Ljava/lang/String;

    .line 92
    const/4 v2, 0x3

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    .line 93
    const/4 v2, 0x4

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->tooldir:Ljava/lang/String;

    .line 94
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->clearTemp()V

    .line 95
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    .line 97
    const-string v26, ""

    .line 98
    .local v26, "finalText":Ljava/lang/String;
    const-string v12, ""

    .line 100
    .local v12, "beginText":Ljava/lang/String;
    const/16 v23, 0x0

    .local v23, "error":Z
    const/16 v22, 0x0

    .line 101
    .local v22, "end":Z
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->getClassesDex()V

    .line 108
    :try_start_0
    new-instance v27, Ljava/io/FileInputStream;

    const/4 v2, 0x1

    aget-object v2, p0, v2

    move-object/from16 v0, v27

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 109
    .local v27, "fis":Ljava/io/FileInputStream;
    new-instance v28, Ljava/io/InputStreamReader;

    const-string v2, "UTF-8"

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-direct {v0, v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 110
    .local v28, "isr":Ljava/io/InputStreamReader;
    new-instance v13, Ljava/io/BufferedReader;

    move-object/from16 v0, v28

    invoke-direct {v13, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 111
    .local v13, "br":Ljava/io/BufferedReader;
    const-string v16, ""

    .line 112
    .local v16, "data":Ljava/lang/String;
    const/16 v2, 0x3e8

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v47, v0

    .line 113
    .local v47, "txtdata":[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v38, v0

    const/4 v2, 0x0

    const-string v7, ""

    aput-object v7, v38, v2

    .line 115
    .local v38, "orhex":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 117
    .local v3, "byteOrig":[B
    const/4 v4, 0x0

    .line 119
    .local v4, "mask":[I
    const/16 v42, 0x1

    .local v42, "result":Z
    const/16 v45, 0x1

    .local v45, "sumresult":Z
    const/16 v32, 0x0

    .local v32, "libr":Z
    const/4 v11, 0x0

    .local v11, "begin":Z
    const/16 v36, 0x0

    .local v36, "mark_search":Z
    const/16 v25, 0x0

    .line 120
    .local v25, "fileInApk":Z
    const-string v48, ""

    .line 121
    .local v48, "value1":Ljava/lang/String;
    const-string v49, ""

    .line 122
    .local v49, "value2":Ljava/lang/String;
    const-string v50, ""

    .line 123
    .local v50, "value3":Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    .line 124
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    .line 125
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    .line 126
    const/16 v40, 0x0

    .line 128
    .local v40, "r":I
    :goto_0
    invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_5d

    .line 130
    const-string v2, ""

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 131
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->packageName:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v2}, Lcom/chelpus/Utils;->apply_TAGS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 133
    :cond_0
    aput-object v16, v47, v40

    .line 135
    if-eqz v11, :cond_2

    aget-object v2, v47, v40

    const-string v7, "["

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    aget-object v2, v47, v40

    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    aget-object v2, v47, v40

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 137
    :cond_1
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 138
    const/4 v11, 0x0

    .line 140
    :cond_2
    if-eqz v11, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v7, v47, v40

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 141
    :cond_3
    aget-object v2, v47, v40

    const-string v7, "["

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    aget-object v2, v47, v40

    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 142
    sget v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    packed-switch v2, :pswitch_data_0

    .line 281
    :cond_4
    :goto_1
    :pswitch_0
    aget-object v2, v47, v40

    const-string v7, "[BEGIN]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 282
    const/4 v2, 0x0

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 283
    const/4 v11, 0x1

    .line 286
    :cond_5
    aget-object v2, v47, v40

    const-string v7, "[CLASSES]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    aget-object v2, v47, v40

    const-string v7, "[ODEX]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 287
    :cond_6
    const/4 v2, 0x1

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 289
    :cond_7
    aget-object v2, v47, v40

    const-string v7, "[PACKAGE]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 290
    const/4 v2, 0x5

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 293
    :cond_8
    if-eqz v32, :cond_9

    .line 294
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 295
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 297
    :try_start_1
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 298
    .local v29, "json_obj":Lorg/json/JSONObject;
    const-string v2, "name"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v48

    .line 302
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_2
    :try_start_2
    sget v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    packed-switch v2, :pswitch_data_1

    .line 325
    :goto_3
    :pswitch_1
    const/16 v32, 0x0

    .line 327
    :cond_9
    if-eqz v25, :cond_a

    .line 328
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 329
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 331
    :try_start_3
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 332
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "name"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v48

    .line 336
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_4
    :try_start_4
    invoke-static/range {v48 .. v48}, Lcom/chelpus/root/utils/createapkcustom;->getFileFromApk(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 337
    .local v24, "file":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, v24

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 338
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, v24

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    .line 342
    :goto_5
    const/16 v25, 0x0

    .line 344
    .end local v24    # "file":Ljava/lang/String;
    :cond_a
    aget-object v2, v47, v40

    const-string v7, "[LIB-ARMEABI]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 345
    const/4 v2, 0x6

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 346
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->unpack:Z

    .line 347
    const/16 v25, 0x0

    .line 348
    const/16 v32, 0x1

    .line 350
    :cond_b
    aget-object v2, v47, v40

    const-string v7, "[LIB-ARMEABI-V7A]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 351
    const/4 v2, 0x7

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 352
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->unpack:Z

    .line 353
    const/16 v25, 0x0

    .line 354
    const/16 v32, 0x1

    .line 356
    :cond_c
    aget-object v2, v47, v40

    const-string v7, "[LIB-MIPS]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 357
    const/16 v2, 0x8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 358
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->unpack:Z

    .line 359
    const/16 v25, 0x0

    .line 360
    const/16 v32, 0x1

    .line 362
    :cond_d
    aget-object v2, v47, v40

    const-string v7, "[LIB-X86]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 363
    const/16 v2, 0x9

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 364
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->unpack:Z

    .line 365
    const/16 v25, 0x0

    .line 366
    const/16 v32, 0x1

    .line 368
    :cond_e
    aget-object v2, v47, v40

    const-string v7, "[LIB]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 369
    const/4 v2, 0x2

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 370
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->unpack:Z

    .line 371
    const/16 v25, 0x0

    .line 372
    const/16 v32, 0x1

    .line 375
    :cond_f
    aget-object v2, v47, v40

    const-string v7, "[FILE_IN_APK]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 376
    const/16 v2, 0xa

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 377
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->unpack:Z

    .line 378
    const/16 v32, 0x0

    .line 379
    const/16 v25, 0x1

    .line 384
    :cond_10
    aget-object v2, v47, v40

    const-string v7, "group"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2

    move-result v2

    if-eqz v2, :cond_11

    .line 386
    :try_start_5
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 387
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "group"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_2

    .line 393
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :cond_11
    :goto_6
    :try_start_6
    aget-object v2, v47, v40

    const-string v7, "original"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 395
    if-eqz v36, :cond_12

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/createapkcustom;->searchProcess(Ljava/util/ArrayList;)Z
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_2

    move-result v45

    const/16 v36, 0x0

    .line 397
    :cond_12
    :try_start_7
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 398
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "original"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_2

    move-result-object v48

    .line 402
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_7
    :try_start_8
    invoke-virtual/range {v48 .. v48}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v48

    .line 404
    const-string v2, "[ \t]+"

    move-object/from16 v0, v48

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v38, v0

    .line 405
    const-string v2, "[ \t]+"

    move-object/from16 v0, v48

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v38

    .line 406
    move-object/from16 v0, v38

    array-length v2, v0

    new-array v4, v2, [I

    .line 407
    move-object/from16 v0, v38

    array-length v2, v0

    new-array v3, v2, [B
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_2

    .line 409
    const/16 v46, 0x0

    .local v46, "t":I
    :goto_8
    :try_start_9
    move-object/from16 v0, v38

    array-length v2, v0

    move/from16 v0, v46

    if-ge v0, v2, :cond_2e

    .line 410
    aget-object v2, v38, v46

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_13

    aget-object v2, v38, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_13

    const/16 v23, 0x1

    const-string v2, "60"

    aput-object v2, v38, v46

    .line 411
    :cond_13
    aget-object v2, v38, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_14

    aget-object v2, v38, v46

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2d

    :cond_14
    const-string v2, "60"

    aput-object v2, v38, v46

    const/4 v2, 0x1

    aput v2, v4, v46

    .line 413
    :goto_9
    aget-object v2, v38, v46

    const-string v7, "W"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_15

    aget-object v2, v38, v46

    const-string v7, "w"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_15

    aget-object v2, v38, v46

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_15

    aget-object v2, v38, v46

    const-string v7, "r"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 414
    :cond_15
    aget-object v2, v38, v46

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "w"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "r"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v37

    .line 415
    .local v37, "or":Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v51

    .local v51, "y":I
    add-int/lit8 v51, v51, 0x2

    .line 416
    aput v51, v4, v46

    const-string v2, "60"

    aput-object v2, v38, v46

    .line 417
    .end local v37    # "or":Ljava/lang/String;
    .end local v51    # "y":I
    :cond_16
    aget-object v2, v38, v46

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v3, v46
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_2

    .line 409
    add-int/lit8 v46, v46, 0x1

    goto/16 :goto_8

    .line 145
    .end local v46    # "t":I
    :pswitch_2
    :try_start_a
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 146
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->getClassesDex()V

    .line 147
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    if-eqz v2, :cond_1a

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1a

    .line 149
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v7, 0x1

    if-le v2, v7, :cond_17

    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multidex:Z

    .line 150
    :cond_17
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_18
    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/io/File;

    .line 151
    .local v14, "cl":Ljava/io/File;
    sput-object v14, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    .line 152
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------------"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 153
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Patch for "

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v52, ":"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 154
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------------\n"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 157
    sget-boolean v7, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    if-nez v7, :cond_19

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/createapkcustom;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v42

    .line 158
    :cond_19
    if-nez v42, :cond_18

    const/16 v45, 0x0

    goto :goto_a

    .line 162
    .end local v14    # "cl":Ljava/io/File;
    :cond_1a
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multidex:Z

    .line 163
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    .line 164
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 165
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 166
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_2

    goto/16 :goto_1

    .line 615
    .end local v3    # "byteOrig":[B
    .end local v4    # "mask":[I
    .end local v11    # "begin":Z
    .end local v13    # "br":Ljava/io/BufferedReader;
    .end local v16    # "data":Ljava/lang/String;
    .end local v25    # "fileInApk":Z
    .end local v27    # "fis":Ljava/io/FileInputStream;
    .end local v28    # "isr":Ljava/io/InputStreamReader;
    .end local v32    # "libr":Z
    .end local v36    # "mark_search":Z
    .end local v38    # "orhex":[Ljava/lang/String;
    .end local v40    # "r":I
    .end local v42    # "result":Z
    .end local v45    # "sumresult":Z
    .end local v47    # "txtdata":[Ljava/lang/String;
    .end local v48    # "value1":Ljava/lang/String;
    .end local v49    # "value2":Ljava/lang/String;
    .end local v50    # "value3":Ljava/lang/String;
    :catch_0
    move-exception v19

    .line 617
    .local v19, "e1":Ljava/io/FileNotFoundException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Custom Patch not Found!\n"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 623
    .end local v19    # "e1":Ljava/io/FileNotFoundException;
    :goto_b
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->clearTemp()V

    .line 625
    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->allresult:Ljava/lang/String;

    move-object/from16 v42, v0

    .line 627
    .local v42, "result":Ljava/lang/String;
    :try_start_b
    invoke-virtual/range {v39 .. v39}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_12

    .line 632
    :goto_c
    return-object v42

    .line 172
    .restart local v3    # "byteOrig":[B
    .restart local v4    # "mask":[I
    .restart local v11    # "begin":Z
    .restart local v13    # "br":Ljava/io/BufferedReader;
    .restart local v16    # "data":Ljava/lang/String;
    .restart local v25    # "fileInApk":Z
    .restart local v27    # "fis":Ljava/io/FileInputStream;
    .restart local v28    # "isr":Ljava/io/InputStreamReader;
    .restart local v32    # "libr":Z
    .restart local v36    # "mark_search":Z
    .restart local v38    # "orhex":[Ljava/lang/String;
    .restart local v40    # "r":I
    .local v42, "result":Z
    .restart local v45    # "sumresult":Z
    .restart local v47    # "txtdata":[Ljava/lang/String;
    .restart local v48    # "value1":Ljava/lang/String;
    .restart local v49    # "value2":Ljava/lang/String;
    .restart local v50    # "value3":Ljava/lang/String;
    :pswitch_3
    :try_start_c
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "---------------------------"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 173
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch for file from apk\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v52, Ljava/lang/StringBuilder;

    invoke-direct/range {v52 .. v52}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v53, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v52

    const-string v53, "/tmp"

    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v52

    invoke-virtual/range {v52 .. v52}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v52

    const-string v53, ""

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 174
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "---------------------------\n"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 176
    sget-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    if-nez v2, :cond_1b

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/createapkcustom;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v42

    .line 177
    :cond_1b
    if-nez v42, :cond_1c

    const/16 v45, 0x0

    .line 179
    :cond_1c
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 182
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 183
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I
    :try_end_c
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_2

    goto/16 :goto_1

    .line 618
    .end local v3    # "byteOrig":[B
    .end local v4    # "mask":[I
    .end local v11    # "begin":Z
    .end local v13    # "br":Ljava/io/BufferedReader;
    .end local v16    # "data":Ljava/lang/String;
    .end local v25    # "fileInApk":Z
    .end local v27    # "fis":Ljava/io/FileInputStream;
    .end local v28    # "isr":Ljava/io/InputStreamReader;
    .end local v32    # "libr":Z
    .end local v36    # "mark_search":Z
    .end local v38    # "orhex":[Ljava/lang/String;
    .end local v40    # "r":I
    .end local v42    # "result":Z
    .end local v45    # "sumresult":Z
    .end local v47    # "txtdata":[Ljava/lang/String;
    .end local v48    # "value1":Ljava/lang/String;
    .end local v49    # "value2":Ljava/lang/String;
    .end local v50    # "value3":Ljava/lang/String;
    :catch_1
    move-exception v20

    .line 619
    .local v20, "e2":Ljava/io/IOException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch process Error LP: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 186
    .end local v20    # "e2":Ljava/io/IOException;
    .restart local v3    # "byteOrig":[B
    .restart local v4    # "mask":[I
    .restart local v11    # "begin":Z
    .restart local v13    # "br":Ljava/io/BufferedReader;
    .restart local v16    # "data":Ljava/lang/String;
    .restart local v25    # "fileInApk":Z
    .restart local v27    # "fis":Ljava/io/FileInputStream;
    .restart local v28    # "isr":Ljava/io/InputStreamReader;
    .restart local v32    # "libr":Z
    .restart local v36    # "mark_search":Z
    .restart local v38    # "orhex":[Ljava/lang/String;
    .restart local v40    # "r":I
    .restart local v42    # "result":Z
    .restart local v45    # "sumresult":Z
    .restart local v47    # "txtdata":[Ljava/lang/String;
    .restart local v48    # "value1":Ljava/lang/String;
    .restart local v49    # "value2":Ljava/lang/String;
    .restart local v50    # "value3":Ljava/lang/String;
    :pswitch_4
    :try_start_d
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    .line 187
    .local v31, "lib":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, v31

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    .line 188
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 189
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Patch for libraries \n"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-virtual/range {v52 .. v52}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v52

    new-instance v53, Ljava/lang/StringBuilder;

    invoke-direct/range {v53 .. v53}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v54, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    const-string v54, "/tmp"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v53

    const-string v54, ""

    invoke-virtual/range {v52 .. v54}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v52, ":"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 190
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------\n"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 192
    sget-boolean v7, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    if-nez v7, :cond_1d

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/createapkcustom;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v42

    .line 193
    :cond_1d
    if-nez v42, :cond_1e

    const/16 v45, 0x0

    .line 194
    :cond_1e
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_2

    goto :goto_d

    .line 620
    .end local v3    # "byteOrig":[B
    .end local v4    # "mask":[I
    .end local v11    # "begin":Z
    .end local v13    # "br":Ljava/io/BufferedReader;
    .end local v16    # "data":Ljava/lang/String;
    .end local v25    # "fileInApk":Z
    .end local v27    # "fis":Ljava/io/FileInputStream;
    .end local v28    # "isr":Ljava/io/InputStreamReader;
    .end local v31    # "lib":Ljava/lang/String;
    .end local v32    # "libr":Z
    .end local v36    # "mark_search":Z
    .end local v38    # "orhex":[Ljava/lang/String;
    .end local v40    # "r":I
    .end local v42    # "result":Z
    .end local v45    # "sumresult":Z
    .end local v47    # "txtdata":[Ljava/lang/String;
    .end local v48    # "value1":Ljava/lang/String;
    .end local v49    # "value2":Ljava/lang/String;
    .end local v50    # "value3":Ljava/lang/String;
    :catch_2
    move-exception v21

    .line 621
    .local v21, "e3":Ljava/lang/InterruptedException;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_b

    .line 197
    .end local v21    # "e3":Ljava/lang/InterruptedException;
    .restart local v3    # "byteOrig":[B
    .restart local v4    # "mask":[I
    .restart local v11    # "begin":Z
    .restart local v13    # "br":Ljava/io/BufferedReader;
    .restart local v16    # "data":Ljava/lang/String;
    .restart local v25    # "fileInApk":Z
    .restart local v27    # "fis":Ljava/io/FileInputStream;
    .restart local v28    # "isr":Ljava/io/InputStreamReader;
    .restart local v32    # "libr":Z
    .restart local v36    # "mark_search":Z
    .restart local v38    # "orhex":[Ljava/lang/String;
    .restart local v40    # "r":I
    .restart local v42    # "result":Z
    .restart local v45    # "sumresult":Z
    .restart local v47    # "txtdata":[Ljava/lang/String;
    .restart local v48    # "value1":Ljava/lang/String;
    .restart local v49    # "value2":Ljava/lang/String;
    .restart local v50    # "value3":Ljava/lang/String;
    :cond_1f
    const/4 v2, 0x0

    :try_start_e
    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    .line 198
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    .line 199
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 200
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 201
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    goto/16 :goto_1

    .line 204
    :pswitch_5
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_22

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    .line 205
    .restart local v31    # "lib":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, v31

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    .line 206
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "--------------------------------"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 207
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Patch for (armeabi) libraries \n"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-virtual/range {v52 .. v52}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v52

    new-instance v53, Ljava/lang/StringBuilder;

    invoke-direct/range {v53 .. v53}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v54, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    const-string v54, "/tmp"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v53

    const-string v54, ""

    invoke-virtual/range {v52 .. v54}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v52, ":"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 208
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "--------------------------------\n"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 210
    sget-boolean v7, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    if-nez v7, :cond_20

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/createapkcustom;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v42

    .line 211
    :cond_20
    if-nez v42, :cond_21

    const/16 v45, 0x0

    .line 212
    :cond_21
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_e

    .line 215
    .end local v31    # "lib":Ljava/lang/String;
    :cond_22
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    .line 216
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    .line 217
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 218
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 219
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    goto/16 :goto_1

    .line 222
    :pswitch_6
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    .line 223
    .restart local v31    # "lib":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, v31

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    .line 224
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------------------"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 225
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Patch for (armeabi-v7a) libraries \n"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-virtual/range {v52 .. v52}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v52

    new-instance v53, Ljava/lang/StringBuilder;

    invoke-direct/range {v53 .. v53}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v54, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    const-string v54, "/tmp"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v53

    const-string v54, ""

    invoke-virtual/range {v52 .. v54}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v52, ":"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 226
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------------------\n"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 228
    sget-boolean v7, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    if-nez v7, :cond_23

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/createapkcustom;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v42

    .line 229
    :cond_23
    if-nez v42, :cond_24

    const/16 v45, 0x0

    .line 230
    :cond_24
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 233
    .end local v31    # "lib":Ljava/lang/String;
    :cond_25
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    .line 234
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    .line 235
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 236
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 237
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    goto/16 :goto_1

    .line 240
    :pswitch_7
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_28

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    .line 241
    .restart local v31    # "lib":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, v31

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    .line 242
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 243
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Patch for (MIPS) libraries \n"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-virtual/range {v52 .. v52}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v52

    new-instance v53, Ljava/lang/StringBuilder;

    invoke-direct/range {v53 .. v53}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v54, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    const-string v54, "/tmp"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v53

    const-string v54, ""

    invoke-virtual/range {v52 .. v54}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v52, ":"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 244
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------\n"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 246
    sget-boolean v7, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    if-nez v7, :cond_26

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/createapkcustom;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v42

    .line 247
    :cond_26
    if-nez v42, :cond_27

    const/16 v45, 0x0

    .line 248
    :cond_27
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 251
    .end local v31    # "lib":Ljava/lang/String;
    :cond_28
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    .line 252
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    .line 253
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 254
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 255
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    goto/16 :goto_1

    .line 258
    :pswitch_8
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    .line 259
    .restart local v31    # "lib":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, v31

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    .line 260
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 261
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Patch for (x86) libraries \n"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-virtual/range {v52 .. v52}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v52

    new-instance v53, Ljava/lang/StringBuilder;

    invoke-direct/range {v53 .. v53}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v54, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    const-string v54, "/tmp"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v53

    const-string v54, ""

    invoke-virtual/range {v52 .. v54}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v52, ":"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 262
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------\n"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 264
    sget-boolean v7, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    if-nez v7, :cond_29

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/createapkcustom;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v42

    .line 265
    :cond_29
    if-nez v42, :cond_2a

    const/16 v45, 0x0

    .line 266
    :cond_2a
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_11

    .line 269
    .end local v31    # "lib":Ljava/lang/String;
    :cond_2b
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    .line 270
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    .line 271
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 272
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 273
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    goto/16 :goto_1

    .line 299
    :catch_3
    move-exception v18

    .line 300
    .local v18, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error name of libraries read!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 304
    .end local v18    # "e":Lorg/json/JSONException;
    :pswitch_9
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 305
    const/4 v2, 0x4

    move-object/from16 v0, v48

    invoke-static {v2, v0}, Lcom/chelpus/root/utils/createapkcustom;->searchlib(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    goto/16 :goto_3

    .line 308
    :pswitch_a
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 309
    const/4 v2, 0x0

    move-object/from16 v0, v48

    invoke-static {v2, v0}, Lcom/chelpus/root/utils/createapkcustom;->searchlib(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    goto/16 :goto_3

    .line 312
    :pswitch_b
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 313
    const/4 v2, 0x1

    move-object/from16 v0, v48

    invoke-static {v2, v0}, Lcom/chelpus/root/utils/createapkcustom;->searchlib(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    goto/16 :goto_3

    .line 316
    :pswitch_c
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 317
    const/4 v2, 0x2

    move-object/from16 v0, v48

    invoke-static {v2, v0}, Lcom/chelpus/root/utils/createapkcustom;->searchlib(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    goto/16 :goto_3

    .line 320
    :pswitch_d
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 321
    const/4 v2, 0x3

    move-object/from16 v0, v48

    invoke-static {v2, v0}, Lcom/chelpus/root/utils/createapkcustom;->searchlib(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    goto/16 :goto_3

    .line 333
    :catch_4
    move-exception v18

    .line 334
    .restart local v18    # "e":Lorg/json/JSONException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error name of file from apk read!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 340
    .end local v18    # "e":Lorg/json/JSONException;
    .restart local v24    # "file":Ljava/lang/String;
    :cond_2c
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "file for patch not found in apk."

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 388
    .end local v24    # "file":Ljava/lang/String;
    :catch_5
    move-exception v18

    .line 389
    .restart local v18    # "e":Lorg/json/JSONException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error original hex read!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 390
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    goto/16 :goto_6

    .line 399
    .end local v18    # "e":Lorg/json/JSONException;
    :catch_6
    move-exception v18

    .line 400
    .restart local v18    # "e":Lorg/json/JSONException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error original hex read!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_2

    goto/16 :goto_7

    .line 411
    .end local v18    # "e":Lorg/json/JSONException;
    .restart local v46    # "t":I
    :cond_2d
    const/4 v2, 0x0

    :try_start_f
    aput v2, v4, v46
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_2

    goto/16 :goto_9

    .line 419
    :catch_7
    move-exception v18

    .local v18, "e":Ljava/lang/Exception;
    :try_start_10
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 421
    .end local v18    # "e":Ljava/lang/Exception;
    .end local v46    # "t":I
    :cond_2e
    aget-object v2, v47, v40

    const-string v7, "\"object\""

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_10
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_2

    move-result v2

    if-eqz v2, :cond_2f

    .line 424
    :try_start_11
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 425
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "object"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_11
    .catch Lorg/json/JSONException; {:try_start_11 .. :try_end_11} :catch_8
    .catch Ljava/io/FileNotFoundException; {:try_start_11 .. :try_end_11} :catch_0
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_11 .. :try_end_11} :catch_2

    move-result-object v50

    .line 429
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_12
    :try_start_12
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dalvikvm -Xverify:none -Xdexopt:none -cp "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v7, 0x5

    aget-object v7, p0, v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v7, 0x6

    aget-object v7, p0, v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ".createnerorunpatch "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v7, 0x0

    aget-object v7, p0, v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "object"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->tooldir:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 430
    .local v15, "cmd":Ljava/lang/String;
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v35

    .line 431
    .local v35, "localProcess9":Ljava/lang/Process;
    invoke-virtual/range {v35 .. v35}, Ljava/lang/Process;->waitFor()I

    .line 432
    new-instance v34, Ljava/io/DataInputStream;

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 433
    .local v34, "localDataInputStream3":Ljava/io/DataInputStream;
    invoke-virtual/range {v34 .. v34}, Ljava/io/DataInputStream;->available()I

    move-result v2

    new-array v10, v2, [B

    .line 434
    .local v10, "arrayOfByte":[B
    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/io/DataInputStream;->read([B)I

    .line 435
    new-instance v43, Ljava/lang/String;

    move-object/from16 v0, v43

    invoke-direct {v0, v10}, Ljava/lang/String;-><init>([B)V

    .line 438
    .local v43, "str":Ljava/lang/String;
    invoke-virtual/range {v35 .. v35}, Ljava/lang/Process;->destroy()V

    .line 440
    const-string v2, "Done"

    move-object/from16 v0, v43

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_33

    .line 441
    const-string v44, "Object patched!\n\n"

    .line 442
    .local v44, "str2":Ljava/lang/String;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v44

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 443
    const/16 v45, 0x1

    .line 452
    :goto_13
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-static {v2}, Lcom/chelpus/root/utils/createapkcustom;->fixadler(Ljava/io/File;)V

    .line 455
    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    .line 459
    .end local v10    # "arrayOfByte":[B
    .end local v15    # "cmd":Ljava/lang/String;
    .end local v34    # "localDataInputStream3":Ljava/io/DataInputStream;
    .end local v35    # "localProcess9":Ljava/lang/Process;
    .end local v43    # "str":Ljava/lang/String;
    .end local v44    # "str2":Ljava/lang/String;
    :cond_2f
    aget-object v2, v47, v40

    const-string v7, "search"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_12
    .catch Ljava/io/FileNotFoundException; {:try_start_12 .. :try_end_12} :catch_0
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_12 .. :try_end_12} :catch_2

    move-result v2

    if-eqz v2, :cond_36

    .line 462
    :try_start_13
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 463
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "search"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_13
    .catch Lorg/json/JSONException; {:try_start_13 .. :try_end_13} :catch_9
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_13} :catch_0
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_13 .. :try_end_13} :catch_2

    move-result-object v50

    .line 467
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_14
    :try_start_14
    invoke-virtual/range {v50 .. v50}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v50

    .line 468
    const-string v2, "[ \t]+"

    move-object/from16 v0, v50

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v38, v0

    .line 469
    const-string v2, "[ \t]+"

    move-object/from16 v0, v50

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v38

    .line 470
    move-object/from16 v0, v38

    array-length v2, v0

    new-array v4, v2, [I

    .line 471
    move-object/from16 v0, v38

    array-length v2, v0

    new-array v3, v2, [B
    :try_end_14
    .catch Ljava/io/FileNotFoundException; {:try_start_14 .. :try_end_14} :catch_0
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_14 .. :try_end_14} :catch_2

    .line 473
    const/16 v46, 0x0

    .restart local v46    # "t":I
    :goto_15
    :try_start_15
    move-object/from16 v0, v38

    array-length v2, v0

    move/from16 v0, v46

    if-ge v0, v2, :cond_35

    .line 474
    aget-object v2, v38, v46

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_30

    aget-object v2, v38, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_30

    const/16 v23, 0x1

    const-string v2, "60"

    aput-object v2, v38, v46

    .line 475
    :cond_30
    aget-object v2, v38, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_31

    aget-object v2, v38, v46

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_34

    :cond_31
    const-string v2, "60"

    aput-object v2, v38, v46

    const/4 v2, 0x1

    aput v2, v4, v46

    .line 476
    :goto_16
    aget-object v2, v38, v46

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_32

    .line 477
    aget-object v2, v38, v46

    const-string v7, "R"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v37

    .line 478
    .restart local v37    # "or":Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v51

    .restart local v51    # "y":I
    add-int/lit8 v51, v51, 0x2

    .line 479
    aput v51, v4, v46

    const-string v2, "60"

    aput-object v2, v38, v46

    .line 480
    .end local v37    # "or":Ljava/lang/String;
    .end local v51    # "y":I
    :cond_32
    aget-object v2, v38, v46

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v3, v46
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_a
    .catch Ljava/io/FileNotFoundException; {:try_start_15 .. :try_end_15} :catch_0
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_15 .. :try_end_15} :catch_2

    .line 473
    add-int/lit8 v46, v46, 0x1

    goto :goto_15

    .line 426
    .end local v46    # "t":I
    :catch_8
    move-exception v18

    .line 427
    .local v18, "e":Lorg/json/JSONException;
    :try_start_16
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error number by object!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_12

    .line 447
    .end local v18    # "e":Lorg/json/JSONException;
    .restart local v10    # "arrayOfByte":[B
    .restart local v15    # "cmd":Ljava/lang/String;
    .restart local v34    # "localDataInputStream3":Ljava/io/DataInputStream;
    .restart local v35    # "localProcess9":Ljava/lang/Process;
    .restart local v43    # "str":Ljava/lang/String;
    :cond_33
    const-string v44, "Object not found!\n\n"

    .line 448
    .restart local v44    # "str2":Ljava/lang/String;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v44

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 449
    const/16 v45, 0x0

    goto/16 :goto_13

    .line 464
    .end local v10    # "arrayOfByte":[B
    .end local v15    # "cmd":Ljava/lang/String;
    .end local v34    # "localDataInputStream3":Ljava/io/DataInputStream;
    .end local v35    # "localProcess9":Ljava/lang/Process;
    .end local v43    # "str":Ljava/lang/String;
    .end local v44    # "str2":Ljava/lang/String;
    :catch_9
    move-exception v18

    .line 465
    .restart local v18    # "e":Lorg/json/JSONException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error search hex read!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_16
    .catch Ljava/io/FileNotFoundException; {:try_start_16 .. :try_end_16} :catch_0
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_16} :catch_2

    goto/16 :goto_14

    .line 475
    .end local v18    # "e":Lorg/json/JSONException;
    .restart local v46    # "t":I
    :cond_34
    const/4 v2, 0x0

    :try_start_17
    aput v2, v4, v46
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_a
    .catch Ljava/io/FileNotFoundException; {:try_start_17 .. :try_end_17} :catch_0
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_17 .. :try_end_17} :catch_2

    goto :goto_16

    .line 483
    :catch_a
    move-exception v18

    .local v18, "e":Ljava/lang/Exception;
    :try_start_18
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "search pattern read: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 485
    .end local v18    # "e":Ljava/lang/Exception;
    :cond_35
    if-eqz v23, :cond_40

    const/16 v42, 0x0

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Patterns to search not valid!\n"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 493
    .end local v46    # "t":I
    :cond_36
    :goto_17
    aget-object v2, v47, v40

    const-string v7, "replaced"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_18
    .catch Ljava/io/FileNotFoundException; {:try_start_18 .. :try_end_18} :catch_0
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_18 .. :try_end_18} :catch_2

    move-result v2

    if-eqz v2, :cond_46

    .line 496
    :try_start_19
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 497
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "replaced"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_19
    .catch Lorg/json/JSONException; {:try_start_19 .. :try_end_19} :catch_c
    .catch Ljava/io/FileNotFoundException; {:try_start_19 .. :try_end_19} :catch_0
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_19 .. :try_end_19} :catch_2

    move-result-object v49

    .line 501
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_18
    :try_start_1a
    invoke-virtual/range {v49 .. v49}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v49

    .line 503
    const-string v2, "[ \t]+"

    move-object/from16 v0, v49

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v41, v0

    .line 504
    .local v41, "rephex":[Ljava/lang/String;
    const-string v2, "[ \t]+"

    move-object/from16 v0, v49

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v41

    .line 505
    move-object/from16 v0, v41

    array-length v2, v0

    new-array v6, v2, [I

    .line 506
    .local v6, "rep_mask":[I
    move-object/from16 v0, v41

    array-length v2, v0

    new-array v5, v2, [B
    :try_end_1a
    .catch Ljava/io/FileNotFoundException; {:try_start_1a .. :try_end_1a} :catch_0
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1a .. :try_end_1a} :catch_2

    .line 508
    .local v5, "byteReplace":[B
    const/16 v46, 0x0

    .restart local v46    # "t":I
    :goto_19
    :try_start_1b
    move-object/from16 v0, v41

    array-length v2, v0

    move/from16 v0, v46

    if-ge v0, v2, :cond_42

    .line 509
    aget-object v2, v41, v46

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_37

    aget-object v2, v41, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_37

    const/16 v23, 0x1

    const-string v2, "60"

    aput-object v2, v41, v46

    .line 510
    :cond_37
    aget-object v2, v41, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_38

    aget-object v2, v41, v46

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_41

    :cond_38
    const-string v2, "60"

    aput-object v2, v41, v46

    const/4 v2, 0x0

    aput v2, v6, v46

    .line 511
    :goto_1a
    aget-object v2, v41, v46

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "sq"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_39

    const-string v2, "60"

    aput-object v2, v41, v46

    const/16 v2, 0xfd

    aput v2, v6, v46

    .line 512
    :cond_39
    aget-object v2, v41, v46

    const-string v7, "s1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3a

    aget-object v2, v41, v46

    const-string v7, "S1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3b

    :cond_3a
    const-string v2, "60"

    aput-object v2, v41, v46

    const/16 v2, 0xfe

    aput v2, v6, v46

    .line 513
    :cond_3b
    aget-object v2, v41, v46

    const-string v7, "s0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3c

    aget-object v2, v41, v46

    const-string v7, "S0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3d

    :cond_3c
    const-string v2, "60"

    aput-object v2, v41, v46

    const/16 v2, 0xff

    aput v2, v6, v46

    .line 514
    :cond_3d
    aget-object v2, v41, v46

    const-string v7, "W"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3e

    aget-object v2, v41, v46

    const-string v7, "w"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3e

    aget-object v2, v41, v46

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3e

    aget-object v2, v41, v46

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3f

    .line 515
    :cond_3e
    aget-object v2, v41, v46

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "w"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "r"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v37

    .line 516
    .restart local v37    # "or":Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v51

    .restart local v51    # "y":I
    add-int/lit8 v51, v51, 0x2

    .line 517
    aput v51, v6, v46

    const-string v2, "60"

    aput-object v2, v41, v46

    .line 518
    .end local v37    # "or":Ljava/lang/String;
    .end local v51    # "y":I
    :cond_3f
    aget-object v2, v41, v46

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v5, v46
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_d
    .catch Ljava/io/FileNotFoundException; {:try_start_1b .. :try_end_1b} :catch_0
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1b .. :try_end_1b} :catch_2

    .line 508
    add-int/lit8 v46, v46, 0x1

    goto/16 :goto_19

    .line 486
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v41    # "rephex":[Ljava/lang/String;
    :cond_40
    const/16 v36, 0x1

    .line 488
    :try_start_1c
    new-instance v33, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v0, v33

    invoke-direct {v0, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;-><init>([B[I)V

    .line 489
    .local v33, "local":Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    array-length v2, v3

    new-array v2, v2, [B

    move-object/from16 v0, v33

    iput-object v2, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    .line 490
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_b
    .catch Ljava/io/FileNotFoundException; {:try_start_1c .. :try_end_1c} :catch_0
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1c .. :try_end_1c} :catch_2

    goto/16 :goto_17

    .line 491
    .end local v33    # "local":Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    :catch_b
    move-exception v18

    .restart local v18    # "e":Ljava/lang/Exception;
    :try_start_1d
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_17

    .line 498
    .end local v18    # "e":Ljava/lang/Exception;
    .end local v46    # "t":I
    :catch_c
    move-exception v18

    .line 499
    .local v18, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error replaced hex read!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1d
    .catch Ljava/io/FileNotFoundException; {:try_start_1d .. :try_end_1d} :catch_0
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1d .. :try_end_1d} :catch_2

    goto/16 :goto_18

    .line 510
    .end local v18    # "e":Lorg/json/JSONException;
    .restart local v5    # "byteReplace":[B
    .restart local v6    # "rep_mask":[I
    .restart local v41    # "rephex":[Ljava/lang/String;
    .restart local v46    # "t":I
    :cond_41
    const/4 v2, 0x1

    :try_start_1e
    aput v2, v6, v46
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_d
    .catch Ljava/io/FileNotFoundException; {:try_start_1e .. :try_end_1e} :catch_0
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1e .. :try_end_1e} :catch_2

    goto/16 :goto_1a

    .line 520
    :catch_d
    move-exception v18

    .local v18, "e":Ljava/lang/Exception;
    :try_start_1f
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 521
    .end local v18    # "e":Ljava/lang/Exception;
    :cond_42
    array-length v2, v6

    array-length v7, v4

    if-ne v2, v7, :cond_43

    array-length v2, v3

    array-length v7, v5

    if-ne v2, v7, :cond_43

    array-length v2, v5

    const/4 v7, 0x4

    if-lt v2, v7, :cond_43

    array-length v2, v3

    const/4 v7, 0x4

    if-ge v2, v7, :cond_44

    :cond_43
    const/16 v23, 0x1

    .line 522
    :cond_44
    if-eqz v23, :cond_45

    const/16 v42, 0x0

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 523
    :cond_45
    if-nez v23, :cond_46

    .line 524
    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v52

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 525
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    .line 529
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v41    # "rephex":[Ljava/lang/String;
    .end local v46    # "t":I
    :cond_46
    aget-object v2, v47, v40

    const-string v7, "insert"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_1f
    .catch Ljava/io/FileNotFoundException; {:try_start_1f .. :try_end_1f} :catch_0
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1f .. :try_end_1f} :catch_2

    move-result v2

    if-eqz v2, :cond_56

    .line 532
    :try_start_20
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 533
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "insert"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_20
    .catch Lorg/json/JSONException; {:try_start_20 .. :try_end_20} :catch_e
    .catch Ljava/io/FileNotFoundException; {:try_start_20 .. :try_end_20} :catch_0
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_20} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_20 .. :try_end_20} :catch_2

    move-result-object v49

    .line 537
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_1b
    :try_start_21
    invoke-virtual/range {v49 .. v49}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v49

    .line 539
    const-string v2, "[ \t]+"

    move-object/from16 v0, v49

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v41, v0

    .line 540
    .restart local v41    # "rephex":[Ljava/lang/String;
    const-string v2, "[ \t]+"

    move-object/from16 v0, v49

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v41

    .line 541
    move-object/from16 v0, v41

    array-length v2, v0

    new-array v6, v2, [I

    .line 542
    .restart local v6    # "rep_mask":[I
    move-object/from16 v0, v41

    array-length v2, v0

    new-array v5, v2, [B
    :try_end_21
    .catch Ljava/io/FileNotFoundException; {:try_start_21 .. :try_end_21} :catch_0
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_21} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_21 .. :try_end_21} :catch_2

    .line 544
    .restart local v5    # "byteReplace":[B
    const/16 v46, 0x0

    .restart local v46    # "t":I
    :goto_1c
    :try_start_22
    move-object/from16 v0, v41

    array-length v2, v0

    move/from16 v0, v46

    if-ge v0, v2, :cond_51

    .line 545
    aget-object v2, v41, v46

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_47

    aget-object v2, v41, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_47

    const/16 v23, 0x1

    const-string v2, "60"

    aput-object v2, v41, v46

    .line 546
    :cond_47
    aget-object v2, v41, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_48

    aget-object v2, v41, v46

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_50

    :cond_48
    const-string v2, "60"

    aput-object v2, v41, v46

    const/4 v2, 0x0

    aput v2, v6, v46

    .line 547
    :goto_1d
    aget-object v2, v41, v46

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "sq"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_49

    const-string v2, "60"

    aput-object v2, v41, v46

    const/16 v2, 0xfd

    aput v2, v6, v46

    .line 548
    :cond_49
    aget-object v2, v41, v46

    const-string v7, "s1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4a

    aget-object v2, v41, v46

    const-string v7, "S1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4b

    :cond_4a
    const-string v2, "60"

    aput-object v2, v41, v46

    const/16 v2, 0xfe

    aput v2, v6, v46

    .line 549
    :cond_4b
    aget-object v2, v41, v46

    const-string v7, "s0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4c

    aget-object v2, v41, v46

    const-string v7, "S0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4d

    :cond_4c
    const-string v2, "60"

    aput-object v2, v41, v46

    const/16 v2, 0xff

    aput v2, v6, v46

    .line 550
    :cond_4d
    aget-object v2, v41, v46

    const-string v7, "W"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4e

    aget-object v2, v41, v46

    const-string v7, "w"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4e

    aget-object v2, v41, v46

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4e

    aget-object v2, v41, v46

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4f

    .line 551
    :cond_4e
    aget-object v2, v41, v46

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "w"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "r"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v37

    .line 552
    .restart local v37    # "or":Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v51

    .restart local v51    # "y":I
    add-int/lit8 v51, v51, 0x2

    .line 553
    aput v51, v6, v46

    const-string v2, "60"

    aput-object v2, v41, v46

    .line 554
    .end local v37    # "or":Ljava/lang/String;
    .end local v51    # "y":I
    :cond_4f
    aget-object v2, v41, v46

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v5, v46
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_f
    .catch Ljava/io/FileNotFoundException; {:try_start_22 .. :try_end_22} :catch_0
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_22} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_22 .. :try_end_22} :catch_2

    .line 544
    add-int/lit8 v46, v46, 0x1

    goto/16 :goto_1c

    .line 534
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v41    # "rephex":[Ljava/lang/String;
    .end local v46    # "t":I
    :catch_e
    move-exception v18

    .line 535
    .local v18, "e":Lorg/json/JSONException;
    :try_start_23
    const-string v2, "Error LP: Error insert hex read!"

    invoke-static {v2}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z
    :try_end_23
    .catch Ljava/io/FileNotFoundException; {:try_start_23 .. :try_end_23} :catch_0
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_23} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_23 .. :try_end_23} :catch_2

    goto/16 :goto_1b

    .line 546
    .end local v18    # "e":Lorg/json/JSONException;
    .restart local v5    # "byteReplace":[B
    .restart local v6    # "rep_mask":[I
    .restart local v41    # "rephex":[Ljava/lang/String;
    .restart local v46    # "t":I
    :cond_50
    const/4 v2, 0x1

    :try_start_24
    aput v2, v6, v46
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_f
    .catch Ljava/io/FileNotFoundException; {:try_start_24 .. :try_end_24} :catch_0
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_24} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_24 .. :try_end_24} :catch_2

    goto/16 :goto_1d

    .line 556
    :catch_f
    move-exception v18

    .local v18, "e":Ljava/lang/Exception;
    :try_start_25
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 557
    .end local v18    # "e":Ljava/lang/Exception;
    :cond_51
    array-length v2, v5

    const/4 v7, 0x4

    if-lt v2, v7, :cond_52

    array-length v2, v3

    const/4 v7, 0x4

    if-ge v2, v7, :cond_53

    :cond_52
    const/16 v23, 0x1

    .line 558
    :cond_53
    if-eqz v23, :cond_54

    const/16 v42, 0x0

    const-string v2, "Error LP: Dimensions of the original hex-string and repleced must be >3.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"

    invoke-static {v2}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 559
    :cond_54
    if-nez v23, :cond_56

    .line 561
    sget-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    if-nez v2, :cond_55

    sget-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multidex:Z

    if-eqz v2, :cond_5c

    :cond_55
    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    const-string v7, "all_lib"

    const/4 v8, 0x1

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v52

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 563
    :goto_1e
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    .line 567
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v41    # "rephex":[Ljava/lang/String;
    .end local v46    # "t":I
    :cond_56
    aget-object v2, v47, v40

    const-string v7, "replace_from_file"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_25
    .catch Ljava/io/FileNotFoundException; {:try_start_25 .. :try_end_25} :catch_0
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_25} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_25 .. :try_end_25} :catch_2

    move-result v2

    if-eqz v2, :cond_59

    .line 570
    :try_start_26
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 571
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "replace_from_file"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_26
    .catch Lorg/json/JSONException; {:try_start_26 .. :try_end_26} :catch_10
    .catch Ljava/io/FileNotFoundException; {:try_start_26 .. :try_end_26} :catch_0
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_26} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_26 .. :try_end_26} :catch_2

    move-result-object v49

    .line 575
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_1f
    :try_start_27
    invoke-virtual/range {v49 .. v49}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v49

    .line 577
    new-instance v9, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v7, Ljava/io/File;

    const/4 v8, 0x1

    aget-object v8, p0, v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "/"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 578
    .local v9, "arrayFile":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v7

    long-to-int v0, v7

    move/from16 v30, v0

    .line 579
    .local v30, "len":I
    move/from16 v0, v30

    new-array v5, v0, [B
    :try_end_27
    .catch Ljava/io/FileNotFoundException; {:try_start_27 .. :try_end_27} :catch_0
    .catch Ljava/io/IOException; {:try_start_27 .. :try_end_27} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_27 .. :try_end_27} :catch_2

    .line 583
    .restart local v5    # "byteReplace":[B
    :try_start_28
    new-instance v17, Ljava/io/FileInputStream;

    move-object/from16 v0, v17

    invoke-direct {v0, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 584
    .local v17, "data3":Ljava/io/FileInputStream;
    :cond_57
    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/io/FileInputStream;->read([B)I
    :try_end_28
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_28} :catch_11
    .catch Ljava/io/FileNotFoundException; {:try_start_28 .. :try_end_28} :catch_0
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_28} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_28 .. :try_end_28} :catch_2

    move-result v2

    if-gtz v2, :cond_57

    .line 588
    .end local v17    # "data3":Ljava/io/FileInputStream;
    :goto_20
    :try_start_29
    move/from16 v0, v30

    new-array v6, v0, [I

    .line 589
    .restart local v6    # "rep_mask":[I
    const/4 v2, 0x1

    invoke-static {v6, v2}, Ljava/util/Arrays;->fill([II)V

    .line 592
    if-eqz v23, :cond_58

    const/16 v42, 0x0

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 593
    :cond_58
    if-nez v23, :cond_59

    .line 594
    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v52

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 595
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    .line 599
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v9    # "arrayFile":Ljava/io/File;
    .end local v30    # "len":I
    :cond_59
    if-eqz v22, :cond_5a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v7, v47, v40

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 600
    :cond_5a
    const-string v2, "[END]"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5b

    .line 601
    const/4 v2, 0x4

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 602
    const/16 v22, 0x1

    .line 604
    :cond_5b
    add-int/lit8 v40, v40, 0x1

    goto/16 :goto_0

    .line 562
    .restart local v5    # "byteReplace":[B
    .restart local v6    # "rep_mask":[I
    .restart local v41    # "rephex":[Ljava/lang/String;
    .restart local v46    # "t":I
    :cond_5c
    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v52

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1e

    .line 572
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v41    # "rephex":[Ljava/lang/String;
    .end local v46    # "t":I
    :catch_10
    move-exception v18

    .line 573
    .local v18, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error replaced hex read!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_1f

    .line 585
    .end local v18    # "e":Lorg/json/JSONException;
    .restart local v5    # "byteReplace":[B
    .restart local v9    # "arrayFile":Ljava/io/File;
    .restart local v30    # "len":I
    :catch_11
    move-exception v18

    .line 586
    .local v18, "e":Ljava/lang/Exception;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_20

    .line 607
    .end local v5    # "byteReplace":[B
    .end local v9    # "arrayFile":Ljava/io/File;
    .end local v18    # "e":Ljava/lang/Exception;
    .end local v30    # "len":I
    :cond_5d
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_5e

    .line 608
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/createapkcustom;->zipLib(Ljava/util/ArrayList;)V

    .line 610
    :cond_5e
    if-eqz v45, :cond_5f

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 611
    :cond_5f
    if-nez v45, :cond_60

    sget-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->patchteil:Z

    if-eqz v2, :cond_61

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Not all patterns are replaced, but the program can work, test it!\nCustom Patch not valid for this Version of the Programm or already patched. "

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 613
    :cond_60
    :goto_21
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->clearTemp()V

    .line 614
    invoke-virtual/range {v27 .. v27}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_b

    .line 612
    :cond_61
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Custom Patch not valid for this Version of the Programm or already patched. "

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_29
    .catch Ljava/io/FileNotFoundException; {:try_start_29 .. :try_end_29} :catch_0
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_29} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_29 .. :try_end_29} :catch_2

    goto :goto_21

    .line 628
    .end local v3    # "byteOrig":[B
    .end local v4    # "mask":[I
    .end local v11    # "begin":Z
    .end local v13    # "br":Ljava/io/BufferedReader;
    .end local v16    # "data":Ljava/lang/String;
    .end local v25    # "fileInApk":Z
    .end local v27    # "fis":Ljava/io/FileInputStream;
    .end local v28    # "isr":Ljava/io/InputStreamReader;
    .end local v32    # "libr":Z
    .end local v36    # "mark_search":Z
    .end local v38    # "orhex":[Ljava/lang/String;
    .end local v40    # "r":I
    .end local v45    # "sumresult":Z
    .end local v47    # "txtdata":[Ljava/lang/String;
    .end local v48    # "value1":Ljava/lang/String;
    .end local v49    # "value2":Ljava/lang/String;
    .end local v50    # "value3":Ljava/lang/String;
    .local v42, "result":Ljava/lang/String;
    :catch_12
    move-exception v18

    .line 630
    .local v18, "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_c

    .line 142
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_3
    .end packed-switch

    .line 302
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_9
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public static patchProcess(Ljava/util/ArrayList;)Z
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 637
    .local p0, "patchlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;>;"
    const/16 v19, 0x1

    .line 640
    .local v19, "patch":Z
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    const-string v5, "rw"

    invoke-direct {v3, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 641
    .local v2, "ChannelDex":Ljava/nio/channels/FileChannel;
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v13

    .line 643
    .local v13, "fileBytes":Ljava/nio/MappedByteBuffer;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v3

    array-length v3, v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object/from16 v20, v0

    .line 645
    .local v20, "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object/from16 v20, v0

    .line 646
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object v0, v3

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object/from16 v20, v0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    .line 648
    const/4 v11, -0x1

    .line 651
    .local v11, "curentPos":I
    :cond_0
    :try_start_1
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 653
    add-int/lit8 v3, v11, 0x1

    invoke-virtual {v13, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 654
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v11

    .line 655
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v10

    .line 657
    .local v10, "curentByte":B
    const/4 v14, 0x0

    .local v14, "g":I
    :goto_0
    move-object/from16 v0, v20

    array-length v3, v0

    if-ge v14, v3, :cond_0

    .line 658
    invoke-virtual {v13, v11}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 660
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    if-eq v10, v3, :cond_1

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/4 v4, 0x1

    if-le v3, v4, :cond_f

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    aget-object v4, v20, v14

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    if-ne v10, v3, :cond_f

    .line 662
    :cond_1
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-nez v3, :cond_2

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    aput-byte v10, v3, v4
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4

    .line 664
    :cond_2
    :try_start_2
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/4 v4, 0x1

    if-le v3, v4, :cond_3

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xfd

    if-ge v3, v4, :cond_3

    aget-object v3, v20, v14

    iget-object v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v5, 0x0

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    aget-object v6, v20, v14

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    add-int/lit8 v6, v6, -0x2

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    aput-byte v3, v4, v5
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_4

    .line 666
    :cond_3
    :goto_1
    :try_start_3
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xfd

    if-ne v3, v4, :cond_4

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    and-int/lit8 v5, v10, 0xf

    and-int/lit8 v6, v10, 0xf

    mul-int/lit8 v6, v6, 0x10

    add-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 667
    :cond_4
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xfe

    if-ne v3, v4, :cond_5

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    and-int/lit8 v5, v10, 0xf

    add-int/lit8 v5, v5, 0x10

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 668
    :cond_5
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xff

    if-ne v3, v4, :cond_6

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    and-int/lit8 v5, v10, 0xf

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 669
    :cond_6
    const/4 v15, 0x1

    .line 670
    .local v15, "i":I
    add-int/lit8 v3, v11, 0x1

    invoke-virtual {v13, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 671
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v21

    .line 673
    .local v21, "prufbyte":B
    :goto_2
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    aget-byte v3, v3, v15

    move/from16 v0, v21

    if-eq v0, v3, :cond_8

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    aget v3, v3, v15

    const/4 v4, 0x1

    if-le v3, v4, :cond_7

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    aget-object v4, v20, v14

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    aget v4, v4, v15

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    move/from16 v0, v21

    if-eq v0, v3, :cond_8

    :cond_7
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    aget v3, v3, v15
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_4

    const/4 v4, 0x1

    if-ne v3, v4, :cond_f

    .line 676
    :cond_8
    :try_start_4
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    if-nez v3, :cond_9

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    aput-byte v21, v3, v15

    .line 678
    :cond_9
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    const/4 v4, 0x1

    if-le v3, v4, :cond_a

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    const/16 v4, 0xfd

    if-ge v3, v4, :cond_a

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    add-int/lit8 v23, v3, -0x2

    .local v23, "y":I
    aget-object v3, v20, v14

    iget-object v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    aput-byte v3, v4, v15
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_4

    .line 680
    .end local v23    # "y":I
    :cond_a
    :goto_3
    :try_start_5
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xfd

    if-ne v3, v4, :cond_b

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    and-int/lit8 v5, v10, 0xf

    and-int/lit8 v6, v10, 0xf

    mul-int/lit8 v6, v6, 0x10

    add-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 681
    :cond_b
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    const/16 v4, 0xfe

    if-ne v3, v4, :cond_c

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    and-int/lit8 v4, v21, 0xf

    add-int/lit8 v4, v4, 0x10

    int-to-byte v4, v4

    aput-byte v4, v3, v15

    .line 682
    :cond_c
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    const/16 v4, 0xff

    if-ne v3, v4, :cond_d

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    and-int/lit8 v4, v21, 0xf

    int-to-byte v4, v4

    aput-byte v4, v3, v15

    .line 684
    :cond_d
    add-int/lit8 v15, v15, 0x1

    .line 686
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    array-length v3, v3

    if-ne v15, v3, :cond_14

    .line 688
    aget-object v3, v20, v14

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->insert:Z

    if-eqz v3, :cond_e

    .line 689
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v18

    .line 690
    .local v18, "p":I
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v3

    long-to-int v3, v3

    sub-int v16, v3, v18

    .line 693
    .local v16, "lenght":I
    move/from16 v0, v16

    new-array v8, v0, [B

    .line 694
    .local v8, "buf":[B
    const/4 v3, 0x0

    move/from16 v0, v16

    invoke-virtual {v13, v8, v3, v0}, Ljava/nio/MappedByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 696
    invoke-static {v8}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 697
    .local v9, "buffer":Ljava/nio/ByteBuffer;
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    array-length v3, v3

    aget-object v4, v20, v14

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    array-length v4, v4

    sub-int/2addr v3, v4

    add-int v3, v3, v18

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 698
    invoke-virtual {v2, v9}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 703
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v13

    .line 704
    move/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 706
    .end local v8    # "buf":[B
    .end local v9    # "buffer":Ljava/nio/ByteBuffer;
    .end local v16    # "lenght":I
    .end local v18    # "p":I
    :cond_e
    int-to-long v3, v11

    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 707
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 708
    .restart local v9    # "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v2, v9}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 709
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 710
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\nPattern N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v14, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": Patch done! \n(Offset: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 711
    aget-object v3, v20, v14

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->result:Z

    .line 712
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->patchteil:Z

    .line 657
    .end local v9    # "buffer":Ljava/nio/ByteBuffer;
    .end local v15    # "i":I
    .end local v21    # "prufbyte":B
    :cond_f
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 665
    :catch_0
    move-exception v12

    .local v12, "e":Ljava/lang/Exception;
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    add-int/lit8 v23, v3, -0x2

    .restart local v23    # "y":I
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Byte N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " not found! Please edit search pattern for byte "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_1

    .line 727
    .end local v10    # "curentByte":B
    .end local v12    # "e":Ljava/lang/Exception;
    .end local v14    # "g":I
    .end local v23    # "y":I
    :catch_1
    move-exception v12

    .line 728
    .local v12, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_6
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v4, "Byte by search not found! Please edit pattern for search.\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 732
    .end local v12    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_10
    :goto_4
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 734
    const/4 v14, 0x0

    .restart local v14    # "g":I
    :goto_5
    move-object/from16 v0, v20

    array-length v3, v0

    if-ge v14, v3, :cond_1b

    .line 736
    aget-object v3, v20, v14

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->result:Z

    if-nez v3, :cond_19

    .line 737
    const/16 v22, 0x0

    .line 738
    .local v22, "trueGroup":Z
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_15

    .line 739
    const/16 v23, 0x0

    .restart local v23    # "y":I
    :goto_6
    move-object/from16 v0, v20

    array-length v3, v0

    move/from16 v0, v23

    if-ge v0, v3, :cond_15

    .line 740
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    aget-object v4, v20, v23

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    aget-object v3, v20, v23

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->result:Z

    if-eqz v3, :cond_13

    .line 741
    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->multidex:Z

    if-nez v3, :cond_11

    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    if-eqz v3, :cond_12

    :cond_11
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/nio/BufferUnderflowException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    .line 742
    :cond_12
    const/16 v22, 0x1

    .line 739
    :cond_13
    add-int/lit8 v23, v23, 0x1

    goto :goto_6

    .line 679
    .end local v22    # "trueGroup":Z
    .end local v23    # "y":I
    .restart local v10    # "curentByte":B
    .restart local v15    # "i":I
    .restart local v21    # "prufbyte":B
    :catch_2
    move-exception v12

    .local v12, "e":Ljava/lang/Exception;
    :try_start_7
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    add-int/lit8 v23, v3, -0x2

    .restart local v23    # "y":I
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Byte N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " not found! Please edit search pattern for byte "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 729
    .end local v10    # "curentByte":B
    .end local v12    # "e":Ljava/lang/Exception;
    .end local v14    # "g":I
    .end local v15    # "i":I
    .end local v21    # "prufbyte":B
    .end local v23    # "y":I
    :catch_3
    move-exception v3

    goto/16 :goto_4

    .line 719
    .restart local v10    # "curentByte":B
    .restart local v14    # "g":I
    .restart local v15    # "i":I
    .restart local v21    # "prufbyte":B
    :cond_14
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->get()B
    :try_end_7
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_4

    move-result v21

    goto/16 :goto_2

    .line 746
    .end local v10    # "curentByte":B
    .end local v15    # "i":I
    .end local v21    # "prufbyte":B
    .restart local v22    # "trueGroup":Z
    :cond_15
    if-nez v22, :cond_16

    :try_start_8
    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    if-nez v3, :cond_16

    .line 747
    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->multidex:Z

    if-eqz v3, :cond_17

    .line 748
    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    if-nez v3, :cond_16

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    sget-object v5, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 749
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\nPattern N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v14, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":\nError LP: Pattern not found!\nor patch is already applied?!\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 750
    const/16 v19, 0x0

    .line 734
    .end local v22    # "trueGroup":Z
    :cond_16
    :goto_7
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_5

    .line 753
    .restart local v22    # "trueGroup":Z
    :cond_17
    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    if-eqz v3, :cond_18

    .line 757
    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    if-nez v3, :cond_16

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    new-instance v5, Ljava/io/File;

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    sget-object v6, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 758
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\nPattern N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v14, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":\nError LP: Pattern not found!\nor patch is already applied?!\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 759
    const/16 v19, 0x0

    goto :goto_7

    .line 762
    :cond_18
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\nPattern N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v14, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":\nError LP: Pattern not found!\nor patch is already applied?!\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 763
    const/16 v19, 0x0

    goto :goto_7

    .line 769
    .end local v22    # "trueGroup":Z
    :cond_19
    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->multidex:Z

    if-nez v3, :cond_1a

    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    if-eqz v3, :cond_16

    :cond_1a
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/nio/BufferUnderflowException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    goto/16 :goto_7

    .line 774
    .end local v2    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v11    # "curentPos":I
    .end local v13    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .end local v14    # "g":I
    .end local v20    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    :catch_4
    move-exception v17

    .line 775
    .local v17, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v4, "Error LP: Program files are not found!\nMove Program to internal storage."

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 781
    .end local v17    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_1b
    :goto_8
    sget v3, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1c

    .line 782
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-static {v3}, Lcom/chelpus/root/utils/createapkcustom;->fixadler(Ljava/io/File;)V

    .line 786
    :cond_1c
    const/4 v3, 0x1

    return v3

    .line 778
    :catch_5
    move-exception v12

    .line 779
    .restart local v12    # "e":Ljava/lang/Exception;
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception e"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_8

    .line 776
    .end local v12    # "e":Ljava/lang/Exception;
    :catch_6
    move-exception v3

    goto :goto_8

    .line 730
    .restart local v2    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .restart local v11    # "curentPos":I
    .restart local v13    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .restart local v20    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    :catch_7
    move-exception v3

    goto/16 :goto_4
.end method

.method public static searchProcess(Ljava/util/ArrayList;)Z
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 791
    .local p0, "searchlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;>;"
    const/16 v17, 0x1

    .line 793
    .local v17, "patch":Z
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    const-string v5, "rw"

    invoke-direct {v3, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 794
    .local v2, "ChannelDex2":Ljava/nio/channels/FileChannel;
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v11

    .line 796
    .local v11, "fileBytes2":Ljava/nio/MappedByteBuffer;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v3

    array-length v3, v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v18, v0

    .line 798
    .local v18, "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v18, v0

    .line 799
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object v0, v3

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v18, v0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    .line 805
    const-wide/16 v14, 0x0

    .local v14, "j":J
    :goto_0
    :try_start_1
    invoke-virtual {v11}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 807
    invoke-virtual {v11}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v9

    .line 808
    .local v9, "curentPos":I
    invoke-virtual {v11}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v8

    .line 810
    .local v8, "curentByte":B
    const/4 v12, 0x0

    .local v12, "g":I
    :goto_1
    move-object/from16 v0, v18

    array-length v3, v0

    if-ge v12, v3, :cond_7

    .line 811
    invoke-virtual {v11, v9}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 813
    aget-object v3, v18, v12

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-nez v3, :cond_5

    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origByte:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    if-eq v8, v3, :cond_0

    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-eqz v3, :cond_5

    .line 815
    :cond_0
    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-eqz v3, :cond_1

    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    const/4 v4, 0x0

    aput-byte v8, v3, v4

    .line 816
    :cond_1
    const/4 v13, 0x1

    .line 817
    .local v13, "i":I
    add-int/lit8 v3, v9, 0x1

    invoke-virtual {v11, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 818
    invoke-virtual {v11}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v19

    .line 820
    .local v19, "prufbyte":B
    :goto_2
    aget-object v3, v18, v12

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-nez v3, :cond_2

    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origByte:[B

    aget-byte v3, v3, v13

    move/from16 v0, v19

    if-eq v0, v3, :cond_3

    :cond_2
    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v13

    if-eqz v3, :cond_5

    .line 822
    :cond_3
    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v13

    if-lez v3, :cond_4

    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    aput-byte v19, v3, v13

    .line 824
    :cond_4
    add-int/lit8 v13, v13, 0x1

    .line 826
    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origByte:[B

    array-length v3, v3

    if-ne v13, v3, :cond_6

    .line 829
    aget-object v3, v18, v12

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    .line 830
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->patchteil:Z

    .line 810
    .end local v13    # "i":I
    .end local v19    # "prufbyte":B
    :cond_5
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 834
    .restart local v13    # "i":I
    .restart local v19    # "prufbyte":B
    :cond_6
    invoke-virtual {v11}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v19

    goto :goto_2

    .line 839
    .end local v13    # "i":I
    .end local v19    # "prufbyte":B
    :cond_7
    add-int/lit8 v3, v9, 0x1

    invoke-virtual {v11, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/nio/BufferUnderflowException; {:try_start_1 .. :try_end_1} :catch_3

    .line 805
    const-wide/16 v3, 0x1

    add-long/2addr v14, v3

    goto/16 :goto_0

    .line 842
    .end local v8    # "curentByte":B
    .end local v9    # "curentPos":I
    .end local v12    # "g":I
    :catch_0
    move-exception v10

    .line 843
    .local v10, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 845
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_8
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 846
    const/4 v12, 0x0

    .restart local v12    # "g":I
    :goto_3
    move-object/from16 v0, v18

    array-length v3, v0

    if-ge v12, v3, :cond_a

    .line 848
    aget-object v3, v18, v12

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-nez v3, :cond_9

    .line 850
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bytes by serach N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v12, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":\nError LP: Bytes not found!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 851
    const/16 v17, 0x0

    .line 846
    :cond_9
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 854
    :cond_a
    const/4 v12, 0x0

    :goto_4
    move-object/from16 v0, v18

    array-length v3, v0

    if-ge v12, v3, :cond_d

    .line 855
    aget-object v3, v18, v12

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-eqz v3, :cond_b

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\nBytes by search N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v12, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 857
    :cond_b
    const/16 v20, 0x0

    .local v20, "w":I
    :goto_5
    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    array-length v3, v3

    move/from16 v0, v20

    if-ge v0, v3, :cond_e

    .line 858
    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v20

    const/4 v4, 0x1

    if-le v3, v4, :cond_c

    .line 859
    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v20
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/nio/BufferUnderflowException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    add-int/lit8 v21, v3, -0x2

    .line 860
    .local v21, "y":I
    :try_start_3
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    aget-object v4, v18, v12

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    aget-byte v4, v4, v20

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/nio/BufferUnderflowException; {:try_start_3 .. :try_end_3} :catch_3

    .line 861
    :goto_6
    :try_start_4
    aget-object v3, v18, v12

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-eqz v3, :cond_c

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "R"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 857
    .end local v21    # "y":I
    :cond_c
    add-int/lit8 v20, v20, 0x1

    goto :goto_5

    .line 860
    .restart local v21    # "y":I
    :catch_1
    move-exception v10

    .restart local v10    # "e":Ljava/lang/Exception;
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    aget-object v4, v18, v12

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    aget-byte v4, v4, v20

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/nio/BufferUnderflowException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_6

    .line 867
    .end local v2    # "ChannelDex2":Ljava/nio/channels/FileChannel;
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v11    # "fileBytes2":Ljava/nio/MappedByteBuffer;
    .end local v12    # "g":I
    .end local v14    # "j":J
    .end local v18    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    .end local v20    # "w":I
    .end local v21    # "y":I
    :catch_2
    move-exception v16

    .line 868
    .local v16, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v4, "Error LP: Program files are not found!\nMove Program to internal storage."

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 875
    .end local v16    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_d
    :goto_7
    return v17

    .line 864
    .restart local v2    # "ChannelDex2":Ljava/nio/channels/FileChannel;
    .restart local v11    # "fileBytes2":Ljava/nio/MappedByteBuffer;
    .restart local v12    # "g":I
    .restart local v14    # "j":J
    .restart local v18    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    .restart local v20    # "w":I
    :cond_e
    :try_start_5
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/nio/BufferUnderflowException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 854
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_4

    .line 869
    .end local v2    # "ChannelDex2":Ljava/nio/channels/FileChannel;
    .end local v11    # "fileBytes2":Ljava/nio/MappedByteBuffer;
    .end local v12    # "g":I
    .end local v14    # "j":J
    .end local v18    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    .end local v20    # "w":I
    :catch_3
    move-exception v10

    .line 870
    .local v10, "e":Ljava/nio/BufferUnderflowException;
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception e"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v10}, Ljava/nio/BufferUnderflowException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_7

    .line 871
    .end local v10    # "e":Ljava/nio/BufferUnderflowException;
    :catch_4
    move-exception v10

    .line 872
    .local v10, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception e"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_7
.end method

.method public static searchlib(ILjava/lang/String;)Ljava/util/ArrayList;
    .locals 16
    .param p0, "architectura"    # I
    .param p1, "libname"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 904
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 909
    .local v10, "libs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    new-instance v1, Ljava/io/File;

    sget-object v11, Lcom/chelpus/root/utils/createapkcustom;->appdir:Ljava/lang/String;

    invoke-direct {v1, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 910
    .local v1, "apk":Ljava/io/File;
    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/Modified/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/chelpus/root/utils/createapkcustom;->packageName:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ".apk"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v11, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    .line 911
    sget-object v11, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_0

    sget-object v11, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-static {v1, v11}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 913
    :cond_0
    sget-object v11, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-static {v11}, Lcom/chelpus/root/utils/createapkcustom;->extractLibs(Ljava/io/File;)V

    .line 914
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    const-string v12, "*"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 915
    const/4 v11, 0x1

    sput-boolean v11, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    .line 916
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 917
    .local v9, "foundlibs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    new-instance v11, Lcom/chelpus/Utils;

    const-string v12, ""

    invoke-direct {v11, v12}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    new-instance v12, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v14, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/tmp/lib/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v13, ".so"

    invoke-virtual {v11, v12, v13, v9}, Lcom/chelpus/Utils;->findFileEndText(Ljava/io/File;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    .line 918
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_2

    .line 919
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/io/File;

    .line 921
    .local v8, "file":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v12, v12, v14

    if-lez v12, :cond_1

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 970
    .end local v1    # "apk":Ljava/io/File;
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "foundlibs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :catch_0
    move-exception v7

    .line 971
    .local v7, "e":Ljava/io/FileNotFoundException;
    sget-object v11, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Lib not found!"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v7}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 976
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    :cond_2
    :goto_1
    return-object v10

    .line 926
    .restart local v1    # "apk":Ljava/io/File;
    :cond_3
    packed-switch p0, :pswitch_data_0

    goto :goto_1

    .line 928
    :pswitch_0
    :try_start_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 929
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/armeabi/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 930
    .local v2, "arh":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/tmp/lib/armeabi/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 931
    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 973
    .end local v1    # "apk":Ljava/io/File;
    .end local v2    # "arh":Ljava/lang/String;
    :catch_1
    move-exception v7

    .line 974
    .local v7, "e":Ljava/lang/Exception;
    sget-object v11, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Lib select error: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 933
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v1    # "apk":Ljava/io/File;
    .restart local v2    # "arh":Ljava/lang/String;
    :cond_4
    :try_start_2
    new-instance v11, Ljava/io/FileNotFoundException;

    invoke-direct {v11}, Ljava/io/FileNotFoundException;-><init>()V

    throw v11

    .line 935
    .end local v2    # "arh":Ljava/lang/String;
    :pswitch_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 936
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/armeabi-v7a/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 937
    .local v3, "arh1":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/tmp/lib/armeabi-v7a/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 938
    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 940
    :cond_5
    new-instance v11, Ljava/io/FileNotFoundException;

    invoke-direct {v11}, Ljava/io/FileNotFoundException;-><init>()V

    throw v11

    .line 943
    .end local v3    # "arh1":Ljava/lang/String;
    :pswitch_2
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 944
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/mips/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 945
    .local v4, "arh2":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/tmp/lib/mips/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 946
    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 948
    :cond_6
    new-instance v11, Ljava/io/FileNotFoundException;

    invoke-direct {v11}, Ljava/io/FileNotFoundException;-><init>()V

    throw v11

    .line 951
    .end local v4    # "arh2":Ljava/lang/String;
    :pswitch_3
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 952
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/x86/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 953
    .local v5, "arh3":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/tmp/lib/x86/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 954
    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 956
    :cond_7
    new-instance v11, Ljava/io/FileNotFoundException;

    invoke-direct {v11}, Ljava/io/FileNotFoundException;-><init>()V

    throw v11

    .line 959
    .end local v5    # "arh3":Ljava/lang/String;
    :pswitch_4
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/armeabi/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 960
    .local v6, "arh4":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_8

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 961
    :cond_8
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/armeabi-v7a/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 962
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_9

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 963
    :cond_9
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/mips/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 964
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_a

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 965
    :cond_a
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/x86/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 966
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1

    .line 926
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static unzip(Ljava/io/File;)V
    .locals 13
    .param p0, "apk"    # Ljava/io/File;

    .prologue
    .line 1046
    sget-object v9, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 1048
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1049
    .local v3, "fin":Ljava/io/FileInputStream;
    new-instance v7, Ljava/util/zip/ZipInputStream;

    invoke-direct {v7, v3}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1050
    .local v7, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v6, 0x0

    .line 1051
    .local v6, "ze":Ljava/util/zip/ZipEntry;
    :cond_0
    :goto_0
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 1056
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    const-string v10, "classes"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, ".dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 1057
    new-instance v4, Ljava/io/FileOutputStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/Modified/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1059
    .local v4, "fout":Ljava/io/FileOutputStream;
    const/16 v9, 0x400

    new-array v0, v9, [B

    .line 1061
    .local v0, "buffer":[B
    :goto_1
    invoke-virtual {v7, v0}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v5

    .local v5, "length":I
    const/4 v9, -0x1

    if-eq v5, v9, :cond_1

    .line 1062
    const/4 v9, 0x0

    invoke-virtual {v4, v0, v9, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1076
    .end local v0    # "buffer":[B
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .end local v4    # "fout":Ljava/io/FileOutputStream;
    .end local v5    # "length":I
    .end local v6    # "ze":Ljava/util/zip/ZipEntry;
    .end local v7    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v1

    .line 1078
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v8, Lnet/lingala/zip4j/core/ZipFile;

    invoke-direct {v8, p0}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 1082
    .local v8, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    const-string v9, "classes.dex"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Modified/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1083
    sget-object v9, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "classes.dex"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 1094
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v8    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_2
    return-void

    .line 1064
    .restart local v0    # "buffer":[B
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fout":Ljava/io/FileOutputStream;
    .restart local v5    # "length":I
    .restart local v6    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v7    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_1
    :try_start_2
    sget-object v9, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1065
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1066
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_0

    .line 1074
    .end local v0    # "buffer":[B
    .end local v4    # "fout":Ljava/io/FileOutputStream;
    .end local v5    # "length":I
    :cond_2
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1075
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 1085
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .end local v6    # "ze":Ljava/util/zip/ZipEntry;
    .end local v7    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 1086
    .local v2, "e1":Lnet/lingala/zip4j/exception/ZipException;
    sget-object v9, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error LP: Error classes.dex decompress! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1087
    sget-object v9, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception e1"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_2

    .line 1088
    .end local v2    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_2
    move-exception v2

    .line 1089
    .local v2, "e1":Ljava/lang/Exception;
    sget-object v9, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error LP: Error classes.dex decompress! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1090
    sget-object v9, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception e1"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public static zipLib(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1183
    .local p0, "filesIn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1185
    .local v2, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1186
    .local v1, "file":Ljava/lang/String;
    new-instance v4, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/tmp/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1192
    .end local v1    # "file":Ljava/lang/String;
    .end local v2    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    :catch_0
    move-exception v0

    .line 1193
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error LP: Error libs compress! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1196
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_1
    return-void

    .line 1188
    .restart local v2    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    :cond_1
    :try_start_1
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "checlpis.zip"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/chelpus/Utils;->addFilesToZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1189
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 1190
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "checlpis.zip"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
