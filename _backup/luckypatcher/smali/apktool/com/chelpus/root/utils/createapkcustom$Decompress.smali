.class public Lcom/chelpus/root/utils/createapkcustom$Decompress;
.super Ljava/lang/Object;
.source "createapkcustom.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chelpus/root/utils/createapkcustom;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Decompress"
.end annotation


# instance fields
.field private _location:Ljava/lang/String;

.field private _zipFile:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "zipFile"    # Ljava/lang/String;
    .param p2, "location"    # Ljava/lang/String;

    .prologue
    .line 1201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202
    iput-object p1, p0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_zipFile:Ljava/lang/String;

    .line 1203
    iput-object p2, p0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    .line 1205
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_dirChecker(Ljava/lang/String;)V

    .line 1206
    return-void
.end method

.method private _dirChecker(Ljava/lang/String;)V
    .locals 3
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    .line 1341
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1342
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1343
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1344
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 1346
    :cond_1
    return-void
.end method


# virtual methods
.method public unzip(Ljava/lang/String;)Ljava/lang/String;
    .locals 20
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    .line 1272
    :try_start_0
    new-instance v9, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_zipFile:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v9, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 1273
    .local v9, "fin":Ljava/io/FileInputStream;
    new-instance v15, Ljava/util/zip/ZipInputStream;

    invoke-direct {v15, v9}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1274
    .local v15, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v14, 0x0

    .line 1275
    .local v14, "ze":Ljava/util/zip/ZipEntry;
    :cond_0
    :goto_0
    invoke-virtual {v15}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v14

    if-eqz v14, :cond_6

    .line 1278
    invoke-virtual {v14}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_1

    .line 1279
    invoke-virtual {v14}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_dirChecker(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1313
    .end local v9    # "fin":Ljava/io/FileInputStream;
    .end local v14    # "ze":Ljava/util/zip/ZipEntry;
    .end local v15    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v5

    .line 1315
    .local v5, "e":Ljava/lang/Exception;
    # getter for: Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->access$000()Ljava/io/PrintStream;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Decompressunzip "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1317
    :try_start_1
    new-instance v16, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_zipFile:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-direct/range {v16 .. v17}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 1320
    .local v16, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    invoke-virtual/range {v16 .. v16}, Lnet/lingala/zip4j/core/ZipFile;->getFileHeaders()Ljava/util/List;

    move-result-object v8

    .line 1323
    .local v8, "fileHeaderList":Ljava/util/List;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v11, v0, :cond_7

    .line 1324
    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lnet/lingala/zip4j/model/FileHeader;

    .line 1325
    .local v7, "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    invoke-virtual {v7}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 1326
    # getter for: Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->access$000()Ljava/io/PrintStream;

    move-result-object v17

    invoke-virtual {v7}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1327
    invoke-virtual {v7}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v16 .. v18}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1328
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v17

    .line 1338
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v7    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .end local v8    # "fileHeaderList":Ljava/util/List;
    .end local v11    # "i":I
    .end local v16    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_2
    return-object v17

    .line 1282
    .restart local v9    # "fin":Ljava/io/FileInputStream;
    .restart local v14    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v15    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_1
    :try_start_2
    const-string v17, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 1283
    const-string v17, "/"

    const-string v18, ""

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1286
    :cond_2
    invoke-virtual {v14}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 1287
    invoke-virtual {v14}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v18, "\\/+"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 1288
    .local v13, "tail":[Ljava/lang/String;
    const-string v4, ""

    .line 1289
    .local v4, "data_dir":Ljava/lang/String;
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_3
    array-length v0, v13

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    if-ge v11, v0, :cond_4

    .line 1290
    aget-object v17, v13, v11

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_3

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    aget-object v18, v13, v11

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1289
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 1293
    :cond_4
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_dirChecker(Ljava/lang/String;)V

    .line 1294
    new-instance v10, Ljava/io/FileOutputStream;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v14}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1295
    .local v10, "fout":Ljava/io/FileOutputStream;
    const/16 v17, 0x400

    move/from16 v0, v17

    new-array v3, v0, [B

    .line 1297
    .local v3, "buffer":[B
    :goto_4
    invoke-virtual {v15, v3}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v12

    .local v12, "length":I
    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v12, v0, :cond_5

    .line 1298
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v10, v3, v0, v12}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_4

    .line 1302
    :cond_5
    invoke-virtual {v15}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1303
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 1304
    invoke-virtual {v15}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1305
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    .line 1306
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v14}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_2

    .line 1311
    .end local v3    # "buffer":[B
    .end local v4    # "data_dir":Ljava/lang/String;
    .end local v10    # "fout":Ljava/io/FileOutputStream;
    .end local v11    # "i":I
    .end local v12    # "length":I
    .end local v13    # "tail":[Ljava/lang/String;
    :cond_6
    invoke-virtual {v15}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1312
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1338
    .end local v9    # "fin":Ljava/io/FileInputStream;
    .end local v14    # "ze":Ljava/util/zip/ZipEntry;
    .end local v15    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_7
    :goto_5
    const-string v17, ""

    goto/16 :goto_2

    .line 1323
    .restart local v5    # "e":Ljava/lang/Exception;
    .restart local v7    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .restart local v8    # "fileHeaderList":Ljava/util/List;
    .restart local v11    # "i":I
    .restart local v16    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :cond_8
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 1332
    .end local v7    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .end local v8    # "fileHeaderList":Ljava/util/List;
    .end local v11    # "i":I
    .end local v16    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :catch_1
    move-exception v6

    .line 1333
    .local v6, "e1":Lnet/lingala/zip4j/exception/ZipException;
    invoke-virtual {v6}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    goto :goto_5

    .line 1334
    .end local v6    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_2
    move-exception v6

    .line 1335
    .local v6, "e1":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5
.end method

.method public unzip()V
    .locals 18

    .prologue
    .line 1210
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_zipFile:Ljava/lang/String;

    invoke-direct {v7, v15}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 1211
    .local v7, "fin":Ljava/io/FileInputStream;
    new-instance v13, Ljava/util/zip/ZipInputStream;

    invoke-direct {v13, v7}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1212
    .local v13, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v12, 0x0

    .line 1213
    .local v12, "ze":Ljava/util/zip/ZipEntry;
    :cond_0
    :goto_0
    invoke-virtual {v13}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v12

    if-eqz v12, :cond_6

    .line 1216
    invoke-virtual {v12}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v15

    if-eqz v15, :cond_2

    .line 1217
    invoke-virtual {v12}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_dirChecker(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1244
    .end local v7    # "fin":Ljava/io/FileInputStream;
    .end local v12    # "ze":Ljava/util/zip/ZipEntry;
    .end local v13    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v3

    .line 1246
    .local v3, "e":Ljava/lang/Exception;
    # getter for: Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->access$000()Ljava/io/PrintStream;

    move-result-object v15

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Decompressunzip "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1248
    :try_start_1
    new-instance v14, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_zipFile:Ljava/lang/String;

    invoke-direct {v14, v15}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 1251
    .local v14, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    invoke-virtual {v14}, Lnet/lingala/zip4j/core/ZipFile;->getFileHeaders()Ljava/util/List;

    move-result-object v6

    .line 1254
    .local v6, "fileHeaderList":Ljava/util/List;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v15

    if-ge v9, v15, :cond_7

    .line 1255
    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/lingala/zip4j/model/FileHeader;

    .line 1256
    .local v5, "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    invoke-virtual {v5}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v15

    const-string v16, ".so"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 1257
    # getter for: Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->access$000()Ljava/io/PrintStream;

    move-result-object v15

    invoke-virtual {v5}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1258
    invoke-virtual {v5}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 1254
    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 1220
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v5    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .end local v6    # "fileHeaderList":Ljava/util/List;
    .end local v9    # "i":I
    .end local v14    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    .restart local v7    # "fin":Ljava/io/FileInputStream;
    .restart local v12    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v13    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_2
    :try_start_2
    invoke-virtual {v12}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, ".so"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 1221
    invoke-virtual {v12}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "\\/+"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 1222
    .local v11, "tail":[Ljava/lang/String;
    const-string v2, ""

    .line 1223
    .local v2, "data_dir":Ljava/lang/String;
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_2
    array-length v15, v11

    add-int/lit8 v15, v15, -0x1

    if-ge v9, v15, :cond_4

    .line 1224
    aget-object v15, v11, v9

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_3

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    aget-object v16, v11, v9

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1223
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 1227
    :cond_4
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_dirChecker(Ljava/lang/String;)V

    .line 1228
    new-instance v8, Ljava/io/FileOutputStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v12}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v8, v15}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1229
    .local v8, "fout":Ljava/io/FileOutputStream;
    const/16 v15, 0x400

    new-array v1, v15, [B

    .line 1231
    .local v1, "buffer":[B
    :goto_3
    invoke-virtual {v13, v1}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v10

    .local v10, "length":I
    const/4 v15, -0x1

    if-eq v10, v15, :cond_5

    .line 1232
    const/4 v15, 0x0

    invoke-virtual {v8, v1, v15, v10}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_3

    .line 1236
    :cond_5
    invoke-virtual {v13}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1237
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_0

    .line 1242
    .end local v1    # "buffer":[B
    .end local v2    # "data_dir":Ljava/lang/String;
    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .end local v9    # "i":I
    .end local v10    # "length":I
    .end local v11    # "tail":[Ljava/lang/String;
    :cond_6
    invoke-virtual {v13}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1243
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1269
    .end local v7    # "fin":Ljava/io/FileInputStream;
    .end local v12    # "ze":Ljava/util/zip/ZipEntry;
    .end local v13    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_7
    :goto_4
    return-void

    .line 1262
    .restart local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v4

    .line 1263
    .local v4, "e1":Lnet/lingala/zip4j/exception/ZipException;
    invoke-virtual {v4}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    goto :goto_4

    .line 1264
    .end local v4    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_2
    move-exception v4

    .line 1265
    .local v4, "e1":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4
.end method
