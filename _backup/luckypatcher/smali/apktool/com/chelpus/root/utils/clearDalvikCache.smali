.class public Lcom/chelpus/root/utils/clearDalvikCache;
.super Ljava/lang/Object;
.source "clearDalvikCache.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 16
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 14
    new-instance v10, Lcom/chelpus/root/utils/clearDalvikCache$1;

    invoke-direct {v10}, Lcom/chelpus/root/utils/clearDalvikCache$1;-><init>()V

    invoke-static {v10}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 15
    const/4 v10, 0x0

    aget-object v10, p0, v10

    sput-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    .line 16
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/ClearDalvik.on"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 17
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/ClearDalvik.on"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 18
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/ClearDalvik.on"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 19
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v11, "LuckyPatcher: (error) try delete ClearDalvik.on - fault!"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 109
    :cond_0
    :goto_0
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 110
    return-void

    .line 22
    :cond_1
    :try_start_0
    new-instance v10, Ljava/io/File;

    const-string v11, "/mnt/asec"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 23
    .local v3, "mnt_asec":[Ljava/io/File;
    array-length v12, v3

    const/4 v10, 0x0

    move v11, v10

    :goto_1
    if-ge v11, v12, :cond_4

    aget-object v0, v3, v11

    .line 24
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 25
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 26
    .local v2, "files":[Ljava/io/File;
    array-length v13, v2

    const/4 v10, 0x0

    :goto_2
    if-ge v10, v13, :cond_3

    aget-object v1, v2, v10

    .line 27
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v14

    const-string v15, ".odex"

    invoke-virtual {v14, v15}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 28
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    const-string v15, "rw"

    invoke-static {v14, v15}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 29
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 23
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "files":[Ljava/io/File;
    :cond_3
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto :goto_1

    .line 35
    .end local v0    # "dir":Ljava/io/File;
    .end local v3    # "mnt_asec":[Ljava/io/File;
    :catch_0
    move-exception v10

    .line 37
    :cond_4
    :try_start_1
    new-instance v10, Ljava/io/File;

    const-string v11, "/data/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_8

    .line 38
    new-instance v10, Ljava/io/File;

    const-string v11, "/data/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 39
    .restart local v2    # "files":[Ljava/io/File;
    array-length v12, v2

    const/4 v10, 0x0

    move v11, v10

    :goto_3
    if-ge v11, v12, :cond_8

    aget-object v9, v2, v11

    .line 41
    .local v9, "tail":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 42
    invoke-virtual {v9}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    .line 43
    .local v5, "oats":[Ljava/io/File;
    if-eqz v5, :cond_6

    array-length v10, v5

    if-lez v10, :cond_6

    .line 44
    array-length v13, v5

    const/4 v10, 0x0

    :goto_4
    if-ge v10, v13, :cond_6

    aget-object v4, v5, v10

    .line 45
    .local v4, "oat":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 44
    :cond_5
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .line 49
    .end local v4    # "oat":Ljava/io/File;
    .end local v5    # "oats":[Ljava/io/File;
    :cond_6
    invoke-virtual {v9}, Ljava/io/File;->isFile()Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-virtual {v9}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 39
    :cond_7
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto :goto_3

    .line 51
    .end local v2    # "files":[Ljava/io/File;
    .end local v9    # "tail":Ljava/io/File;
    :catch_1
    move-exception v10

    .line 53
    :cond_8
    :try_start_2
    new-instance v10, Ljava/io/File;

    const-string v11, "/cache/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 54
    new-instance v10, Ljava/io/File;

    const-string v11, "/cache/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "files":[Ljava/lang/String;
    array-length v11, v2

    const/4 v10, 0x0

    :goto_5
    if-ge v10, v11, :cond_9

    aget-object v9, v2, v10

    .line 56
    .local v9, "tail":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "/cache/dalvik-cache/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 55
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 58
    .end local v2    # "files":[Ljava/lang/String;
    .end local v9    # "tail":Ljava/lang/String;
    :catch_2
    move-exception v10

    .line 60
    :cond_9
    :try_start_3
    new-instance v10, Ljava/io/File;

    const-string v11, "/sd-ext/data/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_a

    .line 61
    new-instance v10, Ljava/io/File;

    const-string v11, "/sd-ext/data/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 62
    .restart local v2    # "files":[Ljava/lang/String;
    array-length v11, v2

    const/4 v10, 0x0

    :goto_6
    if-ge v10, v11, :cond_a

    aget-object v9, v2, v10

    .line 63
    .restart local v9    # "tail":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "/sd-ext/data/dalvik-cache/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 62
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 65
    .end local v2    # "files":[Ljava/lang/String;
    .end local v9    # "tail":Ljava/lang/String;
    :catch_3
    move-exception v10

    .line 67
    :cond_a
    :try_start_4
    new-instance v10, Ljava/io/File;

    const-string v11, "/sd-ext/data/cache/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_b

    .line 68
    new-instance v10, Ljava/io/File;

    const-string v11, "/sd-ext/data/cache/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 69
    .restart local v2    # "files":[Ljava/lang/String;
    array-length v11, v2

    const/4 v10, 0x0

    :goto_7
    if-ge v10, v11, :cond_b

    aget-object v9, v2, v10

    .line 70
    .restart local v9    # "tail":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "/sd-ext/data/cache/dalvik-cache/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->delete()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 69
    add-int/lit8 v10, v10, 0x1

    goto :goto_7

    .line 72
    .end local v2    # "files":[Ljava/lang/String;
    .end local v9    # "tail":Ljava/lang/String;
    :catch_4
    move-exception v10

    .line 74
    :cond_b
    :try_start_5
    new-instance v10, Ljava/io/File;

    const-string v11, "/data/app"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_d

    .line 75
    new-instance v10, Ljava/io/File;

    const-string v11, "/data/app"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 76
    .restart local v2    # "files":[Ljava/lang/String;
    array-length v11, v2

    const/4 v10, 0x0

    :goto_8
    if-ge v10, v11, :cond_d

    aget-object v9, v2, v10

    .line 77
    .restart local v9    # "tail":Ljava/lang/String;
    const-string v12, ".odex"

    invoke-virtual {v9, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 78
    new-instance v12, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "/data/app/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->delete()Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    .line 76
    :cond_c
    add-int/lit8 v10, v10, 0x1

    goto :goto_8

    .line 80
    .end local v2    # "files":[Ljava/lang/String;
    .end local v9    # "tail":Ljava/lang/String;
    :catch_5
    move-exception v10

    .line 81
    :cond_d
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v11, "LuckyPatcher: Dalvik-Cache deleted."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 84
    :try_start_6
    const-string v10, "/system"

    const-string v11, "rw"

    invoke-static {v10, v11}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 85
    new-instance v10, Ljava/io/File;

    const-string v11, "/system/app"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    .line 86
    .local v8, "system_files":[Ljava/io/File;
    array-length v11, v8

    const/4 v10, 0x0

    :goto_9
    if-ge v10, v11, :cond_f

    aget-object v7, v8, v10

    .line 87
    .local v7, "sys_file":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v6, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 88
    .local v6, "odex":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_e

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v12

    invoke-virtual {v12}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, ".apk"

    invoke-virtual {v12, v13}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 89
    invoke-static {v7}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 90
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 86
    :cond_e
    add-int/lit8 v10, v10, 0x1

    goto :goto_9

    .line 94
    .end local v6    # "odex":Ljava/io/File;
    .end local v7    # "sys_file":Ljava/io/File;
    :cond_f
    new-instance v10, Ljava/io/File;

    const-string v11, "/system/priv-app"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    .line 95
    array-length v11, v8

    const/4 v10, 0x0

    :goto_a
    if-ge v10, v11, :cond_11

    aget-object v7, v8, v10

    .line 96
    .restart local v7    # "sys_file":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v6, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 97
    .restart local v6    # "odex":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_10

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v12

    invoke-virtual {v12}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, ".apk"

    invoke-virtual {v12, v13}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_10

    .line 98
    invoke-static {v7}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v12

    if-eqz v12, :cond_10

    .line 99
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 95
    :cond_10
    add-int/lit8 v10, v10, 0x1

    goto :goto_a

    .line 103
    .end local v6    # "odex":Ljava/io/File;
    .end local v7    # "sys_file":Ljava/io/File;
    :cond_11
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v11, "LuckyPatcher: System apps deodex all."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    .line 104
    .end local v8    # "system_files":[Ljava/io/File;
    :catch_6
    move-exception v10

    goto/16 :goto_0
.end method
