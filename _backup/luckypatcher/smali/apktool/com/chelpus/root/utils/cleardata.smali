.class public Lcom/chelpus/root/utils/cleardata;
.super Ljava/lang/Object;
.source "cleardata.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 10
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 14
    new-instance v7, Lcom/chelpus/root/utils/cleardata$1;

    invoke-direct {v7}, Lcom/chelpus/root/utils/cleardata$1;-><init>()V

    invoke-static {v7}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 16
    new-instance v5, Lcom/chelpus/Utils;

    const-string v7, "clear data"

    invoke-direct {v5, v7}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    .line 18
    .local v5, "ut":Lcom/chelpus/Utils;
    const/4 v7, 0x1

    :try_start_0
    aget-object v4, p0, v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    .local v4, "system":Ljava/lang/String;
    :goto_0
    :try_start_1
    new-instance v0, Ljava/io/File;

    const/4 v7, 0x0

    aget-object v7, p0, v7

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 25
    .local v0, "appDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 26
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 27
    .local v1, "children":[Ljava/lang/String;
    array-length v8, v1

    move v7, v6

    :goto_1
    if-ge v7, v8, :cond_2

    aget-object v3, v1, v7

    .line 28
    .local v3, "s":Ljava/lang/String;
    const-string v9, "lib"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "system"

    invoke-virtual {v4, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 30
    :cond_0
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5, v9}, Lcom/chelpus/Utils;->deleteFolder(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 27
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 19
    .end local v0    # "appDir":Ljava/io/File;
    .end local v1    # "children":[Ljava/lang/String;
    .end local v3    # "s":Ljava/lang/String;
    .end local v4    # "system":Ljava/lang/String;
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "not"

    .restart local v4    # "system":Ljava/lang/String;
    goto :goto_0

    .line 35
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "appDir":Ljava/io/File;
    :cond_2
    :try_start_2
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "ok"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 36
    new-instance v0, Ljava/io/File;

    .end local v0    # "appDir":Ljava/io/File;
    const/4 v7, 0x0

    aget-object v7, p0, v7

    const-string v8, "/data/data/"

    const-string v9, "/dbdata/databases/"

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 37
    .restart local v0    # "appDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 38
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 39
    .restart local v1    # "children":[Ljava/lang/String;
    array-length v8, v1

    move v7, v6

    :goto_2
    if-ge v7, v8, :cond_5

    aget-object v3, v1, v7

    .line 40
    .restart local v3    # "s":Ljava/lang/String;
    const-string v9, "lib"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    const-string v9, "system"

    invoke-virtual {v4, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 41
    :cond_3
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5, v9}, Lcom/chelpus/Utils;->deleteFolder(Ljava/io/File;)V

    .line 39
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 47
    .end local v1    # "children":[Ljava/lang/String;
    .end local v3    # "s":Ljava/lang/String;
    :cond_5
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "ok"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 48
    new-instance v0, Ljava/io/File;

    .end local v0    # "appDir":Ljava/io/File;
    const/4 v7, 0x0

    aget-object v7, p0, v7

    const-string v8, "/data/data/"

    const-string v9, "/dbdata/data/"

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 49
    .restart local v0    # "appDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 50
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 51
    .restart local v1    # "children":[Ljava/lang/String;
    array-length v7, v1

    :goto_3
    if-ge v6, v7, :cond_8

    aget-object v3, v1, v6

    .line 52
    .restart local v3    # "s":Ljava/lang/String;
    const-string v8, "lib"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    const-string v8, "system"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 53
    :cond_6
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Lcom/chelpus/Utils;->deleteFolder(Ljava/io/File;)V

    .line 51
    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 58
    .end local v1    # "children":[Ljava/lang/String;
    .end local v3    # "s":Ljava/lang/String;
    :cond_8
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "ok"

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 60
    const-string v6, "system"

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 61
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 67
    .end local v0    # "appDir":Ljava/io/File;
    :cond_9
    :goto_4
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 68
    return-void

    .line 64
    :catch_1
    move-exception v2

    .line 65
    .restart local v2    # "e":Ljava/lang/Exception;
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception e"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_4
.end method
