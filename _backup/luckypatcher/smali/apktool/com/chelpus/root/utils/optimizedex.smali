.class public Lcom/chelpus/root/utils/optimizedex;
.super Ljava/lang/Object;
.source "optimizedex.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 8
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 12
    new-instance v5, Lcom/chelpus/root/utils/optimizedex$1;

    invoke-direct {v5}, Lcom/chelpus/root/utils/optimizedex$1;-><init>()V

    invoke-static {v5}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 13
    const/4 v5, 0x0

    aget-object v0, p0, v5

    .line 14
    .local v0, "path_apk":Ljava/lang/String;
    aget-object v3, p0, v7

    .line 15
    .local v3, "uid":Ljava/lang/String;
    const/4 v5, 0x2

    aget-object v2, p0, v5

    .line 16
    .local v2, "runtime":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/tmp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 17
    .local v4, "workingDir":Ljava/lang/String;
    const/16 v1, 0xff

    .line 18
    .local v1, "res":I
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 19
    invoke-static {v0, v3}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "ART"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v0, v4, v5, v3, v6}, Lcom/chelpus/Utils;->dexopt(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)I

    move-result v1

    .line 23
    :goto_0
    if-nez v1, :cond_0

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "chelpus_return_0"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 24
    :cond_0
    if-ne v1, v7, :cond_1

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "chelpus_return_1"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 25
    :cond_1
    const/4 v5, 0x4

    if-ne v1, v5, :cond_2

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "chelpus_return_4"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 27
    :cond_2
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 28
    return-void

    .line 21
    :cond_3
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "chelpus_return_10"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method
