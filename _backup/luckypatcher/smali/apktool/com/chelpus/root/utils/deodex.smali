.class public Lcom/chelpus/root/utils/deodex;
.super Ljava/lang/Object;
.source "deodex.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 8
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 14
    new-instance v5, Lcom/chelpus/root/utils/deodex$1;

    invoke-direct {v5}, Lcom/chelpus/root/utils/deodex$1;-><init>()V

    invoke-static {v5}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 15
    const/4 v5, 0x0

    aget-object v5, p0, v5

    invoke-static {v5}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V

    .line 16
    aget-object v5, p0, v7

    const-string v6, "rw"

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 20
    const/4 v5, 0x1

    :try_start_0
    aget-object v5, p0, v5

    invoke-static {v5}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 21
    .local v2, "dalvik":Ljava/io/File;
    if-nez v2, :cond_5

    .line 22
    new-instance v5, Ljava/io/FileNotFoundException;

    invoke-direct {v5}, Ljava/io/FileNotFoundException;-><init>()V

    throw v5
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 38
    .end local v2    # "dalvik":Ljava/io/File;
    :catch_0
    move-exception v4

    .line 39
    .local v4, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 40
    new-instance v5, Ljava/io/File;

    aget-object v6, p0, v7

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 41
    aget-object v5, p0, v7

    invoke-static {v5, v7}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "backTemp":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 43
    .local v0, "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 44
    :cond_0
    new-instance v0, Ljava/io/File;

    .end local v0    # "backFile":Ljava/io/File;
    const-string v5, "-2"

    const-string v6, "-1"

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 45
    .restart local v0    # "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 46
    :cond_1
    new-instance v0, Ljava/io/File;

    .end local v0    # "backFile":Ljava/io/File;
    const-string v5, "-1"

    const-string v6, "-2"

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 47
    .restart local v0    # "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 48
    :cond_2
    new-instance v0, Ljava/io/File;

    .end local v0    # "backFile":Ljava/io/File;
    const-string v5, "-2"

    const-string v6, ""

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 49
    .restart local v0    # "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 50
    :cond_3
    new-instance v0, Ljava/io/File;

    .end local v0    # "backFile":Ljava/io/File;
    const-string v5, "-1"

    const-string v6, ""

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 51
    .restart local v0    # "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 59
    .end local v0    # "backFile":Ljava/io/File;
    .end local v1    # "backTemp":Ljava/lang/String;
    .end local v4    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_4
    :goto_0
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 60
    return-void

    .line 24
    .restart local v2    # "dalvik":Ljava/io/File;
    :cond_5
    const/4 v5, 0x1

    :try_start_1
    aget-object v5, p0, v5

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 25
    .restart local v1    # "backTemp":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 26
    .restart local v0    # "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 27
    :cond_6
    new-instance v0, Ljava/io/File;

    .end local v0    # "backFile":Ljava/io/File;
    const-string v5, "-2"

    const-string v6, "-1"

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 28
    .restart local v0    # "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 29
    :cond_7
    new-instance v0, Ljava/io/File;

    .end local v0    # "backFile":Ljava/io/File;
    const-string v5, "-1"

    const-string v6, "-2"

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 30
    .restart local v0    # "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 31
    :cond_8
    new-instance v0, Ljava/io/File;

    .end local v0    # "backFile":Ljava/io/File;
    const-string v5, "-2"

    const-string v6, ""

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 32
    .restart local v0    # "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 33
    :cond_9
    new-instance v0, Ljava/io/File;

    .end local v0    # "backFile":Ljava/io/File;
    const-string v5, "-1"

    const-string v6, ""

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 34
    .restart local v0    # "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 54
    .end local v0    # "backFile":Ljava/io/File;
    .end local v1    # "backTemp":Ljava/lang/String;
    .end local v2    # "dalvik":Ljava/io/File;
    :catch_1
    move-exception v3

    .line 55
    .local v3, "e":Ljava/lang/Exception;
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception e"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
