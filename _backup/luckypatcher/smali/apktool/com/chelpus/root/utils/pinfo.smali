.class public Lcom/chelpus/root/utils/pinfo;
.super Ljava/lang/Object;
.source "pinfo.java"


# static fields
.field public static classesFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public static toolfilesdir:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/pinfo;->classesFiles:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 13
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x3

    .line 24
    new-instance v6, Lcom/chelpus/root/utils/pinfo$1;

    invoke-direct {v6}, Lcom/chelpus/root/utils/pinfo$1;-><init>()V

    invoke-static {v6}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 26
    aget-object v4, p0, v10

    .line 27
    .local v4, "source_file_v":Ljava/lang/String;
    aget-object v6, p0, v11

    sput-object v6, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    .line 28
    aget-object v5, p0, v12

    .line 29
    .local v5, "uid":Ljava/lang/String;
    aget-object v6, p0, v9

    if-eqz v6, :cond_2

    aget-object v6, p0, v9

    const-string v7, "recovery"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 31
    :try_start_0
    invoke-static {v4, v5}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 32
    .local v3, "odex":Ljava/lang/String;
    const-string v6, "rw"

    invoke-static {v3, v6}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 34
    invoke-static {v4}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 35
    .local v1, "dc":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_lpbackup"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 36
    .local v0, "b_dc":Ljava/io/File;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 37
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 38
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 47
    :goto_0
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_lpbackup"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 48
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_lpbackup"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 66
    .end local v0    # "b_dc":Ljava/io/File;
    .end local v1    # "dc":Ljava/io/File;
    .end local v3    # "odex":Ljava/lang/String;
    :cond_0
    :goto_1
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 67
    return-void

    .line 41
    .restart local v0    # "b_dc":Ljava/io/File;
    .restart local v1    # "dc":Ljava/io/File;
    .restart local v3    # "odex":Ljava/lang/String;
    :cond_1
    :try_start_1
    sget-object v6, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v4, v5, v7}, Lcom/chelpus/Utils;->create_DC_root(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 42
    :catch_0
    move-exception v2

    .line 43
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 50
    .end local v0    # "b_dc":Ljava/io/File;
    .end local v1    # "dc":Ljava/io/File;
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "odex":Ljava/lang/String;
    :catch_1
    move-exception v2

    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 52
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/p.apk"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/chelpus/root/utils/pinfo;->unzipART(Ljava/io/File;)V

    .line 53
    invoke-static {v4, v5}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 54
    .restart local v3    # "odex":Ljava/lang/String;
    invoke-static {v4}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 55
    .restart local v1    # "dc":Ljava/io/File;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 56
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_lpbackup"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 57
    sget-object v6, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_lpbackup"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/chelpus/Utils;->dalvikvm_copyFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 58
    new-array v6, v9, [Ljava/lang/String;

    const-string v7, "chmod"

    aput-object v7, v6, v10

    const-string v7, "777"

    aput-object v7, v6, v11

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v12

    invoke-static {v6}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 59
    new-array v6, v9, [Ljava/lang/String;

    const-string v7, "chown"

    aput-object v7, v6, v10

    const-string v7, "0:0"

    aput-object v7, v6, v11

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v12

    invoke-static {v6}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 60
    new-array v6, v9, [Ljava/lang/String;

    const-string v7, "chown"

    aput-object v7, v6, v10

    const-string v7, "0.0"

    aput-object v7, v6, v11

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v12

    invoke-static {v6}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 62
    :cond_3
    const-string v6, "rw"

    invoke-static {v3, v6}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 63
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_4

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_lpbackup"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 64
    :cond_4
    sget-object v6, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    sget-object v7, Lcom/chelpus/root/utils/pinfo;->classesFiles:Ljava/util/ArrayList;

    invoke-static {v6, v7, v4, v5, v3}, Lcom/chelpus/Utils;->create_ODEX_root(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public static unzipART(Ljava/io/File;)V
    .locals 22
    .param p0, "apk"    # Ljava/io/File;

    .prologue
    .line 69
    const/4 v7, 0x0

    .local v7, "found1":Z
    const/4 v8, 0x0

    .line 71
    .local v8, "found2":Z
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 72
    .local v6, "fin":Ljava/io/FileInputStream;
    new-instance v16, Ljava/util/zip/ZipInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v6}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 73
    .local v16, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v15, 0x0

    .line 75
    .local v15, "ze":Ljava/util/zip/ZipEntry;
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v15

    .line 76
    const/4 v14, 0x1

    .line 77
    .local v14, "search":Z
    :goto_0
    if-eqz v15, :cond_4

    if-eqz v14, :cond_4

    .line 83
    invoke-virtual {v15}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v11

    .line 85
    .local v11, "haystack":Ljava/lang/String;
    const-string v18, "classes"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1

    const-string v18, ".dex"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1

    const-string v18, "/"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 87
    new-instance v9, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 89
    .local v9, "fout":Ljava/io/FileOutputStream;
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v2, v0, [B

    .line 92
    .local v2, "buffer":[B
    :goto_1
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v12

    .local v12, "length":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v12, v0, :cond_0

    .line 93
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v9, v2, v0, v12}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 125
    .end local v2    # "buffer":[B
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .end local v9    # "fout":Ljava/io/FileOutputStream;
    .end local v11    # "haystack":Ljava/lang/String;
    .end local v12    # "length":I
    .end local v14    # "search":Z
    .end local v15    # "ze":Ljava/util/zip/ZipEntry;
    .end local v16    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v4

    .line 127
    .local v4, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v17, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 131
    .local v17, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    const-string v18, "classes.dex"

    sget-object v19, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v17 .. v19}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    sget-object v18, Lcom/chelpus/root/utils/pinfo;->classesFiles:Ljava/util/ArrayList;

    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "classes.dex"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "classes.dex"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 134
    const-string v18, "AndroidManifest.xml"

    sget-object v19, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v17 .. v19}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "AndroidManifest.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 144
    .end local v17    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_2
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 148
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_3
    return-void

    .line 97
    .restart local v2    # "buffer":[B
    .restart local v6    # "fin":Ljava/io/FileInputStream;
    .restart local v9    # "fout":Ljava/io/FileOutputStream;
    .restart local v11    # "haystack":Ljava/lang/String;
    .restart local v12    # "length":I
    .restart local v14    # "search":Z
    .restart local v15    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v16    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_0
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 98
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 99
    sget-object v18, Lcom/chelpus/root/utils/pinfo;->classesFiles:Ljava/util/ArrayList;

    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 103
    .end local v2    # "buffer":[B
    .end local v9    # "fout":Ljava/io/FileOutputStream;
    .end local v12    # "length":I
    :cond_1
    const-string v18, "AndroidManifest.xml"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 104
    new-instance v10, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "AndroidManifest.xml"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 106
    .local v10, "fout2":Ljava/io/FileOutputStream;
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v3, v0, [B

    .line 108
    .local v3, "buffer2":[B
    :goto_4
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v13

    .local v13, "length2":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v13, v0, :cond_2

    .line 109
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v10, v3, v0, v13}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_4

    .line 111
    :cond_2
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 112
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 113
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/pinfo;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "AndroidManifest.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 114
    const/4 v8, 0x1

    .line 116
    .end local v3    # "buffer2":[B
    .end local v10    # "fout2":Ljava/io/FileOutputStream;
    .end local v13    # "length2":I
    :cond_3
    if-eqz v7, :cond_5

    if-eqz v8, :cond_5

    .line 117
    const/4 v14, 0x0

    .line 123
    .end local v11    # "haystack":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->close()V

    .line 124
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_3

    .line 120
    .restart local v11    # "haystack":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v15

    .line 122
    goto/16 :goto_0

    .line 137
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .end local v11    # "haystack":Ljava/lang/String;
    .end local v14    # "search":Z
    .end local v15    # "ze":Ljava/util/zip/ZipEntry;
    .end local v16    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v5

    .line 138
    .local v5, "e1":Lnet/lingala/zip4j/exception/ZipException;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error classes.dex decompress! "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 139
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_2

    .line 140
    .end local v5    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_2
    move-exception v5

    .line 141
    .local v5, "e1":Ljava/lang/Exception;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error classes.dex decompress! "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 142
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_2
.end method
