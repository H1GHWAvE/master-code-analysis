.class public Lcom/chelpus/root/utils/live_backup;
.super Ljava/lang/Object;
.source "live_backup.java"


# static fields
.field private static dalvikDexIn:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-string v0, "/data/dalvik-cache/data@app@zamenitetodelo-1.apk@classes.dex"

    sput-object v0, Lcom/chelpus/root/utils/live_backup;->dalvikDexIn:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 11
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 16
    new-instance v8, Lcom/chelpus/root/utils/live_backup$1;

    invoke-direct {v8}, Lcom/chelpus/root/utils/live_backup$1;-><init>()V

    invoke-static {v8}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 23
    sget-object v8, Lcom/chelpus/root/utils/live_backup;->dalvikDexIn:Ljava/lang/String;

    const-string v9, "zamenitetodelo"

    const/4 v10, 0x0

    aget-object v10, p0, v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 27
    .local v2, "dalvikDex":Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 28
    .local v5, "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 29
    .restart local v5    # "localFile1":Ljava/io/File;
    :cond_0
    new-instance v6, Ljava/io/File;

    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 30
    .local v6, "localFile2":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    move-object v6, v5

    .line 31
    :cond_1
    const-string v8, "data@app"

    const-string v9, "mnt@asec"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 32
    .local v3, "dalvikDexTemp":Ljava/lang/String;
    const-string v8, ".apk@classes.dex"

    const-string v9, "@pkg.apk@classes.dex"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 33
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 34
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    move-object v6, v5

    .line 35
    :cond_2
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 36
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    move-object v6, v5

    .line 37
    :cond_3
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 38
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_4

    move-object v6, v5

    .line 39
    :cond_4
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_5

    new-instance v8, Ljava/io/FileNotFoundException;

    invoke-direct {v8}, Ljava/io/FileNotFoundException;-><init>()V

    throw v8
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 49
    .end local v3    # "dalvikDexTemp":Ljava/lang/String;
    .end local v5    # "localFile1":Ljava/io/File;
    .end local v6    # "localFile2":Ljava/io/File;
    :catch_0
    move-exception v7

    .line 50
    .local v7, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v9, "Error: Backup failed!"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 55
    .end local v7    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :goto_0
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 56
    return-void

    .line 42
    .restart local v3    # "dalvikDexTemp":Ljava/lang/String;
    .restart local v5    # "localFile1":Ljava/io/File;
    .restart local v6    # "localFile2":Ljava/io/File;
    :cond_5
    :try_start_1
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "classes"

    const-string v10, "backup"

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "backTemp":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 44
    .local v0, "backFile":Ljava/io/File;
    invoke-static {v6, v0}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 45
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v9, "Backup - done!"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 52
    .end local v0    # "backFile":Ljava/io/File;
    .end local v1    # "backTemp":Ljava/lang/String;
    .end local v3    # "dalvikDexTemp":Ljava/lang/String;
    .end local v5    # "localFile1":Ljava/io/File;
    .end local v6    # "localFile2":Ljava/io/File;
    :catch_1
    move-exception v4

    .line 53
    .local v4, "e":Ljava/lang/Exception;
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception e"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method
