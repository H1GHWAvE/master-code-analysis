.class public Lcom/chelpus/root/utils/remove;
.super Ljava/lang/Object;
.source "remove.java"


# static fields
.field private static dalvikDexIn:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-string v0, "/data/dalvik-cache/data@app@zamenitetodelo-1.apk@classes.dex"

    sput-object v0, Lcom/chelpus/root/utils/remove;->dalvikDexIn:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 44
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 22
    new-instance v3, Lcom/chelpus/root/utils/remove$1;

    invoke-direct {v3}, Lcom/chelpus/root/utils/remove$1;-><init>()V

    invoke-static {v3}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 23
    const/16 v3, 0xc

    new-array v8, v3, [B

    fill-array-data v8, :array_0

    .line 24
    .local v8, "byteOrig":[B
    const/16 v3, 0xc

    new-array v0, v3, [B

    move-object/from16 v29, v0

    fill-array-data v29, :array_1

    .line 25
    .local v29, "mask":[B
    const/16 v3, 0xc

    new-array v12, v3, [B

    fill-array-data v12, :array_2

    .line 26
    .local v12, "byteReplace":[B
    const/16 v3, 0xc

    new-array v0, v3, [B

    move-object/from16 v39, v0

    fill-array-data v39, :array_3

    .line 28
    .local v39, "rep_mask":[B
    const/16 v3, 0xc

    new-array v9, v3, [B

    fill-array-data v9, :array_4

    .line 29
    .local v9, "byteOrig2":[B
    const/16 v3, 0xc

    new-array v0, v3, [B

    move-object/from16 v30, v0

    fill-array-data v30, :array_5

    .line 30
    .local v30, "mask2":[B
    const/16 v3, 0xc

    new-array v13, v3, [B

    fill-array-data v13, :array_6

    .line 31
    .local v13, "byteReplace2":[B
    const/16 v3, 0xc

    new-array v0, v3, [B

    move-object/from16 v40, v0

    fill-array-data v40, :array_7

    .line 33
    .local v40, "rep_mask2":[B
    const/16 v3, 0xe

    new-array v10, v3, [B

    fill-array-data v10, :array_8

    .line 34
    .local v10, "byteOrig3":[B
    const/16 v3, 0xe

    new-array v0, v3, [B

    move-object/from16 v31, v0

    fill-array-data v31, :array_9

    .line 35
    .local v31, "mask3":[B
    const/16 v3, 0xe

    new-array v14, v3, [B

    fill-array-data v14, :array_a

    .line 36
    .local v14, "byteReplace3":[B
    const/16 v3, 0xe

    new-array v0, v3, [B

    move-object/from16 v41, v0

    fill-array-data v41, :array_b

    .line 38
    .local v41, "rep_mask3":[B
    const/16 v3, 0xe

    new-array v11, v3, [B

    fill-array-data v11, :array_c

    .line 39
    .local v11, "byteOrig4":[B
    const/16 v3, 0xe

    new-array v0, v3, [B

    move-object/from16 v32, v0

    fill-array-data v32, :array_d

    .line 40
    .local v32, "mask4":[B
    const/16 v3, 0xe

    new-array v15, v3, [B

    fill-array-data v15, :array_e

    .line 41
    .local v15, "byteReplace4":[B
    const/16 v3, 0xe

    new-array v0, v3, [B

    move-object/from16 v42, v0

    fill-array-data v42, :array_f

    .line 48
    .local v42, "rep_mask4":[B
    sget-object v3, Lcom/chelpus/root/utils/remove;->dalvikDexIn:Ljava/lang/String;

    const-string v4, "zamenitetodelo"

    const/4 v5, 0x0

    aget-object v5, p0, v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v19

    .line 52
    .local v19, "dalvikDex":Ljava/lang/String;
    :try_start_0
    new-instance v26, Ljava/io/File;

    move-object/from16 v0, v26

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 53
    .local v26, "localFile1":Ljava/io/File;
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v26, Ljava/io/File;

    .end local v26    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, "-2"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 54
    .restart local v26    # "localFile1":Ljava/io/File;
    :cond_0
    new-instance v27, Ljava/io/File;

    const-string v3, "-1"

    const-string v4, ""

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 55
    .local v27, "localFile2":Ljava/io/File;
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v27, v26

    .line 56
    :cond_1
    const-string v3, "data@app"

    const-string v4, "mnt@asec"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v20

    .line 57
    .local v20, "dalvikDexTemp":Ljava/lang/String;
    const-string v3, ".apk@classes.dex"

    const-string v4, "@pkg.apk@classes.dex"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v20

    .line 58
    new-instance v26, Ljava/io/File;

    .end local v26    # "localFile1":Ljava/io/File;
    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 59
    .restart local v26    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v27, v26

    .line 60
    :cond_2
    new-instance v26, Ljava/io/File;

    .end local v26    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, "-2"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 61
    .restart local v26    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    move-object/from16 v27, v26

    .line 62
    :cond_3
    new-instance v26, Ljava/io/File;

    .end local v26    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, ""

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 63
    .restart local v26    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v27, v26

    .line 64
    :cond_4
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_6

    new-instance v3, Ljava/io/FileNotFoundException;

    invoke-direct {v3}, Ljava/io/FileNotFoundException;-><init>()V

    throw v3
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 236
    .end local v20    # "dalvikDexTemp":Ljava/lang/String;
    .end local v26    # "localFile1":Ljava/io/File;
    .end local v27    # "localFile2":Ljava/io/File;
    :catch_0
    move-exception v28

    .line 237
    .local v28, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 244
    .end local v28    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_5
    :goto_0
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 245
    return-void

    .line 66
    .restart local v20    # "dalvikDexTemp":Ljava/lang/String;
    .restart local v26    # "localFile1":Ljava/io/File;
    .restart local v27    # "localFile2":Ljava/io/File;
    :cond_6
    :try_start_1
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v4, "rw"

    move-object/from16 v0, v27

    invoke-direct {v3, v0, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 67
    .local v2, "ChannelDex":Ljava/nio/channels/FileChannel;
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v22

    .line 68
    .local v22, "fileBytes":Ljava/nio/MappedByteBuffer;
    const/16 v33, 0x0

    .line 69
    .local v33, "patch":Z
    const/16 v34, 0x0

    .line 70
    .local v34, "patch1":Z
    const/16 v35, 0x0

    .line 71
    .local v35, "patch2":Z
    const/16 v36, 0x0

    .line 72
    .local v36, "patch3":Z
    const/16 v37, 0x0

    .line 75
    .local v37, "patch4":Z
    const/16 v43, 0x0

    .line 77
    .local v43, "repbyte":B
    const-wide/16 v24, 0x0

    .local v24, "j":J
    :goto_1
    :try_start_2
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_23

    .line 80
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v18

    .line 81
    .local v18, "curentPos":I
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v17

    .line 84
    .local v17, "curentByte":B
    const/4 v3, 0x0

    aget-byte v3, v8, v3

    move/from16 v0, v17

    if-ne v0, v3, :cond_f

    .line 86
    const/4 v3, 0x0

    aget-byte v3, v39, v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_7

    move/from16 v43, v17

    .line 87
    :cond_7
    const/4 v3, 0x0

    aget-byte v3, v39, v3

    if-nez v3, :cond_8

    const/4 v3, 0x0

    aput-byte v17, v12, v3

    .line 88
    :cond_8
    const/16 v23, 0x1

    .local v23, "i":I
    const/16 v16, 0x3e8

    .line 89
    .local v16, "c":I
    add-int/lit8 v3, v18, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 90
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v38

    .line 92
    .local v38, "prufbyte":B
    :goto_2
    aget-byte v3, v8, v23

    move/from16 v0, v38

    if-eq v0, v3, :cond_9

    aget-byte v3, v29, v23

    if-eqz v3, :cond_f

    .line 94
    :cond_9
    aget-byte v3, v39, v23

    const/4 v4, 0x2

    if-ne v3, v4, :cond_a

    move/from16 v43, v38

    .line 95
    :cond_a
    aget-byte v3, v39, v23

    if-eqz v3, :cond_b

    aget-byte v3, v39, v23

    const/4 v4, 0x2

    if-ne v3, v4, :cond_c

    :cond_b
    aput-byte v38, v12, v23

    .line 96
    :cond_c
    aget-byte v3, v39, v23

    const/4 v4, 0x3

    if-ne v3, v4, :cond_d

    move/from16 v16, v23

    .line 97
    :cond_d
    add-int/lit8 v23, v23, 0x1

    .line 98
    array-length v3, v8

    move/from16 v0, v23

    if-ne v0, v3, :cond_1f

    .line 100
    const/16 v3, 0x3e8

    move/from16 v0, v16

    if-ge v0, v3, :cond_e

    aput-byte v43, v12, v16

    .line 101
    :cond_e
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 102
    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 103
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 105
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Check License Key Fixed!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 106
    const/16 v37, 0x1

    .line 116
    .end local v16    # "c":I
    .end local v23    # "i":I
    .end local v38    # "prufbyte":B
    :cond_f
    const/4 v3, 0x0

    aget-byte v3, v9, v3

    move/from16 v0, v17

    if-ne v0, v3, :cond_14

    .line 118
    const/4 v3, 0x0

    aget-byte v3, v40, v3

    if-nez v3, :cond_10

    const/4 v3, 0x0

    aput-byte v17, v13, v3

    .line 119
    :cond_10
    const/16 v23, 0x1

    .line 120
    .restart local v23    # "i":I
    add-int/lit8 v3, v18, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 121
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v38

    .line 123
    .restart local v38    # "prufbyte":B
    :goto_3
    aget-byte v3, v9, v23

    move/from16 v0, v38

    if-eq v0, v3, :cond_11

    aget-byte v3, v30, v23

    const/4 v4, 0x1

    if-ne v3, v4, :cond_13

    .line 125
    :cond_11
    aget-byte v3, v40, v23

    if-nez v3, :cond_12

    aput-byte v38, v13, v23

    .line 126
    :cond_12
    add-int/lit8 v23, v23, 0x1

    .line 128
    array-length v3, v9

    move/from16 v0, v23

    if-ne v0, v3, :cond_20

    .line 130
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 131
    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 132
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 134
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "License Key Fixed2!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 135
    const/16 v34, 0x1

    .line 143
    :cond_13
    add-int/lit8 v3, v18, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 145
    .end local v23    # "i":I
    .end local v38    # "prufbyte":B
    :cond_14
    const/4 v3, 0x0

    aget-byte v3, v10, v3

    move/from16 v0, v17

    if-ne v0, v3, :cond_19

    .line 147
    const/4 v3, 0x0

    aget-byte v3, v41, v3

    if-nez v3, :cond_15

    const/4 v3, 0x0

    aput-byte v17, v14, v3

    .line 148
    :cond_15
    const/16 v23, 0x1

    .line 149
    .restart local v23    # "i":I
    add-int/lit8 v3, v18, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 150
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v38

    .line 152
    .restart local v38    # "prufbyte":B
    :goto_4
    aget-byte v3, v10, v23

    move/from16 v0, v38

    if-eq v0, v3, :cond_16

    aget-byte v3, v31, v23

    const/4 v4, 0x1

    if-ne v3, v4, :cond_18

    .line 154
    :cond_16
    aget-byte v3, v41, v23

    if-nez v3, :cond_17

    aput-byte v38, v14, v23

    .line 155
    :cond_17
    add-int/lit8 v23, v23, 0x1

    .line 157
    array-length v3, v10

    move/from16 v0, v23

    if-ne v0, v3, :cond_21

    .line 159
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 160
    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 161
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 163
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Internet Connection Fixed!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 164
    const/16 v35, 0x1

    .line 169
    :cond_18
    add-int/lit8 v3, v18, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 171
    .end local v23    # "i":I
    .end local v38    # "prufbyte":B
    :cond_19
    const/4 v3, 0x0

    aget-byte v3, v11, v3

    move/from16 v0, v17

    if-ne v0, v3, :cond_1e

    .line 173
    const/4 v3, 0x0

    aget-byte v3, v42, v3

    if-nez v3, :cond_1a

    const/4 v3, 0x0

    aput-byte v17, v15, v3

    .line 174
    :cond_1a
    const/16 v23, 0x1

    .line 175
    .restart local v23    # "i":I
    add-int/lit8 v3, v18, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 176
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v38

    .line 178
    .restart local v38    # "prufbyte":B
    :goto_5
    aget-byte v3, v11, v23

    move/from16 v0, v38

    if-eq v0, v3, :cond_1b

    aget-byte v3, v32, v23

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1d

    .line 180
    :cond_1b
    aget-byte v3, v42, v23

    if-nez v3, :cond_1c

    aput-byte v38, v15, v23

    .line 181
    :cond_1c
    add-int/lit8 v23, v23, 0x1

    .line 182
    array-length v3, v11

    move/from16 v0, v23

    if-ne v0, v3, :cond_22

    .line 184
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 185
    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 186
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 188
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Cached License Key Fixed!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 190
    const/16 v33, 0x1

    .line 195
    :cond_1d
    add-int/lit8 v3, v18, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 223
    .end local v23    # "i":I
    .end local v38    # "prufbyte":B
    :cond_1e
    add-int/lit8 v3, v18, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 77
    const-wide/16 v3, 0x1

    add-long v24, v24, v3

    goto/16 :goto_1

    .line 110
    .restart local v16    # "c":I
    .restart local v23    # "i":I
    .restart local v38    # "prufbyte":B
    :cond_1f
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v38

    goto/16 :goto_2

    .line 140
    .end local v16    # "c":I
    :cond_20
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v38

    goto/16 :goto_3

    .line 167
    :cond_21
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v38

    goto/16 :goto_4

    .line 193
    :cond_22
    invoke-virtual/range {v22 .. v22}, Ljava/nio/MappedByteBuffer;->get()B
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v38

    goto :goto_5

    .line 225
    .end local v17    # "curentByte":B
    .end local v18    # "curentPos":I
    .end local v23    # "i":I
    .end local v38    # "prufbyte":B
    :catch_1
    move-exception v21

    .line 226
    .local v21, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 228
    .end local v21    # "e":Ljava/lang/Exception;
    :cond_23
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 230
    if-nez v34, :cond_24

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: License Key2 patch Failed!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 231
    :cond_24
    if-nez v35, :cond_25

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Internet Connection patch Failed!\nor patch is already applied?!\n\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 232
    :cond_25
    if-nez v37, :cond_26

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Check License Key patch Failed!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 233
    :cond_26
    if-nez v36, :cond_27

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Market Free patch Failed!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 234
    :cond_27
    if-nez v33, :cond_5

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Cached License Key patch Failed!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 239
    .end local v2    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v20    # "dalvikDexTemp":Ljava/lang/String;
    .end local v22    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .end local v24    # "j":J
    .end local v26    # "localFile1":Ljava/io/File;
    .end local v27    # "localFile2":Ljava/io/File;
    .end local v33    # "patch":Z
    .end local v34    # "patch1":Z
    .end local v35    # "patch2":Z
    .end local v36    # "patch3":Z
    .end local v37    # "patch4":Z
    .end local v43    # "repbyte":B
    :catch_2
    move-exception v21

    .line 240
    .restart local v21    # "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception e"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 23
    nop

    :array_0
    .array-data 1
        0xat
        0x1t
        0x38t
        0x1t
        0xet
        0x0t
        0x1at
        0x0t
        0xct
        0x2t
        0x1at
        0x1t
    .end array-data

    .line 24
    :array_1
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 25
    :array_2
    .array-data 1
        0x12t
        0x4t
        0x29t
        0x0t
        0x0t
        0x0t
        0x71t
        0x40t
        0x55t
        0x2dt
        0xet
        0x33t
    .end array-data

    .line 26
    :array_3
    .array-data 1
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 28
    :array_4
    .array-data 1
        0x38t
        0x1t
        0x2bt
        0x0t
        0x6et
        0x10t
        0x13t
        0x7t
        0x7t
        0x0t
        0xct
        0x1t
    .end array-data

    .line 29
    :array_5
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 30
    :array_6
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x6et
        0x10t
        0x13t
        0x7t
        0x7t
        0x0t
        0xct
        0x1t
    .end array-data

    .line 31
    :array_7
    .array-data 1
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 33
    :array_8
    .array-data 1
        0x38t
        0x0t
        0x6t
        0x0t
        0x70t
        0x10t
        0x75t
        0x7t
        0x4t
        0x0t
        0xet
        0x0t
        0x54t
        0x40t
    .end array-data

    .line 34
    nop

    :array_9
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
    .end array-data

    .line 35
    nop

    :array_a
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x70t
        0x10t
        0x75t
        0x7t
        0x4t
        0x0t
        0xet
        0x0t
        0x54t
        0x40t
    .end array-data

    .line 36
    nop

    :array_b
    .array-data 1
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 38
    nop

    :array_c
    .array-data 1
        0x38t
        0x0t
        0x57t
        0x0t
        0x54t
        0x60t
        0x2t
        0x3t
        0x54t
        0x0t
        0x1t
        0x2t
        0x54t
        0x61t
    .end array-data

    .line 39
    nop

    :array_d
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 40
    nop

    :array_e
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x70t
        0x10t
        0x75t
        0x7t
        0x4t
        0x0t
        0xet
        0x0t
        0x54t
        0x40t
    .end array-data

    .line 41
    nop

    :array_f
    .array-data 1
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method
