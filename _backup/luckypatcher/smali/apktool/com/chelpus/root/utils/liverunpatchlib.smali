.class public Lcom/chelpus/root/utils/liverunpatchlib;
.super Ljava/lang/Object;
.source "liverunpatchlib.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 28
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 15
    new-instance v2, Lcom/chelpus/root/utils/liverunpatchlib$1;

    invoke-direct {v2}, Lcom/chelpus/root/utils/liverunpatchlib$1;-><init>()V

    invoke-static {v2}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 18
    const/4 v2, 0x1

    aget-object v2, p0, v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v22

    .line 19
    .local v22, "orhex":[Ljava/lang/String;
    const/4 v2, 0x2

    aget-object v2, p0, v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v26

    .line 20
    .local v26, "rephex":[Ljava/lang/String;
    move-object/from16 v0, v22

    array-length v2, v0

    new-array v7, v2, [B

    .line 21
    .local v7, "byteOrig":[B
    move-object/from16 v0, v26

    array-length v2, v0

    new-array v8, v2, [B

    .line 22
    .local v8, "byteReplace":[B
    move-object/from16 v0, v22

    array-length v2, v0

    new-array v0, v2, [B

    move-object/from16 v21, v0

    .line 23
    .local v21, "mask":[B
    move-object/from16 v0, v26

    array-length v2, v0

    new-array v0, v2, [B

    move-object/from16 v25, v0

    .line 25
    .local v25, "rep_mask":[B
    const/4 v13, 0x0

    .line 27
    .local v13, "error":Z
    move-object/from16 v0, v25

    array-length v2, v0

    move-object/from16 v0, v21

    array-length v3, v0

    if-ne v2, v3, :cond_11

    array-length v2, v7

    array-length v3, v8

    if-ne v2, v3, :cond_11

    array-length v2, v8

    const/4 v3, 0x3

    if-le v2, v3, :cond_11

    array-length v2, v7

    const/4 v3, 0x3

    if-le v2, v3, :cond_11

    .line 30
    const/16 v27, 0x0

    .local v27, "t":I
    :goto_0
    move-object/from16 v0, v22

    array-length v2, v0

    move/from16 v0, v27

    if-ge v0, v2, :cond_4

    .line 33
    aget-object v2, v22, v27

    const-string v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    aget-object v2, v22, v27

    const-string v3, "**"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v13, 0x1

    const-string v2, "00"

    aput-object v2, v22, v27

    .line 34
    :cond_0
    aget-object v2, v26, v27

    const-string v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    aget-object v2, v26, v27

    const-string v3, "**"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v13, 0x1

    const-string v2, "00"

    aput-object v2, v26, v27

    .line 35
    :cond_1
    aget-object v2, v22, v27

    const-string v3, "**"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "00"

    aput-object v2, v22, v27

    const/4 v2, 0x1

    aput-byte v2, v21, v27

    .line 36
    :goto_1
    aget-object v2, v22, v27

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v7, v27

    .line 38
    aget-object v2, v26, v27

    const-string v3, "**"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "00"

    aput-object v2, v26, v27

    const/4 v2, 0x0

    aput-byte v2, v25, v27

    .line 39
    :goto_2
    aget-object v2, v26, v27

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v8, v27

    .line 30
    add-int/lit8 v27, v27, 0x1

    goto :goto_0

    .line 35
    :cond_2
    const/4 v2, 0x0

    aput-byte v2, v21, v27

    goto :goto_1

    .line 38
    :cond_3
    const/4 v2, 0x1

    aput-byte v2, v25, v27

    goto :goto_2

    .line 47
    :cond_4
    if-nez v13, :cond_10

    .line 50
    const/4 v2, 0x0

    aget-object v11, p0, v2

    .line 54
    .local v11, "dalvikDex":Ljava/lang/String;
    :try_start_0
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    invoke-direct {v0, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 57
    .local v18, "localFile1":Ljava/io/File;
    new-instance v19, Ljava/io/File;

    const-string v2, "/data/data/"

    const-string v3, "/mnt/asec/"

    invoke-virtual {v11, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 58
    .local v19, "localFile2":Ljava/io/File;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v19, v18

    .line 59
    :cond_5
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_6

    new-instance v19, Ljava/io/File;

    .end local v19    # "localFile2":Ljava/io/File;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 60
    .restart local v19    # "localFile2":Ljava/io/File;
    :cond_6
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_7

    new-instance v19, Ljava/io/File;

    .end local v19    # "localFile2":Ljava/io/File;
    const-string v2, "-1"

    const-string v3, "-2"

    invoke-virtual {v11, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 61
    .restart local v19    # "localFile2":Ljava/io/File;
    :cond_7
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_9

    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-direct {v2}, Ljava/io/FileNotFoundException;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 117
    .end local v18    # "localFile1":Ljava/io/File;
    .end local v19    # "localFile2":Ljava/io/File;
    :catch_0
    move-exception v20

    .line 118
    .local v20, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Error: Program files are not found!\n\nMove Program to internal storage."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 130
    .end local v11    # "dalvikDex":Ljava/lang/String;
    .end local v20    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    .end local v27    # "t":I
    :cond_8
    :goto_3
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 131
    return-void

    .line 64
    .restart local v11    # "dalvikDex":Ljava/lang/String;
    .restart local v18    # "localFile1":Ljava/io/File;
    .restart local v19    # "localFile2":Ljava/io/File;
    .restart local v27    # "t":I
    :cond_9
    :try_start_1
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v3, "rw"

    move-object/from16 v0, v19

    invoke-direct {v2, v0, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 65
    .local v1, "ChannelDex":Ljava/nio/channels/FileChannel;
    sget-object v2, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v3, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v5

    long-to-int v5, v5

    int-to-long v5, v5

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v14

    .line 66
    .local v14, "fileBytes":Ljava/nio/MappedByteBuffer;
    const/16 v23, 0x0

    .line 71
    .local v23, "patch":Z
    const-wide/16 v16, 0x0

    .local v16, "j":J
    :goto_4
    :try_start_2
    invoke-virtual {v14}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 74
    invoke-virtual {v14}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v10

    .line 75
    .local v10, "curentPos":I
    invoke-virtual {v14}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v9

    .line 79
    .local v9, "curentByte":B
    const/4 v2, 0x0

    aget-byte v2, v7, v2

    if-ne v9, v2, :cond_d

    .line 81
    const/4 v2, 0x0

    aget-byte v2, v25, v2

    if-nez v2, :cond_a

    const/4 v2, 0x0

    aput-byte v9, v8, v2

    .line 82
    :cond_a
    const/4 v15, 0x1

    .line 83
    .local v15, "i":I
    add-int/lit8 v2, v10, 0x1

    invoke-virtual {v14, v2}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 84
    invoke-virtual {v14}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v24

    .line 86
    .local v24, "prufbyte":B
    :goto_5
    aget-byte v2, v7, v15

    move/from16 v0, v24

    if-eq v0, v2, :cond_b

    aget-byte v2, v21, v15

    const/4 v3, 0x1

    if-ne v2, v3, :cond_d

    .line 88
    :cond_b
    aget-byte v2, v25, v15

    if-nez v2, :cond_c

    aput-byte v24, v8, v15

    .line 89
    :cond_c
    add-int/lit8 v15, v15, 0x1

    .line 91
    array-length v2, v7

    if-ne v15, v2, :cond_e

    .line 93
    invoke-virtual {v14, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 94
    invoke-virtual {v14, v8}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 95
    invoke-virtual {v14}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 97
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Offset in file:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v10}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - Patch done!\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 98
    const/16 v23, 0x1

    .line 108
    .end local v15    # "i":I
    .end local v24    # "prufbyte":B
    :cond_d
    add-int/lit8 v2, v10, 0x1

    invoke-virtual {v14, v2}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 71
    const-wide/16 v2, 0x1

    add-long v16, v16, v2

    goto :goto_4

    .line 103
    .restart local v15    # "i":I
    .restart local v24    # "prufbyte":B
    :cond_e
    invoke-virtual {v14}, Ljava/nio/MappedByteBuffer;->get()B
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v24

    goto :goto_5

    .line 110
    .end local v9    # "curentByte":B
    .end local v10    # "curentPos":I
    .end local v15    # "i":I
    .end local v24    # "prufbyte":B
    :catch_1
    move-exception v12

    .line 111
    .local v12, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 113
    .end local v12    # "e":Ljava/lang/Exception;
    :cond_f
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 115
    if-nez v23, :cond_8

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Error: Pattern not found!\nor patch is already applied?!\n"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_3

    .line 120
    .end local v1    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v14    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .end local v16    # "j":J
    .end local v18    # "localFile1":Ljava/io/File;
    .end local v19    # "localFile2":Ljava/io/File;
    .end local v23    # "patch":Z
    :catch_2
    move-exception v2

    goto/16 :goto_3

    .line 124
    .end local v11    # "dalvikDex":Ljava/lang/String;
    :cond_10
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Error: Pattern not valid!\n\nPattern can not be \"*4\" or \"A*\", etc.\n\n It can only be ** "

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 128
    .end local v27    # "t":I
    :cond_11
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Error: Original & Replace hex-string not valid!\n\nOriginal.hex.length != Replacmant.hex.length.\nOR\nLength of hex-string < 4"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_3
.end method
