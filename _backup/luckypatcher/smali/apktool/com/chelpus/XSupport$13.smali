.class Lcom/chelpus/XSupport$13;
.super Lde/robv/android/xposed/XC_MethodHook;
.source "XSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/chelpus/XSupport;->handleLoadPackage(Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/chelpus/XSupport;


# direct methods
.method constructor <init>(Lcom/chelpus/XSupport;)V
    .locals 0
    .param p1, "this$0"    # Lcom/chelpus/XSupport;

    .prologue
    .line 396
    iput-object p1, p0, Lcom/chelpus/XSupport$13;->this$0:Lcom/chelpus/XSupport;

    invoke-direct {p0}, Lde/robv/android/xposed/XC_MethodHook;-><init>()V

    return-void
.end method


# virtual methods
.method protected beforeHookedMethod(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)V
    .locals 5
    .param p1, "param"    # Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 400
    iget-object v3, p0, Lcom/chelpus/XSupport$13;->this$0:Lcom/chelpus/XSupport;

    invoke-virtual {v3}, Lcom/chelpus/XSupport;->loadPrefs()V

    .line 401
    sget-boolean v3, Lcom/chelpus/XSupport;->enable:Z

    if-eqz v3, :cond_1

    sget-boolean v3, Lcom/chelpus/XSupport;->patch3:Z

    if-eqz v3, :cond_1

    .line 402
    const/4 v2, 0x1

    .line 403
    .local v2, "id":I
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_0

    const/4 v2, 0x2

    .line 406
    :cond_0
    :try_start_0
    iget-object v3, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->args:[Ljava/lang/Object;

    aget-object v3, v3, v2

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 411
    .local v1, "flags":I
    :goto_0
    and-int/lit16 v3, v1, 0x80

    if-nez v3, :cond_1

    .line 412
    or-int/lit16 v1, v1, 0x80

    .line 413
    iget-object v3, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->args:[Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    .line 417
    .end local v1    # "flags":I
    .end local v2    # "id":I
    :cond_1
    return-void

    .line 407
    .restart local v2    # "id":I
    :catch_0
    move-exception v0

    .line 408
    .local v0, "e":Ljava/lang/ClassCastException;
    iget-object v3, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->args:[Ljava/lang/Object;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 409
    .restart local v1    # "flags":I
    const/4 v2, 0x1

    goto :goto_0
.end method
