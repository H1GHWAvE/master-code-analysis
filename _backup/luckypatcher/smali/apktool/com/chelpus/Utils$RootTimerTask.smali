.class Lcom/chelpus/Utils$RootTimerTask;
.super Ljava/util/TimerTask;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chelpus/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RootTimerTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/chelpus/Utils;

.field work:Lcom/chelpus/Utils$Worker;


# direct methods
.method public constructor <init>(Lcom/chelpus/Utils;Lcom/chelpus/Utils$Worker;)V
    .locals 1
    .param p1, "this$0"    # Lcom/chelpus/Utils;
    .param p2, "worker"    # Lcom/chelpus/Utils$Worker;

    .prologue
    .line 828
    iput-object p1, p0, Lcom/chelpus/Utils$RootTimerTask;->this$0:Lcom/chelpus/Utils;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 827
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chelpus/Utils$RootTimerTask;->work:Lcom/chelpus/Utils$Worker;

    .line 829
    iput-object p2, p0, Lcom/chelpus/Utils$RootTimerTask;->work:Lcom/chelpus/Utils$Worker;

    .line 830
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 834
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->result:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 835
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "timeout for wait root. exit..."

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 837
    :try_start_0
    iget-object v4, p0, Lcom/chelpus/Utils$RootTimerTask;->work:Lcom/chelpus/Utils$Worker;

    invoke-virtual {v4}, Lcom/chelpus/Utils$Worker;->interrupt()V

    .line 838
    iget-object v4, p0, Lcom/chelpus/Utils$RootTimerTask;->work:Lcom/chelpus/Utils$Worker;

    iget-object v4, v4, Lcom/chelpus/Utils$Worker;->input:Ljava/io/DataInputStream;

    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 840
    :try_start_1
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v4, :cond_0

    .line 841
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/16 v4, 0x17

    invoke-static {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 842
    :cond_0
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/4 v4, 0x1

    invoke-static {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 843
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/16 v4, 0xb

    invoke-static {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 844
    const-string v1, ""
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 846
    .local v1, "commands":Ljava/lang/String;
    :try_start_2
    iget-object v4, p0, Lcom/chelpus/Utils$RootTimerTask;->work:Lcom/chelpus/Utils$Worker;

    # getter for: Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;
    invoke-static {v4}, Lcom/chelpus/Utils$Worker;->access$100(Lcom/chelpus/Utils$Worker;)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_1

    aget-object v0, v5, v4

    .line 847
    .local v0, "com":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    .line 846
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 849
    .end local v0    # "com":Ljava/lang/String;
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 850
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v5, "Root error"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Your root hung at commands:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\nTry restart operation or updating your superSu and binary su."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMessage(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 853
    .end local v1    # "commands":Ljava/lang/String;
    :goto_1
    :try_start_4
    iget-object v4, p0, Lcom/chelpus/Utils$RootTimerTask;->work:Lcom/chelpus/Utils$Worker;

    # getter for: Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;
    invoke-static {v4}, Lcom/chelpus/Utils$Worker;->access$100(Lcom/chelpus/Utils$Worker;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    :goto_2
    if-ge v3, v5, :cond_3

    aget-object v0, v4, v3

    .line 854
    .restart local v0    # "com":Ljava/lang/String;
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Lucky Patcher: freezes root commands:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 853
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 851
    .end local v0    # "com":Ljava/lang/String;
    :catch_1
    move-exception v2

    .restart local v2    # "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 859
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v2

    .line 860
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 863
    .end local v2    # "e":Ljava/io/IOException;
    :cond_2
    :goto_3
    return-void

    .line 856
    :catch_3
    move-exception v2

    .local v2, "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 857
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    invoke-static {}, Lcom/chelpus/Utils;->exitRoot()V

    .line 858
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->semaphoreRoot:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_3
.end method
