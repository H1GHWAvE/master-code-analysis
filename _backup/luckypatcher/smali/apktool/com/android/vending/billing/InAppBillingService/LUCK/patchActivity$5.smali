.class Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$5;
.super Ljava/lang/Object;
.source "patchActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->onWindowFocusChanged(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    .prologue
    .line 367
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$5;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/16 v4, 0x13

    const/4 v3, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 370
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "Install_location"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 371
    .local v0, "location":I
    if-eq v0, v3, :cond_0

    .line 372
    sget-boolean v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v1, :cond_1

    .line 373
    new-instance v1, Lcom/chelpus/Utils;

    const-string v2, ""

    invoke-direct {v1, v2}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    new-array v2, v7, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pm setInstallLocation "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, "skipOut"

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    .line 374
    new-instance v1, Lcom/chelpus/Utils;

    const-string v2, ""

    invoke-direct {v1, v2}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    new-array v2, v7, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pm set-install-location "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, "skipOut"

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    .line 382
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v4, :cond_2

    .line 377
    new-array v1, v7, [Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pm setInstallLocation "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    const-string v2, "skipOut"

    aput-object v2, v1, v6

    invoke-static {v1}, Lcom/chelpus/Utils;->cmd([Ljava/lang/String;)Ljava/lang/String;

    .line 378
    :cond_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v4, :cond_0

    .line 379
    new-array v1, v7, [Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pm set-install-location "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    const-string v2, "skipOut"

    aput-object v2, v1, v6

    invoke-static {v1}, Lcom/chelpus/Utils;->cmd([Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method
