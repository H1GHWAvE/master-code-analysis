.class Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25;
.super Ljava/lang/Object;
.source "patchActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->toolbar_backups_click(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    .prologue
    .line 1139
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 1144
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1145
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;>;"
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Backup"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 1146
    .local v1, "files":[Ljava/lang/String;
    if-eqz v1, :cond_2

    array-length v3, v1

    if-eqz v3, :cond_2

    .line 1147
    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v0, v1, v3

    .line 1151
    .local v0, "file":Ljava/lang/String;
    :try_start_0
    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v6

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/Backup/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x1

    invoke-direct {v5, v6, v7, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;-><init>(Landroid/content/Context;Ljava/io/File;Z)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1147
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1155
    .end local v0    # "file":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_1

    .line 1156
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$1;

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const v5, 0x7f040009

    invoke-direct {v3, p0, v4, v5, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25;Landroid/content/Context;ILjava/util/List;)V

    sput-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    .line 1243
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    new-instance v4, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$2;

    invoke-direct {v4, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25;)V

    invoke-virtual {v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1272
    :goto_2
    return-void

    .line 1254
    :cond_1
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    new-instance v4, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$3;

    invoke-direct {v4, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25;)V

    invoke-virtual {v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_2

    .line 1264
    :cond_2
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    new-instance v4, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$4;

    invoke-direct {v4, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25;)V

    invoke-virtual {v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_2

    .line 1152
    .restart local v0    # "file":Ljava/lang/String;
    :catch_0
    move-exception v5

    goto :goto_1
.end method
