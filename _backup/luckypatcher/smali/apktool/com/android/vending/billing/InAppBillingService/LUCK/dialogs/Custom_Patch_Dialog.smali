.class public Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog;
.super Ljava/lang/Object;
.source "Custom_Patch_Dialog.java"


# instance fields
.field dialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog;->dialog:Landroid/app/Dialog;

    .line 27
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog;->dialog:Landroid/app/Dialog;

    .line 114
    :cond_0
    return-void
.end method

.method public onCreateDialog()Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 40
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "Custom Dialog create."

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 41
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v5, :cond_0

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog;->dismiss()V

    .line 42
    :cond_1
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const v6, 0x7f040024

    invoke-static {v5, v6, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 45
    .local v2, "d":Landroid/widget/LinearLayout;
    const v5, 0x7f0d008b

    :try_start_0
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0d008c

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 46
    .local v0, "body":Landroid/widget/LinearLayout;
    const/4 v4, 0x0

    .line 48
    .local v4, "tv":Landroid/widget/TextView;
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    if-nez v5, :cond_2

    const-string v5, " "

    sput-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 49
    :cond_2
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v6, "SU Java-Code Running!"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 50
    const-string v5, "Root not stable. Try again or update your root."

    sput-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 53
    :cond_3
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v6, "Error LP:"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v6, "Object not found!"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 54
    const v5, 0x7f0d008d

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .end local v4    # "tv":Landroid/widget/TextView;
    check-cast v4, Landroid/widget/TextView;

    .line 55
    .restart local v4    # "tv":Landroid/widget/TextView;
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v6, "#ff00ff73"

    const-string v7, "bold"

    invoke-static {v5, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 58
    :cond_4
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v6, "Error LP:"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v6, "Object not found!"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 59
    :cond_5
    const v5, 0x7f0d008d

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .end local v4    # "tv":Landroid/widget/TextView;
    check-cast v4, Landroid/widget/TextView;

    .line 60
    .restart local v4    # "tv":Landroid/widget/TextView;
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v6, "#ffff0055"

    const-string v7, "bold"

    invoke-static {v5, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    .end local v0    # "body":Landroid/widget/LinearLayout;
    .end local v4    # "tv":Landroid/widget/TextView;
    :cond_6
    :goto_0
    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 66
    .local v1, "builder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    const v5, 0x7f02002c

    invoke-virtual {v1, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setIcon(I)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 67
    const v5, 0x7f070002

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 68
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setCancelable(Z)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 69
    const v5, 0x104000a

    .line 70
    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    .line 69
    invoke-virtual {v1, v5, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 71
    sget v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->func:I

    if-eqz v5, :cond_7

    .line 73
    const v5, 0x7f070146

    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog$1;

    invoke-direct {v6, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog;)V

    invoke-virtual {v1, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 105
    :cond_7
    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setView(Landroid/view/View;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 107
    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v5

    return-object v5

    .line 63
    .end local v1    # "builder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :catch_0
    move-exception v3

    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog;->dismiss()V

    goto :goto_0
.end method

.method public showDialog()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog;->dialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog;->onCreateDialog()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog;->dialog:Landroid/app/Dialog;

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 34
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom_Patch_Dialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 36
    :cond_1
    return-void
.end method
