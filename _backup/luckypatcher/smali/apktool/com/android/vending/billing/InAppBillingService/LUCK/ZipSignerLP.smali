.class public Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;
.super Ljava/lang/Object;
.source "ZipSignerLP.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable;
    }
.end annotation


# static fields
.field private static final CERT_RSA_NAME:Ljava/lang/String; = "META-INF/CERT.RSA"

.field private static final CERT_SF_NAME:Ljava/lang/String; = "META-INF/CERT.SF"

.field public static final KEY_NONE:Ljava/lang/String; = "none"

.field public static final KEY_TESTKEY:Ljava/lang/String; = "testkey"

.field public static final MODE_AUTO:Ljava/lang/String; = "auto"

.field public static final MODE_AUTO_NONE:Ljava/lang/String; = "auto-none"

.field public static final MODE_AUTO_TESTKEY:Ljava/lang/String; = "auto-testkey"

.field public static final SUPPORTED_KEY_MODES:[Ljava/lang/String;

.field static log:Lkellinwood/logging/LoggerInterface;

.field private static stripPattern:Ljava/util/regex/Pattern;


# instance fields
.field autoKeyDetect:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field autoKeyObservable:Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable;

.field private canceled:Z

.field keySet:Lkellinwood/security/zipsigner/KeySet;

.field keymode:Ljava/lang/String;

.field loadedKeys:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/security/zipsigner/KeySet;",
            ">;"
        }
    .end annotation
.end field

.field private progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 108
    const/4 v0, 0x0

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->log:Lkellinwood/logging/LoggerInterface;

    .line 114
    const-string v0, "^META-INF/(.*)[.](SF|RSA|DSA)$"

    .line 115
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->stripPattern:Ljava/util/regex/Pattern;

    .line 132
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "auto-testkey"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "auto"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "auto-none"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "media"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "platform"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "shared"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "testkey"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "none"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->SUPPORTED_KEY_MODES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z

    .line 106
    new-instance v0, Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-direct {v0}, Lkellinwood/security/zipsigner/ProgressHelper;-><init>()V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    .line 117
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->loadedKeys:Ljava/util/Map;

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    .line 135
    const-string v0, "testkey"

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keymode:Ljava/lang/String;

    .line 137
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->autoKeyDetect:Ljava/util/Map;

    .line 139
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable;

    invoke-direct {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable;-><init>()V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->autoKeyObservable:Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable;

    .line 143
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->autoKeyDetect:Ljava/util/Map;

    const-string v1, "aa9852bc5a53272ac8031d49b65e4b0e"

    const-string v2, "media"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->autoKeyDetect:Ljava/util/Map;

    const-string v1, "e60418c4b638f20d0721e115674ca11f"

    const-string v2, "platform"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->autoKeyDetect:Ljava/util/Map;

    const-string v1, "3e24e49741b60c215c010dc6048fca7d"

    const-string v2, "shared"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->autoKeyDetect:Ljava/util/Map;

    const-string v1, "dab2cead827ef5313f28e22b6fa8479f"

    const-string v2, "testkey"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    return-void
.end method

.method private addDigestsToManifest(Ljava/util/Map;Ljava/util/ArrayList;)Ljava/util/jar/Manifest;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/zipio/ZioEntry;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;",
            ">;)",
            "Ljava/util/jar/Manifest;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 393
    .local p1, "entries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .local p2, "modified_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    const/4 v12, 0x0

    .line 394
    .local v12, "input":Ljava/util/jar/Manifest;
    const-string v19, "META-INF/MANIFEST.MF"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lkellinwood/zipio/ZioEntry;

    .line 395
    .local v14, "manifestEntry":Lkellinwood/zipio/ZioEntry;
    if-eqz v14, :cond_0

    .line 396
    new-instance v12, Ljava/util/jar/Manifest;

    .end local v12    # "input":Ljava/util/jar/Manifest;
    invoke-direct {v12}, Ljava/util/jar/Manifest;-><init>()V

    .line 397
    .restart local v12    # "input":Ljava/util/jar/Manifest;
    invoke-virtual {v14}, Lkellinwood/zipio/ZioEntry;->getInputStream()Ljava/io/InputStream;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/jar/Manifest;->read(Ljava/io/InputStream;)V

    .line 399
    :cond_0
    new-instance v18, Ljava/util/jar/Manifest;

    invoke-direct/range {v18 .. v18}, Ljava/util/jar/Manifest;-><init>()V

    .line 400
    .local v18, "output":Ljava/util/jar/Manifest;
    invoke-virtual/range {v18 .. v18}, Ljava/util/jar/Manifest;->getMainAttributes()Ljava/util/jar/Attributes;

    move-result-object v13

    .line 401
    .local v13, "main":Ljava/util/jar/Attributes;
    if-eqz v12, :cond_4

    .line 402
    invoke-virtual {v12}, Ljava/util/jar/Manifest;->getMainAttributes()Ljava/util/jar/Attributes;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/util/jar/Attributes;->putAll(Ljava/util/Map;)V

    .line 409
    :goto_0
    const-string v19, "SHA1"

    invoke-static/range {v19 .. v19}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v15

    .line 410
    .local v15, "md":Ljava/security/MessageDigest;
    const/16 v19, 0x200

    move/from16 v0, v19

    new-array v3, v0, [B

    .line 417
    .local v3, "buffer":[B
    new-instance v4, Ljava/util/TreeMap;

    invoke-direct {v4}, Ljava/util/TreeMap;-><init>()V

    .line 418
    .local v4, "byName":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/util/TreeMap;->putAll(Ljava/util/Map;)V

    .line 420
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v7

    .line 421
    .local v7, "debug":Z
    if-eqz v7, :cond_1

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v19

    const-string v20, "Manifest entries:"

    invoke-interface/range {v19 .. v20}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 422
    :cond_1
    invoke-virtual {v4}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_2
    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_3

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lkellinwood/zipio/ZioEntry;

    .line 423
    .local v9, "entry":Lkellinwood/zipio/ZioEntry;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z

    move/from16 v20, v0

    if-eqz v20, :cond_5

    .line 458
    .end local v9    # "entry":Lkellinwood/zipio/ZioEntry;
    :cond_3
    return-object v18

    .line 404
    .end local v3    # "buffer":[B
    .end local v4    # "byName":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .end local v7    # "debug":Z
    .end local v15    # "md":Ljava/security/MessageDigest;
    :cond_4
    const-string v19, "Manifest-Version"

    const-string v20, "1.0"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v13, v0, v1}, Ljava/util/jar/Attributes;->putValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 405
    const-string v19, "Created-By"

    const-string v20, "1.0 (Android SignApk)"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v13, v0, v1}, Ljava/util/jar/Attributes;->putValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 424
    .restart local v3    # "buffer":[B
    .restart local v4    # "byName":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .restart local v7    # "debug":Z
    .restart local v9    # "entry":Lkellinwood/zipio/ZioEntry;
    .restart local v15    # "md":Ljava/security/MessageDigest;
    :cond_5
    invoke-virtual {v9}, Lkellinwood/zipio/ZioEntry;->getName()Ljava/lang/String;

    move-result-object v16

    .line 425
    .local v16, "name":Ljava/lang/String;
    if-eqz v7, :cond_6

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 426
    :cond_6
    invoke-virtual {v9}, Lkellinwood/zipio/ZioEntry;->isDirectory()Z

    move-result v20

    if-nez v20, :cond_2

    const-string v20, "META-INF/MANIFEST.MF"

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_2

    const-string v20, "META-INF/CERT.SF"

    .line 427
    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_2

    const-string v20, "META-INF/CERT.RSA"

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_2

    sget-object v20, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->stripPattern:Ljava/util/regex/Pattern;

    if-eqz v20, :cond_7

    sget-object v20, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->stripPattern:Ljava/util/regex/Pattern;

    .line 429
    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/util/regex/Matcher;->matches()Z

    move-result v20

    if-nez v20, :cond_2

    .line 431
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "Generating manifest"

    invoke-virtual/range {v20 .. v22}, Lkellinwood/security/zipsigner/ProgressHelper;->progress(ILjava/lang/String;)V

    .line 432
    invoke-virtual {v9}, Lkellinwood/zipio/ZioEntry;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 433
    .local v5, "data":Ljava/io/InputStream;
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_8
    :goto_2
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_9

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    .line 434
    .local v10, "file":Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;
    iget-object v0, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->fileName:Ljava/lang/String;

    move-object/from16 v21, v0

    iget-object v0, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->basePath:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, ""

    invoke-virtual/range {v21 .. v23}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 436
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    iget-object v0, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->fileName:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 437
    .end local v5    # "data":Ljava/io/InputStream;
    .local v6, "data":Ljava/io/InputStream;
    :try_start_1
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "LuckyPatcher (signer): Additional files to manifest added! "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v5, v6

    .line 440
    .end local v6    # "data":Ljava/io/InputStream;
    .restart local v5    # "data":Ljava/io/InputStream;
    goto :goto_2

    .line 438
    :catch_0
    move-exception v8

    .line 439
    .local v8, "e":Ljava/lang/Exception;
    :goto_3
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_2

    .line 443
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v10    # "file":Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;
    :cond_9
    :goto_4
    invoke-virtual {v5, v3}, Ljava/io/InputStream;->read([B)I

    move-result v17

    .local v17, "num":I
    if-lez v17, :cond_a

    .line 444
    const/16 v20, 0x0

    move/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v15, v3, v0, v1}, Ljava/security/MessageDigest;->update([BII)V

    goto :goto_4

    .line 446
    :cond_a
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 447
    const/4 v2, 0x0

    .line 448
    .local v2, "attr":Ljava/util/jar/Attributes;
    if-eqz v12, :cond_b

    .line 449
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/util/jar/Manifest;->getAttributes(Ljava/lang/String;)Ljava/util/jar/Attributes;

    move-result-object v11

    .line 450
    .local v11, "inAttr":Ljava/util/jar/Attributes;
    if-eqz v11, :cond_b

    new-instance v2, Ljava/util/jar/Attributes;

    .end local v2    # "attr":Ljava/util/jar/Attributes;
    invoke-direct {v2, v11}, Ljava/util/jar/Attributes;-><init>(Ljava/util/jar/Attributes;)V

    .line 452
    .end local v11    # "inAttr":Ljava/util/jar/Attributes;
    .restart local v2    # "attr":Ljava/util/jar/Attributes;
    :cond_b
    if-nez v2, :cond_c

    new-instance v2, Ljava/util/jar/Attributes;

    .end local v2    # "attr":Ljava/util/jar/Attributes;
    invoke-direct {v2}, Ljava/util/jar/Attributes;-><init>()V

    .line 453
    .restart local v2    # "attr":Ljava/util/jar/Attributes;
    :cond_c
    const-string v20, "SHA1-Digest"

    invoke-virtual {v15}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lkellinwood/security/zipsigner/Base64;->encode([B)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v2, v0, v1}, Ljava/util/jar/Attributes;->putValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 454
    invoke-virtual/range {v18 .. v18}, Ljava/util/jar/Manifest;->getEntries()Ljava/util/Map;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 438
    .end local v2    # "attr":Ljava/util/jar/Attributes;
    .end local v5    # "data":Ljava/io/InputStream;
    .end local v17    # "num":I
    .restart local v6    # "data":Ljava/io/InputStream;
    .restart local v10    # "file":Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;
    :catch_1
    move-exception v8

    move-object v5, v6

    .end local v6    # "data":Ljava/io/InputStream;
    .restart local v5    # "data":Ljava/io/InputStream;
    goto :goto_3
.end method

.method private copyFiles(Ljava/util/Map;Lkellinwood/zipio/ZipOutput;Ljava/util/ArrayList;)V
    .locals 19
    .param p2, "output"    # Lkellinwood/zipio/ZipOutput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/zipio/ZioEntry;",
            ">;",
            "Lkellinwood/zipio/ZipOutput;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 645
    .local p1, "input":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .local p3, "modified_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lkellinwood/zipio/ZioEntry;

    .line 646
    .local v9, "inEntry":Lkellinwood/zipio/ZioEntry;
    invoke-virtual {v9}, Lkellinwood/zipio/ZioEntry;->getName()Ljava/lang/String;

    move-result-object v11

    .line 647
    .local v11, "name":Ljava/lang/String;
    const/4 v10, 0x0

    .line 648
    .local v10, "mark":Z
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_1
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    .line 650
    .local v7, "file":Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;
    iget-object v0, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->fileName:Ljava/lang/String;

    move-object/from16 v16, v0

    iget-object v0, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->basePath:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, ""

    invoke-virtual/range {v16 .. v18}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 652
    :try_start_0
    new-instance v8, Ljava/io/File;

    iget-object v0, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->fileName:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 653
    .local v8, "fileF":Ljava/io/File;
    const/16 v16, 0x2000

    move/from16 v0, v16

    new-array v3, v0, [B

    .line 654
    .local v3, "buffer":[B
    new-instance v5, Ljava/io/FileInputStream;

    iget-object v0, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->fileName:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v5, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 656
    .local v5, "data":Ljava/io/FileInputStream;
    new-instance v2, Lkellinwood/zipio/ZioEntry;

    invoke-direct {v2, v11}, Lkellinwood/zipio/ZioEntry;-><init>(Ljava/lang/String;)V

    .line 657
    .local v2, "addFile":Lkellinwood/zipio/ZioEntry;
    invoke-virtual {v9}, Lkellinwood/zipio/ZioEntry;->getCompression()S

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lkellinwood/zipio/ZioEntry;->setCompression(I)V

    .line 658
    invoke-virtual {v9}, Lkellinwood/zipio/ZioEntry;->getTime()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Lkellinwood/zipio/ZioEntry;->setTime(J)V

    .line 659
    invoke-virtual {v2}, Lkellinwood/zipio/ZioEntry;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v13

    .line 661
    .local v13, "str":Ljava/io/OutputStream;
    new-instance v4, Ljava/util/zip/CRC32;

    invoke-direct {v4}, Ljava/util/zip/CRC32;-><init>()V

    .line 663
    .local v4, "crc":Ljava/util/zip/CRC32;
    invoke-virtual {v4}, Ljava/util/zip/CRC32;->reset()V

    .line 664
    :goto_2
    invoke-virtual {v5, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v12

    .local v12, "num":I
    if-lez v12, :cond_2

    .line 665
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v13, v3, v0, v12}, Ljava/io/OutputStream;->write([BII)V

    .line 666
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v4, v3, v0, v12}, Ljava/util/zip/CRC32;->update([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 674
    .end local v2    # "addFile":Lkellinwood/zipio/ZioEntry;
    .end local v3    # "buffer":[B
    .end local v4    # "crc":Ljava/util/zip/CRC32;
    .end local v5    # "data":Ljava/io/FileInputStream;
    .end local v8    # "fileF":Ljava/io/File;
    .end local v12    # "num":I
    .end local v13    # "str":Ljava/io/OutputStream;
    :catch_0
    move-exception v6

    .line 675
    .local v6, "e":Ljava/lang/Exception;
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_1

    .line 668
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v2    # "addFile":Lkellinwood/zipio/ZioEntry;
    .restart local v3    # "buffer":[B
    .restart local v4    # "crc":Ljava/util/zip/CRC32;
    .restart local v5    # "data":Ljava/io/FileInputStream;
    .restart local v8    # "fileF":Ljava/io/File;
    .restart local v12    # "num":I
    .restart local v13    # "str":Ljava/io/OutputStream;
    :cond_2
    :try_start_1
    invoke-virtual {v13}, Ljava/io/OutputStream;->flush()V

    .line 669
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 670
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lkellinwood/zipio/ZipOutput;->write(Lkellinwood/zipio/ZioEntry;)V

    .line 671
    const/4 v10, 0x1

    .line 672
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    .line 673
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "LuckyPatcher (signer): Additional files added! "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 679
    .end local v2    # "addFile":Lkellinwood/zipio/ZioEntry;
    .end local v3    # "buffer":[B
    .end local v4    # "crc":Ljava/util/zip/CRC32;
    .end local v5    # "data":Ljava/io/FileInputStream;
    .end local v7    # "file":Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;
    .end local v8    # "fileF":Ljava/io/File;
    .end local v12    # "num":I
    .end local v13    # "str":Ljava/io/OutputStream;
    :cond_3
    if-nez v10, :cond_0

    .line 680
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Lkellinwood/zipio/ZipOutput;->write(Lkellinwood/zipio/ZioEntry;)V

    goto/16 :goto_0

    .line 682
    .end local v9    # "inEntry":Lkellinwood/zipio/ZioEntry;
    .end local v10    # "mark":Z
    .end local v11    # "name":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method private copyFiles(Ljava/util/jar/Manifest;Ljava/util/Map;Ljava/util/jar/JarOutputStream;JLjava/util/ArrayList;)V
    .locals 23
    .param p1, "manifest"    # Ljava/util/jar/Manifest;
    .param p3, "output"    # Ljava/util/jar/JarOutputStream;
    .param p4, "timestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/jar/Manifest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/zipio/ZioEntry;",
            ">;",
            "Ljava/util/jar/JarOutputStream;",
            "J",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 540
    .local p2, "input":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .local p6, "modified_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/jar/Manifest;->getEntries()Ljava/util/Map;

    move-result-object v7

    .line 541
    .local v7, "entries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/jar/Attributes;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v14, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 542
    .local v14, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v14}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 543
    const/16 v16, 0x0

    .line 544
    .local v16, "outEntry":Ljava/util/jar/JarEntry;
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_0
    :goto_0
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_9

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 545
    .local v13, "name":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lkellinwood/zipio/ZioEntry;

    .line 548
    .local v10, "inEntry":Lkellinwood/zipio/ZioEntry;
    :try_start_0
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lkellinwood/zipio/ZioEntry;

    invoke-virtual/range {v18 .. v18}, Lkellinwood/zipio/ZioEntry;->getCompression()S

    move-result v18

    if-nez v18, :cond_5

    .line 551
    new-instance v17, Ljava/util/jar/JarEntry;

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lkellinwood/zipio/ZioEntry;

    invoke-virtual/range {v18 .. v18}, Lkellinwood/zipio/ZioEntry;->getName()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/util/jar/JarEntry;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 552
    .end local v16    # "outEntry":Ljava/util/jar/JarEntry;
    .local v17, "outEntry":Ljava/util/jar/JarEntry;
    const/16 v18, 0x0

    :try_start_1
    invoke-virtual/range {v17 .. v18}, Ljava/util/jar/JarEntry;->setMethod(I)V

    .line 553
    const/4 v12, 0x0

    .line 554
    .local v12, "mark2":Z
    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_1
    :goto_1
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_2

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    .line 555
    .local v8, "file":Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;
    iget-object v0, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->fileName:Ljava/lang/String;

    move-object/from16 v18, v0

    iget-object v0, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->basePath:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, ""

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v18

    if-eqz v18, :cond_1

    .line 557
    :try_start_2
    new-instance v9, Ljava/io/File;

    iget-object v0, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->fileName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 558
    .local v9, "fileF":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v21

    move-wide/from16 v0, v21

    long-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    new-array v3, v0, [B

    .line 559
    .local v3, "buffer":[B
    new-instance v5, Ljava/io/FileInputStream;

    iget-object v0, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->fileName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v5, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 560
    .local v5, "data":Ljava/io/FileInputStream;
    invoke-virtual {v5, v3}, Ljava/io/FileInputStream;->read([B)I

    .line 561
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 562
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v21

    move-object/from16 v0, v17

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Ljava/util/jar/JarEntry;->setCompressedSize(J)V

    .line 563
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v21

    move-object/from16 v0, v17

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Ljava/util/jar/JarEntry;->setSize(J)V

    .line 564
    new-instance v4, Ljava/util/zip/CRC32;

    invoke-direct {v4}, Ljava/util/zip/CRC32;-><init>()V

    .line 566
    .local v4, "crc":Ljava/util/zip/CRC32;
    invoke-virtual {v4, v3}, Ljava/util/zip/CRC32;->update([B)V

    .line 567
    invoke-virtual {v4}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v21

    move-object/from16 v0, v17

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Ljava/util/jar/JarEntry;->setCrc(J)V

    .line 568
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lkellinwood/zipio/ZioEntry;

    invoke-virtual/range {v18 .. v18}, Lkellinwood/zipio/ZioEntry;->getTime()J

    move-result-wide v21

    move-object/from16 v0, v17

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Ljava/util/jar/JarEntry;->setTime(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 569
    const/4 v12, 0x1

    goto/16 :goto_1

    .line 571
    .end local v3    # "buffer":[B
    .end local v4    # "crc":Ljava/util/zip/CRC32;
    .end local v5    # "data":Ljava/io/FileInputStream;
    .end local v9    # "fileF":Ljava/io/File;
    :catch_0
    move-exception v6

    .line 572
    .local v6, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v18, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1

    .line 629
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v8    # "file":Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;
    .end local v12    # "mark2":Z
    :catch_1
    move-exception v6

    move-object/from16 v16, v17

    .line 630
    .end local v17    # "outEntry":Ljava/util/jar/JarEntry;
    .restart local v6    # "e":Ljava/lang/Exception;
    .restart local v16    # "outEntry":Ljava/util/jar/JarEntry;
    :goto_2
    sget-object v18, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 631
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 576
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v16    # "outEntry":Ljava/util/jar/JarEntry;
    .restart local v12    # "mark2":Z
    .restart local v17    # "outEntry":Ljava/util/jar/JarEntry;
    :cond_2
    if-nez v12, :cond_3

    .line 577
    :try_start_4
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lkellinwood/zipio/ZioEntry;

    invoke-virtual/range {v18 .. v18}, Lkellinwood/zipio/ZioEntry;->getSize()I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v17

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/util/jar/JarEntry;->setCompressedSize(J)V

    .line 578
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lkellinwood/zipio/ZioEntry;

    invoke-virtual/range {v18 .. v18}, Lkellinwood/zipio/ZioEntry;->getSize()I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v17

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/util/jar/JarEntry;->setSize(J)V

    .line 579
    new-instance v4, Ljava/util/zip/CRC32;

    invoke-direct {v4}, Ljava/util/zip/CRC32;-><init>()V

    .line 581
    .restart local v4    # "crc":Ljava/util/zip/CRC32;
    invoke-virtual {v10}, Lkellinwood/zipio/ZioEntry;->getData()[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/util/zip/CRC32;->update([B)V

    .line 582
    invoke-virtual {v4}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v20

    move-object/from16 v0, v17

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/util/jar/JarEntry;->setCrc(J)V

    .line 583
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lkellinwood/zipio/ZioEntry;

    invoke-virtual/range {v18 .. v18}, Lkellinwood/zipio/ZioEntry;->getTime()J

    move-result-wide v20

    move-object/from16 v0, v17

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/util/jar/JarEntry;->setTime(J)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .end local v4    # "crc":Ljava/util/zip/CRC32;
    :cond_3
    move-object/from16 v16, v17

    .line 594
    .end local v12    # "mark2":Z
    .end local v17    # "outEntry":Ljava/util/jar/JarEntry;
    .restart local v16    # "outEntry":Ljava/util/jar/JarEntry;
    :goto_3
    const/4 v11, 0x0

    .line 595
    .local v11, "mark":Z
    :try_start_5
    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_4
    :goto_4
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_7

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    .line 596
    .restart local v8    # "file":Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;
    iget-object v0, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->fileName:Ljava/lang/String;

    move-object/from16 v20, v0

    iget-object v0, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->basePath:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, ""

    invoke-virtual/range {v20 .. v22}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    move-result v20

    if-eqz v20, :cond_4

    .line 598
    :try_start_6
    new-instance v9, Ljava/io/File;

    iget-object v0, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->fileName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 599
    .restart local v9    # "fileF":Ljava/io/File;
    const/16 v20, 0x2000

    move/from16 v0, v20

    new-array v3, v0, [B

    .line 600
    .restart local v3    # "buffer":[B
    new-instance v5, Ljava/io/FileInputStream;

    iget-object v0, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;->fileName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v5, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 602
    .restart local v5    # "data":Ljava/io/FileInputStream;
    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/jar/JarOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 605
    :goto_5
    invoke-virtual {v5, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v15

    .local v15, "num":I
    if-lez v15, :cond_6

    .line 606
    const/16 v20, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1, v15}, Ljava/util/jar/JarOutputStream;->write([BII)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_5

    .line 613
    .end local v3    # "buffer":[B
    .end local v5    # "data":Ljava/io/FileInputStream;
    .end local v9    # "fileF":Ljava/io/File;
    .end local v15    # "num":I
    :catch_2
    move-exception v6

    .line 614
    .restart local v6    # "e":Ljava/lang/Exception;
    :try_start_7
    sget-object v20, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_4

    .line 629
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v8    # "file":Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;
    .end local v11    # "mark":Z
    :catch_3
    move-exception v6

    goto/16 :goto_2

    .line 590
    :cond_5
    new-instance v17, Ljava/util/jar/JarEntry;

    move-object/from16 v0, v17

    invoke-direct {v0, v13}, Ljava/util/jar/JarEntry;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    .line 591
    .end local v16    # "outEntry":Ljava/util/jar/JarEntry;
    .restart local v17    # "outEntry":Ljava/util/jar/JarEntry;
    :try_start_8
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lkellinwood/zipio/ZioEntry;

    invoke-virtual/range {v18 .. v18}, Lkellinwood/zipio/ZioEntry;->getTime()J

    move-result-wide v20

    move-object/from16 v0, v17

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/util/jar/JarEntry;->setTime(J)V

    .line 592
    invoke-virtual {v10}, Lkellinwood/zipio/ZioEntry;->getCompression()S

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/jar/JarEntry;->setMethod(I)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    move-object/from16 v16, v17

    .end local v17    # "outEntry":Ljava/util/jar/JarEntry;
    .restart local v16    # "outEntry":Ljava/util/jar/JarEntry;
    goto/16 :goto_3

    .line 608
    .restart local v3    # "buffer":[B
    .restart local v5    # "data":Ljava/io/FileInputStream;
    .restart local v8    # "file":Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;
    .restart local v9    # "fileF":Ljava/io/File;
    .restart local v11    # "mark":Z
    .restart local v15    # "num":I
    :cond_6
    :try_start_9
    invoke-virtual/range {p3 .. p3}, Ljava/util/jar/JarOutputStream;->flush()V

    .line 609
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 610
    const/4 v11, 0x1

    .line 611
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 612
    sget-object v20, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "LuckyPatcher (signer): Additional files added! "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2

    goto/16 :goto_4

    .line 618
    .end local v3    # "buffer":[B
    .end local v5    # "data":Ljava/io/FileInputStream;
    .end local v8    # "file":Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;
    .end local v9    # "fileF":Ljava/io/File;
    .end local v15    # "num":I
    :cond_7
    if-nez v11, :cond_0

    .line 619
    :try_start_a
    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/jar/JarOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 621
    invoke-virtual {v10}, Lkellinwood/zipio/ZioEntry;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 622
    .local v5, "data":Ljava/io/InputStream;
    const/16 v18, 0x2000

    move/from16 v0, v18

    new-array v3, v0, [B

    .line 623
    .restart local v3    # "buffer":[B
    const/4 v15, 0x0

    .line 624
    .restart local v15    # "num":I
    :goto_6
    invoke-virtual {v5, v3}, Ljava/io/InputStream;->read([B)I

    move-result v15

    if-lez v15, :cond_8

    .line 625
    const/16 v18, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1, v15}, Ljava/util/jar/JarOutputStream;->write([BII)V

    goto :goto_6

    .line 627
    :cond_8
    invoke-virtual/range {p3 .. p3}, Ljava/util/jar/JarOutputStream;->flush()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    goto/16 :goto_0

    .line 637
    .end local v3    # "buffer":[B
    .end local v5    # "data":Ljava/io/InputStream;
    .end local v10    # "inEntry":Lkellinwood/zipio/ZioEntry;
    .end local v11    # "mark":Z
    .end local v13    # "name":Ljava/lang/String;
    .end local v15    # "num":I
    :cond_9
    return-void
.end method

.method private decryptPrivateKey([BLjava/lang/String;)Ljava/security/spec/KeySpec;
    .locals 8
    .param p1, "encryptedPrivateKey"    # [B
    .param p2, "keyPassword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 317
    :try_start_0
    new-instance v1, Ljavax/crypto/EncryptedPrivateKeyInfo;

    invoke-direct {v1, p1}, Ljavax/crypto/EncryptedPrivateKeyInfo;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 323
    .local v1, "epkInfo":Ljavax/crypto/EncryptedPrivateKeyInfo;
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    .line 325
    .local v4, "keyPasswd":[C
    invoke-virtual {v1}, Ljavax/crypto/EncryptedPrivateKeyInfo;->getAlgName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v5

    .line 326
    .local v5, "skFactory":Ljavax/crypto/SecretKeyFactory;
    new-instance v6, Ljavax/crypto/spec/PBEKeySpec;

    invoke-direct {v6, v4}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C)V

    invoke-virtual {v5, v6}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    move-result-object v3

    .line 328
    .local v3, "key":Ljava/security/Key;
    invoke-virtual {v1}, Ljavax/crypto/EncryptedPrivateKeyInfo;->getAlgName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 329
    .local v0, "cipher":Ljavax/crypto/Cipher;
    const/4 v6, 0x2

    invoke-virtual {v1}, Ljavax/crypto/EncryptedPrivateKeyInfo;->getAlgParameters()Ljava/security/AlgorithmParameters;

    move-result-object v7

    invoke-virtual {v0, v6, v3, v7}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/AlgorithmParameters;)V

    .line 332
    :try_start_1
    invoke-virtual {v1, v0}, Ljavax/crypto/EncryptedPrivateKeyInfo;->getKeySpec(Ljavax/crypto/Cipher;)Ljava/security/spec/PKCS8EncodedKeySpec;
    :try_end_1
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    .end local v0    # "cipher":Ljavax/crypto/Cipher;
    .end local v1    # "epkInfo":Ljavax/crypto/EncryptedPrivateKeyInfo;
    .end local v3    # "key":Ljava/security/Key;
    .end local v4    # "keyPasswd":[C
    .end local v5    # "skFactory":Ljavax/crypto/SecretKeyFactory;
    :goto_0
    return-object v6

    .line 318
    :catch_0
    move-exception v2

    .line 320
    .local v2, "ex":Ljava/io/IOException;
    const/4 v6, 0x0

    goto :goto_0

    .line 333
    .end local v2    # "ex":Ljava/io/IOException;
    .restart local v0    # "cipher":Ljavax/crypto/Cipher;
    .restart local v1    # "epkInfo":Ljavax/crypto/EncryptedPrivateKeyInfo;
    .restart local v3    # "key":Ljava/security/Key;
    .restart local v4    # "keyPasswd":[C
    .restart local v5    # "skFactory":Ljavax/crypto/SecretKeyFactory;
    :catch_1
    move-exception v2

    .line 334
    .local v2, "ex":Ljava/security/spec/InvalidKeySpecException;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v6

    const-string v7, "signapk: Password for private key may be bad."

    invoke-interface {v6, v7}, Lkellinwood/logging/LoggerInterface;->error(Ljava/lang/String;)V

    .line 335
    throw v2
.end method

.method private generateSignatureFile(Ljava/util/jar/Manifest;Ljava/io/OutputStream;)V
    .locals 10
    .param p1, "manifest"    # Ljava/util/jar/Manifest;
    .param p2, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 467
    const-string v6, "Signature-Version: 1.0\r\n"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/OutputStream;->write([B)V

    .line 468
    const-string v6, "Created-By: 1.0 (Android SignApk)\r\n"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/OutputStream;->write([B)V

    .line 472
    const-string v6, "SHA1"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v3

    .line 473
    .local v3, "md":Ljava/security/MessageDigest;
    new-instance v5, Ljava/io/PrintStream;

    new-instance v6, Ljava/security/DigestOutputStream;

    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-direct {v6, v7, v3}, Ljava/security/DigestOutputStream;-><init>(Ljava/io/OutputStream;Ljava/security/MessageDigest;)V

    const/4 v7, 0x1

    const-string v8, "UTF-8"

    invoke-direct {v5, v6, v7, v8}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;ZLjava/lang/String;)V

    .line 478
    .local v5, "print":Ljava/io/PrintStream;
    invoke-virtual {p1, v5}, Ljava/util/jar/Manifest;->write(Ljava/io/OutputStream;)V

    .line 479
    invoke-virtual {v5}, Ljava/io/PrintStream;->flush()V

    .line 481
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SHA1-Digest-Manifest: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v7

    invoke-static {v7}, Lkellinwood/security/zipsigner/Base64;->encode([B)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/OutputStream;->write([B)V

    .line 483
    invoke-virtual {p1}, Ljava/util/jar/Manifest;->getEntries()Ljava/util/Map;

    move-result-object v1

    .line 484
    .local v1, "entries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/jar/Attributes;>;"
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 485
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/jar/Attributes;>;"
    iget-boolean v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z

    if-eqz v6, :cond_1

    .line 500
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/jar/Attributes;>;"
    :cond_0
    return-void

    .line 486
    .restart local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/jar/Attributes;>;"
    :cond_1
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    const/4 v8, 0x0

    const-string v9, "Generating signature file"

    invoke-virtual {v6, v8, v9}, Lkellinwood/security/zipsigner/ProgressHelper;->progress(ILjava/lang/String;)V

    .line 488
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Name: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "\r\n"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 489
    .local v4, "nameEntry":Ljava/lang/String;
    invoke-virtual {v5, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 490
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/jar/Attributes;

    invoke-virtual {v6}, Ljava/util/jar/Attributes;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 491
    .local v0, "att":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;Ljava/lang/Object;>;"
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\r\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_1

    .line 493
    .end local v0    # "att":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;Ljava/lang/Object;>;"
    :cond_2
    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 494
    invoke-virtual {v5}, Ljava/io/PrintStream;->flush()V

    .line 496
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/OutputStream;->write([B)V

    .line 497
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SHA1-Digest: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v8

    invoke-static {v8}, Lkellinwood/security/zipsigner/Base64;->encode([B)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "\r\n\r\n"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/OutputStream;->write([B)V

    goto/16 :goto_0
.end method

.method public static getLogger()Lkellinwood/logging/LoggerInterface;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->log:Lkellinwood/logging/LoggerInterface;

    if-nez v0, :cond_0

    const-class v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkellinwood/logging/LoggerManager;->getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->log:Lkellinwood/logging/LoggerInterface;

    .line 122
    :cond_0
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->log:Lkellinwood/logging/LoggerInterface;

    return-object v0
.end method

.method public static getSupportedKeyModes()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->SUPPORTED_KEY_MODES:[Ljava/lang/String;

    return-object v0
.end method

.method private writeSignatureBlock([B[BLjava/security/cert/X509Certificate;Ljava/io/OutputStream;)V
    .locals 7
    .param p1, "signatureBlockTemplate"    # [B
    .param p2, "signatureBytes"    # [B
    .param p3, "publicKey"    # Ljava/security/cert/X509Certificate;
    .param p4, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 508
    if-eqz p1, :cond_0

    .line 509
    invoke-virtual {p4, p1}, Ljava/io/OutputStream;->write([B)V

    .line 510
    invoke-virtual {p4, p2}, Ljava/io/OutputStream;->write([B)V

    .line 528
    :goto_0
    return-void

    .line 515
    :cond_0
    :try_start_0
    const-string v3, "kellinwood.sigblock.SignatureBlockWriter"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 516
    .local v0, "sigWriterClass":Ljava/lang/Class;
    const-string v3, "writeSignatureBlock"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [B

    .line 517
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Ljava/security/cert/X509Certificate;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-class v6, Ljava/io/OutputStream;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 519
    .local v1, "sigWriterMethod":Ljava/lang/reflect/Method;
    if-nez v1, :cond_1

    .line 520
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "writeSignatureBlock() method not found."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 522
    .end local v0    # "sigWriterClass":Ljava/lang/Class;
    .end local v1    # "sigWriterMethod":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v2

    .line 523
    .local v2, "x":Ljava/lang/Exception;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Lkellinwood/logging/LoggerInterface;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 524
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to invoke writeSignatureBlock(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 525
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 521
    .end local v2    # "x":Ljava/lang/Exception;
    .restart local v0    # "sigWriterClass":Ljava/lang/Class;
    .restart local v1    # "sigWriterMethod":Ljava/lang/reflect/Method;
    :cond_1
    const/4 v3, 0x0

    const/4 v4, 0x3

    :try_start_1
    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x1

    aput-object p3, v4, v5

    const/4 v5, 0x2

    aput-object p4, v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public addAutoKeyObserver(Ljava/util/Observer;)V
    .locals 1
    .param p1, "o"    # Ljava/util/Observer;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->autoKeyObservable:Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable;

    invoke-virtual {v0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable;->addObserver(Ljava/util/Observer;)V

    .line 153
    return-void
.end method

.method public addProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
    .locals 1
    .param p1, "l"    # Lkellinwood/security/zipsigner/ProgressListener;

    .prologue
    .line 993
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-virtual {v0, p1}, Lkellinwood/security/zipsigner/ProgressHelper;->addProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V

    .line 994
    return-void
.end method

.method protected autoDetectKey(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 18
    .param p1, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/zipio/ZioEntry;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    .local p2, "zioEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v11

    invoke-interface {v11}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v3

    .line 179
    .local v3, "debug":Z
    const-string v11, "auto"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 230
    .end local p1    # "mode":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 183
    .restart local p1    # "mode":Ljava/lang/String;
    :cond_0
    const/4 v7, 0x0

    .line 185
    .local v7, "keyName":Ljava/lang/String;
    invoke-interface/range {p2 .. p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 186
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 187
    .local v6, "entryName":Ljava/lang/String;
    const-string v11, "META-INF/"

    invoke-virtual {v6, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    const-string v11, ".RSA"

    invoke-virtual {v6, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 191
    const-string v11, "MD5"

    invoke-static {v11}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v8

    .line 192
    .local v8, "md5":Ljava/security/MessageDigest;
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lkellinwood/zipio/ZioEntry;

    invoke-virtual {v11}, Lkellinwood/zipio/ZioEntry;->getData()[B

    move-result-object v5

    .line 193
    .local v5, "entryData":[B
    array-length v11, v5

    const/16 v13, 0x5b2

    if-ge v11, v13, :cond_4

    .line 219
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .end local v5    # "entryData":[B
    .end local v6    # "entryName":Ljava/lang/String;
    .end local v8    # "md5":Ljava/security/MessageDigest;
    :cond_2
    const-string v11, "auto-testkey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 221
    if-eqz v3, :cond_3

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Falling back to key="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 222
    :cond_3
    const-string p1, "testkey"

    goto :goto_0

    .line 194
    .restart local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .restart local v5    # "entryData":[B
    .restart local v6    # "entryName":Ljava/lang/String;
    .restart local v8    # "md5":Ljava/security/MessageDigest;
    :cond_4
    const/4 v11, 0x0

    const/16 v13, 0x5b2

    invoke-virtual {v8, v5, v11, v13}, Ljava/security/MessageDigest;->update([BII)V

    .line 195
    invoke-virtual {v8}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v10

    .line 198
    .local v10, "rawDigest":[B
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 199
    .local v2, "builder":Ljava/lang/StringBuilder;
    array-length v13, v10

    const/4 v11, 0x0

    :goto_1
    if-ge v11, v13, :cond_5

    aget-byte v1, v10, v11

    .line 200
    .local v1, "b":B
    const-string v14, "%02x"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 203
    .end local v1    # "b":B
    :cond_5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 205
    .local v9, "md5String":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->autoKeyDetect:Ljava/util/Map;

    invoke-interface {v11, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "keyName":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 208
    .restart local v7    # "keyName":Ljava/lang/String;
    if-eqz v3, :cond_6

    .line 209
    if-eqz v7, :cond_7

    .line 210
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v11

    const-string v13, "Auto-determined key=%s using md5=%s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v7, v14, v15

    const/4 v15, 0x1

    aput-object v9, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v11, v13}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 215
    :cond_6
    :goto_2
    if-eqz v7, :cond_1

    move-object/from16 p1, v7

    goto/16 :goto_0

    .line 212
    :cond_7
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v11

    const-string v13, "Auto key determination failed for md5=%s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v9, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v11, v13}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    goto :goto_2

    .line 224
    .end local v2    # "builder":Ljava/lang/StringBuilder;
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .end local v5    # "entryData":[B
    .end local v6    # "entryName":Ljava/lang/String;
    .end local v8    # "md5":Ljava/security/MessageDigest;
    .end local v9    # "md5String":Ljava/lang/String;
    .end local v10    # "rawDigest":[B
    :cond_8
    const-string v11, "auto-none"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 226
    if-eqz v3, :cond_9

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v11

    const-string v12, "Unable to determine key, returning: none"

    invoke-interface {v11, v12}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 227
    :cond_9
    const-string p1, "none"

    goto/16 :goto_0

    .line 230
    :cond_a
    const/16 p1, 0x0

    goto/16 :goto_0
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z

    .line 274
    return-void
.end method

.method public getKeySet()Lkellinwood/security/zipsigner/KeySet;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    return-object v0
.end method

.method public getKeymode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keymode:Ljava/lang/String;

    return-object v0
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 282
    iget-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z

    return v0
.end method

.method public loadKeys(Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 237
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->loadedKeys:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkellinwood/security/zipsigner/KeySet;

    iput-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    .line 238
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    if-eqz v3, :cond_1

    .line 261
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    new-instance v3, Lkellinwood/security/zipsigner/KeySet;

    invoke-direct {v3}, Lkellinwood/security/zipsigner/KeySet;-><init>()V

    iput-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    .line 241
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual {v3, p1}, Lkellinwood/security/zipsigner/KeySet;->setName(Ljava/lang/String;)V

    .line 242
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->loadedKeys:Ljava/util/Map;

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-interface {v3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    const-string v3, "none"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 246
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    const/4 v4, 0x1

    const-string v5, "Loading certificate and private key"

    invoke-virtual {v3, v4, v5}, Lkellinwood/security/zipsigner/ProgressHelper;->progress(ILjava/lang/String;)V

    .line 249
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/keys/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".pk8"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    .line 250
    .local v0, "privateKeyUrl":Ljava/net/URL;
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->readPrivateKey(Ljava/net/URL;Ljava/lang/String;)Ljava/security/PrivateKey;

    move-result-object v4

    invoke-virtual {v3, v4}, Lkellinwood/security/zipsigner/KeySet;->setPrivateKey(Ljava/security/PrivateKey;)V

    .line 253
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/keys/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".x509.pem"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v1

    .line 254
    .local v1, "publicKeyUrl":Ljava/net/URL;
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual {p0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->readPublicKey(Ljava/net/URL;)Ljava/security/cert/X509Certificate;

    move-result-object v4

    invoke-virtual {v3, v4}, Lkellinwood/security/zipsigner/KeySet;->setPublicKey(Ljava/security/cert/X509Certificate;)V

    .line 257
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/keys/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".sbt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v2

    .line 258
    .local v2, "sigBlockTemplateUrl":Ljava/net/URL;
    if-eqz v2, :cond_0

    .line 259
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual {p0, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->readContentAsBytes(Ljava/net/URL;)[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lkellinwood/security/zipsigner/KeySet;->setSigBlockTemplate([B)V

    goto/16 :goto_0
.end method

.method public loadProvider(Ljava/lang/String;)V
    .locals 3
    .param p1, "providerClassName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;
        }
    .end annotation

    .prologue
    .line 287
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 288
    .local v1, "providerClass":Ljava/lang/Class;
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/Provider;

    .line 289
    .local v0, "provider":Ljava/security/Provider;
    const/4 v2, 0x1

    invoke-static {v0, v2}, Ljava/security/Security;->insertProviderAt(Ljava/security/Provider;I)I

    .line 290
    return-void
.end method

.method public readContentAsBytes(Ljava/io/InputStream;)[B
    .locals 5
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 350
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 352
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    const/16 v4, 0x800

    new-array v1, v4, [B

    .line 354
    .local v1, "buffer":[B
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 355
    .local v3, "numRead":I
    :goto_0
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 356
    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 357
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    goto :goto_0

    .line 360
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 361
    .local v2, "bytes":[B
    return-object v2
.end method

.method public readContentAsBytes(Ljava/net/URL;)[B
    .locals 1
    .param p1, "contentUrl"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 343
    invoke-virtual {p1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->readContentAsBytes(Ljava/io/InputStream;)[B

    move-result-object v0

    return-object v0
.end method

.method public readPrivateKey(Ljava/net/URL;Ljava/lang/String;)Ljava/security/PrivateKey;
    .locals 5
    .param p1, "privateKeyUrl"    # Ljava/net/URL;
    .param p2, "keyPassword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 369
    new-instance v2, Ljava/io/DataInputStream;

    invoke-virtual {p1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 371
    .local v2, "input":Ljava/io/DataInputStream;
    :try_start_0
    invoke-virtual {p0, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->readContentAsBytes(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 373
    .local v0, "bytes":[B
    invoke-direct {p0, v0, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->decryptPrivateKey([BLjava/lang/String;)Ljava/security/spec/KeySpec;

    move-result-object v3

    .line 374
    .local v3, "spec":Ljava/security/spec/KeySpec;
    if-nez v3, :cond_0

    .line 375
    new-instance v3, Ljava/security/spec/PKCS8EncodedKeySpec;

    .end local v3    # "spec":Ljava/security/spec/KeySpec;
    invoke-direct {v3, v0}, Ljava/security/spec/PKCS8EncodedKeySpec;-><init>([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 379
    .restart local v3    # "spec":Ljava/security/spec/KeySpec;
    :cond_0
    :try_start_1
    const-string v4, "RSA"

    invoke-static {v4}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
    :try_end_1
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 384
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V

    :goto_0
    return-object v4

    .line 380
    :catch_0
    move-exception v1

    .line 381
    .local v1, "ex":Ljava/security/spec/InvalidKeySpecException;
    :try_start_2
    const-string v4, "DSA"

    invoke-static {v4}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 384
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V

    goto :goto_0

    .end local v0    # "bytes":[B
    .end local v1    # "ex":Ljava/security/spec/InvalidKeySpecException;
    .end local v3    # "spec":Ljava/security/spec/KeySpec;
    :catchall_0
    move-exception v4

    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V

    throw v4
.end method

.method public readPublicKey(Ljava/net/URL;)Ljava/security/cert/X509Certificate;
    .locals 3
    .param p1, "publicKeyUrl"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 295
    invoke-virtual {p1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v1

    .line 297
    .local v1, "input":Ljava/io/InputStream;
    :try_start_0
    const-string v2, "X.509"

    invoke-static {v2}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v0

    .line 298
    .local v0, "cf":Ljava/security/cert/CertificateFactory;
    invoke-virtual {v0, v1}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v2

    check-cast v2, Ljava/security/cert/X509Certificate;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-object v2

    .end local v0    # "cf":Ljava/security/cert/CertificateFactory;
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v2
.end method

.method public declared-synchronized removeProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
    .locals 1
    .param p1, "l"    # Lkellinwood/security/zipsigner/ProgressListener;

    .prologue
    .line 997
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-virtual {v0, p1}, Lkellinwood/security/zipsigner/ProgressHelper;->removeProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 998
    monitor-exit p0

    return-void

    .line 997
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public resetCanceled()V
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z

    .line 279
    return-void
.end method

.method public setKeymode(Ljava/lang/String;)V
    .locals 3
    .param p1, "km"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 160
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    invoke-interface {v0}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setKeymode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 161
    :cond_0
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keymode:Ljava/lang/String;

    .line 162
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keymode:Ljava/lang/String;

    const-string v1, "auto"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    .line 168
    :goto_0
    return-void

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-virtual {v0}, Lkellinwood/security/zipsigner/ProgressHelper;->initProgress()V

    .line 166
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keymode:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->loadKeys(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setKeys(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "publicKey"    # Ljava/security/cert/X509Certificate;
    .param p3, "privateKey"    # Ljava/security/PrivateKey;
    .param p4, "signatureBlockTemplate"    # [B

    .prologue
    .line 264
    new-instance v0, Lkellinwood/security/zipsigner/KeySet;

    invoke-direct {v0, p1, p2, p3, p4}, Lkellinwood/security/zipsigner/KeySet;-><init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    .line 265
    return-void
.end method

.method public signZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "inputZipFilename"    # Ljava/lang/String;
    .param p2, "outputZipFilename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 736
    .local p3, "modified_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v0

    .line 737
    .local v0, "inFile":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v2

    .line 739
    .local v2, "outFile":Ljava/io/File;
    invoke-virtual {v0, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 740
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Input and output files are the same.  Specify a different name for the output."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 743
    :cond_0
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-virtual {v3}, Lkellinwood/security/zipsigner/ProgressHelper;->initProgress()V

    .line 744
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    const/4 v4, 0x1

    const-string v5, "Parsing the input\'s central directory"

    invoke-virtual {v3, v4, v5}, Lkellinwood/security/zipsigner/ProgressHelper;->progress(ILjava/lang/String;)V

    .line 746
    invoke-static {p1}, Lkellinwood/zipio/ZipInput;->read(Ljava/lang/String;)Lkellinwood/zipio/ZipInput;

    move-result-object v1

    .line 747
    .local v1, "input":Lkellinwood/zipio/ZipInput;
    invoke-virtual {v1}, Lkellinwood/zipio/ZipInput;->getEntries()Ljava/util/Map;

    move-result-object v3

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3, v4, p2, p3}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->signZip(Ljava/util/Map;Ljava/io/OutputStream;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 748
    return-void
.end method

.method public signZip(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 11
    .param p1, "keystoreURL"    # Ljava/net/URL;
    .param p2, "keystoreType"    # Ljava/lang/String;
    .param p3, "keystorePw"    # Ljava/lang/String;
    .param p4, "certAlias"    # Ljava/lang/String;
    .param p5, "certPw"    # Ljava/lang/String;
    .param p6, "inputZipFilename"    # Ljava/lang/String;
    .param p7, "outputZipFilename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URL;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;,
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 693
    .local p8, "modified_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    const/4 v6, 0x0

    .line 697
    .local v6, "keystoreStream":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 698
    .local v5, "keystore":Ljava/security/KeyStore;
    if-nez p2, :cond_0

    :try_start_0
    invoke-static {}, Ljava/security/KeyStore;->getDefaultType()Ljava/lang/String;

    move-result-object p2

    .line 699
    :cond_0
    invoke-static {p2}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v5

    .line 701
    invoke-virtual {p1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v6

    .line 702
    invoke-virtual {p3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 703
    invoke-virtual {v5, p4}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v3

    .line 704
    .local v3, "cert":Ljava/security/cert/Certificate;
    move-object v0, v3

    check-cast v0, Ljava/security/cert/X509Certificate;

    move-object v8, v0

    .line 705
    .local v8, "publicKey":Ljava/security/cert/X509Certificate;
    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    invoke-virtual {v5, p4, v9}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object v4

    .line 706
    .local v4, "key":Ljava/security/Key;
    move-object v0, v4

    check-cast v0, Ljava/security/PrivateKey;

    move-object v7, v0

    .line 708
    .local v7, "privateKey":Ljava/security/PrivateKey;
    const-string v9, "custom"

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v8, v7, v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->setKeys(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V

    .line 710
    move-object/from16 v0, p6

    move-object/from16 v1, p7

    move-object/from16 v2, p8

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->signZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 712
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 714
    :cond_1
    return-void

    .line 712
    .end local v3    # "cert":Ljava/security/cert/Certificate;
    .end local v4    # "key":Ljava/security/Key;
    .end local v7    # "privateKey":Ljava/security/PrivateKey;
    .end local v8    # "publicKey":Ljava/security/cert/X509Certificate;
    :catchall_0
    move-exception v9

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    :cond_2
    throw v9
.end method

.method public signZip(Ljava/util/Map;Ljava/io/OutputStream;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 33
    .param p2, "outputStream"    # Ljava/io/OutputStream;
    .param p3, "outputZipFilename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/zipio/ZioEntry;",
            ">;",
            "Ljava/io/OutputStream;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 758
    .local p1, "zioEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .local p4, "modified_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    invoke-interface {v4}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v12

    .line 760
    .local v12, "debug":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-virtual {v4}, Lkellinwood/security/zipsigner/ProgressHelper;->initProgress()V

    .line 761
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    if-nez v4, :cond_2

    .line 762
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keymode:Ljava/lang/String;

    const-string v6, "auto"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 763
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v6, "No keys configured for signing the file!"

    invoke-direct {v4, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 766
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keymode:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v4, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->autoDetectKey(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v15

    .line 767
    .local v15, "keyName":Ljava/lang/String;
    if-nez v15, :cond_1

    .line 768
    new-instance v4, Lkellinwood/security/zipsigner/AutoKeyException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unable to auto-select key for signing "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Lkellinwood/security/zipsigner/AutoKeyException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 770
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->autoKeyObservable:Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable;

    invoke-virtual {v4, v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable;->notifyObservers(Ljava/lang/Object;)V

    .line 772
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->loadKeys(Ljava/lang/String;)V

    .line 777
    .end local v15    # "keyName":Ljava/lang/String;
    :cond_2
    const/16 v27, 0x0

    .line 778
    .local v27, "zipOutput":Lkellinwood/zipio/ZipOutput;
    const/16 v19, 0x0

    .line 783
    .local v19, "outputJar":Ljava/util/jar/JarOutputStream;
    :try_start_0
    new-instance v28, Lkellinwood/zipio/ZipOutput;

    move-object/from16 v0, v28

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lkellinwood/zipio/ZipOutput;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 784
    .end local v27    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    .local v28, "zipOutput":Lkellinwood/zipio/ZipOutput;
    :try_start_1
    new-instance v7, Ljava/util/jar/JarOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    move-object/from16 v0, p3

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v4}, Ljava/util/jar/JarOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 788
    .end local v19    # "outputJar":Ljava/util/jar/JarOutputStream;
    .local v7, "outputJar":Ljava/util/jar/JarOutputStream;
    :try_start_2
    const-string v4, "none"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual {v6}, Lkellinwood/security/zipsigner/KeySet;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 789
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Lkellinwood/security/zipsigner/ProgressHelper;->setProgressTotalItems(I)V

    .line 790
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lkellinwood/security/zipsigner/ProgressHelper;->setProgressCurrentItem(I)V

    .line 791
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v28

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->copyFiles(Ljava/util/Map;Lkellinwood/zipio/ZipOutput;Ljava/util/ArrayList;)V

    .line 792
    invoke-virtual/range {v28 .. v28}, Lkellinwood/zipio/ZipOutput;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 980
    if-eqz v7, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keymode:Ljava/lang/String;

    const-string v6, "none"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v7}, Ljava/util/jar/JarOutputStream;->close()V

    .line 982
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z

    if-eqz v4, :cond_4

    .line 984
    if-eqz p3, :cond_4

    :try_start_3
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    .line 990
    :cond_4
    :goto_0
    return-void

    .line 985
    :catch_0
    move-exception v25

    .line 986
    .local v25, "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, ":"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    goto :goto_0

    .line 797
    .end local v25    # "t":Ljava/lang/Throwable;
    :cond_5
    const/16 v20, 0x0

    .line 798
    .local v20, "progressTotalItems":I
    :try_start_4
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lkellinwood/zipio/ZioEntry;

    .line 799
    .local v13, "entry":Lkellinwood/zipio/ZioEntry;
    invoke-virtual {v13}, Lkellinwood/zipio/ZioEntry;->getName()Ljava/lang/String;

    move-result-object v17

    .line 800
    .local v17, "name":Ljava/lang/String;
    invoke-virtual {v13}, Lkellinwood/zipio/ZioEntry;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "META-INF/MANIFEST.MF"

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "META-INF/CERT.SF"

    .line 801
    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "META-INF/CERT.RSA"

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->stripPattern:Ljava/util/regex/Pattern;

    if-eqz v6, :cond_7

    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->stripPattern:Ljava/util/regex/Pattern;

    .line 803
    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-nez v6, :cond_6

    .line 804
    :cond_7
    add-int/lit8 v20, v20, 0x3

    goto :goto_1

    .line 807
    .end local v13    # "entry":Lkellinwood/zipio/ZioEntry;
    .end local v17    # "name":Ljava/lang/String;
    :cond_8
    add-int/lit8 v20, v20, 0x1

    .line 808
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Lkellinwood/security/zipsigner/ProgressHelper;->setProgressTotalItems(I)V

    .line 809
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lkellinwood/security/zipsigner/ProgressHelper;->setProgressCurrentItem(I)V

    .line 812
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual {v4}, Lkellinwood/security/zipsigner/KeySet;->getPublicKey()Ljava/security/cert/X509Certificate;

    move-result-object v4

    invoke-virtual {v4}, Ljava/security/cert/X509Certificate;->getNotBefore()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v29

    const-wide/32 v31, 0x36ee80

    add-long v8, v29, v31

    .line 816
    .local v8, "timestamp":J
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->addDigestsToManifest(Ljava/util/Map;Ljava/util/ArrayList;)Ljava/util/jar/Manifest;

    move-result-object v5

    .line 817
    .local v5, "manifest":Ljava/util/jar/Manifest;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v4, :cond_a

    .line 980
    if-eqz v7, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keymode:Ljava/lang/String;

    const-string v6, "none"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    invoke-virtual {v7}, Ljava/util/jar/JarOutputStream;->close()V

    .line 982
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z

    if-eqz v4, :cond_4

    .line 984
    if-eqz p3, :cond_4

    :try_start_5
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    .line 985
    :catch_1
    move-exception v25

    .line 986
    .restart local v25    # "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, ":"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 823
    .end local v25    # "t":Ljava/lang/Throwable;
    :cond_a
    :try_start_6
    new-instance v14, Ljava/util/jar/JarEntry;

    const-string v4, "META-INF/MANIFEST.MF"

    invoke-direct {v14, v4}, Ljava/util/jar/JarEntry;-><init>(Ljava/lang/String;)V

    .line 824
    .local v14, "je":Ljava/util/jar/JarEntry;
    invoke-virtual {v14, v8, v9}, Ljava/util/jar/JarEntry;->setTime(J)V

    .line 825
    invoke-virtual {v7, v14}, Ljava/util/jar/JarOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 826
    invoke-virtual {v5, v7}, Ljava/util/jar/Manifest;->write(Ljava/io/OutputStream;)V

    .line 850
    new-instance v23, Lkellinwood/security/zipsigner/ZipSignature;

    invoke-direct/range {v23 .. v23}, Lkellinwood/security/zipsigner/ZipSignature;-><init>()V

    .line 851
    .local v23, "signature":Lkellinwood/security/zipsigner/ZipSignature;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual {v4}, Lkellinwood/security/zipsigner/KeySet;->getPrivateKey()Ljava/security/PrivateKey;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Lkellinwood/security/zipsigner/ZipSignature;->initSign(Ljava/security/PrivateKey;)V

    .line 864
    new-instance v18, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v18 .. v18}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 865
    .local v18, "out":Ljava/io/ByteArrayOutputStream;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v5, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->generateSignatureFile(Ljava/util/jar/Manifest;Ljava/io/OutputStream;)V

    .line 866
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    if-eqz v4, :cond_c

    .line 980
    if-eqz v7, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keymode:Ljava/lang/String;

    const-string v6, "none"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    invoke-virtual {v7}, Ljava/util/jar/JarOutputStream;->close()V

    .line 982
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z

    if-eqz v4, :cond_4

    .line 984
    if-eqz p3, :cond_4

    :try_start_7
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_0

    .line 985
    :catch_2
    move-exception v25

    .line 986
    .restart local v25    # "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, ":"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 867
    .end local v25    # "t":Ljava/lang/Throwable;
    :cond_c
    :try_start_8
    invoke-virtual/range {v18 .. v18}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v21

    .line 868
    .local v21, "sfBytes":[B
    if-eqz v12, :cond_d

    .line 869
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Signature File: \n"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v10, Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-direct {v10, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, "\n"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 870
    invoke-static/range {v21 .. v21}, Lkellinwood/security/zipsigner/HexDumpEncoder;->encode([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 869
    invoke-interface {v4, v6}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 885
    :cond_d
    new-instance v14, Ljava/util/jar/JarEntry;

    .end local v14    # "je":Ljava/util/jar/JarEntry;
    const-string v4, "META-INF/CERT.SF"

    invoke-direct {v14, v4}, Ljava/util/jar/JarEntry;-><init>(Ljava/lang/String;)V

    .line 886
    .restart local v14    # "je":Ljava/util/jar/JarEntry;
    invoke-virtual {v14, v8, v9}, Ljava/util/jar/JarEntry;->setTime(J)V

    .line 887
    invoke-virtual {v7, v14}, Ljava/util/jar/JarOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 888
    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/jar/JarOutputStream;->write([B)V

    .line 891
    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lkellinwood/security/zipsigner/ZipSignature;->update([B)V

    .line 892
    invoke-virtual/range {v23 .. v23}, Lkellinwood/security/zipsigner/ZipSignature;->sign()[B

    move-result-object v24

    .line 894
    .local v24, "signatureBytes":[B
    if-eqz v12, :cond_e

    .line 896
    const-string v4, "SHA1"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v16

    .line 897
    .local v16, "md":Ljava/security/MessageDigest;
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 898
    invoke-virtual/range {v16 .. v16}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v22

    .line 899
    .local v22, "sfDigest":[B
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Sig File SHA1: \n"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static/range {v22 .. v22}, Lkellinwood/security/zipsigner/HexDumpEncoder;->encode([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 901
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Signature: \n"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static/range {v24 .. v24}, Lkellinwood/security/zipsigner/HexDumpEncoder;->encode([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 903
    const-string v4, "RSA/ECB/PKCS1Padding"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v11

    .line 904
    .local v11, "cipher":Ljavax/crypto/Cipher;
    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual {v6}, Lkellinwood/security/zipsigner/KeySet;->getPublicKey()Ljava/security/cert/X509Certificate;

    move-result-object v6

    invoke-virtual {v11, v4, v6}, Ljavax/crypto/Cipher;->init(ILjava/security/cert/Certificate;)V

    .line 906
    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v26

    .line 907
    .local v26, "tmpData":[B
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Signature Decrypted: \n"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static/range {v26 .. v26}, Lkellinwood/security/zipsigner/HexDumpEncoder;->encode([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 931
    .end local v11    # "cipher":Ljavax/crypto/Cipher;
    .end local v16    # "md":Ljava/security/MessageDigest;
    .end local v22    # "sfDigest":[B
    .end local v26    # "tmpData":[B
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    const/4 v6, 0x0

    const-string v10, "Generating signature block file"

    invoke-virtual {v4, v6, v10}, Lkellinwood/security/zipsigner/ProgressHelper;->progress(ILjava/lang/String;)V

    .line 934
    new-instance v14, Ljava/util/jar/JarEntry;

    .end local v14    # "je":Ljava/util/jar/JarEntry;
    const-string v4, "META-INF/CERT.RSA"

    invoke-direct {v14, v4}, Ljava/util/jar/JarEntry;-><init>(Ljava/lang/String;)V

    .line 935
    .restart local v14    # "je":Ljava/util/jar/JarEntry;
    invoke-virtual {v14, v8, v9}, Ljava/util/jar/JarEntry;->setTime(J)V

    .line 936
    invoke-virtual {v7, v14}, Ljava/util/jar/JarOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 938
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual {v4}, Lkellinwood/security/zipsigner/KeySet;->getSigBlockTemplate()[B

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keySet:Lkellinwood/security/zipsigner/KeySet;

    invoke-virtual {v6}, Lkellinwood/security/zipsigner/KeySet;->getPublicKey()Ljava/security/cert/X509Certificate;

    move-result-object v6

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v4, v1, v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->writeSignatureBlock([B[BLjava/security/cert/X509Certificate;Ljava/io/OutputStream;)V

    .line 972
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    if-eqz v4, :cond_10

    .line 980
    if-eqz v7, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keymode:Ljava/lang/String;

    const-string v6, "none"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_f

    invoke-virtual {v7}, Ljava/util/jar/JarOutputStream;->close()V

    .line 982
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z

    if-eqz v4, :cond_4

    .line 984
    if-eqz p3, :cond_4

    :try_start_9
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_3

    goto/16 :goto_0

    .line 985
    :catch_3
    move-exception v25

    .line 986
    .restart local v25    # "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, ":"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v25    # "t":Ljava/lang/Throwable;
    :cond_10
    move-object/from16 v4, p0

    move-object/from16 v6, p1

    move-object/from16 v10, p4

    .line 975
    :try_start_a
    invoke-direct/range {v4 .. v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->copyFiles(Ljava/util/jar/Manifest;Ljava/util/Map;Ljava/util/jar/JarOutputStream;JLjava/util/ArrayList;)V

    .line 976
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    if-eqz v4, :cond_12

    .line 980
    if-eqz v7, :cond_11

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keymode:Ljava/lang/String;

    const-string v6, "none"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    invoke-virtual {v7}, Ljava/util/jar/JarOutputStream;->close()V

    .line 982
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z

    if-eqz v4, :cond_4

    .line 984
    if-eqz p3, :cond_4

    :try_start_b
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_4

    goto/16 :goto_0

    .line 985
    :catch_4
    move-exception v25

    .line 986
    .restart local v25    # "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, ":"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 980
    .end local v25    # "t":Ljava/lang/Throwable;
    :cond_12
    if-eqz v7, :cond_13

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keymode:Ljava/lang/String;

    const-string v6, "none"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_13

    invoke-virtual {v7}, Ljava/util/jar/JarOutputStream;->close()V

    .line 982
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z

    if-eqz v4, :cond_4

    .line 984
    if-eqz p3, :cond_4

    :try_start_c
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_5

    goto/16 :goto_0

    .line 985
    :catch_5
    move-exception v25

    .line 986
    .restart local v25    # "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, ":"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 980
    .end local v5    # "manifest":Ljava/util/jar/Manifest;
    .end local v7    # "outputJar":Ljava/util/jar/JarOutputStream;
    .end local v8    # "timestamp":J
    .end local v14    # "je":Ljava/util/jar/JarEntry;
    .end local v18    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v20    # "progressTotalItems":I
    .end local v21    # "sfBytes":[B
    .end local v23    # "signature":Lkellinwood/security/zipsigner/ZipSignature;
    .end local v24    # "signatureBytes":[B
    .end local v25    # "t":Ljava/lang/Throwable;
    .end local v28    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    .restart local v19    # "outputJar":Ljava/util/jar/JarOutputStream;
    .restart local v27    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    :catchall_0
    move-exception v4

    move-object/from16 v7, v19

    .end local v19    # "outputJar":Ljava/util/jar/JarOutputStream;
    .restart local v7    # "outputJar":Ljava/util/jar/JarOutputStream;
    :goto_2
    if-eqz v7, :cond_14

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->keymode:Ljava/lang/String;

    const-string v10, "none"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_14

    invoke-virtual {v7}, Ljava/util/jar/JarOutputStream;->close()V

    .line 982
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->canceled:Z

    if-eqz v6, :cond_15

    .line 984
    if-eqz p3, :cond_15

    :try_start_d
    new-instance v6, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->delete()Z
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_6

    .line 987
    :cond_15
    :goto_3
    throw v4

    .line 985
    :catch_6
    move-exception v25

    .line 986
    .restart local v25    # "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v6

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v29, ":"

    move-object/from16 v0, v29

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v10}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    goto :goto_3

    .line 980
    .end local v7    # "outputJar":Ljava/util/jar/JarOutputStream;
    .end local v25    # "t":Ljava/lang/Throwable;
    .end local v27    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    .restart local v19    # "outputJar":Ljava/util/jar/JarOutputStream;
    .restart local v28    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    :catchall_1
    move-exception v4

    move-object/from16 v7, v19

    .end local v19    # "outputJar":Ljava/util/jar/JarOutputStream;
    .restart local v7    # "outputJar":Ljava/util/jar/JarOutputStream;
    move-object/from16 v27, v28

    .end local v28    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    .restart local v27    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    goto :goto_2

    .end local v27    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    .restart local v28    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    :catchall_2
    move-exception v4

    move-object/from16 v27, v28

    .end local v28    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    .restart local v27    # "zipOutput":Lkellinwood/zipio/ZipOutput;
    goto :goto_2
.end method

.method public signZip(Ljava/util/Map;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p2, "outputZipFilename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/zipio/ZioEntry;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 723
    .local p1, "zioEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
    .local p3, "modified_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->progressHelper:Lkellinwood/security/zipsigner/ProgressHelper;

    invoke-virtual {v0}, Lkellinwood/security/zipsigner/ProgressHelper;->initProgress()V

    .line 724
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->signZip(Ljava/util/Map;Ljava/io/OutputStream;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 725
    return-void
.end method
