.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CheckUpdate"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 15846
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 15849
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: check new version to Internet."

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 15852
    :try_start_0
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v10, "http://chelpus.defcon5.biz/Version.txt"

    iput-object v10, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->BuildVersionPath:Ljava/lang/String;

    .line 15853
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/4 v10, 0x0

    iput v10, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 15855
    const/4 v6, 0x1

    .line 15856
    .local v6, "mirror":Z
    :goto_0
    if-eqz v6, :cond_1

    .line 15858
    :try_start_1
    new-instance v8, Ljava/net/URL;

    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v9, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->BuildVersionPath:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 15860
    .local v8, "u":Ljava/net/URL;
    invoke-virtual {v8}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    .line 15861
    .local v2, "c":Ljava/net/HttpURLConnection;
    const-string v9, "GET"

    invoke-virtual {v2, v9}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 15862
    const v9, 0xf4240

    invoke-virtual {v2, v9}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 15863
    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 15864
    const-string v9, "Cache-Control"

    const-string v10, "no-cache"

    invoke-virtual {v2, v9, v10}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 15865
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 15870
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 15872
    .local v4, "in":Ljava/io/InputStream;
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 15874
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    const/16 v9, 0x2000

    new-array v1, v9, [B

    .line 15878
    .local v1, "buffer":[B
    const/4 v5, 0x0

    .line 15879
    .local v5, "len1":I
    :goto_1
    invoke-virtual {v4, v1}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v9, -0x1

    if-eq v5, v9, :cond_0

    .line 15880
    const/4 v9, 0x0

    invoke-virtual {v0, v1, v9, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_1

    .line 15888
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "buffer":[B
    .end local v2    # "c":Ljava/net/HttpURLConnection;
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v5    # "len1":I
    .end local v8    # "u":Ljava/net/URL;
    :catch_0
    move-exception v9

    .line 15910
    :goto_2
    const/4 v6, 0x0

    goto :goto_0

    .line 15882
    .restart local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "buffer":[B
    .restart local v2    # "c":Ljava/net/HttpURLConnection;
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "len1":I
    .restart local v8    # "u":Ljava/net/URL;
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v7

    .line 15884
    .local v7, "s2":Ljava/lang/String;
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    iput v10, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    .line 15887
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_2

    .line 15891
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "buffer":[B
    .end local v2    # "c":Ljava/net/HttpURLConnection;
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v5    # "len1":I
    .end local v7    # "s2":Ljava/lang/String;
    .end local v8    # "u":Ljava/net/URL;
    :catch_1
    move-exception v3

    .line 15892
    .local v3, "e":Ljava/io/IOException;
    :try_start_2
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher(AutoUpdate): Internet connection not found."

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 15918
    .end local v3    # "e":Ljava/io/IOException;
    .end local v6    # "mirror":Z
    :catch_2
    move-exception v3

    .line 15919
    .local v3, "e":Ljava/lang/Exception;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: Internet permission removed from LP."

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 15926
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 15928
    :try_start_3
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Update"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v11, v11, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    move-result v9

    if-eqz v9, :cond_3

    .line 15957
    :cond_2
    :goto_3
    return-void

    .line 15897
    .restart local v6    # "mirror":Z
    :catch_3
    move-exception v3

    .line 15898
    .restart local v3    # "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 15934
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v6    # "mirror":Z
    :cond_3
    :try_start_5
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v9, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    sget v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->versionCodeLocal:I

    if-le v9, v10, :cond_2

    .line 15937
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v9, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    const/16 v10, 0x3e7

    if-ne v9, v10, :cond_4

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v9

    const-string v10, "999"

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-nez v9, :cond_2

    .line 15939
    :cond_4
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate$1;

    invoke-direct {v10, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate;)V

    invoke-virtual {v9, v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 15945
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v9, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->FalseHttp:I

    const/4 v10, 0x2

    if-lt v9, v10, :cond_2

    .line 15946
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    const v10, 0x7f070180

    invoke-static {v10}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_3

    .line 15952
    :catch_4
    move-exception v3

    .line 15954
    .restart local v3    # "e":Ljava/lang/Exception;
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$CheckUpdate;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 15955
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 15895
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v6    # "mirror":Z
    :catch_5
    move-exception v9

    goto/16 :goto_2
.end method
