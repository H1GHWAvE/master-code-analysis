.class public Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog;
.super Ljava/lang/Object;
.source "Menu_Dialog.java"


# instance fields
.field dialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog;->dialog:Landroid/app/Dialog;

    .line 25
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog;->dialog:Landroid/app/Dialog;

    .line 87
    :cond_0
    return-void
.end method

.method public onCreateDialog()Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 39
    :try_start_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Menu Dialog create."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 40
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog;->dismiss()V

    .line 42
    :cond_1
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 44
    .local v0, "builder5":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    if-eqz v2, :cond_2

    .line 47
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 48
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog$1;

    invoke-direct {v3, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog;)V

    invoke-virtual {v0, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 68
    :cond_2
    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog$2;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog;)V

    invoke-virtual {v0, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 78
    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 80
    .end local v0    # "builder5":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :goto_0
    return-object v2

    .line 79
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 80
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public showDialog()V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog;->dialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog;->onCreateDialog()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog;->dialog:Landroid/app/Dialog;

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 32
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 34
    :cond_1
    return-void
.end method
