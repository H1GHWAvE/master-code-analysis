.class public Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
.super Ljava/lang/Object;
.source "PkgListItem.java"


# static fields
.field public static final PKG_STORED_EXTERNAL:I = 0x1

.field public static final PKG_STORED_INTERNAL:I = 0x0

.field public static final PKG_STOREPREF_AUTO:I = 0x0

.field public static final PKG_STOREPREF_EXT:I = 0x2

.field public static final PKG_STOREPREF_INT:I = 0x1

.field public static final lastIntPosition:I = 0xa

.field public static final noneAppIntPosition:I = 0x6


# instance fields
.field public ads:Z

.field public billing:Z

.field public boot_ads:Z

.field public boot_custom:Z

.field public boot_lvl:Z

.field public boot_manual:Z

.field public custom:Z

.field public enable:Z

.field public hidden:Z

.field public icon:Landroid/graphics/drawable/Drawable;

.field public lvl:Z

.field public modified:Z

.field public name:Ljava/lang/String;

.field public odex:Z

.field public on_sd:Z

.field public pkgName:Ljava/lang/String;

.field public selected:Z

.field public statusi:Ljava/lang/String;

.field public stored:I

.field public storepref:I

.field public system:Z

.field public updatetime:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "p"    # Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->statusi:Ljava/lang/String;

    .line 31
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    .line 32
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    .line 33
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    .line 34
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    .line 35
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    .line 36
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    .line 37
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    .line 38
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    .line 39
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    .line 40
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    .line 41
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    .line 42
    iput v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    .line 43
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    .line 44
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->selected:Z

    .line 45
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->on_sd:Z

    .line 60
    iget-object v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    .line 61
    iget-object v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    .line 62
    iget v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->storepref:I

    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->storepref:I

    .line 63
    iget v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 64
    iget-boolean v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    .line 65
    iget-object v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->statusi:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->statusi:Ljava/lang/String;

    .line 66
    iget-boolean v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    .line 67
    iget-boolean v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    .line 68
    iget-boolean v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    .line 69
    iget-boolean v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    .line 70
    iget-boolean v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    .line 71
    iget-boolean v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    .line 72
    iget-boolean v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    .line 73
    iget-boolean v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    .line 74
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-static {v0}, Lcom/chelpus/Utils;->isInstalledOnSdCard(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->on_sd:Z

    .line 75
    iget-boolean v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    .line 76
    iget-boolean v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    .line 77
    iget-object v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->icon:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->icon:Landroid/graphics/drawable/Drawable;

    .line 78
    iget-boolean v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    .line 79
    iget-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    if-nez v0, :cond_0

    const/16 v0, 0xa

    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 80
    :cond_0
    iget v0, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IZ)V
    .locals 36
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packName"    # Ljava/lang/String;
    .param p3, "days"    # I
    .param p4, "iconTrig"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 200
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    .line 30
    const-string v3, ""

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->statusi:Ljava/lang/String;

    .line 31
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    .line 32
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    .line 33
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    .line 34
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    .line 35
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    .line 36
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    .line 37
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    .line 38
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    .line 39
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    .line 40
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    .line 41
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    .line 42
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    .line 43
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    .line 44
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->selected:Z

    .line 45
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->on_sd:Z

    .line 201
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v25

    .line 202
    .local v25, "pm":Landroid/content/pm/PackageManager;
    const/4 v10, 0x0

    .line 204
    .local v10, "appInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz p4, :cond_0

    .line 205
    const/16 v29, 0x23

    .line 206
    .local v29, "scaleImage":I
    :try_start_0
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    move/from16 v27, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 207
    .local v27, "scale":F
    const/high16 v3, 0x420c0000    # 35.0f

    mul-float v3, v3, v27

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 v17, v0

    .line 209
    .local v17, "imgSize":I
    const/4 v2, 0x0

    .line 212
    .local v2, "iconka":Landroid/graphics/Bitmap;
    :try_start_1
    move-object/from16 v0, v25

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 214
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 215
    .local v5, "width":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 217
    .local v6, "height":I
    move/from16 v21, v17

    .line 218
    .local v21, "newWidth":I
    move/from16 v20, v17

    .line 219
    .local v20, "newHeight":I
    move/from16 v0, v21

    int-to-float v3, v0

    int-to-float v4, v5

    div-float v30, v3, v4

    .line 220
    .local v30, "scaleWidth":F
    move/from16 v0, v20

    int-to-float v3, v0

    int-to-float v4, v6

    div-float v28, v3, v4

    .line 222
    .local v28, "scaleHeight":F
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 223
    .local v7, "bMatrix":Landroid/graphics/Matrix;
    move/from16 v0, v30

    move/from16 v1, v28

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 225
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v26

    .line 226
    .local v26, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v12, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, v26

    invoke-direct {v12, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 229
    .local v12, "bmd":Landroid/graphics/drawable/BitmapDrawable;
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->icon:Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_3

    .line 231
    const/4 v2, 0x0

    .line 232
    const/4 v12, 0x0

    .line 234
    const/4 v7, 0x0

    .line 253
    .end local v2    # "iconka":Landroid/graphics/Bitmap;
    .end local v5    # "width":I
    .end local v6    # "height":I
    .end local v7    # "bMatrix":Landroid/graphics/Matrix;
    .end local v12    # "bmd":Landroid/graphics/drawable/BitmapDrawable;
    .end local v17    # "imgSize":I
    .end local v20    # "newHeight":I
    .end local v21    # "newWidth":I
    .end local v26    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v27    # "scale":F
    .end local v28    # "scaleHeight":F
    .end local v29    # "scaleImage":I
    .end local v30    # "scaleWidth":F
    :cond_0
    :goto_0
    move-object/from16 v22, p2

    .line 254
    .local v22, "packageName":Ljava/lang/String;
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    .line 255
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-static {v3}, Lcom/chelpus/Utils;->isInstalledOnSdCard(Ljava/lang/String;)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->on_sd:Z

    .line 256
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    .line 257
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    .line 258
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    .line 259
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    .line 260
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    .line 261
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    .line 262
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    .line 263
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    .line 264
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    .line 266
    if-eqz v22, :cond_1

    const-string v3, ""

    move-object/from16 v0, v22

    if-ne v0, v3, :cond_2

    .line 267
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "package scan error"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 237
    .end local v22    # "packageName":Ljava/lang/String;
    .restart local v2    # "iconka":Landroid/graphics/Bitmap;
    .restart local v17    # "imgSize":I
    .restart local v27    # "scale":F
    .restart local v29    # "scaleImage":I
    :catch_0
    move-exception v14

    .line 239
    .local v14, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_2
    invoke-virtual {v14}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 247
    .end local v2    # "iconka":Landroid/graphics/Bitmap;
    .end local v14    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v17    # "imgSize":I
    .end local v27    # "scale":F
    :catch_1
    move-exception v3

    goto :goto_0

    .line 240
    .restart local v2    # "iconka":Landroid/graphics/Bitmap;
    .restart local v17    # "imgSize":I
    .restart local v27    # "scale":F
    :catch_2
    move-exception v14

    .line 241
    .local v14, "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 242
    .end local v14    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v14

    .line 243
    .local v14, "e":Ljava/lang/OutOfMemoryError;
    goto :goto_0

    .line 269
    .end local v2    # "iconka":Landroid/graphics/Bitmap;
    .end local v14    # "e":Ljava/lang/OutOfMemoryError;
    .end local v17    # "imgSize":I
    .end local v27    # "scale":F
    .end local v29    # "scaleImage":I
    .restart local v22    # "packageName":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v25

    .line 275
    :try_start_3
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_5

    .line 278
    if-nez v10, :cond_3

    const/4 v3, 0x0

    :try_start_4
    move-object/from16 v0, v25

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v10, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 279
    :cond_3
    iget-boolean v3, v10, Landroid/content/pm/ApplicationInfo;->enabled:Z

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    .line 280
    const/4 v3, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    .line 281
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v0, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v23, v0

    .line 282
    .local v23, "pkgdir":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/chelpus/Utils;->getfirstInstallTime(Ljava/lang/String;Z)J

    move-result-wide v32

    .line 283
    .local v32, "time":J
    const-wide/16 v3, 0x3e8

    div-long v3, v32, v3

    long-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    .line 285
    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->isOdex(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 286
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_5

    .line 295
    :goto_1
    const/4 v3, 0x2

    :try_start_5
    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 296
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->storepref:I

    .line 297
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/chelpus/Utils;->isModified(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    .line 304
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->bootlist:[Ljava/lang/String;

    if-eqz v3, :cond_a

    .line 306
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_2
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->bootlist:[Ljava/lang/String;

    array-length v3, v3

    move/from16 v0, v16

    if-ge v0, v3, :cond_a

    .line 307
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->bootlist:[Ljava/lang/String;

    aget-object v3, v3, v16

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v31, v0

    .line 308
    .local v31, "templist":[Ljava/lang/String;
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->bootlist:[Ljava/lang/String;

    aget-object v3, v3, v16

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v31

    .line 311
    const/4 v3, 0x0

    aget-object v3, v31, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 312
    const/16 v18, 0x1

    .local v18, "j":I
    :goto_3
    move-object/from16 v0, v31

    array-length v3, v0

    move/from16 v0, v18

    if-ge v0, v3, :cond_9

    .line 314
    aget-object v3, v31, v18

    const-string v4, "ads"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    .line 315
    :cond_4
    aget-object v3, v31, v18

    const-string v4, "lvl"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    .line 316
    :cond_5
    aget-object v3, v31, v18

    const-string v4, "custom"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_5

    .line 312
    :cond_6
    add-int/lit8 v18, v18, 0x1

    goto :goto_3

    .line 288
    .end local v16    # "i":I
    .end local v18    # "j":I
    .end local v31    # "templist":[Ljava/lang/String;
    :cond_7
    const/4 v3, 0x0

    :try_start_6
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z
    :try_end_6
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_1

    .line 291
    .end local v23    # "pkgdir":Ljava/lang/String;
    .end local v32    # "time":J
    :catch_4
    move-exception v14

    .line 292
    .local v14, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_7
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 293
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "package scan error"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_7
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_5

    .line 408
    .end local v14    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_5
    move-exception v14

    .line 409
    .local v14, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v14}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 410
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    .line 411
    const/16 v24, 0x0

    .line 413
    .local v24, "pkginfo":Landroid/content/pm/PackageInfo;
    const/16 v3, 0x1001

    :try_start_8
    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v24

    .line 414
    const/4 v13, 0x0

    .local v13, "d":I
    :goto_4
    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    array-length v3, v3

    if-ge v13, v3, :cond_1a

    .line 415
    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    aget-object v3, v3, v13

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/chelpus/Utils;->isAds(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 416
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 417
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_a

    .line 414
    :cond_8
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 306
    .end local v13    # "d":I
    .end local v14    # "e":Ljava/lang/NullPointerException;
    .end local v24    # "pkginfo":Landroid/content/pm/PackageInfo;
    .restart local v16    # "i":I
    .restart local v23    # "pkgdir":Ljava/lang/String;
    .restart local v31    # "templist":[Ljava/lang/String;
    .restart local v32    # "time":J
    :cond_9
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_2

    .line 323
    .end local v16    # "i":I
    .end local v31    # "templist":[Ljava/lang/String;
    :cond_a
    :try_start_9
    const-string v3, "bootlist"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v11

    .line 324
    .local v11, "base":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v19

    .line 325
    .local v19, "list":[Ljava/io/File;
    if-eqz v19, :cond_c

    .line 326
    move-object/from16 v0, v19

    array-length v4, v0

    const/4 v3, 0x0

    :goto_5
    if-ge v3, v4, :cond_c

    aget-object v9, v19, v3

    .line 327
    .local v9, "aList":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v34, "bootlist"

    move-object/from16 v0, v34

    invoke-virtual {v8, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_b

    .line 328
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    const/4 v8, 0x1

    move-object/from16 v0, p0

    iput-boolean v8, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    .line 326
    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 332
    .end local v9    # "aList":Ljava/io/File;
    :cond_c
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;
    :try_end_9
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_9} :catch_5

    .line 334
    if-nez v10, :cond_d

    const/4 v3, 0x0

    :try_start_a
    move-object/from16 v0, v25

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v10, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 341
    :cond_d
    iget v3, v10, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_e

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6
    .catch Ljava/lang/NullPointerException; {:try_start_a .. :try_end_a} :catch_5

    .line 345
    :cond_e
    :goto_6
    const/16 v24, 0x0

    .line 347
    .restart local v24    # "pkginfo":Landroid/content/pm/PackageInfo;
    const/16 v3, 0x1001

    :try_start_b
    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v24

    .line 348
    const/4 v13, 0x0

    .restart local v13    # "d":I
    :goto_7
    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    array-length v3, v3

    if-ge v13, v3, :cond_10

    .line 349
    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    aget-object v3, v3, v13

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/chelpus/Utils;->isAds(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 350
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 351
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_b} :catch_5

    .line 348
    :cond_f
    add-int/lit8 v13, v13, 0x1

    goto :goto_7

    .line 342
    .end local v13    # "d":I
    .end local v24    # "pkginfo":Landroid/content/pm/PackageInfo;
    :catch_6
    move-exception v14

    .line 343
    .local v14, "e":Ljava/lang/Exception;
    :try_start_c
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LuckyPatcher (PkgListItem): "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/NullPointerException; {:try_start_c .. :try_end_c} :catch_5

    goto :goto_6

    .line 353
    .end local v14    # "e":Ljava/lang/Exception;
    .restart local v24    # "pkginfo":Landroid/content/pm/PackageInfo;
    :catch_7
    move-exception v3

    .line 356
    :cond_10
    if-nez v24, :cond_11

    .line 357
    :try_start_d
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/16 v4, 0x1000

    move-object/from16 v0, v22

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_8
    .catch Ljava/lang/NullPointerException; {:try_start_d .. :try_end_d} :catch_5

    move-result-object v24

    .line 362
    :cond_11
    :goto_8
    :try_start_e
    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    if-eqz v3, :cond_13

    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    array-length v3, v3

    if-eqz v3, :cond_13

    .line 363
    const/4 v13, 0x0

    .restart local v13    # "d":I
    :goto_9
    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    array-length v3, v3

    if-ge v13, v3, :cond_13

    .line 364
    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    aget-object v3, v3, v13

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "BILLING"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    .line 366
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    .line 367
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-nez v3, :cond_12

    const/4 v3, 0x3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 369
    :cond_12
    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    aget-object v3, v3, v13

    const-string v4, "CHECK_LICENSE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_19

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_19

    .line 370
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-nez v3, :cond_18

    .line 371
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    .line 372
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 373
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-eqz v3, :cond_19

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    if-eqz v3, :cond_19

    .line 383
    .end local v13    # "d":I
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-nez v3, :cond_14

    .line 384
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-nez v3, :cond_14

    .line 385
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    if-nez v3, :cond_14

    .line 386
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I
    :try_end_e
    .catch Ljava/lang/NullPointerException; {:try_start_e .. :try_end_e} :catch_5

    .line 396
    :cond_14
    :try_start_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-static {v3}, Lcom/chelpus/Utils;->isCustomPatchesForPkg(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 397
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 398
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    .line 401
    :cond_15
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v34, 0x3e8

    div-long v3, v3, v34

    long-to-int v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const v4, 0x15180

    mul-int v4, v4, p3

    if-ge v3, v4, :cond_16

    .line 402
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_9
    .catch Ljava/lang/NullPointerException; {:try_start_f .. :try_end_f} :catch_5

    .line 469
    .end local v11    # "base":Ljava/io/File;
    .end local v19    # "list":[Ljava/io/File;
    .end local v23    # "pkgdir":Ljava/lang/String;
    .end local v32    # "time":J
    :cond_16
    :goto_a
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    if-nez v3, :cond_17

    const/16 v3, 0xa

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 470
    :cond_17
    const/16 v25, 0x0

    .line 472
    return-void

    .line 359
    .restart local v11    # "base":Ljava/io/File;
    .restart local v19    # "list":[Ljava/io/File;
    .restart local v23    # "pkgdir":Ljava/lang/String;
    .restart local v32    # "time":J
    :catch_8
    move-exception v15

    .line 360
    .local v15, "e1":Ljava/lang/Exception;
    :try_start_10
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_8

    .line 375
    .end local v15    # "e1":Ljava/lang/Exception;
    .restart local v13    # "d":I
    :cond_18
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 376
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    .line 377
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-eqz v3, :cond_19

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    if-nez v3, :cond_13

    .line 363
    :cond_19
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_9

    .line 404
    .end local v13    # "d":I
    :catch_9
    move-exception v15

    .line 405
    .restart local v15    # "e1":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LuckyPatcher (PkgListItem): Custom patches not found! "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 406
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_10
    .catch Ljava/lang/NullPointerException; {:try_start_10 .. :try_end_10} :catch_5

    goto :goto_a

    .line 419
    .end local v11    # "base":Ljava/io/File;
    .end local v15    # "e1":Ljava/lang/Exception;
    .end local v19    # "list":[Ljava/io/File;
    .end local v23    # "pkgdir":Ljava/lang/String;
    .end local v32    # "time":J
    .local v14, "e":Ljava/lang/NullPointerException;
    :catch_a
    move-exception v3

    .line 422
    :cond_1a
    if-nez v24, :cond_1b

    .line 423
    :try_start_11
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/16 v4, 0x1000

    move-object/from16 v0, v22

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_c

    move-result-object v24

    .line 428
    :cond_1b
    :goto_b
    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    if-eqz v3, :cond_1d

    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    array-length v3, v3

    if-eqz v3, :cond_1d

    .line 429
    const/4 v13, 0x0

    .restart local v13    # "d":I
    :goto_c
    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    array-length v3, v3

    if-ge v13, v3, :cond_1d

    .line 430
    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    aget-object v3, v3, v13

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "BILLING"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1c

    .line 431
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    .line 432
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-nez v3, :cond_1c

    const/4 v3, 0x3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 434
    :cond_1c
    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    aget-object v3, v3, v13

    const-string v4, "CHECK_LICENSE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_21

    .line 435
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-nez v3, :cond_20

    .line 436
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    .line 437
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 438
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-eqz v3, :cond_21

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    if-eqz v3, :cond_21

    .line 449
    .end local v13    # "d":I
    :cond_1d
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-nez v3, :cond_1e

    .line 450
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-nez v3, :cond_1e

    .line 451
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    if-nez v3, :cond_1e

    .line 452
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 457
    :cond_1e
    :try_start_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-static {v3}, Lcom/chelpus/Utils;->isCustomPatchesForPkg(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 458
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 459
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    .line 461
    :cond_1f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v34, 0x3e8

    div-long v3, v3, v34

    long-to-int v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const v4, 0x15180

    mul-int v4, v4, p3

    if-ge v3, v4, :cond_16

    .line 462
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_b

    goto/16 :goto_a

    .line 464
    :catch_b
    move-exception v15

    .line 465
    .restart local v15    # "e1":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LuckyPatcher (PkgListItem): Custom patches not found! "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 466
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_a

    .line 425
    .end local v15    # "e1":Ljava/lang/Exception;
    :catch_c
    move-exception v15

    .line 426
    .restart local v15    # "e1":Ljava/lang/Exception;
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_b

    .line 440
    .end local v15    # "e1":Ljava/lang/Exception;
    .restart local v13    # "d":I
    :cond_20
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 441
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    .line 442
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-eqz v3, :cond_21

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    if-nez v3, :cond_1d

    .line 429
    :cond_21
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_c
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;IIIIIIIIIILandroid/graphics/Bitmap;IIZZZ)V
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pkgNamein"    # Ljava/lang/String;
    .param p3, "pkgLabel"    # Ljava/lang/String;
    .param p4, "storedin"    # I
    .param p5, "storeprefin"    # I
    .param p6, "hiddenin"    # I
    .param p7, "statusiin"    # Ljava/lang/String;
    .param p8, "boot_adsin"    # I
    .param p9, "boot_lvlin"    # I
    .param p10, "boot_customin"    # I
    .param p11, "boot_manualin"    # I
    .param p12, "customin"    # I
    .param p13, "lvlin"    # I
    .param p14, "adsin"    # I
    .param p15, "modifiedin"    # I
    .param p16, "systemin"    # I
    .param p17, "odexin"    # I
    .param p18, "iconin"    # Landroid/graphics/Bitmap;
    .param p19, "updatetimein"    # I
    .param p20, "billingin"    # I
    .param p21, "enablein"    # Z
    .param p22, "sd_on"    # Z
    .param p23, "skipIconRead"    # Z

    .prologue
    .line 111
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    .line 30
    const-string v3, ""

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->statusi:Ljava/lang/String;

    .line 31
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    .line 32
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    .line 33
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    .line 34
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    .line 35
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    .line 36
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    .line 37
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    .line 38
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    .line 39
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    .line 40
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    .line 41
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    .line 42
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    .line 43
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    .line 44
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->selected:Z

    .line 45
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->on_sd:Z

    .line 113
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    .line 114
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    .line 115
    move/from16 v0, p5

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->storepref:I

    .line 116
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 117
    move/from16 v0, p21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    .line 118
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    if-nez v3, :cond_0

    const/16 v3, 0xa

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 119
    :cond_0
    if-nez p6, :cond_2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    .line 121
    :goto_0
    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->statusi:Ljava/lang/String;

    .line 122
    if-nez p8, :cond_3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    .line 124
    :goto_1
    if-nez p9, :cond_4

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    .line 126
    :goto_2
    if-nez p10, :cond_5

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    .line 128
    :goto_3
    if-nez p11, :cond_6

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    .line 130
    :goto_4
    if-nez p12, :cond_7

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    .line 132
    :goto_5
    if-nez p13, :cond_8

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    .line 134
    :goto_6
    if-nez p14, :cond_9

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    .line 136
    :goto_7
    if-nez p15, :cond_a

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    .line 138
    :goto_8
    move/from16 v0, p22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->on_sd:Z

    .line 139
    if-nez p16, :cond_b

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    .line 141
    :goto_9
    if-nez p17, :cond_c

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    .line 143
    :goto_a
    if-nez p20, :cond_d

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    .line 145
    :goto_b
    if-nez p23, :cond_1

    .line 146
    if-eqz p18, :cond_e

    .line 147
    move/from16 v0, p19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    .line 148
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p18

    invoke-direct {v3, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->icon:Landroid/graphics/drawable/Drawable;

    .line 198
    :cond_1
    :goto_c
    return-void

    .line 120
    :cond_2
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    goto :goto_0

    .line 123
    :cond_3
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    goto :goto_1

    .line 125
    :cond_4
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    goto :goto_2

    .line 127
    :cond_5
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    goto :goto_3

    .line 129
    :cond_6
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    goto :goto_4

    .line 131
    :cond_7
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    goto :goto_5

    .line 133
    :cond_8
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    goto :goto_6

    .line 135
    :cond_9
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    goto :goto_7

    .line 137
    :cond_a
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    goto :goto_8

    .line 140
    :cond_b
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    goto :goto_9

    .line 142
    :cond_c
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    goto :goto_a

    .line 144
    :cond_d
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    goto :goto_b

    .line 150
    :cond_e
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    .line 152
    .local v14, "pm":Landroid/content/pm/PackageManager;
    const/16 v18, 0x23

    .line 153
    .local v18, "scaleImage":I
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    move/from16 v16, v0

    .line 154
    .local v16, "scale":F
    const/high16 v3, 0x420c0000    # 35.0f

    mul-float v3, v3, v16

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v11, v3

    .line 156
    .local v11, "imgSize":I
    const/4 v2, 0x0

    .line 159
    .local v2, "iconka":Landroid/graphics/Bitmap;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v14, v3}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 161
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 162
    .local v5, "width":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 164
    .local v6, "height":I
    move v13, v11

    .line 165
    .local v13, "newWidth":I
    move v12, v11

    .line 166
    .local v12, "newHeight":I
    int-to-float v3, v13

    int-to-float v4, v5

    div-float v19, v3, v4

    .line 167
    .local v19, "scaleWidth":F
    int-to-float v3, v12

    int-to-float v4, v6

    div-float v17, v3, v4

    .line 169
    .local v17, "scaleHeight":F
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 170
    .local v7, "bMatrix":Landroid/graphics/Matrix;
    move/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 172
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 173
    .local v15, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v9, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v9, v15}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 176
    .local v9, "bmd":Landroid/graphics/drawable/BitmapDrawable;
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->icon:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    .line 178
    const/4 v2, 0x0

    .line 179
    const/4 v9, 0x0

    .line 180
    const/4 v14, 0x0

    .line 181
    const/4 v7, 0x0

    .line 183
    goto/16 :goto_c

    .line 184
    .end local v5    # "width":I
    .end local v6    # "height":I
    .end local v7    # "bMatrix":Landroid/graphics/Matrix;
    .end local v9    # "bmd":Landroid/graphics/drawable/BitmapDrawable;
    .end local v12    # "newHeight":I
    .end local v13    # "newWidth":I
    .end local v15    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v17    # "scaleHeight":F
    .end local v19    # "scaleWidth":F
    :catch_0
    move-exception v10

    .line 186
    .local v10, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v10}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_c

    .line 187
    .end local v10    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v10

    .line 188
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_c

    .line 189
    .end local v10    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v10

    .line 190
    .local v10, "e":Ljava/lang/OutOfMemoryError;
    goto/16 :goto_c
.end method


# virtual methods
.method public equalsPli(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)Z
    .locals 5
    .param p1, "item"    # Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .prologue
    const/4 v1, 0x0

    .line 480
    :try_start_0
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    .line 481
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    iget v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->storepref:I

    iget v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->storepref:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    iget-boolean v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    iget-boolean v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    iget-boolean v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    iget-boolean v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    iget-boolean v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    iget-boolean v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    iget-boolean v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    iget-boolean v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    iget-boolean v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->on_sd:Z

    iget-boolean v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->on_sd:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    iget-boolean v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    iget-boolean v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    iget-boolean v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    iget-boolean v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    iget v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v2, v3, :cond_0

    .line 497
    const/4 v1, 0x1

    .line 503
    :cond_0
    :goto_0
    return v1

    .line 498
    :catch_0
    move-exception v0

    .line 499
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LuckyPacther: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 500
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public getColor()I
    .locals 2

    .prologue
    .line 513
    const v0, 0x7f0b0001

    .line 515
    .local v0, "color":I
    iget v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    packed-switch v1, :pswitch_data_0

    .line 535
    :goto_0
    return v0

    .line 517
    :pswitch_0
    const v0, 0x7f0b0005

    .line 519
    goto :goto_0

    .line 521
    :pswitch_1
    const/high16 v0, 0x7f0b0000

    .line 523
    goto :goto_0

    .line 525
    :pswitch_2
    const v0, 0x7f0b0003

    .line 528
    goto :goto_0

    .line 530
    :pswitch_3
    const v0, 0x7f0b0002

    goto :goto_0

    .line 515
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public saveItem()V
    .locals 2

    .prologue
    .line 507
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    if-nez v0, :cond_0

    .line 508
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    .line 509
    :cond_0
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    invoke-virtual {v0, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->savePackage(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V

    .line 510
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    return-object v0
.end method
