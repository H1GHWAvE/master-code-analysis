.class Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2;
.super Ljava/lang/Object;
.source "PackageChangeReceiver.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->connectToBilling()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    .prologue
    .line 573
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 8
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 584
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Billing service try to connect."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 586
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 587
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Firmware not support hacked billing"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 591
    sget-boolean v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-nez v2, :cond_0

    .line 593
    const v2, 0x7f070234

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f07025b

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2$1;

    invoke-direct {v4, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2;)V

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2$2;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2;)V

    invoke-static {v2, v3, v4, v5}, Lcom/chelpus/Utils;->showSystemWindow(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 625
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v3, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3, v7, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 628
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v3, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3, v7, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 640
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/inapp_widget;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 642
    .local v1, "in":Landroid/content/Intent;
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/inapp_widget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 643
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 645
    :try_start_0
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->mServiceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 654
    .end local v1    # "in":Landroid/content/Intent;
    :goto_1
    return-void

    .line 632
    :cond_0
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2$3;

    invoke-direct {v3, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 637
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 646
    .restart local v1    # "in":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 649
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "in":Landroid/content/Intent;
    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Billing service connected."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 651
    :try_start_1
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->mServiceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 652
    :catch_1
    move-exception v0

    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 576
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Billing service disconnected."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 577
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->mSetupDone:Z

    .line 578
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

    .line 579
    return-void
.end method
