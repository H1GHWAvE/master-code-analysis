.class public Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom2_Dialog;
.super Ljava/lang/Object;
.source "Custom2_Dialog.java"


# instance fields
.field dialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom2_Dialog;->dialog:Landroid/app/Dialog;

    .line 30
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom2_Dialog;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom2_Dialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom2_Dialog;->dialog:Landroid/app/Dialog;

    .line 159
    :cond_0
    return-void
.end method

.method public onCreateDialog()Landroid/app/Dialog;
    .locals 19

    .prologue
    .line 43
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v15

    const v16, 0x7f040023

    const/16 v17, 0x0

    invoke-static/range {v15 .. v17}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    .line 44
    .local v5, "d":Landroid/widget/RelativeLayout;
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v16, "Custom2 Dialog create."

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 45
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v15, :cond_0

    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v15

    if-nez v15, :cond_1

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom2_Dialog;->dismiss()V

    .line 46
    :cond_1
    const v15, 0x7f0d002f

    invoke-virtual {v5, v15}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    const v16, 0x7f0d0088

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    .line 47
    .local v11, "scrbody":Landroid/widget/RelativeLayout;
    const v15, 0x7f0d002e

    invoke-virtual {v5, v15}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    .line 52
    const v15, 0x7f0d000d

    :try_start_0
    invoke-virtual {v11, v15}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 53
    .local v13, "txt2":Landroid/widget/TextView;
    const v15, 0x7f0d0034

    invoke-virtual {v11, v15}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    .line 54
    const-string v12, "-----------------------------------------------\n"

    .line 55
    .local v12, "str2":Ljava/lang/String;
    const-string v15, ""

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0700d7

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 58
    const v15, -0xff008d

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 60
    const-string v12, "-----------------------------------------------\n"

    .line 61
    const-string v15, ""

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 62
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 64
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 65
    const v15, -0xff008d

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 67
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f070202

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 68
    const v15, -0xff008d

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 70
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v15

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v15

    iget-object v12, v15, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 71
    const v15, -0xff008d

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 73
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f0701f5

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 74
    const v15, -0xff008d

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 77
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v16

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v16

    move-object/from16 v0, v16

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 78
    const v15, -0xff008d

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 97
    :goto_0
    const-string v12, "\n-----------------------------------------------\n"

    .line 99
    const-string v15, ""

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 100
    const-string v2, ""

    .line 101
    .local v2, "beginText":Ljava/lang/String;
    const/4 v4, 0x0

    .line 102
    .local v4, "custom":Ljava/lang/String;
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->customselect:Ljava/io/File;

    invoke-virtual {v15}, Ljava/io/File;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 105
    :try_start_1
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v4}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 106
    .local v8, "fis":Ljava/io/FileInputStream;
    new-instance v10, Ljava/io/InputStreamReader;

    const-string v15, "UTF-8"

    invoke-direct {v10, v8, v15}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 107
    .local v10, "isr":Ljava/io/InputStreamReader;
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 108
    .local v3, "br":Ljava/io/BufferedReader;
    const-string v6, ""

    .line 110
    .local v6, "data":Ljava/lang/String;
    const/16 v15, 0x7d0

    new-array v14, v15, [Ljava/lang/String;

    .line 111
    .local v14, "txtdata":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 112
    .local v1, "begin":Z
    const/4 v9, 0x0

    .line 114
    .local v9, "g":I
    :cond_2
    :goto_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 115
    aput-object v6, v14, v9

    .line 117
    if-eqz v1, :cond_4

    aget-object v15, v14, v9

    const-string v16, "["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_3

    aget-object v15, v14, v9

    const-string v16, "]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_3

    aget-object v15, v14, v9

    const-string v16, "{"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 118
    :cond_3
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\n"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 119
    const/4 v1, 0x0

    .line 121
    :cond_4
    if-eqz v1, :cond_5

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    aget-object v16, v14, v9

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 122
    :cond_5
    aget-object v15, v14, v9

    invoke-virtual {v15}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v15

    const-string v16, "[BEGIN]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v15

    if-eqz v15, :cond_2

    .line 123
    const/4 v1, 0x1

    goto :goto_1

    .line 80
    .end local v1    # "begin":Z
    .end local v2    # "beginText":Ljava/lang/String;
    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v4    # "custom":Ljava/lang/String;
    .end local v6    # "data":Ljava/lang/String;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "g":I
    .end local v10    # "isr":Ljava/io/InputStreamReader;
    .end local v14    # "txtdata":[Ljava/lang/String;
    :cond_6
    :try_start_2
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getApkLabelName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 81
    const v15, -0xff008d

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 83
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f070202

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 84
    const v15, -0xff008d

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 86
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;

    invoke-static {v15}, Lcom/chelpus/Utils;->getApkPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v15

    iget-object v12, v15, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 87
    const v15, -0xff008d

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 89
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f0701f5

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 90
    const v15, -0xff008d

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 93
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getApkPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v16

    move-object/from16 v0, v16

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 94
    const v15, -0xff008d

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 145
    .end local v12    # "str2":Ljava/lang/String;
    .end local v13    # "txt2":Landroid/widget/TextView;
    :catch_0
    move-exception v15

    .line 151
    :goto_2
    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v16 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setCancelable(Z)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v15

    invoke-virtual {v15, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setView(Landroid/view/View;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v15

    invoke-virtual {v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v15

    return-object v15

    .line 126
    .restart local v1    # "begin":Z
    .restart local v2    # "beginText":Ljava/lang/String;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "custom":Ljava/lang/String;
    .restart local v6    # "data":Ljava/lang/String;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "g":I
    .restart local v10    # "isr":Ljava/io/InputStreamReader;
    .restart local v12    # "str2":Ljava/lang/String;
    .restart local v13    # "txt2":Landroid/widget/TextView;
    .restart local v14    # "txtdata":[Ljava/lang/String;
    :cond_7
    :try_start_3
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    .line 132
    .end local v1    # "begin":Z
    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v6    # "data":Ljava/lang/String;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "g":I
    .end local v10    # "isr":Ljava/io/InputStreamReader;
    .end local v14    # "txtdata":[Ljava/lang/String;
    :goto_3
    :try_start_4
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0700d5

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 133
    const v15, -0xf1bbe

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 135
    const-string v12, "-----------------------------------------------\n"

    .line 136
    const-string v15, ""

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 138
    move-object v12, v2

    .line 139
    const v15, -0xf1bbe

    const-string v16, "bold"

    move-object/from16 v0, v16

    invoke-static {v12, v15, v0}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 147
    .end local v2    # "beginText":Ljava/lang/String;
    .end local v4    # "custom":Ljava/lang/String;
    .end local v12    # "str2":Ljava/lang/String;
    .end local v13    # "txt2":Landroid/widget/TextView;
    :catch_1
    move-exception v7

    .line 149
    .local v7, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v7}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 127
    .end local v7    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "beginText":Ljava/lang/String;
    .restart local v4    # "custom":Ljava/lang/String;
    .restart local v12    # "str2":Ljava/lang/String;
    .restart local v13    # "txt2":Landroid/widget/TextView;
    :catch_2
    move-exception v7

    .line 128
    .local v7, "e":Ljava/io/FileNotFoundException;
    :try_start_5
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v16, "Custom Patch not Found in\n/sdcard/LuckyPatcher/\n"

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_3

    .line 129
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v7

    .line 130
    .local v7, "e":Ljava/io/IOException;
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_3
.end method

.method public showDialog()V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom2_Dialog;->dialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom2_Dialog;->onCreateDialog()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom2_Dialog;->dialog:Landroid/app/Dialog;

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom2_Dialog;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 37
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Custom2_Dialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 39
    :cond_1
    return-void
.end method
