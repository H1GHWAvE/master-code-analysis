.class Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;
.super Ljava/lang/Object;
.source "AlertDlg.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field displayWidth:I

.field orient:I

.field setForSize:I

.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .prologue
    const/4 v0, 0x0

    .line 44
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->setForSize:I

    .line 46
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->displayWidth:I

    .line 47
    const/16 v0, 0xff

    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->orient:I

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 12

    .prologue
    const/16 v11, 0x384

    const/4 v8, 0x0

    .line 50
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    invoke-virtual {v7}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    .line 51
    .local v6, "w":Landroid/view/Window;
    invoke-virtual {v6}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 52
    .local v0, "current_width":I
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v1, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 53
    .local v1, "display_height":I
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v2, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 54
    .local v2, "display_width":I
    if-le v1, v2, :cond_5

    iput v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->orient:I

    .line 56
    :goto_0
    iget v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->displayWidth:I

    if-eqz v7, :cond_0

    iget v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->displayWidth:I

    if-eq v7, v2, :cond_1

    .line 57
    :cond_0
    iput v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->setForSize:I

    .line 58
    iput v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->displayWidth:I

    .line 60
    :cond_1
    iget v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->setForSize:I

    if-eqz v7, :cond_2

    iget v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->setForSize:I

    if-eq v0, v7, :cond_4

    .line 62
    :cond_2
    const/16 v7, 0x11

    invoke-virtual {v6, v7}, Landroid/view/Window;->setGravity(I)V

    .line 64
    invoke-virtual {v6}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 66
    .local v3, "lp":Landroid/view/WindowManager$LayoutParams;
    int-to-double v7, v2

    const-wide v9, 0x3fef5c28f5c28f5cL    # 0.98

    mul-double/2addr v7, v9

    double-to-int v4, v7

    .line 68
    .local v4, "max_width":I
    int-to-double v7, v2

    const-wide/high16 v9, 0x3fe8000000000000L    # 0.75

    mul-double/2addr v7, v9

    double-to-int v5, v7

    .line 70
    .local v5, "min_width":I
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "wight:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 71
    iget v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->orient:I

    packed-switch v7, :pswitch_data_0

    .line 110
    :cond_3
    :goto_1
    iget v7, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->setForSize:I

    .line 112
    invoke-virtual {v6, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 114
    .end local v3    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v4    # "max_width":I
    .end local v5    # "min_width":I
    :cond_4
    return-void

    .line 55
    :cond_5
    const/4 v7, 0x1

    iput v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->orient:I

    goto :goto_0

    .line 73
    .restart local v3    # "lp":Landroid/view/WindowManager$LayoutParams;
    .restart local v4    # "max_width":I
    .restart local v5    # "min_width":I
    :pswitch_0
    if-ge v2, v11, :cond_7

    .line 74
    if-lt v0, v5, :cond_6

    .line 75
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 76
    iput v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->setForSize:I

    goto :goto_1

    .line 78
    :cond_6
    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 79
    iput v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->setForSize:I

    goto :goto_1

    .line 82
    :cond_7
    if-lt v0, v4, :cond_8

    .line 83
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 84
    iput v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->setForSize:I

    goto :goto_1

    .line 86
    :cond_8
    if-ge v0, v5, :cond_9

    .line 87
    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 88
    iput v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->setForSize:I

    goto :goto_1

    .line 90
    :cond_9
    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    goto :goto_1

    .line 95
    :pswitch_1
    if-ge v2, v11, :cond_a

    .line 96
    if-ge v0, v5, :cond_3

    .line 97
    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 98
    iput v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->setForSize:I

    goto :goto_1

    .line 101
    :cond_a
    if-ge v0, v5, :cond_b

    .line 102
    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 103
    iput v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;->setForSize:I

    goto :goto_1

    .line 105
    :cond_b
    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    goto :goto_1

    .line 71
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
