.class public Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;
.super Landroid/app/Activity;
.source "BinderActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;,
        Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$byNameFile;
    }
.end annotation


# instance fields
.field public bindes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;",
            ">;"
        }
    .end annotation
.end field

.field public context:Landroid/content/Context;

.field private current:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

.field private filebrowser:Landroid/widget/ListView;

.field public lv:Landroid/widget/ListView;

.field private myPath:Landroid/widget/TextView;

.field public pp4:Landroid/app/Dialog;

.field private root:Ljava/lang/String;

.field public sizeText:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 47
    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->pp4:Landroid/app/Dialog;

    .line 48
    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->bindes:Ljava/util/ArrayList;

    .line 49
    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->lv:Landroid/widget/ListView;

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->sizeText:I

    .line 738
    return-void
.end method

.method static synthetic access$000(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->filebrowser:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Landroid/widget/ListView;)Landroid/widget/ListView;
    .locals 0
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;
    .param p1, "x1"    # Landroid/widget/ListView;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->filebrowser:Landroid/widget/ListView;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Ljava/lang/String;Landroid/widget/ListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/widget/ListView;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->getDir(Ljava/lang/String;Landroid/widget/ListView;)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->myPath:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->myPath:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$300(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;)Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;
    .locals 1
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->current:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;)Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;
    .locals 0
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;
    .param p1, "x1"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->current:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->root:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->root:Ljava/lang/String;

    return-object p1
.end method

.method public static getBindes(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 574
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 575
    .local v8, "tempbindes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;>;"
    new-instance v0, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "binder"

    invoke-virtual {p0, v10, v11}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/bind.txt"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 578
    .local v0, "bindfile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_0

    .line 580
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 583
    :cond_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 584
    .local v4, "fis":Ljava/io/FileInputStream;
    new-instance v5, Ljava/io/InputStreamReader;

    const-string v9, "UTF-8"

    invoke-direct {v5, v4, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 585
    .local v5, "isr":Ljava/io/InputStreamReader;
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 586
    .local v1, "br":Ljava/io/BufferedReader;
    const-string v2, ""

    .line 588
    .local v2, "data":Ljava/lang/String;
    const-string v7, ""

    .line 589
    .local v7, "temp":Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 590
    move-object v7, v2

    .line 591
    const-string v9, ";"

    invoke-virtual {v7, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 592
    .local v6, "tails":[Ljava/lang/String;
    array-length v9, v6

    const/4 v10, 0x2

    if-ne v9, v10, :cond_1

    .line 593
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v11, 0x0

    aget-object v11, v6, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " ; "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x1

    aget-object v11, v6, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 594
    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;

    const/4 v10, 0x0

    aget-object v10, v6, v10

    const/4 v11, 0x1

    aget-object v11, v6, v11

    invoke-direct {v9, v10, v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 599
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v2    # "data":Ljava/lang/String;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v5    # "isr":Ljava/io/InputStreamReader;
    .end local v6    # "tails":[Ljava/lang/String;
    .end local v7    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 600
    .local v3, "e":Ljava/io/FileNotFoundException;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Not found bind.txt"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 605
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :goto_1
    return-object v8

    .line 598
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "data":Ljava/lang/String;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "isr":Ljava/io/InputStreamReader;
    .restart local v7    # "temp":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 601
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v2    # "data":Ljava/lang/String;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v5    # "isr":Ljava/io/InputStreamReader;
    .end local v7    # "temp":Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 602
    .local v3, "e":Ljava/io/IOException;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getDir(Ljava/lang/String;Landroid/widget/ListView;)V
    .locals 9
    .param p1, "dirPath"    # Ljava/lang/String;
    .param p2, "lv"    # Landroid/widget/ListView;

    .prologue
    .line 631
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->myPath:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const v8, 0x7f070038

    invoke-static {v8}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 632
    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    const-string v7, "/"

    invoke-direct {v6, p0, v7, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->current:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    .line 635
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 636
    .local v5, "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;>;"
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 637
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 638
    .local v3, "files":[Ljava/io/File;
    if-nez v3, :cond_0

    .line 641
    :cond_0
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->root:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 642
    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->root:Ljava/lang/String;

    invoke-direct {v6, p0, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 643
    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    const-string v7, "../"

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, p0, v7, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 647
    :cond_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v6, v3

    if-ge v4, v6, :cond_4

    .line 648
    aget-object v1, v3, v4

    .line 650
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 652
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, ".apk"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 653
    :cond_2
    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, p0, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 647
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 658
    .end local v1    # "file":Ljava/io/File;
    :cond_4
    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$3;

    const v6, 0x7f040027

    invoke-direct {v2, p0, p0, v6, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Landroid/content/Context;ILjava/util/List;)V

    .line 709
    .local v2, "fileList":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;>;"
    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$byNameFile;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$byNameFile;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;)V

    invoke-virtual {v2, v6}, Landroid/widget/ArrayAdapter;->sort(Ljava/util/Comparator;)V

    .line 711
    invoke-virtual {p2, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 712
    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 713
    return-void
.end method

.method public static savetoFile(Ljava/util/ArrayList;Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 609
    .local p0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;>;"
    new-instance v0, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "binder"

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/bind.txt"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 611
    .local v0, "bindfile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 613
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 616
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 617
    .local v4, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;

    .line 618
    .local v3, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ";"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 623
    .end local v3    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v1

    .line 625
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 628
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    return-void

    .line 620
    .restart local v4    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 621
    .local v2, "fos":Ljava/io/FileOutputStream;
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/io/FileOutputStream;->write([B)V

    .line 622
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    iput-object p0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->context:Landroid/content/Context;

    .line 62
    new-instance v0, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "binder"

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/bind.txt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 64
    .local v0, "bindfile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 67
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->getBindes(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->bindes:Ljava/util/ArrayList;

    .line 75
    const v4, 0x7f04000a

    invoke-virtual {p0, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->setContentView(I)V

    .line 76
    const v4, 0x7f0d003b

    invoke-virtual {p0, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 79
    .local v1, "button":Landroid/widget/Button;
    const v4, 0x7f0d003c

    invoke-virtual {p0, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->lv:Landroid/widget/ListView;

    .line 81
    new-instance v4, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;

    const v5, 0x7f040010

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->bindes:Ljava/util/ArrayList;

    invoke-direct {v4, p0, p0, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Landroid/content/Context;ILjava/util/List;)V

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adaptBind:Landroid/widget/ArrayAdapter;

    .line 214
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->lv:Landroid/widget/ListView;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adaptBind:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 215
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->lv:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->invalidateViews()V

    .line 217
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->lv:Landroid/widget/ListView;

    const/high16 v5, -0x1000000

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 221
    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;)V

    .line 558
    .local v2, "clickListener":Landroid/view/View$OnClickListener;
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 561
    return-void

    .line 68
    .end local v1    # "button":Landroid/widget/Button;
    .end local v2    # "clickListener":Landroid/view/View$OnClickListener;
    :catch_0
    move-exception v3

    .line 70
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 570
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 571
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 565
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 566
    return-void
.end method
