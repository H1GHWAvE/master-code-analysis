.class public Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;
.super Ljava/lang/Object;
.source "Progress_Dialog_Loading.java"


# static fields
.field public static dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;


# instance fields
.field context:Landroid/support/v4/app/FragmentActivity;

.field dialog2:Landroid/app/Dialog;

.field fm:Landroid/support/v4/app/FragmentManager;

.field message:Ljava/lang/String;

.field title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->message:Ljava/lang/String;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->title:Ljava/lang/String;

    .line 20
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->fm:Landroid/support/v4/app/FragmentManager;

    .line 21
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->context:Landroid/support/v4/app/FragmentActivity;

    .line 26
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    .line 24
    return-void
.end method

.method public static newInstance()Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;

    invoke-direct {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;-><init>()V

    .line 29
    .local v0, "f":Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;
    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    .line 193
    :cond_0
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 156
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateDialog()Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 44
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setIncrementStyle()Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 45
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0700ef

    invoke-static {v0}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->title:Ljava/lang/String;

    .line 46
    :cond_0
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 47
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->message:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f070233

    invoke-static {v0}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->message:Ljava/lang/String;

    .line 48
    :cond_1
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 49
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    const v1, 0x7f020033

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setIcon(I)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 50
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setCancelable(Z)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 52
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$1;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;)V

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 61
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->create()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public setCancelable(Z)V
    .locals 1
    .param p1, "trig"    # Z

    .prologue
    .line 186
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 187
    :cond_0
    return-void
.end method

.method public setIndeterminate(ZLandroid/app/Activity;)V
    .locals 3
    .param p1, "indeterminate"    # Z
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 82
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->onCreateDialog()Landroid/app/Dialog;

    .line 83
    :cond_0
    if-eqz p1, :cond_2

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 86
    :goto_0
    if-nez p1, :cond_3

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setIncrementStyle()Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 89
    :goto_1
    :try_start_0
    invoke-static {}, Lcom/chelpus/Utils;->onMainThread()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 90
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    :cond_1
    :goto_2
    return-void

    .line 84
    :cond_2
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    const-string v2, "%1d/%2d"

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setProgressNumberFormat(Ljava/lang/String;)V

    goto :goto_0

    .line 87
    :cond_3
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setDefaultStyle()Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    goto :goto_1

    .line 92
    :cond_4
    :try_start_1
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$3;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;)V

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public setMax(I)V
    .locals 3
    .param p1, "max"    # I

    .prologue
    .line 106
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->onCreateDialog()Landroid/app/Dialog;

    .line 107
    :cond_0
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v1, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setMax(I)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 109
    :try_start_0
    invoke-static {}, Lcom/chelpus/Utils;->onMainThread()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 110
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 120
    :cond_1
    :goto_0
    return-void

    .line 112
    :cond_2
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$4;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;)V

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 119
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->message:Ljava/lang/String;

    .line 65
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->onCreateDialog()Landroid/app/Dialog;

    .line 66
    :cond_0
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 68
    :try_start_0
    invoke-static {}, Lcom/chelpus/Utils;->onMainThread()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 69
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 80
    :cond_1
    :goto_0
    return-void

    .line 71
    :cond_2
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$2;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;)V

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 78
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setProgress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 138
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->onCreateDialog()Landroid/app/Dialog;

    .line 139
    :cond_0
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v1, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setProgress(I)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 141
    :try_start_0
    invoke-static {}, Lcom/chelpus/Utils;->onMainThread()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 142
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 152
    :cond_1
    :goto_0
    return-void

    .line 144
    :cond_2
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$6;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;)V

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 151
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setProgressNumberFormat(Ljava/lang/String;)V
    .locals 3
    .param p1, "format"    # Ljava/lang/String;

    .prologue
    .line 122
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->onCreateDialog()Landroid/app/Dialog;

    .line 123
    :cond_0
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v1, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 125
    :try_start_0
    invoke-static {}, Lcom/chelpus/Utils;->onMainThread()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 126
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 136
    :cond_1
    :goto_0
    return-void

    .line 128
    :cond_2
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$5;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;)V

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 135
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->title:Ljava/lang/String;

    .line 162
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->onCreateDialog()Landroid/app/Dialog;

    .line 163
    :cond_0
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-eqz v1, :cond_1

    .line 164
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 166
    :try_start_0
    invoke-static {}, Lcom/chelpus/Utils;->onMainThread()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 167
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v1, :cond_1

    .line 168
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 184
    :cond_1
    :goto_0
    return-void

    .line 170
    :cond_2
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$7;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;)V

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public showDialog()V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->onCreateDialog()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 37
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 39
    :cond_1
    return-void
.end method
