.class public Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;
.super Ljava/lang/Object;
.source "ProgressDlg.java"


# static fields
.field public static max:I


# instance fields
.field public context:Landroid/content/Context;

.field public dialog:Landroid/app/Dialog;

.field public format:Ljava/lang/String;

.field public incrementStyle:Z

.field public root:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    sput v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->max:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    .line 22
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->root:Landroid/view/View;

    .line 23
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->context:Landroid/content/Context;

    .line 24
    const-string v1, "%1d/%2d"

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->format:Ljava/lang/String;

    .line 26
    iput-boolean v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->incrementStyle:Z

    .line 29
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->context:Landroid/content/Context;

    .line 30
    new-instance v1, Landroid/app/Dialog;

    invoke-direct {v1, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    .line 31
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 32
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 33
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 109
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    const v2, 0x7f040036

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setContentView(I)V

    .line 110
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 111
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    const v2, 0x7f0d00dd

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 112
    .local v0, "progressInc":Landroid/widget/ProgressBar;
    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 113
    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 114
    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 116
    return-void
.end method


# virtual methods
.method public create()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    return-object v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    return v0
.end method

.method public setCancelable(Z)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;
    .locals 1
    .param p1, "cancel"    # Z

    .prologue
    .line 171
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    return-object p0
.end method

.method public setDefaultStyle()Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;
    .locals 3

    .prologue
    .line 193
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->incrementStyle:Z

    .line 195
    :try_start_0
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$7;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    :goto_0
    return-object p0

    .line 203
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setIcon(I)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 120
    :try_start_0
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$2;

    invoke-direct {v2, p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;I)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :goto_0
    return-object p0

    .line 131
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setIncrementStyle()Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;
    .locals 3

    .prologue
    .line 177
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->incrementStyle:Z

    .line 179
    :try_start_0
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$6;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :goto_0
    return-object p0

    .line 189
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setMax(I)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;
    .locals 4
    .param p1, "max"    # I

    .prologue
    .line 208
    :try_start_0
    sput p1, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->max:I

    .line 209
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    const v3, 0x7f0d00dd

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 210
    .local v1, "progressInc":Landroid/widget/ProgressBar;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 211
    sget v2, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->max:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 213
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->context:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$8;

    invoke-direct {v3, p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$8;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;I)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    .end local v1    # "progressInc":Landroid/widget/ProgressBar;
    :goto_0
    return-object p0

    .line 221
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 162
    :try_start_0
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$5;

    invoke-direct {v2, p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :goto_0
    return-object p0

    .line 169
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;
    .locals 1
    .param p1, "cancel_listner"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 175
    return-object p0
.end method

.method public setProgress(I)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 226
    :try_start_0
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$9;

    invoke-direct {v2, p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$9;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;I)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :goto_0
    return-object p0

    .line 235
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setProgressNumberFormat(Ljava/lang/String;)V
    .locals 3
    .param p1, "format1"    # Ljava/lang/String;

    .prologue
    .line 243
    :try_start_0
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->format:Ljava/lang/String;

    .line 244
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$10;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$10;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    :goto_0
    return-void

    .line 253
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setTitle(I)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;
    .locals 3
    .param p1, "str"    # I

    .prologue
    .line 150
    :try_start_0
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$4;

    invoke-direct {v2, p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;I)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :goto_0
    return-object p0

    .line 158
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 135
    :try_start_0
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$3;

    invoke-direct {v2, p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    :goto_0
    return-object p0

    .line 146
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
