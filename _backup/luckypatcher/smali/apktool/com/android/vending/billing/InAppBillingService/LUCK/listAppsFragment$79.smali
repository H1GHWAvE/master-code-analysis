.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$79;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->connectToLicensing()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 9610
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$79;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 9621
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Licensing service try to connect."

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 9622
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$79;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->mServiceL:Lcom/android/vending/licensing/ILicensingService;

    if-nez v1, :cond_0

    .line 9623
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$79;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-static {p2}, Lcom/android/vending/licensing/ILicensingService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/vending/licensing/ILicensingService;

    move-result-object v2

    iput-object v2, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->mServiceL:Lcom/android/vending/licensing/ILicensingService;

    .line 9625
    :try_start_0
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$79;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->mServiceL:Lcom/android/vending/licensing/ILicensingService;

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$79$1;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$79$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$79;)V

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/android/vending/licensing/ILicensingService;->checkLicense(JLjava/lang/String;Lcom/android/vending/licensing/ILicenseResultListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 9686
    :cond_0
    :goto_0
    return-void

    .line 9674
    :catch_0
    move-exception v0

    .line 9675
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 9613
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Licensing service disconnected."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 9614
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$79;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->mServiceL:Lcom/android/vending/licensing/ILicensingService;

    .line 9615
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 9616
    return-void
.end method
