.class public Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;
.super Landroid/appwidget/AppWidgetProvider;
.source "AppDisablerWidget.java"


# static fields
.field public static ACTION_WIDGET_RECEIVER:Ljava/lang/String;

.field public static ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "ActionReceiverWidgetAppDisabler"

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;->ACTION_WIDGET_RECEIVER:Ljava/lang/String;

    .line 27
    const-string v0, "ActionReceiverWidgetAppDisablerUpdate"

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method public static pushWidgetUpdate(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "remoteViews"    # Landroid/widget/RemoteViews;

    .prologue
    .line 29
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 30
    .local v1, "myWidget":Landroid/content/ComponentName;
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 32
    .local v0, "manager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v0, v1, p1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 33
    return-void
.end method

.method static updateAppWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p2, "appWidgetId"    # I

    .prologue
    .line 67
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->init()V

    .line 68
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;-><init>(Landroid/content/Context;ILandroid/appwidget/AppWidgetManager;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 137
    .local v0, "binder":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 138
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 139
    return-void
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    .line 46
    array-length v0, p2

    .line 47
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 48
    aget v2, p2, v1

    invoke-static {p1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->deleteTitlePref(Landroid/content/Context;I)V

    .line 47
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 51
    :cond_0
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 145
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 147
    .local v0, "action":Ljava/lang/String;
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;->ACTION_WIDGET_RECEIVER:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 148
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->init()V

    .line 149
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    .line 150
    .local v3, "handler":Landroid/os/Handler;
    new-instance v5, Ljava/lang/Thread;

    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;

    invoke-direct {v6, p0, p1, p2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;Landroid/content/Context;Landroid/content/Intent;Landroid/os/Handler;)V

    invoke-direct {v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 225
    .local v5, "t":Ljava/lang/Thread;
    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Ljava/lang/Thread;->setPriority(I)V

    .line 226
    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    .line 230
    .end local v3    # "handler":Landroid/os/Handler;
    .end local v5    # "t":Ljava/lang/Thread;
    :cond_0
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 232
    const/4 v6, 0x1

    :try_start_0
    sput-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->appDisabler:Z

    .line 233
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 234
    .local v2, "gm":Landroid/appwidget/AppWidgetManager;
    new-instance v6, Landroid/content/ComponentName;

    const-class v7, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;

    invoke-direct {v6, p1, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v6}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v4

    .line 235
    .local v4, "ids":[I
    invoke-virtual {p0, p1, v2, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    .end local v2    # "gm":Landroid/appwidget/AppWidgetManager;
    .end local v4    # "ids":[I
    :cond_1
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 241
    return-void

    .line 236
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 37
    array-length v0, p3

    .line 38
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 39
    aget v2, p3, v1

    invoke-static {p1, p2, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;->updateAppWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    .line 38
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 41
    :cond_0
    return-void
.end method
