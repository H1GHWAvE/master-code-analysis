.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->apkpermissions_safe(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 14141
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 27

    .prologue
    .line 14144
    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 14145
    .local v17, "pli":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    const/4 v10, 0x0

    .line 14147
    .local v10, "filesDir":Ljava/lang/String;
    const/4 v7, 0x0

    .line 14149
    .local v7, "error":Z
    :try_start_0
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v22

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v22

    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v10, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 14150
    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    move/from16 v22, v0

    if-eqz v22, :cond_0

    const-string v22, "/system"

    const-string v23, "rw"

    invoke-static/range {v22 .. v23}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 14159
    :cond_0
    :goto_0
    if-nez v7, :cond_d

    .line 14160
    new-instance v22, Ljava/io/File;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "/Modified"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->exists()Z

    move-result v22

    if-nez v22, :cond_1

    .line 14161
    new-instance v22, Ljava/io/File;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "/Modified"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->mkdirs()Z

    .line 14162
    :cond_1
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "/Modified/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    new-instance v23, Ljava/io/File;

    move-object/from16 v0, v23

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 14163
    .local v4, "destFile":Ljava/lang/String;
    new-instance v22, Ljava/io/File;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "/Modified"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->exists()Z

    move-result v22

    if-nez v22, :cond_2

    .line 14164
    new-instance v22, Ljava/io/File;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "/Modified"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->mkdirs()Z

    .line 14166
    :cond_2
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "chmod 777 "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 14168
    new-instance v22, Ljava/io/File;

    move-object/from16 v0, v22

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->exists()Z

    move-result v22

    if-eqz v22, :cond_c

    .line 14169
    sget-object v22, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "LuckyPatcher (CopyPackageApk):"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 14171
    const/4 v9, 0x0

    .line 14173
    .local v9, "extrFile":Ljava/lang/String;
    :try_start_1
    new-instance v12, Ljava/io/FileInputStream;

    invoke-direct {v12, v10}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 14174
    .local v12, "fin":Ljava/io/FileInputStream;
    new-instance v20, Ljava/util/zip/ZipInputStream;

    move-object/from16 v0, v20

    invoke-direct {v0, v12}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 14175
    .local v20, "zin":Ljava/util/zip/ZipInputStream;
    const/16 v19, 0x0

    .line 14176
    .local v19, "ze":Ljava/util/zip/ZipEntry;
    :cond_3
    :goto_1
    invoke-virtual/range {v20 .. v20}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v19

    if-eqz v19, :cond_5

    .line 14181
    invoke-virtual/range {v19 .. v19}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v22

    const-string v23, "AndroidManifest.xml"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 14182
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "/Modified/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "AndroidManifest.xml"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 14183
    new-instance v13, Ljava/io/FileOutputStream;

    invoke-direct {v13, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 14185
    .local v13, "fout":Ljava/io/FileOutputStream;
    const/16 v22, 0x2000

    move/from16 v0, v22

    new-array v3, v0, [B

    .line 14187
    .local v3, "buffer":[B
    :goto_2
    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v14

    .local v14, "length":I
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v14, v0, :cond_4

    .line 14188
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v13, v3, v0, v14}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 14198
    .end local v3    # "buffer":[B
    .end local v12    # "fin":Ljava/io/FileInputStream;
    .end local v13    # "fout":Ljava/io/FileOutputStream;
    .end local v14    # "length":I
    .end local v19    # "ze":Ljava/util/zip/ZipEntry;
    .end local v20    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v5

    .line 14200
    .local v5, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v21, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v21

    invoke-direct {v0, v10}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 14204
    .local v21, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "/Modified/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "AndroidManifest.xml"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 14205
    const-string v22, "AndroidManifest.xml"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "/Modified/"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 14216
    .end local v21    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_3
    sget-object v22, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Decompress unzip "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 14218
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_4
    new-instance v22, Ljava/io/File;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "/Modified/AndroidManifest.xml"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->exists()Z

    move-result v22

    if-nez v22, :cond_6

    .line 14220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v22, v0

    new-instance v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$1;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;)V

    invoke-virtual/range {v22 .. v23}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 14392
    .end local v4    # "destFile":Ljava/lang/String;
    .end local v9    # "extrFile":Ljava/lang/String;
    :goto_5
    return-void

    .line 14151
    :catch_1
    move-exception v6

    .line 14153
    .local v6, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 14154
    const/4 v7, 0x1

    .line 14158
    goto/16 :goto_0

    .line 14155
    .end local v6    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_2
    move-exception v5

    .line 14156
    .restart local v5    # "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 14157
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 14190
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v3    # "buffer":[B
    .restart local v4    # "destFile":Ljava/lang/String;
    .restart local v9    # "extrFile":Ljava/lang/String;
    .restart local v12    # "fin":Ljava/io/FileInputStream;
    .restart local v13    # "fout":Ljava/io/FileOutputStream;
    .restart local v14    # "length":I
    .restart local v19    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v20    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_4
    :try_start_3
    invoke-virtual/range {v20 .. v20}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 14191
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_1

    .line 14196
    .end local v3    # "buffer":[B
    .end local v13    # "fout":Ljava/io/FileOutputStream;
    .end local v14    # "length":I
    :cond_5
    invoke-virtual/range {v20 .. v20}, Ljava/util/zip/ZipInputStream;->close()V

    .line 14197
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_4

    .line 14208
    .end local v12    # "fin":Ljava/io/FileInputStream;
    .end local v19    # "ze":Ljava/util/zip/ZipEntry;
    .end local v20    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v5    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v6

    .line 14209
    .local v6, "e1":Lnet/lingala/zip4j/exception/ZipException;
    sget-object v22, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Error classes.dex decompress! "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 14210
    sget-object v22, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Exception e1"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 14211
    .end local v6    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_4
    move-exception v6

    .line 14212
    .local v6, "e1":Ljava/lang/Exception;
    sget-object v22, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Error classes.dex decompress! "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 14213
    sget-object v22, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Exception e1"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 14231
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v6    # "e1":Ljava/lang/Exception;
    :cond_6
    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;

    invoke-direct {v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;-><init>()V

    .line 14233
    .local v8, "ex":Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;
    new-instance v15, Ljava/io/File;

    invoke-direct {v15, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 14236
    .local v15, "manifestFile":Ljava/io/File;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v22, v0

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->permissions:Ljava/util/ArrayList;
    invoke-static/range {v22 .. v22}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->access$2200(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)Ljava/util/ArrayList;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->activities:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->access$2300(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v8, v15, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->disablePermisson(Ljava/io/File;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6

    .line 14266
    :goto_6
    :try_start_5
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 14267
    .local v2, "add_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    new-instance v22, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "/Modified/AndroidManifest.xml"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v25, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "/Modified/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v22 .. v24}, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 14268
    invoke-static {v10, v4, v2}, Lcom/chelpus/Utils;->addFilesToZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    .line 14299
    .end local v2    # "add_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    :goto_7
    new-instance v22, Ljava/io/File;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "/Modified/AndroidManifest.xml"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->exists()Z

    move-result v22

    if-eqz v22, :cond_7

    .line 14300
    new-instance v22, Ljava/io/File;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "/Modified/AndroidManifest.xml"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->delete()Z

    .line 14303
    :cond_7
    new-instance v22, Ljava/io/File;

    move-object/from16 v0, v22

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->exists()Z

    move-result v22

    if-eqz v22, :cond_a

    .line 14307
    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    move/from16 v22, v0

    if-eqz v22, :cond_8

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    move/from16 v22, v0

    if-eqz v22, :cond_b

    const-string v22, "/system/"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_b

    .line 14309
    :cond_8
    :try_start_6
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v22

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v22

    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 14310
    .local v18, "srcDir":Ljava/lang/String;
    const-string v22, "/mnt/asec/"

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 14311
    const-string v22, "/pkg.apk"

    const-string v23, ""

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v22

    const-string v23, "rw"

    invoke-static/range {v22 .. v23}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 14312
    :cond_9
    new-instance v22, Lcom/chelpus/Utils;

    const-string v23, ""

    invoke-direct/range {v22 .. v23}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "rm "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const/16 v26, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-static {v0, v1}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-virtual/range {v22 .. v23}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;
    :try_end_6
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_6} :catch_a
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_b

    .line 14321
    .end local v18    # "srcDir":Ljava/lang/String;
    :goto_8
    new-instance v22, Lcom/chelpus/Utils;

    const-string v23, ""

    invoke-direct/range {v22 .. v23}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "pm install -r -i com.android.vending \'"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "\'"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-virtual/range {v22 .. v23}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    .line 14372
    .end local v4    # "destFile":Ljava/lang/String;
    .end local v8    # "ex":Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;
    .end local v9    # "extrFile":Ljava/lang/String;
    .end local v15    # "manifestFile":Ljava/io/File;
    :cond_a
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v22, v0

    new-instance v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$6;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;)V

    invoke-virtual/range {v22 .. v23}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_5

    .line 14237
    .restart local v4    # "destFile":Ljava/lang/String;
    .restart local v8    # "ex":Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;
    .restart local v9    # "extrFile":Ljava/lang/String;
    .restart local v15    # "manifestFile":Ljava/io/File;
    :catch_5
    move-exception v5

    .line 14239
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_6

    .line 14240
    .end local v5    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v5

    .line 14241
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 14243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v22, v0

    new-instance v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$2;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;)V

    invoke-virtual/range {v22 .. v23}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_5

    .line 14270
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_7
    move-exception v6

    .line 14272
    .restart local v6    # "e1":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 14275
    :try_start_7
    new-instance v21, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 14280
    .restart local v21    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 14281
    .local v11, "filesToAdd":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    new-instance v22, Ljava/io/File;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "/Modified/AndroidManifest.xml"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 14283
    new-instance v16, Lnet/lingala/zip4j/model/ZipParameters;

    invoke-direct/range {v16 .. v16}, Lnet/lingala/zip4j/model/ZipParameters;-><init>()V

    .line 14284
    .local v16, "parameters":Lnet/lingala/zip4j/model/ZipParameters;
    const/16 v22, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lnet/lingala/zip4j/model/ZipParameters;->setCompressionMethod(I)V

    .line 14285
    const/16 v22, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lnet/lingala/zip4j/model/ZipParameters;->setEncryptFiles(Z)V

    .line 14286
    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v11, v1}, Lnet/lingala/zip4j/core/ZipFile;->addFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;)V
    :try_end_7
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_7 .. :try_end_7} :catch_8
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_9

    goto/16 :goto_7

    .line 14288
    .end local v11    # "filesToAdd":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v16    # "parameters":Lnet/lingala/zip4j/model/ZipParameters;
    .end local v21    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :catch_8
    move-exception v5

    .line 14289
    .local v5, "e":Lnet/lingala/zip4j/exception/ZipException;
    invoke-virtual {v5}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 14290
    sget-object v22, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Error classes.dex compress! "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 14291
    .end local v5    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :catch_9
    move-exception v5

    .line 14292
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 14293
    sget-object v22, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Error classes.dex compress! "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 14313
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v6    # "e1":Ljava/lang/Exception;
    :catch_a
    move-exception v6

    .line 14315
    .local v6, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 14316
    const/4 v7, 0x1

    .line 14320
    goto/16 :goto_8

    .line 14317
    .end local v6    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_b
    move-exception v5

    .line 14318
    .restart local v5    # "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 14319
    const/4 v7, 0x1

    goto/16 :goto_8

    .line 14323
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_b
    const/16 v22, 0x1

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-static {v4, v10, v0, v1}, Lcom/chelpus/Utils;->copyFile(Ljava/lang/String;Ljava/lang/String;ZZ)Z

    .line 14324
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "chmod 0644 "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 14325
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "chown 0:0 "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 14326
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "chown 0.0 "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 14328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v22, v0

    new-instance v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$3;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;)V

    invoke-virtual/range {v22 .. v23}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_9

    .line 14351
    .end local v8    # "ex":Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;
    .end local v9    # "extrFile":Ljava/lang/String;
    .end local v15    # "manifestFile":Ljava/io/File;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v22, v0

    new-instance v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$4;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;)V

    invoke-virtual/range {v22 .. v23}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_9

    .line 14362
    .end local v4    # "destFile":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v22, v0

    new-instance v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$5;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;)V

    invoke-virtual/range {v22 .. v23}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_9
.end method
