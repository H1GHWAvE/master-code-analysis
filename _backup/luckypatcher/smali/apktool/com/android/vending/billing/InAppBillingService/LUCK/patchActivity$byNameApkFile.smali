.class final Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$byNameApkFile;
.super Ljava/lang/Object;
.source "patchActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "byNameApkFile"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    .prologue
    .line 1587
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$byNameApkFile;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;)I
    .locals 2
    .param p1, "a"    # Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;
    .param p2, "b"    # Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;

    .prologue
    .line 1589
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1590
    :cond_0
    new-instance v0, Ljava/lang/ClassCastException;

    invoke-direct {v0}, Ljava/lang/ClassCastException;-><init>()V

    throw v0

    .line 1592
    :cond_1
    iget-object v0, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->pkgName:Ljava/lang/String;

    iget-object v1, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1593
    iget v0, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->versionCode:I

    iget v1, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->versionCode:I

    if-le v0, v1, :cond_2

    const/4 v0, 0x1

    .line 1597
    :goto_0
    return v0

    .line 1594
    :cond_2
    iget v0, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->versionCode:I

    iget v1, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->versionCode:I

    if-ge v0, v1, :cond_3

    const/4 v0, -0x1

    goto :goto_0

    .line 1595
    :cond_3
    iget v0, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->versionCode:I

    iget v1, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->versionCode:I

    if-ne v0, v1, :cond_4

    const/4 v0, 0x0

    goto :goto_0

    .line 1597
    :cond_4
    iget-object v0, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->name:Ljava/lang/String;

    iget-object v1, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1587
    check-cast p1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;

    check-cast p2, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;

    invoke-virtual {p0, p1, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$byNameApkFile;->compare(Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;)I

    move-result v0

    return v0
.end method
