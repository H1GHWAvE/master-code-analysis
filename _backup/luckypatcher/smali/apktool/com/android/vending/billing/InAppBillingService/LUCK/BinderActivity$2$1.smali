.class Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;
.super Ljava/lang/Object;
.source "BinderActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

.field final synthetic val$ed1:Landroid/widget/EditText;

.field final synthetic val$ed2:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;Landroid/widget/EditText;Landroid/widget/EditText;)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    .prologue
    .line 235
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;->val$ed1:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;->val$ed2:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 238
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;->val$ed1:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 239
    .local v0, "datadir":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;->val$ed2:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 240
    .local v5, "targetdir":Ljava/lang/String;
    const-string v6, "/"

    invoke-virtual {v0, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 242
    :goto_0
    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 244
    :goto_1
    new-instance v6, Ljava/io/File;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "/"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 245
    :cond_0
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    invoke-virtual {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->getApplication()Landroid/app/Application;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f070033

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 273
    :cond_1
    :goto_2
    return-void

    .line 241
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 243
    :cond_3
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 247
    :cond_4
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v6, :cond_6

    .line 248
    move-object v3, v0

    .line 249
    .local v3, "send_datadir":Ljava/lang/String;
    move-object v4, v5

    .line 250
    .local v4, "send_targetdir":Ljava/lang/String;
    new-instance v6, Ljava/lang/Thread;

    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1$1;

    invoke-direct {v7, p0, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v6, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 254
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 259
    .end local v3    # "send_datadir":Ljava/lang/String;
    .end local v4    # "send_targetdir":Ljava/lang/String;
    :goto_3
    const/4 v1, 0x0

    .line 260
    .local v1, "found":Z
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->bindes:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;

    .line 261
    .local v2, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    iget-object v7, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 262
    iput-object v0, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    .line 263
    iput-object v5, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    .line 264
    const/4 v1, 0x1

    goto :goto_4

    .line 256
    .end local v1    # "found":Z
    .end local v2    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    :cond_6
    new-array v6, v9, [Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "umount \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v6}, Lcom/chelpus/Utils;->cmd([Ljava/lang/String;)Ljava/lang/String;

    .line 257
    new-array v6, v9, [Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mount -o bind \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\' \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v6}, Lcom/chelpus/Utils;->cmd([Ljava/lang/String;)Ljava/lang/String;

    goto :goto_3

    .line 267
    .restart local v1    # "found":Z
    :cond_7
    if-nez v1, :cond_8

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->bindes:Ljava/util/ArrayList;

    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;

    invoke-direct {v7, v0, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    :cond_8
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->bindes:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->context:Landroid/content/Context;

    invoke-static {v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->savetoFile(Ljava/util/ArrayList;Landroid/content/Context;)V

    .line 269
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adaptBind:Landroid/widget/ArrayAdapter;

    invoke-virtual {v6}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 270
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->lv:Landroid/widget/ListView;

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adaptBind:Landroid/widget/ArrayAdapter;

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 271
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->pp4:Landroid/app/Dialog;

    invoke-virtual {v6}, Landroid/app/Dialog;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->pp4:Landroid/app/Dialog;

    invoke-virtual {v6}, Landroid/app/Dialog;->dismiss()V

    goto/16 :goto_2
.end method
