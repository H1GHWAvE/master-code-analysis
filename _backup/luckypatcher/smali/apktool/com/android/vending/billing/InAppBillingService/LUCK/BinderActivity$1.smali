.class Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;
.super Landroid/widget/ArrayAdapter;
.source "BinderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I

    .prologue
    .line 81
    .local p4, "x2":Ljava/util/List;, "Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;>;"
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 15
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 84
    move-object/from16 v7, p2

    .line 87
    .local v7, "row":Landroid/view/View;
    if-nez v7, :cond_0

    .line 89
    iget-object v12, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    iget-object v12, v12, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->context:Landroid/content/Context;

    check-cast v12, Landroid/app/Activity;

    invoke-virtual {v12}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    .line 90
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v12, 0x7f040010

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v4, v12, v0, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 101
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v12, 0x7f0d004b

    invoke-virtual {v7, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 102
    .local v9, "textView":Landroid/widget/TextView;
    const v12, 0x7f0d004c

    invoke-virtual {v7, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 104
    .local v10, "textView2":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v13

    invoke-virtual {v9, v12, v13}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 105
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v13

    invoke-virtual {v10, v12, v13}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 106
    const v12, 0x7f0d004d

    invoke-virtual {v7, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 107
    .local v1, "button_del":Landroid/widget/Button;
    const v12, 0x7f0d002b

    invoke-virtual {v7, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ToggleButton;

    .line 108
    .local v11, "togglebtn":Landroid/widget/ToggleButton;
    const v12, 0x7f0d0033

    invoke-virtual {v7, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ProgressBar;

    .line 110
    .local v6, "progress":Landroid/widget/ProgressBar;
    move/from16 v5, p1

    .line 111
    .local v5, "pos1":I
    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1$1;

    invoke-direct {v2, p0, v11, v6, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;Landroid/widget/ToggleButton;Landroid/widget/ProgressBar;I)V

    .line 157
    .local v2, "clickListener2":Landroid/view/View$OnClickListener;
    invoke-virtual {v11, v2}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    invoke-virtual/range {p0 .. p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;->getItem(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;

    invoke-static {v12}, Lcom/chelpus/Utils;->checkBind(Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;)Z

    move-result v12

    invoke-virtual {v11, v12}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 159
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1$2;

    invoke-direct {v3, p0, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;I)V

    .line 195
    .local v3, "clickListener3":Landroid/view/View$OnClickListener;
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    iget-object v12, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    iget-object v12, v12, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->context:Landroid/content/Context;

    iget-object v13, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    iget v13, v13, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->sizeText:I

    invoke-virtual {v9, v12, v13}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 198
    iget-object v12, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    iget-object v12, v12, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->context:Landroid/content/Context;

    iget-object v13, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    iget v13, v13, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->sizeText:I

    invoke-virtual {v10, v12, v13}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 199
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const v13, 0x7f070034

    invoke-static {v13}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 200
    .local v8, "str2":Ljava/lang/String;
    const-string v12, "#ff00ff00"

    const-string v13, "bold"

    invoke-static {v8, v12, v13}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v12

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    invoke-virtual/range {p0 .. p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;->getItem(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;

    iget-object v8, v12, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    .line 202
    const-string v12, "#ffffffff"

    const-string v13, "italic"

    invoke-static {v8, v12, v13}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v12

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 204
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const v13, 0x7f070035

    invoke-static {v13}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 205
    const-string v12, "#ffffff00"

    const-string v13, "bold"

    invoke-static {v8, v12, v13}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v12

    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    invoke-virtual/range {p0 .. p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;->getItem(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;

    iget-object v8, v12, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    .line 207
    const-string v12, "~chelpus_disabled~"

    const-string v13, ""

    invoke-virtual {v8, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "#ffffffff"

    const-string v14, "italic"

    invoke-static {v12, v13, v14}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v12

    invoke-virtual {v10, v12}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 211
    return-object v7
.end method
