.class Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$3;
.super Ljava/lang/Object;
.source "PackageChangeReceiver.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->connectToLicensing()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    .prologue
    .line 683
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 693
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Licensing service try to connect."

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 694
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 696
    sget-boolean v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v1, :cond_1

    .line 697
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$3$1;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$3$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$3;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 702
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 741
    :goto_0
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Firmware not support lvl emulation"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 744
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->mServiceConnL:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 746
    :goto_1
    return-void

    .line 705
    :cond_1
    const v1, 0x7f070234

    invoke-static {v1}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f07025b

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$3$2;

    invoke-direct {v3, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$3$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$3;)V

    new-instance v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$3$3;

    invoke-direct {v4, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$3$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$3;)V

    invoke-static {v1, v2, v3, v4}, Lcom/chelpus/Utils;->showSystemWindow(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 737
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v1

    new-instance v2, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/google/android/finsky/services/LicensingService;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto :goto_0

    .line 745
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 686
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Licensing service disconnected."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 687
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->mServiceL:Lcom/android/vending/licensing/ILicensingService;

    .line 688
    return-void
.end method
