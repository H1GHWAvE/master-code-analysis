.class Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$29;
.super Ljava/lang/Object;
.source "patchActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->show_file_explorer(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    .prologue
    .line 1476
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$29;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "l":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const v5, 0x7f070245

    const/4 v6, 0x7

    .line 1481
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v3

    invoke-interface {v3, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;

    .line 1482
    .local v1, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;
    new-instance v0, Ljava/io/File;

    iget-object v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;->full:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1483
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1484
    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1485
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object v3, p1

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->filebrowser:Landroid/widget/ListView;

    .line 1487
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v4, Ljava/io/File;

    iget-object v5, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;->full:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    check-cast p1, Landroid/widget/ListView;

    .end local p1    # "l":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v5, 0x0

    invoke-virtual {v3, v4, p1, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getDir(Ljava/lang/String;Landroid/widget/ListView;Z)V

    .line 1568
    :cond_0
    :goto_0
    return-void

    .line 1489
    .restart local p1    # "l":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    :cond_1
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    const v4, 0x108009b

    .line 1490
    invoke-virtual {v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setIcon(I)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1491
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] folder can\'t be read!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v3

    const-string v4, "OK"

    const/4 v5, 0x0

    .line 1492
    invoke-virtual {v3, v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v3

    .line 1489
    invoke-static {v3}, Lcom/chelpus/Utils;->showDialog(Landroid/app/Dialog;)V

    goto :goto_0

    .line 1495
    :cond_2
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iput-object v1, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->current:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;

    .line 1496
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->current:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;->full:Ljava/lang/String;

    sput-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;

    .line 1517
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1518
    .local v2, "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;->file:Ljava/lang/String;

    const-string v4, "core.jar"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;->file:Ljava/lang/String;

    const-string v4, "core.odex"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;->file:Ljava/lang/String;

    const-string v4, "services.jar"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;->file:Ljava/lang/String;

    const-string v4, "services.odex"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;->file:Ljava/lang/String;

    const-string v4, "core-libart.jar"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;->file:Ljava/lang/String;

    const-string v4, "boot.oat"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1519
    :cond_3
    iget-object v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;->file:Ljava/lang/String;

    const-string v4, ".jar"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1520
    new-instance v3, Ljava/io/File;

    iget-object v4, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;->full:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1521
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1529
    :cond_4
    :goto_1
    iget-object v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$ItemFile;->file:Ljava/lang/String;

    const-string v4, ".apk"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1530
    const v3, 0x7f07005b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1531
    const v3, 0x7f070060

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1532
    sget-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-nez v3, :cond_5

    const v3, 0x7f07006a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1533
    :cond_5
    sget-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v3, :cond_6

    const v3, 0x7f07005c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1534
    :cond_6
    const v3, 0x7f0700e0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1535
    const v3, 0x7f0701e8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1537
    :cond_7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 1538
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$29$1;

    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f04001d

    invoke-direct {v3, p0, v4, v5, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$29$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$29;Landroid/content/Context;ILjava/util/List;)V

    sput-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    .line 1563
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-static {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 1564
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-static {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showDialogLP(I)V

    goto/16 :goto_0

    .line 1523
    :cond_8
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const v4, 0x7f070234

    invoke-static {v4}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f07024b

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1526
    :cond_9
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method
