.class public Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;
.super Landroid/app/Activity;
.source "AppDisablerWidgetConfigureActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$byPkgName;
    }
.end annotation


# static fields
.field private static final PREFS_NAME:Ljava/lang/String; = "com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget"

.field private static final PREF_PREFIX_KEY:Ljava/lang/String; = "appwidget_"


# instance fields
.field public context:Landroid/content/Context;

.field public lv:Landroid/widget/ListView;

.field mAppWidgetId:I

.field mAppWidgetText:Landroid/widget/EditText;

.field public packages:[Ljava/lang/String;

.field public sizeText:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 38
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->mAppWidgetId:I

    .line 43
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->packages:[Ljava/lang/String;

    .line 44
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->lv:Landroid/widget/ListView;

    .line 45
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->sizeText:I

    .line 50
    return-void
.end method

.method static deleteTitlePref(Landroid/content/Context;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 250
    const-string v1, "com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 251
    .local v0, "prefs":Landroid/content/SharedPreferences$Editor;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "appwidget_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 252
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 253
    return-void
.end method

.method static loadTitlePref(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 240
    const-string v2, "com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget"

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 241
    .local v0, "prefs":Landroid/content/SharedPreferences;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "appwidget_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 242
    .local v1, "titleValue":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 245
    .end local v1    # "titleValue":Ljava/lang/String;
    :goto_0
    return-object v1

    .restart local v1    # "titleValue":Ljava/lang/String;
    :cond_0
    const-string v1, "NOT_SAVED_APP_DISABLER"

    goto :goto_0
.end method

.method static saveTitlePref(Landroid/content/Context;ILjava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 232
    const-string v1, "com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 233
    .local v0, "prefs":Landroid/content/SharedPreferences$Editor;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "appwidget_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 234
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 235
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 60
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {p0, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->setResult(I)V

    .line 68
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 69
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 70
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 71
    const-string v7, "appWidgetId"

    invoke-virtual {v1, v7, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->mAppWidgetId:I

    .line 76
    :cond_0
    iget v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->mAppWidgetId:I

    if-nez v7, :cond_1

    .line 77
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->finish()V

    .line 226
    :goto_0
    return-void

    .line 83
    :cond_1
    iput-object p0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->context:Landroid/content/Context;

    .line 85
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPackages()[Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->packages:[Ljava/lang/String;

    .line 86
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .local v5, "tmp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;>;"
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->packages:[Ljava/lang/String;

    array-length v8, v7

    :goto_1
    if-ge v6, v8, :cond_4

    aget-object v3, v7, v6

    .line 91
    .local v3, "p":Ljava/lang/String;
    const-string v9, "android"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->context:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 93
    :cond_2
    :try_start_0
    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v3, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v10

    iget-object v10, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v3, v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :cond_3
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 94
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 99
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v3    # "p":Ljava/lang/String;
    :cond_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v4, v6, [Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;

    .line 100
    .local v4, "pkgs":[Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 102
    :try_start_1
    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$byPkgName;

    invoke-direct {v6, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$byPkgName;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;)V

    invoke-static {v4, v6}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 107
    :goto_3
    new-instance v6, Landroid/widget/ListView;

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->context:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->lv:Landroid/widget/ListView;

    .line 111
    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$1;

    const v7, 0x7f04002e

    invoke-direct {v6, p0, p0, v7, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;Landroid/content/Context;I[Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;)V

    sput-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    .line 196
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->lv:Landroid/widget/ListView;

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 197
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->lv:Landroid/widget/ListView;

    invoke-virtual {v6}, Landroid/widget/ListView;->invalidateViews()V

    .line 200
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->lv:Landroid/widget/ListView;

    const/high16 v7, -0x1000000

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 202
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->lv:Landroid/widget/ListView;

    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$2;

    invoke-direct {v7, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;)V

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 224
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->lv:Landroid/widget/ListView;

    invoke-virtual {p0, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->setContentView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 104
    :catch_1
    move-exception v0

    .line 105
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method
