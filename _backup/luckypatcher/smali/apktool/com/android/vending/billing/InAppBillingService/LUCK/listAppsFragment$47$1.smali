.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47$1;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47;

.field final synthetic val$data_dir:Ljava/lang/String;

.field final synthetic val$destFolder:Ljava/io/File;

.field final synthetic val$srcFolder:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47;Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47;

    .prologue
    .line 6413
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47;

    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47$1;->val$srcFolder:Ljava/io/File;

    iput-object p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47$1;->val$destFolder:Ljava/io/File;

    iput-object p4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47$1;->val$data_dir:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const v6, 0x7f070234

    .line 6417
    :try_start_0
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47$1;->val$srcFolder:Ljava/io/File;

    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47$1;->val$destFolder:Ljava/io/File;

    invoke-static {v2, v3}, Lcom/chelpus/Utils;->copyFolder(Ljava/io/File;Ljava/io/File;)V

    .line 6418
    new-instance v1, Lcom/chelpus/Utils;

    const-string v2, ""

    invoke-direct {v1, v2}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    .line 6419
    .local v1, "utils":Lcom/chelpus/Utils;
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47$1;->val$srcFolder:Ljava/io/File;

    invoke-virtual {v1, v2}, Lcom/chelpus/Utils;->deleteFolder(Ljava/io/File;)V

    .line 6421
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "basepath"

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47$1;->val$data_dir:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 6422
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47$1;->val$data_dir:Ljava/lang/String;

    sput-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    .line 6423
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const v3, 0x7f070234

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f070171

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47$1;->val$data_dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMessage(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6430
    .end local v1    # "utils":Lcom/chelpus/Utils;
    :goto_0
    return-void

    .line 6425
    :catch_0
    move-exception v0

    .line 6426
    .local v0, "e2":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 6427
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$47;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f070181

    invoke-static {v4}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
