.class final Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;
.super Ljava/lang/Object;
.source "AppDisablerWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;->updateAppWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$appWidgetId:I

.field final synthetic val$appWidgetManager:Landroid/appwidget/AppWidgetManager;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;ILandroid/appwidget/AppWidgetManager;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$context:Landroid/content/Context;

    iput p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$appWidgetId:I

    iput-object p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$appWidgetManager:Landroid/appwidget/AppWidgetManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const v14, 0x7f040006

    const v13, 0x7f020050

    const v12, 0x7f0d002b

    const/4 v11, 0x0

    const v10, 0x7f0d002a

    .line 71
    sget-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v7, :cond_1

    .line 72
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$context:Landroid/content/Context;

    iget v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$appWidgetId:I

    invoke-static {v7, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->loadTitlePref(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    .line 76
    .local v4, "package_name":Ljava/lang/String;
    :try_start_0
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 79
    .local v5, "pm":Landroid/content/pm/PackageManager;
    new-instance v6, Landroid/widget/RemoteViews;

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f040006

    invoke-direct {v6, v7, v8}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 81
    .local v6, "views":Landroid/widget/RemoteViews;
    const v7, 0x7f0d002a

    const/4 v8, 0x0

    invoke-virtual {v5, v4, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 83
    const/4 v7, 0x0

    invoke-virtual {v5, v4, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    iget-object v7, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v7, v7, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v7, :cond_0

    .line 84
    const v7, 0x7f0d002a

    const-string v8, "#00FF00"

    invoke-static {v8}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 85
    const v7, 0x7f0d002b

    const-string v8, "setBackgroundResource"

    const v9, 0x7f020051

    invoke-virtual {v6, v7, v8, v9}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 91
    :goto_0
    new-instance v1, Landroid/content/Intent;

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$context:Landroid/content/Context;

    const-class v8, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;

    invoke-direct {v1, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 92
    .local v1, "active":Landroid/content/Intent;
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;->ACTION_WIDGET_RECEIVER:Ljava/lang/String;

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    const-string v7, "appWidgetId"

    iget v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$appWidgetId:I

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 94
    const-string v7, "msg"

    invoke-virtual {v1, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$context:Landroid/content/Context;

    iget v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$appWidgetId:I

    const/4 v9, 0x0

    invoke-static {v7, v8, v1, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 99
    .local v0, "actionPendingIntent":Landroid/app/PendingIntent;
    const v7, 0x7f0d002c

    invoke-virtual {v6, v7, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :try_start_1
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$appWidgetManager:Landroid/appwidget/AppWidgetManager;

    iget v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$appWidgetId:I

    invoke-virtual {v7, v8, v6}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 135
    .end local v4    # "package_name":Ljava/lang/String;
    .end local v5    # "pm":Landroid/content/pm/PackageManager;
    :goto_1
    return-void

    .line 87
    .end local v0    # "actionPendingIntent":Landroid/app/PendingIntent;
    .end local v1    # "active":Landroid/content/Intent;
    .restart local v4    # "package_name":Ljava/lang/String;
    .restart local v5    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    const v7, 0x7f0d002a

    :try_start_2
    const-string v8, "#FF0000"

    invoke-static {v8}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 88
    const v7, 0x7f0d002b

    const-string v8, "setBackgroundResource"

    const v9, 0x7f020050

    invoke-virtual {v6, v7, v8, v9}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 106
    .end local v5    # "pm":Landroid/content/pm/PackageManager;
    .end local v6    # "views":Landroid/widget/RemoteViews;
    :catch_0
    move-exception v2

    .line 108
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v6, Landroid/widget/RemoteViews;

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v14}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 109
    .restart local v6    # "views":Landroid/widget/RemoteViews;
    new-instance v1, Landroid/content/Intent;

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$context:Landroid/content/Context;

    const-class v8, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;

    invoke-direct {v1, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    .restart local v1    # "active":Landroid/content/Intent;
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$context:Landroid/content/Context;

    iget v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$appWidgetId:I

    invoke-static {v7, v8, v1, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 111
    .restart local v0    # "actionPendingIntent":Landroid/app/PendingIntent;
    const v7, 0x7f0d002c

    invoke-virtual {v6, v7, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 112
    const-string v7, "setBackgroundResource"

    invoke-virtual {v6, v12, v7, v13}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 113
    const-string v7, "#AAAAAA"

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v10, v7}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 115
    const v7, 0x7f07023c

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v10, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 117
    :try_start_3
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$appWidgetManager:Landroid/appwidget/AppWidgetManager;

    iget v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$appWidgetId:I

    invoke-virtual {v7, v8, v6}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 118
    :catch_1
    move-exception v3

    .local v3, "e1":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 105
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v3    # "e1":Ljava/lang/Exception;
    .restart local v5    # "pm":Landroid/content/pm/PackageManager;
    :catch_2
    move-exception v2

    .local v2, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    .line 122
    .end local v0    # "actionPendingIntent":Landroid/app/PendingIntent;
    .end local v1    # "active":Landroid/content/Intent;
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "package_name":Ljava/lang/String;
    .end local v5    # "pm":Landroid/content/pm/PackageManager;
    .end local v6    # "views":Landroid/widget/RemoteViews;
    :cond_1
    new-instance v6, Landroid/widget/RemoteViews;

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v14}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 123
    .restart local v6    # "views":Landroid/widget/RemoteViews;
    new-instance v1, Landroid/content/Intent;

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$context:Landroid/content/Context;

    const-class v8, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;

    invoke-direct {v1, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 124
    .restart local v1    # "active":Landroid/content/Intent;
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$context:Landroid/content/Context;

    iget v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$appWidgetId:I

    invoke-static {v7, v8, v1, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 125
    .restart local v0    # "actionPendingIntent":Landroid/app/PendingIntent;
    const v7, 0x7f0d002c

    invoke-virtual {v6, v7, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 126
    const-string v7, "setBackgroundResource"

    invoke-virtual {v6, v12, v7, v13}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 127
    const-string v7, "#AAAAAA"

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v10, v7}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 128
    const-string v7, "you need root access"

    invoke-virtual {v6, v10, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 130
    :try_start_5
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$context:Landroid/content/Context;

    invoke-static {v7}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v7

    iget v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$1;->val$appWidgetId:I

    invoke-virtual {v7, v8, v6}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_1

    .line 131
    :catch_3
    move-exception v2

    .line 132
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1
.end method
