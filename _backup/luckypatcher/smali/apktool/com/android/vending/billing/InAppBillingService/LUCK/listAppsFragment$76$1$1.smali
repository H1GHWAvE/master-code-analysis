.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;
.super Landroid/widget/ArrayAdapter;
.source "listAppsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1;

.field txtStatus:Landroid/widget/TextView;

.field txtStatusPatch:Landroid/widget/TextView;

.field txtTitle:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p1, "this$2"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1;
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I

    .prologue
    .line 9269
    .local p4, "x2":Ljava/util/List;, "Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;>;"
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 9276
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->xposedNotify:Z

    if-eqz v6, :cond_0

    .line 9277
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1$1;

    invoke-direct {v7, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;)V

    invoke-virtual {v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 9285
    const/4 v6, 0x0

    sput-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->xposedNotify:Z

    .line 9287
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;

    .line 9288
    .local v3, "p":Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;
    move-object v4, p2

    .line 9295
    .local v4, "row":Landroid/view/View;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v6

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 9296
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f04001f

    const/4 v7, 0x0

    invoke-virtual {v2, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 9298
    const v6, 0x7f0d004e

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtTitle:Landroid/widget/TextView;

    .line 9299
    const v6, 0x7f0d004f

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatus:Landroid/widget/TextView;

    .line 9300
    const v6, 0x7f0d0081

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    .line 9302
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 9303
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 9304
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 9306
    const v6, 0x7f0d0080

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 9307
    .local v0, "chk":Landroid/widget/CheckBox;
    iget-boolean v6, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 9308
    if-nez p1, :cond_4

    .line 9309
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch11()Z

    move-result v6

    if-eqz v6, :cond_13

    .line 9311
    const/4 v1, 0x0

    .line 9312
    .local v1, "i":I
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch11()Z

    move-result v6

    if-eqz v6, :cond_1

    add-int/lit8 v1, v1, 0x1

    .line 9313
    :cond_1
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch12()Z

    move-result v6

    if-eqz v6, :cond_2

    add-int/lit8 v1, v1, 0x1

    .line 9314
    :cond_2
    const/4 v6, 0x2

    if-ne v1, v6, :cond_3

    .line 9315
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 9316
    const/4 v6, 0x1

    iput-boolean v6, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->disabled:Z

    .line 9317
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "2/2 "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f0700bc

    invoke-static {v8}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9319
    :cond_3
    const/4 v6, 0x1

    if-ne v1, v6, :cond_4

    .line 9320
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "1/2 "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f0700bc

    invoke-static {v8}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9331
    .end local v1    # "i":I
    :cond_4
    :goto_0
    const/4 v6, 0x1

    if-ne p1, v6, :cond_5

    .line 9332
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch20()Z

    move-result v6

    if-eqz v6, :cond_15

    .line 9333
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 9334
    const/4 v6, 0x1

    iput-boolean v6, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->disabled:Z

    .line 9335
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700bc

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9344
    :cond_5
    :goto_1
    const/4 v6, 0x2

    if-ne p1, v6, :cond_7

    .line 9345
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->filePatch3:Z

    if-nez v6, :cond_6

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-static {v6}, Lcom/chelpus/Utils;->checkCoreJarPatch30(Landroid/content/pm/PackageManager;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 9346
    :cond_6
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 9347
    const/4 v6, 0x1

    iput-boolean v6, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->disabled:Z

    .line 9348
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700bc

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9357
    :cond_7
    :goto_2
    const/4 v6, 0x3

    if-ne p1, v6, :cond_9

    .line 9358
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 9360
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->filePatch3:Z

    if-nez v6, :cond_8

    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch40()Z

    move-result v6

    if-eqz v6, :cond_19

    .line 9361
    :cond_8
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 9362
    const/4 v6, 0x1

    iput-boolean v6, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->disabled:Z

    .line 9363
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700bc

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9371
    :goto_3
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f07007e

    invoke-static {v8}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "#FF0000"

    const-string v9, "normal"

    invoke-static {v7, v8, v9}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 9373
    :cond_9
    const/4 v6, 0x4

    if-ne p1, v6, :cond_a

    .line 9374
    const v6, 0x7f070082

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    .line 9375
    .local v5, "str2":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v5, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9378
    .end local v5    # "str2":Ljava/lang/String;
    :cond_a
    const/4 v6, 0x5

    if-ne p1, v6, :cond_b

    .line 9379
    const v6, 0x7f070081

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    .line 9380
    .restart local v5    # "str2":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v5, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9383
    .end local v5    # "str2":Ljava/lang/String;
    :cond_b
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 9394
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x1030046

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 9395
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatus:Landroid/widget/TextView;

    const v7, -0x777778

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 9396
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtTitle:Landroid/widget/TextView;

    const/4 v7, -0x1

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 9397
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtTitle:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Name:Ljava/lang/String;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9398
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtTitle:Landroid/widget/TextView;

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 9400
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;

    iget-object v5, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Name:Ljava/lang/String;

    .line 9402
    .restart local v5    # "str2":Ljava/lang/String;
    if-eqz p1, :cond_c

    const/4 v6, 0x1

    if-ne p1, v6, :cond_1b

    .line 9403
    :cond_c
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtTitle:Landroid/widget/TextView;

    const-string v7, "#ff00ff00"

    const-string v8, "bold"

    invoke-static {v5, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9414
    :goto_4
    if-nez p1, :cond_d

    .line 9415
    const v6, 0x7f070075

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    .line 9416
    :cond_d
    const/4 v6, 0x1

    if-ne p1, v6, :cond_e

    .line 9417
    const v6, 0x7f070077

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    .line 9418
    :cond_e
    const/4 v6, 0x2

    if-ne p1, v6, :cond_f

    .line 9419
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f070079

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f07007a

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 9420
    :cond_f
    const/4 v6, 0x3

    if-ne p1, v6, :cond_10

    .line 9421
    const v6, 0x7f07007c

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    .line 9422
    :cond_10
    const/4 v6, 0x4

    if-ne p1, v6, :cond_11

    const-string v5, ""

    .line 9423
    :cond_11
    const/4 v6, 0x5

    if-ne p1, v6, :cond_12

    const-string v5, ""

    .line 9424
    :cond_12
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatus:Landroid/widget/TextView;

    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v5, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 9428
    return-object v4

    .line 9323
    .end local v5    # "str2":Ljava/lang/String;
    :cond_13
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->fileNotSpace:Z

    if-eqz v6, :cond_14

    .line 9324
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700be

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 9326
    :cond_14
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700bd

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 9337
    :cond_15
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->fileNotSpace:Z

    if-eqz v6, :cond_16

    .line 9338
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700be

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 9340
    :cond_16
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700bd

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 9350
    :cond_17
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->fileNotSpace:Z

    if-eqz v6, :cond_18

    .line 9351
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700be

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 9353
    :cond_18
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700bd

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 9365
    :cond_19
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->fileNotSpace:Z

    if-eqz v6, :cond_1a

    .line 9366
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700be

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 9368
    :cond_1a
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700bd

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 9405
    .restart local v5    # "str2":Ljava/lang/String;
    :cond_1b
    const/4 v6, 0x4

    if-eq p1, v6, :cond_1c

    const/4 v6, 0x5

    if-ne p1, v6, :cond_1d

    .line 9406
    :cond_1c
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtTitle:Landroid/widget/TextView;

    const-string v7, "#ffff0000"

    const-string v8, "bold"

    invoke-static {v5, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 9408
    :cond_1d
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$76$1$1;->txtTitle:Landroid/widget/TextView;

    const-string v7, "#ff00ff00"

    const-string v8, "bold"

    invoke-static {v5, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4
.end method
