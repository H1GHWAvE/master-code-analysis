.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$39;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnChildClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->populateAdapter(Ljava/util/ArrayList;Ljava/util/Comparator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 6118
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$39;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 5
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I
    .param p5, "id"    # J

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x0

    .line 6124
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "vibration"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6125
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$39;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$39;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v2, "vibrator"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->vib:Landroid/os/Vibrator;

    .line 6126
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$39;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->vib:Landroid/os/Vibrator;

    const-wide/16 v1, 0x32

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    .line 6128
    :cond_0
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {v0, p3}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getItem(I)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-result-object v0

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 6130
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {v0, p3, p4}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getChild(II)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 6162
    :goto_0
    return v3

    .line 6132
    :sswitch_0
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$39;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->launch_click()V

    goto :goto_0

    .line 6135
    :sswitch_1
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$39;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->contextmenu_click()V

    goto :goto_0

    .line 6138
    :sswitch_2
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$39;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->contextmenutools_click()V

    goto :goto_0

    .line 6141
    :sswitch_3
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$39;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->uninstall_click()V

    goto :goto_0

    .line 6144
    :sswitch_4
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$39;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->cleardata_click()V

    goto :goto_0

    .line 6147
    :sswitch_5
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$39;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->move_to_sdcard_click()V

    goto :goto_0

    .line 6150
    :sswitch_6
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$39;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->move_to_internal_memory_click()V

    goto :goto_0

    .line 6153
    :sswitch_7
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$39;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->show_app_manager_click()V

    goto :goto_0

    .line 6156
    :sswitch_8
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->updateItem(Ljava/lang/String;)V

    .line 6157
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {v1, p3}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getItem(I)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->notifyDataSetChanged(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V

    .line 6158
    invoke-static {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 6159
    invoke-static {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showDialogLP(I)V

    goto :goto_0

    .line 6130
    :sswitch_data_0
    .sparse-switch
        0x7f07000d -> :sswitch_1
        0x7f070016 -> :sswitch_7
        0x7f070017 -> :sswitch_8
        0x7f070045 -> :sswitch_4
        0x7f070068 -> :sswitch_2
        0x7f07008a -> :sswitch_0
        0x7f070172 -> :sswitch_6
        0x7f070174 -> :sswitch_5
        0x7f070223 -> :sswitch_3
    .end sparse-switch
.end method
