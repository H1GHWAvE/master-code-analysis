.class Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp$1;
.super Ljava/lang/Object;
.source "LuckyApp.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 10
    .param p1, "arg0"    # Ljava/lang/Thread;
    .param p2, "arg1"    # Ljava/lang/Throwable;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 20
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FATAL Exception LP "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 22
    :try_start_0
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->init()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    :goto_0
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 25
    const/4 v3, 0x0

    .line 26
    .local v3, "noStart":Z
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;

    const-string v5, "config"

    invoke-virtual {v4, v5, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "force_close"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 27
    const/4 v3, 0x1

    .line 28
    :cond_0
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;

    const-string v5, "config"

    invoke-virtual {v4, v5, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "force_close"

    invoke-interface {v4, v5, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 31
    :try_start_1
    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;

    invoke-direct {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;-><init>()V

    .line 32
    .local v2, "log":Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;->instance:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->collect(Landroid/content/Context;Z)Z

    .line 34
    if-nez v3, :cond_1

    .line 35
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 36
    .local v1, "i":Landroid/content/Intent;
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;

    invoke-virtual {v4, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 40
    .end local v1    # "i":Landroid/content/Intent;
    .end local v2    # "log":Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;
    :cond_1
    :goto_1
    invoke-static {v7}, Ljava/lang/System;->exit(I)V

    .line 41
    return-void

    .line 23
    .end local v3    # "noStart":Z
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 39
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v3    # "noStart":Z
    :catch_1
    move-exception v0

    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_1

    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_2
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v7}, Ljava/lang/System;->exit(I)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
