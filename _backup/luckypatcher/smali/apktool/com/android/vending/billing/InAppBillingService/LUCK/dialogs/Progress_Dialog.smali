.class public Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;
.super Ljava/lang/Object;
.source "Progress_Dialog.java"


# static fields
.field public static dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;


# instance fields
.field dialog2:Landroid/app/Dialog;

.field fm:Landroid/support/v4/app/FragmentManager;

.field message:Ljava/lang/String;

.field title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->message:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->title:Ljava/lang/String;

    .line 18
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->fm:Landroid/support/v4/app/FragmentManager;

    .line 24
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog2:Landroid/app/Dialog;

    .line 22
    return-void
.end method

.method public static newInstance()Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;

    invoke-direct {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;-><init>()V

    .line 27
    .local v0, "f":Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;
    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog2:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog2:Landroid/app/Dialog;

    .line 128
    :cond_0
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog2:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 104
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateDialog()Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 42
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f070233

    invoke-static {v0}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->title:Ljava/lang/String;

    .line 43
    :cond_0
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 44
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->message:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f07014b

    invoke-static {v0}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->message:Ljava/lang/String;

    .line 45
    :cond_1
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 47
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setCancelable(Z)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 48
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog$1;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;)V

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 57
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->create()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public setCancelable(Z)V
    .locals 1
    .param p1, "trig"    # Z

    .prologue
    .line 121
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog2:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 122
    :cond_0
    return-void
.end method

.method public setIndeterminate(Z)V
    .locals 2
    .param p1, "indeterminate"    # Z

    .prologue
    .line 71
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->onCreateDialog()Landroid/app/Dialog;

    .line 72
    :cond_0
    if-nez p1, :cond_1

    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setIncrementStyle()Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 74
    :goto_0
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog$3;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 80
    return-void

    .line 73
    :cond_1
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setDefaultStyle()Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    goto :goto_0
.end method

.method public setMax(I)V
    .locals 2
    .param p1, "max"    # I

    .prologue
    .line 82
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->onCreateDialog()Landroid/app/Dialog;

    .line 83
    :cond_0
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setMax(I)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 84
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog$4;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 90
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 60
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->onCreateDialog()Landroid/app/Dialog;

    .line 61
    :cond_0
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->message:Ljava/lang/String;

    .line 62
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 63
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog$2;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 69
    return-void
.end method

.method public setProgress(I)V
    .locals 2
    .param p1, "progress"    # I

    .prologue
    .line 92
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->onCreateDialog()Landroid/app/Dialog;

    .line 93
    :cond_0
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setProgress(I)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 94
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog$5;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 100
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 109
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->onCreateDialog()Landroid/app/Dialog;

    .line 110
    :cond_0
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->title:Ljava/lang/String;

    .line 111
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 112
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog$6;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 119
    return-void
.end method

.method public showDialog()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog2:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 32
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->onCreateDialog()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog2:Landroid/app/Dialog;

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 35
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog;->dialog2:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 37
    :cond_1
    return-void
.end method
