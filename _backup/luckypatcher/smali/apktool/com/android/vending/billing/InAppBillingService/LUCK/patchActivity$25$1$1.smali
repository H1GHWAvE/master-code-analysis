.class Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$1$1;
.super Ljava/lang/Object;
.source "patchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$1;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$1;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$1;)V
    .locals 0
    .param p1, "this$2"    # Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$1;

    .prologue
    .line 1185
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$1$1;->this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1188
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;

    .line 1189
    .local v1, "cur":Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$1$1$1;

    invoke-direct {v3, p0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$1$1$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25$1$1;Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;)V

    .line 1206
    .local v3, "dialogClickListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 1207
    .local v0, "builder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    const v4, 0x7f070234

    invoke-static {v4}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f070020

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "?"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v4

    const v5, 0x7f070003

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v4

    const v5, 0x7f07017b

    .line 1208
    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v2

    .line 1209
    .local v2, "dialog":Landroid/app/Dialog;
    invoke-static {v2}, Lcom/chelpus/Utils;->showDialog(Landroid/app/Dialog;)V

    .line 1212
    return-void
.end method
