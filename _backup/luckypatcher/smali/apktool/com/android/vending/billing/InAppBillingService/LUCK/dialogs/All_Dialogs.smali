.class public Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;
.super Ljava/lang/Object;
.source "All_Dialogs.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$byPkgName;
    }
.end annotation


# static fields
.field public static final ADD_BOOT:I = 0x2

.field public static final CREATE_APK:I = 0x0

.field public static final CUSTOM_PATCH:I = 0x1


# instance fields
.field dialog:Landroid/app/Dialog;

.field public dialog_int:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/16 v0, 0xff

    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->dialog_int:I

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->dialog:Landroid/app/Dialog;

    .line 88
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->dialog:Landroid/app/Dialog;

    .line 104
    :cond_0
    return-void
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    .line 108
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateDialog()Landroid/app/Dialog;
    .locals 51

    .prologue
    .line 113
    sget-object v45, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v46, "Create dialog"

    invoke-virtual/range {v45 .. v46}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 114
    sget v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dialog_int:I

    move/from16 v0, v45

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->dialog_int:I

    .line 115
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v45, :cond_0

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    if-nez v45, :cond_1

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->dismiss()V

    .line 118
    :cond_1
    :try_start_0
    sget-object v45, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v46, "All Dialog create."

    invoke-virtual/range {v45 .. v46}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 119
    const/16 v32, 0x0

    .line 120
    .local v32, "logdialog":Landroid/app/Dialog;
    sget v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dialog_int:I

    sparse-switch v45, :sswitch_data_0

    .line 1615
    const/16 v32, 0x0

    .line 1626
    .end local v32    # "logdialog":Landroid/app/Dialog;
    :goto_0
    return-object v32

    .line 123
    .restart local v32    # "logdialog":Landroid/app/Dialog;
    :sswitch_0
    new-instance v39, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v39

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 125
    .local v39, "permbuilder6":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_2

    .line 127
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 128
    const/16 v45, 0x1

    move-object/from16 v0, v39

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapterNotClose(Z)V

    .line 129
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$1;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v39

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 144
    const v45, 0x7f0700c6

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$2;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v39

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 176
    :cond_2
    invoke-virtual/range {v39 .. v39}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto :goto_0

    .line 180
    .end local v39    # "permbuilder6":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_1
    new-instance v37, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v37

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 182
    .local v37, "perm1builder6":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_3

    .line 184
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 185
    const/16 v45, 0x1

    move-object/from16 v0, v37

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapterNotClose(Z)V

    .line 186
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$3;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v37

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 201
    const v45, 0x7f0701d0

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$4;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v37

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 229
    :cond_3
    invoke-virtual/range {v37 .. v37}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 232
    .end local v37    # "perm1builder6":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_2
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-direct {v3, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 233
    .local v3, "ads1builder6":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_4

    .line 234
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 235
    const/16 v45, 0x1

    move/from16 v0, v45

    invoke-virtual {v3, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapterNotClose(Z)V

    .line 236
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$5;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v3, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 259
    const v45, 0x7f070146

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$6;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v3, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 273
    :cond_4
    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 277
    .end local v3    # "ads1builder6":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_3
    new-instance v19, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v19

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 278
    .local v19, "compbuilder6":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->component_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_5

    .line 279
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->component_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 280
    const/16 v45, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapterNotClose(Z)V

    .line 281
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->component_adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$7;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 306
    const v45, 0x7f070146

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$8;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$8;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 320
    :cond_5
    invoke-virtual/range {v19 .. v19}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 323
    .end local v19    # "compbuilder6":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_4
    new-instance v38, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v38

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 325
    .local v38, "perm1builder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_6

    .line 327
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 328
    const/16 v45, 0x1

    move-object/from16 v0, v38

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapterNotClose(Z)V

    .line 329
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$9;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$9;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v38

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 344
    const v45, 0x7f0701d0

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$10;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$10;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v38

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 372
    :cond_6
    invoke-virtual/range {v38 .. v38}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 376
    .end local v38    # "perm1builder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_5
    new-instance v45, Ljava/util/ArrayList;

    invoke-direct/range {v45 .. v45}, Ljava/util/ArrayList;-><init>()V

    sput-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->changedPermissions:Ljava/util/ArrayList;

    .line 377
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-direct {v10, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 379
    .local v10, "builder6":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_7

    .line 381
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 382
    const/16 v45, 0x1

    move/from16 v0, v45

    invoke-virtual {v10, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapterNotClose(Z)V

    .line 383
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$11;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$11;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v10, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 413
    const v45, 0x7f0701b7

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$12;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$12;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v10, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 470
    :cond_7
    invoke-virtual {v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 474
    .end local v10    # "builder6":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_6
    new-instance v11, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-direct {v11, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 476
    .local v11, "builder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_8

    .line 478
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 479
    const/16 v45, 0x1

    move/from16 v0, v45

    invoke-virtual {v11, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapterNotClose(Z)V

    .line 480
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$13;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$13;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v11, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 515
    const v45, 0x7f070198

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$14;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$14;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v11, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 538
    :cond_8
    invoke-virtual {v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 541
    .end local v11    # "builder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_7
    new-instance v12, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-direct {v12, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 543
    .local v12, "builder9":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_9

    .line 545
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 547
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$15;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$15;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v12, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 576
    :cond_9
    invoke-virtual {v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 579
    .end local v12    # "builder9":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_8
    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-direct {v7, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 581
    .local v7, "backup_builder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_a

    .line 583
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 584
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$16;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$16;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v7, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 599
    :cond_a
    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 602
    .end local v7    # "backup_builder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_9
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_b

    .line 603
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 606
    :cond_b
    new-instance v43, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    const/16 v46, 0x1

    move-object/from16 v0, v43

    move-object/from16 v1, v45

    move/from16 v2, v46

    invoke-direct {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;Z)V

    .line 607
    .local v43, "tempdialog":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$17;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$17;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v43

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 619
    const/16 v45, 0x1

    move-object/from16 v0, v43

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setCancelable(Z)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 620
    new-instance v45, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$18;

    move-object/from16 v0, v45

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$18;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v43

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 626
    invoke-virtual/range {v43 .. v43}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 630
    .end local v43    # "tempdialog":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_a
    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-direct {v5, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 632
    .local v5, "adsbuilder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_c

    .line 634
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 635
    const/16 v45, 0x1

    move/from16 v0, v45

    invoke-virtual {v5, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapterNotClose(Z)V

    .line 636
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$19;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$19;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v5, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 672
    const v45, 0x7f070198

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$20;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$20;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v5, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 697
    :cond_c
    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 700
    .end local v5    # "adsbuilder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_b
    new-instance v4, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-direct {v4, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 702
    .local v4, "ads2builder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_d

    .line 704
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 705
    const/16 v45, 0x1

    move/from16 v0, v45

    invoke-virtual {v4, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapterNotClose(Z)V

    .line 706
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$21;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$21;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v4, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 729
    const v45, 0x7f0700c6

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$22;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$22;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v4, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 753
    :cond_d
    invoke-virtual {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 756
    .end local v4    # "ads2builder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_c
    new-instance v42, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v42

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 758
    .local v42, "supportbuilder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_e

    .line 760
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 761
    const/16 v45, 0x1

    move-object/from16 v0, v42

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapterNotClose(Z)V

    .line 762
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$23;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$23;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v42

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 790
    const v45, 0x7f070198

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$24;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$24;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v42

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 814
    :cond_e
    invoke-virtual/range {v42 .. v42}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 817
    .end local v42    # "supportbuilder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_d
    new-instance v41, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v41

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 819
    .local v41, "support2builder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_f

    .line 821
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 822
    const/16 v45, 0x1

    move-object/from16 v0, v41

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapterNotClose(Z)V

    .line 823
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$25;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$25;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v41

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 839
    const v45, 0x7f0700c6

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$26;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$26;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v41

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 862
    :cond_f
    invoke-virtual/range {v41 .. v41}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 865
    .end local v41    # "support2builder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_e
    new-instance v33, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v33

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 867
    .local v33, "lvl2builder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_10

    .line 869
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 870
    const/16 v45, 0x1

    move-object/from16 v0, v33

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapterNotClose(Z)V

    .line 871
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$27;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$27;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v33

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 916
    const v45, 0x7f0700c6

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$28;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$28;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v33

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 947
    :cond_10
    invoke-virtual/range {v33 .. v33}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 951
    .end local v33    # "lvl2builder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_f
    new-instance v34, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v34

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 953
    .local v34, "lvlbuilder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_11

    .line 955
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 956
    const/16 v45, 0x1

    move-object/from16 v0, v34

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapterNotClose(Z)V

    .line 957
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$29;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$29;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v34

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 1011
    const v45, 0x7f070198

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$30;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$30;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v34

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 1039
    :cond_11
    invoke-virtual/range {v34 .. v34}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 1041
    .end local v34    # "lvlbuilder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :sswitch_10
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    const v46, 0x7f040020

    const/16 v47, 0x0

    invoke-static/range {v45 .. v47}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/LinearLayout;

    .line 1043
    .local v22, "d":Landroid/widget/LinearLayout;
    const v45, 0x7f0d0082

    move-object/from16 v0, v22

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v29

    check-cast v29, Landroid/widget/ListView;

    .line 1044
    .local v29, "list":Landroid/widget/ListView;
    const v45, 0x7f0d0083

    move-object/from16 v0, v22

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/CheckBox;

    .line 1045
    .local v14, "chk":Landroid/widget/CheckBox;
    const/16 v45, 0x1

    move/from16 v0, v45

    invoke-virtual {v14, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1046
    const v45, 0x7f0700bb

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-virtual {v14, v0}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 1047
    const/16 v45, 0x1

    move/from16 v0, v45

    invoke-virtual {v14, v0}, Landroid/widget/CheckBox;->setMaxLines(I)V

    .line 1048
    const-string v45, "/system/framework/core.jar"

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v45

    if-eqz v45, :cond_13

    invoke-static {}, Lcom/chelpus/Utils;->getCurrentRuntimeValue()Ljava/lang/String;

    move-result-object v45

    const-string v46, "ART"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-nez v45, :cond_13

    .line 1049
    const/16 v45, 0x1

    move/from16 v0, v45

    invoke-virtual {v14, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1050
    const/16 v45, 0x1

    sput-boolean v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchOnlyDalvikCore:Z

    .line 1057
    :goto_1
    new-instance v45, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$31;

    move-object/from16 v0, v45

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$31;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;Landroid/widget/CheckBox;)V

    move-object/from16 v0, v45

    invoke-virtual {v14, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1078
    new-instance v21, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v21

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 1080
    .local v21, "corebuilder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_12

    .line 1081
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 1082
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    move-object/from16 v0, v29

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1083
    const/16 v45, 0x3

    move/from16 v0, v45

    new-array v0, v0, [I

    move-object/from16 v18, v0

    fill-array-data v18, :array_0

    .line 1084
    .local v18, "colors":[I
    new-instance v45, Landroid/graphics/drawable/GradientDrawable;

    sget-object v46, Landroid/graphics/drawable/GradientDrawable$Orientation;->RIGHT_LEFT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    move-object/from16 v0, v29

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1085
    const/16 v45, 0x1

    move-object/from16 v0, v29

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 1086
    new-instance v45, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$32;

    move-object/from16 v0, v45

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$32;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v29

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1141
    invoke-virtual/range {v21 .. v22}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setView(Landroid/view/View;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 1143
    const v45, 0x7f070198

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$33;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$33;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 1190
    .end local v18    # "colors":[I
    :cond_12
    invoke-virtual/range {v21 .. v21}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 1052
    .end local v21    # "corebuilder7":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :cond_13
    const/16 v45, 0x0

    sput-boolean v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchOnlyDalvikCore:Z

    .line 1053
    const/16 v45, 0x0

    move/from16 v0, v45

    invoke-virtual {v14, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1054
    const/16 v45, 0x0

    move/from16 v0, v45

    invoke-virtual {v14, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 1617
    .end local v14    # "chk":Landroid/widget/CheckBox;
    .end local v22    # "d":Landroid/widget/LinearLayout;
    .end local v29    # "list":Landroid/widget/ListView;
    .end local v32    # "logdialog":Landroid/app/Dialog;
    :catch_0
    move-exception v25

    .line 1618
    .local v25, "e":Ljava/lang/Exception;
    sget-object v45, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v46, Ljava/lang/StringBuilder;

    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    const-string v47, "LuckyPatcher (Create Dialog): "

    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v46

    move-object/from16 v0, v46

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v46

    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1619
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->printStackTrace()V

    .line 1620
    new-instance v26, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v26

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 1621
    .local v26, "errorbuilder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    const-string v45, "Error"

    move-object/from16 v0, v26

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v45

    const-string v46, "Sorry...\nShow Dialog - Error..."

    .line 1622
    invoke-virtual/range {v45 .. v46}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v45

    const-string v46, "OK"

    const/16 v47, 0x0

    .line 1623
    invoke-virtual/range {v45 .. v47}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 1624
    invoke-virtual/range {v26 .. v26}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v27

    .local v27, "errordialog":Landroid/app/Dialog;
    move-object/from16 v32, v27

    .line 1626
    goto/16 :goto_0

    .line 1192
    .end local v25    # "e":Ljava/lang/Exception;
    .end local v26    # "errorbuilder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .end local v27    # "errordialog":Landroid/app/Dialog;
    .restart local v32    # "logdialog":Landroid/app/Dialog;
    :sswitch_11
    :try_start_1
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    const v46, 0x7f040020

    const/16 v47, 0x0

    invoke-static/range {v45 .. v47}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/LinearLayout;

    .line 1193
    .local v23, "d_x":Landroid/widget/LinearLayout;
    const v45, 0x7f0d0083

    move-object/from16 v0, v23

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/CheckBox;

    .line 1194
    .local v16, "chk_x":Landroid/widget/CheckBox;
    invoke-static {}, Lcom/chelpus/Utils;->readXposedParamBoolean()Lorg/json/JSONObject;

    move-result-object v45

    const-string v46, "module_on"

    invoke-virtual/range {v45 .. v46}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v45

    move-object/from16 v0, v16

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1195
    invoke-static {}, Lcom/chelpus/Utils;->isXposedEnabled()Z

    move-result v45

    if-eqz v45, :cond_16

    .line 1196
    const/16 v45, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 1197
    const v45, 0x7f070236

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v16

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 1203
    :goto_2
    const/16 v45, 0x2

    move-object/from16 v0, v16

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setMaxLines(I)V

    .line 1205
    new-instance v13, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-direct {v13, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 1207
    .local v13, "builder_x":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    if-eqz v45, :cond_15

    .line 1208
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    const/16 v46, 0x1

    invoke-virtual/range {v45 .. v46}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 1210
    const v45, 0x7f0d0082

    move-object/from16 v0, v23

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/widget/ListView;

    .line 1211
    .local v30, "list_x":Landroid/widget/ListView;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    move-object/from16 v0, v30

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1212
    const/16 v45, 0x3

    move/from16 v0, v45

    new-array v0, v0, [I

    move-object/from16 v18, v0

    fill-array-data v18, :array_1

    .line 1213
    .restart local v18    # "colors":[I
    new-instance v45, Landroid/graphics/drawable/GradientDrawable;

    sget-object v46, Landroid/graphics/drawable/GradientDrawable$Orientation;->RIGHT_LEFT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    move-object/from16 v0, v30

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1214
    const/16 v45, 0x1

    move-object/from16 v0, v30

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 1215
    invoke-static {}, Lcom/chelpus/Utils;->isXposedEnabled()Z

    move-result v45

    if-eqz v45, :cond_14

    .line 1217
    new-instance v45, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$34;

    move-object/from16 v0, v45

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$34;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1235
    :cond_14
    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setView(Landroid/view/View;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 1236
    invoke-static {}, Lcom/chelpus/Utils;->isXposedEnabled()Z

    move-result v45

    if-nez v45, :cond_17

    .line 1237
    const v45, 0x7f070193

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    const/16 v46, 0x0

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v13, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 1333
    .end local v18    # "colors":[I
    .end local v30    # "list_x":Landroid/widget/ListView;
    :cond_15
    :goto_3
    invoke-virtual {v13}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 1199
    .end local v13    # "builder_x":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :cond_16
    const/16 v45, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 1200
    const/16 v45, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1201
    const v45, 0x7f070238

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v16

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1239
    .restart local v13    # "builder_x":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .restart local v18    # "colors":[I
    .restart local v30    # "list_x":Landroid/widget/ListView;
    :cond_17
    const v45, 0x7f070228

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;Landroid/widget/CheckBox;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v13, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    goto :goto_3

    .line 1337
    .end local v13    # "builder_x":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .end local v16    # "chk_x":Landroid/widget/CheckBox;
    .end local v18    # "colors":[I
    .end local v23    # "d_x":Landroid/widget/LinearLayout;
    .end local v30    # "list_x":Landroid/widget/ListView;
    :sswitch_12
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    const v46, 0x7f04001e

    const/16 v47, 0x0

    invoke-static/range {v45 .. v47}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 1339
    .local v8, "body":Landroid/widget/LinearLayout;
    const v45, 0x7f0d007f

    move/from16 v0, v45

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v44

    check-cast v44, Landroid/widget/TextView;

    .line 1341
    .local v44, "txt":Landroid/widget/TextView;
    const v45, 0x7f0d0080

    move/from16 v0, v45

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/CheckBox;

    .line 1343
    .local v15, "chk_b":Landroid/widget/CheckBox;
    const-string v6, ""

    .line 1344
    .local v6, "apply_text":Ljava/lang/String;
    const v45, 0x7f070248

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1345
    const/16 v17, 0x0

    .line 1346
    .local v17, "color":I
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->result_core_patch:Ljava/lang/String;

    const-string v46, "_patch1"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v45

    if-eqz v45, :cond_1b

    .line 1347
    const/16 v28, 0x0

    .line 1348
    .local v28, "i":I
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v46, "Core patched!"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v45

    if-nez v45, :cond_18

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v46, "Core 2 patched!"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v45

    if-eqz v45, :cond_22

    .line 1349
    :cond_18
    const v45, 0x7f0700bc

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    .line 1350
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v46, "Core patched!"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v45

    if-eqz v45, :cond_19

    add-int/lit8 v28, v28, 0x1

    .line 1351
    :cond_19
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v46, "Core 2 patched!"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v45

    if-eqz v45, :cond_1a

    add-int/lit8 v28, v28, 0x1

    .line 1352
    :cond_1a
    const v17, -0xff0100

    .line 1357
    :goto_4
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const v46, 0x7f070074

    invoke-static/range {v46 .. v46}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, " "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, "/2 "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1358
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v45

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, "\n"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    .line 1359
    .local v40, "str2":Ljava/lang/String;
    const-string v45, ""

    move-object/from16 v0, v40

    move/from16 v1, v17

    move-object/from16 v2, v45

    invoke-static {v0, v1, v2}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1361
    .end local v28    # "i":I
    .end local v40    # "str2":Ljava/lang/String;
    :cond_1b
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->result_core_patch:Ljava/lang/String;

    const-string v46, "_patch2"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v45

    if-eqz v45, :cond_1c

    .line 1362
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v46, "Core unsigned install patched!"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v45

    if-eqz v45, :cond_23

    .line 1363
    const v45, 0x7f0700bc

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    .line 1364
    const v17, -0xff0100

    .line 1369
    :goto_5
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const v46, 0x7f070076

    invoke-static/range {v46 .. v46}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, " "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1370
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v45

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, "\n"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    .line 1371
    .restart local v40    # "str2":Ljava/lang/String;
    const-string v45, ""

    move-object/from16 v0, v40

    move/from16 v1, v17

    move-object/from16 v2, v45

    invoke-static {v0, v1, v2}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1373
    .end local v40    # "str2":Ljava/lang/String;
    :cond_1c
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->result_core_patch:Ljava/lang/String;

    const-string v46, "_patch3"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v45

    if-eqz v45, :cond_1d

    .line 1374
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v46, "Core4 patched!"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v45

    if-eqz v45, :cond_24

    .line 1375
    const v45, 0x7f0700bc

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    .line 1376
    const v17, -0xff0100

    .line 1381
    :goto_6
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const v46, 0x7f070078

    invoke-static/range {v46 .. v46}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, " "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1382
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v45

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, "\n"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    .line 1383
    .restart local v40    # "str2":Ljava/lang/String;
    const-string v45, ""

    move-object/from16 v0, v40

    move/from16 v1, v17

    move-object/from16 v2, v45

    invoke-static {v0, v1, v2}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1385
    .end local v40    # "str2":Ljava/lang/String;
    :cond_1d
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->result_core_patch:Ljava/lang/String;

    const-string v46, "restore"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v45

    if-eqz v45, :cond_21

    .line 1386
    const/16 v45, 0x0

    move/from16 v0, v45

    invoke-virtual {v15, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1387
    const/16 v45, 0x0

    move/from16 v0, v45

    invoke-virtual {v15, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 1388
    invoke-static {}, Lcom/chelpus/Utils;->turn_off_patch_on_boot_all()V

    .line 1389
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v46, "Core restored!"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v45

    if-nez v45, :cond_1e

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v46, "Core 2 restored!"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v45

    if-eqz v45, :cond_1f

    .line 1390
    :cond_1e
    const v45, 0x7f070255

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    .line 1391
    const v17, -0xff0100

    .line 1392
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const v46, 0x7f070074

    invoke-static/range {v46 .. v46}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, " "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1393
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v45

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, "\n"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    .line 1394
    .restart local v40    # "str2":Ljava/lang/String;
    const-string v45, ""

    move-object/from16 v0, v40

    move/from16 v1, v17

    move-object/from16 v2, v45

    invoke-static {v0, v1, v2}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1396
    .end local v40    # "str2":Ljava/lang/String;
    :cond_1f
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v46, "Core unsigned install restored!"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v45

    if-eqz v45, :cond_20

    .line 1397
    const v45, 0x7f070255

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    .line 1398
    const v17, -0xff0100

    .line 1399
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const v46, 0x7f070076

    invoke-static/range {v46 .. v46}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, " "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1400
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v45

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, "\n"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    .line 1401
    .restart local v40    # "str2":Ljava/lang/String;
    const-string v45, ""

    move-object/from16 v0, v40

    move/from16 v1, v17

    move-object/from16 v2, v45

    invoke-static {v0, v1, v2}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1403
    .end local v40    # "str2":Ljava/lang/String;
    :cond_20
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v46, "Core4 restored!"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v45

    if-eqz v45, :cond_21

    .line 1404
    const v45, 0x7f070255

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    .line 1405
    const v17, -0xff0100

    .line 1406
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const v46, 0x7f070078

    invoke-static/range {v46 .. v46}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, " "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1407
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v45

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, "\n"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    .line 1408
    .restart local v40    # "str2":Ljava/lang/String;
    const-string v45, ""

    move-object/from16 v0, v40

    move/from16 v1, v17

    move-object/from16 v2, v45

    invoke-static {v0, v1, v2}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1412
    .end local v40    # "str2":Ljava/lang/String;
    :cond_21
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "\n"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    const v46, 0x7f070247

    invoke-static/range {v46 .. v46}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 1414
    const/16 v45, 0x0

    move/from16 v0, v45

    invoke-virtual {v15, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1415
    const v45, 0x7f070249

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-virtual {v15, v0}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 1417
    invoke-virtual {v15}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v45

    if-eqz v45, :cond_25

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->result_core_patch:Ljava/lang/String;

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->turn_on_patch_on_boot(Ljava/lang/String;)V

    .line 1421
    :goto_7
    new-instance v45, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$36;

    move-object/from16 v0, v45

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$36;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v45

    invoke-virtual {v15, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1431
    new-instance v20, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v20

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 1433
    .local v20, "core_result_builder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setView(Landroid/view/View;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 1435
    const v45, 0x7f070193

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$37;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$37;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;Landroid/widget/CheckBox;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v45

    const v46, 0x7f070137

    .line 1443
    invoke-virtual/range {v45 .. v46}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setTitle(I)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 1446
    invoke-virtual/range {v20 .. v20}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    goto/16 :goto_0

    .line 1354
    .end local v20    # "core_result_builder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .restart local v28    # "i":I
    :cond_22
    const v45, 0x7f0700bd

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    .line 1355
    const/high16 v17, -0x10000

    goto/16 :goto_4

    .line 1366
    .end local v28    # "i":I
    :cond_23
    const v45, 0x7f0700bd

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    .line 1367
    const/high16 v17, -0x10000

    goto/16 :goto_5

    .line 1378
    :cond_24
    const v45, 0x7f0700bc

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    .line 1379
    const/high16 v17, -0x10000

    goto/16 :goto_6

    .line 1419
    :cond_25
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->result_core_patch:Ljava/lang/String;

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->turn_off_patch_on_boot(Ljava/lang/String;)V

    goto :goto_7

    .line 1450
    .end local v6    # "apply_text":Ljava/lang/String;
    .end local v8    # "body":Landroid/widget/LinearLayout;
    .end local v15    # "chk_b":Landroid/widget/CheckBox;
    .end local v17    # "color":I
    .end local v44    # "txt":Landroid/widget/TextView;
    :sswitch_13
    const/16 v24, 0x0

    .line 1451
    .local v24, "dialog":Landroid/app/Dialog;
    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-direct {v9, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 1454
    .local v9, "builder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->refresh_boot()V

    .line 1456
    new-instance v45, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;

    sget-object v46, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v46 .. v46}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v46

    const v47, 0x7f040011

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v48

    const-string v49, "viewsize"

    sget-object v50, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/16 v50, 0x0

    invoke-interface/range {v48 .. v50}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v48

    sget-object v49, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->boot_pat:Ljava/util/ArrayList;

    invoke-direct/range {v45 .. v49}, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    sput-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapter_boot:Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;

    .line 1457
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapter_boot:Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$byPkgName;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$byPkgName;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v46

    move-object/from16 v1, v45

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->sorter:Ljava/util/Comparator;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1459
    :try_start_2
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->notifyDataSetChanged()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1463
    :goto_8
    :try_start_3
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapter_boot:Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->notifyDataSetChanged()V

    .line 1464
    const v45, 0x7f0700df

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-virtual {v9, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 1465
    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapter_boot:Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;

    new-instance v46, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$38;

    move-object/from16 v0, v46

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$38;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v9, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 1539
    sget-object v45, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v46, "asd2"

    invoke-virtual/range {v45 .. v46}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1540
    invoke-virtual {v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v24

    move-object/from16 v32, v24

    .line 1541
    goto/16 :goto_0

    .line 1460
    :catch_1
    move-exception v25

    .line 1461
    .restart local v25    # "e":Ljava/lang/Exception;
    sget-object v45, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v46, Ljava/lang/StringBuilder;

    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    const-string v47, "LuckyPatcher(Bootlist dialog): "

    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v46

    move-object/from16 v0, v46

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v46

    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_8

    .line 1546
    .end local v9    # "builder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .end local v24    # "dialog":Landroid/app/Dialog;
    .end local v25    # "e":Ljava/lang/Exception;
    :sswitch_14
    new-instance v31, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v31

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 1548
    .local v31, "logbuilder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    sget v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dialog_int:I

    const v46, 0x54506

    move/from16 v0, v45

    move/from16 v1, v46

    if-ne v0, v1, :cond_26

    .line 1549
    const v45, 0x7f0701d8

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v35

    .line 1552
    .local v35, "message":Ljava/lang/String;
    :goto_9
    const v45, 0x7f070234

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v31

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v45

    const v46, 0x7f02002e

    .line 1553
    invoke-virtual/range {v45 .. v46}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setIcon(I)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v45

    .line 1554
    move-object/from16 v0, v45

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v45

    const v46, 0x7f070003

    .line 1555
    invoke-static/range {v46 .. v46}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v46

    new-instance v47, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$40;

    move-object/from16 v0, v47

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$40;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    invoke-virtual/range {v45 .. v47}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v45

    const v46, 0x7f07017b

    .line 1590
    invoke-static/range {v46 .. v46}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v46

    new-instance v47, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$39;

    move-object/from16 v0, v47

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$39;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V

    invoke-virtual/range {v45 .. v47}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 1595
    invoke-virtual/range {v31 .. v31}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v32

    .line 1596
    goto/16 :goto_0

    .line 1551
    .end local v35    # "message":Ljava/lang/String;
    :cond_26
    const v45, 0x7f0700fb

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v35

    .restart local v35    # "message":Ljava/lang/String;
    goto :goto_9

    .line 1598
    .end local v31    # "logbuilder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .end local v35    # "message":Ljava/lang/String;
    :sswitch_15
    new-instance v36, Landroid/app/ProgressDialog;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v36

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 1599
    .local v36, "pd":Landroid/app/ProgressDialog;
    const-string v45, "Progress"

    move-object/from16 v0, v36

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1600
    const v45, 0x7f070047

    invoke-static/range {v45 .. v45}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v36

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1601
    const/16 v45, 0x1

    move-object/from16 v0, v36

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1602
    move-object/from16 v32, v36

    .line 1603
    goto/16 :goto_0

    .line 1605
    .end local v36    # "pd":Landroid/app/ProgressDialog;
    :sswitch_16
    new-instance v31, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v45, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v45 .. v45}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v45

    move-object/from16 v0, v31

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 1606
    .restart local v31    # "logbuilder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    const-string v45, "Error"

    move-object/from16 v0, v31

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v45

    const v46, 0x7f0700f9

    .line 1607
    invoke-static/range {v46 .. v46}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v45

    const-string v46, "OK"

    const/16 v47, 0x0

    .line 1608
    invoke-virtual/range {v45 .. v47}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 1609
    invoke-virtual/range {v31 .. v31}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v32

    .line 1611
    goto/16 :goto_0

    .line 120
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_13
        0xa -> :sswitch_5
        0xd -> :sswitch_6
        0x10 -> :sswitch_7
        0x11 -> :sswitch_a
        0x12 -> :sswitch_b
        0x13 -> :sswitch_e
        0x14 -> :sswitch_f
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x18 -> :sswitch_10
        0x19 -> :sswitch_4
        0x1a -> :sswitch_8
        0x1c -> :sswitch_9
        0x1d -> :sswitch_2
        0x1f -> :sswitch_3
        0x22 -> :sswitch_c
        0x23 -> :sswitch_d
        0x25 -> :sswitch_12
        0x27 -> :sswitch_11
        0xcb7 -> :sswitch_15
        0x54506 -> :sswitch_14
        0x35f112 -> :sswitch_16
        0x35f3ac -> :sswitch_14
    .end sparse-switch

    .line 1083
    :array_0
    .array-data 4
        -0x664054ac
        -0x4054ac
        -0x664054ac
    .end array-data

    .line 1212
    :array_1
    .array-data 4
        -0x664054ac
        -0x4054ac
        -0x664054ac
    .end array-data
.end method

.method public showDialog()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->dialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->onCreateDialog()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->dialog:Landroid/app/Dialog;

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 97
    :cond_1
    return-void
.end method
