.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->corepatch(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

.field final synthetic val$result:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 2083
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->val$result:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const-wide/16 v9, 0x0

    const/4 v4, 0x0

    .line 2086
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$1;

    invoke-direct {v6, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;)V

    invoke-virtual {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 2095
    :try_start_0
    sget-boolean v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchOnlyDalvikCore:Z

    if-eqz v5, :cond_4

    .line 2096
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "patch only dalvik cache mode"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2097
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v5, ""

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    .line 2098
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v5, ""

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    .line 2099
    const-string v4, ""

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->result_core_patch:Ljava/lang/String;

    .line 2100
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v5, "/system/framework/core.jar"

    invoke-static {v5}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    .line 2101
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 2102
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$2;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 2109
    :cond_0
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v5, "/system/framework/services.jar"

    invoke-static {v5}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    .line 2110
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    if-nez v4, :cond_1

    .line 2111
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$3;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 2117
    :cond_1
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2118
    new-instance v4, Lcom/chelpus/Utils;

    const-string v5, ""

    invoke-direct {v4, v5}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".corepatch "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->val$result:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v8, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v8, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " OnlyDalvik"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 2119
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2120
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$4;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 2142
    :goto_0
    sget-boolean v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->install_market_to_system:Z

    if-eqz v4, :cond_2

    invoke-static {}, Lcom/chelpus/Utils;->reboot()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2283
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$9;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$9;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 2290
    return-void

    .line 2136
    :cond_3
    :try_start_1
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$5;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2280
    :catch_0
    move-exception v0

    .line 2281
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 2144
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_2
    invoke-static {}, Lcom/chelpus/Utils;->turn_off_patch_on_boot_all()V

    .line 2145
    const-string v5, "/system/framework/core.jar"

    invoke-static {v5}, Lcom/chelpus/Utils;->isOdex(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    new-instance v5, Ljava/io/File;

    const-string v6, "/system/framework/core.jar"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    cmp-long v5, v5, v9

    if-eqz v5, :cond_5

    const-string v5, "/system/framework/services.jar"

    invoke-static {v5}, Lcom/chelpus/Utils;->isOdex(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    new-instance v5, Ljava/io/File;

    const-string v6, "system/framework/services.jar"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    cmp-long v5, v5, v9

    if-nez v5, :cond_16

    .line 2146
    :cond_5
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "start odex framework"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2147
    const-string v5, "/system"

    const-string v6, "rw"

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2148
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chattr -ai "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/system/framework/core.jar"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2149
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chattr -ai "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/system/framework/services.jar"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2150
    const-string v5, "chattr -ai /system/framework/core.jar"

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2151
    const-string v5, "chattr -ai /system/framework/services.jar"

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2152
    const-string v5, "chattr -ai /system/framework/core-libart.jar"

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2153
    const/4 v5, 0x6

    new-array v2, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "/data/dalvik-cache/"

    aput-object v6, v2, v5

    const/4 v5, 0x1

    const-string v6, "/data/dalvik-cache/arm/"

    aput-object v6, v2, v5

    const/4 v5, 0x2

    const-string v6, "/sd-ext/data/dalvik-cache/"

    aput-object v6, v2, v5

    const/4 v5, 0x3

    const-string v6, "/cache/dalvik-cache/"

    aput-object v6, v2, v5

    const/4 v5, 0x4

    const-string v6, "/sd-ext/data/cache/dalvik-cache/"

    aput-object v6, v2, v5

    const/4 v5, 0x5

    const-string v6, "/data/cache/dalvik-cache/"

    aput-object v6, v2, v5

    .line 2154
    .local v2, "strings2":[Ljava/lang/String;
    array-length v5, v2

    :goto_2
    if-ge v4, v5, :cond_b

    aget-object v3, v2, v4

    .line 2155
    .local v3, "tail":Ljava/lang/String;
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LuckyPatcher: search dalvik-cache! "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "system@framework@core.jar@classes.dex"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2156
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "system@framework@core.jar@classes.dex"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "system@framework@services.jar@classes.dex"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    invoke-static {}, Lcom/chelpus/Utils;->getCurrentRuntimeValue()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ART"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 2157
    :cond_6
    invoke-static {}, Lcom/chelpus/Utils;->getCurrentRuntimeValue()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ART"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 2158
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "LuckyPatcher: art patch"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2162
    :goto_3
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "system@framework@core.jar@classes.dex"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    .line 2163
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "system@framework@services.jar@classes.dex"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    .line 2164
    const-string v4, ""

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 2165
    const-string v4, "/system/framework/core.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->isOdex(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    new-instance v4, Ljava/io/File;

    const-string v5, "/system/framework/core.jar"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    cmp-long v4, v4, v9

    if-eqz v4, :cond_8

    .line 2166
    :cond_7
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v5, "/system/framework/core.jar"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    .line 2167
    :cond_8
    const-string v4, "/system/framework/services.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->isOdex(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_9

    new-instance v4, Ljava/io/File;

    const-string v5, "/system/framework/services.jar"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    cmp-long v4, v4, v9

    if-eqz v4, :cond_a

    .line 2168
    :cond_9
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v5, "/system/framework/services.jar"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    .line 2169
    :cond_a
    const-string v4, "/system"

    const-string v5, "rw"

    invoke-static {v4, v5}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2171
    new-instance v4, Lcom/chelpus/Utils;

    const-string v5, ""

    invoke-direct {v4, v5}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".corepatch "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->val$result:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v8, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v8, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/chelpus/Utils;->getCurrentRuntimeValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 2172
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2179
    .end local v3    # "tail":Ljava/lang/String;
    :cond_b
    const-string v4, "/system/framework/core.patched"

    invoke-static {v4}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_c

    const-string v4, "/data/data/core.odex"

    invoke-static {v4}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 2180
    :cond_c
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "Result code for found core.patched"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2181
    const-string v1, ""

    .line 2182
    .local v1, "patched_core":Ljava/lang/String;
    const-string v4, "/system/framework/core.patched"

    invoke-static {v4}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2183
    const-string v1, "/system/framework/core.patched"

    .line 2184
    :cond_d
    const-string v4, "/data/data/core.odex"

    invoke-static {v4}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 2185
    const-string v1, "/data/data/core.odex"

    .line 2186
    :cond_e
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "chmod 0644 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2187
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "chown 0.0 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2188
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "chown 0:0 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2190
    const-string v4, "/system/framework/services.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 2191
    const-string v4, "chmod 0644 /system/framework/services.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2192
    const-string v4, "chown 0.0 /system/framework/services.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2193
    const-string v4, "chown 0:0 /system/framework/services.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2195
    :cond_f
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->val$result:Ljava/lang/String;

    const-string v5, "restore"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_12

    .line 2196
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$6;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    .line 2160
    .end local v1    # "patched_core":Ljava/lang/String;
    .restart local v3    # "tail":Ljava/lang/String;
    :cond_10
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "LuckyPatcher: found dalvik-cache! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "system@framework@core.jar@classes.dex"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2154
    :cond_11
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 2243
    .end local v3    # "tail":Ljava/lang/String;
    .restart local v1    # "patched_core":Ljava/lang/String;
    :cond_12
    invoke-static {}, Lcom/chelpus/Utils;->reboot()V

    goto/16 :goto_1

    .line 2246
    .end local v1    # "patched_core":Ljava/lang/String;
    :cond_13
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v5, "LuckyPatcher: odex not equal lenght packed! Not enougth space in /system/!"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 2247
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$7;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 2253
    :cond_14
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v5, "LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 2254
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$8;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13$8;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    .line 2260
    :cond_15
    invoke-static {}, Lcom/chelpus/Utils;->reboot()V

    goto/16 :goto_1

    .line 2265
    .end local v2    # "strings2":[Ljava/lang/String;
    :cond_16
    const-string v4, ""

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 2267
    const-string v4, "/system"

    const-string v5, "rw"

    invoke-static {v4, v5}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2268
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "chattr -ai "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/system/framework/core.jar"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2269
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "chattr -ai "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/system/framework/services.jar"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2270
    const-string v4, "chattr -ai /system/framework/core.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2271
    const-string v4, "chattr -ai /system/framework/services.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2272
    const-string v4, "chattr -ai /system/framework/core-libart.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2274
    new-instance v4, Lcom/chelpus/Utils;

    const-string v5, ""

    invoke-direct {v4, v5}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".corepatch "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$13;->val$result:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/system/framework/core.jar"

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/system/framework/services.jar"

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/chelpus/Utils;->getCurrentRuntimeValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 2275
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2276
    invoke-static {}, Lcom/chelpus/Utils;->reboot()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1
.end method
