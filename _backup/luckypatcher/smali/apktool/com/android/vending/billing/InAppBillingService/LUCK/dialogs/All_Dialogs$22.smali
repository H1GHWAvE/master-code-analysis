.class Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$22;
.super Ljava/lang/Object;
.source "All_Dialogs.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->onCreateDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;

    .prologue
    .line 729
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$22;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v6, 0x7

    .line 734
    const-string v3, "1"

    .line 735
    .local v3, "result":Ljava/lang/String;
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    .line 736
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 737
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;

    .line 738
    .local v2, "patt":Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Status:Z

    if-eqz v4, :cond_0

    if-ge v1, v6, :cond_0

    .line 739
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "pattern"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 740
    :cond_0
    if-ne v1, v6, :cond_1

    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Status:Z

    if-eqz v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "dependencies"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 741
    :cond_1
    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Status:Z

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "fulloffline"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 736
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 744
    .end local v2    # "patt":Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;
    :cond_3
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 745
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v4, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->createapkads(Ljava/lang/String;)V

    .line 749
    :goto_1
    return-void

    .line 747
    :cond_4
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v4, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolbar_createapkads(Ljava/lang/String;)V

    goto :goto_1
.end method
