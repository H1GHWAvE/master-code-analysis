.class public Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;
.super Landroid/appwidget/AppWidgetProvider;
.source "BinderWidget.java"


# static fields
.field public static ACTION_WIDGET_RECEIVER:Ljava/lang/String;

.field public static ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

.field public static widget:Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-string v0, "ActionReceiverWidgetBinder"

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;->ACTION_WIDGET_RECEIVER:Ljava/lang/String;

    .line 32
    const-string v0, "ActionReceiverWidgetBinderUpdate"

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;->widget:Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method public static pushWidgetUpdate(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "remoteViews"    # Landroid/widget/RemoteViews;

    .prologue
    .line 35
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 36
    .local v1, "myWidget":Landroid/content/ComponentName;
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 38
    .local v0, "manager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v0, v1, p1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 39
    return-void
.end method

.method static updateAppWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p2, "appWidgetId"    # I

    .prologue
    .line 74
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->init()V

    .line 75
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$1;

    invoke-direct {v1, p0, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$1;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 160
    .local v0, "binder":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 161
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 163
    return-void
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    .line 53
    array-length v0, p2

    .line 54
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 55
    aget v2, p2, v1

    invoke-static {p1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->deleteTitlePref(Landroid/content/Context;I)V

    .line 54
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 58
    :cond_0
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 169
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 170
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "action":Ljava/lang/String;
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;->ACTION_WIDGET_RECEIVER:Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 174
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->init()V

    .line 175
    const-string v7, "appWidgetId"

    const/4 v8, -0x1

    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 176
    .local v4, "id":I
    invoke-static {p1, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->loadTitlePref(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "NOT_SAVED_BIND"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    sput-object p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;->widget:Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;

    .line 177
    :cond_0
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    .line 179
    .local v3, "handler":Landroid/os/Handler;
    :try_start_0
    new-instance v6, Ljava/lang/Thread;

    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;

    invoke-direct {v7, p0, p2, p1, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;Landroid/content/Intent;Landroid/content/Context;Landroid/os/Handler;)V

    invoke-direct {v6, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 307
    .local v6, "turn_binder":Ljava/lang/Thread;
    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Ljava/lang/Thread;->setPriority(I)V

    .line 308
    sget-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->binder_process:Z

    if-nez v7, :cond_1

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    .end local v3    # "handler":Landroid/os/Handler;
    .end local v4    # "id":I
    .end local v6    # "turn_binder":Ljava/lang/Thread;
    :cond_1
    :goto_0
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 320
    const/4 v7, 0x1

    sput-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->binderWidget:Z

    .line 321
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 322
    .local v2, "gm":Landroid/appwidget/AppWidgetManager;
    new-instance v7, Landroid/content/ComponentName;

    const-class v8, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;

    invoke-direct {v7, p1, v8}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v7}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v5

    .line 323
    .local v5, "ids":[I
    invoke-virtual {p0, p1, v2, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 329
    .end local v2    # "gm":Landroid/appwidget/AppWidgetManager;
    .end local v5    # "ids":[I
    :cond_2
    return-void

    .line 309
    .restart local v3    # "handler":Landroid/os/Handler;
    .restart local v4    # "id":I
    :catch_0
    move-exception v1

    .line 310
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 43
    array-length v0, p3

    .line 44
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 45
    aget v2, p3, v1

    invoke-static {p1, p2, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;->updateAppWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    .line 44
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 48
    :cond_0
    return-void
.end method
