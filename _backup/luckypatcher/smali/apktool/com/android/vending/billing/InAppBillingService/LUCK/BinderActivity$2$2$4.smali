.class Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2$4;
.super Ljava/lang/Object;
.source "BinderActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2;)V
    .locals 0
    .param p1, "this$2"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2;

    .prologue
    .line 364
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2$4;->this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 369
    .local p1, "l":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v2

    invoke-interface {v2, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    .line 370
    .local v1, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;
    new-instance v0, Ljava/io/File;

    iget-object v2, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 371
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 372
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2$4;->this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    # setter for: Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->current:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;
    invoke-static {v2, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->access$302(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;)Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    .line 373
    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 374
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2$4;->this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    iget-object v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    move-object v2, p1

    check-cast v2, Landroid/widget/ListView;

    # setter for: Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->filebrowser:Landroid/widget/ListView;
    invoke-static {v3, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->access$002(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Landroid/widget/ListView;)Landroid/widget/ListView;

    .line 376
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2$4;->this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    new-instance v3, Ljava/io/File;

    iget-object v4, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    check-cast p1, Landroid/widget/ListView;

    .end local p1    # "l":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    # invokes: Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->getDir(Ljava/lang/String;Landroid/widget/ListView;)V
    invoke-static {v2, v3, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->access$100(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Ljava/lang/String;Landroid/widget/ListView;)V

    .line 388
    :cond_0
    :goto_0
    return-void

    .line 378
    .restart local p1    # "l":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    :cond_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2$4;->this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2;

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2$2;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->context:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x108009b

    .line 379
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 380
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] folder can\'t be read!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "OK"

    const/4 v4, 0x0

    .line 381
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 378
    invoke-static {v2}, Lcom/chelpus/Utils;->showDialog(Landroid/app/Dialog;)V

    goto :goto_0
.end method
