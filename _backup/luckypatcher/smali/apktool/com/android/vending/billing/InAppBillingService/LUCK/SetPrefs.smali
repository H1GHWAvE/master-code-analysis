.class public Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;
.super Landroid/preference/PreferenceActivity;
.source "SetPrefs.java"


# instance fields
.field public context:Landroid/content/Context;

.field numberCheckListener:Landroid/preference/Preference$OnPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 1270
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$28;

    invoke-direct {v0, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$28;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->numberCheckListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->showMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->numberCheck(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static copyFolder(Ljava/io/File;Ljava/io/File;)V
    .locals 6
    .param p0, "src"    # Ljava/io/File;
    .param p1, "dest"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1189
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1191
    invoke-virtual {p0, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1195
    invoke-virtual {p0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 1196
    .local v2, "files":[Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 1197
    array-length v5, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v1, v2, v4

    .line 1199
    .local v1, "file":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1200
    .local v3, "srcFile":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1202
    .local v0, "destFile":Ljava/io/File;
    invoke-static {v3, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->copyFolder(Ljava/io/File;Ljava/io/File;)V

    .line 1197
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1209
    .end local v0    # "destFile":Ljava/io/File;
    .end local v1    # "file":Ljava/lang/String;
    .end local v2    # "files":[Ljava/lang/String;
    .end local v3    # "srcFile":Ljava/io/File;
    :cond_0
    invoke-virtual {p0, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1211
    :cond_1
    return-void
.end method

.method private numberCheck(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 1289
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\d*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1290
    const/4 v0, 0x1

    .line 1293
    :goto_0
    return v0

    .line 1292
    :cond_0
    const v0, 0x7f070234

    invoke-static {v0}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f070142

    invoke-static {v1}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p0, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->showMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1293
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 1248
    new-instance v0, Landroid/app/Dialog;

    invoke-direct {v0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 1249
    .local v0, "dialog":Landroid/app/Dialog;
    invoke-virtual {v0, p2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1252
    const v3, 0x7f040030

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setContentView(I)V

    .line 1253
    const v3, 0x7f0d00a6

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1254
    .local v1, "messagetext":Landroid/widget/TextView;
    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1257
    const v3, 0x7f0d00a4

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 1258
    .local v2, "ok":Landroid/widget/Button;
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$27;

    invoke-direct {v3, p0, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$27;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;Landroid/app/Dialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1266
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1268
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 19
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super/range {p0 .. p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->context:Landroid/content/Context;

    .line 52
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v14

    const-string v15, "config"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 53
    .local v11, "settings":Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v14

    const-string v15, "SetPrefs"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 54
    .local v10, "sett":Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v14

    const-string v15, "config"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v12

    .line 55
    .local v12, "shared2":Landroid/content/SharedPreferences;
    invoke-interface {v12}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 56
    .local v7, "editor2":Landroid/content/SharedPreferences$Editor;
    const-string v14, "Update0"

    const/4 v15, 0x1

    invoke-interface {v7, v14, v15}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 57
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 59
    const-string v14, "force_language"

    const-string v15, "default"

    invoke-interface {v11, v14, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 62
    .local v8, "lng":Ljava/lang/String;
    const-string v14, "default"

    invoke-virtual {v8, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 63
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v14

    iget-object v5, v14, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 65
    .local v5, "appLoc3":Ljava/util/Locale;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v14

    invoke-static {v14}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 66
    new-instance v3, Landroid/content/res/Configuration;

    invoke-direct {v3}, Landroid/content/res/Configuration;-><init>()V

    .line 67
    .local v3, "appConfig3":Landroid/content/res/Configuration;
    iput-object v5, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 68
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v14

    .line 69
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v15

    .line 68
    invoke-virtual {v14, v3, v15}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 82
    .end local v3    # "appConfig3":Landroid/content/res/Configuration;
    .end local v5    # "appLoc3":Ljava/util/Locale;
    :goto_0
    const-string v14, "orientstion"

    const/4 v15, 0x3

    invoke-interface {v11, v14, v15}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_0

    .line 83
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->setRequestedOrientation(I)V

    .line 85
    :cond_0
    const-string v14, "orientstion"

    const/4 v15, 0x3

    invoke-interface {v11, v14, v15}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_1

    .line 86
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->setRequestedOrientation(I)V

    .line 88
    :cond_1
    const-string v14, "orientstion"

    const/4 v15, 0x3

    invoke-interface {v11, v14, v15}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v14

    const/4 v15, 0x3

    if-ne v14, v15, :cond_2

    .line 89
    const/4 v14, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->setRequestedOrientation(I)V

    .line 91
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v9

    .line 92
    .local v9, "prefMgr":Landroid/preference/PreferenceManager;
    const-string v14, "SetPrefs"

    invoke-virtual {v9, v14}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 93
    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Landroid/preference/PreferenceManager;->setSharedPreferencesMode(I)V

    .line 96
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "viewsize"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "viewsize"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 97
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "root_force"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "root_force"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 98
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "confirm_exit"

    const-string v16, "confirm_exit"

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 99
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "orientstion"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "orientstion"

    const/16 v18, 0x3

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 100
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "sortby"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "sortby"

    const/16 v18, 0x2

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 101
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "language"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "language"

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 102
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "systemapp"

    const-string v16, "systemapp"

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 103
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "apkname"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "apkname"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 104
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "lvlapp"

    const-string v16, "lvlapp"

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 105
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "adsapp"

    const-string v16, "adsapp"

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 106
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "customapp"

    const-string v16, "customapp"

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 107
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "modifapp"

    const-string v16, "modifapp"

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 108
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "hide_notify"

    const-string v16, "hide_notify"

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 109
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "fixedapp"

    const-string v16, "fixedapp"

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 110
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "noneapp"

    const-string v16, "noneapp"

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 111
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "path"

    const-string v16, "basepath"

    const-string v17, "Noting"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 112
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "extStorageDirectory"

    const-string v16, "extStorageDirectory"

    const-string v17, "Noting"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 113
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "vibration"

    const-string v16, "vibration"

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 114
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "disable_autoupdate"

    const-string v16, "disable_autoupdate"

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 115
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "fast_start"

    const-string v16, "fast_start"

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 116
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "no_icon"

    const-string v16, "no_icon"

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 117
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "hide_title"

    const-string v16, "hide_title"

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 118
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "days_on_up"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "days_on_up"

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 119
    const v14, 0x7f050005

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->addPreferencesFromResource(I)V

    .line 121
    invoke-virtual/range {p0 .. p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v14

    const-string v15, "days_on_up"

    invoke-virtual {v14, v15}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    .line 124
    .local v6, "delayPreference":Landroid/preference/Preference;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->numberCheckListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v6, v14}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 127
    const-string v14, "viewsize"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 165
    const-string v14, "root_force"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$2;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 203
    const-string v14, "orientstion"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$3;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 242
    const-string v14, "sortby"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$4;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 271
    const-string v14, "language"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$5;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 326
    const-string v14, "systemapp"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$6;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 357
    const-string v14, "confirm_exit"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$7;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 386
    const-string v14, "apkname"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$8;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$8;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 413
    const-string v14, "lvlapp"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$9;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$9;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 444
    const-string v14, "adsapp"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$10;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$10;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 475
    const-string v14, "customapp"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$11;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$11;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 506
    const-string v14, "modifapp"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$12;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$12;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 537
    const-string v14, "fixedapp"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$13;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$13;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 568
    const-string v14, "noneapp"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$14;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$14;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 599
    const-string v14, "hide_notify"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$15;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$15;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 626
    const-string v14, "vibration"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$16;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$16;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 653
    const-string v14, "disable_autoupdate"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$17;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$17;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 680
    const-string v14, "fast_start"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$18;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$18;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 707
    const-string v14, "no_icon"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$19;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$19;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 734
    const-string v14, "hide_title"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$20;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$20;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 761
    const-string v14, "path"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 857
    const-string v14, "help"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$22;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$22;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 865
    const-string v14, "upd"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$23;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$23;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 875
    const-string v14, "sendlog"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 933
    const-string v14, "about"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$25;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$25;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 983
    const-string v14, "language"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V

    invoke-virtual {v14, v15}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1184
    return-void

    .line 71
    .end local v6    # "delayPreference":Landroid/preference/Preference;
    .end local v9    # "prefMgr":Landroid/preference/PreferenceManager;
    :cond_3
    const/4 v4, 0x0

    .line 72
    .local v4, "appLoc2":Ljava/util/Locale;
    const-string v14, "_"

    invoke-virtual {v8, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 73
    .local v13, "tails":[Ljava/lang/String;
    array-length v14, v13

    const/4 v15, 0x1

    if-ne v14, v15, :cond_4

    new-instance v4, Ljava/util/Locale;

    .end local v4    # "appLoc2":Ljava/util/Locale;
    const/4 v14, 0x0

    aget-object v14, v13, v14

    invoke-direct {v4, v14}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 74
    .restart local v4    # "appLoc2":Ljava/util/Locale;
    :cond_4
    array-length v14, v13

    const/4 v15, 0x2

    if-ne v14, v15, :cond_5

    new-instance v4, Ljava/util/Locale;

    .end local v4    # "appLoc2":Ljava/util/Locale;
    const/4 v14, 0x0

    aget-object v14, v13, v14

    const/4 v15, 0x1

    aget-object v15, v13, v15

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-direct {v4, v14, v15, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .restart local v4    # "appLoc2":Ljava/util/Locale;
    :cond_5
    array-length v14, v13

    const/4 v15, 0x3

    if-ne v14, v15, :cond_6

    new-instance v4, Ljava/util/Locale;

    .end local v4    # "appLoc2":Ljava/util/Locale;
    const/4 v14, 0x0

    aget-object v14, v13, v14

    const/4 v15, 0x1

    aget-object v15, v13, v15

    const/16 v16, 0x2

    aget-object v16, v13, v16

    move-object/from16 v0, v16

    invoke-direct {v4, v14, v15, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .restart local v4    # "appLoc2":Ljava/util/Locale;
    :cond_6
    invoke-static {v4}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 77
    new-instance v2, Landroid/content/res/Configuration;

    invoke-direct {v2}, Landroid/content/res/Configuration;-><init>()V

    .line 78
    .local v2, "appConfig2":Landroid/content/res/Configuration;
    iput-object v4, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 79
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v14

    .line 80
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v15

    .line 79
    invoke-virtual {v14, v2, v15}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    goto/16 :goto_0
.end method

.method public testPath(ZLjava/lang/String;)Z
    .locals 7
    .param p1, "showDialog"    # Z
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const v6, 0x7f070234

    const v5, 0x7f07015a

    const/4 v1, 0x0

    .line 1214
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "test path."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1217
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 1218
    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1219
    const v2, 0x7f070234

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f07015a

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p0, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->showMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1238
    :cond_1
    :goto_0
    return v1

    .line 1224
    :cond_2
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/tmp.txt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1225
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/tmp.txt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1226
    const/4 v1, 0x1

    goto :goto_0

    .line 1228
    :cond_3
    if-eqz p1, :cond_1

    .line 1229
    const v2, 0x7f070234

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f07015a

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p0, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->showMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1233
    :catch_0
    move-exception v0

    .line 1234
    .local v0, "e":Ljava/io/IOException;
    if-eqz p1, :cond_1

    .line 1235
    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p0, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->showMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
