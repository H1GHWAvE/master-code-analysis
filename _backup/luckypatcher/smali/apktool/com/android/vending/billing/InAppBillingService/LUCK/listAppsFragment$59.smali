.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$59;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->contextlevel0()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 7587
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$59;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 7590
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 7591
    .local v8, "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    sget-boolean v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v10, :cond_1

    .line 7592
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-eqz v10, :cond_0

    const v10, 0x7f070083

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7595
    :cond_0
    new-instance v2, Ljava/lang/Thread;

    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$59$1;

    invoke-direct {v10, p0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$59$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$59;Ljava/util/ArrayList;)V

    invoke-direct {v2, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 7614
    .local v2, "cont":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 7616
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7620
    :goto_0
    const v10, 0x7f070089

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7621
    const v10, 0x7f07006c

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7622
    const v10, 0x7f070067

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7627
    const v10, 0x7f07004d

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7630
    .end local v2    # "cont":Ljava/lang/Thread;
    :cond_1
    const v10, 0x7f0700c7

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7631
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-nez v10, :cond_2

    const v10, 0x7f070046

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7632
    :cond_2
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    if-eqz v10, :cond_3

    sget-boolean v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v10, :cond_3

    .line 7633
    const v10, 0x7f070084

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7635
    :cond_3
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    if-eqz v10, :cond_4

    const v10, 0x7f070062

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7636
    :cond_4
    const/4 v1, 0x0

    .line 7638
    .local v1, "backupFound":Z
    :try_start_1
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Backup"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v6

    .line 7639
    .local v6, "files":[Ljava/lang/String;
    array-length v11, v6

    const/4 v10, 0x0

    :goto_1
    if-ge v10, v11, :cond_6

    aget-object v5, v6, v10

    .line 7641
    .local v5, "file":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v14, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ".ver"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_5

    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v14, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ".ver"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_5

    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v14, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ".apk"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v12

    if-eqz v12, :cond_8

    .line 7642
    :cond_5
    const/4 v1, 0x1

    .line 7649
    .end local v5    # "file":Ljava/lang/String;
    .end local v6    # "files":[Ljava/lang/String;
    :cond_6
    :goto_2
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Backup/Data/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v12, v12, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v7

    .line 7650
    .local v7, "list":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 7651
    .local v3, "datafound":Z
    if-eqz v7, :cond_9

    array-length v10, v7

    if-eqz v10, :cond_9

    .line 7652
    array-length v11, v7

    const/4 v10, 0x0

    :goto_3
    if-ge v10, v11, :cond_9

    aget-object v9, v7, v10

    .line 7653
    .local v9, "t":Ljava/lang/String;
    const-string v12, ".lpbkp"

    invoke-virtual {v9, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 7654
    const/4 v3, 0x1

    .line 7652
    :cond_7
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 7617
    .end local v1    # "backupFound":Z
    .end local v3    # "datafound":Z
    .end local v7    # "list":[Ljava/lang/String;
    .end local v9    # "t":Ljava/lang/String;
    .restart local v2    # "cont":Ljava/lang/Thread;
    :catch_0
    move-exception v4

    .line 7618
    .local v4, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v4}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_0

    .line 7639
    .end local v2    # "cont":Ljava/lang/Thread;
    .end local v4    # "e":Ljava/lang/InterruptedException;
    .restart local v1    # "backupFound":Z
    .restart local v5    # "file":Ljava/lang/String;
    .restart local v6    # "files":[Ljava/lang/String;
    :cond_8
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 7646
    .end local v5    # "file":Ljava/lang/String;
    .end local v6    # "files":[Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 7647
    .local v4, "e":Ljava/lang/Exception;
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v11, "LuckyPatcher error: backup files or directory not found!"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_2

    .line 7658
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v3    # "datafound":Z
    .restart local v7    # "list":[Ljava/lang/String;
    :cond_9
    if-nez v1, :cond_a

    if-eqz v3, :cond_b

    .line 7659
    :cond_a
    if-eqz v1, :cond_14

    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-nez v10, :cond_14

    .line 7660
    const v10, 0x7f0700b6

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7665
    :cond_b
    :goto_4
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-eqz v10, :cond_c

    sget-boolean v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v10, :cond_d

    .line 7666
    :cond_c
    const v10, 0x7f07006d

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7670
    :cond_d
    sget-boolean v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v10, :cond_f

    .line 7671
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-nez v10, :cond_e

    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-eqz v10, :cond_e

    .line 7672
    const v10, 0x7f070070

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7673
    :cond_e
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-nez v10, :cond_f

    const v10, 0x7f07008b

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7675
    :cond_f
    sget-boolean v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v10, :cond_10

    .line 7682
    const v10, 0x7f070051

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7683
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    if-eqz v10, :cond_15

    const v10, 0x7f070055

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7686
    :cond_10
    :goto_5
    sget-boolean v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v10, :cond_13

    .line 7687
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-nez v10, :cond_11

    const v10, 0x7f070176

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7690
    :cond_11
    :try_start_2
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v10

    sget-object v11, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v11, v11, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v10

    iget-object v10, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v10, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 7691
    .local v0, "apk_file":Ljava/lang/String;
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-eqz v10, :cond_13

    const-string v10, "/data/app"

    invoke-virtual {v0, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_12

    const-string v10, "/mnt/asec"

    invoke-virtual {v0, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_12

    const-string v10, "/data/priv-app"

    invoke-virtual {v0, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_13

    .line 7692
    :cond_12
    const v10, 0x7f070140

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    .line 7697
    .end local v0    # "apk_file":Ljava/lang/String;
    :cond_13
    :goto_6
    const v10, 0x7f0701e8

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7701
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$59$2;

    iget-object v11, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$59;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v11

    const v12, 0x7f04002e

    invoke-direct {v10, p0, v11, v12, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$59$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$59;Landroid/content/Context;ILjava/util/List;)V

    sput-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    .line 7937
    iget-object v10, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$59;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v11, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$59$3;

    invoke-direct {v11, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$59$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$59;)V

    invoke-virtual {v10, v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 7969
    return-void

    .line 7662
    :cond_14
    sget-boolean v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v10, :cond_b

    if-eqz v3, :cond_b

    const v10, 0x7f0700b6

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 7684
    :cond_15
    const v10, 0x7f070057

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 7693
    :catch_2
    move-exception v4

    .line 7694
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v4}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_6
.end method
