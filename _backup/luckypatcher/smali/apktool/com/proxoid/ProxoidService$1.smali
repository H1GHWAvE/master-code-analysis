.class Lcom/proxoid/ProxoidService$1;
.super Lcom/proxoid/IProxoidControl$Stub;
.source "ProxoidService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/proxoid/ProxoidService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/proxoid/ProxoidService;


# direct methods
.method constructor <init>(Lcom/proxoid/ProxoidService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/proxoid/ProxoidService;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    invoke-direct {p0}, Lcom/proxoid/IProxoidControl$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public update()Z
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 57
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    # invokes: Lcom/proxoid/ProxoidService;->getSharedPreferences()Landroid/content/SharedPreferences;
    invoke-static {v13}, Lcom/proxoid/ProxoidService;->access$000(Lcom/proxoid/ProxoidService;)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 59
    .local v11, "sp":Landroid/content/SharedPreferences;
    const-string v13, "onoff"

    const/4 v14, 0x0

    invoke-interface {v11, v13, v14}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    .line 60
    .local v12, "start":Z
    const-string v13, "8080"

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 61
    .local v10, "port":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    const-string v14, "Don\\\'t change"

    # setter for: Lcom/proxoid/ProxoidService;->useragent:Ljava/lang/String;
    invoke-static {v13, v14}, Lcom/proxoid/ProxoidService;->access$102(Lcom/proxoid/ProxoidService;Ljava/lang/String;)Ljava/lang/String;

    .line 63
    if-nez v12, :cond_1

    .line 64
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    # invokes: Lcom/proxoid/ProxoidService;->doStop()V
    invoke-static {v13}, Lcom/proxoid/ProxoidService;->access$200(Lcom/proxoid/ProxoidService;)V

    .line 129
    :cond_0
    :goto_0
    const/4 v13, 0x1

    :goto_1
    return v13

    .line 66
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    # getter for: Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;
    invoke-static {v13}, Lcom/proxoid/ProxoidService;->access$300(Lcom/proxoid/ProxoidService;)Lcom/mba/proxylight/ProxyLight;

    move-result-object v13

    if-nez v13, :cond_2

    .line 67
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    new-instance v14, Lcom/proxoid/ProxoidService$1$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/proxoid/ProxoidService$1$1;-><init>(Lcom/proxoid/ProxoidService$1;)V

    # setter for: Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;
    invoke-static {v13, v14}, Lcom/proxoid/ProxoidService;->access$302(Lcom/proxoid/ProxoidService;Lcom/mba/proxylight/ProxyLight;)Lcom/mba/proxylight/ProxyLight;

    .line 80
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    # getter for: Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;
    invoke-static {v13}, Lcom/proxoid/ProxoidService;->access$300(Lcom/proxoid/ProxoidService;)Lcom/mba/proxylight/ProxyLight;

    move-result-object v13

    invoke-virtual {v13}, Lcom/mba/proxylight/ProxyLight;->getRequestFilters()Ljava/util/List;

    move-result-object v13

    new-instance v14, Lcom/proxoid/ProxoidService$UserAgentRequestFilter;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    const/16 v16, 0x0

    invoke-direct/range {v14 .. v16}, Lcom/proxoid/ProxoidService$UserAgentRequestFilter;-><init>(Lcom/proxoid/ProxoidService;Lcom/proxoid/ProxoidService$1;)V

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    # getter for: Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;
    invoke-static {v13}, Lcom/proxoid/ProxoidService;->access$300(Lcom/proxoid/ProxoidService;)Lcom/mba/proxylight/ProxyLight;

    move-result-object v13

    invoke-virtual {v13, v10}, Lcom/mba/proxylight/ProxyLight;->setPort(I)V

    .line 84
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    # getter for: Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;
    invoke-static {v13}, Lcom/proxoid/ProxoidService;->access$300(Lcom/proxoid/ProxoidService;)Lcom/mba/proxylight/ProxyLight;

    move-result-object v13

    invoke-virtual {v13}, Lcom/mba/proxylight/ProxyLight;->getPort()I

    move-result v13

    if-eq v13, v10, :cond_3

    .line 85
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    # getter for: Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;
    invoke-static {v13}, Lcom/proxoid/ProxoidService;->access$300(Lcom/proxoid/ProxoidService;)Lcom/mba/proxylight/ProxyLight;

    move-result-object v13

    invoke-virtual {v13, v10}, Lcom/mba/proxylight/ProxyLight;->setPort(I)V

    .line 87
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    # getter for: Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;
    invoke-static {v13}, Lcom/proxoid/ProxoidService;->access$300(Lcom/proxoid/ProxoidService;)Lcom/mba/proxylight/ProxyLight;

    move-result-object v13

    invoke-virtual {v13}, Lcom/mba/proxylight/ProxyLight;->stop()V

    .line 89
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    # getter for: Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;
    invoke-static {v13}, Lcom/proxoid/ProxoidService;->access$300(Lcom/proxoid/ProxoidService;)Lcom/mba/proxylight/ProxyLight;

    move-result-object v13

    invoke-virtual {v13}, Lcom/mba/proxylight/ProxyLight;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    const-string v14, "Service proxy restarted"

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    .line 99
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    # getter for: Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;
    invoke-static {v13}, Lcom/proxoid/ProxoidService;->access$300(Lcom/proxoid/ProxoidService;)Lcom/mba/proxylight/ProxyLight;

    move-result-object v13

    invoke-virtual {v13}, Lcom/mba/proxylight/ProxyLight;->isRunning()Z

    move-result v13

    if-nez v13, :cond_0

    .line 102
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    # getter for: Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;
    invoke-static {v13}, Lcom/proxoid/ProxoidService;->access$300(Lcom/proxoid/ProxoidService;)Lcom/mba/proxylight/ProxyLight;

    move-result-object v13

    invoke-virtual {v13}, Lcom/mba/proxylight/ProxyLight;->start()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 110
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    const-string v14, "notification"

    invoke-virtual {v13, v14}, Lcom/proxoid/ProxoidService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/NotificationManager;

    .line 111
    .local v7, "mNotificationManager":Landroid/app/NotificationManager;
    const v6, 0x7f020030

    .line 112
    .local v6, "icon":I
    new-instance v8, Landroid/app/Notification;

    const-string v13, "Lucky Patcher proxy running."

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-direct {v8, v6, v13, v14, v15}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 114
    .local v8, "notification":Landroid/app/Notification;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    invoke-virtual {v13}, Lcom/proxoid/ProxoidService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 115
    .local v4, "context":Landroid/content/Context;
    const-string v3, "Lucky Proxoid"

    .line 116
    .local v3, "contentTitle":Ljava/lang/CharSequence;
    const-string v2, "proxy running"

    .line 117
    .local v2, "contentText":Ljava/lang/CharSequence;
    new-instance v9, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    const-class v14, Lcom/proxoid/Proxoid;

    invoke-direct {v9, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 118
    .local v9, "notificationIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static {v13, v14, v9, v15}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 120
    .local v1, "contentIntent":Landroid/app/PendingIntent;
    invoke-virtual {v8, v4, v3, v2, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 121
    iget v13, v8, Landroid/app/Notification;->flags:I

    or-int/lit8 v13, v13, 0x2

    iput v13, v8, Landroid/app/Notification;->flags:I

    .line 123
    # getter for: Lcom/proxoid/ProxoidService;->ID:I
    invoke-static {}, Lcom/proxoid/ProxoidService;->access$500()I

    move-result v13

    invoke-virtual {v7, v13, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 125
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    const-string v14, "Proxy running."

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 90
    .end local v1    # "contentIntent":Landroid/app/PendingIntent;
    .end local v2    # "contentText":Ljava/lang/CharSequence;
    .end local v3    # "contentTitle":Ljava/lang/CharSequence;
    .end local v4    # "context":Landroid/content/Context;
    .end local v6    # "icon":I
    .end local v7    # "mNotificationManager":Landroid/app/NotificationManager;
    .end local v8    # "notification":Landroid/app/Notification;
    .end local v9    # "notificationIntent":Landroid/content/Intent;
    :catch_0
    move-exception v5

    .line 91
    .local v5, "e":Ljava/lang/Exception;
    const-string v13, "ProxoidService"

    const-string v14, ""

    invoke-static {v13, v14, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 92
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    # getter for: Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;
    invoke-static {v13}, Lcom/proxoid/ProxoidService;->access$300(Lcom/proxoid/ProxoidService;)Lcom/mba/proxylight/ProxyLight;

    move-result-object v13

    invoke-virtual {v13}, Lcom/mba/proxylight/ProxyLight;->stop()V

    .line 93
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    const/4 v14, 0x0

    # setter for: Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;
    invoke-static {v13, v14}, Lcom/proxoid/ProxoidService;->access$302(Lcom/proxoid/ProxoidService;Lcom/mba/proxylight/ProxyLight;)Lcom/mba/proxylight/ProxyLight;

    .line 94
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 103
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v5

    .line 104
    .restart local v5    # "e":Ljava/lang/Exception;
    const-string v13, "ProxoidService"

    const-string v14, ""

    invoke-static {v13, v14, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 105
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    # getter for: Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;
    invoke-static {v13}, Lcom/proxoid/ProxoidService;->access$300(Lcom/proxoid/ProxoidService;)Lcom/mba/proxylight/ProxyLight;

    move-result-object v13

    invoke-virtual {v13}, Lcom/mba/proxylight/ProxyLight;->stop()V

    .line 106
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/proxoid/ProxoidService$1;->this$0:Lcom/proxoid/ProxoidService;

    const/4 v14, 0x0

    # setter for: Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;
    invoke-static {v13, v14}, Lcom/proxoid/ProxoidService;->access$302(Lcom/proxoid/ProxoidService;Lcom/mba/proxylight/ProxyLight;)Lcom/mba/proxylight/ProxyLight;

    .line 107
    const/4 v13, 0x0

    goto/16 :goto_1
.end method
