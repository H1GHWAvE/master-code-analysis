.class public Lcom/proxoid/Proxoid;
.super Ljava/lang/Object;
.source "Proxoid.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# static fields
.field protected static final KEY_ONOFF:Ljava/lang/String; = "onoff"

.field protected static final KEY_PORT:Ljava/lang/String; = "port"

.field protected static final KEY_PREFS:Ljava/lang/String; = "proxoidv6"

.field protected static final KEY_USERAGENT:Ljava/lang/String; = "useragent"

.field private static final TAG:Ljava/lang/String; = "proxoid"

.field protected static final USERAGENT_ASIS:Ljava/lang/String; = "asis"

.field protected static final USERAGENT_RANDOM:Ljava/lang/String; = "random"

.field protected static final USERAGENT_REMOVE:Ljava/lang/String; = "remove"

.field protected static final USERAGENT_REPLACE:Ljava/lang/String; = "replace"


# instance fields
.field public mContext:Landroid/content/Context;

.field private proxoidControl:Lcom/proxoid/IProxoidControl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v2, p0, Lcom/proxoid/Proxoid;->proxoidControl:Lcom/proxoid/IProxoidControl;

    .line 27
    iput-object v2, p0, Lcom/proxoid/Proxoid;->mContext:Landroid/content/Context;

    .line 29
    iput-object p1, p0, Lcom/proxoid/Proxoid;->mContext:Landroid/content/Context;

    .line 30
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/proxoid/ProxoidService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 31
    .local v1, "svc":Landroid/content/Intent;
    invoke-virtual {p1, v1, p0, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 32
    invoke-direct {p0}, Lcom/proxoid/Proxoid;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 33
    .local v0, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "onoff"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 34
    return-void
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/proxoid/Proxoid;->mContext:Landroid/content/Context;

    const-string v1, "proxoidv6"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "cn"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 39
    check-cast p2, Lcom/proxoid/IProxoidControl;

    .end local p2    # "binder":Landroid/os/IBinder;
    iput-object p2, p0, Lcom/proxoid/Proxoid;->proxoidControl:Lcom/proxoid/IProxoidControl;

    .line 40
    iget-object v1, p0, Lcom/proxoid/Proxoid;->proxoidControl:Lcom/proxoid/IProxoidControl;

    if-eqz v1, :cond_0

    .line 42
    :try_start_0
    iget-object v1, p0, Lcom/proxoid/Proxoid;->proxoidControl:Lcom/proxoid/IProxoidControl;

    invoke-interface {v1}, Lcom/proxoid/IProxoidControl;->update()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :cond_0
    :goto_0
    return-void

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "cn"    # Landroid/content/ComponentName;

    .prologue
    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/proxoid/Proxoid;->proxoidControl:Lcom/proxoid/IProxoidControl;

    .line 52
    return-void
.end method
