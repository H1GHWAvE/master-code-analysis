.class public Lcom/proxoid/ProxoidService;
.super Landroid/app/Service;
.source "ProxoidService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/proxoid/ProxoidService$UserAgentRequestFilter;
    }
.end annotation


# static fields
.field private static ID:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ProxoidService"


# instance fields
.field private proxy:Lcom/mba/proxylight/ProxyLight;

.field private randomUserAgent:Ljava/lang/String;

.field private useragent:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const v0, 0x7f02002f

    sput v0, Lcom/proxoid/ProxoidService;->ID:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;

    .line 27
    iput-object v0, p0, Lcom/proxoid/ProxoidService;->useragent:Ljava/lang/String;

    .line 29
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const/16 v2, 0x14

    invoke-static {v0, v1, v2}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/proxoid/ProxoidService;->randomUserAgent:Ljava/lang/String;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/proxoid/ProxoidService;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/proxoid/ProxoidService;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/proxoid/ProxoidService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$102(Lcom/proxoid/ProxoidService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/proxoid/ProxoidService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/proxoid/ProxoidService;->useragent:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/proxoid/ProxoidService;)V
    .locals 0
    .param p0, "x0"    # Lcom/proxoid/ProxoidService;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/proxoid/ProxoidService;->doStop()V

    return-void
.end method

.method static synthetic access$300(Lcom/proxoid/ProxoidService;)Lcom/mba/proxylight/ProxyLight;
    .locals 1
    .param p0, "x0"    # Lcom/proxoid/ProxoidService;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;

    return-object v0
.end method

.method static synthetic access$302(Lcom/proxoid/ProxoidService;Lcom/mba/proxylight/ProxyLight;)Lcom/mba/proxylight/ProxyLight;
    .locals 0
    .param p0, "x0"    # Lcom/proxoid/ProxoidService;
    .param p1, "x1"    # Lcom/mba/proxylight/ProxyLight;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;

    return-object p1
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 21
    sget v0, Lcom/proxoid/ProxoidService;->ID:I

    return v0
.end method

.method private doStop()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 148
    iget-object v3, p0, Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;

    invoke-virtual {v3}, Lcom/mba/proxylight/ProxyLight;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 149
    const-string v3, "ProxoidService"

    const-string v4, "stopping"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v3, p0, Lcom/proxoid/ProxoidService;->proxy:Lcom/mba/proxylight/ProxyLight;

    invoke-virtual {v3}, Lcom/mba/proxylight/ProxyLight;->stop()V

    .line 151
    const-string v3, "notification"

    invoke-virtual {p0, v3}, Lcom/proxoid/ProxoidService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 152
    .local v1, "mNotificationManager":Landroid/app/NotificationManager;
    sget v3, Lcom/proxoid/ProxoidService;->ID:I

    invoke-virtual {v1, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 153
    const-string v3, "Proxy stopped."

    invoke-static {p0, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 155
    invoke-direct {p0}, Lcom/proxoid/ProxoidService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 156
    .local v2, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 157
    .local v0, "e":Landroid/content/SharedPreferences$Editor;
    const-string v3, "onoff"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 158
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 160
    .end local v0    # "e":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "mNotificationManager":Landroid/app/NotificationManager;
    .end local v2    # "sp":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 48
    const-string v0, "proxoidv6"

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Landroid/app/Service;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "binder"    # Landroid/content/Intent;

    .prologue
    .line 54
    new-instance v0, Lcom/proxoid/ProxoidService$1;

    invoke-direct {v0, p0}, Lcom/proxoid/ProxoidService$1;-><init>(Lcom/proxoid/ProxoidService;)V

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 137
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 139
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/proxoid/ProxoidService;->doStop()V

    .line 144
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 145
    return-void
.end method
