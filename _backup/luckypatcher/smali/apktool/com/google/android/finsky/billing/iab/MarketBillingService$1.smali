.class Lcom/google/android/finsky/billing/iab/MarketBillingService$1;
.super Lcom/android/vending/billing/IMarketBillingService$Stub;
.source "MarketBillingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/iab/MarketBillingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field request_id:J

.field final synthetic this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/iab/MarketBillingService;)V
    .locals 2
    .param p1, "this$0"    # Lcom/google/android/finsky/billing/iab/MarketBillingService;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-direct {p0}, Lcom/android/vending/billing/IMarketBillingService$Stub;-><init>()V

    .line 97
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->request_id:J

    return-void
.end method

.method private getNextInAppRequestId()J
    .locals 4

    .prologue
    .line 336
    # getter for: Lcom/google/android/finsky/billing/iab/MarketBillingService;->sRandom:Ljava/util/Random;
    invoke-static {}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$200()Ljava/util/Random;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    const-wide v2, 0x7fffffffffffffffL

    and-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->request_id:J

    .line 337
    iget-wide v0, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->request_id:J

    return-wide v0
.end method

.method private updateWithRequestId(Landroid/os/Bundle;J)I
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "requestId"    # J

    .prologue
    .line 345
    const-string v1, "REQUEST_ID"

    invoke-virtual {p1, v1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 346
    const/4 v0, 0x0

    .line 349
    .local v0, "v0":I
    return v0
.end method


# virtual methods
.method public confirmNotifications(Ljava/lang/String;[Ljava/lang/String;)J
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "notifyIds"    # [Ljava/lang/String;

    .prologue
    .line 330
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->getNextInAppRequestId()J

    move-result-wide v0

    .line 333
    .local v0, "v3":J
    return-wide v0
.end method

.method public restoreTransactions(Ljava/lang/String;J)J
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "nonce"    # J

    .prologue
    .line 324
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->getNextInAppRequestId()J

    move-result-wide v0

    .line 326
    .local v0, "v3":J
    return-wide v0
.end method

.method public sendBillingRequest(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 39
    .param p1, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 101
    const-string v31, "BILLING_REQUEST"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 102
    .local v25, "request":Ljava/lang/String;
    const-string v31, "API_VERSION"

    const/16 v32, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 103
    .local v6, "api_version":I
    const-string v31, "PACKAGE_NAME"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 104
    .local v22, "package_name":Ljava/lang/String;
    const-string v31, "DEVELOPER_PAYLOAD"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 105
    .local v10, "dev_payload":Ljava/lang/String;
    const-string v31, "ITEM_ID"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 106
    .local v14, "item_id":Ljava/lang/String;
    const-string v31, "NONCE"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v16

    .line 107
    .local v16, "nonce":J
    const-string v31, "NOTIFY_IDS"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 108
    .local v18, "notify_ids":[Ljava/lang/String;
    if-eqz v14, :cond_0

    # setter for: Lcom/google/android/finsky/billing/iab/MarketBillingService;->item:Ljava/lang/String;
    invoke-static {v14}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$002(Ljava/lang/String;)Ljava/lang/String;

    .line 109
    :cond_0
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 110
    .local v8, "bundle":Landroid/os/Bundle;
    const/16 v30, 0x0

    .line 120
    .local v30, "v15":I
    const-string v20, ""

    .line 121
    .local v20, "pData":Ljava/lang/String;
    const-string v31, "CHECK_BILLING_SUPPORTED"

    move-object/from16 v0, v31

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_1

    .line 122
    sget-object v31, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v32, "CHECK_BILLING_SUPPORTED"

    invoke-virtual/range {v31 .. v32}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 123
    const-string v31, "RESPONSE_CODE"

    const/16 v32, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 320
    :goto_0
    return-object v8

    .line 128
    :cond_1
    const-string v31, "REQUEST_PURCHASE"

    move-object/from16 v0, v31

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_7

    .line 129
    sget-object v31, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v32, "REQUEST_PURCHASE"

    invoke-virtual/range {v31 .. v32}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 130
    const/16 v29, 0x0

    .line 132
    .local v29, "v0":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v31

    const/16 v32, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v29

    .line 136
    :goto_1
    const/4 v13, 0x0

    .line 137
    .local v13, "intent":Landroid/content/Intent;
    if-nez v29, :cond_3

    .line 170
    :cond_2
    const-string v31, "PURCHASE_INTENT"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    const/high16 v34, 0x40000000    # 2.0f

    move-object/from16 v0, v32

    move/from16 v1, v33

    move/from16 v2, v34

    invoke-static {v0, v1, v13, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v32

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 177
    const-string v31, "RESPONSE_CODE"

    const/16 v32, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 133
    .end local v13    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v11

    .line 134
    .local v11, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v11}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 140
    .end local v11    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v13    # "intent":Landroid/content/Intent;
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->getNextInAppRequestId()J

    move-result-wide v26

    .line 141
    .local v26, "request_id":J
    const-string v31, "REQUEST_ID"

    move-object/from16 v0, v31

    move-wide/from16 v1, v26

    invoke-virtual {v8, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 142
    new-instance v13, Landroid/content/Intent;

    .end local v13    # "intent":Landroid/content/Intent;
    const-string v31, "android.intent.action.VIEW"

    move-object/from16 v0, v31

    invoke-direct {v13, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 143
    .restart local v13    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    move-object/from16 v31, v0

    const-class v32, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 144
    const-string v31, "assetid"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "inapp:"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, ":"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    const-string v31, "asset_package"

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    # setter for: Lcom/google/android/finsky/billing/iab/MarketBillingService;->item:Ljava/lang/String;
    invoke-static {v14}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$002(Ljava/lang/String;)Ljava/lang/String;

    .line 147
    if-eqz v10, :cond_6

    # setter for: Lcom/google/android/finsky/billing/iab/MarketBillingService;->dev_pay:Ljava/lang/String;
    invoke-static {v10}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$102(Ljava/lang/String;)Ljava/lang/String;

    .line 149
    :goto_2
    const-string v31, "asset_version_code"

    move-object/from16 v0, v29

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    move/from16 v32, v0

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 150
    const-string v31, "request_id"

    move-object/from16 v0, v31

    move-wide/from16 v1, v26

    invoke-virtual {v13, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 151
    const-string v31, "packageName"

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    const-string v31, "product"

    move-object/from16 v0, v31

    invoke-virtual {v13, v0, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    const-string v31, "payload"

    move-object/from16 v0, v31

    invoke-virtual {v13, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    if-eqz v10, :cond_4

    .line 155
    const-string v31, "developer_payload"

    move-object/from16 v0, v31

    invoke-virtual {v13, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 157
    :cond_4
    new-instance v9, Lcom/google/android/finsky/billing/iab/DbHelper;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-direct {v9, v0, v1}, Lcom/google/android/finsky/billing/iab/DbHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 158
    .local v9, "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    invoke-virtual {v9}, Lcom/google/android/finsky/billing/iab/DbHelper;->getItems()Ljava/util/ArrayList;

    move-result-object v15

    .line 159
    .local v15, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/billing/iab/ItemsListItem;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v31

    if-eqz v31, :cond_2

    .line 160
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v31

    :cond_5
    :goto_3
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_2

    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/billing/iab/ItemsListItem;

    .line 161
    .local v5, "aList":Lcom/google/android/finsky/billing/iab/ItemsListItem;
    iget-object v0, v5, Lcom/google/android/finsky/billing/iab/ItemsListItem;->pData:Ljava/lang/String;

    move-object/from16 v32, v0

    const-string v33, "auto.repeat.LP"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_5

    iget-object v0, v5, Lcom/google/android/finsky/billing/iab/ItemsListItem;->itemID:Ljava/lang/String;

    move-object/from16 v32, v0

    .line 162
    move-object/from16 v0, v32

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_5

    .line 163
    const-string v32, "autorepeat"

    iget-object v0, v5, Lcom/google/android/finsky/billing/iab/ItemsListItem;->pSignature:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3

    .line 148
    .end local v5    # "aList":Lcom/google/android/finsky/billing/iab/ItemsListItem;
    .end local v9    # "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    .end local v15    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/billing/iab/ItemsListItem;>;"
    :cond_6
    const-string v31, ""

    # setter for: Lcom/google/android/finsky/billing/iab/MarketBillingService;->dev_pay:Ljava/lang/String;
    invoke-static/range {v31 .. v31}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$102(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_2

    .line 181
    .end local v13    # "intent":Landroid/content/Intent;
    .end local v26    # "request_id":J
    .end local v29    # "v0":Landroid/content/pm/PackageInfo;
    :cond_7
    const-string v31, "RESTORE_TRANSACTIONS"

    move-object/from16 v0, v31

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_e

    .line 182
    sget-object v31, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v32, "RESTORE_TRANSACTIONS"

    invoke-virtual/range {v31 .. v32}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 184
    new-instance v9, Lcom/google/android/finsky/billing/iab/DbHelper;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-direct {v9, v0, v1}, Lcom/google/android/finsky/billing/iab/DbHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 185
    .restart local v9    # "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    invoke-virtual {v9}, Lcom/google/android/finsky/billing/iab/DbHelper;->getItems()Ljava/util/ArrayList;

    move-result-object v15

    .line 186
    .restart local v15    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/billing/iab/ItemsListItem;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v31

    if-eqz v31, :cond_c

    .line 187
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 188
    .local v7, "base":Lorg/json/JSONObject;
    new-instance v24, Lorg/json/JSONArray;

    invoke-direct/range {v24 .. v24}, Lorg/json/JSONArray;-><init>()V

    .line 189
    .local v24, "purch_list":Lorg/json/JSONArray;
    const-string v28, ""

    .line 193
    .local v28, "signature":Ljava/lang/String;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_4
    :try_start_1
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v31

    move/from16 v0, v31

    if-ge v12, v0, :cond_a

    .line 194
    invoke-virtual {v15, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/google/android/finsky/billing/iab/ItemsListItem;

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/ItemsListItem;->pData:Ljava/lang/String;

    move-object/from16 v31, v0

    const-string v32, "auto.repeat.LP"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-nez v31, :cond_8

    .line 195
    new-instance v32, Lorg/json/JSONObject;

    invoke-virtual {v15, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/google/android/finsky/billing/iab/ItemsListItem;

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/ItemsListItem;->pData:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 197
    invoke-virtual {v15, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/google/android/finsky/billing/iab/ItemsListItem;

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/ItemsListItem;->pSignature:Ljava/lang/String;

    move-object/from16 v31, v0

    const-string v32, "1"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_9

    .line 198
    const-string v28, "1"

    .line 193
    :cond_8
    :goto_5
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    .line 199
    :cond_9
    const-string v28, ""

    goto :goto_5

    .line 203
    :cond_a
    const-string v31, "nonce"

    move-object/from16 v0, v31

    move-wide/from16 v1, v16

    invoke-virtual {v7, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 204
    const-string v31, "orders"

    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 205
    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v20

    .line 206
    const-string v31, "1"

    move-object/from16 v0, v28

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_b

    invoke-static/range {v20 .. v20}, Lcom/chelpus/Utils;->gen_sha1withrsa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 207
    :cond_b
    sget-object v31, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 215
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mNotifier:Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    move-object/from16 v3, v28

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->sendPurchaseStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 218
    .end local v7    # "base":Lorg/json/JSONObject;
    .end local v12    # "i":I
    .end local v24    # "purch_list":Lorg/json/JSONArray;
    .end local v28    # "signature":Ljava/lang/String;
    :cond_c
    const-string v31, "com.dynamixsoftware.printhand"

    move-object/from16 v0, v22

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_d

    .line 224
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 225
    .restart local v7    # "base":Lorg/json/JSONObject;
    new-instance v24, Lorg/json/JSONArray;

    invoke-direct/range {v24 .. v24}, Lorg/json/JSONArray;-><init>()V

    .line 226
    .restart local v24    # "purch_list":Lorg/json/JSONArray;
    new-instance v23, Lorg/json/JSONObject;

    invoke-direct/range {v23 .. v23}, Lorg/json/JSONObject;-><init>()V

    .line 228
    .local v23, "purch":Lorg/json/JSONObject;
    :try_start_2
    const-string v31, "nonce"

    move-object/from16 v0, v31

    move-wide/from16 v1, v16

    invoke-virtual {v7, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 229
    const-string v31, "notificationId"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, ""

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-wide v33, 0xde0b6b3a7640000L

    const-wide v35, 0x7fffffffffffffffL

    invoke-static/range {v33 .. v36}, Lcom/chelpus/Utils;->getRandom(JJ)J

    move-result-wide v33

    invoke-virtual/range {v32 .. v34}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 230
    const-string v31, "orderId"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, ""

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-wide v33, 0xde0b6b3a7640000L

    const-wide v35, 0x7fffffffffffffffL

    invoke-static/range {v33 .. v36}, Lcom/chelpus/Utils;->getRandom(JJ)J

    move-result-wide v33

    invoke-virtual/range {v32 .. v34}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-wide/16 v33, 0x0

    const-wide/16 v35, 0x9

    invoke-static/range {v33 .. v36}, Lcom/chelpus/Utils;->getRandom(JJ)J

    move-result-wide v33

    invoke-virtual/range {v32 .. v34}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "."

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-wide v33, 0x38d7ea4c68000L

    const-wide v35, 0x2386f26fc0ffffL

    invoke-static/range {v33 .. v36}, Lcom/chelpus/Utils;->getRandom(JJ)J

    move-result-wide v33

    invoke-virtual/range {v32 .. v34}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 231
    const-string v31, "packageName"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 232
    const-string v31, "productId"

    const-string v32, "printhand.premium"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 233
    const-string v31, "purchaseTime"

    new-instance v32, Ljava/lang/Long;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v33

    invoke-direct/range {v32 .. v34}, Ljava/lang/Long;-><init>(J)V

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 234
    const-string v31, "purchaseState"

    new-instance v32, Ljava/lang/Integer;

    const/16 v33, 0x0

    invoke-direct/range {v32 .. v33}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 235
    const-string v31, "developerPayload"

    # getter for: Lcom/google/android/finsky/billing/iab/MarketBillingService;->dev_pay:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$100()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 236
    const-string v31, "purchaseToken"

    const/16 v32, 0x18

    invoke-static/range {v32 .. v32}, Lcom/chelpus/Utils;->getRandomStringLowerCase(I)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 237
    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 238
    const-string v31, "orders"

    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 242
    :goto_7
    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v20

    .line 243
    invoke-static/range {v20 .. v20}, Lcom/chelpus/Utils;->gen_sha1withrsa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 244
    .restart local v28    # "signature":Ljava/lang/String;
    sget-object v31, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mNotifier:Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    move-object/from16 v3, v28

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->sendPurchaseStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 247
    .end local v7    # "base":Lorg/json/JSONObject;
    .end local v23    # "purch":Lorg/json/JSONObject;
    .end local v24    # "purch_list":Lorg/json/JSONArray;
    .end local v28    # "signature":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mNotifier:Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->request_id:J

    move-wide/from16 v32, v0

    const/16 v34, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    move-wide/from16 v2, v32

    move/from16 v4, v34

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->sendResponseCode(Ljava/lang/String;JI)Z

    .line 248
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-wide/from16 v2, v16

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->restoreTransactions(Ljava/lang/String;J)J

    move-result-wide v31

    move-object/from16 v0, p0

    move-wide/from16 v1, v31

    invoke-direct {v0, v8, v1, v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->updateWithRequestId(Landroid/os/Bundle;J)I

    move-result v30

    .line 249
    const-string v31, "RESPONSE_CODE"

    move-object/from16 v0, v31

    move/from16 v1, v30

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 208
    .restart local v7    # "base":Lorg/json/JSONObject;
    .restart local v12    # "i":I
    .restart local v24    # "purch_list":Lorg/json/JSONArray;
    .restart local v28    # "signature":Ljava/lang/String;
    :catch_1
    move-exception v11

    .line 209
    .local v11, "e":Lorg/json/JSONException;
    invoke-virtual {v11}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_6

    .line 239
    .end local v11    # "e":Lorg/json/JSONException;
    .end local v12    # "i":I
    .end local v28    # "signature":Ljava/lang/String;
    .restart local v23    # "purch":Lorg/json/JSONObject;
    :catch_2
    move-exception v11

    .line 240
    .restart local v11    # "e":Lorg/json/JSONException;
    invoke-virtual {v11}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_7

    .line 252
    .end local v7    # "base":Lorg/json/JSONObject;
    .end local v9    # "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    .end local v11    # "e":Lorg/json/JSONException;
    .end local v15    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/billing/iab/ItemsListItem;>;"
    .end local v23    # "purch":Lorg/json/JSONObject;
    .end local v24    # "purch_list":Lorg/json/JSONArray;
    :cond_e
    const-string v31, "GET_PURCHASE_INFORMATION"

    move-object/from16 v0, v31

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_13

    .line 253
    sget-object v31, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v32, "GET_PURCHASE_INFORMATION"

    invoke-virtual/range {v31 .. v32}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 258
    const-wide/16 v31, 0x0

    const-wide/32 v33, 0x7fffffff

    invoke-static/range {v31 .. v34}, Lcom/chelpus/Utils;->getRandom(JJ)J

    move-result-wide v31

    move-wide/from16 v0, v31

    long-to-int v0, v0

    move/from16 v19, v0

    .line 262
    .local v19, "order":I
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 263
    .restart local v7    # "base":Lorg/json/JSONObject;
    new-instance v24, Lorg/json/JSONArray;

    invoke-direct/range {v24 .. v24}, Lorg/json/JSONArray;-><init>()V

    .line 264
    .restart local v24    # "purch_list":Lorg/json/JSONArray;
    new-instance v23, Lorg/json/JSONObject;

    invoke-direct/range {v23 .. v23}, Lorg/json/JSONObject;-><init>()V

    .line 266
    .restart local v23    # "purch":Lorg/json/JSONObject;
    :try_start_3
    const-string v31, "nonce"

    move-object/from16 v0, v31

    move-wide/from16 v1, v16

    invoke-virtual {v7, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 267
    const-string v31, "notificationId"

    const/16 v32, 0x0

    aget-object v32, v18, v32

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 268
    const-string v31, "orderId"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-wide v33, 0xde0b6b3a7640000L

    const-wide v35, 0x7fffffffffffffffL

    invoke-static/range {v33 .. v36}, Lcom/chelpus/Utils;->getRandom(JJ)J

    move-result-wide v33

    const-wide/16 v35, 0x0

    const-wide/16 v37, 0x9

    invoke-static/range {v35 .. v38}, Lcom/chelpus/Utils;->getRandom(JJ)J

    move-result-wide v35

    add-long v33, v33, v35

    invoke-virtual/range {v32 .. v34}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "."

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-wide v33, 0x38d7ea4c68000L

    const-wide v35, 0x2386f26fc0ffffL

    invoke-static/range {v33 .. v36}, Lcom/chelpus/Utils;->getRandom(JJ)J

    move-result-wide v33

    invoke-virtual/range {v32 .. v34}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 269
    const-string v31, "packageName"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 270
    const-string v31, "productId"

    # getter for: Lcom/google/android/finsky/billing/iab/MarketBillingService;->item:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$000()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 271
    const-string v31, "purchaseTime"

    new-instance v32, Ljava/lang/Long;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v33

    invoke-direct/range {v32 .. v34}, Ljava/lang/Long;-><init>(J)V

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 272
    const-string v31, "purchaseState"

    new-instance v32, Ljava/lang/Integer;

    const/16 v33, 0x0

    invoke-direct/range {v32 .. v33}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 273
    const-string v31, "developerPayload"

    # getter for: Lcom/google/android/finsky/billing/iab/MarketBillingService;->dev_pay:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$100()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 274
    const-string v31, "purchaseToken"

    const/16 v32, 0x18

    invoke-static/range {v32 .. v32}, Lcom/chelpus/Utils;->getRandomStringLowerCase(I)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 275
    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 276
    const-string v31, "orders"

    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    .line 286
    :goto_8
    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v20

    .line 288
    const-string v28, ""

    .line 289
    .restart local v28    # "signature":Ljava/lang/String;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v31

    const-string v32, "config"

    const/16 v33, 0x4

    invoke-virtual/range {v31 .. v33}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v31

    const-string v32, "UnSign"

    const/16 v33, 0x0

    invoke-interface/range {v31 .. v33}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v31

    if-nez v31, :cond_f

    .line 290
    sget-object v31, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v32, "send sign"

    invoke-virtual/range {v31 .. v32}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 291
    invoke-static/range {v20 .. v20}, Lcom/chelpus/Utils;->gen_sha1withrsa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 293
    :cond_f
    const-string v21, ""

    .line 294
    .local v21, "pSignatureSave":Ljava/lang/String;
    const-string v31, ""

    move-object/from16 v0, v28

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-nez v31, :cond_10

    const-string v21, "1"

    .line 296
    :cond_10
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v31

    const-string v32, "config"

    const/16 v33, 0x4

    invoke-virtual/range {v31 .. v33}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v31

    const-string v32, "SavePurchase"

    const/16 v33, 0x0

    invoke-interface/range {v31 .. v33}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v31

    if-eqz v31, :cond_11

    .line 298
    new-instance v9, Lcom/google/android/finsky/billing/iab/DbHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-direct {v9, v0, v1}, Lcom/google/android/finsky/billing/iab/DbHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 299
    .restart local v9    # "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    new-instance v31, Lcom/google/android/finsky/billing/iab/ItemsListItem;

    # getter for: Lcom/google/android/finsky/billing/iab/MarketBillingService;->item:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$000()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v23 .. v23}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, v33

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/billing/iab/ItemsListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/google/android/finsky/billing/iab/DbHelper;->saveItem(Lcom/google/android/finsky/billing/iab/ItemsListItem;)V

    .line 301
    .end local v9    # "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    :cond_11
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v31

    const-string v32, "config"

    const/16 v33, 0x4

    invoke-virtual/range {v31 .. v33}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v31

    const-string v32, "AutoRepeat"

    const/16 v33, 0x0

    invoke-interface/range {v31 .. v33}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v31

    if-eqz v31, :cond_12

    .line 303
    new-instance v9, Lcom/google/android/finsky/billing/iab/DbHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-direct {v9, v0, v1}, Lcom/google/android/finsky/billing/iab/DbHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 304
    .restart local v9    # "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    new-instance v31, Lcom/google/android/finsky/billing/iab/ItemsListItem;

    # getter for: Lcom/google/android/finsky/billing/iab/MarketBillingService;->item:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$000()Ljava/lang/String;

    move-result-object v32

    const-string v33, "auto.repeat.LP"

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, v33

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/billing/iab/ItemsListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Lcom/google/android/finsky/billing/iab/DbHelper;->saveItem(Lcom/google/android/finsky/billing/iab/ItemsListItem;)V

    .line 306
    .end local v9    # "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mNotifier:Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    move-object/from16 v3, v28

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->sendPurchaseStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mNotifier:Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->request_id:J

    move-wide/from16 v32, v0

    const/16 v34, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    move-wide/from16 v2, v32

    move/from16 v4, v34

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->sendResponseCode(Ljava/lang/String;JI)Z

    .line 308
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-wide/from16 v2, v16

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->restoreTransactions(Ljava/lang/String;J)J

    move-result-wide v31

    move-object/from16 v0, p0

    move-wide/from16 v1, v31

    invoke-direct {v0, v8, v1, v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->updateWithRequestId(Landroid/os/Bundle;J)I

    .line 309
    const-string v31, "RESPONSE_CODE"

    const/16 v32, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 277
    .end local v21    # "pSignatureSave":Ljava/lang/String;
    .end local v28    # "signature":Ljava/lang/String;
    :catch_3
    move-exception v11

    .line 278
    .restart local v11    # "e":Lorg/json/JSONException;
    invoke-virtual {v11}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_8

    .line 312
    .end local v7    # "base":Lorg/json/JSONObject;
    .end local v11    # "e":Lorg/json/JSONException;
    .end local v19    # "order":I
    .end local v23    # "purch":Lorg/json/JSONObject;
    .end local v24    # "purch_list":Lorg/json/JSONArray;
    :cond_13
    const-string v31, "CONFIRM_NOTIFICATIONS"

    move-object/from16 v0, v31

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_14

    .line 313
    sget-object v31, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v32, "CONFIRM_NOTIFICATIONS"

    invoke-virtual/range {v31 .. v32}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 314
    const-string v31, "RESPONSE_CODE"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->confirmNotifications(Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v32

    move-object/from16 v0, p0

    move-wide/from16 v1, v32

    invoke-direct {v0, v8, v1, v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->updateWithRequestId(Landroid/os/Bundle;J)I

    move-result v32

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mNotifier:Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->request_id:J

    move-wide/from16 v32, v0

    const/16 v34, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    move-wide/from16 v2, v32

    move/from16 v4, v34

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->sendResponseCode(Ljava/lang/String;JI)Z

    goto/16 :goto_0

    .line 319
    :cond_14
    const-string v31, "RESPONSE_CODE"

    const/16 v32, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0
.end method
