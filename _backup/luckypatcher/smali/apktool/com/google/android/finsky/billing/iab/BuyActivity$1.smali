.class Lcom/google/android/finsky/billing/iab/BuyActivity$1;
.super Ljava/lang/Object;
.source "BuyActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/iab/BuyActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/iab/BuyActivity;

.field final synthetic val$check:Landroid/widget/CheckBox;

.field final synthetic val$check2:Landroid/widget/CheckBox;

.field final synthetic val$check3:Landroid/widget/CheckBox;

.field final synthetic val$pData:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/iab/BuyActivity;Landroid/widget/CheckBox;Ljava/lang/String;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/finsky/billing/iab/BuyActivity;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->this$0:Lcom/google/android/finsky/billing/iab/BuyActivity;

    iput-object p2, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->val$check:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->val$pData:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->val$check2:Landroid/widget/CheckBox;

    iput-object p5, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->val$check3:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 116
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 117
    .local v0, "data":Landroid/content/Intent;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 118
    .local v2, "extras":Landroid/os/Bundle;
    const-string v4, ""

    .line 119
    .local v4, "signature":Ljava/lang/String;
    const-string v3, ""

    .line 120
    .local v3, "save_signature":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->val$check:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-nez v5, :cond_2

    .line 121
    iget-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->val$pData:Ljava/lang/String;

    invoke-static {v5}, Lcom/chelpus/Utils;->gen_sha1withrsa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 122
    const-string v3, "1"

    .line 127
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->val$check2:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 128
    new-instance v1, Lcom/google/android/finsky/billing/iab/DbHelper;

    iget-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->this$0:Lcom/google/android/finsky/billing/iab/BuyActivity;

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/iab/BuyActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->this$0:Lcom/google/android/finsky/billing/iab/BuyActivity;

    iget-object v6, v6, Lcom/google/android/finsky/billing/iab/BuyActivity;->packageName:Ljava/lang/String;

    invoke-direct {v1, v5, v6}, Lcom/google/android/finsky/billing/iab/DbHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 129
    .local v1, "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    new-instance v5, Lcom/google/android/finsky/billing/iab/ItemsListItem;

    iget-object v6, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->this$0:Lcom/google/android/finsky/billing/iab/BuyActivity;

    iget-object v6, v6, Lcom/google/android/finsky/billing/iab/BuyActivity;->productId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->val$pData:Ljava/lang/String;

    invoke-direct {v5, v6, v7, v3}, Lcom/google/android/finsky/billing/iab/ItemsListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Lcom/google/android/finsky/billing/iab/DbHelper;->saveItem(Lcom/google/android/finsky/billing/iab/ItemsListItem;)V

    .line 131
    .end local v1    # "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->val$check3:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 132
    new-instance v1, Lcom/google/android/finsky/billing/iab/DbHelper;

    iget-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->this$0:Lcom/google/android/finsky/billing/iab/BuyActivity;

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/iab/BuyActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->this$0:Lcom/google/android/finsky/billing/iab/BuyActivity;

    iget-object v6, v6, Lcom/google/android/finsky/billing/iab/BuyActivity;->packageName:Ljava/lang/String;

    invoke-direct {v1, v5, v6}, Lcom/google/android/finsky/billing/iab/DbHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 133
    .restart local v1    # "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    new-instance v5, Lcom/google/android/finsky/billing/iab/ItemsListItem;

    iget-object v6, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->this$0:Lcom/google/android/finsky/billing/iab/BuyActivity;

    iget-object v6, v6, Lcom/google/android/finsky/billing/iab/BuyActivity;->productId:Ljava/lang/String;

    const-string v7, "auto.repeat.LP"

    invoke-direct {v5, v6, v7, v3}, Lcom/google/android/finsky/billing/iab/ItemsListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Lcom/google/android/finsky/billing/iab/DbHelper;->saveItem(Lcom/google/android/finsky/billing/iab/ItemsListItem;)V

    .line 135
    .end local v1    # "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    :cond_1
    const-string v5, "RESPONSE_CODE"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 136
    const-string v5, "INAPP_PURCHASE_DATA"

    iget-object v6, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->val$pData:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v5, "INAPP_DATA_SIGNATURE"

    invoke-virtual {v2, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 139
    iget-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->this$0:Lcom/google/android/finsky/billing/iab/BuyActivity;

    const/4 v6, -0x1

    invoke-virtual {v5, v6, v0}, Lcom/google/android/finsky/billing/iab/BuyActivity;->setResult(ILandroid/content/Intent;)V

    .line 141
    iget-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyActivity$1;->this$0:Lcom/google/android/finsky/billing/iab/BuyActivity;

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/iab/BuyActivity;->finish()V

    .line 143
    return-void

    .line 124
    :cond_2
    const-string v3, ""

    .line 125
    const-string v4, ""

    goto :goto_0
.end method
