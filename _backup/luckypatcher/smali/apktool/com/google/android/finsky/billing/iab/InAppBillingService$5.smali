.class Lcom/google/android/finsky/billing/iab/InAppBillingService$5;
.super Ljava/lang/Object;
.source "InAppBillingService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/iab/InAppBillingService;->connectToBilling(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

.field final synthetic val$pkgName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/finsky/billing/iab/InAppBillingService;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    iput-object p2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->val$pkgName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 167
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Billing service try to connect."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 168
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-static {p2}, Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

    .line 174
    :try_start_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    iget-object v2, v2, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->val$pkgName:Ljava/lang/String;

    const-string v5, "inapp"

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;->isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 175
    .local v1, "response":I
    if-eqz v1, :cond_0

    .line 178
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(I)V

    .line 179
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/finsky/billing/iab/InAppBillingService;->skipSetupDone:Z

    .line 204
    .end local v1    # "response":I
    :goto_0
    return-void

    .line 185
    .restart local v1    # "response":I
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    iget-object v2, v2, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->val$pkgName:Ljava/lang/String;

    const-string v5, "subs"

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;->isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 186
    if-nez v1, :cond_1

    .line 195
    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Billing service connected."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 196
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mSetupDone:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 199
    .end local v1    # "response":I
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 159
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Billing service disconnected."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

    .line 161
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mSetupDone:Z

    .line 162
    return-void
.end method
