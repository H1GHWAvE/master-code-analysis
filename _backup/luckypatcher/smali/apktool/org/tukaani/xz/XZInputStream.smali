.class public Lorg/tukaani/xz/XZInputStream;
.super Ljava/io/InputStream;
.source "XZInputStream.java"


# instance fields
.field private endReached:Z

.field private exception:Ljava/io/IOException;

.field private in:Ljava/io/InputStream;

.field private final memoryLimit:I

.field private final tempBuf:[B

.field private xzIn:Lorg/tukaani/xz/SingleXZInputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lorg/tukaani/xz/XZInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 100
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "memoryLimit"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/tukaani/xz/XZInputStream;->endReached:Z

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/tukaani/xz/XZInputStream;->exception:Ljava/io/IOException;

    .line 70
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/tukaani/xz/XZInputStream;->tempBuf:[B

    .line 132
    iput-object p1, p0, Lorg/tukaani/xz/XZInputStream;->in:Ljava/io/InputStream;

    .line 133
    iput p2, p0, Lorg/tukaani/xz/XZInputStream;->memoryLimit:I

    .line 134
    new-instance v0, Lorg/tukaani/xz/SingleXZInputStream;

    invoke-direct {v0, p1, p2}, Lorg/tukaani/xz/SingleXZInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lorg/tukaani/xz/XZInputStream;->xzIn:Lorg/tukaani/xz/SingleXZInputStream;

    .line 135
    return-void
.end method

.method private prepareNextStream()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 242
    new-instance v2, Ljava/io/DataInputStream;

    iget-object v4, p0, Lorg/tukaani/xz/XZInputStream;->in:Ljava/io/InputStream;

    invoke-direct {v2, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 243
    .local v2, "inData":Ljava/io/DataInputStream;
    const/16 v4, 0xc

    new-array v0, v4, [B

    .line 250
    .local v0, "buf":[B
    :cond_0
    invoke-virtual {v2, v0, v6, v5}, Ljava/io/DataInputStream;->read([BII)I

    move-result v3

    .line 251
    .local v3, "ret":I
    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 252
    iput-boolean v5, p0, Lorg/tukaani/xz/XZInputStream;->endReached:Z

    .line 275
    :goto_0
    return-void

    .line 258
    :cond_1
    invoke-virtual {v2, v0, v5, v7}, Ljava/io/DataInputStream;->readFully([BII)V

    .line 260
    aget-byte v4, v0, v6

    if-nez v4, :cond_2

    aget-byte v4, v0, v5

    if-nez v4, :cond_2

    const/4 v4, 0x2

    aget-byte v4, v0, v4

    if-nez v4, :cond_2

    aget-byte v4, v0, v7

    if-eqz v4, :cond_0

    .line 265
    :cond_2
    const/4 v4, 0x4

    const/16 v5, 0x8

    invoke-virtual {v2, v0, v4, v5}, Ljava/io/DataInputStream;->readFully([BII)V

    .line 268
    :try_start_0
    new-instance v4, Lorg/tukaani/xz/SingleXZInputStream;

    iget-object v5, p0, Lorg/tukaani/xz/XZInputStream;->in:Ljava/io/InputStream;

    iget v6, p0, Lorg/tukaani/xz/XZInputStream;->memoryLimit:I

    invoke-direct {v4, v5, v6, v0}, Lorg/tukaani/xz/SingleXZInputStream;-><init>(Ljava/io/InputStream;I[B)V

    iput-object v4, p0, Lorg/tukaani/xz/XZInputStream;->xzIn:Lorg/tukaani/xz/SingleXZInputStream;
    :try_end_0
    .catch Lorg/tukaani/xz/XZFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 269
    :catch_0
    move-exception v1

    .line 272
    .local v1, "e":Lorg/tukaani/xz/XZFormatException;
    new-instance v4, Lorg/tukaani/xz/CorruptedInputException;

    const-string v5, "Garbage after a valid XZ Stream"

    invoke-direct {v4, v5}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v4
.end method


# virtual methods
.method public available()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 289
    iget-object v0, p0, Lorg/tukaani/xz/XZInputStream;->in:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 290
    new-instance v0, Lorg/tukaani/xz/XZIOException;

    const-string v1, "Stream closed"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 292
    :cond_0
    iget-object v0, p0, Lorg/tukaani/xz/XZInputStream;->exception:Ljava/io/IOException;

    if-eqz v0, :cond_1

    .line 293
    iget-object v0, p0, Lorg/tukaani/xz/XZInputStream;->exception:Ljava/io/IOException;

    throw v0

    .line 295
    :cond_1
    iget-object v0, p0, Lorg/tukaani/xz/XZInputStream;->xzIn:Lorg/tukaani/xz/SingleXZInputStream;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lorg/tukaani/xz/XZInputStream;->xzIn:Lorg/tukaani/xz/SingleXZInputStream;

    invoke-virtual {v0}, Lorg/tukaani/xz/SingleXZInputStream;->available()I

    move-result v0

    goto :goto_0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 305
    iget-object v0, p0, Lorg/tukaani/xz/XZInputStream;->in:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 307
    :try_start_0
    iget-object v0, p0, Lorg/tukaani/xz/XZInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309
    iput-object v1, p0, Lorg/tukaani/xz/XZInputStream;->in:Ljava/io/InputStream;

    .line 312
    :cond_0
    return-void

    .line 309
    :catchall_0
    move-exception v0

    iput-object v1, p0, Lorg/tukaani/xz/XZInputStream;->in:Ljava/io/InputStream;

    throw v0
.end method

.method public read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v0, -0x1

    .line 159
    iget-object v1, p0, Lorg/tukaani/xz/XZInputStream;->tempBuf:[B

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v3, v2}, Lorg/tukaani/xz/XZInputStream;->read([BII)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/tukaani/xz/XZInputStream;->tempBuf:[B

    aget-byte v0, v0, v3

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public read([BII)I
    .locals 6
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 197
    if-ltz p2, :cond_0

    if-ltz p3, :cond_0

    add-int v4, p2, p3

    if-ltz v4, :cond_0

    add-int v4, p2, p3

    array-length v5, p1

    if-le v4, v5, :cond_1

    .line 198
    :cond_0
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v3}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v3

    .line 200
    :cond_1
    if-nez p3, :cond_3

    .line 201
    const/4 v2, 0x0

    .line 238
    :cond_2
    :goto_0
    return v2

    .line 203
    :cond_3
    iget-object v4, p0, Lorg/tukaani/xz/XZInputStream;->in:Ljava/io/InputStream;

    if-nez v4, :cond_4

    .line 204
    new-instance v3, Lorg/tukaani/xz/XZIOException;

    const-string v4, "Stream closed"

    invoke-direct {v3, v4}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 206
    :cond_4
    iget-object v4, p0, Lorg/tukaani/xz/XZInputStream;->exception:Ljava/io/IOException;

    if-eqz v4, :cond_5

    .line 207
    iget-object v3, p0, Lorg/tukaani/xz/XZInputStream;->exception:Ljava/io/IOException;

    throw v3

    .line 209
    :cond_5
    iget-boolean v4, p0, Lorg/tukaani/xz/XZInputStream;->endReached:Z

    if-eqz v4, :cond_6

    move v2, v3

    .line 210
    goto :goto_0

    .line 212
    :cond_6
    const/4 v2, 0x0

    .line 215
    .local v2, "size":I
    :cond_7
    :goto_1
    if-lez p3, :cond_2

    .line 216
    :try_start_0
    iget-object v4, p0, Lorg/tukaani/xz/XZInputStream;->xzIn:Lorg/tukaani/xz/SingleXZInputStream;

    if-nez v4, :cond_8

    .line 217
    invoke-direct {p0}, Lorg/tukaani/xz/XZInputStream;->prepareNextStream()V

    .line 218
    iget-boolean v4, p0, Lorg/tukaani/xz/XZInputStream;->endReached:Z

    if-eqz v4, :cond_8

    .line 219
    if-nez v2, :cond_2

    move v2, v3

    goto :goto_0

    .line 222
    :cond_8
    iget-object v4, p0, Lorg/tukaani/xz/XZInputStream;->xzIn:Lorg/tukaani/xz/SingleXZInputStream;

    invoke-virtual {v4, p1, p2, p3}, Lorg/tukaani/xz/SingleXZInputStream;->read([BII)I

    move-result v1

    .line 224
    .local v1, "ret":I
    if-lez v1, :cond_9

    .line 225
    add-int/2addr v2, v1

    .line 226
    add-int/2addr p2, v1

    .line 227
    sub-int/2addr p3, v1

    goto :goto_1

    .line 228
    :cond_9
    if-ne v1, v3, :cond_7

    .line 229
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/tukaani/xz/XZInputStream;->xzIn:Lorg/tukaani/xz/SingleXZInputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 232
    .end local v1    # "ret":I
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Ljava/io/IOException;
    iput-object v0, p0, Lorg/tukaani/xz/XZInputStream;->exception:Ljava/io/IOException;

    .line 234
    if-nez v2, :cond_2

    .line 235
    throw v0
.end method
