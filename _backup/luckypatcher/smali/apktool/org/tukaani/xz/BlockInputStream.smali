.class Lorg/tukaani/xz/BlockInputStream;
.super Ljava/io/InputStream;
.source "BlockInputStream.java"


# instance fields
.field private final check:Lorg/tukaani/xz/check/Check;

.field private compressedSizeInHeader:J

.field private compressedSizeLimit:J

.field private endReached:Z

.field private filterChain:Ljava/io/InputStream;

.field private final headerSize:I

.field private final inCounted:Lorg/tukaani/xz/CountingInputStream;

.field private final inData:Ljava/io/DataInputStream;

.field private final tempBuf:[B

.field private uncompressedSize:J

.field private uncompressedSizeInHeader:J


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lorg/tukaani/xz/check/Check;IJJ)V
    .locals 21
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "check"    # Lorg/tukaani/xz/check/Check;
    .param p3, "memoryLimit"    # I
    .param p4, "unpaddedSizeInIndex"    # J
    .param p6, "uncompressedSizeInIndex"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/tukaani/xz/IndexIndicatorException;
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/io/InputStream;-><init>()V

    .line 26
    const-wide/16 v17, -0x1

    move-wide/from16 v0, v17

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/BlockInputStream;->uncompressedSizeInHeader:J

    .line 27
    const-wide/16 v17, -0x1

    move-wide/from16 v0, v17

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/BlockInputStream;->compressedSizeInHeader:J

    .line 30
    const-wide/16 v17, 0x0

    move-wide/from16 v0, v17

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/BlockInputStream;->uncompressedSize:J

    .line 31
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/tukaani/xz/BlockInputStream;->endReached:Z

    .line 33
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/tukaani/xz/BlockInputStream;->tempBuf:[B

    .line 39
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/tukaani/xz/BlockInputStream;->check:Lorg/tukaani/xz/check/Check;

    .line 40
    new-instance v17, Ljava/io/DataInputStream;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/tukaani/xz/BlockInputStream;->inData:Ljava/io/DataInputStream;

    .line 42
    const/16 v17, 0x400

    move/from16 v0, v17

    new-array v3, v0, [B

    .line 45
    .local v3, "buf":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/BlockInputStream;->inData:Ljava/io/DataInputStream;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v3, v1, v2}, Ljava/io/DataInputStream;->readFully([BII)V

    .line 48
    const/16 v17, 0x0

    aget-byte v17, v3, v17

    if-nez v17, :cond_0

    .line 49
    new-instance v17, Lorg/tukaani/xz/IndexIndicatorException;

    invoke-direct/range {v17 .. v17}, Lorg/tukaani/xz/IndexIndicatorException;-><init>()V

    throw v17

    .line 52
    :cond_0
    const/16 v17, 0x0

    aget-byte v17, v3, v17

    move/from16 v0, v17

    and-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    mul-int/lit8 v17, v17, 0x4

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tukaani/xz/BlockInputStream;->headerSize:I

    .line 53
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/BlockInputStream;->inData:Ljava/io/DataInputStream;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/BlockInputStream;->headerSize:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v3, v1, v2}, Ljava/io/DataInputStream;->readFully([BII)V

    .line 56
    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/BlockInputStream;->headerSize:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x4

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/BlockInputStream;->headerSize:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x4

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v3, v0, v1, v2}, Lorg/tukaani/xz/common/DecoderUtil;->isCRC32Valid([BIII)Z

    move-result v17

    if-nez v17, :cond_1

    .line 57
    new-instance v17, Lorg/tukaani/xz/CorruptedInputException;

    const-string v18, "XZ Block Header is corrupt"

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 60
    :cond_1
    const/16 v17, 0x1

    aget-byte v17, v3, v17

    and-int/lit8 v17, v17, 0x3c

    if-eqz v17, :cond_2

    .line 61
    new-instance v17, Lorg/tukaani/xz/UnsupportedOptionsException;

    const-string v18, "Unsupported options in XZ Block Header"

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 65
    :cond_2
    const/16 v17, 0x1

    aget-byte v17, v3, v17

    and-int/lit8 v17, v17, 0x3

    add-int/lit8 v8, v17, 0x1

    .line 66
    .local v8, "filterCount":I
    new-array v9, v8, [J

    .line 67
    .local v9, "filterIDs":[J
    new-array v10, v8, [[B

    .line 71
    .local v10, "filterProps":[[B
    new-instance v4, Ljava/io/ByteArrayInputStream;

    const/16 v17, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/BlockInputStream;->headerSize:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x6

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v4, v3, v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 77
    .local v4, "bufStream":Ljava/io/ByteArrayInputStream;
    const-wide v17, 0x7ffffffffffffffcL

    :try_start_0
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/BlockInputStream;->headerSize:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v19, v0

    sub-long v17, v17, v19

    .line 78
    invoke-virtual/range {p2 .. p2}, Lorg/tukaani/xz/check/Check;->getSize()I

    move-result v19

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v19, v0

    sub-long v17, v17, v19

    move-wide/from16 v0, v17

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/BlockInputStream;->compressedSizeLimit:J

    .line 82
    const/16 v17, 0x1

    aget-byte v17, v3, v17

    and-int/lit8 v17, v17, 0x40

    if-eqz v17, :cond_5

    .line 83
    invoke-static {v4}, Lorg/tukaani/xz/common/DecoderUtil;->decodeVLI(Ljava/io/InputStream;)J

    move-result-wide v17

    move-wide/from16 v0, v17

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/BlockInputStream;->compressedSizeInHeader:J

    .line 85
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/BlockInputStream;->compressedSizeInHeader:J

    move-wide/from16 v17, v0

    const-wide/16 v19, 0x0

    cmp-long v17, v17, v19

    if-eqz v17, :cond_3

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/BlockInputStream;->compressedSizeInHeader:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/BlockInputStream;->compressedSizeLimit:J

    move-wide/from16 v19, v0

    cmp-long v17, v17, v19

    if-lez v17, :cond_4

    .line 87
    :cond_3
    new-instance v17, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct/range {v17 .. v17}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v17
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :catch_0
    move-exception v7

    .line 110
    .local v7, "e":Ljava/io/IOException;
    new-instance v17, Lorg/tukaani/xz/CorruptedInputException;

    const-string v18, "XZ Block Header is corrupt"

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 89
    .end local v7    # "e":Ljava/io/IOException;
    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/BlockInputStream;->compressedSizeInHeader:J

    move-wide/from16 v17, v0

    move-wide/from16 v0, v17

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/BlockInputStream;->compressedSizeLimit:J

    .line 94
    :cond_5
    const/16 v17, 0x1

    aget-byte v17, v3, v17

    move/from16 v0, v17

    and-int/lit16 v0, v0, 0x80

    move/from16 v17, v0

    if-eqz v17, :cond_6

    .line 95
    invoke-static {v4}, Lorg/tukaani/xz/common/DecoderUtil;->decodeVLI(Ljava/io/InputStream;)J

    move-result-wide v17

    move-wide/from16 v0, v17

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/BlockInputStream;->uncompressedSizeInHeader:J

    .line 98
    :cond_6
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    if-ge v15, v8, :cond_8

    .line 99
    invoke-static {v4}, Lorg/tukaani/xz/common/DecoderUtil;->decodeVLI(Ljava/io/InputStream;)J

    move-result-wide v17

    aput-wide v17, v9, v15

    .line 101
    invoke-static {v4}, Lorg/tukaani/xz/common/DecoderUtil;->decodeVLI(Ljava/io/InputStream;)J

    move-result-wide v11

    .line 102
    .local v11, "filterPropsSize":J
    invoke-virtual {v4}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v17

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    cmp-long v17, v11, v17

    if-lez v17, :cond_7

    .line 103
    new-instance v17, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct/range {v17 .. v17}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v17

    .line 105
    :cond_7
    long-to-int v0, v11

    move/from16 v17, v0

    move/from16 v0, v17

    new-array v0, v0, [B

    move-object/from16 v17, v0

    aput-object v17, v10, v15

    .line 106
    aget-object v17, v10, v15

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/io/ByteArrayInputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 98
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 114
    .end local v11    # "filterPropsSize":J
    :cond_8
    invoke-virtual {v4}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v15

    :goto_1
    if-lez v15, :cond_a

    .line 115
    invoke-virtual {v4}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v17

    if-eqz v17, :cond_9

    .line 116
    new-instance v17, Lorg/tukaani/xz/UnsupportedOptionsException;

    const-string v18, "Unsupported options in XZ Block Header"

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 114
    :cond_9
    add-int/lit8 v15, v15, -0x1

    goto :goto_1

    .line 121
    :cond_a
    const-wide/16 v17, -0x1

    cmp-long v17, p4, v17

    if-eqz v17, :cond_f

    .line 125
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/BlockInputStream;->headerSize:I

    move/from16 v17, v0

    invoke-virtual/range {p2 .. p2}, Lorg/tukaani/xz/check/Check;->getSize()I

    move-result v18

    add-int v14, v17, v18

    .line 126
    .local v14, "headerAndCheckSize":I
    int-to-long v0, v14

    move-wide/from16 v17, v0

    cmp-long v17, v17, p4

    if-ltz v17, :cond_b

    .line 127
    new-instance v17, Lorg/tukaani/xz/CorruptedInputException;

    const-string v18, "XZ Index does not match a Block Header"

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 133
    :cond_b
    int-to-long v0, v14

    move-wide/from16 v17, v0

    sub-long v5, p4, v17

    .line 135
    .local v5, "compressedSizeFromIndex":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/BlockInputStream;->compressedSizeLimit:J

    move-wide/from16 v17, v0

    cmp-long v17, v5, v17

    if-gtz v17, :cond_c

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/BlockInputStream;->compressedSizeInHeader:J

    move-wide/from16 v17, v0

    const-wide/16 v19, -0x1

    cmp-long v17, v17, v19

    if-eqz v17, :cond_d

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/BlockInputStream;->compressedSizeInHeader:J

    move-wide/from16 v17, v0

    cmp-long v17, v17, v5

    if-eqz v17, :cond_d

    .line 138
    :cond_c
    new-instance v17, Lorg/tukaani/xz/CorruptedInputException;

    const-string v18, "XZ Index does not match a Block Header"

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 144
    :cond_d
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/BlockInputStream;->uncompressedSizeInHeader:J

    move-wide/from16 v17, v0

    const-wide/16 v19, -0x1

    cmp-long v17, v17, v19

    if-eqz v17, :cond_e

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/BlockInputStream;->uncompressedSizeInHeader:J

    move-wide/from16 v17, v0

    cmp-long v17, v17, p6

    if-eqz v17, :cond_e

    .line 146
    new-instance v17, Lorg/tukaani/xz/CorruptedInputException;

    const-string v18, "XZ Index does not match a Block Header"

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 151
    :cond_e
    move-object/from16 v0, p0

    iput-wide v5, v0, Lorg/tukaani/xz/BlockInputStream;->compressedSizeLimit:J

    .line 152
    move-object/from16 v0, p0

    iput-wide v5, v0, Lorg/tukaani/xz/BlockInputStream;->compressedSizeInHeader:J

    .line 153
    move-wide/from16 v0, p6

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/BlockInputStream;->uncompressedSizeInHeader:J

    .line 159
    .end local v5    # "compressedSizeFromIndex":J
    .end local v14    # "headerAndCheckSize":I
    :cond_f
    array-length v0, v9

    move/from16 v17, v0

    move/from16 v0, v17

    new-array v13, v0, [Lorg/tukaani/xz/FilterDecoder;

    .line 161
    .local v13, "filters":[Lorg/tukaani/xz/FilterDecoder;
    const/4 v15, 0x0

    :goto_2
    array-length v0, v13

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v15, v0, :cond_13

    .line 162
    aget-wide v17, v9, v15

    const-wide/16 v19, 0x21

    cmp-long v17, v17, v19

    if-nez v17, :cond_10

    .line 163
    new-instance v17, Lorg/tukaani/xz/LZMA2Decoder;

    aget-object v18, v10, v15

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/LZMA2Decoder;-><init>([B)V

    aput-object v17, v13, v15

    .line 161
    :goto_3
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 165
    :cond_10
    aget-wide v17, v9, v15

    const-wide/16 v19, 0x3

    cmp-long v17, v17, v19

    if-nez v17, :cond_11

    .line 166
    new-instance v17, Lorg/tukaani/xz/DeltaDecoder;

    aget-object v18, v10, v15

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/DeltaDecoder;-><init>([B)V

    aput-object v17, v13, v15

    goto :goto_3

    .line 168
    :cond_11
    aget-wide v17, v9, v15

    invoke-static/range {v17 .. v18}, Lorg/tukaani/xz/BCJDecoder;->isBCJFilterID(J)Z

    move-result v17

    if-eqz v17, :cond_12

    .line 169
    new-instance v17, Lorg/tukaani/xz/BCJDecoder;

    aget-wide v18, v9, v15

    aget-object v20, v10, v15

    invoke-direct/range {v17 .. v20}, Lorg/tukaani/xz/BCJDecoder;-><init>(J[B)V

    aput-object v17, v13, v15

    goto :goto_3

    .line 172
    :cond_12
    new-instance v17, Lorg/tukaani/xz/UnsupportedOptionsException;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Unknown Filter ID "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-wide v19, v9, v15

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 176
    :cond_13
    invoke-static {v13}, Lorg/tukaani/xz/RawCoder;->validate([Lorg/tukaani/xz/FilterCoder;)V

    .line 179
    if-ltz p3, :cond_15

    .line 180
    const/16 v16, 0x0

    .line 181
    .local v16, "memoryNeeded":I
    const/4 v15, 0x0

    :goto_4
    array-length v0, v13

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v15, v0, :cond_14

    .line 182
    aget-object v17, v13, v15

    invoke-interface/range {v17 .. v17}, Lorg/tukaani/xz/FilterDecoder;->getMemoryUsage()I

    move-result v17

    add-int v16, v16, v17

    .line 181
    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    .line 184
    :cond_14
    move/from16 v0, v16

    move/from16 v1, p3

    if-le v0, v1, :cond_15

    .line 185
    new-instance v17, Lorg/tukaani/xz/MemoryLimitException;

    move-object/from16 v0, v17

    move/from16 v1, v16

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lorg/tukaani/xz/MemoryLimitException;-><init>(II)V

    throw v17

    .line 190
    .end local v16    # "memoryNeeded":I
    :cond_15
    new-instance v17, Lorg/tukaani/xz/CountingInputStream;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/tukaani/xz/CountingInputStream;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/tukaani/xz/BlockInputStream;->inCounted:Lorg/tukaani/xz/CountingInputStream;

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/BlockInputStream;->inCounted:Lorg/tukaani/xz/CountingInputStream;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/tukaani/xz/BlockInputStream;->filterChain:Ljava/io/InputStream;

    .line 194
    array-length v0, v13

    move/from16 v17, v0

    add-int/lit8 v15, v17, -0x1

    :goto_5
    if-ltz v15, :cond_16

    .line 195
    aget-object v17, v13, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/BlockInputStream;->filterChain:Ljava/io/InputStream;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Lorg/tukaani/xz/FilterDecoder;->getInputStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/tukaani/xz/BlockInputStream;->filterChain:Ljava/io/InputStream;

    .line 194
    add-int/lit8 v15, v15, -0x1

    goto :goto_5

    .line 196
    :cond_16
    return-void
.end method

.method private validate()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v7, -0x1

    .line 244
    iget-object v5, p0, Lorg/tukaani/xz/BlockInputStream;->inCounted:Lorg/tukaani/xz/CountingInputStream;

    invoke-virtual {v5}, Lorg/tukaani/xz/CountingInputStream;->getSize()J

    move-result-wide v0

    .line 248
    .local v0, "compressedSize":J
    iget-wide v5, p0, Lorg/tukaani/xz/BlockInputStream;->compressedSizeInHeader:J

    cmp-long v5, v5, v7

    if-eqz v5, :cond_0

    iget-wide v5, p0, Lorg/tukaani/xz/BlockInputStream;->compressedSizeInHeader:J

    cmp-long v5, v5, v0

    if-nez v5, :cond_1

    :cond_0
    iget-wide v5, p0, Lorg/tukaani/xz/BlockInputStream;->uncompressedSizeInHeader:J

    cmp-long v5, v5, v7

    if-eqz v5, :cond_2

    iget-wide v5, p0, Lorg/tukaani/xz/BlockInputStream;->uncompressedSizeInHeader:J

    iget-wide v7, p0, Lorg/tukaani/xz/BlockInputStream;->uncompressedSize:J

    cmp-long v5, v5, v7

    if-eqz v5, :cond_2

    .line 252
    :cond_1
    new-instance v5, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v5}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v5

    :cond_2
    move-wide v2, v0

    .line 255
    .end local v0    # "compressedSize":J
    .local v2, "compressedSize":J
    const-wide/16 v5, 0x1

    add-long v0, v2, v5

    .end local v2    # "compressedSize":J
    .restart local v0    # "compressedSize":J
    const-wide/16 v5, 0x3

    and-long/2addr v5, v2

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_3

    .line 256
    iget-object v5, p0, Lorg/tukaani/xz/BlockInputStream;->inData:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v5

    if-eqz v5, :cond_2

    .line 257
    new-instance v5, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v5}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v5

    .line 260
    :cond_3
    iget-object v5, p0, Lorg/tukaani/xz/BlockInputStream;->check:Lorg/tukaani/xz/check/Check;

    invoke-virtual {v5}, Lorg/tukaani/xz/check/Check;->getSize()I

    move-result v5

    new-array v4, v5, [B

    .line 261
    .local v4, "storedCheck":[B
    iget-object v5, p0, Lorg/tukaani/xz/BlockInputStream;->inData:Ljava/io/DataInputStream;

    invoke-virtual {v5, v4}, Ljava/io/DataInputStream;->readFully([B)V

    .line 262
    iget-object v5, p0, Lorg/tukaani/xz/BlockInputStream;->check:Lorg/tukaani/xz/check/Check;

    invoke-virtual {v5}, Lorg/tukaani/xz/check/Check;->finish()[B

    move-result-object v5

    invoke-static {v5, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v5

    if-nez v5, :cond_4

    .line 263
    new-instance v5, Lorg/tukaani/xz/CorruptedInputException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Integrity check ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/tukaani/xz/BlockInputStream;->check:Lorg/tukaani/xz/check/Check;

    .line 264
    invoke-virtual {v7}, Lorg/tukaani/xz/check/Check;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") does not match"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 265
    :cond_4
    return-void
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 268
    iget-object v0, p0, Lorg/tukaani/xz/BlockInputStream;->filterChain:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    return v0
.end method

.method public getUncompressedSize()J
    .locals 2

    .prologue
    .line 276
    iget-wide v0, p0, Lorg/tukaani/xz/BlockInputStream;->uncompressedSize:J

    return-wide v0
.end method

.method public getUnpaddedSize()J
    .locals 4

    .prologue
    .line 272
    iget v0, p0, Lorg/tukaani/xz/BlockInputStream;->headerSize:I

    int-to-long v0, v0

    iget-object v2, p0, Lorg/tukaani/xz/BlockInputStream;->inCounted:Lorg/tukaani/xz/CountingInputStream;

    invoke-virtual {v2}, Lorg/tukaani/xz/CountingInputStream;->getSize()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, Lorg/tukaani/xz/BlockInputStream;->check:Lorg/tukaani/xz/check/Check;

    invoke-virtual {v2}, Lorg/tukaani/xz/check/Check;->getSize()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v0, -0x1

    .line 199
    iget-object v1, p0, Lorg/tukaani/xz/BlockInputStream;->tempBuf:[B

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v3, v2}, Lorg/tukaani/xz/BlockInputStream;->read([BII)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/tukaani/xz/BlockInputStream;->tempBuf:[B

    aget-byte v0, v0, v3

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public read([BII)I
    .locals 11
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v9, 0x0

    const/4 v8, 0x1

    const/4 v3, -0x1

    .line 203
    iget-boolean v4, p0, Lorg/tukaani/xz/BlockInputStream;->endReached:Z

    if-eqz v4, :cond_1

    move v2, v3

    .line 240
    :cond_0
    :goto_0
    return v2

    .line 206
    :cond_1
    iget-object v4, p0, Lorg/tukaani/xz/BlockInputStream;->filterChain:Ljava/io/InputStream;

    invoke-virtual {v4, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 208
    .local v2, "ret":I
    if-lez v2, :cond_6

    .line 209
    iget-object v4, p0, Lorg/tukaani/xz/BlockInputStream;->check:Lorg/tukaani/xz/check/Check;

    invoke-virtual {v4, p1, p2, v2}, Lorg/tukaani/xz/check/Check;->update([BII)V

    .line 210
    iget-wide v4, p0, Lorg/tukaani/xz/BlockInputStream;->uncompressedSize:J

    int-to-long v6, v2

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/tukaani/xz/BlockInputStream;->uncompressedSize:J

    .line 213
    iget-object v4, p0, Lorg/tukaani/xz/BlockInputStream;->inCounted:Lorg/tukaani/xz/CountingInputStream;

    invoke-virtual {v4}, Lorg/tukaani/xz/CountingInputStream;->getSize()J

    move-result-wide v0

    .line 214
    .local v0, "compressedSize":J
    cmp-long v4, v0, v9

    if-ltz v4, :cond_2

    iget-wide v4, p0, Lorg/tukaani/xz/BlockInputStream;->compressedSizeLimit:J

    cmp-long v4, v0, v4

    if-gtz v4, :cond_2

    iget-wide v4, p0, Lorg/tukaani/xz/BlockInputStream;->uncompressedSize:J

    cmp-long v4, v4, v9

    if-ltz v4, :cond_2

    iget-wide v4, p0, Lorg/tukaani/xz/BlockInputStream;->uncompressedSizeInHeader:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_3

    iget-wide v4, p0, Lorg/tukaani/xz/BlockInputStream;->uncompressedSize:J

    iget-wide v6, p0, Lorg/tukaani/xz/BlockInputStream;->uncompressedSizeInHeader:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 219
    :cond_2
    new-instance v3, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v3}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v3

    .line 228
    :cond_3
    if-lt v2, p3, :cond_4

    iget-wide v4, p0, Lorg/tukaani/xz/BlockInputStream;->uncompressedSize:J

    iget-wide v6, p0, Lorg/tukaani/xz/BlockInputStream;->uncompressedSizeInHeader:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 229
    :cond_4
    iget-object v4, p0, Lorg/tukaani/xz/BlockInputStream;->filterChain:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->read()I

    move-result v4

    if-eq v4, v3, :cond_5

    .line 230
    new-instance v3, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v3}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v3

    .line 232
    :cond_5
    invoke-direct {p0}, Lorg/tukaani/xz/BlockInputStream;->validate()V

    .line 233
    iput-boolean v8, p0, Lorg/tukaani/xz/BlockInputStream;->endReached:Z

    goto :goto_0

    .line 235
    .end local v0    # "compressedSize":J
    :cond_6
    if-ne v2, v3, :cond_0

    .line 236
    invoke-direct {p0}, Lorg/tukaani/xz/BlockInputStream;->validate()V

    .line 237
    iput-boolean v8, p0, Lorg/tukaani/xz/BlockInputStream;->endReached:Z

    goto :goto_0
.end method
