.class public final Lorg/tukaani/xz/rangecoder/RangeEncoder;
.super Lorg/tukaani/xz/rangecoder/RangeCoder;
.source "RangeEncoder.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final BIT_PRICE_SHIFT_BITS:I = 0x4

.field private static final MOVE_REDUCING_BITS:I = 0x4

.field private static final prices:[I


# instance fields
.field private final buf:[B

.field private bufPos:I

.field private cache:B

.field private cacheSize:I

.field private low:J

.field private range:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 16
    const-class v4, Lorg/tukaani/xz/rangecoder/RangeEncoder;

    invoke-virtual {v4}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    sput-boolean v4, Lorg/tukaani/xz/rangecoder/RangeEncoder;->$assertionsDisabled:Z

    .line 20
    const/16 v4, 0x80

    new-array v4, v4, [I

    sput-object v4, Lorg/tukaani/xz/rangecoder/RangeEncoder;->prices:[I

    .line 37
    const/16 v1, 0x8

    .local v1, "i":I
    :goto_1
    const/16 v4, 0x800

    if-ge v1, v4, :cond_3

    .line 39
    move v3, v1

    .line 40
    .local v3, "w":I
    const/4 v0, 0x0

    .line 42
    .local v0, "bitCount":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    const/4 v4, 0x4

    if-ge v2, v4, :cond_2

    .line 43
    mul-int/2addr v3, v3

    .line 44
    shl-int/lit8 v0, v0, 0x1

    .line 46
    :goto_3
    const/high16 v4, -0x10000

    and-int/2addr v4, v3

    if-eqz v4, :cond_1

    .line 47
    ushr-int/lit8 v3, v3, 0x1

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 16
    .end local v0    # "bitCount":I
    .end local v1    # "i":I
    .end local v2    # "j":I
    .end local v3    # "w":I
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 42
    .restart local v0    # "bitCount":I
    .restart local v1    # "i":I
    .restart local v2    # "j":I
    .restart local v3    # "w":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 52
    :cond_2
    sget-object v4, Lorg/tukaani/xz/rangecoder/RangeEncoder;->prices:[I

    shr-int/lit8 v5, v1, 0x4

    rsub-int v6, v0, 0xa1

    aput v6, v4, v5

    .line 38
    add-int/lit8 v1, v1, 0x10

    goto :goto_1

    .line 56
    .end local v0    # "bitCount":I
    .end local v2    # "j":I
    .end local v3    # "w":I
    :cond_3
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "bufSize"    # I

    .prologue
    .line 58
    invoke-direct {p0}, Lorg/tukaani/xz/rangecoder/RangeCoder;-><init>()V

    .line 59
    new-array v0, p1, [B

    iput-object v0, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->buf:[B

    .line 60
    invoke-virtual {p0}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->reset()V

    .line 61
    return-void
.end method

.method public static getBitPrice(II)I
    .locals 2
    .param p0, "prob"    # I
    .param p1, "bit"    # I

    .prologue
    .line 127
    sget-boolean v0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 128
    :cond_0
    sget-object v0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->prices:[I

    neg-int v1, p1

    and-int/lit16 v1, v1, 0x7ff

    xor-int/2addr v1, p0

    ushr-int/lit8 v1, v1, 0x4

    aget v0, v0, v1

    return v0
.end method

.method public static getBitTreePrice([SI)I
    .locals 3
    .param p0, "probs"    # [S
    .param p1, "symbol"    # I

    .prologue
    .line 149
    const/4 v1, 0x0

    .line 150
    .local v1, "price":I
    array-length v2, p0

    or-int/2addr p1, v2

    .line 153
    :cond_0
    and-int/lit8 v0, p1, 0x1

    .line 154
    .local v0, "bit":I
    ushr-int/lit8 p1, p1, 0x1

    .line 155
    aget-short v2, p0, p1

    invoke-static {v2, v0}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 156
    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    .line 158
    return v1
.end method

.method public static getDirectBitsPrice(I)I
    .locals 1
    .param p0, "count"    # I

    .prologue
    .line 201
    shl-int/lit8 v0, p0, 0x4

    return v0
.end method

.method public static getReverseBitTreePrice([SI)I
    .locals 4
    .param p0, "probs"    # [S
    .param p1, "symbol"    # I

    .prologue
    .line 174
    const/4 v2, 0x0

    .line 175
    .local v2, "price":I
    const/4 v1, 0x1

    .line 176
    .local v1, "index":I
    array-length v3, p0

    or-int/2addr p1, v3

    .line 179
    :cond_0
    and-int/lit8 v0, p1, 0x1

    .line 180
    .local v0, "bit":I
    ushr-int/lit8 p1, p1, 0x1

    .line 181
    aget-short v3, p0, v1

    invoke-static {v3, v0}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 182
    shl-int/lit8 v3, v1, 0x1

    or-int v1, v3, v0

    .line 183
    const/4 v3, 0x1

    if-ne p1, v3, :cond_0

    .line 185
    return v2
.end method

.method private shiftLow()V
    .locals 6

    .prologue
    .line 87
    iget-wide v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->low:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    long-to-int v0, v2

    .line 89
    .local v0, "lowHi":I
    if-nez v0, :cond_0

    iget-wide v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->low:J

    const-wide v4, 0xff000000L

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 90
    :cond_0
    iget-byte v1, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->cache:B

    .line 93
    .local v1, "temp":I
    :cond_1
    iget-object v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->buf:[B

    iget v3, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->bufPos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->bufPos:I

    add-int v4, v1, v0

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 94
    const/16 v1, 0xff

    .line 95
    iget v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->cacheSize:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->cacheSize:I

    if-nez v2, :cond_1

    .line 97
    iget-wide v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->low:J

    const/16 v4, 0x18

    ushr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    iput-byte v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->cache:B

    .line 100
    .end local v1    # "temp":I
    :cond_2
    iget v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->cacheSize:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->cacheSize:I

    .line 101
    iget-wide v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->low:J

    const-wide/32 v4, 0xffffff

    and-long/2addr v2, v4

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    iput-wide v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->low:J

    .line 102
    return-void
.end method


# virtual methods
.method public encodeBit([SII)V
    .locals 8
    .param p1, "probs"    # [S
    .param p2, "index"    # I
    .param p3, "bit"    # I

    .prologue
    .line 105
    aget-short v1, p1, p2

    .line 106
    .local v1, "prob":I
    iget v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->range:I

    ushr-int/lit8 v2, v2, 0xb

    mul-int v0, v2, v1

    .line 109
    .local v0, "bound":I
    if-nez p3, :cond_1

    .line 110
    iput v0, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->range:I

    .line 111
    rsub-int v2, v1, 0x800

    ushr-int/lit8 v2, v2, 0x5

    add-int/2addr v2, v1

    int-to-short v2, v2

    aput-short v2, p1, p2

    .line 119
    :goto_0
    iget v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->range:I

    const/high16 v3, -0x1000000

    and-int/2addr v2, v3

    if-nez v2, :cond_0

    .line 120
    iget v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->range:I

    shl-int/lit8 v2, v2, 0x8

    iput v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->range:I

    .line 121
    invoke-direct {p0}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->shiftLow()V

    .line 123
    :cond_0
    return-void

    .line 114
    :cond_1
    iget-wide v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->low:J

    int-to-long v4, v0

    const-wide v6, 0xffffffffL

    and-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->low:J

    .line 115
    iget v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->range:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->range:I

    .line 116
    ushr-int/lit8 v2, v1, 0x5

    sub-int v2, v1, v2

    int-to-short v2, v2

    aput-short v2, p1, p2

    goto :goto_0
.end method

.method public encodeBitTree([SI)V
    .locals 4
    .param p1, "probs"    # [S
    .param p2, "symbol"    # I

    .prologue
    .line 133
    const/4 v1, 0x1

    .line 134
    .local v1, "index":I
    array-length v2, p1

    .line 137
    .local v2, "mask":I
    :cond_0
    ushr-int/lit8 v2, v2, 0x1

    .line 138
    and-int v0, p2, v2

    .line 139
    .local v0, "bit":I
    invoke-virtual {p0, p1, v1, v0}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 141
    shl-int/lit8 v1, v1, 0x1

    .line 142
    if-eqz v0, :cond_1

    .line 143
    or-int/lit8 v1, v1, 0x1

    .line 145
    :cond_1
    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 146
    return-void
.end method

.method public encodeDirectBits(II)V
    .locals 4
    .param p1, "value"    # I
    .param p2, "count"    # I

    .prologue
    .line 190
    :cond_0
    iget v0, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->range:I

    ushr-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->range:I

    .line 191
    iget-wide v0, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->low:J

    iget v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->range:I

    add-int/lit8 p2, p2, -0x1

    ushr-int v3, p1, p2

    and-int/lit8 v3, v3, 0x1

    rsub-int/lit8 v3, v3, 0x0

    and-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->low:J

    .line 193
    iget v0, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->range:I

    const/high16 v1, -0x1000000

    and-int/2addr v0, v1

    if-nez v0, :cond_1

    .line 194
    iget v0, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->range:I

    shl-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->range:I

    .line 195
    invoke-direct {p0}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->shiftLow()V

    .line 197
    :cond_1
    if-nez p2, :cond_0

    .line 198
    return-void
.end method

.method public encodeReverseBitTree([SI)V
    .locals 3
    .param p1, "probs"    # [S
    .param p2, "symbol"    # I

    .prologue
    .line 162
    const/4 v1, 0x1

    .line 163
    .local v1, "index":I
    array-length v2, p1

    or-int/2addr p2, v2

    .line 166
    :cond_0
    and-int/lit8 v0, p2, 0x1

    .line 167
    .local v0, "bit":I
    ushr-int/lit8 p2, p2, 0x1

    .line 168
    invoke-virtual {p0, p1, v1, v0}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 169
    shl-int/lit8 v2, v1, 0x1

    or-int v1, v2, v0

    .line 170
    const/4 v2, 0x1

    if-ne p2, v2, :cond_0

    .line 171
    return-void
.end method

.method public finish()I
    .locals 2

    .prologue
    .line 76
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 77
    invoke-direct {p0}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->shiftLow()V

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 79
    :cond_0
    iget v1, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->bufPos:I

    return v1
.end method

.method public getPendingSize()I
    .locals 2

    .prologue
    .line 72
    iget v0, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->bufPos:I

    iget v1, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->cacheSize:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->low:J

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->range:I

    .line 66
    iput-byte v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->cache:B

    .line 67
    const/4 v0, 0x1

    iput v0, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->cacheSize:I

    .line 68
    iput v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->bufPos:I

    .line 69
    return-void
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->buf:[B

    const/4 v1, 0x0

    iget v2, p0, Lorg/tukaani/xz/rangecoder/RangeEncoder;->bufPos:I

    invoke-virtual {p1, v0, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 84
    return-void
.end method
