.class Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;
.super Lorg/tukaani/xz/lzma/LZMACoder$LiteralCoder$LiteralSubcoder;
.source "LZMAEncoder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LiteralSubencoder"
.end annotation


# instance fields
.field final synthetic this$1:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;


# direct methods
.method private constructor <init>(Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;)V
    .locals 0

    .prologue
    .line 540
    iput-object p1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;->this$1:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    invoke-direct {p0, p1}, Lorg/tukaani/xz/lzma/LZMACoder$LiteralCoder$LiteralSubcoder;-><init>(Lorg/tukaani/xz/lzma/LZMACoder$LiteralCoder;)V

    return-void
.end method

.method synthetic constructor <init>(Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;Lorg/tukaani/xz/lzma/LZMAEncoder$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
    .param p2, "x1"    # Lorg/tukaani/xz/lzma/LZMAEncoder$1;

    .prologue
    .line 540
    invoke-direct {p0, p1}, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;-><init>(Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;)V

    return-void
.end method


# virtual methods
.method encode()V
    .locals 10

    .prologue
    const/high16 v9, 0x10000

    .line 542
    iget-object v6, p0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;->this$1:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    iget-object v6, v6, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->this$0:Lorg/tukaani/xz/lzma/LZMAEncoder;

    iget-object v6, v6, Lorg/tukaani/xz/lzma/LZMAEncoder;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    iget-object v7, p0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;->this$1:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    iget-object v7, v7, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->this$0:Lorg/tukaani/xz/lzma/LZMAEncoder;

    iget v7, v7, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    invoke-virtual {v6, v7}, Lorg/tukaani/xz/lz/LZEncoder;->getByte(I)I

    move-result v6

    or-int/lit16 v5, v6, 0x100

    .line 544
    .local v5, "symbol":I
    iget-object v6, p0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;->this$1:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    iget-object v6, v6, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->this$0:Lorg/tukaani/xz/lzma/LZMAEncoder;

    iget-object v6, v6, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v6}, Lorg/tukaani/xz/lzma/State;->isLiteral()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 549
    :cond_0
    ushr-int/lit8 v4, v5, 0x8

    .line 550
    .local v4, "subencoderIndex":I
    ushr-int/lit8 v6, v5, 0x7

    and-int/lit8 v0, v6, 0x1

    .line 551
    .local v0, "bit":I
    iget-object v6, p0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;->this$1:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    iget-object v6, v6, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->this$0:Lorg/tukaani/xz/lzma/LZMAEncoder;

    # getter for: Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;
    invoke-static {v6}, Lorg/tukaani/xz/lzma/LZMAEncoder;->access$100(Lorg/tukaani/xz/lzma/LZMAEncoder;)Lorg/tukaani/xz/rangecoder/RangeEncoder;

    move-result-object v6

    iget-object v7, p0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;->probs:[S

    invoke-virtual {v6, v7, v4, v0}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 552
    shl-int/lit8 v5, v5, 0x1

    .line 553
    if-lt v5, v9, :cond_0

    .line 573
    :goto_0
    iget-object v6, p0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;->this$1:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    iget-object v6, v6, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->this$0:Lorg/tukaani/xz/lzma/LZMAEncoder;

    iget-object v6, v6, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v6}, Lorg/tukaani/xz/lzma/State;->updateLiteral()V

    .line 574
    return-void

    .line 556
    .end local v0    # "bit":I
    .end local v4    # "subencoderIndex":I
    :cond_1
    iget-object v6, p0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;->this$1:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    iget-object v6, v6, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->this$0:Lorg/tukaani/xz/lzma/LZMAEncoder;

    iget-object v6, v6, Lorg/tukaani/xz/lzma/LZMAEncoder;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    iget-object v7, p0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;->this$1:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    iget-object v7, v7, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->this$0:Lorg/tukaani/xz/lzma/LZMAEncoder;

    iget-object v7, v7, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    add-int/lit8 v7, v7, 0x1

    iget-object v8, p0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;->this$1:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    iget-object v8, v8, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->this$0:Lorg/tukaani/xz/lzma/LZMAEncoder;

    iget v8, v8, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    add-int/2addr v7, v8

    invoke-virtual {v6, v7}, Lorg/tukaani/xz/lz/LZEncoder;->getByte(I)I

    move-result v2

    .line 557
    .local v2, "matchByte":I
    const/16 v3, 0x100

    .line 563
    .local v3, "offset":I
    :cond_2
    shl-int/lit8 v2, v2, 0x1

    .line 564
    and-int v1, v2, v3

    .line 565
    .local v1, "matchBit":I
    add-int v6, v3, v1

    ushr-int/lit8 v7, v5, 0x8

    add-int v4, v6, v7

    .line 566
    .restart local v4    # "subencoderIndex":I
    ushr-int/lit8 v6, v5, 0x7

    and-int/lit8 v0, v6, 0x1

    .line 567
    .restart local v0    # "bit":I
    iget-object v6, p0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;->this$1:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    iget-object v6, v6, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->this$0:Lorg/tukaani/xz/lzma/LZMAEncoder;

    # getter for: Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;
    invoke-static {v6}, Lorg/tukaani/xz/lzma/LZMAEncoder;->access$100(Lorg/tukaani/xz/lzma/LZMAEncoder;)Lorg/tukaani/xz/rangecoder/RangeEncoder;

    move-result-object v6

    iget-object v7, p0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;->probs:[S

    invoke-virtual {v6, v7, v4, v0}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 568
    shl-int/lit8 v5, v5, 0x1

    .line 569
    xor-int v6, v2, v5

    xor-int/lit8 v6, v6, -0x1

    and-int/2addr v3, v6

    .line 570
    if-lt v5, v9, :cond_2

    goto :goto_0
.end method

.method getMatchedPrice(II)I
    .locals 7
    .param p1, "symbol"    # I
    .param p2, "matchByte"    # I

    .prologue
    .line 595
    const/4 v3, 0x0

    .line 596
    .local v3, "price":I
    const/16 v2, 0x100

    .line 601
    .local v2, "offset":I
    or-int/lit16 p1, p1, 0x100

    .line 604
    :cond_0
    shl-int/lit8 p2, p2, 0x1

    .line 605
    and-int v1, p2, v2

    .line 606
    .local v1, "matchBit":I
    add-int v5, v2, v1

    ushr-int/lit8 v6, p1, 0x8

    add-int v4, v5, v6

    .line 607
    .local v4, "subencoderIndex":I
    ushr-int/lit8 v5, p1, 0x7

    and-int/lit8 v0, v5, 0x1

    .line 608
    .local v0, "bit":I
    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;->probs:[S

    aget-short v5, v5, v4

    invoke-static {v5, v0}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v5

    add-int/2addr v3, v5

    .line 610
    shl-int/lit8 p1, p1, 0x1

    .line 611
    xor-int v5, p2, p1

    xor-int/lit8 v5, v5, -0x1

    and-int/2addr v2, v5

    .line 612
    const/high16 v5, 0x10000

    if-lt p1, v5, :cond_0

    .line 614
    return v3
.end method

.method getNormalPrice(I)I
    .locals 4
    .param p1, "symbol"    # I

    .prologue
    .line 577
    const/4 v1, 0x0

    .line 581
    .local v1, "price":I
    or-int/lit16 p1, p1, 0x100

    .line 584
    :cond_0
    ushr-int/lit8 v2, p1, 0x8

    .line 585
    .local v2, "subencoderIndex":I
    ushr-int/lit8 v3, p1, 0x7

    and-int/lit8 v0, v3, 0x1

    .line 586
    .local v0, "bit":I
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;->probs:[S

    aget-short v3, v3, v2

    invoke-static {v3, v0}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 588
    shl-int/lit8 p1, p1, 0x1

    .line 589
    const/high16 v3, 0x10000

    if-lt p1, v3, :cond_0

    .line 591
    return v1
.end method
