.class public abstract Lorg/tukaani/xz/lzma/LZMAEncoder;
.super Lorg/tukaani/xz/lzma/LZMACoder;
.source "LZMAEncoder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;,
        Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ALIGN_PRICE_UPDATE_INTERVAL:I = 0x10

.field private static final DIST_PRICE_UPDATE_INTERVAL:I = 0x80

.field private static final LZMA2_COMPRESSED_LIMIT:I = 0xffe6

.field private static final LZMA2_UNCOMPRESSED_LIMIT:I = 0x1ffeef

.field public static final MODE_FAST:I = 0x1

.field public static final MODE_NORMAL:I = 0x2


# instance fields
.field private alignPriceCount:I

.field private final alignPrices:[I

.field back:I

.field private distPriceCount:I

.field private final distSlotPrices:[[I

.field private final distSlotPricesSize:I

.field private final fullDistPrices:[[I

.field final literalEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

.field final lz:Lorg/tukaani/xz/lz/LZEncoder;

.field final matchLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

.field final niceLen:I

.field private final rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

.field readAhead:I

.field final repLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

.field private uncompressedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lorg/tukaani/xz/lzma/LZMAEncoder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/tukaani/xz/lzma/LZMAEncoder;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;Lorg/tukaani/xz/lz/LZEncoder;IIIII)V
    .locals 4
    .param p1, "rc"    # Lorg/tukaani/xz/rangecoder/RangeEncoder;
    .param p2, "lz"    # Lorg/tukaani/xz/lz/LZEncoder;
    .param p3, "lc"    # I
    .param p4, "lp"    # I
    .param p5, "pb"    # I
    .param p6, "dictSize"    # I
    .param p7, "niceLen"    # I

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 168
    invoke-direct {p0, p5}, Lorg/tukaani/xz/lzma/LZMACoder;-><init>(I)V

    .line 55
    iput v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distPriceCount:I

    .line 56
    iput v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->alignPriceCount:I

    .line 60
    const/16 v0, 0x80

    filled-new-array {v3, v0}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->fullDistPrices:[[I

    .line 62
    const/16 v0, 0x10

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->alignPrices:[I

    .line 64
    iput v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->back:I

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    .line 66
    iput v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->uncompressedSize:I

    .line 169
    iput-object p1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    .line 170
    iput-object p2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    .line 171
    iput p7, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->niceLen:I

    .line 173
    new-instance v0, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    invoke-direct {v0, p0, p3, p4}, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;-><init>(Lorg/tukaani/xz/lzma/LZMAEncoder;II)V

    iput-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->literalEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    .line 174
    new-instance v0, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    invoke-direct {v0, p0, p5, p7}, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;-><init>(Lorg/tukaani/xz/lzma/LZMAEncoder;II)V

    iput-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->matchLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    .line 175
    new-instance v0, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    invoke-direct {v0, p0, p5, p7}, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;-><init>(Lorg/tukaani/xz/lzma/LZMAEncoder;II)V

    iput-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->repLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    .line 177
    add-int/lit8 v0, p6, -0x1

    invoke-static {v0}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getDistSlot(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSlotPricesSize:I

    .line 178
    iget v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSlotPricesSize:I

    filled-new-array {v3, v0}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSlotPrices:[[I

    .line 180
    invoke-virtual {p0}, Lorg/tukaani/xz/lzma/LZMAEncoder;->reset()V

    .line 181
    return-void
.end method

.method static synthetic access$100(Lorg/tukaani/xz/lzma/LZMAEncoder;)Lorg/tukaani/xz/rangecoder/RangeEncoder;
    .locals 1
    .param p0, "x0"    # Lorg/tukaani/xz/lzma/LZMAEncoder;

    .prologue
    .line 17
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    return-object v0
.end method

.method private encodeInit()Z
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 225
    sget-boolean v2, Lorg/tukaani/xz/lzma/LZMAEncoder;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    if-eq v2, v5, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 226
    :cond_0
    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v2, v0}, Lorg/tukaani/xz/lz/LZEncoder;->hasEnoughData(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 242
    :goto_0
    return v0

    .line 232
    :cond_1
    invoke-virtual {p0, v1}, Lorg/tukaani/xz/lzma/LZMAEncoder;->skip(I)V

    .line 233
    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isMatch:[[S

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v4}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v2, v3, v0, v0}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 234
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->literalEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->encodeInit()V

    .line 236
    iget v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    .line 237
    sget-boolean v0, Lorg/tukaani/xz/lzma/LZMAEncoder;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    if-eq v0, v5, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 239
    :cond_2
    iget v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->uncompressedSize:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->uncompressedSize:I

    .line 240
    sget-boolean v0, Lorg/tukaani/xz/lzma/LZMAEncoder;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->uncompressedSize:I

    if-eq v0, v1, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    move v0, v1

    .line 242
    goto :goto_0
.end method

.method private encodeMatch(III)V
    .locals 10
    .param p1, "dist"    # I
    .param p2, "len"    # I
    .param p3, "posState"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 283
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v4}, Lorg/tukaani/xz/lzma/State;->updateMatch()V

    .line 284
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->matchLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    invoke-virtual {v4, p2, p3}, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;->encode(II)V

    .line 286
    invoke-static {p1}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getDistSlot(I)I

    move-result v2

    .line 287
    .local v2, "distSlot":I
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSlots:[[S

    invoke-static {p2}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getDistState(I)I

    move-result v6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5, v2}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBitTree([SI)V

    .line 289
    const/4 v4, 0x4

    if-lt v2, v4, :cond_0

    .line 290
    ushr-int/lit8 v4, v2, 0x1

    add-int/lit8 v3, v4, -0x1

    .line 291
    .local v3, "footerBits":I
    and-int/lit8 v4, v2, 0x1

    or-int/lit8 v4, v4, 0x2

    shl-int v0, v4, v3

    .line 292
    .local v0, "base":I
    sub-int v1, p1, v0

    .line 294
    .local v1, "distReduced":I
    const/16 v4, 0xe

    if-ge v2, v4, :cond_1

    .line 295
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSpecial:[[S

    add-int/lit8 v6, v2, -0x4

    aget-object v5, v5, v6

    invoke-virtual {v4, v5, v1}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeReverseBitTree([SI)V

    .line 306
    .end local v0    # "base":I
    .end local v1    # "distReduced":I
    .end local v3    # "footerBits":I
    :cond_0
    :goto_0
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    const/4 v5, 0x3

    iget-object v6, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    aget v6, v6, v9

    aput v6, v4, v5

    .line 307
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    aget v5, v5, v8

    aput v5, v4, v9

    .line 308
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    aget v5, v5, v7

    aput v5, v4, v8

    .line 309
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    aput p1, v4, v7

    .line 311
    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distPriceCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distPriceCount:I

    .line 312
    return-void

    .line 299
    .restart local v0    # "base":I
    .restart local v1    # "distReduced":I
    .restart local v3    # "footerBits":I
    :cond_1
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    ushr-int/lit8 v5, v1, 0x4

    add-int/lit8 v6, v3, -0x4

    invoke-virtual {v4, v5, v6}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeDirectBits(II)V

    .line 301
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distAlign:[S

    and-int/lit8 v6, v1, 0xf

    invoke-virtual {v4, v5, v6}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeReverseBitTree([SI)V

    .line 302
    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->alignPriceCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->alignPriceCount:I

    goto :goto_0
.end method

.method private encodeRepMatch(III)V
    .locals 9
    .param p1, "rep"    # I
    .param p2, "len"    # I
    .param p3, "posState"    # I

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 315
    if-nez p1, :cond_1

    .line 316
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep0:[S

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v5}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v5

    invoke-virtual {v3, v4, v5, v1}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 317
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep0Long:[[S

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v5}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v5

    aget-object v4, v4, v5

    if-ne p2, v2, :cond_0

    :goto_0
    invoke-virtual {v3, v4, p3, v1}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 338
    :goto_1
    if-ne p2, v2, :cond_4

    .line 339
    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v1}, Lorg/tukaani/xz/lzma/State;->updateShortRep()V

    .line 344
    :goto_2
    return-void

    :cond_0
    move v1, v2

    .line 317
    goto :goto_0

    .line 319
    :cond_1
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    aget v0, v3, p1

    .line 320
    .local v0, "dist":I
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep0:[S

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v5}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v5

    invoke-virtual {v3, v4, v5, v2}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 322
    if-ne p1, v2, :cond_2

    .line 323
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep1:[S

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v5}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v5

    invoke-virtual {v3, v4, v5, v1}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 334
    :goto_3
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    aget v4, v4, v1

    aput v4, v3, v2

    .line 335
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    aput v0, v3, v1

    goto :goto_1

    .line 325
    :cond_2
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep1:[S

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v5}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v5

    invoke-virtual {v3, v4, v5, v2}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 326
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep2:[S

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v5}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v5

    add-int/lit8 v6, p1, -0x2

    invoke-virtual {v3, v4, v5, v6}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 328
    if-ne p1, v8, :cond_3

    .line 329
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    aget v4, v4, v7

    aput v4, v3, v8

    .line 331
    :cond_3
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    aget v4, v4, v2

    aput v4, v3, v7

    goto :goto_3

    .line 341
    .end local v0    # "dist":I
    :cond_4
    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->repLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    invoke-virtual {v1, p2, p3}, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;->encode(II)V

    .line 342
    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v1}, Lorg/tukaani/xz/lzma/State;->updateLongRep()V

    goto :goto_2
.end method

.method private encodeSymbol()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 246
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    iget v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Lorg/tukaani/xz/lz/LZEncoder;->hasEnoughData(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 279
    :goto_0
    return v2

    .line 249
    :cond_0
    invoke-virtual {p0}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getNextSymbol()I

    move-result v0

    .line 251
    .local v0, "len":I
    sget-boolean v4, Lorg/tukaani/xz/lzma/LZMAEncoder;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    if-gez v4, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 252
    :cond_1
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v4}, Lorg/tukaani/xz/lz/LZEncoder;->getPos()I

    move-result v4

    iget v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    sub-int/2addr v4, v5

    iget v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->posMask:I

    and-int v1, v4, v5

    .line 254
    .local v1, "posState":I
    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->back:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_3

    .line 256
    sget-boolean v4, Lorg/tukaani/xz/lzma/LZMAEncoder;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    if-eq v0, v3, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 257
    :cond_2
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isMatch:[[S

    iget-object v6, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v6}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5, v1, v2}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 258
    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->literalEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    invoke-virtual {v2}, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->encode()V

    .line 276
    :goto_1
    iget v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    .line 277
    iget v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->uncompressedSize:I

    add-int/2addr v2, v0

    iput v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->uncompressedSize:I

    move v2, v3

    .line 279
    goto :goto_0

    .line 261
    :cond_3
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isMatch:[[S

    iget-object v6, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v6}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5, v1, v3}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 262
    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->back:I

    const/4 v5, 0x4

    if-ge v4, v5, :cond_5

    .line 265
    sget-boolean v2, Lorg/tukaani/xz/lzma/LZMAEncoder;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    neg-int v4, v4

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->reps:[I

    iget v6, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->back:I

    aget v5, v5, v6

    invoke-virtual {v2, v4, v5, v0}, Lorg/tukaani/xz/lz/LZEncoder;->getMatchLen(III)I

    move-result v2

    if-eq v2, v0, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 266
    :cond_4
    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep:[S

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v5}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v5

    invoke-virtual {v2, v4, v5, v3}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 267
    iget v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->back:I

    invoke-direct {p0, v2, v0, v1}, Lorg/tukaani/xz/lzma/LZMAEncoder;->encodeRepMatch(III)V

    goto :goto_1

    .line 270
    :cond_5
    sget-boolean v4, Lorg/tukaani/xz/lzma/LZMAEncoder;->$assertionsDisabled:Z

    if-nez v4, :cond_6

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    iget v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    neg-int v5, v5

    iget v6, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->back:I

    add-int/lit8 v6, v6, -0x4

    invoke-virtual {v4, v5, v6, v0}, Lorg/tukaani/xz/lz/LZEncoder;->getMatchLen(III)I

    move-result v4

    if-eq v4, v0, :cond_6

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 271
    :cond_6
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep:[S

    iget-object v6, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v6}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v6

    invoke-virtual {v4, v5, v6, v2}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->encodeBit([SII)V

    .line 272
    iget v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->back:I

    add-int/lit8 v2, v2, -0x4

    invoke-direct {p0, v2, v0, v1}, Lorg/tukaani/xz/lzma/LZMAEncoder;->encodeMatch(III)V

    goto :goto_1
.end method

.method public static getDistSlot(I)I
    .locals 4
    .param p0, "dist"    # I

    .prologue
    .line 115
    const/4 v2, 0x4

    if-gt p0, v2, :cond_0

    .line 144
    .end local p0    # "dist":I
    :goto_0
    return p0

    .line 118
    .restart local p0    # "dist":I
    :cond_0
    move v1, p0

    .line 119
    .local v1, "n":I
    const/16 v0, 0x1f

    .line 121
    .local v0, "i":I
    const/high16 v2, -0x10000

    and-int/2addr v2, v1

    if-nez v2, :cond_1

    .line 122
    shl-int/lit8 v1, v1, 0x10

    .line 123
    const/16 v0, 0xf

    .line 126
    :cond_1
    const/high16 v2, -0x1000000

    and-int/2addr v2, v1

    if-nez v2, :cond_2

    .line 127
    shl-int/lit8 v1, v1, 0x8

    .line 128
    add-int/lit8 v0, v0, -0x8

    .line 131
    :cond_2
    const/high16 v2, -0x10000000

    and-int/2addr v2, v1

    if-nez v2, :cond_3

    .line 132
    shl-int/lit8 v1, v1, 0x4

    .line 133
    add-int/lit8 v0, v0, -0x4

    .line 136
    :cond_3
    const/high16 v2, -0x40000000    # -2.0f

    and-int/2addr v2, v1

    if-nez v2, :cond_4

    .line 137
    shl-int/lit8 v1, v1, 0x2

    .line 138
    add-int/lit8 v0, v0, -0x2

    .line 141
    :cond_4
    const/high16 v2, -0x80000000

    and-int/2addr v2, v1

    if-nez v2, :cond_5

    .line 142
    add-int/lit8 v0, v0, -0x1

    .line 144
    :cond_5
    shl-int/lit8 v2, v0, 0x1

    add-int/lit8 v3, v0, -0x1

    ushr-int v3, p0, v3

    and-int/lit8 v3, v3, 0x1

    add-int p0, v2, v3

    goto :goto_0
.end method

.method public static getInstance(Lorg/tukaani/xz/rangecoder/RangeEncoder;IIIIIIIII)Lorg/tukaani/xz/lzma/LZMAEncoder;
    .locals 10
    .param p0, "rc"    # Lorg/tukaani/xz/rangecoder/RangeEncoder;
    .param p1, "lc"    # I
    .param p2, "lp"    # I
    .param p3, "pb"    # I
    .param p4, "mode"    # I
    .param p5, "dictSize"    # I
    .param p6, "extraSizeBefore"    # I
    .param p7, "niceLen"    # I
    .param p8, "mf"    # I
    .param p9, "depthLimit"    # I

    .prologue
    .line 94
    packed-switch p4, :pswitch_data_0

    .line 106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 96
    :pswitch_0
    new-instance v0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-direct/range {v0 .. v9}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;-><init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;IIIIIIII)V

    .line 101
    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-direct/range {v0 .. v9}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;-><init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;IIIIIIII)V

    goto :goto_0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getMemoryUsage(IIII)I
    .locals 2
    .param p0, "mode"    # I
    .param p1, "dictSize"    # I
    .param p2, "extraSizeBefore"    # I
    .param p3, "mf"    # I

    .prologue
    .line 70
    const/16 v0, 0x50

    .line 72
    .local v0, "m":I
    packed-switch p0, :pswitch_data_0

    .line 84
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 74
    :pswitch_0
    invoke-static {p1, p2, p3}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->getMemoryUsage(III)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :goto_0
    return v0

    .line 79
    :pswitch_1
    invoke-static {p1, p2, p3}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getMemoryUsage(III)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateAlignPrices()V
    .locals 4

    .prologue
    const/16 v3, 0x10

    .line 472
    iput v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->alignPriceCount:I

    .line 474
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 475
    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->alignPrices:[I

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distAlign:[S

    invoke-static {v2, v0}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getReverseBitTreePrice([SI)I

    move-result v2

    aput v2, v1, v0

    .line 474
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 477
    :cond_0
    return-void
.end method

.method private updateDistPrices()V
    .locals 15

    .prologue
    const/16 v14, 0x80

    const/4 v13, 0x4

    .line 427
    iput v14, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distPriceCount:I

    .line 429
    const/4 v5, 0x0

    .local v5, "distState":I
    :goto_0
    if-ge v5, v13, :cond_3

    .line 430
    const/4 v4, 0x0

    .local v4, "distSlot":I
    :goto_1
    iget v10, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSlotPricesSize:I

    if-ge v4, v10, :cond_0

    .line 431
    iget-object v10, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSlotPrices:[[I

    aget-object v10, v10, v5

    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSlots:[[S

    aget-object v11, v11, v5

    .line 432
    invoke-static {v11, v4}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitTreePrice([SI)I

    move-result v11

    aput v11, v10, v4

    .line 430
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 435
    :cond_0
    const/16 v4, 0xe

    :goto_2
    iget v10, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSlotPricesSize:I

    if-ge v4, v10, :cond_1

    .line 437
    ushr-int/lit8 v10, v4, 0x1

    add-int/lit8 v10, v10, -0x1

    add-int/lit8 v1, v10, -0x4

    .line 438
    .local v1, "count":I
    iget-object v10, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSlotPrices:[[I

    aget-object v10, v10, v5

    aget v11, v10, v4

    .line 439
    invoke-static {v1}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getDirectBitsPrice(I)I

    move-result v12

    add-int/2addr v11, v12

    aput v11, v10, v4

    .line 436
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 442
    .end local v1    # "count":I
    :cond_1
    const/4 v2, 0x0

    .local v2, "dist":I
    :goto_3
    if-ge v2, v13, :cond_2

    .line 443
    iget-object v10, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->fullDistPrices:[[I

    aget-object v10, v10, v5

    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSlotPrices:[[I

    aget-object v11, v11, v5

    aget v11, v11, v2

    aput v11, v10, v2

    .line 442
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 429
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 447
    .end local v2    # "dist":I
    .end local v4    # "distSlot":I
    :cond_3
    const/4 v2, 0x4

    .line 448
    .restart local v2    # "dist":I
    const/4 v4, 0x4

    .restart local v4    # "distSlot":I
    :goto_4
    const/16 v10, 0xe

    if-ge v4, v10, :cond_6

    .line 450
    ushr-int/lit8 v10, v4, 0x1

    add-int/lit8 v6, v10, -0x1

    .line 451
    .local v6, "footerBits":I
    and-int/lit8 v10, v4, 0x1

    or-int/lit8 v10, v10, 0x2

    shl-int v0, v10, v6

    .line 453
    .local v0, "base":I
    iget-object v10, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSpecial:[[S

    add-int/lit8 v11, v4, -0x4

    aget-object v10, v10, v11

    array-length v8, v10

    .line 454
    .local v8, "limit":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_5
    if-ge v7, v8, :cond_5

    .line 455
    sub-int v3, v2, v0

    .line 456
    .local v3, "distReduced":I
    iget-object v10, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSpecial:[[S

    add-int/lit8 v11, v4, -0x4

    aget-object v10, v10, v11

    invoke-static {v10, v3}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getReverseBitTreePrice([SI)I

    move-result v9

    .line 460
    .local v9, "price":I
    const/4 v5, 0x0

    :goto_6
    if-ge v5, v13, :cond_4

    .line 461
    iget-object v10, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->fullDistPrices:[[I

    aget-object v10, v10, v5

    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSlotPrices:[[I

    aget-object v11, v11, v5

    aget v11, v11, v4

    add-int/2addr v11, v9

    aput v11, v10, v2

    .line 460
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 464
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 454
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 449
    .end local v3    # "distReduced":I
    .end local v9    # "price":I
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 468
    .end local v0    # "base":I
    .end local v6    # "footerBits":I
    .end local v7    # "i":I
    .end local v8    # "limit":I
    :cond_6
    sget-boolean v10, Lorg/tukaani/xz/lzma/LZMAEncoder;->$assertionsDisabled:Z

    if-nez v10, :cond_7

    if-eq v2, v14, :cond_7

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 469
    :cond_7
    return-void
.end method


# virtual methods
.method public encodeForLZMA2()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 213
    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v1}, Lorg/tukaani/xz/lz/LZEncoder;->isStarted()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/tukaani/xz/lzma/LZMAEncoder;->encodeInit()Z

    move-result v1

    if-nez v1, :cond_0

    .line 221
    :goto_0
    return v0

    .line 216
    :cond_0
    iget v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->uncompressedSize:I

    const v2, 0x1ffeef

    if-gt v1, v2, :cond_1

    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    .line 217
    invoke-virtual {v1}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getPendingSize()I

    move-result v1

    const v2, 0xffe6

    if-gt v1, v2, :cond_1

    .line 218
    invoke-direct {p0}, Lorg/tukaani/xz/lzma/LZMAEncoder;->encodeSymbol()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 221
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method getAnyMatchPrice(Lorg/tukaani/xz/lzma/State;I)I
    .locals 2
    .param p1, "state"    # Lorg/tukaani/xz/lzma/State;
    .param p2, "posState"    # I

    .prologue
    .line 359
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isMatch:[[S

    invoke-virtual {p1}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v1

    aget-object v0, v0, v1

    aget-short v0, v0, p2

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v0

    return v0
.end method

.method getAnyRepPrice(ILorg/tukaani/xz/lzma/State;)I
    .locals 2
    .param p1, "anyMatchPrice"    # I
    .param p2, "state"    # Lorg/tukaani/xz/lzma/State;

    .prologue
    .line 368
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep:[S

    .line 369
    invoke-virtual {p2}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v1

    aget-short v0, v0, v1

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method public getLZEncoder()Lorg/tukaani/xz/lz/LZEncoder;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    return-object v0
.end method

.method getLongRepAndLenPrice(IILorg/tukaani/xz/lzma/State;I)I
    .locals 4
    .param p1, "rep"    # I
    .param p2, "len"    # I
    .param p3, "state"    # Lorg/tukaani/xz/lzma/State;
    .param p4, "posState"    # I

    .prologue
    .line 401
    invoke-virtual {p0, p3, p4}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getAnyMatchPrice(Lorg/tukaani/xz/lzma/State;I)I

    move-result v0

    .line 402
    .local v0, "anyMatchPrice":I
    invoke-virtual {p0, v0, p3}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getAnyRepPrice(ILorg/tukaani/xz/lzma/State;)I

    move-result v1

    .line 403
    .local v1, "anyRepPrice":I
    invoke-virtual {p0, v1, p1, p3, p4}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getLongRepPrice(IILorg/tukaani/xz/lzma/State;I)I

    move-result v2

    .line 404
    .local v2, "longRepPrice":I
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->repLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    invoke-virtual {v3, p2, p4}, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;->getPrice(II)I

    move-result v3

    add-int/2addr v3, v2

    return v3
.end method

.method getLongRepPrice(IILorg/tukaani/xz/lzma/State;I)I
    .locals 5
    .param p1, "anyRepPrice"    # I
    .param p2, "rep"    # I
    .param p3, "state"    # Lorg/tukaani/xz/lzma/State;
    .param p4, "posState"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 380
    move v0, p1

    .line 382
    .local v0, "price":I
    if-nez p2, :cond_0

    .line 383
    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep0:[S

    invoke-virtual {p3}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v2

    aget-short v1, v1, v2

    invoke-static {v1, v3}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v1

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep0Long:[[S

    .line 385
    invoke-virtual {p3}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v3

    aget-object v2, v2, v3

    aget-short v2, v2, p4

    .line 384
    invoke-static {v2, v4}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 397
    :goto_0
    return v0

    .line 387
    :cond_0
    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep0:[S

    invoke-virtual {p3}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v2

    aget-short v1, v1, v2

    invoke-static {v1, v4}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 389
    if-ne p2, v4, :cond_1

    .line 390
    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep1:[S

    invoke-virtual {p3}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v2

    aget-short v1, v1, v2

    invoke-static {v1, v3}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    .line 392
    :cond_1
    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep1:[S

    invoke-virtual {p3}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v2

    aget-short v1, v1, v2

    invoke-static {v1, v4}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v1

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep2:[S

    .line 393
    invoke-virtual {p3}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v3

    aget-short v2, v2, v3

    add-int/lit8 v3, p2, -0x2

    invoke-static {v2, v3}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method getMatchAndLenPrice(IIII)I
    .locals 6
    .param p1, "normalMatchPrice"    # I
    .param p2, "dist"    # I
    .param p3, "len"    # I
    .param p4, "posState"    # I

    .prologue
    .line 409
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->matchLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    .line 410
    invoke-virtual {v3, p3, p4}, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;->getPrice(II)I

    move-result v3

    add-int v2, p1, v3

    .line 411
    .local v2, "price":I
    invoke-static {p3}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getDistState(I)I

    move-result v1

    .line 413
    .local v1, "distState":I
    const/16 v3, 0x80

    if-ge p2, v3, :cond_0

    .line 414
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->fullDistPrices:[[I

    aget-object v3, v3, v1

    aget v3, v3, p2

    add-int/2addr v2, v3

    .line 423
    :goto_0
    return v2

    .line 418
    :cond_0
    invoke-static {p2}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getDistSlot(I)I

    move-result v0

    .line 419
    .local v0, "distSlot":I
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distSlotPrices:[[I

    aget-object v3, v3, v1

    aget v3, v3, v0

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->alignPrices:[I

    and-int/lit8 v5, p2, 0xf

    aget v4, v4, v5

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    goto :goto_0
.end method

.method getMatches()Lorg/tukaani/xz/lz/Matches;
    .locals 2

    .prologue
    .line 347
    iget v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    .line 348
    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v1}, Lorg/tukaani/xz/lz/LZEncoder;->getMatches()Lorg/tukaani/xz/lz/Matches;

    move-result-object v0

    .line 349
    .local v0, "matches":Lorg/tukaani/xz/lz/Matches;
    sget-boolean v1, Lorg/tukaani/xz/lzma/LZMAEncoder;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v1, v0}, Lorg/tukaani/xz/lz/LZEncoder;->verifyMatches(Lorg/tukaani/xz/lz/Matches;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 350
    :cond_0
    return-object v0
.end method

.method abstract getNextSymbol()I
.end method

.method getNormalMatchPrice(ILorg/tukaani/xz/lzma/State;)I
    .locals 2
    .param p1, "anyMatchPrice"    # I
    .param p2, "state"    # Lorg/tukaani/xz/lzma/State;

    .prologue
    .line 363
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep:[S

    .line 364
    invoke-virtual {p2}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v1

    aget-short v0, v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method getShortRepPrice(ILorg/tukaani/xz/lzma/State;I)I
    .locals 4
    .param p1, "anyRepPrice"    # I
    .param p2, "state"    # Lorg/tukaani/xz/lzma/State;
    .param p3, "posState"    # I

    .prologue
    const/4 v3, 0x0

    .line 373
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep0:[S

    .line 374
    invoke-virtual {p2}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v1

    aget-short v0, v0, v1

    invoke-static {v0, v3}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v0

    add-int/2addr v0, p1

    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->isRep0Long:[[S

    .line 375
    invoke-virtual {p2}, Lorg/tukaani/xz/lzma/State;->get()I

    move-result v2

    aget-object v1, v1, v2

    aget-short v1, v1, p3

    invoke-static {v1, v3}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->getBitPrice(II)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getUncompressedSize()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->uncompressedSize:I

    return v0
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 188
    invoke-super {p0}, Lorg/tukaani/xz/lzma/LZMACoder;->reset()V

    .line 189
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->literalEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->reset()V

    .line 190
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->matchLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;->reset()V

    .line 191
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->repLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;->reset()V

    .line 192
    iput v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distPriceCount:I

    .line 193
    iput v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->alignPriceCount:I

    .line 195
    iget v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->uncompressedSize:I

    iget v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->uncompressedSize:I

    .line 196
    const/4 v0, -0x1

    iput v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    .line 197
    return-void
.end method

.method public resetUncompressedSize()V
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x0

    iput v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->uncompressedSize:I

    .line 205
    return-void
.end method

.method skip(I)V
    .locals 1
    .param p1, "len"    # I

    .prologue
    .line 354
    iget v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->readAhead:I

    .line 355
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v0, p1}, Lorg/tukaani/xz/lz/LZEncoder;->skip(I)V

    .line 356
    return-void
.end method

.method updatePrices()V
    .locals 1

    .prologue
    .line 485
    iget v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->distPriceCount:I

    if-gtz v0, :cond_0

    .line 486
    invoke-direct {p0}, Lorg/tukaani/xz/lzma/LZMAEncoder;->updateDistPrices()V

    .line 488
    :cond_0
    iget v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->alignPriceCount:I

    if-gtz v0, :cond_1

    .line 489
    invoke-direct {p0}, Lorg/tukaani/xz/lzma/LZMAEncoder;->updateAlignPrices()V

    .line 491
    :cond_1
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->matchLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;->updatePrices()V

    .line 492
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoder;->repLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;->updatePrices()V

    .line 493
    return-void
.end method
