.class final Lorg/tukaani/xz/lz/HC4;
.super Lorg/tukaani/xz/lz/LZEncoder;
.source "HC4.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final chain:[I

.field private cyclicPos:I

.field private final cyclicSize:I

.field private final depthLimit:I

.field private final hash:Lorg/tukaani/xz/lz/Hash234;

.field private lzPos:I

.field private final matches:Lorg/tukaani/xz/lz/Matches;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lorg/tukaani/xz/lz/HC4;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/tukaani/xz/lz/HC4;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(IIIIII)V
    .locals 2
    .param p1, "dictSize"    # I
    .param p2, "beforeSizeMin"    # I
    .param p3, "readAheadMax"    # I
    .param p4, "niceLen"    # I
    .param p5, "matchLenMax"    # I
    .param p6, "depthLimit"    # I

    .prologue
    .line 36
    invoke-direct/range {p0 .. p5}, Lorg/tukaani/xz/lz/LZEncoder;-><init>(IIIII)V

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lorg/tukaani/xz/lz/HC4;->cyclicPos:I

    .line 38
    new-instance v0, Lorg/tukaani/xz/lz/Hash234;

    invoke-direct {v0, p1}, Lorg/tukaani/xz/lz/Hash234;-><init>(I)V

    iput-object v0, p0, Lorg/tukaani/xz/lz/HC4;->hash:Lorg/tukaani/xz/lz/Hash234;

    .line 41
    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lorg/tukaani/xz/lz/HC4;->cyclicSize:I

    .line 42
    iget v0, p0, Lorg/tukaani/xz/lz/HC4;->cyclicSize:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/tukaani/xz/lz/HC4;->chain:[I

    .line 43
    iget v0, p0, Lorg/tukaani/xz/lz/HC4;->cyclicSize:I

    iput v0, p0, Lorg/tukaani/xz/lz/HC4;->lzPos:I

    .line 48
    new-instance v0, Lorg/tukaani/xz/lz/Matches;

    add-int/lit8 v1, p4, -0x1

    invoke-direct {v0, v1}, Lorg/tukaani/xz/lz/Matches;-><init>(I)V

    iput-object v0, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    .line 53
    if-lez p6, :cond_0

    .end local p6    # "depthLimit":I
    :goto_0
    iput p6, p0, Lorg/tukaani/xz/lz/HC4;->depthLimit:I

    .line 54
    return-void

    .line 53
    .restart local p6    # "depthLimit":I
    :cond_0
    div-int/lit8 v0, p4, 0x4

    add-int/lit8 p6, v0, 0x4

    goto :goto_0
.end method

.method static getMemoryUsage(I)I
    .locals 2
    .param p0, "dictSize"    # I

    .prologue
    .line 27
    invoke-static {p0}, Lorg/tukaani/xz/lz/Hash234;->getMemoryUsage(I)I

    move-result v0

    div-int/lit16 v1, p0, 0x100

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0xa

    return v0
.end method

.method private movePos()I
    .locals 4

    .prologue
    const v3, 0x7fffffff

    const/4 v2, 0x4

    .line 63
    invoke-virtual {p0, v2, v2}, Lorg/tukaani/xz/lz/HC4;->movePos(II)I

    move-result v0

    .line 65
    .local v0, "avail":I
    if-eqz v0, :cond_1

    .line 66
    iget v2, p0, Lorg/tukaani/xz/lz/HC4;->lzPos:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tukaani/xz/lz/HC4;->lzPos:I

    if-ne v2, v3, :cond_0

    .line 67
    iget v2, p0, Lorg/tukaani/xz/lz/HC4;->cyclicSize:I

    sub-int v1, v3, v2

    .line 68
    .local v1, "normalizationOffset":I
    iget-object v2, p0, Lorg/tukaani/xz/lz/HC4;->hash:Lorg/tukaani/xz/lz/Hash234;

    invoke-virtual {v2, v1}, Lorg/tukaani/xz/lz/Hash234;->normalize(I)V

    .line 69
    iget-object v2, p0, Lorg/tukaani/xz/lz/HC4;->chain:[I

    invoke-static {v2, v1}, Lorg/tukaani/xz/lz/HC4;->normalize([II)V

    .line 70
    iget v2, p0, Lorg/tukaani/xz/lz/HC4;->lzPos:I

    sub-int/2addr v2, v1

    iput v2, p0, Lorg/tukaani/xz/lz/HC4;->lzPos:I

    .line 73
    .end local v1    # "normalizationOffset":I
    :cond_0
    iget v2, p0, Lorg/tukaani/xz/lz/HC4;->cyclicPos:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tukaani/xz/lz/HC4;->cyclicPos:I

    iget v3, p0, Lorg/tukaani/xz/lz/HC4;->cyclicSize:I

    if-ne v2, v3, :cond_1

    .line 74
    const/4 v2, 0x0

    iput v2, p0, Lorg/tukaani/xz/lz/HC4;->cyclicPos:I

    .line 77
    :cond_1
    return v0
.end method


# virtual methods
.method public getMatches()Lorg/tukaani/xz/lz/Matches;
    .locals 15

    .prologue
    .line 81
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    const/4 v12, 0x0

    iput v12, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    .line 82
    iget v9, p0, Lorg/tukaani/xz/lz/HC4;->matchLenMax:I

    .line 83
    .local v9, "matchLenLimit":I
    iget v10, p0, Lorg/tukaani/xz/lz/HC4;->niceLen:I

    .line 84
    .local v10, "niceLenLimit":I
    invoke-direct {p0}, Lorg/tukaani/xz/lz/HC4;->movePos()I

    move-result v0

    .line 86
    .local v0, "avail":I
    if-ge v0, v9, :cond_1

    .line 87
    if-nez v0, :cond_0

    .line 88
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    .line 182
    :goto_0
    return-object v11

    .line 90
    :cond_0
    move v9, v0

    .line 91
    if-le v10, v0, :cond_1

    .line 92
    move v10, v0

    .line 95
    :cond_1
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->hash:Lorg/tukaani/xz/lz/Hash234;

    iget-object v12, p0, Lorg/tukaani/xz/lz/HC4;->buf:[B

    iget v13, p0, Lorg/tukaani/xz/lz/HC4;->readPos:I

    invoke-virtual {v11, v12, v13}, Lorg/tukaani/xz/lz/Hash234;->calcHashes([BI)V

    .line 96
    iget v11, p0, Lorg/tukaani/xz/lz/HC4;->lzPos:I

    iget-object v12, p0, Lorg/tukaani/xz/lz/HC4;->hash:Lorg/tukaani/xz/lz/Hash234;

    invoke-virtual {v12}, Lorg/tukaani/xz/lz/Hash234;->getHash2Pos()I

    move-result v12

    sub-int v3, v11, v12

    .line 97
    .local v3, "delta2":I
    iget v11, p0, Lorg/tukaani/xz/lz/HC4;->lzPos:I

    iget-object v12, p0, Lorg/tukaani/xz/lz/HC4;->hash:Lorg/tukaani/xz/lz/Hash234;

    invoke-virtual {v12}, Lorg/tukaani/xz/lz/Hash234;->getHash3Pos()I

    move-result v12

    sub-int v4, v11, v12

    .line 98
    .local v4, "delta3":I
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->hash:Lorg/tukaani/xz/lz/Hash234;

    invoke-virtual {v11}, Lorg/tukaani/xz/lz/Hash234;->getHash4Pos()I

    move-result v1

    .line 99
    .local v1, "currentMatch":I
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->hash:Lorg/tukaani/xz/lz/Hash234;

    iget v12, p0, Lorg/tukaani/xz/lz/HC4;->lzPos:I

    invoke-virtual {v11, v12}, Lorg/tukaani/xz/lz/Hash234;->updateTables(I)V

    .line 101
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->chain:[I

    iget v12, p0, Lorg/tukaani/xz/lz/HC4;->cyclicPos:I

    aput v1, v11, v12

    .line 103
    const/4 v8, 0x0

    .line 109
    .local v8, "lenBest":I
    iget v11, p0, Lorg/tukaani/xz/lz/HC4;->cyclicSize:I

    if-ge v3, v11, :cond_2

    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->buf:[B

    iget v12, p0, Lorg/tukaani/xz/lz/HC4;->readPos:I

    sub-int/2addr v12, v3

    aget-byte v11, v11, v12

    iget-object v12, p0, Lorg/tukaani/xz/lz/HC4;->buf:[B

    iget v13, p0, Lorg/tukaani/xz/lz/HC4;->readPos:I

    aget-byte v12, v12, v13

    if-ne v11, v12, :cond_2

    .line 110
    const/4 v8, 0x2

    .line 111
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->len:[I

    const/4 v12, 0x0

    const/4 v13, 0x2

    aput v13, v11, v12

    .line 112
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->dist:[I

    const/4 v12, 0x0

    add-int/lit8 v13, v3, -0x1

    aput v13, v11, v12

    .line 113
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    const/4 v12, 0x1

    iput v12, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    .line 120
    :cond_2
    if-eq v3, v4, :cond_3

    iget v11, p0, Lorg/tukaani/xz/lz/HC4;->cyclicSize:I

    if-ge v4, v11, :cond_3

    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->buf:[B

    iget v12, p0, Lorg/tukaani/xz/lz/HC4;->readPos:I

    sub-int/2addr v12, v4

    aget-byte v11, v11, v12

    iget-object v12, p0, Lorg/tukaani/xz/lz/HC4;->buf:[B

    iget v13, p0, Lorg/tukaani/xz/lz/HC4;->readPos:I

    aget-byte v12, v12, v13

    if-ne v11, v12, :cond_3

    .line 122
    const/4 v8, 0x3

    .line 123
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->dist:[I

    iget-object v12, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v13, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v14, v13, 0x1

    iput v14, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v4, -0x1

    aput v12, v11, v13

    .line 124
    move v3, v4

    .line 128
    :cond_3
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v11, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    if-lez v11, :cond_5

    .line 129
    :goto_1
    if-ge v8, v9, :cond_4

    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->buf:[B

    iget v12, p0, Lorg/tukaani/xz/lz/HC4;->readPos:I

    add-int/2addr v12, v8

    sub-int/2addr v12, v3

    aget-byte v11, v11, v12

    iget-object v12, p0, Lorg/tukaani/xz/lz/HC4;->buf:[B

    iget v13, p0, Lorg/tukaani/xz/lz/HC4;->readPos:I

    add-int/2addr v13, v8

    aget-byte v12, v12, v13

    if-ne v11, v12, :cond_4

    .line 131
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 133
    :cond_4
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->len:[I

    iget-object v12, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    aput v8, v11, v12

    .line 137
    if-lt v8, v10, :cond_5

    .line 138
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    goto/16 :goto_0

    .line 143
    :cond_5
    const/4 v11, 0x3

    if-ge v8, v11, :cond_6

    .line 144
    const/4 v8, 0x3

    .line 146
    :cond_6
    iget v5, p0, Lorg/tukaani/xz/lz/HC4;->depthLimit:I

    .line 149
    .local v5, "depth":I
    :goto_2
    iget v11, p0, Lorg/tukaani/xz/lz/HC4;->lzPos:I

    sub-int v2, v11, v1

    .line 154
    .local v2, "delta":I
    add-int/lit8 v6, v5, -0x1

    .end local v5    # "depth":I
    .local v6, "depth":I
    if-eqz v5, :cond_7

    iget v11, p0, Lorg/tukaani/xz/lz/HC4;->cyclicSize:I

    if-lt v2, v11, :cond_8

    .line 155
    :cond_7
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    goto/16 :goto_0

    .line 157
    :cond_8
    iget-object v12, p0, Lorg/tukaani/xz/lz/HC4;->chain:[I

    iget v11, p0, Lorg/tukaani/xz/lz/HC4;->cyclicPos:I

    sub-int v13, v11, v2

    iget v11, p0, Lorg/tukaani/xz/lz/HC4;->cyclicPos:I

    if-le v2, v11, :cond_b

    iget v11, p0, Lorg/tukaani/xz/lz/HC4;->cyclicSize:I

    :goto_3
    add-int/2addr v11, v13

    aget v1, v12, v11

    .line 163
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->buf:[B

    iget v12, p0, Lorg/tukaani/xz/lz/HC4;->readPos:I

    add-int/2addr v12, v8

    sub-int/2addr v12, v2

    aget-byte v11, v11, v12

    iget-object v12, p0, Lorg/tukaani/xz/lz/HC4;->buf:[B

    iget v13, p0, Lorg/tukaani/xz/lz/HC4;->readPos:I

    add-int/2addr v13, v8

    aget-byte v12, v12, v13

    if-ne v11, v12, :cond_c

    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->buf:[B

    iget v12, p0, Lorg/tukaani/xz/lz/HC4;->readPos:I

    sub-int/2addr v12, v2

    aget-byte v11, v11, v12

    iget-object v12, p0, Lorg/tukaani/xz/lz/HC4;->buf:[B

    iget v13, p0, Lorg/tukaani/xz/lz/HC4;->readPos:I

    aget-byte v12, v12, v13

    if-ne v11, v12, :cond_c

    .line 166
    const/4 v7, 0x0

    .line 167
    .local v7, "len":I
    :cond_9
    add-int/lit8 v7, v7, 0x1

    if-ge v7, v9, :cond_a

    .line 168
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->buf:[B

    iget v12, p0, Lorg/tukaani/xz/lz/HC4;->readPos:I

    add-int/2addr v12, v7

    sub-int/2addr v12, v2

    aget-byte v11, v11, v12

    iget-object v12, p0, Lorg/tukaani/xz/lz/HC4;->buf:[B

    iget v13, p0, Lorg/tukaani/xz/lz/HC4;->readPos:I

    add-int/2addr v13, v7

    aget-byte v12, v12, v13

    if-eq v11, v12, :cond_9

    .line 173
    :cond_a
    if-le v7, v8, :cond_c

    .line 174
    move v8, v7

    .line 175
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->len:[I

    iget-object v12, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    aput v7, v11, v12

    .line 176
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->dist:[I

    iget-object v12, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v13, v2, -0x1

    aput v13, v11, v12

    .line 177
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, 0x1

    iput v12, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    .line 181
    if-lt v7, v10, :cond_c

    .line 182
    iget-object v11, p0, Lorg/tukaani/xz/lz/HC4;->matches:Lorg/tukaani/xz/lz/Matches;

    goto/16 :goto_0

    .line 157
    .end local v7    # "len":I
    :cond_b
    const/4 v11, 0x0

    goto :goto_3

    :cond_c
    move v5, v6

    .line 185
    .end local v6    # "depth":I
    .restart local v5    # "depth":I
    goto :goto_2
.end method

.method public skip(I)V
    .locals 4
    .param p1, "len"    # I

    .prologue
    .line 189
    sget-boolean v1, Lorg/tukaani/xz/lz/HC4;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-gez p1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 191
    .end local p1    # "len":I
    .local v0, "len":I
    :goto_0
    add-int/lit8 p1, v0, -0x1

    .end local v0    # "len":I
    .restart local p1    # "len":I
    if-lez v0, :cond_0

    .line 192
    invoke-direct {p0}, Lorg/tukaani/xz/lz/HC4;->movePos()I

    move-result v1

    if-eqz v1, :cond_1

    .line 194
    iget-object v1, p0, Lorg/tukaani/xz/lz/HC4;->hash:Lorg/tukaani/xz/lz/Hash234;

    iget-object v2, p0, Lorg/tukaani/xz/lz/HC4;->buf:[B

    iget v3, p0, Lorg/tukaani/xz/lz/HC4;->readPos:I

    invoke-virtual {v1, v2, v3}, Lorg/tukaani/xz/lz/Hash234;->calcHashes([BI)V

    .line 195
    iget-object v1, p0, Lorg/tukaani/xz/lz/HC4;->chain:[I

    iget v2, p0, Lorg/tukaani/xz/lz/HC4;->cyclicPos:I

    iget-object v3, p0, Lorg/tukaani/xz/lz/HC4;->hash:Lorg/tukaani/xz/lz/Hash234;

    invoke-virtual {v3}, Lorg/tukaani/xz/lz/Hash234;->getHash4Pos()I

    move-result v3

    aput v3, v1, v2

    .line 196
    iget-object v1, p0, Lorg/tukaani/xz/lz/HC4;->hash:Lorg/tukaani/xz/lz/Hash234;

    iget v2, p0, Lorg/tukaani/xz/lz/HC4;->lzPos:I

    invoke-virtual {v1, v2}, Lorg/tukaani/xz/lz/Hash234;->updateTables(I)V

    move v0, p1

    .end local p1    # "len":I
    .restart local v0    # "len":I
    goto :goto_0

    .line 199
    .end local v0    # "len":I
    .restart local p1    # "len":I
    :cond_0
    return-void

    :cond_1
    move v0, p1

    .end local p1    # "len":I
    .restart local v0    # "len":I
    goto :goto_0
.end method
