.class Lorg/tukaani/xz/LZMA2Encoder;
.super Lorg/tukaani/xz/LZMA2Coder;
.source "LZMA2Encoder.java"

# interfaces
.implements Lorg/tukaani/xz/FilterEncoder;


# instance fields
.field private final options:Lorg/tukaani/xz/LZMA2Options;

.field private final props:[B


# direct methods
.method constructor <init>(Lorg/tukaani/xz/LZMA2Options;)V
    .locals 4
    .parameter "options"

    .prologue
    const/4 v3, 0x0

    .line 18
    invoke-direct {p0}, Lorg/tukaani/xz/LZMA2Coder;-><init>()V

    .line 16
    const/4 v1, 0x1

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/tukaani/xz/LZMA2Encoder;->props:[B

    .line 19
    invoke-virtual {p1}, Lorg/tukaani/xz/LZMA2Options;->getPresetDict()[B

    move-result-object v1

    if-eqz v1, :cond_0

    .line 20
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "XZ doesn\'t support a preset dictionary for now"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 23
    :cond_0
    invoke-virtual {p1}, Lorg/tukaani/xz/LZMA2Options;->getMode()I

    move-result v1

    if-nez v1, :cond_1

    .line 24
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2Encoder;->props:[B

    aput-byte v3, v1, v3

    .line 32
    :goto_0
    invoke-virtual {p1}, Lorg/tukaani/xz/LZMA2Options;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/tukaani/xz/LZMA2Options;

    iput-object v1, p0, Lorg/tukaani/xz/LZMA2Encoder;->options:Lorg/tukaani/xz/LZMA2Options;

    .line 33
    return-void

    .line 26
    :cond_1
    invoke-virtual {p1}, Lorg/tukaani/xz/LZMA2Options;->getDictSize()I

    move-result v1

    const/16 v2, 0x1000

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 28
    .local v0, d:I
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2Encoder;->props:[B

    add-int/lit8 v2, v0, -0x1

    invoke-static {v2}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getDistSlot(I)I

    move-result v2

    add-int/lit8 v2, v2, -0x17

    int-to-byte v2, v2

    aput-byte v2, v1, v3

    goto :goto_0
.end method


# virtual methods
.method public getFilterID()J
    .locals 2

    .prologue
    .line 36
    const-wide/16 v0, 0x21

    return-wide v0
.end method

.method public getFilterProps()[B
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/tukaani/xz/LZMA2Encoder;->props:[B

    return-object v0
.end method

.method public getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;
    .locals 1
    .parameter "out"

    .prologue
    .line 48
    iget-object v0, p0, Lorg/tukaani/xz/LZMA2Encoder;->options:Lorg/tukaani/xz/LZMA2Options;

    invoke-virtual {v0, p1}, Lorg/tukaani/xz/LZMA2Options;->getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;

    move-result-object v0

    return-object v0
.end method

.method public supportsFlushing()Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    return v0
.end method
