.class final Lorg/tukaani/xz/lzma/LZMAEncoderNormal;
.super Lorg/tukaani/xz/lzma/LZMAEncoder;
.source "LZMAEncoderNormal.java"


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static EXTRA_SIZE_AFTER:I = 0x0

.field private static EXTRA_SIZE_BEFORE:I = 0x0

.field private static final OPTS:I = 0x1000


# instance fields
.field private matches:Lorg/tukaani/xz/lz/Matches;

.field private final nextState:Lorg/tukaani/xz/lzma/State;

.field private optCur:I

.field private optEnd:I

.field private final opts:[Lorg/tukaani/xz/lzma/Optimum;

.field private final repLens:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x1000

    .line 17
    const-class v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->$assertionsDisabled:Z

    .line 20
    sput v1, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->EXTRA_SIZE_BEFORE:I

    .line 21
    sput v1, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->EXTRA_SIZE_AFTER:I

    return-void

    .line 17
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;IIIIIIII)V
    .locals 9
    .parameter "rc"
    .parameter "lc"
    .parameter "lp"
    .parameter "pb"
    .parameter "dictSize"
    .parameter "extraSizeBefore"
    .parameter "niceLen"
    .parameter "mf"
    .parameter "depthLimit"

    .prologue
    .line 44
    sget v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->EXTRA_SIZE_BEFORE:I

    .line 45
    invoke-static {p6, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    sget v2, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->EXTRA_SIZE_AFTER:I

    const/16 v4, 0x111

    move v0, p5

    move/from16 v3, p7

    move/from16 v5, p8

    move/from16 v6, p9

    .line 44
    invoke-static/range {v0 .. v6}, Lorg/tukaani/xz/lz/LZEncoder;->getInstance(IIIIIII)Lorg/tukaani/xz/lz/LZEncoder;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lorg/tukaani/xz/lzma/LZMAEncoder;-><init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;Lorg/tukaani/xz/lz/LZEncoder;IIIII)V

    .line 23
    const/16 v0, 0x1000

    new-array v0, v0, [Lorg/tukaani/xz/lzma/Optimum;

    iput-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLens:[I

    .line 32
    new-instance v0, Lorg/tukaani/xz/lzma/State;

    invoke-direct {v0}, Lorg/tukaani/xz/lzma/State;-><init>()V

    iput-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->nextState:Lorg/tukaani/xz/lzma/State;

    .line 52
    const/4 v8, 0x0

    .local v8, i:I
    :goto_0
    const/16 v0, 0x1000

    if-ge v8, v0, :cond_0

    .line 53
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    new-instance v1, Lorg/tukaani/xz/lzma/Optimum;

    invoke-direct {v1}, Lorg/tukaani/xz/lzma/Optimum;-><init>()V

    aput-object v1, v0, v8

    .line 52
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 54
    :cond_0
    return-void
.end method

.method private calc1BytePrices(IIII)V
    .locals 18
    .parameter "pos"
    .parameter "posState"
    .parameter "avail"
    .parameter "anyRepPrice"

    .prologue
    .line 374
    const/4 v13, 0x0

    .line 376
    .local v13, nextIsByte:Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lorg/tukaani/xz/lz/LZEncoder;->getByte(I)I

    move-result v4

    .line 377
    .local v4, curByte:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v3, v6}, Lorg/tukaani/xz/lz/LZEncoder;->getByte(I)I

    move-result v5

    .line 380
    .local v5, matchByte:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v6

    iget v0, v3, Lorg/tukaani/xz/lzma/Optimum;->price:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->literalEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    const/4 v7, 0x1

    .line 381
    invoke-virtual {v6, v7}, Lorg/tukaani/xz/lz/LZEncoder;->getByte(I)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v7, v7, v8

    iget-object v8, v7, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    move/from16 v7, p1

    invoke-virtual/range {v3 .. v8}, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->getPrice(IIIILorg/tukaani/xz/lzma/State;)I

    move-result v3

    add-int v12, v17, v3

    .line 383
    .local v12, literalPrice:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/lit8 v6, v6, 0x1

    aget-object v3, v3, v6

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->price:I

    if-ge v12, v3, :cond_0

    .line 384
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/lit8 v6, v6, 0x1

    aget-object v3, v3, v6

    move-object/from16 v0, p0

    iget v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    const/4 v7, -0x1

    invoke-virtual {v3, v12, v6, v7}, Lorg/tukaani/xz/lzma/Optimum;->set1(III)V

    .line 385
    const/4 v13, 0x1

    .line 389
    :cond_0
    if-ne v5, v4, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/lit8 v6, v6, 0x1

    aget-object v3, v3, v6

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->optPrev:I

    move-object/from16 v0, p0

    iget v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    if-eq v3, v6, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/lit8 v6, v6, 0x1

    aget-object v3, v3, v6

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->backPrev:I

    if-eqz v3, :cond_2

    .line 391
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v6

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    move/from16 v1, p4

    move/from16 v2, p2

    invoke-virtual {v0, v1, v3, v2}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getShortRepPrice(ILorg/tukaani/xz/lzma/State;I)I

    move-result v16

    .line 394
    .local v16, shortRepPrice:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/lit8 v6, v6, 0x1

    aget-object v3, v3, v6

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->price:I

    move/from16 v0, v16

    if-gt v0, v3, :cond_2

    .line 395
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/lit8 v6, v6, 0x1

    aget-object v3, v3, v6

    move-object/from16 v0, p0

    iget v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    const/4 v7, 0x0

    move/from16 v0, v16

    invoke-virtual {v3, v0, v6, v7}, Lorg/tukaani/xz/lzma/Optimum;->set1(III)V

    .line 396
    const/4 v13, 0x1

    .line 402
    .end local v16           #shortRepPrice:I
    :cond_2
    if-nez v13, :cond_4

    if-eq v5, v4, :cond_4

    const/4 v3, 0x2

    move/from16 v0, p3

    if-le v0, v3, :cond_4

    .line 403
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->niceLen:I

    add-int/lit8 v6, p3, -0x1

    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 404
    .local v11, lenLimit:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v7, v7, v8

    iget-object v7, v7, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    invoke-virtual {v3, v6, v7, v11}, Lorg/tukaani/xz/lz/LZEncoder;->getMatchLen(III)I

    move-result v10

    .line 406
    .local v10, len:I
    const/4 v3, 0x2

    if-lt v10, v3, :cond_4

    .line 407
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->nextState:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3, v6}, Lorg/tukaani/xz/lzma/State;->set(Lorg/tukaani/xz/lzma/State;)V

    .line 408
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->nextState:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->updateLiteral()V

    .line 409
    add-int/lit8 v3, p1, 0x1

    move-object/from16 v0, p0

    iget v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->posMask:I

    and-int v14, v3, v6

    .line 410
    .local v14, nextPosState:I
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->nextState:Lorg/tukaani/xz/lzma/State;

    .line 411
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10, v6, v14}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getLongRepAndLenPrice(IILorg/tukaani/xz/lzma/State;I)I

    move-result v3

    add-int v15, v12, v3

    .line 414
    .local v15, price:I
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/lit8 v3, v3, 0x1

    add-int v9, v3, v10

    .line 415
    .local v9, i:I
    :goto_0
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    if-ge v3, v9, :cond_3

    .line 416
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v0, p0

    iput v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    aget-object v3, v3, v6

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/Optimum;->reset()V

    goto :goto_0

    .line 418
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v3, v3, v9

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->price:I

    if-ge v15, v3, :cond_4

    .line 419
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v3, v3, v9

    move-object/from16 v0, p0

    iget v6, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    const/4 v7, 0x0

    invoke-virtual {v3, v15, v6, v7}, Lorg/tukaani/xz/lzma/Optimum;->set2(III)V

    .line 422
    .end local v9           #i:I
    .end local v10           #len:I
    .end local v11           #lenLimit:I
    .end local v14           #nextPosState:I
    .end local v15           #price:I
    :cond_4
    return-void
.end method

.method private calcLongRepPrices(IIII)I
    .locals 21
    .parameter "pos"
    .parameter "posState"
    .parameter "avail"
    .parameter "anyRepPrice"

    .prologue
    .line 429
    const/16 v20, 0x2

    .line 430
    .local v20, startLen:I
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->niceLen:I

    move/from16 v0, p3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v16

    .line 432
    .local v16, lenLimit:I
    const/4 v10, 0x0

    .local v10, rep:I
    :goto_0
    const/4 v3, 0x4

    if-ge v10, v3, :cond_7

    .line 433
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v9, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v7, v7, v9

    iget-object v7, v7, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    aget v7, v7, v10

    move/from16 v0, v16

    invoke-virtual {v3, v7, v0}, Lorg/tukaani/xz/lz/LZEncoder;->getMatchLen(II)I

    move-result v11

    .line 434
    .local v11, len:I
    const/4 v3, 0x2

    if-ge v11, v3, :cond_1

    .line 432
    :cond_0
    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 437
    :cond_1
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/2addr v7, v11

    if-ge v3, v7, :cond_2

    .line 438
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    add-int/lit8 v7, v7, 0x1

    move-object/from16 v0, p0

    iput v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    aget-object v3, v3, v7

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/Optimum;->reset()V

    goto :goto_2

    .line 440
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v7

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    move/from16 v1, p4

    move/from16 v2, p2

    invoke-virtual {v0, v1, v10, v3, v2}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getLongRepPrice(IILorg/tukaani/xz/lzma/State;I)I

    move-result v17

    .line 443
    .local v17, longRepPrice:I
    move v13, v11

    .local v13, i:I
    :goto_3
    const/4 v3, 0x2

    if-lt v13, v3, :cond_4

    .line 444
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    .line 445
    move/from16 v0, p2

    invoke-virtual {v3, v13, v0}, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;->getPrice(II)I

    move-result v3

    add-int v8, v17, v3

    .line 446
    .local v8, price:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/2addr v7, v13

    aget-object v3, v3, v7

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->price:I

    if-ge v8, v3, :cond_3

    .line 447
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/2addr v7, v13

    aget-object v3, v3, v7

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    invoke-virtual {v3, v8, v7, v10}, Lorg/tukaani/xz/lzma/Optimum;->set1(III)V

    .line 443
    :cond_3
    add-int/lit8 v13, v13, -0x1

    goto :goto_3

    .line 450
    .end local v8           #price:I
    :cond_4
    if-nez v10, :cond_5

    .line 451
    add-int/lit8 v20, v11, 0x1

    .line 453
    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->niceLen:I

    sub-int v7, p3, v11

    add-int/lit8 v7, v7, -0x1

    invoke-static {v3, v7}, Ljava/lang/Math;->min(II)I

    move-result v15

    .line 454
    .local v15, len2Limit:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    add-int/lit8 v7, v11, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v12, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v9, v9, v12

    iget-object v9, v9, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    aget v9, v9, v10

    invoke-virtual {v3, v7, v9, v15}, Lorg/tukaani/xz/lz/LZEncoder;->getMatchLen(III)I

    move-result v14

    .line 457
    .local v14, len2:I
    const/4 v3, 0x2

    if-lt v14, v3, :cond_0

    .line 459
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    .line 460
    move/from16 v0, p2

    invoke-virtual {v3, v11, v0}, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;->getPrice(II)I

    move-result v3

    add-int v19, v17, v3

    .line 461
    .local v19, price:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->nextState:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v9, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v7, v7, v9

    iget-object v7, v7, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3, v7}, Lorg/tukaani/xz/lzma/State;->set(Lorg/tukaani/xz/lzma/State;)V

    .line 462
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->nextState:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->updateLongRep()V

    .line 465
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    const/4 v7, 0x0

    invoke-virtual {v3, v11, v7}, Lorg/tukaani/xz/lz/LZEncoder;->getByte(II)I

    move-result v4

    .line 466
    .local v4, curByte:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Lorg/tukaani/xz/lz/LZEncoder;->getByte(I)I

    move-result v5

    .line 467
    .local v5, matchByte:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    const/4 v7, 0x1

    invoke-virtual {v3, v11, v7}, Lorg/tukaani/xz/lz/LZEncoder;->getByte(II)I

    move-result v6

    .line 468
    .local v6, prevByte:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->literalEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    add-int v7, p1, v11

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->nextState:Lorg/tukaani/xz/lzma/State;

    invoke-virtual/range {v3 .. v8}, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->getPrice(IIIILorg/tukaani/xz/lzma/State;)I

    move-result v3

    add-int v8, v19, v3

    .line 470
    .end local v19           #price:I
    .restart local v8       #price:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->nextState:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->updateLiteral()V

    .line 473
    add-int v3, p1, v11

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->posMask:I

    and-int v18, v3, v7

    .line 474
    .local v18, nextPosState:I
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->nextState:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v3, v14, v7, v1}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getLongRepAndLenPrice(IILorg/tukaani/xz/lzma/State;I)I

    move-result v3

    add-int/2addr v8, v3

    .line 477
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/2addr v3, v11

    add-int/lit8 v3, v3, 0x1

    add-int v13, v3, v14

    .line 478
    :goto_4
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    if-ge v3, v13, :cond_6

    .line 479
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    add-int/lit8 v7, v7, 0x1

    move-object/from16 v0, p0

    iput v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    aget-object v3, v3, v7

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/Optimum;->reset()V

    goto :goto_4

    .line 481
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v3, v3, v13

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->price:I

    if-ge v8, v3, :cond_0

    .line 482
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v7, v3, v13

    move-object/from16 v0, p0

    iget v9, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    const/4 v12, 0x0

    invoke-virtual/range {v7 .. v12}, Lorg/tukaani/xz/lzma/Optimum;->set3(IIIII)V

    goto/16 :goto_1

    .line 486
    .end local v4           #curByte:I
    .end local v5           #matchByte:I
    .end local v6           #prevByte:I
    .end local v8           #price:I
    .end local v11           #len:I
    .end local v13           #i:I
    .end local v14           #len2:I
    .end local v15           #len2Limit:I
    .end local v17           #longRepPrice:I
    .end local v18           #nextPosState:I
    :cond_7
    return v20
.end method

.method private calcNormalMatchPrices(IIIII)V
    .locals 21
    .parameter "pos"
    .parameter "posState"
    .parameter "avail"
    .parameter "anyMatchPrice"
    .parameter "startLen"

    .prologue
    .line 496
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v3, v3, Lorg/tukaani/xz/lz/Matches;->len:[I

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v7, v7, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v7, v7, -0x1

    aget v3, v3, v7

    move/from16 v0, p3

    if-le v3, v0, :cond_1

    .line 497
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    const/4 v7, 0x0

    iput v7, v3, Lorg/tukaani/xz/lz/Matches;->count:I

    .line 498
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v3, v3, Lorg/tukaani/xz/lz/Matches;->len:[I

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v7, v7, Lorg/tukaani/xz/lz/Matches;->count:I

    aget v3, v3, v7

    move/from16 v0, p3

    if-ge v3, v0, :cond_0

    .line 499
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v7, v3, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v7, v7, 0x1

    iput v7, v3, Lorg/tukaani/xz/lz/Matches;->count:I

    goto :goto_0

    .line 501
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v3, v3, Lorg/tukaani/xz/lz/Matches;->len:[I

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v9, v7, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v10, v9, 0x1

    iput v10, v7, Lorg/tukaani/xz/lz/Matches;->count:I

    aput p3, v3, v9

    .line 504
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v3, v3, Lorg/tukaani/xz/lz/Matches;->len:[I

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v7, v7, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v7, v7, -0x1

    aget v3, v3, v7

    move/from16 v0, p5

    if-ge v3, v0, :cond_2

    .line 565
    :goto_1
    return-void

    .line 507
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v9, v9, Lorg/tukaani/xz/lz/Matches;->len:[I

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v10, v10, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v10, v10, -0x1

    aget v9, v9, v10

    add-int/2addr v7, v9

    if-ge v3, v7, :cond_3

    .line 508
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    add-int/lit8 v7, v7, 0x1

    move-object/from16 v0, p0

    iput v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    aget-object v3, v3, v7

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/Optimum;->reset()V

    goto :goto_2

    .line 510
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v7

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-virtual {v0, v1, v3}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getNormalMatchPrice(ILorg/tukaani/xz/lzma/State;)I

    move-result v20

    .line 513
    .local v20, normalMatchPrice:I
    const/16 v17, 0x0

    .line 514
    .local v17, match:I
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v3, v3, Lorg/tukaani/xz/lz/Matches;->len:[I

    aget v3, v3, v17

    move/from16 v0, p5

    if-le v0, v3, :cond_4

    .line 515
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 517
    :cond_4
    move/from16 v11, p5

    .line 518
    .local v11, len:I
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v3, v3, Lorg/tukaani/xz/lz/Matches;->dist:[I

    aget v13, v3, v17

    .line 522
    .local v13, dist:I
    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, p2

    invoke-virtual {v0, v1, v13, v11, v2}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getMatchAndLenPrice(IIII)I

    move-result v18

    .line 524
    .local v18, matchAndLenPrice:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/2addr v7, v11

    aget-object v3, v3, v7

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->price:I

    move/from16 v0, v18

    if-ge v0, v3, :cond_5

    .line 525
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/2addr v7, v11

    aget-object v3, v3, v7

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/lit8 v9, v13, 0x4

    move/from16 v0, v18

    invoke-virtual {v3, v0, v7, v9}, Lorg/tukaani/xz/lzma/Optimum;->set1(III)V

    .line 528
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v3, v3, Lorg/tukaani/xz/lz/Matches;->len:[I

    aget v3, v3, v17

    if-eq v11, v3, :cond_7

    .line 517
    :cond_6
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 532
    :cond_7
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->niceLen:I

    sub-int v7, p3, v11

    add-int/lit8 v7, v7, -0x1

    invoke-static {v3, v7}, Ljava/lang/Math;->min(II)I

    move-result v16

    .line 533
    .local v16, len2Limit:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    add-int/lit8 v7, v11, 0x1

    move/from16 v0, v16

    invoke-virtual {v3, v7, v13, v0}, Lorg/tukaani/xz/lz/LZEncoder;->getMatchLen(III)I

    move-result v15

    .line 535
    .local v15, len2:I
    const/4 v3, 0x2

    if-lt v15, v3, :cond_9

    .line 536
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->nextState:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v9, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v7, v7, v9

    iget-object v7, v7, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3, v7}, Lorg/tukaani/xz/lzma/State;->set(Lorg/tukaani/xz/lzma/State;)V

    .line 537
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->nextState:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->updateMatch()V

    .line 540
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    const/4 v7, 0x0

    invoke-virtual {v3, v11, v7}, Lorg/tukaani/xz/lz/LZEncoder;->getByte(II)I

    move-result v4

    .line 541
    .local v4, curByte:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Lorg/tukaani/xz/lz/LZEncoder;->getByte(I)I

    move-result v5

    .line 542
    .local v5, matchByte:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    const/4 v7, 0x1

    invoke-virtual {v3, v11, v7}, Lorg/tukaani/xz/lz/LZEncoder;->getByte(II)I

    move-result v6

    .line 543
    .local v6, prevByte:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->literalEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    add-int v7, p1, v11

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->nextState:Lorg/tukaani/xz/lzma/State;

    .line 544
    invoke-virtual/range {v3 .. v8}, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->getPrice(IIIILorg/tukaani/xz/lzma/State;)I

    move-result v3

    add-int v8, v18, v3

    .line 547
    .local v8, price:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->nextState:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->updateLiteral()V

    .line 550
    add-int v3, p1, v11

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->posMask:I

    and-int v19, v3, v7

    .line 551
    .local v19, nextPosState:I
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->nextState:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v3, v15, v7, v1}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getLongRepAndLenPrice(IILorg/tukaani/xz/lzma/State;I)I

    move-result v3

    add-int/2addr v8, v3

    .line 554
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/2addr v3, v11

    add-int/lit8 v3, v3, 0x1

    add-int v14, v3, v15

    .line 555
    .local v14, i:I
    :goto_5
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    if-ge v3, v14, :cond_8

    .line 556
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    add-int/lit8 v7, v7, 0x1

    move-object/from16 v0, p0

    iput v7, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    aget-object v3, v3, v7

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/Optimum;->reset()V

    goto :goto_5

    .line 558
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v3, v3, v14

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->price:I

    if-ge v8, v3, :cond_9

    .line 559
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v7, v3, v14

    move-object/from16 v0, p0

    iget v9, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/lit8 v10, v13, 0x4

    const/4 v12, 0x0

    invoke-virtual/range {v7 .. v12}, Lorg/tukaani/xz/lzma/Optimum;->set3(IIIII)V

    .line 562
    .end local v4           #curByte:I
    .end local v5           #matchByte:I
    .end local v6           #prevByte:I
    .end local v8           #price:I
    .end local v14           #i:I
    .end local v19           #nextPosState:I
    :cond_9
    add-int/lit8 v17, v17, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v3, v3, Lorg/tukaani/xz/lz/Matches;->count:I

    move/from16 v0, v17

    if-ne v0, v3, :cond_6

    goto/16 :goto_1
.end method

.method private convertOpts()I
    .locals 6

    .prologue
    .line 68
    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    iput v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    .line 70
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v4, v4, v5

    iget v1, v4, Lorg/tukaani/xz/lzma/Optimum;->optPrev:I

    .line 73
    .local v1, optPrev:I
    :cond_0
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v0, v4, v5

    .line 75
    .local v0, opt:Lorg/tukaani/xz/lzma/Optimum;
    iget-boolean v4, v0, Lorg/tukaani/xz/lzma/Optimum;->prev1IsLiteral:Z

    if-eqz v4, :cond_1

    .line 76
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v4, v4, v1

    iget v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    iput v5, v4, Lorg/tukaani/xz/lzma/Optimum;->optPrev:I

    .line 77
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v4, v4, v1

    const/4 v5, -0x1

    iput v5, v4, Lorg/tukaani/xz/lzma/Optimum;->backPrev:I

    .line 78
    add-int/lit8 v2, v1, -0x1

    .end local v1           #optPrev:I
    .local v2, optPrev:I
    iput v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    .line 80
    iget-boolean v4, v0, Lorg/tukaani/xz/lzma/Optimum;->hasPrev2:Z

    if-eqz v4, :cond_2

    .line 81
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v4, v4, v2

    add-int/lit8 v5, v2, 0x1

    iput v5, v4, Lorg/tukaani/xz/lzma/Optimum;->optPrev:I

    .line 82
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v4, v4, v2

    iget v5, v0, Lorg/tukaani/xz/lzma/Optimum;->backPrev2:I

    iput v5, v4, Lorg/tukaani/xz/lzma/Optimum;->backPrev:I

    .line 83
    iput v2, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    .line 84
    iget v1, v0, Lorg/tukaani/xz/lzma/Optimum;->optPrev2:I

    .line 88
    .end local v2           #optPrev:I
    .restart local v1       #optPrev:I
    :cond_1
    :goto_0
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v4, v4, v1

    iget v3, v4, Lorg/tukaani/xz/lzma/Optimum;->optPrev:I

    .line 89
    .local v3, temp:I
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v4, v4, v1

    iget v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    iput v5, v4, Lorg/tukaani/xz/lzma/Optimum;->optPrev:I

    .line 90
    iput v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    .line 91
    move v1, v3

    .line 92
    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    if-gtz v4, :cond_0

    .line 94
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget v4, v4, Lorg/tukaani/xz/lzma/Optimum;->optPrev:I

    iput v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    .line 95
    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v4, v4, v5

    iget v4, v4, Lorg/tukaani/xz/lzma/Optimum;->backPrev:I

    iput v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->back:I

    .line 96
    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    return v4

    .end local v1           #optPrev:I
    .end local v3           #temp:I
    .restart local v2       #optPrev:I
    :cond_2
    move v1, v2

    .end local v2           #optPrev:I
    .restart local v1       #optPrev:I
    goto :goto_0
.end method

.method static getMemoryUsage(III)I
    .locals 3
    .parameter "dictSize"
    .parameter "extraSizeBefore"
    .parameter "mf"

    .prologue
    .line 35
    sget v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->EXTRA_SIZE_BEFORE:I

    .line 36
    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    sget v1, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->EXTRA_SIZE_AFTER:I

    const/16 v2, 0x111

    .line 35
    invoke-static {p0, v0, v1, v2, p2}, Lorg/tukaani/xz/lz/LZEncoder;->getMemoryUsage(IIIII)I

    move-result v0

    add-int/lit16 v0, v0, 0x100

    return v0
.end method

.method private updateOptStateAndReps()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x0

    .line 305
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget v1, v3, Lorg/tukaani/xz/lzma/Optimum;->optPrev:I

    .line 306
    .local v1, optPrev:I
    sget-boolean v3, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    if-lt v1, v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 308
    :cond_0
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-boolean v3, v3, Lorg/tukaani/xz/lzma/Optimum;->prev1IsLiteral:Z

    if-eqz v3, :cond_3

    .line 309
    add-int/lit8 v1, v1, -0x1

    .line 311
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-boolean v3, v3, Lorg/tukaani/xz/lzma/Optimum;->hasPrev2:Z

    if-eqz v3, :cond_2

    .line 312
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v6, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v5, v5, v6

    iget v5, v5, Lorg/tukaani/xz/lzma/Optimum;->optPrev2:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3, v4}, Lorg/tukaani/xz/lzma/State;->set(Lorg/tukaani/xz/lzma/State;)V

    .line 313
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->backPrev2:I

    if-ge v3, v8, :cond_1

    .line 314
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->updateLongRep()V

    .line 321
    :goto_0
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->updateLiteral()V

    .line 326
    :goto_1
    iget v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_7

    .line 328
    sget-boolean v3, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->$assertionsDisabled:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->backPrev:I

    if-eqz v3, :cond_4

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->backPrev:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 316
    :cond_1
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->updateMatch()V

    goto :goto_0

    .line 318
    :cond_2
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v4, v4, v1

    iget-object v4, v4, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3, v4}, Lorg/tukaani/xz/lzma/State;->set(Lorg/tukaani/xz/lzma/State;)V

    goto :goto_0

    .line 323
    :cond_3
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v4, v4, v1

    iget-object v4, v4, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3, v4}, Lorg/tukaani/xz/lzma/State;->set(Lorg/tukaani/xz/lzma/State;)V

    goto :goto_1

    .line 330
    :cond_4
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->backPrev:I

    if-nez v3, :cond_6

    .line 331
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->updateShortRep()V

    .line 335
    :goto_2
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v3, v3, v1

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    invoke-static {v3, v7, v4, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 366
    :cond_5
    :goto_3
    return-void

    .line 333
    :cond_6
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->updateLiteral()V

    goto :goto_2

    .line 339
    :cond_7
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-boolean v3, v3, Lorg/tukaani/xz/lzma/Optimum;->prev1IsLiteral:Z

    if-eqz v3, :cond_8

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-boolean v3, v3, Lorg/tukaani/xz/lzma/Optimum;->hasPrev2:Z

    if-eqz v3, :cond_8

    .line 340
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget v1, v3, Lorg/tukaani/xz/lzma/Optimum;->optPrev2:I

    .line 341
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget v0, v3, Lorg/tukaani/xz/lzma/Optimum;->backPrev2:I

    .line 342
    .local v0, back:I
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->updateLongRep()V

    .line 351
    :goto_4
    if-ge v0, v8, :cond_b

    .line 352
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v4, v4, v1

    iget-object v4, v4, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    aget v4, v4, v0

    aput v4, v3, v7

    .line 355
    const/4 v2, 0x1

    .local v2, rep:I
    :goto_5
    if-gt v2, v0, :cond_a

    .line 356
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v4, v4, v1

    iget-object v4, v4, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    add-int/lit8 v5, v2, -0x1

    aget v4, v4, v5

    aput v4, v3, v2

    .line 355
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 344
    .end local v0           #back:I
    .end local v2           #rep:I
    :cond_8
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget v0, v3, Lorg/tukaani/xz/lzma/Optimum;->backPrev:I

    .line 345
    .restart local v0       #back:I
    if-ge v0, v8, :cond_9

    .line 346
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->updateLongRep()V

    goto :goto_4

    .line 348
    :cond_9
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->updateMatch()V

    goto :goto_4

    .line 358
    .restart local v2       #rep:I
    :cond_a
    :goto_6
    if-ge v2, v8, :cond_5

    .line 359
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v4, v4, v1

    iget-object v4, v4, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    aget v4, v4, v2

    aput v4, v3, v2

    .line 358
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 361
    .end local v2           #rep:I
    :cond_b
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    add-int/lit8 v4, v0, -0x4

    aput v4, v3, v7

    .line 362
    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v3, v3, v1

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    iget v5, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    const/4 v5, 0x1

    const/4 v6, 0x3

    invoke-static {v3, v7, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/16 :goto_3
.end method


# virtual methods
.method getNextSymbol()I
    .locals 30

    .prologue
    .line 102
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    move-object/from16 v0, p0

    iget v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    if-ge v3, v8, :cond_0

    .line 103
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v8

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->optPrev:I

    move-object/from16 v0, p0

    iget v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    sub-int v17, v3, v8

    .line 104
    .local v17, len:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v8

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->optPrev:I

    move-object/from16 v0, p0

    iput v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    .line 105
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v8

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->backPrev:I

    move-object/from16 v0, p0

    iput v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->back:I

    .line 298
    .end local v17           #len:I
    :goto_0
    return v17

    .line 109
    :cond_0
    sget-boolean v3, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    move-object/from16 v0, p0

    iget v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    if-eq v3, v8, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 110
    :cond_1
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    .line 111
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    .line 112
    const/4 v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->back:I

    .line 114
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->readAhead:I

    const/4 v8, -0x1

    if-ne v3, v8, :cond_2

    .line 115
    invoke-virtual/range {p0 .. p0}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getMatches()Lorg/tukaani/xz/lz/Matches;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    .line 121
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v3}, Lorg/tukaani/xz/lz/LZEncoder;->getAvail()I

    move-result v3

    const/16 v8, 0x111

    invoke-static {v3, v8}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 122
    .local v11, avail:I
    const/4 v3, 0x2

    if-ge v11, v3, :cond_3

    .line 123
    const/16 v17, 0x1

    goto :goto_0

    .line 126
    :cond_3
    const/16 v25, 0x0

    .line 127
    .local v25, repBest:I
    const/16 v24, 0x0

    .local v24, rep:I
    :goto_1
    const/4 v3, 0x4

    move/from16 v0, v24

    if-ge v0, v3, :cond_6

    .line 128
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLens:[I

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->reps:[I

    aget v9, v9, v24

    invoke-virtual {v8, v9, v11}, Lorg/tukaani/xz/lz/LZEncoder;->getMatchLen(II)I

    move-result v8

    aput v8, v3, v24

    .line 130
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLens:[I

    aget v3, v3, v24

    const/4 v8, 0x2

    if-ge v3, v8, :cond_5

    .line 131
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLens:[I

    const/4 v8, 0x0

    aput v8, v3, v24

    .line 127
    :cond_4
    :goto_2
    add-int/lit8 v24, v24, 0x1

    goto :goto_1

    .line 135
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLens:[I

    aget v3, v3, v24

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLens:[I

    aget v8, v8, v25

    if-le v3, v8, :cond_4

    .line 136
    move/from16 v25, v24

    goto :goto_2

    .line 140
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLens:[I

    aget v3, v3, v25

    move-object/from16 v0, p0

    iget v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->niceLen:I

    if-lt v3, v8, :cond_7

    .line 141
    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->back:I

    .line 142
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLens:[I

    aget v3, v3, v25

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->skip(I)V

    .line 143
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLens:[I

    aget v17, v3, v25

    goto/16 :goto_0

    .line 148
    :cond_7
    const/16 v21, 0x0

    .line 149
    .local v21, mainLen:I
    const/16 v20, 0x0

    .line 150
    .local v20, mainDist:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v3, v3, Lorg/tukaani/xz/lz/Matches;->count:I

    if-lez v3, :cond_8

    .line 151
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v3, v3, Lorg/tukaani/xz/lz/Matches;->len:[I

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v8, v8, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v8, v8, -0x1

    aget v21, v3, v8

    .line 152
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v3, v3, Lorg/tukaani/xz/lz/Matches;->dist:[I

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v8, v8, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v8, v8, -0x1

    aget v20, v3, v8

    .line 155
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->niceLen:I

    move/from16 v0, v21

    if-lt v0, v3, :cond_8

    .line 156
    add-int/lit8 v3, v20, 0x4

    move-object/from16 v0, p0

    iput v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->back:I

    .line 157
    add-int/lit8 v3, v21, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->skip(I)V

    move/from16 v17, v21

    .line 158
    goto/16 :goto_0

    .line 162
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Lorg/tukaani/xz/lz/LZEncoder;->getByte(I)I

    move-result v4

    .line 163
    .local v4, curByte:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->reps:[I

    const/4 v9, 0x0

    aget v8, v8, v9

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v3, v8}, Lorg/tukaani/xz/lz/LZEncoder;->getByte(I)I

    move-result v5

    .line 168
    .local v5, matchByte:I
    const/4 v3, 0x2

    move/from16 v0, v21

    if-ge v0, v3, :cond_9

    if-eq v4, v5, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLens:[I

    aget v3, v3, v25

    const/4 v8, 0x2

    if-ge v3, v8, :cond_9

    .line 170
    const/16 v17, 0x1

    goto/16 :goto_0

    .line 173
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v3}, Lorg/tukaani/xz/lz/LZEncoder;->getPos()I

    move-result v7

    .line 174
    .local v7, pos:I
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->posMask:I

    and-int v10, v7, v3

    .line 178
    .local v10, posState:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Lorg/tukaani/xz/lz/LZEncoder;->getByte(I)I

    move-result v6

    .line 179
    .local v6, prevByte:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->literalEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual/range {v3 .. v8}, Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;->getPrice(IIIILorg/tukaani/xz/lzma/State;)I

    move-result v18

    .line 181
    .local v18, literalPrice:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    const/4 v8, 0x1

    aget-object v3, v3, v8

    const/4 v8, 0x0

    const/4 v9, -0x1

    move/from16 v0, v18

    invoke-virtual {v3, v0, v8, v9}, Lorg/tukaani/xz/lzma/Optimum;->set1(III)V

    .line 184
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->state:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getAnyMatchPrice(Lorg/tukaani/xz/lzma/State;I)I

    move-result v12

    .line 185
    .local v12, anyMatchPrice:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->state:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v3}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getAnyRepPrice(ILorg/tukaani/xz/lzma/State;)I

    move-result v14

    .line 189
    .local v14, anyRepPrice:I
    if-ne v5, v4, :cond_a

    .line 190
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->state:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v3, v10}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getShortRepPrice(ILorg/tukaani/xz/lzma/State;I)I

    move-result v27

    .line 192
    .local v27, shortRepPrice:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    const/4 v8, 0x1

    aget-object v3, v3, v8

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->price:I

    move/from16 v0, v27

    if-ge v0, v3, :cond_a

    .line 193
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    const/4 v8, 0x1

    aget-object v3, v3, v8

    const/4 v8, 0x0

    const/4 v9, 0x0

    move/from16 v0, v27

    invoke-virtual {v3, v0, v8, v9}, Lorg/tukaani/xz/lzma/Optimum;->set1(III)V

    .line 198
    .end local v27           #shortRepPrice:I
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLens:[I

    aget v3, v3, v25

    move/from16 v0, v21

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    .line 199
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    const/4 v8, 0x2

    if-ge v3, v8, :cond_c

    .line 200
    sget-boolean v3, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->$assertionsDisabled:Z

    if-nez v3, :cond_b

    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    if-eqz v3, :cond_b

    new-instance v3, Ljava/lang/AssertionError;

    move-object/from16 v0, p0

    iget v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    invoke-direct {v3, v8}, Ljava/lang/AssertionError;-><init>(I)V

    throw v3

    .line 201
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    const/4 v8, 0x1

    aget-object v3, v3, v8

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->backPrev:I

    move-object/from16 v0, p0

    iput v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->back:I

    .line 202
    const/16 v17, 0x1

    goto/16 :goto_0

    .line 209
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->updatePrices()V

    .line 214
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    const/4 v8, 0x0

    aget-object v3, v3, v8

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->state:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3, v8}, Lorg/tukaani/xz/lzma/State;->set(Lorg/tukaani/xz/lzma/State;)V

    .line 215
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->reps:[I

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    const/16 v28, 0x0

    aget-object v9, v9, v28

    iget-object v9, v9, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    const/16 v28, 0x0

    const/16 v29, 0x4

    move/from16 v0, v28

    move/from16 v1, v29

    invoke-static {v3, v8, v9, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 218
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    move/from16 v16, v0

    .local v16, i:I
    :goto_3
    const/4 v3, 0x2

    move/from16 v0, v16

    if-lt v0, v3, :cond_d

    .line 219
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v3, v3, v16

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/Optimum;->reset()V

    .line 218
    add-int/lit8 v16, v16, -0x1

    goto :goto_3

    .line 222
    :cond_d
    const/16 v24, 0x0

    :goto_4
    const/4 v3, 0x4

    move/from16 v0, v24

    if-ge v0, v3, :cond_11

    .line 223
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLens:[I

    aget v26, v3, v24

    .line 224
    .local v26, repLen:I
    const/4 v3, 0x2

    move/from16 v0, v26

    if-ge v0, v3, :cond_e

    .line 222
    :goto_5
    add-int/lit8 v24, v24, 0x1

    goto :goto_4

    .line 227
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->state:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v14, v1, v3, v10}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getLongRepPrice(IILorg/tukaani/xz/lzma/State;I)I

    move-result v19

    .line 230
    .local v19, longRepPrice:I
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLenEncoder:Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

    move/from16 v0, v26

    invoke-virtual {v3, v0, v10}, Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;->getPrice(II)I

    move-result v3

    add-int v23, v19, v3

    .line 232
    .local v23, price:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v3, v3, v26

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->price:I

    move/from16 v0, v23

    if-ge v0, v3, :cond_10

    .line 233
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v3, v3, v26

    const/4 v8, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v3, v0, v8, v1}, Lorg/tukaani/xz/lzma/Optimum;->set1(III)V

    .line 234
    :cond_10
    add-int/lit8 v26, v26, -0x1

    const/4 v3, 0x2

    move/from16 v0, v26

    if-ge v0, v3, :cond_f

    goto :goto_5

    .line 239
    .end local v19           #longRepPrice:I
    .end local v23           #price:I
    .end local v26           #repLen:I
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->repLens:[I

    const/4 v8, 0x0

    aget v3, v3, v8

    add-int/lit8 v3, v3, 0x1

    const/4 v8, 0x2

    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 240
    .restart local v17       #len:I
    move/from16 v0, v17

    move/from16 v1, v21

    if-gt v0, v1, :cond_15

    .line 241
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->state:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v3}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getNormalMatchPrice(ILorg/tukaani/xz/lzma/State;)I

    move-result v22

    .line 246
    .local v22, normalMatchPrice:I
    const/16 v16, 0x0

    .line 247
    :goto_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v3, v3, Lorg/tukaani/xz/lz/Matches;->len:[I

    aget v3, v3, v16

    move/from16 v0, v17

    if-le v0, v3, :cond_13

    .line 248
    add-int/lit8 v16, v16, 0x1

    goto :goto_6

    .line 261
    .local v15, dist:I
    .restart local v23       #price:I
    :cond_12
    add-int/lit8 v17, v17, 0x1

    .line 251
    .end local v15           #dist:I
    .end local v23           #price:I
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v3, v3, Lorg/tukaani/xz/lz/Matches;->dist:[I

    aget v15, v3, v16

    .line 252
    .restart local v15       #dist:I
    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v17

    invoke-virtual {v0, v1, v15, v2, v10}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getMatchAndLenPrice(IIII)I

    move-result v23

    .line 254
    .restart local v23       #price:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v3, v3, v17

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->price:I

    move/from16 v0, v23

    if-ge v0, v3, :cond_14

    .line 255
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    aget-object v3, v3, v17

    const/4 v8, 0x0

    add-int/lit8 v9, v15, 0x4

    move/from16 v0, v23

    invoke-virtual {v3, v0, v8, v9}, Lorg/tukaani/xz/lzma/Optimum;->set1(III)V

    .line 257
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v3, v3, Lorg/tukaani/xz/lz/Matches;->len:[I

    aget v3, v3, v16

    move/from16 v0, v17

    if-ne v0, v3, :cond_12

    .line 258
    add-int/lit8 v16, v16, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v3, v3, Lorg/tukaani/xz/lz/Matches;->count:I

    move/from16 v0, v16

    if-ne v0, v3, :cond_12

    .line 267
    .end local v15           #dist:I
    .end local v22           #normalMatchPrice:I
    .end local v23           #price:I
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v3}, Lorg/tukaani/xz/lz/LZEncoder;->getAvail()I

    move-result v3

    const/16 v8, 0xfff

    invoke-static {v3, v8}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 272
    :cond_16
    :goto_7
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    move-object/from16 v0, p0

    iget v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    if-ge v3, v8, :cond_17

    .line 273
    invoke-virtual/range {p0 .. p0}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getMatches()Lorg/tukaani/xz/lz/Matches;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    .line 274
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v3, v3, Lorg/tukaani/xz/lz/Matches;->count:I

    if-lez v3, :cond_18

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v3, v3, Lorg/tukaani/xz/lz/Matches;->len:[I

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v8, v8, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v8, v8, -0x1

    aget v3, v3, v8

    move-object/from16 v0, p0

    iget v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->niceLen:I

    if-lt v3, v8, :cond_18

    .line 298
    :cond_17
    invoke-direct/range {p0 .. p0}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->convertOpts()I

    move-result v17

    goto/16 :goto_0

    .line 278
    :cond_18
    add-int/lit8 v11, v11, -0x1

    .line 279
    add-int/lit8 v7, v7, 0x1

    .line 280
    move-object/from16 v0, p0

    iget v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->posMask:I

    and-int v10, v7, v3

    .line 282
    invoke-direct/range {p0 .. p0}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->updateOptStateAndReps()V

    .line 283
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v8

    iget v3, v3, Lorg/tukaani/xz/lzma/Optimum;->price:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v9, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v8, v8, v9

    iget-object v8, v8, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    .line 284
    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v10}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getAnyMatchPrice(Lorg/tukaani/xz/lzma/State;I)I

    move-result v8

    add-int v12, v3, v8

    .line 285
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->opts:[Lorg/tukaani/xz/lzma/Optimum;

    move-object/from16 v0, p0

    iget v8, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    aget-object v3, v3, v8

    iget-object v3, v3, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v3}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->getAnyRepPrice(ILorg/tukaani/xz/lzma/State;)I

    move-result v14

    .line 287
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v10, v11, v14}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->calc1BytePrices(IIII)V

    .line 289
    const/4 v3, 0x2

    if-lt v11, v3, :cond_16

    .line 290
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v10, v11, v14}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->calcLongRepPrices(IIII)I

    move-result v13

    .line 292
    .local v13, startLen:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v3, v3, Lorg/tukaani/xz/lz/Matches;->count:I

    if-lez v3, :cond_16

    move-object/from16 v8, p0

    move v9, v7

    .line 293
    invoke-direct/range {v8 .. v13}, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->calcNormalMatchPrices(IIIII)V

    goto/16 :goto_7
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 57
    iput v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optCur:I

    .line 58
    iput v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoderNormal;->optEnd:I

    .line 59
    invoke-super {p0}, Lorg/tukaani/xz/lzma/LZMAEncoder;->reset()V

    .line 60
    return-void
.end method
