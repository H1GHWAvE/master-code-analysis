.class final Lorg/tukaani/xz/lzma/LZMAEncoderFast;
.super Lorg/tukaani/xz/lzma/LZMAEncoder;
.source "LZMAEncoderFast.java"


# static fields
.field private static EXTRA_SIZE_AFTER:I

.field private static EXTRA_SIZE_BEFORE:I


# instance fields
.field private matches:Lorg/tukaani/xz/lz/Matches;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x1

    sput v0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->EXTRA_SIZE_BEFORE:I

    .line 19
    const/16 v0, 0x110

    sput v0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->EXTRA_SIZE_AFTER:I

    return-void
.end method

.method constructor <init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;IIIIIIII)V
    .locals 8
    .parameter "rc"
    .parameter "lc"
    .parameter "lp"
    .parameter "pb"
    .parameter "dictSize"
    .parameter "extraSizeBefore"
    .parameter "niceLen"
    .parameter "mf"
    .parameter "depthLimit"

    .prologue
    .line 32
    sget v0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->EXTRA_SIZE_BEFORE:I

    .line 33
    invoke-static {p6, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    sget v2, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->EXTRA_SIZE_AFTER:I

    const/16 v4, 0x111

    move v0, p5

    move v3, p7

    move/from16 v5, p8

    move/from16 v6, p9

    .line 32
    invoke-static/range {v0 .. v6}, Lorg/tukaani/xz/lz/LZEncoder;->getInstance(IIIIIII)Lorg/tukaani/xz/lz/LZEncoder;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p7

    invoke-direct/range {v0 .. v7}, Lorg/tukaani/xz/lzma/LZMAEncoder;-><init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;Lorg/tukaani/xz/lz/LZEncoder;IIIII)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    .line 39
    return-void
.end method

.method private changePair(II)Z
    .locals 1
    .parameter "smallDist"
    .parameter "bigDist"

    .prologue
    .line 42
    ushr-int/lit8 v0, p2, 0x7

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static getMemoryUsage(III)I
    .locals 3
    .parameter "dictSize"
    .parameter "extraSizeBefore"
    .parameter "mf"

    .prologue
    .line 24
    sget v0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->EXTRA_SIZE_BEFORE:I

    .line 25
    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    sget v1, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->EXTRA_SIZE_AFTER:I

    const/16 v2, 0x111

    .line 24
    invoke-static {p0, v0, v1, v2, p2}, Lorg/tukaani/xz/lz/LZEncoder;->getMemoryUsage(IIIII)I

    move-result v0

    return v0
.end method


# virtual methods
.method getNextSymbol()I
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v12, -0x1

    const/4 v10, 0x1

    const/4 v13, 0x2

    .line 49
    iget v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->readAhead:I

    if-ne v11, v12, :cond_0

    .line 50
    invoke-virtual {p0}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->getMatches()Lorg/tukaani/xz/lz/Matches;

    move-result-object v11

    iput-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    .line 52
    :cond_0
    iput v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->back:I

    .line 58
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v11}, Lorg/tukaani/xz/lz/LZEncoder;->getAvail()I

    move-result v11

    const/16 v12, 0x111

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 59
    .local v0, avail:I
    if-ge v0, v13, :cond_1

    move v6, v10

    .line 149
    :goto_0
    return v6

    .line 63
    :cond_1
    const/4 v2, 0x0

    .line 64
    .local v2, bestRepLen:I
    const/4 v1, 0x0

    .line 65
    .local v1, bestRepIndex:I
    const/4 v9, 0x0

    .local v9, rep:I
    :goto_1
    if-ge v9, v14, :cond_5

    .line 66
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->reps:[I

    aget v12, v12, v9

    invoke-virtual {v11, v12, v0}, Lorg/tukaani/xz/lz/LZEncoder;->getMatchLen(II)I

    move-result v3

    .line 67
    .local v3, len:I
    if-ge v3, v13, :cond_3

    .line 65
    :cond_2
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 71
    :cond_3
    iget v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->niceLen:I

    if-lt v3, v11, :cond_4

    .line 72
    iput v9, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->back:I

    .line 73
    add-int/lit8 v10, v3, -0x1

    invoke-virtual {p0, v10}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->skip(I)V

    move v6, v3

    .line 74
    goto :goto_0

    .line 78
    :cond_4
    if-le v3, v2, :cond_2

    .line 79
    move v1, v9

    .line 80
    move v2, v3

    goto :goto_2

    .line 84
    .end local v3           #len:I
    :cond_5
    const/4 v6, 0x0

    .line 85
    .local v6, mainLen:I
    const/4 v5, 0x0

    .line 87
    .local v5, mainDist:I
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v11, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    if-lez v11, :cond_9

    .line 88
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->len:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    aget v6, v11, v12

    .line 89
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->dist:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    aget v5, v11, v12

    .line 91
    iget v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->niceLen:I

    if-lt v6, v11, :cond_7

    .line 92
    add-int/lit8 v10, v5, 0x4

    iput v10, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->back:I

    .line 93
    add-int/lit8 v10, v6, -0x1

    invoke-virtual {p0, v10}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->skip(I)V

    goto :goto_0

    .line 102
    :cond_6
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    iput v12, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    .line 103
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->len:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    aget v6, v11, v12

    .line 104
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->dist:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    aget v5, v11, v12

    .line 97
    :cond_7
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v11, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    if-le v11, v10, :cond_8

    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->len:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x2

    aget v11, v11, v12

    add-int/lit8 v11, v11, 0x1

    if-ne v6, v11, :cond_8

    .line 99
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->dist:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x2

    aget v11, v11, v12

    invoke-direct {p0, v11, v5}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->changePair(II)Z

    move-result v11

    if-nez v11, :cond_6

    .line 107
    :cond_8
    if-ne v6, v13, :cond_9

    const/16 v11, 0x80

    if-lt v5, v11, :cond_9

    .line 108
    const/4 v6, 0x1

    .line 111
    :cond_9
    if-lt v2, v13, :cond_c

    .line 112
    add-int/lit8 v11, v2, 0x1

    if-ge v11, v6, :cond_b

    add-int/lit8 v11, v2, 0x2

    if-lt v11, v6, :cond_a

    const/16 v11, 0x200

    if-ge v5, v11, :cond_b

    :cond_a
    add-int/lit8 v11, v2, 0x3

    if-lt v11, v6, :cond_c

    const v11, 0x8000

    if-lt v5, v11, :cond_c

    .line 115
    :cond_b
    iput v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->back:I

    .line 116
    add-int/lit8 v10, v2, -0x1

    invoke-virtual {p0, v10}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->skip(I)V

    move v6, v2

    .line 117
    goto/16 :goto_0

    .line 121
    :cond_c
    if-lt v6, v13, :cond_d

    if-gt v0, v13, :cond_e

    :cond_d
    move v6, v10

    .line 122
    goto/16 :goto_0

    .line 126
    :cond_e
    invoke-virtual {p0}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->getMatches()Lorg/tukaani/xz/lz/Matches;

    move-result-object v11

    iput-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    .line 128
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v11, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    if-lez v11, :cond_12

    .line 129
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->len:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    aget v8, v11, v12

    .line 130
    .local v8, newLen:I
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->dist:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    aget v7, v11, v12

    .line 132
    .local v7, newDist:I
    if-lt v8, v6, :cond_f

    if-lt v7, v5, :cond_11

    :cond_f
    add-int/lit8 v11, v6, 0x1

    if-ne v8, v11, :cond_10

    .line 134
    invoke-direct {p0, v5, v7}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->changePair(II)Z

    move-result v11

    if-eqz v11, :cond_11

    :cond_10
    add-int/lit8 v11, v6, 0x1

    if-gt v8, v11, :cond_11

    add-int/lit8 v11, v8, 0x1

    if-lt v11, v6, :cond_12

    const/4 v11, 0x3

    if-lt v6, v11, :cond_12

    .line 138
    invoke-direct {p0, v7, v5}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->changePair(II)Z

    move-result v11

    if-eqz v11, :cond_12

    :cond_11
    move v6, v10

    .line 139
    goto/16 :goto_0

    .line 142
    .end local v7           #newDist:I
    .end local v8           #newLen:I
    :cond_12
    add-int/lit8 v11, v6, -0x1

    invoke-static {v11, v13}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 143
    .local v4, limit:I
    const/4 v9, 0x0

    :goto_3
    if-ge v9, v14, :cond_14

    .line 144
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->reps:[I

    aget v12, v12, v9

    invoke-virtual {v11, v12, v4}, Lorg/tukaani/xz/lz/LZEncoder;->getMatchLen(II)I

    move-result v11

    if-ne v11, v4, :cond_13

    move v6, v10

    .line 145
    goto/16 :goto_0

    .line 143
    :cond_13
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 147
    :cond_14
    add-int/lit8 v10, v5, 0x4

    iput v10, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->back:I

    .line 148
    add-int/lit8 v10, v6, -0x2

    invoke-virtual {p0, v10}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->skip(I)V

    goto/16 :goto_0
.end method
