.class public Lorg/tukaani/xz/index/IndexEncoder;
.super Lorg/tukaani/xz/index/IndexBase;
.source "IndexEncoder.java"


# instance fields
.field private final records:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lorg/tukaani/xz/XZIOException;

    const-string v1, "XZ Stream or its Index has grown too big"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/tukaani/xz/index/IndexBase;-><init>(Lorg/tukaani/xz/XZIOException;)V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/tukaani/xz/index/IndexEncoder;->records:Ljava/util/ArrayList;

    .line 25
    return-void
.end method


# virtual methods
.method public add(JJ)V
    .locals 2
    .parameter "unpaddedSize"
    .parameter "uncompressedSize"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/tukaani/xz/XZIOException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-super {p0, p1, p2, p3, p4}, Lorg/tukaani/xz/index/IndexBase;->add(JJ)V

    .line 30
    iget-object v0, p0, Lorg/tukaani/xz/index/IndexEncoder;->records:Ljava/util/ArrayList;

    new-instance v1, Lorg/tukaani/xz/index/IndexRecord;

    invoke-direct {v1, p1, p2, p3, p4}, Lorg/tukaani/xz/index/IndexRecord;-><init>(JJ)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    return-void
.end method

.method public encode(Ljava/io/OutputStream;)V
    .locals 9
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 34
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    .line 35
    .local v0, crc32:Ljava/util/zip/CRC32;
    new-instance v2, Ljava/util/zip/CheckedOutputStream;

    invoke-direct {v2, p1, v0}, Ljava/util/zip/CheckedOutputStream;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Checksum;)V

    .line 38
    .local v2, outChecked:Ljava/util/zip/CheckedOutputStream;
    invoke-virtual {v2, v8}, Ljava/util/zip/CheckedOutputStream;->write(I)V

    .line 41
    iget-wide v6, p0, Lorg/tukaani/xz/index/IndexEncoder;->recordCount:J

    invoke-static {v2, v6, v7}, Lorg/tukaani/xz/common/EncoderUtil;->encodeVLI(Ljava/io/OutputStream;J)V

    .line 44
    iget-object v6, p0, Lorg/tukaani/xz/index/IndexEncoder;->records:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 45
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/tukaani/xz/index/IndexRecord;

    .line 46
    .local v3, record:Lorg/tukaani/xz/index/IndexRecord;
    iget-wide v6, v3, Lorg/tukaani/xz/index/IndexRecord;->unpadded:J

    invoke-static {v2, v6, v7}, Lorg/tukaani/xz/common/EncoderUtil;->encodeVLI(Ljava/io/OutputStream;J)V

    .line 47
    iget-wide v6, v3, Lorg/tukaani/xz/index/IndexRecord;->uncompressed:J

    invoke-static {v2, v6, v7}, Lorg/tukaani/xz/common/EncoderUtil;->encodeVLI(Ljava/io/OutputStream;J)V

    goto :goto_0

    .line 51
    .end local v3           #record:Lorg/tukaani/xz/index/IndexRecord;
    :cond_0
    invoke-virtual {p0}, Lorg/tukaani/xz/index/IndexEncoder;->getIndexPaddingSize()I

    move-result v1

    .local v1, i:I
    :goto_1
    if-lez v1, :cond_1

    .line 52
    invoke-virtual {v2, v8}, Ljava/util/zip/CheckedOutputStream;->write(I)V

    .line 51
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 55
    :cond_1
    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v4

    .line 56
    .local v4, value:J
    const/4 v1, 0x0

    :goto_2
    const/4 v6, 0x4

    if-ge v1, v6, :cond_2

    .line 57
    mul-int/lit8 v6, v1, 0x8

    ushr-long v6, v4, v6

    long-to-int v6, v6

    int-to-byte v6, v6

    invoke-virtual {p1, v6}, Ljava/io/OutputStream;->write(I)V

    .line 56
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 58
    :cond_2
    return-void
.end method

.method public bridge synthetic getIndexSize()J
    .locals 2

    .prologue
    .line 20
    invoke-super {p0}, Lorg/tukaani/xz/index/IndexBase;->getIndexSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic getStreamSize()J
    .locals 2

    .prologue
    .line 20
    invoke-super {p0}, Lorg/tukaani/xz/index/IndexBase;->getStreamSize()J

    move-result-wide v0

    return-wide v0
.end method
