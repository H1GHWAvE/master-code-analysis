.class public Lnet/lingala/zip4j/crypto/engine/AESEngine;
.super Ljava/lang/Object;
.source "AESEngine.java"


# static fields
.field private static final S:[B

.field private static final T0:[I

.field private static final rcon:[I


# instance fields
.field private C0:I

.field private C1:I

.field private C2:I

.field private C3:I

.field private rounds:I

.field private workingKey:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x100

    .line 197
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    .line 232
    const/16 v0, 0x1e

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->rcon:[I

    .line 236
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    return-void

    .line 197
    :array_0
    .array-data 0x1
        0x63t
        0x7ct
        0x77t
        0x7bt
        0xf2t
        0x6bt
        0x6ft
        0xc5t
        0x30t
        0x1t
        0x67t
        0x2bt
        0xfet
        0xd7t
        0xabt
        0x76t
        0xcat
        0x82t
        0xc9t
        0x7dt
        0xfat
        0x59t
        0x47t
        0xf0t
        0xadt
        0xd4t
        0xa2t
        0xaft
        0x9ct
        0xa4t
        0x72t
        0xc0t
        0xb7t
        0xfdt
        0x93t
        0x26t
        0x36t
        0x3ft
        0xf7t
        0xcct
        0x34t
        0xa5t
        0xe5t
        0xf1t
        0x71t
        0xd8t
        0x31t
        0x15t
        0x4t
        0xc7t
        0x23t
        0xc3t
        0x18t
        0x96t
        0x5t
        0x9at
        0x7t
        0x12t
        0x80t
        0xe2t
        0xebt
        0x27t
        0xb2t
        0x75t
        0x9t
        0x83t
        0x2ct
        0x1at
        0x1bt
        0x6et
        0x5at
        0xa0t
        0x52t
        0x3bt
        0xd6t
        0xb3t
        0x29t
        0xe3t
        0x2ft
        0x84t
        0x53t
        0xd1t
        0x0t
        0xedt
        0x20t
        0xfct
        0xb1t
        0x5bt
        0x6at
        0xcbt
        0xbet
        0x39t
        0x4at
        0x4ct
        0x58t
        0xcft
        0xd0t
        0xeft
        0xaat
        0xfbt
        0x43t
        0x4dt
        0x33t
        0x85t
        0x45t
        0xf9t
        0x2t
        0x7ft
        0x50t
        0x3ct
        0x9ft
        0xa8t
        0x51t
        0xa3t
        0x40t
        0x8ft
        0x92t
        0x9dt
        0x38t
        0xf5t
        0xbct
        0xb6t
        0xdat
        0x21t
        0x10t
        0xfft
        0xf3t
        0xd2t
        0xcdt
        0xct
        0x13t
        0xect
        0x5ft
        0x97t
        0x44t
        0x17t
        0xc4t
        0xa7t
        0x7et
        0x3dt
        0x64t
        0x5dt
        0x19t
        0x73t
        0x60t
        0x81t
        0x4ft
        0xdct
        0x22t
        0x2at
        0x90t
        0x88t
        0x46t
        0xeet
        0xb8t
        0x14t
        0xdet
        0x5et
        0xbt
        0xdbt
        0xe0t
        0x32t
        0x3at
        0xat
        0x49t
        0x6t
        0x24t
        0x5ct
        0xc2t
        0xd3t
        0xact
        0x62t
        0x91t
        0x95t
        0xe4t
        0x79t
        0xe7t
        0xc8t
        0x37t
        0x6dt
        0x8dt
        0xd5t
        0x4et
        0xa9t
        0x6ct
        0x56t
        0xf4t
        0xeat
        0x65t
        0x7at
        0xaet
        0x8t
        0xbat
        0x78t
        0x25t
        0x2et
        0x1ct
        0xa6t
        0xb4t
        0xc6t
        0xe8t
        0xddt
        0x74t
        0x1ft
        0x4bt
        0xbdt
        0x8bt
        0x8at
        0x70t
        0x3et
        0xb5t
        0x66t
        0x48t
        0x3t
        0xf6t
        0xet
        0x61t
        0x35t
        0x57t
        0xb9t
        0x86t
        0xc1t
        0x1dt
        0x9et
        0xe1t
        0xf8t
        0x98t
        0x11t
        0x69t
        0xd9t
        0x8et
        0x94t
        0x9bt
        0x1et
        0x87t
        0xe9t
        0xcet
        0x55t
        0x28t
        0xdft
        0x8ct
        0xa1t
        0x89t
        0xdt
        0xbft
        0xe6t
        0x42t
        0x68t
        0x41t
        0x99t
        0x2dt
        0xft
        0xb0t
        0x54t
        0xbbt
        0x16t
    .end array-data

    .line 232
    :array_1
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x10t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x40t 0x0t 0x0t 0x0t
        0x80t 0x0t 0x0t 0x0t
        0x1bt 0x0t 0x0t 0x0t
        0x36t 0x0t 0x0t 0x0t
        0x6ct 0x0t 0x0t 0x0t
        0xd8t 0x0t 0x0t 0x0t
        0xabt 0x0t 0x0t 0x0t
        0x4dt 0x0t 0x0t 0x0t
        0x9at 0x0t 0x0t 0x0t
        0x2ft 0x0t 0x0t 0x0t
        0x5et 0x0t 0x0t 0x0t
        0xbct 0x0t 0x0t 0x0t
        0x63t 0x0t 0x0t 0x0t
        0xc6t 0x0t 0x0t 0x0t
        0x97t 0x0t 0x0t 0x0t
        0x35t 0x0t 0x0t 0x0t
        0x6at 0x0t 0x0t 0x0t
        0xd4t 0x0t 0x0t 0x0t
        0xb3t 0x0t 0x0t 0x0t
        0x7dt 0x0t 0x0t 0x0t
        0xfat 0x0t 0x0t 0x0t
        0xeft 0x0t 0x0t 0x0t
        0xc5t 0x0t 0x0t 0x0t
        0x91t 0x0t 0x0t 0x0t
    .end array-data

    .line 236
    :array_2
    .array-data 0x4
        0xc6t 0x63t 0x63t 0xa5t
        0xf8t 0x7ct 0x7ct 0x84t
        0xeet 0x77t 0x77t 0x99t
        0xf6t 0x7bt 0x7bt 0x8dt
        0xfft 0xf2t 0xf2t 0xdt
        0xd6t 0x6bt 0x6bt 0xbdt
        0xdet 0x6ft 0x6ft 0xb1t
        0x91t 0xc5t 0xc5t 0x54t
        0x60t 0x30t 0x30t 0x50t
        0x2t 0x1t 0x1t 0x3t
        0xcet 0x67t 0x67t 0xa9t
        0x56t 0x2bt 0x2bt 0x7dt
        0xe7t 0xfet 0xfet 0x19t
        0xb5t 0xd7t 0xd7t 0x62t
        0x4dt 0xabt 0xabt 0xe6t
        0xect 0x76t 0x76t 0x9at
        0x8ft 0xcat 0xcat 0x45t
        0x1ft 0x82t 0x82t 0x9dt
        0x89t 0xc9t 0xc9t 0x40t
        0xfat 0x7dt 0x7dt 0x87t
        0xeft 0xfat 0xfat 0x15t
        0xb2t 0x59t 0x59t 0xebt
        0x8et 0x47t 0x47t 0xc9t
        0xfbt 0xf0t 0xf0t 0xbt
        0x41t 0xadt 0xadt 0xect
        0xb3t 0xd4t 0xd4t 0x67t
        0x5ft 0xa2t 0xa2t 0xfdt
        0x45t 0xaft 0xaft 0xeat
        0x23t 0x9ct 0x9ct 0xbft
        0x53t 0xa4t 0xa4t 0xf7t
        0xe4t 0x72t 0x72t 0x96t
        0x9bt 0xc0t 0xc0t 0x5bt
        0x75t 0xb7t 0xb7t 0xc2t
        0xe1t 0xfdt 0xfdt 0x1ct
        0x3dt 0x93t 0x93t 0xaet
        0x4ct 0x26t 0x26t 0x6at
        0x6ct 0x36t 0x36t 0x5at
        0x7et 0x3ft 0x3ft 0x41t
        0xf5t 0xf7t 0xf7t 0x2t
        0x83t 0xcct 0xcct 0x4ft
        0x68t 0x34t 0x34t 0x5ct
        0x51t 0xa5t 0xa5t 0xf4t
        0xd1t 0xe5t 0xe5t 0x34t
        0xf9t 0xf1t 0xf1t 0x8t
        0xe2t 0x71t 0x71t 0x93t
        0xabt 0xd8t 0xd8t 0x73t
        0x62t 0x31t 0x31t 0x53t
        0x2at 0x15t 0x15t 0x3ft
        0x8t 0x4t 0x4t 0xct
        0x95t 0xc7t 0xc7t 0x52t
        0x46t 0x23t 0x23t 0x65t
        0x9dt 0xc3t 0xc3t 0x5et
        0x30t 0x18t 0x18t 0x28t
        0x37t 0x96t 0x96t 0xa1t
        0xat 0x5t 0x5t 0xft
        0x2ft 0x9at 0x9at 0xb5t
        0xet 0x7t 0x7t 0x9t
        0x24t 0x12t 0x12t 0x36t
        0x1bt 0x80t 0x80t 0x9bt
        0xdft 0xe2t 0xe2t 0x3dt
        0xcdt 0xebt 0xebt 0x26t
        0x4et 0x27t 0x27t 0x69t
        0x7ft 0xb2t 0xb2t 0xcdt
        0xeat 0x75t 0x75t 0x9ft
        0x12t 0x9t 0x9t 0x1bt
        0x1dt 0x83t 0x83t 0x9et
        0x58t 0x2ct 0x2ct 0x74t
        0x34t 0x1at 0x1at 0x2et
        0x36t 0x1bt 0x1bt 0x2dt
        0xdct 0x6et 0x6et 0xb2t
        0xb4t 0x5at 0x5at 0xeet
        0x5bt 0xa0t 0xa0t 0xfbt
        0xa4t 0x52t 0x52t 0xf6t
        0x76t 0x3bt 0x3bt 0x4dt
        0xb7t 0xd6t 0xd6t 0x61t
        0x7dt 0xb3t 0xb3t 0xcet
        0x52t 0x29t 0x29t 0x7bt
        0xddt 0xe3t 0xe3t 0x3et
        0x5et 0x2ft 0x2ft 0x71t
        0x13t 0x84t 0x84t 0x97t
        0xa6t 0x53t 0x53t 0xf5t
        0xb9t 0xd1t 0xd1t 0x68t
        0x0t 0x0t 0x0t 0x0t
        0xc1t 0xedt 0xedt 0x2ct
        0x40t 0x20t 0x20t 0x60t
        0xe3t 0xfct 0xfct 0x1ft
        0x79t 0xb1t 0xb1t 0xc8t
        0xb6t 0x5bt 0x5bt 0xedt
        0xd4t 0x6at 0x6at 0xbet
        0x8dt 0xcbt 0xcbt 0x46t
        0x67t 0xbet 0xbet 0xd9t
        0x72t 0x39t 0x39t 0x4bt
        0x94t 0x4at 0x4at 0xdet
        0x98t 0x4ct 0x4ct 0xd4t
        0xb0t 0x58t 0x58t 0xe8t
        0x85t 0xcft 0xcft 0x4at
        0xbbt 0xd0t 0xd0t 0x6bt
        0xc5t 0xeft 0xeft 0x2at
        0x4ft 0xaat 0xaat 0xe5t
        0xedt 0xfbt 0xfbt 0x16t
        0x86t 0x43t 0x43t 0xc5t
        0x9at 0x4dt 0x4dt 0xd7t
        0x66t 0x33t 0x33t 0x55t
        0x11t 0x85t 0x85t 0x94t
        0x8at 0x45t 0x45t 0xcft
        0xe9t 0xf9t 0xf9t 0x10t
        0x4t 0x2t 0x2t 0x6t
        0xfet 0x7ft 0x7ft 0x81t
        0xa0t 0x50t 0x50t 0xf0t
        0x78t 0x3ct 0x3ct 0x44t
        0x25t 0x9ft 0x9ft 0xbat
        0x4bt 0xa8t 0xa8t 0xe3t
        0xa2t 0x51t 0x51t 0xf3t
        0x5dt 0xa3t 0xa3t 0xfet
        0x80t 0x40t 0x40t 0xc0t
        0x5t 0x8ft 0x8ft 0x8at
        0x3ft 0x92t 0x92t 0xadt
        0x21t 0x9dt 0x9dt 0xbct
        0x70t 0x38t 0x38t 0x48t
        0xf1t 0xf5t 0xf5t 0x4t
        0x63t 0xbct 0xbct 0xdft
        0x77t 0xb6t 0xb6t 0xc1t
        0xaft 0xdat 0xdat 0x75t
        0x42t 0x21t 0x21t 0x63t
        0x20t 0x10t 0x10t 0x30t
        0xe5t 0xfft 0xfft 0x1at
        0xfdt 0xf3t 0xf3t 0xet
        0xbft 0xd2t 0xd2t 0x6dt
        0x81t 0xcdt 0xcdt 0x4ct
        0x18t 0xct 0xct 0x14t
        0x26t 0x13t 0x13t 0x35t
        0xc3t 0xect 0xect 0x2ft
        0xbet 0x5ft 0x5ft 0xe1t
        0x35t 0x97t 0x97t 0xa2t
        0x88t 0x44t 0x44t 0xcct
        0x2et 0x17t 0x17t 0x39t
        0x93t 0xc4t 0xc4t 0x57t
        0x55t 0xa7t 0xa7t 0xf2t
        0xfct 0x7et 0x7et 0x82t
        0x7at 0x3dt 0x3dt 0x47t
        0xc8t 0x64t 0x64t 0xact
        0xbat 0x5dt 0x5dt 0xe7t
        0x32t 0x19t 0x19t 0x2bt
        0xe6t 0x73t 0x73t 0x95t
        0xc0t 0x60t 0x60t 0xa0t
        0x19t 0x81t 0x81t 0x98t
        0x9et 0x4ft 0x4ft 0xd1t
        0xa3t 0xdct 0xdct 0x7ft
        0x44t 0x22t 0x22t 0x66t
        0x54t 0x2at 0x2at 0x7et
        0x3bt 0x90t 0x90t 0xabt
        0xbt 0x88t 0x88t 0x83t
        0x8ct 0x46t 0x46t 0xcat
        0xc7t 0xeet 0xeet 0x29t
        0x6bt 0xb8t 0xb8t 0xd3t
        0x28t 0x14t 0x14t 0x3ct
        0xa7t 0xdet 0xdet 0x79t
        0xbct 0x5et 0x5et 0xe2t
        0x16t 0xbt 0xbt 0x1dt
        0xadt 0xdbt 0xdbt 0x76t
        0xdbt 0xe0t 0xe0t 0x3bt
        0x64t 0x32t 0x32t 0x56t
        0x74t 0x3at 0x3at 0x4et
        0x14t 0xat 0xat 0x1et
        0x92t 0x49t 0x49t 0xdbt
        0xct 0x6t 0x6t 0xat
        0x48t 0x24t 0x24t 0x6ct
        0xb8t 0x5ct 0x5ct 0xe4t
        0x9ft 0xc2t 0xc2t 0x5dt
        0xbdt 0xd3t 0xd3t 0x6et
        0x43t 0xact 0xact 0xeft
        0xc4t 0x62t 0x62t 0xa6t
        0x39t 0x91t 0x91t 0xa8t
        0x31t 0x95t 0x95t 0xa4t
        0xd3t 0xe4t 0xe4t 0x37t
        0xf2t 0x79t 0x79t 0x8bt
        0xd5t 0xe7t 0xe7t 0x32t
        0x8bt 0xc8t 0xc8t 0x43t
        0x6et 0x37t 0x37t 0x59t
        0xdat 0x6dt 0x6dt 0xb7t
        0x1t 0x8dt 0x8dt 0x8ct
        0xb1t 0xd5t 0xd5t 0x64t
        0x9ct 0x4et 0x4et 0xd2t
        0x49t 0xa9t 0xa9t 0xe0t
        0xd8t 0x6ct 0x6ct 0xb4t
        0xact 0x56t 0x56t 0xfat
        0xf3t 0xf4t 0xf4t 0x7t
        0xcft 0xeat 0xeat 0x25t
        0xcat 0x65t 0x65t 0xaft
        0xf4t 0x7at 0x7at 0x8et
        0x47t 0xaet 0xaet 0xe9t
        0x10t 0x8t 0x8t 0x18t
        0x6ft 0xbat 0xbat 0xd5t
        0xf0t 0x78t 0x78t 0x88t
        0x4at 0x25t 0x25t 0x6ft
        0x5ct 0x2et 0x2et 0x72t
        0x38t 0x1ct 0x1ct 0x24t
        0x57t 0xa6t 0xa6t 0xf1t
        0x73t 0xb4t 0xb4t 0xc7t
        0x97t 0xc6t 0xc6t 0x51t
        0xcbt 0xe8t 0xe8t 0x23t
        0xa1t 0xddt 0xddt 0x7ct
        0xe8t 0x74t 0x74t 0x9ct
        0x3et 0x1ft 0x1ft 0x21t
        0x96t 0x4bt 0x4bt 0xddt
        0x61t 0xbdt 0xbdt 0xdct
        0xdt 0x8bt 0x8bt 0x86t
        0xft 0x8at 0x8at 0x85t
        0xe0t 0x70t 0x70t 0x90t
        0x7ct 0x3et 0x3et 0x42t
        0x71t 0xb5t 0xb5t 0xc4t
        0xcct 0x66t 0x66t 0xaat
        0x90t 0x48t 0x48t 0xd8t
        0x6t 0x3t 0x3t 0x5t
        0xf7t 0xf6t 0xf6t 0x1t
        0x1ct 0xet 0xet 0x12t
        0xc2t 0x61t 0x61t 0xa3t
        0x6at 0x35t 0x35t 0x5ft
        0xaet 0x57t 0x57t 0xf9t
        0x69t 0xb9t 0xb9t 0xd0t
        0x17t 0x86t 0x86t 0x91t
        0x99t 0xc1t 0xc1t 0x58t
        0x3at 0x1dt 0x1dt 0x27t
        0x27t 0x9et 0x9et 0xb9t
        0xd9t 0xe1t 0xe1t 0x38t
        0xebt 0xf8t 0xf8t 0x13t
        0x2bt 0x98t 0x98t 0xb3t
        0x22t 0x11t 0x11t 0x33t
        0xd2t 0x69t 0x69t 0xbbt
        0xa9t 0xd9t 0xd9t 0x70t
        0x7t 0x8et 0x8et 0x89t
        0x33t 0x94t 0x94t 0xa7t
        0x2dt 0x9bt 0x9bt 0xb6t
        0x3ct 0x1et 0x1et 0x22t
        0x15t 0x87t 0x87t 0x92t
        0xc9t 0xe9t 0xe9t 0x20t
        0x87t 0xcet 0xcet 0x49t
        0xaat 0x55t 0x55t 0xfft
        0x50t 0x28t 0x28t 0x78t
        0xa5t 0xdft 0xdft 0x7at
        0x3t 0x8ct 0x8ct 0x8ft
        0x59t 0xa1t 0xa1t 0xf8t
        0x9t 0x89t 0x89t 0x80t
        0x1at 0xdt 0xdt 0x17t
        0x65t 0xbft 0xbft 0xdat
        0xd7t 0xe6t 0xe6t 0x31t
        0x84t 0x42t 0x42t 0xc6t
        0xd0t 0x68t 0x68t 0xb8t
        0x82t 0x41t 0x41t 0xc3t
        0x29t 0x99t 0x99t 0xb0t
        0x5at 0x2dt 0x2dt 0x77t
        0x1et 0xft 0xft 0x11t
        0x7bt 0xb0t 0xb0t 0xcbt
        0xa8t 0x54t 0x54t 0xfct
        0x6dt 0xbbt 0xbbt 0xd6t
        0x2ct 0x16t 0x16t 0x3at
    .end array-data
.end method

.method public constructor <init>([B)V
    .locals 1
    .parameter "key"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    check-cast v0, [[I

    iput-object v0, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->workingKey:[[I

    .line 34
    invoke-virtual {p0, p1}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->init([B)V

    .line 35
    return-void
.end method

.method private final encryptBlock([[I)V
    .locals 14
    .parameter "KW"

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    const/16 v11, 0x18

    const/16 v10, 0x10

    const/16 v9, 0x8

    .line 158
    iget v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    aget-object v7, p1, v12

    aget v7, v7, v12

    xor-int/2addr v6, v7

    iput v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    .line 159
    iget v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    aget-object v7, p1, v12

    aget v7, v7, v13

    xor-int/2addr v6, v7

    iput v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    .line 160
    iget v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    aget-object v7, p1, v12

    const/4 v8, 0x2

    aget v7, v7, v8

    xor-int/2addr v6, v7

    iput v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    .line 161
    iget v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    aget-object v7, p1, v12

    const/4 v8, 0x3

    aget v7, v7, v8

    xor-int/2addr v6, v7

    iput v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    .line 163
    const/4 v0, 0x1

    .line 165
    .local v0, r:I
    :goto_0
    iget v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->rounds:I

    add-int/lit8 v6, v6, -0x1

    if-ge v0, v6, :cond_0

    .line 167
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v7, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    and-int/lit16 v7, v7, 0xff

    aget v6, v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    shr-int/lit8 v8, v8, 0x8

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v11}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    shr-int/lit8 v8, v8, 0x10

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v10}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    shr-int/lit8 v8, v8, 0x18

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v9}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    aget-object v7, p1, v0

    aget v7, v7, v12

    xor-int v2, v6, v7

    .line 168
    .local v2, r0:I
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v7, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    and-int/lit16 v7, v7, 0xff

    aget v6, v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    shr-int/lit8 v8, v8, 0x8

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v11}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    shr-int/lit8 v8, v8, 0x10

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v10}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    shr-int/lit8 v8, v8, 0x18

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v9}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    aget-object v7, p1, v0

    aget v7, v7, v13

    xor-int v3, v6, v7

    .line 169
    .local v3, r1:I
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v7, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    and-int/lit16 v7, v7, 0xff

    aget v6, v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    shr-int/lit8 v8, v8, 0x8

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v11}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    shr-int/lit8 v8, v8, 0x10

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v10}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    shr-int/lit8 v8, v8, 0x18

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v9}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    aget-object v7, p1, v0

    const/4 v8, 0x2

    aget v7, v7, v8

    xor-int v4, v6, v7

    .line 170
    .local v4, r2:I
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v7, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    and-int/lit16 v7, v7, 0xff

    aget v6, v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    shr-int/lit8 v8, v8, 0x8

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v11}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    shr-int/lit8 v8, v8, 0x10

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v10}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    shr-int/lit8 v8, v8, 0x18

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v9}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    add-int/lit8 v1, v0, 0x1

    .end local v0           #r:I
    .local v1, r:I
    aget-object v7, p1, v0

    const/4 v8, 0x3

    aget v7, v7, v8

    xor-int v5, v6, v7

    .line 171
    .local v5, r3:I
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    and-int/lit16 v7, v2, 0xff

    aget v6, v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    shr-int/lit8 v8, v3, 0x8

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v11}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    shr-int/lit8 v8, v4, 0x10

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v10}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    shr-int/lit8 v8, v5, 0x18

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v9}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    aget-object v7, p1, v1

    aget v7, v7, v12

    xor-int/2addr v6, v7

    iput v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    .line 172
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    and-int/lit16 v7, v3, 0xff

    aget v6, v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    shr-int/lit8 v8, v4, 0x8

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v11}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    shr-int/lit8 v8, v5, 0x10

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v10}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    shr-int/lit8 v8, v2, 0x18

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v9}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    aget-object v7, p1, v1

    aget v7, v7, v13

    xor-int/2addr v6, v7

    iput v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    .line 173
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    and-int/lit16 v7, v4, 0xff

    aget v6, v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    shr-int/lit8 v8, v5, 0x8

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v11}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    shr-int/lit8 v8, v2, 0x10

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v10}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    shr-int/lit8 v8, v3, 0x18

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v9}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    aget-object v7, p1, v1

    const/4 v8, 0x2

    aget v7, v7, v8

    xor-int/2addr v6, v7

    iput v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    .line 174
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    and-int/lit16 v7, v5, 0xff

    aget v6, v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    shr-int/lit8 v8, v2, 0x8

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v11}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    shr-int/lit8 v8, v3, 0x10

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v10}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    shr-int/lit8 v8, v4, 0x18

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v9}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    add-int/lit8 v0, v1, 0x1

    .end local v1           #r:I
    .restart local v0       #r:I
    aget-object v7, p1, v1

    const/4 v8, 0x3

    aget v7, v7, v8

    xor-int/2addr v6, v7

    iput v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    goto/16 :goto_0

    .line 177
    .end local v2           #r0:I
    .end local v3           #r1:I
    .end local v4           #r2:I
    .end local v5           #r3:I
    :cond_0
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v7, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    and-int/lit16 v7, v7, 0xff

    aget v6, v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    shr-int/lit8 v8, v8, 0x8

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v11}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    shr-int/lit8 v8, v8, 0x10

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v10}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    shr-int/lit8 v8, v8, 0x18

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v9}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    aget-object v7, p1, v0

    aget v7, v7, v12

    xor-int v2, v6, v7

    .line 178
    .restart local v2       #r0:I
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v7, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    and-int/lit16 v7, v7, 0xff

    aget v6, v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    shr-int/lit8 v8, v8, 0x8

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v11}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    shr-int/lit8 v8, v8, 0x10

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v10}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    shr-int/lit8 v8, v8, 0x18

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v9}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    aget-object v7, p1, v0

    aget v7, v7, v13

    xor-int v3, v6, v7

    .line 179
    .restart local v3       #r1:I
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v7, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    and-int/lit16 v7, v7, 0xff

    aget v6, v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    shr-int/lit8 v8, v8, 0x8

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v11}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    shr-int/lit8 v8, v8, 0x10

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v10}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    shr-int/lit8 v8, v8, 0x18

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v9}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    aget-object v7, p1, v0

    const/4 v8, 0x2

    aget v7, v7, v8

    xor-int v4, v6, v7

    .line 180
    .restart local v4       #r2:I
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v7, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    and-int/lit16 v7, v7, 0xff

    aget v6, v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    shr-int/lit8 v8, v8, 0x8

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v11}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    shr-int/lit8 v8, v8, 0x10

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v10}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->T0:[I

    iget v8, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    shr-int/lit8 v8, v8, 0x18

    and-int/lit16 v8, v8, 0xff

    aget v7, v7, v8

    invoke-direct {p0, v7, v9}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v7

    xor-int/2addr v6, v7

    add-int/lit8 v1, v0, 0x1

    .end local v0           #r:I
    .restart local v1       #r:I
    aget-object v7, p1, v0

    const/4 v8, 0x3

    aget v7, v7, v8

    xor-int v5, v6, v7

    .line 182
    .restart local v5       #r3:I
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    and-int/lit16 v7, v2, 0xff

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v8, v3, 0x8

    and-int/lit16 v8, v8, 0xff

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x8

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v8, v4, 0x10

    and-int/lit16 v8, v8, 0xff

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x10

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v8, v5, 0x18

    and-int/lit16 v8, v8, 0xff

    aget-byte v7, v7, v8

    shl-int/lit8 v7, v7, 0x18

    xor-int/2addr v6, v7

    aget-object v7, p1, v1

    aget v7, v7, v12

    xor-int/2addr v6, v7

    iput v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    .line 183
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    and-int/lit16 v7, v3, 0xff

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v8, v4, 0x8

    and-int/lit16 v8, v8, 0xff

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x8

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v8, v5, 0x10

    and-int/lit16 v8, v8, 0xff

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x10

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v8, v2, 0x18

    and-int/lit16 v8, v8, 0xff

    aget-byte v7, v7, v8

    shl-int/lit8 v7, v7, 0x18

    xor-int/2addr v6, v7

    aget-object v7, p1, v1

    aget v7, v7, v13

    xor-int/2addr v6, v7

    iput v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    .line 184
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    and-int/lit16 v7, v4, 0xff

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v8, v5, 0x8

    and-int/lit16 v8, v8, 0xff

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x8

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v8, v2, 0x10

    and-int/lit16 v8, v8, 0xff

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x10

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v8, v3, 0x18

    and-int/lit16 v8, v8, 0xff

    aget-byte v7, v7, v8

    shl-int/lit8 v7, v7, 0x18

    xor-int/2addr v6, v7

    aget-object v7, p1, v1

    const/4 v8, 0x2

    aget v7, v7, v8

    xor-int/2addr v6, v7

    iput v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    .line 185
    sget-object v6, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    and-int/lit16 v7, v5, 0xff

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v8, v2, 0x8

    and-int/lit16 v8, v8, 0xff

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x8

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v8, v3, 0x10

    and-int/lit16 v8, v8, 0xff

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x10

    xor-int/2addr v6, v7

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v8, v4, 0x18

    and-int/lit16 v8, v8, 0xff

    aget-byte v7, v7, v8

    shl-int/lit8 v7, v7, 0x18

    xor-int/2addr v6, v7

    aget-object v7, p1, v1

    const/4 v8, 0x3

    aget v7, v7, v8

    xor-int/2addr v6, v7

    iput v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    .line 187
    return-void
.end method

.method private generateWorkingKey([B)[[I
    .locals 13
    .parameter "key"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    const/16 v12, 0x8

    const/4 v11, 0x6

    const/4 v10, 0x4

    .line 42
    array-length v6, p1

    div-int/lit8 v3, v6, 0x4

    .line 45
    .local v3, kc:I
    if-eq v3, v10, :cond_0

    if-eq v3, v11, :cond_0

    if-ne v3, v12, :cond_1

    :cond_0
    mul-int/lit8 v6, v3, 0x4

    array-length v7, p1

    if-eq v6, v7, :cond_2

    .line 47
    :cond_1
    new-instance v6, Lnet/lingala/zip4j/exception/ZipException;

    const-string v7, "invalid key length (not 128/192/256)"

    invoke-direct {v6, v7}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 50
    :cond_2
    add-int/lit8 v6, v3, 0x6

    iput v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->rounds:I

    .line 51
    iget v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->rounds:I

    add-int/lit8 v6, v6, 0x1

    filled-new-array {v6, v10}, [I

    move-result-object v6

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v7, v6}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    .line 53
    .local v0, W:[[I
    const/4 v4, 0x0

    .line 54
    .local v4, t:I
    const/4 v1, 0x0

    .line 55
    .local v1, i:I
    :goto_0
    array-length v6, p1

    if-ge v1, v6, :cond_3

    .line 57
    shr-int/lit8 v6, v4, 0x2

    aget-object v6, v0, v6

    and-int/lit8 v7, v4, 0x3

    aget-byte v8, p1, v1

    and-int/lit16 v8, v8, 0xff

    add-int/lit8 v9, v1, 0x1

    aget-byte v9, p1, v9

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x8

    or-int/2addr v8, v9

    add-int/lit8 v9, v1, 0x2

    aget-byte v9, p1, v9

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x10

    or-int/2addr v8, v9

    add-int/lit8 v9, v1, 0x3

    aget-byte v9, p1, v9

    shl-int/lit8 v9, v9, 0x18

    or-int/2addr v8, v9

    aput v8, v6, v7

    .line 58
    add-int/lit8 v1, v1, 0x4

    .line 59
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 62
    :cond_3
    iget v6, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->rounds:I

    add-int/lit8 v6, v6, 0x1

    shl-int/lit8 v2, v6, 0x2

    .line 63
    .local v2, k:I
    move v1, v3

    :goto_1
    if-ge v1, v2, :cond_6

    .line 65
    add-int/lit8 v6, v1, -0x1

    shr-int/lit8 v6, v6, 0x2

    aget-object v6, v0, v6

    add-int/lit8 v7, v1, -0x1

    and-int/lit8 v7, v7, 0x3

    aget v5, v6, v7

    .line 66
    .local v5, temp:I
    rem-int v6, v1, v3

    if-nez v6, :cond_5

    .line 68
    invoke-direct {p0, v5, v12}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->shift(II)I

    move-result v6

    invoke-direct {p0, v6}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->subWord(I)I

    move-result v6

    sget-object v7, Lnet/lingala/zip4j/crypto/engine/AESEngine;->rcon:[I

    div-int v8, v1, v3

    add-int/lit8 v8, v8, -0x1

    aget v7, v7, v8

    xor-int v5, v6, v7

    .line 75
    :cond_4
    :goto_2
    shr-int/lit8 v6, v1, 0x2

    aget-object v6, v0, v6

    and-int/lit8 v7, v1, 0x3

    sub-int v8, v1, v3

    shr-int/lit8 v8, v8, 0x2

    aget-object v8, v0, v8

    sub-int v9, v1, v3

    and-int/lit8 v9, v9, 0x3

    aget v8, v8, v9

    xor-int/2addr v8, v5

    aput v8, v6, v7

    .line 63
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 70
    :cond_5
    if-le v3, v11, :cond_4

    rem-int v6, v1, v3

    if-ne v6, v10, :cond_4

    .line 72
    invoke-direct {p0, v5}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->subWord(I)I

    move-result v5

    goto :goto_2

    .line 77
    .end local v5           #temp:I
    :cond_6
    return-object v0
.end method

.method private shift(II)I
    .locals 2
    .parameter "r"
    .parameter "shift"

    .prologue
    .line 190
    ushr-int v0, p1, p2

    neg-int v1, p2

    shl-int v1, p1, v1

    or-int/2addr v0, v1

    return v0
.end method

.method private final stateIn([BI)V
    .locals 4
    .parameter "bytes"
    .parameter "off"

    .prologue
    .line 108
    move v0, p2

    .line 110
    .local v0, index:I
    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .local v1, index:I
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    .line 111
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    aget-byte v3, p1, v1

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    .line 112
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    aget-byte v3, p1, v0

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    .line 113
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    aget-byte v3, p1, v1

    shl-int/lit8 v3, v3, 0x18

    or-int/2addr v2, v3

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    .line 115
    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    .line 116
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    aget-byte v3, p1, v1

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    .line 117
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    aget-byte v3, p1, v0

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    .line 118
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    aget-byte v3, p1, v1

    shl-int/lit8 v3, v3, 0x18

    or-int/2addr v2, v3

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    .line 120
    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    .line 121
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    aget-byte v3, p1, v1

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    .line 122
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    aget-byte v3, p1, v0

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    .line 123
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    aget-byte v3, p1, v1

    shl-int/lit8 v3, v3, 0x18

    or-int/2addr v2, v3

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    .line 125
    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    .line 126
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    aget-byte v3, p1, v1

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    .line 127
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    aget-byte v3, p1, v0

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    .line 128
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    aget-byte v3, p1, v1

    shl-int/lit8 v3, v3, 0x18

    or-int/2addr v2, v3

    iput v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    .line 129
    return-void
.end method

.method private final stateOut([BI)V
    .locals 3
    .parameter "bytes"
    .parameter "off"

    .prologue
    .line 132
    move v0, p2

    .line 134
    .local v0, index:I
    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .local v1, index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    int-to-byte v2, v2

    aput-byte v2, p1, v0

    .line 135
    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    .line 136
    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    shr-int/lit8 v2, v2, 0x10

    int-to-byte v2, v2

    aput-byte v2, p1, v0

    .line 137
    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C0:I

    shr-int/lit8 v2, v2, 0x18

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    .line 139
    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    int-to-byte v2, v2

    aput-byte v2, p1, v0

    .line 140
    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    .line 141
    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    shr-int/lit8 v2, v2, 0x10

    int-to-byte v2, v2

    aput-byte v2, p1, v0

    .line 142
    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C1:I

    shr-int/lit8 v2, v2, 0x18

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    .line 144
    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    int-to-byte v2, v2

    aput-byte v2, p1, v0

    .line 145
    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    .line 146
    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    shr-int/lit8 v2, v2, 0x10

    int-to-byte v2, v2

    aput-byte v2, p1, v0

    .line 147
    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C2:I

    shr-int/lit8 v2, v2, 0x18

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    .line 149
    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    int-to-byte v2, v2

    aput-byte v2, p1, v0

    .line 150
    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    .line 151
    add-int/lit8 v1, v0, 0x1

    .end local v0           #index:I
    .restart local v1       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    shr-int/lit8 v2, v2, 0x10

    int-to-byte v2, v2

    aput-byte v2, p1, v0

    .line 152
    add-int/lit8 v0, v1, 0x1

    .end local v1           #index:I
    .restart local v0       #index:I
    iget v2, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->C3:I

    shr-int/lit8 v2, v2, 0x18

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    .line 153
    return-void
.end method

.method private subWord(I)I
    .locals 3
    .parameter "x"

    .prologue
    .line 194
    sget-object v0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    and-int/lit16 v1, p1, 0xff

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    sget-object v1, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v2, p1, 0x8

    and-int/lit16 v2, v2, 0xff

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    sget-object v1, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v2, p1, 0x10

    and-int/lit16 v2, v2, 0xff

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    sget-object v1, Lnet/lingala/zip4j/crypto/engine/AESEngine;->S:[B

    shr-int/lit8 v2, p1, 0x18

    and-int/lit16 v2, v2, 0xff

    aget-byte v1, v1, v2

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public init([B)V
    .locals 1
    .parameter "key"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->generateWorkingKey([B)[[I

    move-result-object v0

    iput-object v0, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->workingKey:[[I

    .line 39
    return-void
.end method

.method public processBlock([BI[BI)I
    .locals 2
    .parameter "in"
    .parameter "inOff"
    .parameter "out"
    .parameter "outOff"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->workingKey:[[I

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Lnet/lingala/zip4j/exception/ZipException;

    const-string v1, "AES engine not initialised"

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    add-int/lit8 v0, p2, 0x10

    array-length v1, p1

    if-le v0, v1, :cond_1

    .line 92
    new-instance v0, Lnet/lingala/zip4j/exception/ZipException;

    const-string v1, "input buffer too short"

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_1
    add-int/lit8 v0, p4, 0x10

    array-length v1, p3

    if-le v0, v1, :cond_2

    .line 97
    new-instance v0, Lnet/lingala/zip4j/exception/ZipException;

    const-string v1, "output buffer too short"

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_2
    invoke-direct {p0, p1, p2}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->stateIn([BI)V

    .line 101
    iget-object v0, p0, Lnet/lingala/zip4j/crypto/engine/AESEngine;->workingKey:[[I

    invoke-direct {p0, v0}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->encryptBlock([[I)V

    .line 102
    invoke-direct {p0, p3, p4}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->stateOut([BI)V

    .line 104
    const/16 v0, 0x10

    return v0
.end method

.method public processBlock([B[B)I
    .locals 1
    .parameter "in"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-virtual {p0, p1, v0, p2, v0}, Lnet/lingala/zip4j/crypto/engine/AESEngine;->processBlock([BI[BI)I

    move-result v0

    return v0
.end method
