.class public Lkellinwood/zipio/ZipOutput;
.super Ljava/lang/Object;
.source "ZipOutput.java"


# static fields
.field static log:Lkellinwood/logging/LoggerInterface;


# instance fields
.field entriesWritten:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkellinwood/zipio/ZioEntry;",
            ">;"
        }
    .end annotation
.end field

.field filePointer:I

.field namesWritten:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field out:Ljava/io/OutputStream;

.field outputFilename:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 2
    .parameter "outputFile"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v1, 0x0

    iput-object v1, p0, Lkellinwood/zipio/ZipOutput;->out:Ljava/io/OutputStream;

    .line 39
    const/4 v1, 0x0

    iput v1, p0, Lkellinwood/zipio/ZipOutput;->filePointer:I

    .line 41
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lkellinwood/zipio/ZipOutput;->entriesWritten:Ljava/util/List;

    .line 42
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lkellinwood/zipio/ZipOutput;->namesWritten:Ljava/util/Set;

    .line 53
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lkellinwood/zipio/ZipOutput;->outputFilename:Ljava/lang/String;

    .line 54
    move-object v0, p1

    .line 55
    .local v0, ofile:Ljava/io/File;
    invoke-direct {p0, v0}, Lkellinwood/zipio/ZipOutput;->init(Ljava/io/File;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .parameter "os"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lkellinwood/zipio/ZipOutput;->out:Ljava/io/OutputStream;

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lkellinwood/zipio/ZipOutput;->filePointer:I

    .line 41
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lkellinwood/zipio/ZipOutput;->entriesWritten:Ljava/util/List;

    .line 42
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lkellinwood/zipio/ZipOutput;->namesWritten:Ljava/util/Set;

    .line 68
    iput-object p1, p0, Lkellinwood/zipio/ZipOutput;->out:Ljava/io/OutputStream;

    .line 69
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .parameter "filename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v1, 0x0

    iput-object v1, p0, Lkellinwood/zipio/ZipOutput;->out:Ljava/io/OutputStream;

    .line 39
    const/4 v1, 0x0

    iput v1, p0, Lkellinwood/zipio/ZipOutput;->filePointer:I

    .line 41
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lkellinwood/zipio/ZipOutput;->entriesWritten:Ljava/util/List;

    .line 42
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lkellinwood/zipio/ZipOutput;->namesWritten:Ljava/util/Set;

    .line 46
    iput-object p1, p0, Lkellinwood/zipio/ZipOutput;->outputFilename:Ljava/lang/String;

    .line 47
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lkellinwood/zipio/ZipOutput;->outputFilename:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 48
    .local v0, ofile:Ljava/io/File;
    invoke-direct {p0, v0}, Lkellinwood/zipio/ZipOutput;->init(Ljava/io/File;)V

    .line 49
    return-void
.end method

.method private static getLogger()Lkellinwood/logging/LoggerInterface;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lkellinwood/zipio/ZipOutput;->log:Lkellinwood/logging/LoggerInterface;

    if-nez v0, :cond_0

    const-class v0, Lkellinwood/zipio/ZipOutput;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkellinwood/logging/LoggerManager;->getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    sput-object v0, Lkellinwood/zipio/ZipOutput;->log:Lkellinwood/logging/LoggerInterface;

    .line 73
    :cond_0
    sget-object v0, Lkellinwood/zipio/ZipOutput;->log:Lkellinwood/logging/LoggerInterface;

    return-object v0
.end method

.method private init(Ljava/io/File;)V
    .locals 1
    .parameter "ofile"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 61
    :cond_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lkellinwood/zipio/ZipOutput;->out:Ljava/io/OutputStream;

    .line 62
    invoke-static {}, Lkellinwood/zipio/ZipOutput;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    invoke-interface {v0}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lkellinwood/zipio/ZipOutput;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    invoke-static {v0}, Lkellinwood/zipio/ZipListingHelper;->listHeader(Lkellinwood/logging/LoggerInterface;)V

    .line 64
    :cond_1
    return-void
.end method


# virtual methods
.method public close()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    new-instance v0, Lkellinwood/zipio/CentralEnd;

    invoke-direct {v0}, Lkellinwood/zipio/CentralEnd;-><init>()V

    .line 95
    .local v0, centralEnd:Lkellinwood/zipio/CentralEnd;
    invoke-virtual {p0}, Lkellinwood/zipio/ZipOutput;->getFilePointer()I

    move-result v3

    iput v3, v0, Lkellinwood/zipio/CentralEnd;->centralStartOffset:I

    .line 96
    iget-object v3, p0, Lkellinwood/zipio/ZipOutput;->entriesWritten:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v0, Lkellinwood/zipio/CentralEnd;->totalCentralEntries:S

    iput-short v3, v0, Lkellinwood/zipio/CentralEnd;->numCentralEntries:S

    .line 98
    iget-object v3, p0, Lkellinwood/zipio/ZipOutput;->entriesWritten:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkellinwood/zipio/ZioEntry;

    .line 99
    .local v1, entry:Lkellinwood/zipio/ZioEntry;
    invoke-virtual {v1, p0}, Lkellinwood/zipio/ZioEntry;->write(Lkellinwood/zipio/ZipOutput;)V

    goto :goto_0

    .line 102
    .end local v1           #entry:Lkellinwood/zipio/ZioEntry;
    :cond_0
    invoke-virtual {p0}, Lkellinwood/zipio/ZipOutput;->getFilePointer()I

    move-result v3

    iget v4, v0, Lkellinwood/zipio/CentralEnd;->centralStartOffset:I

    sub-int/2addr v3, v4

    iput v3, v0, Lkellinwood/zipio/CentralEnd;->centralDirectorySize:I

    .line 103
    const-string v3, ""

    iput-object v3, v0, Lkellinwood/zipio/CentralEnd;->fileComment:Ljava/lang/String;

    .line 105
    invoke-virtual {v0, p0}, Lkellinwood/zipio/CentralEnd;->write(Lkellinwood/zipio/ZipOutput;)V

    .line 107
    iget-object v3, p0, Lkellinwood/zipio/ZipOutput;->out:Ljava/io/OutputStream;

    if-eqz v3, :cond_1

    :try_start_0
    iget-object v3, p0, Lkellinwood/zipio/ZipOutput;->out:Ljava/io/OutputStream;

    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :cond_1
    :goto_1
    return-void

    .line 107
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public getFilePointer()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    iget v0, p0, Lkellinwood/zipio/ZipOutput;->filePointer:I

    return v0
.end method

.method public write(Lkellinwood/zipio/ZioEntry;)V
    .locals 4
    .parameter "entry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p1}, Lkellinwood/zipio/ZioEntry;->getName()Ljava/lang/String;

    move-result-object v0

    .line 78
    .local v0, entryName:Ljava/lang/String;
    iget-object v1, p0, Lkellinwood/zipio/ZipOutput;->namesWritten:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 79
    invoke-static {}, Lkellinwood/zipio/ZipOutput;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Skipping duplicate file in output: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lkellinwood/logging/LoggerInterface;->warning(Ljava/lang/String;)V

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    invoke-virtual {p1, p0}, Lkellinwood/zipio/ZioEntry;->writeLocalEntry(Lkellinwood/zipio/ZipOutput;)V

    .line 83
    iget-object v1, p0, Lkellinwood/zipio/ZipOutput;->entriesWritten:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object v1, p0, Lkellinwood/zipio/ZipOutput;->namesWritten:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 85
    invoke-static {}, Lkellinwood/zipio/ZipOutput;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v1

    invoke-interface {v1}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lkellinwood/zipio/ZipOutput;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v1

    invoke-static {v1, p1}, Lkellinwood/zipio/ZipListingHelper;->listEntry(Lkellinwood/logging/LoggerInterface;Lkellinwood/zipio/ZioEntry;)V

    goto :goto_0
.end method

.method public writeBytes([B)V
    .locals 2
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lkellinwood/zipio/ZipOutput;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 145
    iget v0, p0, Lkellinwood/zipio/ZipOutput;->filePointer:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lkellinwood/zipio/ZipOutput;->filePointer:I

    .line 146
    return-void
.end method

.method public writeBytes([BII)V
    .locals 1
    .parameter "value"
    .parameter "offset"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lkellinwood/zipio/ZipOutput;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 151
    iget v0, p0, Lkellinwood/zipio/ZipOutput;->filePointer:I

    add-int/2addr v0, p3

    iput v0, p0, Lkellinwood/zipio/ZipOutput;->filePointer:I

    .line 152
    return-void
.end method

.method public writeInt(I)V
    .locals 4
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    .line 116
    new-array v0, v3, [B

    .line 117
    .local v0, data:[B
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 118
    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 119
    shr-int/lit8 p1, p1, 0x8

    .line 117
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 121
    :cond_0
    iget-object v2, p0, Lkellinwood/zipio/ZipOutput;->out:Ljava/io/OutputStream;

    invoke-virtual {v2, v0}, Ljava/io/OutputStream;->write([B)V

    .line 122
    iget v2, p0, Lkellinwood/zipio/ZipOutput;->filePointer:I

    add-int/lit8 v2, v2, 0x4

    iput v2, p0, Lkellinwood/zipio/ZipOutput;->filePointer:I

    .line 123
    return-void
.end method

.method public writeShort(S)V
    .locals 4
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 126
    new-array v0, v3, [B

    .line 127
    .local v0, data:[B
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 128
    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 129
    shr-int/lit8 v2, p1, 0x8

    int-to-short p1, v2

    .line 127
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 131
    :cond_0
    iget-object v2, p0, Lkellinwood/zipio/ZipOutput;->out:Ljava/io/OutputStream;

    invoke-virtual {v2, v0}, Ljava/io/OutputStream;->write([B)V

    .line 132
    iget v2, p0, Lkellinwood/zipio/ZipOutput;->filePointer:I

    add-int/lit8 v2, v2, 0x2

    iput v2, p0, Lkellinwood/zipio/ZipOutput;->filePointer:I

    .line 133
    return-void
.end method

.method public writeString(Ljava/lang/String;)V
    .locals 3
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 138
    .local v0, data:[B
    iget-object v1, p0, Lkellinwood/zipio/ZipOutput;->out:Ljava/io/OutputStream;

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 139
    iget v1, p0, Lkellinwood/zipio/ZipOutput;->filePointer:I

    array-length v2, v0

    add-int/2addr v1, v2

    iput v1, p0, Lkellinwood/zipio/ZipOutput;->filePointer:I

    .line 140
    return-void
.end method
