.class public Lkellinwood/logging/StreamLogger;
.super Lkellinwood/logging/AbstractLogger;
.source "StreamLogger.java"


# instance fields
.field out:Ljava/io/PrintStream;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/io/PrintStream;)V
    .locals 0
    .parameter "category"
    .parameter "out"

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lkellinwood/logging/AbstractLogger;-><init>(Ljava/lang/String;)V

    .line 27
    iput-object p2, p0, Lkellinwood/logging/StreamLogger;->out:Ljava/io/PrintStream;

    .line 28
    return-void
.end method


# virtual methods
.method protected write(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .parameter "level"
    .parameter "message"
    .parameter "t"

    .prologue
    .line 32
    iget-object v0, p0, Lkellinwood/logging/StreamLogger;->out:Ljava/io/PrintStream;

    invoke-virtual {p0, p1, p2}, Lkellinwood/logging/StreamLogger;->format(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 33
    if-eqz p3, :cond_0

    iget-object v0, p0, Lkellinwood/logging/StreamLogger;->out:Ljava/io/PrintStream;

    invoke-virtual {p3, v0}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 34
    :cond_0
    return-void
.end method
