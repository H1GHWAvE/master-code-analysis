.class public Lkellinwood/logging/android/AndroidLogger;
.super Lkellinwood/logging/AbstractLogger;
.source "AndroidLogger.java"


# instance fields
.field isDebugToastEnabled:Z

.field isErrorToastEnabled:Z

.field isInfoToastEnabled:Z

.field isWarningToastEnabled:Z

.field toastContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .parameter "c"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 30
    invoke-direct {p0, p1}, Lkellinwood/logging/AbstractLogger;-><init>(Ljava/lang/String;)V

    .line 38
    iput-boolean v2, p0, Lkellinwood/logging/android/AndroidLogger;->isErrorToastEnabled:Z

    .line 39
    iput-boolean v2, p0, Lkellinwood/logging/android/AndroidLogger;->isWarningToastEnabled:Z

    .line 40
    iput-boolean v1, p0, Lkellinwood/logging/android/AndroidLogger;->isInfoToastEnabled:Z

    .line 41
    iput-boolean v1, p0, Lkellinwood/logging/android/AndroidLogger;->isDebugToastEnabled:Z

    .line 31
    iget-object v1, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 32
    .local v0, pos:I
    if-lez v0, :cond_0

    iget-object v1, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    .line 34
    :cond_0
    return-void
.end method


# virtual methods
.method public debugLO(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .parameter "message"
    .parameter "t"

    .prologue
    .line 109
    iget-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isDebugToastEnabled:Z

    .line 110
    .local v0, toastState:Z
    const/4 v1, 0x0

    iput-boolean v1, p0, Lkellinwood/logging/android/AndroidLogger;->isDebugToastEnabled:Z

    .line 111
    const-string v1, "DEBUG"

    invoke-virtual {p0, v1, p1, p2}, Lkellinwood/logging/android/AndroidLogger;->writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 112
    iput-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isDebugToastEnabled:Z

    .line 113
    return-void
.end method

.method public errorLO(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .parameter "message"
    .parameter "t"

    .prologue
    .line 85
    iget-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isErrorToastEnabled:Z

    .line 86
    .local v0, toastState:Z
    const/4 v1, 0x0

    iput-boolean v1, p0, Lkellinwood/logging/android/AndroidLogger;->isErrorToastEnabled:Z

    .line 87
    const-string v1, "ERROR"

    invoke-virtual {p0, v1, p1, p2}, Lkellinwood/logging/android/AndroidLogger;->writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 88
    iput-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isErrorToastEnabled:Z

    .line 89
    return-void
.end method

.method public getToastContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lkellinwood/logging/android/AndroidLogger;->toastContext:Landroid/content/Context;

    return-object v0
.end method

.method public infoLO(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .parameter "message"
    .parameter "t"

    .prologue
    .line 101
    iget-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isInfoToastEnabled:Z

    .line 102
    .local v0, toastState:Z
    const/4 v1, 0x0

    iput-boolean v1, p0, Lkellinwood/logging/android/AndroidLogger;->isInfoToastEnabled:Z

    .line 103
    const-string v1, "INFO"

    invoke-virtual {p0, v1, p1, p2}, Lkellinwood/logging/android/AndroidLogger;->writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 104
    iput-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isInfoToastEnabled:Z

    .line 105
    return-void
.end method

.method public isDebugEnabled()Z
    .locals 3

    .prologue
    .line 151
    iget-object v1, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    .line 152
    .local v0, enabled:Z
    return v0
.end method

.method public isDebugToastEnabled()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isDebugToastEnabled:Z

    return v0
.end method

.method public isErrorEnabled()Z
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public isErrorToastEnabled()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isErrorToastEnabled:Z

    return v0
.end method

.method public isInfoEnabled()Z
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public isInfoToastEnabled()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isInfoToastEnabled:Z

    return v0
.end method

.method public isWarningEnabled()Z
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public isWarningToastEnabled()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isWarningToastEnabled:Z

    return v0
.end method

.method public setDebugToastEnabled(Z)V
    .locals 0
    .parameter "isDebugToastEnabled"

    .prologue
    .line 80
    iput-boolean p1, p0, Lkellinwood/logging/android/AndroidLogger;->isDebugToastEnabled:Z

    .line 81
    return-void
.end method

.method public setErrorToastEnabled(Z)V
    .locals 0
    .parameter "isErrorToastEnabled"

    .prologue
    .line 56
    iput-boolean p1, p0, Lkellinwood/logging/android/AndroidLogger;->isErrorToastEnabled:Z

    .line 57
    return-void
.end method

.method public setInfoToastEnabled(Z)V
    .locals 0
    .parameter "isInfoToastEnabled"

    .prologue
    .line 72
    iput-boolean p1, p0, Lkellinwood/logging/android/AndroidLogger;->isInfoToastEnabled:Z

    .line 73
    return-void
.end method

.method public setToastContext(Landroid/content/Context;)V
    .locals 0
    .parameter "toastContext"

    .prologue
    .line 48
    iput-object p1, p0, Lkellinwood/logging/android/AndroidLogger;->toastContext:Landroid/content/Context;

    .line 49
    return-void
.end method

.method public setWarningToastEnabled(Z)V
    .locals 0
    .parameter "isWarningToastEnabled"

    .prologue
    .line 64
    iput-boolean p1, p0, Lkellinwood/logging/android/AndroidLogger;->isWarningToastEnabled:Z

    .line 65
    return-void
.end method

.method protected toast(Ljava/lang/String;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 118
    :try_start_0
    iget-object v1, p0, Lkellinwood/logging/android/AndroidLogger;->toastContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lkellinwood/logging/android/AndroidLogger;->toastContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v1, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, t:Ljava/lang/Throwable;
    iget-object v1, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    invoke-static {v1, p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public warningLO(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .parameter "message"
    .parameter "t"

    .prologue
    .line 93
    iget-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isWarningToastEnabled:Z

    .line 94
    .local v0, toastState:Z
    const/4 v1, 0x0

    iput-boolean v1, p0, Lkellinwood/logging/android/AndroidLogger;->isWarningToastEnabled:Z

    .line 95
    const-string v1, "WARNING"

    invoke-virtual {p0, v1, p1, p2}, Lkellinwood/logging/android/AndroidLogger;->writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 96
    iput-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isWarningToastEnabled:Z

    .line 97
    return-void
.end method

.method public write(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "level"
    .parameter "message"
    .parameter "t"

    .prologue
    .line 127
    const-string v0, "ERROR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128
    if-eqz p3, :cond_1

    iget-object v0, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    invoke-static {v0, p2, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 130
    :goto_0
    iget-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isErrorToastEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lkellinwood/logging/android/AndroidLogger;->toast(Ljava/lang/String;)V

    .line 147
    :cond_0
    :goto_1
    return-void

    .line 129
    :cond_1
    iget-object v0, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 132
    :cond_2
    const-string v0, "DEBUG"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 133
    if-eqz p3, :cond_3

    iget-object v0, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    invoke-static {v0, p2, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 135
    :goto_2
    iget-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isDebugToastEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lkellinwood/logging/android/AndroidLogger;->toast(Ljava/lang/String;)V

    goto :goto_1

    .line 134
    :cond_3
    iget-object v0, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 137
    :cond_4
    const-string v0, "WARNING"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 138
    if-eqz p3, :cond_5

    iget-object v0, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    invoke-static {v0, p2, p3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 140
    :goto_3
    iget-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isWarningToastEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lkellinwood/logging/android/AndroidLogger;->toast(Ljava/lang/String;)V

    goto :goto_1

    .line 139
    :cond_5
    iget-object v0, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 142
    :cond_6
    const-string v0, "INFO"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    if-eqz p3, :cond_7

    iget-object v0, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    invoke-static {v0, p2, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 145
    :goto_4
    iget-boolean v0, p0, Lkellinwood/logging/android/AndroidLogger;->isInfoToastEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lkellinwood/logging/android/AndroidLogger;->toast(Ljava/lang/String;)V

    goto :goto_1

    .line 144
    :cond_7
    iget-object v0, p0, Lkellinwood/logging/android/AndroidLogger;->category:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4
.end method
