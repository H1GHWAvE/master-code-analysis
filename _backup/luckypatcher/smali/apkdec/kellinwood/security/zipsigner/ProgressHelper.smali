.class public Lkellinwood/security/zipsigner/ProgressHelper;
.super Ljava/lang/Object;
.source "ProgressHelper.java"


# instance fields
.field private listeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lkellinwood/security/zipsigner/ProgressListener;",
            ">;"
        }
    .end annotation
.end field

.field private progressCurrentItem:I

.field private progressEvent:Lkellinwood/security/zipsigner/ProgressEvent;

.field private progressTotalItems:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v0, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressTotalItems:I

    .line 23
    iput v0, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressCurrentItem:I

    .line 24
    new-instance v0, Lkellinwood/security/zipsigner/ProgressEvent;

    invoke-direct {v0}, Lkellinwood/security/zipsigner/ProgressEvent;-><init>()V

    iput-object v0, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressEvent:Lkellinwood/security/zipsigner/ProgressEvent;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkellinwood/security/zipsigner/ProgressHelper;->listeners:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public declared-synchronized addProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
    .locals 2
    .parameter "l"

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lkellinwood/security/zipsigner/ProgressHelper;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 71
    .local v0, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lkellinwood/security/zipsigner/ProgressListener;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    iput-object v0, p0, Lkellinwood/security/zipsigner/ProgressHelper;->listeners:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    monitor-exit p0

    return-void

    .line 70
    .end local v0           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lkellinwood/security/zipsigner/ProgressListener;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getProgressCurrentItem()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressCurrentItem:I

    return v0
.end method

.method public getProgressTotalItems()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressTotalItems:I

    return v0
.end method

.method public initProgress()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x2710

    iput v0, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressTotalItems:I

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressCurrentItem:I

    .line 30
    return-void
.end method

.method public progress(ILjava/lang/String;)V
    .locals 5
    .parameter "priority"
    .parameter "message"

    .prologue
    .line 50
    iget v3, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressCurrentItem:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressCurrentItem:I

    .line 53
    iget v3, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressTotalItems:I

    if-nez v3, :cond_0

    const/4 v2, 0x0

    .line 57
    .local v2, percentDone:I
    :goto_0
    iget-object v3, p0, Lkellinwood/security/zipsigner/ProgressHelper;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkellinwood/security/zipsigner/ProgressListener;

    .line 58
    .local v1, listener:Lkellinwood/security/zipsigner/ProgressListener;
    iget-object v3, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressEvent:Lkellinwood/security/zipsigner/ProgressEvent;

    invoke-virtual {v3, p2}, Lkellinwood/security/zipsigner/ProgressEvent;->setMessage(Ljava/lang/String;)V

    .line 59
    iget-object v3, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressEvent:Lkellinwood/security/zipsigner/ProgressEvent;

    invoke-virtual {v3, v2}, Lkellinwood/security/zipsigner/ProgressEvent;->setPercentDone(I)V

    .line 60
    iget-object v3, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressEvent:Lkellinwood/security/zipsigner/ProgressEvent;

    invoke-virtual {v3, p1}, Lkellinwood/security/zipsigner/ProgressEvent;->setPriority(I)V

    .line 61
    iget-object v3, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressEvent:Lkellinwood/security/zipsigner/ProgressEvent;

    invoke-interface {v1, v3}, Lkellinwood/security/zipsigner/ProgressListener;->onProgress(Lkellinwood/security/zipsigner/ProgressEvent;)V

    goto :goto_1

    .line 54
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #listener:Lkellinwood/security/zipsigner/ProgressListener;
    .end local v2           #percentDone:I
    :cond_0
    iget v3, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressCurrentItem:I

    mul-int/lit8 v3, v3, 0x64

    iget v4, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressTotalItems:I

    div-int v2, v3, v4

    .restart local v2       #percentDone:I
    goto :goto_0

    .line 63
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method public declared-synchronized removeProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
    .locals 2
    .parameter "l"

    .prologue
    .line 78
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lkellinwood/security/zipsigner/ProgressHelper;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 79
    .local v0, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lkellinwood/security/zipsigner/ProgressListener;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 80
    iput-object v0, p0, Lkellinwood/security/zipsigner/ProgressHelper;->listeners:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    monitor-exit p0

    return-void

    .line 78
    .end local v0           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lkellinwood/security/zipsigner/ProgressListener;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setProgressCurrentItem(I)V
    .locals 0
    .parameter "progressCurrentItem"

    .prologue
    .line 45
    iput p1, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressCurrentItem:I

    .line 46
    return-void
.end method

.method public setProgressTotalItems(I)V
    .locals 0
    .parameter "progressTotalItems"

    .prologue
    .line 37
    iput p1, p0, Lkellinwood/security/zipsigner/ProgressHelper;->progressTotalItems:I

    .line 38
    return-void
.end method
