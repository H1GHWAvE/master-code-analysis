.class public Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;
.super Ljava/util/LinkedHashMap;
.source "DistinguishedNameValues.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<",
        "Lorg/spongycastle/asn1/ASN1ObjectIdentifier;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 19
    sget-object v0, Lorg/spongycastle/asn1/x500/style/BCStyle;->C:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0, v1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    .line 20
    sget-object v0, Lorg/spongycastle/asn1/x500/style/BCStyle;->ST:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0, v1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    .line 21
    sget-object v0, Lorg/spongycastle/asn1/x500/style/BCStyle;->L:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0, v1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    .line 22
    sget-object v0, Lorg/spongycastle/asn1/x500/style/BCStyle;->STREET:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0, v1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    .line 23
    sget-object v0, Lorg/spongycastle/asn1/x500/style/BCStyle;->O:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0, v1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    .line 24
    sget-object v0, Lorg/spongycastle/asn1/x500/style/BCStyle;->OU:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0, v1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    .line 25
    sget-object v0, Lorg/spongycastle/asn1/x500/style/BCStyle;->CN:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0, v1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public getPrincipal()Lorg/spongycastle/jce/X509Principal;
    .locals 6

    .prologue
    .line 77
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 78
    .local v2, oids:Ljava/util/Vector;,"Ljava/util/Vector<Lorg/spongycastle/asn1/ASN1ObjectIdentifier;>;"
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    .line 80
    .local v3, values:Ljava/util/Vector;,"Ljava/util/Vector<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 81
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 82
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 83
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 87
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;>;"
    :cond_1
    new-instance v4, Lorg/spongycastle/jce/X509Principal;

    invoke-direct {v4, v2, v3}, Lorg/spongycastle/jce/X509Principal;-><init>(Ljava/util/Vector;Ljava/util/Vector;)V

    return-object v4
.end method

.method public bridge synthetic put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 16
    check-cast p1, Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    .end local p1
    check-cast p2, Ljava/lang/String;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "oid"
    .parameter "value"

    .prologue
    .line 29
    if-eqz p2, :cond_0

    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p2, 0x0

    .line 30
    :cond_0
    invoke-virtual {p0, p1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-super {p0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    :goto_0
    return-object p2

    .line 32
    :cond_1
    invoke-super {p0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public setCommonName(Ljava/lang/String;)V
    .locals 1
    .parameter "commonName"

    .prologue
    .line 64
    sget-object v0, Lorg/spongycastle/asn1/x500/style/BCStyle;->CN:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0, p1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    .line 65
    return-void
.end method

.method public setCountry(Ljava/lang/String;)V
    .locals 1
    .parameter "country"

    .prologue
    .line 40
    sget-object v0, Lorg/spongycastle/asn1/x500/style/BCStyle;->C:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0, p1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    .line 41
    return-void
.end method

.method public setLocality(Ljava/lang/String;)V
    .locals 1
    .parameter "locality"

    .prologue
    .line 48
    sget-object v0, Lorg/spongycastle/asn1/x500/style/BCStyle;->L:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0, p1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setOrganization(Ljava/lang/String;)V
    .locals 1
    .parameter "organization"

    .prologue
    .line 56
    sget-object v0, Lorg/spongycastle/asn1/x500/style/BCStyle;->O:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0, p1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setOrganizationalUnit(Ljava/lang/String;)V
    .locals 1
    .parameter "organizationalUnit"

    .prologue
    .line 60
    sget-object v0, Lorg/spongycastle/asn1/x500/style/BCStyle;->OU:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0, p1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 1
    .parameter "state"

    .prologue
    .line 44
    sget-object v0, Lorg/spongycastle/asn1/x500/style/BCStyle;->ST:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0, p1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setStreet(Ljava/lang/String;)V
    .locals 1
    .parameter "street"

    .prologue
    .line 52
    sget-object v0, Lorg/spongycastle/asn1/x500/style/BCStyle;->STREET:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0, p1}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;

    .line 53
    return-void
.end method

.method public size()I
    .locals 4

    .prologue
    .line 69
    const/4 v1, 0x0

    .line 70
    .local v1, result:I
    invoke-virtual {p0}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 71
    .local v2, value:Ljava/lang/String;
    if-eqz v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 73
    .end local v2           #value:Ljava/lang/String;
    :cond_1
    return v1
.end method
