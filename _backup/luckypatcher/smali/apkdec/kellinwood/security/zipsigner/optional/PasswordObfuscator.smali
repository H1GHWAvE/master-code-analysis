.class public Lkellinwood/security/zipsigner/optional/PasswordObfuscator;
.super Ljava/lang/Object;
.source "PasswordObfuscator.java"


# static fields
.field private static instance:Lkellinwood/security/zipsigner/optional/PasswordObfuscator; = null

.field static final x:Ljava/lang/String; = "harold-and-maude"


# instance fields
.field logger:Lkellinwood/logging/LoggerInterface;

.field skeySpec:Ljavax/crypto/spec/SecretKeySpec;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    sput-object v0, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->instance:Lkellinwood/security/zipsigner/optional/PasswordObfuscator;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-class v0, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkellinwood/logging/LoggerManager;->getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    iput-object v0, p0, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->logger:Lkellinwood/logging/LoggerInterface;

    .line 22
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v1, "harold-and-maude"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const-string v2, "AES"

    invoke-direct {v0, v1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    iput-object v0, p0, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->skeySpec:Ljavax/crypto/spec/SecretKeySpec;

    .line 23
    return-void
.end method

.method public static flush([B)V
    .locals 2
    .parameter "charArray"

    .prologue
    .line 122
    if-nez p0, :cond_1

    .line 126
    :cond_0
    return-void

    .line 123
    :cond_1
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 124
    const/4 v1, 0x0

    aput-byte v1, p0, v0

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static flush([C)V
    .locals 2
    .parameter "charArray"

    .prologue
    .line 115
    if-nez p0, :cond_1

    .line 119
    :cond_0
    return-void

    .line 116
    :cond_1
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 117
    const/4 v1, 0x0

    aput-char v1, p0, v0

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static getInstance()Lkellinwood/security/zipsigner/optional/PasswordObfuscator;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->instance:Lkellinwood/security/zipsigner/optional/PasswordObfuscator;

    if-nez v0, :cond_0

    new-instance v0, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;

    invoke-direct {v0}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;-><init>()V

    sput-object v0, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->instance:Lkellinwood/security/zipsigner/optional/PasswordObfuscator;

    .line 27
    :cond_0
    sget-object v0, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->instance:Lkellinwood/security/zipsigner/optional/PasswordObfuscator;

    return-object v0
.end method


# virtual methods
.method public decode(Ljava/lang/String;Ljava/lang/String;)[C
    .locals 13
    .parameter "junk"
    .parameter "password"

    .prologue
    .line 82
    if-nez p2, :cond_0

    const/4 v8, 0x0

    .line 111
    :goto_0
    return-object v8

    .line 85
    :cond_0
    :try_start_0
    const-string v11, "AES/ECB/PKCS5Padding"

    invoke-static {v11}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    .line 86
    .local v2, cipher:Ljavax/crypto/Cipher;
    new-instance v9, Ljavax/crypto/spec/SecretKeySpec;

    const-string v11, "harold-and-maude"

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    const-string v12, "AES"

    invoke-direct {v9, v11, v12}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 87
    .local v9, skeySpec:Ljavax/crypto/spec/SecretKeySpec;
    const/4 v11, 0x2

    invoke-virtual {v2, v11, v9}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 88
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-static {v11}, Lkellinwood/security/zipsigner/Base64;->decode([B)[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    .line 89
    .local v0, bytes:[B
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v11, Ljava/io/InputStreamReader;

    new-instance v12, Ljava/io/ByteArrayInputStream;

    invoke-direct {v12, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v11, v12}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v7, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 90
    .local v7, r:Ljava/io/BufferedReader;
    const/16 v11, 0x80

    new-array v1, v11, [C

    .line 91
    .local v1, cb:[C
    const/4 v5, 0x0

    .line 93
    .local v5, length:I
    :goto_1
    rsub-int v11, v5, 0x80

    invoke-virtual {v7, v1, v5, v11}, Ljava/io/BufferedReader;->read([CII)I

    move-result v6

    .local v6, numRead:I
    const/4 v11, -0x1

    if-eq v6, v11, :cond_1

    .line 94
    add-int/2addr v5, v6

    goto :goto_1

    .line 97
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v11

    if-gt v5, v11, :cond_2

    const/4 v8, 0x0

    goto :goto_0

    .line 99
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v11

    sub-int v11, v5, v11

    new-array v8, v11, [C

    .line 100
    .local v8, result:[C
    const/4 v4, 0x0

    .line 101
    .local v4, j:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .local v3, i:I
    :goto_2
    if-ge v3, v5, :cond_3

    .line 102
    aget-char v11, v1, v3

    aput-char v11, v8, v4

    .line 103
    add-int/lit8 v4, v4, 0x1

    .line 101
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 105
    :cond_3
    invoke-static {v1}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->flush([C)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 108
    .end local v0           #bytes:[B
    .end local v1           #cb:[C
    .end local v2           #cipher:Ljavax/crypto/Cipher;
    .end local v3           #i:I
    .end local v4           #j:I
    .end local v5           #length:I
    .end local v6           #numRead:I
    .end local v7           #r:Ljava/io/BufferedReader;
    .end local v8           #result:[C
    .end local v9           #skeySpec:Ljavax/crypto/spec/SecretKeySpec;
    :catch_0
    move-exception v10

    .line 109
    .local v10, x:Ljava/lang/Exception;
    iget-object v11, p0, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->logger:Lkellinwood/logging/LoggerInterface;

    const-string v12, "Failed to decode password"

    invoke-interface {v11, v12, v10}, Lkellinwood/logging/LoggerInterface;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 111
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public decodeAliasPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[C
    .locals 1
    .parameter "keystorePath"
    .parameter "aliasName"
    .parameter "password"

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->decode(Ljava/lang/String;Ljava/lang/String;)[C

    move-result-object v0

    return-object v0
.end method

.method public decodeKeystorePassword(Ljava/lang/String;Ljava/lang/String;)[C
    .locals 1
    .parameter "keystorePath"
    .parameter "password"

    .prologue
    .line 47
    invoke-virtual {p0, p1, p2}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->decode(Ljava/lang/String;Ljava/lang/String;)[C

    move-result-object v0

    return-object v0
.end method

.method public encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "junk"
    .parameter "password"

    .prologue
    .line 55
    if-nez p2, :cond_0

    const/4 v1, 0x0

    .line 59
    :goto_0
    return-object v1

    .line 56
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 57
    .local v0, c:[C
    invoke-virtual {p0, p1, v0}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->encode(Ljava/lang/String;[C)Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, result:Ljava/lang/String;
    invoke-static {v0}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->flush([C)V

    goto :goto_0
.end method

.method public encode(Ljava/lang/String;[C)Ljava/lang/String;
    .locals 8
    .parameter "junk"
    .parameter "password"

    .prologue
    const/4 v5, 0x0

    .line 63
    if-nez p2, :cond_0

    .line 78
    :goto_0
    return-object v5

    .line 66
    :cond_0
    :try_start_0
    const-string v6, "AES/ECB/PKCS5Padding"

    invoke-static {v6}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 67
    .local v1, cipher:Ljavax/crypto/Cipher;
    const/4 v6, 0x1

    iget-object v7, p0, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->skeySpec:Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v1, v6, v7}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 68
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 69
    .local v0, baos:Ljava/io/ByteArrayOutputStream;
    new-instance v3, Ljava/io/OutputStreamWriter;

    invoke-direct {v3, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 70
    .local v3, w:Ljava/io/Writer;
    invoke-virtual {v3, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 71
    invoke-virtual {v3, p2}, Ljava/io/Writer;->write([C)V

    .line 72
    invoke-virtual {v3}, Ljava/io/Writer;->flush()V

    .line 73
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v1, v6}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v2

    .line 74
    .local v2, encoded:[B
    invoke-static {v2}, Lkellinwood/security/zipsigner/Base64;->encode([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    goto :goto_0

    .line 75
    .end local v0           #baos:Ljava/io/ByteArrayOutputStream;
    .end local v1           #cipher:Ljavax/crypto/Cipher;
    .end local v2           #encoded:[B
    .end local v3           #w:Ljava/io/Writer;
    :catch_0
    move-exception v4

    .line 76
    .local v4, x:Ljava/lang/Exception;
    iget-object v6, p0, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->logger:Lkellinwood/logging/LoggerInterface;

    const-string v7, "Failed to obfuscate password"

    invoke-interface {v6, v7, v4}, Lkellinwood/logging/LoggerInterface;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public encodeAliasPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "keystorePath"
    .parameter "aliasName"
    .parameter "password"

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public encodeAliasPassword(Ljava/lang/String;Ljava/lang/String;[C)Ljava/lang/String;
    .locals 1
    .parameter "keystorePath"
    .parameter "aliasName"
    .parameter "password"

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->encode(Ljava/lang/String;[C)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public encodeKeystorePassword(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "keystorePath"
    .parameter "password"

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public encodeKeystorePassword(Ljava/lang/String;[C)Ljava/lang/String;
    .locals 1
    .parameter "keystorePath"
    .parameter "password"

    .prologue
    .line 35
    invoke-virtual {p0, p1, p2}, Lkellinwood/security/zipsigner/optional/PasswordObfuscator;->encode(Ljava/lang/String;[C)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
