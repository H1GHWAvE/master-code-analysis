.class public Lkellinwood/security/zipsigner/optional/CustomKeySigner;
.super Ljava/lang/Object;
.source "CustomKeySigner.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static signZip(Lkellinwood/security/zipsigner/ZipSigner;Ljava/lang/String;[CLjava/lang/String;[CLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .parameter "zipSigner"
    .parameter "keystorePath"
    .parameter "keystorePw"
    .parameter "certAlias"
    .parameter "certPw"
    .parameter "signatureAlgorithm"
    .parameter "inputZipFilename"
    .parameter "outputZipFilename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p0}, Lkellinwood/security/zipsigner/ZipSigner;->issueLoadingCertAndKeysProgressEvent()V

    .line 27
    invoke-static {p1, p2}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->loadKeyStore(Ljava/lang/String;[C)Ljava/security/KeyStore;

    move-result-object v10

    .line 28
    .local v10, keystore:Ljava/security/KeyStore;
    invoke-virtual {v10, p3}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v8

    .local v8, cert:Ljava/security/cert/Certificate;
    move-object v4, v8

    .line 29
    check-cast v4, Ljava/security/cert/X509Certificate;

    .line 30
    .local v4, publicKey:Ljava/security/cert/X509Certificate;
    invoke-virtual {v10, p3, p4}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object v9

    .local v9, key:Ljava/security/Key;
    move-object v5, v9

    .line 31
    check-cast v5, Ljava/security/PrivateKey;

    .line 33
    .local v5, privateKey:Ljava/security/PrivateKey;
    const-string v3, "custom"

    const/4 v7, 0x0

    move-object v2, p0

    move-object/from16 v6, p5

    invoke-virtual/range {v2 .. v7}, Lkellinwood/security/zipsigner/ZipSigner;->setKeys(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;Ljava/lang/String;[B)V

    .line 34
    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-virtual {p0, v0, v1}, Lkellinwood/security/zipsigner/ZipSigner;->signZip(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    return-void
.end method
