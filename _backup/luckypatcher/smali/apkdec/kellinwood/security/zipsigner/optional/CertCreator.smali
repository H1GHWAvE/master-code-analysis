.class public Lkellinwood/security/zipsigner/optional/CertCreator;
.super Ljava/lang/Object;
.source "CertCreator.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createKey(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILkellinwood/security/zipsigner/optional/DistinguishedNameValues;)Lkellinwood/security/zipsigner/KeySet;
    .locals 16
    .parameter "keyAlgorithm"
    .parameter "keySize"
    .parameter "keyName"
    .parameter "certSignatureAlgorithm"
    .parameter "certValidityYears"
    .parameter "distinguishedNameValues"

    .prologue
    .line 99
    :try_start_0
    invoke-static/range {p0 .. p0}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object v3

    .line 100
    .local v3, keyPairGenerator:Ljava/security/KeyPairGenerator;
    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/security/KeyPairGenerator;->initialize(I)V

    .line 101
    invoke-virtual {v3}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v1

    .line 103
    .local v1, KPair:Ljava/security/KeyPair;
    new-instance v7, Lorg/spongycastle/x509/X509V3CertificateGenerator;

    invoke-direct {v7}, Lorg/spongycastle/x509/X509V3CertificateGenerator;-><init>()V

    .line 105
    .local v7, v3CertGen:Lorg/spongycastle/x509/X509V3CertificateGenerator;
    invoke-virtual/range {p5 .. p5}, Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;->getPrincipal()Lorg/spongycastle/jce/X509Principal;

    move-result-object v5

    .line 108
    .local v5, principal:Lorg/spongycastle/jce/X509Principal;
    new-instance v9, Ljava/security/SecureRandom;

    invoke-direct {v9}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v9}, Ljava/security/SecureRandom;->nextInt()I

    move-result v9

    int-to-long v9, v9

    invoke-static {v9, v10}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v6

    .line 109
    .local v6, serialNumber:Ljava/math/BigInteger;
    :goto_0
    sget-object v9, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v6, v9}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v9

    if-gez v9, :cond_0

    .line 110
    new-instance v9, Ljava/security/SecureRandom;

    invoke-direct {v9}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v9}, Ljava/security/SecureRandom;->nextInt()I

    move-result v9

    int-to-long v9, v9

    invoke-static {v9, v10}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v6

    goto :goto_0

    .line 112
    :cond_0
    invoke-virtual {v7, v6}, Lorg/spongycastle/x509/X509V3CertificateGenerator;->setSerialNumber(Ljava/math/BigInteger;)V

    .line 113
    invoke-virtual {v7, v5}, Lorg/spongycastle/x509/X509V3CertificateGenerator;->setIssuerDN(Lorg/spongycastle/asn1/x509/X509Name;)V

    .line 114
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-wide v12, 0x9a7ec800L

    sub-long/2addr v10, v12

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v9}, Lorg/spongycastle/x509/X509V3CertificateGenerator;->setNotBefore(Ljava/util/Date;)V

    .line 115
    new-instance v9, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-wide v12, 0x75cd78800L

    move/from16 v0, p4

    int-to-long v14, v0

    mul-long/2addr v12, v14

    add-long/2addr v10, v12

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v9}, Lorg/spongycastle/x509/X509V3CertificateGenerator;->setNotAfter(Ljava/util/Date;)V

    .line 116
    invoke-virtual {v7, v5}, Lorg/spongycastle/x509/X509V3CertificateGenerator;->setSubjectDN(Lorg/spongycastle/asn1/x509/X509Name;)V

    .line 118
    invoke-virtual {v1}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v9

    invoke-virtual {v7, v9}, Lorg/spongycastle/x509/X509V3CertificateGenerator;->setPublicKey(Ljava/security/PublicKey;)V

    .line 119
    move-object/from16 v0, p3

    invoke-virtual {v7, v0}, Lorg/spongycastle/x509/X509V3CertificateGenerator;->setSignatureAlgorithm(Ljava/lang/String;)V

    .line 121
    invoke-virtual {v1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v9

    const-string v10, "BC"

    invoke-virtual {v7, v9, v10}, Lorg/spongycastle/x509/X509V3CertificateGenerator;->generate(Ljava/security/PrivateKey;Ljava/lang/String;)Ljava/security/cert/X509Certificate;

    move-result-object v2

    .line 123
    .local v2, PKCertificate:Ljava/security/cert/X509Certificate;
    new-instance v4, Lkellinwood/security/zipsigner/KeySet;

    invoke-direct {v4}, Lkellinwood/security/zipsigner/KeySet;-><init>()V

    .line 124
    .local v4, keySet:Lkellinwood/security/zipsigner/KeySet;
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lkellinwood/security/zipsigner/KeySet;->setName(Ljava/lang/String;)V

    .line 125
    invoke-virtual {v1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v9

    invoke-virtual {v4, v9}, Lkellinwood/security/zipsigner/KeySet;->setPrivateKey(Ljava/security/PrivateKey;)V

    .line 126
    invoke-virtual {v4, v2}, Lkellinwood/security/zipsigner/KeySet;->setPublicKey(Ljava/security/cert/X509Certificate;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    return-object v4

    .line 128
    .end local v1           #KPair:Ljava/security/KeyPair;
    .end local v2           #PKCertificate:Ljava/security/cert/X509Certificate;
    .end local v3           #keyPairGenerator:Ljava/security/KeyPairGenerator;
    .end local v4           #keySet:Lkellinwood/security/zipsigner/KeySet;
    .end local v5           #principal:Lorg/spongycastle/jce/X509Principal;
    .end local v6           #serialNumber:Ljava/math/BigInteger;
    .end local v7           #v3CertGen:Lorg/spongycastle/x509/X509V3CertificateGenerator;
    :catch_0
    move-exception v8

    .line 129
    .local v8, x:Ljava/lang/Exception;
    new-instance v9, Ljava/lang/RuntimeException;

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9
.end method

.method public static createKey(Ljava/lang/String;[CLjava/lang/String;ILjava/lang/String;[CLjava/lang/String;ILkellinwood/security/zipsigner/optional/DistinguishedNameValues;)Lkellinwood/security/zipsigner/KeySet;
    .locals 9
    .parameter "storePath"
    .parameter "storePass"
    .parameter "keyAlgorithm"
    .parameter "keySize"
    .parameter "keyName"
    .parameter "keyPass"
    .parameter "certSignatureAlgorithm"
    .parameter "certValidityYears"
    .parameter "distinguishedNameValues"

    .prologue
    .line 75
    move-object v0, p2

    move v1, p3

    move-object v2, p4

    move-object v3, p6

    move/from16 v4, p7

    move-object/from16 v5, p8

    :try_start_0
    invoke-static/range {v0 .. v5}, Lkellinwood/security/zipsigner/optional/CertCreator;->createKey(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILkellinwood/security/zipsigner/optional/DistinguishedNameValues;)Lkellinwood/security/zipsigner/KeySet;

    move-result-object v6

    .line 78
    .local v6, keySet:Lkellinwood/security/zipsigner/KeySet;
    invoke-static {p0, p1}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->loadKeyStore(Ljava/lang/String;[C)Ljava/security/KeyStore;

    move-result-object v7

    .line 80
    .local v7, privateKS:Ljava/security/KeyStore;
    invoke-virtual {v6}, Lkellinwood/security/zipsigner/KeySet;->getPrivateKey()Ljava/security/PrivateKey;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/security/cert/Certificate;

    const/4 v2, 0x0

    invoke-virtual {v6}, Lkellinwood/security/zipsigner/KeySet;->getPublicKey()Ljava/security/cert/X509Certificate;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v7, p4, v0, p5, v1}, Ljava/security/KeyStore;->setKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V

    .line 84
    invoke-static {v7, p0, p1}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->writeKeyStore(Ljava/security/KeyStore;Ljava/lang/String;[C)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 86
    return-object v6

    .line 88
    .end local v6           #keySet:Lkellinwood/security/zipsigner/KeySet;
    .end local v7           #privateKS:Ljava/security/KeyStore;
    :catch_0
    move-exception v8

    .line 89
    .local v8, x:Ljava/lang/RuntimeException;
    throw v8

    .line 90
    .end local v8           #x:Ljava/lang/RuntimeException;
    :catch_1
    move-exception v8

    .line 91
    .local v8, x:Ljava/lang/Exception;
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static createKeystoreAndKey(Ljava/lang/String;[CLjava/lang/String;ILjava/lang/String;[CLjava/lang/String;ILkellinwood/security/zipsigner/optional/DistinguishedNameValues;)Lkellinwood/security/zipsigner/KeySet;
    .locals 10
    .parameter "storePath"
    .parameter "storePass"
    .parameter "keyAlgorithm"
    .parameter "keySize"
    .parameter "keyName"
    .parameter "keyPass"
    .parameter "certSignatureAlgorithm"
    .parameter "certValidityYears"
    .parameter "distinguishedNameValues"

    .prologue
    .line 42
    move-object v0, p2

    move v1, p3

    move-object v2, p4

    move-object/from16 v3, p6

    move/from16 v4, p7

    move-object/from16 v5, p8

    :try_start_0
    invoke-static/range {v0 .. v5}, Lkellinwood/security/zipsigner/optional/CertCreator;->createKey(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILkellinwood/security/zipsigner/optional/DistinguishedNameValues;)Lkellinwood/security/zipsigner/KeySet;

    move-result-object v6

    .line 46
    .local v6, keySet:Lkellinwood/security/zipsigner/KeySet;
    invoke-static {p0, p1}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->createKeyStore(Ljava/lang/String;[C)Ljava/security/KeyStore;

    move-result-object v7

    .line 48
    .local v7, privateKS:Ljava/security/KeyStore;
    invoke-virtual {v6}, Lkellinwood/security/zipsigner/KeySet;->getPrivateKey()Ljava/security/PrivateKey;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/security/cert/Certificate;

    const/4 v2, 0x0

    invoke-virtual {v6}, Lkellinwood/security/zipsigner/KeySet;->getPublicKey()Ljava/security/cert/X509Certificate;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v7, p4, v0, p5, v1}, Ljava/security/KeyStore;->setKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V

    .line 52
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 53
    .local v8, sfile:Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File already exists: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 59
    .end local v6           #keySet:Lkellinwood/security/zipsigner/KeySet;
    .end local v7           #privateKS:Ljava/security/KeyStore;
    .end local v8           #sfile:Ljava/io/File;
    :catch_0
    move-exception v9

    .line 60
    .local v9, x:Ljava/lang/RuntimeException;
    throw v9

    .line 56
    .end local v9           #x:Ljava/lang/RuntimeException;
    .restart local v6       #keySet:Lkellinwood/security/zipsigner/KeySet;
    .restart local v7       #privateKS:Ljava/security/KeyStore;
    .restart local v8       #sfile:Ljava/io/File;
    :cond_0
    :try_start_1
    invoke-static {v7, p0, p1}, Lkellinwood/security/zipsigner/optional/KeyStoreFileManager;->writeKeyStore(Ljava/security/KeyStore;Ljava/lang/String;[C)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 58
    return-object v6

    .line 61
    .end local v6           #keySet:Lkellinwood/security/zipsigner/KeySet;
    .end local v7           #privateKS:Ljava/security/KeyStore;
    .end local v8           #sfile:Ljava/io/File;
    :catch_1
    move-exception v9

    .line 62
    .local v9, x:Ljava/lang/Exception;
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static createKeystoreAndKey(Ljava/lang/String;[CLjava/lang/String;Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;)V
    .locals 9
    .parameter "storePath"
    .parameter "password"
    .parameter "keyName"
    .parameter "distinguishedNameValues"

    .prologue
    .line 32
    const-string v2, "RSA"

    const/16 v3, 0x800

    const-string v6, "SHA1withRSA"

    const/16 v7, 0x1e

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p1

    move-object v8, p3

    invoke-static/range {v0 .. v8}, Lkellinwood/security/zipsigner/optional/CertCreator;->createKeystoreAndKey(Ljava/lang/String;[CLjava/lang/String;ILjava/lang/String;[CLjava/lang/String;ILkellinwood/security/zipsigner/optional/DistinguishedNameValues;)Lkellinwood/security/zipsigner/KeySet;

    .line 34
    return-void
.end method
