.class public Lkellinwood/security/zipsigner/KeySet;
.super Ljava/lang/Object;
.source "KeySet.java"


# instance fields
.field name:Ljava/lang/String;

.field privateKey:Ljava/security/PrivateKey;

.field publicKey:Ljava/security/cert/X509Certificate;

.field sigBlockTemplate:[B

.field signatureAlgorithm:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->publicKey:Ljava/security/cert/X509Certificate;

    .line 31
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->privateKey:Ljava/security/PrivateKey;

    .line 34
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->sigBlockTemplate:[B

    .line 36
    const-string v0, "SHA1withRSA"

    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->signatureAlgorithm:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;Ljava/lang/String;[B)V
    .locals 1
    .parameter "name"
    .parameter "publicKey"
    .parameter "privateKey"
    .parameter "signatureAlgorithm"
    .parameter "sigBlockTemplate"

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->publicKey:Ljava/security/cert/X509Certificate;

    .line 31
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->privateKey:Ljava/security/PrivateKey;

    .line 34
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->sigBlockTemplate:[B

    .line 36
    const-string v0, "SHA1withRSA"

    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->signatureAlgorithm:Ljava/lang/String;

    .line 51
    iput-object p1, p0, Lkellinwood/security/zipsigner/KeySet;->name:Ljava/lang/String;

    .line 52
    iput-object p2, p0, Lkellinwood/security/zipsigner/KeySet;->publicKey:Ljava/security/cert/X509Certificate;

    .line 53
    iput-object p3, p0, Lkellinwood/security/zipsigner/KeySet;->privateKey:Ljava/security/PrivateKey;

    .line 54
    if-eqz p4, :cond_0

    iput-object p4, p0, Lkellinwood/security/zipsigner/KeySet;->signatureAlgorithm:Ljava/lang/String;

    .line 55
    :cond_0
    iput-object p5, p0, Lkellinwood/security/zipsigner/KeySet;->sigBlockTemplate:[B

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V
    .locals 1
    .parameter "name"
    .parameter "publicKey"
    .parameter "privateKey"
    .parameter "sigBlockTemplate"

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->publicKey:Ljava/security/cert/X509Certificate;

    .line 31
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->privateKey:Ljava/security/PrivateKey;

    .line 34
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->sigBlockTemplate:[B

    .line 36
    const-string v0, "SHA1withRSA"

    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->signatureAlgorithm:Ljava/lang/String;

    .line 43
    iput-object p1, p0, Lkellinwood/security/zipsigner/KeySet;->name:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lkellinwood/security/zipsigner/KeySet;->publicKey:Ljava/security/cert/X509Certificate;

    .line 45
    iput-object p3, p0, Lkellinwood/security/zipsigner/KeySet;->privateKey:Ljava/security/PrivateKey;

    .line 46
    iput-object p4, p0, Lkellinwood/security/zipsigner/KeySet;->sigBlockTemplate:[B

    .line 47
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPrivateKey()Ljava/security/PrivateKey;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->privateKey:Ljava/security/PrivateKey;

    return-object v0
.end method

.method public getPublicKey()Ljava/security/cert/X509Certificate;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->publicKey:Ljava/security/cert/X509Certificate;

    return-object v0
.end method

.method public getSigBlockTemplate()[B
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->sigBlockTemplate:[B

    return-object v0
.end method

.method public getSignatureAlgorithm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->signatureAlgorithm:Ljava/lang/String;

    return-object v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .parameter "name"

    .prologue
    .line 63
    iput-object p1, p0, Lkellinwood/security/zipsigner/KeySet;->name:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setPrivateKey(Ljava/security/PrivateKey;)V
    .locals 0
    .parameter "privateKey"

    .prologue
    .line 79
    iput-object p1, p0, Lkellinwood/security/zipsigner/KeySet;->privateKey:Ljava/security/PrivateKey;

    .line 80
    return-void
.end method

.method public setPublicKey(Ljava/security/cert/X509Certificate;)V
    .locals 0
    .parameter "publicKey"

    .prologue
    .line 71
    iput-object p1, p0, Lkellinwood/security/zipsigner/KeySet;->publicKey:Ljava/security/cert/X509Certificate;

    .line 72
    return-void
.end method

.method public setSigBlockTemplate([B)V
    .locals 0
    .parameter "sigBlockTemplate"

    .prologue
    .line 87
    iput-object p1, p0, Lkellinwood/security/zipsigner/KeySet;->sigBlockTemplate:[B

    .line 88
    return-void
.end method

.method public setSignatureAlgorithm(Ljava/lang/String;)V
    .locals 0
    .parameter "signatureAlgorithm"

    .prologue
    .line 95
    if-nez p1, :cond_0

    const-string p1, "SHA1withRSA"

    .line 97
    :goto_0
    return-void

    .line 96
    :cond_0
    iput-object p1, p0, Lkellinwood/security/zipsigner/KeySet;->signatureAlgorithm:Ljava/lang/String;

    goto :goto_0
.end method
