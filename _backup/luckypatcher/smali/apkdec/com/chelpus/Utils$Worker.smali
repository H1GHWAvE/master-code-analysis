.class Lcom/chelpus/Utils$Worker;
.super Ljava/lang/Thread;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chelpus/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Worker"
.end annotation


# instance fields
.field private commands:[Ljava/lang/String;

.field private exitCode:Ljava/lang/Integer;

.field public input:Ljava/io/DataInputStream;

.field private result:Ljava/lang/String;

.field final synthetic this$0:Lcom/chelpus/Utils;


# direct methods
.method private constructor <init>(Lcom/chelpus/Utils;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1021
    iput-object p1, p0, Lcom/chelpus/Utils$Worker;->this$0:Lcom/chelpus/Utils;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1017
    const-string v0, ""

    iput-object v0, p0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    .line 1018
    iput-object v1, p0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    .line 1019
    iput-object v1, p0, Lcom/chelpus/Utils$Worker;->exitCode:Ljava/lang/Integer;

    .line 1020
    iput-object v1, p0, Lcom/chelpus/Utils$Worker;->input:Ljava/io/DataInputStream;

    .line 1023
    return-void
.end method

.method synthetic constructor <init>(Lcom/chelpus/Utils;Lcom/chelpus/Utils$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 1016
    invoke-direct {p0, p1}, Lcom/chelpus/Utils$Worker;-><init>(Lcom/chelpus/Utils;)V

    return-void
.end method

.method static synthetic access$100(Lcom/chelpus/Utils$Worker;)[Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 1016
    iget-object v0, p0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/chelpus/Utils$Worker;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 1016
    iput-object p1, p0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/chelpus/Utils$Worker;)Ljava/lang/Integer;
    .locals 1
    .parameter "x0"

    .prologue
    .line 1016
    iget-object v0, p0, Lcom/chelpus/Utils$Worker;->exitCode:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$300(Lcom/chelpus/Utils$Worker;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 1016
    iget-object v0, p0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 22

    .prologue
    .line 1027
    const/4 v5, 0x0

    .line 1028
    .local v5, dalvikfound:Z
    const/4 v7, 0x0

    .line 1029
    .local v7, except:Z
    const/4 v2, 0x0

    .line 1030
    .local v2, checkRoot:Z
    const/4 v11, 0x0

    .line 1032
    .local v11, skipOut:Z
    const/4 v10, 0x0

    .line 1033
    .local v10, send_to_dialog:Z
    const/4 v4, 0x0

    .line 1035
    .local v4, custom_patch:Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    const/4 v15, 0x0

    :goto_0
    move/from16 v0, v17

    if-ge v15, v0, :cond_9

    aget-object v3, v16, v15

    .line 1036
    .local v3, command:Ljava/lang/String;
    const-string v18, "skipOut"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    const/4 v11, 0x1

    .line 1037
    :cond_0
    const-string v18, "-Xbootclasspath:"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 1038
    const/4 v5, 0x1

    .line 1039
    :cond_1
    const-string v18, ".runpatchsupport "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_2

    const-string v18, ".runpatchsupportOld "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_2

    const-string v18, ".runpatchads "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_2

    const-string v18, ".odexrunpatch "

    .line 1040
    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_2

    const-string v18, ".custompatch "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 1041
    :cond_2
    const/4 v10, 0x1

    .line 1042
    :cond_3
    const-string v18, ".custompatch "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_4

    const/4 v4, 0x1

    .line 1035
    :cond_4
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 1083
    .end local v3           #command:Ljava/lang/String;
    .local v13, thread_number:I
    :cond_5
    :try_start_0
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->suOutputStream:Ljava/io/DataOutputStream;

    const-string v16, "echo \'chelpus done!\'\n"

    invoke-virtual/range {v15 .. v16}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 1085
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->suOutputStream:Ljava/io/DataOutputStream;

    invoke-virtual {v15}, Ljava/io/DataOutputStream;->flush()V

    .line 1091
    if-nez v11, :cond_13

    .line 1092
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->this$0:Lcom/chelpus/Utils;

    move-object/from16 v0, p0

    invoke-virtual {v15, v10, v0}, Lcom/chelpus/Utils;->getInput(ZLcom/chelpus/Utils$Worker;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1093
    if-eqz v10, :cond_6

    .line 1094
    const/4 v10, 0x0

    .line 1095
    const/4 v4, 0x0

    .line 1099
    :cond_6
    :try_start_1
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->suErrorInputStream:Ljava/io/DataInputStream;

    if-eqz v15, :cond_10

    .line 1101
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->suErrorInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v15}, Ljava/io/DataInputStream;->available()I

    move-result v15

    new-array v1, v15, [B

    .line 1102
    .local v1, arrayErrorOfByte:[B
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->suErrorInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v15, v1}, Ljava/io/DataInputStream;->read([B)I

    .line 1104
    new-instance v15, Ljava/lang/String;

    invoke-direct {v15, v1}, Ljava/lang/String;-><init>([B)V

    sput-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->errorOutput:Ljava/lang/String;

    .line 1105
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->errorOutput:Ljava/lang/String;

    const-string v16, "env: not found"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 1106
    invoke-static {}, Lcom/chelpus/Utils;->exitRoot()V

    .line 1108
    :cond_7
    new-instance v15, Ljava/lang/String;

    invoke-direct {v15, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_f

    .line 1109
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "LuckyApcther root errors: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->errorOutput:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1110
    new-instance v15, Ljava/lang/String;

    invoke-direct {v15, v1}, Ljava/lang/String;-><init>([B)V

    sput-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->errorOutput:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    .line 1161
    .end local v1           #arrayErrorOfByte:[B
    :goto_1
    if-eqz v5, :cond_18

    .line 1162
    const/4 v8, 0x0

    .local v8, i:I
    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    array-length v15, v15

    if-ge v8, v15, :cond_9

    .line 1163
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    aget-object v15, v15, v8

    const-string v16, "-Xbootclasspath:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_15

    .line 1164
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    aget-object v15, v15, v8

    const-string v16, "env LD_LIBRARY_PATH="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_8

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    aget-object v15, v15, v8

    const-string v16, ".checkDataSize "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_8

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    aget-object v15, v15, v8

    const-string v16, ".custompatch "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_14

    .line 1165
    :cond_8
    const/4 v7, 0x1

    .line 1045
    .end local v8           #i:I
    .end local v13           #thread_number:I
    :cond_9
    :goto_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    const-string v16, "SU Java-Code Running!"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_11

    if-nez v7, :cond_11

    .line 1046
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    const/16 v16, 0x0

    aget-object v15, v15, v16

    const-string v16, "env LD_LIBRARY_PATH="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_a

    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "re-run Dalvik on root with environment "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aget-object v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1049
    :cond_a
    :try_start_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    const/16 v16, 0x0

    aget-object v15, v15, v16

    const-string v16, "checkRoot"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 1050
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v16, "LuckyPatcher: test root."

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1051
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "ps init"

    aput-object v17, v15, v16

    .line 1052
    const/4 v2, 0x1

    .line 1058
    :cond_b
    sget v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->countRoot:I

    add-int/lit8 v15, v15, 0x1

    sput v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->countRoot:I

    .line 1059
    sget v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->countRoot:I

    .line 1060
    .restart local v13       #thread_number:I
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Block root thread"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sget v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->countRoot:I

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1062
    :try_start_3
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->semaphoreRoot:Ljava/util/concurrent/Semaphore;

    const-wide/16 v16, 0x12c

    sget-object v18, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v15 .. v18}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v15

    if-nez v15, :cond_c

    .line 1063
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v16, "Root command timeout. Bad root."

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1064
    invoke-static {}, Lcom/chelpus/Utils;->exitRoot()V

    .line 1065
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->semaphoreRoot:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v15}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 1070
    :cond_c
    :goto_4
    :try_start_4
    sget v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->countRoot:I

    add-int/lit8 v15, v15, -0x1

    sput v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->countRoot:I

    .line 1071
    invoke-static {}, Lcom/chelpus/Utils;->getRoot()V

    .line 1073
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "UNBlock root thread N"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1074
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    const/4 v15, 0x0

    :goto_5
    move/from16 v0, v17

    if-ge v15, v0, :cond_5

    aget-object v3, v16, v15

    .line 1075
    .restart local v3       #command:Ljava/lang/String;
    if-eqz v5, :cond_d

    .line 1076
    sget-object v18, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->suOutputStream:Ljava/io/DataOutputStream;

    new-instance v19, Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/busybox killall dalvikvm\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->getBytes()[B

    move-result-object v20

    const-string v21, "ISO-8859-1"

    invoke-direct/range {v19 .. v21}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 1078
    :cond_d
    const-string v18, "skipOut"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_e

    .line 1074
    :goto_6
    add-int/lit8 v15, v15, 0x1

    goto :goto_5

    .line 1067
    .end local v3           #command:Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 1068
    .local v6, e:Ljava/lang/InterruptedException;
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_4

    .line 1146
    .end local v6           #e:Ljava/lang/InterruptedException;
    .end local v13           #thread_number:I
    :catch_1
    move-exception v9

    .line 1147
    .local v9, localIOException:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    .line 1148
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v16, "LuckyPatcher (result root): root not found."

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1151
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->semaphoreRoot:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v15}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1205
    .end local v9           #localIOException:Ljava/io/IOException;
    :goto_7
    return-void

    .line 1081
    .restart local v3       #command:Ljava/lang/String;
    .restart local v13       #thread_number:I
    :cond_e
    :try_start_5
    sget-object v18, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->suOutputStream:Ljava/io/DataOutputStream;

    new-instance v19, Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->getBytes()[B

    move-result-object v20

    const-string v21, "ISO-8859-1"

    invoke-direct/range {v19 .. v21}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_6

    .line 1153
    .end local v3           #command:Ljava/lang/String;
    .end local v13           #thread_number:I
    :catch_2
    move-exception v6

    .line 1154
    .local v6, e:Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 1157
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->semaphoreRoot:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v15}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_7

    .line 1112
    .end local v6           #e:Ljava/lang/Exception;
    .restart local v1       #arrayErrorOfByte:[B
    .restart local v13       #thread_number:I
    :cond_f
    :try_start_6
    const-string v15, ""

    sput-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->errorOutput:Ljava/lang/String;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_1

    .line 1120
    .end local v1           #arrayErrorOfByte:[B
    :catch_3
    move-exception v6

    .line 1121
    .local v6, e:Ljava/io/IOException;
    :try_start_7
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    .line 1122
    invoke-static {}, Lcom/chelpus/Utils;->exitRoot()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_1

    .line 1117
    .end local v6           #e:Ljava/io/IOException;
    :cond_10
    const/4 v7, 0x1

    .line 1187
    .end local v13           #thread_number:I
    :cond_11
    :goto_8
    if-eqz v2, :cond_12

    .line 1188
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "LuckyPatcher (result root): "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1190
    :cond_12
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1a

    if-eqz v2, :cond_1a

    .line 1191
    if-eqz v5, :cond_19

    invoke-static {}, Lcom/chelpus/Utils;->exitRoot()V

    .line 1194
    :goto_9
    const-string v15, "lucky patcher root not found!"

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    goto :goto_7

    .line 1123
    .restart local v13       #thread_number:I
    :catch_4
    move-exception v6

    .line 1124
    .local v6, e:Ljava/lang/Exception;
    :try_start_8
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_1

    .line 1128
    .end local v6           #e:Ljava/lang/Exception;
    :cond_13
    const/4 v7, 0x1

    goto :goto_8

    .line 1168
    .restart local v8       #i:I
    :cond_14
    const-string v15, "LD_LIBRARY_PATH"

    invoke-static {v15}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1169
    .local v14, val:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    aget-object v12, v15, v8

    .line 1170
    .local v12, temp:Ljava/lang/String;
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->errorOutput:Ljava/lang/String;

    const-string v16, "env: not found"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_16

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "env LD_LIBRARY_PATH="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v15, v8

    .line 1175
    :goto_a
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    const-string v16, "SU Java-Code Running!"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_17

    .line 1177
    const/4 v7, 0x1

    .line 1162
    .end local v12           #temp:Ljava/lang/String;
    .end local v14           #val:Ljava/lang/String;
    :cond_15
    :goto_b
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    .line 1173
    .restart local v12       #temp:Ljava/lang/String;
    .restart local v14       #val:Ljava/lang/String;
    :cond_16
    const/4 v7, 0x1

    goto :goto_a

    .line 1179
    :cond_17
    const-string v15, ""

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    goto :goto_b

    .line 1184
    .end local v8           #i:I
    .end local v12           #temp:Ljava/lang/String;
    .end local v14           #val:Ljava/lang/String;
    :cond_18
    const/4 v7, 0x1

    goto/16 :goto_3

    .line 1193
    .end local v13           #thread_number:I
    :cond_19
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->semaphoreRoot:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v15}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_9

    .line 1197
    :cond_1a
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1b

    .line 1199
    const-string v15, "~"

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    .line 1202
    :cond_1b
    if-eqz v5, :cond_1c

    invoke-static {}, Lcom/chelpus/Utils;->exitRoot()V

    goto/16 :goto_7

    .line 1204
    :cond_1c
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->semaphoreRoot:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v15}, Ljava/util/concurrent/Semaphore;->release()V

    goto/16 :goto_7
.end method
