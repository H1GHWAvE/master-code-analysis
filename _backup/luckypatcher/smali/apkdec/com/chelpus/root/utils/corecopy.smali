.class public Lcom/chelpus/root/utils/corecopy;
.super Ljava/lang/Object;
.source "corecopy.java"


# static fields
.field public static toolfilesdir:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/corecopy;->toolfilesdir:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 14
    .parameter "paramArrayOfString"

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 16
    aget-object v6, p0, v13

    if-eqz v6, :cond_0

    aget-object v6, p0, v13

    sput-object v6, Lcom/chelpus/root/utils/corecopy;->toolfilesdir:Ljava/lang/String;

    .line 17
    :cond_0
    new-instance v6, Lcom/chelpus/root/utils/corecopy$1;

    invoke-direct {v6}, Lcom/chelpus/root/utils/corecopy$1;-><init>()V

    invoke-static {v6}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 18
    const-string v6, "/system"

    const-string v7, "rw"

    invoke-static {v6, v7}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 20
    aget-object v6, p0, v11

    const-string v7, "odex"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 21
    new-instance v0, Ljava/io/File;

    const-string v6, "/system/framework/core.patched"

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 22
    .local v0, corepatched:Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v6, "/system/framework/services.patched"

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 23
    .local v1, corepatched2:Ljava/io/File;
    new-instance v2, Ljava/io/File;

    aget-object v6, p0, v10

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 24
    .local v2, dalvikcache:Ljava/io/File;
    new-instance v3, Ljava/io/File;

    aget-object v6, p0, v12

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 25
    .local v3, dalvikcache2:Ljava/io/File;
    const/4 v5, 0x0

    .line 30
    .local v5, symulink:Z
    if-nez v5, :cond_3

    .line 31
    aget-object v6, p0, v10

    const-string v7, "empty"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "/system/framework/not.space"

    invoke-static {v6}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 32
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-nez v6, :cond_6

    .line 33
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 34
    new-instance v6, Ljava/io/File;

    const-string v7, "/system/framework/not.space"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 36
    :try_start_0
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/ClearDalvik.on"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    :goto_0
    new-instance v6, Ljava/io/File;

    const-string v7, "/system/framework/core.odex"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 42
    new-array v6, v12, [Ljava/lang/String;

    const-string v7, "rm"

    aput-object v7, v6, v10

    const-string v7, "/system/framework/core.odex"

    aput-object v7, v6, v11

    invoke-static {v6}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 43
    new-instance v6, Ljava/io/File;

    const-string v7, "/system/framework/core.odex"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 44
    new-instance v6, Ljava/io/File;

    const-string v7, "/system/framework/core.odex"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 47
    :cond_1
    new-instance v6, Ljava/io/File;

    const-string v7, "/system/framework/core.odex"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 48
    new-array v6, v13, [Ljava/lang/String;

    const-string v7, "chmod"

    aput-object v7, v6, v10

    const-string v7, "644"

    aput-object v7, v6, v11

    const-string v7, "/system/framework/core.odex"

    aput-object v7, v6, v12

    invoke-static {v6}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 49
    new-array v6, v13, [Ljava/lang/String;

    const-string v7, "chown"

    aput-object v7, v6, v10

    const-string v7, "0.0"

    aput-object v7, v6, v11

    const-string v7, "/system/framework/core.odex"

    aput-object v7, v6, v12

    invoke-static {v6}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 50
    new-array v6, v13, [Ljava/lang/String;

    const-string v7, "chown"

    aput-object v7, v6, v10

    const-string v7, "0:0"

    aput-object v7, v6, v11

    const-string v7, "/system/framework/core.odex"

    aput-object v7, v6, v12

    invoke-static {v6}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 51
    new-array v6, v12, [Ljava/lang/String;

    const-string v7, "rm"

    aput-object v7, v6, v10

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v11

    invoke-static {v6}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 52
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 53
    :cond_2
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "LuckyPatcher: Core odexed!"

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 125
    .end local v0           #corepatched:Ljava/io/File;
    .end local v1           #corepatched2:Ljava/io/File;
    .end local v2           #dalvikcache:Ljava/io/File;
    .end local v3           #dalvikcache2:Ljava/io/File;
    .end local v5           #symulink:Z
    :cond_3
    :goto_1
    aget-object v6, p0, v11

    const-string v7, "delete"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 126
    new-instance v0, Ljava/io/File;

    const-string v6, "/system/framework/core.patched"

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 127
    .restart local v0       #corepatched:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 129
    .end local v0           #corepatched:Ljava/io/File;
    :cond_4
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 130
    return-void

    .line 37
    .restart local v0       #corepatched:Ljava/io/File;
    .restart local v1       #corepatched2:Ljava/io/File;
    .restart local v2       #dalvikcache:Ljava/io/File;
    .restart local v3       #dalvikcache2:Ljava/io/File;
    .restart local v5       #symulink:Z
    :catch_0
    move-exception v4

    .line 39
    .local v4, e:Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 55
    .end local v4           #e:Ljava/io/IOException;
    :cond_5
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "LuckyPatcher: Core odexed failed!"

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 59
    :cond_6
    :try_start_1
    new-instance v6, Ljava/io/File;

    const-string v7, "/system/framework/not.space"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 64
    :goto_2
    new-instance v6, Ljava/io/File;

    const-string v7, "/system/framework/core.patched"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 65
    new-instance v6, Ljava/io/File;

    const-string v7, "/system/framework/patch1.done"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 66
    new-instance v6, Ljava/io/File;

    const-string v7, "/system/framework/patch1.2.done"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 67
    new-instance v6, Ljava/io/File;

    const-string v7, "/system/framework/patch2.done"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 68
    new-instance v6, Ljava/io/File;

    const-string v7, "/system/framework/patch3.done"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 69
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/ClearDalvik.on"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 70
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "LuckyPatcher: Core odexed failed 2!"

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 60
    :catch_1
    move-exception v4

    .line 62
    .restart local v4       #e:Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method
