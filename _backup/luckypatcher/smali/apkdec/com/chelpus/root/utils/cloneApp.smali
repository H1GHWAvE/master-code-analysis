.class public Lcom/chelpus/root/utils/cloneApp;
.super Ljava/lang/Object;
.source "cloneApp.java"


# static fields
.field public static appdir:Ljava/lang/String;

.field public static classesFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public static crkapk:Ljava/io/File;

.field public static dir:Ljava/lang/String;

.field public static dir2:Ljava/lang/String;

.field public static dirapp:Ljava/lang/String;

.field public static filestopatch:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public static print:Ljava/io/PrintStream;

.field public static result:Ljava/lang/String;

.field public static sddir:Ljava/lang/String;

.field public static system:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "/data/app/"

    sput-object v0, Lcom/chelpus/root/utils/cloneApp;->dirapp:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    sput-boolean v0, Lcom/chelpus/root/utils/cloneApp;->system:Z

    .line 38
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/cloneApp;->dir:Ljava/lang/String;

    .line 39
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/cloneApp;->dir2:Ljava/lang/String;

    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/chelpus/root/utils/cloneApp;->filestopatch:Ljava/util/ArrayList;

    .line 41
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/cloneApp;->sddir:Ljava/lang/String;

    .line 42
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/cloneApp;->appdir:Ljava/lang/String;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/cloneApp;->classesFiles:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearTempSD()V
    .locals 5

    .prologue
    .line 206
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/cloneApp;->sddir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Modified/classes.dex.apk"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 207
    .local v2, tmp:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 208
    .local v1, tempdex:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    .end local v1           #tempdex:Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 209
    :catch_0
    move-exception v0

    .line 211
    .local v0, e:Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 17
    .parameter "paramArrayOfString"

    .prologue
    .line 51
    new-instance v12, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;

    const-string v13, "System.out"

    invoke-direct {v12, v13}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;-><init>(Ljava/lang/String;)V

    .line 52
    .local v12, pout:Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;
    new-instance v13, Ljava/io/PrintStream;

    invoke-direct {v13, v12}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    sput-object v13, Lcom/chelpus/root/utils/cloneApp;->print:Ljava/io/PrintStream;

    .line 54
    new-instance v13, Lcom/chelpus/root/utils/cloneApp$1;

    invoke-direct {v13}, Lcom/chelpus/root/utils/cloneApp$1;-><init>()V

    invoke-static {v13}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 56
    const/4 v13, 0x0

    aget-object v13, p0, v13

    invoke-static {v13}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V

    .line 57
    sget-object v13, Lcom/chelpus/root/utils/cloneApp;->print:Ljava/io/PrintStream;

    const-string v14, "Support-Code Running!"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 58
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v10, patchesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;>;"
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    sput-object v13, Lcom/chelpus/root/utils/cloneApp;->filestopatch:Ljava/util/ArrayList;

    .line 62
    :try_start_0
    new-instance v13, Ljava/io/File;

    const/4 v14, 0x2

    aget-object v14, p0, v14

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 63
    .local v6, files:[Ljava/io/File;
    array-length v14, v6

    const/4 v13, 0x0

    :goto_0
    if-ge v13, v14, :cond_1

    aget-object v4, v6, v13

    .line 64
    .local v4, file:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "busybox"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "reboot"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "dalvikvm"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :cond_0
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 66
    .end local v4           #file:Ljava/io/File;
    .end local v6           #files:[Ljava/io/File;
    :catch_0
    move-exception v3

    .local v3, e:Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 68
    .end local v3           #e:Ljava/lang/Exception;
    :cond_1
    const/4 v13, 0x6

    :try_start_1
    aget-object v13, p0, v13

    if-eqz v13, :cond_2

    const/4 v13, 0x6

    aget-object v13, p0, v13

    invoke-static {v13}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 72
    :cond_2
    :goto_1
    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    sput-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    .line 76
    const/4 v13, 0x0

    :try_start_2
    aget-object v9, p0, v13

    .line 77
    .local v9, packageName:Ljava/lang/String;
    const/4 v13, 0x1

    aget-object v13, p0, v13

    sput-object v13, Lcom/chelpus/root/utils/cloneApp;->appdir:Ljava/lang/String;

    .line 78
    const/4 v13, 0x3

    aget-object v13, p0, v13

    sput-object v13, Lcom/chelpus/root/utils/cloneApp;->sddir:Ljava/lang/String;

    .line 80
    invoke-static {}, Lcom/chelpus/root/utils/cloneApp;->clearTempSD()V

    .line 81
    new-instance v1, Ljava/io/File;

    sget-object v13, Lcom/chelpus/root/utils/cloneApp;->appdir:Ljava/lang/String;

    invoke-direct {v1, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 82
    .local v1, apk:Ljava/io/File;
    invoke-static {v1}, Lcom/chelpus/root/utils/cloneApp;->unzipSD(Ljava/io/File;)V

    .line 83
    new-instance v13, Ljava/io/File;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/chelpus/root/utils/cloneApp;->sddir:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/Modified/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ".apk"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v13, Lcom/chelpus/root/utils/cloneApp;->crkapk:Ljava/io/File;

    .line 84
    sget-object v13, Lcom/chelpus/root/utils/cloneApp;->crkapk:Ljava/io/File;

    invoke-static {v1, v13}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 85
    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    .line 86
    .local v11, pn:[B
    const/4 v7, 0x0

    .local v7, h:I
    :goto_2
    array-length v13, v11

    if-ge v7, v13, :cond_4

    .line 87
    array-length v13, v11

    add-int/lit8 v13, v13, -0x1

    if-ne v7, v13, :cond_3

    aget-byte v13, v11, v7

    add-int/lit8 v13, v13, 0x1

    int-to-byte v13, v13

    aput-byte v13, v11, v7

    .line 86
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 89
    :cond_4
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v11}, Ljava/lang/String;-><init>([B)V

    .line 90
    .local v8, newPackageName:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v14, Lcom/chelpus/root/utils/cloneApp;->sddir:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/Modified/AndroidManifest.xml"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v0, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 91
    .local v0, androidManifest:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_6

    new-instance v13, Ljava/io/FileNotFoundException;

    invoke-direct {v13}, Ljava/io/FileNotFoundException;-><init>()V

    throw v13
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 123
    .end local v0           #androidManifest:Ljava/io/File;
    .end local v1           #apk:Ljava/io/File;
    .end local v7           #h:I
    .end local v8           #newPackageName:Ljava/lang/String;
    .end local v9           #packageName:Ljava/lang/String;
    .end local v11           #pn:[B
    :catch_1
    move-exception v3

    .restart local v3       #e:Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 125
    .end local v3           #e:Ljava/lang/Exception;
    :cond_5
    const-string v13, "Optional Steps After Patch:"

    invoke-static {v13}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 127
    iget-object v13, v12, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->allresult:Ljava/lang/String;

    sput-object v13, Lcom/chelpus/root/utils/cloneApp;->result:Ljava/lang/String;

    .line 128
    return-void

    .line 94
    .restart local v0       #androidManifest:Ljava/io/File;
    .restart local v1       #apk:Ljava/io/File;
    .restart local v7       #h:I
    .restart local v8       #newPackageName:Ljava/lang/String;
    .restart local v9       #packageName:Ljava/lang/String;
    .restart local v11       #pn:[B
    :cond_6
    :try_start_3
    new-instance v13, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;

    invoke-direct {v13}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;-><init>()V

    invoke-virtual {v13, v0, v9, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->changePackageName(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_7

    .line 95
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 101
    :cond_7
    :goto_3
    :try_start_4
    sget-object v13, Lcom/chelpus/root/utils/cloneApp;->classesFiles:Ljava/util/ArrayList;

    if-eqz v13, :cond_8

    sget-object v13, Lcom/chelpus/root/utils/cloneApp;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-nez v13, :cond_9

    .line 102
    :cond_8
    new-instance v13, Ljava/io/FileNotFoundException;

    invoke-direct {v13}, Ljava/io/FileNotFoundException;-><init>()V

    throw v13

    .line 97
    :catch_2
    move-exception v3

    .line 98
    .restart local v3       #e:Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 104
    .end local v3           #e:Ljava/lang/Exception;
    :cond_9
    sget-object v13, Lcom/chelpus/root/utils/cloneApp;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    .line 105
    sget-object v13, Lcom/chelpus/root/utils/cloneApp;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_b

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 106
    .local v2, cl:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_a

    new-instance v13, Ljava/io/FileNotFoundException;

    invoke-direct {v13}, Ljava/io/FileNotFoundException;-><init>()V

    throw v13

    .line 107
    :cond_a
    sget-object v14, Lcom/chelpus/root/utils/cloneApp;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 111
    .end local v2           #cl:Ljava/io/File;
    :cond_b
    sget-object v13, Lcom/chelpus/root/utils/cloneApp;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_5
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    .line 112
    .local v5, filepatch:Ljava/io/File;
    const-string v14, "Find string id."

    invoke-static {v14}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 114
    const-string v14, "String analysis."

    invoke-static {v14}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 115
    sget-object v14, Lcom/chelpus/root/utils/cloneApp;->print:Ljava/io/PrintStream;

    const-string v15, "String analysis."

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 119
    const-string v14, "Analise Results:"

    invoke-static {v14}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 121
    invoke-static {v5}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_5

    .line 70
    .end local v0           #androidManifest:Ljava/io/File;
    .end local v1           #apk:Ljava/io/File;
    .end local v5           #filepatch:Ljava/io/File;
    .end local v7           #h:I
    .end local v8           #newPackageName:Ljava/lang/String;
    .end local v9           #packageName:Ljava/lang/String;
    .end local v11           #pn:[B
    :catch_3
    move-exception v13

    goto/16 :goto_1

    .line 69
    :catch_4
    move-exception v13

    goto/16 :goto_1
.end method

.method public static unzipSD(Ljava/io/File;)V
    .locals 15
    .parameter "apk"

    .prologue
    const/4 v14, -0x1

    .line 133
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 134
    .local v4, fin:Ljava/io/FileInputStream;
    new-instance v8, Ljava/util/zip/ZipInputStream;

    invoke-direct {v8, v4}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 135
    .local v8, zin:Ljava/util/zip/ZipInputStream;
    const/4 v7, 0x0

    .line 136
    .local v7, ze:Ljava/util/zip/ZipEntry;
    const/4 v1, 0x0

    .line 137
    .local v1, classesdex:Z
    :cond_0
    :goto_0
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 142
    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "classes"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, ".dex"

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 143
    new-instance v5, Ljava/io/FileOutputStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/chelpus/root/utils/cloneApp;->sddir:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Modified/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 145
    .local v5, fout:Ljava/io/FileOutputStream;
    const/16 v10, 0x400

    new-array v0, v10, [B

    .line 147
    .local v0, buffer:[B
    :goto_1
    invoke-virtual {v8, v0}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v6

    .local v6, length:I
    if-eq v6, v14, :cond_1

    .line 148
    const/4 v10, 0x0

    invoke-virtual {v5, v0, v10, v6}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 173
    .end local v0           #buffer:[B
    .end local v1           #classesdex:Z
    .end local v4           #fin:Ljava/io/FileInputStream;
    .end local v5           #fout:Ljava/io/FileOutputStream;
    .end local v6           #length:I
    .end local v7           #ze:Ljava/util/zip/ZipEntry;
    .end local v8           #zin:Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v2

    .line 175
    .local v2, e:Ljava/lang/Exception;
    :try_start_1
    new-instance v9, Lnet/lingala/zip4j/core/ZipFile;

    invoke-direct {v9, p0}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 179
    .local v9, zipFile:Lnet/lingala/zip4j/core/ZipFile;
    const-string v10, "classes.dex"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/cloneApp;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    sget-object v10, Lcom/chelpus/root/utils/cloneApp;->classesFiles:Ljava/util/ArrayList;

    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/cloneApp;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/Modified/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "classes.dex"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    const-string v10, "AndroidManifest.xml"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/cloneApp;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 190
    .end local v9           #zipFile:Lnet/lingala/zip4j/core/ZipFile;
    :goto_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Decompressunzip "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 194
    .end local v2           #e:Ljava/lang/Exception;
    :goto_3
    return-void

    .line 150
    .restart local v0       #buffer:[B
    .restart local v1       #classesdex:Z
    .restart local v4       #fin:Ljava/io/FileInputStream;
    .restart local v5       #fout:Ljava/io/FileOutputStream;
    .restart local v6       #length:I
    .restart local v7       #ze:Ljava/util/zip/ZipEntry;
    .restart local v8       #zin:Ljava/util/zip/ZipInputStream;
    :cond_1
    :try_start_2
    sget-object v10, Lcom/chelpus/root/utils/cloneApp;->classesFiles:Ljava/util/ArrayList;

    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/cloneApp;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/Modified/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    const/4 v1, 0x1

    .line 154
    .end local v0           #buffer:[B
    .end local v5           #fout:Ljava/io/FileOutputStream;
    .end local v6           #length:I
    :cond_2
    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "AndroidManifest.xml"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 155
    new-instance v5, Ljava/io/FileOutputStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/chelpus/root/utils/cloneApp;->sddir:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Modified/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "AndroidManifest.xml"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 157
    .restart local v5       #fout:Ljava/io/FileOutputStream;
    const/16 v10, 0x400

    new-array v0, v10, [B

    .line 159
    .restart local v0       #buffer:[B
    :goto_4
    invoke-virtual {v8, v0}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v6

    .restart local v6       #length:I
    if-eq v6, v14, :cond_3

    .line 160
    const/4 v10, 0x0

    invoke-virtual {v5, v0, v10, v6}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_4

    .line 163
    :cond_3
    if-eqz v1, :cond_0

    .line 164
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 165
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_0

    .line 171
    .end local v0           #buffer:[B
    .end local v5           #fout:Ljava/io/FileOutputStream;
    .end local v6           #length:I
    :cond_4
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->close()V

    .line 172
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    .line 183
    .end local v1           #classesdex:Z
    .end local v4           #fin:Ljava/io/FileInputStream;
    .end local v7           #ze:Ljava/util/zip/ZipEntry;
    .end local v8           #zin:Ljava/util/zip/ZipInputStream;
    .restart local v2       #e:Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 184
    .local v3, e1:Lnet/lingala/zip4j/exception/ZipException;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error classes.dex decompress! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 185
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception e1"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_2

    .line 186
    .end local v3           #e1:Lnet/lingala/zip4j/exception/ZipException;
    :catch_2
    move-exception v3

    .line 187
    .local v3, e1:Ljava/lang/Exception;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error classes.dex decompress! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 188
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception e1"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_2
.end method
