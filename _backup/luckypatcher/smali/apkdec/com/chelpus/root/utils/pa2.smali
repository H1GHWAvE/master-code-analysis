.class public Lcom/chelpus/root/utils/pa2;
.super Ljava/lang/Object;
.source "pa2.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 22
    .parameter "paramArrayOfString"

    .prologue
    .line 15
    const/16 v2, 0x9

    new-array v7, v2, [B

    fill-array-data v7, :array_0

    .line 16
    .local v7, byteOrig2:[B
    const/16 v2, 0x9

    new-array v0, v2, [B

    move-object/from16 v18, v0

    fill-array-data v18, :array_1

    .line 17
    .local v18, mask2:[B
    const/16 v2, 0x9

    new-array v8, v2, [B

    fill-array-data v8, :array_2

    .line 18
    .local v8, byteReplace2:[B
    const/16 v2, 0x9

    new-array v0, v2, [B

    move-object/from16 v20, v0

    fill-array-data v20, :array_3

    .line 21
    .local v20, rep_mask2:[B
    :try_start_0
    new-instance v2, Lcom/chelpus/root/utils/pa2$1;

    invoke-direct {v2}, Lcom/chelpus/root/utils/pa2$1;-><init>()V

    invoke-static {v2}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 22
    new-instance v17, Ljava/io/File;

    const-string v2, "/data/data/com.maxmpz.audioplayer/databases/folders.db"

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 23
    .local v17, localFile2:Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-direct {v2}, Ljava/io/FileNotFoundException;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    .end local v17           #localFile2:Ljava/io/File;
    :catch_0
    move-exception v12

    .local v12, e:Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    .line 70
    .end local v12           #e:Ljava/lang/Exception;
    :goto_0
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 71
    return-void

    .line 25
    .restart local v17       #localFile2:Ljava/io/File;
    :cond_0
    :try_start_1
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v3, "rw"

    move-object/from16 v0, v17

    invoke-direct {v2, v0, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 26
    .local v1, ChannelDex:Ljava/nio/channels/FileChannel;
    sget-object v2, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v3, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v5

    long-to-int v5, v5

    int-to-long v5, v5

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v13

    .line 28
    .local v13, fileBytes:Ljava/nio/MappedByteBuffer;
    const/4 v11, 0x0

    .line 30
    .local v11, curentPos2:I
    const/16 v21, 0x0

    .line 32
    .local v21, repbyte:B
    const-wide/16 v15, 0x0

    .local v15, j:J
    :goto_1
    :try_start_2
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 34
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v10

    .line 35
    .local v10, curentPos:I
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v9

    .line 38
    .local v9, curentByte:B
    const/4 v2, 0x0

    aget-byte v2, v7, v2

    if-ne v9, v2, :cond_5

    .line 39
    const/4 v2, 0x0

    aget-byte v2, v20, v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    aput-byte v9, v8, v2

    .line 40
    :cond_1
    const/4 v14, 0x1

    .line 41
    .local v14, i:I
    add-int/lit8 v2, v10, 0x1

    invoke-virtual {v13, v2}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 42
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v19

    .line 44
    .local v19, prufbyte:B
    :goto_2
    aget-byte v2, v7, v14

    move/from16 v0, v19

    if-eq v0, v2, :cond_2

    aget-byte v2, v18, v14

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 45
    :cond_2
    aget-byte v2, v20, v14

    if-nez v2, :cond_3

    aput-byte v19, v8, v14

    .line 46
    :cond_3
    add-int/lit8 v14, v14, 0x1

    .line 48
    array-length v2, v7

    if-ne v14, v2, :cond_6

    .line 49
    invoke-virtual {v13, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 50
    invoke-virtual {v13, v8}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 51
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 61
    :cond_4
    add-int/lit8 v2, v10, 0x1

    invoke-virtual {v13, v2}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 63
    .end local v14           #i:I
    .end local v19           #prufbyte:B
    :cond_5
    add-int/lit8 v2, v10, 0x1

    invoke-virtual {v13, v2}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 32
    const-wide/16 v2, 0x1

    add-long/2addr v15, v2

    goto :goto_1

    .line 58
    .restart local v14       #i:I
    .restart local v19       #prufbyte:B
    :cond_6
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->get()B
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v19

    goto :goto_2

    .line 65
    .end local v9           #curentByte:B
    .end local v10           #curentPos:I
    .end local v14           #i:I
    .end local v19           #prufbyte:B
    :catch_1
    move-exception v12

    .line 66
    .restart local v12       #e:Ljava/lang/Exception;
    :try_start_3
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 68
    .end local v12           #e:Ljava/lang/Exception;
    :cond_7
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 15
    :array_0
    .array-data 0x1
        0x70t
        0x61t
        0x74t
        0x68t
        0x5ft
        0x68t
        0x61t
        0x73t
        0x68t
    .end array-data

    .line 16
    nop

    :array_1
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 17
    nop

    :array_2
    .array-data 0x1
        0x70t
        0x61t
        0x6ct
        0x68t
        0x5ft
        0x68t
        0x61t
        0x73t
        0x68t
    .end array-data

    .line 18
    nop

    :array_3
    .array-data 0x1
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method
