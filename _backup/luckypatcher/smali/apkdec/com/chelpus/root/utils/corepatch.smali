.class public Lcom/chelpus/root/utils/corepatch;
.super Ljava/lang/Object;
.source "corepatch.java"


# static fields
.field public static final MAGIC:[B

.field public static adler:I

.field public static fileBytes:Ljava/nio/MappedByteBuffer;

.field public static lastByteReplace:[B

.field public static lastPatchPosition:I

.field public static not_found_bytes_for_patch:Z

.field public static onlyDalvik:Z

.field public static toolfilesdir:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 25
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/chelpus/root/utils/corepatch;->MAGIC:[B

    .line 28
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/corepatch;->toolfilesdir:Ljava/lang/String;

    .line 29
    sput-boolean v1, Lcom/chelpus/root/utils/corepatch;->onlyDalvik:Z

    .line 30
    sput-boolean v1, Lcom/chelpus/root/utils/corepatch;->not_found_bytes_for_patch:Z

    .line 31
    sput-object v2, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    .line 32
    sput v1, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I

    .line 33
    sput-object v2, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    return-void

    .line 25
    :array_0
    .array-data 0x1
        0x64t
        0x65t
        0x79t
        0xat
        0x30t
        0x33t
        0x35t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static applyPatch(IB[B[B[B[BZ)Z
    .locals 6
    .parameter "curentPos"
    .parameter "currentByte"
    .parameter "byteOrig"
    .parameter "mask"
    .parameter "byteReplace"
    .parameter "rep_mask"
    .parameter "pattern"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2650
    if-eqz p2, :cond_7

    aget-byte v4, p2, v3

    if-ne p1, v4, :cond_7

    if-eqz p6, :cond_7

    .line 2652
    aget-byte v4, p5, v3

    if-nez v4, :cond_0

    aput-byte p1, p4, v3

    .line 2653
    :cond_0
    const/4 v0, 0x1

    .line 2654
    .local v0, i:I
    sget-object v4, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v5, p0, 0x1

    invoke-virtual {v4, v5}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2655
    sget-object v4, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v4}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    .line 2657
    .local v1, prufbyte:B
    :goto_0
    aget-byte v4, p2, v0

    if-eq v1, v4, :cond_1

    aget-byte v4, p3, v0

    if-ne v4, v2, :cond_6

    .line 2659
    :cond_1
    aget-byte v4, p5, v0

    if-nez v4, :cond_2

    aput-byte v1, p4, v0

    .line 2660
    :cond_2
    aget-byte v4, p5, v0

    const/4 v5, 0x3

    if-ne v4, v5, :cond_3

    .line 2661
    and-int/lit8 v4, v1, 0xf

    int-to-byte v4, v4

    aput-byte v4, p4, v0

    .line 2664
    :cond_3
    aget-byte v4, p5, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    .line 2665
    and-int/lit8 v4, v1, 0xf

    and-int/lit8 v5, v1, 0xf

    mul-int/lit8 v5, v5, 0x10

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, p4, v0

    .line 2668
    :cond_4
    add-int/lit8 v0, v0, 0x1

    .line 2670
    array-length v4, p2

    if-ne v0, v4, :cond_5

    .line 2672
    sget-object v3, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v3, p0}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2673
    sget-object v3, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v3, p4}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 2674
    sget-object v3, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v3}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 2682
    .end local v0           #i:I
    .end local v1           #prufbyte:B
    :goto_1
    return v2

    .line 2678
    .restart local v0       #i:I
    .restart local v1       #prufbyte:B
    :cond_5
    sget-object v4, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v4}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    goto :goto_0

    .line 2680
    :cond_6
    sget-object v2, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v4, p0, 0x1

    invoke-virtual {v2, v4}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .end local v0           #i:I
    .end local v1           #prufbyte:B
    :cond_7
    move v2, v3

    .line 2682
    goto :goto_1
.end method

.method private static applyPatchCounter(IB[B[B[B[BIIZ)Z
    .locals 6
    .parameter "curentPos"
    .parameter "currentByte"
    .parameter "byteOrig"
    .parameter "mask"
    .parameter "byteReplace"
    .parameter "rep_mask"
    .parameter "counter"
    .parameter "limitCounter"
    .parameter "pattern"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2685
    if-eqz p2, :cond_8

    aget-byte v4, p2, v3

    if-ne p1, v4, :cond_8

    if-eqz p8, :cond_8

    .line 2687
    aget-byte v4, p5, v3

    if-nez v4, :cond_0

    aput-byte p1, p4, v3

    .line 2688
    :cond_0
    const/4 v0, 0x1

    .line 2689
    .local v0, i:I
    sget-object v4, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v5, p0, 0x1

    invoke-virtual {v4, v5}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2690
    sget-object v4, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v4}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    .line 2692
    .local v1, prufbyte:B
    :goto_0
    aget-byte v4, p2, v0

    if-eq v1, v4, :cond_1

    aget-byte v4, p3, v0

    if-ne v4, v2, :cond_7

    .line 2694
    :cond_1
    aget-byte v4, p5, v0

    if-nez v4, :cond_2

    aput-byte v1, p4, v0

    .line 2695
    :cond_2
    aget-byte v4, p5, v0

    const/4 v5, 0x3

    if-ne v4, v5, :cond_3

    .line 2696
    and-int/lit8 v4, v1, 0xf

    int-to-byte v4, v4

    aput-byte v4, p4, v0

    .line 2699
    :cond_3
    aget-byte v4, p5, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    .line 2700
    and-int/lit8 v4, v1, 0xf

    and-int/lit8 v5, v1, 0xf

    mul-int/lit8 v5, v5, 0x10

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, p4, v0

    .line 2703
    :cond_4
    add-int/lit8 v0, v0, 0x1

    .line 2705
    array-length v4, p2

    if-ne v0, v4, :cond_6

    .line 2707
    if-lt p6, p7, :cond_5

    .line 2708
    sput p0, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I

    .line 2709
    array-length v4, p4

    new-array v4, v4, [B

    sput-object v4, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    .line 2710
    sget-object v4, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    array-length v5, p4

    invoke-static {p4, v3, v4, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2720
    .end local v0           #i:I
    .end local v1           #prufbyte:B
    :cond_5
    :goto_1
    return v2

    .line 2716
    .restart local v0       #i:I
    .restart local v1       #prufbyte:B
    :cond_6
    sget-object v4, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v4}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    goto :goto_0

    .line 2718
    :cond_7
    sget-object v2, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v4, p0, 0x1

    invoke-virtual {v2, v4}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .end local v0           #i:I
    .end local v1           #prufbyte:B
    :cond_8
    move v2, v3

    .line 2720
    goto :goto_1
.end method

.method public static main([Ljava/lang/String;)V
    .locals 332
    .parameter "paramArrayOfString"

    .prologue
    .line 40
    new-instance v9, Lcom/chelpus/root/utils/corepatch$1;

    invoke-direct {v9}, Lcom/chelpus/root/utils/corepatch$1;-><init>()V

    invoke-static {v9}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 41
    const/16 v154, 0x0

    .local v154, pattern1:Z
    const/16 v63, 0x0

    .local v63, pattern2:Z
    const/16 v75, 0x0

    .local v75, pattern3:Z
    const/16 v87, 0x0

    .line 43
    .local v87, pattern4:Z
    const/16 v182, 0x0

    .line 44
    .local v182, byteOrig2:[B
    const/4 v0, 0x0

    move-object/16 v264, v0

    .line 45
    .local v264, mask2:[B
    const/16 v212, 0x0

    .line 46
    .local v212, byteReplace2:[B
    const/4 v0, 0x0

    move-object/16 v310, v0

    .line 48
    .local v310, rep_mask2:[B
    const/4 v5, 0x0

    .line 49
    .local v5, byteOrigOatUpd1:[B
    const/4 v6, 0x0

    .line 50
    .local v6, maskOatUpd1:[B
    const/4 v7, 0x0

    .line 51
    .local v7, byteReplaceOatUpd1:[B
    const/4 v8, 0x0

    .line 53
    .local v8, rep_maskOatUpd1:[B
    const/4 v11, 0x0

    .line 54
    .local v11, byteOrigOat1:[B
    const/4 v12, 0x0

    .line 55
    .local v12, maskOat1:[B
    const/4 v13, 0x0

    .line 56
    .local v13, byteReplaceOat1:[B
    const/4 v14, 0x0

    .line 58
    .local v14, rep_maskOat1:[B
    const/16 v17, 0x0

    .line 59
    .local v17, byteOrigOat2:[B
    const/16 v18, 0x0

    .line 60
    .local v18, maskOat2:[B
    const/16 v19, 0x0

    .line 61
    .local v19, byteReplaceOat2:[B
    const/16 v20, 0x0

    .line 63
    .local v20, rep_maskOat2:[B
    const/16 v23, 0x0

    .line 64
    .local v23, byteOrigOat3:[B
    const/16 v24, 0x0

    .line 65
    .local v24, maskOat3:[B
    const/16 v25, 0x0

    .line 66
    .local v25, byteReplaceOat3:[B
    const/16 v26, 0x0

    .line 68
    .local v26, rep_maskOat3:[B
    const/16 v29, 0x0

    .line 69
    .local v29, byteOrigOatUpd4:[B
    const/16 v30, 0x0

    .line 70
    .local v30, maskOatUpd4:[B
    const/16 v31, 0x0

    .line 71
    .local v31, byteReplaceOatUpd4:[B
    const/16 v32, 0x0

    .line 73
    .local v32, rep_maskOatUpd4:[B
    const/16 v53, 0x0

    .line 74
    .local v53, byteOrigOatUpd5:[B
    const/16 v54, 0x0

    .line 75
    .local v54, maskOatUpd5:[B
    const/16 v55, 0x0

    .line 76
    .local v55, byteReplaceOatUpd5:[B
    const/16 v56, 0x0

    .line 78
    .local v56, rep_maskOatUpd5:[B
    const/16 v35, 0x0

    .line 79
    .local v35, byteOrigOat4:[B
    const/16 v36, 0x0

    .line 80
    .local v36, maskOat4:[B
    const/16 v37, 0x0

    .line 81
    .local v37, byteReplaceOat4:[B
    const/16 v38, 0x0

    .line 83
    .local v38, rep_maskOat4:[B
    const/16 v41, 0x0

    .line 84
    .local v41, byteOrigOat5:[B
    const/16 v42, 0x0

    .line 85
    .local v42, maskOat5:[B
    const/16 v43, 0x0

    .line 86
    .local v43, byteReplaceOat5:[B
    const/16 v44, 0x0

    .line 88
    .local v44, rep_maskOat5:[B
    const/16 v47, 0x0

    .line 89
    .local v47, byteOrigOat6:[B
    const/16 v48, 0x0

    .line 90
    .local v48, maskOat6:[B
    const/16 v49, 0x0

    .line 91
    .local v49, byteReplaceOat6:[B
    const/16 v50, 0x0

    .line 93
    .local v50, rep_maskOat6:[B
    const/16 v59, 0x0

    .line 94
    .local v59, byteOrigOat7:[B
    const/16 v60, 0x0

    .line 95
    .local v60, maskOat7:[B
    const/16 v61, 0x0

    .line 96
    .local v61, byteReplaceOat7:[B
    const/16 v62, 0x0

    .line 99
    .local v62, rep_maskOat7:[B
    const/16 v65, 0x0

    .line 100
    .local v65, byteOrigSOat1:[B
    const/16 v66, 0x0

    .line 101
    .local v66, maskSOat1:[B
    const/16 v67, 0x0

    .line 102
    .local v67, byteReplaceSOat1:[B
    const/16 v68, 0x0

    .line 104
    .local v68, rep_maskSOat1:[B
    const/16 v71, 0x0

    .line 105
    .local v71, byteOrigSOat2:[B
    const/16 v72, 0x0

    .line 106
    .local v72, maskSOat2:[B
    const/16 v73, 0x0

    .line 107
    .local v73, byteReplaceSOat2:[B
    const/16 v74, 0x0

    .line 109
    .local v74, rep_maskSOat2:[B
    const/16 v77, 0x0

    .line 110
    .local v77, byteOrigSOat3:[B
    const/16 v78, 0x0

    .line 111
    .local v78, maskSOat3:[B
    const/16 v79, 0x0

    .line 112
    .local v79, byteReplaceSOat3:[B
    const/16 v80, 0x0

    .line 114
    .local v80, rep_maskSOat3:[B
    const/16 v83, 0x0

    .line 115
    .local v83, byteOrigSOat4:[B
    const/16 v84, 0x0

    .line 116
    .local v84, maskSOat4:[B
    const/16 v85, 0x0

    .line 117
    .local v85, byteReplaceSOat4:[B
    const/16 v86, 0x0

    .line 119
    .local v86, rep_maskSOat4:[B
    const/16 v89, 0x0

    .line 120
    .local v89, byteOrigSOat5:[B
    const/16 v90, 0x0

    .line 121
    .local v90, maskSOat5:[B
    const/16 v91, 0x0

    .line 122
    .local v91, byteReplaceSOat5:[B
    const/16 v92, 0x0

    .line 124
    .local v92, rep_maskSOat5:[B
    const/16 v95, 0x0

    .line 125
    .local v95, byteOrigSOat6:[B
    const/16 v96, 0x0

    .line 126
    .local v96, maskSOat6:[B
    const/16 v97, 0x0

    .line 127
    .local v97, byteReplaceSOat6:[B
    const/16 v98, 0x0

    .line 129
    .local v98, rep_maskSOat6:[B
    const/16 v101, 0x0

    .line 130
    .local v101, byteOrigSOat7:[B
    const/16 v102, 0x0

    .line 131
    .local v102, maskSOat7:[B
    const/16 v103, 0x0

    .line 132
    .local v103, byteReplaceSOat7:[B
    const/16 v104, 0x0

    .line 134
    .local v104, rep_maskSOat7:[B
    const/16 v107, 0x0

    .line 135
    .local v107, byteOrigSOat8:[B
    const/16 v108, 0x0

    .line 136
    .local v108, maskSOat8:[B
    const/16 v109, 0x0

    .line 137
    .local v109, byteReplaceSOat8:[B
    const/16 v110, 0x0

    .line 139
    .local v110, rep_maskSOat8:[B
    const/16 v113, 0x0

    .line 140
    .local v113, byteOrigSOat9:[B
    const/16 v114, 0x0

    .line 141
    .local v114, maskSOat9:[B
    const/16 v115, 0x0

    .line 142
    .local v115, byteReplaceSOat9:[B
    const/16 v116, 0x0

    .line 144
    .local v116, rep_maskSOat9:[B
    const/16 v119, 0x0

    .line 145
    .local v119, byteOrigSOat10:[B
    const/16 v120, 0x0

    .line 146
    .local v120, maskSOat10:[B
    const/16 v121, 0x0

    .line 147
    .local v121, byteReplaceSOat10:[B
    const/16 v122, 0x0

    .line 149
    .local v122, rep_maskSOat10:[B
    const/16 v125, 0x0

    .line 150
    .local v125, byteOrigSOat11:[B
    const/16 v126, 0x0

    .line 151
    .local v126, maskSOat11:[B
    const/16 v127, 0x0

    .line 152
    .local v127, byteReplaceSOat11:[B
    const/16 v128, 0x0

    .line 154
    .local v128, rep_maskSOat11:[B
    const/16 v131, 0x0

    .line 155
    .local v131, byteOrigSOat12:[B
    const/16 v132, 0x0

    .line 156
    .local v132, maskSOat12:[B
    const/16 v133, 0x0

    .line 157
    .local v133, byteReplaceSOat12:[B
    const/16 v134, 0x0

    .line 159
    .local v134, rep_maskSOat12:[B
    const/16 v137, 0x0

    .line 160
    .local v137, byteOrigSOat13:[B
    const/16 v138, 0x0

    .line 161
    .local v138, maskSOat13:[B
    const/16 v139, 0x0

    .line 162
    .local v139, byteReplaceSOat13:[B
    const/16 v140, 0x0

    .line 164
    .local v140, rep_maskSOat13:[B
    const/16 v195, 0x0

    .line 165
    .local v195, byteOrigS2:[B
    const/4 v0, 0x0

    move-object/16 v276, v0

    .line 166
    .local v276, maskS2:[B
    const/16 v225, 0x0

    .line 167
    .local v225, byteReplaceS2:[B
    const/4 v0, 0x0

    move-object/16 v322, v0

    .line 168
    .local v322, rep_maskS2:[B
    const/16 v196, 0x0

    .line 169
    .local v196, byteOrigS3:[B
    const/4 v0, 0x0

    move-object/16 v277, v0

    .line 170
    .local v277, maskS3:[B
    const/16 v226, 0x0

    .line 171
    .local v226, byteReplaceS3:[B
    const/4 v0, 0x0

    move-object/16 v323, v0

    .line 172
    .local v323, rep_maskS3:[B
    const/16 v197, 0x0

    .line 173
    .local v197, byteOrigS4:[B
    const/4 v0, 0x0

    move-object/16 v278, v0

    .line 174
    .local v278, maskS4:[B
    const/16 v227, 0x0

    .line 175
    .local v227, byteReplaceS4:[B
    const/4 v0, 0x0

    move-object/16 v324, v0

    .line 176
    .local v324, rep_maskS4:[B
    const/16 v198, 0x0

    .line 177
    .local v198, byteOrigS5:[B
    const/4 v0, 0x0

    move-object/16 v279, v0

    .line 178
    .local v279, maskS5:[B
    const/16 v228, 0x0

    .line 179
    .local v228, byteReplaceS5:[B
    const/4 v0, 0x0

    move-object/16 v325, v0

    .line 180
    .local v325, rep_maskS5:[B
    const/16 v199, 0x0

    .line 181
    .local v199, byteOrigS6:[B
    const/4 v0, 0x0

    move-object/16 v280, v0

    .line 182
    .local v280, maskS6:[B
    const/16 v229, 0x0

    .line 183
    .local v229, byteReplaceS6:[B
    const/4 v0, 0x0

    move-object/16 v326, v0

    .line 184
    .local v326, rep_maskS6:[B
    const/16 v200, 0x0

    .line 185
    .local v200, byteOrigS7:[B
    const/4 v0, 0x0

    move-object/16 v281, v0

    .line 186
    .local v281, maskS7:[B
    const/16 v230, 0x0

    .line 187
    .local v230, byteReplaceS7:[B
    const/4 v0, 0x0

    move-object/16 v327, v0

    .line 188
    .local v327, rep_maskS7:[B
    const/16 v201, 0x0

    .line 189
    .local v201, byteOrigS8:[B
    const/4 v0, 0x0

    move-object/16 v282, v0

    .line 190
    .local v282, maskS8:[B
    const/16 v231, 0x0

    .line 191
    .local v231, byteReplaceS8:[B
    const/4 v0, 0x0

    move-object/16 v328, v0

    .line 192
    .local v328, rep_maskS8:[B
    const/16 v162, 0x0

    .line 193
    .local v162, counter8:I
    const/16 v165, 0x0

    .line 194
    .local v165, byteOrigS9:[B
    const/16 v166, 0x0

    .line 195
    .local v166, maskS9:[B
    const/16 v167, 0x0

    .line 196
    .local v167, byteReplaceS9:[B
    const/16 v168, 0x0

    .line 197
    .local v168, rep_maskS9:[B
    const/16 v170, 0x0

    .line 198
    .local v170, counter9:I
    const/16 v192, 0x0

    .line 199
    .local v192, byteOrigS10:[B
    const/4 v0, 0x0

    move-object/16 v273, v0

    .line 200
    .local v273, maskS10:[B
    const/16 v222, 0x0

    .line 201
    .local v222, byteReplaceS10:[B
    const/4 v0, 0x0

    move-object/16 v319, v0

    .line 202
    .local v319, rep_maskS10:[B
    const/16 v193, 0x0

    .line 203
    .local v193, byteOrigS11:[B
    const/4 v0, 0x0

    move-object/16 v274, v0

    .line 204
    .local v274, maskS11:[B
    const/16 v223, 0x0

    .line 205
    .local v223, byteReplaceS11:[B
    const/4 v0, 0x0

    move-object/16 v320, v0

    .line 206
    .local v320, rep_maskS11:[B
    const/16 v194, 0x0

    .line 207
    .local v194, byteOrigS12:[B
    const/4 v0, 0x0

    move-object/16 v275, v0

    .line 208
    .local v275, maskS12:[B
    const/16 v224, 0x0

    .line 209
    .local v224, byteReplaceS12:[B
    const/4 v0, 0x0

    move-object/16 v321, v0

    .line 210
    .local v321, rep_maskS12:[B
    const/16 v143, 0x0

    .line 211
    .local v143, byteOrigS13:[B
    const/16 v144, 0x0

    .line 212
    .local v144, maskS13:[B
    const/16 v145, 0x0

    .line 213
    .local v145, byteReplaceS13:[B
    const/16 v146, 0x0

    .line 215
    .local v146, rep_maskS13:[B
    const/16 v185, 0x0

    .line 216
    .local v185, byteOrig3:[B
    const/16 v215, 0x0

    .line 217
    .local v215, byteReplace3:[B
    const/16 v186, 0x0

    .line 218
    .local v186, byteOrig4:[B
    const/4 v0, 0x0

    move-object/16 v267, v0

    .line 219
    .local v267, mask4:[B
    const/16 v216, 0x0

    .line 220
    .local v216, byteReplace4:[B
    const/4 v0, 0x0

    move-object/16 v313, v0

    .line 221
    .local v313, rep_mask4:[B
    const/16 v187, 0x0

    .line 222
    .local v187, byteOrig5:[B
    const/4 v0, 0x0

    move-object/16 v268, v0

    .line 223
    .local v268, mask5:[B
    const/16 v217, 0x0

    .line 224
    .local v217, byteReplace5:[B
    const/4 v0, 0x0

    move-object/16 v314, v0

    .line 225
    .local v314, rep_mask5:[B
    const/16 v188, 0x0

    .line 226
    .local v188, byteOrig6:[B
    const/4 v0, 0x0

    move-object/16 v269, v0

    .line 227
    .local v269, mask6:[B
    const/16 v218, 0x0

    .line 228
    .local v218, byteReplace6:[B
    const/4 v0, 0x0

    move-object/16 v315, v0

    .line 229
    .local v315, rep_mask6:[B
    const/16 v189, 0x0

    .line 230
    .local v189, byteOrig7:[B
    const/4 v0, 0x0

    move-object/16 v270, v0

    .line 231
    .local v270, mask7:[B
    const/16 v219, 0x0

    .line 232
    .local v219, byteReplace7:[B
    const/4 v0, 0x0

    move-object/16 v316, v0

    .line 233
    .local v316, rep_mask7:[B
    const/16 v190, 0x0

    .line 234
    .local v190, byteOrig8:[B
    const/4 v0, 0x0

    move-object/16 v271, v0

    .line 235
    .local v271, mask8:[B
    const/16 v220, 0x0

    .line 236
    .local v220, byteReplace8:[B
    const/4 v0, 0x0

    move-object/16 v317, v0

    .line 237
    .local v317, rep_mask8:[B
    const/16 v191, 0x0

    .line 238
    .local v191, byteOrig9:[B
    const/4 v0, 0x0

    move-object/16 v272, v0

    .line 239
    .local v272, mask9:[B
    const/16 v221, 0x0

    .line 240
    .local v221, byteReplace9:[B
    const/4 v0, 0x0

    move-object/16 v318, v0

    .line 241
    .local v318, rep_mask9:[B
    const/16 v172, 0x0

    .line 242
    .local v172, byteOrig10:[B
    const/16 v254, 0x0

    .line 243
    .local v254, mask10:[B
    const/16 v202, 0x0

    .line 244
    .local v202, byteReplace10:[B
    const/4 v0, 0x0

    move-object/16 v300, v0

    .line 245
    .local v300, rep_mask10:[B
    const/16 v173, 0x0

    .line 246
    .local v173, byteOrig11:[B
    const/16 v255, 0x0

    .line 247
    .local v255, mask11:[B
    const/16 v203, 0x0

    .line 248
    .local v203, byteReplace11:[B
    const/4 v0, 0x0

    move-object/16 v301, v0

    .line 249
    .local v301, rep_mask11:[B
    const/16 v174, 0x0

    .line 250
    .local v174, byteOrig12:[B
    const/4 v0, 0x0

    move-object/16 v256, v0

    .line 251
    .local v256, mask12:[B
    const/16 v204, 0x0

    .line 252
    .local v204, byteReplace12:[B
    const/4 v0, 0x0

    move-object/16 v302, v0

    .line 253
    .local v302, rep_mask12:[B
    const/16 v175, 0x0

    .line 254
    .local v175, byteOrig13:[B
    const/4 v0, 0x0

    move-object/16 v257, v0

    .line 255
    .local v257, mask13:[B
    const/16 v205, 0x0

    .line 256
    .local v205, byteReplace13:[B
    const/4 v0, 0x0

    move-object/16 v303, v0

    .line 257
    .local v303, rep_mask13:[B
    const/16 v176, 0x0

    .line 258
    .local v176, byteOrig14:[B
    const/4 v0, 0x0

    move-object/16 v258, v0

    .line 259
    .local v258, mask14:[B
    const/16 v206, 0x0

    .line 260
    .local v206, byteReplace14:[B
    const/4 v0, 0x0

    move-object/16 v304, v0

    .line 261
    .local v304, rep_mask14:[B
    const/16 v177, 0x0

    .line 262
    .local v177, byteOrig15:[B
    const/4 v0, 0x0

    move-object/16 v259, v0

    .line 263
    .local v259, mask15:[B
    const/16 v207, 0x0

    .line 264
    .local v207, byteReplace15:[B
    const/4 v0, 0x0

    move-object/16 v305, v0

    .line 265
    .local v305, rep_mask15:[B
    const/16 v178, 0x0

    .line 266
    .local v178, byteOrig16:[B
    const/4 v0, 0x0

    move-object/16 v260, v0

    .line 267
    .local v260, mask16:[B
    const/16 v208, 0x0

    .line 268
    .local v208, byteReplace16:[B
    const/4 v0, 0x0

    move-object/16 v306, v0

    .line 269
    .local v306, rep_mask16:[B
    const/16 v179, 0x0

    .line 270
    .local v179, byteOrig17:[B
    const/4 v0, 0x0

    move-object/16 v261, v0

    .line 271
    .local v261, mask17:[B
    const/16 v209, 0x0

    .line 272
    .local v209, byteReplace17:[B
    const/4 v0, 0x0

    move-object/16 v307, v0

    .line 273
    .local v307, rep_mask17:[B
    const/16 v180, 0x0

    .line 274
    .local v180, byteOrig18:[B
    const/4 v0, 0x0

    move-object/16 v262, v0

    .line 275
    .local v262, mask18:[B
    const/16 v210, 0x0

    .line 276
    .local v210, byteReplace18:[B
    const/4 v0, 0x0

    move-object/16 v308, v0

    .line 277
    .local v308, rep_mask18:[B
    const/16 v181, 0x0

    .line 278
    .local v181, byteOrig19:[B
    const/4 v0, 0x0

    move-object/16 v263, v0

    .line 279
    .local v263, mask19:[B
    const/16 v211, 0x0

    .line 280
    .local v211, byteReplace19:[B
    const/4 v0, 0x0

    move-object/16 v309, v0

    .line 281
    .local v309, rep_mask19:[B
    const/16 v183, 0x0

    .line 282
    .local v183, byteOrig20:[B
    const/4 v0, 0x0

    move-object/16 v265, v0

    .line 283
    .local v265, mask20:[B
    const/16 v213, 0x0

    .line 284
    .local v213, byteReplace20:[B
    const/4 v0, 0x0

    move-object/16 v311, v0

    .line 285
    .local v311, rep_mask20:[B
    const/16 v184, 0x0

    .line 286
    .local v184, byteOrig21:[B
    const/4 v0, 0x0

    move-object/16 v266, v0

    .line 287
    .local v266, mask21:[B
    const/16 v214, 0x0

    .line 288
    .local v214, byteReplace21:[B
    const/4 v0, 0x0

    move-object/16 v312, v0

    .line 290
    .local v312, rep_mask21:[B
    :try_start_0
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 291
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 292
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v10, 0x2

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 293
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v10, 0x3

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 294
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v10, 0x4

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 295
    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    if-eqz v9, :cond_0

    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "OnlyDalvik"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 296
    const/4 v9, 0x1

    sput-boolean v9, Lcom/chelpus/root/utils/corepatch;->onlyDalvik:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 298
    :cond_0
    :goto_0
    const/4 v9, 0x3

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    if-eqz v9, :cond_1

    const/4 v9, 0x3

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    sput-object v9, Lcom/chelpus/root/utils/corepatch;->toolfilesdir:Ljava/lang/String;

    .line 299
    :cond_1
    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "framework"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "OnlyDalvik"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string v9, "/system"

    const-string v10, "rw"

    invoke-static {v9, v10}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 301
    :cond_2
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "core.odex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "core.jar"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "core-libart.jar"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "boot.oat"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4

    :cond_3
    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "framework"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 303
    const/16 v154, 0x1

    const/16 v63, 0x1

    .line 304
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    sput-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    .line 305
    const/4 v9, 0x0

    const-string v10, "patch"

    move-object/from16 v0, p0

    aput-object v10, v0, v9

    .line 307
    :cond_4
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "services.jar"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_5

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "services.odex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    :cond_5
    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "framework"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 308
    const/16 v75, 0x1

    .line 309
    const/16 v87, 0x0

    .line 310
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    sput-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    .line 311
    const/4 v9, 0x0

    const-string v10, "patch"

    move-object/from16 v0, p0

    aput-object v10, v0, v9

    .line 315
    :cond_6
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 317
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "_patch1"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_7

    const/16 v154, 0x1

    .line 318
    :cond_7
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "_patch2"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_17a

    const/16 v63, 0x1

    move/16 v296, v63

    .line 319
    .end local v63           #pattern2:Z
    .local v296, pattern2:Z
    :goto_1
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "_patch3"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_179

    const/16 v75, 0x1

    move/16 v297, v75

    .line 320
    .end local v75           #pattern3:Z
    .local v297, pattern3:Z
    :goto_2
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "_patch4"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_178

    const/16 v87, 0x1

    move/16 v298, v87

    .line 323
    .end local v87           #pattern4:Z
    .local v298, pattern4:Z
    :goto_3
    const-string v3, "11 90 11 99 01 29 0F D1 01 26 28 1C C0 68 39 1C"

    .line 324
    .local v3, original:Ljava/lang/String;
    const-string v4, "?? ?? 01 21 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 325
    .local v4, replace:Ljava/lang/String;
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v5, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v6, v9, [B

    .line 326
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v7, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v8, v9, [B

    .line 327
    invoke-static/range {v3 .. v8}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 329
    invoke-static/range {v3 .. v8}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 332
    const-string v3, "39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7"

    .line 333
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 334
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v11, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v12, v9, [B

    .line 335
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v13, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v14, v9, [B

    move-object v9, v3

    move-object v10, v4

    .line 336
    invoke-static/range {v9 .. v14}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 339
    const-string v3, "08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7"

    .line 340
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 341
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v17, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v18, v0

    .line 342
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v19, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v20, v0

    move-object v15, v3

    move-object/from16 v16, v4

    .line 343
    invoke-static/range {v15 .. v20}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 346
    const-string v3, "56 45 00 F0 07 80 01 3C 00 F0 31 80 05 98"

    .line 347
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20"

    .line 348
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v23, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v24, v0

    .line 349
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v25, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v26, v0

    move-object/from16 v21, v3

    move-object/from16 v22, v4

    .line 350
    invoke-static/range {v21 .. v26}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 354
    const-string v3, "89 44 24 40 8B 5C 24 40 83 FB 01 75 32 BE 01 00 00 00 8B C5 8B 40 0C 8B CF"

    .line 355
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 356
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v29, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v30, v0

    .line 357
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v31, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v32, v0

    move-object/from16 v27, v3

    move-object/from16 v28, v4

    .line 358
    invoke-static/range {v27 .. v32}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 361
    const-string v3, "8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5"

    .line 362
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01"

    .line 363
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v35, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v36, v0

    .line 364
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v37, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v38, v0

    move-object/from16 v33, v3

    move-object/from16 v34, v4

    .line 365
    invoke-static/range {v33 .. v38}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 368
    const-string v3, "8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5 8B 6C 24 20"

    .line 369
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01 ?? ?? ?? ??"

    .line 370
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v41, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v42, v0

    .line 371
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v43, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v44, v0

    move-object/from16 v39, v3

    move-object/from16 v40, v4

    .line 372
    invoke-static/range {v39 .. v44}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 375
    const-string v3, "33 D2 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00"

    .line 376
    const-string v4, "B3 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 377
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v47, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v48, v0

    .line 378
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v49, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v50, v0

    move-object/from16 v45, v3

    move-object/from16 v46, v4

    .line 379
    invoke-static/range {v45 .. v50}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 383
    const-string v3, "E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 61 02 00 54 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA"

    .line 384
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 35 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 385
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v53, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v54, v0

    .line 386
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v55, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v56, v0

    move-object/from16 v51, v3

    move-object/from16 v52, v4

    .line 387
    invoke-static/range {v51 .. v56}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 390
    const-string v3, "F7 03 01 AA F9 03 02 AA FA 03 1F 2A F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B"

    .line 391
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? 3A 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 392
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v59, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v60, v0

    .line 393
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v61, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v62, v0

    move-object/from16 v57, v3

    move-object/from16 v58, v4

    .line 394
    invoke-static/range {v57 .. v62}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 398
    const-string v3, "CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 0B 98"

    .line 399
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20"

    .line 400
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v65, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v66, v0

    .line 401
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v67, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v68, v0

    move-object/from16 v63, v3

    move-object/from16 v64, v4

    .line 402
    invoke-static/range {v63 .. v68}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 405
    const-string v3, "00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9D 42"

    .line 406
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9B ??"

    .line 407
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v71, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v72, v0

    .line 408
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v73, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v74, v0

    move-object/from16 v69, v3

    move-object/from16 v70, v4

    .line 409
    invoke-static/range {v69 .. v74}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 412
    const-string v3, "2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 D0 F8 BC 01 D0 F8 28 E0 F0 47"

    .line 413
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 0D B0 BD E8 E0 8D ?? ??"

    .line 414
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v77, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v78, v0

    .line 415
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v79, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v80, v0

    move-object/from16 v75, v3

    move-object/from16 v76, v4

    .line 416
    invoke-static/range {v75 .. v80}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 419
    const-string v3, "1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 F0 47 14 90 14 ?? 00 ?? ?? D1"

    .line 420
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20 ?? ?? ?? ?? ?? ?? ?? ??"

    .line 421
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v83, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v84, v0

    .line 422
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v85, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v86, v0

    move-object/from16 v81, v3

    move-object/from16 v82, v4

    .line 423
    invoke-static/range {v81 .. v86}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 426
    const-string v3, "F0 47 82 46 BA F1 00 0F ?? D1 01 3C ?? F0 ?? ?? 28 ?? 00 2B"

    .line 427
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 428
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v89, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v90, v0

    .line 429
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v91, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v92, v0

    move-object/from16 v87, v3

    move-object/from16 v88, v4

    .line 430
    invoke-static/range {v87 .. v92}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 433
    const-string v3, "F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? D1 D9 F8 ?? ?? 39 1C"

    .line 434
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ??"

    .line 435
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v95, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v96, v0

    .line 436
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v97, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v98, v0

    move-object/from16 v93, v3

    move-object/from16 v94, v4

    .line 437
    invoke-static/range {v93 .. v98}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 440
    const-string v3, "50 F8 0C 00 D0 F8 ?? E0 F0 47 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47"

    .line 441
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 442
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v101, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v102, v0

    .line 443
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v103, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v104, v0

    move-object/from16 v99, v3

    move-object/from16 v100, v4

    .line 444
    invoke-static/range {v99 .. v104}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 449
    const-string v3, "BA 01 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 75 44 8B 54 24 58 85 D2 75 23"

    .line 450
    const-string v4, "?? 00 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? 90 90"

    .line 451
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v107, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v108, v0

    .line 452
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v109, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v110, v0

    move-object/from16 v105, v3

    move-object/from16 v106, v4

    .line 453
    invoke-static/range {v105 .. v110}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 456
    const-string v3, "85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B E9"

    .line 457
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ED"

    .line 458
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v113, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v114, v0

    .line 459
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v115, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v116, v0

    move-object/from16 v111, v3

    move-object/from16 v112, v4

    .line 460
    invoke-static/range {v111 .. v116}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 464
    const-string v3, "3D 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3B 02 00 B5 9C 01 00 B5"

    .line 465
    const-string v4, "FD 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FD 03 1F 2A FD 03 1F 2A"

    .line 466
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v119, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v120, v0

    .line 467
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v121, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v122, v0

    move-object/from16 v117, v3

    move-object/from16 v118, v4

    .line 468
    invoke-static/range {v117 .. v122}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 471
    const-string v3, "3C 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3A 02 00 B5 9B 01 00 B5"

    .line 472
    const-string v4, "FC 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FC 03 1F 2A FC 03 1F 2A"

    .line 473
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v125, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v126, v0

    .line 474
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v127, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v128, v0

    move-object/from16 v123, v3

    move-object/from16 v124, v4

    .line 475
    invoke-static/range {v123 .. v128}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 478
    const-string v3, "E2 33 00 B9 E3 33 40 B9 9F 02 03 6B 6A 13 00 54 A0 16 40 B9"

    .line 479
    const-string v4, "?? ?? ?? ?? E3 03 14 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 480
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v131, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v132, v0

    .line 481
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v133, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v134, v0

    move-object/from16 v129, v3

    move-object/from16 v130, v4

    .line 482
    invoke-static/range {v129 .. v134}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 485
    const-string v3, "D6 02 19 12 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA"

    .line 486
    const-string v4, "D6 02 19 32 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 487
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v137, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v138, v0

    .line 488
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v139, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v140, v0

    move-object/from16 v135, v3

    move-object/from16 v136, v4

    .line 489
    invoke-static/range {v135 .. v140}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 493
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/from16 v195, v0

    .end local v195           #byteOrigS2:[B
    fill-array-data v195, :array_0

    .line 494
    .restart local v195       #byteOrigS2:[B
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/16 v276, v0

    .end local v276           #maskS2:[B
    move-object/from16 v0, v276

    fill-array-data v0, :array_1

    .line 495
    .restart local v276       #maskS2:[B
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/from16 v225, v0

    .end local v225           #byteReplaceS2:[B
    fill-array-data v225, :array_2

    .line 496
    .restart local v225       #byteReplaceS2:[B
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/16 v322, v0

    .end local v322           #rep_maskS2:[B
    move-object/from16 v0, v322

    fill-array-data v0, :array_3

    .line 499
    .restart local v322       #rep_maskS2:[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/from16 v196, v0

    .end local v196           #byteOrigS3:[B
    fill-array-data v196, :array_4

    .line 500
    .restart local v196       #byteOrigS3:[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/16 v277, v0

    .end local v277           #maskS3:[B
    move-object/from16 v0, v277

    fill-array-data v0, :array_5

    .line 501
    .restart local v277       #maskS3:[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/from16 v226, v0

    .end local v226           #byteReplaceS3:[B
    fill-array-data v226, :array_6

    .line 502
    .restart local v226       #byteReplaceS3:[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/16 v323, v0

    .end local v323           #rep_maskS3:[B
    move-object/from16 v0, v323

    fill-array-data v0, :array_7

    .line 505
    .restart local v323       #rep_maskS3:[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/from16 v197, v0

    .end local v197           #byteOrigS4:[B
    fill-array-data v197, :array_8

    .line 506
    .restart local v197       #byteOrigS4:[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/16 v278, v0

    .end local v278           #maskS4:[B
    move-object/from16 v0, v278

    fill-array-data v0, :array_9

    .line 507
    .restart local v278       #maskS4:[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/from16 v227, v0

    .end local v227           #byteReplaceS4:[B
    fill-array-data v227, :array_a

    .line 508
    .restart local v227       #byteReplaceS4:[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/16 v324, v0

    .end local v324           #rep_maskS4:[B
    move-object/from16 v0, v324

    fill-array-data v0, :array_b

    .line 511
    .restart local v324       #rep_maskS4:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/from16 v198, v0

    .end local v198           #byteOrigS5:[B
    fill-array-data v198, :array_c

    .line 512
    .restart local v198       #byteOrigS5:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v279, v0

    .end local v279           #maskS5:[B
    move-object/from16 v0, v279

    fill-array-data v0, :array_d

    .line 513
    .restart local v279       #maskS5:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/from16 v228, v0

    .end local v228           #byteReplaceS5:[B
    fill-array-data v228, :array_e

    .line 514
    .restart local v228       #byteReplaceS5:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v325, v0

    .end local v325           #rep_maskS5:[B
    move-object/from16 v0, v325

    fill-array-data v0, :array_f

    .line 517
    .restart local v325       #rep_maskS5:[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/from16 v199, v0

    .end local v199           #byteOrigS6:[B
    fill-array-data v199, :array_10

    .line 518
    .restart local v199       #byteOrigS6:[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/16 v280, v0

    .end local v280           #maskS6:[B
    move-object/from16 v0, v280

    fill-array-data v0, :array_11

    .line 519
    .restart local v280       #maskS6:[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/from16 v229, v0

    .end local v229           #byteReplaceS6:[B
    fill-array-data v229, :array_12

    .line 520
    .restart local v229       #byteReplaceS6:[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/16 v326, v0

    .end local v326           #rep_maskS6:[B
    move-object/from16 v0, v326

    fill-array-data v0, :array_13

    .line 524
    .restart local v326       #rep_maskS6:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/from16 v200, v0

    .end local v200           #byteOrigS7:[B
    fill-array-data v200, :array_14

    .line 525
    .restart local v200       #byteOrigS7:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v281, v0

    .end local v281           #maskS7:[B
    move-object/from16 v0, v281

    fill-array-data v0, :array_15

    .line 526
    .restart local v281       #maskS7:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/from16 v230, v0

    .end local v230           #byteReplaceS7:[B
    fill-array-data v230, :array_16

    .line 527
    .restart local v230       #byteReplaceS7:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v327, v0

    .end local v327           #rep_maskS7:[B
    move-object/from16 v0, v327

    fill-array-data v0, :array_17

    .line 530
    .restart local v327       #rep_maskS7:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v201, v0

    .end local v201           #byteOrigS8:[B
    fill-array-data v201, :array_18

    .line 531
    .restart local v201       #byteOrigS8:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v282, v0

    .end local v282           #maskS8:[B
    move-object/from16 v0, v282

    fill-array-data v0, :array_19

    .line 532
    .restart local v282       #maskS8:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v231, v0

    .end local v231           #byteReplaceS8:[B
    fill-array-data v231, :array_1a

    .line 533
    .restart local v231       #byteReplaceS8:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v328, v0

    .end local v328           #rep_maskS8:[B
    move-object/from16 v0, v328

    fill-array-data v0, :array_1b

    .line 534
    .restart local v328       #rep_maskS8:[B
    const/16 v162, 0x2

    .line 537
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v165, v0

    .end local v165           #byteOrigS9:[B
    fill-array-data v165, :array_1c

    .line 538
    .restart local v165       #byteOrigS9:[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v166, v0

    .end local v166           #maskS9:[B
    fill-array-data v166, :array_1d

    .line 539
    .restart local v166       #maskS9:[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v167, v0

    .end local v167           #byteReplaceS9:[B
    fill-array-data v167, :array_1e

    .line 540
    .restart local v167       #byteReplaceS9:[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v168, v0

    .end local v168           #rep_maskS9:[B
    fill-array-data v168, :array_1f

    .line 541
    .restart local v168       #rep_maskS9:[B
    const/16 v170, 0x2

    .line 544
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v192, v0

    .end local v192           #byteOrigS10:[B
    fill-array-data v192, :array_20

    .line 545
    .restart local v192       #byteOrigS10:[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v273, v0

    .end local v273           #maskS10:[B
    move-object/from16 v0, v273

    fill-array-data v0, :array_21

    .line 546
    .restart local v273       #maskS10:[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v222, v0

    .end local v222           #byteReplaceS10:[B
    fill-array-data v222, :array_22

    .line 547
    .restart local v222       #byteReplaceS10:[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v319, v0

    .end local v319           #rep_maskS10:[B
    move-object/from16 v0, v319

    fill-array-data v0, :array_23

    .line 550
    .restart local v319       #rep_maskS10:[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v193, v0

    .end local v193           #byteOrigS11:[B
    fill-array-data v193, :array_24

    .line 551
    .restart local v193       #byteOrigS11:[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v274, v0

    .end local v274           #maskS11:[B
    move-object/from16 v0, v274

    fill-array-data v0, :array_25

    .line 552
    .restart local v274       #maskS11:[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v223, v0

    .end local v223           #byteReplaceS11:[B
    fill-array-data v223, :array_26

    .line 553
    .restart local v223       #byteReplaceS11:[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v320, v0

    .end local v320           #rep_maskS11:[B
    move-object/from16 v0, v320

    fill-array-data v0, :array_27

    .line 556
    .restart local v320       #rep_maskS11:[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/from16 v194, v0

    .end local v194           #byteOrigS12:[B
    fill-array-data v194, :array_28

    .line 557
    .restart local v194       #byteOrigS12:[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/16 v275, v0

    .end local v275           #maskS12:[B
    move-object/from16 v0, v275

    fill-array-data v0, :array_29

    .line 558
    .restart local v275       #maskS12:[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/from16 v224, v0

    .end local v224           #byteReplaceS12:[B
    fill-array-data v224, :array_2a

    .line 559
    .restart local v224       #byteReplaceS12:[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/16 v321, v0

    .end local v321           #rep_maskS12:[B
    move-object/from16 v0, v321

    fill-array-data v0, :array_2b

    .line 562
    .restart local v321       #rep_maskS12:[B
    const-string v3, "D5 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00"

    .line 563
    const-string v4, "D6 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 564
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v143, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v144, v0

    .line 565
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v145, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v146, v0

    move-object/from16 v141, v3

    move-object/from16 v142, v4

    .line 566
    invoke-static/range {v141 .. v146}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 570
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/from16 v172, v0

    .end local v172           #byteOrig10:[B
    fill-array-data v172, :array_2c

    .line 571
    .restart local v172       #byteOrig10:[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/from16 v254, v0

    .end local v254           #mask10:[B
    fill-array-data v254, :array_2d

    .line 572
    .restart local v254       #mask10:[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/from16 v202, v0

    .end local v202           #byteReplace10:[B
    fill-array-data v202, :array_2e

    .line 573
    .restart local v202       #byteReplace10:[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/16 v300, v0

    .end local v300           #rep_mask10:[B
    move-object/from16 v0, v300

    fill-array-data v0, :array_2f

    .line 576
    .restart local v300       #rep_mask10:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v173, v0

    .end local v173           #byteOrig11:[B
    fill-array-data v173, :array_30

    .line 577
    .restart local v173       #byteOrig11:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v255, v0

    .end local v255           #mask11:[B
    fill-array-data v255, :array_31

    .line 578
    .restart local v255       #mask11:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v203, v0

    .end local v203           #byteReplace11:[B
    fill-array-data v203, :array_32

    .line 579
    .restart local v203       #byteReplace11:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/16 v301, v0

    .end local v301           #rep_mask11:[B
    move-object/from16 v0, v301

    fill-array-data v0, :array_33

    .line 582
    .restart local v301       #rep_mask11:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v174, v0

    .end local v174           #byteOrig12:[B
    fill-array-data v174, :array_34

    .line 583
    .restart local v174       #byteOrig12:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v256, v0

    .end local v256           #mask12:[B
    move-object/from16 v0, v256

    fill-array-data v0, :array_35

    .line 584
    .restart local v256       #mask12:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v204, v0

    .end local v204           #byteReplace12:[B
    fill-array-data v204, :array_36

    .line 585
    .restart local v204       #byteReplace12:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v302, v0

    .end local v302           #rep_mask12:[B
    move-object/from16 v0, v302

    fill-array-data v0, :array_37

    .line 588
    .restart local v302       #rep_mask12:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v175, v0

    .end local v175           #byteOrig13:[B
    fill-array-data v175, :array_38

    .line 589
    .restart local v175       #byteOrig13:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v257, v0

    .end local v257           #mask13:[B
    move-object/from16 v0, v257

    fill-array-data v0, :array_39

    .line 590
    .restart local v257       #mask13:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v205, v0

    .end local v205           #byteReplace13:[B
    fill-array-data v205, :array_3a

    .line 591
    .restart local v205       #byteReplace13:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v303, v0

    .end local v303           #rep_mask13:[B
    move-object/from16 v0, v303

    fill-array-data v0, :array_3b

    .line 594
    .restart local v303       #rep_mask13:[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/from16 v176, v0

    .end local v176           #byteOrig14:[B
    fill-array-data v176, :array_3c

    .line 595
    .restart local v176       #byteOrig14:[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/16 v258, v0

    .end local v258           #mask14:[B
    move-object/from16 v0, v258

    fill-array-data v0, :array_3d

    .line 596
    .restart local v258       #mask14:[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/from16 v206, v0

    .end local v206           #byteReplace14:[B
    fill-array-data v206, :array_3e

    .line 597
    .restart local v206       #byteReplace14:[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/16 v304, v0

    .end local v304           #rep_mask14:[B
    move-object/from16 v0, v304

    fill-array-data v0, :array_3f

    .line 600
    .restart local v304       #rep_mask14:[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/from16 v177, v0

    .end local v177           #byteOrig15:[B
    fill-array-data v177, :array_40

    .line 601
    .restart local v177       #byteOrig15:[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/16 v259, v0

    .end local v259           #mask15:[B
    move-object/from16 v0, v259

    fill-array-data v0, :array_41

    .line 602
    .restart local v259       #mask15:[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/from16 v207, v0

    .end local v207           #byteReplace15:[B
    fill-array-data v207, :array_42

    .line 603
    .restart local v207       #byteReplace15:[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/16 v305, v0

    .end local v305           #rep_mask15:[B
    move-object/from16 v0, v305

    fill-array-data v0, :array_43

    .line 606
    .restart local v305       #rep_mask15:[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/from16 v178, v0

    .end local v178           #byteOrig16:[B
    fill-array-data v178, :array_44

    .line 607
    .restart local v178       #byteOrig16:[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/16 v260, v0

    .end local v260           #mask16:[B
    move-object/from16 v0, v260

    fill-array-data v0, :array_45

    .line 608
    .restart local v260       #mask16:[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/from16 v208, v0

    .end local v208           #byteReplace16:[B
    fill-array-data v208, :array_46

    .line 609
    .restart local v208       #byteReplace16:[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/16 v306, v0

    .end local v306           #rep_mask16:[B
    move-object/from16 v0, v306

    fill-array-data v0, :array_47

    .line 612
    .restart local v306       #rep_mask16:[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/from16 v179, v0

    .end local v179           #byteOrig17:[B
    fill-array-data v179, :array_48

    .line 613
    .restart local v179       #byteOrig17:[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/16 v261, v0

    .end local v261           #mask17:[B
    move-object/from16 v0, v261

    fill-array-data v0, :array_49

    .line 614
    .restart local v261       #mask17:[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/from16 v209, v0

    .end local v209           #byteReplace17:[B
    fill-array-data v209, :array_4a

    .line 615
    .restart local v209       #byteReplace17:[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/16 v307, v0

    .end local v307           #rep_mask17:[B
    move-object/from16 v0, v307

    fill-array-data v0, :array_4b

    .line 618
    .restart local v307       #rep_mask17:[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/from16 v180, v0

    .end local v180           #byteOrig18:[B
    fill-array-data v180, :array_4c

    .line 619
    .restart local v180       #byteOrig18:[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/16 v262, v0

    .end local v262           #mask18:[B
    move-object/from16 v0, v262

    fill-array-data v0, :array_4d

    .line 620
    .restart local v262       #mask18:[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/from16 v210, v0

    .end local v210           #byteReplace18:[B
    fill-array-data v210, :array_4e

    .line 621
    .restart local v210       #byteReplace18:[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/16 v308, v0

    .end local v308           #rep_mask18:[B
    move-object/from16 v0, v308

    fill-array-data v0, :array_4f

    .line 624
    .restart local v308       #rep_mask18:[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/from16 v181, v0

    .end local v181           #byteOrig19:[B
    fill-array-data v181, :array_50

    .line 625
    .restart local v181       #byteOrig19:[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/16 v263, v0

    .end local v263           #mask19:[B
    move-object/from16 v0, v263

    fill-array-data v0, :array_51

    .line 626
    .restart local v263       #mask19:[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/from16 v211, v0

    .end local v211           #byteReplace19:[B
    fill-array-data v211, :array_52

    .line 627
    .restart local v211       #byteReplace19:[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/16 v309, v0

    .end local v309           #rep_mask19:[B
    move-object/from16 v0, v309

    fill-array-data v0, :array_53

    .line 630
    .restart local v309       #rep_mask19:[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/from16 v183, v0

    .end local v183           #byteOrig20:[B
    fill-array-data v183, :array_54

    .line 631
    .restart local v183       #byteOrig20:[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/16 v265, v0

    .end local v265           #mask20:[B
    move-object/from16 v0, v265

    fill-array-data v0, :array_55

    .line 632
    .restart local v265       #mask20:[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/from16 v213, v0

    .end local v213           #byteReplace20:[B
    fill-array-data v213, :array_56

    .line 633
    .restart local v213       #byteReplace20:[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/16 v311, v0

    .end local v311           #rep_mask20:[B
    move-object/from16 v0, v311

    fill-array-data v0, :array_57

    .line 636
    .restart local v311       #rep_mask20:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v184, v0

    .end local v184           #byteOrig21:[B
    fill-array-data v184, :array_58

    .line 637
    .restart local v184       #byteOrig21:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/16 v266, v0

    .end local v266           #mask21:[B
    move-object/from16 v0, v266

    fill-array-data v0, :array_59

    .line 638
    .restart local v266       #mask21:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v214, v0

    .end local v214           #byteReplace21:[B
    fill-array-data v214, :array_5a

    .line 639
    .restart local v214       #byteReplace21:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/16 v312, v0

    .end local v312           #rep_mask21:[B
    move-object/from16 v0, v312

    fill-array-data v0, :array_5b

    .restart local v312       #rep_mask21:[B
    move/from16 v87, v298

    .end local v298           #pattern4:Z
    .restart local v87       #pattern4:Z
    move/from16 v75, v297

    .end local v297           #pattern3:Z
    .restart local v75       #pattern3:Z
    move/from16 v63, v296

    .line 643
    .end local v3           #original:Ljava/lang/String;
    .end local v4           #replace:Ljava/lang/String;
    .end local v296           #pattern2:Z
    .restart local v63       #pattern2:Z
    :cond_8
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 644
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restoreCore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_177

    const/16 v154, 0x1

    const/16 v63, 0x1

    move/16 v296, v63

    .line 645
    .end local v63           #pattern2:Z
    .restart local v296       #pattern2:Z
    :goto_4
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restoreServices"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_176

    const/16 v75, 0x1

    const/16 v87, 0x1

    move/16 v298, v87

    .end local v87           #pattern4:Z
    .restart local v298       #pattern4:Z
    move/16 v297, v75

    .line 648
    .end local v75           #pattern3:Z
    .restart local v297       #pattern3:Z
    :goto_5
    const-string v3, "11 90 01 21 01 29 0F D1 01 26 28 1C C0 68 39 1C"

    .line 649
    .restart local v3       #original:Ljava/lang/String;
    const-string v4, "?? ?? 11 99 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 650
    .restart local v4       #replace:Ljava/lang/String;
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v5, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v6, v9, [B

    .line 651
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v7, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v8, v9, [B

    .line 652
    invoke-static/range {v3 .. v8}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 654
    invoke-static/range {v3 .. v8}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 657
    const-string v3, "39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7"

    .line 658
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 659
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v11, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v12, v9, [B

    .line 660
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v13, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v14, v9, [B

    move-object v9, v3

    move-object v10, v4

    .line 661
    invoke-static/range {v9 .. v14}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 664
    const-string v3, "08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7"

    .line 665
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 666
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v17, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v18, v0

    .line 667
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v19, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v20, v0

    move-object v15, v3

    move-object/from16 v16, v4

    .line 668
    invoke-static/range {v15 .. v20}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 671
    const-string v3, "56 45 00 F0 07 80 01 3C 00 F0 31 80 01 20"

    .line 672
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 05 98"

    .line 673
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v23, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v24, v0

    .line 674
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v25, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v26, v0

    move-object/from16 v21, v3

    move-object/from16 v22, v4

    .line 675
    invoke-static/range {v21 .. v26}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 679
    const-string v3, "89 44 24 40 8B 5C 24 40 83 FB 01 90 90 BE 01 00 00 00 8B C5 8B 40 0C 8B CF"

    .line 680
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 32 BE ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 681
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v29, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v30, v0

    .line 682
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v31, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v32, v0

    move-object/from16 v27, v3

    move-object/from16 v28, v4

    .line 683
    invoke-static/range {v27 .. v32}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 686
    const-string v3, "8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01"

    .line 687
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5"

    .line 688
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v35, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v36, v0

    .line 689
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v37, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v38, v0

    move-object/from16 v33, v3

    move-object/from16 v34, v4

    .line 690
    invoke-static/range {v33 .. v38}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 693
    const-string v3, "8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01 8B 6C 24 20"

    .line 694
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5 ?? ?? ?? ??"

    .line 695
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v41, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v42, v0

    .line 696
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v43, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v44, v0

    move-object/from16 v39, v3

    move-object/from16 v40, v4

    .line 697
    invoke-static/range {v39 .. v44}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 700
    const-string v3, "B3 01 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00"

    .line 701
    const-string v4, "33 D2 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 702
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v47, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v48, v0

    .line 703
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v49, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v50, v0

    move-object/from16 v45, v3

    move-object/from16 v46, v4

    .line 704
    invoke-static/range {v45 .. v50}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 708
    const-string v3, "E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 35 00 80 52 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA"

    .line 709
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 61 02 00 54 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 710
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v53, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v54, v0

    .line 711
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v55, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v56, v0

    move-object/from16 v51, v3

    move-object/from16 v52, v4

    .line 712
    invoke-static/range {v51 .. v56}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 715
    const-string v3, "F7 03 01 AA F9 03 02 AA 3A 00 80 52 F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B"

    .line 716
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? FA 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 717
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v59, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v60, v0

    .line 718
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v61, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v62, v0

    move-object/from16 v57, v3

    move-object/from16 v58, v4

    .line 719
    invoke-static/range {v57 .. v62}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 725
    const-string v3, "CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 00 20"

    .line 726
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0B 98"

    .line 727
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v65, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v66, v0

    .line 728
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v67, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v68, v0

    move-object/from16 v63, v3

    move-object/from16 v64, v4

    .line 729
    invoke-static/range {v63 .. v68}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 732
    const-string v3, "00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9B 42"

    .line 733
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9D ??"

    .line 734
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v71, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v72, v0

    .line 735
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v73, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v74, v0

    move-object/from16 v69, v3

    move-object/from16 v70, v4

    .line 736
    invoke-static/range {v69 .. v74}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 739
    const-string v3, "2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 01 20 0D B0 BD E8 E0 8D F0 47"

    .line 740
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D0 F8 BC 01 D0 F8 28 E0 ?? ??"

    .line 741
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v77, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v78, v0

    .line 742
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v79, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v80, v0

    move-object/from16 v75, v3

    move-object/from16 v76, v4

    .line 743
    invoke-static/range {v75 .. v80}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 746
    const-string v3, "1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 00 20 14 90 14 ?? 00 ?? ?? D1"

    .line 747
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ??"

    .line 748
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v83, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v84, v0

    .line 749
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v85, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v86, v0

    move-object/from16 v81, v3

    move-object/from16 v82, v4

    .line 750
    invoke-static/range {v81 .. v86}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 753
    const-string v3, "F0 47 82 46 BA F1 00 0F ?? E0 01 3C ?? F0 ?? ?? 28 ?? 00 2B"

    .line 754
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 755
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v89, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v90, v0

    .line 756
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v91, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v92, v0

    move-object/from16 v87, v3

    move-object/from16 v88, v4

    .line 757
    invoke-static/range {v87 .. v92}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 760
    const-string v3, "F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? E0 D9 F8 ?? ?? 39 1C"

    .line 761
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ??"

    .line 762
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v95, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v96, v0

    .line 763
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v97, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v98, v0

    move-object/from16 v93, v3

    move-object/from16 v94, v4

    .line 764
    invoke-static/range {v93 .. v98}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 767
    const-string v3, "50 F8 0C 00 D0 F8 ?? E0 01 20 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47"

    .line 768
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 769
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v101, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v102, v0

    .line 770
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v103, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v104, v0

    move-object/from16 v99, v3

    move-object/from16 v100, v4

    .line 771
    invoke-static/range {v99 .. v104}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 776
    const-string v3, "BA 00 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 90 90 8B 54 24 58 85 D2 90 90"

    .line 777
    const-string v4, "?? 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 44 ?? ?? ?? ?? ?? ?? 75 23"

    .line 778
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v107, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v108, v0

    .line 779
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v109, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v110, v0

    move-object/from16 v105, v3

    move-object/from16 v106, v4

    .line 780
    invoke-static/range {v105 .. v110}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 783
    const-string v3, "85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B ED"

    .line 784
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E9"

    .line 785
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v113, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v114, v0

    .line 786
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v115, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v116, v0

    move-object/from16 v111, v3

    move-object/from16 v112, v4

    .line 787
    invoke-static/range {v111 .. v116}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 791
    const-string v3, "FD 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FD 03 1F 2A FD 03 1F 2A"

    .line 792
    const-string v4, "3D 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3B 02 00 B5 9C 01 00 B5"

    .line 793
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v119, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v120, v0

    .line 794
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v121, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v122, v0

    move-object/from16 v117, v3

    move-object/from16 v118, v4

    .line 795
    invoke-static/range {v117 .. v122}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 798
    const-string v3, "FC 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FC 03 1F 2A FC 03 1F 2A"

    .line 799
    const-string v4, "3C 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3A 02 00 B5 9B 01 00 B5"

    .line 800
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v125, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v126, v0

    .line 801
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v127, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v128, v0

    move-object/from16 v123, v3

    move-object/from16 v124, v4

    .line 802
    invoke-static/range {v123 .. v128}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 805
    const-string v3, "E2 33 00 B9 E3 03 14 2A 9F 02 03 6B 6A 13 00 54 A0 16 40 B9"

    .line 806
    const-string v4, "?? ?? ?? ?? E3 33 40 B9 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 807
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v131, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v132, v0

    .line 808
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v133, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v134, v0

    move-object/from16 v129, v3

    move-object/from16 v130, v4

    .line 809
    invoke-static/range {v129 .. v134}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 812
    const-string v3, "D6 02 19 32 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA"

    .line 813
    const-string v4, "D6 02 19 12 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 814
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v137, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v138, v0

    .line 815
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v139, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v140, v0

    move-object/from16 v135, v3

    move-object/from16 v136, v4

    .line 816
    invoke-static/range {v135 .. v140}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 820
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/from16 v182, v0

    .end local v182           #byteOrig2:[B
    fill-array-data v182, :array_5c

    .line 821
    .restart local v182       #byteOrig2:[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/16 v264, v0

    .end local v264           #mask2:[B
    move-object/from16 v0, v264

    fill-array-data v0, :array_5d

    .line 822
    .restart local v264       #mask2:[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/from16 v212, v0

    .end local v212           #byteReplace2:[B
    fill-array-data v212, :array_5e

    .line 823
    .restart local v212       #byteReplace2:[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/16 v310, v0

    .end local v310           #rep_mask2:[B
    move-object/from16 v0, v310

    fill-array-data v0, :array_5f

    .line 825
    .restart local v310       #rep_mask2:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v185, v0

    .end local v185           #byteOrig3:[B
    fill-array-data v185, :array_60

    .line 826
    .restart local v185       #byteOrig3:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v215, v0

    .end local v215           #byteReplace3:[B
    fill-array-data v215, :array_61

    .line 828
    .restart local v215       #byteReplace3:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v186, v0

    .end local v186           #byteOrig4:[B
    fill-array-data v186, :array_62

    .line 829
    .restart local v186       #byteOrig4:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v267, v0

    .end local v267           #mask4:[B
    move-object/from16 v0, v267

    fill-array-data v0, :array_63

    .line 830
    .restart local v267       #mask4:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v216, v0

    .end local v216           #byteReplace4:[B
    fill-array-data v216, :array_64

    .line 831
    .restart local v216       #byteReplace4:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v313, v0

    .end local v313           #rep_mask4:[B
    move-object/from16 v0, v313

    fill-array-data v0, :array_65

    .line 833
    .restart local v313       #rep_mask4:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v187, v0

    .end local v187           #byteOrig5:[B
    fill-array-data v187, :array_66

    .line 834
    .restart local v187       #byteOrig5:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v268, v0

    .end local v268           #mask5:[B
    move-object/from16 v0, v268

    fill-array-data v0, :array_67

    .line 835
    .restart local v268       #mask5:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v217, v0

    .end local v217           #byteReplace5:[B
    fill-array-data v217, :array_68

    .line 836
    .restart local v217       #byteReplace5:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v314, v0

    .end local v314           #rep_mask5:[B
    move-object/from16 v0, v314

    fill-array-data v0, :array_69

    .line 838
    .restart local v314       #rep_mask5:[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/from16 v188, v0

    .end local v188           #byteOrig6:[B
    fill-array-data v188, :array_6a

    .line 839
    .restart local v188       #byteOrig6:[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/16 v269, v0

    .end local v269           #mask6:[B
    move-object/from16 v0, v269

    fill-array-data v0, :array_6b

    .line 840
    .restart local v269       #mask6:[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/from16 v218, v0

    .end local v218           #byteReplace6:[B
    fill-array-data v218, :array_6c

    .line 841
    .restart local v218       #byteReplace6:[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/16 v315, v0

    .end local v315           #rep_mask6:[B
    move-object/from16 v0, v315

    fill-array-data v0, :array_6d

    .line 843
    .restart local v315       #rep_mask6:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v189, v0

    .end local v189           #byteOrig7:[B
    fill-array-data v189, :array_6e

    .line 844
    .restart local v189       #byteOrig7:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v270, v0

    .end local v270           #mask7:[B
    move-object/from16 v0, v270

    fill-array-data v0, :array_6f

    .line 845
    .restart local v270       #mask7:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v219, v0

    .end local v219           #byteReplace7:[B
    fill-array-data v219, :array_70

    .line 846
    .restart local v219       #byteReplace7:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v316, v0

    .end local v316           #rep_mask7:[B
    move-object/from16 v0, v316

    fill-array-data v0, :array_71

    .line 848
    .restart local v316       #rep_mask7:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v190, v0

    .end local v190           #byteOrig8:[B
    fill-array-data v190, :array_72

    .line 849
    .restart local v190       #byteOrig8:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v271, v0

    .end local v271           #mask8:[B
    move-object/from16 v0, v271

    fill-array-data v0, :array_73

    .line 850
    .restart local v271       #mask8:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v220, v0

    .end local v220           #byteReplace8:[B
    fill-array-data v220, :array_74

    .line 851
    .restart local v220       #byteReplace8:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v317, v0

    .end local v317           #rep_mask8:[B
    move-object/from16 v0, v317

    fill-array-data v0, :array_75

    .line 853
    .restart local v317       #rep_mask8:[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v191, v0

    .end local v191           #byteOrig9:[B
    fill-array-data v191, :array_76

    .line 854
    .restart local v191       #byteOrig9:[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/16 v272, v0

    .end local v272           #mask9:[B
    move-object/from16 v0, v272

    fill-array-data v0, :array_77

    .line 855
    .restart local v272       #mask9:[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v221, v0

    .end local v221           #byteReplace9:[B
    fill-array-data v221, :array_78

    .line 856
    .restart local v221       #byteReplace9:[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/16 v318, v0

    .end local v318           #rep_mask9:[B
    move-object/from16 v0, v318

    fill-array-data v0, :array_79

    .line 861
    .restart local v318       #rep_mask9:[B
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/from16 v195, v0

    .end local v195           #byteOrigS2:[B
    fill-array-data v195, :array_7a

    .line 862
    .restart local v195       #byteOrigS2:[B
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/16 v276, v0

    .end local v276           #maskS2:[B
    move-object/from16 v0, v276

    fill-array-data v0, :array_7b

    .line 863
    .restart local v276       #maskS2:[B
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/from16 v225, v0

    .end local v225           #byteReplaceS2:[B
    fill-array-data v225, :array_7c

    .line 864
    .restart local v225       #byteReplaceS2:[B
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/16 v322, v0

    .end local v322           #rep_maskS2:[B
    move-object/from16 v0, v322

    fill-array-data v0, :array_7d

    .line 867
    .restart local v322       #rep_maskS2:[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/from16 v196, v0

    .end local v196           #byteOrigS3:[B
    fill-array-data v196, :array_7e

    .line 868
    .restart local v196       #byteOrigS3:[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/16 v277, v0

    .end local v277           #maskS3:[B
    move-object/from16 v0, v277

    fill-array-data v0, :array_7f

    .line 869
    .restart local v277       #maskS3:[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/from16 v226, v0

    .end local v226           #byteReplaceS3:[B
    fill-array-data v226, :array_80

    .line 870
    .restart local v226       #byteReplaceS3:[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/16 v323, v0

    .end local v323           #rep_maskS3:[B
    move-object/from16 v0, v323

    fill-array-data v0, :array_81

    .line 873
    .restart local v323       #rep_maskS3:[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/from16 v197, v0

    .end local v197           #byteOrigS4:[B
    fill-array-data v197, :array_82

    .line 874
    .restart local v197       #byteOrigS4:[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/16 v278, v0

    .end local v278           #maskS4:[B
    move-object/from16 v0, v278

    fill-array-data v0, :array_83

    .line 875
    .restart local v278       #maskS4:[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/from16 v227, v0

    .end local v227           #byteReplaceS4:[B
    fill-array-data v227, :array_84

    .line 876
    .restart local v227       #byteReplaceS4:[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/16 v324, v0

    .end local v324           #rep_maskS4:[B
    move-object/from16 v0, v324

    fill-array-data v0, :array_85

    .line 879
    .restart local v324       #rep_maskS4:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/from16 v198, v0

    .end local v198           #byteOrigS5:[B
    fill-array-data v198, :array_86

    .line 880
    .restart local v198       #byteOrigS5:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v279, v0

    .end local v279           #maskS5:[B
    move-object/from16 v0, v279

    fill-array-data v0, :array_87

    .line 881
    .restart local v279       #maskS5:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/from16 v228, v0

    .end local v228           #byteReplaceS5:[B
    fill-array-data v228, :array_88

    .line 882
    .restart local v228       #byteReplaceS5:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v325, v0

    .end local v325           #rep_maskS5:[B
    move-object/from16 v0, v325

    fill-array-data v0, :array_89

    .line 885
    .restart local v325       #rep_maskS5:[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/from16 v199, v0

    .end local v199           #byteOrigS6:[B
    fill-array-data v199, :array_8a

    .line 886
    .restart local v199       #byteOrigS6:[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/16 v280, v0

    .end local v280           #maskS6:[B
    move-object/from16 v0, v280

    fill-array-data v0, :array_8b

    .line 887
    .restart local v280       #maskS6:[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/from16 v229, v0

    .end local v229           #byteReplaceS6:[B
    fill-array-data v229, :array_8c

    .line 888
    .restart local v229       #byteReplaceS6:[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/16 v326, v0

    .end local v326           #rep_maskS6:[B
    move-object/from16 v0, v326

    fill-array-data v0, :array_8d

    .line 891
    .restart local v326       #rep_maskS6:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/from16 v200, v0

    .end local v200           #byteOrigS7:[B
    fill-array-data v200, :array_8e

    .line 892
    .restart local v200       #byteOrigS7:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v281, v0

    .end local v281           #maskS7:[B
    move-object/from16 v0, v281

    fill-array-data v0, :array_8f

    .line 893
    .restart local v281       #maskS7:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/from16 v230, v0

    .end local v230           #byteReplaceS7:[B
    fill-array-data v230, :array_90

    .line 894
    .restart local v230       #byteReplaceS7:[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v327, v0

    .end local v327           #rep_maskS7:[B
    move-object/from16 v0, v327

    fill-array-data v0, :array_91

    .line 897
    .restart local v327       #rep_maskS7:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v201, v0

    .end local v201           #byteOrigS8:[B
    fill-array-data v201, :array_92

    .line 898
    .restart local v201       #byteOrigS8:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v282, v0

    .end local v282           #maskS8:[B
    move-object/from16 v0, v282

    fill-array-data v0, :array_93

    .line 899
    .restart local v282       #maskS8:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v231, v0

    .end local v231           #byteReplaceS8:[B
    fill-array-data v231, :array_94

    .line 900
    .restart local v231       #byteReplaceS8:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v328, v0

    .end local v328           #rep_maskS8:[B
    move-object/from16 v0, v328

    fill-array-data v0, :array_95

    .line 901
    .restart local v328       #rep_maskS8:[B
    const/16 v162, 0x0

    .line 904
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v165, v0

    .end local v165           #byteOrigS9:[B
    fill-array-data v165, :array_96

    .line 905
    .restart local v165       #byteOrigS9:[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v166, v0

    .end local v166           #maskS9:[B
    fill-array-data v166, :array_97

    .line 906
    .restart local v166       #maskS9:[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v167, v0

    .end local v167           #byteReplaceS9:[B
    fill-array-data v167, :array_98

    .line 907
    .restart local v167       #byteReplaceS9:[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v168, v0

    .end local v168           #rep_maskS9:[B
    fill-array-data v168, :array_99

    .line 908
    .restart local v168       #rep_maskS9:[B
    const/16 v170, 0x0

    .line 911
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v192, v0

    .end local v192           #byteOrigS10:[B
    fill-array-data v192, :array_9a

    .line 912
    .restart local v192       #byteOrigS10:[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v273, v0

    .end local v273           #maskS10:[B
    move-object/from16 v0, v273

    fill-array-data v0, :array_9b

    .line 913
    .restart local v273       #maskS10:[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v222, v0

    .end local v222           #byteReplaceS10:[B
    fill-array-data v222, :array_9c

    .line 914
    .restart local v222       #byteReplaceS10:[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v319, v0

    .end local v319           #rep_maskS10:[B
    move-object/from16 v0, v319

    fill-array-data v0, :array_9d

    .line 917
    .restart local v319       #rep_maskS10:[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v193, v0

    .end local v193           #byteOrigS11:[B
    fill-array-data v193, :array_9e

    .line 918
    .restart local v193       #byteOrigS11:[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v274, v0

    .end local v274           #maskS11:[B
    move-object/from16 v0, v274

    fill-array-data v0, :array_9f

    .line 919
    .restart local v274       #maskS11:[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v223, v0

    .end local v223           #byteReplaceS11:[B
    fill-array-data v223, :array_a0

    .line 920
    .restart local v223       #byteReplaceS11:[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v320, v0

    .end local v320           #rep_maskS11:[B
    move-object/from16 v0, v320

    fill-array-data v0, :array_a1

    .line 923
    .restart local v320       #rep_maskS11:[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/from16 v194, v0

    .end local v194           #byteOrigS12:[B
    fill-array-data v194, :array_a2

    .line 924
    .restart local v194       #byteOrigS12:[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/16 v275, v0

    .end local v275           #maskS12:[B
    move-object/from16 v0, v275

    fill-array-data v0, :array_a3

    .line 925
    .restart local v275       #maskS12:[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/from16 v224, v0

    .end local v224           #byteReplaceS12:[B
    fill-array-data v224, :array_a4

    .line 926
    .restart local v224       #byteReplaceS12:[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/16 v321, v0

    .end local v321           #rep_maskS12:[B
    move-object/from16 v0, v321

    fill-array-data v0, :array_a5

    .line 929
    .restart local v321       #rep_maskS12:[B
    const-string v3, "D6 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00"

    .line 930
    const-string v4, "D5 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 931
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v143, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v144, v0

    .line 932
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v145, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v146, v0

    move-object/from16 v141, v3

    move-object/from16 v142, v4

    .line 933
    invoke-static/range {v141 .. v146}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 937
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/from16 v172, v0

    .end local v172           #byteOrig10:[B
    fill-array-data v172, :array_a6

    .line 938
    .restart local v172       #byteOrig10:[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/from16 v254, v0

    .end local v254           #mask10:[B
    fill-array-data v254, :array_a7

    .line 939
    .restart local v254       #mask10:[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/from16 v202, v0

    .end local v202           #byteReplace10:[B
    fill-array-data v202, :array_a8

    .line 940
    .restart local v202       #byteReplace10:[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/16 v300, v0

    .end local v300           #rep_mask10:[B
    move-object/from16 v0, v300

    fill-array-data v0, :array_a9

    .line 943
    .restart local v300       #rep_mask10:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v173, v0

    .end local v173           #byteOrig11:[B
    fill-array-data v173, :array_aa

    .line 944
    .restart local v173       #byteOrig11:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v255, v0

    .end local v255           #mask11:[B
    fill-array-data v255, :array_ab

    .line 945
    .restart local v255       #mask11:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v203, v0

    .end local v203           #byteReplace11:[B
    fill-array-data v203, :array_ac

    .line 946
    .restart local v203       #byteReplace11:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/16 v301, v0

    .end local v301           #rep_mask11:[B
    move-object/from16 v0, v301

    fill-array-data v0, :array_ad

    .line 948
    .restart local v301       #rep_mask11:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v174, v0

    .end local v174           #byteOrig12:[B
    fill-array-data v174, :array_ae

    .line 949
    .restart local v174       #byteOrig12:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v256, v0

    .end local v256           #mask12:[B
    move-object/from16 v0, v256

    fill-array-data v0, :array_af

    .line 950
    .restart local v256       #mask12:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v204, v0

    .end local v204           #byteReplace12:[B
    fill-array-data v204, :array_b0

    .line 951
    .restart local v204       #byteReplace12:[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v302, v0

    .end local v302           #rep_mask12:[B
    move-object/from16 v0, v302

    fill-array-data v0, :array_b1

    .line 954
    .restart local v302       #rep_mask12:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v175, v0

    .end local v175           #byteOrig13:[B
    fill-array-data v175, :array_b2

    .line 955
    .restart local v175       #byteOrig13:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v257, v0

    .end local v257           #mask13:[B
    move-object/from16 v0, v257

    fill-array-data v0, :array_b3

    .line 956
    .restart local v257       #mask13:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v205, v0

    .end local v205           #byteReplace13:[B
    fill-array-data v205, :array_b4

    .line 957
    .restart local v205       #byteReplace13:[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v303, v0

    .end local v303           #rep_mask13:[B
    move-object/from16 v0, v303

    fill-array-data v0, :array_b5

    .line 960
    .restart local v303       #rep_mask13:[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/from16 v176, v0

    .end local v176           #byteOrig14:[B
    fill-array-data v176, :array_b6

    .line 961
    .restart local v176       #byteOrig14:[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/16 v258, v0

    .end local v258           #mask14:[B
    move-object/from16 v0, v258

    fill-array-data v0, :array_b7

    .line 962
    .restart local v258       #mask14:[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/from16 v206, v0

    .end local v206           #byteReplace14:[B
    fill-array-data v206, :array_b8

    .line 963
    .restart local v206       #byteReplace14:[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/16 v304, v0

    .end local v304           #rep_mask14:[B
    move-object/from16 v0, v304

    fill-array-data v0, :array_b9

    .line 966
    .restart local v304       #rep_mask14:[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/from16 v177, v0

    .end local v177           #byteOrig15:[B
    fill-array-data v177, :array_ba

    .line 967
    .restart local v177       #byteOrig15:[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/16 v259, v0

    .end local v259           #mask15:[B
    move-object/from16 v0, v259

    fill-array-data v0, :array_bb

    .line 968
    .restart local v259       #mask15:[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/from16 v207, v0

    .end local v207           #byteReplace15:[B
    fill-array-data v207, :array_bc

    .line 969
    .restart local v207       #byteReplace15:[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/16 v305, v0

    .end local v305           #rep_mask15:[B
    move-object/from16 v0, v305

    fill-array-data v0, :array_bd

    .line 972
    .restart local v305       #rep_mask15:[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/from16 v178, v0

    .end local v178           #byteOrig16:[B
    fill-array-data v178, :array_be

    .line 973
    .restart local v178       #byteOrig16:[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/16 v260, v0

    .end local v260           #mask16:[B
    move-object/from16 v0, v260

    fill-array-data v0, :array_bf

    .line 974
    .restart local v260       #mask16:[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/from16 v208, v0

    .end local v208           #byteReplace16:[B
    fill-array-data v208, :array_c0

    .line 975
    .restart local v208       #byteReplace16:[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/16 v306, v0

    .end local v306           #rep_mask16:[B
    move-object/from16 v0, v306

    fill-array-data v0, :array_c1

    .line 978
    .restart local v306       #rep_mask16:[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/from16 v179, v0

    .end local v179           #byteOrig17:[B
    fill-array-data v179, :array_c2

    .line 979
    .restart local v179       #byteOrig17:[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/16 v261, v0

    .end local v261           #mask17:[B
    move-object/from16 v0, v261

    fill-array-data v0, :array_c3

    .line 980
    .restart local v261       #mask17:[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/from16 v209, v0

    .end local v209           #byteReplace17:[B
    fill-array-data v209, :array_c4

    .line 981
    .restart local v209       #byteReplace17:[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/16 v307, v0

    .end local v307           #rep_mask17:[B
    move-object/from16 v0, v307

    fill-array-data v0, :array_c5

    .line 984
    .restart local v307       #rep_mask17:[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/from16 v180, v0

    .end local v180           #byteOrig18:[B
    fill-array-data v180, :array_c6

    .line 985
    .restart local v180       #byteOrig18:[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/16 v262, v0

    .end local v262           #mask18:[B
    move-object/from16 v0, v262

    fill-array-data v0, :array_c7

    .line 986
    .restart local v262       #mask18:[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/from16 v210, v0

    .end local v210           #byteReplace18:[B
    fill-array-data v210, :array_c8

    .line 987
    .restart local v210       #byteReplace18:[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/16 v308, v0

    .end local v308           #rep_mask18:[B
    move-object/from16 v0, v308

    fill-array-data v0, :array_c9

    .line 990
    .restart local v308       #rep_mask18:[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/from16 v181, v0

    .end local v181           #byteOrig19:[B
    fill-array-data v181, :array_ca

    .line 991
    .restart local v181       #byteOrig19:[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/16 v263, v0

    .end local v263           #mask19:[B
    move-object/from16 v0, v263

    fill-array-data v0, :array_cb

    .line 992
    .restart local v263       #mask19:[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/from16 v211, v0

    .end local v211           #byteReplace19:[B
    fill-array-data v211, :array_cc

    .line 993
    .restart local v211       #byteReplace19:[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/16 v309, v0

    .end local v309           #rep_mask19:[B
    move-object/from16 v0, v309

    fill-array-data v0, :array_cd

    .line 996
    .restart local v309       #rep_mask19:[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/from16 v183, v0

    .end local v183           #byteOrig20:[B
    fill-array-data v183, :array_ce

    .line 997
    .restart local v183       #byteOrig20:[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/16 v265, v0

    .end local v265           #mask20:[B
    move-object/from16 v0, v265

    fill-array-data v0, :array_cf

    .line 998
    .restart local v265       #mask20:[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/from16 v213, v0

    .end local v213           #byteReplace20:[B
    fill-array-data v213, :array_d0

    .line 999
    .restart local v213       #byteReplace20:[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/16 v311, v0

    .end local v311           #rep_mask20:[B
    move-object/from16 v0, v311

    fill-array-data v0, :array_d1

    .line 1002
    .restart local v311       #rep_mask20:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v184, v0

    .end local v184           #byteOrig21:[B
    fill-array-data v184, :array_d2

    .line 1003
    .restart local v184       #byteOrig21:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/16 v266, v0

    .end local v266           #mask21:[B
    move-object/from16 v0, v266

    fill-array-data v0, :array_d3

    .line 1004
    .restart local v266       #mask21:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v214, v0

    .end local v214           #byteReplace21:[B
    fill-array-data v214, :array_d4

    .line 1005
    .restart local v214       #byteReplace21:[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/16 v312, v0

    .end local v312           #rep_mask21:[B
    move-object/from16 v0, v312

    fill-array-data v0, :array_d5

    .restart local v312       #rep_mask21:[B
    move/from16 v87, v298

    .end local v298           #pattern4:Z
    .restart local v87       #pattern4:Z
    move/from16 v75, v297

    .end local v297           #pattern3:Z
    .restart local v75       #pattern3:Z
    move/from16 v63, v296

    .line 1010
    .end local v3           #original:Ljava/lang/String;
    .end local v4           #replace:Ljava/lang/String;
    .end local v296           #pattern2:Z
    .restart local v63       #pattern2:Z
    :cond_9
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_a

    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_a

    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "all"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_10f

    .line 1011
    :cond_a
    new-instance v243, Ljava/util/ArrayList;

    invoke-direct/range {v243 .. v243}, Ljava/util/ArrayList;-><init>()V

    .line 1012
    .local v243, filesToPatch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-virtual/range {v243 .. v243}, Ljava/util/ArrayList;->clear()V

    .line 1013
    if-nez v154, :cond_b

    if-eqz v63, :cond_79

    .line 1015
    :cond_b
    new-instance v0, Ljava/util/ArrayList;

    move-object/16 v283, v0

    invoke-direct/range {v283 .. v283}, Ljava/util/ArrayList;-><init>()V

    .line 1016
    .local v283, oatForPatch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/x86/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_c

    .line 1017
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/x86/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v283

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1019
    :cond_c
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm64/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_d

    .line 1020
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm64/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v283

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1022
    :cond_d
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1023
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v283

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1026
    :cond_e
    invoke-virtual/range {v283 .. v283}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_35

    sget v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->api:I

    const/16 v10, 0x15

    if-lt v9, v10, :cond_35

    .line 1028
    const/4 v0, 0x0

    move/16 v295, v0

    .local v295, patchOatUpd1:Z
    const/4 v0, 0x0

    move/16 v289, v0

    .local v289, patchOat1:Z
    const/4 v0, 0x0

    move/16 v290, v0

    .local v290, patchOat2:Z
    const/4 v0, 0x0

    move/16 v291, v0

    .local v291, patchOat3:Z
    const/4 v0, 0x0

    move/16 v292, v0

    .line 1029
    .local v292, patchOat4:Z
    invoke-virtual/range {v283 .. v283}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v64

    :goto_6
    invoke-interface/range {v64 .. v64}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_33

    invoke-interface/range {v64 .. v64}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v241

    check-cast v241, Ljava/io/File;

    .line 1030
    .local v241, fileForPatch:Ljava/io/File;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "oat file for patch:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v241 .. v241}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1031
    const/16 v147, 0x0

    .line 1032
    .local v147, ChannelDex:Ljava/nio/channels/FileChannel;
    const/4 v0, 0x0

    move/16 v331, v0

    .local v331, uni:Z
    const/4 v0, 0x0

    move/16 v284, v0

    .local v284, p11:Z
    const/4 v0, 0x0

    move/16 v285, v0

    .local v285, p12:Z
    const/4 v0, 0x0

    move/16 v286, v0

    .line 1034
    .local v286, p2:Z
    :try_start_1
    new-instance v9, Ljava/io/RandomAccessFile;

    const-string v10, "rw"

    move-object/from16 v0, v241

    invoke-direct {v9, v0, v10}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v147

    .line 1035
    sget-object v148, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v149, 0x0

    invoke-virtual/range {v147 .. v147}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v9

    long-to-int v9, v9

    int-to-long v0, v9

    move-wide/from16 v151, v0

    invoke-virtual/range {v147 .. v152}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v9

    sput-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    .line 1037
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    const/16 v10, 0x1018

    invoke-virtual {v9, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1038
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v9

    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v10

    sget-object v15, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v15}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v15

    sget-object v16, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v16

    move/from16 v0, v16

    invoke-static {v9, v10, v15, v0}, Lcom/chelpus/Utils;->convertFourBytesToInt(BBBB)I

    move-result v0

    move/16 v299, v0

    .line 1039
    .local v299, posit:I
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Start position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v299

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1040
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    move/from16 v0, v299

    invoke-virtual {v9, v0}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1043
    :goto_7
    :try_start_2
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v9

    if-eqz v9, :cond_31

    .line 1045
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v148

    .line 1046
    .local v148, curentPos:I
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v149

    .local v149, curentByte:B
    move-object/from16 v150, v5

    move-object/from16 v151, v6

    move-object/from16 v152, v7

    move-object/from16 v153, v8

    .line 1047
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 1048
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 1049
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1050
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1052
    :cond_f
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 1053
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1055
    :cond_10
    const/4 v0, 0x1

    move/16 v295, v0

    .line 1056
    const/4 v0, 0x1

    move/16 v331, v0

    const/4 v0, 0x1

    move/16 v284, v0

    const/4 v0, 0x1

    move/16 v285, v0

    :cond_11
    move/from16 v9, v148

    move/from16 v10, v149

    move/from16 v15, v154

    .line 1059
    invoke-static/range {v9 .. v15}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_14

    .line 1060
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 1061
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1062
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1064
    :cond_12
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_13

    .line 1065
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1067
    :cond_13
    const/4 v0, 0x1

    move/16 v289, v0

    .line 1068
    const/4 v0, 0x1

    move/16 v284, v0

    :cond_14
    move/from16 v27, v148

    move/from16 v28, v149

    move/from16 v33, v154

    .line 1071
    invoke-static/range {v27 .. v33}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_17

    .line 1072
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_15

    .line 1073
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1074
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1076
    :cond_15
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_16

    .line 1077
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1079
    :cond_16
    const/4 v0, 0x1

    move/16 v289, v0

    .line 1080
    const/4 v0, 0x1

    move/16 v284, v0

    const/4 v0, 0x1

    move/16 v285, v0

    const/4 v0, 0x1

    move/16 v331, v0

    :cond_17
    move/from16 v51, v148

    move/from16 v52, v149

    move/from16 v57, v154

    .line 1083
    invoke-static/range {v51 .. v57}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_1a

    .line 1084
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_18

    .line 1085
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1086
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1088
    :cond_18
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_19

    .line 1089
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1091
    :cond_19
    const/4 v0, 0x1

    move/16 v289, v0

    .line 1092
    const/4 v0, 0x1

    move/16 v331, v0

    const/4 v0, 0x1

    move/16 v284, v0

    const/4 v0, 0x1

    move/16 v285, v0

    :cond_1a
    move/from16 v33, v148

    move/from16 v34, v149

    move/from16 v39, v154

    .line 1095
    invoke-static/range {v33 .. v39}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_1d

    .line 1096
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1b

    .line 1097
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1098
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1100
    :cond_1b
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1c

    .line 1101
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1103
    :cond_1c
    const/4 v0, 0x1

    move/16 v289, v0

    .line 1104
    const/4 v0, 0x1

    move/16 v284, v0

    :cond_1d
    move/from16 v15, v148

    move/from16 v16, v149

    move/from16 v21, v154

    .line 1107
    invoke-static/range {v15 .. v21}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_20

    .line 1108
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1e

    .line 1109
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1110
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1112
    :cond_1e
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1f

    .line 1113
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1115
    :cond_1f
    const/4 v0, 0x1

    move/16 v290, v0

    .line 1116
    const/4 v0, 0x1

    move/16 v285, v0

    :cond_20
    move/from16 v39, v148

    move/from16 v40, v149

    move/from16 v45, v154

    .line 1118
    invoke-static/range {v39 .. v45}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_23

    .line 1119
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_21

    .line 1120
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1121
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1123
    :cond_21
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_22

    .line 1124
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1126
    :cond_22
    const/4 v0, 0x1

    move/16 v290, v0

    .line 1127
    const/4 v0, 0x1

    move/16 v285, v0

    :cond_23
    move/from16 v21, v148

    move/from16 v22, v149

    move/from16 v27, v63

    .line 1129
    invoke-static/range {v21 .. v27}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_26

    .line 1130
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_24

    .line 1131
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1132
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1134
    :cond_24
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_25

    .line 1135
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1137
    :cond_25
    const/4 v0, 0x1

    move/16 v291, v0

    .line 1138
    const/4 v0, 0x1

    move/16 v286, v0

    :cond_26
    move/from16 v45, v148

    move/from16 v46, v149

    move/from16 v51, v63

    .line 1140
    invoke-static/range {v45 .. v51}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_29

    .line 1141
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_27

    .line 1142
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1143
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1145
    :cond_27
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_28

    .line 1146
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1148
    :cond_28
    const/4 v0, 0x1

    move/16 v291, v0

    .line 1149
    const/4 v0, 0x1

    move/16 v286, v0

    :cond_29
    move/from16 v57, v148

    move/from16 v58, v149

    .line 1151
    invoke-static/range {v57 .. v63}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2c

    .line 1152
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2a

    .line 1153
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1154
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1156
    :cond_2a
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2b

    .line 1157
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1159
    :cond_2b
    const/4 v0, 0x1

    move/16 v291, v0

    .line 1160
    const/4 v0, 0x1

    move/16 v286, v0

    .line 1162
    :cond_2c
    if-eqz v154, :cond_2d

    if-eqz v63, :cond_2d

    move/from16 v0, v331

    if-eqz v0, :cond_2d

    move/from16 v0, v286

    if-nez v0, :cond_31

    :cond_2d
    if-eqz v154, :cond_2e

    if-eqz v63, :cond_2e

    move/from16 v0, v331

    if-eqz v0, :cond_2e

    move/from16 v0, v284

    if-eqz v0, :cond_2e

    move/from16 v0, v285

    if-eqz v0, :cond_2e

    move/from16 v0, v286

    if-nez v0, :cond_31

    :cond_2e
    if-eqz v154, :cond_2f

    if-nez v63, :cond_2f

    move/from16 v0, v331

    if-nez v0, :cond_31

    :cond_2f
    if-eqz v154, :cond_30

    if-nez v63, :cond_30

    move/from16 v0, v331

    if-eqz v0, :cond_30

    move/from16 v0, v284

    if-eqz v0, :cond_30

    move/from16 v0, v285

    if-nez v0, :cond_31

    :cond_30
    if-nez v154, :cond_32

    if-eqz v63, :cond_32

    move/from16 v0, v286

    if-eqz v0, :cond_32

    .line 1182
    .end local v148           #curentPos:I
    .end local v149           #curentByte:B
    :cond_31
    :goto_8
    :try_start_3
    invoke-virtual/range {v147 .. v147}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_6

    .line 1183
    .end local v299           #posit:I
    :catch_0
    move-exception v240

    .line 1184
    .local v240, e:Ljava/io/IOException;
    invoke-virtual/range {v240 .. v240}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_6

    .line 297
    .end local v147           #ChannelDex:Ljava/nio/channels/FileChannel;
    .end local v240           #e:Ljava/io/IOException;
    .end local v241           #fileForPatch:Ljava/io/File;
    .end local v243           #filesToPatch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v283           #oatForPatch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v284           #p11:Z
    .end local v285           #p12:Z
    .end local v286           #p2:Z
    .end local v289           #patchOat1:Z
    .end local v290           #patchOat2:Z
    .end local v291           #patchOat3:Z
    .end local v292           #patchOat4:Z
    .end local v295           #patchOatUpd1:Z
    .end local v331           #uni:Z
    :catch_1
    move-exception v240

    .local v240, e:Ljava/lang/Exception;
    invoke-virtual/range {v240 .. v240}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1176
    .end local v240           #e:Ljava/lang/Exception;
    .restart local v147       #ChannelDex:Ljava/nio/channels/FileChannel;
    .restart local v148       #curentPos:I
    .restart local v149       #curentByte:B
    .restart local v241       #fileForPatch:Ljava/io/File;
    .restart local v243       #filesToPatch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v283       #oatForPatch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v284       #p11:Z
    .restart local v285       #p12:Z
    .restart local v286       #p2:Z
    .restart local v289       #patchOat1:Z
    .restart local v290       #patchOat2:Z
    .restart local v291       #patchOat3:Z
    .restart local v292       #patchOat4:Z
    .restart local v295       #patchOatUpd1:Z
    .restart local v299       #posit:I
    .restart local v331       #uni:Z
    :cond_32
    :try_start_4
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v10, v148, 0x1

    invoke-virtual {v9, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_7

    .line 1178
    .end local v148           #curentPos:I
    .end local v149           #curentByte:B
    :catch_2
    move-exception v240

    .line 1179
    .restart local v240       #e:Ljava/lang/Exception;
    :try_start_5
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v240

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_8

    .line 1187
    .end local v147           #ChannelDex:Ljava/nio/channels/FileChannel;
    .end local v240           #e:Ljava/lang/Exception;
    .end local v241           #fileForPatch:Ljava/io/File;
    .end local v284           #p11:Z
    .end local v285           #p12:Z
    .end local v286           #p2:Z
    .end local v299           #posit:I
    .end local v331           #uni:Z
    :cond_33
    move/from16 v0, v295

    if-nez v0, :cond_34

    move/from16 v0, v289

    if-nez v0, :cond_34

    move/from16 v0, v290

    if-nez v0, :cond_34

    move/from16 v0, v291

    if-eqz v0, :cond_35

    .line 1188
    :cond_34
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1189
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@boot.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1190
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1191
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@boot.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1192
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1193
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@boot.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1194
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/reboot"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1204
    .end local v289           #patchOat1:Z
    .end local v290           #patchOat2:Z
    .end local v291           #patchOat3:Z
    .end local v292           #patchOat4:Z
    .end local v295           #patchOatUpd1:Z
    :cond_35
    const/16 v251, 0x0

    .line 1205
    .local v251, localFile2:Ljava/io/File;
    const/4 v9, 0x4

    :try_start_6
    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "framework"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_77

    .line 1206
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "core.jar"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_36

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "core-libart.jar"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_37

    .line 1207
    :cond_36
    new-instance v9, Ljava/io/File;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v9

    if-eqz v9, :cond_37

    .line 1208
    new-instance v0, Ljava/io/File;

    move-object/16 v329, v0

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    move-object/from16 v0, v329

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1209
    .local v329, serjar:Ljava/io/File;
    invoke-static/range {v329 .. v329}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v329

    invoke-static {v0, v9}, Lcom/chelpus/root/utils/corepatch;->unzip(Ljava/io/File;Ljava/lang/String;)V

    .line 1210
    new-instance v232, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v329 .. v329}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v232

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1211
    .local v232, clasdex:Ljava/io/File;
    invoke-virtual/range {v232 .. v232}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_37

    .line 1212
    move-object/from16 v0, v243

    move-object/from16 v1, v232

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1217
    .end local v232           #clasdex:Ljava/io/File;
    .end local v329           #serjar:Ljava/io/File;
    :cond_37
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "core.odex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_38

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "boot.oat"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_39

    .line 1218
    :cond_38
    new-instance v9, Ljava/io/File;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v243

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1253
    :cond_39
    :goto_9
    invoke-virtual/range {v243 .. v243}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v64

    :cond_3a
    :goto_a
    invoke-interface/range {v64 .. v64}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_79

    invoke-interface/range {v64 .. v64}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v245

    check-cast v245, Ljava/io/File;

    .line 1255
    .local v245, fp:Ljava/io/File;
    move-object/from16 v251, v245

    .line 1257
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "file for patch: "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v15, " size:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->length()J

    move-result-wide v15

    move-wide v0, v15

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1259
    new-instance v9, Ljava/io/RandomAccessFile;

    const-string v10, "rw"

    move-object/from16 v0, v251

    invoke-direct {v9, v0, v10}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v147

    .line 1260
    .restart local v147       #ChannelDex:Ljava/nio/channels/FileChannel;
    sget-object v156, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v157, 0x0

    invoke-virtual/range {v147 .. v147}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v9

    long-to-int v9, v9

    int-to-long v0, v9

    move-wide/from16 v159, v0

    move-object/from16 v155, v147

    invoke-virtual/range {v155 .. v160}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v9

    sput-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    .line 1262
    const/4 v0, 0x0

    move/16 v287, v0

    .line 1267
    .local v287, patch1:Z
    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "boot.oat"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_7

    move-result v9

    if-nez v9, :cond_dc

    .line 1270
    const-wide/16 v249, 0x0

    .local v249, j:J
    :goto_b
    :try_start_7
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v9

    if-eqz v9, :cond_d9

    .line 1272
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v148

    .line 1273
    .restart local v148       #curentPos:I
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v149

    .restart local v149       #curentByte:B
    move-object/from16 v150, v182

    move-object/from16 v151, v264

    move-object/from16 v152, v212

    move-object/from16 v153, v310

    .line 1275
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_3d

    .line 1276
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3b

    .line 1277
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1279
    :cond_3b
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3c

    .line 1280
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1282
    :cond_3c
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_3d
    move-object/from16 v150, v185

    move-object/from16 v151, v264

    move-object/from16 v152, v215

    move-object/from16 v153, v310

    .line 1284
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_40

    .line 1285
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3e

    .line 1286
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1288
    :cond_3e
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3f

    .line 1289
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1291
    :cond_3f
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_40
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v186

    move-object/from16 v158, v267

    move-object/from16 v159, v216

    move-object/from16 v160, v313

    move/from16 v161, v63

    .line 1293
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_43

    .line 1294
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_41

    .line 1295
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1297
    :cond_41
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_42

    .line 1298
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1300
    :cond_42
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_43
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v187

    move-object/from16 v158, v268

    move-object/from16 v159, v217

    move-object/from16 v160, v314

    move/from16 v161, v63

    .line 1302
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_46

    .line 1303
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_44

    .line 1304
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1306
    :cond_44
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_45

    .line 1307
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1309
    :cond_45
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_46
    move-object/from16 v150, v188

    move-object/from16 v151, v269

    move-object/from16 v152, v218

    move-object/from16 v153, v315

    .line 1311
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_49

    .line 1312
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_47

    .line 1313
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1315
    :cond_47
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_48

    .line 1316
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1318
    :cond_48
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_49
    move-object/from16 v150, v189

    move-object/from16 v151, v270

    move-object/from16 v152, v219

    move-object/from16 v153, v316

    .line 1320
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_4c

    .line 1321
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4a

    .line 1322
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1324
    :cond_4a
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4b

    .line 1325
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1327
    :cond_4b
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_4c
    move-object/from16 v150, v190

    move-object/from16 v151, v271

    move-object/from16 v152, v220

    move-object/from16 v153, v317

    .line 1329
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_4f

    .line 1330
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4d

    .line 1331
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1333
    :cond_4d
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4e

    .line 1334
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1336
    :cond_4e
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_4f
    move-object/from16 v150, v191

    move-object/from16 v151, v272

    move-object/from16 v152, v221

    move-object/from16 v153, v318

    .line 1338
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_52

    .line 1339
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_50

    .line 1340
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1342
    :cond_50
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_51

    .line 1343
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1345
    :cond_51
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_52
    move-object/from16 v150, v172

    move-object/from16 v151, v254

    move-object/from16 v152, v202

    move-object/from16 v153, v300

    .line 1347
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_55

    .line 1348
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_53

    .line 1349
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1351
    :cond_53
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_54

    .line 1352
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1354
    :cond_54
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_55
    move-object/from16 v150, v173

    move-object/from16 v151, v255

    move-object/from16 v152, v203

    move-object/from16 v153, v301

    .line 1356
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_58

    .line 1357
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_56

    .line 1358
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1360
    :cond_56
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_57

    .line 1361
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1363
    :cond_57
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_58
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v174

    move-object/from16 v158, v256

    move-object/from16 v159, v204

    move-object/from16 v160, v302

    move/from16 v161, v63

    .line 1365
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_5b

    .line 1366
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_59

    .line 1367
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1369
    :cond_59
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5a

    .line 1370
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1372
    :cond_5a
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_5b
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v175

    move-object/from16 v158, v257

    move-object/from16 v159, v205

    move-object/from16 v160, v303

    move/from16 v161, v63

    .line 1374
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_5e

    .line 1375
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5c

    .line 1376
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1378
    :cond_5c
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5d

    .line 1379
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1381
    :cond_5d
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_5e
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v183

    move-object/from16 v158, v265

    move-object/from16 v159, v213

    move-object/from16 v160, v311

    move/from16 v161, v63

    .line 1383
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_61

    .line 1384
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5f

    .line 1385
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1387
    :cond_5f
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_60

    .line 1388
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1390
    :cond_60
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_61
    move-object/from16 v150, v184

    move-object/from16 v151, v266

    move-object/from16 v152, v214

    move-object/from16 v153, v312

    .line 1392
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_64

    .line 1393
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_62

    .line 1394
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1396
    :cond_62
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_63

    .line 1397
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1399
    :cond_63
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_64
    move-object/from16 v150, v176

    move-object/from16 v151, v258

    move-object/from16 v152, v206

    move-object/from16 v153, v304

    .line 1401
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_67

    .line 1402
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_65

    .line 1403
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1405
    :cond_65
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_66

    .line 1406
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1408
    :cond_66
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_67
    move-object/from16 v150, v177

    move-object/from16 v151, v259

    move-object/from16 v152, v207

    move-object/from16 v153, v305

    .line 1410
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_6a

    .line 1411
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_68

    .line 1412
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1414
    :cond_68
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_69

    .line 1415
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1417
    :cond_69
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_6a
    move-object/from16 v150, v178

    move-object/from16 v151, v260

    move-object/from16 v152, v208

    move-object/from16 v153, v306

    .line 1419
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_6d

    .line 1420
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6b

    .line 1421
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1423
    :cond_6b
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6c

    .line 1424
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1426
    :cond_6c
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_6d
    move-object/from16 v150, v179

    move-object/from16 v151, v261

    move-object/from16 v152, v209

    move-object/from16 v153, v307

    .line 1428
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_70

    .line 1429
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6e

    .line 1430
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1432
    :cond_6e
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6f

    .line 1433
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1435
    :cond_6f
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_70
    move-object/from16 v150, v180

    move-object/from16 v151, v262

    move-object/from16 v152, v210

    move-object/from16 v153, v308

    .line 1437
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_73

    .line 1438
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_71

    .line 1439
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1441
    :cond_71
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_72

    .line 1442
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1444
    :cond_72
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_73
    move-object/from16 v150, v181

    move-object/from16 v151, v263

    move-object/from16 v152, v211

    move-object/from16 v153, v309

    .line 1446
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_76

    .line 1447
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_74

    .line 1448
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1450
    :cond_74
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_75

    .line 1451
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1453
    :cond_75
    const/4 v0, 0x1

    move/16 v287, v0

    .line 1456
    :cond_76
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v10, v148, 0x1

    invoke-virtual {v9, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_3

    .line 1270
    const-wide/16 v9, 0x1

    add-long v249, v249, v9

    goto/16 :goto_b

    .line 1221
    .end local v147           #ChannelDex:Ljava/nio/channels/FileChannel;
    .end local v148           #curentPos:I
    .end local v149           #curentByte:B
    .end local v245           #fp:Ljava/io/File;
    .end local v249           #j:J
    .end local v287           #patch1:Z
    :cond_77
    const/4 v9, 0x4

    :try_start_8
    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "ART"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_d4

    .line 1222
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_78

    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v9

    const-wide/16 v15, 0x0

    cmp-long v9, v9, v15

    if-eqz v9, :cond_78

    .line 1223
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v243

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1225
    :cond_78
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.jar"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v9

    if-eqz v9, :cond_39

    .line 1226
    new-instance v0, Ljava/io/File;

    move-object/16 v329, v0

    const-string v9, "/system/framework/core-libart.jar"

    move-object/from16 v0, v329

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1227
    .restart local v329       #serjar:Ljava/io/File;
    const-string v9, "/data/app"

    move-object/from16 v0, v329

    invoke-static {v0, v9}, Lcom/chelpus/root/utils/corepatch;->unzip(Ljava/io/File;Ljava/lang/String;)V

    .line 1228
    new-instance v232, Ljava/io/File;

    const-string v9, "/data/app/classes.dex"

    move-object/from16 v0, v232

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1229
    .restart local v232       #clasdex:Ljava/io/File;
    invoke-virtual/range {v232 .. v232}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_39

    .line 1230
    move-object/from16 v0, v243

    move-object/from16 v1, v232

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7

    goto/16 :goto_9

    .line 1740
    .end local v232           #clasdex:Ljava/io/File;
    .end local v329           #serjar:Ljava/io/File;
    :catch_3
    move-exception v253

    .line 1741
    .local v253, localFileNotFoundException:Ljava/io/FileNotFoundException;
    :goto_c
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Error: core.odex not found!\n\nPlease Odex core.jar and try again!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1748
    .end local v251           #localFile2:Ljava/io/File;
    .end local v253           #localFileNotFoundException:Ljava/io/FileNotFoundException;
    .end local v283           #oatForPatch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    :cond_79
    :goto_d
    if-nez v75, :cond_7a

    if-eqz v87, :cond_10f

    .line 1749
    :cond_7a
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Start patch for services.jar"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1750
    const-string v247, ""

    .line 1751
    .local v247, indexDir:Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_7b

    const-string v247, "/arm"

    .line 1752
    :cond_7b
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm64/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_7c

    const-string v247, "/arm64"

    .line 1753
    :cond_7c
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/x86/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_7d

    const-string v247, "/x86"

    .line 1754
    :cond_7d
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm/services.odex.xz"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_7e

    const-string v247, "/arm"

    .line 1755
    :cond_7e
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm64/services.odex.xz"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_7f

    const-string v247, "/arm64"

    .line 1756
    :cond_7f
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/x86/services.odex.xz"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_80

    const-string v247, "/x86"

    .line 1757
    :cond_80
    const-string v9, ""

    move-object/from16 v0, v247

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_ab

    sget v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->api:I

    const/16 v10, 0x15

    if-lt v9, v10, :cond_ab

    .line 1760
    const/16 v147, 0x0

    .line 1761
    .restart local v147       #ChannelDex:Ljava/nio/channels/FileChannel;
    const/4 v0, 0x0

    move/16 v289, v0

    .restart local v289       #patchOat1:Z
    const/4 v0, 0x0

    move/16 v290, v0

    .restart local v290       #patchOat2:Z
    const/4 v0, 0x0

    move/16 v291, v0

    .restart local v291       #patchOat3:Z
    const/4 v0, 0x0

    move/16 v292, v0

    .restart local v292       #patchOat4:Z
    const/4 v0, 0x0

    move/16 v293, v0

    .local v293, patchOat5:Z
    const/4 v0, 0x0

    move/16 v294, v0

    .line 1762
    .local v294, patchOat6:Z
    new-instance v244, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "/system/framework"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v247

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/services.odex"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v244

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1763
    .local v244, for_patch:Ljava/io/File;
    const/16 v246, 0x1

    .line 1764
    .local v246, good_odex:Z
    new-instance v9, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "/system/framework"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v247

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v15, "/services.odex.xz"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_81

    .line 1766
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "try unpack services.odex.xz"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1767
    new-instance v9, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "/system/framework"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v247

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v15, "/services.odex.xz"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "/system/framework"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v247

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/chelpus/Utils;->XZDecompress(Ljava/io/File;Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_10a

    .line 1768
    const/16 v246, 0x0

    .line 1769
    new-instance v9, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "/system/framework"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v247

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v15, "/services.odex"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1770
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "not enought space for unpack services.odex.xz"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1777
    :cond_81
    :goto_e
    if-eqz v246, :cond_10c

    .line 1779
    :try_start_9
    new-instance v9, Ljava/io/RandomAccessFile;

    const-string v10, "rw"

    move-object/from16 v0, v244

    invoke-direct {v9, v0, v10}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v147

    .line 1780
    sget-object v156, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v157, 0x0

    invoke-virtual/range {v147 .. v147}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v9

    long-to-int v9, v9

    int-to-long v0, v9

    move-wide/from16 v159, v0

    move-object/from16 v155, v147

    invoke-virtual/range {v155 .. v160}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v9

    sput-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    .line 1782
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    const/16 v10, 0x1018

    invoke-virtual {v9, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1783
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v9

    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v10

    sget-object v15, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v15}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v15

    sget-object v16, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v16

    move/from16 v0, v16

    invoke-static {v9, v10, v15, v0}, Lcom/chelpus/Utils;->convertFourBytesToInt(BBBB)I

    move-result v0

    move/16 v299, v0

    .line 1784
    .restart local v299       #posit:I
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Start position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v299

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1785
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    move/from16 v0, v299

    invoke-virtual {v9, v0}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_c

    .line 1788
    :goto_f
    :try_start_a
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v9

    if-eqz v9, :cond_a9

    .line 1790
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v148

    .line 1791
    .restart local v148       #curentPos:I
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v149

    .restart local v149       #curentByte:B
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v65

    move-object/from16 v158, v66

    move-object/from16 v159, v67

    move-object/from16 v160, v68

    move/from16 v161, v75

    .line 1792
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_84

    .line 1793
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_82

    .line 1794
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1796
    :cond_82
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_83

    .line 1797
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1799
    :cond_83
    const/4 v0, 0x1

    move/16 v289, v0

    :cond_84
    move/from16 v69, v148

    move/from16 v70, v149

    .line 1801
    invoke-static/range {v69 .. v75}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_87

    .line 1802
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_85

    .line 1803
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1805
    :cond_85
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_86

    .line 1806
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1808
    :cond_86
    const/4 v0, 0x1

    move/16 v290, v0

    :cond_87
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v77

    move-object/from16 v158, v78

    move-object/from16 v159, v79

    move-object/from16 v160, v80

    move/from16 v161, v75

    .line 1810
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_8a

    .line 1811
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_88

    .line 1812
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!checkUpgradeKeySetLP\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1814
    :cond_88
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_89

    .line 1815
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!checkUpgradeKeySetLP\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1817
    :cond_89
    const/4 v0, 0x1

    move/16 v291, v0

    :cond_8a
    move/from16 v81, v148

    move/from16 v82, v149

    .line 1819
    invoke-static/range {v81 .. v87}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_8d

    .line 1820
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_8b

    .line 1821
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!queryIntentServices\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1823
    :cond_8b
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_8c

    .line 1824
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!queryIntentServices\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1826
    :cond_8c
    const/4 v0, 0x1

    move/16 v292, v0

    :cond_8d
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v89

    move-object/from16 v158, v90

    move-object/from16 v159, v91

    move-object/from16 v160, v92

    move/from16 v161, v87

    .line 1828
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_90

    .line 1829
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_8e

    .line 1830
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!buildResolveList\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1832
    :cond_8e
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_8f

    .line 1833
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!buildResolveList\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1835
    :cond_8f
    const/4 v0, 0x1

    move/16 v293, v0

    :cond_90
    move/from16 v93, v148

    move/from16 v94, v149

    move/from16 v99, v75

    .line 1837
    invoke-static/range {v93 .. v99}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_93

    .line 1838
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_91

    .line 1839
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!FixForCM\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1841
    :cond_91
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_92

    .line 1842
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!FixForCM\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1844
    :cond_92
    const/4 v0, 0x1

    move/16 v294, v0

    :cond_93
    move/from16 v99, v148

    move/from16 v100, v149

    move/from16 v105, v75

    .line 1846
    invoke-static/range {v99 .. v105}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_96

    .line 1847
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_94

    .line 1848
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!FixForCM\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1850
    :cond_94
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_95

    .line 1851
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!FixForCM\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1853
    :cond_95
    const/4 v0, 0x1

    move/16 v294, v0

    :cond_96
    move/from16 v105, v148

    move/from16 v106, v149

    move/from16 v111, v75

    .line 1855
    invoke-static/range {v105 .. v111}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_99

    .line 1856
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_97

    .line 1857
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1859
    :cond_97
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_98

    .line 1860
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1862
    :cond_98
    const/4 v0, 0x1

    move/16 v289, v0

    :cond_99
    move/from16 v111, v148

    move/from16 v112, v149

    move/from16 v117, v75

    .line 1864
    invoke-static/range {v111 .. v117}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_9c

    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_9a

    .line 1865
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1867
    :cond_9a
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_9b

    .line 1868
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1870
    :cond_9b
    const/4 v0, 0x1

    move/16 v290, v0

    :cond_9c
    move/from16 v129, v148

    move/from16 v130, v149

    move/from16 v135, v75

    .line 1872
    invoke-static/range {v129 .. v135}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_9f

    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_9d

    .line 1873
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1875
    :cond_9d
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_9e

    .line 1876
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1878
    :cond_9e
    const/4 v0, 0x1

    move/16 v290, v0

    :cond_9f
    move/from16 v135, v148

    move/from16 v136, v149

    move/from16 v141, v75

    .line 1880
    invoke-static/range {v135 .. v141}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_a2

    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_a0

    .line 1881
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1883
    :cond_a0
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_a1

    .line 1884
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1886
    :cond_a1
    const/4 v0, 0x1

    move/16 v290, v0

    :cond_a2
    move/from16 v117, v148

    move/from16 v118, v149

    move/from16 v123, v75

    .line 1888
    invoke-static/range {v117 .. v123}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_a5

    .line 1889
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_a3

    .line 1890
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1892
    :cond_a3
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_a4

    .line 1893
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1895
    :cond_a4
    const/4 v0, 0x1

    move/16 v289, v0

    :cond_a5
    move/from16 v123, v148

    move/from16 v124, v149

    move/from16 v129, v75

    .line 1897
    invoke-static/range {v123 .. v129}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_a8

    .line 1898
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_a6

    .line 1899
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1901
    :cond_a6
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_a7

    .line 1902
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1904
    :cond_a7
    const/4 v0, 0x1

    move/16 v289, v0

    .line 1907
    :cond_a8
    if-eqz v75, :cond_10b

    move/from16 v0, v289

    if-eqz v0, :cond_10b

    move/from16 v0, v290

    if-eqz v0, :cond_10b

    move/from16 v0, v291

    if-eqz v0, :cond_10b

    move/from16 v0, v292

    if-eqz v0, :cond_10b

    move/from16 v0, v293

    if-eqz v0, :cond_10b

    move/from16 v0, v294

    if-eqz v0, :cond_10b

    .line 1908
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@services.jar@classes.dex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1909
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@services.jar@classes.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1910
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@services.jar@classes.dex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1911
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@services.jar@classes.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1912
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@services.jar@classes.dex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1913
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@services.jar@classes.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1914
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/reboot"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_b
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_c

    .line 1923
    .end local v148           #curentPos:I
    .end local v149           #curentByte:B
    :cond_a9
    :goto_10
    :try_start_b
    invoke-virtual/range {v147 .. v147}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_c

    .line 1930
    .end local v299           #posit:I
    :goto_11
    move/from16 v0, v289

    if-nez v0, :cond_aa

    move/from16 v0, v290

    if-nez v0, :cond_aa

    move/from16 v0, v291

    if-nez v0, :cond_aa

    move/from16 v0, v292

    if-nez v0, :cond_aa

    move/from16 v0, v293

    if-nez v0, :cond_aa

    move/from16 v0, v294

    if-eqz v0, :cond_ab

    .line 1931
    :cond_aa
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@services.jar@classes.dex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1932
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@services.jar@classes.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1933
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@services.jar@classes.dex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1934
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@services.jar@classes.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1935
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@services.jar@classes.dex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1936
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@services.jar@classes.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1937
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/reboot"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1947
    .end local v147           #ChannelDex:Ljava/nio/channels/FileChannel;
    .end local v244           #for_patch:Ljava/io/File;
    .end local v246           #good_odex:Z
    .end local v289           #patchOat1:Z
    .end local v290           #patchOat2:Z
    .end local v291           #patchOat3:Z
    .end local v292           #patchOat4:Z
    .end local v293           #patchOat5:Z
    .end local v294           #patchOat6:Z
    :cond_ab
    const/16 v251, 0x0

    .line 1948
    .restart local v251       #localFile2:Ljava/io/File;
    const/4 v9, 0x4

    :try_start_c
    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "framework"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_10d

    .line 1949
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "services.jar"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_ac

    .line 1950
    new-instance v9, Ljava/io/File;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v9

    if-eqz v9, :cond_ac

    .line 1951
    new-instance v0, Ljava/io/File;

    move-object/16 v329, v0

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    move-object/from16 v0, v329

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1952
    .restart local v329       #serjar:Ljava/io/File;
    invoke-static/range {v329 .. v329}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v329

    invoke-static {v0, v9}, Lcom/chelpus/root/utils/corepatch;->unzip(Ljava/io/File;Ljava/lang/String;)V

    .line 1953
    new-instance v232, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v329 .. v329}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v232

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1954
    .restart local v232       #clasdex:Ljava/io/File;
    invoke-virtual/range {v232 .. v232}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_ac

    .line 1955
    move-object/from16 v0, v243

    move-object/from16 v1, v232

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1960
    .end local v232           #clasdex:Ljava/io/File;
    .end local v329           #serjar:Ljava/io/File;
    :cond_ac
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "services.odex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_ad

    .line 1961
    new-instance v9, Ljava/io/File;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v243

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2007
    :cond_ad
    :goto_12
    invoke-virtual/range {v243 .. v243}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_ae
    :goto_13
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_10f

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v245

    check-cast v245, Ljava/io/File;

    .line 2008
    .restart local v245       #fp:Ljava/io/File;
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Start patch for "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {v245 .. v245}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2009
    move-object/from16 v251, v245

    .line 2010
    const/16 v248, 0x0

    .line 2011
    .local v248, isOat:Z
    invoke-static/range {v251 .. v251}, Lcom/chelpus/Utils;->isELFfiles(Ljava/io/File;)Z

    move-result v10

    if-eqz v10, :cond_af

    const/16 v248, 0x1

    .line 2013
    :cond_af
    new-instance v10, Ljava/io/RandomAccessFile;

    const-string v15, "rw"

    move-object/from16 v0, v251

    invoke-direct {v10, v0, v15}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v147

    .line 2014
    .restart local v147       #ChannelDex:Ljava/nio/channels/FileChannel;
    sget-object v156, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v157, 0x0

    invoke-virtual/range {v147 .. v147}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v15

    long-to-int v10, v15

    int-to-long v0, v10

    move-wide/from16 v159, v0

    move-object/from16 v155, v147

    invoke-virtual/range {v155 .. v160}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v10

    sput-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    .line 2016
    const/4 v0, 0x0

    move/16 v288, v0

    .line 2018
    .local v288, patch4:Z
    const/16 v236, 0x0

    .local v236, counter:I
    const/16 v169, 0x0

    .line 2019
    .local v169, counter2:I
    if-nez v248, :cond_119

    .line 2020
    const/4 v10, 0x0

    sput-object v10, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    .line 2021
    const/4 v10, 0x0

    sput v10, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I
    :try_end_c
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_d
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_10

    .line 2025
    const-wide/16 v249, 0x0

    .restart local v249       #j:J
    :goto_14
    :try_start_d
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v10

    if-eqz v10, :cond_115

    .line 2027
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v148

    .line 2028
    .restart local v148       #curentPos:I
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v149

    .restart local v149       #curentByte:B
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v195

    move-object/from16 v158, v276

    move-object/from16 v159, v225

    move-object/from16 v160, v322

    move/from16 v161, v75

    .line 2029
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_b2

    .line 2030
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_b0

    .line 2031
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2033
    :cond_b0
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_b1

    .line 2034
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2036
    :cond_b1
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_b2
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v198

    move-object/from16 v158, v279

    move-object/from16 v159, v228

    move-object/from16 v160, v325

    move/from16 v161, v75

    .line 2038
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_b5

    .line 2039
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_b3

    .line 2040
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2042
    :cond_b3
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_b4

    .line 2043
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2045
    :cond_b4
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_b5
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v199

    move-object/from16 v158, v280

    move-object/from16 v159, v229

    move-object/from16 v160, v326

    move/from16 v161, v75

    .line 2047
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_b8

    .line 2048
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_b6

    .line 2049
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!checkUpgradeKeySetLP\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2051
    :cond_b6
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_b7

    .line 2052
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!checkUpgradeKeySetLP\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2054
    :cond_b7
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_b8
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v200

    move-object/from16 v158, v281

    move-object/from16 v159, v230

    move-object/from16 v160, v327

    move/from16 v161, v75

    .line 2056
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_bb

    .line 2057
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_b9

    .line 2058
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nCM12"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2060
    :cond_b9
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_ba

    .line 2061
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nCM12"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2063
    :cond_ba
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_bb
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v201

    move-object/from16 v158, v282

    move-object/from16 v159, v231

    move-object/from16 v160, v328

    move/from16 v161, v236

    move/from16 v163, v87

    .line 2065
    invoke-static/range {v155 .. v163}, Lcom/chelpus/root/utils/corepatch;->applyPatchCounter(IB[B[B[B[BIIZ)Z

    move-result v10

    if-eqz v10, :cond_be

    .line 2066
    add-int/lit8 v236, v236, 0x1

    .line 2067
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_bc

    .line 2068
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2070
    :cond_bc
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_bd

    .line 2071
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2073
    :cond_bd
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_be
    move/from16 v163, v148

    move/from16 v164, v149

    move/from16 v171, v87

    .line 2075
    invoke-static/range {v163 .. v171}, Lcom/chelpus/root/utils/corepatch;->applyPatchCounter(IB[B[B[B[BIIZ)Z

    move-result v10

    if-eqz v10, :cond_c1

    .line 2076
    add-int/lit8 v169, v169, 0x1

    .line 2077
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_bf

    .line 2078
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2080
    :cond_bf
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_c0

    .line 2081
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2083
    :cond_c0
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_c1
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v192

    move-object/from16 v158, v273

    move-object/from16 v159, v222

    move-object/from16 v160, v319

    move/from16 v161, v87

    .line 2085
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_c4

    .line 2086
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_c2

    .line 2087
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2089
    :cond_c2
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_c3

    .line 2090
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2092
    :cond_c3
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_c4
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v193

    move-object/from16 v158, v274

    move-object/from16 v159, v223

    move-object/from16 v160, v320

    move/from16 v161, v87

    .line 2094
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_c7

    .line 2095
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_c5

    .line 2096
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2098
    :cond_c5
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_c6

    .line 2099
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2101
    :cond_c6
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_c7
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v194

    move-object/from16 v158, v275

    move-object/from16 v159, v224

    move-object/from16 v160, v321

    move/from16 v161, v87

    .line 2103
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_ca

    .line 2104
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_c8

    .line 2105
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2107
    :cond_c8
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_c9

    .line 2108
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2110
    :cond_c9
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_ca
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v196

    move-object/from16 v158, v277

    move-object/from16 v159, v226

    move-object/from16 v160, v323

    move/from16 v161, v75

    .line 2113
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_cd

    .line 2114
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_cb

    .line 2115
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nCM11"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2117
    :cond_cb
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_cc

    .line 2118
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nCM11"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2120
    :cond_cc
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_cd
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v197

    move-object/from16 v158, v278

    move-object/from16 v159, v227

    move-object/from16 v160, v324

    move/from16 v161, v75

    .line 2122
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_d0

    .line 2123
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_ce

    .line 2124
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2126
    :cond_ce
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_cf

    .line 2127
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2129
    :cond_cf
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_d0
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v143

    move-object/from16 v158, v144

    move-object/from16 v159, v145

    move-object/from16 v160, v146

    move/from16 v161, v75

    .line 2131
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_d3

    .line 2132
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_d1

    .line 2133
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2135
    :cond_d1
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_d2

    .line 2136
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2138
    :cond_d2
    const/4 v0, 0x1

    move/16 v288, v0

    .line 2141
    :cond_d3
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v15, v148, 0x1

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_11
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_d

    .line 2025
    const-wide/16 v15, 0x1

    add-long v249, v249, v15

    goto/16 :goto_14

    .line 1235
    .end local v147           #ChannelDex:Ljava/nio/channels/FileChannel;
    .end local v148           #curentPos:I
    .end local v149           #curentByte:B
    .end local v169           #counter2:I
    .end local v236           #counter:I
    .end local v245           #fp:Ljava/io/File;
    .end local v247           #indexDir:Ljava/lang/String;
    .end local v248           #isOat:Z
    .end local v249           #j:J
    .end local v288           #patch4:Z
    .restart local v283       #oatForPatch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    :cond_d4
    :try_start_e
    sget-boolean v9, Lcom/chelpus/root/utils/corepatch;->onlyDalvik:Z

    if-eqz v9, :cond_d6

    .line 1236
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "OnlyDalvik: add for patch "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v15, 0x1

    move-object/from16 v0, p0

    aget-object v15, v0, v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1237
    new-instance v252, Ljava/io/File;

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    move-object/from16 v0, v252

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_e} :catch_3
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_7

    .line 1238
    .end local v251           #localFile2:Ljava/io/File;
    .local v252, localFile2:Ljava/io/File;
    :try_start_f
    invoke-virtual/range {v252 .. v252}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_d5

    new-instance v9, Ljava/io/FileNotFoundException;

    invoke-direct {v9}, Ljava/io/FileNotFoundException;-><init>()V

    throw v9

    .line 1740
    :catch_4
    move-exception v253

    move-object/from16 v251, v252

    .end local v252           #localFile2:Ljava/io/File;
    .restart local v251       #localFile2:Ljava/io/File;
    goto/16 :goto_c

    .line 1239
    .end local v251           #localFile2:Ljava/io/File;
    .restart local v252       #localFile2:Ljava/io/File;
    :cond_d5
    move-object/from16 v0, v243

    move-object/from16 v1, v252

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_f
    .catch Ljava/io/FileNotFoundException; {:try_start_f .. :try_end_f} :catch_4
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_5

    move-object/from16 v251, v252

    .end local v252           #localFile2:Ljava/io/File;
    .restart local v251       #localFile2:Ljava/io/File;
    goto/16 :goto_9

    .line 1241
    :cond_d6
    :try_start_10
    new-instance v252, Ljava/io/File;

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    move-object/from16 v0, v252

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_10} :catch_3
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_7

    .line 1242
    .end local v251           #localFile2:Ljava/io/File;
    .restart local v252       #localFile2:Ljava/io/File;
    :try_start_11
    invoke-virtual/range {v252 .. v252}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_d7

    new-instance v9, Ljava/io/FileNotFoundException;

    invoke-direct {v9}, Ljava/io/FileNotFoundException;-><init>()V

    throw v9
    :try_end_11
    .catch Ljava/io/FileNotFoundException; {:try_start_11 .. :try_end_11} :catch_4
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_5

    .line 1743
    :catch_5
    move-exception v240

    move-object/from16 v251, v252

    .line 1744
    .end local v252           #localFile2:Ljava/io/File;
    .restart local v240       #e:Ljava/lang/Exception;
    .restart local v251       #localFile2:Ljava/io/File;
    :goto_15
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception e"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v240 .. v240}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_d

    .line 1243
    .end local v240           #e:Ljava/lang/Exception;
    .end local v251           #localFile2:Ljava/io/File;
    .restart local v252       #localFile2:Ljava/io/File;
    :cond_d7
    :try_start_12
    invoke-virtual/range {v252 .. v252}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "system@framework@core.jar@classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_d8

    .line 1244
    new-instance v234, Ljava/io/File;

    const-string v9, "/system/framework/core.odex"

    move-object/from16 v0, v234

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1245
    .local v234, coreodex:Ljava/io/File;
    invoke-virtual/range {v234 .. v234}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_d8

    .line 1246
    invoke-virtual/range {v234 .. v234}, Ljava/io/File;->length()J

    move-result-wide v9

    const-wide/16 v15, 0x0

    cmp-long v9, v9, v15

    if-nez v9, :cond_d8

    invoke-virtual/range {v234 .. v234}, Ljava/io/File;->delete()Z

    .line 1249
    .end local v234           #coreodex:Ljava/io/File;
    :cond_d8
    move-object/from16 v0, v243

    move-object/from16 v1, v252

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_12
    .catch Ljava/io/FileNotFoundException; {:try_start_12 .. :try_end_12} :catch_4
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_5

    move-object/from16 v251, v252

    .end local v252           #localFile2:Ljava/io/File;
    .restart local v251       #localFile2:Ljava/io/File;
    goto/16 :goto_9

    .line 1459
    .restart local v147       #ChannelDex:Ljava/nio/channels/FileChannel;
    .restart local v245       #fp:Ljava/io/File;
    .restart local v249       #j:J
    .restart local v287       #patch1:Z
    :catch_6
    move-exception v240

    .line 1460
    .restart local v240       #e:Ljava/lang/Exception;
    :try_start_13
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v240

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1602
    .end local v240           #e:Ljava/lang/Exception;
    .end local v249           #j:J
    :cond_d9
    :goto_16
    invoke-virtual/range {v147 .. v147}, Ljava/nio/channels/FileChannel;->close()V

    .line 1604
    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "framework"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_100

    .line 1605
    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_da

    .line 1606
    const/4 v9, 0x0

    sput-boolean v9, Lcom/chelpus/root/utils/corepatch;->not_found_bytes_for_patch:Z

    .line 1607
    invoke-static/range {v251 .. v251}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 1608
    move/from16 v0, v287

    if-eqz v0, :cond_ff

    .line 1609
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, ".jar"

    const-string v15, "-patched.jar"

    invoke-virtual {v9, v10, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v239

    .line 1610
    .local v239, destination:Ljava/lang/String;
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v0, v0, v9

    move-object/16 v330, v0

    .line 1611
    .local v330, source:Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, v239

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1612
    new-instance v242, Ljava/util/ArrayList;

    invoke-direct/range {v242 .. v242}, Ljava/util/ArrayList;-><init>()V

    .line 1613
    .local v242, files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "add files"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1614
    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v251 .. v251}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v9, v10, v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v242

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_13
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_13} :catch_3
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_7

    .line 1616
    :try_start_14
    move-object/from16 v0, v330

    move-object/from16 v1, v239

    move-object/from16 v2, v242

    invoke-static {v0, v1, v2}, Lcom/chelpus/Utils;->addFilesToZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1617
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "add files finish"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1618
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, v330

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_9
    .catch Ljava/io/FileNotFoundException; {:try_start_14 .. :try_end_14} :catch_3

    .line 1629
    .end local v239           #destination:Ljava/lang/String;
    .end local v242           #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v330           #source:Ljava/lang/String;
    :cond_da
    :goto_17
    :try_start_15
    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/core.odex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_db

    .line 1630
    const/4 v9, 0x0

    move-object/from16 v0, v251

    invoke-static {v0, v9}, Lcom/chelpus/Utils;->fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V

    .line 1631
    move/from16 v0, v287

    if-eqz v0, :cond_db

    new-instance v9, Ljava/io/File;

    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const-string v15, "/core.odex"

    const-string v16, "/core-patched.odex"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v251

    invoke-virtual {v0, v9}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1633
    :cond_db
    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/boot.oat"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3a

    .line 1634
    move/from16 v0, v287

    if-eqz v0, :cond_3a

    new-instance v9, Ljava/io/File;

    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const-string v15, "/boot.oat"

    const-string v16, "/boot-patched.oat"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v251

    invoke-virtual {v0, v9}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto/16 :goto_a

    .line 1743
    .end local v147           #ChannelDex:Ljava/nio/channels/FileChannel;
    .end local v245           #fp:Ljava/io/File;
    .end local v287           #patch1:Z
    :catch_7
    move-exception v240

    goto/16 :goto_15

    .line 1464
    .restart local v147       #ChannelDex:Ljava/nio/channels/FileChannel;
    .restart local v245       #fp:Ljava/io/File;
    .restart local v287       #patch1:Z
    :cond_dc
    const/4 v0, 0x0

    move/16 v295, v0

    .restart local v295       #patchOatUpd1:Z
    const/4 v0, 0x0

    move/16 v289, v0

    .restart local v289       #patchOat1:Z
    const/4 v0, 0x0

    move/16 v290, v0

    .restart local v290       #patchOat2:Z
    const/4 v0, 0x0

    move/16 v291, v0

    .restart local v291       #patchOat3:Z
    const/4 v0, 0x0

    move/16 v292, v0

    .line 1465
    .restart local v292       #patchOat4:Z
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    const/16 v10, 0x1018

    invoke-virtual {v9, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1466
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v9

    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v10

    sget-object v15, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v15}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v15

    sget-object v16, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v16

    move/from16 v0, v16

    invoke-static {v9, v10, v15, v0}, Lcom/chelpus/Utils;->convertFourBytesToInt(BBBB)I

    move-result v0

    move/16 v299, v0

    .line 1467
    .restart local v299       #posit:I
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Start position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v299

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1468
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    move/from16 v0, v299

    invoke-virtual {v9, v0}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_15
    .catch Ljava/io/FileNotFoundException; {:try_start_15 .. :try_end_15} :catch_3
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_7

    .line 1470
    :goto_18
    :try_start_16
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v9

    if-eqz v9, :cond_d9

    .line 1472
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v148

    .line 1473
    .restart local v148       #curentPos:I
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v149

    .restart local v149       #curentByte:B
    move-object/from16 v150, v5

    move-object/from16 v151, v6

    move-object/from16 v152, v7

    move-object/from16 v153, v8

    .line 1474
    invoke-static/range {v148 .. v154}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_df

    .line 1475
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_dd

    .line 1476
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1477
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1479
    :cond_dd
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_de

    .line 1480
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1482
    :cond_de
    const/4 v0, 0x1

    move/16 v289, v0

    .line 1483
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_df
    move/from16 v9, v148

    move/from16 v10, v149

    move/from16 v15, v154

    .line 1486
    invoke-static/range {v9 .. v15}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_e2

    .line 1487
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_e0

    .line 1488
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1489
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1491
    :cond_e0
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_e1

    .line 1492
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1494
    :cond_e1
    const/4 v0, 0x1

    move/16 v289, v0

    .line 1495
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_e2
    move/from16 v27, v148

    move/from16 v28, v149

    move/from16 v33, v154

    .line 1498
    invoke-static/range {v27 .. v33}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_e5

    .line 1499
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_e3

    .line 1500
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1501
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1503
    :cond_e3
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_e4

    .line 1504
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1506
    :cond_e4
    const/4 v0, 0x1

    move/16 v289, v0

    .line 1507
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_e5
    move/from16 v51, v148

    move/from16 v52, v149

    move/from16 v57, v154

    .line 1510
    invoke-static/range {v51 .. v57}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_e8

    .line 1511
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_e6

    .line 1512
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1513
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1515
    :cond_e6
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_e7

    .line 1516
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1518
    :cond_e7
    const/4 v0, 0x1

    move/16 v289, v0

    .line 1519
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_e8
    move/from16 v33, v148

    move/from16 v34, v149

    move/from16 v39, v154

    .line 1522
    invoke-static/range {v33 .. v39}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_eb

    .line 1523
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_e9

    .line 1524
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1525
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1527
    :cond_e9
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_ea

    .line 1528
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1530
    :cond_ea
    const/4 v0, 0x1

    move/16 v289, v0

    .line 1531
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_eb
    move/from16 v15, v148

    move/from16 v16, v149

    move/from16 v21, v154

    .line 1534
    invoke-static/range {v15 .. v21}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_ee

    .line 1535
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_ec

    .line 1536
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1537
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1539
    :cond_ec
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_ed

    .line 1540
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1542
    :cond_ed
    const/4 v0, 0x1

    move/16 v290, v0

    .line 1543
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_ee
    move/from16 v39, v148

    move/from16 v40, v149

    move/from16 v45, v154

    .line 1545
    invoke-static/range {v39 .. v45}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_f1

    .line 1546
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_ef

    .line 1547
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1548
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1550
    :cond_ef
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_f0

    .line 1551
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1553
    :cond_f0
    const/4 v0, 0x1

    move/16 v290, v0

    .line 1554
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_f1
    move/from16 v21, v148

    move/from16 v22, v149

    move/from16 v27, v63

    .line 1556
    invoke-static/range {v21 .. v27}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_f4

    .line 1557
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_f2

    .line 1558
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1559
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1561
    :cond_f2
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_f3

    .line 1562
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1564
    :cond_f3
    const/4 v0, 0x1

    move/16 v291, v0

    .line 1565
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_f4
    move/from16 v45, v148

    move/from16 v46, v149

    move/from16 v51, v63

    .line 1567
    invoke-static/range {v45 .. v51}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_f7

    .line 1568
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_f5

    .line 1569
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1570
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1572
    :cond_f5
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_f6

    .line 1573
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1575
    :cond_f6
    const/4 v0, 0x1

    move/16 v291, v0

    .line 1576
    const/4 v0, 0x1

    move/16 v287, v0

    :cond_f7
    move/from16 v57, v148

    move/from16 v58, v149

    .line 1578
    invoke-static/range {v57 .. v63}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_fa

    .line 1579
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_f8

    .line 1580
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1581
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v148

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1583
    :cond_f8
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_f9

    .line 1584
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1586
    :cond_f9
    const/4 v0, 0x1

    move/16 v291, v0

    .line 1587
    const/4 v0, 0x1

    move/16 v287, v0

    .line 1589
    :cond_fa
    if-eqz v154, :cond_fb

    move/from16 v0, v63

    move/from16 v1, v295

    and-int/2addr v0, v1

    move v9, v0

    if-eqz v9, :cond_fb

    move/from16 v0, v289

    if-eqz v0, :cond_fb

    move/from16 v0, v290

    if-eqz v0, :cond_fb

    move/from16 v0, v291

    if-nez v0, :cond_fd

    :cond_fb
    if-eqz v154, :cond_fc

    if-nez v63, :cond_fc

    move/from16 v0, v295

    if-eqz v0, :cond_fc

    move/from16 v0, v289

    if-eqz v0, :cond_fc

    move/from16 v0, v290

    if-nez v0, :cond_fd

    :cond_fc
    if-nez v154, :cond_fe

    if-eqz v63, :cond_fe

    move/from16 v0, v291

    if-eqz v0, :cond_fe

    .line 1592
    :cond_fd
    const/4 v0, 0x1

    move/16 v287, v0

    .line 1593
    goto/16 :goto_16

    .line 1595
    :cond_fe
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v10, v148, 0x1

    invoke-virtual {v9, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_8
    .catch Ljava/io/FileNotFoundException; {:try_start_16 .. :try_end_16} :catch_3

    goto/16 :goto_18

    .line 1597
    .end local v148           #curentPos:I
    .end local v149           #curentByte:B
    :catch_8
    move-exception v240

    .line 1598
    .restart local v240       #e:Ljava/lang/Exception;
    :try_start_17
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v240

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_16

    .line 1620
    .end local v240           #e:Ljava/lang/Exception;
    .end local v289           #patchOat1:Z
    .end local v290           #patchOat2:Z
    .end local v291           #patchOat3:Z
    .end local v292           #patchOat4:Z
    .end local v295           #patchOatUpd1:Z
    .end local v299           #posit:I
    .restart local v239       #destination:Ljava/lang/String;
    .restart local v242       #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .restart local v330       #source:Ljava/lang/String;
    :catch_9
    move-exception v240

    .line 1621
    .restart local v240       #e:Ljava/lang/Exception;
    invoke-virtual/range {v240 .. v240}, Ljava/lang/Exception;->printStackTrace()V

    .line 1623
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, v239

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    goto/16 :goto_17

    .line 1626
    .end local v239           #destination:Ljava/lang/String;
    .end local v240           #e:Ljava/lang/Exception;
    .end local v242           #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v330           #source:Ljava/lang/String;
    :cond_ff
    const/4 v9, 0x1

    sput-boolean v9, Lcom/chelpus/root/utils/corepatch;->not_found_bytes_for_patch:Z

    goto/16 :goto_17

    .line 1637
    :cond_100
    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_104

    .line 1638
    invoke-static/range {v251 .. v251}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 1641
    :goto_19
    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "ART"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_106

    .line 1643
    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3a

    move/from16 v0, v287

    if-eqz v0, :cond_3a

    .line 1644
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "start"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1645
    const-string v239, "/system/framework/core-libart.backup"

    .line 1646
    .restart local v239       #destination:Ljava/lang/String;
    const-string v0, "/system/framework/core-libart.jar"

    move-object/16 v330, v0

    .line 1647
    .restart local v330       #source:Ljava/lang/String;
    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v0, v330

    move-object/from16 v1, v239

    invoke-static {v0, v1, v9, v10}, Lcom/chelpus/Utils;->copyFile(Ljava/lang/String;Ljava/lang/String;ZZ)Z

    move-result v9

    if-eqz v9, :cond_105

    .line 1648
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "good space"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1649
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, v239

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1650
    new-instance v242, Ljava/util/ArrayList;

    invoke-direct/range {v242 .. v242}, Ljava/util/ArrayList;-><init>()V

    .line 1651
    .restart local v242       #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "add files"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1652
    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    const-string v10, "/data/app/classes.dex"

    const-string v15, "/data/app/"

    invoke-direct {v9, v10, v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v242

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_17
    .catch Ljava/io/FileNotFoundException; {:try_start_17 .. :try_end_17} :catch_3
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_7

    .line 1654
    :try_start_18
    const-string v9, "/system/framework/core-libart.jar"

    const-string v10, "/system/framework/core-libart.backup"

    move-object/from16 v0, v242

    invoke-static {v9, v10, v0}, Lcom/chelpus/Utils;->addFilesToZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1656
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "add files finish"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1657
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chmod"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0644"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    const-string v15, "/system/framework/core-libart.backup"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1658
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chown"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0:0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    const-string v15, "/system/framework/core-libart.backup"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1659
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chmod"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0.0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    const-string v15, "/system/framework/core-libart.backup"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1661
    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "rm"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "/system/framework/core-libart.jar"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1662
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.jar"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_101

    .line 1663
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.jar"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1664
    :cond_101
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "mv"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "/system/framework/core.backup"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    const-string v15, "/system/framework/core-libart.jar"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1665
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.jar"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_102

    .line 1666
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.backup"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v10, Ljava/io/File;

    const-string v15, "/system/framework/core-libart.jar"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1667
    :cond_102
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chmod"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0644"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    const-string v15, "/system/framework/core-libart.jar"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1668
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chown"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0:0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    const-string v15, "/system/framework/core-libart.jar"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1669
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chmod"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0.0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    const-string v15, "/system/framework/core-libart.jar"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1670
    const-string v9, "/system/framework/core-libart.jar"

    invoke-static {v9}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v238

    .line 1671
    .local v238, dalv:Ljava/io/File;
    if-eqz v238, :cond_103

    .line 1672
    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "rm"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    invoke-virtual/range {v238 .. v238}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1673
    invoke-virtual/range {v238 .. v238}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_103

    invoke-virtual/range {v238 .. v238}, Ljava/io/File;->delete()Z

    .line 1676
    :cond_103
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@arm@boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1677
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@arm@boot.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1678
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@arm@boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1679
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@arm@boot.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1680
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@arm@boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1681
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@arm@boot.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_a
    .catch Ljava/io/FileNotFoundException; {:try_start_18 .. :try_end_18} :catch_3

    .line 1687
    .end local v238           #dalv:Ljava/io/File;
    :goto_1a
    :try_start_19
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "finish"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1639
    .end local v239           #destination:Ljava/lang/String;
    .end local v242           #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v330           #source:Ljava/lang/String;
    :cond_104
    const-string v9, "/system/framework/core.jar"

    move-object/from16 v0, v251

    invoke-static {v0, v9}, Lcom/chelpus/Utils;->fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V

    goto/16 :goto_19

    .line 1682
    .restart local v239       #destination:Ljava/lang/String;
    .restart local v242       #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .restart local v330       #source:Ljava/lang/String;
    :catch_a
    move-exception v240

    .line 1683
    .restart local v240       #e:Ljava/lang/Exception;
    invoke-virtual/range {v240 .. v240}, Ljava/lang/Exception;->printStackTrace()V

    .line 1684
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1685
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core.backup"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    goto :goto_1a

    .line 1690
    .end local v240           #e:Ljava/lang/Exception;
    .end local v242           #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    :cond_105
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, v239

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1693
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1699
    .end local v239           #destination:Ljava/lang/String;
    .end local v330           #source:Ljava/lang/String;
    :cond_106
    sget-boolean v9, Lcom/chelpus/root/utils/corepatch;->onlyDalvik:Z

    if-nez v9, :cond_3a

    .line 1700
    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "system@framework@core.jar@classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_107

    move/from16 v0, v287

    if-eqz v0, :cond_107

    .line 1701
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: dalvik-cache patched! "

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1702
    new-instance v235, Ljava/io/File;

    const-string v9, "/system/framework/core.patched"

    move-object/from16 v0, v235

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1703
    .local v235, corepatched:Ljava/io/File;
    const-string v239, "/system/framework/core.patched"

    .line 1704
    .restart local v239       #destination:Ljava/lang/String;
    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    move-object/16 v330, v0

    .line 1705
    .restart local v330       #source:Ljava/lang/String;
    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v0, v330

    move-object/from16 v1, v239

    invoke-static {v0, v1, v9, v10}, Lcom/chelpus/Utils;->copyFile(Ljava/lang/String;Ljava/lang/String;ZZ)Z

    move-result v9

    if-eqz v9, :cond_109

    .line 1706
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chmod"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0644"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    aput-object v239, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1707
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chown"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0.0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    aput-object v239, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1708
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chown"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0:0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    aput-object v239, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1729
    .end local v235           #corepatched:Ljava/io/File;
    .end local v239           #destination:Ljava/lang/String;
    .end local v330           #source:Ljava/lang/String;
    :cond_107
    :goto_1b
    new-instance v233, Ljava/io/File;

    const-string v9, "/system/framework/core.patched"

    move-object/from16 v0, v233

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1730
    .local v233, core:Ljava/io/File;
    invoke-virtual/range {v233 .. v233}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_108

    .line 1731
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: root found core.patched! "

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1732
    :cond_108
    new-instance v233, Ljava/io/File;

    .end local v233           #core:Ljava/io/File;
    const-string v9, "/system/framework/core.odex"

    move-object/from16 v0, v233

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1733
    .restart local v233       #core:Ljava/io/File;
    invoke-virtual/range {v233 .. v233}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_3a

    .line 1734
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: root found core.odex! "

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1710
    .end local v233           #core:Ljava/io/File;
    .restart local v235       #corepatched:Ljava/io/File;
    .restart local v239       #destination:Ljava/lang/String;
    .restart local v330       #source:Ljava/lang/String;
    :cond_109
    invoke-virtual/range {v235 .. v235}, Ljava/io/File;->delete()Z

    .line 1711
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: not space to system for odex core.jar! "

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1712
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_19
    .catch Ljava/io/FileNotFoundException; {:try_start_19 .. :try_end_19} :catch_3
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_7

    goto :goto_1b

    .line 1772
    .end local v235           #corepatched:Ljava/io/File;
    .end local v239           #destination:Ljava/lang/String;
    .end local v245           #fp:Ljava/io/File;
    .end local v251           #localFile2:Ljava/io/File;
    .end local v283           #oatForPatch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v287           #patch1:Z
    .end local v330           #source:Ljava/lang/String;
    .restart local v244       #for_patch:Ljava/io/File;
    .restart local v246       #good_odex:Z
    .restart local v247       #indexDir:Ljava/lang/String;
    .restart local v289       #patchOat1:Z
    .restart local v290       #patchOat2:Z
    .restart local v291       #patchOat3:Z
    .restart local v292       #patchOat4:Z
    .restart local v293       #patchOat5:Z
    .restart local v294       #patchOat6:Z
    :cond_10a
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chmod"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "644"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "/system/framework"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v247

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/services.odex"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1773
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chown"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0:0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "/system/framework"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v247

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/services.odex"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1774
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chown"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0.0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "/system/framework"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v247

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/services.odex"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    goto/16 :goto_e

    .line 1917
    .restart local v148       #curentPos:I
    .restart local v149       #curentByte:B
    .restart local v299       #posit:I
    :cond_10b
    :try_start_1a
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v10, v148, 0x1

    invoke-virtual {v9, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_b
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_c

    goto/16 :goto_f

    .line 1919
    .end local v148           #curentPos:I
    .end local v149           #curentByte:B
    :catch_b
    move-exception v240

    .line 1920
    .restart local v240       #e:Ljava/lang/Exception;
    :try_start_1b
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v240

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_c

    goto/16 :goto_10

    .line 1924
    .end local v240           #e:Ljava/lang/Exception;
    .end local v299           #posit:I
    :catch_c
    move-exception v240

    .line 1925
    .local v240, e:Ljava/io/IOException;
    invoke-virtual/range {v240 .. v240}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_11

    .line 1928
    .end local v240           #e:Ljava/io/IOException;
    :cond_10c
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_11

    .line 1964
    .end local v147           #ChannelDex:Ljava/nio/channels/FileChannel;
    .end local v244           #for_patch:Ljava/io/File;
    .end local v246           #good_odex:Z
    .end local v289           #patchOat1:Z
    .end local v290           #patchOat2:Z
    .end local v291           #patchOat3:Z
    .end local v292           #patchOat4:Z
    .end local v293           #patchOat5:Z
    .end local v294           #patchOat6:Z
    .restart local v251       #localFile2:Ljava/io/File;
    :cond_10d
    const/4 v9, 0x4

    :try_start_1c
    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "ART"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_110

    .line 1966
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_10e

    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v9

    const-wide/16 v15, 0x0

    cmp-long v9, v9, v15

    if-eqz v9, :cond_10e

    .line 1967
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Add services.odex for patch"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1968
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v243

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1971
    :cond_10e
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/services.jar"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v9

    if-eqz v9, :cond_ad

    .line 1972
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "services.jar contain classes,dex"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1973
    new-instance v0, Ljava/io/File;

    move-object/16 v329, v0

    const-string v9, "/system/framework/services.jar"

    move-object/from16 v0, v329

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1974
    .restart local v329       #serjar:Ljava/io/File;
    const-string v9, "/data/app"

    move-object/from16 v0, v329

    invoke-static {v0, v9}, Lcom/chelpus/root/utils/corepatch;->unzip(Ljava/io/File;Ljava/lang/String;)V

    .line 1975
    new-instance v232, Ljava/io/File;

    const-string v9, "/data/app/classes.dex"

    move-object/from16 v0, v232

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1976
    .restart local v232       #clasdex:Ljava/io/File;
    invoke-virtual/range {v232 .. v232}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_ad

    .line 1977
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Add classes.dex for patch"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1978
    move-object/from16 v0, v243

    move-object/from16 v1, v232

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1c
    .catch Ljava/io/FileNotFoundException; {:try_start_1c .. :try_end_1c} :catch_d
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_10

    goto/16 :goto_12

    .line 2566
    .end local v232           #clasdex:Ljava/io/File;
    .end local v329           #serjar:Ljava/io/File;
    :catch_d
    move-exception v253

    .line 2567
    .restart local v253       #localFileNotFoundException:Ljava/io/FileNotFoundException;
    :goto_1c
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Error: services.odex not found!\n\nPlease Odex services.jar and try again!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2576
    .end local v243           #filesToPatch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v247           #indexDir:Ljava/lang/String;
    .end local v251           #localFile2:Ljava/io/File;
    .end local v253           #localFileNotFoundException:Ljava/io/FileNotFoundException;
    :cond_10f
    :goto_1d
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 2577
    return-void

    .line 1983
    .restart local v243       #filesToPatch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v247       #indexDir:Ljava/lang/String;
    .restart local v251       #localFile2:Ljava/io/File;
    :cond_110
    :try_start_1d
    sget-boolean v9, Lcom/chelpus/root/utils/corepatch;->onlyDalvik:Z

    if-eqz v9, :cond_112

    .line 1984
    new-instance v252, Ljava/io/File;

    const/4 v9, 0x2

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    move-object/from16 v0, v252

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_1d
    .catch Ljava/io/FileNotFoundException; {:try_start_1d .. :try_end_1d} :catch_d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_10

    .line 1985
    .end local v251           #localFile2:Ljava/io/File;
    .restart local v252       #localFile2:Ljava/io/File;
    :try_start_1e
    invoke-virtual/range {v252 .. v252}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_111

    new-instance v9, Ljava/io/FileNotFoundException;

    invoke-direct {v9}, Ljava/io/FileNotFoundException;-><init>()V

    throw v9

    .line 2566
    :catch_e
    move-exception v253

    move-object/from16 v251, v252

    .end local v252           #localFile2:Ljava/io/File;
    .restart local v251       #localFile2:Ljava/io/File;
    goto :goto_1c

    .line 1986
    .end local v251           #localFile2:Ljava/io/File;
    .restart local v252       #localFile2:Ljava/io/File;
    :cond_111
    move-object/from16 v0, v243

    move-object/from16 v1, v252

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1e
    .catch Ljava/io/FileNotFoundException; {:try_start_1e .. :try_end_1e} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_f

    move-object/from16 v251, v252

    .end local v252           #localFile2:Ljava/io/File;
    .restart local v251       #localFile2:Ljava/io/File;
    goto/16 :goto_12

    .line 1988
    :cond_112
    :try_start_1f
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Vhodjashij file "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v15, 0x2

    move-object/from16 v0, p0

    aget-object v15, v0, v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1989
    new-instance v252, Ljava/io/File;

    const/4 v9, 0x2

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    move-object/from16 v0, v252

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_1f
    .catch Ljava/io/FileNotFoundException; {:try_start_1f .. :try_end_1f} :catch_d
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_10

    .line 1990
    .end local v251           #localFile2:Ljava/io/File;
    .restart local v252       #localFile2:Ljava/io/File;
    :try_start_20
    invoke-virtual/range {v252 .. v252}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_113

    new-instance v9, Ljava/io/FileNotFoundException;

    invoke-direct {v9}, Ljava/io/FileNotFoundException;-><init>()V

    throw v9
    :try_end_20
    .catch Ljava/io/FileNotFoundException; {:try_start_20 .. :try_end_20} :catch_e
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_f

    .line 2569
    :catch_f
    move-exception v240

    move-object/from16 v251, v252

    .line 2570
    .end local v252           #localFile2:Ljava/io/File;
    .local v240, e:Ljava/lang/Exception;
    .restart local v251       #localFile2:Ljava/io/File;
    :goto_1e
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception e"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v240 .. v240}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_1d

    .line 1991
    .end local v240           #e:Ljava/lang/Exception;
    .end local v251           #localFile2:Ljava/io/File;
    .restart local v252       #localFile2:Ljava/io/File;
    :cond_113
    :try_start_21
    invoke-virtual/range {v252 .. v252}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "system@framework@services.jar@classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_175

    .line 1992
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Vhodjashij file byl dalvick-cache "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v15, 0x2

    move-object/from16 v0, p0

    aget-object v15, v0, v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1993
    new-instance v0, Ljava/io/File;

    move-object/16 v329, v0

    const-string v9, "/system/framework/services.jar"

    move-object/from16 v0, v329

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1994
    .restart local v329       #serjar:Ljava/io/File;
    const-string v9, "/data/app"

    move-object/from16 v0, v329

    invoke-static {v0, v9}, Lcom/chelpus/root/utils/corepatch;->unzip(Ljava/io/File;Ljava/lang/String;)V

    .line 1995
    new-instance v232, Ljava/io/File;

    const-string v9, "/data/app/classes.dex"

    move-object/from16 v0, v232

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1996
    .restart local v232       #clasdex:Ljava/io/File;
    invoke-virtual/range {v232 .. v232}, Ljava/io/File;->exists()Z
    :try_end_21
    .catch Ljava/io/FileNotFoundException; {:try_start_21 .. :try_end_21} :catch_e
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_f

    move-result v9

    if-eqz v9, :cond_174

    move-object/from16 v251, v232

    .line 1997
    .end local v252           #localFile2:Ljava/io/File;
    .restart local v251       #localFile2:Ljava/io/File;
    :goto_1f
    :try_start_22
    new-instance v234, Ljava/io/File;

    const-string v9, "/system/framework/services.odex"

    move-object/from16 v0, v234

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1998
    .restart local v234       #coreodex:Ljava/io/File;
    invoke-virtual/range {v234 .. v234}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_114

    .line 1999
    invoke-virtual/range {v234 .. v234}, Ljava/io/File;->length()J

    move-result-wide v9

    const-wide/16 v15, 0x0

    cmp-long v9, v9, v15

    if-nez v9, :cond_114

    invoke-virtual/range {v234 .. v234}, Ljava/io/File;->delete()Z

    .line 2002
    .end local v232           #clasdex:Ljava/io/File;
    .end local v234           #coreodex:Ljava/io/File;
    .end local v329           #serjar:Ljava/io/File;
    :cond_114
    :goto_20
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Add file for patch "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2003
    move-object/from16 v0, v243

    move-object/from16 v1, v251

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_12

    .line 2569
    :catch_10
    move-exception v240

    goto/16 :goto_1e

    .line 2144
    .restart local v147       #ChannelDex:Ljava/nio/channels/FileChannel;
    .restart local v169       #counter2:I
    .restart local v236       #counter:I
    .restart local v245       #fp:Ljava/io/File;
    .restart local v248       #isOat:Z
    .restart local v249       #j:J
    .restart local v288       #patch4:Z
    :catch_11
    move-exception v240

    .line 2145
    .restart local v240       #e:Ljava/lang/Exception;
    invoke-virtual/range {v240 .. v240}, Ljava/lang/Exception;->printStackTrace()V

    .line 2146
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v240

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2148
    .end local v240           #e:Ljava/lang/Exception;
    :cond_115
    sget v10, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I

    if-lez v10, :cond_116

    sget-object v10, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    if-eqz v10, :cond_116

    .line 2149
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    sget v15, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2150
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    sget-object v15, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 2151
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 2293
    .end local v249           #j:J
    :cond_116
    :goto_21
    invoke-virtual/range {v147 .. v147}, Ljava/nio/channels/FileChannel;->close()V

    .line 2295
    const/4 v10, 0x4

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "framework"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_143

    .line 2296
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Rebuild file!"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2297
    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v15, "/classes.dex"

    invoke-virtual {v10, v15}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_117

    .line 2298
    invoke-static/range {v251 .. v251}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 2299
    move/from16 v0, v288

    if-eqz v0, :cond_142

    .line 2300
    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/corepatch;->not_found_bytes_for_patch:Z

    .line 2301
    const/4 v10, 0x1

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "/services.jar"

    const-string v16, "/services-patched.jar"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v239

    .line 2302
    .restart local v239       #destination:Ljava/lang/String;
    const/4 v10, 0x1

    move-object/from16 v0, p0

    aget-object v0, v0, v10

    move-object/16 v330, v0

    .line 2303
    .restart local v330       #source:Ljava/lang/String;
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, v239

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2304
    new-instance v242, Ljava/util/ArrayList;

    invoke-direct/range {v242 .. v242}, Ljava/util/ArrayList;-><init>()V

    .line 2305
    .restart local v242       #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "add files"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2306
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v251 .. v251}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v21, "/"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v10, v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v242

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_22
    .catch Ljava/io/FileNotFoundException; {:try_start_22 .. :try_end_22} :catch_d
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_10

    .line 2308
    :try_start_23
    move-object/from16 v0, v330

    move-object/from16 v1, v239

    move-object/from16 v2, v242

    invoke-static {v0, v1, v2}, Lcom/chelpus/Utils;->addFilesToZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2309
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "add files finish"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2310
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, v330

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_13
    .catch Ljava/io/FileNotFoundException; {:try_start_23 .. :try_end_23} :catch_d

    .line 2321
    .end local v239           #destination:Ljava/lang/String;
    .end local v242           #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v330           #source:Ljava/lang/String;
    :cond_117
    :goto_22
    :try_start_24
    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v15, "/services.odex"

    invoke-virtual {v10, v15}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_ae

    .line 2322
    invoke-static/range {v251 .. v251}, Lcom/chelpus/Utils;->isELFfiles(Ljava/io/File;)Z

    move-result v10

    if-nez v10, :cond_118

    .line 2323
    const/4 v10, 0x0

    move-object/from16 v0, v251

    invoke-static {v0, v10}, Lcom/chelpus/Utils;->fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V

    .line 2325
    :cond_118
    move/from16 v0, v288

    if-eqz v0, :cond_ae

    .line 2326
    new-instance v10, Ljava/io/File;

    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    const-string v16, "/services.odex"

    const-string v21, "/services-patched.odex"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v251

    invoke-virtual {v0, v10}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto/16 :goto_13

    .line 2154
    :cond_119
    const/4 v0, 0x0

    move/16 v289, v0

    .restart local v289       #patchOat1:Z
    const/4 v0, 0x0

    move/16 v290, v0

    .restart local v290       #patchOat2:Z
    const/4 v0, 0x0

    move/16 v291, v0

    .restart local v291       #patchOat3:Z
    const/4 v0, 0x0

    move/16 v292, v0

    .restart local v292       #patchOat4:Z
    const/4 v0, 0x0

    move/16 v293, v0

    .restart local v293       #patchOat5:Z
    const/4 v0, 0x0

    move/16 v294, v0

    .line 2155
    .restart local v294       #patchOat6:Z
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    const/16 v15, 0x1018

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2156
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v10

    sget-object v15, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v15}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v15

    sget-object v16, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v16

    sget-object v21, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual/range {v21 .. v21}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v21

    move/from16 v0, v16

    move/from16 v1, v21

    invoke-static {v10, v15, v0, v1}, Lcom/chelpus/Utils;->convertFourBytesToInt(BBBB)I

    move-result v0

    move/16 v299, v0

    .line 2157
    .restart local v299       #posit:I
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Start position:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, v299

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2158
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    move/from16 v0, v299

    invoke-virtual {v10, v0}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_24
    .catch Ljava/io/FileNotFoundException; {:try_start_24 .. :try_end_24} :catch_d
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_10

    .line 2161
    :goto_23
    :try_start_25
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v10

    if-eqz v10, :cond_116

    .line 2163
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v148

    .line 2164
    .restart local v148       #curentPos:I
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v149

    .restart local v149       #curentByte:B
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v65

    move-object/from16 v158, v66

    move-object/from16 v159, v67

    move-object/from16 v160, v68

    move/from16 v161, v75

    .line 2165
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_11c

    .line 2166
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_11a

    .line 2167
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2169
    :cond_11a
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_11b

    .line 2170
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2172
    :cond_11b
    const/4 v0, 0x1

    move/16 v289, v0

    .line 2173
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_11c
    move/from16 v69, v148

    move/from16 v70, v149

    .line 2175
    invoke-static/range {v69 .. v75}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_11f

    .line 2176
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_11d

    .line 2177
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2179
    :cond_11d
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_11e

    .line 2180
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2182
    :cond_11e
    const/4 v0, 0x1

    move/16 v290, v0

    const/4 v0, 0x1

    move/16 v288, v0

    :cond_11f
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v77

    move-object/from16 v158, v78

    move-object/from16 v159, v79

    move-object/from16 v160, v80

    move/from16 v161, v75

    .line 2184
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_122

    .line 2185
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_120

    .line 2186
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!checkUpgradeKeySetLP\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2188
    :cond_120
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_121

    .line 2189
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!checkUpgradeKeySetLP\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2191
    :cond_121
    const/4 v0, 0x1

    move/16 v291, v0

    const/4 v0, 0x1

    move/16 v288, v0

    :cond_122
    move/from16 v81, v148

    move/from16 v82, v149

    .line 2193
    invoke-static/range {v81 .. v87}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_125

    .line 2194
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_123

    .line 2195
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!queryIntentServices\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2197
    :cond_123
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_124

    .line 2198
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!queryIntentServices\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2200
    :cond_124
    const/4 v0, 0x1

    move/16 v292, v0

    const/4 v0, 0x1

    move/16 v288, v0

    :cond_125
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v89

    move-object/from16 v158, v90

    move-object/from16 v159, v91

    move-object/from16 v160, v92

    move/from16 v161, v87

    .line 2202
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_128

    .line 2203
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_126

    .line 2204
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!buildResolveList\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2206
    :cond_126
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_127

    .line 2207
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!buildResolveList\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2209
    :cond_127
    const/4 v0, 0x1

    move/16 v293, v0

    const/4 v0, 0x1

    move/16 v288, v0

    :cond_128
    move/from16 v93, v148

    move/from16 v94, v149

    move/from16 v99, v75

    .line 2211
    invoke-static/range {v93 .. v99}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_12b

    .line 2212
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_129

    .line 2213
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!FixForCM\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2215
    :cond_129
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_12a

    .line 2216
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!FixForCM\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2218
    :cond_12a
    const/4 v0, 0x1

    move/16 v294, v0

    const/4 v0, 0x1

    move/16 v288, v0

    :cond_12b
    move/from16 v99, v148

    move/from16 v100, v149

    move/from16 v105, v75

    .line 2220
    invoke-static/range {v99 .. v105}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_12e

    .line 2221
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_12c

    .line 2222
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!FixForCM\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2224
    :cond_12c
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_12d

    .line 2225
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!FixForCM\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2227
    :cond_12d
    const/4 v0, 0x1

    move/16 v294, v0

    const/4 v0, 0x1

    move/16 v288, v0

    :cond_12e
    move/from16 v105, v148

    move/from16 v106, v149

    move/from16 v111, v75

    .line 2229
    invoke-static/range {v105 .. v111}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_131

    .line 2230
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_12f

    .line 2231
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2233
    :cond_12f
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_130

    .line 2234
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2236
    :cond_130
    const/4 v0, 0x1

    move/16 v289, v0

    const/4 v0, 0x1

    move/16 v288, v0

    :cond_131
    move/from16 v117, v148

    move/from16 v118, v149

    move/from16 v123, v75

    .line 2238
    invoke-static/range {v117 .. v123}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_134

    .line 2239
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_132

    .line 2240
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2242
    :cond_132
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_133

    .line 2243
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2245
    :cond_133
    const/4 v0, 0x1

    move/16 v289, v0

    const/4 v0, 0x1

    move/16 v288, v0

    :cond_134
    move/from16 v123, v148

    move/from16 v124, v149

    move/from16 v129, v75

    .line 2247
    invoke-static/range {v123 .. v129}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_137

    .line 2248
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_135

    .line 2249
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2251
    :cond_135
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_136

    .line 2252
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2254
    :cond_136
    const/4 v0, 0x1

    move/16 v289, v0

    const/4 v0, 0x1

    move/16 v288, v0

    :cond_137
    move/from16 v111, v148

    move/from16 v112, v149

    move/from16 v117, v75

    .line 2256
    invoke-static/range {v111 .. v117}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_13a

    .line 2257
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_138

    .line 2258
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2260
    :cond_138
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_139

    .line 2261
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2263
    :cond_139
    const/4 v0, 0x1

    move/16 v290, v0

    const/4 v0, 0x1

    move/16 v288, v0

    :cond_13a
    move/from16 v129, v148

    move/from16 v130, v149

    move/from16 v135, v75

    .line 2265
    invoke-static/range {v129 .. v135}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_13d

    .line 2266
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_13b

    .line 2267
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2269
    :cond_13b
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_13c

    .line 2270
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2272
    :cond_13c
    const/4 v0, 0x1

    move/16 v290, v0

    const/4 v0, 0x1

    move/16 v288, v0

    :cond_13d
    move/from16 v135, v148

    move/from16 v136, v149

    move/from16 v141, v75

    .line 2274
    invoke-static/range {v135 .. v141}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_140

    .line 2275
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_13e

    .line 2276
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2278
    :cond_13e
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_13f

    .line 2279
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2281
    :cond_13f
    const/4 v0, 0x1

    move/16 v290, v0

    const/4 v0, 0x1

    move/16 v288, v0

    .line 2283
    :cond_140
    if-eqz v75, :cond_141

    move/from16 v0, v289

    if-eqz v0, :cond_141

    move/from16 v0, v290

    if-eqz v0, :cond_141

    move/from16 v0, v291

    if-eqz v0, :cond_141

    move/from16 v0, v292

    if-eqz v0, :cond_141

    move/from16 v0, v293

    if-eqz v0, :cond_141

    move/from16 v0, v294

    if-nez v0, :cond_116

    .line 2287
    :cond_141
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v15, v148, 0x1

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_25} :catch_12
    .catch Ljava/io/FileNotFoundException; {:try_start_25 .. :try_end_25} :catch_d

    goto/16 :goto_23

    .line 2289
    .end local v148           #curentPos:I
    .end local v149           #curentByte:B
    :catch_12
    move-exception v240

    .line 2290
    .restart local v240       #e:Ljava/lang/Exception;
    :try_start_26
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v240

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_21

    .line 2312
    .end local v240           #e:Ljava/lang/Exception;
    .end local v289           #patchOat1:Z
    .end local v290           #patchOat2:Z
    .end local v291           #patchOat3:Z
    .end local v292           #patchOat4:Z
    .end local v293           #patchOat5:Z
    .end local v294           #patchOat6:Z
    .end local v299           #posit:I
    .restart local v239       #destination:Ljava/lang/String;
    .restart local v242       #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .restart local v330       #source:Ljava/lang/String;
    :catch_13
    move-exception v240

    .line 2313
    .restart local v240       #e:Ljava/lang/Exception;
    invoke-virtual/range {v240 .. v240}, Ljava/lang/Exception;->printStackTrace()V

    .line 2315
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, v239

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    goto/16 :goto_22

    .line 2318
    .end local v239           #destination:Ljava/lang/String;
    .end local v240           #e:Ljava/lang/Exception;
    .end local v242           #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v330           #source:Ljava/lang/String;
    :cond_142
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/corepatch;->not_found_bytes_for_patch:Z

    goto/16 :goto_22

    .line 2329
    :cond_143
    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v15, "/classes.dex"

    invoke-virtual {v10, v15}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_14b

    invoke-static/range {v251 .. v251}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 2332
    :goto_24
    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v15, "system@framework@services.jar@classes.dex"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_144

    move/from16 v0, v288

    if-eqz v0, :cond_144

    .line 2334
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "LuckyPatcher: dalvik-cache patched! "

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2339
    :cond_144
    invoke-virtual/range {v251 .. v251}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v15, "/classes.dex"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_148

    move/from16 v0, v288

    if-eqz v0, :cond_148

    .line 2340
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "start"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2341
    const-string v239, "/system/framework/services.backup"

    .line 2342
    .restart local v239       #destination:Ljava/lang/String;
    const-string v0, "/system/framework/services.jar"

    move-object/16 v330, v0

    .line 2343
    .restart local v330       #source:Ljava/lang/String;
    const/4 v10, 0x1

    const/4 v15, 0x0

    move-object/from16 v0, v330

    move-object/from16 v1, v239

    invoke-static {v0, v1, v10, v15}, Lcom/chelpus/Utils;->copyFile(Ljava/lang/String;Ljava/lang/String;ZZ)Z

    move-result v10

    if-eqz v10, :cond_14c

    .line 2344
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "good space"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2345
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, v239

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2346
    new-instance v242, Ljava/util/ArrayList;

    invoke-direct/range {v242 .. v242}, Ljava/util/ArrayList;-><init>()V

    .line 2347
    .restart local v242       #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "add files"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2348
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    const-string v15, "/data/app/classes.dex"

    const-string v16, "/data/app/"

    move-object/from16 v0, v16

    invoke-direct {v10, v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v242

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_26
    .catch Ljava/io/FileNotFoundException; {:try_start_26 .. :try_end_26} :catch_d
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_26} :catch_10

    .line 2350
    :try_start_27
    const-string v10, "/system/framework/services.jar"

    const-string v15, "/system/framework/services.backup"

    move-object/from16 v0, v242

    invoke-static {v10, v15, v0}, Lcom/chelpus/Utils;->addFilesToZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2351
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "add files finish"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2352
    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "chmod"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "0644"

    aput-object v16, v10, v15

    const/4 v15, 0x2

    const-string v16, "/system/framework/services.backup"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2353
    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "chown"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "0:0"

    aput-object v16, v10, v15

    const/4 v15, 0x2

    const-string v16, "/system/framework/services.backup"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2354
    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "chmod"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "0.0"

    aput-object v16, v10, v15

    const/4 v15, 0x2

    const-string v16, "/system/framework/services.backup"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2355
    new-instance v234, Ljava/io/File;

    const-string v10, "/system/framework/services.odex"

    move-object/from16 v0, v234

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2357
    .restart local v234       #coreodex:Ljava/io/File;
    const-string v10, "/system/framework/services.jar"

    invoke-static {v10}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v237

    .line 2358
    .local v237, d:Ljava/io/File;
    invoke-virtual/range {v234 .. v234}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_145

    .line 2359
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "fix odex na osnove rebuild services"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2360
    const-string v10, "/system/framework/services.backup"

    move-object/from16 v0, v234

    invoke-static {v0, v10}, Lcom/chelpus/Utils;->fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V

    .line 2363
    :cond_145
    new-instance v10, Ljava/io/File;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/ClearDalvik.on"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->createNewFile()Z

    .line 2364
    new-instance v10, Ljava/io/File;

    const-string v15, "/system/framework/services.jar"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2365
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/system/framework/services.jar"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2366
    new-instance v10, Ljava/io/File;

    const-string v15, "/system/framework/services.backup"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v15, Ljava/io/File;

    const-string v16, "/system/framework/services.jar"

    invoke-direct/range {v15 .. v16}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v15}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 2367
    new-instance v10, Ljava/io/File;

    const-string v15, "/system/framework/services.jar"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_146

    .line 2368
    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "mv"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/system/framework/services.backup"

    aput-object v16, v10, v15

    const/4 v15, 0x2

    const-string v16, "/system/framework/services.jar"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_27} :catch_15
    .catch Ljava/io/FileNotFoundException; {:try_start_27 .. :try_end_27} :catch_d

    .line 2370
    :cond_146
    if-eqz v237, :cond_147

    .line 2371
    const/4 v10, 0x2

    :try_start_28
    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    invoke-virtual/range {v237 .. v237}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2372
    invoke-virtual/range {v237 .. v237}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_147

    invoke-virtual/range {v237 .. v237}, Ljava/io/File;->delete()Z

    .line 2374
    :cond_147
    new-instance v10, Ljava/io/File;

    const-string v15, "/data/dalvik-cache/arm/system@framework@arm@boot.oat"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2375
    new-instance v10, Ljava/io/File;

    const-string v15, "/data/dalvik-cache/arm/system@framework@arm@boot.art"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2376
    new-instance v10, Ljava/io/File;

    const-string v15, "/data/dalvik-cache/arm64/system@framework@arm@boot.oat"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2377
    new-instance v10, Ljava/io/File;

    const-string v15, "/data/dalvik-cache/arm64/system@framework@arm@boot.art"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2378
    new-instance v10, Ljava/io/File;

    const-string v15, "/data/dalvik-cache/x86/system@framework@arm@boot.oat"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2379
    new-instance v10, Ljava/io/File;

    const-string v15, "/data/dalvik-cache/x86/system@framework@arm@boot.art"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z
    :try_end_28
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_28} :catch_14
    .catch Ljava/io/FileNotFoundException; {:try_start_28 .. :try_end_28} :catch_d

    .line 2383
    :goto_25
    const/4 v10, 0x2

    :try_start_29
    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/data/dalvik-cache/*.dex"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2384
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/data/dalvik-cache/*.oat"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2385
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/data/dalvik-cache/*.art"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2386
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/data/dalvik-cache/arm/*.dex"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2387
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/data/dalvik-cache/arm/*.art"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2388
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/data/dalvik-cache/arm/*.oat"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2389
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/data/dalvik-cache/arm64/*.dex"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2390
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/data/dalvik-cache/arm64/*.art"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2391
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/data/dalvik-cache/arm64/*.oat"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2392
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/data/dalvik-cache/x86/*.dex"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2393
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/data/dalvik-cache/x86/*.art"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2394
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/data/dalvik-cache/x86/*.oat"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_29} :catch_15
    .catch Ljava/io/FileNotFoundException; {:try_start_29 .. :try_end_29} :catch_d

    .line 2401
    .end local v234           #coreodex:Ljava/io/File;
    .end local v237           #d:Ljava/io/File;
    :goto_26
    :try_start_2a
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "finish"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2548
    .end local v239           #destination:Ljava/lang/String;
    .end local v242           #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v330           #source:Ljava/lang/String;
    :cond_148
    :goto_27
    sget-boolean v10, Lcom/chelpus/root/utils/corepatch;->onlyDalvik:Z

    if-nez v10, :cond_ae

    .line 2549
    new-instance v233, Ljava/io/File;

    const-string v10, "/system/framework/services.patched"

    move-object/from16 v0, v233

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2550
    .restart local v233       #core:Ljava/io/File;
    invoke-virtual/range {v233 .. v233}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_149

    .line 2551
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "LuckyPatcher: root found services.patched! "

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2552
    :cond_149
    new-instance v233, Ljava/io/File;

    .end local v233           #core:Ljava/io/File;
    const-string v10, "/system/framework/services.odex"

    move-object/from16 v0, v233

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2553
    .restart local v233       #core:Ljava/io/File;
    invoke-virtual/range {v233 .. v233}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_14a

    .line 2554
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "LuckyPatcher: root found services.odex! "

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2555
    :cond_14a
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_ae

    .line 2556
    new-instance v10, Ljava/io/File;

    const-string v15, "/system/framework/patch3.done"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    goto/16 :goto_13

    .line 2330
    .end local v233           #core:Ljava/io/File;
    :cond_14b
    const-string v10, "/system/framework/services.jar"

    move-object/from16 v0, v251

    invoke-static {v0, v10}, Lcom/chelpus/Utils;->fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2a
    .catch Ljava/io/FileNotFoundException; {:try_start_2a .. :try_end_2a} :catch_d
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_2a} :catch_10

    goto/16 :goto_24

    .line 2380
    .restart local v234       #coreodex:Ljava/io/File;
    .restart local v237       #d:Ljava/io/File;
    .restart local v239       #destination:Ljava/lang/String;
    .restart local v242       #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .restart local v330       #source:Ljava/lang/String;
    :catch_14
    move-exception v240

    .line 2381
    .restart local v240       #e:Ljava/lang/Exception;
    :try_start_2b
    invoke-virtual/range {v240 .. v240}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_2b} :catch_15
    .catch Ljava/io/FileNotFoundException; {:try_start_2b .. :try_end_2b} :catch_d

    goto/16 :goto_25

    .line 2395
    .end local v234           #coreodex:Ljava/io/File;
    .end local v237           #d:Ljava/io/File;
    .end local v240           #e:Ljava/lang/Exception;
    :catch_15
    move-exception v240

    .line 2396
    .restart local v240       #e:Ljava/lang/Exception;
    :try_start_2c
    invoke-virtual/range {v240 .. v240}, Ljava/lang/Exception;->printStackTrace()V

    .line 2397
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2398
    new-instance v10, Ljava/io/File;

    const-string v15, "/system/framework/services.backup"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    goto :goto_26

    .line 2404
    .end local v240           #e:Ljava/lang/Exception;
    .end local v242           #files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    :cond_14c
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, v239

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2405
    new-instance v252, Ljava/io/File;

    const/4 v10, 0x2

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    move-object/from16 v0, v252

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_2c
    .catch Ljava/io/FileNotFoundException; {:try_start_2c .. :try_end_2c} :catch_d
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_2c} :catch_10

    .line 2406
    .end local v251           #localFile2:Ljava/io/File;
    .restart local v252       #localFile2:Ljava/io/File;
    :try_start_2d
    new-instance v10, Ljava/io/RandomAccessFile;

    const-string v15, "rw"

    move-object/from16 v0, v252

    invoke-direct {v10, v0, v15}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v147

    .line 2407
    sget-object v156, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v157, 0x0

    invoke-virtual/range {v147 .. v147}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v15

    long-to-int v10, v15

    int-to-long v0, v10

    move-wide/from16 v159, v0

    move-object/from16 v155, v147

    invoke-virtual/range {v155 .. v160}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v10

    sput-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    .line 2408
    const/16 v236, 0x0

    const/16 v169, 0x0

    .line 2409
    const/4 v10, 0x0

    sput-object v10, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    .line 2410
    const/4 v10, 0x0

    sput v10, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I
    :try_end_2d
    .catch Ljava/io/FileNotFoundException; {:try_start_2d .. :try_end_2d} :catch_e
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_2d} :catch_f

    .line 2411
    const/4 v0, 0x0

    move/16 v288, v0

    .line 2413
    const-wide/16 v249, 0x0

    .restart local v249       #j:J
    :goto_28
    :try_start_2e
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v10

    if-eqz v10, :cond_171

    .line 2415
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v148

    .line 2416
    .restart local v148       #curentPos:I
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v149

    .restart local v149       #curentByte:B
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v195

    move-object/from16 v158, v276

    move-object/from16 v159, v225

    move-object/from16 v160, v322

    move/from16 v161, v75

    .line 2417
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_14f

    .line 2418
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_14d

    .line 2419
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2421
    :cond_14d
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_14e

    .line 2422
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2424
    :cond_14e
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_14f
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v198

    move-object/from16 v158, v279

    move-object/from16 v159, v228

    move-object/from16 v160, v325

    move/from16 v161, v75

    .line 2426
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_152

    .line 2427
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_150

    .line 2428
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2430
    :cond_150
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_151

    .line 2431
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2433
    :cond_151
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_152
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v199

    move-object/from16 v158, v280

    move-object/from16 v159, v229

    move-object/from16 v160, v326

    move/from16 v161, v75

    .line 2435
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_155

    .line 2436
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_153

    .line 2437
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!checkUpgradeKeySetLP\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2439
    :cond_153
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_154

    .line 2440
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!checkUpgradeKeySetLP\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2442
    :cond_154
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_155
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v200

    move-object/from16 v158, v281

    move-object/from16 v159, v230

    move-object/from16 v160, v327

    move/from16 v161, v75

    .line 2444
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_158

    .line 2445
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_156

    .line 2446
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nCM12"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2448
    :cond_156
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_157

    .line 2449
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nCM12"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2451
    :cond_157
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_158
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v201

    move-object/from16 v158, v282

    move-object/from16 v159, v231

    move-object/from16 v160, v328

    move/from16 v161, v236

    move/from16 v163, v87

    .line 2453
    invoke-static/range {v155 .. v163}, Lcom/chelpus/root/utils/corepatch;->applyPatchCounter(IB[B[B[B[BIIZ)Z

    move-result v10

    if-eqz v10, :cond_15b

    .line 2454
    add-int/lit8 v236, v236, 0x1

    .line 2455
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_159

    .line 2456
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2458
    :cond_159
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_15a

    .line 2459
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2461
    :cond_15a
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_15b
    move/from16 v163, v148

    move/from16 v164, v149

    move/from16 v171, v87

    .line 2463
    invoke-static/range {v163 .. v171}, Lcom/chelpus/root/utils/corepatch;->applyPatchCounter(IB[B[B[B[BIIZ)Z

    move-result v10

    if-eqz v10, :cond_15e

    .line 2464
    add-int/lit8 v169, v169, 0x1

    .line 2465
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_15c

    .line 2466
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2468
    :cond_15c
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_15d

    .line 2469
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2471
    :cond_15d
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_15e
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v192

    move-object/from16 v158, v273

    move-object/from16 v159, v222

    move-object/from16 v160, v319

    move/from16 v161, v87

    .line 2473
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_161

    .line 2474
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_15f

    .line 2475
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2477
    :cond_15f
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_160

    .line 2478
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2480
    :cond_160
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_161
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v193

    move-object/from16 v158, v274

    move-object/from16 v159, v223

    move-object/from16 v160, v320

    move/from16 v161, v87

    .line 2482
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_164

    .line 2483
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_162

    .line 2484
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2486
    :cond_162
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_163

    .line 2487
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2489
    :cond_163
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_164
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v194

    move-object/from16 v158, v275

    move-object/from16 v159, v224

    move-object/from16 v160, v321

    move/from16 v161, v87

    .line 2491
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_167

    .line 2492
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_165

    .line 2493
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2495
    :cond_165
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_166

    .line 2496
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2498
    :cond_166
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_167
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v196

    move-object/from16 v158, v277

    move-object/from16 v159, v226

    move-object/from16 v160, v323

    move/from16 v161, v75

    .line 2500
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_16a

    .line 2501
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_168

    .line 2502
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nCM11"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2504
    :cond_168
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_169

    .line 2505
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nCM11"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2507
    :cond_169
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_16a
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v197

    move-object/from16 v158, v278

    move-object/from16 v159, v227

    move-object/from16 v160, v324

    move/from16 v161, v75

    .line 2509
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_16d

    .line 2510
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_16b

    .line 2511
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2513
    :cond_16b
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_16c

    .line 2514
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2516
    :cond_16c
    const/4 v0, 0x1

    move/16 v288, v0

    :cond_16d
    move/from16 v155, v148

    move/from16 v156, v149

    move-object/from16 v157, v143

    move-object/from16 v158, v144

    move-object/from16 v159, v145

    move-object/from16 v160, v146

    move/from16 v161, v75

    .line 2518
    invoke-static/range {v155 .. v161}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_170

    .line 2519
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_16e

    .line 2520
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2522
    :cond_16e
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_16f

    .line 2523
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2525
    :cond_16f
    const/4 v0, 0x1

    move/16 v288, v0

    .line 2528
    :cond_170
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v15, v148, 0x1

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_2e} :catch_16
    .catch Ljava/io/FileNotFoundException; {:try_start_2e .. :try_end_2e} :catch_e

    .line 2413
    const-wide/16 v15, 0x1

    add-long v249, v249, v15

    goto/16 :goto_28

    .line 2530
    .end local v148           #curentPos:I
    .end local v149           #curentByte:B
    :catch_16
    move-exception v240

    .line 2531
    .restart local v240       #e:Ljava/lang/Exception;
    :try_start_2f
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v240

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2533
    .end local v240           #e:Ljava/lang/Exception;
    :cond_171
    sget v10, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I

    if-lez v10, :cond_172

    sget-object v10, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    if-eqz v10, :cond_172

    .line 2534
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    sget v15, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2535
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    sget-object v15, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 2536
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 2538
    :cond_172
    invoke-virtual/range {v147 .. v147}, Ljava/nio/channels/FileChannel;->close()V

    .line 2540
    invoke-virtual/range {v252 .. v252}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v15, "/classes.dex"

    invoke-virtual {v10, v15}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_173

    .line 2541
    invoke-static/range {v252 .. v252}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 2544
    :goto_29
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move-object/from16 v251, v252

    .end local v252           #localFile2:Ljava/io/File;
    .restart local v251       #localFile2:Ljava/io/File;
    goto/16 :goto_27

    .line 2542
    .end local v251           #localFile2:Ljava/io/File;
    .restart local v252       #localFile2:Ljava/io/File;
    :cond_173
    const-string v10, "/system/framework/services.jar"

    move-object/from16 v0, v252

    invoke-static {v0, v10}, Lcom/chelpus/Utils;->fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2f
    .catch Ljava/io/FileNotFoundException; {:try_start_2f .. :try_end_2f} :catch_e
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_2f} :catch_f

    goto :goto_29

    .end local v147           #ChannelDex:Ljava/nio/channels/FileChannel;
    .end local v169           #counter2:I
    .end local v236           #counter:I
    .end local v239           #destination:Ljava/lang/String;
    .end local v245           #fp:Ljava/io/File;
    .end local v248           #isOat:Z
    .end local v249           #j:J
    .end local v288           #patch4:Z
    .end local v330           #source:Ljava/lang/String;
    .restart local v232       #clasdex:Ljava/io/File;
    .restart local v329       #serjar:Ljava/io/File;
    :cond_174
    move-object/from16 v251, v252

    .end local v252           #localFile2:Ljava/io/File;
    .restart local v251       #localFile2:Ljava/io/File;
    goto/16 :goto_1f

    .end local v232           #clasdex:Ljava/io/File;
    .end local v251           #localFile2:Ljava/io/File;
    .end local v329           #serjar:Ljava/io/File;
    .restart local v252       #localFile2:Ljava/io/File;
    :cond_175
    move-object/from16 v251, v252

    .end local v252           #localFile2:Ljava/io/File;
    .restart local v251       #localFile2:Ljava/io/File;
    goto/16 :goto_20

    .end local v63           #pattern2:Z
    .end local v243           #filesToPatch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v247           #indexDir:Ljava/lang/String;
    .end local v251           #localFile2:Ljava/io/File;
    .restart local v296       #pattern2:Z
    :cond_176
    move/16 v298, v87

    .end local v87           #pattern4:Z
    .restart local v298       #pattern4:Z
    move/16 v297, v75

    .end local v75           #pattern3:Z
    .restart local v297       #pattern3:Z
    goto/16 :goto_5

    .end local v296           #pattern2:Z
    .end local v297           #pattern3:Z
    .end local v298           #pattern4:Z
    .restart local v63       #pattern2:Z
    .restart local v75       #pattern3:Z
    .restart local v87       #pattern4:Z
    :cond_177
    move/16 v296, v63

    .end local v63           #pattern2:Z
    .restart local v296       #pattern2:Z
    goto/16 :goto_4

    .end local v75           #pattern3:Z
    .restart local v297       #pattern3:Z
    :cond_178
    move/16 v298, v87

    .end local v87           #pattern4:Z
    .restart local v298       #pattern4:Z
    goto/16 :goto_3

    .end local v297           #pattern3:Z
    .end local v298           #pattern4:Z
    .restart local v75       #pattern3:Z
    .restart local v87       #pattern4:Z
    :cond_179
    move/16 v297, v75

    .end local v75           #pattern3:Z
    .restart local v297       #pattern3:Z
    goto/16 :goto_2

    .end local v296           #pattern2:Z
    .end local v297           #pattern3:Z
    .restart local v63       #pattern2:Z
    .restart local v75       #pattern3:Z
    :cond_17a
    move/16 v296, v63

    .end local v63           #pattern2:Z
    .restart local v296       #pattern2:Z
    goto/16 :goto_1

    .line 493
    nop

    :array_0
    .array-data 0x1
        0x39t
        0x66t
        0x8t
        0x0t
        0x39t
        0x66t
        0x4t
        0x0t
        0x12t
        0x16t
        0xft
        0x6t
        0x12t
        0xf6t
        0x28t
        0xfet
    .end array-data

    .line 494
    :array_1
    .array-data 0x1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 495
    :array_2
    .array-data 0x1
        0x39t
        0x66t
        0x8t
        0x0t
        0x39t
        0x66t
        0x4t
        0x0t
        0x12t
        0x16t
        0x12t
        0x6t
        0x12t
        0x6t
        0xft
        0x6t
    .end array-data

    .line 496
    :array_3
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 499
    :array_4
    .array-data 0x1
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        0xedt
        0xfft
    .end array-data

    .line 500
    :array_5
    .array-data 0x1
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 501
    :array_6
    .array-data 0x1
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        0xedt
        0xfft
    .end array-data

    .line 502
    :array_7
    .array-data 0x1
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 505
    :array_8
    .array-data 0x1
        0x80t
        0x0t
        0x39t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x35t
        0x87t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 506
    :array_9
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 507
    :array_a
    .array-data 0x1
        0x80t
        0x0t
        0x39t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x35t
        0x88t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 508
    :array_b
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 511
    :array_c
    .array-data 0x1
        0x39t
        0x66t
        0x7t
        0x0t
        0x39t
        0x66t
        0x3t
        0x0t
        0xft
        0x6t
        0x12t
        0xf6t
        0x28t
        0xfet
    .end array-data

    .line 512
    nop

    :array_d
    .array-data 0x1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 513
    nop

    :array_e
    .array-data 0x1
        0x39t
        0x66t
        0x7t
        0x0t
        0x39t
        0x66t
        0x3t
        0x0t
        0x12t
        0x6t
        0x12t
        0x6t
        0xft
        0x6t
    .end array-data

    .line 514
    nop

    :array_f
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 517
    nop

    :array_10
    .array-data 0x1
        0x54t
        0x66t
        0x66t
        0x66t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x38t
        0x4t
        0x4t
        0x0t
        0x12t
        0x14t
        0xft
        0x4t
    .end array-data

    .line 518
    :array_11
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 519
    :array_12
    .array-data 0x1
        0x54t
        0x66t
        0x66t
        0x66t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x39t
        0x4t
        0x4t
        0x0t
        0x12t
        0x14t
        0xft
        0x4t
    .end array-data

    .line 520
    :array_13
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 524
    :array_14
    .array-data 0x1
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        0xedt
        0xfft
    .end array-data

    .line 525
    nop

    :array_15
    .array-data 0x1
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 526
    nop

    :array_16
    .array-data 0x1
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        0xedt
        0xfft
    .end array-data

    .line 527
    nop

    :array_17
    .array-data 0x1
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 530
    nop

    :array_18
    .array-data 0x1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x39t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x1et
        0x66t
    .end array-data

    .line 531
    :array_19
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
    .end array-data

    .line 532
    :array_1a
    .array-data 0x1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x33t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x1et
        0x66t
    .end array-data

    .line 533
    :array_1b
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x2t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 537
    :array_1c
    .array-data 0x1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x39t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
    .end array-data

    .line 538
    nop

    :array_1d
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 539
    nop

    :array_1e
    .array-data 0x1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x33t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
    .end array-data

    .line 540
    nop

    :array_1f
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x2t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 544
    nop

    :array_20
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 545
    nop

    :array_21
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 546
    nop

    :array_22
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 547
    nop

    :array_23
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 550
    nop

    :array_24
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x38t
    .end array-data

    .line 551
    nop

    :array_25
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 552
    nop

    :array_26
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x38t
    .end array-data

    .line 553
    nop

    :array_27
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 556
    nop

    :array_28
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 557
    nop

    :array_29
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 558
    nop

    :array_2a
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 559
    nop

    :array_2b
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 570
    nop

    :array_2c
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 571
    :array_2d
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 572
    :array_2e
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 573
    :array_2f
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 576
    :array_30
    .array-data 0x1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 577
    :array_31
    .array-data 0x1
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 578
    :array_32
    .array-data 0x1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 579
    :array_33
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 582
    :array_34
    .array-data 0x1
        0x12t
        0x3t
        0x21t
        0x41t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 583
    nop

    :array_35
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 584
    nop

    :array_36
    .array-data 0x1
        0x12t
        0x13t
        0x21t
        0x41t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 585
    nop

    :array_37
    .array-data 0x1
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 588
    nop

    :array_38
    .array-data 0x1
        0x12t
        0x1t
        0x21t
        0x42t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 589
    :array_39
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 590
    :array_3a
    .array-data 0x1
        0x12t
        0x11t
        0x21t
        0x42t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 591
    :array_3b
    .array-data 0x1
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 594
    :array_3c
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xet
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 595
    :array_3d
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 596
    :array_3e
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xet
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 597
    :array_3f
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 600
    :array_40
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 601
    :array_41
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 602
    :array_42
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 603
    :array_43
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 606
    :array_44
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x21t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 607
    nop

    :array_45
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 608
    nop

    :array_46
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x21t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 609
    nop

    :array_47
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 612
    nop

    :array_48
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 613
    :array_49
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 614
    :array_4a
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 615
    :array_4b
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 618
    :array_4c
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 619
    nop

    :array_4d
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 620
    nop

    :array_4e
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 621
    nop

    :array_4f
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 624
    nop

    :array_50
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 625
    nop

    :array_51
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 626
    nop

    :array_52
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 627
    nop

    :array_53
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 630
    nop

    :array_54
    .array-data 0x1
        0x12t
        0x2t
        0x21t
        0x53t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x2t
        0x12t
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 631
    nop

    :array_55
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 632
    nop

    :array_56
    .array-data 0x1
        0x12t
        0x12t
        0x21t
        0x53t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x2t
        0x12t
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 633
    nop

    :array_57
    .array-data 0x1
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 636
    nop

    :array_58
    .array-data 0x1
        0xf2t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0xf8t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 637
    :array_59
    .array-data 0x1
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 638
    :array_5a
    .array-data 0x1
        0xf2t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0xf8t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 639
    :array_5b
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 820
    :array_5c
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
        0x0t
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 821
    :array_5d
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 822
    :array_5e
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 823
    :array_5f
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 825
    :array_60
    .array-data 0x1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
        0x0t
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 826
    :array_61
    .array-data 0x1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 828
    :array_62
    .array-data 0x1
        0x12t
        0x10t
        0xft
        0x0t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 829
    nop

    :array_63
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 830
    nop

    :array_64
    .array-data 0x1
        0x12t
        0x3t
        0x21t
        0x41t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 831
    nop

    :array_65
    .array-data 0x1
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 833
    nop

    :array_66
    .array-data 0x1
        0x12t
        0x10t
        0xft
        0x0t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 834
    :array_67
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 835
    :array_68
    .array-data 0x1
        0x12t
        0x1t
        0x21t
        0x42t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 836
    :array_69
    .array-data 0x1
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 838
    :array_6a
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
        0x0t
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 839
    :array_6b
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 840
    :array_6c
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xet
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 841
    :array_6d
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 843
    :array_6e
    .array-data 0x1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
        0x0t
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x38t
        0x3t
        0xbt
        0x0t
    .end array-data

    .line 844
    nop

    :array_6f
    .array-data 0x1
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 845
    nop

    :array_70
    .array-data 0x1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x38t
        0x3t
        0xbt
        0x0t
    .end array-data

    .line 846
    nop

    :array_71
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 848
    nop

    :array_72
    .array-data 0x1
        0xf2t
        0x20t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
        0x0t
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x38t
        0x3t
        0xbt
        0x0t
    .end array-data

    .line 849
    nop

    :array_73
    .array-data 0x1
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 850
    nop

    :array_74
    .array-data 0x1
        0xf2t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x38t
        0x3t
        0xbt
        0x0t
    .end array-data

    .line 851
    nop

    :array_75
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 853
    nop

    :array_76
    .array-data 0x1
        0xf2t
        0x20t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
        0x0t
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x71t
        0x10t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x1t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x38t
        0x3t
        0xbt
        0x0t
    .end array-data

    .line 854
    nop

    :array_77
    .array-data 0x1
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 855
    nop

    :array_78
    .array-data 0x1
        0xf2t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xet
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x71t
        0x10t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x1t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x38t
        0x3t
        0xbt
        0x0t
    .end array-data

    .line 856
    nop

    :array_79
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 861
    nop

    :array_7a
    .array-data 0x1
        0x39t
        0x66t
        0x8t
        0x0t
        0x39t
        0x66t
        0x4t
        0x0t
        0x12t
        0x16t
        0x12t
        0x6t
        0x12t
        0x6t
        0xft
        0x6t
    .end array-data

    .line 862
    :array_7b
    .array-data 0x1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 863
    :array_7c
    .array-data 0x1
        0x39t
        0x66t
        0x8t
        0x0t
        0x39t
        0x66t
        0x4t
        0x0t
        0x12t
        0x16t
        0xft
        0x6t
        0x12t
        0xf6t
        0x28t
        0xfet
    .end array-data

    .line 864
    :array_7d
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 867
    :array_7e
    .array-data 0x1
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        0xedt
        0xfft
    .end array-data

    .line 868
    :array_7f
    .array-data 0x1
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 869
    :array_80
    .array-data 0x1
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        0xedt
        0xfft
    .end array-data

    .line 870
    :array_81
    .array-data 0x1
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 873
    :array_82
    .array-data 0x1
        0x80t
        0x0t
        0x39t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x35t
        0x88t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 874
    :array_83
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 875
    :array_84
    .array-data 0x1
        0x80t
        0x0t
        0x39t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x35t
        0x87t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 876
    :array_85
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 879
    :array_86
    .array-data 0x1
        0x39t
        0x66t
        0x7t
        0x0t
        0x39t
        0x66t
        0x3t
        0x0t
        0x12t
        0x6t
        0x12t
        0x6t
        0xft
        0x6t
    .end array-data

    .line 880
    nop

    :array_87
    .array-data 0x1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 881
    nop

    :array_88
    .array-data 0x1
        0x39t
        0x66t
        0x7t
        0x0t
        0x39t
        0x66t
        0x3t
        0x0t
        0xft
        0x6t
        0x12t
        0xf6t
        0x28t
        0xfet
    .end array-data

    .line 882
    nop

    :array_89
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 885
    nop

    :array_8a
    .array-data 0x1
        0x54t
        0x66t
        0x66t
        0x66t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x39t
        0x4t
        0x4t
        0x0t
        0x12t
        0x14t
        0xft
        0x4t
    .end array-data

    .line 886
    :array_8b
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 887
    :array_8c
    .array-data 0x1
        0x54t
        0x66t
        0x66t
        0x66t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x38t
        0x4t
        0x4t
        0x0t
        0x12t
        0x14t
        0xft
        0x4t
    .end array-data

    .line 888
    :array_8d
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 891
    :array_8e
    .array-data 0x1
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        0xedt
        0xfft
    .end array-data

    .line 892
    nop

    :array_8f
    .array-data 0x1
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 893
    nop

    :array_90
    .array-data 0x1
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        0xedt
        0xfft
    .end array-data

    .line 894
    nop

    :array_91
    .array-data 0x1
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 897
    nop

    :array_92
    .array-data 0x1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x33t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x1et
        0x66t
    .end array-data

    .line 898
    :array_93
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
    .end array-data

    .line 899
    :array_94
    .array-data 0x1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x39t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x1et
        0x66t
    .end array-data

    .line 900
    :array_95
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x3t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 904
    :array_96
    .array-data 0x1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x33t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
    .end array-data

    .line 905
    nop

    :array_97
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 906
    nop

    :array_98
    .array-data 0x1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x39t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
    .end array-data

    .line 907
    nop

    :array_99
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x3t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 911
    nop

    :array_9a
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 912
    nop

    :array_9b
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 913
    nop

    :array_9c
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 914
    nop

    :array_9d
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 917
    nop

    :array_9e
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x38t
    .end array-data

    .line 918
    nop

    :array_9f
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 919
    nop

    :array_a0
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x38t
    .end array-data

    .line 920
    nop

    :array_a1
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 923
    nop

    :array_a2
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 924
    nop

    :array_a3
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 925
    nop

    :array_a4
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 926
    nop

    :array_a5
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 937
    nop

    :array_a6
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 938
    :array_a7
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 939
    :array_a8
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 940
    :array_a9
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 943
    :array_aa
    .array-data 0x1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 944
    :array_ab
    .array-data 0x1
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 945
    :array_ac
    .array-data 0x1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 946
    :array_ad
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 948
    :array_ae
    .array-data 0x1
        0x12t
        0x13t
        0x21t
        0x41t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 949
    nop

    :array_af
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 950
    nop

    :array_b0
    .array-data 0x1
        0x12t
        0x3t
        0x21t
        0x41t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 951
    nop

    :array_b1
    .array-data 0x1
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 954
    nop

    :array_b2
    .array-data 0x1
        0x12t
        0x11t
        0x21t
        0x42t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 955
    :array_b3
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 956
    :array_b4
    .array-data 0x1
        0x12t
        0x1t
        0x21t
        0x42t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 957
    :array_b5
    .array-data 0x1
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 960
    :array_b6
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xet
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 961
    :array_b7
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 962
    :array_b8
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xet
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 963
    :array_b9
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 966
    :array_ba
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 967
    :array_bb
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 968
    :array_bc
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 969
    :array_bd
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 972
    :array_be
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 973
    nop

    :array_bf
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 974
    nop

    :array_c0
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 975
    nop

    :array_c1
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 978
    nop

    :array_c2
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 979
    :array_c3
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 980
    :array_c4
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 981
    :array_c5
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 984
    :array_c6
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 985
    nop

    :array_c7
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 986
    nop

    :array_c8
    .array-data 0x1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 987
    nop

    :array_c9
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 990
    nop

    :array_ca
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 991
    nop

    :array_cb
    .array-data 0x1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 992
    nop

    :array_cc
    .array-data 0x1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x90t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 993
    nop

    :array_cd
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 996
    nop

    :array_ce
    .array-data 0x1
        0x12t
        0x12t
        0x21t
        0x53t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x2t
        0x12t
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 997
    nop

    :array_cf
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 998
    nop

    :array_d0
    .array-data 0x1
        0x12t
        0x2t
        0x21t
        0x53t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x2t
        0x12t
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 999
    nop

    :array_d1
    .array-data 0x1
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1002
    nop

    :array_d2
    .array-data 0x1
        0xf2t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0xf8t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 1003
    :array_d3
    .array-data 0x1
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1004
    :array_d4
    .array-data 0x1
        0xf2t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0xf8t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 1005
    :array_d5
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data
.end method

.method public static unzip(Ljava/io/File;Ljava/lang/String;)V
    .locals 18
    .parameter "apk"
    .parameter "dirr"

    .prologue
    .line 2583
    const/4 v6, 0x0

    .line 2584
    .local v6, found1:Z
    move-object/from16 v2, p1

    .line 2586
    .local v2, dir:Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 2587
    .local v5, fin:Ljava/io/FileInputStream;
    new-instance v12, Ljava/util/zip/ZipInputStream;

    invoke-direct {v12, v5}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2588
    .local v12, zin:Ljava/util/zip/ZipInputStream;
    const/4 v11, 0x0

    .line 2590
    .local v11, ze:Ljava/util/zip/ZipEntry;
    invoke-virtual {v12}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v11

    .line 2591
    const/4 v10, 0x1

    .line 2592
    .local v10, search:Z
    :goto_0
    if-eqz v11, :cond_2

    if-eqz v10, :cond_2

    .line 2598
    invoke-virtual {v11}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v8

    .line 2600
    .local v8, haystack:Ljava/lang/String;
    const-string v14, "classes.dex"

    invoke-virtual {v8, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 2602
    new-instance v7, Ljava/io/FileOutputStream;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "classes.dex"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v7, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 2604
    .local v7, fout:Ljava/io/FileOutputStream;
    const/16 v14, 0x800

    new-array v1, v14, [B

    .line 2607
    .local v1, buffer:[B
    :goto_1
    invoke-virtual {v12, v1}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v9

    .local v9, length:I
    const/4 v14, -0x1

    if-eq v9, v14, :cond_0

    .line 2608
    const/4 v14, 0x0

    invoke-virtual {v7, v1, v14, v9}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2627
    .end local v1           #buffer:[B
    .end local v5           #fin:Ljava/io/FileInputStream;
    .end local v7           #fout:Ljava/io/FileOutputStream;
    .end local v8           #haystack:Ljava/lang/String;
    .end local v9           #length:I
    .end local v10           #search:Z
    .end local v11           #ze:Ljava/util/zip/ZipEntry;
    .end local v12           #zin:Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v3

    .line 2629
    .local v3, e:Ljava/lang/Exception;
    :try_start_1
    new-instance v13, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 2633
    .local v13, zipFile:Lnet/lingala/zip4j/core/ZipFile;
    const-string v14, "classes.dex"

    invoke-virtual {v13, v14, v2}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 2643
    .end local v13           #zipFile:Lnet/lingala/zip4j/core/ZipFile;
    :goto_2
    sget-object v14, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Exception e"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2647
    .end local v3           #e:Ljava/lang/Exception;
    :goto_3
    return-void

    .line 2611
    .restart local v1       #buffer:[B
    .restart local v5       #fin:Ljava/io/FileInputStream;
    .restart local v7       #fout:Ljava/io/FileOutputStream;
    .restart local v8       #haystack:Ljava/lang/String;
    .restart local v9       #length:I
    .restart local v10       #search:Z
    .restart local v11       #ze:Ljava/util/zip/ZipEntry;
    .restart local v12       #zin:Ljava/util/zip/ZipInputStream;
    :cond_0
    const/4 v14, 0x3

    :try_start_2
    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "chmod"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string v16, "777"

    aput-object v16, v14, v15

    const/4 v15, 0x2

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "classes.dex"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v14}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2612
    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "chown"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string v16, "0.0"

    aput-object v16, v14, v15

    const/4 v15, 0x2

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "classes.dex"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v14}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2613
    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "chown"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string v16, "0:0"

    aput-object v16, v14, v15

    const/4 v15, 0x2

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "classes.dex"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v14}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2614
    invoke-virtual {v12}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 2615
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 2616
    const/4 v6, 0x1

    .line 2618
    .end local v1           #buffer:[B
    .end local v7           #fout:Ljava/io/FileOutputStream;
    .end local v9           #length:I
    :cond_1
    if-eqz v6, :cond_3

    .line 2619
    const/4 v10, 0x0

    .line 2625
    .end local v8           #haystack:Ljava/lang/String;
    :cond_2
    invoke-virtual {v12}, Ljava/util/zip/ZipInputStream;->close()V

    .line 2626
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_3

    .line 2622
    .restart local v8       #haystack:Ljava/lang/String;
    :cond_3
    invoke-virtual {v12}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v11

    .line 2624
    goto/16 :goto_0

    .line 2636
    .end local v5           #fin:Ljava/io/FileInputStream;
    .end local v8           #haystack:Ljava/lang/String;
    .end local v10           #search:Z
    .end local v11           #ze:Ljava/util/zip/ZipEntry;
    .end local v12           #zin:Ljava/util/zip/ZipInputStream;
    .restart local v3       #e:Ljava/lang/Exception;
    :catch_1
    move-exception v4

    .line 2637
    .local v4, e1:Lnet/lingala/zip4j/exception/ZipException;
    sget-object v14, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Error classes.dex decompress! "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2638
    sget-object v14, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Exception e1"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2639
    .end local v4           #e1:Lnet/lingala/zip4j/exception/ZipException;
    :catch_2
    move-exception v4

    .line 2640
    .local v4, e1:Ljava/lang/Exception;
    sget-object v14, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Error classes.dex decompress! "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2641
    sget-object v14, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Exception e1"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2
.end method
