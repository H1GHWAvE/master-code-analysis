.class public Lcom/chelpus/root/utils/odexSystemApps;
.super Ljava/lang/Object;
.source "odexSystemApps.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 17
    .parameter "paramArrayOfString"

    .prologue
    .line 13
    new-instance v9, Lcom/chelpus/root/utils/odexSystemApps$1;

    invoke-direct {v9}, Lcom/chelpus/root/utils/odexSystemApps$1;-><init>()V

    invoke-static {v9}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 14
    const/4 v9, 0x0

    aget-object v8, p0, v9

    .line 15
    .local v8, toolsfiledir:Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    const-string v9, "/system/app"

    invoke-direct {v7, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 16
    .local v7, systemdir:Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 17
    .local v4, files:[Ljava/io/File;
    array-length v11, v4

    const/4 v9, 0x0

    move v10, v9

    :goto_0
    if-ge v10, v11, :cond_5

    aget-object v3, v4, v10

    .line 18
    .local v3, file:Ljava/io/File;
    sget v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->api:I

    const/16 v12, 0x15

    if-lt v9, v12, :cond_2

    .line 19
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 20
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 21
    .local v0, appFiles:[Ljava/io/File;
    if-eqz v0, :cond_3

    array-length v9, v0

    if-lez v9, :cond_3

    .line 22
    array-length v12, v0

    const/4 v9, 0x0

    :goto_1
    if-ge v9, v12, :cond_3

    aget-object v5, v0, v9

    .line 23
    .local v5, inFile:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    const-string v14, ".apk"

    invoke-virtual {v13, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 24
    new-instance v6, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    const-string v14, "0"

    invoke-static {v13, v14}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v6, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 25
    .local v6, odex:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 26
    .local v1, dalvik:Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 28
    :try_start_0
    invoke-static {v1, v6}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 29
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v13

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v15

    cmp-long v13, v13, v15

    if-eqz v13, :cond_1

    .line 30
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 31
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "Not enought space!"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 22
    .end local v1           #dalvik:Ljava/io/File;
    .end local v6           #odex:Ljava/io/File;
    :cond_0
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 33
    .restart local v1       #dalvik:Ljava/io/File;
    .restart local v6       #odex:Ljava/io/File;
    :cond_1
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "chmod"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "644"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v13}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 34
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "chown"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0.0"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v13}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 35
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "chown"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0:0"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v13}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 36
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 38
    :catch_0
    move-exception v2

    .line 39
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 40
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "IO Exception!"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 41
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 51
    .end local v0           #appFiles:[Ljava/io/File;
    .end local v1           #dalvik:Ljava/io/File;
    .end local v2           #e:Ljava/lang/Exception;
    .end local v5           #inFile:Ljava/io/File;
    .end local v6           #odex:Ljava/io/File;
    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    const-string v12, ".apk"

    invoke-virtual {v9, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 52
    new-instance v6, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    const-string v12, "0"

    invoke-static {v9, v12}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 53
    .restart local v6       #odex:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 54
    .restart local v1       #dalvik:Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 56
    :try_start_1
    invoke-static {v1, v6}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 57
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v12

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v14

    cmp-long v9, v12, v14

    if-eqz v9, :cond_4

    .line 58
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 59
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "Not enought space!"

    invoke-virtual {v9, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 17
    .end local v1           #dalvik:Ljava/io/File;
    .end local v6           #odex:Ljava/io/File;
    :cond_3
    :goto_3
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    goto/16 :goto_0

    .line 61
    .restart local v1       #dalvik:Ljava/io/File;
    .restart local v6       #odex:Ljava/io/File;
    :cond_4
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v12, 0x0

    const-string v13, "chmod"

    aput-object v13, v9, v12

    const/4 v12, 0x1

    const-string v13, "644"

    aput-object v13, v9, v12

    const/4 v12, 0x2

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v9, v12

    invoke-static {v9}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 62
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v12, 0x0

    const-string v13, "chown"

    aput-object v13, v9, v12

    const/4 v12, 0x1

    const-string v13, "0.0"

    aput-object v13, v9, v12

    const/4 v12, 0x2

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v9, v12

    invoke-static {v9}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 63
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v12, 0x0

    const-string v13, "chown"

    aput-object v13, v9, v12

    const/4 v12, 0x1

    const-string v13, "0:0"

    aput-object v13, v9, v12

    const/4 v12, 0x2

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v9, v12

    invoke-static {v9}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 64
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 66
    :catch_1
    move-exception v2

    .line 67
    .restart local v2       #e:Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 68
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "IO Exception!"

    invoke-virtual {v9, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 69
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 76
    .end local v1           #dalvik:Ljava/io/File;
    .end local v2           #e:Ljava/lang/Exception;
    .end local v3           #file:Ljava/io/File;
    .end local v6           #odex:Ljava/io/File;
    :cond_5
    new-instance v7, Ljava/io/File;

    .end local v7           #systemdir:Ljava/io/File;
    const-string v9, "/system/priv-app"

    invoke-direct {v7, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 77
    .restart local v7       #systemdir:Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_b

    .line 78
    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 79
    array-length v11, v4

    const/4 v9, 0x0

    move v10, v9

    :goto_4
    if-ge v10, v11, :cond_b

    aget-object v3, v4, v10

    .line 80
    .restart local v3       #file:Ljava/io/File;
    sget v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->api:I

    const/16 v12, 0x15

    if-lt v9, v12, :cond_8

    .line 81
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_9

    .line 82
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 83
    .restart local v0       #appFiles:[Ljava/io/File;
    if-eqz v0, :cond_9

    array-length v9, v0

    if-lez v9, :cond_9

    .line 84
    array-length v12, v0

    const/4 v9, 0x0

    :goto_5
    if-ge v9, v12, :cond_9

    aget-object v5, v0, v9

    .line 85
    .restart local v5       #inFile:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    const-string v14, ".apk"

    invoke-virtual {v13, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 86
    new-instance v6, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    const-string v14, "0"

    invoke-static {v13, v14}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v6, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 87
    .restart local v6       #odex:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 88
    .restart local v1       #dalvik:Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_6

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 90
    :try_start_2
    invoke-static {v1, v6}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 91
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v13

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v15

    cmp-long v13, v13, v15

    if-eqz v13, :cond_7

    .line 92
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 93
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "Not enought space!"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 84
    .end local v1           #dalvik:Ljava/io/File;
    .end local v6           #odex:Ljava/io/File;
    :cond_6
    :goto_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 95
    .restart local v1       #dalvik:Ljava/io/File;
    .restart local v6       #odex:Ljava/io/File;
    :cond_7
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "chmod"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "644"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v13}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 96
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "chown"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0.0"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v13}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 97
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "chown"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0:0"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v13}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 98
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_6

    .line 100
    :catch_2
    move-exception v2

    .line 101
    .restart local v2       #e:Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 102
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "IO Exception!"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 103
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 113
    .end local v0           #appFiles:[Ljava/io/File;
    .end local v1           #dalvik:Ljava/io/File;
    .end local v2           #e:Ljava/lang/Exception;
    .end local v5           #inFile:Ljava/io/File;
    .end local v6           #odex:Ljava/io/File;
    :cond_8
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    const-string v12, ".apk"

    invoke-virtual {v9, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 114
    new-instance v6, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    const-string v12, "0"

    invoke-static {v9, v12}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 115
    .restart local v6       #odex:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 116
    .restart local v1       #dalvik:Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_9

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_9

    .line 118
    :try_start_3
    invoke-static {v1, v6}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 119
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v12

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v14

    cmp-long v9, v12, v14

    if-eqz v9, :cond_a

    .line 120
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 121
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "Not enought space!"

    invoke-virtual {v9, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 79
    .end local v1           #dalvik:Ljava/io/File;
    .end local v6           #odex:Ljava/io/File;
    :cond_9
    :goto_7
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    goto/16 :goto_4

    .line 123
    .restart local v1       #dalvik:Ljava/io/File;
    .restart local v6       #odex:Ljava/io/File;
    :cond_a
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v12, 0x0

    const-string v13, "chmod"

    aput-object v13, v9, v12

    const/4 v12, 0x1

    const-string v13, "644"

    aput-object v13, v9, v12

    const/4 v12, 0x2

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v9, v12

    invoke-static {v9}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 124
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v12, 0x0

    const-string v13, "chown"

    aput-object v13, v9, v12

    const/4 v12, 0x1

    const-string v13, "0.0"

    aput-object v13, v9, v12

    const/4 v12, 0x2

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v9, v12

    invoke-static {v9}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 125
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v12, 0x0

    const-string v13, "chown"

    aput-object v13, v9, v12

    const/4 v12, 0x1

    const-string v13, "0:0"

    aput-object v13, v9, v12

    const/4 v12, 0x2

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v9, v12

    invoke-static {v9}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 126
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_7

    .line 128
    :catch_3
    move-exception v2

    .line 129
    .restart local v2       #e:Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 130
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "IO Exception!"

    invoke-virtual {v9, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 131
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_7

    .line 139
    .end local v1           #dalvik:Ljava/io/File;
    .end local v2           #e:Ljava/lang/Exception;
    .end local v3           #file:Ljava/io/File;
    .end local v6           #odex:Ljava/io/File;
    :cond_b
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 140
    return-void
.end method
