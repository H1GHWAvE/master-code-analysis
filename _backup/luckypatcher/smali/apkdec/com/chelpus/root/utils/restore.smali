.class public Lcom/chelpus/root/utils/restore;
.super Ljava/lang/Object;
.source "restore.java"


# static fields
.field private static dalvikDexIn:Ljava/lang/String;

.field private static dalvikDexIn2:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-string v0, "/cache/dalvik-cache/data@app@zamenitetodelo-1.apk@classes.dex"

    sput-object v0, Lcom/chelpus/root/utils/restore;->dalvikDexIn2:Ljava/lang/String;

    .line 11
    const-string v0, "/data/dalvik-cache/data@app@zamenitetodelo-1.apk@classes.dex"

    sput-object v0, Lcom/chelpus/root/utils/restore;->dalvikDexIn:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 11
    .parameter "paramArrayOfString"

    .prologue
    const/4 v10, 0x0

    .line 20
    invoke-static {}, Lcom/chelpus/Utils;->startRootJava()V

    .line 22
    sget-object v8, Lcom/chelpus/root/utils/restore;->dalvikDexIn:Ljava/lang/String;

    const-string v9, "zamenitetodelo"

    aget-object v10, p0, v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 26
    .local v2, dalvikDex:Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 27
    .local v5, localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 28
    .restart local v5       #localFile1:Ljava/io/File;
    :cond_0
    new-instance v6, Ljava/io/File;

    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 29
    .local v6, localFile2:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    move-object v6, v5

    .line 30
    :cond_1
    const-string v8, "data@app"

    const-string v9, "mnt@asec"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 31
    .local v3, dalvikDexTemp:Ljava/lang/String;
    const-string v8, ".apk@classes.dex"

    const-string v9, "@pkg.apk@classes.dex"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 32
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 33
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    move-object v6, v5

    .line 34
    :cond_2
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 35
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    move-object v6, v5

    .line 36
    :cond_3
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 37
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_4

    move-object v6, v5

    .line 39
    :cond_4
    sget-object v8, Lcom/chelpus/root/utils/restore;->dalvikDexIn2:Ljava/lang/String;

    const-string v9, "zamenitetodelo"

    const/4 v10, 0x0

    aget-object v10, p0, v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 40
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 41
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_5

    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 43
    .restart local v5       #localFile1:Ljava/io/File;
    :cond_5
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_6

    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 44
    .restart local v5       #localFile1:Ljava/io/File;
    :cond_6
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_7

    move-object v6, v5

    .line 45
    :cond_7
    const-string v8, "data@app"

    const-string v9, "mnt@asec"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 46
    const-string v8, ".apk@classes.dex"

    const-string v9, "@pkg.apk@classes.dex"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 47
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 48
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_8

    move-object v6, v5

    .line 49
    :cond_8
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 50
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_9

    move-object v6, v5

    .line 51
    :cond_9
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 52
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_a

    move-object v6, v5

    .line 54
    :cond_a
    sget-object v8, Lcom/chelpus/root/utils/restore;->dalvikDexIn:Ljava/lang/String;

    const-string v9, "zamenitetodelo"

    const/4 v10, 0x0

    aget-object v10, p0, v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 55
    const-string v8, "/data/"

    const-string v9, "/sd-ext/data/"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 56
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 57
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_b

    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 59
    .restart local v5       #localFile1:Ljava/io/File;
    :cond_b
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_c

    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 60
    .restart local v5       #localFile1:Ljava/io/File;
    :cond_c
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_d

    move-object v6, v5

    .line 61
    :cond_d
    const-string v8, "data@app"

    const-string v9, "mnt@asec"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 62
    const-string v8, ".apk@classes.dex"

    const-string v9, "@pkg.apk@classes.dex"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 63
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 64
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_e

    move-object v6, v5

    .line 65
    :cond_e
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 66
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_f

    move-object v6, v5

    .line 67
    :cond_f
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 68
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_10

    move-object v6, v5

    .line 70
    :cond_10
    sget-object v8, Lcom/chelpus/root/utils/restore;->dalvikDexIn2:Ljava/lang/String;

    const-string v9, "zamenitetodelo"

    const/4 v10, 0x0

    aget-object v10, p0, v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 71
    const-string v8, "/cache/"

    const-string v9, "/sd-ext/data/cache/"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 72
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 73
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_11

    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 75
    .restart local v5       #localFile1:Ljava/io/File;
    :cond_11
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_12

    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 76
    .restart local v5       #localFile1:Ljava/io/File;
    :cond_12
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_13

    move-object v6, v5

    .line 77
    :cond_13
    const-string v8, "data@app"

    const-string v9, "mnt@asec"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 78
    const-string v8, ".apk@classes.dex"

    const-string v9, "@pkg.apk@classes.dex"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 79
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 80
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_14

    move-object v6, v5

    .line 81
    :cond_14
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 82
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_15

    move-object v6, v5

    .line 83
    :cond_15
    new-instance v5, Ljava/io/File;

    .end local v5           #localFile1:Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 84
    .restart local v5       #localFile1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_16

    move-object v6, v5

    .line 85
    :cond_16
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_17

    new-instance v8, Ljava/io/FileNotFoundException;

    invoke-direct {v8}, Ljava/io/FileNotFoundException;-><init>()V

    throw v8
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 96
    .end local v3           #dalvikDexTemp:Ljava/lang/String;
    .end local v5           #localFile1:Ljava/io/File;
    .end local v6           #localFile2:Ljava/io/File;
    :catch_0
    move-exception v7

    .line 97
    .local v7, localFileNotFoundException:Ljava/io/FileNotFoundException;
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v9, "Error: Backup files are not found!"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 102
    .end local v7           #localFileNotFoundException:Ljava/io/FileNotFoundException;
    :goto_0
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 103
    return-void

    .line 88
    .restart local v3       #dalvikDexTemp:Ljava/lang/String;
    .restart local v5       #localFile1:Ljava/io/File;
    .restart local v6       #localFile2:Ljava/io/File;
    :cond_17
    :try_start_1
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "classes"

    const-string v10, "backup"

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 89
    .local v1, backTemp:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 90
    .local v0, backFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_18

    new-instance v8, Ljava/io/FileNotFoundException;

    invoke-direct {v8}, Ljava/io/FileNotFoundException;-><init>()V

    throw v8
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 99
    .end local v0           #backFile:Ljava/io/File;
    .end local v1           #backTemp:Ljava/lang/String;
    .end local v3           #dalvikDexTemp:Ljava/lang/String;
    .end local v5           #localFile1:Ljava/io/File;
    .end local v6           #localFile2:Ljava/io/File;
    :catch_1
    move-exception v4

    .line 100
    .local v4, e:Ljava/lang/Exception;
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception e"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 91
    .end local v4           #e:Ljava/lang/Exception;
    .restart local v0       #backFile:Ljava/io/File;
    .restart local v1       #backTemp:Ljava/lang/String;
    .restart local v3       #dalvikDexTemp:Ljava/lang/String;
    .restart local v5       #localFile1:Ljava/io/File;
    .restart local v6       #localFile2:Ljava/io/File;
    :cond_18
    :try_start_2
    invoke-static {v0, v6}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 92
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v9, "Restore - done!"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0
.end method
