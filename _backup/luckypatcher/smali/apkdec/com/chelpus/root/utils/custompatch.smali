.class public Lcom/chelpus/root/utils/custompatch;
.super Ljava/lang/Object;
.source "custompatch.java"


# static fields
.field public static ART:Z = false

.field static final BUFFER:I = 0x800

.field public static final MAGIC:[B = null

.field public static OdexPatch:Z = false

.field public static adler:I = 0x0

.field public static armv7:Z = false

.field public static arrayFile2:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static final beginTag:I = 0x0

.field public static classesFiles:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static final classesTag:I = 0x1

.field public static convert:Z = false

.field private static dataBase:Ljava/lang/String; = null

.field private static dataBaseExist:Z = false

.field public static dir:Ljava/lang/String; = null

.field public static dir2:Ljava/lang/String; = null

.field public static dirapp:Ljava/lang/String; = null

.field private static final endTag:I = 0x4

.field private static final fileInApkTag:I = 0xe

.field public static fixunpack:Z = false

.field public static goodResult:Z = false

.field private static group:Ljava/lang/String; = null

.field private static final libTagALL:I = 0x2

.field private static final libTagARMEABI:I = 0x6

.field private static final libTagARMEABIV7A:I = 0x7

.field private static final libTagMIPS:I = 0x8

.field private static final libTagx86:I = 0x9

.field public static localFile2:Ljava/io/File; = null

.field public static log:Ljava/lang/String; = null

.field public static manualpatch:Z = false

.field public static multidex:Z = false

.field public static multilib_patch:Z = false

.field public static odex:Z = false

.field private static final odexTag:I = 0xa

.field public static odexpatch:Z = false

.field private static final odexpatchTag:I = 0xb

.field private static final otherfilesTag:I = 0x3

.field private static final packageTag:I = 0x5

.field private static pat:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;",
            ">;"
        }
    .end annotation
.end field

.field public static patchteil:Z = false

.field public static pkgName:Ljava/lang/String; = null

.field public static sddir:Ljava/lang/String; = null

.field private static search:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field private static searchStr:Ljava/lang/String; = null

.field private static ser:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final set_copy_file_Tag:I = 0xf

.field private static final set_permissions_Tag:I = 0xd

.field private static final sqlTag:I = 0xc

.field public static system:Z

.field public static tag:I

.field public static uid:Ljava/lang/String;

.field public static unpack:Z

.field private static withFramework:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 38
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->MAGIC:[B

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    .line 42
    sput-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    .line 43
    sput-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    .line 44
    sput-object v2, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    .line 45
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->patchteil:Z

    .line 46
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 47
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->fixunpack:Z

    .line 48
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->manualpatch:Z

    .line 49
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 50
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    .line 51
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->dir2:Ljava/lang/String;

    .line 52
    const-string v0, "/data/app/"

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    .line 53
    const-string v0, "/data/app/"

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->sddir:Ljava/lang/String;

    .line 54
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->uid:Ljava/lang/String;

    .line 55
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->system:Z

    .line 56
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->odexpatch:Z

    .line 57
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->OdexPatch:Z

    .line 58
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->armv7:Z

    .line 59
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->ART:Z

    .line 60
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 79
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->dataBaseExist:Z

    .line 80
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->dataBase:Ljava/lang/String;

    .line 81
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    .line 82
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    .line 83
    const/4 v0, 0x1

    sput-boolean v0, Lcom/chelpus/root/utils/custompatch;->withFramework:Z

    .line 84
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    .line 85
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->log:Ljava/lang/String;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    .line 90
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->multidex:Z

    .line 91
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    .line 92
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    return-void

    .line 38
    nop

    :array_0
    .array-data 0x1
        0x64t
        0x65t
        0x79t
        0xat
        0x30t
        0x33t
        0x35t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addToLog(Ljava/lang/String;)V
    .locals 2
    .parameter "str"

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/chelpus/root/utils/custompatch;->log:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->log:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public static clearTemp()V
    .locals 7

    .prologue
    .line 1894
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/AndroidManifest.xml"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1895
    .local v3, tmp:Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1896
    .local v2, tempdex:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1897
    :cond_0
    sget-object v4, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 1898
    sget-object v4, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 1899
    .local v0, cl:Ljava/io/File;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1900
    new-instance v2, Ljava/io/File;

    .end local v2           #tempdex:Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1901
    .restart local v2       #tempdex:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1932
    .end local v0           #cl:Ljava/io/File;
    .end local v2           #tempdex:Ljava/io/File;
    :catch_0
    move-exception v1

    .line 1934
    .local v1, e:Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1936
    .end local v1           #e:Ljava/lang/Exception;
    :cond_2
    :goto_1
    return-void

    .line 1904
    .restart local v2       #tempdex:Ljava/io/File;
    :cond_3
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1905
    new-instance v2, Ljava/io/File;

    .end local v2           #tempdex:Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1906
    .restart local v2       #tempdex:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1907
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes1.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1908
    new-instance v2, Ljava/io/File;

    .end local v2           #tempdex:Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1909
    .restart local v2       #tempdex:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1910
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes2.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1911
    new-instance v2, Ljava/io/File;

    .end local v2           #tempdex:Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1912
    .restart local v2       #tempdex:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1913
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes3.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1914
    new-instance v2, Ljava/io/File;

    .end local v2           #tempdex:Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1915
    .restart local v2       #tempdex:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1916
    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes4.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1917
    new-instance v2, Ljava/io/File;

    .end local v2           #tempdex:Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1918
    .restart local v2       #tempdex:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1919
    :cond_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes5.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1920
    new-instance v2, Ljava/io/File;

    .end local v2           #tempdex:Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1921
    .restart local v2       #tempdex:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1922
    :cond_9
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes6.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1923
    new-instance v2, Ljava/io/File;

    .end local v2           #tempdex:Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1924
    .restart local v2       #tempdex:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1925
    :cond_a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes.dex.apk"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1926
    new-instance v2, Ljava/io/File;

    .end local v2           #tempdex:Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1927
    .restart local v2       #tempdex:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1928
    :cond_b
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes.dex.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1929
    new-instance v2, Ljava/io/File;

    .end local v2           #tempdex:Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1930
    .restart local v2       #tempdex:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method public static main([Ljava/lang/String;)V
    .locals 73
    .parameter "paramArrayOfString"

    .prologue
    .line 99
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SU Java-Code Running! "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v7, Lcom/chelpus/root/utils/custompatch$1;

    invoke-direct {v7}, Lcom/chelpus/root/utils/custompatch$1;-><init>()V

    .line 100
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getEnclosingClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 99
    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 102
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 103
    const/4 v2, 0x0

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 105
    :try_start_1
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-static {v2}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V

    .line 106
    const/4 v2, 0x2

    aget-object v2, p0, v2

    const-string v7, "rw"

    invoke-static {v2, v7}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 111
    :goto_0
    :try_start_2
    const-string v2, ""

    const-string v7, ""

    invoke-static {v2, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 117
    :goto_1
    const/16 v2, 0x9

    :try_start_3
    aget-object v2, p0, v2

    if-eqz v2, :cond_0

    const/16 v2, 0x9

    aget-object v2, p0, v2

    const-string v7, "ART"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->ART:Z

    .line 118
    :cond_0
    const/16 v2, 0xa

    aget-object v2, p0, v2

    if-eqz v2, :cond_1

    const/16 v2, 0xa

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->uid:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 123
    :cond_1
    :goto_2
    :try_start_4
    new-instance v2, Ljava/io/File;

    const/4 v7, 0x4

    aget-object v7, p0, v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v34

    .line 124
    .local v34, files:[Ljava/io/File;
    move-object/from16 v0, v34

    array-length v7, v0

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v7, :cond_3

    aget-object v32, v34, v2

    .line 125
    .local v32, file:Ljava/io/File;
    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->isFile()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v70, "busybox"

    move-object/from16 v0, v70

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v70, "reboot"

    move-object/from16 v0, v70

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v70, "dalvikvm"

    move-object/from16 v0, v70

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 126
    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->delete()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 124
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 107
    .end local v32           #file:Ljava/io/File;
    .end local v34           #files:[Ljava/io/File;
    :catch_0
    move-exception v29

    .line 108
    .local v29, e:Ljava/lang/Exception;
    :try_start_5
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 1183
    .end local v29           #e:Ljava/lang/Exception;
    :catch_1
    move-exception v29

    .restart local v29       #e:Ljava/lang/Exception;
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V

    .line 1185
    .end local v29           #e:Ljava/lang/Exception;
    :goto_4
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->log:Ljava/lang/String;

    invoke-static {v2}, Lcom/chelpus/Utils;->sendFromRootCP(Ljava/lang/String;)Z

    .line 1188
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    .line 1189
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 1190
    return-void

    .line 112
    :catch_2
    move-exception v29

    .line 113
    .local v29, e:Ljava/lang/UnsatisfiedLinkError;
    :try_start_6
    const-string v2, "withoutFramework"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 114
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->withFramework:Z

    goto/16 :goto_1

    .line 119
    .end local v29           #e:Ljava/lang/UnsatisfiedLinkError;
    :catch_3
    move-exception v29

    .line 120
    .local v29, e:Ljava/lang/Exception;
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 128
    .end local v29           #e:Ljava/lang/Exception;
    :catch_4
    move-exception v29

    .line 129
    .restart local v29       #e:Ljava/lang/Exception;
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V

    .line 131
    .end local v29           #e:Ljava/lang/Exception;
    :cond_3
    const/4 v2, 0x3

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->sddir:Ljava/lang/String;

    .line 132
    const/4 v2, 0x4

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    .line 133
    const/4 v2, 0x4

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->dir2:Ljava/lang/String;

    .line 134
    const/4 v2, 0x2

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    .line 135
    invoke-static {}, Lcom/chelpus/root/utils/custompatch;->clearTemp()V

    .line 136
    const-string v22, ""

    .line 137
    .local v22, dalvikfixfile:Ljava/lang/String;
    const-string v35, ""

    .line 138
    .local v35, finalText:Ljava/lang/String;
    const-string v15, ""

    .line 140
    .local v15, beginText:Ljava/lang/String;
    const/16 v31, 0x0

    .local v31, error:Z
    const/16 v30, 0x0

    .local v30, end:Z
    const/16 v21, 0x0

    .line 141
    .local v21, dalvikfix:Z
    const/4 v2, 0x6

    aget-object v2, p0, v2

    const-string v7, "not_system"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->system:Z

    .line 142
    :cond_4
    const/4 v2, 0x6

    aget-object v2, p0, v2

    const-string v7, "system"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->system:Z

    .line 143
    :cond_5
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->system:Z

    if-eqz v2, :cond_6

    .line 144
    new-instance v10, Ljava/io/File;

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    invoke-direct {v10, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 145
    .local v10, appapk:Ljava/io/File;
    new-instance v11, Ljava/io/File;

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-static {v2, v7}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v11, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 146
    .local v11, appodex:Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {v10}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 147
    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odexpatch:Z

    .line 148
    sput-object v11, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 149
    const-string v2, "\nOdex Application.\nOnly ODEX patch is enabled.\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 154
    .end local v10           #appapk:Ljava/io/File;
    .end local v11           #appodex:Ljava/io/File;
    :cond_6
    const-string v44, ""

    .line 155
    .local v44, line:Ljava/lang/String;
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->OdexPatch:Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 157
    :try_start_7
    new-instance v37, Ljava/io/FileInputStream;

    const/4 v2, 0x1

    aget-object v2, p0, v2

    move-object/from16 v0, v37

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 158
    .local v37, fis1:Ljava/io/FileInputStream;
    new-instance v39, Ljava/io/InputStreamReader;

    const-string v2, "UTF-8"

    move-object/from16 v0, v39

    move-object/from16 v1, v37

    invoke-direct {v0, v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 159
    .local v39, isr1:Ljava/io/InputStreamReader;
    new-instance v17, Ljava/io/BufferedReader;

    move-object/from16 v0, v17

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 160
    .local v17, br1:Ljava/io/BufferedReader;
    :cond_7
    :goto_5
    invoke-virtual/range {v17 .. v17}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v44

    if-eqz v44, :cond_34

    .line 161
    invoke-virtual/range {v44 .. v44}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[ODEX-PATCH]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->OdexPatch:Z

    .line 162
    :cond_8
    invoke-virtual/range {v44 .. v44}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[LIB-ARMEABI-V7A]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->armv7:Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_5

    .line 167
    .end local v17           #br1:Ljava/io/BufferedReader;
    .end local v37           #fis1:Ljava/io/FileInputStream;
    .end local v39           #isr1:Ljava/io/InputStreamReader;
    :catch_5
    move-exception v2

    .line 170
    :goto_6
    const/4 v2, 0x2

    :try_start_8
    aget-object v2, p0, v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->searchDalvik(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    .line 174
    :try_start_9
    new-instance v36, Ljava/io/FileInputStream;

    const/4 v2, 0x1

    aget-object v2, p0, v2

    move-object/from16 v0, v36

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 175
    .local v36, fis:Ljava/io/FileInputStream;
    new-instance v38, Ljava/io/InputStreamReader;

    const-string v2, "UTF-8"

    move-object/from16 v0, v38

    move-object/from16 v1, v36

    invoke-direct {v0, v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 176
    .local v38, isr:Ljava/io/InputStreamReader;
    new-instance v16, Ljava/io/BufferedReader;

    move-object/from16 v0, v16

    move-object/from16 v1, v38

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 177
    .local v16, br:Ljava/io/BufferedReader;
    const-string v23, ""

    .line 178
    .local v23, data:Ljava/lang/String;
    const/16 v2, 0x7d0

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v65, v0

    .line 179
    .local v65, txtdata:[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v52, v0

    const/4 v2, 0x0

    const-string v7, ""

    aput-object v7, v52, v2

    .line 181
    .local v52, orhex:[Ljava/lang/String;
    const/4 v3, 0x0

    .line 183
    .local v3, byteOrig:[B
    const/4 v4, 0x0

    .line 185
    .local v4, mask:[I
    const-string v33, ""

    .line 186
    .local v33, file_name:Ljava/lang/String;
    const-string v56, ""

    .line 187
    .local v56, permissionsSet:Ljava/lang/String;
    const-string v54, ""

    .line 188
    .local v54, path_for_copy:Ljava/lang/String;
    const/16 v60, 0x1

    .local v60, result:Z
    const/16 v63, 0x1

    .local v63, sumresult:Z
    const/16 v43, 0x0

    .local v43, libr:Z
    const/4 v14, 0x0

    .local v14, begin:Z
    const/16 v48, 0x0

    .local v48, mark_search:Z
    const/16 v53, 0x0

    .local v53, other:Z
    const/16 v55, 0x0

    .local v55, permissions:Z
    const/16 v20, 0x0

    .line 189
    .local v20, copy_file:Z
    const-string v66, ""

    .line 190
    .local v66, value1:Ljava/lang/String;
    const-string v67, ""

    .line 191
    .local v67, value2:Ljava/lang/String;
    const-string v68, ""

    .line 192
    .local v68, value3:Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    .line 193
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    .line 194
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    .line 195
    const/16 v57, 0x0

    .line 197
    .local v57, r:I
    :goto_7
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v23

    if-eqz v23, :cond_9e

    .line 199
    const-string v2, ""

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 200
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-static {v0, v2}, Lcom/chelpus/Utils;->apply_TAGS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 202
    :cond_9
    aput-object v23, v65, v57

    .line 204
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[PACKAGE]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 229
    const/4 v2, 0x5

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 230
    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 232
    new-instance v9, Ljava/io/File;

    const/4 v2, 0x2

    aget-object v2, p0, v2

    invoke-direct {v9, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 233
    .local v9, apk:Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_a

    .line 234
    new-instance v9, Ljava/io/File;

    .end local v9           #apk:Ljava/io/File;
    const/4 v2, 0x2

    aget-object v2, p0, v2

    const-string v7, "-1/"

    const-string v8, "-2/"

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 235
    .restart local v9       #apk:Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 236
    const/4 v2, 0x2

    const/4 v7, 0x2

    aget-object v7, p0, v7

    const-string v8, "-1/"

    const-string v70, "-2/"

    move-object/from16 v0, v70

    invoke-virtual {v7, v8, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, p0, v2

    .line 239
    :cond_a
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_b

    .line 240
    new-instance v9, Ljava/io/File;

    .end local v9           #apk:Ljava/io/File;
    const/4 v2, 0x2

    aget-object v2, p0, v2

    const-string v7, "-1/"

    const-string v8, "/"

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 241
    .restart local v9       #apk:Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 242
    const/4 v2, 0x2

    const/4 v7, 0x2

    aget-object v7, p0, v7

    const-string v8, "-1/"

    const-string v70, ""

    move-object/from16 v0, v70

    invoke-virtual {v7, v8, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, p0, v2

    .line 246
    :cond_b
    invoke-static {v9}, Lcom/chelpus/root/utils/custompatch;->unzip(Ljava/io/File;)V

    .line 248
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->odexpatch:Z

    if-nez v2, :cond_e

    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->OdexPatch:Z

    if-nez v2, :cond_e

    .line 249
    const/4 v2, 0x2

    aget-object v2, p0, v2

    const/4 v7, 0x1

    invoke-static {v2, v7}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v50

    .line 250
    .local v50, odexstr:Ljava/lang/String;
    new-instance v49, Ljava/io/File;

    invoke-direct/range {v49 .. v50}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 251
    .local v49, odexfile:Ljava/io/File;
    invoke-virtual/range {v49 .. v49}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-virtual/range {v49 .. v49}, Ljava/io/File;->delete()Z

    .line 252
    :cond_c
    new-instance v49, Ljava/io/File;

    .end local v49           #odexfile:Ljava/io/File;
    const-string v2, "-1"

    const-string v7, "-2"

    move-object/from16 v0, v50

    invoke-virtual {v0, v2, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v49

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 253
    .restart local v49       #odexfile:Ljava/io/File;
    invoke-virtual/range {v49 .. v49}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual/range {v49 .. v49}, Ljava/io/File;->delete()Z

    .line 254
    :cond_d
    new-instance v49, Ljava/io/File;

    .end local v49           #odexfile:Ljava/io/File;
    const-string v2, "-2"

    const-string v7, "-1"

    move-object/from16 v0, v50

    invoke-virtual {v0, v2, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v49

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 255
    .restart local v49       #odexfile:Ljava/io/File;
    invoke-virtual/range {v49 .. v49}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual/range {v49 .. v49}, Ljava/io/File;->delete()Z

    .line 260
    .end local v9           #apk:Ljava/io/File;
    .end local v49           #odexfile:Ljava/io/File;
    .end local v50           #odexstr:Ljava/lang/String;
    :cond_e
    if-eqz v14, :cond_10

    aget-object v2, v65, v57

    const-string v7, "["

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    aget-object v2, v65, v57

    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 261
    :cond_f
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 262
    const/4 v14, 0x0

    .line 264
    :cond_10
    if-eqz v14, :cond_11

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v7, v65, v57

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 266
    :cond_11
    aget-object v2, v65, v57

    const-string v7, "["

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_12

    aget-object v2, v65, v57

    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 267
    sget v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    packed-switch v2, :pswitch_data_0

    .line 577
    :cond_12
    :goto_8
    :pswitch_0
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[BEGIN]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 578
    const/4 v2, 0x0

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 579
    const/4 v14, 0x1

    .line 582
    :cond_13
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[CLASSES]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 583
    const/4 v2, 0x1

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 584
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    if-eqz v2, :cond_14

    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "classes.dex"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 588
    :cond_14
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[ODEX]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 589
    const/16 v2, 0xa

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 593
    :cond_15
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[SQLITE]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 594
    const/16 v2, 0xc

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 598
    :cond_16
    sget v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    const/16 v7, 0xc

    if-ne v2, v7, :cond_1b

    .line 599
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 600
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 601
    aget-object v2, v65, v57

    const-string v7, "database"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7

    move-result v2

    if-eqz v2, :cond_1a

    .line 603
    :try_start_a
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 604
    .local v40, json_obj:Lorg/json/JSONObject;
    const-string v2, "database"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v66

    .line 605
    new-instance v25, Ljava/io/File;

    move-object/from16 v0, v25

    move-object/from16 v1, v66

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 606
    .local v25, db:Ljava/io/File;
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "chmod"

    aput-object v8, v2, v7

    const/4 v7, 0x1

    const-string v8, "777"

    aput-object v8, v2, v7

    const/4 v7, 0x2

    aput-object v66, v2, v7

    invoke-static {v2}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 607
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->withFramework:Z

    if-nez v2, :cond_17

    .line 608
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-static {v0, v2}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 609
    :cond_17
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_65

    .line 610
    const/4 v2, 0x0

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 611
    const/4 v2, 0x0

    aget-object v2, p0, v2

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/chelpus/root/utils/custompatch;->searchfile(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    if-eqz v2, :cond_64

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_64

    .line 613
    sget-object v25, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 614
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "chmod"

    aput-object v8, v2, v7

    const/4 v7, 0x1

    const-string v8, "777"

    aput-object v8, v2, v7

    const/4 v7, 0x2

    aput-object v66, v2, v7

    invoke-static {v2}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 615
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->withFramework:Z

    if-nez v2, :cond_18

    .line 616
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-static {v0, v2}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 617
    :cond_18
    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->dataBaseExist:Z

    .line 620
    :goto_9
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->dataBaseExist:Z

    if-eqz v2, :cond_19

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->dataBase:Ljava/lang/String;

    .line 621
    :cond_19
    const-string v2, "---------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 622
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Open SqLite database\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 623
    const-string v2, "---------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_8
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_6
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_7

    .line 628
    .end local v25           #db:Ljava/io/File;
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :cond_1a
    :goto_a
    :try_start_b
    aget-object v2, v65, v57

    const-string v7, "execute"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1b

    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->dataBaseExist:Z
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_6
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7

    if-eqz v2, :cond_1b

    .line 630
    :try_start_c
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 631
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    const-string v2, "execute"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v66

    .line 632
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Execute:\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v66

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 633
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->withFramework:Z
    :try_end_c
    .catch Lorg/json/JSONException; {:try_start_c .. :try_end_c} :catch_a
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_6
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_7

    if-eqz v2, :cond_1b

    .line 635
    :try_start_d
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->dataBase:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v2, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v26

    .line 636
    .local v26, db3:Landroid/database/sqlite/SQLiteDatabase;
    move-object/from16 v0, v26

    move-object/from16 v1, v66

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 638
    invoke-virtual/range {v26 .. v26}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_9
    .catch Lorg/json/JSONException; {:try_start_d .. :try_end_d} :catch_a
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_6

    .line 649
    .end local v26           #db3:Landroid/database/sqlite/SQLiteDatabase;
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :cond_1b
    :goto_b
    if-eqz v53, :cond_1c

    .line 650
    :try_start_e
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 651
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_e
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_e} :catch_6
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_7

    .line 653
    :try_start_f
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 654
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    const-string v2, "name"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v66

    .line 655
    const-string v2, "---------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 656
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Patch for file \n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v66

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ":"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 657
    const-string v2, "---------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_f
    .catch Lorg/json/JSONException; {:try_start_f .. :try_end_f} :catch_b
    .catch Ljava/io/FileNotFoundException; {:try_start_f .. :try_end_f} :catch_6
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_7

    .line 661
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :goto_c
    const/4 v2, 0x0

    :try_start_10
    aget-object v2, p0, v2

    move-object/from16 v0, v66

    invoke-static {v2, v0}, Lcom/chelpus/root/utils/custompatch;->searchfile(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    const/16 v53, 0x0

    .line 664
    :cond_1c
    if-eqz v55, :cond_1e

    .line 665
    aget-object v2, v65, v57

    const-string v7, "file_name"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_10
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_10} :catch_6
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_7

    move-result v2

    if-eqz v2, :cond_1d

    .line 667
    :try_start_11
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 668
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    const-string v2, "file_name"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_11
    .catch Lorg/json/JSONException; {:try_start_11 .. :try_end_11} :catch_c
    .catch Ljava/io/FileNotFoundException; {:try_start_11 .. :try_end_11} :catch_6
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_7

    move-result-object v33

    .line 673
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :goto_d
    const/4 v2, 0x0

    :try_start_12
    aget-object v2, p0, v2

    move-object/from16 v0, v33

    invoke-static {v2, v0}, Lcom/chelpus/root/utils/custompatch;->searchfile(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    :cond_1d
    aget-object v2, v65, v57

    const-string v7, "permissions"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1e

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1e

    aget-object v2, v65, v57

    const-string v7, "["

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1e

    aget-object v2, v65, v57

    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_12
    .catch Ljava/io/FileNotFoundException; {:try_start_12 .. :try_end_12} :catch_6
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_7

    move-result v2

    if-nez v2, :cond_1e

    .line 677
    :try_start_13
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 678
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    const-string v2, "permissions"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_13
    .catch Lorg/json/JSONException; {:try_start_13 .. :try_end_13} :catch_d
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_13} :catch_6
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_7

    move-result-object v56

    .line 684
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :cond_1e
    :goto_e
    if-eqz v20, :cond_20

    .line 685
    :try_start_14
    aget-object v2, v65, v57

    const-string v7, "file_name"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_14
    .catch Ljava/io/FileNotFoundException; {:try_start_14 .. :try_end_14} :catch_6
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_7

    move-result v2

    if-eqz v2, :cond_1f

    .line 687
    :try_start_15
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 688
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    const-string v2, "file_name"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_15
    .catch Lorg/json/JSONException; {:try_start_15 .. :try_end_15} :catch_e
    .catch Ljava/io/FileNotFoundException; {:try_start_15 .. :try_end_15} :catch_6
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_7

    move-result-object v33

    .line 695
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :cond_1f
    :goto_f
    :try_start_16
    aget-object v2, v65, v57

    const-string v7, "\"to\":"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_20

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_20

    aget-object v2, v65, v57

    const-string v7, "["

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_20

    aget-object v2, v65, v57

    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_16
    .catch Ljava/io/FileNotFoundException; {:try_start_16 .. :try_end_16} :catch_6
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_7

    move-result v2

    if-nez v2, :cond_20

    .line 697
    :try_start_17
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 698
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    const-string v2, "to"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_17
    .catch Lorg/json/JSONException; {:try_start_17 .. :try_end_17} :catch_f
    .catch Ljava/io/FileNotFoundException; {:try_start_17 .. :try_end_17} :catch_6
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_7

    move-result-object v54

    .line 704
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :cond_20
    :goto_10
    if-eqz v43, :cond_21

    .line 705
    :try_start_18
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 706
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_18
    .catch Ljava/io/FileNotFoundException; {:try_start_18 .. :try_end_18} :catch_6
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_7

    .line 708
    :try_start_19
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 709
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    const-string v2, "name"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_19
    .catch Lorg/json/JSONException; {:try_start_19 .. :try_end_19} :catch_10
    .catch Ljava/io/FileNotFoundException; {:try_start_19 .. :try_end_19} :catch_6
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_7

    move-result-object v66

    .line 713
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :goto_11
    :try_start_1a
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 714
    const/4 v2, 0x0

    aget-object v2, p0, v2

    const/4 v7, 0x2

    aget-object v7, p0, v7

    move-object/from16 v0, v66

    invoke-static {v2, v0, v7}, Lcom/chelpus/root/utils/custompatch;->searchlib(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    .line 716
    const/16 v43, 0x0

    .line 718
    :cond_21
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[LIB]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 719
    const/4 v2, 0x2

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 720
    const/16 v43, 0x1

    .line 721
    const/16 v53, 0x0

    .line 722
    const/16 v55, 0x0

    .line 723
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 724
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 725
    const/16 v20, 0x0

    .line 728
    :cond_22
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[LIB-ARMEABI]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 729
    const/4 v2, 0x7

    aget-object v2, p0, v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "armeabi"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    const/4 v2, 0x7

    aget-object v2, p0, v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "armeabi-v7a"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_66

    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->armv7:Z

    if-nez v2, :cond_66

    .line 730
    :cond_23
    const/4 v2, 0x6

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 731
    const/16 v43, 0x1

    .line 732
    const/16 v53, 0x0

    .line 733
    const/16 v55, 0x0

    .line 734
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 735
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 736
    const/16 v20, 0x0

    .line 742
    :cond_24
    :goto_12
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[LIB-ARMEABI-V7A]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 743
    const/4 v2, 0x7

    aget-object v2, p0, v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "armeabi-v7a"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_67

    .line 744
    const/4 v2, 0x7

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 745
    const/16 v43, 0x1

    .line 746
    const/16 v53, 0x0

    .line 747
    const/16 v55, 0x0

    .line 748
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 749
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 750
    const/16 v20, 0x0

    .line 756
    :cond_25
    :goto_13
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[LIB-MIPS]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 757
    const/4 v2, 0x7

    aget-object v2, p0, v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "mips"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_68

    .line 758
    const/16 v2, 0x8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 759
    const/16 v43, 0x1

    .line 760
    const/16 v53, 0x0

    .line 761
    const/16 v55, 0x0

    .line 762
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 763
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 764
    const/16 v20, 0x0

    .line 770
    :cond_26
    :goto_14
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[LIB-X86]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 771
    const/4 v2, 0x7

    aget-object v2, p0, v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "x86"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_69

    .line 772
    const/16 v2, 0x9

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 773
    const/16 v43, 0x1

    .line 774
    const/16 v53, 0x0

    .line 775
    const/16 v55, 0x0

    .line 776
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 777
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 778
    const/16 v20, 0x0

    .line 785
    :cond_27
    :goto_15
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[OTHER FILES]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 786
    const/4 v2, 0x3

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 787
    const/16 v43, 0x0

    .line 788
    const/16 v53, 0x1

    .line 789
    const/16 v55, 0x0

    .line 790
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 791
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 792
    const/16 v20, 0x0

    .line 795
    :cond_28
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[SET_PERMISSIONS]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 796
    const/16 v2, 0xd

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 797
    const/16 v43, 0x0

    .line 798
    const/16 v53, 0x0

    .line 799
    const/16 v55, 0x1

    .line 800
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 801
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 802
    const/16 v20, 0x0

    .line 805
    :cond_29
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[COPY_FILE]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 806
    const/16 v2, 0xf

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 807
    const/16 v43, 0x0

    .line 808
    const/16 v53, 0x0

    .line 809
    const/16 v55, 0x0

    .line 810
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 811
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 812
    const/16 v20, 0x1

    .line 815
    :cond_2a
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[ODEX-PATCH]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 816
    const/16 v2, 0xb

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 817
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 818
    const/16 v20, 0x0

    .line 819
    const/16 v43, 0x0

    .line 820
    const/16 v53, 0x0

    .line 821
    const/16 v55, 0x0

    .line 822
    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 824
    :cond_2b
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[FILE_IN_APK]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 825
    const/16 v2, 0xe

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 826
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 827
    const/16 v43, 0x0

    .line 828
    const/16 v53, 0x0

    .line 829
    const/16 v55, 0x0

    .line 830
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 831
    const/16 v20, 0x0

    .line 834
    :cond_2c
    aget-object v2, v65, v57

    const-string v7, "group"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2d

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2d

    aget-object v2, v65, v57

    const-string v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_1a
    .catch Ljava/io/FileNotFoundException; {:try_start_1a .. :try_end_1a} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_7

    move-result v2

    if-eqz v2, :cond_2d

    .line 836
    :try_start_1b
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 837
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    const-string v2, "group"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;
    :try_end_1b
    .catch Lorg/json/JSONException; {:try_start_1b .. :try_end_1b} :catch_11
    .catch Ljava/io/FileNotFoundException; {:try_start_1b .. :try_end_1b} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_7

    .line 843
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :cond_2d
    :goto_16
    :try_start_1c
    aget-object v2, v65, v57

    const-string v7, "original"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6b

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6b

    aget-object v2, v65, v57

    const-string v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6b

    .line 845
    if-eqz v48, :cond_2e

    .line 846
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->searchProcess(Ljava/util/ArrayList;)Z
    :try_end_1c
    .catch Ljava/io/FileNotFoundException; {:try_start_1c .. :try_end_1c} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_7

    move-result v63

    .line 847
    const/16 v48, 0x0

    .line 850
    :cond_2e
    :try_start_1d
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 851
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    const-string v2, "original"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1d
    .catch Lorg/json/JSONException; {:try_start_1d .. :try_end_1d} :catch_12
    .catch Ljava/io/FileNotFoundException; {:try_start_1d .. :try_end_1d} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_7

    move-result-object v66

    .line 855
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :goto_17
    :try_start_1e
    invoke-virtual/range {v66 .. v66}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v66

    .line 856
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    if-eqz v2, :cond_2f

    .line 857
    invoke-static/range {v66 .. v66}, Lcom/chelpus/Utils;->rework(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v66

    .line 859
    :cond_2f
    const-string v2, "[ \t]+"

    move-object/from16 v0, v66

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v52, v0

    .line 860
    const-string v2, "[ \t]+"

    move-object/from16 v0, v66

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v52

    .line 861
    move-object/from16 v0, v52

    array-length v2, v0

    new-array v4, v2, [I

    .line 862
    move-object/from16 v0, v52

    array-length v2, v0

    new-array v3, v2, [B
    :try_end_1e
    .catch Ljava/io/FileNotFoundException; {:try_start_1e .. :try_end_1e} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_7

    .line 864
    const/16 v64, 0x0

    .local v64, t:I
    :goto_18
    :try_start_1f
    move-object/from16 v0, v52

    array-length v2, v0

    move/from16 v0, v64

    if-ge v0, v2, :cond_6b

    .line 865
    aget-object v2, v52, v64

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_30

    aget-object v2, v52, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_30

    .line 866
    const/16 v31, 0x1

    .line 867
    const-string v2, "60"

    aput-object v2, v52, v64

    .line 869
    :cond_30
    aget-object v2, v52, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_31

    aget-object v2, v52, v64

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6a

    .line 870
    :cond_31
    const-string v2, "60"

    aput-object v2, v52, v64

    .line 871
    const/4 v2, 0x1

    aput v2, v4, v64

    .line 874
    :goto_19
    aget-object v2, v52, v64

    const-string v7, "W"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_32

    aget-object v2, v52, v64

    const-string v7, "w"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_32

    aget-object v2, v52, v64

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_32

    aget-object v2, v52, v64

    const-string v7, "r"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_33

    .line 876
    :cond_32
    aget-object v2, v52, v64

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "w"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "r"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v51

    .line 878
    .local v51, or:Ljava/lang/String;
    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v69

    .line 879
    .local v69, y:I
    add-int/lit8 v69, v69, 0x2

    .line 880
    aput v69, v4, v64

    .line 881
    const-string v2, "60"

    aput-object v2, v52, v64

    .line 883
    .end local v51           #or:Ljava/lang/String;
    .end local v69           #y:I
    :cond_33
    aget-object v2, v52, v64

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v3, v64
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_13
    .catch Ljava/io/FileNotFoundException; {:try_start_1f .. :try_end_1f} :catch_6

    .line 864
    add-int/lit8 v64, v64, 0x1

    goto/16 :goto_18

    .line 165
    .end local v3           #byteOrig:[B
    .end local v4           #mask:[I
    .end local v14           #begin:Z
    .end local v16           #br:Ljava/io/BufferedReader;
    .end local v20           #copy_file:Z
    .end local v23           #data:Ljava/lang/String;
    .end local v33           #file_name:Ljava/lang/String;
    .end local v36           #fis:Ljava/io/FileInputStream;
    .end local v38           #isr:Ljava/io/InputStreamReader;
    .end local v43           #libr:Z
    .end local v48           #mark_search:Z
    .end local v52           #orhex:[Ljava/lang/String;
    .end local v53           #other:Z
    .end local v54           #path_for_copy:Ljava/lang/String;
    .end local v55           #permissions:Z
    .end local v56           #permissionsSet:Ljava/lang/String;
    .end local v57           #r:I
    .end local v60           #result:Z
    .end local v63           #sumresult:Z
    .end local v64           #t:I
    .end local v65           #txtdata:[Ljava/lang/String;
    .end local v66           #value1:Ljava/lang/String;
    .end local v67           #value2:Ljava/lang/String;
    .end local v68           #value3:Ljava/lang/String;
    .restart local v17       #br1:Ljava/io/BufferedReader;
    .restart local v37       #fis1:Ljava/io/FileInputStream;
    .restart local v39       #isr1:Ljava/io/InputStreamReader;
    :cond_34
    :try_start_20
    invoke-virtual/range {v17 .. v17}, Ljava/io/BufferedReader;->close()V

    .line 166
    invoke-virtual/range {v37 .. v37}, Ljava/io/FileInputStream;->close()V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_20} :catch_5
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_1

    goto/16 :goto_6

    .line 270
    .end local v17           #br1:Ljava/io/BufferedReader;
    .end local v37           #fis1:Ljava/io/FileInputStream;
    .end local v39           #isr1:Ljava/io/InputStreamReader;
    .restart local v3       #byteOrig:[B
    .restart local v4       #mask:[I
    .restart local v14       #begin:Z
    .restart local v16       #br:Ljava/io/BufferedReader;
    .restart local v20       #copy_file:Z
    .restart local v23       #data:Ljava/lang/String;
    .restart local v33       #file_name:Ljava/lang/String;
    .restart local v36       #fis:Ljava/io/FileInputStream;
    .restart local v38       #isr:Ljava/io/InputStreamReader;
    .restart local v43       #libr:Z
    .restart local v48       #mark_search:Z
    .restart local v52       #orhex:[Ljava/lang/String;
    .restart local v53       #other:Z
    .restart local v54       #path_for_copy:Ljava/lang/String;
    .restart local v55       #permissions:Z
    .restart local v56       #permissionsSet:Ljava/lang/String;
    .restart local v57       #r:I
    .restart local v60       #result:Z
    .restart local v63       #sumresult:Z
    .restart local v65       #txtdata:[Ljava/lang/String;
    .restart local v66       #value1:Ljava/lang/String;
    .restart local v67       #value2:Ljava/lang/String;
    .restart local v68       #value3:Ljava/lang/String;
    :pswitch_1
    :try_start_21
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->odexpatch:Z

    if-eqz v2, :cond_38

    .line 271
    new-instance v2, Ljava/io/File;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 272
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_12

    .line 273
    const-string v2, "---------------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 274
    const-string v2, "classes.dex not found!\nApply patch for odex:"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 275
    const-string v2, "---------------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 277
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_35

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 278
    :cond_35
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->manualpatch:Z

    if-nez v2, :cond_36

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 279
    :cond_36
    if-nez v60, :cond_37

    const/16 v63, 0x0

    .line 280
    :cond_37
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 281
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 282
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 283
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;
    :try_end_21
    .catch Ljava/io/FileNotFoundException; {:try_start_21 .. :try_end_21} :catch_6
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_7

    goto/16 :goto_8

    .line 1176
    .end local v3           #byteOrig:[B
    .end local v4           #mask:[I
    .end local v14           #begin:Z
    .end local v16           #br:Ljava/io/BufferedReader;
    .end local v20           #copy_file:Z
    .end local v23           #data:Ljava/lang/String;
    .end local v33           #file_name:Ljava/lang/String;
    .end local v36           #fis:Ljava/io/FileInputStream;
    .end local v38           #isr:Ljava/io/InputStreamReader;
    .end local v43           #libr:Z
    .end local v48           #mark_search:Z
    .end local v52           #orhex:[Ljava/lang/String;
    .end local v53           #other:Z
    .end local v54           #path_for_copy:Ljava/lang/String;
    .end local v55           #permissions:Z
    .end local v56           #permissionsSet:Ljava/lang/String;
    .end local v57           #r:I
    .end local v60           #result:Z
    .end local v63           #sumresult:Z
    .end local v65           #txtdata:[Ljava/lang/String;
    .end local v66           #value1:Ljava/lang/String;
    .end local v67           #value2:Ljava/lang/String;
    .end local v68           #value3:Ljava/lang/String;
    :catch_6
    move-exception v29

    .line 1178
    .local v29, e:Ljava/io/FileNotFoundException;
    :try_start_22
    const-string v2, "Custom Patch not Found. It\'s problem with root. Don\'t have access to SD card from root.  If you use SuperSu, try disable \"mount namespace separation\" in SuperSu. If it not help, please update SuperSu and update binary file su from SuperSu."

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_1

    goto/16 :goto_4

    .line 287
    .end local v29           #e:Ljava/io/FileNotFoundException;
    .restart local v3       #byteOrig:[B
    .restart local v4       #mask:[I
    .restart local v14       #begin:Z
    .restart local v16       #br:Ljava/io/BufferedReader;
    .restart local v20       #copy_file:Z
    .restart local v23       #data:Ljava/lang/String;
    .restart local v33       #file_name:Ljava/lang/String;
    .restart local v36       #fis:Ljava/io/FileInputStream;
    .restart local v38       #isr:Ljava/io/InputStreamReader;
    .restart local v43       #libr:Z
    .restart local v48       #mark_search:Z
    .restart local v52       #orhex:[Ljava/lang/String;
    .restart local v53       #other:Z
    .restart local v54       #path_for_copy:Ljava/lang/String;
    .restart local v55       #permissions:Z
    .restart local v56       #permissionsSet:Ljava/lang/String;
    .restart local v57       #r:I
    .restart local v60       #result:Z
    .restart local v63       #sumresult:Z
    .restart local v65       #txtdata:[Ljava/lang/String;
    .restart local v66       #value1:Ljava/lang/String;
    .restart local v67       #value2:Ljava/lang/String;
    .restart local v68       #value3:Ljava/lang/String;
    :cond_38
    :try_start_23
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    if-nez v2, :cond_3d

    .line 289
    const/4 v2, 0x2

    aget-object v2, p0, v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->searchDalvik(Ljava/lang/String;)V

    .line 290
    new-instance v2, Ljava/io/File;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_39

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v7, ".odex"

    invoke-virtual {v2, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_39

    .line 291
    new-instance v2, Ljava/io/File;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 292
    const-string v2, "---------------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 293
    const-string v2, "odex file removed before\npatch for dalvik-cache..."

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 294
    const-string v2, "---------------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 296
    :cond_39
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_12

    .line 297
    const-string v2, "---------------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 298
    const-string v2, "Patch for dalvik-cache:"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 299
    const-string v2, "---------------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 301
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3a

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 302
    :cond_3a
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->manualpatch:Z

    if-nez v2, :cond_3b

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 303
    :cond_3b
    if-nez v60, :cond_3c

    const/16 v63, 0x0

    .line 304
    :cond_3c
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 305
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 306
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 307
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;
    :try_end_23
    .catch Ljava/io/FileNotFoundException; {:try_start_23 .. :try_end_23} :catch_6
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_7

    goto/16 :goto_8

    .line 1179
    .end local v3           #byteOrig:[B
    .end local v4           #mask:[I
    .end local v14           #begin:Z
    .end local v16           #br:Ljava/io/BufferedReader;
    .end local v20           #copy_file:Z
    .end local v23           #data:Ljava/lang/String;
    .end local v33           #file_name:Ljava/lang/String;
    .end local v36           #fis:Ljava/io/FileInputStream;
    .end local v38           #isr:Ljava/io/InputStreamReader;
    .end local v43           #libr:Z
    .end local v48           #mark_search:Z
    .end local v52           #orhex:[Ljava/lang/String;
    .end local v53           #other:Z
    .end local v54           #path_for_copy:Ljava/lang/String;
    .end local v55           #permissions:Z
    .end local v56           #permissionsSet:Ljava/lang/String;
    .end local v57           #r:I
    .end local v60           #result:Z
    .end local v63           #sumresult:Z
    .end local v65           #txtdata:[Ljava/lang/String;
    .end local v66           #value1:Ljava/lang/String;
    .end local v67           #value2:Ljava/lang/String;
    .end local v68           #value3:Ljava/lang/String;
    :catch_7
    move-exception v29

    .line 1180
    .local v29, e:Ljava/lang/Exception;
    :try_start_24
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V

    .line 1181
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_1

    goto/16 :goto_4

    .line 310
    .end local v29           #e:Ljava/lang/Exception;
    .restart local v3       #byteOrig:[B
    .restart local v4       #mask:[I
    .restart local v14       #begin:Z
    .restart local v16       #br:Ljava/io/BufferedReader;
    .restart local v20       #copy_file:Z
    .restart local v23       #data:Ljava/lang/String;
    .restart local v33       #file_name:Ljava/lang/String;
    .restart local v36       #fis:Ljava/io/FileInputStream;
    .restart local v38       #isr:Ljava/io/InputStreamReader;
    .restart local v43       #libr:Z
    .restart local v48       #mark_search:Z
    .restart local v52       #orhex:[Ljava/lang/String;
    .restart local v53       #other:Z
    .restart local v54       #path_for_copy:Ljava/lang/String;
    .restart local v55       #permissions:Z
    .restart local v56       #permissionsSet:Ljava/lang/String;
    .restart local v57       #r:I
    .restart local v60       #result:Z
    .restart local v63       #sumresult:Z
    .restart local v65       #txtdata:[Ljava/lang/String;
    .restart local v66       #value1:Ljava/lang/String;
    .restart local v67       #value2:Ljava/lang/String;
    .restart local v68       #value3:Ljava/lang/String;
    :cond_3d
    :try_start_25
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    if-eqz v2, :cond_12

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_12

    .line 312
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v7, 0x1

    if-le v2, v7, :cond_3e

    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->multidex:Z

    .line 313
    :cond_3e
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3f
    :goto_1a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_42

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/io/File;

    .line 314
    .local v18, cl:Ljava/io/File;
    sput-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 315
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_3f

    .line 316
    const-string v7, "---------------------------------"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 317
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 318
    const-string v7, "---------------------------------\n"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 320
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_40

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 321
    :cond_40
    sget-boolean v7, Lcom/chelpus/root/utils/custompatch;->manualpatch:Z

    if-nez v7, :cond_41

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 322
    :cond_41
    if-nez v60, :cond_3f

    const/16 v63, 0x0

    goto :goto_1a

    .line 326
    .end local v18           #cl:Ljava/io/File;
    :cond_42
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->multidex:Z

    .line 327
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    .line 328
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 329
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 330
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 331
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_8

    .line 337
    :pswitch_2
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 338
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_44

    .line 339
    const-string v2, "---------------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 340
    const-string v2, "Dalvik-cache fixed to odex!\nPatch for odex:"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 341
    const-string v2, "---------------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 342
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_43

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 343
    :cond_43
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 344
    if-nez v60, :cond_44

    const/16 v63, 0x0

    .line 346
    :cond_44
    const-string v2, "---------------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 347
    const-string v2, "Dalvik-cache fixed to odex!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 348
    const-string v2, "---------------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 349
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_45

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 350
    :cond_45
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    if-nez v2, :cond_46

    const/4 v2, 0x2

    aget-object v2, p0, v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->searchDalvik(Ljava/lang/String;)V

    .line 351
    :cond_46
    const/4 v2, 0x0

    aget-object v2, p0, v2

    const/4 v7, 0x2

    aget-object v7, p0, v7

    invoke-static {v2, v7}, Lcom/chelpus/root/utils/custompatch;->searchDalvikOdex(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_47

    .line 353
    const/16 v21, 0x1

    .line 354
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v22

    .line 356
    :cond_47
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 357
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 358
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 360
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_8

    .line 364
    :pswitch_3
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 365
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4b

    .line 366
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    if-eqz v2, :cond_4b

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4b

    .line 367
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_48
    :goto_1b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/io/File;

    .line 368
    .local v42, lib:Ljava/io/File;
    sput-object v42, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 369
    const-string v7, "---------------------------"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 370
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch for libraries \n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 371
    const-string v7, "---------------------------\n"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 372
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_49

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 373
    :cond_49
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 374
    if-nez v60, :cond_48

    const/16 v63, 0x0

    goto :goto_1b

    .line 376
    .end local v42           #lib:Ljava/io/File;
    :cond_4a
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    .line 377
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    .line 378
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 382
    :cond_4b
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 383
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 384
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 385
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_8

    .line 388
    :pswitch_4
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 389
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4f

    .line 390
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    if-eqz v2, :cond_4f

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4f

    .line 391
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4c
    :goto_1c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/io/File;

    .line 392
    .restart local v42       #lib:Ljava/io/File;
    sput-object v42, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 393
    const-string v7, "--------------------------------"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 394
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch for (armeabi) libraries \n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 395
    const-string v7, "--------------------------------\n"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 396
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4d

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 397
    :cond_4d
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 398
    if-nez v60, :cond_4c

    const/16 v63, 0x0

    goto :goto_1c

    .line 400
    .end local v42           #lib:Ljava/io/File;
    :cond_4e
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    .line 401
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    .line 402
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 406
    :cond_4f
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 407
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 408
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 409
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_8

    .line 412
    :pswitch_5
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 413
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_53

    .line 414
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    if-eqz v2, :cond_53

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_53

    .line 415
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_50
    :goto_1d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_52

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/io/File;

    .line 416
    .restart local v42       #lib:Ljava/io/File;
    sput-object v42, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 417
    const-string v7, "---------------------------------------"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 418
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch for (armeabi-v7a) libraries \n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 419
    const-string v7, "---------------------------------------\n"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 420
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_51

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 421
    :cond_51
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 422
    if-nez v60, :cond_50

    const/16 v63, 0x0

    goto :goto_1d

    .line 424
    .end local v42           #lib:Ljava/io/File;
    :cond_52
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    .line 425
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    .line 426
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 431
    :cond_53
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 432
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 433
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 434
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_8

    .line 437
    :pswitch_6
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 438
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_57

    .line 439
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    if-eqz v2, :cond_57

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_57

    .line 440
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_54
    :goto_1e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_56

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/io/File;

    .line 441
    .restart local v42       #lib:Ljava/io/File;
    sput-object v42, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 442
    const-string v7, "---------------------------"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 443
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch for (MIPS) libraries \n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 444
    const-string v7, "---------------------------\n"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 445
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_55

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 446
    :cond_55
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 447
    if-nez v60, :cond_54

    const/16 v63, 0x0

    goto :goto_1e

    .line 449
    .end local v42           #lib:Ljava/io/File;
    :cond_56
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    .line 450
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    .line 451
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 455
    :cond_57
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 456
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 457
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 458
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_8

    .line 461
    :pswitch_7
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 462
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_5b

    .line 463
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    if-eqz v2, :cond_5b

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_5b

    .line 464
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_58
    :goto_1f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/io/File;

    .line 465
    .restart local v42       #lib:Ljava/io/File;
    sput-object v42, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 466
    const-string v7, "---------------------------"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 467
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch for (x86) libraries \n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 468
    const-string v7, "---------------------------\n"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 469
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_59

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 470
    :cond_59
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 471
    if-nez v60, :cond_58

    const/16 v63, 0x0

    goto :goto_1f

    .line 473
    .end local v42           #lib:Ljava/io/File;
    :cond_5a
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    .line 474
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    .line 475
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 479
    :cond_5b
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 480
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 481
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 482
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_8

    .line 486
    :pswitch_8
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 487
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_5d

    .line 489
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->manualpatch:Z

    if-nez v2, :cond_5c

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 490
    :cond_5c
    if-nez v60, :cond_5d

    const/16 v63, 0x0

    .line 492
    :cond_5d
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 493
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 494
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 495
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_8

    .line 499
    :pswitch_9
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 500
    const-string v2, "---------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 501
    const-string v2, "Patch for odex:"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 502
    const-string v2, "---------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 503
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5e

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 504
    :cond_5e
    new-instance v2, Ljava/io/File;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 505
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_5f

    .line 506
    const-string v2, "Odex not found! Please use befor other Patch, and after run Custom Patch!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 507
    :cond_5f
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v7

    const-wide/16 v70, 0x0

    cmp-long v2, v7, v70

    if-nez v2, :cond_60

    .line 508
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 509
    const-string v2, "Odex not found! Please use befor other Patch, and after run Custom Patch!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 511
    :cond_60
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 512
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 514
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_61

    .line 515
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 516
    if-nez v60, :cond_61

    const/16 v63, 0x0

    .line 518
    :cond_61
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 519
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 520
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 521
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_8

    .line 524
    :pswitch_a
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 525
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 526
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 527
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 528
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_8

    .line 531
    :pswitch_b
    const/16 v55, 0x0

    .line 532
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 533
    const-string v2, "---------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 534
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Set permissions "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " for file:\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 535
    const-string v2, "---------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 536
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "chmod"

    aput-object v8, v2, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v70, ""

    move-object/from16 v0, v70

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v56

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    const/4 v7, 0x2

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    invoke-static {v2}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    goto/16 :goto_8

    .line 541
    :pswitch_c
    const/16 v20, 0x0

    .line 542
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, v54

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v27

    .line 543
    .local v27, dir_for_copy:Ljava/io/File;
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->mkdirs()Z

    .line 544
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "chmod"

    aput-object v8, v2, v7

    const/4 v7, 0x1

    const-string v8, "777"

    aput-object v8, v2, v7

    const/4 v7, 0x2

    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    invoke-static {v2}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 545
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v28

    .line 547
    .local v28, dir_for_test:Ljava/lang/String;
    const-string v2, "---------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 548
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Copy file "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " to:\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v54

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 549
    const-string v2, "---------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 550
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->sddir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_62

    .line 551
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error: File "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " not found to dir\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->sddir:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "/"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 553
    :cond_62
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->sddir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v7, Ljava/io/File;

    move-object/from16 v0, v54

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v7}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 555
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, v54

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_63

    new-instance v2, Ljava/io/File;

    move-object/from16 v0, v54

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v7

    new-instance v2, Ljava/io/File;

    new-instance v70, Ljava/lang/StringBuilder;

    invoke-direct/range {v70 .. v70}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v71, Lcom/chelpus/root/utils/custompatch;->sddir:Ljava/lang/String;

    invoke-virtual/range {v70 .. v71}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v70

    const-string v71, "/"

    invoke-virtual/range {v70 .. v71}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v70

    move-object/from16 v0, v70

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v70

    invoke-virtual/range {v70 .. v70}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v70

    move-object/from16 v0, v70

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v70

    cmp-long v2, v7, v70

    if-nez v2, :cond_63

    .line 556
    const-string v2, "File copied success."

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 560
    :goto_20
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "chmod"

    aput-object v8, v2, v7

    const/4 v7, 0x1

    const-string v8, "777"

    aput-object v8, v2, v7

    const/4 v7, 0x2

    aput-object v54, v2, v7

    invoke-static {v2}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    goto/16 :goto_8

    .line 558
    :cond_63
    const-string v2, "File copied with error. Try again."

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto :goto_20

    .line 566
    .end local v27           #dir_for_copy:Ljava/io/File;
    .end local v28           #dir_for_test:Ljava/lang/String;
    :pswitch_d
    const-string v2, "---------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 567
    const-string v2, "Patch for file from apk:\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 568
    const-string v2, "---------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 569
    const-string v2, "You must run rebuild for this application with custom patch, then patch will work.\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_25
    .catch Ljava/io/FileNotFoundException; {:try_start_25 .. :try_end_25} :catch_6
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_25} :catch_7

    goto/16 :goto_8

    .line 618
    .restart local v25       #db:Ljava/io/File;
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    :cond_64
    :try_start_26
    const-string v66, "Error LP: File of Database not Found!"

    goto/16 :goto_9

    .line 619
    :cond_65
    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->dataBaseExist:Z
    :try_end_26
    .catch Lorg/json/JSONException; {:try_start_26 .. :try_end_26} :catch_8
    .catch Ljava/io/FileNotFoundException; {:try_start_26 .. :try_end_26} :catch_6
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_26} :catch_7

    goto/16 :goto_9

    .line 624
    .end local v25           #db:Ljava/io/File;
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :catch_8
    move-exception v29

    .line 625
    .local v29, e:Lorg/json/JSONException;
    :try_start_27
    const-string v2, "Error LP: Error Name of Database read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_27
    .catch Ljava/io/FileNotFoundException; {:try_start_27 .. :try_end_27} :catch_6
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_27} :catch_7

    goto/16 :goto_a

    .line 639
    .end local v29           #e:Lorg/json/JSONException;
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    :catch_9
    move-exception v29

    .line 640
    .local v29, e:Ljava/lang/Exception;
    :try_start_28
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LuckyPatcher: SQL error - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_28
    .catch Lorg/json/JSONException; {:try_start_28 .. :try_end_28} :catch_a
    .catch Ljava/io/FileNotFoundException; {:try_start_28 .. :try_end_28} :catch_6
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_28} :catch_7

    goto/16 :goto_b

    .line 643
    .end local v29           #e:Ljava/lang/Exception;
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :catch_a
    move-exception v29

    .line 644
    .local v29, e:Lorg/json/JSONException;
    :try_start_29
    const-string v2, "Error LP: Error SQL exec read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 658
    .end local v29           #e:Lorg/json/JSONException;
    :catch_b
    move-exception v29

    .line 659
    .restart local v29       #e:Lorg/json/JSONException;
    const-string v2, "Error LP: Error name of file read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_c

    .line 669
    .end local v29           #e:Lorg/json/JSONException;
    :catch_c
    move-exception v29

    .line 670
    .restart local v29       #e:Lorg/json/JSONException;
    invoke-virtual/range {v29 .. v29}, Lorg/json/JSONException;->printStackTrace()V

    .line 671
    const-string v2, "Error LP: Error name of file read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_d

    .line 679
    .end local v29           #e:Lorg/json/JSONException;
    :catch_d
    move-exception v29

    .line 680
    .restart local v29       #e:Lorg/json/JSONException;
    const-string v2, "Error LP: Error permissions read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 689
    .end local v29           #e:Lorg/json/JSONException;
    :catch_e
    move-exception v29

    .line 690
    .restart local v29       #e:Lorg/json/JSONException;
    invoke-virtual/range {v29 .. v29}, Lorg/json/JSONException;->printStackTrace()V

    .line 691
    const-string v2, "Error LP: Error name of file read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_f

    .line 699
    .end local v29           #e:Lorg/json/JSONException;
    :catch_f
    move-exception v29

    .line 700
    .restart local v29       #e:Lorg/json/JSONException;
    const-string v2, "Error LP: Error file copy read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_10

    .line 710
    .end local v29           #e:Lorg/json/JSONException;
    :catch_10
    move-exception v29

    .line 711
    .restart local v29       #e:Lorg/json/JSONException;
    const-string v2, "Error LP: Error name of libraries read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_11

    .line 739
    .end local v29           #e:Lorg/json/JSONException;
    :cond_66
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    goto/16 :goto_12

    .line 753
    :cond_67
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    goto/16 :goto_13

    .line 767
    :cond_68
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    goto/16 :goto_14

    .line 781
    :cond_69
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    goto/16 :goto_15

    .line 838
    :catch_11
    move-exception v29

    .line 839
    .restart local v29       #e:Lorg/json/JSONException;
    const-string v2, "Error LP: Error original hex read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 840
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    goto/16 :goto_16

    .line 852
    .end local v29           #e:Lorg/json/JSONException;
    :catch_12
    move-exception v29

    .line 853
    .restart local v29       #e:Lorg/json/JSONException;
    const-string v2, "Error LP: Error original hex read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_29
    .catch Ljava/io/FileNotFoundException; {:try_start_29 .. :try_end_29} :catch_6
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_29} :catch_7

    goto/16 :goto_17

    .line 872
    .end local v29           #e:Lorg/json/JSONException;
    .restart local v64       #t:I
    :cond_6a
    const/4 v2, 0x0

    :try_start_2a
    aput v2, v4, v64
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_2a} :catch_13
    .catch Ljava/io/FileNotFoundException; {:try_start_2a .. :try_end_2a} :catch_6

    goto/16 :goto_19

    .line 885
    :catch_13
    move-exception v29

    .line 886
    .local v29, e:Ljava/lang/Exception;
    :try_start_2b
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 889
    .end local v29           #e:Ljava/lang/Exception;
    .end local v64           #t:I
    :cond_6b
    aget-object v2, v65, v57

    const-string v7, "\"object\""

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6c

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6c

    aget-object v2, v65, v57

    const-string v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_2b
    .catch Ljava/io/FileNotFoundException; {:try_start_2b .. :try_end_2b} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_2b} :catch_7

    move-result v2

    if-eqz v2, :cond_6c

    .line 892
    :try_start_2c
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 893
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    const-string v2, "object"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2c
    .catch Lorg/json/JSONException; {:try_start_2c .. :try_end_2c} :catch_14
    .catch Ljava/io/FileNotFoundException; {:try_start_2c .. :try_end_2c} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_2c} :catch_7

    move-result-object v68

    .line 897
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :goto_21
    :try_start_2d
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dalvikvm -Xverify:none -Xdexopt:none -cp "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v7, 0x5

    aget-object v7, p0, v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v7, 0x8

    aget-object v7, p0, v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ".nerorunpatch "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v7, 0x0

    aget-object v7, p0, v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "object"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v68

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 898
    .local v19, cmd:Ljava/lang/String;
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v47

    .line 899
    .local v47, localProcess9:Ljava/lang/Process;
    invoke-virtual/range {v47 .. v47}, Ljava/lang/Process;->waitFor()I

    .line 900
    new-instance v46, Ljava/io/DataInputStream;

    invoke-virtual/range {v47 .. v47}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    move-object/from16 v0, v46

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 901
    .local v46, localDataInputStream3:Ljava/io/DataInputStream;
    invoke-virtual/range {v46 .. v46}, Ljava/io/DataInputStream;->available()I

    move-result v2

    new-array v13, v2, [B

    .line 902
    .local v13, arrayOfByte:[B
    move-object/from16 v0, v46

    invoke-virtual {v0, v13}, Ljava/io/DataInputStream;->read([B)I

    .line 903
    new-instance v61, Ljava/lang/String;

    move-object/from16 v0, v61

    invoke-direct {v0, v13}, Ljava/lang/String;-><init>([B)V

    .line 906
    .local v61, str:Ljava/lang/String;
    invoke-virtual/range {v47 .. v47}, Ljava/lang/Process;->destroy()V

    .line 908
    const-string v2, "Done"

    move-object/from16 v0, v61

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_71

    .line 909
    const-string v62, "Object patched!\n\n"

    .line 910
    .local v62, str2:Ljava/lang/String;
    invoke-static/range {v62 .. v62}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 911
    const/16 v63, 0x1

    .line 919
    :goto_22
    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->manualpatch:Z

    .line 922
    .end local v13           #arrayOfByte:[B
    .end local v19           #cmd:Ljava/lang/String;
    .end local v46           #localDataInputStream3:Ljava/io/DataInputStream;
    .end local v47           #localProcess9:Ljava/lang/Process;
    .end local v61           #str:Ljava/lang/String;
    .end local v62           #str2:Ljava/lang/String;
    :cond_6c
    aget-object v2, v65, v57

    const-string v7, "search"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_74

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_74

    aget-object v2, v65, v57

    const-string v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_2d
    .catch Ljava/io/FileNotFoundException; {:try_start_2d .. :try_end_2d} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_2d} :catch_7

    move-result v2

    if-eqz v2, :cond_74

    .line 925
    :try_start_2e
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 926
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    const-string v2, "search"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2e
    .catch Lorg/json/JSONException; {:try_start_2e .. :try_end_2e} :catch_15
    .catch Ljava/io/FileNotFoundException; {:try_start_2e .. :try_end_2e} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_2e} :catch_7

    move-result-object v68

    .line 930
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :goto_23
    :try_start_2f
    invoke-virtual/range {v68 .. v68}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v68

    .line 931
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    if-eqz v2, :cond_6d

    .line 932
    invoke-static/range {v68 .. v68}, Lcom/chelpus/Utils;->rework(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v68

    .line 934
    :cond_6d
    const-string v2, "[ \t]+"

    move-object/from16 v0, v68

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v52, v0

    .line 935
    const-string v2, "[ \t]+"

    move-object/from16 v0, v68

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v52

    .line 936
    move-object/from16 v0, v52

    array-length v2, v0

    new-array v4, v2, [I

    .line 937
    move-object/from16 v0, v52

    array-length v2, v0

    new-array v3, v2, [B
    :try_end_2f
    .catch Ljava/io/FileNotFoundException; {:try_start_2f .. :try_end_2f} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_2f} :catch_7

    .line 939
    const/16 v64, 0x0

    .restart local v64       #t:I
    :goto_24
    :try_start_30
    move-object/from16 v0, v52

    array-length v2, v0

    move/from16 v0, v64

    if-ge v0, v2, :cond_73

    .line 940
    aget-object v2, v52, v64

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6e

    aget-object v2, v52, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6e

    .line 941
    const/16 v31, 0x1

    .line 942
    const-string v2, "60"

    aput-object v2, v52, v64

    .line 944
    :cond_6e
    aget-object v2, v52, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6f

    aget-object v2, v52, v64

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_72

    .line 945
    :cond_6f
    const-string v2, "60"

    aput-object v2, v52, v64

    .line 946
    const/4 v2, 0x1

    aput v2, v4, v64

    .line 948
    :goto_25
    aget-object v2, v52, v64

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_70

    .line 950
    aget-object v2, v52, v64

    const-string v7, "R"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v51

    .line 952
    .restart local v51       #or:Ljava/lang/String;
    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v69

    .line 953
    .restart local v69       #y:I
    add-int/lit8 v69, v69, 0x2

    .line 954
    aput v69, v4, v64

    .line 955
    const-string v2, "60"

    aput-object v2, v52, v64

    .line 957
    .end local v51           #or:Ljava/lang/String;
    .end local v69           #y:I
    :cond_70
    aget-object v2, v52, v64

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v3, v64
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_30} :catch_16
    .catch Ljava/io/FileNotFoundException; {:try_start_30 .. :try_end_30} :catch_6

    .line 939
    add-int/lit8 v64, v64, 0x1

    goto :goto_24

    .line 894
    .end local v64           #t:I
    :catch_14
    move-exception v29

    .line 895
    .local v29, e:Lorg/json/JSONException;
    :try_start_31
    const-string v2, "Error LP: Error number by object!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_21

    .line 914
    .end local v29           #e:Lorg/json/JSONException;
    .restart local v13       #arrayOfByte:[B
    .restart local v19       #cmd:Ljava/lang/String;
    .restart local v46       #localDataInputStream3:Ljava/io/DataInputStream;
    .restart local v47       #localProcess9:Ljava/lang/Process;
    .restart local v61       #str:Ljava/lang/String;
    :cond_71
    const-string v62, "Object not found!\n\n"

    .line 915
    .restart local v62       #str2:Ljava/lang/String;
    invoke-static/range {v62 .. v62}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 916
    const/16 v63, 0x0

    goto/16 :goto_22

    .line 927
    .end local v13           #arrayOfByte:[B
    .end local v19           #cmd:Ljava/lang/String;
    .end local v46           #localDataInputStream3:Ljava/io/DataInputStream;
    .end local v47           #localProcess9:Ljava/lang/Process;
    .end local v61           #str:Ljava/lang/String;
    .end local v62           #str2:Ljava/lang/String;
    :catch_15
    move-exception v29

    .line 928
    .restart local v29       #e:Lorg/json/JSONException;
    const-string v2, "Error LP: Error search hex read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_31
    .catch Ljava/io/FileNotFoundException; {:try_start_31 .. :try_end_31} :catch_6
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_31} :catch_7

    goto/16 :goto_23

    .line 947
    .end local v29           #e:Lorg/json/JSONException;
    .restart local v64       #t:I
    :cond_72
    const/4 v2, 0x0

    :try_start_32
    aput v2, v4, v64
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_32} :catch_16
    .catch Ljava/io/FileNotFoundException; {:try_start_32 .. :try_end_32} :catch_6

    goto :goto_25

    .line 960
    :catch_16
    move-exception v29

    .line 961
    .local v29, e:Ljava/lang/Exception;
    :try_start_33
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 963
    .end local v29           #e:Ljava/lang/Exception;
    :cond_73
    if-eqz v31, :cond_7f

    .line 964
    const/16 v60, 0x0

    .line 965
    const-string v2, "Error LP: Patterns to search not valid!\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 980
    .end local v64           #t:I
    :cond_74
    :goto_26
    aget-object v2, v65, v57

    const-string v7, "replaced"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_86

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_86

    aget-object v2, v65, v57

    const-string v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_33
    .catch Ljava/io/FileNotFoundException; {:try_start_33 .. :try_end_33} :catch_6
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_33} :catch_7

    move-result v2

    if-eqz v2, :cond_86

    .line 983
    :try_start_34
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 984
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    const-string v2, "replaced"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_34
    .catch Lorg/json/JSONException; {:try_start_34 .. :try_end_34} :catch_18
    .catch Ljava/io/FileNotFoundException; {:try_start_34 .. :try_end_34} :catch_6
    .catch Ljava/lang/Exception; {:try_start_34 .. :try_end_34} :catch_7

    move-result-object v67

    .line 988
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :goto_27
    :try_start_35
    invoke-virtual/range {v67 .. v67}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v67

    .line 989
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    if-eqz v2, :cond_75

    .line 990
    invoke-static/range {v67 .. v67}, Lcom/chelpus/Utils;->rework(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v67

    .line 992
    :cond_75
    const-string v2, "[ \t]+"

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v58, v0

    .line 993
    .local v58, rephex:[Ljava/lang/String;
    const-string v2, "[ \t]+"

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v58

    .line 994
    move-object/from16 v0, v58

    array-length v2, v0

    new-array v6, v2, [I

    .line 995
    .local v6, rep_mask:[I
    move-object/from16 v0, v58

    array-length v2, v0

    new-array v5, v2, [B
    :try_end_35
    .catch Ljava/io/FileNotFoundException; {:try_start_35 .. :try_end_35} :catch_6
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_35} :catch_7

    .line 997
    .local v5, byteReplace:[B
    const/16 v64, 0x0

    .restart local v64       #t:I
    :goto_28
    :try_start_36
    move-object/from16 v0, v58

    array-length v2, v0

    move/from16 v0, v64

    if-ge v0, v2, :cond_81

    .line 998
    aget-object v2, v58, v64

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_76

    aget-object v2, v58, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_76

    .line 999
    const/16 v31, 0x1

    .line 1000
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1002
    :cond_76
    aget-object v2, v58, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_77

    aget-object v2, v58, v64

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_80

    .line 1003
    :cond_77
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1004
    const/4 v2, 0x0

    aput v2, v6, v64

    .line 1006
    :goto_29
    aget-object v2, v58, v64

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "sq"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_78

    .line 1007
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1008
    const/16 v2, 0xfd

    aput v2, v6, v64

    .line 1010
    :cond_78
    aget-object v2, v58, v64

    const-string v7, "s1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_79

    aget-object v2, v58, v64

    const-string v7, "S1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7a

    .line 1011
    :cond_79
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1012
    const/16 v2, 0xfe

    aput v2, v6, v64

    .line 1014
    :cond_7a
    aget-object v2, v58, v64

    const-string v7, "s0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7b

    aget-object v2, v58, v64

    const-string v7, "S0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7c

    .line 1015
    :cond_7b
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1016
    const/16 v2, 0xff

    aput v2, v6, v64

    .line 1018
    :cond_7c
    aget-object v2, v58, v64

    const-string v7, "W"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7d

    aget-object v2, v58, v64

    const-string v7, "w"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7d

    aget-object v2, v58, v64

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7d

    aget-object v2, v58, v64

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7e

    .line 1020
    :cond_7d
    aget-object v2, v58, v64

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "w"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "r"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v51

    .line 1022
    .restart local v51       #or:Ljava/lang/String;
    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v69

    .line 1023
    .restart local v69       #y:I
    add-int/lit8 v69, v69, 0x2

    .line 1024
    aput v69, v6, v64

    .line 1025
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1027
    .end local v51           #or:Ljava/lang/String;
    .end local v69           #y:I
    :cond_7e
    aget-object v2, v58, v64

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v5, v64
    :try_end_36
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_36} :catch_19
    .catch Ljava/io/FileNotFoundException; {:try_start_36 .. :try_end_36} :catch_6

    .line 997
    add-int/lit8 v64, v64, 0x1

    goto/16 :goto_28

    .line 967
    .end local v5           #byteReplace:[B
    .end local v6           #rep_mask:[I
    .end local v58           #rephex:[Ljava/lang/String;
    :cond_7f
    const/16 v48, 0x1

    .line 969
    :try_start_37
    new-instance v45, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v0, v45

    invoke-direct {v0, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;-><init>([B[I)V

    .line 970
    .local v45, local:Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    array-length v2, v3

    new-array v2, v2, [B

    move-object/from16 v0, v45

    iput-object v2, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    .line 971
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    move-object/from16 v0, v45

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_37
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_37} :catch_17
    .catch Ljava/io/FileNotFoundException; {:try_start_37 .. :try_end_37} :catch_6

    goto/16 :goto_26

    .line 972
    .end local v45           #local:Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    :catch_17
    move-exception v29

    .line 973
    .restart local v29       #e:Ljava/lang/Exception;
    :try_start_38
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V

    .line 974
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_26

    .line 985
    .end local v29           #e:Ljava/lang/Exception;
    .end local v64           #t:I
    :catch_18
    move-exception v29

    .line 986
    .local v29, e:Lorg/json/JSONException;
    const-string v2, "Error LP: Error replaced hex read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_38
    .catch Ljava/io/FileNotFoundException; {:try_start_38 .. :try_end_38} :catch_6
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_38} :catch_7

    goto/16 :goto_27

    .line 1005
    .end local v29           #e:Lorg/json/JSONException;
    .restart local v5       #byteReplace:[B
    .restart local v6       #rep_mask:[I
    .restart local v58       #rephex:[Ljava/lang/String;
    .restart local v64       #t:I
    :cond_80
    const/4 v2, 0x1

    :try_start_39
    aput v2, v6, v64
    :try_end_39
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_39} :catch_19
    .catch Ljava/io/FileNotFoundException; {:try_start_39 .. :try_end_39} :catch_6

    goto/16 :goto_29

    .line 1029
    :catch_19
    move-exception v29

    .line 1030
    .local v29, e:Ljava/lang/Exception;
    :try_start_3a
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1032
    .end local v29           #e:Ljava/lang/Exception;
    :cond_81
    array-length v2, v6

    array-length v7, v4

    if-ne v2, v7, :cond_82

    array-length v2, v3

    array-length v7, v5

    if-ne v2, v7, :cond_82

    array-length v2, v5

    const/4 v7, 0x4

    if-lt v2, v7, :cond_82

    array-length v2, v3

    const/4 v7, 0x4

    if-ge v2, v7, :cond_83

    .line 1033
    :cond_82
    const/16 v31, 0x1

    .line 1034
    :cond_83
    if-eqz v31, :cond_84

    .line 1035
    const/16 v60, 0x0

    .line 1036
    const-string v2, "Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1038
    :cond_84
    if-nez v31, :cond_86

    .line 1039
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    if-nez v2, :cond_85

    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->multidex:Z

    if-eqz v2, :cond_91

    :cond_85
    sget-object v70, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    const-string v7, "all_lib"

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v70

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1041
    :goto_2a
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    .line 1045
    .end local v5           #byteReplace:[B
    .end local v6           #rep_mask:[I
    .end local v58           #rephex:[Ljava/lang/String;
    .end local v64           #t:I
    :cond_86
    aget-object v2, v65, v57

    const-string v7, "insert"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_97

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_97

    aget-object v2, v65, v57

    const-string v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_3a
    .catch Ljava/io/FileNotFoundException; {:try_start_3a .. :try_end_3a} :catch_6
    .catch Ljava/lang/Exception; {:try_start_3a .. :try_end_3a} :catch_7

    move-result v2

    if-eqz v2, :cond_97

    .line 1048
    :try_start_3b
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1049
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    const-string v2, "insert"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3b
    .catch Lorg/json/JSONException; {:try_start_3b .. :try_end_3b} :catch_1a
    .catch Ljava/io/FileNotFoundException; {:try_start_3b .. :try_end_3b} :catch_6
    .catch Ljava/lang/Exception; {:try_start_3b .. :try_end_3b} :catch_7

    move-result-object v67

    .line 1053
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :goto_2b
    :try_start_3c
    invoke-virtual/range {v67 .. v67}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v67

    .line 1054
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    if-eqz v2, :cond_87

    .line 1055
    invoke-static/range {v67 .. v67}, Lcom/chelpus/Utils;->rework(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v67

    .line 1057
    :cond_87
    const-string v2, "[ \t]+"

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v58, v0

    .line 1058
    .restart local v58       #rephex:[Ljava/lang/String;
    const-string v2, "[ \t]+"

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v58

    .line 1059
    move-object/from16 v0, v58

    array-length v2, v0

    new-array v6, v2, [I

    .line 1060
    .restart local v6       #rep_mask:[I
    move-object/from16 v0, v58

    array-length v2, v0

    new-array v5, v2, [B
    :try_end_3c
    .catch Ljava/io/FileNotFoundException; {:try_start_3c .. :try_end_3c} :catch_6
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_3c} :catch_7

    .line 1062
    .restart local v5       #byteReplace:[B
    const/16 v64, 0x0

    .restart local v64       #t:I
    :goto_2c
    :try_start_3d
    move-object/from16 v0, v58

    array-length v2, v0

    move/from16 v0, v64

    if-ge v0, v2, :cond_93

    .line 1063
    aget-object v2, v58, v64

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_88

    aget-object v2, v58, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_88

    .line 1064
    const/16 v31, 0x1

    .line 1065
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1067
    :cond_88
    aget-object v2, v58, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_89

    aget-object v2, v58, v64

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_92

    .line 1068
    :cond_89
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1069
    const/4 v2, 0x0

    aput v2, v6, v64

    .line 1071
    :goto_2d
    aget-object v2, v58, v64

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "sq"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8a

    .line 1072
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1073
    const/16 v2, 0xfd

    aput v2, v6, v64

    .line 1075
    :cond_8a
    aget-object v2, v58, v64

    const-string v7, "s1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8b

    aget-object v2, v58, v64

    const-string v7, "S1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8c

    .line 1076
    :cond_8b
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1077
    const/16 v2, 0xfe

    aput v2, v6, v64

    .line 1079
    :cond_8c
    aget-object v2, v58, v64

    const-string v7, "s0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8d

    aget-object v2, v58, v64

    const-string v7, "S0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8e

    .line 1080
    :cond_8d
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1081
    const/16 v2, 0xff

    aput v2, v6, v64

    .line 1083
    :cond_8e
    aget-object v2, v58, v64

    const-string v7, "W"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8f

    aget-object v2, v58, v64

    const-string v7, "w"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8f

    aget-object v2, v58, v64

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8f

    aget-object v2, v58, v64

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_90

    .line 1085
    :cond_8f
    aget-object v2, v58, v64

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "w"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "r"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v51

    .line 1087
    .restart local v51       #or:Ljava/lang/String;
    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v69

    .line 1088
    .restart local v69       #y:I
    add-int/lit8 v69, v69, 0x2

    .line 1089
    aput v69, v6, v64

    .line 1090
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1092
    .end local v51           #or:Ljava/lang/String;
    .end local v69           #y:I
    :cond_90
    aget-object v2, v58, v64

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v5, v64
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_3d} :catch_1b
    .catch Ljava/io/FileNotFoundException; {:try_start_3d .. :try_end_3d} :catch_6

    .line 1062
    add-int/lit8 v64, v64, 0x1

    goto/16 :goto_2c

    .line 1040
    :cond_91
    :try_start_3e
    sget-object v70, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v70

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2a

    .line 1050
    .end local v5           #byteReplace:[B
    .end local v6           #rep_mask:[I
    .end local v58           #rephex:[Ljava/lang/String;
    .end local v64           #t:I
    :catch_1a
    move-exception v29

    .line 1051
    .local v29, e:Lorg/json/JSONException;
    const-string v2, "Error LP: Error insert hex read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_3e
    .catch Ljava/io/FileNotFoundException; {:try_start_3e .. :try_end_3e} :catch_6
    .catch Ljava/lang/Exception; {:try_start_3e .. :try_end_3e} :catch_7

    goto/16 :goto_2b

    .line 1070
    .end local v29           #e:Lorg/json/JSONException;
    .restart local v5       #byteReplace:[B
    .restart local v6       #rep_mask:[I
    .restart local v58       #rephex:[Ljava/lang/String;
    .restart local v64       #t:I
    :cond_92
    const/4 v2, 0x1

    :try_start_3f
    aput v2, v6, v64
    :try_end_3f
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_3f} :catch_1b
    .catch Ljava/io/FileNotFoundException; {:try_start_3f .. :try_end_3f} :catch_6

    goto/16 :goto_2d

    .line 1094
    :catch_1b
    move-exception v29

    .line 1095
    .local v29, e:Ljava/lang/Exception;
    :try_start_40
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1097
    .end local v29           #e:Ljava/lang/Exception;
    :cond_93
    array-length v2, v5

    const/4 v7, 0x4

    if-lt v2, v7, :cond_94

    array-length v2, v3

    const/4 v7, 0x4

    if-ge v2, v7, :cond_95

    :cond_94
    const/16 v31, 0x1

    .line 1098
    :cond_95
    if-eqz v31, :cond_96

    .line 1099
    const/16 v60, 0x0

    .line 1100
    const-string v2, "Error LP: Dimensions of the original hex-string and repleced must be >3.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1102
    :cond_96
    if-nez v31, :cond_97

    .line 1103
    sget-object v70, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v70

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1104
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    .line 1108
    .end local v5           #byteReplace:[B
    .end local v6           #rep_mask:[I
    .end local v58           #rephex:[Ljava/lang/String;
    .end local v64           #t:I
    :cond_97
    aget-object v2, v65, v57

    const-string v7, "replace_from_file"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9a

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9a

    aget-object v2, v65, v57

    const-string v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_40
    .catch Ljava/io/FileNotFoundException; {:try_start_40 .. :try_end_40} :catch_6
    .catch Ljava/lang/Exception; {:try_start_40 .. :try_end_40} :catch_7

    move-result v2

    if-eqz v2, :cond_9a

    .line 1111
    :try_start_41
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1112
    .restart local v40       #json_obj:Lorg/json/JSONObject;
    const-string v2, "replace_from_file"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_41
    .catch Lorg/json/JSONException; {:try_start_41 .. :try_end_41} :catch_1c
    .catch Ljava/io/FileNotFoundException; {:try_start_41 .. :try_end_41} :catch_6
    .catch Ljava/lang/Exception; {:try_start_41 .. :try_end_41} :catch_7

    move-result-object v67

    .line 1116
    .end local v40           #json_obj:Lorg/json/JSONObject;
    :goto_2e
    :try_start_42
    invoke-virtual/range {v67 .. v67}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v67

    .line 1118
    new-instance v12, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v7, Ljava/io/File;

    const/4 v8, 0x1

    aget-object v8, p0, v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "/"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v12, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1119
    .local v12, arrayFile:Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v7

    long-to-int v0, v7

    move/from16 v41, v0

    .line 1120
    .local v41, len:I
    move/from16 v0, v41

    new-array v5, v0, [B
    :try_end_42
    .catch Ljava/io/FileNotFoundException; {:try_start_42 .. :try_end_42} :catch_6
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_42} :catch_7

    .line 1124
    .restart local v5       #byteReplace:[B
    :try_start_43
    new-instance v24, Ljava/io/FileInputStream;

    move-object/from16 v0, v24

    invoke-direct {v0, v12}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1125
    .local v24, data3:Ljava/io/FileInputStream;
    :cond_98
    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/io/FileInputStream;->read([B)I

    move-result v2

    if-gtz v2, :cond_98

    .line 1127
    invoke-virtual/range {v24 .. v24}, Ljava/io/FileInputStream;->close()V
    :try_end_43
    .catch Ljava/lang/Exception; {:try_start_43 .. :try_end_43} :catch_1d
    .catch Ljava/io/FileNotFoundException; {:try_start_43 .. :try_end_43} :catch_6

    .line 1131
    .end local v24           #data3:Ljava/io/FileInputStream;
    :goto_2f
    :try_start_44
    move/from16 v0, v41

    new-array v6, v0, [I

    .line 1132
    .restart local v6       #rep_mask:[I
    const/4 v2, 0x1

    invoke-static {v6, v2}, Ljava/util/Arrays;->fill([II)V

    .line 1135
    if-eqz v31, :cond_99

    .line 1136
    const/16 v60, 0x0

    .line 1137
    const-string v2, "Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1139
    :cond_99
    if-nez v31, :cond_9a

    .line 1140
    sget-object v70, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v70

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1141
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    .line 1145
    .end local v5           #byteReplace:[B
    .end local v6           #rep_mask:[I
    .end local v12           #arrayFile:Ljava/io/File;
    .end local v41           #len:I
    :cond_9a
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[ADD-BOOT]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9b

    .line 1146
    const-string v2, "Patch on Reboot added!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1149
    :cond_9b
    if-eqz v30, :cond_9c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v7, v65, v57

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    .line 1150
    :cond_9c
    const-string v2, "[END]"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9d

    .line 1151
    const/4 v2, 0x4

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 1152
    const/16 v30, 0x1

    .line 1154
    :cond_9d
    add-int/lit8 v57, v57, 0x1

    goto/16 :goto_7

    .line 1113
    :catch_1c
    move-exception v29

    .line 1114
    .local v29, e:Lorg/json/JSONException;
    const-string v2, "Error LP: Error replaced hex read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_2e

    .line 1128
    .end local v29           #e:Lorg/json/JSONException;
    .restart local v5       #byteReplace:[B
    .restart local v12       #arrayFile:Ljava/io/File;
    .restart local v41       #len:I
    :catch_1d
    move-exception v29

    .line 1129
    .local v29, e:Ljava/lang/Exception;
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2f

    .line 1157
    .end local v5           #byteReplace:[B
    .end local v12           #arrayFile:Ljava/io/File;
    .end local v29           #e:Ljava/lang/Exception;
    .end local v41           #len:I
    :cond_9e
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedReader;->close()V

    .line 1158
    if-eqz v63, :cond_9f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1159
    :cond_9f
    if-nez v63, :cond_a0

    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->patchteil:Z

    if-eqz v2, :cond_a4

    .line 1160
    const-string v2, "Not all patterns are replaced, but the program can work, test it!\nCustom Patch not valid for this Version of the Programm or already patched. "

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1163
    :cond_a0
    :goto_30
    if-eqz v21, :cond_a1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Changes Fix to: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1165
    :cond_a1
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->fixunpack:Z

    if-eqz v2, :cond_a3

    .line 1166
    const-string v2, "Analise Results:"

    invoke-static {v2}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1167
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    sget-object v70, Lcom/chelpus/root/utils/custompatch;->uid:Ljava/lang/String;

    sget-object v71, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    sget-object v72, Lcom/chelpus/root/utils/custompatch;->uid:Ljava/lang/String;

    invoke-static/range {v71 .. v72}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v71

    move-object/from16 v0, v70

    move-object/from16 v1, v71

    invoke-static {v2, v7, v8, v0, v1}, Lcom/chelpus/Utils;->create_ODEX_root(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v59

    .line 1168
    .local v59, res:I
    if-eqz v59, :cond_a2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Create odex error "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v59

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1169
    :cond_a2
    if-nez v59, :cond_a3

    .line 1170
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\n~Package reworked!~\nChanges Fix to: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v7, 0x2

    aget-object v7, p0, v7

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1174
    .end local v59           #res:I
    :cond_a3
    invoke-virtual/range {v38 .. v38}, Ljava/io/InputStreamReader;->close()V

    .line 1175
    invoke-virtual/range {v36 .. v36}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_4

    .line 1162
    :cond_a4
    const-string v2, "Custom Patch not valid for this Version of the Programm or already patched. "

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_44
    .catch Ljava/io/FileNotFoundException; {:try_start_44 .. :try_end_44} :catch_6
    .catch Ljava/lang/Exception; {:try_start_44 .. :try_end_44} :catch_7

    goto/16 :goto_30

    .line 267
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_2
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_d
        :pswitch_c
    .end packed-switch
.end method

.method public static patchProcess(Ljava/util/ArrayList;)Z
    .locals 30
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1193
    .local p0, patchlist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;>;"
    const/16 v23, 0x1

    .line 1194
    .local v23, patch:Z
    if-eqz p0, :cond_0

    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 1195
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    .line 1196
    .local v19, it:Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    const/4 v4, 0x0

    move-object/from16 v0, v19

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->result:Z

    goto :goto_0

    .line 1199
    .end local v19           #it:Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    :cond_0
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "chmod"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "777"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1201
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    const-string v5, "rw"

    invoke-direct {v3, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 1202
    .local v2, ChannelDex:Ljava/nio/channels/FileChannel;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Size file:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/Utils;->sendFromRootCP(Ljava/lang/String;)Z

    .line 1203
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v16

    .line 1205
    .local v16, fileBytes:Ljava/nio/MappedByteBuffer;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v3

    array-length v3, v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object/from16 v25, v0

    .line 1207
    .local v25, patches:[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object/from16 v25, v0

    .line 1208
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object v0, v3

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object/from16 v25, v0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    .line 1210
    const/4 v13, -0x1

    .line 1213
    .local v13, curentPos:I
    const/16 v26, 0x0

    .line 1214
    .local v26, period:I
    const/4 v8, 0x0

    .line 1215
    .local v8, Interapt:Z
    :cond_1
    :try_start_1
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_14

    if-nez v8, :cond_14

    .line 1217
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v3

    sub-int v3, v3, v26

    const v4, 0x7d000

    if-le v3, v4, :cond_2

    .line 1218
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Progress size:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/Utils;->sendFromRootCP(Ljava/lang/String;)Z

    .line 1219
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v26

    .line 1221
    :cond_2
    add-int/lit8 v3, v13, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1222
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v13

    .line 1223
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v12

    .line 1225
    .local v12, curentByte:B
    const/16 v17, 0x0

    .local v17, g:I
    :goto_1
    move-object/from16 v0, v25

    array-length v3, v0

    move/from16 v0, v17

    if-ge v0, v3, :cond_1

    .line 1226
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1228
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    if-eq v12, v3, :cond_3

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/4 v4, 0x1

    if-le v3, v4, :cond_13

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    aget-object v4, v25, v17

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    if-ne v12, v3, :cond_13

    .line 1230
    :cond_3
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-nez v3, :cond_4

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    aput-byte v12, v3, v4
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4

    .line 1232
    :cond_4
    :try_start_2
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/4 v4, 0x1

    if-le v3, v4, :cond_5

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xfd

    if-ge v3, v4, :cond_5

    aget-object v3, v25, v17

    iget-object v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v5, 0x0

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    aget-object v6, v25, v17

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    add-int/lit8 v6, v6, -0x2

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    aput-byte v3, v4, v5
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_4

    .line 1234
    :cond_5
    :goto_2
    :try_start_3
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xfd

    if-ne v3, v4, :cond_6

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    and-int/lit8 v5, v12, 0xf

    and-int/lit8 v6, v12, 0xf

    mul-int/lit8 v6, v6, 0x10

    add-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 1235
    :cond_6
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xfe

    if-ne v3, v4, :cond_7

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    and-int/lit8 v5, v12, 0xf

    add-int/lit8 v5, v5, 0x10

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 1236
    :cond_7
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xff

    if-ne v3, v4, :cond_8

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    and-int/lit8 v5, v12, 0xf

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 1237
    :cond_8
    const/16 v18, 0x1

    .line 1238
    .local v18, i:I
    add-int/lit8 v3, v13, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1239
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v27

    .line 1241
    .local v27, prufbyte:B
    :goto_3
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    aget-byte v3, v3, v18

    move/from16 v0, v27

    if-eq v0, v3, :cond_a

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    aget v3, v3, v18

    const/4 v4, 0x1

    if-le v3, v4, :cond_9

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    aget-object v4, v25, v17

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    aget v4, v4, v18

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    move/from16 v0, v27

    if-eq v0, v3, :cond_a

    :cond_9
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    aget v3, v3, v18
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_4

    const/4 v4, 0x1

    if-ne v3, v4, :cond_13

    .line 1244
    :cond_a
    :try_start_4
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    if-nez v3, :cond_b

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    aput-byte v27, v3, v18

    .line 1246
    :cond_b
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    const/4 v4, 0x1

    if-le v3, v4, :cond_c

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    const/16 v4, 0xfd

    if-ge v3, v4, :cond_c

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    add-int/lit8 v29, v3, -0x2

    .local v29, y:I
    aget-object v3, v25, v17

    iget-object v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    aput-byte v3, v4, v18
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_4

    .line 1248
    .end local v29           #y:I
    :cond_c
    :goto_4
    :try_start_5
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xfd

    if-ne v3, v4, :cond_d

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    and-int/lit8 v5, v12, 0xf

    and-int/lit8 v6, v12, 0xf

    mul-int/lit8 v6, v6, 0x10

    add-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 1249
    :cond_d
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    const/16 v4, 0xfe

    if-ne v3, v4, :cond_e

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    and-int/lit8 v4, v27, 0xf

    add-int/lit8 v4, v4, 0x10

    int-to-byte v4, v4

    aput-byte v4, v3, v18

    .line 1250
    :cond_e
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    const/16 v4, 0xff

    if-ne v3, v4, :cond_f

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    and-int/lit8 v4, v27, 0xf

    int-to-byte v4, v4

    aput-byte v4, v3, v18

    .line 1252
    :cond_f
    add-int/lit8 v18, v18, 0x1

    .line 1254
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    array-length v3, v3

    move/from16 v0, v18

    if-ne v0, v3, :cond_19

    .line 1256
    const/4 v15, 0x0

    .line 1257
    .local v15, f:Z
    aget-object v3, v25, v17

    iget-object v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    array-length v5, v4

    const/4 v3, 0x0

    :goto_5
    if-ge v3, v5, :cond_10

    aget v9, v4, v3

    .local v9, b:I
    if-nez v9, :cond_18

    const/4 v15, 0x1

    .line 1258
    .end local v9           #b:I
    :cond_10
    if-nez v15, :cond_11

    const/4 v8, 0x1

    .line 1261
    :cond_11
    aget-object v3, v25, v17

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->insert:Z

    if-eqz v3, :cond_12

    .line 1262
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v22

    .line 1263
    .local v22, p:I
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v3

    long-to-int v3, v3

    sub-int v20, v3, v22

    .line 1266
    .local v20, lenght:I
    move/from16 v0, v20

    new-array v10, v0, [B

    .line 1267
    .local v10, buf:[B
    const/4 v3, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v10, v3, v1}, Ljava/nio/MappedByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 1269
    invoke-static {v10}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 1270
    .local v11, buffer:Ljava/nio/ByteBuffer;
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    array-length v3, v3

    aget-object v4, v25, v17

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    array-length v4, v4

    sub-int/2addr v3, v4

    add-int v3, v3, v22

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 1271
    invoke-virtual {v2, v11}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 1276
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v16

    .line 1277
    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1279
    .end local v10           #buf:[B
    .end local v11           #buffer:Ljava/nio/ByteBuffer;
    .end local v20           #lenght:I
    .end local v22           #p:I
    :cond_12
    int-to-long v3, v13

    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 1280
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 1281
    .restart local v11       #buffer:Ljava/nio/ByteBuffer;
    invoke-virtual {v2, v11}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 1282
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 1283
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\nPattern N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v17, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": Patch done! \n(Offset: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1284
    aget-object v3, v25, v17

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->result:Z

    .line 1285
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/custompatch;->patchteil:Z

    .line 1225
    .end local v11           #buffer:Ljava/nio/ByteBuffer;
    .end local v15           #f:Z
    .end local v18           #i:I
    .end local v27           #prufbyte:B
    :cond_13
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_1

    .line 1233
    :catch_0
    move-exception v14

    .local v14, e:Ljava/lang/Exception;
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    add-int/lit8 v29, v3, -0x2

    .restart local v29       #y:I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Byte N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not found! Please edit search pattern for byte "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_2

    .line 1300
    .end local v12           #curentByte:B
    .end local v14           #e:Ljava/lang/Exception;
    .end local v17           #g:I
    .end local v29           #y:I
    :catch_1
    move-exception v14

    .line 1301
    .local v14, e:Ljava/lang/IndexOutOfBoundsException;
    :try_start_6
    invoke-virtual {v14}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 1302
    const-string v3, "Byte by search not found! Please edit pattern for search.\n"

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1307
    .end local v14           #e:Ljava/lang/IndexOutOfBoundsException;
    :cond_14
    :goto_6
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 1308
    const-string v3, "Analise Results:"

    invoke-static {v3}, Lcom/chelpus/Utils;->sendFromRootCP(Ljava/lang/String;)Z

    .line 1309
    const/16 v17, 0x0

    .restart local v17       #g:I
    :goto_7
    move-object/from16 v0, v25

    array-length v3, v0

    move/from16 v0, v17

    if-ge v0, v3, :cond_20

    .line 1311
    aget-object v3, v25, v17

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->result:Z

    if-nez v3, :cond_1e

    .line 1313
    const/16 v28, 0x0

    .line 1314
    .local v28, trueGroup:Z
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1a

    .line 1315
    move-object/from16 v0, v25

    array-length v4, v0

    const/4 v3, 0x0

    :goto_8
    if-ge v3, v4, :cond_1a

    aget-object v24, v25, v3

    .line 1316
    .local v24, patche:Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    aget-object v5, v25, v17

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    move-object/from16 v0, v24

    iget-object v6, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    move-object/from16 v0, v24

    iget-boolean v5, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->result:Z

    if-eqz v5, :cond_17

    .line 1317
    sget-boolean v5, Lcom/chelpus/root/utils/custompatch;->multidex:Z

    if-nez v5, :cond_15

    sget-boolean v5, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    if-eqz v5, :cond_16

    .line 1319
    :cond_15
    const/4 v5, 0x1

    sput-boolean v5, Lcom/chelpus/root/utils/custompatch;->goodResult:Z
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/nio/BufferUnderflowException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    .line 1321
    :cond_16
    const/16 v28, 0x1

    .line 1315
    :cond_17
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 1247
    .end local v24           #patche:Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    .end local v28           #trueGroup:Z
    .restart local v12       #curentByte:B
    .restart local v18       #i:I
    .restart local v27       #prufbyte:B
    :catch_2
    move-exception v14

    .local v14, e:Ljava/lang/Exception;
    :try_start_7
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    add-int/lit8 v29, v3, -0x2

    .restart local v29       #y:I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Byte N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not found! Please edit search pattern for byte "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1303
    .end local v12           #curentByte:B
    .end local v14           #e:Ljava/lang/Exception;
    .end local v17           #g:I
    .end local v18           #i:I
    .end local v27           #prufbyte:B
    .end local v29           #y:I
    :catch_3
    move-exception v3

    goto/16 :goto_6

    .line 1257
    .restart local v9       #b:I
    .restart local v12       #curentByte:B
    .restart local v15       #f:Z
    .restart local v17       #g:I
    .restart local v18       #i:I
    .restart local v27       #prufbyte:B
    :cond_18
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    .line 1292
    .end local v9           #b:I
    .end local v15           #f:Z
    :cond_19
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->get()B
    :try_end_7
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_4

    move-result v27

    goto/16 :goto_3

    .line 1325
    .end local v12           #curentByte:B
    .end local v18           #i:I
    .end local v27           #prufbyte:B
    .restart local v28       #trueGroup:Z
    :cond_1a
    if-nez v28, :cond_1b

    :try_start_8
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    if-nez v3, :cond_1b

    .line 1326
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->multidex:Z

    if-eqz v3, :cond_1c

    .line 1327
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    if-nez v3, :cond_1b

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 1328
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\nPattern N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v17, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":\nError LP: Pattern not found!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1329
    const/16 v23, 0x0

    .line 1309
    .end local v28           #trueGroup:Z
    :cond_1b
    :goto_9
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_7

    .line 1332
    .restart local v28       #trueGroup:Z
    :cond_1c
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    if-eqz v3, :cond_1d

    .line 1333
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    if-nez v3, :cond_1b

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 1334
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\nPattern N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v17, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":\nError LP: Pattern not found!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1335
    const/16 v23, 0x0

    goto :goto_9

    .line 1338
    :cond_1d
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\nPattern N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v17, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":\nError LP: Pattern not found!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1339
    const/16 v23, 0x0

    goto :goto_9

    .line 1346
    .end local v28           #trueGroup:Z
    :cond_1e
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->multidex:Z

    if-nez v3, :cond_1f

    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    if-eqz v3, :cond_1b

    :cond_1f
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/custompatch;->goodResult:Z
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/nio/BufferUnderflowException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_9

    .line 1351
    .end local v2           #ChannelDex:Ljava/nio/channels/FileChannel;
    .end local v8           #Interapt:Z
    .end local v13           #curentPos:I
    .end local v16           #fileBytes:Ljava/nio/MappedByteBuffer;
    .end local v17           #g:I
    .end local v25           #patches:[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    .end local v26           #period:I
    :catch_4
    move-exception v21

    .line 1352
    .local v21, localFileNotFoundException:Ljava/io/FileNotFoundException;
    const-string v3, "Error LP: Program files are not found!\nMove Program to internal storage."

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1360
    .end local v21           #localFileNotFoundException:Ljava/io/FileNotFoundException;
    :cond_20
    :goto_a
    sget v3, Lcom/chelpus/root/utils/custompatch;->tag:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_21

    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    if-eqz v3, :cond_22

    :cond_21
    sget v3, Lcom/chelpus/root/utils/custompatch;->tag:I

    const/16 v4, 0xa

    if-ne v3, v4, :cond_23

    .line 1361
    :cond_22
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->ART:Z

    if-nez v3, :cond_23

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/chelpus/Utils;->fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V

    .line 1363
    :cond_23
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    if-eqz v3, :cond_24

    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->odexpatch:Z

    if-nez v3, :cond_24

    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->OdexPatch:Z

    if-nez v3, :cond_24

    .line 1364
    sget-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-static {v3}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 1366
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/custompatch;->fixunpack:Z

    .line 1368
    :cond_24
    return v23

    .line 1356
    :catch_5
    move-exception v14

    .line 1357
    .restart local v14       #e:Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception e"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v14}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto :goto_a

    .line 1353
    .end local v14           #e:Ljava/lang/Exception;
    :catch_6
    move-exception v3

    goto :goto_a

    .line 1304
    .restart local v2       #ChannelDex:Ljava/nio/channels/FileChannel;
    .restart local v8       #Interapt:Z
    .restart local v13       #curentPos:I
    .restart local v16       #fileBytes:Ljava/nio/MappedByteBuffer;
    .restart local v25       #patches:[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    .restart local v26       #period:I
    :catch_7
    move-exception v3

    goto/16 :goto_6
.end method

.method public static searchDalvik(Ljava/lang/String;)V
    .locals 6
    .parameter "apk_file"

    .prologue
    .line 1471
    invoke-static {p0}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 1475
    .local v0, dalv:Ljava/io/File;
    if-eqz v0, :cond_0

    .line 1476
    :try_start_0
    sput-object v0, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1478
    :cond_0
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->odexpatch:Z

    if-eqz v3, :cond_1

    .line 1479
    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1483
    :cond_1
    :try_start_1
    sget-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1486
    :cond_2
    :goto_0
    :try_start_2
    sget-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    new-instance v3, Ljava/io/FileNotFoundException;

    invoke-direct {v3}, Ljava/io/FileNotFoundException;-><init>()V

    throw v3
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1488
    :catch_0
    move-exception v2

    .line 1489
    .local v2, localFileNotFoundException:Ljava/io/FileNotFoundException;
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->system:Z

    if-nez v3, :cond_3

    const-string v3, "Error LP: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1495
    .end local v2           #localFileNotFoundException:Ljava/io/FileNotFoundException;
    :cond_3
    :goto_1
    return-void

    .line 1485
    :catch_1
    move-exception v1

    .local v1, e:Ljava/lang/Exception;
    :try_start_3
    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 1491
    .end local v1           #e:Ljava/lang/Exception;
    :catch_2
    move-exception v1

    .line 1492
    .restart local v1       #e:Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static searchDalvikOdex(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .parameter "param"
    .parameter "param2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 1499
    invoke-static {p0}, Lcom/chelpus/root/utils/custompatch;->searchDalvik(Ljava/lang/String;)V

    .line 1503
    :try_start_0
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->odexpatch:Z

    if-nez v3, :cond_6

    .line 1504
    sget-object v3, Lcom/chelpus/root/utils/custompatch;->uid:Ljava/lang/String;

    invoke-static {p1, v3}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1505
    .local v1, backTemp:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1507
    .local v0, backFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1508
    :cond_0
    new-instance v0, Ljava/io/File;

    .end local v0           #backFile:Ljava/io/File;
    const-string v3, "-2"

    const-string v4, "-1"

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1509
    .restart local v0       #backFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1510
    :cond_1
    new-instance v0, Ljava/io/File;

    .end local v0           #backFile:Ljava/io/File;
    const-string v3, "-1"

    const-string v4, "-2"

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1511
    .restart local v0       #backFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1512
    :cond_2
    new-instance v0, Ljava/io/File;

    .end local v0           #backFile:Ljava/io/File;
    const-string v3, "-2"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1513
    .restart local v0       #backFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1514
    :cond_3
    new-instance v0, Ljava/io/File;

    .end local v0           #backFile:Ljava/io/File;
    const-string v3, "-1"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1515
    .restart local v0       #backFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1516
    :cond_4
    new-instance v0, Ljava/io/File;

    .end local v0           #backFile:Ljava/io/File;
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1517
    .restart local v0       #backFile:Ljava/io/File;
    sget-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-static {v3, v0}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 1518
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_5

    .line 1519
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "chmod"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "644"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1520
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "chown"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "1000."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/chelpus/root/utils/custompatch;->uid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1521
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "chown"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "1000:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/chelpus/root/utils/custompatch;->uid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1522
    sput-object v0, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1534
    .end local v0           #backFile:Ljava/io/File;
    .end local v1           #backTemp:Ljava/lang/String;
    :cond_5
    :goto_0
    return-void

    .line 1525
    :cond_6
    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1529
    :catch_0
    move-exception v2

    .line 1530
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 1531
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception e"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static searchProcess(Ljava/util/ArrayList;)Z
    .locals 24
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1373
    .local p0, searchlist:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;>;"
    const/16 v19, 0x1

    .line 1374
    .local v19, patch:Z
    const-string v3, ""

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    .line 1375
    if-eqz p0, :cond_0

    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 1376
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    .line 1377
    .local v15, it:Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    const/4 v4, 0x0

    iput-boolean v4, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    goto :goto_0

    .line 1382
    .end local v15           #it:Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    const-string v5, "rw"

    invoke-direct {v3, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 1383
    .local v2, ChannelDex2:Ljava/nio/channels/FileChannel;
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v12

    .line 1385
    .local v12, fileBytes2:Ljava/nio/MappedByteBuffer;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v3

    array-length v3, v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v20, v0

    .line 1387
    .local v20, patches:[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v20, v0

    .line 1388
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object v0, v3

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v20, v0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    .line 1394
    const-wide/16 v16, 0x0

    .local v16, j:J
    :goto_1
    :try_start_1
    invoke-virtual {v12}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1396
    invoke-virtual {v12}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v10

    .line 1397
    .local v10, curentPos:I
    invoke-virtual {v12}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v9

    .line 1399
    .local v9, curentByte:B
    const/4 v13, 0x0

    .local v13, g:I
    :goto_2
    move-object/from16 v0, v20

    array-length v3, v0

    if-ge v13, v3, :cond_8

    .line 1400
    invoke-virtual {v12, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1402
    aget-object v3, v20, v13

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-nez v3, :cond_6

    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origByte:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    if-eq v9, v3, :cond_1

    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-eqz v3, :cond_6

    .line 1404
    :cond_1
    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-eqz v3, :cond_2

    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    const/4 v4, 0x0

    aput-byte v9, v3, v4

    .line 1405
    :cond_2
    const/4 v14, 0x1

    .line 1406
    .local v14, i:I
    add-int/lit8 v3, v10, 0x1

    invoke-virtual {v12, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1407
    invoke-virtual {v12}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v21

    .line 1409
    .local v21, prufbyte:B
    :goto_3
    aget-object v3, v20, v13

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-nez v3, :cond_3

    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origByte:[B

    aget-byte v3, v3, v14

    move/from16 v0, v21

    if-eq v0, v3, :cond_4

    :cond_3
    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v14

    if-eqz v3, :cond_6

    .line 1411
    :cond_4
    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v14

    if-lez v3, :cond_5

    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    aput-byte v21, v3, v14

    .line 1413
    :cond_5
    add-int/lit8 v14, v14, 0x1

    .line 1415
    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origByte:[B

    array-length v3, v3

    if-ne v14, v3, :cond_7

    .line 1417
    aget-object v3, v20, v13

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    .line 1418
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/custompatch;->patchteil:Z

    .line 1399
    .end local v14           #i:I
    .end local v21           #prufbyte:B
    :cond_6
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 1421
    .restart local v14       #i:I
    .restart local v21       #prufbyte:B
    :cond_7
    invoke-virtual {v12}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v21

    goto :goto_3

    .line 1426
    .end local v14           #i:I
    .end local v21           #prufbyte:B
    :cond_8
    add-int/lit8 v3, v10, 0x1

    invoke-virtual {v12, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/nio/BufferUnderflowException; {:try_start_1 .. :try_end_1} :catch_3

    .line 1394
    const-wide/16 v3, 0x1

    add-long v16, v16, v3

    goto/16 :goto_1

    .line 1429
    .end local v9           #curentByte:B
    .end local v10           #curentPos:I
    .end local v13           #g:I
    :catch_0
    move-exception v11

    .line 1430
    .local v11, e:Ljava/lang/Exception;
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Search byte error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1432
    .end local v11           #e:Ljava/lang/Exception;
    :cond_9
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 1433
    const/4 v13, 0x0

    .restart local v13       #g:I
    :goto_4
    move-object/from16 v0, v20

    array-length v3, v0

    if-ge v13, v3, :cond_b

    .line 1435
    aget-object v3, v20, v13

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-nez v3, :cond_a

    .line 1437
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Bytes by serach N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v13, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":\nError LP: Bytes not found!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    .line 1438
    const/16 v19, 0x0

    .line 1433
    :cond_a
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 1441
    :cond_b
    const/4 v13, 0x0

    :goto_5
    move-object/from16 v0, v20

    array-length v3, v0

    if-ge v13, v3, :cond_e

    .line 1442
    aget-object v3, v20, v13

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-eqz v3, :cond_c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\nBytes by search N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v13, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    .line 1444
    :cond_c
    const/16 v22, 0x0

    .local v22, w:I
    :goto_6
    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    array-length v3, v3

    move/from16 v0, v22

    if-ge v0, v3, :cond_f

    .line 1445
    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v22

    const/4 v4, 0x1

    if-le v3, v4, :cond_d

    .line 1446
    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v22
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/nio/BufferUnderflowException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    add-int/lit8 v23, v3, -0x2

    .line 1447
    .local v23, y:I
    :try_start_3
    sget-object v3, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    aget-object v4, v20, v13

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    aget-byte v4, v4, v22

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/nio/BufferUnderflowException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1448
    :goto_7
    :try_start_4
    aget-object v3, v20, v13

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-eqz v3, :cond_d

    .line 1449
    const/4 v3, 0x1

    new-array v8, v3, [B

    .line 1450
    .local v8, bytik:[B
    const/4 v4, 0x0

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    aput-byte v3, v8, v4

    .line 1451
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "R"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v8}, Lcom/chelpus/Utils;->bytesToHex([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    .line 1444
    .end local v8           #bytik:[B
    .end local v23           #y:I
    :cond_d
    add-int/lit8 v22, v22, 0x1

    goto :goto_6

    .line 1447
    .restart local v23       #y:I
    :catch_1
    move-exception v11

    .restart local v11       #e:Ljava/lang/Exception;
    sget-object v3, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    aget-object v4, v20, v13

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    aget-byte v4, v4, v22

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/nio/BufferUnderflowException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_7

    .line 1458
    .end local v2           #ChannelDex2:Ljava/nio/channels/FileChannel;
    .end local v11           #e:Ljava/lang/Exception;
    .end local v12           #fileBytes2:Ljava/nio/MappedByteBuffer;
    .end local v13           #g:I
    .end local v16           #j:J
    .end local v20           #patches:[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    .end local v22           #w:I
    .end local v23           #y:I
    :catch_2
    move-exception v18

    .line 1459
    .local v18, localFileNotFoundException:Ljava/io/FileNotFoundException;
    const-string v3, "Error LP: Program files are not found!\nMove Program to internal storage."

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1465
    .end local v18           #localFileNotFoundException:Ljava/io/FileNotFoundException;
    :cond_e
    :goto_8
    return v19

    .line 1455
    .restart local v2       #ChannelDex2:Ljava/nio/channels/FileChannel;
    .restart local v12       #fileBytes2:Ljava/nio/MappedByteBuffer;
    .restart local v13       #g:I
    .restart local v16       #j:J
    .restart local v20       #patches:[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    .restart local v22       #w:I
    :cond_f
    :try_start_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/nio/BufferUnderflowException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 1441
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_5

    .line 1460
    .end local v2           #ChannelDex2:Ljava/nio/channels/FileChannel;
    .end local v12           #fileBytes2:Ljava/nio/MappedByteBuffer;
    .end local v13           #g:I
    .end local v16           #j:J
    .end local v20           #patches:[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    .end local v22           #w:I
    :catch_3
    move-exception v11

    .line 1461
    .local v11, e:Ljava/nio/BufferUnderflowException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception e"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11}, Ljava/nio/BufferUnderflowException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto :goto_8

    .line 1462
    .end local v11           #e:Ljava/nio/BufferUnderflowException;
    :catch_4
    move-exception v11

    .line 1463
    .local v11, e:Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception e"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto :goto_8
.end method

.method public static searchfile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .parameter "param"
    .parameter "name"

    .prologue
    .line 1684
    :try_start_0
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "/mnt/sdcard"

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1752
    :cond_0
    :goto_0
    return-void

    .line 1686
    :cond_1
    const-string v7, "/mnt/sdcard"

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "/sdcard"

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1687
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1688
    .local v5, replacers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v7, "/mnt/sdcard/"

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1689
    const-string v7, "/storage/emulated/legacy/"

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1690
    const-string v7, "/storage/emulated/0/"

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1691
    const-string v7, "/storage/sdcard0/"

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1692
    const-string v7, "/storage/sdcard/"

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1693
    const-string v7, "/storage/sdcard1/"

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1694
    const-string v7, "/sdcard/"

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1695
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1696
    .local v4, replacer:Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    const-string v9, "/mnt/sdcard/"

    invoke-virtual {p1, v9, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1697
    sget-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v8

    if-eqz v8, :cond_3

    .line 1700
    :try_start_1
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "test.tmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->createNewFile()Z

    .line 1701
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "test.tmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1702
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "test.tmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1711
    .end local v4           #replacer:Ljava/lang/String;
    :cond_4
    :try_start_2
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1712
    new-instance v7, Ljava/io/FileNotFoundException;

    invoke-direct {v7}, Ljava/io/FileNotFoundException;-><init>()V

    throw v7
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1745
    .end local v5           #replacers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 1747
    .local v1, localFileNotFoundException:Ljava/io/FileNotFoundException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error LP: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " are not found!\n\nRun the application file to appear in the folder with the data.!\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1704
    .end local v1           #localFileNotFoundException:Ljava/io/FileNotFoundException;
    .restart local v4       #replacer:Ljava/lang/String;
    .restart local v5       #replacers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    :try_start_3
    new-instance v8, Ljava/io/File;

    const-string v9, "/figjvaja_papka"

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1

    .line 1705
    :catch_1
    move-exception v0

    .line 1706
    .local v0, e:Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1707
    new-instance v8, Ljava/io/File;

    const-string v9, "/figjvaja_papka"

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_1

    .line 1749
    .end local v0           #e:Ljava/lang/Exception;
    .end local v4           #replacer:Ljava/lang/String;
    .end local v5           #replacers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_2
    move-exception v0

    .line 1750
    .restart local v0       #e:Ljava/lang/Exception;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception e"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1714
    .end local v0           #e:Ljava/lang/Exception;
    :cond_6
    :try_start_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/data/data/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/shared_prefs/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/dbdata/databases/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/shared_prefs/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_7

    const-string v7, "/shared_prefs/"

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 1715
    :cond_7
    const-string v3, ""

    .line 1716
    .local v3, rep_str:Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/data/data/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/shared_prefs/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1717
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/data/data/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/shared_prefs/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1718
    :cond_8
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/dbdata/databases/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/shared_prefs/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1719
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/dbdata/databases/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/shared_prefs/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1720
    :cond_9
    const-string v7, "/shared_prefs/"

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    const-string v3, "/shared_prefs/"

    .line 1721
    :cond_a
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/data/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/shared_prefs/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v3, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1722
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_b

    .line 1723
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/dbdata/databases/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/shared_prefs/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v3, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1724
    :cond_b
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1725
    new-instance v7, Ljava/io/FileNotFoundException;

    invoke-direct {v7}, Ljava/io/FileNotFoundException;-><init>()V

    throw v7

    .line 1727
    .end local v3           #rep_str:Ljava/lang/String;
    :cond_c
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/data/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1728
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_d

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/mnt/asec/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "-1"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1729
    :cond_d
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_e

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/mnt/asec/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "-2"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1730
    :cond_e
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_f

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/mnt/asec/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1731
    :cond_f
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_10

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/sd-ext/data/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1732
    :cond_10
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_11

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/sdext2/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1733
    :cond_11
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_12

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/sdext1/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1734
    :cond_12
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_13

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/sdext/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1735
    :cond_13
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_14

    .line 1736
    new-instance v6, Lcom/chelpus/Utils;

    const-string v7, "fgh"

    invoke-direct {v6, v7}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    .line 1737
    .local v6, ut:Lcom/chelpus/Utils;
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/data/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7, p1}, Lcom/chelpus/Utils;->findFile(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1738
    .local v2, re:Ljava/lang/String;
    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_14

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1740
    .end local v2           #re:Ljava/lang/String;
    .end local v6           #ut:Lcom/chelpus/Utils;
    :cond_14
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    new-instance v7, Ljava/io/FileNotFoundException;

    invoke-direct {v7}, Ljava/io/FileNotFoundException;-><init>()V

    throw v7
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
.end method

.method public static searchlib(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 24
    .parameter "pkgName"
    .parameter "libname"
    .parameter "apk_file"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1537
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1540
    .local v13, libs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    :try_start_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1541
    .local v6, dataDirs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v18, "/data/data/"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1542
    const-string v18, "/mnt/asec/"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1543
    const-string v18, "/sd-ext/data/"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1544
    const-string v18, "/data/sdext2/"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1545
    const-string v18, "/data/sdext1/"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1546
    const-string v18, "/data/sdext/"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1548
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 1549
    .local v16, pakDirs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1550
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "-1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1551
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "-2"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1553
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    const-string v19, "*"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 1554
    const/16 v18, 0x1

    sput-boolean v18, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    .line 1555
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "/data/data/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/lib/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v11

    .line 1556
    .local v11, getFiles:[Ljava/io/File;
    if-eqz v11, :cond_1

    array-length v0, v11

    move/from16 v18, v0

    if-lez v18, :cond_1

    .line 1557
    array-length v0, v11

    move/from16 v19, v0

    const/16 v18, 0x0

    :goto_0
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_1

    aget-object v9, v11, v18

    .line 1558
    .local v9, file:Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v20

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-lez v20, :cond_0

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v20

    const-string v21, ".so"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_0

    invoke-static {v9, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1557
    :cond_0
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    .line 1561
    .end local v9           #file:Ljava/io/File;
    :cond_1
    const-string v12, ""

    .line 1562
    .local v12, index:Ljava/lang/String;
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v18

    const-string v19, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    const-string v19, ".apk"

    const-string v20, ""

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    .line 1563
    new-instance v18, Ljava/io/File;

    const-string v19, "/data/app-lib"

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 1564
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "/data/app-lib/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v11

    .line 1565
    if-eqz v11, :cond_3

    array-length v0, v11

    move/from16 v18, v0

    if-lez v18, :cond_3

    .line 1566
    array-length v0, v11

    move/from16 v19, v0

    const/16 v18, 0x0

    :goto_1
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_3

    aget-object v9, v11, v18

    .line 1567
    .restart local v9       #file:Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v20

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-lez v20, :cond_2

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v20

    const-string v21, ".so"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_2

    invoke-static {v9, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1566
    :cond_2
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 1571
    .end local v9           #file:Ljava/io/File;
    :cond_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_4
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_5

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 1573
    .local v7, dir:Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1574
    .local v4, d:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v19

    if-eqz v19, :cond_6

    .line 1575
    new-instance v3, Lcom/chelpus/Utils;

    const-string v19, "sdf"

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    .line 1576
    .local v3, ad:Lcom/chelpus/Utils;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1577
    .local v10, foundFiles:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    const-string v19, ".so"

    move-object/from16 v0, v19

    invoke-virtual {v3, v4, v0, v10}, Lcom/chelpus/Utils;->findFileEndText(Ljava/io/File;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v17

    .line 1578
    .local v17, result:Ljava/lang/String;
    const-string v19, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_6

    .line 1579
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-lez v19, :cond_6

    .line 1580
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_6

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/io/File;

    .line 1581
    .restart local v9       #file:Ljava/io/File;
    invoke-static {v9, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1582
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Found lib:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 1671
    .end local v3           #ad:Lcom/chelpus/Utils;
    .end local v4           #d:Ljava/io/File;
    .end local v6           #dataDirs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7           #dir:Ljava/lang/String;
    .end local v9           #file:Ljava/io/File;
    .end local v10           #foundFiles:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v11           #getFiles:[Ljava/io/File;
    .end local v12           #index:Ljava/lang/String;
    .end local v16           #pakDirs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v17           #result:Ljava/lang/String;
    :catch_0
    move-exception v14

    .line 1672
    .local v14, localFileNotFoundException:Ljava/io/FileNotFoundException;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error LP: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " are not found!\n\nCheck the location libraries to solve problems!\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1673
    const/4 v13, 0x0

    .line 1679
    .end local v13           #libs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v14           #localFileNotFoundException:Ljava/io/FileNotFoundException;
    :cond_5
    :goto_3
    return-object v13

    .line 1587
    .restart local v4       #d:Ljava/io/File;
    .restart local v6       #dataDirs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v7       #dir:Ljava/lang/String;
    .restart local v11       #getFiles:[Ljava/io/File;
    .restart local v12       #index:Ljava/lang/String;
    .restart local v13       #libs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v16       #pakDirs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    :try_start_1
    new-instance v5, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1588
    .local v5, d1:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v19

    if-eqz v19, :cond_4

    .line 1589
    new-instance v3, Lcom/chelpus/Utils;

    const-string v19, "sdf"

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    .line 1590
    .restart local v3       #ad:Lcom/chelpus/Utils;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1591
    .restart local v10       #foundFiles:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    const-string v19, ".so"

    move-object/from16 v0, v19

    invoke-virtual {v3, v5, v0, v10}, Lcom/chelpus/Utils;->findFileEndText(Ljava/io/File;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v17

    .line 1592
    .restart local v17       #result:Ljava/lang/String;
    const-string v19, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_4

    .line 1593
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-lez v19, :cond_4

    .line 1594
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_4

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/io/File;

    .line 1595
    .restart local v9       #file:Ljava/io/File;
    invoke-static {v9, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1596
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Found lib:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    .line 1675
    .end local v3           #ad:Lcom/chelpus/Utils;
    .end local v4           #d:Ljava/io/File;
    .end local v5           #d1:Ljava/io/File;
    .end local v6           #dataDirs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7           #dir:Ljava/lang/String;
    .end local v9           #file:Ljava/io/File;
    .end local v10           #foundFiles:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v11           #getFiles:[Ljava/io/File;
    .end local v12           #index:Ljava/lang/String;
    .end local v16           #pakDirs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v17           #result:Ljava/lang/String;
    :catch_1
    move-exception v8

    .line 1676
    .local v8, e:Ljava/lang/Exception;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1677
    const/4 v13, 0x0

    goto/16 :goto_3

    .line 1605
    .end local v8           #e:Ljava/lang/Exception;
    .restart local v6       #dataDirs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v16       #pakDirs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_7
    :try_start_2
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "/data/data/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/lib/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1608
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-nez v18, :cond_a

    .line 1609
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_8
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_b

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 1610
    .restart local v7       #dir:Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_9
    :goto_5
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_8

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 1611
    .local v15, pak:Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1612
    .restart local v4       #d:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_9

    .line 1613
    new-instance v3, Lcom/chelpus/Utils;

    const-string v20, "sdf"

    move-object/from16 v0, v20

    invoke-direct {v3, v0}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    .line 1614
    .restart local v3       #ad:Lcom/chelpus/Utils;
    move-object/from16 v0, p1

    invoke-virtual {v3, v4, v0}, Lcom/chelpus/Utils;->findFile(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 1615
    .restart local v17       #result:Ljava/lang/String;
    const-string v20, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_9

    .line 1616
    new-instance v20, Ljava/io/File;

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v20, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1618
    sget-object v20, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    move-object/from16 v0, v20

    invoke-static {v0, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1620
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Found lib:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto :goto_5

    .line 1627
    .end local v3           #ad:Lcom/chelpus/Utils;
    .end local v4           #d:Ljava/io/File;
    .end local v7           #dir:Ljava/lang/String;
    .end local v15           #pak:Ljava/lang/String;
    .end local v17           #result:Ljava/lang/String;
    :cond_a
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    move-object/from16 v0, v18

    invoke-static {v0, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1633
    :cond_b
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v7

    .line 1634
    .local v7, dir:Ljava/io/File;
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/lib"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_e

    .line 1635
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/lib/arm/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1638
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_c

    .line 1640
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    move-object/from16 v0, v18

    invoke-static {v0, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1643
    :cond_c
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/lib/x86/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1644
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_d

    .line 1646
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    move-object/from16 v0, v18

    invoke-static {v0, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1649
    :cond_d
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/lib/mips/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1650
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_e

    .line 1652
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    move-object/from16 v0, v18

    invoke-static {v0, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1658
    :cond_e
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-nez v18, :cond_f

    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "/system/lib/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1659
    :cond_f
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-nez v18, :cond_10

    new-instance v18, Ljava/io/FileNotFoundException;

    invoke-direct/range {v18 .. v18}, Ljava/io/FileNotFoundException;-><init>()V

    throw v18

    .line 1661
    :cond_10
    const-string v12, ""

    .line 1662
    .restart local v12       #index:Ljava/lang/String;
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v18

    const-string v19, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    const-string v19, ".apk"

    const-string v20, ""

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    .line 1663
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "/data/app-lib/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 1665
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "/data/app-lib/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-static {v0, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_3
.end method

.method public static unzip(Ljava/io/File;)V
    .locals 22
    .parameter "apk"

    .prologue
    .line 1755
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->clear()V

    .line 1756
    const/4 v7, 0x0

    .local v7, found1:Z
    const/4 v8, 0x0

    .line 1758
    .local v8, found2:Z
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1759
    .local v6, fin:Ljava/io/FileInputStream;
    new-instance v16, Ljava/util/zip/ZipInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v6}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1760
    .local v16, zin:Ljava/util/zip/ZipInputStream;
    const/4 v15, 0x0

    .line 1762
    .local v15, ze:Ljava/util/zip/ZipEntry;
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v15

    .line 1763
    const/4 v14, 0x1

    .line 1764
    .local v14, search:Z
    :goto_0
    if-eqz v15, :cond_4

    if-eqz v14, :cond_4

    .line 1770
    invoke-virtual {v15}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v11

    .line 1772
    .local v11, haystack:Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v18

    const-string v19, "classes"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1

    const-string v18, ".dex"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1

    const-string v18, "/"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 1774
    new-instance v9, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1776
    .local v9, fout:Ljava/io/FileOutputStream;
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v2, v0, [B

    .line 1779
    .local v2, buffer:[B
    :goto_1
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v12

    .local v12, length:I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v12, v0, :cond_0

    .line 1780
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v9, v2, v0, v12}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1812
    .end local v2           #buffer:[B
    .end local v6           #fin:Ljava/io/FileInputStream;
    .end local v9           #fout:Ljava/io/FileOutputStream;
    .end local v11           #haystack:Ljava/lang/String;
    .end local v12           #length:I
    .end local v14           #search:Z
    .end local v15           #ze:Ljava/util/zip/ZipEntry;
    .end local v16           #zin:Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v4

    .line 1813
    .local v4, e:Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 1815
    :try_start_1
    new-instance v17, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 1819
    .local v17, zipFile:Lnet/lingala/zip4j/core/ZipFile;
    const-string v18, "classes.dex"

    sget-object v19, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v17 .. v19}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1820
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "classes.dex"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1821
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "classes.dex"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1822
    const-string v18, "AndroidManifest.xml"

    sget-object v19, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v17 .. v19}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1823
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "AndroidManifest.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V
    :try_end_1
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 1834
    .end local v17           #zipFile:Lnet/lingala/zip4j/core/ZipFile;
    :goto_2
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1838
    .end local v4           #e:Ljava/lang/Exception;
    :goto_3
    return-void

    .line 1784
    .restart local v2       #buffer:[B
    .restart local v6       #fin:Ljava/io/FileInputStream;
    .restart local v9       #fout:Ljava/io/FileOutputStream;
    .restart local v11       #haystack:Ljava/lang/String;
    .restart local v12       #length:I
    .restart local v14       #search:Z
    .restart local v15       #ze:Ljava/util/zip/ZipEntry;
    .restart local v16       #zin:Ljava/util/zip/ZipInputStream;
    :cond_0
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1785
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 1786
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1787
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1790
    .end local v2           #buffer:[B
    .end local v9           #fout:Ljava/io/FileOutputStream;
    .end local v12           #length:I
    :cond_1
    const-string v18, "AndroidManifest.xml"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 1791
    new-instance v10, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "AndroidManifest.xml"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1793
    .local v10, fout2:Ljava/io/FileOutputStream;
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v3, v0, [B

    .line 1795
    .local v3, buffer2:[B
    :goto_4
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v13

    .local v13, length2:I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v13, v0, :cond_2

    .line 1796
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v10, v3, v0, v13}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_4

    .line 1798
    :cond_2
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1799
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 1800
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "AndroidManifest.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1801
    const/4 v8, 0x1

    .line 1803
    .end local v3           #buffer2:[B
    .end local v10           #fout2:Ljava/io/FileOutputStream;
    .end local v13           #length2:I
    :cond_3
    if-eqz v7, :cond_5

    if-eqz v8, :cond_5

    .line 1804
    const/4 v14, 0x0

    .line 1810
    .end local v11           #haystack:Ljava/lang/String;
    :cond_4
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1811
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_3

    .line 1807
    .restart local v11       #haystack:Ljava/lang/String;
    :cond_5
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v15

    .line 1809
    goto/16 :goto_0

    .line 1825
    .end local v6           #fin:Ljava/io/FileInputStream;
    .end local v11           #haystack:Ljava/lang/String;
    .end local v14           #search:Z
    .end local v15           #ze:Ljava/util/zip/ZipEntry;
    .end local v16           #zin:Ljava/util/zip/ZipInputStream;
    .restart local v4       #e:Ljava/lang/Exception;
    :catch_1
    move-exception v5

    .line 1826
    .local v5, e1:Lnet/lingala/zip4j/exception/ZipException;
    invoke-virtual {v5}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 1827
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error LP: Error classes.dex decompress! "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1828
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1829
    .end local v5           #e1:Lnet/lingala/zip4j/exception/ZipException;
    :catch_2
    move-exception v5

    .line 1830
    .local v5, e1:Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 1831
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error LP: Error classes.dex decompress! "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1832
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_2
.end method
