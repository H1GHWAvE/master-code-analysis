.class public Lcom/chelpus/root/utils/install_to_system$Decompress;
.super Ljava/lang/Object;
.source "install_to_system.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chelpus/root/utils/install_to_system;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Decompress"
.end annotation


# instance fields
.field private _location:Ljava/lang/String;

.field private _zipFile:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "zipFile"
    .parameter "location"

    .prologue
    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    iput-object p1, p0, Lcom/chelpus/root/utils/install_to_system$Decompress;->_zipFile:Ljava/lang/String;

    .line 245
    iput-object p2, p0, Lcom/chelpus/root/utils/install_to_system$Decompress;->_location:Ljava/lang/String;

    .line 247
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/chelpus/root/utils/install_to_system$Decompress;->_dirChecker(Ljava/lang/String;)V

    .line 248
    return-void
.end method

.method private _dirChecker(Ljava/lang/String;)V
    .locals 3
    .parameter "dir"

    .prologue
    .line 314
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/chelpus/root/utils/install_to_system$Decompress;->_location:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 316
    .local v0, f:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    .line 317
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 319
    :cond_0
    return-void
.end method


# virtual methods
.method public unzip()V
    .locals 21

    .prologue
    .line 252
    :try_start_0
    new-instance v10, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/install_to_system$Decompress;->_zipFile:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 253
    .local v10, fin:Ljava/io/FileInputStream;
    new-instance v16, Ljava/util/zip/ZipInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v10}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 254
    .local v16, zin:Ljava/util/zip/ZipInputStream;
    const/4 v15, 0x0

    .line 255
    .local v15, ze:Ljava/util/zip/ZipEntry;
    :cond_0
    :goto_0
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v15

    if-eqz v15, :cond_7

    .line 258
    invoke-virtual {v15}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 259
    invoke-virtual {v15}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/chelpus/root/utils/install_to_system$Decompress;->_dirChecker(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 286
    .end local v10           #fin:Ljava/io/FileInputStream;
    .end local v15           #ze:Ljava/util/zip/ZipEntry;
    .end local v16           #zin:Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v6

    .line 287
    .local v6, e:Ljava/lang/Exception;
    sget-object v18, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Decompressunzip "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 288
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 290
    :try_start_1
    new-instance v17, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/install_to_system$Decompress;->_zipFile:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-direct/range {v17 .. v18}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 293
    .local v17, zipFile:Lnet/lingala/zip4j/core/ZipFile;
    invoke-virtual/range {v17 .. v17}, Lnet/lingala/zip4j/core/ZipFile;->getFileHeaders()Ljava/util/List;

    move-result-object v9

    .line 296
    .local v9, fileHeaderList:Ljava/util/List;
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_1
    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_2

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 297
    .local v3, aFileHeaderList:Ljava/lang/Object;
    move-object v0, v3

    check-cast v0, Lnet/lingala/zip4j/model/FileHeader;

    move-object v8, v0

    .line 298
    .local v8, fileHeader:Lnet/lingala/zip4j/model/FileHeader;
    invoke-virtual {v8}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v19

    const-string v20, ".so"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_1

    invoke-virtual {v8}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v19

    const-string v20, "libjnigraphics.so"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_1

    .line 299
    sget-object v19, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v8}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 300
    invoke-virtual {v8}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/install_to_system$Decompress;->_location:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 304
    .end local v3           #aFileHeaderList:Ljava/lang/Object;
    .end local v8           #fileHeader:Lnet/lingala/zip4j/model/FileHeader;
    .end local v9           #fileHeaderList:Ljava/util/List;
    .end local v17           #zipFile:Lnet/lingala/zip4j/core/ZipFile;
    :catch_1
    move-exception v7

    .line 305
    .local v7, e1:Lnet/lingala/zip4j/exception/ZipException;
    invoke-virtual {v7}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 311
    .end local v6           #e:Ljava/lang/Exception;
    .end local v7           #e1:Lnet/lingala/zip4j/exception/ZipException;
    :cond_2
    :goto_2
    return-void

    .line 262
    .restart local v10       #fin:Ljava/io/FileInputStream;
    .restart local v15       #ze:Ljava/util/zip/ZipEntry;
    .restart local v16       #zin:Ljava/util/zip/ZipInputStream;
    :cond_3
    :try_start_2
    invoke-virtual {v15}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v18

    const-string v19, ".so"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-virtual {v15}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v18

    const-string v19, "libjnigraphics.so"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 263
    invoke-virtual {v15}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v18

    const-string v19, "\\/+"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 264
    .local v14, tail:[Ljava/lang/String;
    const-string v5, ""

    .line 265
    .local v5, data_dir:Ljava/lang/String;
    const/4 v12, 0x0

    .local v12, i:I
    :goto_3
    array-length v0, v14

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    if-ge v12, v0, :cond_5

    .line 266
    aget-object v18, v14, v12

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_4

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v14, v12

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 265
    :cond_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 269
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/chelpus/root/utils/install_to_system$Decompress;->_dirChecker(Ljava/lang/String;)V

    .line 270
    new-instance v11, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/install_to_system$Decompress;->_location:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v15}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v11, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 271
    .local v11, fout:Ljava/io/FileOutputStream;
    const/16 v18, 0x400

    move/from16 v0, v18

    new-array v4, v0, [B

    .line 273
    .local v4, buffer:[B
    :goto_4
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v13

    .local v13, length:I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v13, v0, :cond_6

    .line 274
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v11, v4, v0, v13}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_4

    .line 278
    :cond_6
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 279
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_0

    .line 284
    .end local v4           #buffer:[B
    .end local v5           #data_dir:Ljava/lang/String;
    .end local v11           #fout:Ljava/io/FileOutputStream;
    .end local v12           #i:I
    .end local v13           #length:I
    .end local v14           #tail:[Ljava/lang/String;
    :cond_7
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->close()V

    .line 285
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    .line 306
    .end local v10           #fin:Ljava/io/FileInputStream;
    .end local v15           #ze:Ljava/util/zip/ZipEntry;
    .end local v16           #zin:Ljava/util/zip/ZipInputStream;
    .restart local v6       #e:Ljava/lang/Exception;
    :catch_2
    move-exception v7

    .line 307
    .local v7, e1:Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2
.end method
