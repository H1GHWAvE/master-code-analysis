.class public Lcom/chelpus/root/utils/AdsBlockON;
.super Ljava/lang/Object;
.source "AdsBlockON.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 25
    .parameter "params"

    .prologue
    .line 12
    new-instance v21, Lcom/chelpus/root/utils/AdsBlockON$1;

    invoke-direct/range {v21 .. v21}, Lcom/chelpus/root/utils/AdsBlockON$1;-><init>()V

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 13
    const-string v14, "#Lucky Patcher block Ads start#"

    .line 14
    .local v14, starttag:Ljava/lang/String;
    const-string v7, "#Lucky Patcher block Ads finish#"

    .line 16
    .local v7, endtag:Ljava/lang/String;
    const/16 v21, 0x0

    aget-object v8, p0, v21

    .line 17
    .local v8, hosts:Ljava/lang/String;
    const/16 v21, 0x1

    aget-object v16, p0, v21

    .line 18
    .local v16, tmp_hosts:Ljava/lang/String;
    const/16 v21, 0x2

    aget-object v13, p0, v21

    .line 19
    .local v13, raw:Ljava/lang/String;
    const/16 v21, 0x3

    aget-object v17, p0, v21

    .line 20
    .local v17, toolfilesdir:Ljava/lang/String;
    const-string v4, "/data/data/hosts"

    .line 21
    .local v4, data_hosts:Ljava/lang/String;
    const/4 v9, 0x0

    .line 22
    .local v9, hostsToData:Z
    invoke-static {v8}, Lcom/chelpus/Utils;->getSimulink(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 23
    .local v15, sym_path:Ljava/lang/String;
    const-string v21, ""

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_0

    move-object v4, v15

    .line 25
    :cond_0
    :try_start_0
    const-string v21, "/system"

    const-string v22, "rw"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 26
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "777"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 27
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chattr"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "-ai"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 28
    const/4 v11, 0x0

    .line 29
    .local v11, ram:Ljava/io/RandomAccessFile;
    const/16 v18, 0x0

    .line 30
    .local v18, wr:Ljava/io/RandomAccessFile;
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_1

    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v21

    new-instance v23, Ljava/io/File;

    move-object/from16 v0, v23

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v23

    cmp-long v21, v21, v23

    if-nez v21, :cond_1

    .line 31
    const/4 v9, 0x1

    .line 32
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "Hosts to Data"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 33
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "777"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 34
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chattr"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "-ai"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    .line 37
    :cond_1
    :try_start_1
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    move-result v21

    if-nez v21, :cond_2

    .line 38
    new-instance v21, Ljava/io/File;

    const-string v22, "/system/etc/hosts"

    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v22, "127.0.0.1       localhost\n"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z

    .line 40
    :cond_2
    if-nez v9, :cond_4

    new-instance v12, Ljava/io/RandomAccessFile;

    const-string v21, "r"

    move-object/from16 v0, v21

    invoke-direct {v12, v8, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .end local v11           #ram:Ljava/io/RandomAccessFile;
    .local v12, ram:Ljava/io/RandomAccessFile;
    move-object v11, v12

    .line 42
    .end local v12           #ram:Ljava/io/RandomAccessFile;
    .restart local v11       #ram:Ljava/io/RandomAccessFile;
    :goto_0
    const-wide/16 v21, 0x0

    move-wide/from16 v0, v21

    invoke-virtual {v11, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 43
    new-instance v19, Ljava/io/RandomAccessFile;

    const-string v21, "rw"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2

    .line 44
    .end local v18           #wr:Ljava/io/RandomAccessFile;
    .local v19, wr:Ljava/io/RandomAccessFile;
    const-wide/16 v21, 0x0

    :try_start_2
    move-object/from16 v0, v19

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 45
    const-wide/16 v21, 0x0

    move-object/from16 v0, v19

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 47
    const/16 v20, 0x1

    .line 48
    .local v20, write:Z
    :cond_3
    :goto_1
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;

    move-result-object v10

    .local v10, line:Ljava/lang/String;
    if-eqz v10, :cond_6

    .line 49
    invoke-virtual {v10, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_5

    if-eqz v20, :cond_5

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "\n"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    .line 51
    :goto_2
    invoke-virtual {v10, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2

    move-result v21

    if-eqz v21, :cond_3

    const/16 v20, 0x1

    goto :goto_1

    .line 41
    .end local v10           #line:Ljava/lang/String;
    .end local v19           #wr:Ljava/io/RandomAccessFile;
    .end local v20           #write:Z
    .restart local v18       #wr:Ljava/io/RandomAccessFile;
    :cond_4
    :try_start_3
    new-instance v12, Ljava/io/RandomAccessFile;

    const-string v21, "r"

    move-object/from16 v0, v21

    invoke-direct {v12, v4, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_2

    .end local v11           #ram:Ljava/io/RandomAccessFile;
    .restart local v12       #ram:Ljava/io/RandomAccessFile;
    move-object v11, v12

    .end local v12           #ram:Ljava/io/RandomAccessFile;
    .restart local v11       #ram:Ljava/io/RandomAccessFile;
    goto :goto_0

    .line 50
    .end local v18           #wr:Ljava/io/RandomAccessFile;
    .restart local v10       #line:Ljava/lang/String;
    .restart local v19       #wr:Ljava/io/RandomAccessFile;
    .restart local v20       #write:Z
    :cond_5
    const/16 v20, 0x0

    goto :goto_2

    .line 54
    :cond_6
    :try_start_4
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->close()V

    .line 55
    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_2

    .line 65
    :try_start_5
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v22, Ljava/io/File;

    move-object/from16 v0, v22

    invoke-direct {v0, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->save_text_to_end_file_from_file(Ljava/io/File;Ljava/io/File;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_2

    move-result v21

    if-eqz v21, :cond_d

    .line 67
    :try_start_6
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "/system"

    const-string v23, "rw"

    invoke-static/range {v22 .. v23}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Z)V

    .line 68
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "777"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 69
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "/system"

    const-string v23, "rw"

    invoke-static/range {v22 .. v23}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Z)V

    .line 70
    if-nez v9, :cond_a

    .line 71
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v22, Ljava/io/File;

    const-string v23, "/system/etc/hosts"

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 72
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v21

    new-instance v23, Ljava/io/File;

    const-string v24, "/system/etc/hosts"

    invoke-direct/range {v23 .. v24}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v23

    cmp-long v21, v21, v23

    if-eqz v21, :cond_8

    .line 73
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(I)V

    .line 74
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/io/File;

    const-string v23, "/system/etc/hosts"

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->length()J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Ljava/io/PrintStream;->println(J)V

    .line 76
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 77
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v22, Ljava/io/File;

    const-string v23, "/data/data/hosts"

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 78
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v21

    new-instance v23, Ljava/io/File;

    const-string v24, "/data/data/hosts"

    invoke-direct/range {v23 .. v24}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v23

    cmp-long v21, v21, v23

    if-eqz v21, :cond_7

    .line 79
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "no_space_to_data"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 80
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 81
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 82
    new-instance v21, Ljava/io/File;

    const-string v22, "/system/etc/hosts"

    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v22, "127.0.0.1       localhost\n"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z

    .line 83
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 84
    const-string v21, "/system"

    const-string v22, "ro"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_2

    .line 171
    .end local v10           #line:Ljava/lang/String;
    .end local v11           #ram:Ljava/io/RandomAccessFile;
    .end local v19           #wr:Ljava/io/RandomAccessFile;
    .end local v20           #write:Z
    :goto_3
    return-void

    .line 56
    .restart local v11       #ram:Ljava/io/RandomAccessFile;
    .restart local v18       #wr:Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v6

    .line 57
    .local v6, e1:Ljava/lang/Exception;
    :goto_4
    :try_start_7
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 58
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "no_space_to_data"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 59
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 60
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 61
    const-string v21, "/system"

    const-string v22, "ro"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_3

    .line 161
    .end local v6           #e1:Ljava/lang/Exception;
    .end local v11           #ram:Ljava/io/RandomAccessFile;
    .end local v18           #wr:Ljava/io/RandomAccessFile;
    :catch_1
    move-exception v5

    .line 163
    .local v5, e:Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 164
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "unknown error"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 170
    .end local v5           #e:Ljava/lang/Exception;
    :goto_5
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    goto :goto_3

    .line 87
    .restart local v10       #line:Ljava/lang/String;
    .restart local v11       #ram:Ljava/io/RandomAccessFile;
    .restart local v19       #wr:Ljava/io/RandomAccessFile;
    .restart local v20       #write:Z
    :cond_7
    const/16 v21, 0x3

    :try_start_8
    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 88
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chattr"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "-ai"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 89
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chown"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0.0"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 90
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chown"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0:0"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 91
    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "ln"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "-s"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    const/16 v22, 0x3

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 92
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 93
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 94
    const-string v21, "/system"

    const-string v22, "ro"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 135
    :cond_8
    :goto_6
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "host updated!"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_2

    .line 154
    :cond_9
    :goto_7
    :try_start_9
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 155
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_9 .. :try_end_9} :catch_2

    goto/16 :goto_5

    .line 166
    .end local v10           #line:Ljava/lang/String;
    .end local v11           #ram:Ljava/io/RandomAccessFile;
    .end local v19           #wr:Ljava/io/RandomAccessFile;
    .end local v20           #write:Z
    :catch_2
    move-exception v3

    .line 167
    .local v3, E:Ljava/lang/OutOfMemoryError;
    invoke-virtual {v3}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 168
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "out.of.memory"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 100
    .end local v3           #E:Ljava/lang/OutOfMemoryError;
    .restart local v10       #line:Ljava/lang/String;
    .restart local v11       #ram:Ljava/io/RandomAccessFile;
    .restart local v19       #wr:Ljava/io/RandomAccessFile;
    .restart local v20       #write:Z
    :cond_a
    :try_start_a
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 101
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v22, Ljava/io/File;

    const-string v23, "/data/data/hosts"

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 102
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v21

    new-instance v23, Ljava/io/File;

    const-string v24, "/data/data/hosts"

    invoke-direct/range {v23 .. v24}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v23

    cmp-long v21, v21, v23

    if-eqz v21, :cond_b

    .line 103
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "no_space_to_data"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 104
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 105
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 106
    new-instance v21, Ljava/io/File;

    const-string v22, "/system/etc/hosts"

    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v22, "127.0.0.1       localhost\n"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z

    .line 107
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 108
    const-string v21, "/system"

    const-string v22, "ro"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_2

    goto/16 :goto_3

    .line 137
    :catch_3
    move-exception v5

    .line 139
    .restart local v5       #e:Ljava/lang/Exception;
    :try_start_b
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 140
    new-instance v21, Ljava/io/File;

    const-string v22, "/system/etc/hosts"

    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v22, "127.0.0.1       localhost\n"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z

    .line 141
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "777"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 142
    new-instance v21, Ljava/io/File;

    const-string v22, "/system/etc/hosts"

    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v22, Ljava/io/File;

    move-object/from16 v0, v22

    invoke-direct {v0, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->save_text_to_end_file_from_file(Ljava/io/File;Ljava/io/File;)Z

    move-result v21

    if-nez v21, :cond_9

    .line 144
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "out.of.memory"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_b .. :try_end_b} :catch_2

    goto/16 :goto_7

    .line 111
    .end local v5           #e:Ljava/lang/Exception;
    :cond_b
    :try_start_c
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v21

    new-instance v23, Ljava/io/File;

    move-object/from16 v0, v23

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v23

    cmp-long v21, v21, v23

    if-eqz v21, :cond_c

    .line 112
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0777"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 113
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 114
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0777"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 115
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chattr"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "-ai"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 116
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chown"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0.0"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 117
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chown"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0:0"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 118
    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "ln"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "-s"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    const/16 v22, 0x3

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 119
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 127
    :goto_8
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 128
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 129
    const-string v21, "/system"

    const-string v22, "ro"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_6

    .line 121
    :cond_c
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 122
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chattr"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "-ai"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 123
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chown"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0.0"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 124
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chown"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0:0"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_c .. :try_end_c} :catch_2

    goto/16 :goto_8

    .line 151
    :cond_d
    :try_start_d
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "no_space_to_data"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_d .. :try_end_d} :catch_2

    goto/16 :goto_7

    .line 56
    .end local v10           #line:Ljava/lang/String;
    .end local v20           #write:Z
    :catch_4
    move-exception v6

    move-object/from16 v18, v19

    .end local v19           #wr:Ljava/io/RandomAccessFile;
    .restart local v18       #wr:Ljava/io/RandomAccessFile;
    goto/16 :goto_4
.end method
