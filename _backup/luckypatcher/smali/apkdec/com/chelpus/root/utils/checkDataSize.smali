.class public Lcom/chelpus/root/utils/checkDataSize;
.super Ljava/lang/Object;
.source "checkDataSize.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 9
    .parameter "paramArrayOfString"

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 13
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 14
    new-instance v0, Ljava/io/File;

    aget-object v4, p0, v8

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 15
    .local v0, dir:Ljava/io/File;
    new-instance v3, Lcom/chelpus/Utils;

    const-string v4, "calculSize"

    invoke-direct {v3, v4}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    .line 16
    .local v3, u:Lcom/chelpus/Utils;
    const/4 v2, 0x0

    .line 18
    .local v2, size:F
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {v3, v0, v4}, Lcom/chelpus/Utils;->sizeFolder(Ljava/io/File;Z)F
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 23
    :goto_0
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "%.3f"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 24
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 25
    return-void

    .line 19
    :catch_0
    move-exception v1

    .line 21
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
