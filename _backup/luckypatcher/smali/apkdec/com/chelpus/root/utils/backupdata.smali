.class public Lcom/chelpus/root/utils/backupdata;
.super Ljava/lang/Object;
.source "backupdata.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 25
    .parameter "paramArrayOfString"

    .prologue
    .line 17
    new-instance v21, Lcom/chelpus/root/utils/backupdata$1;

    invoke-direct/range {v21 .. v21}, Lcom/chelpus/root/utils/backupdata$1;-><init>()V

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 18
    const/16 v21, 0x0

    aget-object v15, p0, v21

    .line 19
    .local v15, pkg:Ljava/lang/String;
    const/16 v21, 0x1

    aget-object v5, p0, v21

    .line 20
    .local v5, datadir:Ljava/lang/String;
    const/16 v21, 0x2

    aget-object v2, p0, v21

    .line 21
    .local v2, backup_data_dir:Ljava/lang/String;
    const/16 v21, 0x3

    aget-object v16, p0, v21

    .line 22
    .local v16, sd_data_dir:Ljava/lang/String;
    const/4 v10, 0x0

    .line 23
    .local v10, error:Z
    new-instance v3, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/data.lpbkp"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 24
    .local v3, data:Ljava/io/File;
    new-instance v4, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/data.lpbkp.tmp"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 25
    .local v4, data2:Ljava/io/File;
    new-instance v7, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/dbdata.lpbkp"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 26
    .local v7, dbdata:Ljava/io/File;
    new-instance v8, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/dbdata.lpbkp.tmp"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 27
    .local v8, dbdata2:Ljava/io/File;
    new-instance v17, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/sddata.lpbkp"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 28
    .local v17, sddata:Ljava/io/File;
    new-instance v18, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/sddata.lpbkp.tmp"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 38
    .local v18, sddata2:Ljava/io/File;
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_0

    invoke-virtual {v3, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 39
    :cond_0
    new-instance v19, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v19

    invoke-direct {v0, v3}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 40
    .local v19, zipFile:Lnet/lingala/zip4j/core/ZipFile;
    new-instance v14, Lnet/lingala/zip4j/model/ZipParameters;

    invoke-direct {v14}, Lnet/lingala/zip4j/model/ZipParameters;-><init>()V

    .line 41
    .local v14, parameters:Lnet/lingala/zip4j/model/ZipParameters;
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Lnet/lingala/zip4j/model/ZipParameters;->setCompressionMethod(I)V

    .line 42
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Lnet/lingala/zip4j/model/ZipParameters;->setCompressionLevel(I)V

    .line 43
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 44
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 45
    .local v6, datadirs:Ljava/io/File;
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 46
    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v13

    .line 47
    .local v13, folders:[Ljava/io/File;
    if-eqz v13, :cond_5

    array-length v0, v13

    move/from16 v21, v0

    if-eqz v21, :cond_5

    .line 48
    array-length v0, v13

    move/from16 v22, v0

    const/16 v21, 0x0

    :goto_0
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_5

    aget-object v11, v13, v21

    .line 49
    .local v11, file:Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v23

    if-eqz v23, :cond_4

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v23

    const-string v24, "lib"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result v23

    if-nez v23, :cond_4

    .line 51
    :try_start_1
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 52
    move-object/from16 v0, v19

    invoke-virtual {v0, v11, v14}, Lnet/lingala/zip4j/core/ZipFile;->addFolder(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    :try_end_1
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 48
    :cond_1
    :goto_1
    add-int/lit8 v21, v21, 0x1

    goto :goto_0

    .line 53
    :catch_0
    move-exception v9

    .line 54
    .local v9, e:Lnet/lingala/zip4j/exception/ZipException;
    :try_start_2
    invoke-virtual {v9}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 55
    const/4 v10, 0x1

    .line 56
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v24, "error"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_1

    .line 111
    .end local v6           #datadirs:Ljava/io/File;
    .end local v9           #e:Lnet/lingala/zip4j/exception/ZipException;
    .end local v11           #file:Ljava/io/File;
    .end local v13           #folders:[Ljava/io/File;
    .end local v14           #parameters:Lnet/lingala/zip4j/model/ZipParameters;
    .end local v19           #zipFile:Lnet/lingala/zip4j/core/ZipFile;
    :catch_1
    move-exception v9

    .line 112
    .restart local v9       #e:Lnet/lingala/zip4j/exception/ZipException;
    :try_start_3
    invoke-virtual {v9}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 113
    const/4 v10, 0x1

    .line 114
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "error"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 123
    .end local v9           #e:Lnet/lingala/zip4j/exception/ZipException;
    :cond_2
    :goto_2
    if-nez v10, :cond_3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v21

    if-nez v21, :cond_3

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v21

    if-nez v21, :cond_3

    .line 124
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "empty data..."

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 126
    :cond_3
    if-nez v10, :cond_b

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v21

    if-nez v21, :cond_b

    .line 127
    invoke-virtual {v4, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 131
    :goto_3
    if-nez v10, :cond_c

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v21

    if-nez v21, :cond_c

    .line 132
    invoke-virtual {v8, v7}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 136
    :goto_4
    if-nez v10, :cond_d

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v21

    if-nez v21, :cond_d

    .line 137
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 141
    :goto_5
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 142
    return-void

    .line 58
    .restart local v6       #datadirs:Ljava/io/File;
    .restart local v11       #file:Ljava/io/File;
    .restart local v13       #folders:[Ljava/io/File;
    .restart local v14       #parameters:Lnet/lingala/zip4j/model/ZipParameters;
    .restart local v19       #zipFile:Lnet/lingala/zip4j/core/ZipFile;
    :cond_4
    :try_start_4
    invoke-virtual {v11}, Ljava/io/File;->isFile()Z
    :try_end_4
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    move-result v23

    if-eqz v23, :cond_1

    :try_start_5
    move-object/from16 v0, v19

    invoke-virtual {v0, v11, v14}, Lnet/lingala/zip4j/core/ZipFile;->addFile(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    :try_end_5
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    :catch_2
    move-exception v9

    .line 59
    .restart local v9       #e:Lnet/lingala/zip4j/exception/ZipException;
    :try_start_6
    invoke-virtual {v9}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 60
    const/4 v10, 0x1

    .line 61
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v24, "error"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_6
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_1

    .line 117
    .end local v6           #datadirs:Ljava/io/File;
    .end local v9           #e:Lnet/lingala/zip4j/exception/ZipException;
    .end local v11           #file:Ljava/io/File;
    .end local v13           #folders:[Ljava/io/File;
    .end local v14           #parameters:Lnet/lingala/zip4j/model/ZipParameters;
    .end local v19           #zipFile:Lnet/lingala/zip4j/core/ZipFile;
    :catch_3
    move-exception v9

    .line 118
    .local v9, e:Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 119
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Exception e"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 120
    const/4 v10, 0x1

    .line 121
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "error"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_2

    .line 65
    .end local v9           #e:Ljava/lang/Exception;
    .restart local v6       #datadirs:Ljava/io/File;
    .restart local v13       #folders:[Ljava/io/File;
    .restart local v14       #parameters:Lnet/lingala/zip4j/model/ZipParameters;
    .restart local v19       #zipFile:Lnet/lingala/zip4j/core/ZipFile;
    :cond_5
    :try_start_7
    new-instance v21, Ljava/io/File;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "/dbdata/databases/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_8

    .line 67
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_6

    invoke-virtual {v7, v8}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 68
    :cond_6
    new-instance v20, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v20

    invoke-direct {v0, v7}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 69
    .local v20, zipFile2:Lnet/lingala/zip4j/core/ZipFile;
    new-instance v21, Ljava/io/File;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "/dbdata/databases/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v12

    .line 70
    .local v12, files:[Ljava/io/File;
    if-eqz v13, :cond_8

    array-length v0, v13

    move/from16 v21, v0

    if-eqz v21, :cond_8

    .line 71
    array-length v0, v12

    move/from16 v22, v0

    const/16 v21, 0x0

    :goto_6
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_8

    aget-object v11, v12, v21

    .line 72
    .restart local v11       #file:Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z
    :try_end_7
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    move-result v23

    if-eqz v23, :cond_7

    .line 74
    :try_start_8
    move-object/from16 v0, v20

    invoke-virtual {v0, v11, v14}, Lnet/lingala/zip4j/core/ZipFile;->addFolder(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    :try_end_8
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    .line 71
    :goto_7
    add-int/lit8 v21, v21, 0x1

    goto :goto_6

    .line 75
    :catch_4
    move-exception v9

    .line 76
    .local v9, e:Lnet/lingala/zip4j/exception/ZipException;
    :try_start_9
    invoke-virtual {v9}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 77
    const/4 v10, 0x1

    .line 78
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v24, "error"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_9
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_7

    .line 80
    .end local v9           #e:Lnet/lingala/zip4j/exception/ZipException;
    :cond_7
    :try_start_a
    move-object/from16 v0, v20

    invoke-virtual {v0, v11, v14}, Lnet/lingala/zip4j/core/ZipFile;->addFile(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    :try_end_a
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    goto :goto_7

    :catch_5
    move-exception v9

    .line 81
    .restart local v9       #e:Lnet/lingala/zip4j/exception/ZipException;
    :try_start_b
    invoke-virtual {v9}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 82
    const/4 v10, 0x1

    .line 83
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v24, "error"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_7

    .line 88
    .end local v9           #e:Lnet/lingala/zip4j/exception/ZipException;
    .end local v11           #file:Ljava/io/File;
    .end local v12           #files:[Ljava/io/File;
    .end local v20           #zipFile2:Lnet/lingala/zip4j/core/ZipFile;
    :cond_8
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_2

    .line 90
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_9

    invoke-virtual/range {v17 .. v18}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 91
    :cond_9
    new-instance v20, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 92
    .restart local v20       #zipFile2:Lnet/lingala/zip4j/core/ZipFile;
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v12

    .line 93
    .restart local v12       #files:[Ljava/io/File;
    if-eqz v13, :cond_2

    array-length v0, v13

    move/from16 v21, v0

    if-eqz v21, :cond_2

    .line 94
    array-length v0, v12

    move/from16 v22, v0

    const/16 v21, 0x0

    :goto_8
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_2

    aget-object v11, v12, v21

    .line 95
    .restart local v11       #file:Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z
    :try_end_b
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3

    move-result v23

    if-eqz v23, :cond_a

    .line 97
    :try_start_c
    move-object/from16 v0, v20

    invoke-virtual {v0, v11, v14}, Lnet/lingala/zip4j/core/ZipFile;->addFolder(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    :try_end_c
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_c .. :try_end_c} :catch_6
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3

    .line 94
    :goto_9
    add-int/lit8 v21, v21, 0x1

    goto :goto_8

    .line 98
    :catch_6
    move-exception v9

    .line 99
    .restart local v9       #e:Lnet/lingala/zip4j/exception/ZipException;
    :try_start_d
    invoke-virtual {v9}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 100
    const/4 v10, 0x1

    .line 101
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v24, "error"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_d
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3

    goto :goto_9

    .line 103
    .end local v9           #e:Lnet/lingala/zip4j/exception/ZipException;
    :cond_a
    :try_start_e
    move-object/from16 v0, v20

    invoke-virtual {v0, v11, v14}, Lnet/lingala/zip4j/core/ZipFile;->addFile(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    :try_end_e
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_e .. :try_end_e} :catch_7
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3

    goto :goto_9

    :catch_7
    move-exception v9

    .line 104
    .restart local v9       #e:Lnet/lingala/zip4j/exception/ZipException;
    :try_start_f
    invoke-virtual {v9}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 105
    const/4 v10, 0x1

    .line 106
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v24, "error"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_f
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_f .. :try_end_f} :catch_1
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_3

    goto :goto_9

    .line 129
    .end local v6           #datadirs:Ljava/io/File;
    .end local v9           #e:Lnet/lingala/zip4j/exception/ZipException;
    .end local v11           #file:Ljava/io/File;
    .end local v12           #files:[Ljava/io/File;
    .end local v13           #folders:[Ljava/io/File;
    .end local v14           #parameters:Lnet/lingala/zip4j/model/ZipParameters;
    .end local v19           #zipFile:Lnet/lingala/zip4j/core/ZipFile;
    .end local v20           #zipFile2:Lnet/lingala/zip4j/core/ZipFile;
    :cond_b
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto/16 :goto_3

    .line 134
    :cond_c
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    goto/16 :goto_4

    .line 139
    :cond_d
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->delete()Z

    goto/16 :goto_5
.end method
