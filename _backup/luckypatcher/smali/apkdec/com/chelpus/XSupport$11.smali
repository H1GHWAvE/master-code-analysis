.class Lcom/chelpus/XSupport$11;
.super Lde/robv/android/xposed/XC_MethodHook;
.source "XSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/chelpus/XSupport;->handleLoadPackage(Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/chelpus/XSupport;


# direct methods
.method constructor <init>(Lcom/chelpus/XSupport;)V
    .locals 0
    .parameter "this$0"

    .prologue
    .line 351
    iput-object p1, p0, Lcom/chelpus/XSupport$11;->this$0:Lcom/chelpus/XSupport;

    invoke-direct {p0}, Lde/robv/android/xposed/XC_MethodHook;-><init>()V

    return-void
.end method


# virtual methods
.method protected beforeHookedMethod(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)V
    .locals 4
    .parameter "param"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 355
    iget-object v2, p0, Lcom/chelpus/XSupport$11;->this$0:Lcom/chelpus/XSupport;

    invoke-virtual {v2}, Lcom/chelpus/XSupport;->loadPrefs()V

    .line 356
    sget-boolean v2, Lcom/chelpus/XSupport;->enable:Z

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/chelpus/XSupport;->patch3:Z

    if-eqz v2, :cond_0

    .line 358
    sget-boolean v2, Lcom/chelpus/Common;->JB_MR1_NEWER:Z

    if-eqz v2, :cond_1

    const/4 v1, 0x2

    .line 359
    .local v1, id:I
    :goto_0
    iget-object v2, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->args:[Ljava/lang/Object;

    aget-object v2, v2, v1

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 361
    .local v0, flags:I
    and-int/lit16 v2, v0, 0x80

    if-nez v2, :cond_0

    .line 362
    or-int/lit16 v0, v0, 0x80

    .line 363
    iget-object v2, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->args:[Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    .line 367
    .end local v0           #flags:I
    .end local v1           #id:I
    :cond_0
    return-void

    .line 358
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method
