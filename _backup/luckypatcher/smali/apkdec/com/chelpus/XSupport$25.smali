.class Lcom/chelpus/XSupport$25;
.super Lde/robv/android/xposed/XC_MethodHook;
.source "XSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/chelpus/XSupport;->handleLoadPackage(Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/chelpus/XSupport;


# direct methods
.method constructor <init>(Lcom/chelpus/XSupport;)V
    .locals 0
    .parameter "this$0"

    .prologue
    .line 786
    iput-object p1, p0, Lcom/chelpus/XSupport$25;->this$0:Lcom/chelpus/XSupport;

    invoke-direct {p0}, Lde/robv/android/xposed/XC_MethodHook;-><init>()V

    return-void
.end method


# virtual methods
.method protected beforeHookedMethod(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)V
    .locals 3
    .parameter "param"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 791
    iget-object v1, p0, Lcom/chelpus/XSupport$25;->this$0:Lcom/chelpus/XSupport;

    invoke-virtual {v1}, Lcom/chelpus/XSupport;->loadPrefs()V

    .line 792
    sget-boolean v1, Lcom/chelpus/XSupport;->enable:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/chelpus/XSupport;->hide:Z

    if-eqz v1, :cond_0

    .line 793
    iget-object v1, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->args:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v1, v2

    check-cast v0, Ljava/lang/String;

    .line 794
    .local v0, pkg:Ljava/lang/String;
    if-eqz v0, :cond_0

    const-class v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 795
    iget-object v1, p0, Lcom/chelpus/XSupport$25;->this$0:Lcom/chelpus/XSupport;

    invoke-virtual {v1, p1}, Lcom/chelpus/XSupport;->checkForHide(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 796
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->setResult(Ljava/lang/Object;)V

    .line 802
    .end local v0           #pkg:Ljava/lang/String;
    :cond_0
    return-void
.end method
