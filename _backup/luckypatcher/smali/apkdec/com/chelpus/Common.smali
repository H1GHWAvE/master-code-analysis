.class public Lcom/chelpus/Common;
.super Ljava/lang/Object;
.source "Common.java"


# static fields
.field public static final ACTION_BACKUP_APK_FILE:Ljava/lang/String; = "xinstaller.intent.action.BACKUP_APK_FILE"

.field public static final ACTION_BACKUP_PREFERENCES:Ljava/lang/String; = "xinstaller.intent.action.BACKUP_PREFERENCES"

.field public static final ACTION_DELETE_APK_FILE:Ljava/lang/String; = "xinstaller.intent.action.DELETE_APK_FILE"

.field public static final ACTION_RESET_PREFERENCES:Ljava/lang/String; = "xinstaller.intent.action.RESET_PREFERENCES"

.field public static final ACTION_RESTORE_PREFERENCES:Ljava/lang/String; = "xinstaller.intent.action.RESTORE_PREFERENCES"

.field public static final ACTION_UNINSTALL_SYSTEM_APP:Ljava/lang/String; = "xinstaller.intent.action.UNINSTALL_SYSTEM_APP"

.field public static final ANDROID_PKG:Ljava/lang/String; = "android"

.field public static final BACKUPCONFIRM_PKG:Ljava/lang/String; = "com.android.backupconfirm"

.field public static final BACKUPRESTORECONFIRMATION:Ljava/lang/String; = "com.android.backupconfirm.BackupRestoreConfirmation"

.field public static final CANBEONSDCARDCHECKER:Ljava/lang/String; = "com.android.settings.applications.CanBeOnSdCardChecker"

.field public static final DEBUG_ENABLE_DEBUGGER:I = 0x1

.field public static final DELETE_KEEP_DATA:I = 0x1

.field public static final DEVICEPOLICYMANAGERSERVICE:Ljava/lang/String; = null

.field public static final FDROIDAPPDETAILS:Ljava/lang/String; = "org.fdroid.fdroid.AppDetails"

.field public static final FDROID_PKG:Ljava/lang/String; = "org.fdroid.fdroid"

.field public static final FILE:Ljava/lang/String; = "file"

.field public static final GOOGLEPLAY_PKG:Ljava/lang/String; = "com.android.vending"

.field public static final INSTALLAPPPROGRESS:Ljava/lang/String; = "com.android.packageinstaller.InstallAppProgress"

.field public static final INSTALLEDAPPDETAILS:Ljava/lang/String; = "com.android.settings.applications.InstalledAppDetails"

.field public static final INSTALL_ALLOW_DOWNGRADE:I = 0x80

.field public static final INSTALL_EXTERNAL:I = 0x8

.field public static final INSTALL_FORWARD_LOCK:I = 0x1

.field public static final INSTALL_SUCCEEDED:I = 0x1

.field public static final InAppServ:I = 0x0

.field public static final JARVERIFIER:Ljava/lang/String; = "java.util.jar.JarVerifier$VerifierEntry"

#the value of this static final field might be set in the static constructor
.field public static final JB_MR1_NEWER:Z = false

#the value of this static final field might be set in the static constructor
.field public static final JB_MR2_NEWER:Z = false

#the value of this static final field might be set in the static constructor
.field public static final JB_NEWER:Z = false

#the value of this static final field might be set in the static constructor
.field public static final KITKAT_NEWER:Z = false

.field public static final LATEST_ANDROID_RELEASE:I = 0x15

#the value of this static final field might be set in the static constructor
.field public static final LOLLIPOP_NEWER:Z = false

.field public static final LicServ:I = 0x1

.field public static final PACKAGE:Ljava/lang/String; = "package"

.field public static final PACKAGEINSTALLERACTIVITY:Ljava/lang/String; = "com.android.packageinstaller.PackageInstallerActivity"

.field public static final PACKAGEINSTALLER_PKG:Ljava/lang/String; = "com.android.packageinstaller"

.field public static final PACKAGEMANAGERREPOSITORY:Ljava/lang/String; = "com.google.android.finsky.appstate.PackageManagerRepository"

.field public static final PACKAGEMANAGERSERVICE:Ljava/lang/String; = "com.android.server.pm.PackageManagerService"

.field public static final PACKAGEPARSER:Ljava/lang/String; = "android.content.pm.PackageParser"

.field public static final PACKAGE_NAME:Ljava/lang/String; = null

.field public static final PACKAGE_PREFERENCES:Ljava/lang/String; = null

.field public static final PACKAGE_TAG:Ljava/lang/String; = "XInstaller"

.field public static final PMcontext:I = 0x2

.field public static final PREFERENCE:Ljava/lang/String; = "preference"

.field public static final PREF_DISABLE_CHECK_DUPLICATED_PERMISSION:Ljava/lang/String; = "disable_check_duplicated_permissions"

.field public static final PREF_DISABLE_CHECK_PERMISSION:Ljava/lang/String; = "disable_check_permissions"

.field public static final PREF_DISABLE_CHECK_SDK_VERSION:Ljava/lang/String; = "disable_check_sdk_version"

.field public static final PREF_DISABLE_CHECK_SIGNATURE:Ljava/lang/String; = "disable_check_signatures"

.field public static final PREF_DISABLE_CHECK_SIGNATURE_FDROID:Ljava/lang/String; = "disable_check_signatures_fdroid"

.field public static final PREF_DISABLE_FORWARD_LOCK:Ljava/lang/String; = "disable_forward_lock"

.field public static final PREF_DISABLE_INSTALL_BACKGROUND:Ljava/lang/String; = "disable_install_background"

.field public static final PREF_DISABLE_UNINSTALL_BACKGROUND:Ljava/lang/String; = "disable_uninstall_background"

.field public static final PREF_DISABLE_VERIFY_APP:Ljava/lang/String; = "disable_verify_apps"

.field public static final PREF_DISABLE_VERIFY_JAR:Ljava/lang/String; = "disable_verify_jar"

.field public static final PREF_DISABLE_VERIFY_SIGNATURE:Ljava/lang/String; = "disable_verify_signatures"

.field public static final PREF_ENABLED_DOWNGRADE_APP:Ljava/lang/String; = "enable_downgrade_apps"

.field public static final PREF_ENABLE_AUTO_BACKUP:Ljava/lang/String; = "enable_auto_backup"

.field public static final PREF_ENABLE_AUTO_CLOSE_INSTALL:Ljava/lang/String; = "enable_auto_close_install"

.field public static final PREF_ENABLE_AUTO_CLOSE_UNINSTALL:Ljava/lang/String; = "enable_auto_close_uninstall"

.field public static final PREF_ENABLE_AUTO_ENABLE_CLEAR_BUTTON:Ljava/lang/String; = "enable_auto_enable_clear_buttons"

.field public static final PREF_ENABLE_AUTO_HIDE_INSTALL:Ljava/lang/String; = "enable_auto_hide_install"

.field public static final PREF_ENABLE_AUTO_INSTALL:Ljava/lang/String; = "enable_auto_install"

.field public static final PREF_ENABLE_AUTO_UNINSTALL:Ljava/lang/String; = "enable_auto_uninstall"

.field public static final PREF_ENABLE_BACKUP_APK_FILE:Ljava/lang/String; = "enable_backup_apk_files"

.field public static final PREF_ENABLE_BACKUP_APP_PACKAGE:Ljava/lang/String; = "enable_backup_app_packages"

.field public static final PREF_ENABLE_DEBUG_APP:Ljava/lang/String; = "enable_debug_apps"

.field public static final PREF_ENABLE_DELETE_APK_FILE_INSTALL:Ljava/lang/String; = "enable_delete_apk_files_install"

.field public static final PREF_ENABLE_DISABLE_SYSTEM_APP:Ljava/lang/String; = "enable_disable_system_apps"

.field public static final PREF_ENABLE_EXPERT_MODE:Ljava/lang/String; = "enable_expert_mode"

.field public static final PREF_ENABLE_EXPORT_APP:Ljava/lang/String; = "enable_export_apps"

.field public static final PREF_ENABLE_INSTALL_EXTERNAL_STORAGE:Ljava/lang/String; = "enable_install_external_storage"

.field public static final PREF_ENABLE_INSTALL_UNKNOWN_APP:Ljava/lang/String; = "enable_install_unknown_apps"

.field public static final PREF_ENABLE_INSTALL_UNSIGNED_APP:Ljava/lang/String; = "enable_install_unsigned_apps"

.field public static final PREF_ENABLE_KEEP_APP_DATA:Ljava/lang/String; = "enable_keep_apps_data"

.field public static final PREF_ENABLE_LAUNCH_APP:Ljava/lang/String; = "enable_launch_apps"

.field public static final PREF_ENABLE_LAUNCH_INSTALL:Ljava/lang/String; = "enable_auto_launch_install"

.field public static final PREF_ENABLE_MODULE:Ljava/lang/String; = "enable_module"

.field public static final PREF_ENABLE_MOVE_APP:Ljava/lang/String; = "enable_move_apps"

.field public static final PREF_ENABLE_OPEN_APP_GOOGLE_PLAY:Ljava/lang/String; = "enable_open_apps_google_play"

.field public static final PREF_ENABLE_SHOW_BUTTON:Ljava/lang/String; = "enable_show_buttons"

.field public static final PREF_ENABLE_SHOW_PACKAGE_NAME:Ljava/lang/String; = "enable_show_package_name"

.field public static final PREF_ENABLE_SHOW_VERSION:Ljava/lang/String; = "enable_show_version"

.field public static final PREF_ENABLE_UNINSTALL_DEVICE_ADMIN:Ljava/lang/String; = "enable_uninstall_device_admins"

.field public static final PREF_ENABLE_UNINSTALL_SYSTEM_APP:Ljava/lang/String; = "enable_uninstall_system_apps"

.field public static final ROOT_UID:I = 0x0

#the value of this static final field might be set in the static constructor
.field public static final SDK:I = 0x0

.field public static final SETTINGS_PKG:Ljava/lang/String; = "com.android.settings"

.field public static final SIGNATURE:Ljava/lang/String; = "java.security.Signature"

.field public static final UNINSTALLAPPPROGRESS:Ljava/lang/String; = "com.android.packageinstaller.UninstallAppProgress"

.field public static final UNINSTALLERACTIVITY:Ljava/lang/String; = "com.android.packageinstaller.UninstallerActivity"

.field public static final UTILS:Ljava/lang/String; = "com.android.settings.Utils"

.field public static final VALUE:Ljava/lang/String; = "value"

.field public static final getPackageManagerF:I = 0x1

.field public static final mBase:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 67
    const-class v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/chelpus/Common;->PACKAGE_NAME:Ljava/lang/String;

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/chelpus/Common;->PACKAGE_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_preferences"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/chelpus/Common;->PACKAGE_PREFERENCES:Ljava/lang/String;

    .line 80
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/chelpus/Common;->SDK:I

    .line 90
    sget v0, Lcom/chelpus/Common;->SDK:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/chelpus/Common;->JB_NEWER:Z

    .line 92
    sget v0, Lcom/chelpus/Common;->SDK:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_1

    move v0, v1

    :goto_1
    sput-boolean v0, Lcom/chelpus/Common;->JB_MR1_NEWER:Z

    .line 94
    sget v0, Lcom/chelpus/Common;->SDK:I

    const/16 v3, 0x12

    if-lt v0, v3, :cond_2

    move v0, v1

    :goto_2
    sput-boolean v0, Lcom/chelpus/Common;->JB_MR2_NEWER:Z

    .line 96
    sget v0, Lcom/chelpus/Common;->SDK:I

    const/16 v3, 0x13

    if-lt v0, v3, :cond_3

    move v0, v1

    :goto_3
    sput-boolean v0, Lcom/chelpus/Common;->KITKAT_NEWER:Z

    .line 98
    sget v0, Lcom/chelpus/Common;->SDK:I

    const/16 v3, 0x15

    if-lt v0, v3, :cond_4

    :goto_4
    sput-boolean v1, Lcom/chelpus/Common;->LOLLIPOP_NEWER:Z

    .line 103
    sget-boolean v0, Lcom/chelpus/Common;->LOLLIPOP_NEWER:Z

    if-eqz v0, :cond_5

    const-string v0, "com.android.server.devicepolicy.DevicePolicyManagerService"

    :goto_5
    sput-object v0, Lcom/chelpus/Common;->DEVICEPOLICYMANAGERSERVICE:Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    .line 90
    goto :goto_0

    :cond_1
    move v0, v2

    .line 92
    goto :goto_1

    :cond_2
    move v0, v2

    .line 94
    goto :goto_2

    :cond_3
    move v0, v2

    .line 96
    goto :goto_3

    :cond_4
    move v1, v2

    .line 98
    goto :goto_4

    .line 103
    :cond_5
    const-string v0, "com.android.server.DevicePolicyManagerService"

    goto :goto_5
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
