.class Lcom/mba/proxylight/RequestProcessor$1;
.super Ljava/lang/Object;
.source "RequestProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mba/proxylight/RequestProcessor;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mba/proxylight/RequestProcessor;


# direct methods
.method constructor <init>(Lcom/mba/proxylight/RequestProcessor;)V
    .locals 0
    .parameter "this$0"

    .prologue
    .line 49
    iput-object p1, p0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 39

    .prologue
    .line 51
    invoke-static {}, Lcom/mba/proxylight/RequestProcessor;->access$008()I

    .line 54
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    monitor-enter v36
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    const/16 v37, 0x1

    move-object/from16 v0, v35

    move/from16 v1, v37

    #setter for: Lcom/mba/proxylight/RequestProcessor;->alive:Z
    invoke-static {v0, v1}, Lcom/mba/proxylight/RequestProcessor;->access$102(Lcom/mba/proxylight/RequestProcessor;Z)Z

    .line 56
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->selector:Ljava/nio/channels/Selector;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$200(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/channels/Selector;

    move-result-object v35

    if-nez v35, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->shutdown:Z
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$300(Lcom/mba/proxylight/RequestProcessor;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v35

    if-nez v35, :cond_0

    .line 60
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    const-wide/16 v37, 0x4e20

    move-object/from16 v0, v35

    move-wide/from16 v1, v37

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 66
    :cond_0
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->shutdown:Z
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$300(Lcom/mba/proxylight/RequestProcessor;)Z

    move-result v35

    if-eqz v35, :cond_1

    .line 67
    monitor-exit v36
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #calls: Lcom/mba/proxylight/RequestProcessor;->closeAll()V
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$800(Lcom/mba/proxylight/RequestProcessor;)V

    .line 262
    invoke-static {}, Lcom/mba/proxylight/RequestProcessor;->access$010()I

    .line 263
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "Fin du processor "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v37, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->processorIdx:I
    invoke-static/range {v37 .. v37}, Lcom/mba/proxylight/RequestProcessor;->access$700(Lcom/mba/proxylight/RequestProcessor;)I

    move-result v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    const/16 v37, 0x0

    invoke-virtual/range {v35 .. v37}, Lcom/mba/proxylight/RequestProcessor;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 67
    :goto_1
    return-void

    .line 61
    :catch_0
    move-exception v7

    .line 62
    .local v7, e:Ljava/lang/InterruptedException;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    const/16 v37, 0x0

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1, v7}, Lcom/mba/proxylight/RequestProcessor;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 63
    monitor-exit v36
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #calls: Lcom/mba/proxylight/RequestProcessor;->closeAll()V
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$800(Lcom/mba/proxylight/RequestProcessor;)V

    .line 262
    invoke-static {}, Lcom/mba/proxylight/RequestProcessor;->access$010()I

    .line 263
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "Fin du processor "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v37, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->processorIdx:I
    invoke-static/range {v37 .. v37}, Lcom/mba/proxylight/RequestProcessor;->access$700(Lcom/mba/proxylight/RequestProcessor;)I

    move-result v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    const/16 v37, 0x0

    invoke-virtual/range {v35 .. v37}, Lcom/mba/proxylight/RequestProcessor;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 69
    .end local v7           #e:Ljava/lang/InterruptedException;
    :cond_1
    :try_start_5
    monitor-exit v36
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 72
    const/16 v25, 0x0

    .line 73
    .local v25, request:Lcom/mba/proxylight/Request;
    const/4 v6, 0x0

    .line 74
    .local v6, contentLength:I
    :try_start_6
    sget-object v33, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->STATUS_LINE:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    .line 75
    .local v33, step:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->selector:Ljava/nio/channels/Selector;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$200(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/channels/Selector;

    move-result-object v35

    if-eqz v35, :cond_3

    .line 76
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->selector:Ljava/nio/channels/Selector;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$200(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/channels/Selector;

    move-result-object v35

    const-wide/16 v36, 0x1388

    invoke-virtual/range {v35 .. v37}, Ljava/nio/channels/Selector;->select(J)I

    .line 77
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$400(Lcom/mba/proxylight/RequestProcessor;)Lcom/mba/proxylight/Socket;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    move-result-object v35

    if-nez v35, :cond_4

    .line 252
    :cond_3
    :try_start_7
    sget-object v35, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "inData:"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v37, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->inData:Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Lcom/mba/proxylight/RequestProcessor;->access$1200(Lcom/mba/proxylight/RequestProcessor;)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    const-string v36, ""

    #setter for: Lcom/mba/proxylight/RequestProcessor;->inData:Ljava/lang/String;
    invoke-static/range {v35 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$1202(Lcom/mba/proxylight/RequestProcessor;Ljava/lang/String;)Ljava/lang/String;

    .line 254
    sget-object v35, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "outData:"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v37, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->outData:Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Lcom/mba/proxylight/RequestProcessor;->access$1900(Lcom/mba/proxylight/RequestProcessor;)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    const-string v36, ""

    #setter for: Lcom/mba/proxylight/RequestProcessor;->outData:Ljava/lang/String;
    invoke-static/range {v35 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$1902(Lcom/mba/proxylight/RequestProcessor;Ljava/lang/String;)Ljava/lang/String;

    .line 256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #calls: Lcom/mba/proxylight/RequestProcessor;->closeAll()V
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$800(Lcom/mba/proxylight/RequestProcessor;)V

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->recycle()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 261
    .end local v6           #contentLength:I
    .end local v25           #request:Lcom/mba/proxylight/Request;
    .end local v33           #step:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
    :catchall_0
    move-exception v35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    #calls: Lcom/mba/proxylight/RequestProcessor;->closeAll()V
    invoke-static/range {v36 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$800(Lcom/mba/proxylight/RequestProcessor;)V

    .line 262
    invoke-static {}, Lcom/mba/proxylight/RequestProcessor;->access$010()I

    .line 263
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "Fin du processor "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v38, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->processorIdx:I
    invoke-static/range {v38 .. v38}, Lcom/mba/proxylight/RequestProcessor;->access$700(Lcom/mba/proxylight/RequestProcessor;)I

    move-result v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    const/16 v38, 0x0

    invoke-virtual/range {v36 .. v38}, Lcom/mba/proxylight/RequestProcessor;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v35

    .line 69
    :catchall_1
    move-exception v35

    :try_start_8
    monitor-exit v36
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v35
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 80
    .restart local v6       #contentLength:I
    .restart local v25       #request:Lcom/mba/proxylight/Request;
    .restart local v33       #step:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
    :cond_4
    :try_start_a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 81
    .local v20, now:J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->selector:Ljava/nio/channels/Selector;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$200(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/channels/Selector;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v35

    invoke-interface/range {v35 .. v35}, Ljava/util/Set;->size()I

    move-result v35

    if-nez v35, :cond_8

    .line 82
    invoke-static {}, Lcom/mba/proxylight/RequestProcessor;->access$500()J

    move-result-wide v35

    sub-long v18, v20, v35

    .line 84
    .local v18, limit:J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->outSockets:Ljava/util/Map;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$600(Lcom/mba/proxylight/RequestProcessor;)Ljava/util/Map;

    move-result-object v35

    invoke-interface/range {v35 .. v35}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v35

    invoke-interface/range {v35 .. v35}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;>;"
    :cond_5
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v35

    if-eqz v35, :cond_6

    .line 85
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    .line 86
    .local v8, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;"
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/mba/proxylight/Socket;

    .line 87
    .local v31, so:Lcom/mba/proxylight/Socket;
    move-object/from16 v0, v31

    iget-wide v0, v0, Lcom/mba/proxylight/Socket;->lastRead:J

    move-wide/from16 v35, v0

    move-object/from16 v0, v31

    iget-wide v0, v0, Lcom/mba/proxylight/Socket;->lastWrite:J

    move-wide/from16 v37, v0

    invoke-static/range {v35 .. v38}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v16

    .line 88
    .local v16, lastOp:J
    cmp-long v35, v16, v18

    if-gez v35, :cond_5

    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "processeur "

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v37, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->processorIdx:I
    invoke-static/range {v37 .. v37}, Lcom/mba/proxylight/RequestProcessor;->access$700(Lcom/mba/proxylight/RequestProcessor;)I

    move-result v37

    move-object/from16 v0, v35

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, " : Fermeture pour inactivite de la socket vers "

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    move-object/from16 v0, v37

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/mba/proxylight/RequestProcessor;->debug(Ljava/lang/String;)V

    .line 90
    if-eqz v25, :cond_a

    const-string v35, "CONNECT"

    invoke-virtual/range {v25 .. v25}, Lcom/mba/proxylight/Request;->getMethod()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_a

    .line 92
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #calls: Lcom/mba/proxylight/RequestProcessor;->closeAll()V
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$800(Lcom/mba/proxylight/RequestProcessor;)V

    .line 108
    .end local v8           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;"
    .end local v16           #lastOp:J
    .end local v31           #so:Lcom/mba/proxylight/Socket;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->outSockets:Ljava/util/Map;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$600(Lcom/mba/proxylight/RequestProcessor;)Ljava/util/Map;

    move-result-object v35

    invoke-interface/range {v35 .. v35}, Ljava/util/Map;->size()I

    move-result v35

    if-nez v35, :cond_7

    .line 110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #calls: Lcom/mba/proxylight/RequestProcessor;->closeAll()V
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$800(Lcom/mba/proxylight/RequestProcessor;)V

    .line 112
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$400(Lcom/mba/proxylight/RequestProcessor;)Lcom/mba/proxylight/Socket;

    move-result-object v35

    if-eqz v35, :cond_3

    .line 116
    .end local v11           #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;>;"
    .end local v18           #limit:J
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->selector:Ljava/nio/channels/Selector;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$200(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/channels/Selector;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v35

    invoke-interface/range {v35 .. v35}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .line 117
    .local v28, selectedKeys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    :cond_9
    :goto_4
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v35

    if-eqz v35, :cond_2

    .line 118
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/nio/channels/SelectionKey;

    .line 119
    .local v15, key:Ljava/nio/channels/SelectionKey;
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->remove()V

    .line 120
    invoke-virtual {v15}, Ljava/nio/channels/SelectionKey;->isValid()Z

    move-result v35

    if-eqz v35, :cond_9

    .line 121
    invoke-virtual {v15}, Ljava/nio/channels/SelectionKey;->isReadable()Z

    move-result v35

    if-eqz v35, :cond_9

    .line 122
    invoke-virtual {v15}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Lcom/mba/proxylight/Socket;

    .line 123
    .local v32, socket:Lcom/mba/proxylight/Socket;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$400(Lcom/mba/proxylight/RequestProcessor;)Lcom/mba/proxylight/Socket;

    move-result-object v35

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    if-ne v0, v1, :cond_1e

    .line 124
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$1000(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/ByteBuffer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;
    invoke-static/range {v36 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$400(Lcom/mba/proxylight/RequestProcessor;)Lcom/mba/proxylight/Socket;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v37, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;
    invoke-static/range {v37 .. v37}, Lcom/mba/proxylight/RequestProcessor;->access$1000(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/ByteBuffer;

    move-result-object v37

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-object/from16 v2, v37

    move-wide/from16 v3, v20

    #calls: Lcom/mba/proxylight/RequestProcessor;->read(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)I
    invoke-static {v0, v1, v2, v3, v4}, Lcom/mba/proxylight/RequestProcessor;->access$1100(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)I

    move-result v22

    .line 126
    .local v22, numRead:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v37, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->inData:Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Lcom/mba/proxylight/RequestProcessor;->access$1200(Lcom/mba/proxylight/RequestProcessor;)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    new-instance v37, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v38, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;
    invoke-static/range {v38 .. v38}, Lcom/mba/proxylight/RequestProcessor;->access$1000(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/ByteBuffer;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v38

    invoke-direct/range {v37 .. v38}, Ljava/lang/String;-><init>([B)V

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    #setter for: Lcom/mba/proxylight/RequestProcessor;->inData:Ljava/lang/String;
    invoke-static/range {v35 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$1202(Lcom/mba/proxylight/RequestProcessor;Ljava/lang/String;)Ljava/lang/String;

    .line 127
    const/16 v35, -0x1

    move/from16 v0, v22

    move/from16 v1, v35

    if-ne v0, v1, :cond_b

    .line 129
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #calls: Lcom/mba/proxylight/RequestProcessor;->closeAll()V
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$800(Lcom/mba/proxylight/RequestProcessor;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    goto/16 :goto_2

    .line 249
    .end local v15           #key:Ljava/nio/channels/SelectionKey;
    .end local v20           #now:J
    .end local v22           #numRead:I
    .end local v28           #selectedKeys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .end local v32           #socket:Lcom/mba/proxylight/Socket;
    .end local v33           #step:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
    :catch_1
    move-exception v7

    .line 250
    .local v7, e:Ljava/lang/Exception;
    :goto_5
    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    const/16 v36, 0x0

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-virtual {v0, v1, v7}, Lcom/mba/proxylight/RequestProcessor;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 252
    :try_start_c
    sget-object v35, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "inData:"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v37, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->inData:Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Lcom/mba/proxylight/RequestProcessor;->access$1200(Lcom/mba/proxylight/RequestProcessor;)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    const-string v36, ""

    #setter for: Lcom/mba/proxylight/RequestProcessor;->inData:Ljava/lang/String;
    invoke-static/range {v35 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$1202(Lcom/mba/proxylight/RequestProcessor;Ljava/lang/String;)Ljava/lang/String;

    .line 254
    sget-object v35, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "outData:"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v37, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->outData:Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Lcom/mba/proxylight/RequestProcessor;->access$1900(Lcom/mba/proxylight/RequestProcessor;)Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    const-string v36, ""

    #setter for: Lcom/mba/proxylight/RequestProcessor;->outData:Ljava/lang/String;
    invoke-static/range {v35 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$1902(Lcom/mba/proxylight/RequestProcessor;Ljava/lang/String;)Ljava/lang/String;

    .line 256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #calls: Lcom/mba/proxylight/RequestProcessor;->closeAll()V
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$800(Lcom/mba/proxylight/RequestProcessor;)V

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->recycle()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    .line 96
    .end local v7           #e:Ljava/lang/Exception;
    .restart local v8       #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;"
    .restart local v11       #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;>;"
    .restart local v16       #lastOp:J
    .restart local v18       #limit:J
    .restart local v20       #now:J
    .restart local v31       #so:Lcom/mba/proxylight/Socket;
    .restart local v33       #step:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
    :cond_a
    :try_start_d
    invoke-interface {v11}, Ljava/util/Iterator;->remove()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1

    .line 98
    :try_start_e
    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Ljava/nio/channels/SocketChannel;->close()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_2

    .line 102
    :goto_6
    :try_start_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->currentOutSocket:Lcom/mba/proxylight/Socket;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$900(Lcom/mba/proxylight/RequestProcessor;)Lcom/mba/proxylight/Socket;

    move-result-object v35

    move-object/from16 v0, v31

    move-object/from16 v1, v35

    if-ne v0, v1, :cond_5

    .line 103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    const/16 v36, 0x0

    #setter for: Lcom/mba/proxylight/RequestProcessor;->currentOutSocket:Lcom/mba/proxylight/Socket;
    invoke-static/range {v35 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$902(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;)Lcom/mba/proxylight/Socket;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_1

    goto/16 :goto_3

    .line 252
    .end local v8           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;"
    .end local v11           #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;>;"
    .end local v16           #lastOp:J
    .end local v18           #limit:J
    .end local v20           #now:J
    .end local v25           #request:Lcom/mba/proxylight/Request;
    .end local v31           #so:Lcom/mba/proxylight/Socket;
    .end local v33           #step:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
    :catchall_2
    move-exception v35

    :try_start_10
    sget-object v36, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "inData:"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v38, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->inData:Ljava/lang/String;
    invoke-static/range {v38 .. v38}, Lcom/mba/proxylight/RequestProcessor;->access$1200(Lcom/mba/proxylight/RequestProcessor;)Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    const-string v37, ""

    #setter for: Lcom/mba/proxylight/RequestProcessor;->inData:Ljava/lang/String;
    invoke-static/range {v36 .. v37}, Lcom/mba/proxylight/RequestProcessor;->access$1202(Lcom/mba/proxylight/RequestProcessor;Ljava/lang/String;)Ljava/lang/String;

    .line 254
    sget-object v36, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "outData:"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v38, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->outData:Ljava/lang/String;
    invoke-static/range {v38 .. v38}, Lcom/mba/proxylight/RequestProcessor;->access$1900(Lcom/mba/proxylight/RequestProcessor;)Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    const-string v37, ""

    #setter for: Lcom/mba/proxylight/RequestProcessor;->outData:Ljava/lang/String;
    invoke-static/range {v36 .. v37}, Lcom/mba/proxylight/RequestProcessor;->access$1902(Lcom/mba/proxylight/RequestProcessor;Ljava/lang/String;)Ljava/lang/String;

    .line 256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    #calls: Lcom/mba/proxylight/RequestProcessor;->closeAll()V
    invoke-static/range {v36 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$800(Lcom/mba/proxylight/RequestProcessor;)V

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/mba/proxylight/RequestProcessor;->recycle()V

    throw v35
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 99
    .restart local v8       #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;"
    .restart local v11       #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;>;"
    .restart local v16       #lastOp:J
    .restart local v18       #limit:J
    .restart local v20       #now:J
    .restart local v25       #request:Lcom/mba/proxylight/Request;
    .restart local v31       #so:Lcom/mba/proxylight/Socket;
    .restart local v33       #step:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
    :catch_2
    move-exception v9

    .line 100
    .local v9, es:Ljava/lang/Exception;
    :try_start_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    const-string v36, ""

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-virtual {v0, v1, v9}, Lcom/mba/proxylight/RequestProcessor;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_6

    .line 132
    .end local v8           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;"
    .end local v9           #es:Ljava/lang/Exception;
    .end local v11           #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;>;"
    .end local v16           #lastOp:J
    .end local v18           #limit:J
    .end local v31           #so:Lcom/mba/proxylight/Socket;
    .restart local v15       #key:Ljava/nio/channels/SelectionKey;
    .restart local v22       #numRead:I
    .restart local v28       #selectedKeys:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .restart local v32       #socket:Lcom/mba/proxylight/Socket;
    :cond_b
    if-lez v22, :cond_9

    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$1000(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/ByteBuffer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_1

    move-object/from16 v26, v25

    .line 134
    .end local v25           #request:Lcom/mba/proxylight/Request;
    .local v26, request:Lcom/mba/proxylight/Request;
    :cond_c
    :goto_7
    :try_start_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$1000(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/ByteBuffer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v35

    if-lez v35, :cond_22

    .line 135
    sget-object v35, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->STATUS_LINE:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    if-ne v0, v1, :cond_d

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;
    invoke-static/range {v36 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$1000(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/ByteBuffer;

    move-result-object v36

    #calls: Lcom/mba/proxylight/RequestProcessor;->readNext(Ljava/nio/ByteBuffer;)Ljava/lang/String;
    invoke-static/range {v35 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$1300(Lcom/mba/proxylight/RequestProcessor;Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v27

    .line 138
    .local v27, s:Ljava/lang/String;
    if-eqz v27, :cond_21

    .line 139
    new-instance v25, Lcom/mba/proxylight/Request;

    invoke-direct/range {v25 .. v25}, Lcom/mba/proxylight/Request;-><init>()V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_3

    .line 140
    .end local v26           #request:Lcom/mba/proxylight/Request;
    .restart local v25       #request:Lcom/mba/proxylight/Request;
    :try_start_13
    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/mba/proxylight/Request;->setStatusline(Ljava/lang/String;)V

    .line 141
    sget-object v33, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->REQUEST_HEADERS:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_1

    :goto_8
    move-object/from16 v26, v25

    .line 143
    .end local v25           #request:Lcom/mba/proxylight/Request;
    .restart local v26       #request:Lcom/mba/proxylight/Request;
    goto :goto_7

    .end local v27           #s:Ljava/lang/String;
    :cond_d
    :try_start_14
    sget-object v35, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->REQUEST_HEADERS:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    if-ne v0, v1, :cond_1b

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;
    invoke-static/range {v36 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$1000(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/ByteBuffer;

    move-result-object v36

    #calls: Lcom/mba/proxylight/RequestProcessor;->readNext(Ljava/nio/ByteBuffer;)Ljava/lang/String;
    invoke-static/range {v35 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$1300(Lcom/mba/proxylight/RequestProcessor;Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v27

    .line 145
    .restart local v27       #s:Ljava/lang/String;
    if-eqz v27, :cond_c

    .line 146
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v35

    if-nez v35, :cond_1a

    .line 149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v26

    #calls: Lcom/mba/proxylight/RequestProcessor;->filterRequest(Lcom/mba/proxylight/Request;)Z
    invoke-static {v0, v1}, Lcom/mba/proxylight/RequestProcessor;->access$1400(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Request;)Z

    move-result v35

    if-eqz v35, :cond_e

    .line 150
    new-instance v35, Ljava/lang/Exception;

    const-string v36, "Requete interdite."

    invoke-direct/range {v35 .. v36}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v35

    .line 249
    .end local v27           #s:Ljava/lang/String;
    :catch_3
    move-exception v7

    move-object/from16 v25, v26

    .end local v26           #request:Lcom/mba/proxylight/Request;
    .restart local v25       #request:Lcom/mba/proxylight/Request;
    goto/16 :goto_5

    .line 153
    .end local v25           #request:Lcom/mba/proxylight/Request;
    .restart local v26       #request:Lcom/mba/proxylight/Request;
    .restart local v27       #s:Ljava/lang/String;
    :cond_e
    const-string v35, "GET"

    invoke-virtual/range {v26 .. v26}, Lcom/mba/proxylight/Request;->getMethod()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    .line 154
    .local v13, isGet:Z
    if-nez v13, :cond_f

    const-string v35, "POST"

    invoke-virtual/range {v26 .. v26}, Lcom/mba/proxylight/Request;->getMethod()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_f

    const/4 v14, 0x1

    .line 155
    .local v14, isPost:Z
    :goto_9
    if-nez v13, :cond_10

    if-nez v14, :cond_10

    const-string v35, "CONNECT"

    invoke-virtual/range {v26 .. v26}, Lcom/mba/proxylight/Request;->getMethod()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_10

    const/4 v12, 0x1

    .line 157
    .local v12, isConnect:Z
    :goto_a
    if-nez v13, :cond_11

    if-nez v14, :cond_11

    if-nez v12, :cond_11

    .line 158
    new-instance v35, Ljava/lang/RuntimeException;

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "Unknown method : "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v26 .. v26}, Lcom/mba/proxylight/Request;->getMethod()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-direct/range {v35 .. v36}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v35

    .line 154
    .end local v12           #isConnect:Z
    .end local v14           #isPost:Z
    :cond_f
    const/4 v14, 0x0

    goto :goto_9

    .line 155
    .restart local v14       #isPost:Z
    :cond_10
    const/4 v12, 0x0

    goto :goto_a

    .line 161
    .restart local v12       #isConnect:Z
    :cond_11
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v26 .. v26}, Lcom/mba/proxylight/Request;->getHost()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, ":"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v26 .. v26}, Lcom/mba/proxylight/Request;->getPort()I

    move-result v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 162
    .local v23, oh:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->outSockets:Ljava/util/Map;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$600(Lcom/mba/proxylight/RequestProcessor;)Ljava/util/Map;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/mba/proxylight/Socket;

    .line 163
    .local v24, outSocket:Lcom/mba/proxylight/Socket;
    if-nez v24, :cond_14

    .line 166
    new-instance v24, Lcom/mba/proxylight/Socket;

    .end local v24           #outSocket:Lcom/mba/proxylight/Socket;
    invoke-direct/range {v24 .. v24}, Lcom/mba/proxylight/Socket;-><init>()V

    .line 167
    .restart local v24       #outSocket:Lcom/mba/proxylight/Socket;
    invoke-static {}, Ljava/nio/channels/SocketChannel;->open()Ljava/nio/channels/SocketChannel;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v24

    iput-object v0, v1, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    .line 168
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    move-object/from16 v35, v0

    const/16 v36, 0x0

    invoke-virtual/range {v35 .. v36}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 169
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    move-object/from16 v35, v0

    new-instance v36, Ljava/net/InetSocketAddress;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v37, v0

    invoke-virtual/range {v26 .. v26}, Lcom/mba/proxylight/Request;->getHost()Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Lcom/mba/proxylight/RequestProcessor;->resolve(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v37

    invoke-virtual/range {v26 .. v26}, Lcom/mba/proxylight/Request;->getPort()I

    move-result v38

    invoke-direct/range {v36 .. v38}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual/range {v35 .. v36}, Ljava/nio/channels/SocketChannel;->connect(Ljava/net/SocketAddress;)Z

    move-result v35

    if-nez v35, :cond_13

    .line 171
    :cond_12
    const-wide/16 v35, 0x32

    invoke-static/range {v35 .. v36}, Ljava/lang/Thread;->sleep(J)V

    .line 172
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Ljava/nio/channels/SocketChannel;->finishConnect()Z

    move-result v35

    if-eqz v35, :cond_12

    .line 174
    :cond_13
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->selector:Ljava/nio/channels/Selector;
    invoke-static/range {v36 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$200(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/channels/Selector;

    move-result-object v36

    const/16 v37, 0x1

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move/from16 v2, v37

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Ljava/nio/channels/SocketChannel;->register(Ljava/nio/channels/Selector;ILjava/lang/Object;)Ljava/nio/channels/SelectionKey;

    .line 175
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->outSockets:Ljava/util/Map;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$600(Lcom/mba/proxylight/RequestProcessor;)Ljava/util/Map;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "Ajout d\'une socket vers "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, " sur le processeur "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v37, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->processorIdx:I
    invoke-static/range {v37 .. v37}, Lcom/mba/proxylight/RequestProcessor;->access$700(Lcom/mba/proxylight/RequestProcessor;)I

    move-result v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, ". Socket count="

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v37, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->outSockets:Ljava/util/Map;
    invoke-static/range {v37 .. v37}, Lcom/mba/proxylight/RequestProcessor;->access$600(Lcom/mba/proxylight/RequestProcessor;)Ljava/util/Map;

    move-result-object v37

    invoke-interface/range {v37 .. v37}, Ljava/util/Map;->size()I

    move-result v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Lcom/mba/proxylight/RequestProcessor;->debug(Ljava/lang/String;)V

    .line 178
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v24

    #setter for: Lcom/mba/proxylight/RequestProcessor;->currentOutSocket:Lcom/mba/proxylight/Socket;
    invoke-static {v0, v1}, Lcom/mba/proxylight/RequestProcessor;->access$902(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;)Lcom/mba/proxylight/Socket;

    .line 180
    if-eqz v12, :cond_15

    .line 183
    invoke-static {}, Lcom/mba/proxylight/RequestProcessor;->access$1500()[B

    move-result-object v35

    invoke-static/range {v35 .. v35}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 184
    .local v5, b:Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;
    invoke-static/range {v36 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$400(Lcom/mba/proxylight/RequestProcessor;)Lcom/mba/proxylight/Socket;

    move-result-object v36

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-wide/from16 v2, v20

    #calls: Lcom/mba/proxylight/RequestProcessor;->write(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)V
    invoke-static {v0, v1, v5, v2, v3}, Lcom/mba/proxylight/RequestProcessor;->access$1600(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)V

    .line 185
    sget-object v33, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->TRANSFER:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    .line 186
    goto/16 :goto_7

    .line 188
    .end local v5           #b:Ljava/nio/ByteBuffer;
    :cond_15
    new-instance v35, Ljava/lang/StringBuffer;

    invoke-virtual/range {v26 .. v26}, Lcom/mba/proxylight/Request;->getMethod()Ljava/lang/String;

    move-result-object v36

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v36, " "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v29

    .line 189
    .local v29, send:Ljava/lang/StringBuffer;
    invoke-virtual/range {v26 .. v26}, Lcom/mba/proxylight/Request;->getUrl()Ljava/lang/String;

    move-result-object v34

    .line 190
    .local v34, url:Ljava/lang/String;
    const-string v35, "/"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v35

    if-nez v35, :cond_16

    .line 191
    const/16 v35, 0x2f

    const/16 v36, 0x8

    invoke-virtual/range {v34 .. v36}, Ljava/lang/String;->indexOf(II)I

    move-result v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v34

    .line 193
    :cond_16
    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    const-string v36, " "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    invoke-virtual/range {v26 .. v26}, Lcom/mba/proxylight/Request;->getProtocol()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    const-string v36, "\r\n"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 194
    invoke-virtual/range {v26 .. v26}, Lcom/mba/proxylight/Request;->getHeaders()Ljava/util/Map;

    move-result-object v35

    invoke-interface/range {v35 .. v35}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v35

    invoke-interface/range {v35 .. v35}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v36

    :goto_b
    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->hasNext()Z

    move-result v35

    if-eqz v35, :cond_17

    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map$Entry;

    .line 195
    .local v10, h:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    move-object/from16 v0, v29

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    const-string v37, ": "

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v37

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    move-object/from16 v0, v37

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v35

    const-string v37, "\r\n"

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_b

    .line 197
    .end local v10           #h:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_17
    const-string v35, "\r\n"

    move-object/from16 v0, v29

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 198
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->getBytes()[B

    move-result-object v30

    .line 200
    .local v30, sendBytes:[B
    invoke-static/range {v30 .. v30}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 201
    .restart local v5       #b:Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v24

    move-wide/from16 v2, v20

    #calls: Lcom/mba/proxylight/RequestProcessor;->write(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)V
    invoke-static {v0, v1, v5, v2, v3}, Lcom/mba/proxylight/RequestProcessor;->access$1600(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)V

    .line 203
    const/4 v6, 0x0

    .line 204
    if-eqz v14, :cond_18

    .line 205
    invoke-virtual/range {v26 .. v26}, Lcom/mba/proxylight/Request;->getHeaders()Ljava/util/Map;

    move-result-object v35

    const-string v36, "Content-Length"

    invoke-interface/range {v35 .. v36}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 207
    :cond_18
    if-nez v6, :cond_19

    sget-object v33, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->STATUS_LINE:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    :goto_c
    goto/16 :goto_7

    :cond_19
    sget-object v33, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->REQUEST_CONTENT:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    goto :goto_c

    .line 210
    .end local v5           #b:Ljava/nio/ByteBuffer;
    .end local v12           #isConnect:Z
    .end local v13           #isGet:Z
    .end local v14           #isPost:Z
    .end local v23           #oh:Ljava/lang/String;
    .end local v24           #outSocket:Lcom/mba/proxylight/Socket;
    .end local v29           #send:Ljava/lang/StringBuffer;
    .end local v30           #sendBytes:[B
    .end local v34           #url:Ljava/lang/String;
    :cond_1a
    invoke-virtual/range {v26 .. v27}, Lcom/mba/proxylight/Request;->addHeader(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 213
    .end local v27           #s:Ljava/lang/String;
    :cond_1b
    sget-object v35, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->REQUEST_CONTENT:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    if-ne v0, v1, :cond_1d

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$1000(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/ByteBuffer;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v35

    sub-int v6, v6, v35

    .line 215
    if-gtz v6, :cond_1c

    .line 216
    sget-object v33, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->STATUS_LINE:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    .line 218
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->currentOutSocket:Lcom/mba/proxylight/Socket;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$900(Lcom/mba/proxylight/RequestProcessor;)Lcom/mba/proxylight/Socket;

    move-result-object v35

    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;
    invoke-static/range {v36 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$1000(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/ByteBuffer;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I

    goto/16 :goto_7

    .line 219
    :cond_1d
    sget-object v35, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->TRANSFER:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    if-ne v0, v1, :cond_c

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->currentOutSocket:Lcom/mba/proxylight/Socket;
    invoke-static/range {v36 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$900(Lcom/mba/proxylight/RequestProcessor;)Lcom/mba/proxylight/Socket;

    move-result-object v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v37, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;
    invoke-static/range {v37 .. v37}, Lcom/mba/proxylight/RequestProcessor;->access$1000(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/ByteBuffer;

    move-result-object v37

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-object/from16 v2, v37

    move-wide/from16 v3, v20

    #calls: Lcom/mba/proxylight/RequestProcessor;->write(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/mba/proxylight/RequestProcessor;->access$1600(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_2
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_3

    goto/16 :goto_7

    .line 225
    .end local v22           #numRead:I
    .end local v26           #request:Lcom/mba/proxylight/Request;
    .restart local v25       #request:Lcom/mba/proxylight/Request;
    :cond_1e
    :try_start_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->currentOutSocket:Lcom/mba/proxylight/Socket;
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$900(Lcom/mba/proxylight/RequestProcessor;)Lcom/mba/proxylight/Socket;

    move-result-object v35

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_1f

    .line 228
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v32

    #calls: Lcom/mba/proxylight/RequestProcessor;->closeOutSocket(Lcom/mba/proxylight/Socket;)V
    invoke-static {v0, v1}, Lcom/mba/proxylight/RequestProcessor;->access$1700(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;)V

    goto/16 :goto_4

    .line 232
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v36, v0

    #getter for: Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;
    invoke-static/range {v36 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$400(Lcom/mba/proxylight/RequestProcessor;)Lcom/mba/proxylight/Socket;

    move-result-object v36

    move-object/from16 v0, v35

    move-object/from16 v1, v32

    move-object/from16 v2, v36

    move-wide/from16 v3, v20

    #calls: Lcom/mba/proxylight/RequestProcessor;->transfer(Lcom/mba/proxylight/Socket;Lcom/mba/proxylight/Socket;J)Z
    invoke-static {v0, v1, v2, v3, v4}, Lcom/mba/proxylight/RequestProcessor;->access$1800(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;Lcom/mba/proxylight/Socket;J)Z

    move-result v35

    if-nez v35, :cond_9

    .line 234
    const-string v35, "CONNECT"

    invoke-virtual/range {v25 .. v25}, Lcom/mba/proxylight/Request;->getMethod()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_20

    .line 235
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    #calls: Lcom/mba/proxylight/RequestProcessor;->closeAll()V
    invoke-static/range {v35 .. v35}, Lcom/mba/proxylight/RequestProcessor;->access$800(Lcom/mba/proxylight/RequestProcessor;)V

    goto/16 :goto_2

    .line 238
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v32

    #calls: Lcom/mba/proxylight/RequestProcessor;->closeOutSocket(Lcom/mba/proxylight/Socket;)V
    invoke-static {v0, v1}, Lcom/mba/proxylight/RequestProcessor;->access$1700(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;)V

    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mba/proxylight/RequestProcessor$1;->this$0:Lcom/mba/proxylight/RequestProcessor;

    move-object/from16 v35, v0

    const/16 v36, 0x0

    #setter for: Lcom/mba/proxylight/RequestProcessor;->currentOutSocket:Lcom/mba/proxylight/Socket;
    invoke-static/range {v35 .. v36}, Lcom/mba/proxylight/RequestProcessor;->access$902(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;)Lcom/mba/proxylight/Socket;
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_2
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_1

    goto/16 :goto_2

    .end local v25           #request:Lcom/mba/proxylight/Request;
    .restart local v22       #numRead:I
    .restart local v26       #request:Lcom/mba/proxylight/Request;
    .restart local v27       #s:Ljava/lang/String;
    :cond_21
    move-object/from16 v25, v26

    .end local v26           #request:Lcom/mba/proxylight/Request;
    .restart local v25       #request:Lcom/mba/proxylight/Request;
    goto/16 :goto_8

    .end local v25           #request:Lcom/mba/proxylight/Request;
    .end local v27           #s:Ljava/lang/String;
    .restart local v26       #request:Lcom/mba/proxylight/Request;
    :cond_22
    move-object/from16 v25, v26

    .end local v26           #request:Lcom/mba/proxylight/Request;
    .restart local v25       #request:Lcom/mba/proxylight/Request;
    goto/16 :goto_4
.end method
