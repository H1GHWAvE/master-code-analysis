.class public Lcom/mba/proxylight/Socket;
.super Ljava/lang/Object;
.source "Socket.java"


# instance fields
.field public created:J

.field public lastRead:J

.field public lastWrite:J

.field public socket:Ljava/nio/channels/SocketChannel;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    .line 8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mba/proxylight/Socket;->created:J

    .line 9
    iget-wide v0, p0, Lcom/mba/proxylight/Socket;->created:J

    iput-wide v0, p0, Lcom/mba/proxylight/Socket;->lastWrite:J

    .line 10
    iget-wide v0, p0, Lcom/mba/proxylight/Socket;->created:J

    iput-wide v0, p0, Lcom/mba/proxylight/Socket;->lastRead:J

    return-void
.end method
