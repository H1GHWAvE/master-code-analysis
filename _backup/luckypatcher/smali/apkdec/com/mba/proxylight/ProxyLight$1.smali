.class Lcom/mba/proxylight/ProxyLight$1;
.super Ljava/lang/Object;
.source "ProxyLight.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mba/proxylight/ProxyLight;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mba/proxylight/ProxyLight;


# direct methods
.method constructor <init>(Lcom/mba/proxylight/ProxyLight;)V
    .locals 0
    .parameter "this$0"

    .prologue
    .line 62
    iput-object p1, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 69
    :try_start_0
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    invoke-static {}, Ljava/nio/channels/ServerSocketChannel;->open()Ljava/nio/channels/ServerSocketChannel;

    move-result-object v7

    iput-object v7, v6, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;

    .line 70
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iget-object v6, v6, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/nio/channels/ServerSocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 71
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iget-object v6, v6, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;

    invoke-virtual {v6}, Ljava/nio/channels/ServerSocketChannel;->socket()Ljava/net/ServerSocket;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/net/ServerSocket;->setReuseAddress(Z)V

    .line 72
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iget-object v6, v6, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;

    invoke-virtual {v6}, Ljava/nio/channels/ServerSocketChannel;->socket()Ljava/net/ServerSocket;

    move-result-object v6

    new-instance v7, Ljava/net/InetSocketAddress;

    const/16 v8, 0x1f90

    invoke-direct {v7, v8}, Ljava/net/InetSocketAddress;-><init>(I)V

    invoke-virtual {v6, v7}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;)V

    .line 74
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    move-result-object v7

    #setter for: Lcom/mba/proxylight/ProxyLight;->selector:Ljava/nio/channels/Selector;
    invoke-static {v6, v7}, Lcom/mba/proxylight/ProxyLight;->access$002(Lcom/mba/proxylight/ProxyLight;Ljava/nio/channels/Selector;)Ljava/nio/channels/Selector;

    .line 75
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iget-object v6, v6, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;

    iget-object v7, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    #getter for: Lcom/mba/proxylight/ProxyLight;->selector:Ljava/nio/channels/Selector;
    invoke-static {v7}, Lcom/mba/proxylight/ProxyLight;->access$000(Lcom/mba/proxylight/ProxyLight;)Ljava/nio/channels/Selector;

    move-result-object v7

    const/16 v8, 0x10

    invoke-virtual {v6, v7, v8}, Ljava/nio/channels/ServerSocketChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;

    .line 78
    :cond_0
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    #getter for: Lcom/mba/proxylight/ProxyLight;->selector:Ljava/nio/channels/Selector;
    invoke-static {v6}, Lcom/mba/proxylight/ProxyLight;->access$000(Lcom/mba/proxylight/ProxyLight;)Ljava/nio/channels/Selector;

    move-result-object v6

    invoke-virtual {v6}, Ljava/nio/channels/Selector;->select()I

    .line 79
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iget-object v6, v6, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v6, :cond_1

    .line 150
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iput-boolean v9, v6, Lcom/mba/proxylight/ProxyLight;->running:Z

    .line 152
    :goto_0
    return-void

    .line 82
    :cond_1
    :try_start_1
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    #getter for: Lcom/mba/proxylight/ProxyLight;->selector:Ljava/nio/channels/Selector;
    invoke-static {v6}, Lcom/mba/proxylight/ProxyLight;->access$000(Lcom/mba/proxylight/ProxyLight;)Ljava/nio/channels/Selector;

    move-result-object v6

    invoke-virtual {v6}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 84
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 85
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/nio/channels/SelectionKey;

    .line 86
    .local v2, key:Ljava/nio/channels/SelectionKey;
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 88
    invoke-virtual {v2}, Ljava/nio/channels/SelectionKey;->isValid()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 89
    invoke-virtual {v2}, Ljava/nio/channels/SelectionKey;->isAcceptable()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    move-result v6

    if-eqz v6, :cond_2

    .line 91
    const/4 v3, 0x0

    .line 92
    .local v3, p:Lcom/mba/proxylight/RequestProcessor;
    :try_start_2
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    #getter for: Lcom/mba/proxylight/ProxyLight;->processors:Ljava/util/Stack;
    invoke-static {v6}, Lcom/mba/proxylight/ProxyLight;->access$100(Lcom/mba/proxylight/ProxyLight;)Ljava/util/Stack;

    move-result-object v7

    monitor-enter v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_2 .. :try_end_2} :catch_1

    .line 93
    :cond_3
    :try_start_3
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    #getter for: Lcom/mba/proxylight/ProxyLight;->processors:Ljava/util/Stack;
    invoke-static {v6}, Lcom/mba/proxylight/ProxyLight;->access$100(Lcom/mba/proxylight/ProxyLight;)Ljava/util/Stack;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Stack;->size()I

    move-result v6

    if-lez v6, :cond_4

    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    #getter for: Lcom/mba/proxylight/ProxyLight;->processors:Ljava/util/Stack;
    invoke-static {v6}, Lcom/mba/proxylight/ProxyLight;->access$100(Lcom/mba/proxylight/ProxyLight;)Ljava/util/Stack;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/mba/proxylight/RequestProcessor;

    move-object v3, v0

    invoke-virtual {v3}, Lcom/mba/proxylight/RequestProcessor;->isAlive()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v6

    if-eqz v6, :cond_3

    :cond_4
    move-object v4, v3

    .line 94
    .end local v3           #p:Lcom/mba/proxylight/RequestProcessor;
    .local v4, p:Lcom/mba/proxylight/RequestProcessor;
    :try_start_4
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 95
    if-eqz v4, :cond_5

    :try_start_5
    invoke-virtual {v4}, Lcom/mba/proxylight/RequestProcessor;->isAlive()Z

    move-result v6

    if-nez v6, :cond_7

    .line 96
    :cond_5
    new-instance v3, Lcom/mba/proxylight/ProxyLight$1$1;

    invoke-direct {v3, p0}, Lcom/mba/proxylight/ProxyLight$1$1;-><init>(Lcom/mba/proxylight/ProxyLight$1;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_5 .. :try_end_5} :catch_1

    .line 132
    .end local v4           #p:Lcom/mba/proxylight/RequestProcessor;
    .restart local v3       #p:Lcom/mba/proxylight/RequestProcessor;
    :goto_2
    :try_start_6
    invoke-virtual {v3, v2}, Lcom/mba/proxylight/RequestProcessor;->process(Ljava/nio/channels/SelectionKey;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    .line 133
    :catch_0
    move-exception v5

    .line 134
    .local v5, t:Ljava/lang/Throwable;
    :goto_3
    :try_start_7
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iget-object v6, v6, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2

    if-nez v6, :cond_6

    .line 150
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iput-boolean v9, v6, Lcom/mba/proxylight/ProxyLight;->running:Z

    goto :goto_0

    .line 94
    .end local v5           #t:Ljava/lang/Throwable;
    :catchall_0
    move-exception v6

    :goto_4
    :try_start_8
    monitor-exit v7
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    throw v6
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_9 .. :try_end_9} :catch_1

    .line 145
    .end local v1           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .end local v2           #key:Ljava/nio/channels/SelectionKey;
    .end local v3           #p:Lcom/mba/proxylight/RequestProcessor;
    :catch_1
    move-exception v6

    .line 150
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iput-boolean v9, v6, Lcom/mba/proxylight/ProxyLight;->running:Z

    goto :goto_0

    .line 138
    .restart local v1       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .restart local v2       #key:Ljava/nio/channels/SelectionKey;
    .restart local v3       #p:Lcom/mba/proxylight/RequestProcessor;
    .restart local v5       #t:Ljava/lang/Throwable;
    :cond_6
    :try_start_a
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v5}, Lcom/mba/proxylight/ProxyLight;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_1

    .line 147
    .end local v1           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .end local v2           #key:Ljava/nio/channels/SelectionKey;
    .end local v3           #p:Lcom/mba/proxylight/RequestProcessor;
    .end local v5           #t:Ljava/lang/Throwable;
    :catch_2
    move-exception v5

    .line 148
    .restart local v5       #t:Ljava/lang/Throwable;
    :try_start_b
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v5}, Lcom/mba/proxylight/ProxyLight;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 150
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iput-boolean v9, v6, Lcom/mba/proxylight/ProxyLight;->running:Z

    goto/16 :goto_0

    .end local v5           #t:Ljava/lang/Throwable;
    :catchall_1
    move-exception v6

    iget-object v7, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iput-boolean v9, v7, Lcom/mba/proxylight/ProxyLight;->running:Z

    throw v6

    .line 133
    .restart local v1       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .restart local v2       #key:Ljava/nio/channels/SelectionKey;
    .restart local v4       #p:Lcom/mba/proxylight/RequestProcessor;
    :catch_3
    move-exception v5

    move-object v3, v4

    .end local v4           #p:Lcom/mba/proxylight/RequestProcessor;
    .restart local v3       #p:Lcom/mba/proxylight/RequestProcessor;
    goto :goto_3

    .line 94
    .end local v3           #p:Lcom/mba/proxylight/RequestProcessor;
    .restart local v4       #p:Lcom/mba/proxylight/RequestProcessor;
    :catchall_2
    move-exception v6

    move-object v3, v4

    .end local v4           #p:Lcom/mba/proxylight/RequestProcessor;
    .restart local v3       #p:Lcom/mba/proxylight/RequestProcessor;
    goto :goto_4

    .end local v3           #p:Lcom/mba/proxylight/RequestProcessor;
    .restart local v4       #p:Lcom/mba/proxylight/RequestProcessor;
    :cond_7
    move-object v3, v4

    .end local v4           #p:Lcom/mba/proxylight/RequestProcessor;
    .restart local v3       #p:Lcom/mba/proxylight/RequestProcessor;
    goto :goto_2
.end method
