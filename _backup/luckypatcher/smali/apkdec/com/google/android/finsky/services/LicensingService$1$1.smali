.class Lcom/google/android/finsky/services/LicensingService$1$1;
.super Lcom/android/vending/licensing/ILicenseResultListener$Stub;
.source "LicensingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/services/LicensingService$1;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/finsky/services/LicensingService$1;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/LicensingService$1;)V
    .locals 0
    .parameter "this$1"

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/finsky/services/LicensingService$1$1;->this$1:Lcom/google/android/finsky/services/LicensingService$1;

    invoke-direct {p0}, Lcom/android/vending/licensing/ILicenseResultListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public verifyLicense(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter "responseCode1"
    .parameter "signedData1"
    .parameter "signature1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/services/LicensingService$1$1;->this$1:Lcom/google/android/finsky/services/LicensingService$1;

    iget-object v0, v0, Lcom/google/android/finsky/services/LicensingService$1;->this$0:Lcom/google/android/finsky/services/LicensingService;

    iput p1, v0, Lcom/google/android/finsky/services/LicensingService;->responseCode:I

    .line 58
    iget-object v0, p0, Lcom/google/android/finsky/services/LicensingService$1$1;->this$1:Lcom/google/android/finsky/services/LicensingService$1;

    iget-object v0, v0, Lcom/google/android/finsky/services/LicensingService$1;->this$0:Lcom/google/android/finsky/services/LicensingService;

    iget v0, v0, Lcom/google/android/finsky/services/LicensingService;->responseCode:I

    const/16 v1, 0x103

    if-ne v0, v1, :cond_0

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/services/LicensingService$1$1;->this$1:Lcom/google/android/finsky/services/LicensingService$1;

    iget-object v0, v0, Lcom/google/android/finsky/services/LicensingService$1;->this$0:Lcom/google/android/finsky/services/LicensingService;

    iput-object p2, v0, Lcom/google/android/finsky/services/LicensingService;->signedData:Ljava/lang/String;

    .line 64
    iget-object v0, p0, Lcom/google/android/finsky/services/LicensingService$1$1;->this$1:Lcom/google/android/finsky/services/LicensingService$1;

    iget-object v0, v0, Lcom/google/android/finsky/services/LicensingService$1;->this$0:Lcom/google/android/finsky/services/LicensingService;

    iput-object p3, v0, Lcom/google/android/finsky/services/LicensingService;->signature:Ljava/lang/String;

    .line 65
    iget-object v0, p0, Lcom/google/android/finsky/services/LicensingService$1$1;->this$1:Lcom/google/android/finsky/services/LicensingService$1;

    iget-object v0, v0, Lcom/google/android/finsky/services/LicensingService$1;->this$0:Lcom/google/android/finsky/services/LicensingService;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/services/LicensingService;->mSetupDone:Z

    .line 66
    return-void
.end method
