.class public Lcom/google/android/finsky/billing/iab/MarketBillingService;
.super Landroid/app/Service;
.source "MarketBillingService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "BillingHack"

.field private static context:Landroid/content/Context;

.field private static dev_pay:Ljava/lang/String;

.field private static item:Ljava/lang/String;

.field private static sRandom:Ljava/util/Random;


# instance fields
.field private final mBinder:Lcom/android/vending/billing/IMarketBillingService$Stub;

.field mNotifier:Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

.field mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-string v0, ""

    sput-object v0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->item:Ljava/lang/String;

    .line 35
    const-string v0, ""

    sput-object v0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->dev_pay:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->sRandom:Ljava/util/Random;

    .line 42
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->sRandom:Ljava/util/Random;

    .line 43
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 96
    new-instance v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;-><init>(Lcom/google/android/finsky/billing/iab/MarketBillingService;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mBinder:Lcom/android/vending/billing/IMarketBillingService$Stub;

    .line 52
    new-instance v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

    invoke-direct {v0, p0, p0}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;-><init>(Lcom/google/android/finsky/billing/iab/MarketBillingService;Lcom/google/android/finsky/billing/iab/MarketBillingService;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mNotifier:Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

    .line 54
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->item:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"

    .prologue
    .line 25
    sput-object p0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->item:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->dev_pay:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"

    .prologue
    .line 25
    sput-object p0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->dev_pay:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200()Ljava/util/Random;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->sRandom:Ljava/util/Random;

    return-object v0
.end method

.method public static findReceiverName(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 7
    .parameter "context"
    .parameter "packageName"
    .parameter "intent"

    .prologue
    .line 357
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, p2, v5}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    .line 358
    invoke-interface {v4}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 360
    .local v0, v0:Ljava/util/ListIterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 361
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v1

    .local v1, v2:Ljava/lang/Object;
    move-object v4, v1

    .line 362
    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v4, :cond_0

    move-object v4, v1

    .line 366
    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 382
    check-cast v1, Landroid/content/pm/ResolveInfo;

    .end local v1           #v2:Ljava/lang/Object;
    iget-object v4, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {p2, p1, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 383
    move-object v2, p2

    .line 384
    .local v2, v4:Landroid/content/Intent;
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-object v3, v2

    .line 385
    .end local v2           #v4:Landroid/content/Intent;
    .local v3, v4:Landroid/content/Intent;
    :goto_0
    return-object v3

    .line 373
    .end local v3           #v4:Landroid/content/Intent;
    :cond_1
    move-object v2, p2

    .line 374
    .restart local v2       #v4:Landroid/content/Intent;
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cannot find billing receiver in package \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' for action: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 375
    const/4 v2, 0x0

    move-object v3, v2

    .line 377
    .end local v2           #v4:Landroid/content/Intent;
    .restart local v3       #v4:Landroid/content/Intent;
    goto :goto_0
.end method

.method public static sendResponseCode(Landroid/content/Context;Ljava/lang/String;JI)Z
    .locals 5
    .parameter "context"
    .parameter "packageName"
    .parameter "requestId"
    .parameter "responseCode"

    .prologue
    .line 390
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.vending.billing.RESPONSE_CODE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, p1, v3}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->findReceiverName(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 392
    .local v0, v0:Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 393
    const/4 v1, 0x0

    .line 402
    .local v1, v1:Z
    :goto_0
    return v1

    .line 396
    .end local v1           #v1:Z
    :cond_0
    const-string v2, "request_id"

    invoke-virtual {v0, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 397
    const-string v2, "response_code"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 398
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 399
    const/4 v1, 0x1

    .restart local v1       #v1:Z
    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .parameter "intent"

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mBinder:Lcom/android/vending/billing/IMarketBillingService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 48
    sput-object p0, Lcom/google/android/finsky/billing/iab/MarketBillingService;->context:Landroid/content/Context;

    .line 49
    return-void
.end method
