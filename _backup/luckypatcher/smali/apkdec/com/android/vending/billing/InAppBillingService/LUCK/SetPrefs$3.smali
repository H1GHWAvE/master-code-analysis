.class Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$3;
.super Ljava/lang/Object;
.source "SetPrefs.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V
    .locals 0
    .parameter "this$0"

    .prologue
    .line 203
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .parameter "arg0"
    .parameter "newValue"

    .prologue
    const/4 v5, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 206
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v3

    const-string v4, "config"

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 208
    .local v0, settings:Landroid/content/SharedPreferences;
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    const-string v4, "orientstion"

    invoke-virtual {v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    if-ne p1, v3, :cond_0

    const-string v3, "2"

    invoke-virtual {p2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 209
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "orientstion"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 210
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-virtual {v2, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->setRequestedOrientation(I)V

    .line 211
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "settings_change"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 235
    :goto_0
    return v1

    .line 215
    :cond_0
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    const-string v4, "orientstion"

    invoke-virtual {v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    if-ne p1, v3, :cond_1

    const-string v3, "1"

    invoke-virtual {p2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 218
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "orientstion"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 219
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-virtual {v3, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->setRequestedOrientation(I)V

    .line 220
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "settings_change"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 224
    :cond_1
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    const-string v4, "orientstion"

    invoke-virtual {v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    if-ne p1, v3, :cond_2

    const-string v3, "3"

    invoke-virtual {p2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 227
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "orientstion"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 228
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->setRequestedOrientation(I)V

    .line 229
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "settings_change"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    :cond_2
    move v1, v2

    .line 235
    goto :goto_0
.end method
