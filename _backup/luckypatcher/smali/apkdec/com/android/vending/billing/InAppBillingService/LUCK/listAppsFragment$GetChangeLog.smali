.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;
.super Landroid/os/AsyncTask;
.source "listAppsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GetChangeLog"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .locals 0
    .parameter "this$0"

    .prologue
    .line 10995
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 17
    .parameter "params"

    .prologue
    .line 10999
    sget-object v14, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "LuckyPatcher: download changelog."

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 11000
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v15, "New"

    iput-object v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->Text:Ljava/lang/String;

    .line 11001
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/4 v15, 0x0

    iput v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->FalseHttp:I

    .line 11002
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/4 v15, 0x0

    iput v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    .line 11003
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v15, "LuckyPatcher.apk"

    iput-object v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->ApkName:Ljava/lang/String;

    .line 11004
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v15, "Lucky Patcher"

    iput-object v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->AppName:Ljava/lang/String;

    .line 11006
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v15, "http://content.wuala.com/contents/chelpus/Luckypatcher/"

    iput-object v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->Mirror1:Ljava/lang/String;

    .line 11007
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v15, "http://chelpus.defcon5.biz/Version.txt"

    iput-object v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->BuildVersionPath:Ljava/lang/String;

    .line 11008
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v15, "http://chelpus.defcon5.biz/Changelogs.txt"

    iput-object v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->BuildVersionChanges:Ljava/lang/String;

    .line 11009
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "package:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->luckyPackage:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->PackageName:Ljava/lang/String;

    .line 11010
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "http://chelpus.defcon5.biz/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->ApkName:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->urlpath:Ljava/lang/String;

    .line 11011
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/Download/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 11012
    .local v1, PATH:Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 11013
    .local v6, file:Ljava/io/File;
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v14, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->ApkName:Ljava/lang/String;

    invoke-direct {v10, v6, v14}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 11014
    .local v10, outputFile:Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 11016
    :cond_0
    const/4 v9, 0x1

    .line 11017
    .local v9, mirror:Z
    :goto_0
    if-eqz v9, :cond_2

    .line 11019
    :try_start_0
    new-instance v12, Ljava/net/URL;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v14, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->BuildVersionPath:Ljava/lang/String;

    invoke-direct {v12, v14}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 11021
    .local v12, u:Ljava/net/URL;
    invoke-virtual {v12}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v4

    check-cast v4, Ljava/net/HttpURLConnection;

    .line 11022
    .local v4, c:Ljava/net/HttpURLConnection;
    const-string v14, "GET"

    invoke-virtual {v4, v14}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 11023
    const v14, 0xf4240

    invoke-virtual {v4, v14}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 11024
    const/4 v14, 0x0

    invoke-virtual {v4, v14}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 11025
    const-string v14, "Cache-Control"

    const-string v15, "no-cache"

    invoke-virtual {v4, v14, v15}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 11026
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->connect()V

    .line 11031
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    .line 11033
    .local v7, in:Ljava/io/InputStream;
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 11035
    .local v2, baos:Ljava/io/ByteArrayOutputStream;
    const/16 v14, 0x2000

    new-array v3, v14, [B

    .line 11039
    .local v3, buffer:[B
    const/4 v8, 0x0

    .line 11040
    .local v8, len1:I
    :goto_1
    invoke-virtual {v7, v3}, Ljava/io/InputStream;->read([B)I

    move-result v8

    const/4 v14, -0x1

    if-eq v8, v14, :cond_1

    .line 11041
    const/4 v14, 0x0

    invoke-virtual {v2, v3, v14, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_1

    .line 11050
    .end local v2           #baos:Ljava/io/ByteArrayOutputStream;
    .end local v3           #buffer:[B
    .end local v4           #c:Ljava/net/HttpURLConnection;
    .end local v7           #in:Ljava/io/InputStream;
    .end local v8           #len1:I
    .end local v12           #u:Ljava/net/URL;
    :catch_0
    move-exception v5

    .line 11052
    .local v5, e:Ljava/net/MalformedURLException;
    invoke-virtual {v5}, Ljava/net/MalformedURLException;->printStackTrace()V

    .line 11079
    .end local v5           #e:Ljava/net/MalformedURLException;
    :goto_2
    const/4 v9, 0x0

    goto :goto_0

    .line 11044
    .restart local v2       #baos:Ljava/io/ByteArrayOutputStream;
    .restart local v3       #buffer:[B
    .restart local v4       #c:Ljava/net/HttpURLConnection;
    .restart local v7       #in:Ljava/io/InputStream;
    .restart local v8       #len1:I
    .restart local v12       #u:Ljava/net/URL;
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v11

    .line 11046
    .local v11, s:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    iput v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    .line 11049
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_2

    .line 11053
    .end local v2           #baos:Ljava/io/ByteArrayOutputStream;
    .end local v3           #buffer:[B
    .end local v4           #c:Ljava/net/HttpURLConnection;
    .end local v7           #in:Ljava/io/InputStream;
    .end local v8           #len1:I
    .end local v11           #s:Ljava/lang/String;
    .end local v12           #u:Ljava/net/URL;
    :catch_1
    move-exception v5

    .line 11054
    .local v5, e:Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 11055
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->FalseHttp:I

    add-int/lit8 v15, v15, 0x1

    iput v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->FalseHttp:I

    goto :goto_2

    .line 11057
    .end local v5           #e:Ljava/io/IOException;
    :catch_2
    move-exception v5

    .line 11058
    .local v5, e:Ljava/lang/NumberFormatException;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->FalseHttp:I

    add-int/lit8 v15, v15, 0x1

    iput v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->FalseHttp:I

    goto :goto_2

    .line 11060
    .end local v5           #e:Ljava/lang/NumberFormatException;
    :catch_3
    move-exception v5

    .line 11061
    .local v5, e:Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 11086
    .end local v5           #e:Ljava/lang/Exception;
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v14, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    sget v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->versionCodeLocal:I

    if-le v14, v15, :cond_3

    .line 11087
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v14, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    const/16 v15, 0x3e7

    if-ne v14, v15, :cond_4

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v14

    const-string v15, "999"

    const/16 v16, 0x0

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 11135
    :cond_3
    :goto_3
    const/4 v14, 0x1

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    return-object v14

    .line 11092
    :cond_4
    :try_start_2
    new-instance v13, Ljava/net/URL;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v14, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->BuildVersionChanges:Ljava/lang/String;

    invoke-direct {v13, v14}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 11094
    .local v13, ur:Ljava/net/URL;
    invoke-virtual {v13}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v4

    check-cast v4, Ljava/net/HttpURLConnection;

    .line 11095
    .restart local v4       #c:Ljava/net/HttpURLConnection;
    const-string v14, "GET"

    invoke-virtual {v4, v14}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 11096
    const v14, 0xf4240

    invoke-virtual {v4, v14}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 11097
    const/4 v14, 0x0

    invoke-virtual {v4, v14}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 11098
    const-string v14, "Cache-Control"

    const-string v15, "no-cache"

    invoke-virtual {v4, v14, v15}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 11099
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->connect()V

    .line 11104
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    .line 11106
    .restart local v7       #in:Ljava/io/InputStream;
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 11108
    .restart local v2       #baos:Ljava/io/ByteArrayOutputStream;
    const/16 v14, 0x2000

    new-array v3, v14, [B

    .line 11112
    .restart local v3       #buffer:[B
    const/4 v8, 0x0

    .line 11113
    .restart local v8       #len1:I
    :goto_4
    invoke-virtual {v7, v3}, Ljava/io/InputStream;->read([B)I

    move-result v8

    const/4 v14, -0x1

    if-eq v8, v14, :cond_5

    .line 11114
    const/4 v14, 0x0

    invoke-virtual {v2, v3, v14, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    goto :goto_4

    .line 11122
    .end local v2           #baos:Ljava/io/ByteArrayOutputStream;
    .end local v3           #buffer:[B
    .end local v4           #c:Ljava/net/HttpURLConnection;
    .end local v7           #in:Ljava/io/InputStream;
    .end local v8           #len1:I
    .end local v13           #ur:Ljava/net/URL;
    :catch_4
    move-exception v5

    .line 11124
    .local v5, e:Ljava/net/MalformedURLException;
    invoke-virtual {v5}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_3

    .line 11118
    .end local v5           #e:Ljava/net/MalformedURLException;
    .restart local v2       #baos:Ljava/io/ByteArrayOutputStream;
    .restart local v3       #buffer:[B
    .restart local v4       #c:Ljava/net/HttpURLConnection;
    .restart local v7       #in:Ljava/io/InputStream;
    .restart local v8       #len1:I
    .restart local v13       #ur:Ljava/net/URL;
    :cond_5
    :try_start_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->Changes:Ljava/lang/String;

    .line 11119
    new-instance v14, Ljava/io/File;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/Changes/changelog.txt"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v15, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->Changes:Ljava/lang/String;

    invoke-static {v14, v15}, Lcom/chelpus/Utils;->save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z

    .line 11121
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6

    goto :goto_3

    .line 11125
    .end local v2           #baos:Ljava/io/ByteArrayOutputStream;
    .end local v3           #buffer:[B
    .end local v4           #c:Ljava/net/HttpURLConnection;
    .end local v7           #in:Ljava/io/InputStream;
    .end local v8           #len1:I
    .end local v13           #ur:Ljava/net/URL;
    :catch_5
    move-exception v5

    .line 11126
    .local v5, e:Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 11130
    .end local v5           #e:Ljava/io/IOException;
    :catch_6
    move-exception v5

    .line 11131
    .local v5, e:Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    .line 11128
    .end local v5           #e:Ljava/lang/Exception;
    :catch_7
    move-exception v14

    goto/16 :goto_3
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 10995
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 6
    .parameter "result"

    .prologue
    .line 11140
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 11142
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog$1;

    invoke-direct {v3, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;)V

    .line 11170
    .local v3, dialogClickListener:Landroid/content/DialogInterface$OnClickListener;
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f07017a

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f070023

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->version:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\nChangeLog:\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->Changes:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f070226

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/chelpus/Utils;->showDialogCustomYes(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 11175
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 10995
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$GetChangeLog;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
