.class Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1$1;
.super Lpxb/android/axml/NodeVisitor;
.source "AxmlExample.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1;->child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1;Lpxb/android/axml/NodeVisitor;)V
    .locals 0
    .parameter "this$2"
    .parameter "x0"

    .prologue
    .line 93
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1$1;->this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1;

    invoke-direct {p0, p2}, Lpxb/android/axml/NodeVisitor;-><init>(Lpxb/android/axml/NodeVisitor;)V

    return-void
.end method


# virtual methods
.method public attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
    .locals 8
    .parameter "ns"
    .parameter "name"
    .parameter "resourceId"
    .parameter "type"
    .parameter "obj"

    .prologue
    .line 98
    const-string v0, "http://schemas.android.com/apk/res/android"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "name"

    .line 99
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v7, p5

    .line 100
    check-cast v7, Ljava/lang/String;

    .line 101
    .local v7, serviceClass:Ljava/lang/String;
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "name "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "obj "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1$1;->this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1;

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1;

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->found1:Z

    .line 103
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1$1;->this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1;

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1;

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->permissions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move-object v5, p5

    .end local p5
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 104
    .local v6, perm:Ljava/lang/String;
    invoke-virtual {v7, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 106
    const-string v1, "disabled_"

    invoke-virtual {v7, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 107
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "disabled_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v6, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p5

    .line 109
    .local p5, obj:Ljava/lang/String;
    :goto_1
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1$1;->this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1;

    iget-object v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1;

    iget-object v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->found1:Z

    .line 110
    invoke-super/range {p0 .. p5}, Lpxb/android/axml/NodeVisitor;->attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V

    move-object v5, p5

    goto :goto_0

    .line 108
    .end local p5           #obj:Ljava/lang/String;
    :cond_1
    const-string v1, "disabled_"

    const-string v2, ""

    invoke-virtual {v7, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p5

    .restart local p5       #obj:Ljava/lang/String;
    goto :goto_1

    .line 114
    .end local v6           #perm:Ljava/lang/String;
    .end local p5           #obj:Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1$1;->this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1;

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1;

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;

    iget-boolean v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->found1:Z

    if-nez v0, :cond_3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-super/range {v0 .. v5}, Lpxb/android/axml/NodeVisitor;->attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V

    .line 121
    .end local v7           #serviceClass:Ljava/lang/String;
    :cond_3
    :goto_2
    return-void

    .line 118
    .local p5, obj:Ljava/lang/Object;
    :cond_4
    invoke-super/range {p0 .. p5}, Lpxb/android/axml/NodeVisitor;->attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V

    goto :goto_2
.end method
