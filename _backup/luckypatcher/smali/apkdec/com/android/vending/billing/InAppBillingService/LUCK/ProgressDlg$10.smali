.class Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$10;
.super Ljava/lang/Object;
.source "ProgressDlg.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setProgressNumberFormat(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;)V
    .locals 0
    .parameter "this$0"

    .prologue
    .line 244
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$10;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 247
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$10;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    const v3, 0x7f0d00df

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 249
    .local v0, inc_info:Landroid/widget/TextView;
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$10;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    const v3, 0x7f0d00dd

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 250
    .local v1, progressInc:Landroid/widget/ProgressBar;
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$10;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->format:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget v5, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->max:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    return-void
.end method
