.class Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;
.super Ljava/lang/Object;
.source "PkgListItemAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Refresh_Packages"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;)V
    .locals 0
    .parameter "this$0"

    .prologue
    .line 658
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 28

    .prologue
    .line 662
    const/16 v21, 0x0

    sput-boolean v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->refresh:Z

    .line 663
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v21, v0

    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->add:Ljava/util/ArrayList;

    .line 664
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v21, v0

    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->del:Ljava/util/ArrayList;

    .line 665
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 666
    .local v12, patAll:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPackages()[Ljava/lang/String;

    move-result-object v11

    .line 670
    .local v11, packages:[Ljava/lang/String;
    :try_start_0
    sget-object v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    if-nez v21, :cond_0

    .line 671
    new-instance v21, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    .line 673
    :cond_0
    sget-object v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    const/16 v22, 0x1

    const/16 v23, 0x1

    invoke-virtual/range {v21 .. v23}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->getPackage(ZZ)Ljava/util/ArrayList;

    move-result-object v12

    .line 676
    const/16 v17, 0x0

    .line 677
    .local v17, sysshow:Z
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v21

    const-string v22, "systemapp"

    const/16 v23, 0x0

    invoke-interface/range {v21 .. v23}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v17

    .line 679
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->add:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->clear()V

    .line 681
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v21

    new-array v13, v0, [Ljava/lang/String;

    .line 682
    .local v13, patArray:[Ljava/lang/String;
    const/4 v8, 0x0

    .line 683
    .local v8, k:I
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_1

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 684
    .local v14, pkg:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    iget-object v0, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v13, v8

    .line 685
    add-int/lit8 v8, v8, 0x1

    .line 686
    goto :goto_0

    .line 687
    .end local v14           #pkg:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_1
    array-length v0, v11

    move/from16 v23, v0

    const/16 v21, 0x0

    move/from16 v22, v21

    :goto_1
    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_7

    aget-object v15, v11, v22
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    .line 690
    .local v15, pkgname:Ljava/lang/String;
    const/4 v6, 0x0

    .line 692
    .local v6, found:Z
    :try_start_1
    array-length v0, v13

    move/from16 v24, v0

    const/16 v21, 0x0

    :goto_2
    move/from16 v0, v21

    move/from16 v1, v24

    if-ge v0, v1, :cond_5

    aget-object v16, v13, v21

    .line 694
    .local v16, strr:Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_2

    .line 695
    const/4 v6, 0x1

    .line 697
    :cond_2
    const-string v25, "android"

    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v25, v0

    #getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;
    invoke-static/range {v25 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->access$000(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;)Landroid/content/Context;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v25

    if-eqz v25, :cond_4

    :cond_3
    const/4 v6, 0x1

    .line 692
    :cond_4
    add-int/lit8 v21, v21, 0x1

    goto :goto_2

    .line 699
    .end local v16           #strr:Ljava/lang/String;
    :cond_5
    if-nez v6, :cond_6

    .line 702
    :try_start_2
    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v21

    sget v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I

    const/16 v25, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v9, v0, v15, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;-><init>(Landroid/content/Context;Ljava/lang/String;IZ)V

    .line 703
    .local v9, local:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    invoke-virtual {v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->saveItem()V

    .line 704
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->add:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    .line 687
    .end local v9           #local:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_6
    :goto_3
    add-int/lit8 v21, v22, 0x1

    move/from16 v22, v21

    goto :goto_1

    .line 705
    :catch_0
    move-exception v5

    .line 706
    .local v5, e:Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 707
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "LuckyPatcher (refreshAddApplication): Error "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    .line 711
    .end local v5           #e:Ljava/lang/Exception;
    :catch_1
    move-exception v5

    .line 713
    .local v5, e:Ljava/lang/IllegalArgumentException;
    :try_start_4
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "LuckyPatcher (refreshAddApps1): "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_3

    .line 842
    .end local v5           #e:Ljava/lang/IllegalArgumentException;
    .end local v6           #found:Z
    .end local v8           #k:I
    .end local v13           #patArray:[Ljava/lang/String;
    .end local v15           #pkgname:Ljava/lang/String;
    .end local v17           #sysshow:Z
    :catch_2
    move-exception v5

    .line 843
    .local v5, e:Ljava/lang/RuntimeException;
    const/16 v21, 0x0

    sput-boolean v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->addapps:Z

    .line 844
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v21, v0

    #getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;
    invoke-static/range {v21 .. v21}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->access$000(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;)Landroid/content/Context;

    move-result-object v21

    check-cast v21, Landroid/app/Activity;

    new-instance v22, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages$2;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;)V

    invoke-virtual/range {v21 .. v22}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 871
    .end local v5           #e:Ljava/lang/RuntimeException;
    :goto_4
    return-void

    .line 715
    .restart local v6       #found:Z
    .restart local v8       #k:I
    .restart local v13       #patArray:[Ljava/lang/String;
    .restart local v15       #pkgname:Ljava/lang/String;
    .restart local v17       #sysshow:Z
    :catch_3
    move-exception v5

    .line 716
    .local v5, e:Ljava/lang/Exception;
    :try_start_5
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "LuckyPatcher (refreshApps1): "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 717
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    .line 721
    .end local v5           #e:Ljava/lang/Exception;
    .end local v6           #found:Z
    .end local v15           #pkgname:Ljava/lang/String;
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_2

    move-result-wide v18

    .line 725
    .local v18, t:J
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v24, v0

    const/16 v21, 0x0

    move/from16 v22, v21

    :goto_5
    move/from16 v0, v22

    move/from16 v1, v24

    if-ge v0, v1, :cond_b

    aget-object v7, v23, v22

    .line 727
    .local v7, item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    const/4 v10, 0x0

    .line 728
    .local v10, marker:Z
    array-length v0, v11

    move/from16 v25, v0

    const/16 v21, 0x0

    :goto_6
    move/from16 v0, v21

    move/from16 v1, v25

    if-ge v0, v1, :cond_9

    aget-object v15, v11, v21

    .line 729
    .restart local v15       #pkgname:Ljava/lang/String;
    iget-object v0, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_8

    .line 730
    const/4 v10, 0x1

    .line 728
    :cond_8
    add-int/lit8 v21, v21, 0x1

    goto :goto_6

    .line 733
    .end local v15           #pkgname:Ljava/lang/String;
    :cond_9
    if-nez v10, :cond_a

    .line 734
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->del:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 735
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "delete item!"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 725
    :cond_a
    add-int/lit8 v21, v22, 0x1

    move/from16 v22, v21

    goto :goto_5

    .line 738
    .end local v7           #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .end local v10           #marker:Z
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->add:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    if-nez v21, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->add:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    if-gtz v21, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->del:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    if-nez v21, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->del:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    if-lez v21, :cond_d

    .line 741
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v21, v0

    #getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;
    invoke-static/range {v21 .. v21}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->access$000(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;)Landroid/content/Context;

    move-result-object v21

    check-cast v21, Landroid/app/Activity;

    new-instance v22, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages$1;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;)V

    invoke-virtual/range {v21 .. v22}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_2

    .line 775
    :cond_d
    :goto_7
    const/4 v4, 0x1

    .line 776
    .local v4, concurent_test:Z
    :goto_8
    if-eqz v4, :cond_10

    .line 779
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v23, v0

    const/16 v21, 0x0

    :goto_9
    move/from16 v0, v21

    move/from16 v1, v23

    if-ge v0, v1, :cond_f

    aget-object v7, v22, v21

    .line 782
    .restart local v7       #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v24

    iget-object v0, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v25, v0

    sget v26, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I

    const/16 v27, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-direct {v9, v0, v1, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;-><init>(Landroid/content/Context;Ljava/lang/String;IZ)V

    .line 783
    .restart local v9       #local:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v24, v0

    iget-object v0, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getItem(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-result-object v20

    .line 786
    .local v20, tmp:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->equalsPli(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)Z

    move-result v24

    if-nez v24, :cond_e

    .line 804
    iget-object v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    .line 805
    iget-object v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    .line 806
    iget v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->storepref:I

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->storepref:I

    .line 807
    iget v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 808
    iget-boolean v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    .line 809
    iget-object v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->statusi:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->statusi:Ljava/lang/String;

    .line 810
    iget-boolean v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    .line 811
    iget-boolean v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    .line 812
    iget-boolean v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    .line 813
    iget-boolean v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    .line 814
    iget-boolean v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    .line 815
    iget-boolean v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    .line 816
    iget-boolean v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    .line 817
    iget-boolean v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    .line 818
    iget-boolean v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    .line 819
    iget-boolean v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    .line 820
    iget-boolean v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    .line 821
    iget v0, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v20

    iput v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    .line 825
    invoke-virtual {v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->saveItem()V
    :try_end_7
    .catch Ljava/util/ConcurrentModificationException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_2

    .line 779
    :cond_e
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_9

    .line 771
    .end local v4           #concurent_test:Z
    .end local v7           #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .end local v9           #local:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .end local v20           #tmp:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :catch_4
    move-exception v5

    .line 772
    .restart local v5       #e:Ljava/lang/Exception;
    :try_start_8
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "LuckyPatcher (refreshApps2): Error Package Scan! "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 773
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_7

    .line 829
    .end local v5           #e:Ljava/lang/Exception;
    .restart local v4       #concurent_test:Z
    :catch_5
    move-exception v5

    .line 831
    .local v5, e:Ljava/util/ConcurrentModificationException;
    const/4 v4, 0x1

    .line 832
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "LuckyPatcher:retry refresh app (concurent)."

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 834
    .end local v5           #e:Ljava/util/ConcurrentModificationException;
    :catch_6
    move-exception v5

    .line 835
    .local v5, e:Ljava/lang/Exception;
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "LuckyPatcher (refreshApps3): Error Package Scan! "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 836
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 838
    .end local v5           #e:Ljava/lang/Exception;
    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_8

    .line 841
    :cond_10
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    sub-long v22, v22, v18

    invoke-virtual/range {v21 .. v23}, Ljava/io/PrintStream;->println(J)V
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_2

    .line 858
    const/16 v21, 0x0

    sput-boolean v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->addapps:Z

    .line 860
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    move-object/from16 v21, v0

    #getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;
    invoke-static/range {v21 .. v21}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->access$000(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;)Landroid/content/Context;

    move-result-object v21

    check-cast v21, Landroid/app/Activity;

    new-instance v22, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages$3;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;)V

    invoke-virtual/range {v21 .. v22}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_4
.end method
