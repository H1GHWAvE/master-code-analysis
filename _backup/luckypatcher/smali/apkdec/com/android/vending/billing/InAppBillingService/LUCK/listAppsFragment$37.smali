.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$37;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->populateAdapter(Ljava/util/ArrayList;Ljava/util/Comparator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .locals 0
    .parameter "this$0"

    .prologue
    .line 6057
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$37;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 5
    .parameter "parent"
    .parameter "v"
    .parameter "groupPosition"
    .parameter "id"

    .prologue
    const/4 v2, 0x1

    .line 6060
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->menu_adapter:Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;

    invoke-virtual {v3, p3}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    move-result-object v1

    .line 6061
    .local v1, item:Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;
    iget-object v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->childs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_0

    iget v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    if-ne v3, v2, :cond_2

    .line 6062
    :cond_0
    iget v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    packed-switch v3, :pswitch_data_0

    .line 6097
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$37;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v4, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu:I

    invoke-virtual {v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runId(I)V

    .line 6105
    :cond_1
    :goto_0
    :pswitch_0
    return v2

    .line 6066
    :pswitch_1
    iget v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu:I

    sparse-switch v3, :sswitch_data_0

    goto :goto_0

    .line 6086
    :sswitch_0
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$37;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showAbout()V

    goto :goto_0

    .line 6068
    :sswitch_1
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$37;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->selectLanguage()V

    goto :goto_0

    .line 6071
    :sswitch_2
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$37;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->changeDefaultDir()V

    goto :goto_0

    .line 6074
    :sswitch_3
    new-instance v0, Landroid/content/Intent;

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-class v4, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 6075
    .local v0, intent:Landroid/content/Intent;
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$37;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 6078
    .end local v0           #intent:Landroid/content/Intent;
    :sswitch_4
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v3, :cond_1

    .line 6079
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$37;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runUpdate()V

    goto :goto_0

    .line 6083
    :sswitch_5
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$37;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->sendLog()V

    goto :goto_0

    .line 6089
    :sswitch_6
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$37;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->changeDayOnUp()V

    goto :goto_0

    .line 6094
    :pswitch_2
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$37;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v4, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->runCode:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 6105
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 6062
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 6066
    :sswitch_data_0
    .sparse-switch
        0x7f070006 -> :sswitch_0
        0x7f0700dc -> :sswitch_6
        0x7f0700e3 -> :sswitch_2
        0x7f07012b -> :sswitch_3
        0x7f070145 -> :sswitch_1
        0x7f0701d9 -> :sswitch_5
        0x7f070226 -> :sswitch_4
    .end sparse-switch
.end method
