.class public Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
.super Ljava/lang/Object;
.source "AlertDlg.java"


# instance fields
.field public adapter:Landroid/widget/ArrayAdapter;

.field public context:Landroid/content/Context;

.field public dialog:Landroid/app/Dialog;

.field public not_close:Z

.field public root:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .parameter "context"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    .line 33
    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->root:Landroid/view/View;

    .line 34
    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->context:Landroid/content/Context;

    .line 35
    iput-boolean v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->not_close:Z

    .line 285
    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->adapter:Landroid/widget/ArrayAdapter;

    .line 37
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->context:Landroid/content/Context;

    .line 38
    new-instance v0, Landroid/app/Dialog;

    invoke-direct {v0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    .line 39
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 40
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 43
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 116
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v1, 0x7f040004

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 118
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 120
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .parameter "context"
    .parameter "style"

    .prologue
    const/4 v1, 0x0

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    .line 33
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->root:Landroid/view/View;

    .line 34
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->context:Landroid/content/Context;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->not_close:Z

    .line 285
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->adapter:Landroid/widget/ArrayAdapter;

    .line 122
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->context:Landroid/content/Context;

    .line 123
    new-instance v0, Landroid/app/Dialog;

    invoke-direct {v0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    .line 124
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v1, 0x7f040004

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 125
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 127
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2
    .parameter "context"
    .parameter "fullscreen"

    .prologue
    const/4 v1, 0x0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    .line 33
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->root:Landroid/view/View;

    .line 34
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->context:Landroid/content/Context;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->not_close:Z

    .line 285
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->adapter:Landroid/widget/ArrayAdapter;

    .line 129
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->context:Landroid/content/Context;

    .line 130
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$2;

    const v1, 0x7f080005

    invoke-direct {v0, p0, p1, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    .line 139
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v1, 0x7f040004

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 140
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 141
    return-void
.end method


# virtual methods
.method public create()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    return-object v0
.end method

.method public setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 6
    .parameter "array_adapter"
    .parameter "ok_l"

    .prologue
    const/4 v5, 0x3

    .line 267
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->adapter:Landroid/widget/ArrayAdapter;

    .line 268
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v4, 0x7f0d0027

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 269
    .local v2, list:Landroid/widget/ListView;
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v4, 0x7f0d0028

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 271
    .local v0, button_layout:Landroid/widget/LinearLayout;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 272
    invoke-virtual {v2, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 273
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$9;

    invoke-direct {v3, p0, p2, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$9;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;Landroid/widget/AdapterView$OnItemClickListener;Landroid/widget/ArrayAdapter;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 281
    new-array v1, v5, [I

    fill-array-data v1, :array_0

    .line 282
    .local v1, colors:[I
    new-instance v3, Landroid/graphics/drawable/GradientDrawable;

    sget-object v4, Landroid/graphics/drawable/GradientDrawable$Orientation;->RIGHT_LEFT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-direct {v3, v4, v1}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 283
    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 284
    return-object p0

    .line 281
    nop

    :array_0
    .array-data 0x4
        0x54t 0xabt 0xbft 0x33t
        0x54t 0xabt 0xbft 0x33t
        0x54t 0xabt 0xbft 0x33t
    .end array-data
.end method

.method public setAdapterNotClose(Z)V
    .locals 0
    .parameter "bool"

    .prologue
    .line 288
    iput-boolean p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->not_close:Z

    .line 289
    return-void
.end method

.method public setCancelable(Z)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 1
    .parameter "cancel"

    .prologue
    .line 189
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    return-object p0
.end method

.method public setIcon(I)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 6
    .parameter "id"

    .prologue
    const/4 v5, 0x0

    .line 150
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v4, 0x7f0d0020

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 151
    .local v2, linear:Landroid/widget/LinearLayout;
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 152
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v4, 0x7f0d0021

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 153
    .local v0, icon:Landroid/widget/ImageView;
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 154
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 155
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v4, 0x7f0d0023

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 156
    .local v1, line:Landroid/widget/TextView;
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 157
    return-object p0
.end method

.method public setMessage(Landroid/text/SpannableString;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 4
    .parameter "message"

    .prologue
    .line 182
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v3, 0x7f0d0026

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 183
    .local v1, title:Landroid/widget/TextView;
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v3, 0x7f0d0025

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 184
    .local v0, scroll:Landroid/widget/ScrollView;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 185
    new-instance v2, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v2}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 186
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 4
    .parameter "message"

    .prologue
    .line 174
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v3, 0x7f0d0026

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 175
    .local v1, title:Landroid/widget/TextView;
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v3, 0x7f0d0025

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 176
    .local v0, scroll:Landroid/widget/ScrollView;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 177
    new-instance v2, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v2}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 178
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    return-object p0
.end method

.method public setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 3
    .parameter "text"
    .parameter "click_listner"

    .prologue
    .line 252
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v2, 0x7f0d0008

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 253
    .local v0, button:Landroid/widget/Button;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 254
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 255
    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$8;

    invoke-direct {v1, p0, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$8;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 262
    return-object p0
.end method

.method public setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 3
    .parameter "text"
    .parameter "click_listner"

    .prologue
    .line 215
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v2, 0x7f0d0008

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 216
    .local v0, button:Landroid/widget/Button;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 217
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 218
    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$5;

    invoke-direct {v1, p0, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    return-object p0
.end method

.method public setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 3
    .parameter "text"
    .parameter "click_listner"

    .prologue
    .line 240
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v2, 0x7f0d0029

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 241
    .local v0, button:Landroid/widget/Button;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 242
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 243
    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$7;

    invoke-direct {v1, p0, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    return-object p0
.end method

.method public setNeutralButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 3
    .parameter "text"
    .parameter "click_listner"

    .prologue
    .line 203
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v2, 0x7f0d0029

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 204
    .local v0, button:Landroid/widget/Button;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 205
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 206
    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$4;

    invoke-direct {v1, p0, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    return-object p0
.end method

.method public setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 1
    .parameter "cancel_listner"

    .prologue
    .line 264
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 265
    return-object p0
.end method

.method public setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 3
    .parameter "text"
    .parameter "click_listner"

    .prologue
    .line 227
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v2, 0x7f0d0007

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 228
    .local v0, button:Landroid/widget/Button;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 229
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 230
    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$6;

    invoke-direct {v1, p0, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    return-object p0
.end method

.method public setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 3
    .parameter "text"
    .parameter "click_listner"

    .prologue
    .line 191
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v2, 0x7f0d0007

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 192
    .local v0, button:Landroid/widget/Button;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 193
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 194
    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$3;

    invoke-direct {v1, p0, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    return-object p0
.end method

.method public setTitle(I)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 4
    .parameter "str"

    .prologue
    .line 168
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v3, 0x7f0d0020

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 169
    .local v0, linear:Landroid/widget/LinearLayout;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 170
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v3, 0x7f0d0022

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 171
    .local v1, title:Landroid/widget/TextView;
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(I)V

    .line 172
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 6
    .parameter "str"

    .prologue
    const/4 v5, 0x0

    .line 159
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v4, 0x7f0d0020

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 160
    .local v1, linear:Landroid/widget/LinearLayout;
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 161
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v4, 0x7f0d0022

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 162
    .local v2, title:Landroid/widget/TextView;
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v4, 0x7f0d0023

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 165
    .local v0, line:Landroid/widget/TextView;
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 166
    return-object p0
.end method

.method public setView(Landroid/view/View;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    .locals 4
    .parameter "view"

    .prologue
    .line 143
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v3, 0x7f0d0024

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 144
    .local v1, linear:Landroid/widget/LinearLayout;
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->dialog:Landroid/app/Dialog;

    const v3, 0x7f0d0028

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 146
    .local v0, button_layout:Landroid/widget/LinearLayout;
    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 147
    return-object p0
.end method
