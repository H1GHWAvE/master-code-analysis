.class public Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;
.super Landroid/app/Activity;
.source "BinderWidgetConfigureActivity.java"


# static fields
.field private static final PREFS_NAME:Ljava/lang/String; = "com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget"

.field private static final PREF_PREFIX_KEY:Ljava/lang/String; = "appwidget_"


# instance fields
.field public bindes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;",
            ">;"
        }
    .end annotation
.end field

.field public context:Landroid/content/Context;

.field public lv:Landroid/widget/ListView;

.field mAppWidgetId:I

.field mAppWidgetText:Landroid/widget/EditText;

.field public sizeText:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 34
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->mAppWidgetId:I

    .line 39
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->bindes:Ljava/util/ArrayList;

    .line 40
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->lv:Landroid/widget/ListView;

    .line 41
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->sizeText:I

    .line 46
    return-void
.end method

.method static deleteTitlePref(Landroid/content/Context;I)V
    .locals 3
    .parameter "context"
    .parameter "appWidgetId"

    .prologue
    .line 181
    const-string v1, "com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 182
    .local v0, prefs:Landroid/content/SharedPreferences$Editor;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "appwidget_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 183
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 184
    return-void
.end method

.method static loadTitlePref(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4
    .parameter "context"
    .parameter "appWidgetId"

    .prologue
    .line 171
    const-string v2, "com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget"

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 172
    .local v0, prefs:Landroid/content/SharedPreferences;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "appwidget_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 173
    .local v1, titleValue:Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 176
    .end local v1           #titleValue:Ljava/lang/String;
    :goto_0
    return-object v1

    .restart local v1       #titleValue:Ljava/lang/String;
    :cond_0
    const-string v1, "NOT_SAVED_BIND"

    goto :goto_0
.end method

.method static saveTitlePref(Landroid/content/Context;ILjava/lang/String;)V
    .locals 3
    .parameter "context"
    .parameter "appWidgetId"
    .parameter "text"

    .prologue
    .line 163
    const-string v1, "com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 164
    .local v0, prefs:Landroid/content/SharedPreferences$Editor;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "appwidget_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 165
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 166
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .parameter "icicle"

    .prologue
    const/4 v8, 0x0

    .line 50
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->setResult(I)V

    .line 58
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 59
    .local v3, intent:Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 60
    .local v2, extras:Landroid/os/Bundle;
    if-eqz v2, :cond_0

    .line 61
    const-string v4, "appWidgetId"

    invoke-virtual {v2, v4, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->mAppWidgetId:I

    .line 66
    :cond_0
    iget v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->mAppWidgetId:I

    if-nez v4, :cond_1

    .line 67
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->finish()V

    .line 157
    :goto_0
    return-void

    .line 73
    :cond_1
    iput-object p0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->context:Landroid/content/Context;

    .line 75
    new-instance v0, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "binder"

    invoke-virtual {p0, v5, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/bind.txt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 77
    .local v0, bindfile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 80
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->getBindes(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->bindes:Ljava/util/ArrayList;

    .line 91
    new-instance v4, Landroid/widget/ListView;

    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->context:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->lv:Landroid/widget/ListView;

    .line 92
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->bindes:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_3

    .line 93
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->bindes:Ljava/util/ArrayList;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;

    const-string v6, "empty"

    const-string v7, "empty"

    invoke-direct {v5, v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_3
    new-instance v4, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$1;

    const v5, 0x7f04001d

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->bindes:Ljava/util/ArrayList;

    invoke-direct {v4, p0, p0, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;Landroid/content/Context;ILjava/util/List;)V

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adaptBind:Landroid/widget/ArrayAdapter;

    .line 125
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->lv:Landroid/widget/ListView;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adaptBind:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 126
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->lv:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->invalidateViews()V

    .line 128
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->lv:Landroid/widget/ListView;

    const/high16 v5, -0x100

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 129
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->bindes:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->bindes:Ljava/util/ArrayList;

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v5, "empty"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->bindes:Ljava/util/ArrayList;

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    const-string v5, "empty"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 155
    :goto_2
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->lv:Landroid/widget/ListView;

    invoke-virtual {p0, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->setContentView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 81
    :catch_0
    move-exception v1

    .line 83
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 132
    .end local v1           #e:Ljava/io/IOException;
    :cond_4
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->lv:Landroid/widget/ListView;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$2;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_2
.end method
