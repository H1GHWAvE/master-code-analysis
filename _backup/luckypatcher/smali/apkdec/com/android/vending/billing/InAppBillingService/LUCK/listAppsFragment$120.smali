.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;
.super Landroid/widget/ArrayAdapter;
.source "listAppsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getComponents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/Components;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .parameter "this$0"
    .parameter "x0"
    .parameter "x1"
    .parameter

    .prologue
    .line 14726
    .local p4, x2:Ljava/util/List;,"Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/Components;>;"
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v9, 0x0

    const v10, 0x7f0701b9

    .line 14732
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v7

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 14733
    .local v2, inflater:Landroid/view/LayoutInflater;
    const v7, 0x7f04001d

    invoke-virtual {v2, v7, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 14734
    .local v6, view:Landroid/view/View;
    const v7, 0x7f0d0039

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 14736
    .local v5, textView:Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v8

    invoke-virtual {v5, v7, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 14737
    const/high16 v7, 0x40a0

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v7, v8

    const/high16 v8, 0x3f00

    add-float/2addr v7, v8

    float-to-int v0, v7

    .line 14738
    .local v0, dp5:I
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 14739
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->header:Z

    if-eqz v7, :cond_1

    .line 14740
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v4, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    .line 14742
    .local v4, str2:Ljava/lang/String;
    const-string v7, "#ffffffff"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14743
    const v7, -0xbbbbbc

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundColor(I)V

    .line 14825
    .end local v4           #str2:Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v6

    .line 14747
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->activity:Z

    if-eqz v7, :cond_3

    .line 14748
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 14750
    .restart local v4       #str2:Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Status:Z

    if-eqz v7, :cond_7

    .line 14751
    const-string v7, "#ff00ffff"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14752
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/Utils;->isAds(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 14753
    const-string v7, "#ffffff00"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14759
    :cond_2
    :goto_1
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/Utils;->isAds(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 14760
    const v7, 0x7f070008

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 14763
    :goto_2
    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 14765
    .end local v4           #str2:Ljava/lang/String;
    :cond_3
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->permission:Z

    if-eqz v7, :cond_4

    .line 14766
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 14768
    .restart local v4       #str2:Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Status:Z

    if-eqz v7, :cond_9

    .line 14769
    const-string v7, "#ff00ff00"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14775
    :goto_3
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 14777
    .local v3, pm:Landroid/content/pm/PackageManager;
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    const-string v8, "chelpa_per_"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "chelpus_"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/content/pm/PackageManager;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/content/pm/PermissionInfo;->loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 14779
    if-nez v4, :cond_a

    .line 14780
    const v7, 0x7f0701b9

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 14781
    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 14795
    .end local v3           #pm:Landroid/content/pm/PackageManager;
    .end local v4           #str2:Ljava/lang/String;
    :cond_4
    :goto_4
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->service:Z

    if-eqz v7, :cond_5

    .line 14796
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 14798
    .restart local v4       #str2:Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Status:Z

    if-eqz v7, :cond_b

    .line 14799
    const-string v7, "#ff00ff00"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14805
    .end local v4           #str2:Ljava/lang/String;
    :cond_5
    :goto_5
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->reciver:Z

    if-eqz v7, :cond_6

    .line 14806
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 14808
    .restart local v4       #str2:Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Status:Z

    if-eqz v7, :cond_c

    .line 14809
    const-string v7, "#ff00ffff"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14814
    .end local v4           #str2:Ljava/lang/String;
    :cond_6
    :goto_6
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->provider:Z

    if-eqz v7, :cond_0

    .line 14815
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 14817
    .restart local v4       #str2:Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$120;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Status:Z

    if-eqz v7, :cond_d

    .line 14818
    const-string v7, "#ff00ff00"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 14756
    :cond_7
    const-string v7, "#ffff0000"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 14761
    :cond_8
    const v7, 0x7f070009

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    .line 14771
    :cond_9
    const-string v7, "#ffff0000"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 14783
    .restart local v3       #pm:Landroid/content/pm/PackageManager;
    :cond_a
    :try_start_1
    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_4

    .line 14785
    :catch_0
    move-exception v1

    .line 14787
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-static {v10}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 14788
    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 14789
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v1

    .line 14790
    .local v1, e:Ljava/lang/NullPointerException;
    invoke-static {v10}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 14791
    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 14802
    .end local v1           #e:Ljava/lang/NullPointerException;
    .end local v3           #pm:Landroid/content/pm/PackageManager;
    :cond_b
    const-string v7, "#ffff0000"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 14812
    :cond_c
    const-string v7, "#ffff0000"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 14821
    :cond_d
    const-string v7, "#ffff0000"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
