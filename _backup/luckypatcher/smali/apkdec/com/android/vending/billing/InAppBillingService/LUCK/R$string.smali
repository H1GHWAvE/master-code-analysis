.class public final Lcom/android/vending/billing/InAppBillingService/LUCK/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final AppTools:I = 0x7f070000

.field public static final P_not_found:I = 0x7f070001

.field public static final PatchResult:I = 0x7f070002

.field public static final Yes:I = 0x7f070003

.field public static final about_working_dir:I = 0x7f070004

.field public static final aboutlp:I = 0x7f070005

.field public static final aboutmenu:I = 0x7f070006

.field public static final abouttitle:I = 0x7f070007

.field public static final activity_ads_descr:I = 0x7f070008

.field public static final activity_descr:I = 0x7f070009

.field public static final add_widget:I = 0x7f07025d

.field public static final additinfo:I = 0x7f07000a

.field public static final adsdescr:I = 0x7f07000b

.field public static final adstitle:I = 0x7f07000c

.field public static final advanced_menu:I = 0x7f07000d

.field public static final all_custom_patches_updated:I = 0x7f07000e

.field public static final allchanges:I = 0x7f07000f

.field public static final android_patches_for_bootlist:I = 0x7f07023a

.field public static final android_patches_for_bootlist_status:I = 0x7f07023b

.field public static final apk_create_option:I = 0x7f070010

.field public static final apk_not_found_error:I = 0x7f070011

.field public static final apk_size:I = 0x7f070012

.field public static final apkdescr:I = 0x7f070013

.field public static final apklabel:I = 0x7f070014

.field public static final apkpackage:I = 0x7f070015

.field public static final app_dialog_no_root_app_control:I = 0x7f070016

.field public static final app_disabler_widget_pkg_not_found:I = 0x7f07023c

.field public static final app_info:I = 0x7f070017

.field public static final app_install_success:I = 0x7f070018

.field public static final app_name:I = 0x7f070019

.field public static final app_not_found:I = 0x7f07001a

.field public static final app_not_found_adapter:I = 0x7f07023d

.field public static final apps_not_found:I = 0x7f07023e

.field public static final appwidget_text:I = 0x7f07025e

.field public static final back:I = 0x7f07001b

.field public static final backbutton:I = 0x7f07001c

.field public static final backup_data_empty:I = 0x7f07001d

.field public static final backup_data_error:I = 0x7f07001e

.field public static final backup_data_success:I = 0x7f07001f

.field public static final backup_delete_message:I = 0x7f070020

.field public static final backup_done:I = 0x7f070021

.field public static final backups_not_found:I = 0x7f070022

.field public static final begin_changelog:I = 0x7f070023

.field public static final billing_buy:I = 0x7f070024

.field public static final billing_hack:I = 0x7f070025

.field public static final billing_hack2_auto_repeat_note:I = 0x7f070026

.field public static final billing_hack2_new:I = 0x7f070027

.field public static final billing_hack_menu:I = 0x7f070028

.field public static final billing_hack_menu_descr:I = 0x7f070029

.field public static final billing_hack_menu_disable:I = 0x7f07002a

.field public static final billing_hack_menu_enable:I = 0x7f07002b

.field public static final billing_hack_option:I = 0x7f07002c

.field public static final billing_hack_option2:I = 0x7f07002d

.field public static final bind_button:I = 0x7f07002e

.field public static final bind_delete:I = 0x7f07002f

.field public static final bind_dialog_button:I = 0x7f070030

.field public static final bind_dialog_source:I = 0x7f070031

.field public static final bind_dialog_target:I = 0x7f070032

.field public static final bind_error:I = 0x7f070033

.field public static final bind_source:I = 0x7f070034

.field public static final bind_target:I = 0x7f070035

.field public static final binder_browser:I = 0x7f070036

.field public static final binder_browser_create_dir:I = 0x7f070037

.field public static final binder_browser_current_dir:I = 0x7f070038

.field public static final binder_browser_select:I = 0x7f070039

.field public static final binder_delete:I = 0x7f07003a

.field public static final bootview:I = 0x7f07003b

.field public static final busybox_not_found:I = 0x7f07003c

.field public static final button_advanced_filter:I = 0x7f07003d

.field public static final button_block_internet:I = 0x7f07023f

.field public static final button_for_delete:I = 0x7f07003e

.field public static final button_integrate_update_apps:I = 0x7f070240

.field public static final button_move_to_internal:I = 0x7f070241

.field public static final button_move_to_sdcard:I = 0x7f07003f

.field public static final button_unblock_internet:I = 0x7f070242

.field public static final button_uninstall_apps:I = 0x7f070243

.field public static final cancel:I = 0x7f070040

.field public static final change_icon_lp:I = 0x7f070041

.field public static final change_icon_lp_descr:I = 0x7f070042

.field public static final changelog:I = 0x7f070043

.field public static final cleardalvik:I = 0x7f070044

.field public static final cleardata:I = 0x7f070045

.field public static final cloneApplication:I = 0x7f070046

.field public static final collect_logs:I = 0x7f070047

.field public static final configure:I = 0x7f07025f

.field public static final context_automodeslvl:I = 0x7f070048

.field public static final context_backup_apk:I = 0x7f070049

.field public static final context_backup_data:I = 0x7f07004a

.field public static final context_batch_operations:I = 0x7f07004b

.field public static final context_block_internet_selected_apps:I = 0x7f070244

.field public static final context_blockads:I = 0x7f07004c

.field public static final context_change_components_menu:I = 0x7f07004d

.field public static final context_clearhosts:I = 0x7f07004e

.field public static final context_crt_apk_permission:I = 0x7f07004f

.field public static final context_dependencies:I = 0x7f070050

.field public static final context_dexopt_app:I = 0x7f070051

.field public static final context_disableActivity:I = 0x7f070052

.field public static final context_disableComponents:I = 0x7f070053

.field public static final context_disable_google_ads:I = 0x7f070054

.field public static final context_disable_package:I = 0x7f070055

.field public static final context_enable_google_ads:I = 0x7f070056

.field public static final context_enable_package:I = 0x7f070057

.field public static final context_hard_apk_permission:I = 0x7f070058

.field public static final context_hide_lp:I = 0x7f070059

.field public static final context_hide_lp_descr:I = 0x7f07005a

.field public static final context_install:I = 0x7f07005b

.field public static final context_install_as_system:I = 0x7f07005c

.field public static final context_integrate_update_selected_apps:I = 0x7f07005d

.field public static final context_move_selected_apps_to_internal:I = 0x7f07005e

.field public static final context_move_selected_apps_to_sdcard:I = 0x7f07005f

.field public static final context_patch_framework:I = 0x7f070245

.field public static final context_rebuild:I = 0x7f070060

.field public static final context_removeAds:I = 0x7f070061

.field public static final context_remove_saved_purchaces:I = 0x7f070062

.field public static final context_restore_apk:I = 0x7f070063

.field public static final context_restore_data:I = 0x7f070064

.field public static final context_safe_apk_permission:I = 0x7f070065

.field public static final context_safe_sign_apk_permission:I = 0x7f070066

.field public static final context_support_patch:I = 0x7f070067

.field public static final context_tools:I = 0x7f070068

.field public static final context_unblock_internet_selected_apps:I = 0x7f070246

.field public static final context_unblockads:I = 0x7f070069

.field public static final context_uninstall_if_installed:I = 0x7f07006a

.field public static final context_uninstall_selected_apps:I = 0x7f07006b

.field public static final contextads:I = 0x7f07006c

.field public static final contextbackup:I = 0x7f07006d

.field public static final contextblockfileads:I = 0x7f07006e

.field public static final contextblockfileads_descr:I = 0x7f07006f

.field public static final contextboot:I = 0x7f070070

.field public static final contextbootads:I = 0x7f070071

.field public static final contextbootcustom:I = 0x7f070072

.field public static final contextbootlvl:I = 0x7f070073

.field public static final contextcorepatch1:I = 0x7f070074

.field public static final contextcorepatch1_descr:I = 0x7f070075

.field public static final contextcorepatch2:I = 0x7f070076

.field public static final contextcorepatch2_descr:I = 0x7f070077

.field public static final contextcorepatch3:I = 0x7f070078

.field public static final contextcorepatch3_descr:I = 0x7f070079

.field public static final contextcorepatch3_descr_plus:I = 0x7f07007a

.field public static final contextcorepatch4:I = 0x7f07007b

.field public static final contextcorepatch4_descr:I = 0x7f07007c

.field public static final contextcorepatch4_descr_xposed:I = 0x7f07007d

.field public static final contextcorepatch4_note:I = 0x7f07007e

.field public static final contextcorepatchrestore:I = 0x7f07007f

.field public static final contextcorepatchrestore2:I = 0x7f070080

.field public static final contextcorepatchrestore2_descr:I = 0x7f070081

.field public static final contextcorepatchrestore_descr:I = 0x7f070082

.field public static final contextcustompatch:I = 0x7f070083

.field public static final contextdeodex:I = 0x7f070084

.field public static final contextextpatch:I = 0x7f070085

.field public static final contextfix:I = 0x7f070086

.field public static final contextfulloffline:I = 0x7f070087

.field public static final contextfulloffline_descr:I = 0x7f070088

.field public static final contextignore:I = 0x7f070089

.field public static final contextlaunchapp:I = 0x7f07008a

.field public static final contextlivepatch:I = 0x7f07008b

.field public static final contextodex:I = 0x7f07008c

.field public static final contextpatt1:I = 0x7f07008d

.field public static final contextpatt1_descr:I = 0x7f07008e

.field public static final contextpatt2:I = 0x7f07008f

.field public static final contextpatt2_descr:I = 0x7f070090

.field public static final contextpatt3:I = 0x7f070091

.field public static final contextpatt3_descr:I = 0x7f070092

.field public static final contextpatt4:I = 0x7f070093

.field public static final contextpatt4_descr:I = 0x7f070094

.field public static final contextpatt5:I = 0x7f070095

.field public static final contextpatt5_descr:I = 0x7f070096

.field public static final contextpatt6:I = 0x7f070097

.field public static final contextpatt6_descr:I = 0x7f070098

.field public static final contextpattads1:I = 0x7f070099

.field public static final contextpattads1_descr:I = 0x7f07009a

.field public static final contextpattads2:I = 0x7f07009b

.field public static final contextpattads2_descr:I = 0x7f07009c

.field public static final contextpattads3:I = 0x7f07009d

.field public static final contextpattads3_descr:I = 0x7f07009e

.field public static final contextpattads4:I = 0x7f07009f

.field public static final contextpattads4_descr:I = 0x7f0700a0

.field public static final contextpattads5:I = 0x7f0700a1

.field public static final contextpattads5_descr:I = 0x7f0700a2

.field public static final contextpattads6:I = 0x7f0700a3

.field public static final contextpattads6_descr:I = 0x7f0700a4

.field public static final contextpattamazon:I = 0x7f0700a5

.field public static final contextpattamazon_descr:I = 0x7f0700a6

.field public static final contextpattdependencies_descr:I = 0x7f0700a7

.field public static final contextpattlvl1:I = 0x7f0700a8

.field public static final contextpattlvl1_descr:I = 0x7f0700a9

.field public static final contextpattlvl2:I = 0x7f0700aa

.field public static final contextpattlvl2_descr:I = 0x7f0700ab

.field public static final contextpattlvl3:I = 0x7f0700ac

.field public static final contextpattlvl3_descr:I = 0x7f0700ad

.field public static final contextpattlvl4:I = 0x7f0700ae

.field public static final contextpattlvl4_descr:I = 0x7f0700af

.field public static final contextpattlvl5:I = 0x7f0700b0

.field public static final contextpattlvl5_descr:I = 0x7f0700b1

.field public static final contextpattlvl6:I = 0x7f0700b2

.field public static final contextpattlvl6_descr:I = 0x7f0700b3

.field public static final contextpattsamsung:I = 0x7f0700b4

.field public static final contextpattsamsung_descr:I = 0x7f0700b5

.field public static final contextrestore:I = 0x7f0700b6

.field public static final contextselpatt:I = 0x7f0700b7

.field public static final copyright:I = 0x7f0700b8

.field public static final core_error_dalvik_cache_not_found_for_core_jar:I = 0x7f0700b9

.field public static final core_error_dalvik_cache_not_found_for_services_jar:I = 0x7f0700ba

.field public static final core_only_dalvik_cache_end:I = 0x7f070247

.field public static final core_only_dalvik_cache_finished:I = 0x7f070248

.field public static final core_option:I = 0x7f0700bb

.field public static final core_patch_apply:I = 0x7f0700bc

.field public static final core_patch_nopatch:I = 0x7f0700bd

.field public static final core_patch_nopatch_no_space1:I = 0x7f0700be

.field public static final core_result_checkbox:I = 0x7f070249

.field public static final corepatches:I = 0x7f0700bf

.field public static final corrupt_download:I = 0x7f0700c0

.field public static final create:I = 0x7f0700c1

.field public static final create_clone_app:I = 0x7f0700c2

.field public static final create_dir_error:I = 0x7f0700c3

.field public static final create_dir_error_2:I = 0x7f0700c4

.field public static final create_lic_done:I = 0x7f0700c5

.field public static final create_perm_ok:I = 0x7f0700c6

.field public static final createapk:I = 0x7f0700c7

.field public static final createapkads:I = 0x7f0700c8

.field public static final createapkcustom:I = 0x7f0700c9

.field public static final createapklvl:I = 0x7f0700ca

.field public static final createapklvlautoinverse:I = 0x7f0700cb

.field public static final createapklvlextreme:I = 0x7f0700cc

.field public static final createapksupport:I = 0x7f0700cd

.field public static final createdialog1:I = 0x7f0700ce

.field public static final createdialog1_clone:I = 0x7f0700cf

.field public static final createdialog2:I = 0x7f0700d0

.field public static final createdialog3:I = 0x7f0700d1

.field public static final createdialog3_clone:I = 0x7f0700d2

.field public static final createdialog4:I = 0x7f0700d3

.field public static final createdialog5:I = 0x7f0700d4

.field public static final custom_descript:I = 0x7f0700d5

.field public static final custom_patches_update_not_found:I = 0x7f0700d6

.field public static final custom_sel_app:I = 0x7f0700d7

.field public static final customdescr:I = 0x7f0700d8

.field public static final customtitle:I = 0x7f0700d9

.field public static final dalvik_cache_size:I = 0x7f0700da

.field public static final data_dir_size:I = 0x7f0700db

.field public static final days_on_up:I = 0x7f0700dc

.field public static final days_on_up_descr:I = 0x7f0700dd

.field public static final default_target:I = 0x7f0700de

.field public static final delboot:I = 0x7f0700df

.field public static final delete_file:I = 0x7f0700e0

.field public static final delete_file_message:I = 0x7f0700e1

.field public static final dialog_system:I = 0x7f0700e2

.field public static final dir_change:I = 0x7f0700e3

.field public static final dir_change_descr:I = 0x7f0700e4

.field public static final dirbinder:I = 0x7f0700e5

.field public static final dirr_chg:I = 0x7f0700e6

.field public static final disable_autoupdate:I = 0x7f0700e7

.field public static final disable_autoupdate_descr:I = 0x7f0700e8

.field public static final disable_inapp_services_google:I = 0x7f0700e9

.field public static final disable_inapp_services_google_descr:I = 0x7f0700ea

.field public static final disable_license_services_google:I = 0x7f0700eb

.field public static final disable_license_services_google_descr:I = 0x7f0700ec

.field public static final disabled_package:I = 0x7f0700ed

.field public static final dontusepatch:I = 0x7f0700ee

.field public static final download:I = 0x7f0700ef

.field public static final empty:I = 0x7f0700f0

.field public static final empty_binders:I = 0x7f0700f1

.field public static final enabled_package:I = 0x7f0700f2

.field public static final enter_name_dir:I = 0x7f0700f3

.field public static final equals_version:I = 0x7f0700f4

.field public static final error:I = 0x7f07024a

.field public static final error_apk_to_system:I = 0x7f0700f5

.field public static final error_app_installed_on_sdcard:I = 0x7f0700f6

.field public static final error_app_installed_on_sdcard_block:I = 0x7f0700f7

.field public static final error_backup_apk:I = 0x7f0700f8

.field public static final error_classes_not_found:I = 0x7f07024b

.field public static final error_collect_logs:I = 0x7f0700f9

.field public static final error_create_oat:I = 0x7f0700fa

.field public static final error_detect:I = 0x7f0700fb

.field public static final error_get_pkgInfo:I = 0x7f0700fc

.field public static final error_launch:I = 0x7f0700fd

.field public static final error_lenghts_patterns:I = 0x7f0700fe

.field public static final error_message_for_share_app:I = 0x7f07024c

.field public static final error_not_found_lic_data:I = 0x7f0700ff

.field public static final error_not_found_storage_lic:I = 0x7f070100

.field public static final error_odex_app:I = 0x7f070101

.field public static final error_odex_app_impossible:I = 0x7f07024d

.field public static final error_original_pattern:I = 0x7f070102

.field public static final error_out_of_memory:I = 0x7f070103

.field public static final error_path:I = 0x7f070104

.field public static final error_path_exists:I = 0x7f070105

.field public static final error_replace_pattern:I = 0x7f070106

.field public static final error_restore:I = 0x7f070107

.field public static final extpat1:I = 0x7f070108

.field public static final extpat2:I = 0x7f070109

.field public static final extpat3:I = 0x7f07010a

.field public static final extres1:I = 0x7f07010b

.field public static final extres2:I = 0x7f07010c

.field public static final fast_start:I = 0x7f07010d

.field public static final fast_start_descr:I = 0x7f07010e

.field public static final file_not_deleted:I = 0x7f07024e

.field public static final file_not_found:I = 0x7f07010f

.field public static final filter:I = 0x7f070110

.field public static final filter0:I = 0x7f070111

.field public static final filter1:I = 0x7f070112

.field public static final filter10:I = 0x7f070113

.field public static final filter11:I = 0x7f070114

.field public static final filter12:I = 0x7f070115

.field public static final filter13:I = 0x7f070116

.field public static final filter2:I = 0x7f070117

.field public static final filter3:I = 0x7f070118

.field public static final filter4:I = 0x7f070119

.field public static final filter5:I = 0x7f07011a

.field public static final filter6:I = 0x7f07011b

.field public static final filter7:I = 0x7f07011c

.field public static final filter8:I = 0x7f07011d

.field public static final filter9:I = 0x7f07011e

.field public static final filterdescr:I = 0x7f07011f

.field public static final final_market_message:I = 0x7f070120

.field public static final fixbutton:I = 0x7f070121

.field public static final fixeddescr:I = 0x7f070122

.field public static final fixedtitle:I = 0x7f070123

.field public static final framework_file_patched:I = 0x7f07024f

.field public static final framework_limit:I = 0x7f070250

.field public static final framework_patch_not_result:I = 0x7f070251

.field public static final go_to_file_path:I = 0x7f070124

.field public static final goodresult:I = 0x7f070125

.field public static final have_luck:I = 0x7f070126

.field public static final header_activities:I = 0x7f070127

.field public static final header_providers:I = 0x7f070128

.field public static final header_receivers:I = 0x7f070129

.field public static final header_services:I = 0x7f07012a

.field public static final help:I = 0x7f07012b

.field public static final help_common:I = 0x7f07012c

.field public static final help_custom:I = 0x7f07012d

.field public static final hide_notify:I = 0x7f07012e

.field public static final hide_notify_descr:I = 0x7f07012f

.field public static final hide_title_app:I = 0x7f070130

.field public static final hide_title_app_descr:I = 0x7f070131

.field public static final host_down:I = 0x7f070132

.field public static final hosts_default:I = 0x7f070133

.field public static final hosts_not_found_changes:I = 0x7f070134

.field public static final hosts_updated:I = 0x7f070135

.field public static final inapp_emulation_auto_repeat:I = 0x7f070136

.field public static final information:I = 0x7f070137

.field public static final install:I = 0x7f070138

.field public static final install_as_user_app:I = 0x7f070139

.field public static final install_as_user_app_note:I = 0x7f07013a

.field public static final install_cloned_app:I = 0x7f07013b

.field public static final install_date:I = 0x7f07013c

.field public static final installsupersu:I = 0x7f07013d

.field public static final integrate_to_system:I = 0x7f07013e

.field public static final integrate_updates_result:I = 0x7f07013f

.field public static final integrate_updates_to_system:I = 0x7f070140

.field public static final internet_not_found:I = 0x7f070141

.field public static final is_an_invalid_number:I = 0x7f070142

.field public static final langdef:I = 0x7f070143

.field public static final langen:I = 0x7f070144

.field public static final langmenu:I = 0x7f070145

.field public static final launchbutton:I = 0x7f070146

.field public static final licensing_hack_menu:I = 0x7f070147

.field public static final licensing_hack_menu_descr:I = 0x7f070148

.field public static final licensing_hack_menu_disable:I = 0x7f070149

.field public static final licensing_hack_menu_enable:I = 0x7f07014a

.field public static final line:I = 0x7f070260

.field public static final loadpkg:I = 0x7f07014b

.field public static final lp_billing_hack_title:I = 0x7f07014c

.field public static final lvldescr:I = 0x7f07014d

.field public static final lvltitle:I = 0x7f07014e

.field public static final market_description_text:I = 0x7f07014f

.field public static final market_install:I = 0x7f070150

.field public static final market_version1:I = 0x7f070151

.field public static final market_version2:I = 0x7f070152

.field public static final market_version3:I = 0x7f070153

.field public static final market_version4:I = 0x7f070154

.field public static final market_version5:I = 0x7f070155

.field public static final market_version6:I = 0x7f070156

.field public static final menu_download_custom_patches:I = 0x7f070157

.field public static final messageAndroidManifest:I = 0x7f070158

.field public static final messageODEX1:I = 0x7f070159

.field public static final messagePath:I = 0x7f07015a

.field public static final messageSD:I = 0x7f07015b

.field public static final message_allow_non_market_app:I = 0x7f07015c

.field public static final message_clear_all_dalvik_cache:I = 0x7f07015d

.field public static final message_clear_data:I = 0x7f07015e

.field public static final message_delete_odex_file:I = 0x7f07015f

.field public static final message_delete_package_new:I = 0x7f070252

.field public static final message_done_move_to_internal:I = 0x7f070160

.field public static final message_done_move_to_sdcard:I = 0x7f070161

.field public static final message_error_move_to_internal:I = 0x7f070162

.field public static final message_error_move_to_sdcard:I = 0x7f070163

.field public static final message_exit:I = 0x7f070164

.field public static final message_reboot:I = 0x7f070165

.field public static final message_remove_all_odex:I = 0x7f070166

.field public static final message_warning_InApp_service_disable:I = 0x7f070167

.field public static final message_warning_LVL_emulation_service_enable:I = 0x7f070168

.field public static final message_warning_LVL_service_disable:I = 0x7f070169

.field public static final mod_market_check:I = 0x7f07016a

.field public static final mod_market_check_failed:I = 0x7f07016b

.field public static final mod_market_check_internet_off:I = 0x7f07016c

.field public static final mod_market_check_title:I = 0x7f07016d

.field public static final mod_market_check_true:I = 0x7f07016e

.field public static final modifdescr:I = 0x7f07016f

.field public static final modiftitle:I = 0x7f070170

.field public static final move_data:I = 0x7f070171

.field public static final move_to_internal:I = 0x7f070172

.field public static final move_to_internal_message:I = 0x7f070173

.field public static final move_to_sdcard:I = 0x7f070174

.field public static final move_to_sdcard_message:I = 0x7f070175

.field public static final move_to_sys:I = 0x7f070176

.field public static final move_to_system:I = 0x7f070177

.field public static final mpatcher_help:I = 0x7f070178

.field public static final new_method_lvl:I = 0x7f070179

.field public static final new_version:I = 0x7f07017a

.field public static final no:I = 0x7f07017b

.field public static final no_freespace_patch:I = 0x7f07017c

.field public static final no_icon:I = 0x7f07017d

.field public static final no_icon_descr:I = 0x7f07017e

.field public static final no_root:I = 0x7f07017f

.field public static final no_site:I = 0x7f070180

.field public static final no_space:I = 0x7f070181

.field public static final no_space_in_data_all:I = 0x7f070182

.field public static final no_space_in_system:I = 0x7f070183

.field public static final no_space_in_system_all:I = 0x7f070184

.field public static final nofilter:I = 0x7f070185

.field public static final nonedescr:I = 0x7f070186

.field public static final nonetitle:I = 0x7f070187

.field public static final not_detect:I = 0x7f070188

.field public static final not_found_changelog:I = 0x7f070189

.field public static final notify_android_patch_on_boot:I = 0x7f07018a

.field public static final notify_android_patch_on_boot_mes_core_jar:I = 0x7f070253

.field public static final notify_android_patch_on_boot_mes_services_jar:I = 0x7f070254

.field public static final notify_directory_binder:I = 0x7f07018b

.field public static final notify_directory_binder_from:I = 0x7f07018c

.field public static final notify_directory_binder_to:I = 0x7f07018d

.field public static final notify_patch_on_boot:I = 0x7f07018e

.field public static final notify_patched:I = 0x7f07018f

.field public static final odex_all_system_app:I = 0x7f070190

.field public static final odex_all_system_app_message:I = 0x7f070191

.field public static final odexed_not_all_apps:I = 0x7f070192

.field public static final ok:I = 0x7f070193

.field public static final on_top_apps_marker:I = 0x7f070194

.field public static final or_hex_str:I = 0x7f070195

.field public static final orientmenu:I = 0x7f070196

.field public static final others:I = 0x7f070197

.field public static final patch_ok:I = 0x7f070198

.field public static final patch_progress_data_parse:I = 0x7f070199

.field public static final patch_progress_get_classes:I = 0x7f07019a

.field public static final patch_progress_strings_analisis:I = 0x7f07019b

.field public static final patch_step1:I = 0x7f07019c

.field public static final patch_step2:I = 0x7f07019d

.field public static final patch_step3:I = 0x7f07019e

.field public static final patch_step4:I = 0x7f07019f

.field public static final patch_step6:I = 0x7f0701a0

.field public static final patch_step_http:I = 0x7f0701a1

.field public static final patch_to_Android_At_once:I = 0x7f0701a2

.field public static final patchbutton:I = 0x7f0701a3

.field public static final pattern1:I = 0x7f0701a4

.field public static final pattern1_f:I = 0x7f0701a5

.field public static final pattern2:I = 0x7f0701a6

.field public static final pattern2_f:I = 0x7f0701a7

.field public static final pattern3:I = 0x7f0701a8

.field public static final pattern3_f:I = 0x7f0701a9

.field public static final pattern4:I = 0x7f0701aa

.field public static final pattern4_f:I = 0x7f0701ab

.field public static final pattern5:I = 0x7f0701ac

.field public static final pattern5_f:I = 0x7f0701ad

.field public static final pattern6:I = 0x7f0701ae

.field public static final pattern6_f:I = 0x7f0701af

.field public static final pattern7:I = 0x7f0701b0

.field public static final pattern7_f:I = 0x7f0701b1

.field public static final patternamazon:I = 0x7f0701b2

.field public static final patternamazon_f:I = 0x7f0701b3

.field public static final patterndependencies:I = 0x7f0701b4

.field public static final patternsamsung:I = 0x7f0701b5

.field public static final patternsamsung_f:I = 0x7f0701b6

.field public static final perm_ok:I = 0x7f0701b7

.field public static final permission:I = 0x7f0701b8

.field public static final permission_not_descr:I = 0x7f0701b9

.field public static final permission_not_found:I = 0x7f0701ba

.field public static final permissions:I = 0x7f0701bb

.field public static final poorresult:I = 0x7f0701bc

.field public static final proxyGP:I = 0x7f070261

.field public static final proxyGP_descr:I = 0x7f070262

.field public static final punkt1:I = 0x7f0701bd

.field public static final reboot:I = 0x7f0701be

.field public static final reboot_message:I = 0x7f0701bf

.field public static final rebuild_info:I = 0x7f0701c0

.field public static final rebuild_message:I = 0x7f0701c1

.field public static final remodex:I = 0x7f0701c2

.field public static final remodexerror:I = 0x7f0701c3

.field public static final remove_all_saved_purchases:I = 0x7f0701c4

.field public static final remove_all_saved_purchases_message:I = 0x7f0701c5

.field public static final removefixes:I = 0x7f0701c6

.field public static final rep_hex_str:I = 0x7f0701c7

.field public static final restore_build:I = 0x7f0701c8

.field public static final restore_data_error:I = 0x7f0701c9

.field public static final restore_data_success:I = 0x7f0701ca

.field public static final restore_message:I = 0x7f0701cb

.field public static final restore_version:I = 0x7f0701cc

.field public static final restorebutton:I = 0x7f0701cd

.field public static final restored:I = 0x7f070255

.field public static final root_access_not_found:I = 0x7f0701ce

.field public static final root_needed:I = 0x7f0701cf

.field public static final safe_perm_ok:I = 0x7f0701d0

.field public static final safe_perm_use_warning:I = 0x7f0701d1

.field public static final savebutton:I = 0x7f0701d2

.field public static final saved_object:I = 0x7f0701d3

.field public static final search:I = 0x7f0701d4

.field public static final sel_target:I = 0x7f0701d5

.field public static final select_filter:I = 0x7f0701d6

.field public static final select_market:I = 0x7f0701d7

.field public static final send_logs:I = 0x7f0701d8

.field public static final sendlog:I = 0x7f0701d9

.field public static final set_default_to_install:I = 0x7f0701da

.field public static final set_default_to_install_auto:I = 0x7f0701db

.field public static final set_default_to_install_internal_memory:I = 0x7f0701dc

.field public static final set_default_to_install_sdcard:I = 0x7f0701dd

.field public static final set_switchers_def:I = 0x7f0701de

.field public static final setting_confirm_exit:I = 0x7f0701df

.field public static final setting_confirm_exit_descr:I = 0x7f0701e0

.field public static final settings:I = 0x7f0701e1

.field public static final settings_force_root:I = 0x7f0701e2

.field public static final settings_force_root_description:I = 0x7f0701e3

.field public static final settings_force_root_off:I = 0x7f0701e4

.field public static final settings_force_root_on:I = 0x7f0701e5

.field public static final settings_root_auto:I = 0x7f0701e6

.field public static final share:I = 0x7f070256

.field public static final share_message:I = 0x7f0701e7

.field public static final share_this_app:I = 0x7f0701e8

.field public static final site_pattern:I = 0x7f0701e9

.field public static final sortbyname:I = 0x7f0701ea

.field public static final sortbystatus:I = 0x7f0701eb

.field public static final sortbytime:I = 0x7f0701ec

.field public static final sortmenu:I = 0x7f0701ed

.field public static final stat_boot_ads:I = 0x7f0701ee

.field public static final stat_boot_custom:I = 0x7f0701ef

.field public static final stat_boot_lvl:I = 0x7f0701f0

.field public static final stat_launch_activity:I = 0x7f0701f1

.field public static final statads:I = 0x7f0701f2

.field public static final statappplace:I = 0x7f0701f3

.field public static final statbilling:I = 0x7f0701f4

.field public static final statbuild:I = 0x7f0701f5

.field public static final statcustom:I = 0x7f0701f6

.field public static final statdataplace:I = 0x7f0701f7

.field public static final statfix:I = 0x7f0701f8

.field public static final statlvl:I = 0x7f0701f9

.field public static final statmodified:I = 0x7f0701fa

.field public static final statnotfix:I = 0x7f0701fb

.field public static final statnotfound:I = 0x7f0701fc

.field public static final statnotmodified:I = 0x7f0701fd

.field public static final statnotsys:I = 0x7f0701fe

.field public static final statnull:I = 0x7f0701ff

.field public static final statsys:I = 0x7f070200

.field public static final statuserid:I = 0x7f070201

.field public static final statversion:I = 0x7f070202

.field public static final support_patch_billing:I = 0x7f070203

.field public static final support_patch_billing_descr:I = 0x7f070204

.field public static final support_patch_free_intent:I = 0x7f070205

.field public static final support_patch_free_intent_descr:I = 0x7f070206

.field public static final support_patch_lvl:I = 0x7f070207

.field public static final support_patch_lvl_descr:I = 0x7f070208

.field public static final switcher_auto_backup:I = 0x7f070209

.field public static final switcher_auto_backup_descr:I = 0x7f07020a

.field public static final switcher_auto_backup_only_gp:I = 0x7f07020b

.field public static final switcher_auto_backup_only_gp_descr:I = 0x7f07020c

.field public static final switcher_auto_integrate_update:I = 0x7f07020d

.field public static final switcher_auto_integrate_update_descr:I = 0x7f07020e

.field public static final switcher_auto_move_to_internal:I = 0x7f07020f

.field public static final switcher_auto_move_to_internal_descr:I = 0x7f070210

.field public static final switcher_auto_move_to_sd:I = 0x7f070211

.field public static final switcher_auto_move_to_sd_descr:I = 0x7f070212

.field public static final sysdescr:I = 0x7f070213

.field public static final system_app_change:I = 0x7f070214

.field public static final systitle:I = 0x7f070215

.field public static final text_size:I = 0x7f070216

.field public static final title_for_block_internet:I = 0x7f070257

.field public static final title_for_select_integrate_update_apps:I = 0x7f070217

.field public static final title_for_select_move_to_internal:I = 0x7f070218

.field public static final title_for_select_move_to_sdcard:I = 0x7f070219

.field public static final title_for_select_uninstall:I = 0x7f07021a

.field public static final title_for_unblock_internet:I = 0x7f070258

.field public static final title_upd:I = 0x7f07021b

.field public static final toolbar_adfree:I = 0x7f07021c

.field public static final toolbar_backups:I = 0x7f07021d

.field public static final toolbar_rebuild:I = 0x7f07021e

.field public static final toolbar_switchers:I = 0x7f07021f

.field public static final toolbar_system_utils:I = 0x7f070220

.field public static final tools_menu_block_internet:I = 0x7f070259

.field public static final tools_menu_unblock_internet:I = 0x7f07025a

.field public static final truble:I = 0x7f070221

.field public static final uninstall:I = 0x7f070222

.field public static final uninstallapp:I = 0x7f070223

.field public static final unknown_error:I = 0x7f070224

.field public static final unused_odex_delete:I = 0x7f070225

.field public static final update:I = 0x7f070226

.field public static final updatebusybox:I = 0x7f070227

.field public static final usepatch:I = 0x7f070228

.field public static final vers:I = 0x7f070229

.field public static final vibration:I = 0x7f07022a

.field public static final vibration_descr:I = 0x7f07022b

.field public static final viewlandscape:I = 0x7f07022c

.field public static final viewlarge:I = 0x7f07022d

.field public static final viewmedium:I = 0x7f07022e

.field public static final viewmenu:I = 0x7f07022f

.field public static final viewportret:I = 0x7f070230

.field public static final viewsensor:I = 0x7f070231

.field public static final viewsmall:I = 0x7f070232

.field public static final wait:I = 0x7f070233

.field public static final warning:I = 0x7f070234

.field public static final warning_inapp_emulation:I = 0x7f07025b

.field public static final well_done:I = 0x7f070235

.field public static final xposed_module_option:I = 0x7f070236

.field public static final xposed_notify:I = 0x7f070237

.field public static final xposed_option_off:I = 0x7f070238

.field public static final xposed_settings:I = 0x7f070239

.field public static final xposeddescription:I = 0x7f07025c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
