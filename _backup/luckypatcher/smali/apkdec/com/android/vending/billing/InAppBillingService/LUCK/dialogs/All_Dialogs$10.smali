.class Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$10;
.super Ljava/lang/Object;
.source "All_Dialogs.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->onCreateDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V
    .locals 0
    .parameter "this$0"

    .prologue
    .line 344
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$10;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 347
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 348
    .local v3, permiss:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 349
    .local v0, activ:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 350
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->perm_adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    .line 351
    .local v2, id1:Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Status:Z

    if-nez v4, :cond_0

    iget-object v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v5, "chelpus_"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 352
    iget-object v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    :cond_0
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Status:Z

    if-eqz v4, :cond_1

    iget-object v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v5, "chelpa_per_"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 354
    iget-object v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v5, "chelpa_per_"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v5, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v6, "chelpa_per_"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 357
    :cond_1
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Status:Z

    if-nez v4, :cond_2

    iget-object v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v5, "chelpus_"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v5, "disabled_"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 358
    iget-object v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v5, "chelpus_"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 359
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v7, "chelpus_"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 361
    :cond_2
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Status:Z

    if-eqz v4, :cond_3

    iget-object v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v5, "chelpus_disabled_"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 362
    iget-object v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v5, "chelpus_"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v7, "chelpus_"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 349
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 367
    .end local v2           #id1:Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;
    :cond_4
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-virtual {v4, v5, v3, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->apkpermissions_safe(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    .line 368
    return-void
.end method
