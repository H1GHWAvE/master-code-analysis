.class Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;
.super Ljava/lang/Object;
.source "PackageChangeReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

.field final synthetic val$ctx:Landroid/content/Context;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;Landroid/content/Intent;Landroid/content/Context;)V
    .locals 0
    .parameter "this$0"
    .parameter
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    iput-object p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 30

    .prologue
    .line 52
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 53
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v24

    const-string v25, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 54
    new-instance v12, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v24, v0

    const-class v25, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v12, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 55
    .local v12, in:Landroid/content/Intent;
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 57
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->init()V

    .line 59
    :try_start_0
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 61
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v24

    const-class v25, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_1

    .line 62
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v24, v0

    const-string v25, "android.intent.extra.changed_component_name_list"

    invoke-virtual/range {v24 .. v25}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 63
    .local v7, components:[Ljava/lang/String;
    if-eqz v7, :cond_1

    array-length v0, v7

    move/from16 v24, v0

    if-lez v24, :cond_1

    .line 64
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v24, :cond_1

    .line 65
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "update adapt "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const/16 v26, 0x0

    aget-object v26, v7, v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 66
    const/16 v24, 0x0

    aget-object v24, v7, v24

    const-string v25, "com.android.vending.billing.InAppBillingService.LUCK.MainActivity"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v24

    if-eqz v24, :cond_1

    .line 69
    :try_start_1
    sget-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v24, :cond_8

    .line 70
    new-instance v11, Landroid/content/Intent;

    const-string v24, "android.intent.action.MAIN"

    move-object/from16 v0, v24

    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 71
    .local v11, i:Landroid/content/Intent;
    const-string v24, "android.intent.category.HOME"

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string v24, "android.intent.category.DEFAULT"

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v11, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v23

    .line 74
    .local v23, resolves:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :cond_0
    :goto_0
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_8

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/content/pm/ResolveInfo;

    .line 75
    .local v22, res:Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v25, v0

    if-eqz v25, :cond_0

    .line 76
    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    move-object/from16 v21, v0

    .line 77
    .local v21, pn:Ljava/lang/String;
    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 78
    .local v17, pkg:Ljava/lang/String;
    new-instance v25, Ljava/lang/Thread;

    new-instance v26, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$1;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct/range {v25 .. v26}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 84
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 94
    .end local v11           #i:Landroid/content/Intent;
    .end local v17           #pkg:Ljava/lang/String;
    .end local v21           #pn:Ljava/lang/String;
    .end local v22           #res:Landroid/content/pm/ResolveInfo;
    .end local v23           #resolves:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :catch_0
    move-exception v8

    .local v8, e:Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 100
    .end local v7           #components:[Ljava/lang/String;
    .end local v8           #e:Ljava/lang/Exception;
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v24

    const-string v25, "com.android.vending"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v24

    const-string v25, "com.google.android.gms"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 101
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v24, v0

    const-string v25, "android.intent.extra.changed_component_name_list"

    invoke-virtual/range {v24 .. v25}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 102
    .restart local v7       #components:[Ljava/lang/String;
    if-eqz v7, :cond_4

    array-length v0, v7

    move/from16 v24, v0

    if-lez v24, :cond_4

    .line 103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v24

    const-string v25, "com.google.android.gms"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 104
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v24, :cond_3

    .line 105
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "update adapt "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const/16 v26, 0x0

    aget-object v26, v7, v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 106
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v25, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$3;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;)V

    invoke-virtual/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 114
    :cond_3
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    if-eqz v24, :cond_4

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v24, :cond_4

    .line 115
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "update adapt "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const/16 v26, 0x0

    aget-object v26, v7, v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 116
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v25, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$4;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;)V

    invoke-virtual/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 126
    .end local v7           #components:[Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v24

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_37

    .line 127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v24, v0

    const-string v25, "android.intent.extra.changed_component_name_list"

    invoke-virtual/range {v24 .. v25}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 128
    .restart local v7       #components:[Ljava/lang/String;
    if-eqz v7, :cond_37

    array-length v0, v7

    move/from16 v24, v0

    if-lez v24, :cond_37

    .line 129
    const/4 v5, 0x0

    .line 130
    .local v5, billingServiceChanged:Z
    const/16 v16, 0x0

    .line 131
    .local v16, licensingServiceChanged:Z
    const/4 v9, 0x0

    .line 132
    .local v9, enableGoogle:Z
    const/4 v10, 0x0

    .line 133
    .local v10, enableLicGoogle:Z
    array-length v0, v7

    move/from16 v25, v0

    const/16 v24, 0x0

    :goto_2
    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_33

    aget-object v6, v7, v24

    .line 134
    .local v6, comp:Ljava/lang/String;
    const-string v26, "com.google.android.finsky.billing.iab.MarketBillingService"

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_5

    const-string v26, "com.google.android.finsky.billing.iab.InAppBillingService"

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_6

    .line 135
    :cond_5
    sget-object v26, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 136
    new-instance v14, Landroid/content/Intent;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/inapp_widget;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v14, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 137
    .local v14, in4:Landroid/content/Intent;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v14, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    sget-object v26, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/inapp_widget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-virtual {v14, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 140
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v26

    new-instance v27, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v28

    const-class v29, Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-direct/range {v27 .. v29}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v26 .. v27}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_31

    .line 141
    const/4 v5, 0x1

    .line 146
    .end local v14           #in4:Landroid/content/Intent;
    :cond_6
    :goto_3
    const-string v26, "com.licensinghack.LicensingService"

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 147
    sget-object v26, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 149
    new-instance v13, Landroid/content/Intent;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/lvl_widget;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v13, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 150
    .local v13, in1:Landroid/content/Intent;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v13, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 151
    sget-object v26, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/lvl_widget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-virtual {v13, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 153
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v26

    new-instance v27, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v28

    const-class v29, Lcom/google/android/finsky/services/LicensingService;

    invoke-direct/range {v27 .. v29}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v26 .. v27}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_32

    .line 154
    const/16 v16, 0x1

    .line 133
    .end local v13           #in1:Landroid/content/Intent;
    :cond_7
    :goto_4
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_2

    .line 88
    .end local v5           #billingServiceChanged:Z
    .end local v6           #comp:Ljava/lang/String;
    .end local v9           #enableGoogle:Z
    .end local v10           #enableLicGoogle:Z
    .end local v16           #licensingServiceChanged:Z
    :cond_8
    :try_start_3
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v25, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$2;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;)V

    invoke-virtual/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1

    .line 227
    .end local v7           #components:[Ljava/lang/String;
    :catch_1
    move-exception v8

    .line 228
    .local v8, e:Ljava/lang/RuntimeException;
    invoke-virtual {v8}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 231
    .end local v8           #e:Ljava/lang/RuntimeException;
    .end local v12           #in:Landroid/content/Intent;
    :cond_9
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v24

    const-string v25, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_12

    .line 233
    new-instance v12, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v24, v0

    const-class v25, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v12, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 234
    .restart local v12       #in:Landroid/content/Intent;
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 236
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->init()V

    .line 238
    :try_start_4
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v19

    .line 241
    .local v19, pkgname:Ljava/lang/String;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v24

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    move/from16 v2, v25

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 242
    sget-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v24, :cond_c

    .line 245
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "-1.odex"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_a

    .line 246
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-1.odex"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 247
    :cond_a
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "-2.odex"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_b

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-2.odex"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 248
    :cond_b
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ".odex"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_c

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ".odex"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 252
    :cond_c
    const-string v24, "com.android.vending"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 253
    :cond_d
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v26, v0

    const-class v27, Lcom/google/android/finsky/services/LicensingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v24 .. v25}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v24

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_39

    .line 254
    const/16 v24, 0x1

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->market_licensing_services(Z)V

    .line 256
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/services/LicensingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x2

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 266
    :goto_6
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v26, v0

    const-class v27, Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v24 .. v25}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v24

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_e

    .line 267
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v26, v0

    const-class v27, Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v24 .. v25}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v24

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_3a

    .line 268
    :cond_e
    const/16 v24, 0x1

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->market_billing_services(Z)V

    .line 270
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x2

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 273
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x2

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 288
    :cond_f
    :goto_7
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    const-string v25, "manual_path"

    const/16 v26, 0x0

    invoke-interface/range {v24 .. v26}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v24

    if-eqz v24, :cond_11

    .line 289
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    const-string v25, "days_on_up"

    const/16 v26, 0x1

    invoke-interface/range {v24 .. v26}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v24

    sput v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_3

    .line 293
    :try_start_5
    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v25

    sget v26, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I

    const/16 v27, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-direct {v15, v0, v1, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;-><init>(Landroid/content/Context;Ljava/lang/String;IZ)V

    .line 294
    .local v15, item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    if-nez v24, :cond_10

    .line 295
    new-instance v24, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    .line 296
    :cond_10
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->savePackage(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V

    .line 297
    sget-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->desktop_launch:Z

    if-eqz v24, :cond_11

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    if-eqz v24, :cond_11

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v24, :cond_11

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v24 .. v24}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v24

    if-eqz v24, :cond_11

    .line 298
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v24 .. v24}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v24

    new-instance v25, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$8;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$8;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V

    invoke-virtual/range {v24 .. v25}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_3

    .line 322
    .end local v15           #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_11
    :goto_8
    const/16 v24, 0x1

    :try_start_6
    sput-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->refresh:Z
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_3

    .line 331
    .end local v12           #in:Landroid/content/Intent;
    .end local v19           #pkgname:Ljava/lang/String;
    :cond_12
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v24

    const-string v25, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_20

    .line 332
    new-instance v12, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v24, v0

    const-class v25, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v12, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 333
    .restart local v12       #in:Landroid/content/Intent;
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 334
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 335
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->init()V

    .line 337
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v19

    .line 338
    .restart local v19       #pkgname:Ljava/lang/String;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v24

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    move/from16 v2, v25

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 339
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    const-string v25, "switch_auto_backup_apk"

    const/16 v26, 0x0

    invoke-interface/range {v24 .. v26}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v24

    if-eqz v24, :cond_13

    .line 340
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "Backup app on update"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 341
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    const-string v25, "switch_auto_backup_apk_only_gp"

    const/16 v26, 0x0

    invoke-interface/range {v24 .. v26}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v24

    if-nez v24, :cond_13

    .line 342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->backup(Ljava/lang/String;)Ljava/lang/String;

    .line 346
    :cond_13
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    const-string v25, "switch_auto_backup_apk_only_gp"

    const/16 v26, 0x0

    invoke-interface/range {v24 .. v26}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v24

    if-eqz v24, :cond_14

    .line 347
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "Backup app on update"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_6

    .line 349
    :try_start_8
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    const-string v25, "com.android.vending"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_14

    .line 350
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->backup(Ljava/lang/String;)Ljava/lang/String;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_6

    .line 354
    :cond_14
    :goto_a
    :try_start_9
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->handlerToast:Landroid/os/Handler;

    if-nez v24, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->handler:Landroid/os/Handler;

    move-object/from16 v24, v0

    sput-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->handlerToast:Landroid/os/Handler;

    .line 355
    :cond_15
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    const-string v25, "days_on_up"

    const/16 v26, 0x1

    invoke-interface/range {v24 .. v26}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v24

    sput v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I

    .line 356
    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v25

    sget v26, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I

    const/16 v27, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-direct {v15, v0, v1, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;-><init>(Landroid/content/Context;Ljava/lang/String;IZ)V

    .line 357
    .restart local v15       #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    const-string v4, ""
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_6

    .line 359
    .local v4, appDir:Ljava/lang/String;
    :try_start_a
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v24

    iget-object v0, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v24 .. v26}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v24

    move-object/from16 v0, v24

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v4, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;
    :try_end_a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_6

    .line 367
    :goto_b
    :try_start_b
    iget-boolean v0, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    move/from16 v24, v0

    if-eqz v24, :cond_16

    const-string v24, "/data"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_16

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    const-string v25, "switch_auto_integrate_update"

    const/16 v26, 0x0

    invoke-interface/range {v24 .. v26}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v24

    if-eqz v24, :cond_16

    sget-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v24, :cond_16

    .line 368
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "Integrate update to system on update app"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 369
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 370
    .local v20, plis:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    const/16 v24, 0x0

    const/16 v25, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->integrate_to_system(Ljava/util/ArrayList;ZZ)V

    .line 374
    .end local v20           #plis:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    :cond_16
    iget-boolean v0, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    move/from16 v24, v0

    if-nez v24, :cond_17

    const-string v24, "/data/"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_17

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    const-string v25, "switch_auto_move_to_sd"

    const/16 v26, 0x0

    invoke-interface/range {v24 .. v26}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v24

    if-eqz v24, :cond_17

    sget-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v24, :cond_17

    .line 375
    new-instance v24, Lcom/chelpus/Utils;

    const-string v25, ""

    invoke-direct/range {v24 .. v25}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "pm install -r -s -i com.android.vending "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-virtual/range {v24 .. v25}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    .line 377
    :cond_17
    iget-boolean v0, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    move/from16 v24, v0

    if-nez v24, :cond_18

    const-string v24, "/mnt/"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_18

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    const-string v25, "switch_auto_move_to_internal"

    const/16 v26, 0x0

    invoke-interface/range {v24 .. v26}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v24

    if-eqz v24, :cond_18

    sget-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v24, :cond_18

    .line 378
    new-instance v24, Lcom/chelpus/Utils;

    const-string v25, ""

    invoke-direct/range {v24 .. v25}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "pm install -r -f -i com.android.vending "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-virtual/range {v24 .. v25}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    .line 380
    :cond_18
    sget-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v24, :cond_1b

    .line 382
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "-1.odex"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_19

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-1.odex"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 383
    :cond_19
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "-2.odex"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_1a

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-2.odex"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 384
    :cond_1a
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ".odex"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_1b

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ".odex"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 388
    :cond_1b
    const-string v24, "com.android.vending"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_1d

    .line 389
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v26, v0

    const-class v27, Lcom/google/android/finsky/services/LicensingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v24 .. v25}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v24

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_3b

    .line 390
    const/16 v24, 0x1

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->market_licensing_services(Z)V

    .line 391
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/services/LicensingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x2

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 400
    :goto_c
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v26, v0

    const-class v27, Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v24 .. v25}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v24

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_1c

    .line 401
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v26, v0

    const-class v27, Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v24 .. v25}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v24

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_3c

    .line 402
    :cond_1c
    const/16 v24, 0x1

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->market_billing_services(Z)V

    .line 404
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x2

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 407
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x2

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 422
    :cond_1d
    :goto_d
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    const-string v25, "manual_path"

    const/16 v26, 0x0

    invoke-interface/range {v24 .. v26}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v24

    if-eqz v24, :cond_1f

    .line 423
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    const-string v25, "days_on_up"

    const/16 v26, 0x1

    invoke-interface/range {v24 .. v26}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v24

    sput v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I

    .line 428
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    if-nez v24, :cond_1e

    .line 429
    new-instance v24, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    .line 430
    :cond_1e
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->savePackage(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V

    .line 431
    sget-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->desktop_launch:Z

    if-eqz v24, :cond_1f

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    if-eqz v24, :cond_1f

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v24, :cond_1f

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v24 .. v24}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v24

    if-eqz v24, :cond_1f

    .line 432
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v24 .. v24}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v24

    new-instance v25, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$9;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$9;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V

    invoke-virtual/range {v24 .. v25}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 447
    :cond_1f
    const/16 v24, 0x1

    sput-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->refresh:Z
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_6

    .line 452
    .end local v4           #appDir:Ljava/lang/String;
    .end local v12           #in:Landroid/content/Intent;
    .end local v15           #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .end local v19           #pkgname:Ljava/lang/String;
    :cond_20
    :goto_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v24

    const-string v25, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_2f

    .line 453
    new-instance v12, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v24, v0

    const-class v25, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v12, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 454
    .restart local v12       #in:Landroid/content/Intent;
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 455
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 456
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "delete trigger "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v26, v0

    const-string v27, "android.intent.extra.REPLACING"

    const/16 v28, 0x0

    invoke-virtual/range {v26 .. v28}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 457
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v24, v0

    const-string v25, "android.intent.extra.REPLACING"

    const/16 v26, 0x0

    invoke-virtual/range {v24 .. v26}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v24

    if-nez v24, :cond_2f

    .line 458
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->init()V

    .line 460
    :try_start_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v19

    .line 461
    .restart local v19       #pkgname:Ljava/lang/String;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 462
    sget-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v24, :cond_2b

    .line 463
    sget v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->api:I

    const/16 v25, 0x15

    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_28

    .line 464
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "-1/arm"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_21

    .line 465
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm -rf /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-1"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 466
    :cond_21
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "-2/arm"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_22

    .line 467
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm -rf /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-2"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 468
    :cond_22
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "-3/arm"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_23

    .line 469
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm -rf /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-3"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 470
    :cond_23
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "-4/arm"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_24

    .line 471
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm -rf /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-4"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 472
    :cond_24
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "-1/x86"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_25

    .line 473
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm -rf /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-1"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 474
    :cond_25
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "-2/x86"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_26

    .line 475
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm -rf /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-2"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 476
    :cond_26
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "-3/x86"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_27

    .line 477
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm -rf /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-3"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 478
    :cond_27
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "-4/x86"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_28

    .line 479
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm -rf /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-4"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 481
    :cond_28
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "-1.odex"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_29

    .line 482
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-1.odex"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 483
    :cond_29
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "-2.odex"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_2a

    .line 484
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-2.odex"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 485
    :cond_2a
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "/data/app/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ".odex"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_2b

    .line 486
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rm /data/app/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ".odex"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 490
    :cond_2b
    const-string v24, "com.android.vending"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_2d

    .line 491
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v26, v0

    const-class v27, Lcom/google/android/finsky/services/LicensingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v24 .. v25}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v24

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_3d

    .line 492
    const/16 v24, 0x1

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->market_licensing_services(Z)V

    .line 494
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/services/LicensingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x2

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 504
    :goto_f
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v26, v0

    const-class v27, Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v24 .. v25}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v24

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_2c

    .line 505
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$ctx:Landroid/content/Context;

    move-object/from16 v26, v0

    const-class v27, Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v24 .. v25}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v24

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_3e

    .line 506
    :cond_2c
    const/16 v24, 0x1

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->market_billing_services(Z)V

    .line 508
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x2

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 511
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x2

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 524
    :cond_2d
    :goto_10
    const/16 v24, 0x1

    sput-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->refresh:Z

    .line 525
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v18

    .line 526
    .local v18, pkgName:Ljava/lang/String;
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    if-nez v24, :cond_2e

    .line 527
    new-instance v24, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    .line 528
    :cond_2e
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->deletePackage(Ljava/lang/String;)V

    .line 530
    sget-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->desktop_launch:Z

    if-eqz v24, :cond_2f

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    if-eqz v24, :cond_2f

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v24, :cond_2f

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v24 .. v24}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v24

    if-eqz v24, :cond_2f

    .line 531
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v24 .. v24}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v24

    new-instance v25, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$10;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$10;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v25}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_7

    .line 548
    .end local v12           #in:Landroid/content/Intent;
    .end local v18           #pkgName:Ljava/lang/String;
    .end local v19           #pkgname:Ljava/lang/String;
    :cond_2f
    :goto_11
    sget-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchOnBoot:Z

    if-nez v24, :cond_30

    sget-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->desktop_launch:Z

    if-nez v24, :cond_30

    .line 549
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "LP - exit."

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 552
    :cond_30
    return-void

    .line 143
    .restart local v5       #billingServiceChanged:Z
    .restart local v6       #comp:Ljava/lang/String;
    .restart local v7       #components:[Ljava/lang/String;
    .restart local v9       #enableGoogle:Z
    .restart local v10       #enableLicGoogle:Z
    .restart local v12       #in:Landroid/content/Intent;
    .restart local v14       #in4:Landroid/content/Intent;
    .restart local v16       #licensingServiceChanged:Z
    :cond_31
    const/4 v9, 0x1

    goto/16 :goto_3

    .line 156
    .end local v14           #in4:Landroid/content/Intent;
    .restart local v13       #in1:Landroid/content/Intent;
    :cond_32
    const/4 v10, 0x1

    goto/16 :goto_4

    .line 160
    .end local v6           #comp:Ljava/lang/String;
    .end local v13           #in1:Landroid/content/Intent;
    :cond_33
    if-eqz v5, :cond_34

    .line 167
    :try_start_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->connectToBilling()V

    .line 169
    :cond_34
    if-eqz v16, :cond_35

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->connectToLicensing()V

    .line 172
    :cond_35
    if-eqz v9, :cond_36

    .line 173
    new-instance v24, Ljava/lang/Thread;

    new-instance v25, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$5;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;)V

    invoke-direct/range {v24 .. v25}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 178
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Thread;->start()V

    .line 180
    :cond_36
    if-eqz v10, :cond_37

    .line 181
    new-instance v24, Ljava/lang/Thread;

    new-instance v25, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$6;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;)V

    invoke-direct/range {v24 .. v25}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 186
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Thread;->start()V

    .line 190
    .end local v5           #billingServiceChanged:Z
    .end local v7           #components:[Ljava/lang/String;
    .end local v9           #enableGoogle:Z
    .end local v10           #enableLicGoogle:Z
    .end local v16           #licensingServiceChanged:Z
    :cond_37
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v24

    const-string v25, "days_on_up"

    const/16 v26, 0x1

    invoke-interface/range {v24 .. v26}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v24

    sput v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_1

    .line 193
    :try_start_e
    new-instance v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;->val$intent:Landroid/content/Intent;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v25

    sget v26, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I

    const/16 v27, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-direct {v15, v0, v1, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;-><init>(Landroid/content/Context;Ljava/lang/String;IZ)V

    .line 194
    .restart local v15       #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    if-nez v24, :cond_38

    .line 195
    new-instance v24, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    .line 196
    :cond_38
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->savePackage(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V

    .line 197
    sget-boolean v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->desktop_launch:Z

    if-eqz v24, :cond_9

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    if-eqz v24, :cond_9

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v24, :cond_9

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v24 .. v24}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v24

    if-eqz v24, :cond_9

    .line 198
    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v24 .. v24}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v24

    new-instance v25, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$7;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$1;Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V

    invoke-virtual/range {v24 .. v25}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_1

    goto/16 :goto_5

    .line 218
    .end local v15           #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :catch_2
    move-exception v8

    .line 219
    .local v8, e:Ljava/lang/Exception;
    :try_start_f
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 220
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "Item dont create. And dont add to database."

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_f} :catch_1

    goto/16 :goto_5

    .line 261
    .end local v8           #e:Ljava/lang/Exception;
    .restart local v19       #pkgname:Ljava/lang/String;
    :cond_39
    :try_start_10
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/services/LicensingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x1

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_3

    goto/16 :goto_6

    .line 327
    .end local v19           #pkgname:Ljava/lang/String;
    :catch_3
    move-exception v8

    .line 328
    .local v8, e:Ljava/lang/RuntimeException;
    invoke-virtual {v8}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto/16 :goto_9

    .line 279
    .end local v8           #e:Ljava/lang/RuntimeException;
    .restart local v19       #pkgname:Ljava/lang/String;
    :cond_3a
    :try_start_11
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x1

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 282
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x1

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto/16 :goto_7

    .line 317
    :catch_4
    move-exception v8

    .line 318
    .local v8, e:Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 319
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "Item dont create. And dont add to database."

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/lang/RuntimeException; {:try_start_11 .. :try_end_11} :catch_3

    goto/16 :goto_8

    .line 360
    .end local v8           #e:Ljava/lang/Exception;
    .restart local v4       #appDir:Ljava/lang/String;
    .restart local v15       #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :catch_5
    move-exception v8

    .line 361
    .local v8, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_12
    invoke-virtual {v8}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
    :try_end_12
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_12} :catch_6

    goto/16 :goto_b

    .line 448
    .end local v4           #appDir:Ljava/lang/String;
    .end local v8           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v15           #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .end local v19           #pkgname:Ljava/lang/String;
    :catch_6
    move-exception v8

    .line 449
    .local v8, e:Ljava/lang/RuntimeException;
    invoke-virtual {v8}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto/16 :goto_e

    .line 395
    .end local v8           #e:Ljava/lang/RuntimeException;
    .restart local v4       #appDir:Ljava/lang/String;
    .restart local v15       #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .restart local v19       #pkgname:Ljava/lang/String;
    :cond_3b
    :try_start_13
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/services/LicensingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x1

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto/16 :goto_c

    .line 413
    :cond_3c
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x1

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 416
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x1

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_13
    .catch Ljava/lang/RuntimeException; {:try_start_13 .. :try_end_13} :catch_6

    goto/16 :goto_d

    .line 499
    .end local v4           #appDir:Ljava/lang/String;
    .end local v15           #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_3d
    :try_start_14
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/services/LicensingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x1

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_14
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_14} :catch_7

    goto/16 :goto_f

    .line 543
    .end local v19           #pkgname:Ljava/lang/String;
    :catch_7
    move-exception v8

    .line 544
    .restart local v8       #e:Ljava/lang/RuntimeException;
    invoke-virtual {v8}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto/16 :goto_11

    .line 516
    .end local v8           #e:Ljava/lang/RuntimeException;
    .restart local v19       #pkgname:Ljava/lang/String;
    :cond_3e
    :try_start_15
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x1

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 519
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v24

    new-instance v25, Landroid/content/ComponentName;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v26

    const-class v27, Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-direct/range {v25 .. v27}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v26, 0x1

    const/16 v27, 0x1

    invoke-virtual/range {v24 .. v27}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_15
    .catch Ljava/lang/RuntimeException; {:try_start_15 .. :try_end_15} :catch_7

    goto/16 :goto_10

    .line 352
    :catch_8
    move-exception v24

    goto/16 :goto_a
.end method
