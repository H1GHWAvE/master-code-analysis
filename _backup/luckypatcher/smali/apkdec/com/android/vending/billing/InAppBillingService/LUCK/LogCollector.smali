.class public Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;
.super Ljava/lang/Object;
.source "LogCollector.java"


# static fields
.field public static final LINE_SEPARATOR:Ljava/lang/String; = "\n"

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mLastLogs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPackageName:Ljava/lang/String;

.field private mPattern:Ljava/util/regex/Pattern;

.field private mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPackageName:Ljava/lang/String;

    .line 66
    const-string v1, "(.*)E\\/AndroidRuntime\\(\\s*\\d+\\)\\:\\s*at\\s%s.*"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPackageName:Ljava/lang/String;

    const-string v4, "."

    const-string v5, "\\."

    .line 67
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 66
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, pattern:Ljava/lang/String;
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPattern:Ljava/util/regex/Pattern;

    .line 69
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    .line 70
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v1

    const-string v2, "LogCollector"

    invoke-virtual {v1, v2, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPrefs:Landroid/content/SharedPreferences;

    .line 71
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mLastLogs:Ljava/util/ArrayList;

    .line 72
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 73
    return-void
.end method

.method private collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V
    .locals 17
    .parameter
    .parameter "format"
    .parameter "buffer"
    .parameter "filterSpecs"
    .parameter "noRoot"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, outLines:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->clear()V

    .line 115
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v11, params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p2, :cond_0

    .line 118
    const-string p2, "time"

    .line 121
    :cond_0
    const-string v13, "-v"

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    if-eqz p3, :cond_1

    .line 125
    const-string v13, "-b"

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    :cond_1
    if-eqz p4, :cond_2

    .line 130
    move-object/from16 v0, p4

    invoke-static {v11, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 135
    :cond_2
    :try_start_0
    sget-boolean v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v13, :cond_3

    if-eqz p5, :cond_8

    .line 136
    :cond_3
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "Collect logs no root."

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 137
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 138
    .local v5, commandLine:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v13, "logcat"

    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    const-string v13, "-d"

    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 141
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v13

    new-array v3, v13, [Ljava/lang/String;

    .line 142
    .local v3, cmdLine:[Ljava/lang/String;
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    .end local v3           #cmdLine:[Ljava/lang/String;
    check-cast v3, [Ljava/lang/String;

    .line 143
    .restart local v3       #cmdLine:[Ljava/lang/String;
    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-string v15, "sdcard"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 144
    const/4 v13, 0x4

    new-array v4, v13, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "logcat"

    aput-object v14, v4, v13

    const/4 v13, 0x1

    const-string v14, "-d"

    aput-object v14, v4, v13

    const/4 v13, 0x2

    const-string v14, "System.out:*"

    aput-object v14, v4, v13

    const/4 v13, 0x3

    const-string v14, "*:S"

    aput-object v14, v4, v13

    .line 145
    .local v4, cmdLine2:[Ljava/lang/String;
    move-object v3, v4

    .line 147
    .end local v4           #cmdLine2:[Ljava/lang/String;
    :cond_4
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v12

    .line 148
    .local v12, process:Ljava/lang/Process;
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v13, Ljava/io/InputStreamReader;

    invoke-virtual {v12}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 154
    .local v2, bufferedReader:Ljava/io/BufferedReader;
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    .local v9, line:Ljava/lang/String;
    if-eqz v9, :cond_6

    .line 155
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 246
    .end local v2           #bufferedReader:Ljava/io/BufferedReader;
    .end local v3           #cmdLine:[Ljava/lang/String;
    .end local v5           #commandLine:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9           #line:Ljava/lang/String;
    .end local v12           #process:Ljava/lang/Process;
    :catch_0
    move-exception v6

    .line 250
    .local v6, e:Ljava/io/IOException;
    :try_start_1
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v13

    const-string v14, "su"

    invoke-virtual {v13, v14}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v12

    .line 253
    .restart local v12       #process:Ljava/lang/Process;
    new-instance v10, Ljava/io/DataOutputStream;

    invoke-virtual {v12}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v13

    invoke-direct {v10, v13}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 255
    .local v10, os:Ljava/io/DataOutputStream;
    new-instance v8, Ljava/io/DataInputStream;

    invoke-virtual {v12}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v13

    invoke-direct {v8, v13}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 256
    .local v8, errorGobbler:Ljava/io/DataInputStream;
    invoke-virtual {v8}, Ljava/io/DataInputStream;->available()I

    move-result v13

    new-array v1, v13, [B

    .line 257
    .local v1, arrayErrorOfByte:[B
    invoke-virtual {v8, v1}, Ljava/io/DataInputStream;->read([B)I

    .line 258
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 260
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v13, Ljava/io/InputStreamReader;

    invoke-virtual {v12}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 261
    .restart local v2       #bufferedReader:Ljava/io/BufferedReader;
    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-string v15, "sdcard"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_10

    .line 262
    const-string v13, "logcat -d System.out:* *:S\n"

    invoke-virtual {v10, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 266
    :goto_1
    invoke-virtual {v10}, Ljava/io/DataOutputStream;->flush()V

    .line 267
    invoke-virtual {v10}, Ljava/io/DataOutputStream;->close()V

    .line 273
    const-string v9, ""

    .line 274
    .restart local v9       #line:Ljava/lang/String;
    :goto_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_11

    .line 275
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 279
    .end local v1           #arrayErrorOfByte:[B
    .end local v2           #bufferedReader:Ljava/io/BufferedReader;
    .end local v8           #errorGobbler:Ljava/io/DataInputStream;
    .end local v9           #line:Ljava/lang/String;
    .end local v10           #os:Ljava/io/DataOutputStream;
    .end local v12           #process:Ljava/lang/Process;
    :catch_1
    move-exception v7

    .line 281
    .local v7, e1:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    .line 285
    .end local v6           #e:Ljava/io/IOException;
    .end local v7           #e1:Ljava/io/IOException;
    :cond_5
    :goto_3
    return-void

    .line 157
    .restart local v2       #bufferedReader:Ljava/io/BufferedReader;
    .restart local v3       #cmdLine:[Ljava/lang/String;
    .restart local v5       #commandLine:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v9       #line:Ljava/lang/String;
    .restart local v12       #process:Ljava/lang/Process;
    :cond_6
    :try_start_2
    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    .line 158
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 213
    :cond_7
    :goto_4
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v13

    if-nez v13, :cond_5

    .line 215
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v13

    const-string v14, "su"

    invoke-virtual {v13, v14}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v12

    .line 218
    new-instance v10, Ljava/io/DataOutputStream;

    invoke-virtual {v12}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v13

    invoke-direct {v10, v13}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 220
    .restart local v10       #os:Ljava/io/DataOutputStream;
    new-instance v8, Ljava/io/DataInputStream;

    invoke-virtual {v12}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v13

    invoke-direct {v8, v13}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 221
    .restart local v8       #errorGobbler:Ljava/io/DataInputStream;
    invoke-virtual {v8}, Ljava/io/DataInputStream;->available()I

    move-result v13

    new-array v1, v13, [B

    .line 222
    .restart local v1       #arrayErrorOfByte:[B
    invoke-virtual {v8, v1}, Ljava/io/DataInputStream;->read([B)I

    .line 223
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 225
    new-instance v2, Ljava/io/BufferedReader;

    .end local v2           #bufferedReader:Ljava/io/BufferedReader;
    new-instance v13, Ljava/io/InputStreamReader;

    invoke-virtual {v12}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 226
    .restart local v2       #bufferedReader:Ljava/io/BufferedReader;
    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-string v15, "sdcard"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_e

    .line 227
    const-string v13, "logcat -d System.out:* *:S\n"

    invoke-virtual {v10, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 231
    :goto_5
    invoke-virtual {v10}, Ljava/io/DataOutputStream;->flush()V

    .line 232
    invoke-virtual {v10}, Ljava/io/DataOutputStream;->close()V

    .line 239
    :goto_6
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_f

    .line 240
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 160
    .end local v1           #arrayErrorOfByte:[B
    .end local v2           #bufferedReader:Ljava/io/BufferedReader;
    .end local v3           #cmdLine:[Ljava/lang/String;
    .end local v5           #commandLine:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8           #errorGobbler:Ljava/io/DataInputStream;
    .end local v9           #line:Ljava/lang/String;
    .end local v10           #os:Ljava/io/DataOutputStream;
    .end local v12           #process:Ljava/lang/Process;
    :cond_8
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v13

    const-string v14, "su"

    invoke-virtual {v13, v14}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v12

    .line 161
    .restart local v12       #process:Ljava/lang/Process;
    new-instance v10, Ljava/io/DataOutputStream;

    invoke-virtual {v12}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v13

    invoke-direct {v10, v13}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 162
    .restart local v10       #os:Ljava/io/DataOutputStream;
    new-instance v8, Ljava/io/DataInputStream;

    invoke-virtual {v12}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v13

    invoke-direct {v8, v13}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 163
    .restart local v8       #errorGobbler:Ljava/io/DataInputStream;
    invoke-virtual {v8}, Ljava/io/DataInputStream;->available()I

    move-result v13

    new-array v1, v13, [B

    .line 164
    .restart local v1       #arrayErrorOfByte:[B
    invoke-virtual {v8, v1}, Ljava/io/DataInputStream;->read([B)I

    .line 165
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 166
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v13, Ljava/io/InputStreamReader;

    invoke-virtual {v12}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 167
    .restart local v2       #bufferedReader:Ljava/io/BufferedReader;
    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-string v15, "sdcard"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_c

    .line 168
    const-string v13, "logcat -d System.out:* *:S\n"

    invoke-virtual {v10, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 172
    :goto_7
    invoke-virtual {v10}, Ljava/io/DataOutputStream;->flush()V

    .line 173
    invoke-virtual {v10}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 181
    :goto_8
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    .restart local v9       #line:Ljava/lang/String;
    if-eqz v9, :cond_9

    .line 182
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_8

    .line 184
    .end local v9           #line:Ljava/lang/String;
    :catch_2
    move-exception v6

    .line 185
    .local v6, e:Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 187
    .end local v6           #e:Ljava/lang/Exception;
    :cond_9
    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 188
    :cond_a
    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    .line 189
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "Collect logs no root."

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 190
    const-string v13, "\n\n\n*********************************** NO ROOT *******************************************************\n\n\n"

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 192
    .restart local v5       #commandLine:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v13, "logcat"

    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    const-string v13, "-d"

    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 195
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v13

    new-array v3, v13, [Ljava/lang/String;

    .line 196
    .restart local v3       #cmdLine:[Ljava/lang/String;
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    .end local v3           #cmdLine:[Ljava/lang/String;
    check-cast v3, [Ljava/lang/String;

    .line 197
    .restart local v3       #cmdLine:[Ljava/lang/String;
    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-string v15, "sdcard"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 198
    const/4 v13, 0x4

    new-array v4, v13, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "logcat"

    aput-object v14, v4, v13

    const/4 v13, 0x1

    const-string v14, "-d"

    aput-object v14, v4, v13

    const/4 v13, 0x2

    const-string v14, "System.out:*"

    aput-object v14, v4, v13

    const/4 v13, 0x3

    const-string v14, "*:S"

    aput-object v14, v4, v13

    .line 199
    .restart local v4       #cmdLine2:[Ljava/lang/String;
    move-object v3, v4

    .line 201
    .end local v4           #cmdLine2:[Ljava/lang/String;
    :cond_b
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v12

    .line 202
    new-instance v2, Ljava/io/BufferedReader;

    .end local v2           #bufferedReader:Ljava/io/BufferedReader;
    new-instance v13, Ljava/io/InputStreamReader;

    invoke-virtual {v12}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 207
    .restart local v2       #bufferedReader:Ljava/io/BufferedReader;
    :goto_9
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    .restart local v9       #line:Ljava/lang/String;
    if-eqz v9, :cond_d

    .line 208
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 170
    .end local v3           #cmdLine:[Ljava/lang/String;
    .end local v5           #commandLine:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9           #line:Ljava/lang/String;
    :cond_c
    const-string v13, "logcat -d -v time\n"

    invoke-virtual {v10, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 210
    .restart local v3       #cmdLine:[Ljava/lang/String;
    .restart local v5       #commandLine:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v9       #line:Ljava/lang/String;
    :cond_d
    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    .line 211
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    goto/16 :goto_4

    .line 229
    :cond_e
    const-string v13, "logcat -d\n"

    invoke-virtual {v10, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 242
    :cond_f
    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    .line 243
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_3

    .line 264
    .end local v3           #cmdLine:[Ljava/lang/String;
    .end local v5           #commandLine:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9           #line:Ljava/lang/String;
    .local v6, e:Ljava/io/IOException;
    :cond_10
    :try_start_5
    const-string v13, "logcat -d\n"

    invoke-virtual {v10, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 277
    .restart local v9       #line:Ljava/lang/String;
    :cond_11
    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_3
.end method

.method private collectPhoneInfo()Ljava/lang/String;
    .locals 4

    .prologue
    .line 329
    const-string v0, "Carrier:%s\nModel:%s\nFirmware:%s\n"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public collect(Landroid/content/Context;Z)Z
    .locals 17
    .parameter "context"
    .parameter "writeToFile"

    .prologue
    .line 341
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mLastLogs:Ljava/util/ArrayList;

    .line 342
    .local v2, lines:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-boolean v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v1, :cond_0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V

    .line 344
    :goto_0
    if-eqz p2, :cond_4

    .line 345
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 347
    const/16 v16, 0x0

    .line 348
    .local v16, sb:Ljava/lang/StringBuilder;
    const-string v15, ""

    .line 350
    .local v15, preface:Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v15, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 354
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Lucky Patcher "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 356
    invoke-direct/range {p0 .. p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->collectPhoneInfo()Ljava/lang/String;

    move-result-object v14

    .line 357
    .local v14, phoneInfo:Ljava/lang/String;
    const-string v1, "\n"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 361
    :goto_2
    :try_start_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .local v10, line:Ljava/lang/String;
    const-string v3, "\n"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/util/ConcurrentModificationException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 364
    .end local v10           #line:Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 365
    .local v8, e:Ljava/util/ConcurrentModificationException;
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "LuckyPatcher (LogCollector): try again collect log."

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 366
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 367
    const-string v1, "\n"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 343
    .end local v8           #e:Ljava/util/ConcurrentModificationException;
    .end local v14           #phoneInfo:Ljava/lang/String;
    .end local v15           #preface:Ljava/lang/String;
    .end local v16           #sb:Ljava/lang/StringBuilder;
    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 351
    .restart local v15       #preface:Ljava/lang/String;
    .restart local v16       #sb:Ljava/lang/StringBuilder;
    :catch_1
    move-exception v8

    .line 352
    .local v8, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v8}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 370
    .end local v8           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v14       #phoneInfo:Ljava/lang/String;
    :cond_1
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 372
    .local v7, content:Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    const-string v1, "abrakakdabra"

    invoke-direct {v12, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 376
    .local v12, logfile:Ljava/io/File;
    :try_start_2
    new-instance v13, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/Log/log.txt"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v13, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 377
    .end local v12           #logfile:Ljava/io/File;
    .local v13, logfile:Ljava/io/File;
    :try_start_3
    new-instance v11, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/Log"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v11, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 378
    .local v11, logdir:Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->mkdirs()Z

    .line 379
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 380
    :cond_2
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Log/log.zip"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 381
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Log/log.zip"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 382
    :cond_3
    invoke-virtual {v13}, Ljava/io/File;->createNewFile()Z

    .line 383
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 385
    .local v9, fout:Ljava/io/FileOutputStream;
    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 393
    .end local v7           #content:Ljava/lang/String;
    .end local v9           #fout:Ljava/io/FileOutputStream;
    .end local v11           #logdir:Ljava/io/File;
    .end local v13           #logfile:Ljava/io/File;
    .end local v14           #phoneInfo:Ljava/lang/String;
    .end local v15           #preface:Ljava/lang/String;
    .end local v16           #sb:Ljava/lang/StringBuilder;
    :cond_4
    :goto_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_5

    const/4 v1, 0x1

    :goto_5
    return v1

    .line 387
    .restart local v7       #content:Ljava/lang/String;
    .restart local v12       #logfile:Ljava/io/File;
    .restart local v14       #phoneInfo:Ljava/lang/String;
    .restart local v15       #preface:Ljava/lang/String;
    .restart local v16       #sb:Ljava/lang/StringBuilder;
    :catch_2
    move-exception v8

    .line 389
    .local v8, e:Ljava/io/IOException;
    :goto_6
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 393
    .end local v7           #content:Ljava/lang/String;
    .end local v8           #e:Ljava/io/IOException;
    .end local v12           #logfile:Ljava/io/File;
    .end local v14           #phoneInfo:Ljava/lang/String;
    .end local v15           #preface:Ljava/lang/String;
    .end local v16           #sb:Ljava/lang/StringBuilder;
    :cond_5
    const/4 v1, 0x0

    goto :goto_5

    .line 387
    .restart local v7       #content:Ljava/lang/String;
    .restart local v13       #logfile:Ljava/io/File;
    .restart local v14       #phoneInfo:Ljava/lang/String;
    .restart local v15       #preface:Ljava/lang/String;
    .restart local v16       #sb:Ljava/lang/StringBuilder;
    :catch_3
    move-exception v8

    move-object v12, v13

    .end local v13           #logfile:Ljava/io/File;
    .restart local v12       #logfile:Ljava/io/File;
    goto :goto_6
.end method

.method public hasForceCloseHappened()Z
    .locals 14

    .prologue
    .line 288
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v2, "*:E"

    aput-object v2, v4, v0

    .line 289
    .local v4, filterSpecs:[Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 290
    .local v1, lines:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-boolean v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v0, :cond_1

    const-string v2, "time"

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V

    .line 293
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 294
    const/4 v8, 0x0

    .line 295
    .local v8, forceClosedSinceLastCheck:Z
    const/4 v7, 0x0

    .line 296
    .local v7, error_found:Z
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 298
    .local v10, line:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v11

    .line 299
    .local v11, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v11}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    .line 300
    .local v9, isMyStackTrace:Z
    iget-object v12, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPrefs:Landroid/content/SharedPreferences;

    .line 301
    .local v12, prefs:Landroid/content/SharedPreferences;
    if-eqz v9, :cond_0

    .line 303
    const/4 v7, 0x1

    .line 304
    const/4 v2, 0x1

    invoke-virtual {v11, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v13

    .line 305
    .local v13, timestamp:Ljava/lang/String;
    const/4 v2, 0x0

    invoke-interface {v12, v13, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 309
    .local v6, appeared:Z
    if-nez v6, :cond_0

    .line 311
    const/4 v8, 0x1

    .line 312
    invoke-interface {v12}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v13, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .line 291
    .end local v6           #appeared:Z
    .end local v7           #error_found:Z
    .end local v8           #forceClosedSinceLastCheck:Z
    .end local v9           #isMyStackTrace:Z
    .end local v10           #line:Ljava/lang/String;
    .end local v11           #matcher:Ljava/util/regex/Matcher;
    .end local v12           #prefs:Landroid/content/SharedPreferences;
    .end local v13           #timestamp:Ljava/lang/String;
    :cond_1
    const-string v2, "time"

    const/4 v3, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V

    goto :goto_0

    .line 316
    .restart local v7       #error_found:Z
    .restart local v8       #forceClosedSinceLastCheck:Z
    :cond_2
    if-nez v8, :cond_3

    if-nez v7, :cond_3

    .line 317
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Clear all FC logcat prefs..."

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 325
    .end local v7           #error_found:Z
    .end local v8           #forceClosedSinceLastCheck:Z
    :cond_3
    :goto_2
    return v8

    .line 322
    :cond_4
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Clear all FC logcat prefs..."

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 323
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 325
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public sendLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15
    .parameter "context"
    .parameter "email"
    .parameter "subject"
    .parameter "preface"

    .prologue
    .line 398
    new-instance v7, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Log/log.txt"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v7, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 399
    .local v7, logfile:Ljava/io/File;
    const-string v11, "file://"

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 401
    .local v9, uri:Landroid/net/Uri;
    :try_start_0
    new-instance v10, Lnet/lingala/zip4j/core/ZipFile;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Log/log.zip"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 402
    .local v10, zip:Lnet/lingala/zip4j/core/ZipFile;
    new-instance v8, Lnet/lingala/zip4j/model/ZipParameters;

    invoke-direct {v8}, Lnet/lingala/zip4j/model/ZipParameters;-><init>()V

    .line 404
    .local v8, parameters:Lnet/lingala/zip4j/model/ZipParameters;
    const/16 v11, 0x8

    invoke-virtual {v8, v11}, Lnet/lingala/zip4j/model/ZipParameters;->setCompressionMethod(I)V

    .line 406
    const/4 v11, 0x5

    invoke-virtual {v8, v11}, Lnet/lingala/zip4j/model/ZipParameters;->setCompressionLevel(I)V

    .line 407
    invoke-virtual {v10, v7, v8}, Lnet/lingala/zip4j/core/ZipFile;->addFile(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V

    .line 408
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "file://"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Log/log.zip"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v9

    .line 416
    .end local v8           #parameters:Lnet/lingala/zip4j/model/ZipParameters;
    .end local v10           #zip:Lnet/lingala/zip4j/core/ZipFile;
    :goto_0
    const-string v2, "not found"

    .line 417
    .local v2, dalvikvm_file:Ljava/lang/String;
    const-string v1, ""

    .line 418
    .local v1, content:Ljava/lang/String;
    const-string v11, "/system/bin/dalvikvm"

    invoke-static {v11}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    const-string v2, "/system/bin/dalvikvm"

    .line 419
    :cond_0
    const-string v11, "/system/bin/dalvikvm32"

    invoke-static {v11}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    const-string v2, "/system/bin/dalvikvm32"

    .line 420
    :cond_1
    sget-object v11, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-string v13, "sdcard"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v12

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 421
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Email contain log.txt with bugs by Lucky Patcher! Please describe the Problem and tap to send email.\n\n\nRoot:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-boolean v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Runtime: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lcom/chelpus/Utils;->getCurrentRuntimeValue()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "DeviceID: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "android_id"

    invoke-static {v12, v13}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Lucky Patcher ver: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->version:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "LP files directory: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "dalvikvm file:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 426
    :goto_1
    :try_start_1
    invoke-static {}, Ljava/lang/System;->getenv()Ljava/util/Map;

    move-result-object v5

    .line 427
    .local v5, envs:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 429
    .local v4, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, "\n"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, ":"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 430
    goto :goto_2

    .line 410
    .end local v1           #content:Ljava/lang/String;
    .end local v2           #dalvikvm_file:Ljava/lang/String;
    .end local v4           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5           #envs:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_0
    move-exception v3

    .line 412
    .local v3, e:Lnet/lingala/zip4j/exception/ZipException;
    invoke-virtual {v3}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    goto/16 :goto_0

    .line 423
    .end local v3           #e:Lnet/lingala/zip4j/exception/ZipException;
    .restart local v1       #content:Ljava/lang/String;
    .restart local v2       #dalvikvm_file:Ljava/lang/String;
    :cond_2
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Email contain log.txt with bugs by Lucky Patcher! Please describe the Problem and tap to send email.\n\n\nRoot:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-boolean v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Runtime: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lcom/chelpus/Utils;->getCurrentRuntimeValue()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "DeviceID: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "android_id"

    invoke-static {v12, v13}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Lucky Patcher ver: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->version:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "LP files directory: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "dalvikvm file:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 432
    :catch_1
    move-exception v3

    .local v3, e:Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 433
    .end local v3           #e:Ljava/lang/Exception;
    :cond_3
    new-instance v6, Landroid/content/Intent;

    const-string v11, "android.intent.action.SEND"

    invoke-direct {v6, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 434
    .local v6, intent:Landroid/content/Intent;
    const-string v11, "text/plain"

    invoke-virtual {v6, v11}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 435
    const-string v11, "android.intent.extra.EMAIL"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    aput-object p2, v12, v13

    invoke-virtual {v6, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 436
    const-string v11, "android.intent.extra.SUBJECT"

    move-object/from16 v0, p3

    invoke-virtual {v6, v11, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 437
    const-string v11, "android.intent.extra.TEXT"

    invoke-virtual {v6, v11, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 439
    const-string v11, "android.intent.extra.STREAM"

    invoke-virtual {v6, v11, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 442
    :try_start_2
    sget-object v11, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-string v12, "Send mail"

    invoke-static {v6, v12}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    .line 444
    :goto_3
    return-void

    .line 443
    :catch_2
    move-exception v3

    .local v3, e:Ljava/lang/RuntimeException;
    invoke-virtual {v3}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_3

    .line 413
    .end local v1           #content:Ljava/lang/String;
    .end local v2           #dalvikvm_file:Ljava/lang/String;
    .end local v3           #e:Ljava/lang/RuntimeException;
    .end local v6           #intent:Landroid/content/Intent;
    :catch_3
    move-exception v11

    goto/16 :goto_0
.end method
