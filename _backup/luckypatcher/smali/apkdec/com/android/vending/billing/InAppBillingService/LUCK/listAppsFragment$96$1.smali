.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;)V
    .locals 0
    .parameter "this$1"

    .prologue
    .line 12639
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 12642
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    #getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->apkitem:Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;
    invoke-static {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->access$1900(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;

    move-result-object v6

    iget-object v4, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->pkgName:Ljava/lang/String;

    .line 12643
    .local v4, pkgName:Ljava/lang/String;
    const-string v0, ""

    .line 12644
    .local v0, app_dir:Ljava/lang/String;
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v6, :cond_3

    .line 12647
    :try_start_0
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v4, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-object v6, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v6, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 12648
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "LuckyAppManager (restore): app uzhe ustanovleno, restore from selected backup."

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 12649
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LuckyAppManager (restore):"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 12653
    :goto_0
    :try_start_1
    const-string v6, ""

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 12654
    const/4 v6, 0x0

    invoke-static {v0, v6}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    const-string v7, "RW"

    invoke-static {v6, v7}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 12655
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rm "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v0, v7}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 12656
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    #getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->apkitem:Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;
    invoke-static {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->access$1900(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;

    move-result-object v7

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->backupfile:Ljava/io/File;

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 12658
    new-instance v6, Lcom/chelpus/Utils;

    const-string v7, ""

    invoke-direct {v6, v7}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "pm install -r -i com.android.vending \'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    #getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->apkitem:Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;
    invoke-static {v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->access$1900(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;

    move-result-object v10

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->backupfile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v6, v7}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 12659
    .local v5, result:Ljava/lang/String;
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->errorOutput:Ljava/lang/String;

    .line 12660
    .local v3, error:Ljava/lang/String;
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result pm:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 12661
    const-string v1, ""

    .line 12662
    .local v1, app_dir_new:Ljava/lang/String;
    new-instance v6, Lcom/chelpus/Utils;

    const-string v7, "w"

    invoke-direct {v6, v7}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const-wide/16 v7, 0xfa0

    invoke-virtual {v6, v7, v8}, Lcom/chelpus/Utils;->waitLP(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 12664
    :try_start_2
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v4, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-object v6, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v6, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 12665
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LuckyAppManager (restore): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 12668
    :goto_1
    :try_start_3
    const-string v6, "Success"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->errorOutput:Ljava/lang/String;

    const-string v7, "Success"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, ""

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 12670
    :cond_1
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1$1;

    invoke-direct {v7, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;)V

    invoke-virtual {v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 12837
    .end local v1           #app_dir_new:Ljava/lang/String;
    .end local v3           #error:Ljava/lang/String;
    .end local v5           #result:Ljava/lang/String;
    :goto_2
    return-void

    .line 12684
    .restart local v1       #app_dir_new:Ljava/lang/String;
    .restart local v3       #error:Ljava/lang/String;
    .restart local v5       #result:Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1$2;

    invoke-direct {v7, p0, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 12749
    .end local v1           #app_dir_new:Ljava/lang/String;
    .end local v3           #error:Ljava/lang/String;
    .end local v5           #result:Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 12750
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 12830
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1$7;

    invoke-direct {v7, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;)V

    invoke-virtual {v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto :goto_2

    .line 12754
    .end local v2           #e:Ljava/lang/Exception;
    :cond_3
    :try_start_4
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    #getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->apkitem:Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;
    invoke-static {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->access$1900(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;

    move-result-object v7

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->pkgName:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 12813
    :goto_3
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1$6;

    invoke-direct {v7, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;)V

    invoke-virtual {v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto :goto_2

    .line 12755
    :catch_1
    move-exception v2

    .line 12756
    .local v2, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "install_non_market_apps"

    invoke-static {v6, v7, v9}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    .line 12757
    .local v5, result:I
    if-nez v5, :cond_4

    .line 12759
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1$3;

    invoke-direct {v7, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;)V

    invoke-virtual {v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 12783
    :cond_4
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1$4;

    invoke-direct {v7, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;)V

    invoke-virtual {v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto :goto_2

    .line 12799
    .end local v2           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v5           #result:I
    :catch_2
    move-exception v2

    .line 12800
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 12802
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1$5;

    invoke-direct {v7, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$96$1;)V

    invoke-virtual {v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto :goto_3

    .line 12666
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v1       #app_dir_new:Ljava/lang/String;
    .restart local v3       #error:Ljava/lang/String;
    .local v5, result:Ljava/lang/String;
    :catch_3
    move-exception v6

    goto/16 :goto_1

    .line 12650
    .end local v1           #app_dir_new:Ljava/lang/String;
    .end local v3           #error:Ljava/lang/String;
    .end local v5           #result:Ljava/lang/String;
    :catch_4
    move-exception v6

    goto/16 :goto_0
.end method
