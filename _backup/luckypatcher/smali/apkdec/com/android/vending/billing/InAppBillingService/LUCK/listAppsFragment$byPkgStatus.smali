.class final Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$byPkgStatus;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "byPkgStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .locals 0
    .parameter "this$0"

    .prologue
    .line 6727
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$byPkgStatus;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)I
    .locals 6
    .parameter "a"
    .parameter "b"

    .prologue
    .line 6729
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 6730
    :cond_0
    new-instance v4, Ljava/lang/ClassCastException;

    invoke-direct {v4}, Ljava/lang/ClassCastException;-><init>()V

    throw v4

    .line 6733
    :cond_1
    iget v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget v5, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v1

    .line 6734
    .local v1, ret:I
    if-eqz v1, :cond_2

    .line 6757
    .end local v1           #ret:I
    :goto_0
    return v1

    .line 6737
    .restart local v1       #ret:I
    :cond_2
    iget v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    if-nez v4, :cond_b

    .line 6738
    const/16 v2, 0xff

    .line 6739
    .local v2, statA:I
    const/16 v3, 0xff

    .line 6740
    .local v3, statB:I
    iget-boolean v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-nez v4, :cond_3

    iget-boolean v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-nez v4, :cond_3

    const/4 v2, 0x4

    .line 6741
    :cond_3
    iget-boolean v4, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-nez v4, :cond_4

    iget-boolean v4, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-nez v4, :cond_4

    const/4 v3, 0x4

    .line 6742
    :cond_4
    iget-boolean v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-eqz v4, :cond_5

    const/4 v2, 0x3

    .line 6743
    :cond_5
    iget-boolean v4, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-eqz v4, :cond_6

    const/4 v3, 0x3

    .line 6744
    :cond_6
    iget-boolean v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-eqz v4, :cond_7

    const/4 v2, 0x2

    .line 6745
    :cond_7
    iget-boolean v4, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-eqz v4, :cond_8

    const/4 v3, 0x2

    .line 6746
    :cond_8
    iget-boolean v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-eqz v4, :cond_9

    const/4 v2, 0x1

    .line 6747
    :cond_9
    iget-boolean v4, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-eqz v4, :cond_a

    const/4 v3, 0x1

    .line 6748
    :cond_a
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v1

    goto :goto_0

    .line 6754
    .end local v2           #statA:I
    .end local v3           #statB:I
    :cond_b
    :try_start_0
    invoke-virtual {p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 6755
    :catch_0
    move-exception v0

    .line 6756
    .local v0, e:Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 6757
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 6727
    check-cast p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    check-cast p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-virtual {p0, p1, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$byPkgStatus;->compare(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)I

    move-result v0

    return v0
.end method
