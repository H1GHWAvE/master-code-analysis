.class public Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "MenuItemAdapter.java"


# static fields
.field public static final TEXT_DEFAULT:I = 0x0

.field public static final TEXT_LARGE:I = 0x2

.field public static final TEXT_MEDIUM:I = 0x1

.field public static final TEXT_SMALL:I


# instance fields
.field public context:Landroid/content/Context;

.field public groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

.field public groupsViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private imgIcon:Landroid/widget/ImageView;

.field private size:I

.field public sorter:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 2
    .parameter "context"
    .parameter "sizeText"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, groups:Ljava/util/List;,"Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;>;"
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 30
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    .line 36
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    .line 99
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groupsViews:Ljava/util/ArrayList;

    .line 47
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    .line 48
    iput p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->size:I

    .line 49
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    invoke-interface {p3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    .line 50
    sput-object p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->menu_adapter:Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 3
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, groups:Ljava/util/List;,"Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;>;"
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 30
    new-array v0, v1, [Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    .line 36
    iput-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    .line 99
    iput-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groupsViews:Ljava/util/ArrayList;

    .line 40
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    .line 41
    iput v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->size:I

    .line 42
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    invoke-interface {p2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    .line 43
    sput-object p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->menu_adapter:Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;

    .line 44
    return-void
.end method

.method private getColor(I)I
    .locals 2
    .parameter "group"

    .prologue
    .line 913
    const-string v1, "#DDDDDD"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 914
    .local v0, color:I
    sparse-switch p1, :sswitch_data_0

    .line 964
    :goto_0
    return v0

    .line 916
    :sswitch_0
    const-string v1, "#99cccc"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 917
    goto :goto_0

    .line 919
    :sswitch_1
    const-string v1, "#99cccc"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 920
    goto :goto_0

    .line 922
    :sswitch_2
    const-string v1, "#c2f055"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 923
    goto :goto_0

    .line 925
    :sswitch_3
    const-string v1, "#c2f055"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 926
    goto :goto_0

    .line 928
    :sswitch_4
    const-string v1, "#c2f055"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 929
    goto :goto_0

    .line 931
    :sswitch_5
    const-string v1, "#cc99cc"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 932
    goto :goto_0

    .line 934
    :sswitch_6
    const-string v1, "#cc99cc"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 935
    goto :goto_0

    .line 937
    :sswitch_7
    const-string v1, "#cc99cc"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 938
    goto :goto_0

    .line 940
    :sswitch_8
    const-string v1, "#cc99cc"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 941
    goto :goto_0

    .line 943
    :sswitch_9
    const-string v1, "#ffffbb"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 944
    goto :goto_0

    .line 946
    :sswitch_a
    const-string v1, "#c2f055"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 947
    goto :goto_0

    .line 949
    :sswitch_b
    const-string v1, "#c2f055"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 950
    goto :goto_0

    .line 952
    :sswitch_c
    const-string v1, "#fe6c00"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 953
    goto :goto_0

    .line 955
    :sswitch_d
    const-string v1, "#ffffbb"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 956
    goto :goto_0

    .line 958
    :sswitch_e
    const-string v1, "#c2f055"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 959
    goto :goto_0

    .line 961
    :sswitch_f
    const-string v1, "#fe6c00"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 914
    :sswitch_data_0
    .sparse-switch
        0x7f07003b -> :sswitch_0
        0x7f070044 -> :sswitch_e
        0x7f07004b -> :sswitch_1
        0x7f07004e -> :sswitch_a
        0x7f0700bf -> :sswitch_5
        0x7f0700e5 -> :sswitch_4
        0x7f070150 -> :sswitch_8
        0x7f07016a -> :sswitch_7
        0x7f070190 -> :sswitch_9
        0x7f0701be -> :sswitch_c
        0x7f0701c4 -> :sswitch_3
        0x7f0701c6 -> :sswitch_b
        0x7f0701cf -> :sswitch_f
        0x7f0701da -> :sswitch_d
        0x7f07021c -> :sswitch_2
        0x7f070239 -> :sswitch_6
    .end sparse-switch
.end method


# virtual methods
.method public add(Ljava/util/ArrayList;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, groupsIn:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    .line 55
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->notifyDataSetChanged()V

    .line 56
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 675
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    .line 676
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groupsViews:Ljava/util/ArrayList;

    .line 677
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->notifyDataSetChanged()V

    .line 678
    return-void
.end method

.method public getChild(II)Ljava/lang/Integer;
    .locals 1
    .parameter "groupPosition"
    .parameter "childPosition"

    .prologue
    .line 200
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 201
    :cond_0
    const/4 v0, 0x0

    .line 203
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->childs:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    goto :goto_0
.end method

.method public bridge synthetic getChild(II)Ljava/lang/Object;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->getChild(II)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2
    .parameter "groupPosition"
    .parameter "childPosition"

    .prologue
    .line 81
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 44
    .parameter "groupPosition"
    .parameter "childPosition"
    .parameter "isLastChild"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 213
    invoke-virtual/range {p0 .. p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    move-result-object v27

    .line 214
    .local v27, item:Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;
    invoke-virtual/range {p0 .. p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->getChild(II)Ljava/lang/Integer;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 216
    .local v3, child_item:I
    packed-switch v3, :pswitch_data_0

    .line 651
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v41, v0

    const-string v42, "layout_inflater"

    invoke-virtual/range {v41 .. v42}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/view/LayoutInflater;

    .line 652
    .local v20, inflater3:Landroid/view/LayoutInflater;
    const v41, 0x7f04001c

    const/16 v42, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v41

    move-object/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 654
    const v41, 0x7f0d007e

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v40

    check-cast v40, Landroid/widget/TextView;

    .line 655
    .local v40, textChild:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v41, v0

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v42

    invoke-virtual/range {v40 .. v42}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 656
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v41, v0

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v42

    invoke-virtual/range {v40 .. v42}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 657
    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 658
    const v41, 0x7f0d007d

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/ImageView;

    .line 659
    .local v18, image:Landroid/widget/ImageView;
    move-object/from16 v0, v27

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu:I

    move/from16 v41, v0

    move-object/from16 v0, p0

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->getImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v41

    move-object/from16 v0, v18

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 660
    invoke-virtual/range {p0 .. p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    move-result-object v41

    move-object/from16 v0, v41

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu:I

    move/from16 v41, v0

    move-object/from16 v0, p0

    move/from16 v1, v41

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->getColor(I)I

    move-result v11

    .line 661
    .local v11, color:I
    sget-object v41, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, v18

    move-object/from16 v1, v41

    invoke-virtual {v0, v11, v1}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 662
    move-object/from16 v0, v40

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 666
    .end local v11           #color:I
    .end local v18           #image:Landroid/widget/ImageView;
    .end local v20           #inflater3:Landroid/view/LayoutInflater;
    .end local v40           #textChild:Landroid/widget/TextView;
    :goto_0
    return-object p4

    .line 219
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v41, v0

    const-string v42, "layout_inflater"

    invoke-virtual/range {v41 .. v42}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/view/LayoutInflater;

    .line 220
    .local v19, inflater:Landroid/view/LayoutInflater;
    const v41, 0x7f04001b

    const/16 v42, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v41

    move-object/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 222
    const v41, 0x7f0d0079

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/RadioGroup;

    .line 223
    .local v12, group:Landroid/widget/RadioGroup;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v41

    const-string v42, "viewsize"

    const/16 v43, 0x0

    invoke-interface/range {v41 .. v43}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v41

    packed-switch v41, :pswitch_data_1

    .line 237
    :goto_1
    new-instance v41, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$1;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;)V

    move-object/from16 v0, v41

    invoke-virtual {v12, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    goto :goto_0

    .line 225
    :pswitch_1
    const v41, 0x7f0d007a

    move/from16 v0, v41

    invoke-virtual {v12, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v39

    check-cast v39, Landroid/widget/RadioButton;

    .line 226
    .local v39, rb_small:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v39

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1

    .line 229
    .end local v39           #rb_small:Landroid/widget/RadioButton;
    :pswitch_2
    const v41, 0x7f0d007b

    move/from16 v0, v41

    invoke-virtual {v12, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v36

    check-cast v36, Landroid/widget/RadioButton;

    .line 230
    .local v36, rb_medium:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v36

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1

    .line 233
    .end local v36           #rb_medium:Landroid/widget/RadioButton;
    :pswitch_3
    const v41, 0x7f0d007c

    move/from16 v0, v41

    invoke-virtual {v12, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v35

    check-cast v35, Landroid/widget/RadioButton;

    .line 234
    .local v35, rb_large:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v35

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1

    .line 264
    .end local v12           #group:Landroid/widget/RadioGroup;
    .end local v19           #inflater:Landroid/view/LayoutInflater;
    .end local v35           #rb_large:Landroid/widget/RadioButton;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v41, v0

    const-string v42, "layout_inflater"

    invoke-virtual/range {v41 .. v42}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/view/LayoutInflater;

    .line 265
    .local v21, inflater4:Landroid/view/LayoutInflater;
    const v41, 0x7f040018

    const/16 v42, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v41

    move-object/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 267
    const v41, 0x7f0d006d

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RadioGroup;

    .line 268
    .local v13, group4:Landroid/widget/RadioGroup;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v41

    const-string v42, "orientstion"

    const/16 v43, 0x3

    invoke-interface/range {v41 .. v43}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v41

    packed-switch v41, :pswitch_data_2

    .line 282
    :goto_2
    new-instance v41, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$2;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;)V

    move-object/from16 v0, v41

    invoke-virtual {v13, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 270
    :pswitch_5
    const v41, 0x7f0d006e

    move/from16 v0, v41

    invoke-virtual {v13, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Landroid/widget/RadioButton;

    .line 271
    .local v37, rb_portret:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v37

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_2

    .line 274
    .end local v37           #rb_portret:Landroid/widget/RadioButton;
    :pswitch_6
    const v41, 0x7f0d0070

    move/from16 v0, v41

    invoke-virtual {v13, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/RadioButton;

    .line 275
    .local v34, rb_landscape:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v34

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_2

    .line 278
    .end local v34           #rb_landscape:Landroid/widget/RadioButton;
    :pswitch_7
    const v41, 0x7f0d006f

    move/from16 v0, v41

    invoke-virtual {v13, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v38

    check-cast v38, Landroid/widget/RadioButton;

    .line 279
    .local v38, rb_sensor:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v38

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_2

    .line 309
    .end local v13           #group4:Landroid/widget/RadioGroup;
    .end local v21           #inflater4:Landroid/view/LayoutInflater;
    .end local v38           #rb_sensor:Landroid/widget/RadioButton;
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v41, v0

    const-string v42, "layout_inflater"

    invoke-virtual/range {v41 .. v42}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/view/LayoutInflater;

    .line 310
    .local v22, inflater5:Landroid/view/LayoutInflater;
    const v41, 0x7f04001a

    const/16 v42, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v41

    move-object/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 312
    const v41, 0x7f0d0075

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/RadioGroup;

    .line 313
    .local v14, group5:Landroid/widget/RadioGroup;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v41

    const-string v42, "sortby"

    const/16 v43, 0x2

    invoke-interface/range {v41 .. v43}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v41

    packed-switch v41, :pswitch_data_3

    .line 327
    :goto_3
    new-instance v41, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$3;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;)V

    move-object/from16 v0, v41

    invoke-virtual {v14, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 315
    :pswitch_9
    const v41, 0x7f0d0076

    move/from16 v0, v41

    invoke-virtual {v14, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Landroid/widget/RadioButton;

    .line 316
    .restart local v37       #rb_portret:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v37

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_3

    .line 319
    .end local v37           #rb_portret:Landroid/widget/RadioButton;
    :pswitch_a
    const v41, 0x7f0d0077

    move/from16 v0, v41

    invoke-virtual {v14, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/RadioButton;

    .line 320
    .restart local v34       #rb_landscape:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v34

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_3

    .line 323
    .end local v34           #rb_landscape:Landroid/widget/RadioButton;
    :pswitch_b
    const v41, 0x7f0d0078

    move/from16 v0, v41

    invoke-virtual {v14, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v38

    check-cast v38, Landroid/widget/RadioButton;

    .line 324
    .restart local v38       #rb_sensor:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v38

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_3

    .line 354
    .end local v14           #group5:Landroid/widget/RadioGroup;
    .end local v22           #inflater5:Landroid/view/LayoutInflater;
    .end local v38           #rb_sensor:Landroid/widget/RadioButton;
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v41, v0

    const-string v42, "layout_inflater"

    invoke-virtual/range {v41 .. v42}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/view/LayoutInflater;

    .line 355
    .local v23, inflater6:Landroid/view/LayoutInflater;
    const v41, 0x7f040013

    const/16 v42, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v41

    move-object/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 357
    const v41, 0x7f0d005a

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    .line 358
    .local v7, chkLVL:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v41

    const-string v42, "lvlapp"

    const/16 v43, 0x1

    invoke-interface/range {v41 .. v43}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v41

    move/from16 v0, v41

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 359
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "\n"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const v42, 0x7f07014d

    invoke-static/range {v42 .. v42}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    const v42, -0x777778

    const-string v43, ""

    invoke-static/range {v41 .. v43}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->append(Ljava/lang/CharSequence;)V

    .line 360
    new-instance v41, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$4;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;)V

    move-object/from16 v0, v41

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 369
    const v41, 0x7f0d005b

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 370
    .local v4, chkADS:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v41

    const-string v42, "adsapp"

    const/16 v43, 0x1

    invoke-interface/range {v41 .. v43}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v41

    move/from16 v0, v41

    invoke-virtual {v4, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 371
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "\n"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const v42, 0x7f07000b

    invoke-static/range {v42 .. v42}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    const v42, -0x777778

    const-string v43, ""

    invoke-static/range {v41 .. v43}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Landroid/widget/CheckBox;->append(Ljava/lang/CharSequence;)V

    .line 372
    new-instance v41, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$5;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;)V

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 381
    const v41, 0x7f0d005c

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    .line 382
    .local v5, chkCustom:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v41

    const-string v42, "customapp"

    const/16 v43, 0x1

    invoke-interface/range {v41 .. v43}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v41

    move/from16 v0, v41

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 383
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "\n"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const v42, 0x7f0700d8

    invoke-static/range {v42 .. v42}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    const v42, -0x777778

    const-string v43, ""

    invoke-static/range {v41 .. v43}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->append(Ljava/lang/CharSequence;)V

    .line 384
    new-instance v41, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$6;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;)V

    move-object/from16 v0, v41

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 393
    const v41, 0x7f0d005d

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/CheckBox;

    .line 394
    .local v8, chkMOD:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v41

    const-string v42, "modifapp"

    const/16 v43, 0x1

    invoke-interface/range {v41 .. v43}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v41

    move/from16 v0, v41

    invoke-virtual {v8, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 395
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "\n"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const v42, 0x7f07016f

    invoke-static/range {v42 .. v42}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    const v42, -0x777778

    const-string v43, ""

    invoke-static/range {v41 .. v43}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v8, v0}, Landroid/widget/CheckBox;->append(Ljava/lang/CharSequence;)V

    .line 396
    new-instance v41, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$7;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;)V

    move-object/from16 v0, v41

    invoke-virtual {v8, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 405
    const v41, 0x7f0d005e

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    .line 406
    .local v6, chkFIX:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v41

    const-string v42, "fixedapp"

    const/16 v43, 0x1

    invoke-interface/range {v41 .. v43}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v41

    move/from16 v0, v41

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 407
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "\n"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const v42, 0x7f070122

    invoke-static/range {v42 .. v42}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    const v42, -0x777778

    const-string v43, ""

    invoke-static/range {v41 .. v43}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->append(Ljava/lang/CharSequence;)V

    .line 408
    new-instance v41, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$8;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$8;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;)V

    move-object/from16 v0, v41

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 417
    const v41, 0x7f0d005f

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/CheckBox;

    .line 418
    .local v9, chkNONE:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v41

    const-string v42, "noneapp"

    const/16 v43, 0x1

    invoke-interface/range {v41 .. v43}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v41

    move/from16 v0, v41

    invoke-virtual {v9, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 419
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "\n"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const v42, 0x7f070186

    invoke-static/range {v42 .. v42}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    const v42, -0x777778

    const-string v43, ""

    invoke-static/range {v41 .. v43}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v9, v0}, Landroid/widget/CheckBox;->append(Ljava/lang/CharSequence;)V

    .line 420
    new-instance v41, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$9;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$9;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;)V

    move-object/from16 v0, v41

    invoke-virtual {v9, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 429
    const v41, 0x7f0d0060

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    .line 430
    .local v10, chkSYS:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v41

    const-string v42, "systemapp"

    const/16 v43, 0x0

    invoke-interface/range {v41 .. v43}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v41

    move/from16 v0, v41

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 431
    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "\n"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const v42, 0x7f070213

    invoke-static/range {v42 .. v42}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    const v42, -0x777778

    const-string v43, ""

    invoke-static/range {v41 .. v43}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->append(Ljava/lang/CharSequence;)V

    .line 432
    new-instance v41, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$10;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$10;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;)V

    move-object/from16 v0, v41

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 444
    .end local v4           #chkADS:Landroid/widget/CheckBox;
    .end local v5           #chkCustom:Landroid/widget/CheckBox;
    .end local v6           #chkFIX:Landroid/widget/CheckBox;
    .end local v7           #chkLVL:Landroid/widget/CheckBox;
    .end local v8           #chkMOD:Landroid/widget/CheckBox;
    .end local v9           #chkNONE:Landroid/widget/CheckBox;
    .end local v10           #chkSYS:Landroid/widget/CheckBox;
    .end local v23           #inflater6:Landroid/view/LayoutInflater;
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v41, v0

    const-string v42, "layout_inflater"

    invoke-virtual/range {v41 .. v42}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/view/LayoutInflater;

    .line 445
    .local v24, inflater7:Landroid/view/LayoutInflater;
    const v41, 0x7f040019

    const/16 v42, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v41

    move-object/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 447
    const v41, 0x7f0d0071

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/RadioGroup;

    .line 448
    .local v15, group7:Landroid/widget/RadioGroup;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v41

    const-string v42, "root_force"

    const/16 v43, 0x0

    invoke-interface/range {v41 .. v43}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v41

    packed-switch v41, :pswitch_data_4

    .line 462
    :goto_4
    new-instance v41, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$11;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$11;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;)V

    move-object/from16 v0, v41

    invoke-virtual {v15, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 450
    :pswitch_e
    const v41, 0x7f0d0072

    move/from16 v0, v41

    invoke-virtual {v15, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Landroid/widget/RadioButton;

    .line 451
    .restart local v37       #rb_portret:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v37

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_4

    .line 454
    .end local v37           #rb_portret:Landroid/widget/RadioButton;
    :pswitch_f
    const v41, 0x7f0d0073

    move/from16 v0, v41

    invoke-virtual {v15, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/RadioButton;

    .line 455
    .restart local v34       #rb_landscape:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v34

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_4

    .line 458
    .end local v34           #rb_landscape:Landroid/widget/RadioButton;
    :pswitch_10
    const v41, 0x7f0d0074

    move/from16 v0, v41

    invoke-virtual {v15, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v38

    check-cast v38, Landroid/widget/RadioButton;

    .line 459
    .restart local v38       #rb_sensor:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v38

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_4

    .line 489
    .end local v15           #group7:Landroid/widget/RadioGroup;
    .end local v24           #inflater7:Landroid/view/LayoutInflater;
    .end local v38           #rb_sensor:Landroid/widget/RadioButton;
    :pswitch_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v41, v0

    const-string v42, "layout_inflater"

    invoke-virtual/range {v41 .. v42}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/view/LayoutInflater;

    .line 490
    .local v25, inflater8:Landroid/view/LayoutInflater;
    const v41, 0x7f040017

    const/16 v42, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v41

    move-object/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 492
    const v41, 0x7f0d006a

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/RadioGroup;

    .line 493
    .local v16, group8:Landroid/widget/RadioGroup;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v41

    const-string v42, "apkname"

    const/16 v43, 0x1

    invoke-interface/range {v41 .. v43}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v41

    packed-switch v41, :pswitch_data_5

    .line 503
    :goto_5
    new-instance v41, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$12;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$12;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 495
    :pswitch_12
    const v41, 0x7f0d006b

    move-object/from16 v0, v16

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Landroid/widget/RadioButton;

    .line 496
    .restart local v37       #rb_portret:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v37

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_5

    .line 499
    .end local v37           #rb_portret:Landroid/widget/RadioButton;
    :pswitch_13
    const v41, 0x7f0d006c

    move-object/from16 v0, v16

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/RadioButton;

    .line 500
    .restart local v34       #rb_landscape:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v34

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_5

    .line 525
    .end local v16           #group8:Landroid/widget/RadioGroup;
    .end local v25           #inflater8:Landroid/view/LayoutInflater;
    .end local v34           #rb_landscape:Landroid/widget/RadioButton;
    :pswitch_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v41, v0

    const-string v42, "layout_inflater"

    invoke-virtual/range {v41 .. v42}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/LayoutInflater;

    .line 526
    .local v26, inflater9:Landroid/view/LayoutInflater;
    const v41, 0x7f040016

    const/16 v42, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v41

    move-object/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 528
    const v41, 0x7f0d0063

    move-object/from16 v0, p4

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/RadioGroup;

    .line 529
    .local v17, group9:Landroid/widget/RadioGroup;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v41

    const-string v42, "default_icon_for_lp"

    const/16 v43, 0x0

    invoke-interface/range {v41 .. v43}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v41

    packed-switch v41, :pswitch_data_6

    .line 555
    :goto_6
    new-instance v41, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$13;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter$13;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 531
    :pswitch_15
    const v41, 0x7f0d0064

    move-object/from16 v0, v17

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v28

    check-cast v28, Landroid/widget/RadioButton;

    .line 532
    .local v28, rb0:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v28

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_6

    .line 535
    .end local v28           #rb0:Landroid/widget/RadioButton;
    :pswitch_16
    const v41, 0x7f0d0065

    move-object/from16 v0, v17

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v29

    check-cast v29, Landroid/widget/RadioButton;

    .line 536
    .local v29, rb2:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v29

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_6

    .line 539
    .end local v29           #rb2:Landroid/widget/RadioButton;
    :pswitch_17
    const v41, 0x7f0d0066

    move-object/from16 v0, v17

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/widget/RadioButton;

    .line 540
    .local v30, rb3:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v30

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_6

    .line 543
    .end local v30           #rb3:Landroid/widget/RadioButton;
    :pswitch_18
    const v41, 0x7f0d0067

    move-object/from16 v0, v17

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/RadioButton;

    .line 544
    .local v31, rb4:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v31

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_6

    .line 547
    .end local v31           #rb4:Landroid/widget/RadioButton;
    :pswitch_19
    const v41, 0x7f0d0068

    move-object/from16 v0, v17

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/RadioButton;

    .line 548
    .local v32, rb5:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_6

    .line 551
    .end local v32           #rb5:Landroid/widget/RadioButton;
    :pswitch_1a
    const v41, 0x7f0d0069

    move-object/from16 v0, v17

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/RadioButton;

    .line 552
    .local v33, rb6:Landroid/widget/RadioButton;
    const/16 v41, 0x1

    move-object/from16 v0, v33

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_6

    .line 216
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_8
        :pswitch_c
        :pswitch_d
        :pswitch_11
        :pswitch_14
    .end packed-switch

    .line 223
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 268
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_5
    .end packed-switch

    .line 313
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 448
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 493
    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_12
        :pswitch_13
    .end packed-switch

    .line 529
    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
    .end packed-switch
.end method

.method public getChildrenCount(I)I
    .locals 1
    .parameter "groupPosition"

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 68
    :cond_0
    const/4 v0, 0x0

    .line 70
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->childs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;
    .locals 1
    .parameter "groupPosition"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public bridge synthetic getGroup(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 60
    :cond_0
    const/4 v0, 0x0

    .line 62
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    array-length v0, v0

    goto :goto_0
.end method

.method public getGroupId(I)J
    .locals 2
    .parameter "groupPosition"

    .prologue
    .line 208
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 26
    .parameter "groupPosition"
    .parameter "isExpanded"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 102
    invoke-virtual/range {p0 .. p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    move-result-object v15

    .line 103
    .local v15, menuItem:Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;
    iget v7, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu:I

    .line 105
    .local v7, group:I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groupsViews:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p3

    .end local p3
    check-cast p3, Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    .restart local p3
    :goto_0
    if-nez p3, :cond_0

    .line 108
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    const-string v22, "layout_inflater"

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/view/LayoutInflater;

    .line 109
    .local v12, inflater:Landroid/view/LayoutInflater;
    iget v0, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    move/from16 v21, v0

    packed-switch v21, :pswitch_data_0

    .line 114
    const v21, 0x7f04002c

    const/16 v22, 0x0

    move/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v12, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    .line 116
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groupsViews:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    if-nez v21, :cond_0

    .line 117
    new-instance v21, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->size:I

    move/from16 v22, v0

    invoke-direct/range {v21 .. v22}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groupsViews:Ljava/util/ArrayList;

    .line 118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groups:[Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v23, v0

    const/16 v21, 0x0

    :goto_2
    move/from16 v0, v21

    move/from16 v1, v23

    if-ge v0, v1, :cond_0

    aget-object v13, v22, v21

    .line 119
    .local v13, it:Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groupsViews:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    add-int/lit8 v21, v21, 0x1

    goto :goto_2

    .line 106
    .end local v12           #inflater:Landroid/view/LayoutInflater;
    .end local v13           #it:Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;
    .end local p3
    :catch_0
    move-exception v6

    .local v6, e:Ljava/lang/Exception;
    const/16 p3, 0x0

    .restart local p3
    goto :goto_0

    .line 111
    .end local v6           #e:Ljava/lang/Exception;
    .restart local v12       #inflater:Landroid/view/LayoutInflater;
    :pswitch_0
    const v21, 0x7f04002b

    const/16 v22, 0x0

    move/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v12, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    .line 112
    goto :goto_1

    .line 124
    .end local v12           #inflater:Landroid/view/LayoutInflater;
    :cond_0
    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 125
    iget v0, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    move/from16 v21, v0

    packed-switch v21, :pswitch_data_1

    .line 179
    :goto_3
    iget v0, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu_descr:I

    move/from16 v21, v0

    if-eqz v21, :cond_1

    .line 180
    const v21, 0x7f0d00ba

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 181
    .local v20, textGroupDescr:Landroid/widget/TextView;
    iget v0, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu_descr:I

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    const v21, -0x333334

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setTextColor(I)V

    .line 188
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->groupsViews:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 189
    return-object p3

    .line 127
    .end local v20           #textGroupDescr:Landroid/widget/TextView;
    :pswitch_1
    const v21, 0x7f0d00b9

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 128
    .local v16, textGroup:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 129
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 130
    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v21

    const/16 v22, 0x1

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 132
    const v21, 0x7f0d00b8

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 133
    .local v8, icon:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->getImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 134
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 135
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->getColor(I)I

    move-result v5

    .line 136
    .local v5, color:I
    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 137
    sget-object v21, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, v21

    invoke-virtual {v8, v5, v0}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto/16 :goto_3

    .line 140
    .end local v5           #color:I
    .end local v8           #icon:Landroid/widget/ImageView;
    .end local v16           #textGroup:Landroid/widget/TextView;
    :pswitch_2
    const v21, 0x7f0d00bc

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/LinearLayout;

    .line 141
    .local v14, lin:Landroid/widget/LinearLayout;
    const/16 v21, 0xa

    const/16 v22, 0x5

    const/16 v23, 0x32

    const/16 v24, 0x5

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 142
    const v21, 0x7f0d00b9

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 143
    .local v17, textGroup2:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v22

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v22

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 145
    const-string v21, "#000000"

    invoke-static/range {v21 .. v21}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 146
    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v21

    const/16 v22, 0x1

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 147
    const v21, 0x7f0d00b8

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 148
    .local v9, icon2:Landroid/widget/ImageView;
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 149
    const-string v21, "#9F9F9F"

    invoke-static/range {v21 .. v21}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 150
    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 153
    .end local v9           #icon2:Landroid/widget/ImageView;
    .end local v14           #lin:Landroid/widget/LinearLayout;
    .end local v17           #textGroup2:Landroid/widget/TextView;
    :pswitch_3
    const v21, 0x7f0d00b9

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 154
    .local v18, textGroup3:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v22

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v22

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 156
    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    invoke-virtual/range {v18 .. v18}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v21

    const/16 v22, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 158
    const-string v21, "#feeb9c"

    invoke-static/range {v21 .. v21}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 159
    const v21, 0x7f0d00b8

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 160
    .local v10, icon3:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->getImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 161
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 164
    .end local v10           #icon3:Landroid/widget/ImageView;
    .end local v18           #textGroup3:Landroid/widget/TextView;
    :pswitch_4
    const v21, 0x7f0d00b9

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 165
    .local v19, textGroup4:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v22

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v22

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 167
    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    invoke-virtual/range {v19 .. v19}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v21

    const/16 v22, 0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 169
    const-string v21, "#feeb9c"

    invoke-static/range {v21 .. v21}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 170
    const v21, 0x7f0d00b8

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 171
    .local v11, icon4:Landroid/widget/ImageView;
    const v21, 0x7f0d00bb

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 172
    .local v4, chk:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v21

    iget-object v0, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPref:Ljava/lang/String;

    move-object/from16 v22, v0

    iget-boolean v0, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPrefDef:Z

    move/from16 v23, v0

    invoke-interface/range {v21 .. v23}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 173
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 175
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->getImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 176
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 185
    .end local v4           #chk:Landroid/widget/CheckBox;
    .end local v11           #icon4:Landroid/widget/ImageView;
    .end local v19           #textGroup4:Landroid/widget/TextView;
    :cond_1
    const v21, 0x7f0d00ba

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 186
    .restart local v20       #textGroupDescr:Landroid/widget/TextView;
    const/16 v21, 0x8

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 109
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch

    .line 125
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method getImage(I)Landroid/graphics/drawable/Drawable;
    .locals 7
    .parameter "group"

    .prologue
    const v6, 0x7f02001a

    const v5, 0x7f020001

    const v4, 0x7f030005

    const v3, 0x7f030002

    const/high16 v2, 0x7f02

    .line 680
    const/4 v0, 0x0

    .line 682
    .local v0, iconka:Landroid/graphics/drawable/Drawable;
    sparse-switch p1, :sswitch_data_0

    .line 910
    :goto_0
    return-object v0

    .line 684
    :sswitch_0
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020040

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 687
    goto :goto_0

    .line 690
    :sswitch_1
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 691
    const-string v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0

    .line 696
    :sswitch_2
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 700
    goto :goto_0

    .line 702
    :sswitch_3
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 703
    goto :goto_0

    .line 705
    :sswitch_4
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 709
    goto :goto_0

    .line 711
    :sswitch_5
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 715
    goto :goto_0

    .line 717
    :sswitch_6
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 718
    const-string v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0

    .line 722
    :sswitch_7
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 723
    const-string v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0

    .line 727
    :sswitch_8
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 728
    const-string v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0

    .line 732
    :sswitch_9
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 733
    const-string v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto/16 :goto_0

    .line 737
    :sswitch_a
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 738
    const-string v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto/16 :goto_0

    .line 742
    :sswitch_b
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 743
    const-string v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto/16 :goto_0

    .line 747
    :sswitch_c
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 748
    const-string v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto/16 :goto_0

    .line 752
    :sswitch_d
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020024

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 756
    goto/16 :goto_0

    .line 758
    :sswitch_e
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020025

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 762
    goto/16 :goto_0

    .line 764
    :sswitch_f
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020025

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 768
    goto/16 :goto_0

    .line 770
    :sswitch_10
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02001f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 774
    goto/16 :goto_0

    .line 777
    :sswitch_11
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 781
    goto/16 :goto_0

    .line 784
    :sswitch_12
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 788
    goto/16 :goto_0

    .line 791
    :sswitch_13
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 792
    const-string v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto/16 :goto_0

    .line 798
    :sswitch_14
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020022

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 801
    goto/16 :goto_0

    .line 803
    :sswitch_15
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 806
    goto/16 :goto_0

    .line 808
    :sswitch_16
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 811
    goto/16 :goto_0

    .line 813
    :sswitch_17
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 816
    goto/16 :goto_0

    .line 818
    :sswitch_18
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 821
    goto/16 :goto_0

    .line 823
    :sswitch_19
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 826
    goto/16 :goto_0

    .line 828
    :sswitch_1a
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 831
    goto/16 :goto_0

    .line 833
    :sswitch_1b
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 836
    goto/16 :goto_0

    .line 838
    :sswitch_1c
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 841
    goto/16 :goto_0

    .line 843
    :sswitch_1d
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 846
    goto/16 :goto_0

    .line 848
    :sswitch_1e
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 851
    goto/16 :goto_0

    .line 853
    :sswitch_1f
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 856
    goto/16 :goto_0

    .line 858
    :sswitch_20
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 861
    goto/16 :goto_0

    .line 863
    :sswitch_21
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 866
    goto/16 :goto_0

    .line 868
    :sswitch_22
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 871
    goto/16 :goto_0

    .line 873
    :sswitch_23
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 876
    goto/16 :goto_0

    .line 878
    :sswitch_24
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 881
    goto/16 :goto_0

    .line 883
    :sswitch_25
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 886
    goto/16 :goto_0

    .line 888
    :sswitch_26
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 891
    goto/16 :goto_0

    .line 893
    :sswitch_27
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 896
    goto/16 :goto_0

    .line 898
    :sswitch_28
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 901
    goto/16 :goto_0

    .line 903
    :sswitch_29
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 682
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f070006 -> :sswitch_29
        0x7f070010 -> :sswitch_20
        0x7f07001b -> :sswitch_0
        0x7f07002a -> :sswitch_a
        0x7f07002b -> :sswitch_b
        0x7f07003b -> :sswitch_1
        0x7f070041 -> :sswitch_21
        0x7f070044 -> :sswitch_12
        0x7f07004b -> :sswitch_2
        0x7f07005f -> :sswitch_4
        0x7f07006b -> :sswitch_5
        0x7f0700bf -> :sswitch_6
        0x7f0700dc -> :sswitch_1c
        0x7f0700e3 -> :sswitch_1b
        0x7f0700e5 -> :sswitch_d
        0x7f0700e7 -> :sswitch_24
        0x7f07010d -> :sswitch_1e
        0x7f070110 -> :sswitch_18
        0x7f07012b -> :sswitch_26
        0x7f07012e -> :sswitch_22
        0x7f070130 -> :sswitch_23
        0x7f070145 -> :sswitch_19
        0x7f070149 -> :sswitch_8
        0x7f07014a -> :sswitch_9
        0x7f070150 -> :sswitch_e
        0x7f07016a -> :sswitch_f
        0x7f07017d -> :sswitch_1f
        0x7f070190 -> :sswitch_10
        0x7f070196 -> :sswitch_16
        0x7f0701be -> :sswitch_14
        0x7f0701c4 -> :sswitch_c
        0x7f0701c6 -> :sswitch_11
        0x7f0701d9 -> :sswitch_28
        0x7f0701da -> :sswitch_13
        0x7f0701df -> :sswitch_1d
        0x7f0701e2 -> :sswitch_1a
        0x7f0701ed -> :sswitch_17
        0x7f070216 -> :sswitch_15
        0x7f07021c -> :sswitch_3
        0x7f070226 -> :sswitch_27
        0x7f07022a -> :sswitch_25
        0x7f070239 -> :sswitch_7
    .end sparse-switch
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .parameter "groupPosition"
    .parameter "childPosition"

    .prologue
    .line 671
    const/4 v0, 0x1

    return v0
.end method

.method public onGroupCollapsed(I)V
    .locals 0
    .parameter "groupPosition"

    .prologue
    .line 91
    invoke-super {p0, p1}, Landroid/widget/BaseExpandableListAdapter;->onGroupCollapsed(I)V

    .line 92
    return-void
.end method

.method public onGroupExpanded(I)V
    .locals 0
    .parameter "groupPosition"

    .prologue
    .line 96
    invoke-super {p0, p1}, Landroid/widget/BaseExpandableListAdapter;->onGroupExpanded(I)V

    .line 98
    return-void
.end method

.method public setTextSize(I)V
    .locals 0
    .parameter "s"

    .prologue
    .line 194
    iput p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->size:I

    .line 195
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->notifyDataSetChanged()V

    .line 196
    return-void
.end method
