.class public Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;
.super Ljava/io/OutputStream;
.source "LogOutputStream.java"


# instance fields
.field public allresult:Ljava/lang/String;

.field private bos:Ljava/io/ByteArrayOutputStream;

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter "name"

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 8
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->bos:Ljava/io/ByteArrayOutputStream;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->allresult:Ljava/lang/String;

    .line 13
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->name:Ljava/lang/String;

    .line 14
    return-void
.end method


# virtual methods
.method public write(I)V
    .locals 3
    .parameter "b"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    const/16 v1, 0xa

    if-ne p1, v1, :cond_0

    .line 19
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->bos:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 20
    .local v0, s:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->allresult:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->allresult:Ljava/lang/String;

    .line 22
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->bos:Ljava/io/ByteArrayOutputStream;

    .line 26
    .end local v0           #s:Ljava/lang/String;
    :goto_0
    return-void

    .line 24
    :cond_0
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->bos:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0
.end method
