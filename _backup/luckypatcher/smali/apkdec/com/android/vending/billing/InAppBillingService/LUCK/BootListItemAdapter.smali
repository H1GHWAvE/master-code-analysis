.class public Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;
.super Landroid/widget/ArrayAdapter;
.source "BootListItemAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
        ">;"
    }
.end annotation


# static fields
.field public static final TEXT_DEFAULT:I = 0x0

.field public static final TEXT_LARGE:I = 0x2

.field public static final TEXT_MEDIUM:I = 0x1

.field public static final TEXT_SMALL:I


# instance fields
.field context:Landroid/content/Context;

.field data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
            ">;"
        }
    .end annotation
.end field

.field imgIcon:Landroid/widget/ImageView;

.field layoutResourceId:I

.field size:I

.field public sorter:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
            ">;"
        }
    .end annotation
.end field

.field txtStatus:Landroid/widget/TextView;

.field txtTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/util/List;)V
    .locals 0
    .parameter "context"
    .parameter "layout"
    .parameter "s"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p4, p:Ljava/util/List;,"Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    invoke-direct {p0, p1, p2, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 46
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->context:Landroid/content/Context;

    .line 47
    iput p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->layoutResourceId:I

    .line 48
    iput p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->size:I

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .parameter "context"
    .parameter "layout"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p3, p:Ljava/util/List;,"Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 38
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->context:Landroid/content/Context;

    .line 39
    iput p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->layoutResourceId:I

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->size:I

    .line 41
    iput-object p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->data:Ljava/util/List;

    .line 42
    return-void
.end method


# virtual methods
.method public getItem(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .locals 2
    .parameter "name"

    .prologue
    .line 157
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 158
    invoke-virtual {p0, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    invoke-virtual {p0, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 162
    :goto_1
    return-object v1

    .line 157
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 162
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v9, 0x1

    const v11, -0x777778

    const/high16 v10, -0x100

    .line 52
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 53
    .local v4, p:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    invoke-virtual {p3, v10}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 55
    iget-object v7, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    if-eqz v7, :cond_0

    iget-boolean v7, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    if-nez v7, :cond_0

    iget-boolean v7, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    if-nez v7, :cond_0

    iget-boolean v7, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    if-nez v7, :cond_0

    iget-boolean v7, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    if-nez v7, :cond_0

    .line 56
    new-instance v3, Landroid/view/View;

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->context:Landroid/content/Context;

    invoke-direct {v3, v7}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 139
    :goto_0
    return-object v3

    .line 62
    :cond_0
    move-object v6, p2

    .line 64
    .local v6, row:Landroid/view/View;
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->context:Landroid/content/Context;

    check-cast v7, Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 65
    .local v2, inflater:Landroid/view/LayoutInflater;
    iget v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->layoutResourceId:I

    const/4 v8, 0x0

    invoke-virtual {v2, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 68
    invoke-virtual {v6, v10}, Landroid/view/View;->setBackgroundColor(I)V

    .line 70
    const v7, 0x7f0d004e

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtTitle:Landroid/widget/TextView;

    .line 71
    const v7, 0x7f0d004f

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtStatus:Landroid/widget/TextView;

    .line 72
    const v7, 0x7f0d0038

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->imgIcon:Landroid/widget/ImageView;

    .line 76
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtTitle:Landroid/widget/TextView;

    iget-object v8, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->imgIcon:Landroid/widget/ImageView;

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setMaxHeight(I)V

    .line 83
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->imgIcon:Landroid/widget/ImageView;

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setMaxWidth(I)V

    .line 85
    :try_start_0
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->imgIcon:Landroid/widget/ImageView;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v8

    iget-object v9, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_1
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtTitle:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->context:Landroid/content/Context;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 97
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtStatus:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->context:Landroid/content/Context;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 98
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtTitle:Landroid/widget/TextView;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 99
    const-string v0, ""

    .line 100
    .local v0, boot:Ljava/lang/String;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v5

    .line 101
    .local v5, r:Landroid/content/res/Resources;
    iget-boolean v7, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    if-eqz v7, :cond_1

    .line 102
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f0701ee

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "; "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 103
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtTitle:Landroid/widget/TextView;

    const v8, -0xff0001

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 105
    :cond_1
    iget-boolean v7, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    if-eqz v7, :cond_2

    .line 106
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f0701f0

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "; "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 107
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtTitle:Landroid/widget/TextView;

    const v8, -0xff0100

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 109
    :cond_2
    iget-boolean v7, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    if-eqz v7, :cond_3

    .line 110
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f0701ef

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "; "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 111
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtTitle:Landroid/widget/TextView;

    const/16 v8, -0x100

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 113
    :cond_3
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtStatus:Landroid/widget/TextView;

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v7, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const v8, 0x7f07023a

    invoke-static {v8}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 115
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->imgIcon:Landroid/widget/ImageView;

    const v8, 0x7f02002f

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 116
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtStatus:Landroid/widget/TextView;

    const v8, 0x7f07023b

    invoke-static {v8}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    :cond_4
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtStatus:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->context:Landroid/content/Context;

    const v9, 0x1030046

    invoke-virtual {v7, v8, v9}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 122
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtStatus:Landroid/widget/TextView;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 123
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtTitle:Landroid/widget/TextView;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 124
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->txtStatus:Landroid/widget/TextView;

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setBackgroundColor(I)V

    move-object v3, v6

    .line 139
    goto/16 :goto_0

    .line 86
    .end local v0           #boot:Ljava/lang/String;
    .end local v5           #r:Landroid/content/res/Resources;
    :catch_0
    move-exception v1

    .line 88
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x2

    return v0
.end method

.method public setTextSize(I)V
    .locals 0
    .parameter "s"

    .prologue
    .line 147
    iput p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->size:I

    .line 148
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->notifyDataSetChanged()V

    .line 149
    return-void
.end method

.method public sort()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->sorter:Ljava/util/Comparator;

    invoke-super {p0, v0}, Landroid/widget/ArrayAdapter;->sort(Ljava/util/Comparator;)V

    .line 153
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter;->notifyDataSetChanged()V

    .line 154
    return-void
.end method
