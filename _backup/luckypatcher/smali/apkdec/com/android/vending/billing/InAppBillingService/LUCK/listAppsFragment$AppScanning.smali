.class public Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AppScanning"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method public constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .locals 0
    .parameter "this$0"

    .prologue
    .line 10692
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 10700
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$1;

    invoke-direct {v9, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;)V

    invoke-virtual {v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 10712
    const/4 v8, 0x0

    :try_start_0
    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;

    .line 10713
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;

    .line 10714
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    if-nez v8, :cond_0

    .line 10715
    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 10718
    :cond_0
    :try_start_1
    sget-boolean v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->sqlScanLite:Z

    if-eqz v8, :cond_1

    .line 10720
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$2;

    invoke-direct {v9, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;)V

    invoke-virtual {v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    .line 10733
    :cond_1
    :goto_0
    const/4 v5, 0x0

    .line 10734
    .local v5, skipIcon:Z
    :try_start_2
    sget-boolean v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->fast_start:Z

    if-eqz v8, :cond_2

    const/4 v5, 0x1

    .line 10736
    :cond_2
    if-nez v5, :cond_3

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v9, "no_icon"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 10737
    :cond_3
    sget v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    if-nez v8, :cond_7

    .line 10738
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    const/4 v9, 0x0

    invoke-virtual {v8, v9, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->getPackage(ZZ)Ljava/util/ArrayList;

    move-result-object v8

    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;

    .line 10742
    :goto_1
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_a

    sget v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    if-nez v8, :cond_a

    .line 10743
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 10744
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPackages()[Ljava/lang/String;

    move-result-object v3

    .line 10745
    .local v3, packages:[Ljava/lang/String;
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    array-length v9, v3

    iput v9, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->maxcount:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 10750
    :try_start_3
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$3;

    invoke-direct {v9, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;)V

    invoke-virtual {v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 10771
    :goto_2
    :try_start_4
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/4 v9, 0x1

    iput v9, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->count:I

    .line 10772
    const/4 v6, 0x0

    .line 10773
    .local v6, sysshow:Z
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v9, "systemapp"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result v6

    .line 10776
    :try_start_5
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$4;

    invoke-direct {v9, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;)V

    invoke-virtual {v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 10788
    :goto_3
    const/4 v1, 0x1

    .line 10789
    .local v1, iconTrigger:Z
    :try_start_6
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v9, "no_icon"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_4

    const/4 v1, 0x0

    .line 10790
    :cond_4
    array-length v8, v3

    :goto_4
    if-ge v7, v8, :cond_9

    aget-object v4, v3, v7
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 10793
    .local v4, pkgname:Ljava/lang/String;
    :try_start_7
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v10, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->count:I

    add-int/lit8 v10, v10, 0x1

    iput v10, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->count:I

    .line 10795
    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v9

    sget v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I

    invoke-direct {v2, v9, v4, v10, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;-><init>(Landroid/content/Context;Ljava/lang/String;IZ)V

    .line 10796
    .local v2, local:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    invoke-virtual {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->saveItem()V

    .line 10800
    iget-boolean v9, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    if-nez v9, :cond_5

    iget-boolean v9, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    if-nez v9, :cond_5

    iget-boolean v9, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    if-nez v9, :cond_5

    iget-boolean v9, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    if-eqz v9, :cond_6

    .line 10801
    :cond_5
    sget-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->boot_pat:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    .line 10811
    .end local v2           #local:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_6
    :goto_5
    :try_start_8
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$5;

    invoke-direct {v10, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;)V

    invoke-virtual {v9, v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 10790
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 10740
    .end local v1           #iconTrigger:Z
    .end local v3           #packages:[Ljava/lang/String;
    .end local v4           #pkgname:Ljava/lang/String;
    .end local v6           #sysshow:Z
    :cond_7
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    const/4 v9, 0x1

    invoke-virtual {v8, v9, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->getPackage(ZZ)Ljava/util/ArrayList;

    move-result-object v8

    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    goto/16 :goto_1

    .line 10837
    .end local v5           #skipIcon:Z
    :catch_0
    move-exception v0

    .line 10838
    .local v0, e:Ljava/lang/Exception;
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "LuckyPatcher (AppScanner): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 10839
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 10841
    .end local v0           #e:Ljava/lang/Exception;
    :cond_8
    :goto_6
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$7;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;)V

    invoke-virtual {v7, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 10863
    return-void

    .line 10821
    .restart local v1       #iconTrigger:Z
    .restart local v3       #packages:[Ljava/lang/String;
    .restart local v5       #skipIcon:Z
    .restart local v6       #sysshow:Z
    :cond_9
    :try_start_9
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->getPackage(ZZ)Ljava/util/ArrayList;

    move-result-object v7

    sput-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;

    .line 10823
    .end local v1           #iconTrigger:Z
    .end local v3           #packages:[Ljava/lang/String;
    .end local v6           #sysshow:Z
    :cond_a
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_8

    sget v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    if-eqz v7, :cond_8

    .line 10824
    const/4 v7, 0x0

    sput v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    .line 10825
    sget-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapterSelect:Z

    if-eqz v7, :cond_b

    .line 10826
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->resetBatchOperation()V

    .line 10828
    :cond_b
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->handler:Landroid/os/Handler;

    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$6;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    goto :goto_6

    .line 10806
    .restart local v1       #iconTrigger:Z
    .restart local v3       #packages:[Ljava/lang/String;
    .restart local v4       #pkgname:Ljava/lang/String;
    .restart local v6       #sysshow:Z
    :catch_1
    move-exception v9

    goto :goto_5

    .line 10786
    .end local v1           #iconTrigger:Z
    .end local v4           #pkgname:Ljava/lang/String;
    :catch_2
    move-exception v8

    goto/16 :goto_3

    .line 10767
    .end local v6           #sysshow:Z
    :catch_3
    move-exception v8

    goto/16 :goto_2

    .line 10731
    .end local v3           #packages:[Ljava/lang/String;
    .end local v5           #skipIcon:Z
    :catch_4
    move-exception v8

    goto/16 :goto_0
.end method
