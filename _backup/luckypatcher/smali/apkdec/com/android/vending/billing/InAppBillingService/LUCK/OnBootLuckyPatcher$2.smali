.class Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher$2;
.super Ljava/lang/Object;
.source "OnBootLuckyPatcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher;Landroid/content/Context;)V
    .locals 0
    .parameter "this$0"
    .parameter

    .prologue
    .line 61
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher;

    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 64
    new-instance v0, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher$2;->val$context:Landroid/content/Context;

    const-string v11, "binder"

    invoke-virtual {v10, v11, v13}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/bind.txt"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 66
    .local v0, bindfile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v9

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-lez v9, :cond_2

    .line 67
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher binder start!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 71
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_0

    .line 73
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 76
    :cond_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 77
    .local v4, fis:Ljava/io/FileInputStream;
    new-instance v6, Ljava/io/InputStreamReader;

    const-string v9, "UTF-8"

    invoke-direct {v6, v4, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 78
    .local v6, isr:Ljava/io/InputStreamReader;
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 79
    .local v1, br:Ljava/io/BufferedReader;
    const-string v2, ""

    .line 81
    .local v2, data:Ljava/lang/String;
    const-string v8, ""

    .line 83
    .local v8, temp:Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 84
    move-object v8, v2

    .line 85
    const-string v9, ";"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 86
    .local v7, tails:[Ljava/lang/String;
    array-length v9, v7

    const/4 v10, 0x2

    if-ne v9, v10, :cond_1

    .line 87
    const-string v9, "mount"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "-o bind \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x0

    aget-object v11, v7, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\' \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x1

    aget-object v11, v7, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    aget-object v11, v7, v11

    const/4 v12, 0x1

    aget-object v12, v7, v12

    invoke-static {v9, v10, v11, v12}, Lcom/chelpus/Utils;->verify_bind_and_run(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 95
    .end local v1           #br:Ljava/io/BufferedReader;
    .end local v2           #data:Ljava/lang/String;
    .end local v4           #fis:Ljava/io/FileInputStream;
    .end local v6           #isr:Ljava/io/InputStreamReader;
    .end local v7           #tails:[Ljava/lang/String;
    .end local v8           #temp:Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 96
    .local v3, e:Ljava/io/FileNotFoundException;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Not found bind.txt"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 100
    .end local v3           #e:Ljava/io/FileNotFoundException;
    :goto_1
    sput-boolean v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchOnBoot:Z

    .line 101
    invoke-static {}, Lcom/chelpus/Utils;->exit()V

    .line 103
    :cond_2
    return-void

    .line 91
    .restart local v1       #br:Ljava/io/BufferedReader;
    .restart local v2       #data:Ljava/lang/String;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    .restart local v6       #isr:Ljava/io/InputStreamReader;
    .restart local v8       #temp:Ljava/lang/String;
    :cond_3
    :try_start_1
    new-instance v5, Landroid/content/Intent;

    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher$2;->val$context:Landroid/content/Context;

    const-class v10, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;

    invoke-direct {v5, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 92
    .local v5, i:Landroid/content/Intent;
    sget-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    invoke-virtual {v5, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher$2;->val$context:Landroid/content/Context;

    invoke-virtual {v9, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 94
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 97
    .end local v1           #br:Ljava/io/BufferedReader;
    .end local v2           #data:Ljava/lang/String;
    .end local v4           #fis:Ljava/io/FileInputStream;
    .end local v5           #i:Landroid/content/Intent;
    .end local v6           #isr:Ljava/io/InputStreamReader;
    .end local v8           #temp:Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 98
    .local v3, e:Ljava/io/IOException;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1
.end method
