.class public Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;
.super Ljava/lang/Object;
.source "Progress_Dialog_2.java"


# static fields
.field public static dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;


# instance fields
.field dialog2:Landroid/app/Dialog;

.field fm:Landroid/support/v4/app/FragmentManager;

.field message:Ljava/lang/String;

.field title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->message:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->title:Ljava/lang/String;

    .line 17
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->fm:Landroid/support/v4/app/FragmentManager;

    .line 23
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog2:Landroid/app/Dialog;

    .line 21
    return-void
.end method

.method public static newInstance()Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;

    invoke-direct {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;-><init>()V

    .line 26
    .local v0, f:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;
    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog2:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog2:Landroid/app/Dialog;

    .line 123
    :cond_0
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog2:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 102
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateDialog()Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 42
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->message:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Loading..."

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->message:Ljava/lang/String;

    .line 43
    :cond_0
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 44
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setCancelable(Z)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 45
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2$1;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;)V

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 53
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->create()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public setCancelable(Z)V
    .locals 1
    .parameter "trig"

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog2:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 99
    :cond_0
    return-void
.end method

.method public setIndeterminate(Z)V
    .locals 2
    .parameter "indeterminate"

    .prologue
    .line 67
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->onCreateDialog()Landroid/app/Dialog;

    .line 68
    :cond_0
    if-nez p1, :cond_1

    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setIncrementStyle()Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 70
    :goto_0
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2$3;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 76
    return-void

    .line 69
    :cond_1
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setDefaultStyle()Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    goto :goto_0
.end method

.method public setMax(I)V
    .locals 2
    .parameter "max"

    .prologue
    .line 78
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->onCreateDialog()Landroid/app/Dialog;

    .line 79
    :cond_0
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setMax(I)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 80
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2$4;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 86
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 2
    .parameter "text"

    .prologue
    .line 56
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->message:Ljava/lang/String;

    .line 57
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->onCreateDialog()Landroid/app/Dialog;

    .line 58
    :cond_0
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 59
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2$2;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 65
    return-void
.end method

.method public setProgress(I)V
    .locals 2
    .parameter "progress"

    .prologue
    .line 88
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->onCreateDialog()Landroid/app/Dialog;

    .line 89
    :cond_0
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setProgress(I)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 90
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2$5;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 96
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2
    .parameter "text"

    .prologue
    .line 107
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->onCreateDialog()Landroid/app/Dialog;

    .line 108
    :cond_0
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->title:Ljava/lang/String;

    .line 109
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 110
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2$6;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 117
    return-void
.end method

.method public showDialog()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog2:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->onCreateDialog()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog2:Landroid/app/Dialog;

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 34
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->dialog2:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 36
    :cond_1
    return-void
.end method
