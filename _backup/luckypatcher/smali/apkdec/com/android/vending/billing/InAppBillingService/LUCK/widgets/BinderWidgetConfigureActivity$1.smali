.class Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$1;
.super Landroid/widget/ArrayAdapter;
.source "BinderWidgetConfigureActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .parameter "this$0"
    .parameter "x0"
    .parameter "x1"
    .parameter

    .prologue
    .line 95
    .local p4, x2:Ljava/util/List;,"Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;>;"
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v6, 0x0

    .line 98
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 99
    .local v3, view:Landroid/view/View;
    const v4, 0x7f0d0039

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 101
    .local v2, textView:Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$1;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 103
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->context:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;

    iget v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->sizeText:I

    invoke-virtual {v2, v4, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 104
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$1;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;

    .line 105
    .local v0, item:Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    const-string v5, "empty"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v5, "empty"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 106
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f0700f0

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f0700f1

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    invoke-virtual {v3, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 108
    invoke-virtual {v3, v6}, Landroid/view/View;->setClickable(Z)V

    .line 122
    :goto_0
    return-object v3

    .line 110
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f070034

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 111
    .local v1, str2:Ljava/lang/String;
    const-string v4, "#ff00ff00"

    const-string v5, "bold"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v1, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    .line 113
    const-string v4, "#ffffffff"

    const-string v5, "italic"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 115
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f070035

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 116
    const-string v4, "#ffffff00"

    const-string v5, "bold"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v1, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    .line 118
    const-string v4, "~chelpus_disabled~"

    const-string v5, ""

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "#ffffffff"

    const-string v6, "italic"

    invoke-static {v4, v5, v6}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
