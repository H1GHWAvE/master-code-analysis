.class Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1;
.super Ljava/lang/Object;
.source "All_Dialogs.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;)V
    .locals 0
    .parameter "this$1"

    .prologue
    .line 1242
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    .line 1246
    sget-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v9}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    .line 1247
    .local v0, count:I
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 1248
    .local v8, settings:Lorg/json/JSONObject;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-ge v2, v0, :cond_7

    .line 1249
    sget-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v9, v2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;

    .line 1250
    .local v6, patt:Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-boolean v11, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1251
    if-nez v2, :cond_0

    .line 1253
    :try_start_0
    const-string v9, "patch1"

    iget-boolean v10, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1258
    :cond_0
    :goto_1
    if-ne v2, v14, :cond_1

    .line 1260
    :try_start_1
    const-string v9, "patch2"

    iget-boolean v10, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1265
    :cond_1
    :goto_2
    const/4 v9, 0x2

    if-ne v2, v9, :cond_2

    .line 1267
    :try_start_2
    const-string v9, "patch3"

    iget-boolean v10, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1272
    :cond_2
    :goto_3
    const/4 v9, 0x3

    if-ne v2, v9, :cond_4

    .line 1274
    :try_start_3
    const-string v9, "com.android.vending"

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/chelpus/Utils;->getPkgInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    .line 1275
    .local v7, pi:Landroid/content/pm/PackageInfo;
    if-eqz v7, :cond_3

    .line 1276
    new-instance v5, Ljava/io/File;

    iget-object v9, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v9, v9, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_4

    .line 1277
    .local v5, odex:Ljava/io/File;
    const-wide/16 v3, 0x0

    .line 1279
    .local v3, lenght:J
    :try_start_4
    invoke-virtual {v5}, Ljava/io/File;->length()J
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    move-result-wide v3

    .line 1282
    :goto_4
    const-wide/32 v9, 0x100000

    cmp-long v9, v3, v9

    if-gtz v9, :cond_3

    const-wide/16 v9, 0x0

    cmp-long v9, v3, v9

    if-nez v9, :cond_6

    .line 1295
    .end local v3           #lenght:J
    .end local v5           #odex:Ljava/io/File;
    :cond_3
    :goto_5
    :try_start_5
    const-string v9, "patch4"

    iget-boolean v10, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_4

    .line 1300
    .end local v7           #pi:Landroid/content/pm/PackageInfo;
    :cond_4
    :goto_6
    const/4 v9, 0x4

    if-ne v2, v9, :cond_5

    .line 1302
    :try_start_6
    const-string v9, "hide"

    iget-boolean v10, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_5

    .line 1248
    :cond_5
    :goto_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1254
    :catch_0
    move-exception v1

    .line 1255
    .local v1, e:Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 1261
    .end local v1           #e:Lorg/json/JSONException;
    :catch_1
    move-exception v1

    .line 1262
    .restart local v1       #e:Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 1268
    .end local v1           #e:Lorg/json/JSONException;
    :catch_2
    move-exception v1

    .line 1269
    .restart local v1       #e:Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    .line 1286
    .end local v1           #e:Lorg/json/JSONException;
    .restart local v3       #lenght:J
    .restart local v5       #odex:Ljava/io/File;
    .restart local v7       #pi:Landroid/content/pm/PackageInfo;
    :cond_6
    :try_start_7
    new-instance v9, Lcom/chelpus/Utils;

    const-string v10, ""

    invoke-direct {v9, v10}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ".pinfo "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v13, v13, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v13, v13, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " recovery"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v9, v10}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    .line 1287
    const/4 v9, 0x1

    invoke-static {v9}, Lcom/chelpus/Utils;->market_billing_services(Z)V

    .line 1288
    const/4 v9, 0x1

    invoke-static {v9}, Lcom/chelpus/Utils;->market_licensing_services(Z)V

    .line 1289
    const-string v9, "com.android.vending"

    invoke-static {v9}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_5

    .line 1290
    :catch_3
    move-exception v1

    .line 1291
    .local v1, e:Ljava/lang/Exception;
    :try_start_8
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_5

    .line 1296
    .end local v1           #e:Ljava/lang/Exception;
    .end local v3           #lenght:J
    .end local v5           #odex:Ljava/io/File;
    .end local v7           #pi:Landroid/content/pm/PackageInfo;
    :catch_4
    move-exception v1

    .line 1297
    .local v1, e:Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_6

    .line 1303
    .end local v1           #e:Lorg/json/JSONException;
    :catch_5
    move-exception v1

    .line 1304
    .restart local v1       #e:Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_7

    .line 1310
    .end local v1           #e:Lorg/json/JSONException;
    .end local v6           #patt:Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;
    :cond_7
    :try_start_9
    const-string v9, "module_on"

    iget-object v10, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;->val$chk_x:Landroid/widget/CheckBox;

    invoke-virtual {v10}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v10

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_6

    .line 1314
    :goto_8
    new-instance v9, Ljava/lang/Thread;

    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1$1;

    invoke-direct {v10, p0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1;Lorg/json/JSONObject;)V

    invoke-direct {v9, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1322
    invoke-virtual {v9}, Ljava/lang/Thread;->start()V

    .line 1324
    return-void

    .line 1311
    :catch_6
    move-exception v1

    .line 1312
    .restart local v1       #e:Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_8

    .line 1280
    .end local v1           #e:Lorg/json/JSONException;
    .restart local v3       #lenght:J
    .restart local v5       #odex:Ljava/io/File;
    .restart local v6       #patt:Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;
    .restart local v7       #pi:Landroid/content/pm/PackageInfo;
    :catch_7
    move-exception v9

    goto/16 :goto_4
.end method
