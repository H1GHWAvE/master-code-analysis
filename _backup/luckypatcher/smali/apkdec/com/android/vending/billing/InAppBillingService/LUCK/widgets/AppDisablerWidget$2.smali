.class Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;
.super Ljava/lang/Object;
.source "AppDisablerWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$handler:Landroid/os/Handler;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;Landroid/content/Context;Landroid/content/Intent;Landroid/os/Handler;)V
    .locals 0
    .parameter "this$0"
    .parameter
    .parameter
    .parameter

    .prologue
    .line 150
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget;

    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;->val$intent:Landroid/content/Intent;

    iput-object p4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;->val$handler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 154
    :try_start_0
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;->val$context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 155
    .local v4, pm:Landroid/content/pm/PackageManager;
    const-string v3, ""

    .line 156
    .local v3, pkgName:Ljava/lang/String;
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;->val$intent:Landroid/content/Intent;

    const-string v9, "appWidgetId"

    const/4 v10, -0x1

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 157
    .local v2, id:I
    const/4 v8, -0x1

    if-eq v2, v8, :cond_0

    .line 158
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;->val$context:Landroid/content/Context;

    invoke-static {v8, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->loadTitlePref(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "NOT_SAVED_APP_DISABLER"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 159
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;->val$context:Landroid/content/Context;

    invoke-static {v8, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->loadTitlePref(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 163
    new-instance v5, Landroid/widget/RemoteViews;

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;->val$context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f040006

    invoke-direct {v5, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 165
    .local v5, remoteViews:Landroid/widget/RemoteViews;
    const-string v6, ""

    .line 166
    .local v6, result:Ljava/lang/String;
    const/4 v8, 0x0

    invoke-virtual {v4, v3, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v8

    iget-object v8, v8, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v8, v8, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v8, :cond_1

    .line 167
    new-instance v8, Lcom/chelpus/Utils;

    const-string v9, ""

    invoke-direct {v8, v9}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "pm disable "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 169
    const v8, 0x7f0d002b

    const-string v9, "setBackgroundResource"

    const v10, 0x7f020050

    invoke-virtual {v5, v8, v9, v10}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 170
    const v8, 0x7f0d002a

    const-string v9, "#FF0000"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 171
    move-object v1, v3

    .line 172
    .local v1, fin_item:Ljava/lang/String;
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;->val$handler:Landroid/os/Handler;

    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2$1;

    invoke-direct {v9, p0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 223
    .end local v1           #fin_item:Ljava/lang/String;
    .end local v2           #id:I
    .end local v3           #pkgName:Ljava/lang/String;
    .end local v4           #pm:Landroid/content/pm/PackageManager;
    .end local v5           #remoteViews:Landroid/widget/RemoteViews;
    .end local v6           #result:Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 183
    .restart local v2       #id:I
    .restart local v3       #pkgName:Ljava/lang/String;
    .restart local v4       #pm:Landroid/content/pm/PackageManager;
    .restart local v5       #remoteViews:Landroid/widget/RemoteViews;
    .restart local v6       #result:Ljava/lang/String;
    :cond_1
    new-instance v8, Lcom/chelpus/Utils;

    const-string v9, ""

    invoke-direct {v8, v9}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "pm enable "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 184
    const v8, 0x7f0d002a

    const-string v9, "#00FF00"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 185
    const v8, 0x7f0d002b

    const-string v9, "setBackgroundResource"

    const v10, 0x7f020051

    invoke-virtual {v5, v8, v9, v10}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 186
    move-object v1, v3

    .line 187
    .restart local v1       #fin_item:Ljava/lang/String;
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;->val$handler:Landroid/os/Handler;

    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2$2;

    invoke-direct {v9, p0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 202
    .end local v1           #fin_item:Ljava/lang/String;
    .end local v2           #id:I
    .end local v3           #pkgName:Ljava/lang/String;
    .end local v4           #pm:Landroid/content/pm/PackageManager;
    .end local v5           #remoteViews:Landroid/widget/RemoteViews;
    .end local v6           #result:Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 203
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 205
    new-instance v7, Landroid/widget/RemoteViews;

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;->val$context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f040006

    invoke-direct {v7, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 206
    .local v7, views:Landroid/widget/RemoteViews;
    const v8, 0x7f0d002b

    const-string v9, "setEnabled"

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    .line 207
    const v8, 0x7f0d002b

    const-string v9, "setBackgroundResource"

    const v10, 0x7f020050

    invoke-virtual {v7, v8, v9, v10}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 208
    const v8, 0x7f0d002a

    const-string v9, "#AAAAAA"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 210
    const v8, 0x7f0d002a

    const v9, 0x7f07023c

    invoke-static {v9}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 211
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;->val$handler:Landroid/os/Handler;

    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2$3;

    invoke-direct {v9, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidget$2;)V

    invoke-virtual {v8, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0
.end method
