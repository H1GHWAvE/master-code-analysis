.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;
.super Landroid/widget/ArrayAdapter;
.source "listAppsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getLPActivity()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .parameter "this$0"
    .parameter "x0"
    .parameter "x1"
    .parameter

    .prologue
    .line 14590
    .local p4, x2:Ljava/util/List;,"Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;>;"
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 14594
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 14595
    .local v3, view:Landroid/view/View;
    const v4, 0x7f0d0039

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 14597
    .local v2, textView:Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 14603
    const/high16 v4, 0x40a0

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x3f00

    add-float/2addr v4, v5

    float-to-int v0, v4

    .line 14604
    .local v0, dp5:I
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 14609
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v6, "disabled_"

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "chelpa_per_"

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "chelpus_"

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "android.permission."

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "com.android.vending."

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 14611
    .local v1, str2:Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-boolean v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Status:Z

    if-eqz v4, :cond_1

    .line 14612
    const-string v4, "#ff00ffff"

    const-string v5, "bold"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14613
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    invoke-static {v4}, Lcom/chelpus/Utils;->isAds(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 14614
    const-string v4, "#ffffff00"

    const-string v5, "bold"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14619
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    invoke-static {v4}, Lcom/chelpus/Utils;->isAds(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 14620
    const v4, 0x7f070008

    invoke-static {v4}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v1

    .line 14623
    :goto_1
    const-string v4, "#ff888888"

    const-string v5, "italic"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 14627
    return-object v3

    .line 14617
    :cond_1
    const-string v4, "#ffff0000"

    const-string v5, "bold"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 14621
    :cond_2
    const v4, 0x7f070009

    invoke-static {v4}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
