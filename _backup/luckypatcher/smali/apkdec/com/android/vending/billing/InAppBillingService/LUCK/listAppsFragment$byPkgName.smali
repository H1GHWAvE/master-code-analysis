.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$byPkgName;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "byPkgName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .locals 0
    .parameter "this$0"

    .prologue
    .line 6698
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$byPkgName;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)I
    .locals 3
    .parameter "a"
    .parameter "b"

    .prologue
    .line 6700
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 6701
    :cond_0
    new-instance v1, Ljava/lang/ClassCastException;

    invoke-direct {v1}, Ljava/lang/ClassCastException;-><init>()V

    throw v1

    .line 6703
    :cond_1
    iget v1, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    if-eqz v1, :cond_2

    iget v1, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    if-nez v1, :cond_4

    .line 6705
    :cond_2
    iget v1, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    .line 6706
    .local v0, ret:I
    if-nez v0, :cond_3

    .line 6707
    invoke-virtual {p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    .line 6714
    :goto_0
    return v1

    :cond_3
    move v1, v0

    .line 6709
    goto :goto_0

    .line 6712
    .end local v0           #ret:I
    :cond_4
    invoke-virtual {p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    .restart local v0       #ret:I
    move v1, v0

    .line 6714
    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 6698
    check-cast p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    check-cast p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-virtual {p0, p1, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$byPkgName;->compare(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)I

    move-result v0

    return v0
.end method
