.class public Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher;
.super Landroid/content/BroadcastReceiver;
.source "OnBootLuckyPatcher.java"


# static fields
.field public static bootlist:[Ljava/lang/String;

.field public static contextB:Landroid/content/Context;

.field public static notifyIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 26
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "empty"

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher;->bootlist:[Ljava/lang/String;

    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher;->contextB:Landroid/content/Context;

    .line 28
    sput v2, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher;->notifyIndex:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher;->showNotify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private showNotify(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14
    .parameter "id"
    .parameter "title"
    .parameter "tickerText"
    .parameter "text"

    .prologue
    .line 250
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v11

    const-string v12, "hide_notify"

    const/4 v13, 0x0

    invoke-interface {v11, v12, v13}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    if-nez v11, :cond_0

    .line 251
    const v5, 0x7f020030

    .line 252
    .local v5, icon:I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 253
    .local v9, when:J
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v4

    .line 254
    .local v4, context2:Landroid/content/Context;
    move-object/from16 v3, p2

    .line 255
    .local v3, contentTitle:Ljava/lang/CharSequence;
    move-object/from16 v2, p4

    .line 257
    .local v2, contentText:Ljava/lang/CharSequence;
    new-instance v7, Landroid/content/Intent;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v11

    const-class v12, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    invoke-direct {v7, v11, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 258
    .local v7, notificationIntent:Landroid/content/Intent;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v11, v12, v7, v13}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 259
    .local v1, contentIntent:Landroid/app/PendingIntent;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v11

    const-string v12, "notification"

    invoke-virtual {v11, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/NotificationManager;

    .line 260
    .local v8, notificationManager:Landroid/app/NotificationManager;
    new-instance v6, Landroid/app/Notification;

    move-object/from16 v0, p3

    invoke-direct {v6, v5, v0, v9, v10}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 261
    .local v6, notification:Landroid/app/Notification;
    invoke-virtual {v6, v4, v3, v2, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 262
    invoke-virtual {v8, p1, v6}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 265
    .end local v1           #contentIntent:Landroid/app/PendingIntent;
    .end local v2           #contentText:Ljava/lang/CharSequence;
    .end local v3           #contentTitle:Ljava/lang/CharSequence;
    .end local v4           #context2:Landroid/content/Context;
    .end local v5           #icon:I
    .end local v6           #notification:Landroid/app/Notification;
    .end local v7           #notificationIntent:Landroid/content/Intent;
    .end local v8           #notificationManager:Landroid/app/NotificationManager;
    .end local v9           #when:J
    :cond_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .parameter "context"
    .parameter "intent"

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 31
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "load LP"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 32
    sput-object p1, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher;->contextB:Landroid/content/Context;

    .line 33
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->init()V

    .line 34
    new-instance v5, Ljava/lang/Thread;

    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher$1;

    invoke-direct {v6, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher;)V

    invoke-direct {v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 49
    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    .line 58
    sget-boolean v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v5, :cond_3

    .line 59
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string v6, "android.intent.action.UMS_DISCONNECTED"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string v6, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 60
    :cond_0
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "load LP"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 61
    new-instance v5, Ljava/lang/Thread;

    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher$2;

    invoke-direct {v6, p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher;Landroid/content/Context;)V

    invoke-direct {v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 104
    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    .line 105
    sput-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchOnBoot:Z

    .line 108
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string v6, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 109
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "LuckyPatcher: ACTION_EXTERNAL_APPLICATIONS_AVAILABLE"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 110
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v6, "OnBootService"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 111
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "OnBootService"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 112
    sput-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchOnBoot:Z

    .line 113
    new-instance v4, Landroid/content/Intent;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchService;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 114
    .local v4, serviceLauncher:Landroid/content/Intent;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 118
    .end local v4           #serviceLauncher:Landroid/content/Intent;
    :cond_2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 119
    .local v0, handler:Landroid/os/Handler;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string v6, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 120
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "OnBootService"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 122
    const-string v5, "alarm"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    .line 123
    .local v2, mgr:Landroid/app/AlarmManager;
    new-instance v1, Landroid/content/Intent;

    const-class v5, Lcom/android/vending/billing/InAppBillingService/LUCK/OnAlarmReceiver;

    invoke-direct {v1, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 124
    .local v1, i:Landroid/content/Intent;
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/OnAlarmReceiver;->ACTION_WIDGET_RECEIVER:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 125
    invoke-static {p1, v8, v1, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 126
    .local v3, pi:Landroid/app/PendingIntent;
    const/4 v5, 0x2

    const-wide/32 v6, 0x493e0

    invoke-virtual {v2, v5, v6, v7, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 127
    new-instance v5, Ljava/lang/Thread;

    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher$3;

    invoke-direct {v6, p0, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/OnBootLuckyPatcher;Landroid/os/Handler;)V

    invoke-direct {v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 237
    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    .line 242
    .end local v0           #handler:Landroid/os/Handler;
    .end local v1           #i:Landroid/content/Intent;
    .end local v2           #mgr:Landroid/app/AlarmManager;
    .end local v3           #pi:Landroid/app/PendingIntent;
    :cond_3
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v6, "OnBootService"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-static {}, Lcom/chelpus/Utils;->exit()V

    .line 244
    :cond_4
    return-void
.end method
