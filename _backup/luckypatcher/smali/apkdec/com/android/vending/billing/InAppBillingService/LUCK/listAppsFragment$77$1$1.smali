.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;
.super Landroid/widget/ArrayAdapter;
.source "listAppsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1;

.field txtStatus:Landroid/widget/TextView;

.field txtTitle:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .parameter "this$2"
    .parameter "x0"
    .parameter "x1"
    .parameter

    .prologue
    .line 9479
    .local p4, x2:Ljava/util/List;,"Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;>;"
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 9487
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;

    .line 9488
    .local v2, p:Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;
    move-object v3, p2

    .line 9490
    .local v3, row:Landroid/view/View;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v5

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 9491
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v5, 0x7f04003f

    invoke-virtual {v1, v5, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 9493
    const v5, 0x7f0d004e

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtTitle:Landroid/widget/TextView;

    .line 9494
    const v5, 0x7f0d004f

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatus:Landroid/widget/TextView;

    .line 9497
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 9498
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 9501
    const v5, 0x7f0d0080

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 9502
    .local v0, chk:Landroid/widget/CheckBox;
    iget-boolean v5, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 9504
    invoke-static {}, Lcom/chelpus/Utils;->isXposedEnabled()Z

    move-result v5

    if-nez v5, :cond_5

    .line 9505
    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 9508
    :goto_0
    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 9509
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x1030046

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 9510
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatus:Landroid/widget/TextView;

    const v6, -0x777778

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 9511
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtTitle:Landroid/widget/TextView;

    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 9512
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtTitle:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Name:Ljava/lang/String;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9513
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtTitle:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 9515
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;

    iget-object v4, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Name:Ljava/lang/String;

    .line 9517
    .local v4, str2:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtTitle:Landroid/widget/TextView;

    const-string v6, "#ff00ff00"

    const-string v7, "bold"

    invoke-static {v4, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9519
    if-nez p1, :cond_0

    .line 9520
    const v5, 0x7f070075

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 9521
    :cond_0
    if-ne p1, v9, :cond_1

    .line 9522
    const v5, 0x7f070077

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 9523
    :cond_1
    const/4 v5, 0x2

    if-ne p1, v5, :cond_2

    .line 9524
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f070079

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f07007a

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 9525
    :cond_2
    const/4 v5, 0x3

    if-ne p1, v5, :cond_3

    .line 9526
    const v5, 0x7f07007d

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 9527
    :cond_3
    const/4 v5, 0x4

    if-ne p1, v5, :cond_4

    .line 9528
    const v5, 0x7f07005a

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 9530
    :cond_4
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatus:Landroid/widget/TextView;

    const-string v6, "#ff888888"

    const-string v7, "italic"

    invoke-static {v4, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 9532
    return-object v3

    .line 9507
    .end local v4           #str2:Ljava/lang/String;
    :cond_5
    invoke-virtual {v0, v9}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto/16 :goto_0
.end method
