.class Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$11;
.super Landroid/widget/ArrayAdapter;
.source "LivepatchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->loadFileList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Landroid/content/Context;II[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;)V
    .locals 0
    .parameter "this$0"
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 599
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$11;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v6, 0x0

    .line 603
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$11;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    .line 628
    .local v1, p:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 629
    .local v3, view:Landroid/view/View;
    const v4, 0x1020014

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 630
    .local v2, textView:Landroid/widget/TextView;
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$11;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    invoke-virtual {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 632
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$11;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 638
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$11;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    .line 639
    #getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;
    invoke-static {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$400(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    move-result-object v4

    aget-object v4, v4, p1

    iget v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;->icon:I

    .line 638
    invoke-virtual {v2, v4, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 640
    const/high16 v4, 0x40a0

    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$11;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x3f00

    add-float/2addr v4, v5

    float-to-int v0, v4

    .line 641
    .local v0, dp5:I
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 646
    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 647
    iget-object v4, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;->file:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 648
    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 650
    return-object v3
.end method
