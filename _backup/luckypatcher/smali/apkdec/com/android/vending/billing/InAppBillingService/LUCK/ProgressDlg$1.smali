.class Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;
.super Ljava/lang/Object;
.source "ProgressDlg.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field current_width:I

.field displayWidth:I

.field orient:I

.field setForSize:I

.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;)V
    .locals 2
    .parameter "this$0"

    .prologue
    const/4 v1, 0x0

    .line 34
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->setForSize:I

    .line 36
    iput v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->displayWidth:I

    .line 37
    const/16 v0, 0xff

    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->orient:I

    .line 38
    iput v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->current_width:I

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 11

    .prologue
    const/16 v10, 0x384

    const/4 v7, 0x0

    .line 41
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-boolean v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->incrementStyle:Z

    if-eqz v6, :cond_4

    .line 42
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->dialog:Landroid/app/Dialog;

    invoke-virtual {v6}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    .line 43
    .local v5, w:Landroid/view/Window;
    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    iput v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->current_width:I

    .line 44
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v0, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 45
    .local v0, display_height:I
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v1, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 46
    .local v1, display_width:I
    if-le v0, v1, :cond_5

    iput v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->orient:I

    .line 48
    :goto_0
    iget v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->displayWidth:I

    if-eqz v6, :cond_0

    iget v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->displayWidth:I

    if-eq v6, v1, :cond_1

    .line 49
    :cond_0
    iput v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->setForSize:I

    .line 50
    iput v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->displayWidth:I

    .line 52
    :cond_1
    iget v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->setForSize:I

    if-eqz v6, :cond_2

    iget v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->current_width:I

    iget v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->setForSize:I

    if-eq v6, v7, :cond_4

    .line 54
    :cond_2
    const/16 v6, 0x11

    invoke-virtual {v5, v6}, Landroid/view/Window;->setGravity(I)V

    .line 56
    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 58
    .local v2, lp:Landroid/view/WindowManager$LayoutParams;
    int-to-double v6, v1

    const-wide v8, 0x3fef5c28f5c28f5cL

    mul-double/2addr v6, v8

    double-to-int v3, v6

    .line 60
    .local v3, max_width:I
    int-to-double v6, v1

    const-wide v8, 0x3fe3333333333333L

    mul-double/2addr v6, v8

    double-to-int v4, v6

    .line 62
    .local v4, min_width:I
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "wight:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->current_width:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 63
    iget v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->orient:I

    packed-switch v6, :pswitch_data_0

    .line 104
    :cond_3
    :goto_1
    invoke-virtual {v5, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 107
    .end local v0           #display_height:I
    .end local v1           #display_width:I
    .end local v2           #lp:Landroid/view/WindowManager$LayoutParams;
    .end local v3           #max_width:I
    .end local v4           #min_width:I
    .end local v5           #w:Landroid/view/Window;
    :cond_4
    return-void

    .line 47
    .restart local v0       #display_height:I
    .restart local v1       #display_width:I
    .restart local v5       #w:Landroid/view/Window;
    :cond_5
    const/4 v6, 0x1

    iput v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->orient:I

    goto :goto_0

    .line 65
    .restart local v2       #lp:Landroid/view/WindowManager$LayoutParams;
    .restart local v3       #max_width:I
    .restart local v4       #min_width:I
    :pswitch_0
    if-ge v1, v10, :cond_7

    .line 66
    iget v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->current_width:I

    if-lt v6, v4, :cond_6

    .line 67
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 68
    iput v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->setForSize:I

    goto :goto_1

    .line 70
    :cond_6
    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 71
    iput v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->setForSize:I

    goto :goto_1

    .line 74
    :cond_7
    iget v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->current_width:I

    if-lt v6, v3, :cond_8

    .line 75
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 76
    iput v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->setForSize:I

    goto :goto_1

    .line 78
    :cond_8
    iget v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->current_width:I

    if-ge v6, v4, :cond_9

    .line 79
    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 80
    iput v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->setForSize:I

    goto :goto_1

    .line 82
    :cond_9
    iget v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->current_width:I

    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    goto :goto_1

    .line 87
    :pswitch_1
    if-ge v1, v10, :cond_a

    .line 88
    iget v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->current_width:I

    if-ge v6, v4, :cond_3

    .line 89
    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 90
    iput v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->setForSize:I

    goto :goto_1

    .line 93
    :cond_a
    iget v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->current_width:I

    if-ge v6, v4, :cond_b

    .line 94
    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 95
    iput v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->setForSize:I

    goto :goto_1

    .line 97
    :cond_b
    iget v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg$1;->current_width:I

    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    goto :goto_1

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
