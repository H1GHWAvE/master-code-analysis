.class public Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DatabaseHelper.java"


# static fields
.field static final ads:Ljava/lang/String; = "ads"

.field static final billing:Ljava/lang/String; = "billing"

.field static final boot_ads:Ljava/lang/String; = "boot_ads"

.field static final boot_custom:Ljava/lang/String; = "boot_custom"

.field static final boot_lvl:Ljava/lang/String; = "boot_lvl"

.field static final boot_manual:Ljava/lang/String; = "boot_manual"

.field public static contextdb:Landroid/content/Context; = null

.field static final custom:Ljava/lang/String; = "custom"

.field public static db:Landroid/database/sqlite/SQLiteDatabase; = null

.field static final dbName:Ljava/lang/String; = "PackagesDB"

.field public static getPackage:Z = false

.field static final hidden:Ljava/lang/String; = "hidden"

.field static final icon:Ljava/lang/String; = "icon"

.field static final lvl:Ljava/lang/String; = "lvl"

.field static final modified:Ljava/lang/String; = "modified"

.field static final odex:Ljava/lang/String; = "odex"

.field static final packagesTable:Ljava/lang/String; = "Packages"

.field static final pkgLabel:Ljava/lang/String; = "pkgLabel"

.field static final pkgName:Ljava/lang/String; = "pkgName"

.field public static savePackage:Z = false

.field static final statusi:Ljava/lang/String; = "statusi"

.field static final stored:Ljava/lang/String; = "stored"

.field static final storepref:Ljava/lang/String; = "storepref"

.field static final system:Ljava/lang/String; = "system"

.field static final updatetime:Ljava/lang/String; = "updatetime"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 49
    sput-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->contextdb:Landroid/content/Context;

    .line 50
    sput-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 51
    sput-boolean v0, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->getPackage:Z

    .line 52
    sput-boolean v0, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->savePackage:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .parameter "context"

    .prologue
    const/16 v4, 0x2a

    .line 56
    const-string v1, "PackagesDB"

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2, v4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 57
    sput-object p1, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->contextdb:Landroid/content/Context;

    .line 59
    :try_start_0
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sput-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 60
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SQLite base version is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 61
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v1

    if-eq v1, v4, :cond_0

    .line 62
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "SQL delete and recreate."

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 63
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "DROP TABLE IF EXISTS Packages"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 64
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 68
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public deletePackage(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V
    .locals 5
    .parameter "deleteObject"

    .prologue
    .line 452
    :try_start_0
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "Packages"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pkgName = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 459
    :goto_0
    return-void

    .line 454
    :catch_0
    move-exception v0

    .line 456
    .local v0, e:Ljava/lang/Exception;
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LuckyPatcher-Error: deletePackage "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deletePackage(Ljava/lang/String;)V
    .locals 5
    .parameter "pkgName"

    .prologue
    .line 464
    :try_start_0
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "Packages"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pkgName = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    :goto_0
    return-void

    .line 466
    :catch_0
    move-exception v0

    .line 468
    .local v0, e:Ljava/lang/Exception;
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LuckyPatcher-Error: deletePackage "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getPackage(ZZ)Ljava/util/ArrayList;
    .locals 41
    .parameter "All"
    .parameter "skipIconRead"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    new-instance v33, Ljava/util/ArrayList;

    invoke-direct/range {v33 .. v33}, Ljava/util/ArrayList;-><init>()V

    .line 100
    .local v33, pat:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    const/16 v23, 0x0

    .line 101
    .local v23, enable:Z
    const/16 v24, 0x0

    .line 102
    .local v24, sd_on:Z
    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->clear()V

    .line 114
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v35

    .line 115
    .local v35, pkg:Landroid/content/pm/PackageManager;
    const/4 v3, 0x1

    sput-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->getPackage:Z

    .line 119
    :try_start_0
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "Packages"

    const/16 v25, 0x13

    move/from16 v0, v25

    new-array v4, v0, [Ljava/lang/String;

    const/16 v25, 0x0

    const-string v37, "pkgName"

    aput-object v37, v4, v25

    const/16 v25, 0x1

    const-string v37, "pkgLabel"

    aput-object v37, v4, v25

    const/16 v25, 0x2

    const-string v37, "stored"

    aput-object v37, v4, v25

    const/16 v25, 0x3

    const-string v37, "storepref"

    aput-object v37, v4, v25

    const/16 v25, 0x4

    const-string v37, "hidden"

    aput-object v37, v4, v25

    const/16 v25, 0x5

    const-string v37, "statusi"

    aput-object v37, v4, v25

    const/16 v25, 0x6

    const-string v37, "boot_ads"

    aput-object v37, v4, v25

    const/16 v25, 0x7

    const-string v37, "boot_lvl"

    aput-object v37, v4, v25

    const/16 v25, 0x8

    const-string v37, "boot_custom"

    aput-object v37, v4, v25

    const/16 v25, 0x9

    const-string v37, "boot_manual"

    aput-object v37, v4, v25

    const/16 v25, 0xa

    const-string v37, "custom"

    aput-object v37, v4, v25

    const/16 v25, 0xb

    const-string v37, "lvl"

    aput-object v37, v4, v25

    const/16 v25, 0xc

    const-string v37, "ads"

    aput-object v37, v4, v25

    const/16 v25, 0xd

    const-string v37, "modified"

    aput-object v37, v4, v25

    const/16 v25, 0xe

    const-string v37, "system"

    aput-object v37, v4, v25

    const/16 v25, 0xf

    const-string v37, "odex"

    aput-object v37, v4, v25

    const/16 v25, 0x10

    const-string v37, "icon"

    aput-object v37, v4, v25

    const/16 v25, 0x11

    const-string v37, "updatetime"

    aput-object v37, v4, v25

    const/16 v25, 0x12

    const-string v37, "billing"

    aput-object v37, v4, v25

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v27

    .line 122
    .local v27, c:Landroid/database/Cursor;
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    .line 126
    :cond_0
    :try_start_1
    const-string v3, "pkgName"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v4

    .line 128
    .local v4, pkgName:Ljava/lang/String;
    const/4 v3, 0x0

    :try_start_2
    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v0, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v26, v0

    .line 129
    .local v26, appInfo:Landroid/content/pm/ApplicationInfo;
    move-object/from16 v0, v26

    iget-boolean v0, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    move/from16 v23, v0

    .line 130
    move-object/from16 v0, v26

    iget v3, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v25, 0x4

    and-int v3, v3, v25

    const/high16 v25, 0x4

    move/from16 v0, v25

    if-ne v3, v0, :cond_4

    const/16 v24, 0x1

    .line 133
    :goto_0
    const-string v3, "pkgLabel"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 134
    .local v5, pkgLabel:Ljava/lang/String;
    const-string v3, "stored"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 135
    .local v6, stored:I
    const-string v3, "storepref"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 136
    .local v7, storepref:I
    const-string v3, "hidden"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 137
    .local v8, hidden:I
    const-string v3, "statusi"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 138
    .local v9, statusi:Ljava/lang/String;
    const-string v3, "boot_ads"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 139
    .local v10, boot_ads:I
    const-string v3, "boot_lvl"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 140
    .local v11, boot_lvl:I
    const-string v3, "boot_custom"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 141
    .local v12, boot_custom:I
    const-string v3, "boot_manual"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 142
    .local v13, boot_manual:I
    const-string v3, "custom"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 143
    .local v14, custom:I
    const-string v3, "lvl"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 144
    .local v15, lvl:I
    const-string v3, "ads"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 145
    .local v16, ads:I
    const-string v3, "modified"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 146
    .local v17, modified:I
    const-string v3, "system"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 147
    .local v18, system:I
    const-string v3, "odex"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 148
    .local v19, odex:I
    const-string v3, "billing"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    move-result v22

    .line 150
    .local v22, billing:I
    const/16 v20, 0x0

    .line 151
    .local v20, icon:Landroid/graphics/Bitmap;
    if-nez p2, :cond_1

    .line 153
    :try_start_3
    const-string v3, "icon"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v30

    .line 154
    .local v30, imageByteArray:[B
    if-eqz v30, :cond_1

    .line 156
    new-instance v31, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 157
    .local v31, imageStream:Ljava/io/ByteArrayInputStream;
    invoke-static/range {v31 .. v31}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v20

    .line 166
    .end local v30           #imageByteArray:[B
    .end local v31           #imageStream:Ljava/io/ByteArrayInputStream;
    :cond_1
    :goto_1
    :try_start_4
    const-string v3, "updatetime"

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 170
    .local v21, updatetime:I
    if-eqz v6, :cond_5

    .line 173
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v37

    const-wide/16 v39, 0x3e8

    div-long v37, v37, v39

    move-wide/from16 v0, v37

    long-to-int v3, v0

    sub-int v3, v3, v21

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const v25, 0x15180

    sget v37, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I

    mul-int v25, v25, v37

    move/from16 v0, v25

    if-ge v3, v0, :cond_2

    .line 175
    const/4 v6, 0x0

    .line 176
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "UPDATE Packages SET stored=0 WHERE pkgName=\'"

    move-object/from16 v0, v25

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v37, "\'"

    move-object/from16 v0, v25

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 178
    :cond_2
    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v3

    move/from16 v25, p2

    invoke-direct/range {v2 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;IIIIIIIIIILandroid/graphics/Bitmap;IIZZZ)V

    .line 193
    .local v2, item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :goto_2
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v25, "systemapp"

    const/16 v37, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v37

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v36

    .line 194
    .local v36, sysshow:Z
    if-nez p1, :cond_11

    .line 195
    const-string v3, "android"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-class v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 197
    :cond_3
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 310
    .end local v2           #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .end local v5           #pkgLabel:Ljava/lang/String;
    .end local v6           #stored:I
    .end local v7           #storepref:I
    .end local v8           #hidden:I
    .end local v9           #statusi:Ljava/lang/String;
    .end local v10           #boot_ads:I
    .end local v11           #boot_lvl:I
    .end local v12           #boot_custom:I
    .end local v13           #boot_manual:I
    .end local v14           #custom:I
    .end local v15           #lvl:I
    .end local v16           #ads:I
    .end local v17           #modified:I
    .end local v18           #system:I
    .end local v19           #odex:I
    .end local v20           #icon:Landroid/graphics/Bitmap;
    .end local v21           #updatetime:I
    .end local v22           #billing:I
    .end local v26           #appInfo:Landroid/content/pm/ApplicationInfo;
    .end local v36           #sysshow:Z
    :catch_0
    move-exception v3

    .line 320
    .end local v4           #pkgName:Ljava/lang/String;
    :goto_3
    :try_start_5
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 321
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    .line 331
    :goto_4
    const/4 v3, 0x0

    :try_start_6
    sput-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->getPackage:Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    .line 337
    .end local v27           #c:Landroid/database/Cursor;
    :goto_5
    return-object v33

    .line 130
    .restart local v4       #pkgName:Ljava/lang/String;
    .restart local v26       #appInfo:Landroid/content/pm/ApplicationInfo;
    .restart local v27       #c:Landroid/database/Cursor;
    :cond_4
    const/16 v24, 0x0

    goto/16 :goto_0

    .line 160
    .restart local v5       #pkgLabel:Ljava/lang/String;
    .restart local v6       #stored:I
    .restart local v7       #storepref:I
    .restart local v8       #hidden:I
    .restart local v9       #statusi:Ljava/lang/String;
    .restart local v10       #boot_ads:I
    .restart local v11       #boot_lvl:I
    .restart local v12       #boot_custom:I
    .restart local v13       #boot_manual:I
    .restart local v14       #custom:I
    .restart local v15       #lvl:I
    .restart local v16       #ads:I
    .restart local v17       #modified:I
    .restart local v18       #system:I
    .restart local v19       #odex:I
    .restart local v20       #icon:Landroid/graphics/Bitmap;
    .restart local v22       #billing:I
    :catch_1
    move-exception v28

    .line 161
    .local v28, e:Ljava/lang/Exception;
    :try_start_7
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 315
    .end local v5           #pkgLabel:Ljava/lang/String;
    .end local v6           #stored:I
    .end local v7           #storepref:I
    .end local v8           #hidden:I
    .end local v9           #statusi:Ljava/lang/String;
    .end local v10           #boot_ads:I
    .end local v11           #boot_lvl:I
    .end local v12           #boot_custom:I
    .end local v13           #boot_manual:I
    .end local v14           #custom:I
    .end local v15           #lvl:I
    .end local v16           #ads:I
    .end local v17           #modified:I
    .end local v18           #system:I
    .end local v19           #odex:I
    .end local v20           #icon:Landroid/graphics/Bitmap;
    .end local v22           #billing:I
    .end local v26           #appInfo:Landroid/content/pm/ApplicationInfo;
    .end local v28           #e:Ljava/lang/Exception;
    :catch_2
    move-exception v3

    goto :goto_3

    .line 162
    .restart local v5       #pkgLabel:Ljava/lang/String;
    .restart local v6       #stored:I
    .restart local v7       #storepref:I
    .restart local v8       #hidden:I
    .restart local v9       #statusi:Ljava/lang/String;
    .restart local v10       #boot_ads:I
    .restart local v11       #boot_lvl:I
    .restart local v12       #boot_custom:I
    .restart local v13       #boot_manual:I
    .restart local v14       #custom:I
    .restart local v15       #lvl:I
    .restart local v16       #ads:I
    .restart local v17       #modified:I
    .restart local v18       #system:I
    .restart local v19       #odex:I
    .restart local v20       #icon:Landroid/graphics/Bitmap;
    .restart local v22       #billing:I
    .restart local v26       #appInfo:Landroid/content/pm/ApplicationInfo;
    :catch_3
    move-exception v28

    .line 163
    .local v28, e:Ljava/lang/OutOfMemoryError;
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 182
    .end local v28           #e:Ljava/lang/OutOfMemoryError;
    .restart local v21       #updatetime:I
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v37

    const-wide/16 v39, 0x3e8

    div-long v37, v37, v39

    move-wide/from16 v0, v37

    long-to-int v3, v0

    sub-int v3, v3, v21

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const v25, 0x15180

    sget v37, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I

    mul-int v25, v25, v37

    move/from16 v0, v25

    if-le v3, v0, :cond_6

    .line 183
    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v3

    sget v25, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I

    move/from16 v0, v25

    move/from16 v1, p2

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;-><init>(Landroid/content/Context;Ljava/lang/String;IZ)V

    .line 184
    .restart local v2       #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->savePackage(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V

    goto/16 :goto_2

    .line 317
    .end local v2           #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .end local v4           #pkgName:Ljava/lang/String;
    .end local v5           #pkgLabel:Ljava/lang/String;
    .end local v6           #stored:I
    .end local v7           #storepref:I
    .end local v8           #hidden:I
    .end local v9           #statusi:Ljava/lang/String;
    .end local v10           #boot_ads:I
    .end local v11           #boot_lvl:I
    .end local v12           #boot_custom:I
    .end local v13           #boot_manual:I
    .end local v14           #custom:I
    .end local v15           #lvl:I
    .end local v16           #ads:I
    .end local v17           #modified:I
    .end local v18           #system:I
    .end local v19           #odex:I
    .end local v20           #icon:Landroid/graphics/Bitmap;
    .end local v21           #updatetime:I
    .end local v22           #billing:I
    .end local v26           #appInfo:Landroid/content/pm/ApplicationInfo;
    :catch_4
    move-exception v3

    goto :goto_3

    .line 186
    .restart local v4       #pkgName:Ljava/lang/String;
    .restart local v5       #pkgLabel:Ljava/lang/String;
    .restart local v6       #stored:I
    .restart local v7       #storepref:I
    .restart local v8       #hidden:I
    .restart local v9       #statusi:Ljava/lang/String;
    .restart local v10       #boot_ads:I
    .restart local v11       #boot_lvl:I
    .restart local v12       #boot_custom:I
    .restart local v13       #boot_manual:I
    .restart local v14       #custom:I
    .restart local v15       #lvl:I
    .restart local v16       #ads:I
    .restart local v17       #modified:I
    .restart local v18       #system:I
    .restart local v19       #odex:I
    .restart local v20       #icon:Landroid/graphics/Bitmap;
    .restart local v21       #updatetime:I
    .restart local v22       #billing:I
    .restart local v26       #appInfo:Landroid/content/pm/ApplicationInfo;
    :cond_6
    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v3

    move/from16 v25, p2

    invoke-direct/range {v2 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;IIIIIIIIIILandroid/graphics/Bitmap;IIZZZ)V

    .restart local v2       #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    goto/16 :goto_2

    .line 200
    .restart local v36       #sysshow:Z
    :cond_7
    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-eqz v3, :cond_8

    if-nez v36, :cond_8

    .line 201
    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-nez v3, :cond_8

    .line 202
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan filter"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 204
    :cond_8
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v25, "lvlapp"

    const/16 v37, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v37

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_9

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-eqz v3, :cond_9

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-nez v3, :cond_9

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-nez v3, :cond_9

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    if-nez v3, :cond_9

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    if-nez v3, :cond_9

    .line 205
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 206
    :cond_9
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v25, "adsapp"

    const/16 v37, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v37

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_a

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-eqz v3, :cond_a

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-nez v3, :cond_a

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-nez v3, :cond_a

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    if-nez v3, :cond_a

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    if-nez v3, :cond_a

    .line 207
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 208
    :cond_a
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v25, "lvlapp"

    const/16 v37, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v37

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_b

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v25, "adsapp"

    const/16 v37, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v37

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_b

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-eqz v3, :cond_b

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-eqz v3, :cond_b

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-nez v3, :cond_b

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    if-nez v3, :cond_b

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    if-nez v3, :cond_b

    .line 209
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 210
    :cond_b
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v25, "noneapp"

    const/16 v37, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v37

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_c

    const/4 v3, 0x6

    if-ne v6, v3, :cond_c

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    if-nez v3, :cond_c

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    if-nez v3, :cond_c

    .line 211
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 212
    :cond_c
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v25, "customapp"

    const/16 v37, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v37

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_d

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-eqz v3, :cond_d

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    if-nez v3, :cond_d

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    if-nez v3, :cond_d

    .line 213
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 214
    :cond_d
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v25, "fixedapp"

    const/16 v37, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v37

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_e

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    if-eqz v3, :cond_e

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    if-nez v3, :cond_e

    .line 215
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 217
    :cond_e
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v25, "modifapp"

    const/16 v37, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v37

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_f

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    if-nez v3, :cond_f

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    if-eqz v3, :cond_f

    .line 218
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 220
    :cond_f
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v25, "fixedapp"

    const/16 v37, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v37

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_14

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v25, "modifapp"

    const/16 v37, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v37

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_14

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    if-nez v3, :cond_10

    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    if-eqz v3, :cond_14

    .line 221
    :cond_10
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 223
    :cond_11
    const-string v3, "android"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->contextdb:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    :cond_12
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 224
    :cond_13
    sget v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    if-eqz v3, :cond_14

    .line 225
    sget v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    sparse-switch v3, :sswitch_data_0

    .line 309
    :cond_14
    :goto_6
    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 227
    :sswitch_0
    const/4 v3, 0x0

    sput v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    goto :goto_6

    .line 230
    :sswitch_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v37

    const-wide/16 v39, 0x3e8

    div-long v37, v37, v39

    move-wide/from16 v0, v37

    long-to-int v3, v0

    sub-int v3, v3, v21

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const v25, 0x15180

    move/from16 v0, v25

    if-le v3, v0, :cond_14

    .line 231
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 235
    :sswitch_2
    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-nez v3, :cond_14

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 238
    :sswitch_3
    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-nez v3, :cond_14

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 241
    :sswitch_4
    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-nez v3, :cond_14

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 244
    :sswitch_5
    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    if-nez v3, :cond_14

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 247
    :sswitch_6
    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    if-nez v3, :cond_14

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 251
    :sswitch_7
    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    if-nez v3, :cond_14

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 254
    :sswitch_8
    move-object/from16 v0, v26

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v25, "/data/"

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 257
    :sswitch_9
    move-object/from16 v0, v26

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v25, "/mnt/"

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 260
    :sswitch_a
    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-nez v3, :cond_15

    iget-object v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-static {v3}, Lcom/chelpus/Utils;->isInstalledOnSdCard(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    :cond_15
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 263
    :sswitch_b
    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-nez v3, :cond_14

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 266
    :sswitch_c
    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-eqz v3, :cond_16

    move-object/from16 v0, v26

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v25, "/data/"

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    move-object/from16 v0, v26

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v25, "/mnt/"

    .line 267
    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    :cond_16
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 270
    :sswitch_d
    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-eqz v3, :cond_14

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 273
    :sswitch_e
    const-string v3, "android.permission.INTERNET"

    iget-object v0, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v25

    invoke-virtual {v0, v3, v1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    const/16 v25, -0x1

    move/from16 v0, v25

    if-ne v3, v0, :cond_17

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 275
    :cond_17
    new-instance v3, Landroid/content/ComponentName;

    iget-object v0, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string v37, "android.permission.INTERNET"

    move-object/from16 v0, v25

    move-object/from16 v1, v37

    invoke-direct {v3, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v35

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    const/16 v25, 0x2

    move/from16 v0, v25

    if-ne v3, v0, :cond_14

    .line 276
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 280
    :sswitch_f
    const-string v3, "android.permission.INTERNET"

    iget-object v0, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v25

    invoke-virtual {v0, v3, v1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    const/16 v25, -0x1

    move/from16 v0, v25

    if-ne v3, v0, :cond_18

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 282
    :cond_18
    new-instance v3, Landroid/content/ComponentName;

    iget-object v0, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string v37, "android.permission.INTERNET"

    move-object/from16 v0, v25

    move-object/from16 v1, v37

    invoke-direct {v3, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v35

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    const/16 v25, 0x1

    move/from16 v0, v25

    if-eq v3, v0, :cond_19

    new-instance v3, Landroid/content/ComponentName;

    iget-object v0, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v25, v0

    const-string v37, "android.permission.INTERNET"

    move-object/from16 v0, v25

    move-object/from16 v1, v37

    invoke-direct {v3, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    move-object/from16 v0, v35

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    if-nez v3, :cond_14

    .line 284
    :cond_19
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 288
    :sswitch_10
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v0, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v25, v0

    const/16 v37, 0x1000

    move-object/from16 v0, v25

    move/from16 v1, v37

    invoke-virtual {v3, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v0, v3, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    move-object/from16 v34, v0

    .line 290
    .local v34, perm:[Ljava/lang/String;
    const/16 v29, 0x0

    .line 291
    .local v29, found:Z
    if-eqz v34, :cond_1c

    .line 292
    move-object/from16 v0, v34

    array-length v0, v0

    move/from16 v25, v0

    const/4 v3, 0x0

    :goto_7
    move/from16 v0, v25

    if-ge v3, v0, :cond_1c

    aget-object v32, v34, v3

    .line 293
    .local v32, p:Ljava/lang/String;
    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v37

    const-string v38, "SEND_SMS"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v37

    if-nez v37, :cond_1a

    const-string v37, "CALL_PHONE"

    move-object/from16 v0, v32

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v37

    if-eqz v37, :cond_1b

    .line 294
    :cond_1a
    const/16 v29, 0x1

    .line 292
    :cond_1b
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 298
    .end local v32           #p:Ljava/lang/String;
    :cond_1c
    if-nez v29, :cond_14

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 303
    .end local v29           #found:Z
    .end local v34           #perm:[Ljava/lang/String;
    :sswitch_11
    iget-boolean v3, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    if-eqz v3, :cond_14

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v25, "package scan error"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_7
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    .line 324
    .end local v2           #item:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .end local v4           #pkgName:Ljava/lang/String;
    .end local v5           #pkgLabel:Ljava/lang/String;
    .end local v6           #stored:I
    .end local v7           #storepref:I
    .end local v8           #hidden:I
    .end local v9           #statusi:Ljava/lang/String;
    .end local v10           #boot_ads:I
    .end local v11           #boot_lvl:I
    .end local v12           #boot_custom:I
    .end local v13           #boot_manual:I
    .end local v14           #custom:I
    .end local v15           #lvl:I
    .end local v16           #ads:I
    .end local v17           #modified:I
    .end local v18           #system:I
    .end local v19           #odex:I
    .end local v20           #icon:Landroid/graphics/Bitmap;
    .end local v21           #updatetime:I
    .end local v22           #billing:I
    .end local v26           #appInfo:Landroid/content/pm/ApplicationInfo;
    .end local v36           #sysshow:Z
    :catch_5
    move-exception v28

    .line 325
    .local v28, e:Ljava/lang/Exception;
    :try_start_8
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    goto/16 :goto_4

    .line 332
    .end local v27           #c:Landroid/database/Cursor;
    .end local v28           #e:Ljava/lang/Exception;
    :catch_6
    move-exception v28

    .line 333
    .restart local v28       #e:Ljava/lang/Exception;
    const/4 v3, 0x0

    sput-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->getPackage:Z

    .line 335
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "LuckyPatcher-Error: getPackage "

    move-object/from16 v0, v25

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 225
    :sswitch_data_0
    .sparse-switch
        0x789 -> :sswitch_a
        0x78a -> :sswitch_d
        0x78b -> :sswitch_e
        0x78c -> :sswitch_f
        0x7f0d000f -> :sswitch_0
        0x7f0d0010 -> :sswitch_1
        0x7f0d0011 -> :sswitch_2
        0x7f0d0012 -> :sswitch_3
        0x7f0d0013 -> :sswitch_4
        0x7f0d0014 -> :sswitch_5
        0x7f0d0015 -> :sswitch_6
        0x7f0d0016 -> :sswitch_7
        0x7f0d0017 -> :sswitch_8
        0x7f0d0018 -> :sswitch_9
        0x7f0d0019 -> :sswitch_11
        0x7f0d001a -> :sswitch_b
        0x7f0d001b -> :sswitch_c
        0x7f0d001c -> :sswitch_10
    .end sparse-switch
.end method

.method public isOpen()Z
    .locals 1

    .prologue
    .line 479
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 480
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .parameter "db"

    .prologue
    .line 80
    const-string v0, "CREATE TABLE Packages (pkgName TEXT PRIMARY KEY, pkgLabel TEXT, stored Integer, storepref Integer, hidden Integer, statusi TEXT, boot_ads Integer, boot_lvl Integer, boot_custom Integer, boot_manual Integer, custom Integer, lvl Integer, ads Integer, modified Integer, system Integer, odex Integer, icon BLOB, updatetime Integer, billing Integer );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    .prologue
    .line 93
    const-string v0, "DROP TABLE IF EXISTS Packages"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 96
    return-void
.end method

.method public savePackage(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V
    .locals 11
    .parameter "savedObject"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/sqlite/SQLiteException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 352
    const/4 v7, 0x1

    :try_start_0
    sput-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->savePackage:Z

    .line 354
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 356
    .local v1, cv:Landroid/content/ContentValues;
    const-string v7, "pkgName"

    iget-object v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    const-string v7, "pkgLabel"

    iget-object v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    const-string v7, "stored"

    iget v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 359
    const-string v7, "storepref"

    iget v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->storepref:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 360
    const-string v7, "hidden"

    iget-boolean v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 361
    const-string v7, "statusi"

    iget-object v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->statusi:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    const-string v7, "boot_ads"

    iget-boolean v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 363
    const-string v7, "boot_lvl"

    iget-boolean v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 364
    const-string v7, "boot_custom"

    iget-boolean v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 365
    const-string v7, "boot_manual"

    iget-boolean v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 366
    const-string v7, "custom"

    iget-boolean v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 367
    const-string v7, "lvl"

    iget-boolean v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 368
    const-string v7, "ads"

    iget-boolean v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 369
    const-string v7, "modified"

    iget-boolean v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 370
    const-string v7, "system"

    iget-boolean v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 371
    const-string v7, "odex"

    iget-boolean v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 372
    const-string v7, "updatetime"

    iget v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 373
    const-string v7, "billing"

    iget-boolean v8, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 375
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 376
    .local v6, pm:Landroid/content/pm/PackageManager;
    iget-object v7, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    iget-object v5, v7, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 387
    .local v5, pkgdir:Ljava/lang/String;
    const/4 v3, 0x0

    .line 416
    .local v3, image:Landroid/graphics/Bitmap;
    :try_start_1
    iget-object v7, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->icon:Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_0

    .line 417
    iget-object v7, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->icon:Landroid/graphics/drawable/Drawable;

    check-cast v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 419
    :cond_0
    if-eqz v3, :cond_1

    .line 421
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 423
    .local v4, out:Ljava/io/ByteArrayOutputStream;
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x64

    invoke-virtual {v3, v7, v8, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 425
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 427
    .local v0, buffer:[B
    const-string v7, "icon"

    invoke-virtual {v1, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 431
    .end local v0           #buffer:[B
    .end local v4           #out:Ljava/io/ByteArrayOutputStream;
    :cond_1
    :goto_0
    const/4 v6, 0x0

    .line 434
    :try_start_2
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v8, "Packages"

    const-string v9, "pkgName"

    invoke-virtual {v7, v8, v9, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 439
    :goto_1
    const/4 v7, 0x0

    :try_start_3
    sput-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->savePackage:Z

    .line 441
    const/4 v7, 0x0

    sput-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->savePackage:Z

    .line 447
    .end local v1           #cv:Landroid/content/ContentValues;
    .end local v3           #image:Landroid/graphics/Bitmap;
    .end local v5           #pkgdir:Ljava/lang/String;
    .end local v6           #pm:Landroid/content/pm/PackageManager;
    :goto_2
    return-void

    .line 435
    .restart local v1       #cv:Landroid/content/ContentValues;
    .restart local v3       #image:Landroid/graphics/Bitmap;
    .restart local v5       #pkgdir:Ljava/lang/String;
    .restart local v6       #pm:Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v2

    .line 437
    .local v2, e:Ljava/lang/Exception;
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v8, "Packages"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9, v1}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 442
    .end local v1           #cv:Landroid/content/ContentValues;
    .end local v2           #e:Ljava/lang/Exception;
    .end local v3           #image:Landroid/graphics/Bitmap;
    .end local v5           #pkgdir:Ljava/lang/String;
    .end local v6           #pm:Landroid/content/pm/PackageManager;
    :catch_1
    move-exception v2

    .line 443
    .restart local v2       #e:Ljava/lang/Exception;
    sput-boolean v10, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->savePackage:Z

    .line 445
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "LuckyPatcher-Error: savePackage "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_2

    .line 429
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v1       #cv:Landroid/content/ContentValues;
    .restart local v3       #image:Landroid/graphics/Bitmap;
    .restart local v5       #pkgdir:Ljava/lang/String;
    .restart local v6       #pm:Landroid/content/pm/PackageManager;
    :catch_2
    move-exception v7

    goto :goto_0
.end method

.method public updatePackage(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 476
    .local p1, pli:Ljava/util/List;,"Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    return-void
.end method
