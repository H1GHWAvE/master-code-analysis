.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolbar_createapkpermissions(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .locals 0
    .parameter "this$0"

    .prologue
    .line 13693
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 36

    .prologue
    .line 13697
    sget-object v32, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;

    invoke-static/range {v32 .. v32}, Lcom/chelpus/Utils;->getApkInfo(Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;

    move-result-object v21

    .line 13698
    .local v21, pli:Landroid/content/pm/ApplicationInfo;
    sget-object v32, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;

    invoke-static/range {v32 .. v32}, Lcom/chelpus/Utils;->getApkPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v20

    .line 13699
    .local v20, pkgInfo:Landroid/content/pm/PackageInfo;
    const/4 v11, 0x0

    .line 13700
    .local v11, error:Z
    const-string v28, ""

    .line 13701
    .local v28, versionPkg:Ljava/lang/String;
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, ".v."

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, ".b."

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v20

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    move/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 13702
    if-nez v11, :cond_1

    .line 13703
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;

    .line 13704
    .local v8, destFile:Ljava/lang/String;
    new-instance v32, Ljava/io/File;

    sget-object v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;

    invoke-direct/range {v32 .. v33}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->exists()Z

    move-result v32

    if-eqz v32, :cond_8

    .line 13705
    const/4 v13, 0x0

    .line 13707
    .local v13, extrFile:Ljava/lang/String;
    :try_start_0
    new-instance v14, Ljava/io/FileInputStream;

    invoke-direct {v14, v8}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 13708
    .local v14, fin:Ljava/io/FileInputStream;
    new-instance v30, Ljava/util/zip/ZipInputStream;

    move-object/from16 v0, v30

    invoke-direct {v0, v14}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 13709
    .local v30, zin:Ljava/util/zip/ZipInputStream;
    const/16 v29, 0x0

    .line 13710
    .local v29, ze:Ljava/util/zip/ZipEntry;
    :cond_0
    :goto_0
    invoke-virtual/range {v30 .. v30}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v29

    if-eqz v29, :cond_3

    .line 13715
    invoke-virtual/range {v29 .. v29}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v32

    const-string v33, "AndroidManifest.xml"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_0

    .line 13716
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "/Modified/"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "AndroidManifest.xml"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 13717
    new-instance v17, Ljava/io/FileOutputStream;

    move-object/from16 v0, v17

    invoke-direct {v0, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 13719
    .local v17, fout:Ljava/io/FileOutputStream;
    const/16 v32, 0x2000

    move/from16 v0, v32

    new-array v5, v0, [B

    .line 13721
    .local v5, buffer:[B
    :goto_1
    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v18

    .local v18, length:I
    const/16 v32, -0x1

    move/from16 v0, v18

    move/from16 v1, v32

    if-eq v0, v1, :cond_2

    .line 13722
    const/16 v32, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v32

    move/from16 v2, v18

    invoke-virtual {v0, v5, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 13732
    .end local v5           #buffer:[B
    .end local v14           #fin:Ljava/io/FileInputStream;
    .end local v17           #fout:Ljava/io/FileOutputStream;
    .end local v18           #length:I
    .end local v29           #ze:Ljava/util/zip/ZipEntry;
    .end local v30           #zin:Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v9

    .line 13734
    .local v9, e:Ljava/lang/Exception;
    :try_start_1
    new-instance v31, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v31

    invoke-direct {v0, v8}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 13735
    .local v31, zipFile:Lnet/lingala/zip4j/core/ZipFile;
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "/Modified/"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "AndroidManifest.xml"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 13736
    const-string v32, "AndroidManifest.xml"

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v34, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "/Modified"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v31 .. v33}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 13749
    .end local v9           #e:Ljava/lang/Exception;
    .end local v31           #zipFile:Lnet/lingala/zip4j/core/ZipFile;
    :goto_2
    new-instance v32, Ljava/io/File;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v34, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "/Modified/AndroidManifest.xml"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->exists()Z

    move-result v32

    if-nez v32, :cond_4

    .line 13751
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v32, v0

    new-instance v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114$1;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;)V

    invoke-virtual/range {v32 .. v33}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 13874
    .end local v8           #destFile:Ljava/lang/String;
    .end local v13           #extrFile:Ljava/lang/String;
    :cond_1
    :goto_3
    return-void

    .line 13724
    .restart local v5       #buffer:[B
    .restart local v8       #destFile:Ljava/lang/String;
    .restart local v13       #extrFile:Ljava/lang/String;
    .restart local v14       #fin:Ljava/io/FileInputStream;
    .restart local v17       #fout:Ljava/io/FileOutputStream;
    .restart local v18       #length:I
    .restart local v29       #ze:Ljava/util/zip/ZipEntry;
    .restart local v30       #zin:Ljava/util/zip/ZipInputStream;
    :cond_2
    :try_start_2
    invoke-virtual/range {v30 .. v30}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 13725
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_0

    .line 13730
    .end local v5           #buffer:[B
    .end local v17           #fout:Ljava/io/FileOutputStream;
    .end local v18           #length:I
    :cond_3
    invoke-virtual/range {v30 .. v30}, Ljava/util/zip/ZipInputStream;->close()V

    .line 13731
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 13738
    .end local v14           #fin:Ljava/io/FileInputStream;
    .end local v29           #ze:Ljava/util/zip/ZipEntry;
    .end local v30           #zin:Ljava/util/zip/ZipInputStream;
    .restart local v9       #e:Ljava/lang/Exception;
    :catch_1
    move-exception v10

    .line 13739
    .local v10, e1:Lnet/lingala/zip4j/exception/ZipException;
    sget-object v32, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "Error classes.dex decompress! "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 13740
    invoke-virtual {v10}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    goto :goto_2

    .line 13741
    .end local v10           #e1:Lnet/lingala/zip4j/exception/ZipException;
    :catch_2
    move-exception v10

    .line 13742
    .local v10, e1:Ljava/lang/Exception;
    sget-object v32, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "Error classes.dex decompress! "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 13743
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 13761
    .end local v9           #e:Ljava/lang/Exception;
    .end local v10           #e1:Ljava/lang/Exception;
    :cond_4
    new-instance v12, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;

    invoke-direct {v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;-><init>()V

    .line 13763
    .local v12, ex:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;
    new-instance v19, Ljava/io/File;

    move-object/from16 v0, v19

    invoke-direct {v0, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 13766
    .local v19, manifestFile:Ljava/io/File;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v32, v0

    #getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->permissions:Ljava/util/ArrayList;
    invoke-static/range {v32 .. v32}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->access$2200(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)Ljava/util/ArrayList;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v33, v0

    #getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->activities:Ljava/util/ArrayList;
    invoke-static/range {v33 .. v33}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->access$2300(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)Ljava/util/ArrayList;

    move-result-object v33

    move-object/from16 v0, v19

    move-object/from16 v1, v32

    move-object/from16 v2, v33

    invoke-virtual {v12, v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->disablePermisson(Ljava/io/File;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    .line 13795
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v32, v0

    #calls: Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSignatureKeys()V
    invoke-static/range {v32 .. v32}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->access$1300(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V

    .line 13796
    new-instance v27, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;

    invoke-direct/range {v27 .. v27}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;-><init>()V

    .line 13798
    .local v27, signer:Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;
    :try_start_4
    new-instance v32, Ljava/io/File;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v34, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "/Modified/Keys/"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "testkey.x509.pem"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v7

    .line 13799
    .local v7, certUrl:Ljava/net/URL;
    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->readPublicKey(Ljava/net/URL;)Ljava/security/cert/X509Certificate;

    move-result-object v6

    .line 13800
    .local v6, cert:Ljava/security/cert/X509Certificate;
    new-instance v32, Ljava/io/File;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v34, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "/Modified/Keys/"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "testkey.sbt"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v25

    .line 13801
    .local v25, sbtUrl:Ljava/net/URL;
    move-object/from16 v0, v27

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->readContentAsBytes(Ljava/net/URL;)[B

    move-result-object v26

    .line 13802
    .local v26, sigBlockTemplate:[B
    new-instance v32, Ljava/io/File;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v34, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "/Modified/Keys/"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "testkey.pk8"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v23

    .line 13803
    .local v23, privateKeyUrl:Ljava/net/URL;
    const-string v32, "lp"

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->readPrivateKey(Ljava/net/URL;Ljava/lang/String;)Ljava/security/PrivateKey;

    move-result-object v22

    .line 13804
    .local v22, privateKey:Ljava/security/PrivateKey;
    const-string v32, "custom"

    move-object/from16 v0, v27

    move-object/from16 v1, v32

    move-object/from16 v2, v22

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v6, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->setKeys(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V

    .line 13806
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 13807
    .local v4, add_files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    new-instance v32, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v34, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "/Modified/AndroidManifest.xml"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v35, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, "/Modified/"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-direct/range {v32 .. v34}, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 13809
    sget-object v32, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v34, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "/Modified/"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v21

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "."

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "Permissions.Removed"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, ".apk"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v27

    move-object/from16 v1, v32

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v2, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->signZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_6

    .line 13816
    .end local v4           #add_files:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v6           #cert:Ljava/security/cert/X509Certificate;
    .end local v7           #certUrl:Ljava/net/URL;
    .end local v22           #privateKey:Ljava/security/PrivateKey;
    .end local v23           #privateKeyUrl:Ljava/net/URL;
    .end local v25           #sbtUrl:Ljava/net/URL;
    .end local v26           #sigBlockTemplate:[B
    :goto_4
    new-instance v15, Ljava/io/File;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "/Modified/"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v21

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "."

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "Permissions.Removed"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, ".apk"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-direct {v15, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 13817
    .local v15, finalapk:Ljava/io/File;
    const-string v24, ""

    .line 13818
    .local v24, resultfile:Ljava/lang/String;
    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v32

    if-eqz v32, :cond_7

    .line 13820
    new-instance v16, Ljava/io/File;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "/Modified/"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    sget-object v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;

    invoke-static/range {v33 .. v33}, Lcom/chelpus/Utils;->getApkLabelName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    const-string v34, " "

    const-string v35, "."

    invoke-virtual/range {v33 .. v35}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v16

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 13821
    .local v16, folderstorage:Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->mkdirs()Z

    .line 13823
    :try_start_5
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v32

    const-string v33, "apkname"

    const/16 v34, 0x0

    invoke-interface/range {v32 .. v34}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v32

    if-nez v32, :cond_5

    .line 13824
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "/"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    sget-object v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;

    invoke-static/range {v33 .. v33}, Lcom/chelpus/Utils;->getApkLabelName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    const-string v34, " "

    const-string v35, "."

    invoke-virtual/range {v33 .. v35}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "."

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "Permissions.Removed"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, ".apk"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 13825
    new-instance v32, Ljava/io/File;

    move-object/from16 v0, v32

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    invoke-virtual {v15, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 13828
    :cond_5
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v32

    const-string v33, "apkname"

    const/16 v34, 0x0

    invoke-interface/range {v32 .. v34}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v32

    if-eqz v32, :cond_6

    .line 13829
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "/"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v21

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "."

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "Permissions.Removed"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, ".apk"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 13830
    new-instance v32, Ljava/io/File;

    move-object/from16 v0, v32

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    invoke-virtual {v15, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 13832
    :cond_6
    sput-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 13865
    .end local v16           #folderstorage:Ljava/io/File;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v32, v0

    new-instance v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114$6;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;)V

    invoke-virtual/range {v32 .. v33}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_3

    .line 13767
    .end local v15           #finalapk:Ljava/io/File;
    .end local v24           #resultfile:Ljava/lang/String;
    .end local v27           #signer:Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;
    :catch_3
    move-exception v9

    .line 13769
    .local v9, e:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    .line 13771
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v32, v0

    new-instance v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114$2;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;)V

    invoke-virtual/range {v32 .. v33}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_3

    .line 13780
    .end local v9           #e:Ljava/io/IOException;
    :catch_4
    move-exception v9

    .line 13781
    .local v9, e:Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 13783
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v32, v0

    new-instance v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114$3;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;)V

    invoke-virtual/range {v32 .. v33}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_3

    .line 13833
    .end local v9           #e:Ljava/lang/Exception;
    .restart local v15       #finalapk:Ljava/io/File;
    .restart local v16       #folderstorage:Ljava/io/File;
    .restart local v24       #resultfile:Ljava/lang/String;
    .restart local v27       #signer:Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;
    :catch_5
    move-exception v9

    .line 13835
    .local v9, e:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    .line 13837
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v32, v0

    new-instance v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114$4;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;)V

    invoke-virtual/range {v32 .. v33}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_3

    .line 13853
    .end local v9           #e:Ljava/io/IOException;
    .end local v12           #ex:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;
    .end local v13           #extrFile:Ljava/lang/String;
    .end local v15           #finalapk:Ljava/io/File;
    .end local v16           #folderstorage:Ljava/io/File;
    .end local v19           #manifestFile:Ljava/io/File;
    .end local v24           #resultfile:Ljava/lang/String;
    .end local v27           #signer:Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v32, v0

    new-instance v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114$5;

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$114;)V

    invoke-virtual/range {v32 .. v33}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_3

    .line 13812
    .restart local v12       #ex:Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;
    .restart local v13       #extrFile:Ljava/lang/String;
    .restart local v19       #manifestFile:Ljava/io/File;
    .restart local v27       #signer:Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;
    :catch_6
    move-exception v32

    goto/16 :goto_4
.end method
