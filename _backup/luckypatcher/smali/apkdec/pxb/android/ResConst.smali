.class public interface abstract Lpxb/android/ResConst;
.super Ljava/lang/Object;
.source "ResConst.java"


# static fields
.field public static final RES_STRING_POOL_TYPE:I = 0x1

.field public static final RES_TABLE_PACKAGE_TYPE:I = 0x200

.field public static final RES_TABLE_TYPE:I = 0x2

.field public static final RES_TABLE_TYPE_SPEC_TYPE:I = 0x202

.field public static final RES_TABLE_TYPE_TYPE:I = 0x201

.field public static final RES_XML_CDATA_TYPE:I = 0x104

.field public static final RES_XML_END_ELEMENT_TYPE:I = 0x103

.field public static final RES_XML_END_NAMESPACE_TYPE:I = 0x101

.field public static final RES_XML_RESOURCE_MAP_TYPE:I = 0x180

.field public static final RES_XML_START_ELEMENT_TYPE:I = 0x102

.field public static final RES_XML_START_NAMESPACE_TYPE:I = 0x100

.field public static final RES_XML_TYPE:I = 0x3
