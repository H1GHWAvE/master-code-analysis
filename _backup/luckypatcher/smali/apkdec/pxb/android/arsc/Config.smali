.class public Lpxb/android/arsc/Config;
.super Ljava/lang/Object;
.source "Config.java"


# instance fields
.field public final entryCount:I

.field public final id:[B

.field public resources:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lpxb/android/arsc/ResEntry;",
            ">;"
        }
    .end annotation
.end field

.field wChunkSize:I

.field wEntryStart:I

.field wPosition:I


# direct methods
.method public constructor <init>([BI)V
    .locals 1
    .parameter "id"
    .parameter "entryCount"

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lpxb/android/arsc/Config;->resources:Ljava/util/Map;

    .line 31
    iput-object p1, p0, Lpxb/android/arsc/Config;->id:[B

    .line 32
    iput p2, p0, Lpxb/android/arsc/Config;->entryCount:I

    .line 33
    return-void
.end method
