.class public Lpxb/android/arsc/ArscDumper;
.super Ljava/lang/Object;
.source "ArscDumper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dump(Ljava/util/List;)V
    .locals 17
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lpxb/android/arsc/Pkg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, pkgs:Ljava/util/List;,"Ljava/util/List<Lpxb/android/arsc/Pkg;>;"
    const/4 v11, 0x0

    .local v11, x:I
    :goto_0
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v12

    if-ge v11, v12, :cond_4

    .line 34
    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lpxb/android/arsc/Pkg;

    .line 36
    .local v7, pkg:Lpxb/android/arsc/Pkg;
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v13, "  Package %d id=%d name=%s typeCount=%d"

    const/4 v14, 0x4

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    iget v0, v7, Lpxb/android/arsc/Pkg;->id:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    iget-object v0, v7, Lpxb/android/arsc/Pkg;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    const/4 v15, 0x3

    iget-object v0, v7, Lpxb/android/arsc/Pkg;->types:Ljava/util/TreeMap;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/TreeMap;->size()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 38
    iget-object v12, v7, Lpxb/android/arsc/Pkg;->types:Ljava/util/TreeMap;

    invoke-virtual {v12}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lpxb/android/arsc/Type;

    .line 39
    .local v10, type:Lpxb/android/arsc/Type;
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v13, "    type %d %s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    iget v0, v10, Lpxb/android/arsc/Type;->id:I

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    iget-object v0, v10, Lpxb/android/arsc/Type;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 41
    iget v12, v7, Lpxb/android/arsc/Pkg;->id:I

    shl-int/lit8 v12, v12, 0x18

    iget v13, v10, Lpxb/android/arsc/Type;->id:I

    shl-int/lit8 v13, v13, 0x10

    or-int v8, v12, v13

    .line 42
    .local v8, resPrefix:I
    const/4 v4, 0x0

    .local v4, i:I
    :goto_1
    iget-object v12, v10, Lpxb/android/arsc/Type;->specs:[Lpxb/android/arsc/ResSpec;

    array-length v12, v12

    if-ge v4, v12, :cond_1

    .line 43
    invoke-virtual {v10, v4}, Lpxb/android/arsc/Type;->getSpec(I)Lpxb/android/arsc/ResSpec;

    move-result-object v9

    .line 44
    .local v9, spec:Lpxb/android/arsc/ResSpec;
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v13, "      spec 0x%08x 0x%08x %s"

    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    iget v0, v9, Lpxb/android/arsc/ResSpec;->id:I

    move/from16 v16, v0

    or-int v16, v16, v8

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    iget v0, v9, Lpxb/android/arsc/ResSpec;->flags:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    iget-object v0, v9, Lpxb/android/arsc/ResSpec;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 42
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 47
    .end local v9           #spec:Lpxb/android/arsc/ResSpec;
    :cond_1
    const/4 v4, 0x0

    :goto_2
    iget-object v12, v10, Lpxb/android/arsc/Type;->configs:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-ge v4, v12, :cond_0

    .line 48
    iget-object v12, v10, Lpxb/android/arsc/Type;->configs:Ljava/util/List;

    invoke-interface {v12, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpxb/android/arsc/Config;

    .line 49
    .local v1, config:Lpxb/android/arsc/Config;
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v13, "      config"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 51
    new-instance v2, Ljava/util/ArrayList;

    iget-object v12, v1, Lpxb/android/arsc/Config;->resources:Ljava/util/Map;

    invoke-interface {v12}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v12

    invoke-direct {v2, v12}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 52
    .local v2, entries:Ljava/util/List;,"Ljava/util/List<Lpxb/android/arsc/ResEntry;>;"
    const/4 v6, 0x0

    .local v6, j:I
    :goto_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v12

    if-ge v6, v12, :cond_2

    .line 53
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lpxb/android/arsc/ResEntry;

    .line 54
    .local v3, entry:Lpxb/android/arsc/ResEntry;
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v13, "        resource 0x%08x %-20s: %s"

    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    iget-object v0, v3, Lpxb/android/arsc/ResEntry;->spec:Lpxb/android/arsc/ResSpec;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lpxb/android/arsc/ResSpec;->id:I

    move/from16 v16, v0

    or-int v16, v16, v8

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    iget-object v0, v3, Lpxb/android/arsc/ResEntry;->spec:Lpxb/android/arsc/ResSpec;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lpxb/android/arsc/ResSpec;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    const/4 v15, 0x2

    iget-object v0, v3, Lpxb/android/arsc/ResEntry;->value:Ljava/lang/Object;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 52
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 47
    .end local v3           #entry:Lpxb/android/arsc/ResEntry;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 33
    .end local v1           #config:Lpxb/android/arsc/Config;
    .end local v2           #entries:Ljava/util/List;,"Ljava/util/List<Lpxb/android/arsc/ResEntry;>;"
    .end local v4           #i:I
    .end local v6           #j:I
    .end local v8           #resPrefix:I
    .end local v10           #type:Lpxb/android/arsc/Type;
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 60
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v7           #pkg:Lpxb/android/arsc/Pkg;
    :cond_4
    return-void
.end method

.method public static varargs main([Ljava/lang/String;)V
    .locals 4
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    array-length v2, p0

    if-nez v2, :cond_0

    .line 64
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v3, "asrc-dump file.arsc"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 72
    :goto_0
    return-void

    .line 67
    :cond_0
    new-instance v2, Ljava/io/File;

    const/4 v3, 0x0

    aget-object v3, p0, v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lpxb/android/axml/Util;->readFile(Ljava/io/File;)[B

    move-result-object v0

    .line 68
    .local v0, data:[B
    new-instance v2, Lpxb/android/arsc/ArscParser;

    invoke-direct {v2, v0}, Lpxb/android/arsc/ArscParser;-><init>([B)V

    invoke-virtual {v2}, Lpxb/android/arsc/ArscParser;->parse()Ljava/util/List;

    move-result-object v1

    .line 70
    .local v1, pkgs:Ljava/util/List;,"Ljava/util/List<Lpxb/android/arsc/Pkg;>;"
    invoke-static {v1}, Lpxb/android/arsc/ArscDumper;->dump(Ljava/util/List;)V

    goto :goto_0
.end method
