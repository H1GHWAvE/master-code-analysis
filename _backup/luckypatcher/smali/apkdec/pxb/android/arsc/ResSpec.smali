.class public Lpxb/android/arsc/ResSpec;
.super Ljava/lang/Object;
.source "ResSpec.java"


# instance fields
.field public flags:I

.field public final id:I

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .parameter "id"

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lpxb/android/arsc/ResSpec;->id:I

    .line 26
    return-void
.end method


# virtual methods
.method public updateName(Ljava/lang/String;)V
    .locals 2
    .parameter "s"

    .prologue
    .line 29
    iget-object v0, p0, Lpxb/android/arsc/ResSpec;->name:Ljava/lang/String;

    .line 30
    .local v0, name:Ljava/lang/String;
    if-nez v0, :cond_1

    .line 31
    iput-object p1, p0, Lpxb/android/arsc/ResSpec;->name:Ljava/lang/String;

    .line 35
    :cond_0
    return-void

    .line 32
    :cond_1
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 33
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    throw v1
.end method
