.class public Lpxb/android/arsc/Value;
.super Ljava/lang/Object;
.source "Value.java"


# instance fields
.field public final data:I

.field public raw:Ljava/lang/String;

.field public final type:I


# direct methods
.method public constructor <init>(IILjava/lang/String;)V
    .locals 0
    .parameter "type"
    .parameter "data"
    .parameter "raw"

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lpxb/android/arsc/Value;->type:I

    .line 26
    iput p2, p0, Lpxb/android/arsc/Value;->data:I

    .line 27
    iput-object p3, p0, Lpxb/android/arsc/Value;->raw:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 31
    iget v0, p0, Lpxb/android/arsc/Value;->type:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 32
    iget-object v0, p0, Lpxb/android/arsc/Value;->raw:Ljava/lang/String;

    .line 34
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "{t=0x%02x d=0x%08x}"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lpxb/android/arsc/Value;->type:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lpxb/android/arsc/Value;->data:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
