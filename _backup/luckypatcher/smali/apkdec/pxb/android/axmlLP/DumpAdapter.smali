.class public Lpxb/android/axmlLP/DumpAdapter;
.super Lpxb/android/axmlLP/AxmlVisitor;
.source "DumpAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;
    }
.end annotation


# instance fields
.field private nses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0}, Lpxb/android/axmlLP/AxmlVisitor;-><init>()V

    .line 111
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/DumpAdapter;->nses:Ljava/util/Map;

    .line 114
    return-void
.end method

.method public constructor <init>(Lpxb/android/axmlLP/AxmlVisitor;)V
    .locals 1
    .parameter "av"

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lpxb/android/axmlLP/AxmlVisitor;-><init>(Lpxb/android/axmlLP/AxmlVisitor;)V

    .line 111
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/DumpAdapter;->nses:Ljava/util/Map;

    .line 118
    return-void
.end method


# virtual methods
.method public end()V
    .locals 0

    .prologue
    .line 122
    invoke-super {p0}, Lpxb/android/axmlLP/AxmlVisitor;->end()V

    .line 123
    return-void
.end method

.method public first(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    .locals 5
    .parameter "ns"
    .parameter "name"

    .prologue
    .line 127
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "<"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 128
    if-eqz p1, :cond_0

    .line 129
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lpxb/android/axmlLP/DumpAdapter;->nses:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 131
    :cond_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, p2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 132
    invoke-super {p0, p1, p2}, Lpxb/android/axmlLP/AxmlVisitor;->first(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    move-result-object v0

    .line 133
    .local v0, nv:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    if-eqz v0, :cond_1

    .line 134
    new-instance v1, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;

    const/4 v2, 0x1

    iget-object v3, p0, Lpxb/android/axmlLP/DumpAdapter;->nses:Ljava/util/Map;

    invoke-direct {v1, v0, v2, v3}, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;-><init>(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;ILjava/util/Map;)V

    .line 137
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public ns(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .parameter "prefix"
    .parameter "uri"
    .parameter "ln"

    .prologue
    .line 142
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lpxb/android/axmlLP/DumpAdapter;->nses:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    invoke-super {p0, p1, p2, p3}, Lpxb/android/axmlLP/AxmlVisitor;->ns(Ljava/lang/String;Ljava/lang/String;I)V

    .line 145
    return-void
.end method
