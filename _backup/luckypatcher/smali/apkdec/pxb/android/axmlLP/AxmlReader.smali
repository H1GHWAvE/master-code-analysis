.class public Lpxb/android/axmlLP/AxmlReader;
.super Ljava/lang/Object;
.source "AxmlReader.java"


# static fields
.field static final CHUNK_AXML_FILE:I = 0x80003

.field static final CHUNK_RESOURCEIDS:I = 0x80180

.field static final CHUNK_STRINGS:I = 0x1c0001

.field static final CHUNK_XML_END_NAMESPACE:I = 0x100101

.field static final CHUNK_XML_END_TAG:I = 0x100103

.field static final CHUNK_XML_START_NAMESPACE:I = 0x100100

.field static final CHUNK_XML_START_TAG:I = 0x100102

.field static final CHUNK_XML_TEXT:I = 0x100104

.field public static final EMPTY_VISITOR:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor; = null

.field static final UTF8_FLAG:I = 0x100


# instance fields
.field private in:Lcom/googlecode/dex2jar/reader/io/DataIn;

.field private resourceIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private stringItems:Lpxb/android/axmlLP/StringItems;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lpxb/android/axmlLP/AxmlReader$1;

    invoke-direct {v0}, Lpxb/android/axmlLP/AxmlReader$1;-><init>()V

    sput-object v0, Lpxb/android/axmlLP/AxmlReader;->EMPTY_VISITOR:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    return-void
.end method

.method public constructor <init>(Lcom/googlecode/dex2jar/reader/io/DataIn;)V
    .locals 1
    .parameter "in"

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlReader;->resourceIds:Ljava/util/List;

    .line 56
    new-instance v0, Lpxb/android/axmlLP/StringItems;

    invoke-direct {v0}, Lpxb/android/axmlLP/StringItems;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlReader;->stringItems:Lpxb/android/axmlLP/StringItems;

    .line 64
    iput-object p1, p0, Lpxb/android/axmlLP/AxmlReader;->in:Lcom/googlecode/dex2jar/reader/io/DataIn;

    .line 65
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .parameter "data"

    .prologue
    .line 59
    invoke-static {p1}, Lcom/googlecode/dex2jar/reader/io/ArrayDataIn;->le([B)Lcom/googlecode/dex2jar/reader/io/ArrayDataIn;

    move-result-object v0

    invoke-direct {p0, v0}, Lpxb/android/axmlLP/AxmlReader;-><init>(Lcom/googlecode/dex2jar/reader/io/DataIn;)V

    .line 60
    return-void
.end method


# virtual methods
.method public accept(Lpxb/android/axmlLP/AxmlVisitor;)V
    .locals 28
    .parameter "documentVisitor"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/axmlLP/AxmlReader;->in:Lcom/googlecode/dex2jar/reader/io/DataIn;

    move-object/from16 v16, v0

    .line 71
    .local v16, in:Lcom/googlecode/dex2jar/reader/io/DataIn;
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v25

    .line 72
    .local v25, type:I
    const v26, 0x80003

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_0

    .line 73
    new-instance v26, Ljava/lang/RuntimeException;

    invoke-direct/range {v26 .. v26}, Ljava/lang/RuntimeException;-><init>()V

    throw v26

    .line 75
    :cond_0
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v13

    .line 77
    .local v13, fileSize:I
    if-nez p1, :cond_1

    sget-object v23, Lpxb/android/axmlLP/AxmlReader;->EMPTY_VISITOR:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    .line 84
    .local v23, root:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    :goto_0
    move-object/from16 v4, v23

    .line 85
    .local v4, tos:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    new-instance v20, Ljava/util/Stack;

    invoke-direct/range {v20 .. v20}, Ljava/util/Stack;-><init>()V

    .line 86
    .local v20, nvs:Ljava/util/Stack;,"Ljava/util/Stack<Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;>;"
    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->getCurrentPosition()I

    move-result v21

    .local v21, p:I
    :goto_1
    move/from16 v0, v21

    if-ge v0, v13, :cond_c

    .line 93
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v25

    .line 94
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v24

    .line 95
    .local v24, size:I
    sparse-switch v25, :sswitch_data_0

    .line 193
    new-instance v26, Ljava/lang/RuntimeException;

    invoke-direct/range {v26 .. v26}, Ljava/lang/RuntimeException;-><init>()V

    throw v26

    .line 77
    .end local v4           #tos:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    .end local v20           #nvs:Ljava/util/Stack;,"Ljava/util/Stack<Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;>;"
    .end local v21           #p:I
    .end local v23           #root:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    .end local v24           #size:I
    :cond_1
    new-instance v23, Lpxb/android/axmlLP/AxmlReader$2;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lpxb/android/axmlLP/AxmlReader$2;-><init>(Lpxb/android/axmlLP/AxmlReader;Lpxb/android/axmlLP/AxmlVisitor;)V

    goto :goto_0

    .line 98
    .restart local v4       #tos:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    .restart local v20       #nvs:Ljava/util/Stack;,"Ljava/util/Stack<Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;>;"
    .restart local v21       #p:I
    .restart local v23       #root:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    .restart local v24       #size:I
    :sswitch_0
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v17

    .line 99
    .local v17, lineNumber:I
    const/16 v26, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->skip(I)V

    .line 100
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v19

    .line 101
    .local v19, nsIdx:I
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v18

    .line 102
    .local v18, nameIdx:I
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v14

    .line 103
    .local v14, flag:I
    const v26, 0x140014

    move/from16 v0, v26

    if-eq v14, v0, :cond_2

    .line 104
    new-instance v26, Ljava/lang/RuntimeException;

    invoke-direct/range {v26 .. v26}, Ljava/lang/RuntimeException;-><init>()V

    throw v26

    .line 106
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/axmlLP/AxmlReader;->stringItems:Lpxb/android/axmlLP/StringItems;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lpxb/android/axmlLP/StringItems;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lpxb/android/axmlLP/StringItem;

    move-object/from16 v0, v26

    iget-object v6, v0, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    .line 107
    .local v6, name:Ljava/lang/String;
    if-ltz v19, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/axmlLP/AxmlReader;->stringItems:Lpxb/android/axmlLP/StringItems;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lpxb/android/axmlLP/StringItems;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lpxb/android/axmlLP/StringItem;

    move-object/from16 v0, v26

    iget-object v5, v0, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    .line 109
    .local v5, ns:Ljava/lang/String;
    :goto_2
    invoke-virtual {v4, v5, v6}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    move-result-object v4

    .line 110
    if-nez v4, :cond_3

    .line 111
    sget-object v4, Lpxb/android/axmlLP/AxmlReader;->EMPTY_VISITOR:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    .line 113
    :cond_3
    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    move/from16 v0, v17

    invoke-virtual {v4, v0}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->line(I)V

    .line 117
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readUShortx()I

    move-result v11

    .line 121
    .local v11, attributeCount:I
    const/16 v26, 0x6

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->skip(I)V

    .line 122
    sget-object v26, Lpxb/android/axmlLP/AxmlReader;->EMPTY_VISITOR:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    move-object/from16 v0, v26

    if-eq v4, v0, :cond_8

    .line 123
    const/4 v15, 0x0

    .local v15, i:I
    :goto_3
    if-ge v15, v11, :cond_9

    .line 124
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v19

    .line 125
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v18

    .line 126
    const/16 v26, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->skip(I)V

    .line 127
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v26

    ushr-int/lit8 v8, v26, 0x18

    .line 128
    .local v8, aValueType:I
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v10

    .line 129
    .local v10, aValue:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/axmlLP/AxmlReader;->stringItems:Lpxb/android/axmlLP/StringItems;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lpxb/android/axmlLP/StringItems;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lpxb/android/axmlLP/StringItem;

    move-object/from16 v0, v26

    iget-object v6, v0, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    .line 130
    if-ltz v19, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/axmlLP/AxmlReader;->stringItems:Lpxb/android/axmlLP/StringItems;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lpxb/android/axmlLP/StringItems;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lpxb/android/axmlLP/StringItem;

    move-object/from16 v0, v26

    iget-object v5, v0, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    .line 131
    :goto_4
    const/4 v9, 0x0

    .line 132
    .local v9, value:Ljava/lang/Object;
    sparse-switch v8, :sswitch_data_1

    .line 140
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 142
    .end local v9           #value:Ljava/lang/Object;
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/axmlLP/AxmlReader;->resourceIds:Ljava/util/List;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v26

    move/from16 v0, v18

    move/from16 v1, v26

    if-ge v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/axmlLP/AxmlReader;->resourceIds:Ljava/util/List;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 143
    .local v7, resourceId:I
    :goto_6
    invoke-virtual/range {v4 .. v9}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V

    .line 123
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 107
    .end local v5           #ns:Ljava/lang/String;
    .end local v7           #resourceId:I
    .end local v8           #aValueType:I
    .end local v10           #aValue:I
    .end local v11           #attributeCount:I
    .end local v15           #i:I
    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 130
    .restart local v5       #ns:Ljava/lang/String;
    .restart local v8       #aValueType:I
    .restart local v10       #aValue:I
    .restart local v11       #attributeCount:I
    .restart local v15       #i:I
    :cond_5
    const/4 v5, 0x0

    goto :goto_4

    .line 134
    .restart local v9       #value:Ljava/lang/Object;
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/axmlLP/AxmlReader;->stringItems:Lpxb/android/axmlLP/StringItems;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Lpxb/android/axmlLP/StringItems;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lpxb/android/axmlLP/StringItem;

    move-object/from16 v0, v26

    iget-object v9, v0, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    .line 135
    .local v9, value:Ljava/lang/String;
    goto :goto_5

    .line 137
    .local v9, value:Ljava/lang/Object;
    :sswitch_2
    if-eqz v10, :cond_6

    const/16 v26, 0x1

    :goto_7
    invoke-static/range {v26 .. v26}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 138
    .local v9, value:Ljava/lang/Boolean;
    goto :goto_5

    .line 137
    .local v9, value:Ljava/lang/Object;
    :cond_6
    const/16 v26, 0x0

    goto :goto_7

    .line 142
    .end local v9           #value:Ljava/lang/Object;
    :cond_7
    const/4 v7, -0x1

    goto :goto_6

    .line 146
    .end local v8           #aValueType:I
    .end local v10           #aValue:I
    .end local v15           #i:I
    :cond_8
    const/16 v26, 0x14

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->skip(I)V

    .line 195
    .end local v5           #ns:Ljava/lang/String;
    .end local v6           #name:Ljava/lang/String;
    .end local v11           #attributeCount:I
    .end local v14           #flag:I
    .end local v17           #lineNumber:I
    .end local v18           #nameIdx:I
    .end local v19           #nsIdx:I
    :cond_9
    :goto_8
    add-int v26, v21, v24

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->move(I)V

    .line 92
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->getCurrentPosition()I

    move-result v21

    goto/16 :goto_1

    .line 151
    :sswitch_3
    add-int/lit8 v26, v24, -0x8

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->skip(I)V

    .line 152
    invoke-virtual {v4}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->end()V

    .line 153
    invoke-virtual/range {v20 .. v20}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 154
    invoke-virtual/range {v20 .. v20}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v4

    .end local v4           #tos:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    check-cast v4, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    .line 156
    .restart local v4       #tos:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    goto :goto_8

    .line 158
    :sswitch_4
    if-nez p1, :cond_a

    .line 159
    const/16 v26, 0x10

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->skip(I)V

    goto :goto_8

    .line 161
    :cond_a
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v17

    .line 162
    .restart local v17       #lineNumber:I
    const/16 v26, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->skip(I)V

    .line 163
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v22

    .line 164
    .local v22, prefixIdx:I
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v19

    .line 165
    .restart local v19       #nsIdx:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/axmlLP/AxmlReader;->stringItems:Lpxb/android/axmlLP/StringItems;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lpxb/android/axmlLP/StringItems;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lpxb/android/axmlLP/StringItem;

    move-object/from16 v0, v26

    iget-object v0, v0, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/axmlLP/AxmlReader;->stringItems:Lpxb/android/axmlLP/StringItems;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lpxb/android/axmlLP/StringItems;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lpxb/android/axmlLP/StringItem;

    move-object/from16 v0, v26

    iget-object v0, v0, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    move-object/from16 v2, v26

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lpxb/android/axmlLP/AxmlVisitor;->ns(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_8

    .line 169
    .end local v17           #lineNumber:I
    .end local v19           #nsIdx:I
    .end local v22           #prefixIdx:I
    :sswitch_5
    add-int/lit8 v26, v24, -0x8

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->skip(I)V

    goto/16 :goto_8

    .line 172
    :sswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/axmlLP/AxmlReader;->stringItems:Lpxb/android/axmlLP/StringItems;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lpxb/android/axmlLP/StringItems;->read(Lcom/googlecode/dex2jar/reader/io/DataIn;I)V

    goto/16 :goto_8

    .line 175
    :sswitch_7
    div-int/lit8 v26, v24, 0x4

    add-int/lit8 v12, v26, -0x2

    .line 176
    .local v12, count:I
    const/4 v15, 0x0

    .restart local v15       #i:I
    :goto_9
    if-ge v15, v12, :cond_9

    .line 177
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/axmlLP/AxmlReader;->resourceIds:Ljava/util/List;

    move-object/from16 v26, v0

    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    invoke-interface/range {v26 .. v27}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    add-int/lit8 v15, v15, 0x1

    goto :goto_9

    .line 181
    .end local v12           #count:I
    .end local v15           #i:I
    :sswitch_8
    sget-object v26, Lpxb/android/axmlLP/AxmlReader;->EMPTY_VISITOR:Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    move-object/from16 v0, v26

    if-ne v4, v0, :cond_b

    .line 182
    const/16 v26, 0x14

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->skip(I)V

    goto/16 :goto_8

    .line 184
    :cond_b
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v17

    .line 185
    .restart local v17       #lineNumber:I
    const/16 v26, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->skip(I)V

    .line 186
    invoke-interface/range {v16 .. v16}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v18

    .line 187
    .restart local v18       #nameIdx:I
    const/16 v26, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-interface {v0, v1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->skip(I)V

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/axmlLP/AxmlReader;->stringItems:Lpxb/android/axmlLP/StringItems;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lpxb/android/axmlLP/StringItems;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lpxb/android/axmlLP/StringItem;

    move-object/from16 v0, v26

    iget-object v6, v0, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    .line 189
    .restart local v6       #name:Ljava/lang/String;
    move/from16 v0, v17

    invoke-virtual {v4, v0, v6}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->text(ILjava/lang/String;)V

    goto/16 :goto_8

    .line 197
    .end local v6           #name:Ljava/lang/String;
    .end local v17           #lineNumber:I
    .end local v18           #nameIdx:I
    .end local v24           #size:I
    :cond_c
    return-void

    .line 95
    :sswitch_data_0
    .sparse-switch
        0x80180 -> :sswitch_7
        0x100100 -> :sswitch_4
        0x100101 -> :sswitch_5
        0x100102 -> :sswitch_0
        0x100103 -> :sswitch_3
        0x100104 -> :sswitch_8
        0x1c0001 -> :sswitch_6
    .end sparse-switch

    .line 132
    :sswitch_data_1
    .sparse-switch
        0x3 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method
