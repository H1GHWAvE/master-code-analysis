.class Lpxb/android/axmlLP/StringItems;
.super Ljava/util/ArrayList;
.source "StringItems.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Lpxb/android/axmlLP/StringItem;",
        ">;"
    }
.end annotation


# instance fields
.field stringData:[B


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-void
.end method


# virtual methods
.method public getSize()I
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lpxb/android/axmlLP/StringItems;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x14

    iget-object v1, p0, Lpxb/android/axmlLP/StringItems;->stringData:[B

    array-length v1, v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    return v0
.end method

.method public prepare()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 38
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 39
    .local v0, baos:Ljava/io/ByteArrayOutputStream;
    const/4 v2, 0x0

    .line 40
    .local v2, i:I
    const/4 v8, 0x0

    .line 41
    .local v8, offset:I
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 42
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 43
    .local v6, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lpxb/android/axmlLP/StringItems;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lpxb/android/axmlLP/StringItem;

    .line 44
    .local v4, item:Lpxb/android/axmlLP/StringItem;
    add-int/lit8 v3, v2, 0x1

    .end local v2           #i:I
    .local v3, i:I
    iput v2, v4, Lpxb/android/axmlLP/StringItem;->index:I

    .line 45
    iget-object v9, v4, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    .line 46
    .local v9, stringData:Ljava/lang/String;
    invoke-interface {v6, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 47
    .local v7, of:Ljava/lang/Integer;
    if-eqz v7, :cond_0

    .line 48
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v11

    iput v11, v4, Lpxb/android/axmlLP/StringItem;->dataOffset:I

    :goto_1
    move v2, v3

    .line 61
    .end local v3           #i:I
    .restart local v2       #i:I
    goto :goto_0

    .line 50
    .end local v2           #i:I
    .restart local v3       #i:I
    :cond_0
    iput v8, v4, Lpxb/android/axmlLP/StringItem;->dataOffset:I

    .line 51
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v6, v9, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v5

    .line 53
    .local v5, length:I
    const-string v11, "UTF-16LE"

    invoke-virtual {v9, v11}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    .line 54
    .local v1, data:[B
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 55
    shr-int/lit8 v11, v5, 0x8

    invoke-virtual {v0, v11}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 56
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 57
    invoke-virtual {v0, v12}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 58
    invoke-virtual {v0, v12}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 59
    array-length v11, v1

    add-int/lit8 v11, v11, 0x4

    add-int/2addr v8, v11

    goto :goto_1

    .line 63
    .end local v1           #data:[B
    .end local v3           #i:I
    .end local v4           #item:Lpxb/android/axmlLP/StringItem;
    .end local v5           #length:I
    .end local v7           #of:Ljava/lang/Integer;
    .end local v9           #stringData:Ljava/lang/String;
    .restart local v2       #i:I
    :cond_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    iput-object v10, p0, Lpxb/android/axmlLP/StringItems;->stringData:[B

    .line 64
    return-void
.end method

.method public read(Lcom/googlecode/dex2jar/reader/io/DataIn;I)V
    .locals 22
    .parameter "in"
    .parameter "size"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->getCurrentPosition()I

    move-result v20

    add-int/lit8 v18, v20, -0x4

    .line 68
    .local v18, trunkOffset:I
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v12

    .line 69
    .local v12, stringCount:I
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v16

    .line 70
    .local v16, styleOffsetCount:I
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v6

    .line 71
    .local v6, flags:I
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v13

    .line 72
    .local v13, stringDataOffset:I
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v17

    .line 73
    .local v17, stylesOffset:I
    const/4 v7, 0x0

    .local v7, i:I
    :goto_0
    if-ge v7, v12, :cond_0

    .line 74
    new-instance v14, Lpxb/android/axmlLP/StringItem;

    invoke-direct {v14}, Lpxb/android/axmlLP/StringItem;-><init>()V

    .line 75
    .local v14, stringItem:Lpxb/android/axmlLP/StringItem;
    iput v7, v14, Lpxb/android/axmlLP/StringItem;->index:I

    .line 76
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readIntx()I

    move-result v20

    move/from16 v0, v20

    iput v0, v14, Lpxb/android/axmlLP/StringItem;->dataOffset:I

    .line 77
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lpxb/android/axmlLP/StringItems;->add(Ljava/lang/Object;)Z

    .line 73
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 79
    .end local v14           #stringItem:Lpxb/android/axmlLP/StringItem;
    :cond_0
    new-instance v15, Ljava/util/TreeMap;

    invoke-direct {v15}, Ljava/util/TreeMap;-><init>()V

    .line 80
    .local v15, stringMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;"
    if-eqz v16, :cond_1

    .line 81
    new-instance v20, Ljava/lang/RuntimeException;

    invoke-direct/range {v20 .. v20}, Ljava/lang/RuntimeException;-><init>()V

    throw v20

    .line 88
    :cond_1
    if-nez v17, :cond_2

    move/from16 v5, p2

    .line 89
    .local v5, endOfStringData:I
    :goto_1
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->getCurrentPosition()I

    move-result v2

    .line 90
    .local v2, base:I
    and-int/lit16 v0, v6, 0x100

    move/from16 v20, v0

    if-eqz v20, :cond_4

    .line 91
    move v10, v2

    .local v10, p:I
    :goto_2
    if-ge v10, v5, :cond_5

    .line 92
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readLeb128()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-int v9, v0

    .line 93
    .local v9, length:I
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    add-int/lit8 v20, v9, 0xa

    move/from16 v0, v20

    invoke-direct {v3, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 94
    .local v3, bos:Ljava/io/ByteArrayOutputStream;
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readByte()I

    move-result v11

    .local v11, r:I
    :goto_3
    if-eqz v11, :cond_3

    .line 95
    invoke-virtual {v3, v11}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 94
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readByte()I

    move-result v11

    goto :goto_3

    .end local v2           #base:I
    .end local v3           #bos:Ljava/io/ByteArrayOutputStream;
    .end local v5           #endOfStringData:I
    .end local v9           #length:I
    .end local v10           #p:I
    .end local v11           #r:I
    :cond_2
    move/from16 v5, v17

    .line 88
    goto :goto_1

    .line 97
    .restart local v2       #base:I
    .restart local v3       #bos:Ljava/io/ByteArrayOutputStream;
    .restart local v5       #endOfStringData:I
    .restart local v9       #length:I
    .restart local v10       #p:I
    .restart local v11       #r:I
    :cond_3
    new-instance v19, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v20

    const-string v21, "UTF-8"

    invoke-direct/range {v19 .. v21}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 98
    .local v19, value:Ljava/lang/String;
    sub-int v20, v10, v2

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-interface {v15, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->getCurrentPosition()I

    move-result v10

    goto :goto_2

    .line 101
    .end local v3           #bos:Ljava/io/ByteArrayOutputStream;
    .end local v9           #length:I
    .end local v10           #p:I
    .end local v11           #r:I
    .end local v19           #value:Ljava/lang/String;
    :cond_4
    move v10, v2

    .restart local v10       #p:I
    :goto_4
    if-ge v10, v5, :cond_5

    .line 102
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readShortx()I

    move-result v9

    .line 103
    .restart local v9       #length:I
    mul-int/lit8 v20, v9, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->readBytes(I)[B

    move-result-object v4

    .line 104
    .local v4, data:[B
    const/16 v20, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->skip(I)V

    .line 105
    new-instance v19, Ljava/lang/String;

    const-string v20, "UTF-16LE"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v4, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 106
    .restart local v19       #value:Ljava/lang/String;
    sub-int v20, v10, v2

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-interface {v15, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/dex2jar/reader/io/DataIn;->getCurrentPosition()I

    move-result v10

    goto :goto_4

    .line 110
    .end local v4           #data:[B
    .end local v9           #length:I
    .end local v19           #value:Ljava/lang/String;
    :cond_5
    if-eqz v17, :cond_6

    .line 113
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lpxb/android/axmlLP/StringItems;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_5
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_7

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lpxb/android/axmlLP/StringItem;

    .line 114
    .local v8, item:Lpxb/android/axmlLP/StringItem;
    iget v0, v8, Lpxb/android/axmlLP/StringItem;->dataOffset:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v15, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    move-object/from16 v0, v20

    iput-object v0, v8, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    goto :goto_5

    .line 117
    .end local v8           #item:Lpxb/android/axmlLP/StringItem;
    :cond_7
    return-void
.end method

.method public write(Lcom/googlecode/dex2jar/reader/io/DataOut;)V
    .locals 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 120
    invoke-virtual {p0}, Lpxb/android/axmlLP/StringItems;->size()I

    move-result v1

    invoke-interface {p1, v1}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 121
    invoke-interface {p1, v2}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 122
    invoke-interface {p1, v2}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 123
    invoke-virtual {p0}, Lpxb/android/axmlLP/StringItems;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x1c

    invoke-interface {p1, v1}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 124
    invoke-interface {p1, v2}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 125
    invoke-virtual {p0}, Lpxb/android/axmlLP/StringItems;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpxb/android/axmlLP/StringItem;

    .line 126
    .local v0, item:Lpxb/android/axmlLP/StringItem;
    iget v2, v0, Lpxb/android/axmlLP/StringItem;->dataOffset:I

    invoke-interface {p1, v2}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    goto :goto_0

    .line 128
    .end local v0           #item:Lpxb/android/axmlLP/StringItem;
    :cond_0
    iget-object v1, p0, Lpxb/android/axmlLP/StringItems;->stringData:[B

    invoke-interface {p1, v1}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeBytes([B)V

    .line 130
    return-void
.end method
