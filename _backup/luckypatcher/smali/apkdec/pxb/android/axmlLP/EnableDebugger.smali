.class public Lpxb/android/axmlLP/EnableDebugger;
.super Ljava/lang/Object;
.source "EnableDebugger.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs main([Ljava/lang/String;)V
    .locals 4
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 16
    array-length v0, p0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 17
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "test5 in out"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 21
    :goto_0
    return-void

    .line 20
    :cond_0
    new-instance v0, Lpxb/android/axmlLP/EnableDebugger;

    invoke-direct {v0}, Lpxb/android/axmlLP/EnableDebugger;-><init>()V

    new-instance v1, Ljava/io/File;

    const/4 v2, 0x0

    aget-object v2, p0, v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    const/4 v3, 0x1

    aget-object v3, p0, v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lpxb/android/axmlLP/EnableDebugger;->a(Ljava/io/File;Ljava/io/File;)V

    goto :goto_0
.end method


# virtual methods
.method a(Ljava/io/File;Ljava/io/File;)V
    .locals 7
    .parameter "a"
    .parameter "b"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 24
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 25
    .local v1, is:Ljava/io/InputStream;
    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v6

    new-array v5, v6, [B

    .line 26
    .local v5, xml:[B
    invoke-virtual {v1, v5}, Ljava/io/InputStream;->read([B)I

    .line 27
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 29
    new-instance v3, Lpxb/android/axmlLP/AxmlReader;

    invoke-direct {v3, v5}, Lpxb/android/axmlLP/AxmlReader;-><init>([B)V

    .line 30
    .local v3, rd:Lpxb/android/axmlLP/AxmlReader;
    new-instance v4, Lpxb/android/axmlLP/AxmlWriter;

    invoke-direct {v4}, Lpxb/android/axmlLP/AxmlWriter;-><init>()V

    .line 31
    .local v4, wr:Lpxb/android/axmlLP/AxmlWriter;
    new-instance v6, Lpxb/android/axmlLP/EnableDebugger$1;

    invoke-direct {v6, p0, v4}, Lpxb/android/axmlLP/EnableDebugger$1;-><init>(Lpxb/android/axmlLP/EnableDebugger;Lpxb/android/axmlLP/AxmlVisitor;)V

    invoke-virtual {v3, v6}, Lpxb/android/axmlLP/AxmlReader;->accept(Lpxb/android/axmlLP/AxmlVisitor;)V

    .line 63
    invoke-virtual {v4}, Lpxb/android/axmlLP/AxmlWriter;->toByteArray()[B

    move-result-object v2

    .line 64
    .local v2, modified:[B
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 65
    .local v0, fos:Ljava/io/FileOutputStream;
    invoke-virtual {v0, v2}, Ljava/io/FileOutputStream;->write([B)V

    .line 66
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 67
    return-void
.end method
