.class public Lpxb/android/axml/AxmlWriter;
.super Lpxb/android/axml/AxmlVisitor;
.source "AxmlWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpxb/android/axml/AxmlWriter$Ns;,
        Lpxb/android/axml/AxmlWriter$NodeImpl;,
        Lpxb/android/axml/AxmlWriter$Attr;
    }
.end annotation


# static fields
.field static final ATTR_CMP:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lpxb/android/axml/AxmlWriter$Attr;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private firsts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/axml/AxmlWriter$NodeImpl;",
            ">;"
        }
    .end annotation
.end field

.field private nses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lpxb/android/axml/AxmlWriter$Ns;",
            ">;"
        }
    .end annotation
.end field

.field private otherString:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/StringItem;",
            ">;"
        }
    .end annotation
.end field

.field private resourceId2Str:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lpxb/android/StringItem;",
            ">;"
        }
    .end annotation
.end field

.field private resourceIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private resourceString:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/StringItem;",
            ">;"
        }
    .end annotation
.end field

.field private stringItems:Lpxb/android/StringItems;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lpxb/android/axml/AxmlWriter$1;

    invoke-direct {v0}, Lpxb/android/axml/AxmlWriter$1;-><init>()V

    sput-object v0, Lpxb/android/axml/AxmlWriter;->ATTR_CMP:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Lpxb/android/axml/AxmlVisitor;-><init>()V

    .line 280
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter;->firsts:Ljava/util/List;

    .line 282
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter;->nses:Ljava/util/Map;

    .line 284
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter;->otherString:Ljava/util/List;

    .line 286
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter;->resourceId2Str:Ljava/util/Map;

    .line 288
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter;->resourceIds:Ljava/util/List;

    .line 290
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter;->resourceString:Ljava/util/List;

    .line 292
    new-instance v0, Lpxb/android/StringItems;

    invoke-direct {v0}, Lpxb/android/StringItems;-><init>()V

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter;->stringItems:Lpxb/android/StringItems;

    return-void
.end method

.method private prepare()I
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 314
    const/4 v6, 0x0

    .line 316
    .local v6, size:I
    iget-object v8, p0, Lpxb/android/axml/AxmlWriter;->firsts:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lpxb/android/axml/AxmlWriter$NodeImpl;

    .line 317
    .local v3, first:Lpxb/android/axml/AxmlWriter$NodeImpl;
    invoke-virtual {v3, p0}, Lpxb/android/axml/AxmlWriter$NodeImpl;->prepare(Lpxb/android/axml/AxmlWriter;)I

    move-result v8

    add-int/2addr v6, v8

    .line 318
    goto :goto_0

    .line 320
    .end local v3           #first:Lpxb/android/axml/AxmlWriter$NodeImpl;
    :cond_0
    const/4 v0, 0x0

    .line 321
    .local v0, a:I
    iget-object v8, p0, Lpxb/android/axml/AxmlWriter;->nses:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 322
    .local v2, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lpxb/android/axml/AxmlWriter$Ns;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lpxb/android/axml/AxmlWriter$Ns;

    .line 323
    .local v5, ns:Lpxb/android/axml/AxmlWriter$Ns;
    if-nez v5, :cond_1

    .line 324
    new-instance v5, Lpxb/android/axml/AxmlWriter$Ns;

    .end local v5           #ns:Lpxb/android/axml/AxmlWriter$Ns;
    new-instance v9, Lpxb/android/StringItem;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v9, v8}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v12, v9, v13}, Lpxb/android/axml/AxmlWriter$Ns;-><init>(Lpxb/android/StringItem;Lpxb/android/StringItem;I)V

    .line 325
    .restart local v5       #ns:Lpxb/android/axml/AxmlWriter$Ns;
    invoke-interface {v2, v5}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    :cond_1
    iget-object v8, v5, Lpxb/android/axml/AxmlWriter$Ns;->prefix:Lpxb/android/StringItem;

    if-nez v8, :cond_2

    .line 328
    new-instance v8, Lpxb/android/StringItem;

    const-string v9, "axml_auto_%02d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    add-int/lit8 v1, v0, 0x1

    .end local v0           #a:I
    .local v1, a:I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v13

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    iput-object v8, v5, Lpxb/android/axml/AxmlWriter$Ns;->prefix:Lpxb/android/StringItem;

    move v0, v1

    .line 330
    .end local v1           #a:I
    .restart local v0       #a:I
    :cond_2
    iget-object v8, v5, Lpxb/android/axml/AxmlWriter$Ns;->prefix:Lpxb/android/StringItem;

    invoke-virtual {p0, v8}, Lpxb/android/axml/AxmlWriter;->update(Lpxb/android/StringItem;)Lpxb/android/StringItem;

    move-result-object v8

    iput-object v8, v5, Lpxb/android/axml/AxmlWriter$Ns;->prefix:Lpxb/android/StringItem;

    .line 331
    iget-object v8, v5, Lpxb/android/axml/AxmlWriter$Ns;->uri:Lpxb/android/StringItem;

    invoke-virtual {p0, v8}, Lpxb/android/axml/AxmlWriter;->update(Lpxb/android/StringItem;)Lpxb/android/StringItem;

    move-result-object v8

    iput-object v8, v5, Lpxb/android/axml/AxmlWriter$Ns;->uri:Lpxb/android/StringItem;

    goto :goto_1

    .line 335
    .end local v2           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lpxb/android/axml/AxmlWriter$Ns;>;"
    .end local v5           #ns:Lpxb/android/axml/AxmlWriter$Ns;
    :cond_3
    iget-object v8, p0, Lpxb/android/axml/AxmlWriter;->nses:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v8

    mul-int/lit8 v8, v8, 0x18

    mul-int/lit8 v8, v8, 0x2

    add-int/2addr v6, v8

    .line 337
    iget-object v8, p0, Lpxb/android/axml/AxmlWriter;->stringItems:Lpxb/android/StringItems;

    iget-object v9, p0, Lpxb/android/axml/AxmlWriter;->resourceString:Ljava/util/List;

    invoke-virtual {v8, v9}, Lpxb/android/StringItems;->addAll(Ljava/util/Collection;)Z

    .line 338
    iput-object v12, p0, Lpxb/android/axml/AxmlWriter;->resourceString:Ljava/util/List;

    .line 339
    iget-object v8, p0, Lpxb/android/axml/AxmlWriter;->stringItems:Lpxb/android/StringItems;

    iget-object v9, p0, Lpxb/android/axml/AxmlWriter;->otherString:Ljava/util/List;

    invoke-virtual {v8, v9}, Lpxb/android/StringItems;->addAll(Ljava/util/Collection;)Z

    .line 340
    iput-object v12, p0, Lpxb/android/axml/AxmlWriter;->otherString:Ljava/util/List;

    .line 341
    iget-object v8, p0, Lpxb/android/axml/AxmlWriter;->stringItems:Lpxb/android/StringItems;

    invoke-virtual {v8}, Lpxb/android/StringItems;->prepare()V

    .line 342
    iget-object v8, p0, Lpxb/android/axml/AxmlWriter;->stringItems:Lpxb/android/StringItems;

    invoke-virtual {v8}, Lpxb/android/StringItems;->getSize()I

    move-result v7

    .line 343
    .local v7, stringSize:I
    rem-int/lit8 v8, v7, 0x4

    if-eqz v8, :cond_4

    .line 344
    rem-int/lit8 v8, v7, 0x4

    rsub-int/lit8 v8, v8, 0x4

    add-int/2addr v7, v8

    .line 346
    :cond_4
    add-int/lit8 v8, v7, 0x8

    add-int/2addr v6, v8

    .line 347
    iget-object v8, p0, Lpxb/android/axml/AxmlWriter;->resourceIds:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    mul-int/lit8 v8, v8, 0x4

    add-int/lit8 v8, v8, 0x8

    add-int/2addr v6, v8

    .line 348
    return v6
.end method


# virtual methods
.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
    .locals 2
    .parameter "ns"
    .parameter "name"

    .prologue
    .line 299
    new-instance v0, Lpxb/android/axml/AxmlWriter$NodeImpl;

    invoke-direct {v0, p1, p2}, Lpxb/android/axml/AxmlWriter$NodeImpl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    .local v0, first:Lpxb/android/axml/AxmlWriter$NodeImpl;
    iget-object v1, p0, Lpxb/android/axml/AxmlWriter;->firsts:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    return-object v0
.end method

.method public end()V
    .locals 0

    .prologue
    .line 306
    return-void
.end method

.method public ns(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .parameter "prefix"
    .parameter "uri"
    .parameter "ln"

    .prologue
    .line 310
    iget-object v1, p0, Lpxb/android/axml/AxmlWriter;->nses:Ljava/util/Map;

    new-instance v2, Lpxb/android/axml/AxmlWriter$Ns;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    new-instance v3, Lpxb/android/StringItem;

    invoke-direct {v3, p2}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v0, v3, p3}, Lpxb/android/axml/AxmlWriter$Ns;-><init>(Lpxb/android/StringItem;Lpxb/android/StringItem;I)V

    invoke-interface {v1, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    return-void

    .line 310
    :cond_0
    new-instance v0, Lpxb/android/StringItem;

    invoke-direct {v0, p1}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public toByteArray()[B
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v13, 0x18

    const/4 v12, -0x1

    .line 353
    invoke-direct {p0}, Lpxb/android/axml/AxmlWriter;->prepare()I

    move-result v10

    add-int/lit8 v7, v10, 0x8

    .line 354
    .local v7, size:I
    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v10

    sget-object v11, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v10, v11}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 356
    .local v5, out:Ljava/nio/ByteBuffer;
    const v10, 0x80003

    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 357
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 359
    iget-object v10, p0, Lpxb/android/axml/AxmlWriter;->stringItems:Lpxb/android/StringItems;

    invoke-virtual {v10}, Lpxb/android/StringItems;->getSize()I

    move-result v9

    .line 360
    .local v9, stringSize:I
    const/4 v6, 0x0

    .line 361
    .local v6, padding:I
    rem-int/lit8 v10, v9, 0x4

    if-eqz v10, :cond_0

    .line 362
    rem-int/lit8 v10, v9, 0x4

    rsub-int/lit8 v6, v10, 0x4

    .line 364
    :cond_0
    const v10, 0x1c0001

    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 365
    add-int v10, v9, v6

    add-int/lit8 v10, v10, 0x8

    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 366
    iget-object v10, p0, Lpxb/android/axml/AxmlWriter;->stringItems:Lpxb/android/StringItems;

    invoke-virtual {v10, v5}, Lpxb/android/StringItems;->write(Ljava/nio/ByteBuffer;)V

    .line 367
    new-array v10, v6, [B

    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 369
    const v10, 0x80180

    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 370
    iget-object v10, p0, Lpxb/android/axml/AxmlWriter;->resourceIds:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    mul-int/lit8 v10, v10, 0x4

    add-int/lit8 v10, v10, 0x8

    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 371
    iget-object v10, p0, Lpxb/android/axml/AxmlWriter;->resourceIds:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 372
    .local v2, i:Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 375
    .end local v2           #i:Ljava/lang/Integer;
    :cond_1
    new-instance v8, Ljava/util/Stack;

    invoke-direct {v8}, Ljava/util/Stack;-><init>()V

    .line 376
    .local v8, stack:Ljava/util/Stack;,"Ljava/util/Stack<Lpxb/android/axml/AxmlWriter$Ns;>;"
    iget-object v10, p0, Lpxb/android/axml/AxmlWriter;->nses:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 377
    .local v0, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lpxb/android/axml/AxmlWriter$Ns;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lpxb/android/axml/AxmlWriter$Ns;

    .line 378
    .local v4, ns:Lpxb/android/axml/AxmlWriter$Ns;
    invoke-virtual {v8, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    const v10, 0x100100

    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 380
    invoke-virtual {v5, v13}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 381
    invoke-virtual {v5, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 382
    invoke-virtual {v5, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 383
    iget-object v10, v4, Lpxb/android/axml/AxmlWriter$Ns;->prefix:Lpxb/android/StringItem;

    iget v10, v10, Lpxb/android/StringItem;->index:I

    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 384
    iget-object v10, v4, Lpxb/android/axml/AxmlWriter$Ns;->uri:Lpxb/android/StringItem;

    iget v10, v10, Lpxb/android/StringItem;->index:I

    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    .line 387
    .end local v0           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lpxb/android/axml/AxmlWriter$Ns;>;"
    .end local v4           #ns:Lpxb/android/axml/AxmlWriter$Ns;
    :cond_2
    iget-object v10, p0, Lpxb/android/axml/AxmlWriter;->firsts:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpxb/android/axml/AxmlWriter$NodeImpl;

    .line 388
    .local v1, first:Lpxb/android/axml/AxmlWriter$NodeImpl;
    invoke-virtual {v1, v5}, Lpxb/android/axml/AxmlWriter$NodeImpl;->write(Ljava/nio/ByteBuffer;)V

    goto :goto_2

    .line 391
    .end local v1           #first:Lpxb/android/axml/AxmlWriter$NodeImpl;
    :cond_3
    :goto_3
    invoke-virtual {v8}, Ljava/util/Stack;->size()I

    move-result v10

    if-lez v10, :cond_4

    .line 392
    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lpxb/android/axml/AxmlWriter$Ns;

    .line 393
    .restart local v4       #ns:Lpxb/android/axml/AxmlWriter$Ns;
    const v10, 0x100101

    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 394
    invoke-virtual {v5, v13}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 395
    iget v10, v4, Lpxb/android/axml/AxmlWriter$Ns;->ln:I

    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 396
    invoke-virtual {v5, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 397
    iget-object v10, v4, Lpxb/android/axml/AxmlWriter$Ns;->prefix:Lpxb/android/StringItem;

    iget v10, v10, Lpxb/android/StringItem;->index:I

    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 398
    iget-object v10, v4, Lpxb/android/axml/AxmlWriter$Ns;->uri:Lpxb/android/StringItem;

    iget v10, v10, Lpxb/android/StringItem;->index:I

    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_3

    .line 400
    .end local v4           #ns:Lpxb/android/axml/AxmlWriter$Ns;
    :cond_4
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v10

    return-object v10
.end method

.method update(Lpxb/android/StringItem;)Lpxb/android/StringItem;
    .locals 3
    .parameter "item"

    .prologue
    .line 404
    if-nez p1, :cond_0

    .line 405
    const/4 v0, 0x0

    .line 412
    :goto_0
    return-object v0

    .line 406
    :cond_0
    iget-object v2, p0, Lpxb/android/axml/AxmlWriter;->otherString:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 407
    .local v1, i:I
    if-gez v1, :cond_1

    .line 408
    new-instance v0, Lpxb/android/StringItem;

    iget-object v2, p1, Lpxb/android/StringItem;->data:Ljava/lang/String;

    invoke-direct {v0, v2}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    .line 409
    .local v0, copy:Lpxb/android/StringItem;
    iget-object v2, p0, Lpxb/android/axml/AxmlWriter;->otherString:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 412
    .end local v0           #copy:Lpxb/android/StringItem;
    :cond_1
    iget-object v2, p0, Lpxb/android/axml/AxmlWriter;->otherString:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpxb/android/StringItem;

    move-object v0, v2

    goto :goto_0
.end method

.method updateNs(Lpxb/android/StringItem;)Lpxb/android/StringItem;
    .locals 3
    .parameter "item"

    .prologue
    const/4 v1, 0x0

    .line 417
    if-nez p1, :cond_0

    .line 424
    :goto_0
    return-object v1

    .line 420
    :cond_0
    iget-object v0, p1, Lpxb/android/StringItem;->data:Ljava/lang/String;

    .line 421
    .local v0, ns:Ljava/lang/String;
    iget-object v2, p0, Lpxb/android/axml/AxmlWriter;->nses:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 422
    iget-object v2, p0, Lpxb/android/axml/AxmlWriter;->nses:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    :cond_1
    invoke-virtual {p0, p1}, Lpxb/android/axml/AxmlWriter;->update(Lpxb/android/StringItem;)Lpxb/android/StringItem;

    move-result-object v1

    goto :goto_0
.end method

.method updateWithResourceId(Lpxb/android/StringItem;I)Lpxb/android/StringItem;
    .locals 5
    .parameter "name"
    .parameter "resourceId"

    .prologue
    .line 428
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lpxb/android/StringItem;->data:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 429
    .local v2, key:Ljava/lang/String;
    iget-object v3, p0, Lpxb/android/axml/AxmlWriter;->resourceId2Str:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpxb/android/StringItem;

    .line 430
    .local v1, item:Lpxb/android/StringItem;
    if-eqz v1, :cond_0

    .line 437
    .end local v1           #item:Lpxb/android/StringItem;
    :goto_0
    return-object v1

    .line 433
    .restart local v1       #item:Lpxb/android/StringItem;
    :cond_0
    new-instance v0, Lpxb/android/StringItem;

    iget-object v3, p1, Lpxb/android/StringItem;->data:Ljava/lang/String;

    invoke-direct {v0, v3}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    .line 434
    .local v0, copy:Lpxb/android/StringItem;
    iget-object v3, p0, Lpxb/android/axml/AxmlWriter;->resourceIds:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 435
    iget-object v3, p0, Lpxb/android/axml/AxmlWriter;->resourceString:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 436
    iget-object v3, p0, Lpxb/android/axml/AxmlWriter;->resourceId2Str:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 437
    goto :goto_0
.end method
