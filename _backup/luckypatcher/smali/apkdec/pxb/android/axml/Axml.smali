.class public Lpxb/android/axml/Axml;
.super Lpxb/android/axml/AxmlVisitor;
.source "Axml.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpxb/android/axml/Axml$Ns;,
        Lpxb/android/axml/Axml$Node;
    }
.end annotation


# instance fields
.field public firsts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/axml/Axml$Node;",
            ">;"
        }
    .end annotation
.end field

.field public nses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/axml/Axml$Ns;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lpxb/android/axml/AxmlVisitor;-><init>()V

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axml/Axml;->firsts:Ljava/util/List;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axml/Axml;->nses:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public accept(Lpxb/android/axml/AxmlVisitor;)V
    .locals 4
    .parameter "visitor"

    .prologue
    .line 117
    iget-object v3, p0, Lpxb/android/axml/Axml;->nses:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpxb/android/axml/Axml$Ns;

    .line 118
    .local v2, ns:Lpxb/android/axml/Axml$Ns;
    invoke-virtual {v2, p1}, Lpxb/android/axml/Axml$Ns;->accept(Lpxb/android/axml/AxmlVisitor;)V

    goto :goto_0

    .line 120
    .end local v2           #ns:Lpxb/android/axml/Axml$Ns;
    :cond_0
    iget-object v3, p0, Lpxb/android/axml/Axml;->firsts:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpxb/android/axml/Axml$Node;

    .line 121
    .local v0, first:Lpxb/android/axml/Axml$Node;
    invoke-virtual {v0, p1}, Lpxb/android/axml/Axml$Node;->accept(Lpxb/android/axml/NodeVisitor;)V

    goto :goto_1

    .line 123
    .end local v0           #first:Lpxb/android/axml/Axml$Node;
    :cond_1
    return-void
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
    .locals 2
    .parameter "ns"
    .parameter "name"

    .prologue
    .line 127
    new-instance v0, Lpxb/android/axml/Axml$Node;

    invoke-direct {v0}, Lpxb/android/axml/Axml$Node;-><init>()V

    .line 128
    .local v0, node:Lpxb/android/axml/Axml$Node;
    iput-object p2, v0, Lpxb/android/axml/Axml$Node;->name:Ljava/lang/String;

    .line 129
    iput-object p1, v0, Lpxb/android/axml/Axml$Node;->ns:Ljava/lang/String;

    .line 130
    iget-object v1, p0, Lpxb/android/axml/Axml;->firsts:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    return-object v0
.end method

.method public ns(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .parameter "prefix"
    .parameter "uri"
    .parameter "ln"

    .prologue
    .line 136
    new-instance v0, Lpxb/android/axml/Axml$Ns;

    invoke-direct {v0}, Lpxb/android/axml/Axml$Ns;-><init>()V

    .line 137
    .local v0, ns:Lpxb/android/axml/Axml$Ns;
    iput-object p1, v0, Lpxb/android/axml/Axml$Ns;->prefix:Ljava/lang/String;

    .line 138
    iput-object p2, v0, Lpxb/android/axml/Axml$Ns;->uri:Ljava/lang/String;

    .line 139
    iput p3, v0, Lpxb/android/axml/Axml$Ns;->ln:I

    .line 140
    iget-object v1, p0, Lpxb/android/axml/Axml;->nses:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    return-void
.end method
