.class public abstract Lpxb/android/axml/NodeVisitor;
.super Ljava/lang/Object;
.source "NodeVisitor.java"


# static fields
.field public static final TYPE_FIRST_INT:I = 0x10

.field public static final TYPE_INT_BOOLEAN:I = 0x12

.field public static final TYPE_INT_HEX:I = 0x11

.field public static final TYPE_REFERENCE:I = 0x1

.field public static final TYPE_STRING:I = 0x3


# instance fields
.field protected nv:Lpxb/android/axml/NodeVisitor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public constructor <init>(Lpxb/android/axml/NodeVisitor;)V
    .locals 0
    .parameter "nv"

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lpxb/android/axml/NodeVisitor;->nv:Lpxb/android/axml/NodeVisitor;

    .line 34
    return-void
.end method


# virtual methods
.method public attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
    .locals 6
    .parameter "ns"
    .parameter "name"
    .parameter "resourceId"
    .parameter "type"
    .parameter "obj"

    .prologue
    .line 48
    iget-object v0, p0, Lpxb/android/axml/NodeVisitor;->nv:Lpxb/android/axml/NodeVisitor;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lpxb/android/axml/NodeVisitor;->nv:Lpxb/android/axml/NodeVisitor;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lpxb/android/axml/NodeVisitor;->attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V

    .line 51
    :cond_0
    return-void
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
    .locals 1
    .parameter "ns"
    .parameter "name"

    .prologue
    .line 61
    iget-object v0, p0, Lpxb/android/axml/NodeVisitor;->nv:Lpxb/android/axml/NodeVisitor;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lpxb/android/axml/NodeVisitor;->nv:Lpxb/android/axml/NodeVisitor;

    invoke-virtual {v0, p1, p2}, Lpxb/android/axml/NodeVisitor;->child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public end()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lpxb/android/axml/NodeVisitor;->nv:Lpxb/android/axml/NodeVisitor;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lpxb/android/axml/NodeVisitor;->nv:Lpxb/android/axml/NodeVisitor;

    invoke-virtual {v0}, Lpxb/android/axml/NodeVisitor;->end()V

    .line 74
    :cond_0
    return-void
.end method

.method public line(I)V
    .locals 1
    .parameter "ln"

    .prologue
    .line 82
    iget-object v0, p0, Lpxb/android/axml/NodeVisitor;->nv:Lpxb/android/axml/NodeVisitor;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lpxb/android/axml/NodeVisitor;->nv:Lpxb/android/axml/NodeVisitor;

    invoke-virtual {v0, p1}, Lpxb/android/axml/NodeVisitor;->line(I)V

    .line 85
    :cond_0
    return-void
.end method

.method public text(ILjava/lang/String;)V
    .locals 1
    .parameter "lineNumber"
    .parameter "value"

    .prologue
    .line 93
    iget-object v0, p0, Lpxb/android/axml/NodeVisitor;->nv:Lpxb/android/axml/NodeVisitor;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lpxb/android/axml/NodeVisitor;->nv:Lpxb/android/axml/NodeVisitor;

    invoke-virtual {v0, p1, p2}, Lpxb/android/axml/NodeVisitor;->text(ILjava/lang/String;)V

    .line 96
    :cond_0
    return-void
.end method
