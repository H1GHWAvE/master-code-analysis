.class public Lpxb/android/axml/AxmlReader;
.super Ljava/lang/Object;
.source "AxmlReader.java"


# static fields
.field public static final EMPTY_VISITOR:Lpxb/android/axml/NodeVisitor;


# instance fields
.field final parser:Lpxb/android/axml/AxmlParser;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lpxb/android/axml/AxmlReader$1;

    invoke-direct {v0}, Lpxb/android/axml/AxmlReader$1;-><init>()V

    sput-object v0, Lpxb/android/axml/AxmlReader;->EMPTY_VISITOR:Lpxb/android/axml/NodeVisitor;

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .parameter "data"

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lpxb/android/axml/AxmlParser;

    invoke-direct {v0, p1}, Lpxb/android/axml/AxmlParser;-><init>([B)V

    iput-object v0, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    .line 47
    return-void
.end method


# virtual methods
.method public accept(Lpxb/android/axml/AxmlVisitor;)V
    .locals 9
    .parameter "av"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    new-instance v7, Ljava/util/Stack;

    invoke-direct {v7}, Ljava/util/Stack;-><init>()V

    .line 51
    .local v7, nvs:Ljava/util/Stack;,"Ljava/util/Stack<Lpxb/android/axml/NodeVisitor;>;"
    move-object v0, p1

    .line 53
    .local v0, tos:Lpxb/android/axml/NodeVisitor;
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v1}, Lpxb/android/axml/AxmlParser;->next()I

    move-result v8

    .line 54
    .local v8, type:I
    packed-switch v8, :pswitch_data_0

    goto :goto_0

    .line 56
    :pswitch_1
    invoke-virtual {v7, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iget-object v1, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v1}, Lpxb/android/axml/AxmlParser;->getNamespaceUri()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v2}, Lpxb/android/axml/AxmlParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lpxb/android/axml/NodeVisitor;->child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_1

    .line 59
    sget-object v1, Lpxb/android/axml/AxmlReader;->EMPTY_VISITOR:Lpxb/android/axml/NodeVisitor;

    if-eq v0, v1, :cond_0

    .line 60
    iget-object v1, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v1}, Lpxb/android/axml/AxmlParser;->getLineNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Lpxb/android/axml/NodeVisitor;->line(I)V

    .line 61
    const/4 v6, 0x0

    .local v6, i:I
    :goto_1
    iget-object v1, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v1}, Lpxb/android/axml/AxmlParser;->getAttrCount()I

    move-result v1

    if-ge v6, v1, :cond_0

    .line 62
    iget-object v1, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v1, v6}, Lpxb/android/axml/AxmlParser;->getAttrNs(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v2, v6}, Lpxb/android/axml/AxmlParser;->getAttrName(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v3, v6}, Lpxb/android/axml/AxmlParser;->getAttrResId(I)I

    move-result v3

    iget-object v4, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v4, v6}, Lpxb/android/axml/AxmlParser;->getAttrType(I)I

    move-result v4

    iget-object v5, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v5, v6}, Lpxb/android/axml/AxmlParser;->getAttrValue(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lpxb/android/axml/NodeVisitor;->attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V

    .line 61
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 67
    .end local v6           #i:I
    :cond_1
    sget-object v0, Lpxb/android/axml/AxmlReader;->EMPTY_VISITOR:Lpxb/android/axml/NodeVisitor;

    .line 69
    goto :goto_0

    .line 71
    :pswitch_2
    invoke-virtual {v0}, Lpxb/android/axml/NodeVisitor;->end()V

    .line 72
    invoke-virtual {v7}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #tos:Lpxb/android/axml/NodeVisitor;
    check-cast v0, Lpxb/android/axml/NodeVisitor;

    .line 73
    .restart local v0       #tos:Lpxb/android/axml/NodeVisitor;
    goto :goto_0

    .line 75
    :pswitch_3
    iget-object v1, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v1}, Lpxb/android/axml/AxmlParser;->getNamespacePrefix()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v2}, Lpxb/android/axml/AxmlParser;->getNamespaceUri()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v3}, Lpxb/android/axml/AxmlParser;->getLineNumber()I

    move-result v3

    invoke-virtual {p1, v1, v2, v3}, Lpxb/android/axml/AxmlVisitor;->ns(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 80
    :pswitch_4
    iget-object v1, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v1}, Lpxb/android/axml/AxmlParser;->getLineNumber()I

    move-result v1

    iget-object v2, p0, Lpxb/android/axml/AxmlReader;->parser:Lpxb/android/axml/AxmlParser;

    invoke-virtual {v2}, Lpxb/android/axml/AxmlParser;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lpxb/android/axml/NodeVisitor;->text(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 83
    :pswitch_5
    return-void

    .line 54
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
