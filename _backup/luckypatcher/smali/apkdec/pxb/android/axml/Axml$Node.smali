.class public Lpxb/android/axml/Axml$Node;
.super Lpxb/android/axml/NodeVisitor;
.source "Axml.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpxb/android/axml/Axml;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Node"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpxb/android/axml/Axml$Node$Text;,
        Lpxb/android/axml/Axml$Node$Attr;
    }
.end annotation


# instance fields
.field public attrs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/axml/Axml$Node$Attr;",
            ">;"
        }
    .end annotation
.end field

.field public children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/axml/Axml$Node;",
            ">;"
        }
    .end annotation
.end field

.field public ln:Ljava/lang/Integer;

.field public name:Ljava/lang/String;

.field public ns:Ljava/lang/String;

.field public text:Lpxb/android/axml/Axml$Node$Text;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lpxb/android/axml/NodeVisitor;-><init>()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axml/Axml$Node;->attrs:Ljava/util/List;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axml/Axml$Node;->children:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public accept(Lpxb/android/axml/NodeVisitor;)V
    .locals 3
    .parameter "nodeVisitor"

    .prologue
    .line 50
    iget-object v1, p0, Lpxb/android/axml/Axml$Node;->ns:Ljava/lang/String;

    iget-object v2, p0, Lpxb/android/axml/Axml$Node;->name:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lpxb/android/axml/NodeVisitor;->child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;

    move-result-object v0

    .line 51
    .local v0, nodeVisitor2:Lpxb/android/axml/NodeVisitor;
    invoke-virtual {p0, v0}, Lpxb/android/axml/Axml$Node;->acceptB(Lpxb/android/axml/NodeVisitor;)V

    .line 52
    invoke-virtual {v0}, Lpxb/android/axml/NodeVisitor;->end()V

    .line 53
    return-void
.end method

.method public acceptB(Lpxb/android/axml/NodeVisitor;)V
    .locals 4
    .parameter "nodeVisitor"

    .prologue
    .line 56
    iget-object v3, p0, Lpxb/android/axml/Axml$Node;->text:Lpxb/android/axml/Axml$Node$Text;

    if-eqz v3, :cond_0

    .line 57
    iget-object v3, p0, Lpxb/android/axml/Axml$Node;->text:Lpxb/android/axml/Axml$Node$Text;

    invoke-virtual {v3, p1}, Lpxb/android/axml/Axml$Node$Text;->accept(Lpxb/android/axml/NodeVisitor;)V

    .line 59
    :cond_0
    iget-object v3, p0, Lpxb/android/axml/Axml$Node;->attrs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpxb/android/axml/Axml$Node$Attr;

    .line 60
    .local v0, a:Lpxb/android/axml/Axml$Node$Attr;
    invoke-virtual {v0, p1}, Lpxb/android/axml/Axml$Node$Attr;->accept(Lpxb/android/axml/NodeVisitor;)V

    goto :goto_0

    .line 62
    .end local v0           #a:Lpxb/android/axml/Axml$Node$Attr;
    :cond_1
    iget-object v3, p0, Lpxb/android/axml/Axml$Node;->ln:Ljava/lang/Integer;

    if-eqz v3, :cond_2

    .line 63
    iget-object v3, p0, Lpxb/android/axml/Axml$Node;->ln:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v3}, Lpxb/android/axml/NodeVisitor;->line(I)V

    .line 65
    :cond_2
    iget-object v3, p0, Lpxb/android/axml/Axml$Node;->children:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpxb/android/axml/Axml$Node;

    .line 66
    .local v1, c:Lpxb/android/axml/Axml$Node;
    invoke-virtual {v1, p1}, Lpxb/android/axml/Axml$Node;->accept(Lpxb/android/axml/NodeVisitor;)V

    goto :goto_1

    .line 68
    .end local v1           #c:Lpxb/android/axml/Axml$Node;
    :cond_3
    return-void
.end method

.method public attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
    .locals 2
    .parameter "ns"
    .parameter "name"
    .parameter "resourceId"
    .parameter "type"
    .parameter "obj"

    .prologue
    .line 72
    new-instance v0, Lpxb/android/axml/Axml$Node$Attr;

    invoke-direct {v0}, Lpxb/android/axml/Axml$Node$Attr;-><init>()V

    .line 73
    .local v0, attr:Lpxb/android/axml/Axml$Node$Attr;
    iput-object p2, v0, Lpxb/android/axml/Axml$Node$Attr;->name:Ljava/lang/String;

    .line 74
    iput-object p1, v0, Lpxb/android/axml/Axml$Node$Attr;->ns:Ljava/lang/String;

    .line 75
    iput p3, v0, Lpxb/android/axml/Axml$Node$Attr;->resourceId:I

    .line 76
    iput p4, v0, Lpxb/android/axml/Axml$Node$Attr;->type:I

    .line 77
    iput-object p5, v0, Lpxb/android/axml/Axml$Node$Attr;->value:Ljava/lang/Object;

    .line 78
    iget-object v1, p0, Lpxb/android/axml/Axml$Node;->attrs:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    return-void
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
    .locals 2
    .parameter "ns"
    .parameter "name"

    .prologue
    .line 83
    new-instance v0, Lpxb/android/axml/Axml$Node;

    invoke-direct {v0}, Lpxb/android/axml/Axml$Node;-><init>()V

    .line 84
    .local v0, node:Lpxb/android/axml/Axml$Node;
    iput-object p2, v0, Lpxb/android/axml/Axml$Node;->name:Ljava/lang/String;

    .line 85
    iput-object p1, v0, Lpxb/android/axml/Axml$Node;->ns:Ljava/lang/String;

    .line 86
    iget-object v1, p0, Lpxb/android/axml/Axml$Node;->children:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    return-object v0
.end method

.method public line(I)V
    .locals 1
    .parameter "ln"

    .prologue
    .line 92
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpxb/android/axml/Axml$Node;->ln:Ljava/lang/Integer;

    .line 93
    return-void
.end method

.method public text(ILjava/lang/String;)V
    .locals 1
    .parameter "lineNumber"
    .parameter "value"

    .prologue
    .line 97
    new-instance v0, Lpxb/android/axml/Axml$Node$Text;

    invoke-direct {v0}, Lpxb/android/axml/Axml$Node$Text;-><init>()V

    .line 98
    .local v0, text:Lpxb/android/axml/Axml$Node$Text;
    iput p1, v0, Lpxb/android/axml/Axml$Node$Text;->ln:I

    .line 99
    iput-object p2, v0, Lpxb/android/axml/Axml$Node$Text;->text:Ljava/lang/String;

    .line 100
    iput-object v0, p0, Lpxb/android/axml/Axml$Node;->text:Lpxb/android/axml/Axml$Node$Text;

    .line 101
    return-void
.end method
