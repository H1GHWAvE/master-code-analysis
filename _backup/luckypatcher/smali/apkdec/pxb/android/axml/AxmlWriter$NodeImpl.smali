.class Lpxb/android/axml/AxmlWriter$NodeImpl;
.super Lpxb/android/axml/NodeVisitor;
.source "AxmlWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpxb/android/axml/AxmlWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "NodeImpl"
.end annotation


# instance fields
.field private attrs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lpxb/android/axml/AxmlWriter$Attr;",
            ">;"
        }
    .end annotation
.end field

.field private children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/axml/AxmlWriter$NodeImpl;",
            ">;"
        }
    .end annotation
.end field

.field clz:Lpxb/android/axml/AxmlWriter$Attr;

.field id:Lpxb/android/axml/AxmlWriter$Attr;

.field private line:I

.field private name:Lpxb/android/StringItem;

.field private ns:Lpxb/android/StringItem;

.field style:Lpxb/android/axml/AxmlWriter$Attr;

.field private text:Lpxb/android/StringItem;

.field private textLineNumber:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "ns"
    .parameter "name"

    .prologue
    const/4 v1, 0x0

    .line 127
    invoke-direct {p0, v1}, Lpxb/android/axml/NodeVisitor;-><init>(Lpxb/android/axml/NodeVisitor;)V

    .line 115
    new-instance v0, Ljava/util/TreeSet;

    sget-object v2, Lpxb/android/axml/AxmlWriter;->ATTR_CMP:Ljava/util/Comparator;

    invoke-direct {v0, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->attrs:Ljava/util/Set;

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->children:Ljava/util/List;

    .line 128
    if-nez p1, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->ns:Lpxb/android/StringItem;

    .line 129
    if-nez p2, :cond_1

    :goto_1
    iput-object v1, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->name:Lpxb/android/StringItem;

    .line 130
    return-void

    .line 128
    :cond_0
    new-instance v0, Lpxb/android/StringItem;

    invoke-direct {v0, p1}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 129
    :cond_1
    new-instance v1, Lpxb/android/StringItem;

    invoke-direct {v1, p2}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
    .locals 6
    .parameter "ns"
    .parameter "name"
    .parameter "resourceId"
    .parameter "type"
    .parameter "value"

    .prologue
    const/4 v4, 0x0

    .line 134
    if-nez p2, :cond_0

    .line 135
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "name can\'t be null"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 137
    :cond_0
    new-instance v0, Lpxb/android/axml/AxmlWriter$Attr;

    if-nez p1, :cond_2

    move-object v3, v4

    :goto_0
    new-instance v5, Lpxb/android/StringItem;

    invoke-direct {v5, p2}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v3, v5, p3}, Lpxb/android/axml/AxmlWriter$Attr;-><init>(Lpxb/android/StringItem;Lpxb/android/StringItem;I)V

    .line 138
    .local v0, a:Lpxb/android/axml/AxmlWriter$Attr;
    iput p4, v0, Lpxb/android/axml/AxmlWriter$Attr;->type:I

    .line 140
    instance-of v3, p5, Lpxb/android/axml/ValueWrapper;

    if-eqz v3, :cond_3

    move-object v2, p5

    .line 141
    check-cast v2, Lpxb/android/axml/ValueWrapper;

    .line 142
    .local v2, valueWrapper:Lpxb/android/axml/ValueWrapper;
    iget-object v3, v2, Lpxb/android/axml/ValueWrapper;->raw:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 143
    new-instance v3, Lpxb/android/StringItem;

    iget-object v4, v2, Lpxb/android/axml/ValueWrapper;->raw:Ljava/lang/String;

    invoke-direct {v3, v4}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    iput-object v3, v0, Lpxb/android/axml/AxmlWriter$Attr;->raw:Lpxb/android/StringItem;

    .line 145
    :cond_1
    iget v3, v2, Lpxb/android/axml/ValueWrapper;->ref:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lpxb/android/axml/AxmlWriter$Attr;->value:Ljava/lang/Object;

    .line 146
    iget v3, v2, Lpxb/android/axml/ValueWrapper;->type:I

    packed-switch v3, :pswitch_data_0

    .line 167
    .end local v2           #valueWrapper:Lpxb/android/axml/ValueWrapper;
    .end local p5
    :goto_1
    iget-object v3, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->attrs:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 168
    return-void

    .line 137
    .end local v0           #a:Lpxb/android/axml/AxmlWriter$Attr;
    .restart local p5
    :cond_2
    new-instance v3, Lpxb/android/StringItem;

    invoke-direct {v3, p1}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 148
    .restart local v0       #a:Lpxb/android/axml/AxmlWriter$Attr;
    .restart local v2       #valueWrapper:Lpxb/android/axml/ValueWrapper;
    :pswitch_0
    iput-object v0, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->clz:Lpxb/android/axml/AxmlWriter$Attr;

    goto :goto_1

    .line 151
    :pswitch_1
    iput-object v0, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->id:Lpxb/android/axml/AxmlWriter$Attr;

    goto :goto_1

    .line 154
    :pswitch_2
    iput-object v0, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->style:Lpxb/android/axml/AxmlWriter$Attr;

    goto :goto_1

    .line 157
    .end local v2           #valueWrapper:Lpxb/android/axml/ValueWrapper;
    :cond_3
    const/4 v3, 0x3

    if-ne p4, v3, :cond_4

    .line 158
    new-instance v1, Lpxb/android/StringItem;

    check-cast p5, Ljava/lang/String;

    .end local p5
    invoke-direct {v1, p5}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    .line 159
    .local v1, raw:Lpxb/android/StringItem;
    iput-object v1, v0, Lpxb/android/axml/AxmlWriter$Attr;->raw:Lpxb/android/StringItem;

    .line 160
    iput-object v1, v0, Lpxb/android/axml/AxmlWriter$Attr;->value:Ljava/lang/Object;

    goto :goto_1

    .line 163
    .end local v1           #raw:Lpxb/android/StringItem;
    .restart local p5
    :cond_4
    iput-object v4, v0, Lpxb/android/axml/AxmlWriter$Attr;->raw:Lpxb/android/StringItem;

    .line 164
    iput-object p5, v0, Lpxb/android/axml/AxmlWriter$Attr;->value:Ljava/lang/Object;

    goto :goto_1

    .line 146
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
    .locals 2
    .parameter "ns"
    .parameter "name"

    .prologue
    .line 172
    new-instance v0, Lpxb/android/axml/AxmlWriter$NodeImpl;

    invoke-direct {v0, p1, p2}, Lpxb/android/axml/AxmlWriter$NodeImpl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    .local v0, child:Lpxb/android/axml/AxmlWriter$NodeImpl;
    iget-object v1, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->children:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    return-object v0
.end method

.method public end()V
    .locals 0

    .prologue
    .line 179
    return-void
.end method

.method public line(I)V
    .locals 0
    .parameter "ln"

    .prologue
    .line 183
    iput p1, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->line:I

    .line 184
    return-void
.end method

.method public prepare(Lpxb/android/axml/AxmlWriter;)I
    .locals 7
    .parameter "axmlWriter"

    .prologue
    .line 187
    iget-object v6, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->ns:Lpxb/android/StringItem;

    invoke-virtual {p1, v6}, Lpxb/android/axml/AxmlWriter;->updateNs(Lpxb/android/StringItem;)Lpxb/android/StringItem;

    move-result-object v6

    iput-object v6, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->ns:Lpxb/android/StringItem;

    .line 188
    iget-object v6, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->name:Lpxb/android/StringItem;

    invoke-virtual {p1, v6}, Lpxb/android/axml/AxmlWriter;->update(Lpxb/android/StringItem;)Lpxb/android/StringItem;

    move-result-object v6

    iput-object v6, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->name:Lpxb/android/StringItem;

    .line 190
    const/4 v1, 0x0

    .line 191
    .local v1, attrIndex:I
    iget-object v6, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->attrs:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpxb/android/axml/AxmlWriter$Attr;

    .line 192
    .local v0, attr:Lpxb/android/axml/AxmlWriter$Attr;
    add-int/lit8 v2, v1, 0x1

    .end local v1           #attrIndex:I
    .local v2, attrIndex:I
    iput v1, v0, Lpxb/android/axml/AxmlWriter$Attr;->index:I

    .line 193
    invoke-virtual {v0, p1}, Lpxb/android/axml/AxmlWriter$Attr;->prepare(Lpxb/android/axml/AxmlWriter;)V

    move v1, v2

    .line 194
    .end local v2           #attrIndex:I
    .restart local v1       #attrIndex:I
    goto :goto_0

    .line 196
    .end local v0           #attr:Lpxb/android/axml/AxmlWriter$Attr;
    :cond_0
    iget-object v6, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->text:Lpxb/android/StringItem;

    invoke-virtual {p1, v6}, Lpxb/android/axml/AxmlWriter;->update(Lpxb/android/StringItem;)Lpxb/android/StringItem;

    move-result-object v6

    iput-object v6, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->text:Lpxb/android/StringItem;

    .line 197
    iget-object v6, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->attrs:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x14

    add-int/lit8 v5, v6, 0x3c

    .line 199
    .local v5, size:I
    iget-object v6, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->children:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lpxb/android/axml/AxmlWriter$NodeImpl;

    .line 200
    .local v3, child:Lpxb/android/axml/AxmlWriter$NodeImpl;
    invoke-virtual {v3, p1}, Lpxb/android/axml/AxmlWriter$NodeImpl;->prepare(Lpxb/android/axml/AxmlWriter;)I

    move-result v6

    add-int/2addr v5, v6

    .line 201
    goto :goto_1

    .line 202
    .end local v3           #child:Lpxb/android/axml/AxmlWriter$NodeImpl;
    :cond_1
    iget-object v6, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->text:Lpxb/android/StringItem;

    if-eqz v6, :cond_2

    .line 203
    add-int/lit8 v5, v5, 0x1c

    .line 205
    :cond_2
    return v5
.end method

.method public text(ILjava/lang/String;)V
    .locals 1
    .parameter "ln"
    .parameter "value"

    .prologue
    .line 210
    new-instance v0, Lpxb/android/StringItem;

    invoke-direct {v0, p2}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->text:Lpxb/android/StringItem;

    .line 211
    iput p1, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->textLineNumber:I

    .line 212
    return-void
.end method

.method write(Ljava/nio/ByteBuffer;)V
    .locals 7
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 216
    const v4, 0x100102

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 217
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->attrs:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x14

    add-int/lit8 v4, v4, 0x24

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 218
    iget v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->line:I

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 219
    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 220
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->ns:Lpxb/android/StringItem;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->ns:Lpxb/android/StringItem;

    iget v4, v4, Lpxb/android/StringItem;->index:I

    :goto_0
    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 221
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->name:Lpxb/android/StringItem;

    iget v4, v4, Lpxb/android/StringItem;->index:I

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 222
    const v4, 0x140014

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 223
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->attrs:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    int-to-short v4, v4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 224
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->id:Lpxb/android/axml/AxmlWriter$Attr;

    if-nez v4, :cond_1

    move v4, v6

    :goto_1
    int-to-short v4, v4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 225
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->clz:Lpxb/android/axml/AxmlWriter$Attr;

    if-nez v4, :cond_2

    move v4, v6

    :goto_2
    int-to-short v4, v4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 226
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->style:Lpxb/android/axml/AxmlWriter$Attr;

    if-nez v4, :cond_3

    move v4, v6

    :goto_3
    int-to-short v4, v4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 227
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->attrs:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpxb/android/axml/AxmlWriter$Attr;

    .line 228
    .local v0, attr:Lpxb/android/axml/AxmlWriter$Attr;
    iget-object v4, v0, Lpxb/android/axml/AxmlWriter$Attr;->ns:Lpxb/android/StringItem;

    if-nez v4, :cond_4

    move v4, v5

    :goto_5
    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 229
    iget-object v4, v0, Lpxb/android/axml/AxmlWriter$Attr;->name:Lpxb/android/StringItem;

    iget v4, v4, Lpxb/android/StringItem;->index:I

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 230
    iget-object v4, v0, Lpxb/android/axml/AxmlWriter$Attr;->raw:Lpxb/android/StringItem;

    if-eqz v4, :cond_5

    iget-object v4, v0, Lpxb/android/axml/AxmlWriter$Attr;->raw:Lpxb/android/StringItem;

    iget v4, v4, Lpxb/android/StringItem;->index:I

    :goto_6
    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 231
    iget v4, v0, Lpxb/android/axml/AxmlWriter$Attr;->type:I

    shl-int/lit8 v4, v4, 0x18

    or-int/lit8 v4, v4, 0x8

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 232
    iget-object v3, v0, Lpxb/android/axml/AxmlWriter$Attr;->value:Ljava/lang/Object;

    .line 233
    .local v3, v:Ljava/lang/Object;
    instance-of v4, v3, Lpxb/android/StringItem;

    if-eqz v4, :cond_6

    .line 234
    iget-object v4, v0, Lpxb/android/axml/AxmlWriter$Attr;->value:Ljava/lang/Object;

    check-cast v4, Lpxb/android/StringItem;

    iget v4, v4, Lpxb/android/StringItem;->index:I

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_4

    .end local v0           #attr:Lpxb/android/axml/AxmlWriter$Attr;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #v:Ljava/lang/Object;
    :cond_0
    move v4, v5

    .line 220
    goto :goto_0

    .line 224
    :cond_1
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->id:Lpxb/android/axml/AxmlWriter$Attr;

    iget v4, v4, Lpxb/android/axml/AxmlWriter$Attr;->index:I

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 225
    :cond_2
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->clz:Lpxb/android/axml/AxmlWriter$Attr;

    iget v4, v4, Lpxb/android/axml/AxmlWriter$Attr;->index:I

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 226
    :cond_3
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->style:Lpxb/android/axml/AxmlWriter$Attr;

    iget v4, v4, Lpxb/android/axml/AxmlWriter$Attr;->index:I

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 228
    .restart local v0       #attr:Lpxb/android/axml/AxmlWriter$Attr;
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_4
    iget-object v4, v0, Lpxb/android/axml/AxmlWriter$Attr;->ns:Lpxb/android/StringItem;

    iget v4, v4, Lpxb/android/StringItem;->index:I

    goto :goto_5

    :cond_5
    move v4, v5

    .line 230
    goto :goto_6

    .line 235
    .restart local v3       #v:Ljava/lang/Object;
    :cond_6
    instance-of v4, v3, Ljava/lang/Boolean;

    if-eqz v4, :cond_8

    .line 236
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    move v4, v5

    :goto_7
    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_4

    :cond_7
    move v4, v6

    goto :goto_7

    .line 238
    :cond_8
    iget-object v4, v0, Lpxb/android/axml/AxmlWriter$Attr;->value:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_4

    .line 242
    .end local v0           #attr:Lpxb/android/axml/AxmlWriter$Attr;
    .end local v3           #v:Ljava/lang/Object;
    :cond_9
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->text:Lpxb/android/StringItem;

    if-eqz v4, :cond_a

    .line 243
    const v4, 0x100104

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 244
    const/16 v4, 0x1c

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 245
    iget v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->textLineNumber:I

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 246
    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 247
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->text:Lpxb/android/StringItem;

    iget v4, v4, Lpxb/android/StringItem;->index:I

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 248
    const/16 v4, 0x8

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 249
    invoke-virtual {p1, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 253
    :cond_a
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->children:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpxb/android/axml/AxmlWriter$NodeImpl;

    .line 254
    .local v1, child:Lpxb/android/axml/AxmlWriter$NodeImpl;
    invoke-virtual {v1, p1}, Lpxb/android/axml/AxmlWriter$NodeImpl;->write(Ljava/nio/ByteBuffer;)V

    goto :goto_8

    .line 258
    .end local v1           #child:Lpxb/android/axml/AxmlWriter$NodeImpl;
    :cond_b
    const v4, 0x100103

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 259
    const/16 v4, 0x18

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 260
    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 261
    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 262
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->ns:Lpxb/android/StringItem;

    if-eqz v4, :cond_c

    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->ns:Lpxb/android/StringItem;

    iget v5, v4, Lpxb/android/StringItem;->index:I

    :cond_c
    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 263
    iget-object v4, p0, Lpxb/android/axml/AxmlWriter$NodeImpl;->name:Lpxb/android/StringItem;

    iget v4, v4, Lpxb/android/StringItem;->index:I

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 264
    return-void
.end method
