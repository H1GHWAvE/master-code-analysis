.bytecode 50.0
.class public synchronized abstract kellinwood/logging/AbstractLogger
.super java/lang/Object
.implements kellinwood/logging/LoggerInterface

.field protected 'category' Ljava/lang/String;

.field 'dateFormat' Ljava/text/SimpleDateFormat;

.method public <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/text/SimpleDateFormat
dup
ldc "yyyy-MM-dd hh:mm:ss"
invokespecial java/text/SimpleDateFormat/<init>(Ljava/lang/String;)V
putfield kellinwood/logging/AbstractLogger/dateFormat Ljava/text/SimpleDateFormat;
aload 0
aload 1
putfield kellinwood/logging/AbstractLogger/category Ljava/lang/String;
return
.limit locals 2
.limit stack 4
.end method

.method public debug(Ljava/lang/String;)V
aload 0
ldc "DEBUG"
aload 1
aconst_null
invokevirtual kellinwood/logging/AbstractLogger/writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 2
.limit stack 4
.end method

.method public debug(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
ldc "DEBUG"
aload 1
aload 2
invokevirtual kellinwood/logging/AbstractLogger/writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 3
.limit stack 4
.end method

.method public error(Ljava/lang/String;)V
aload 0
ldc "ERROR"
aload 1
aconst_null
invokevirtual kellinwood/logging/AbstractLogger/writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 2
.limit stack 4
.end method

.method public error(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
ldc "ERROR"
aload 1
aload 2
invokevirtual kellinwood/logging/AbstractLogger/writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 3
.limit stack 4
.end method

.method protected format(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
ldc "%s %s %s: %s\n"
iconst_4
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/logging/AbstractLogger/dateFormat Ljava/text/SimpleDateFormat;
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
invokevirtual java/text/SimpleDateFormat/format(Ljava/util/Date;)Ljava/lang/String;
aastore
dup
iconst_1
aload 1
aastore
dup
iconst_2
aload 0
getfield kellinwood/logging/AbstractLogger/category Ljava/lang/String;
aastore
dup
iconst_3
aload 2
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 7
.end method

.method public info(Ljava/lang/String;)V
aload 0
ldc "INFO"
aload 1
aconst_null
invokevirtual kellinwood/logging/AbstractLogger/writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 2
.limit stack 4
.end method

.method public info(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
ldc "INFO"
aload 1
aload 2
invokevirtual kellinwood/logging/AbstractLogger/writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 3
.limit stack 4
.end method

.method public isDebugEnabled()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isErrorEnabled()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isInfoEnabled()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isWarningEnabled()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public warning(Ljava/lang/String;)V
aload 0
ldc "WARNING"
aload 1
aconst_null
invokevirtual kellinwood/logging/AbstractLogger/writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 2
.limit stack 4
.end method

.method public warning(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
ldc "WARNING"
aload 1
aload 2
invokevirtual kellinwood/logging/AbstractLogger/writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 3
.limit stack 4
.end method

.method protected abstract write(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method protected writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
aload 2
astore 4
aload 2
ifnonnull L0
aload 3
ifnull L1
aload 3
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
astore 4
L0:
aload 0
aload 1
aload 4
aload 3
invokevirtual kellinwood/logging/AbstractLogger/write(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return
L1:
ldc "null"
astore 4
goto L0
.limit locals 5
.limit stack 4
.end method
