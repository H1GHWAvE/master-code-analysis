.bytecode 50.0
.class public synchronized kellinwood/logging/android/AndroidLogger
.super kellinwood/logging/AbstractLogger

.field 'isDebugToastEnabled' Z

.field 'isErrorToastEnabled' Z

.field 'isInfoToastEnabled' Z

.field 'isWarningToastEnabled' Z

.field 'toastContext' Landroid/content/Context;

.method public <init>(Ljava/lang/String;)V
aload 0
aload 1
invokespecial kellinwood/logging/AbstractLogger/<init>(Ljava/lang/String;)V
aload 0
iconst_1
putfield kellinwood/logging/android/AndroidLogger/isErrorToastEnabled Z
aload 0
iconst_1
putfield kellinwood/logging/android/AndroidLogger/isWarningToastEnabled Z
aload 0
iconst_0
putfield kellinwood/logging/android/AndroidLogger/isInfoToastEnabled Z
aload 0
iconst_0
putfield kellinwood/logging/android/AndroidLogger/isDebugToastEnabled Z
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
bipush 46
invokevirtual java/lang/String/lastIndexOf(I)I
istore 2
iload 2
ifle L0
aload 0
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
iload 2
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
putfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
L0:
return
.limit locals 3
.limit stack 4
.end method

.method public debugLO(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
getfield kellinwood/logging/android/AndroidLogger/isDebugToastEnabled Z
istore 3
aload 0
iconst_0
putfield kellinwood/logging/android/AndroidLogger/isDebugToastEnabled Z
aload 0
ldc "DEBUG"
aload 1
aload 2
invokevirtual kellinwood/logging/android/AndroidLogger/writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
iload 3
putfield kellinwood/logging/android/AndroidLogger/isDebugToastEnabled Z
return
.limit locals 4
.limit stack 4
.end method

.method public errorLO(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
getfield kellinwood/logging/android/AndroidLogger/isErrorToastEnabled Z
istore 3
aload 0
iconst_0
putfield kellinwood/logging/android/AndroidLogger/isErrorToastEnabled Z
aload 0
ldc "ERROR"
aload 1
aload 2
invokevirtual kellinwood/logging/android/AndroidLogger/writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
iload 3
putfield kellinwood/logging/android/AndroidLogger/isErrorToastEnabled Z
return
.limit locals 4
.limit stack 4
.end method

.method public getToastContext()Landroid/content/Context;
aload 0
getfield kellinwood/logging/android/AndroidLogger/toastContext Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method public infoLO(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
getfield kellinwood/logging/android/AndroidLogger/isInfoToastEnabled Z
istore 3
aload 0
iconst_0
putfield kellinwood/logging/android/AndroidLogger/isInfoToastEnabled Z
aload 0
ldc "INFO"
aload 1
aload 2
invokevirtual kellinwood/logging/android/AndroidLogger/writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
iload 3
putfield kellinwood/logging/android/AndroidLogger/isInfoToastEnabled Z
return
.limit locals 4
.limit stack 4
.end method

.method public isDebugEnabled()Z
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
iconst_3
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isDebugToastEnabled()Z
aload 0
getfield kellinwood/logging/android/AndroidLogger/isDebugToastEnabled Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isErrorEnabled()Z
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
bipush 6
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isErrorToastEnabled()Z
aload 0
getfield kellinwood/logging/android/AndroidLogger/isErrorToastEnabled Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isInfoEnabled()Z
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
iconst_4
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isInfoToastEnabled()Z
aload 0
getfield kellinwood/logging/android/AndroidLogger/isInfoToastEnabled Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isWarningEnabled()Z
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
iconst_5
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isWarningToastEnabled()Z
aload 0
getfield kellinwood/logging/android/AndroidLogger/isWarningToastEnabled Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setDebugToastEnabled(Z)V
aload 0
iload 1
putfield kellinwood/logging/android/AndroidLogger/isDebugToastEnabled Z
return
.limit locals 2
.limit stack 2
.end method

.method public setErrorToastEnabled(Z)V
aload 0
iload 1
putfield kellinwood/logging/android/AndroidLogger/isErrorToastEnabled Z
return
.limit locals 2
.limit stack 2
.end method

.method public setInfoToastEnabled(Z)V
aload 0
iload 1
putfield kellinwood/logging/android/AndroidLogger/isInfoToastEnabled Z
return
.limit locals 2
.limit stack 2
.end method

.method public setToastContext(Landroid/content/Context;)V
aload 0
aload 1
putfield kellinwood/logging/android/AndroidLogger/toastContext Landroid/content/Context;
return
.limit locals 2
.limit stack 2
.end method

.method public setWarningToastEnabled(Z)V
aload 0
iload 1
putfield kellinwood/logging/android/AndroidLogger/isWarningToastEnabled Z
return
.limit locals 2
.limit stack 2
.end method

.method protected toast(Ljava/lang/String;)V
.catch java/lang/Throwable from L0 to L1 using L2
L0:
aload 0
getfield kellinwood/logging/android/AndroidLogger/toastContext Landroid/content/Context;
ifnull L1
aload 0
getfield kellinwood/logging/android/AndroidLogger/toastContext Landroid/content/Context;
aload 1
iconst_1
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
invokevirtual android/widget/Toast/show()V
L1:
return
L2:
astore 2
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
aload 1
aload 2
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 3
.limit stack 3
.end method

.method public warningLO(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
getfield kellinwood/logging/android/AndroidLogger/isWarningToastEnabled Z
istore 3
aload 0
iconst_0
putfield kellinwood/logging/android/AndroidLogger/isWarningToastEnabled Z
aload 0
ldc "WARNING"
aload 1
aload 2
invokevirtual kellinwood/logging/android/AndroidLogger/writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
iload 3
putfield kellinwood/logging/android/AndroidLogger/isWarningToastEnabled Z
return
.limit locals 4
.limit stack 4
.end method

.method public write(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
ldc "ERROR"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 3
ifnull L1
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
aload 2
aload 3
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L2:
aload 0
getfield kellinwood/logging/android/AndroidLogger/isErrorToastEnabled Z
ifeq L3
aload 0
aload 2
invokevirtual kellinwood/logging/android/AndroidLogger/toast(Ljava/lang/String;)V
L3:
return
L1:
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
aload 2
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L2
L0:
ldc "DEBUG"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
aload 3
ifnull L5
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
aload 2
aload 3
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L6:
aload 0
getfield kellinwood/logging/android/AndroidLogger/isDebugToastEnabled Z
ifeq L3
aload 0
aload 2
invokevirtual kellinwood/logging/android/AndroidLogger/toast(Ljava/lang/String;)V
return
L5:
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
aload 2
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L6
L4:
ldc "WARNING"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L7
aload 3
ifnull L8
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
aload 2
aload 3
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L9:
aload 0
getfield kellinwood/logging/android/AndroidLogger/isWarningToastEnabled Z
ifeq L3
aload 0
aload 2
invokevirtual kellinwood/logging/android/AndroidLogger/toast(Ljava/lang/String;)V
return
L8:
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
aload 2
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L9
L7:
ldc "INFO"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
aload 3
ifnull L10
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
aload 2
aload 3
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L11:
aload 0
getfield kellinwood/logging/android/AndroidLogger/isInfoToastEnabled Z
ifeq L3
aload 0
aload 2
invokevirtual kellinwood/logging/android/AndroidLogger/toast(Ljava/lang/String;)V
return
L10:
aload 0
getfield kellinwood/logging/android/AndroidLogger/category Ljava/lang/String;
aload 2
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L11
.limit locals 4
.limit stack 3
.end method
