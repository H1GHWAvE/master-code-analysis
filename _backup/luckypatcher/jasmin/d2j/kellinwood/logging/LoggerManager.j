.bytecode 50.0
.class public synchronized kellinwood/logging/LoggerManager
.super java/lang/Object

.field static 'factory' Lkellinwood/logging/LoggerFactory;

.field static 'loggers' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Lkellinwood/logging/LoggerInterface;>;"

.method static <clinit>()V
new kellinwood/logging/NullLoggerFactory
dup
invokespecial kellinwood/logging/NullLoggerFactory/<init>()V
putstatic kellinwood/logging/LoggerManager/factory Lkellinwood/logging/LoggerFactory;
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
putstatic kellinwood/logging/LoggerManager/loggers Ljava/util/Map;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
getstatic kellinwood/logging/LoggerManager/loggers Ljava/util/Map;
aload 0
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/logging/LoggerInterface
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
getstatic kellinwood/logging/LoggerManager/factory Lkellinwood/logging/LoggerFactory;
aload 0
invokeinterface kellinwood/logging/LoggerFactory/getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface; 1
astore 1
getstatic kellinwood/logging/LoggerManager/loggers Ljava/util/Map;
aload 0
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L0:
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method public static setLoggerFactory(Lkellinwood/logging/LoggerFactory;)V
aload 0
putstatic kellinwood/logging/LoggerManager/factory Lkellinwood/logging/LoggerFactory;
return
.limit locals 1
.limit stack 1
.end method
