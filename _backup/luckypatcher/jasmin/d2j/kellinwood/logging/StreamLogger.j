.bytecode 50.0
.class public synchronized kellinwood/logging/StreamLogger
.super kellinwood/logging/AbstractLogger

.field 'out' Ljava/io/PrintStream;

.method public <init>(Ljava/lang/String;Ljava/io/PrintStream;)V
aload 0
aload 1
invokespecial kellinwood/logging/AbstractLogger/<init>(Ljava/lang/String;)V
aload 0
aload 2
putfield kellinwood/logging/StreamLogger/out Ljava/io/PrintStream;
return
.limit locals 3
.limit stack 2
.end method

.method protected write(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
getfield kellinwood/logging/StreamLogger/out Ljava/io/PrintStream;
aload 0
aload 1
aload 2
invokevirtual kellinwood/logging/StreamLogger/format(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
aload 3
ifnull L0
aload 3
aload 0
getfield kellinwood/logging/StreamLogger/out Ljava/io/PrintStream;
invokevirtual java/lang/Throwable/printStackTrace(Ljava/io/PrintStream;)V
L0:
return
.limit locals 4
.limit stack 4
.end method
