.bytecode 50.0
.class public abstract interface kellinwood/logging/LoggerInterface
.super java/lang/Object

.field public static final 'DEBUG' Ljava/lang/String; = "DEBUG"

.field public static final 'ERROR' Ljava/lang/String; = "ERROR"

.field public static final 'INFO' Ljava/lang/String; = "INFO"

.field public static final 'WARNING' Ljava/lang/String; = "WARNING"

.method public abstract debug(Ljava/lang/String;)V
.end method

.method public abstract debug(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract error(Ljava/lang/String;)V
.end method

.method public abstract error(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract info(Ljava/lang/String;)V
.end method

.method public abstract info(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract isDebugEnabled()Z
.end method

.method public abstract isErrorEnabled()Z
.end method

.method public abstract isInfoEnabled()Z
.end method

.method public abstract isWarningEnabled()Z
.end method

.method public abstract warning(Ljava/lang/String;)V
.end method

.method public abstract warning(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method
