.bytecode 50.0
.class public synchronized kellinwood/logging/log4j/Log4jLogger
.super java/lang/Object
.implements kellinwood/logging/LoggerInterface

.field 'log' Lorg/apache/log4j/Logger;

.method public <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic org/apache/log4j/Logger/getLogger(Ljava/lang/String;)Lorg/apache/log4j/Logger;
putfield kellinwood/logging/log4j/Log4jLogger/log Lorg/apache/log4j/Logger;
return
.limit locals 2
.limit stack 2
.end method

.method public debug(Ljava/lang/String;)V
aload 0
getfield kellinwood/logging/log4j/Log4jLogger/log Lorg/apache/log4j/Logger;
aload 1
invokevirtual org/apache/log4j/Logger/debug(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method public debug(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
getfield kellinwood/logging/log4j/Log4jLogger/log Lorg/apache/log4j/Logger;
aload 1
aload 2
invokevirtual org/apache/log4j/Logger/debug(Ljava/lang/Object;Ljava/lang/Throwable;)V
return
.limit locals 3
.limit stack 3
.end method

.method public error(Ljava/lang/String;)V
aload 0
getfield kellinwood/logging/log4j/Log4jLogger/log Lorg/apache/log4j/Logger;
aload 1
invokevirtual org/apache/log4j/Logger/error(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method public error(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
getfield kellinwood/logging/log4j/Log4jLogger/log Lorg/apache/log4j/Logger;
aload 1
aload 2
invokevirtual org/apache/log4j/Logger/error(Ljava/lang/Object;Ljava/lang/Throwable;)V
return
.limit locals 3
.limit stack 3
.end method

.method public info(Ljava/lang/String;)V
aload 0
getfield kellinwood/logging/log4j/Log4jLogger/log Lorg/apache/log4j/Logger;
aload 1
invokevirtual org/apache/log4j/Logger/info(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method public info(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
getfield kellinwood/logging/log4j/Log4jLogger/log Lorg/apache/log4j/Logger;
aload 1
aload 2
invokevirtual org/apache/log4j/Logger/info(Ljava/lang/Object;Ljava/lang/Throwable;)V
return
.limit locals 3
.limit stack 3
.end method

.method public isDebugEnabled()Z
aload 0
getfield kellinwood/logging/log4j/Log4jLogger/log Lorg/apache/log4j/Logger;
invokevirtual org/apache/log4j/Logger/isDebugEnabled()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isErrorEnabled()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isInfoEnabled()Z
aload 0
getfield kellinwood/logging/log4j/Log4jLogger/log Lorg/apache/log4j/Logger;
invokevirtual org/apache/log4j/Logger/isInfoEnabled()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isWarningEnabled()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public warning(Ljava/lang/String;)V
aload 0
getfield kellinwood/logging/log4j/Log4jLogger/log Lorg/apache/log4j/Logger;
aload 1
invokevirtual org/apache/log4j/Logger/warn(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method public warning(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
getfield kellinwood/logging/log4j/Log4jLogger/log Lorg/apache/log4j/Logger;
aload 1
aload 2
invokevirtual org/apache/log4j/Logger/warn(Ljava/lang/Object;Ljava/lang/Throwable;)V
return
.limit locals 3
.limit stack 3
.end method
