.bytecode 50.0
.class public synchronized kellinwood/zipio/ZipListingHelper
.super java/lang/Object

.field static 'dateFormat' Ljava/text/DateFormat;

.method static <clinit>()V
new java/text/SimpleDateFormat
dup
ldc "MM-dd-yy HH:mm"
invokespecial java/text/SimpleDateFormat/<init>(Ljava/lang/String;)V
putstatic kellinwood/zipio/ZipListingHelper/dateFormat Ljava/text/DateFormat;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static listEntry(Lkellinwood/logging/LoggerInterface;Lkellinwood/zipio/ZioEntry;)V
iconst_0
istore 2
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getSize()I
ifle L0
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getSize()I
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getCompressedSize()I
isub
bipush 100
imul
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getSize()I
idiv
istore 2
L0:
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getSize()I
istore 3
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getCompression()S
ifne L1
ldc "Stored"
astore 4
L2:
aload 0
ldc "%8d  %6s %8d %4d%% %s  %08x  %s"
bipush 7
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 4
aastore
dup
iconst_2
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getCompressedSize()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_3
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_4
getstatic kellinwood/zipio/ZipListingHelper/dateFormat Ljava/text/DateFormat;
new java/util/Date
dup
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getTime()J
invokespecial java/util/Date/<init>(J)V
invokevirtual java/text/DateFormat/format(Ljava/util/Date;)Ljava/lang/String;
aastore
dup
iconst_5
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getCrc32()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
bipush 6
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
return
L1:
ldc "Defl:N"
astore 4
goto L2
.limit locals 5
.limit stack 10
.end method

.method public static listHeader(Lkellinwood/logging/LoggerInterface;)V
aload 0
ldc " Length   Method    Size  Ratio   Date   Time   CRC-32    Name"
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
aload 0
ldc "--------  ------  ------- -----   ----   ----   ------    ----"
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
return
.limit locals 1
.limit stack 2
.end method
