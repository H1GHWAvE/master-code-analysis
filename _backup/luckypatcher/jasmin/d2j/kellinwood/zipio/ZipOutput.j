.bytecode 50.0
.class public synchronized kellinwood/zipio/ZipOutput
.super java/lang/Object

.field static 'log' Lkellinwood/logging/LoggerInterface;

.field 'entriesWritten' Ljava/util/List; signature "Ljava/util/List<Lkellinwood/zipio/ZioEntry;>;"

.field 'filePointer' I

.field 'namesWritten' Ljava/util/Set; signature "Ljava/util/Set<Ljava/lang/String;>;"

.field 'out' Ljava/io/OutputStream;

.field 'outputFilename' Ljava/lang/String;

.method public <init>(Ljava/io/File;)V
.throws java/io/IOException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield kellinwood/zipio/ZipOutput/out Ljava/io/OutputStream;
aload 0
iconst_0
putfield kellinwood/zipio/ZipOutput/filePointer I
aload 0
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
putfield kellinwood/zipio/ZipOutput/entriesWritten Ljava/util/List;
aload 0
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
putfield kellinwood/zipio/ZipOutput/namesWritten Ljava/util/Set;
aload 0
aload 1
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
putfield kellinwood/zipio/ZipOutput/outputFilename Ljava/lang/String;
aload 0
aload 1
invokespecial kellinwood/zipio/ZipOutput/init(Ljava/io/File;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/io/OutputStream;)V
.throws java/io/IOException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield kellinwood/zipio/ZipOutput/out Ljava/io/OutputStream;
aload 0
iconst_0
putfield kellinwood/zipio/ZipOutput/filePointer I
aload 0
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
putfield kellinwood/zipio/ZipOutput/entriesWritten Ljava/util/List;
aload 0
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
putfield kellinwood/zipio/ZipOutput/namesWritten Ljava/util/Set;
aload 0
aload 1
putfield kellinwood/zipio/ZipOutput/out Ljava/io/OutputStream;
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/lang/String;)V
.throws java/io/IOException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield kellinwood/zipio/ZipOutput/out Ljava/io/OutputStream;
aload 0
iconst_0
putfield kellinwood/zipio/ZipOutput/filePointer I
aload 0
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
putfield kellinwood/zipio/ZipOutput/entriesWritten Ljava/util/List;
aload 0
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
putfield kellinwood/zipio/ZipOutput/namesWritten Ljava/util/Set;
aload 0
aload 1
putfield kellinwood/zipio/ZipOutput/outputFilename Ljava/lang/String;
aload 0
new java/io/File
dup
aload 0
getfield kellinwood/zipio/ZipOutput/outputFilename Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokespecial kellinwood/zipio/ZipOutput/init(Ljava/io/File;)V
return
.limit locals 2
.limit stack 4
.end method

.method private static getLogger()Lkellinwood/logging/LoggerInterface;
getstatic kellinwood/zipio/ZipOutput/log Lkellinwood/logging/LoggerInterface;
ifnonnull L0
ldc kellinwood/zipio/ZipOutput
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic kellinwood/logging/LoggerManager/getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
putstatic kellinwood/zipio/ZipOutput/log Lkellinwood/logging/LoggerInterface;
L0:
getstatic kellinwood/zipio/ZipOutput/log Lkellinwood/logging/LoggerInterface;
areturn
.limit locals 0
.limit stack 1
.end method

.method private init(Ljava/io/File;)V
.throws java/io/IOException
aload 1
invokevirtual java/io/File/exists()Z
ifeq L0
aload 1
invokevirtual java/io/File/delete()Z
pop
L0:
aload 0
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
putfield kellinwood/zipio/ZipOutput/out Ljava/io/OutputStream;
invokestatic kellinwood/zipio/ZipOutput/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
ifeq L1
invokestatic kellinwood/zipio/ZipOutput/getLogger()Lkellinwood/logging/LoggerInterface;
invokestatic kellinwood/zipio/ZipListingHelper/listHeader(Lkellinwood/logging/LoggerInterface;)V
L1:
return
.limit locals 2
.limit stack 4
.end method

.method public close()V
.throws java/io/IOException
.catch java/lang/Throwable from L0 to L1 using L2
new kellinwood/zipio/CentralEnd
dup
invokespecial kellinwood/zipio/CentralEnd/<init>()V
astore 2
aload 2
aload 0
invokevirtual kellinwood/zipio/ZipOutput/getFilePointer()I
putfield kellinwood/zipio/CentralEnd/centralStartOffset I
aload 0
getfield kellinwood/zipio/ZipOutput/entriesWritten Ljava/util/List;
invokeinterface java/util/List/size()I 0
i2s
istore 1
aload 2
iload 1
putfield kellinwood/zipio/CentralEnd/totalCentralEntries S
aload 2
iload 1
putfield kellinwood/zipio/CentralEnd/numCentralEntries S
aload 0
getfield kellinwood/zipio/ZipOutput/entriesWritten Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L3:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast kellinwood/zipio/ZioEntry
aload 0
invokevirtual kellinwood/zipio/ZioEntry/write(Lkellinwood/zipio/ZipOutput;)V
goto L3
L4:
aload 2
aload 0
invokevirtual kellinwood/zipio/ZipOutput/getFilePointer()I
aload 2
getfield kellinwood/zipio/CentralEnd/centralStartOffset I
isub
putfield kellinwood/zipio/CentralEnd/centralDirectorySize I
aload 2
ldc ""
putfield kellinwood/zipio/CentralEnd/fileComment Ljava/lang/String;
aload 2
aload 0
invokevirtual kellinwood/zipio/CentralEnd/write(Lkellinwood/zipio/ZipOutput;)V
aload 0
getfield kellinwood/zipio/ZipOutput/out Ljava/io/OutputStream;
ifnull L1
L0:
aload 0
getfield kellinwood/zipio/ZipOutput/out Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/close()V
L1:
return
L2:
astore 2
return
.limit locals 4
.limit stack 3
.end method

.method public getFilePointer()I
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZipOutput/filePointer I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public write(Lkellinwood/zipio/ZioEntry;)V
.throws java/io/IOException
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
astore 2
aload 0
getfield kellinwood/zipio/ZipOutput/namesWritten Ljava/util/Set;
aload 2
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifeq L0
invokestatic kellinwood/zipio/ZipOutput/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Skipping duplicate file in output: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
L1:
return
L0:
aload 1
aload 0
invokevirtual kellinwood/zipio/ZioEntry/writeLocalEntry(Lkellinwood/zipio/ZipOutput;)V
aload 0
getfield kellinwood/zipio/ZipOutput/entriesWritten Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield kellinwood/zipio/ZipOutput/namesWritten Ljava/util/Set;
aload 2
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
invokestatic kellinwood/zipio/ZipOutput/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
ifeq L1
invokestatic kellinwood/zipio/ZipOutput/getLogger()Lkellinwood/logging/LoggerInterface;
aload 1
invokestatic kellinwood/zipio/ZipListingHelper/listEntry(Lkellinwood/logging/LoggerInterface;Lkellinwood/zipio/ZioEntry;)V
return
.limit locals 3
.limit stack 3
.end method

.method public writeBytes([B)V
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZipOutput/out Ljava/io/OutputStream;
aload 1
invokevirtual java/io/OutputStream/write([B)V
aload 0
aload 0
getfield kellinwood/zipio/ZipOutput/filePointer I
aload 1
arraylength
iadd
putfield kellinwood/zipio/ZipOutput/filePointer I
return
.limit locals 2
.limit stack 3
.end method

.method public writeBytes([BII)V
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZipOutput/out Ljava/io/OutputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/OutputStream/write([BII)V
aload 0
aload 0
getfield kellinwood/zipio/ZipOutput/filePointer I
iload 3
iadd
putfield kellinwood/zipio/ZipOutput/filePointer I
return
.limit locals 4
.limit stack 4
.end method

.method public writeInt(I)V
.throws java/io/IOException
iconst_4
newarray byte
astore 4
iconst_0
istore 3
iload 1
istore 2
iload 3
istore 1
L0:
iload 1
iconst_4
if_icmpge L1
aload 4
iload 1
iload 2
sipush 255
iand
i2b
bastore
iload 2
bipush 8
ishr
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
getfield kellinwood/zipio/ZipOutput/out Ljava/io/OutputStream;
aload 4
invokevirtual java/io/OutputStream/write([B)V
aload 0
aload 0
getfield kellinwood/zipio/ZipOutput/filePointer I
iconst_4
iadd
putfield kellinwood/zipio/ZipOutput/filePointer I
return
.limit locals 5
.limit stack 4
.end method

.method public writeShort(S)V
.throws java/io/IOException
iconst_2
newarray byte
astore 4
iconst_0
istore 3
iload 1
istore 2
iload 3
istore 1
L0:
iload 1
iconst_2
if_icmpge L1
aload 4
iload 1
iload 2
sipush 255
iand
i2b
bastore
iload 2
bipush 8
ishr
i2s
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
getfield kellinwood/zipio/ZipOutput/out Ljava/io/OutputStream;
aload 4
invokevirtual java/io/OutputStream/write([B)V
aload 0
aload 0
getfield kellinwood/zipio/ZipOutput/filePointer I
iconst_2
iadd
putfield kellinwood/zipio/ZipOutput/filePointer I
return
.limit locals 5
.limit stack 4
.end method

.method public writeString(Ljava/lang/String;)V
.throws java/io/IOException
aload 1
invokevirtual java/lang/String/getBytes()[B
astore 1
aload 0
getfield kellinwood/zipio/ZipOutput/out Ljava/io/OutputStream;
aload 1
invokevirtual java/io/OutputStream/write([B)V
aload 0
aload 0
getfield kellinwood/zipio/ZipOutput/filePointer I
aload 1
arraylength
iadd
putfield kellinwood/zipio/ZipOutput/filePointer I
return
.limit locals 2
.limit stack 3
.end method
