.bytecode 50.0
.class public synchronized kellinwood/zipio/ZioEntry
.super java/lang/Object
.implements java/lang/Cloneable

.field private static 'alignBytes' [B

.field private static 'log' Lkellinwood/logging/LoggerInterface;

.field private 'compressedSize' I

.field private 'compression' S

.field private 'crc32' I

.field private 'data' [B

.field private 'dataPosition' J

.field private 'diskNumberStart' S

.field private 'entryOut' Lkellinwood/zipio/ZioEntryOutputStream;

.field private 'externalAttributes' I

.field private 'extraData' [B

.field private 'fileComment' Ljava/lang/String;

.field private 'filename' Ljava/lang/String;

.field private 'generalPurposeBits' S

.field private 'internalAttributes' S

.field private 'localHeaderOffset' I

.field private 'modificationDate' S

.field private 'modificationTime' S

.field private 'numAlignBytes' S

.field private 'size' I

.field private 'versionMadeBy' S

.field private 'versionRequired' S

.field private 'zipInput' Lkellinwood/zipio/ZipInput;

.method static <clinit>()V
iconst_4
newarray byte
putstatic kellinwood/zipio/ZioEntry/alignBytes [B
return
.limit locals 0
.limit stack 1
.end method

.method public <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntry/numAlignBytes S
aload 0
ldc2_w -1L
putfield kellinwood/zipio/ZioEntry/dataPosition J
aload 0
aconst_null
putfield kellinwood/zipio/ZioEntry/data [B
aload 0
aconst_null
putfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
aload 0
aload 1
putfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
aload 0
ldc ""
putfield kellinwood/zipio/ZioEntry/fileComment Ljava/lang/String;
aload 0
bipush 8
putfield kellinwood/zipio/ZioEntry/compression S
aload 0
iconst_0
newarray byte
putfield kellinwood/zipio/ZioEntry/extraData [B
aload 0
invokestatic java/lang/System/currentTimeMillis()J
invokevirtual kellinwood/zipio/ZioEntry/setTime(J)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/String;)V
.throws java/io/IOException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntry/numAlignBytes S
aload 0
ldc2_w -1L
putfield kellinwood/zipio/ZioEntry/dataPosition J
aload 0
aconst_null
putfield kellinwood/zipio/ZioEntry/data [B
aload 0
aconst_null
putfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
aload 0
new kellinwood/zipio/ZipInput
dup
aload 2
invokespecial kellinwood/zipio/ZipInput/<init>(Ljava/lang/String;)V
putfield kellinwood/zipio/ZioEntry/zipInput Lkellinwood/zipio/ZipInput;
aload 0
aload 1
putfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
aload 0
ldc ""
putfield kellinwood/zipio/ZioEntry/fileComment Ljava/lang/String;
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntry/compression S
aload 0
aload 0
getfield kellinwood/zipio/ZioEntry/zipInput Lkellinwood/zipio/ZipInput;
invokevirtual kellinwood/zipio/ZipInput/getFileLength()J
l2i
putfield kellinwood/zipio/ZioEntry/size I
aload 0
aload 0
getfield kellinwood/zipio/ZioEntry/size I
putfield kellinwood/zipio/ZioEntry/compressedSize I
invokestatic kellinwood/zipio/ZioEntry/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
ifeq L0
invokestatic kellinwood/zipio/ZioEntry/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Computing CRC for %s, size=%d"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
dup
iconst_1
aload 0
getfield kellinwood/zipio/ZioEntry/size I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L0:
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 1
sipush 8096
newarray byte
astore 5
iconst_0
istore 3
L1:
iload 3
aload 0
getfield kellinwood/zipio/ZioEntry/size I
if_icmpeq L2
aload 0
getfield kellinwood/zipio/ZioEntry/zipInput Lkellinwood/zipio/ZipInput;
aload 5
iconst_0
aload 5
arraylength
aload 0
getfield kellinwood/zipio/ZioEntry/size I
iload 3
isub
invokestatic java/lang/Math/min(II)I
invokevirtual kellinwood/zipio/ZipInput/read([BII)I
istore 4
iload 4
ifle L1
aload 1
aload 5
iconst_0
iload 4
invokevirtual java/util/zip/CRC32/update([BII)V
iload 3
iload 4
iadd
istore 3
goto L1
L2:
aload 0
aload 1
invokevirtual java/util/zip/CRC32/getValue()J
l2i
putfield kellinwood/zipio/ZioEntry/crc32 I
aload 0
getfield kellinwood/zipio/ZioEntry/zipInput Lkellinwood/zipio/ZipInput;
lconst_0
invokevirtual kellinwood/zipio/ZipInput/seek(J)V
aload 0
lconst_0
putfield kellinwood/zipio/ZioEntry/dataPosition J
aload 0
iconst_0
newarray byte
putfield kellinwood/zipio/ZioEntry/extraData [B
aload 0
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/lastModified()J
invokevirtual kellinwood/zipio/ZioEntry/setTime(J)V
return
.limit locals 6
.limit stack 6
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/String;SIII)V
.throws java/io/IOException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntry/numAlignBytes S
aload 0
ldc2_w -1L
putfield kellinwood/zipio/ZioEntry/dataPosition J
aload 0
aconst_null
putfield kellinwood/zipio/ZioEntry/data [B
aload 0
aconst_null
putfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
aload 0
new kellinwood/zipio/ZipInput
dup
aload 2
invokespecial kellinwood/zipio/ZipInput/<init>(Ljava/lang/String;)V
putfield kellinwood/zipio/ZioEntry/zipInput Lkellinwood/zipio/ZipInput;
aload 0
aload 1
putfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
aload 0
ldc ""
putfield kellinwood/zipio/ZioEntry/fileComment Ljava/lang/String;
aload 0
iload 3
putfield kellinwood/zipio/ZioEntry/compression S
aload 0
iload 4
putfield kellinwood/zipio/ZioEntry/crc32 I
aload 0
iload 5
putfield kellinwood/zipio/ZioEntry/compressedSize I
aload 0
iload 6
putfield kellinwood/zipio/ZioEntry/size I
aload 0
lconst_0
putfield kellinwood/zipio/ZioEntry/dataPosition J
aload 0
iconst_0
newarray byte
putfield kellinwood/zipio/ZioEntry/extraData [B
aload 0
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/lastModified()J
invokevirtual kellinwood/zipio/ZioEntry/setTime(J)V
return
.limit locals 7
.limit stack 4
.end method

.method public <init>(Lkellinwood/zipio/ZipInput;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntry/numAlignBytes S
aload 0
ldc2_w -1L
putfield kellinwood/zipio/ZioEntry/dataPosition J
aload 0
aconst_null
putfield kellinwood/zipio/ZioEntry/data [B
aload 0
aconst_null
putfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
aload 0
aload 1
putfield kellinwood/zipio/ZioEntry/zipInput Lkellinwood/zipio/ZipInput;
return
.limit locals 2
.limit stack 3
.end method

.method private doRead(Lkellinwood/zipio/ZipInput;)V
.throws java/io/IOException
invokestatic kellinwood/zipio/ZioEntry/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
istore 5
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
putfield kellinwood/zipio/ZioEntry/versionMadeBy S
iload 5
ifeq L0
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Version made by: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/versionMadeBy S
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L0:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
putfield kellinwood/zipio/ZioEntry/versionRequired S
iload 5
ifeq L1
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Version required: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/versionRequired S
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L1:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
putfield kellinwood/zipio/ZioEntry/generalPurposeBits S
iload 5
ifeq L2
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "General purpose bits: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/generalPurposeBits S
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L2:
aload 0
getfield kellinwood/zipio/ZioEntry/generalPurposeBits S
ldc_w 63473
iand
ifeq L3
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Can't handle general purpose bits == "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/generalPurposeBits S
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
putfield kellinwood/zipio/ZioEntry/compression S
iload 5
ifeq L4
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Compression: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/compression S
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L4:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
putfield kellinwood/zipio/ZioEntry/modificationTime S
iload 5
ifeq L5
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Modification time: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/modificationTime S
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L5:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
putfield kellinwood/zipio/ZioEntry/modificationDate S
iload 5
ifeq L6
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Modification date: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/modificationDate S
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L6:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readInt()I
putfield kellinwood/zipio/ZioEntry/crc32 I
iload 5
ifeq L7
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "CRC-32: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/crc32 I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L7:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readInt()I
putfield kellinwood/zipio/ZioEntry/compressedSize I
iload 5
ifeq L8
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Compressed size: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/compressedSize I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L8:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readInt()I
putfield kellinwood/zipio/ZioEntry/size I
iload 5
ifeq L9
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Size: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/size I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L9:
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
istore 2
iload 5
ifeq L10
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "File name length: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L10:
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
istore 3
iload 5
ifeq L11
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Extra length: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L11:
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
istore 4
iload 5
ifeq L12
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "File comment length: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 4
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L12:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
putfield kellinwood/zipio/ZioEntry/diskNumberStart S
iload 5
ifeq L13
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Disk number start: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/diskNumberStart S
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L13:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
putfield kellinwood/zipio/ZioEntry/internalAttributes S
iload 5
ifeq L14
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Internal attributes: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/internalAttributes S
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L14:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readInt()I
putfield kellinwood/zipio/ZioEntry/externalAttributes I
iload 5
ifeq L15
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "External attributes: 0x%08x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/externalAttributes I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L15:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readInt()I
putfield kellinwood/zipio/ZioEntry/localHeaderOffset I
iload 5
ifeq L16
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Local header offset: 0x%08x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/localHeaderOffset I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L16:
aload 0
aload 1
iload 2
invokevirtual kellinwood/zipio/ZipInput/readString(I)Ljava/lang/String;
putfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
iload 5
ifeq L17
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Filename: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L17:
aload 0
aload 1
iload 3
invokevirtual kellinwood/zipio/ZipInput/readBytes(I)[B
putfield kellinwood/zipio/ZioEntry/extraData [B
aload 0
aload 1
iload 4
invokevirtual kellinwood/zipio/ZipInput/readString(I)Ljava/lang/String;
putfield kellinwood/zipio/ZioEntry/fileComment Ljava/lang/String;
iload 5
ifeq L18
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "File comment: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield kellinwood/zipio/ZioEntry/fileComment Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L18:
aload 0
aload 0
getfield kellinwood/zipio/ZioEntry/generalPurposeBits S
sipush 2048
iand
i2s
putfield kellinwood/zipio/ZioEntry/generalPurposeBits S
aload 0
getfield kellinwood/zipio/ZioEntry/size I
ifne L19
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntry/compressedSize I
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntry/compression S
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntry/crc32 I
L19:
return
.limit locals 6
.limit stack 8
.end method

.method public static getLogger()Lkellinwood/logging/LoggerInterface;
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ifnonnull L0
ldc kellinwood/zipio/ZioEntry
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic kellinwood/logging/LoggerManager/getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
putstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
L0:
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static read(Lkellinwood/zipio/ZipInput;)Lkellinwood/zipio/ZioEntry;
.throws java/io/IOException
aload 0
invokevirtual kellinwood/zipio/ZipInput/readInt()I
ldc_w 33639248
if_icmpeq L0
aload 0
aload 0
invokevirtual kellinwood/zipio/ZipInput/getFilePointer()J
ldc2_w 4L
lsub
invokevirtual kellinwood/zipio/ZipInput/seek(J)V
aconst_null
areturn
L0:
new kellinwood/zipio/ZioEntry
dup
aload 0
invokespecial kellinwood/zipio/ZioEntry/<init>(Lkellinwood/zipio/ZipInput;)V
astore 1
aload 1
aload 0
invokespecial kellinwood/zipio/ZioEntry/doRead(Lkellinwood/zipio/ZipInput;)V
aload 1
areturn
.limit locals 2
.limit stack 5
.end method

.method public getClonedEntry(Ljava/lang/String;)Lkellinwood/zipio/ZioEntry;
.catch java/lang/CloneNotSupportedException from L0 to L1 using L2
L0:
aload 0
invokevirtual java/lang/Object/clone()Ljava/lang/Object;
checkcast kellinwood/zipio/ZioEntry
astore 2
L1:
aload 2
aload 1
invokevirtual kellinwood/zipio/ZioEntry/setName(Ljava/lang/String;)V
aload 2
areturn
L2:
astore 1
new java/lang/IllegalStateException
dup
ldc "clone() failed!"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public getCompressedSize()I
aload 0
getfield kellinwood/zipio/ZioEntry/compressedSize I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getCompression()S
aload 0
getfield kellinwood/zipio/ZioEntry/compression S
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getCrc32()I
aload 0
getfield kellinwood/zipio/ZioEntry/crc32 I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getData()[B
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZioEntry/data [B
ifnull L0
aload 0
getfield kellinwood/zipio/ZioEntry/data [B
astore 3
L1:
aload 3
areturn
L0:
aload 0
getfield kellinwood/zipio/ZioEntry/size I
newarray byte
astore 4
aload 0
invokevirtual kellinwood/zipio/ZioEntry/getInputStream()Ljava/io/InputStream;
astore 5
iconst_0
istore 1
L2:
aload 4
astore 3
iload 1
aload 0
getfield kellinwood/zipio/ZioEntry/size I
if_icmpeq L1
aload 5
aload 4
iload 1
aload 0
getfield kellinwood/zipio/ZioEntry/size I
iload 1
isub
invokevirtual java/io/InputStream/read([BII)I
istore 2
iload 2
ifge L3
new java/lang/IllegalStateException
dup
ldc "Read failed, expecting %d bytes, got %d instead"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/size I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L3:
iload 1
iload 2
iadd
istore 1
goto L2
.limit locals 6
.limit stack 7
.end method

.method public getDataPosition()J
aload 0
getfield kellinwood/zipio/ZioEntry/dataPosition J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getDiskNumberStart()S
aload 0
getfield kellinwood/zipio/ZioEntry/diskNumberStart S
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getEntryOut()Lkellinwood/zipio/ZioEntryOutputStream;
aload 0
getfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getExternalAttributes()I
aload 0
getfield kellinwood/zipio/ZioEntry/externalAttributes I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getExtraData()[B
aload 0
getfield kellinwood/zipio/ZioEntry/extraData [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getFileComment()Ljava/lang/String;
aload 0
getfield kellinwood/zipio/ZioEntry/fileComment Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getGeneralPurposeBits()S
aload 0
getfield kellinwood/zipio/ZioEntry/generalPurposeBits S
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getInputStream()Ljava/io/InputStream;
.throws java/io/IOException
aload 0
aconst_null
invokevirtual kellinwood/zipio/ZioEntry/getInputStream(Ljava/io/OutputStream;)Ljava/io/InputStream;
areturn
.limit locals 1
.limit stack 2
.end method

.method public getInputStream(Ljava/io/OutputStream;)Ljava/io/InputStream;
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
ifnull L0
aload 0
getfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
invokevirtual kellinwood/zipio/ZioEntryOutputStream/close()V
aload 0
aload 0
getfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
invokevirtual kellinwood/zipio/ZioEntryOutputStream/getSize()I
putfield kellinwood/zipio/ZioEntry/size I
aload 0
aload 0
getfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
invokevirtual kellinwood/zipio/ZioEntryOutputStream/getWrappedStream()Ljava/io/OutputStream;
checkcast java/io/ByteArrayOutputStream
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
putfield kellinwood/zipio/ZioEntry/data [B
aload 0
aload 0
getfield kellinwood/zipio/ZioEntry/data [B
arraylength
putfield kellinwood/zipio/ZioEntry/compressedSize I
aload 0
aload 0
getfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
invokevirtual kellinwood/zipio/ZioEntryOutputStream/getCRC()I
putfield kellinwood/zipio/ZioEntry/crc32 I
aload 0
aconst_null
putfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
new java/io/ByteArrayInputStream
dup
aload 0
getfield kellinwood/zipio/ZioEntry/data [B
invokespecial java/io/ByteArrayInputStream/<init>([B)V
astore 1
aload 0
getfield kellinwood/zipio/ZioEntry/compression S
ifne L1
aload 1
areturn
L1:
new java/util/zip/InflaterInputStream
dup
new java/io/SequenceInputStream
dup
aload 1
new java/io/ByteArrayInputStream
dup
iconst_1
newarray byte
invokespecial java/io/ByteArrayInputStream/<init>([B)V
invokespecial java/io/SequenceInputStream/<init>(Ljava/io/InputStream;Ljava/io/InputStream;)V
new java/util/zip/Inflater
dup
iconst_1
invokespecial java/util/zip/Inflater/<init>(Z)V
invokespecial java/util/zip/InflaterInputStream/<init>(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V
areturn
L0:
new kellinwood/zipio/ZioEntryInputStream
dup
aload 0
invokespecial kellinwood/zipio/ZioEntryInputStream/<init>(Lkellinwood/zipio/ZioEntry;)V
astore 2
aload 1
ifnull L2
aload 2
aload 1
invokevirtual kellinwood/zipio/ZioEntryInputStream/setMonitorStream(Ljava/io/OutputStream;)V
L2:
aload 0
getfield kellinwood/zipio/ZioEntry/compression S
ifeq L3
aload 2
iconst_1
invokevirtual kellinwood/zipio/ZioEntryInputStream/setReturnDummyByte(Z)V
new java/util/zip/InflaterInputStream
dup
aload 2
new java/util/zip/Inflater
dup
iconst_1
invokespecial java/util/zip/Inflater/<init>(Z)V
invokespecial java/util/zip/InflaterInputStream/<init>(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V
areturn
L3:
aload 2
areturn
.limit locals 3
.limit stack 8
.end method

.method public getInternalAttributes()S
aload 0
getfield kellinwood/zipio/ZioEntry/internalAttributes S
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getLocalHeaderOffset()I
aload 0
getfield kellinwood/zipio/ZioEntry/localHeaderOffset I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getName()Ljava/lang/String;
aload 0
getfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getOutputStream()Ljava/io/OutputStream;
aload 0
new kellinwood/zipio/ZioEntryOutputStream
dup
aload 0
getfield kellinwood/zipio/ZioEntry/compression S
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
invokespecial kellinwood/zipio/ZioEntryOutputStream/<init>(ILjava/io/OutputStream;)V
putfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
aload 0
getfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
areturn
.limit locals 1
.limit stack 6
.end method

.method public getSize()I
aload 0
getfield kellinwood/zipio/ZioEntry/size I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getTime()J
new java/util/Date
dup
aload 0
getfield kellinwood/zipio/ZioEntry/modificationDate S
bipush 9
ishr
bipush 127
iand
bipush 80
iadd
aload 0
getfield kellinwood/zipio/ZioEntry/modificationDate S
iconst_5
ishr
bipush 15
iand
iconst_1
isub
aload 0
getfield kellinwood/zipio/ZioEntry/modificationDate S
bipush 31
iand
aload 0
getfield kellinwood/zipio/ZioEntry/modificationTime S
bipush 11
ishr
bipush 31
iand
aload 0
getfield kellinwood/zipio/ZioEntry/modificationTime S
iconst_5
ishr
bipush 63
iand
aload 0
getfield kellinwood/zipio/ZioEntry/modificationTime S
iconst_1
ishl
bipush 62
iand
invokespecial java/util/Date/<init>(IIIIII)V
invokevirtual java/util/Date/getTime()J
lreturn
.limit locals 1
.limit stack 9
.end method

.method public getVersionMadeBy()S
aload 0
getfield kellinwood/zipio/ZioEntry/versionMadeBy S
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getVersionRequired()S
aload 0
getfield kellinwood/zipio/ZioEntry/versionRequired S
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getZipInput()Lkellinwood/zipio/ZipInput;
aload 0
getfield kellinwood/zipio/ZioEntry/zipInput Lkellinwood/zipio/ZipInput;
areturn
.limit locals 1
.limit stack 1
.end method

.method public isDirectory()Z
aload 0
getfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public readLocalHeader()V
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZioEntry/zipInput Lkellinwood/zipio/ZipInput;
astore 5
invokestatic kellinwood/zipio/ZioEntry/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
istore 4
aload 5
aload 0
getfield kellinwood/zipio/ZioEntry/localHeaderOffset I
i2l
invokevirtual kellinwood/zipio/ZipInput/seek(J)V
iload 4
ifeq L0
invokestatic kellinwood/zipio/ZioEntry/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "FILE POSITION: 0x%08x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 5
invokevirtual kellinwood/zipio/ZipInput/getFilePointer()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L0:
aload 5
invokevirtual kellinwood/zipio/ZipInput/readInt()I
ldc_w 67324752
if_icmpeq L1
new java/lang/IllegalStateException
dup
ldc "Local header not found at pos=0x%08x, file=%s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 5
invokevirtual kellinwood/zipio/ZipInput/getFilePointer()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
dup
iconst_1
aload 0
getfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 5
invokevirtual kellinwood/zipio/ZipInput/readShort()S
istore 1
iload 4
ifeq L2
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Version required: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L2:
aload 5
invokevirtual kellinwood/zipio/ZipInput/readShort()S
istore 1
iload 4
ifeq L3
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "General purpose bits: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L3:
aload 5
invokevirtual kellinwood/zipio/ZipInput/readShort()S
istore 1
iload 4
ifeq L4
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Compression: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L4:
aload 5
invokevirtual kellinwood/zipio/ZipInput/readShort()S
istore 1
iload 4
ifeq L5
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Modification time: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L5:
aload 5
invokevirtual kellinwood/zipio/ZipInput/readShort()S
istore 1
iload 4
ifeq L6
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Modification date: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L6:
aload 5
invokevirtual kellinwood/zipio/ZipInput/readInt()I
istore 3
iload 4
ifeq L7
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "CRC-32: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L7:
aload 5
invokevirtual kellinwood/zipio/ZipInput/readInt()I
istore 3
iload 4
ifeq L8
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Compressed size: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L8:
aload 5
invokevirtual kellinwood/zipio/ZipInput/readInt()I
istore 3
iload 4
ifeq L9
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Size: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L9:
aload 5
invokevirtual kellinwood/zipio/ZipInput/readShort()S
istore 1
iload 4
ifeq L10
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "File name length: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L10:
aload 5
invokevirtual kellinwood/zipio/ZipInput/readShort()S
istore 2
iload 4
ifeq L11
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Extra length: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L11:
aload 5
iload 1
invokevirtual kellinwood/zipio/ZipInput/readString(I)Ljava/lang/String;
astore 6
iload 4
ifeq L12
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Filename: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L12:
aload 5
iload 2
invokevirtual kellinwood/zipio/ZipInput/readBytes(I)[B
pop
aload 0
aload 5
invokevirtual kellinwood/zipio/ZipInput/getFilePointer()J
putfield kellinwood/zipio/ZioEntry/dataPosition J
iload 4
ifeq L13
getstatic kellinwood/zipio/ZioEntry/log Lkellinwood/logging/LoggerInterface;
ldc "Data position: 0x%08x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/dataPosition J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L13:
return
.limit locals 7
.limit stack 8
.end method

.method public setCompression(I)V
aload 0
iload 1
i2s
putfield kellinwood/zipio/ZioEntry/compression S
return
.limit locals 2
.limit stack 2
.end method

.method public setName(Ljava/lang/String;)V
aload 0
aload 1
putfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setTime(J)V
new java/util/Date
dup
lload 1
invokespecial java/util/Date/<init>(J)V
astore 4
aload 4
invokevirtual java/util/Date/getYear()I
sipush 1900
iadd
istore 3
iload 3
sipush 1980
if_icmpge L0
ldc2_w 2162688L
lstore 1
L1:
aload 0
lload 1
bipush 16
lshr
l2i
i2s
putfield kellinwood/zipio/ZioEntry/modificationDate S
aload 0
ldc2_w 65535L
lload 1
land
l2i
i2s
putfield kellinwood/zipio/ZioEntry/modificationTime S
return
L0:
iload 3
sipush 1980
isub
bipush 25
ishl
aload 4
invokevirtual java/util/Date/getMonth()I
iconst_1
iadd
bipush 21
ishl
ior
aload 4
invokevirtual java/util/Date/getDate()I
bipush 16
ishl
ior
aload 4
invokevirtual java/util/Date/getHours()I
bipush 11
ishl
ior
aload 4
invokevirtual java/util/Date/getMinutes()I
iconst_5
ishl
ior
aload 4
invokevirtual java/util/Date/getSeconds()I
iconst_1
ishr
ior
i2l
lstore 1
goto L1
.limit locals 5
.limit stack 5
.end method

.method public write(Lkellinwood/zipio/ZipOutput;)V
.throws java/io/IOException
invokestatic kellinwood/zipio/ZioEntry/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
pop
aload 1
ldc_w 33639248
invokevirtual kellinwood/zipio/ZipOutput/writeInt(I)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/versionMadeBy S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/versionRequired S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/generalPurposeBits S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/compression S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/modificationTime S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/modificationDate S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/crc32 I
invokevirtual kellinwood/zipio/ZipOutput/writeInt(I)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/compressedSize I
invokevirtual kellinwood/zipio/ZipOutput/writeInt(I)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/size I
invokevirtual kellinwood/zipio/ZipOutput/writeInt(I)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
invokevirtual java/lang/String/length()I
i2s
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/extraData [B
arraylength
aload 0
getfield kellinwood/zipio/ZioEntry/numAlignBytes S
iadd
i2s
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/fileComment Ljava/lang/String;
invokevirtual java/lang/String/length()I
i2s
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/diskNumberStart S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/internalAttributes S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/externalAttributes I
invokevirtual kellinwood/zipio/ZipOutput/writeInt(I)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/localHeaderOffset I
invokevirtual kellinwood/zipio/ZipOutput/writeInt(I)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
invokevirtual kellinwood/zipio/ZipOutput/writeString(Ljava/lang/String;)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/extraData [B
invokevirtual kellinwood/zipio/ZipOutput/writeBytes([B)V
aload 0
getfield kellinwood/zipio/ZioEntry/numAlignBytes S
ifle L0
aload 1
getstatic kellinwood/zipio/ZioEntry/alignBytes [B
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/numAlignBytes S
invokevirtual kellinwood/zipio/ZipOutput/writeBytes([BII)V
L0:
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/fileComment Ljava/lang/String;
invokevirtual kellinwood/zipio/ZipOutput/writeString(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 4
.end method

.method public writeLocalEntry(Lkellinwood/zipio/ZipOutput;)V
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZioEntry/data [B
ifnonnull L0
aload 0
getfield kellinwood/zipio/ZioEntry/dataPosition J
lconst_0
lcmp
ifge L0
aload 0
getfield kellinwood/zipio/ZioEntry/zipInput Lkellinwood/zipio/ZipInput;
ifnull L0
aload 0
invokevirtual kellinwood/zipio/ZioEntry/readLocalHeader()V
L0:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipOutput/getFilePointer()I
putfield kellinwood/zipio/ZioEntry/localHeaderOffset I
invokestatic kellinwood/zipio/ZioEntry/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
istore 4
iload 4
ifeq L1
invokestatic kellinwood/zipio/ZioEntry/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Writing local header at 0x%08x - %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/localHeaderOffset I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 0
getfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L1:
aload 0
getfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
ifnull L2
aload 0
getfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
invokevirtual kellinwood/zipio/ZioEntryOutputStream/close()V
aload 0
aload 0
getfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
invokevirtual kellinwood/zipio/ZioEntryOutputStream/getSize()I
putfield kellinwood/zipio/ZioEntry/size I
aload 0
aload 0
getfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
invokevirtual kellinwood/zipio/ZioEntryOutputStream/getWrappedStream()Ljava/io/OutputStream;
checkcast java/io/ByteArrayOutputStream
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
putfield kellinwood/zipio/ZioEntry/data [B
aload 0
aload 0
getfield kellinwood/zipio/ZioEntry/data [B
arraylength
putfield kellinwood/zipio/ZioEntry/compressedSize I
aload 0
aload 0
getfield kellinwood/zipio/ZioEntry/entryOut Lkellinwood/zipio/ZioEntryOutputStream;
invokevirtual kellinwood/zipio/ZioEntryOutputStream/getCRC()I
putfield kellinwood/zipio/ZioEntry/crc32 I
L2:
aload 1
ldc_w 67324752
invokevirtual kellinwood/zipio/ZipOutput/writeInt(I)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/versionRequired S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/generalPurposeBits S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/compression S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/modificationTime S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/modificationDate S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/crc32 I
invokevirtual kellinwood/zipio/ZipOutput/writeInt(I)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/compressedSize I
invokevirtual kellinwood/zipio/ZipOutput/writeInt(I)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/size I
invokevirtual kellinwood/zipio/ZipOutput/writeInt(I)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
invokevirtual java/lang/String/length()I
i2s
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntry/numAlignBytes S
aload 0
getfield kellinwood/zipio/ZioEntry/compression S
ifne L3
aload 1
invokevirtual kellinwood/zipio/ZipOutput/getFilePointer()I
iconst_2
iadd
aload 0
getfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
invokevirtual java/lang/String/length()I
iadd
aload 0
getfield kellinwood/zipio/ZioEntry/extraData [B
arraylength
iadd
i2l
ldc2_w 4L
lrem
l2i
i2s
istore 2
iload 2
ifle L3
aload 0
iconst_4
iload 2
isub
i2s
putfield kellinwood/zipio/ZioEntry/numAlignBytes S
L3:
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/extraData [B
arraylength
aload 0
getfield kellinwood/zipio/ZioEntry/numAlignBytes S
iadd
i2s
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
invokevirtual kellinwood/zipio/ZipOutput/writeString(Ljava/lang/String;)V
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/extraData [B
invokevirtual kellinwood/zipio/ZipOutput/writeBytes([B)V
aload 0
getfield kellinwood/zipio/ZioEntry/numAlignBytes S
ifle L4
aload 1
getstatic kellinwood/zipio/ZioEntry/alignBytes [B
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/numAlignBytes S
invokevirtual kellinwood/zipio/ZipOutput/writeBytes([BII)V
L4:
iload 4
ifeq L5
invokestatic kellinwood/zipio/ZioEntry/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Data position 0x%08x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual kellinwood/zipio/ZipOutput/getFilePointer()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L5:
aload 0
getfield kellinwood/zipio/ZioEntry/data [B
ifnull L6
aload 1
aload 0
getfield kellinwood/zipio/ZioEntry/data [B
invokevirtual kellinwood/zipio/ZipOutput/writeBytes([B)V
iload 4
ifeq L7
invokestatic kellinwood/zipio/ZioEntry/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Wrote %d bytes"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/data [B
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L7:
return
L6:
iload 4
ifeq L8
invokestatic kellinwood/zipio/ZioEntry/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Seeking to position 0x%08x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/dataPosition J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L8:
aload 0
getfield kellinwood/zipio/ZioEntry/zipInput Lkellinwood/zipio/ZipInput;
aload 0
getfield kellinwood/zipio/ZioEntry/dataPosition J
invokevirtual kellinwood/zipio/ZipInput/seek(J)V
aload 0
getfield kellinwood/zipio/ZioEntry/compressedSize I
sipush 8096
invokestatic java/lang/Math/min(II)I
istore 2
iload 2
newarray byte
astore 7
lconst_0
lstore 5
L9:
lload 5
aload 0
getfield kellinwood/zipio/ZioEntry/compressedSize I
i2l
lcmp
ifeq L7
aload 0
getfield kellinwood/zipio/ZioEntry/zipInput Lkellinwood/zipio/ZipInput;
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
aload 7
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/compressedSize I
i2l
lload 5
lsub
iload 2
i2l
invokestatic java/lang/Math/min(JJ)J
l2i
invokevirtual java/io/RandomAccessFile/read([BII)I
istore 3
iload 3
ifle L10
aload 1
aload 7
iconst_0
iload 3
invokevirtual kellinwood/zipio/ZipOutput/writeBytes([BII)V
iload 4
ifeq L11
invokestatic kellinwood/zipio/ZioEntry/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Wrote %d bytes"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L11:
lload 5
iload 3
i2l
ladd
lstore 5
goto L9
L10:
new java/lang/IllegalStateException
dup
ldc "EOF reached while copying %s with %d bytes left to go"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZioEntry/filename Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield kellinwood/zipio/ZioEntry/compressedSize I
i2l
lload 5
lsub
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 8
.limit stack 10
.end method
