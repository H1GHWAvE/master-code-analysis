.bytecode 50.0
.class public synchronized kellinwood/zipio/ZipInput
.super java/lang/Object

.field static 'log' Lkellinwood/logging/LoggerInterface;

.field 'centralEnd' Lkellinwood/zipio/CentralEnd;

.field 'fileLength' J

.field 'in' Ljava/io/RandomAccessFile;

.field public 'inputFilename' Ljava/lang/String;

.field 'manifest' Ljava/util/jar/Manifest;

.field 'scanIterations' I

.field 'zioEntries' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"

.method public <init>(Ljava/lang/String;)V
.throws java/io/IOException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
aload 0
iconst_0
putfield kellinwood/zipio/ZipInput/scanIterations I
aload 0
new java/util/LinkedHashMap
dup
invokespecial java/util/LinkedHashMap/<init>()V
putfield kellinwood/zipio/ZipInput/zioEntries Ljava/util/Map;
aload 0
aload 1
putfield kellinwood/zipio/ZipInput/inputFilename Ljava/lang/String;
aload 0
new java/io/RandomAccessFile
dup
new java/io/File
dup
aload 0
getfield kellinwood/zipio/ZipInput/inputFilename Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
putfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
aload 0
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/length()J
putfield kellinwood/zipio/ZipInput/fileLength J
return
.limit locals 2
.limit stack 6
.end method

.method private doRead()V
.catch java/lang/Throwable from L0 to L1 using L2
.catch java/lang/Throwable from L3 to L4 using L2
.catch java/lang/Throwable from L4 to L5 using L2
.catch java/lang/Throwable from L6 to L7 using L2
.catch java/lang/Throwable from L8 to L9 using L2
L0:
aload 0
sipush 256
invokevirtual kellinwood/zipio/ZipInput/scanForEOCDR(I)J
lstore 2
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
lload 2
invokevirtual java/io/RandomAccessFile/seek(J)V
aload 0
aload 0
invokestatic kellinwood/zipio/CentralEnd/read(Lkellinwood/zipio/ZipInput;)Lkellinwood/zipio/CentralEnd;
putfield kellinwood/zipio/ZipInput/centralEnd Lkellinwood/zipio/CentralEnd;
invokestatic kellinwood/zipio/ZipInput/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
istore 4
L1:
iload 4
ifeq L4
L3:
invokestatic kellinwood/zipio/ZipInput/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "EOCD found in %d iterations"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZipInput/scanIterations I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
invokestatic kellinwood/zipio/ZipInput/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Directory entries=%d, size=%d, offset=%d/0x%08x"
iconst_4
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/ZipInput/centralEnd Lkellinwood/zipio/CentralEnd;
getfield kellinwood/zipio/CentralEnd/totalCentralEntries S
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
dup
iconst_1
aload 0
getfield kellinwood/zipio/ZipInput/centralEnd Lkellinwood/zipio/CentralEnd;
getfield kellinwood/zipio/CentralEnd/centralDirectorySize I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
aload 0
getfield kellinwood/zipio/ZipInput/centralEnd Lkellinwood/zipio/CentralEnd;
getfield kellinwood/zipio/CentralEnd/centralStartOffset I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_3
aload 0
getfield kellinwood/zipio/ZipInput/centralEnd Lkellinwood/zipio/CentralEnd;
getfield kellinwood/zipio/CentralEnd/centralStartOffset I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
invokestatic kellinwood/zipio/ZipInput/getLogger()Lkellinwood/logging/LoggerInterface;
invokestatic kellinwood/zipio/ZipListingHelper/listHeader(Lkellinwood/logging/LoggerInterface;)V
L4:
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
aload 0
getfield kellinwood/zipio/ZipInput/centralEnd Lkellinwood/zipio/CentralEnd;
getfield kellinwood/zipio/CentralEnd/centralStartOffset I
i2l
invokevirtual java/io/RandomAccessFile/seek(J)V
L5:
iconst_0
istore 1
L6:
iload 1
aload 0
getfield kellinwood/zipio/ZipInput/centralEnd Lkellinwood/zipio/CentralEnd;
getfield kellinwood/zipio/CentralEnd/totalCentralEntries S
if_icmpge L10
aload 0
invokestatic kellinwood/zipio/ZioEntry/read(Lkellinwood/zipio/ZipInput;)Lkellinwood/zipio/ZioEntry;
astore 5
aload 0
getfield kellinwood/zipio/ZipInput/zioEntries Ljava/util/Map;
aload 5
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
aload 5
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L7:
iload 4
ifeq L9
L8:
invokestatic kellinwood/zipio/ZipInput/getLogger()Lkellinwood/logging/LoggerInterface;
aload 5
invokestatic kellinwood/zipio/ZipListingHelper/listEntry(Lkellinwood/logging/LoggerInterface;Lkellinwood/zipio/ZioEntry;)V
L9:
iload 1
iconst_1
iadd
istore 1
goto L6
L2:
astore 5
aload 5
invokevirtual java/lang/Throwable/printStackTrace()V
L10:
return
.limit locals 6
.limit stack 6
.end method

.method private static getLogger()Lkellinwood/logging/LoggerInterface;
getstatic kellinwood/zipio/ZipInput/log Lkellinwood/logging/LoggerInterface;
ifnonnull L0
ldc kellinwood/zipio/ZipInput
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic kellinwood/logging/LoggerManager/getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
putstatic kellinwood/zipio/ZipInput/log Lkellinwood/logging/LoggerInterface;
L0:
getstatic kellinwood/zipio/ZipInput/log Lkellinwood/logging/LoggerInterface;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static read(Ljava/lang/String;)Lkellinwood/zipio/ZipInput;
.throws java/io/IOException
new kellinwood/zipio/ZipInput
dup
aload 0
invokespecial kellinwood/zipio/ZipInput/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokespecial kellinwood/zipio/ZipInput/doRead()V
aload 0
areturn
.limit locals 1
.limit stack 3
.end method

.method public close()V
.catch java/lang/Throwable from L0 to L1 using L2
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
ifnull L1
L0:
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/close()V
L1:
return
L2:
astore 1
return
.limit locals 2
.limit stack 1
.end method

.method public getEntries()Ljava/util/Map;
.signature "()Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;"
aload 0
getfield kellinwood/zipio/ZipInput/zioEntries Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getEntry(Ljava/lang/String;)Lkellinwood/zipio/ZioEntry;
aload 0
getfield kellinwood/zipio/ZipInput/zioEntries Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/zipio/ZioEntry
areturn
.limit locals 2
.limit stack 2
.end method

.method public getFileLength()J
aload 0
getfield kellinwood/zipio/ZipInput/fileLength J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getFilePointer()J
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/getFilePointer()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getFilename()Ljava/lang/String;
aload 0
getfield kellinwood/zipio/ZipInput/inputFilename Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getManifest()Ljava/util/jar/Manifest;
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZipInput/manifest Ljava/util/jar/Manifest;
ifnonnull L0
aload 0
getfield kellinwood/zipio/ZipInput/zioEntries Ljava/util/Map;
ldc "META-INF/MANIFEST.MF"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/zipio/ZioEntry
astore 1
aload 1
ifnull L0
aload 0
new java/util/jar/Manifest
dup
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getInputStream()Ljava/io/InputStream;
invokespecial java/util/jar/Manifest/<init>(Ljava/io/InputStream;)V
putfield kellinwood/zipio/ZipInput/manifest Ljava/util/jar/Manifest;
L0:
aload 0
getfield kellinwood/zipio/ZipInput/manifest Ljava/util/jar/Manifest;
areturn
.limit locals 2
.limit stack 4
.end method

.method public list(Ljava/lang/String;)Ljava/util/Collection;
.signature "(Ljava/lang/String;)Ljava/util/Collection<Ljava/lang/String;>;"
aload 1
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L0
new java/lang/IllegalArgumentException
dup
ldc "Invalid path -- does not end with '/'"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
astore 2
aload 1
ldc "/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L1
aload 1
iconst_1
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 2
L1:
ldc "^%s([^/]+/?).*"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
astore 1
new java/util/TreeSet
dup
invokespecial java/util/TreeSet/<init>()V
astore 2
aload 0
getfield kellinwood/zipio/ZipInput/zioEntries Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L2:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 4
aload 4
invokevirtual java/util/regex/Matcher/matches()Z
ifeq L2
aload 2
aload 4
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
goto L2
L3:
aload 2
areturn
.limit locals 5
.limit stack 5
.end method

.method public read([BII)I
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
aload 1
iload 2
iload 3
invokevirtual java/io/RandomAccessFile/read([BII)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method public readByte()B
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/readByte()B
ireturn
.limit locals 1
.limit stack 1
.end method

.method public readBytes(I)[B
.throws java/io/IOException
iload 1
newarray byte
astore 3
iconst_0
istore 2
L0:
iload 2
iload 1
if_icmpge L1
aload 3
iload 2
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/readByte()B
bastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 3
areturn
.limit locals 4
.limit stack 3
.end method

.method public readInt()I
.throws java/io/IOException
iconst_0
istore 2
iconst_0
istore 1
L0:
iload 1
iconst_4
if_icmpge L1
iload 2
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/readUnsignedByte()I
iload 1
bipush 8
imul
ishl
ior
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 2
ireturn
.limit locals 3
.limit stack 4
.end method

.method public readShort()S
.throws java/io/IOException
iconst_0
istore 1
iconst_0
istore 2
L0:
iload 2
iconst_2
if_icmpge L1
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/readUnsignedByte()I
iload 2
bipush 8
imul
ishl
iload 1
ior
i2s
istore 1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iload 1
ireturn
.limit locals 3
.limit stack 3
.end method

.method public readString(I)Ljava/lang/String;
.throws java/io/IOException
iload 1
newarray byte
astore 3
iconst_0
istore 2
L0:
iload 2
iload 1
if_icmpge L1
aload 3
iload 2
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/readByte()B
bastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
new java/lang/String
dup
aload 3
invokespecial java/lang/String/<init>([B)V
areturn
.limit locals 4
.limit stack 3
.end method

.method public scanForEOCDR(I)J
.throws java/io/IOException
iload 1
i2l
aload 0
getfield kellinwood/zipio/ZipInput/fileLength J
lcmp
ifgt L0
iload 1
ldc_w 65536
if_icmple L1
L0:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "End of central directory not found in "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield kellinwood/zipio/ZipInput/inputFilename Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield kellinwood/zipio/ZipInput/fileLength J
iload 1
i2l
invokestatic java/lang/Math/min(JJ)J
l2i
istore 3
iload 3
newarray byte
astore 4
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
aload 0
getfield kellinwood/zipio/ZipInput/fileLength J
iload 3
i2l
lsub
invokevirtual java/io/RandomAccessFile/seek(J)V
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
aload 4
invokevirtual java/io/RandomAccessFile/readFully([B)V
iload 3
bipush 22
isub
istore 2
L2:
iload 2
iflt L3
aload 0
aload 0
getfield kellinwood/zipio/ZipInput/scanIterations I
iconst_1
iadd
putfield kellinwood/zipio/ZipInput/scanIterations I
aload 4
iload 2
baload
bipush 80
if_icmpne L4
aload 4
iload 2
iconst_1
iadd
baload
bipush 75
if_icmpne L4
aload 4
iload 2
iconst_2
iadd
baload
iconst_5
if_icmpne L4
aload 4
iload 2
iconst_3
iadd
baload
bipush 6
if_icmpne L4
aload 0
getfield kellinwood/zipio/ZipInput/fileLength J
iload 3
i2l
lsub
iload 2
i2l
ladd
lreturn
L4:
iload 2
iconst_1
isub
istore 2
goto L2
L3:
aload 0
iload 1
iconst_2
imul
invokevirtual kellinwood/zipio/ZipInput/scanForEOCDR(I)J
lreturn
.limit locals 5
.limit stack 5
.end method

.method public seek(J)V
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
lload 1
invokevirtual java/io/RandomAccessFile/seek(J)V
return
.limit locals 3
.limit stack 3
.end method
