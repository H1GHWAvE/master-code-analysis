.bytecode 50.0
.class public synchronized kellinwood/zipio/ZioEntryOutputStream
.super java/io/OutputStream

.field 'crc' Ljava/util/zip/CRC32;

.field 'crcValue' I

.field 'downstream' Ljava/io/OutputStream;

.field 'size' I

.field 'wrapped' Ljava/io/OutputStream;

.method public <init>(ILjava/io/OutputStream;)V
aload 0
invokespecial java/io/OutputStream/<init>()V
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntryOutputStream/size I
aload 0
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
putfield kellinwood/zipio/ZioEntryOutputStream/crc Ljava/util/zip/CRC32;
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntryOutputStream/crcValue I
aload 0
aload 2
putfield kellinwood/zipio/ZioEntryOutputStream/wrapped Ljava/io/OutputStream;
iload 1
ifeq L0
aload 0
new java/util/zip/DeflaterOutputStream
dup
aload 2
new java/util/zip/Deflater
dup
bipush 9
iconst_1
invokespecial java/util/zip/Deflater/<init>(IZ)V
invokespecial java/util/zip/DeflaterOutputStream/<init>(Ljava/io/OutputStream;Ljava/util/zip/Deflater;)V
putfield kellinwood/zipio/ZioEntryOutputStream/downstream Ljava/io/OutputStream;
return
L0:
aload 0
aload 2
putfield kellinwood/zipio/ZioEntryOutputStream/downstream Ljava/io/OutputStream;
return
.limit locals 3
.limit stack 8
.end method

.method public close()V
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/downstream Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/flush()V
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/downstream Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/close()V
aload 0
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/crc Ljava/util/zip/CRC32;
invokevirtual java/util/zip/CRC32/getValue()J
l2i
putfield kellinwood/zipio/ZioEntryOutputStream/crcValue I
return
.limit locals 1
.limit stack 3
.end method

.method public flush()V
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/downstream Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/flush()V
return
.limit locals 1
.limit stack 1
.end method

.method public getCRC()I
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/crcValue I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSize()I
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/size I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getWrappedStream()Ljava/io/OutputStream;
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/wrapped Ljava/io/OutputStream;
areturn
.limit locals 1
.limit stack 1
.end method

.method public write(I)V
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/downstream Ljava/io/OutputStream;
iload 1
invokevirtual java/io/OutputStream/write(I)V
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/crc Ljava/util/zip/CRC32;
iload 1
invokevirtual java/util/zip/CRC32/update(I)V
aload 0
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/size I
iconst_1
iadd
putfield kellinwood/zipio/ZioEntryOutputStream/size I
return
.limit locals 2
.limit stack 3
.end method

.method public write([B)V
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/downstream Ljava/io/OutputStream;
aload 1
invokevirtual java/io/OutputStream/write([B)V
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/crc Ljava/util/zip/CRC32;
aload 1
invokevirtual java/util/zip/CRC32/update([B)V
aload 0
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/size I
aload 1
arraylength
iadd
putfield kellinwood/zipio/ZioEntryOutputStream/size I
return
.limit locals 2
.limit stack 3
.end method

.method public write([BII)V
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/downstream Ljava/io/OutputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/OutputStream/write([BII)V
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/crc Ljava/util/zip/CRC32;
aload 1
iload 2
iload 3
invokevirtual java/util/zip/CRC32/update([BII)V
aload 0
aload 0
getfield kellinwood/zipio/ZioEntryOutputStream/size I
iload 3
iadd
putfield kellinwood/zipio/ZioEntryOutputStream/size I
return
.limit locals 4
.limit stack 4
.end method
