.bytecode 50.0
.class public synchronized kellinwood/zipio/CentralEnd
.super java/lang/Object

.field private static 'log' Lkellinwood/logging/LoggerInterface;

.field public 'centralDirectorySize' I

.field public 'centralStartDisk' S

.field public 'centralStartOffset' I

.field public 'fileComment' Ljava/lang/String;

.field public 'numCentralEntries' S

.field public 'numberThisDisk' S

.field public 'signature' I

.field public 'totalCentralEntries' S

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc_w 101010256
putfield kellinwood/zipio/CentralEnd/signature I
aload 0
iconst_0
putfield kellinwood/zipio/CentralEnd/numberThisDisk S
aload 0
iconst_0
putfield kellinwood/zipio/CentralEnd/centralStartDisk S
return
.limit locals 1
.limit stack 2
.end method

.method private doRead(Lkellinwood/zipio/ZipInput;)V
.throws java/io/IOException
invokestatic kellinwood/zipio/CentralEnd/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
istore 2
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
putfield kellinwood/zipio/CentralEnd/numberThisDisk S
iload 2
ifeq L0
getstatic kellinwood/zipio/CentralEnd/log Lkellinwood/logging/LoggerInterface;
ldc "This disk number: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/CentralEnd/numberThisDisk S
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L0:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
putfield kellinwood/zipio/CentralEnd/centralStartDisk S
iload 2
ifeq L1
getstatic kellinwood/zipio/CentralEnd/log Lkellinwood/logging/LoggerInterface;
ldc "Central dir start disk number: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/CentralEnd/centralStartDisk S
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L1:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
putfield kellinwood/zipio/CentralEnd/numCentralEntries S
iload 2
ifeq L2
getstatic kellinwood/zipio/CentralEnd/log Lkellinwood/logging/LoggerInterface;
ldc "Central entries on this disk: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/CentralEnd/numCentralEntries S
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L2:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
putfield kellinwood/zipio/CentralEnd/totalCentralEntries S
iload 2
ifeq L3
getstatic kellinwood/zipio/CentralEnd/log Lkellinwood/logging/LoggerInterface;
ldc "Total number of central entries: 0x%04x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/CentralEnd/totalCentralEntries S
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L3:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readInt()I
putfield kellinwood/zipio/CentralEnd/centralDirectorySize I
iload 2
ifeq L4
getstatic kellinwood/zipio/CentralEnd/log Lkellinwood/logging/LoggerInterface;
ldc "Central directory size: 0x%08x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/CentralEnd/centralDirectorySize I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L4:
aload 0
aload 1
invokevirtual kellinwood/zipio/ZipInput/readInt()I
putfield kellinwood/zipio/CentralEnd/centralStartOffset I
iload 2
ifeq L5
getstatic kellinwood/zipio/CentralEnd/log Lkellinwood/logging/LoggerInterface;
ldc "Central directory offset: 0x%08x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield kellinwood/zipio/CentralEnd/centralStartOffset I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L5:
aload 0
aload 1
aload 1
invokevirtual kellinwood/zipio/ZipInput/readShort()S
invokevirtual kellinwood/zipio/ZipInput/readString(I)Ljava/lang/String;
putfield kellinwood/zipio/CentralEnd/fileComment Ljava/lang/String;
iload 2
ifeq L6
getstatic kellinwood/zipio/CentralEnd/log Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ".ZIP file comment: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield kellinwood/zipio/CentralEnd/fileComment Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L6:
return
.limit locals 3
.limit stack 6
.end method

.method public static getLogger()Lkellinwood/logging/LoggerInterface;
getstatic kellinwood/zipio/CentralEnd/log Lkellinwood/logging/LoggerInterface;
ifnonnull L0
ldc kellinwood/zipio/CentralEnd
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic kellinwood/logging/LoggerManager/getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
putstatic kellinwood/zipio/CentralEnd/log Lkellinwood/logging/LoggerInterface;
L0:
getstatic kellinwood/zipio/CentralEnd/log Lkellinwood/logging/LoggerInterface;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static read(Lkellinwood/zipio/ZipInput;)Lkellinwood/zipio/CentralEnd;
.throws java/io/IOException
aload 0
invokevirtual kellinwood/zipio/ZipInput/readInt()I
ldc_w 101010256
if_icmpeq L0
aload 0
aload 0
invokevirtual kellinwood/zipio/ZipInput/getFilePointer()J
ldc2_w 4L
lsub
invokevirtual kellinwood/zipio/ZipInput/seek(J)V
aconst_null
areturn
L0:
new kellinwood/zipio/CentralEnd
dup
invokespecial kellinwood/zipio/CentralEnd/<init>()V
astore 1
aload 1
aload 0
invokespecial kellinwood/zipio/CentralEnd/doRead(Lkellinwood/zipio/ZipInput;)V
aload 1
areturn
.limit locals 2
.limit stack 5
.end method

.method public write(Lkellinwood/zipio/ZipOutput;)V
.throws java/io/IOException
invokestatic kellinwood/zipio/CentralEnd/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
pop
aload 1
aload 0
getfield kellinwood/zipio/CentralEnd/signature I
invokevirtual kellinwood/zipio/ZipOutput/writeInt(I)V
aload 1
aload 0
getfield kellinwood/zipio/CentralEnd/numberThisDisk S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/CentralEnd/centralStartDisk S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/CentralEnd/numCentralEntries S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/CentralEnd/totalCentralEntries S
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/CentralEnd/centralDirectorySize I
invokevirtual kellinwood/zipio/ZipOutput/writeInt(I)V
aload 1
aload 0
getfield kellinwood/zipio/CentralEnd/centralStartOffset I
invokevirtual kellinwood/zipio/ZipOutput/writeInt(I)V
aload 1
aload 0
getfield kellinwood/zipio/CentralEnd/fileComment Ljava/lang/String;
invokevirtual java/lang/String/length()I
i2s
invokevirtual kellinwood/zipio/ZipOutput/writeShort(S)V
aload 1
aload 0
getfield kellinwood/zipio/CentralEnd/fileComment Ljava/lang/String;
invokevirtual kellinwood/zipio/ZipOutput/writeString(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 2
.end method
