.bytecode 50.0
.class public synchronized kellinwood/zipio/ZioEntryInputStream
.super java/io/InputStream

.field 'debug' Z

.field 'log' Lkellinwood/logging/LoggerInterface;

.field 'monitor' Ljava/io/OutputStream;

.field 'offset' I

.field 'raf' Ljava/io/RandomAccessFile;

.field 'returnDummyByte' Z

.field 'size' I

.method public <init>(Lkellinwood/zipio/ZioEntry;)V
.throws java/io/IOException
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntryInputStream/returnDummyByte Z
aload 0
aconst_null
putfield kellinwood/zipio/ZioEntryInputStream/monitor Ljava/io/OutputStream;
aload 0
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic kellinwood/logging/LoggerManager/getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
putfield kellinwood/zipio/ZioEntryInputStream/log Lkellinwood/logging/LoggerInterface;
aload 0
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/log Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
putfield kellinwood/zipio/ZioEntryInputStream/debug Z
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntryInputStream/offset I
aload 0
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getCompressedSize()I
putfield kellinwood/zipio/ZioEntryInputStream/size I
aload 0
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getZipInput()Lkellinwood/zipio/ZipInput;
getfield kellinwood/zipio/ZipInput/in Ljava/io/RandomAccessFile;
putfield kellinwood/zipio/ZioEntryInputStream/raf Ljava/io/RandomAccessFile;
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getDataPosition()J
lconst_0
lcmp
iflt L0
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/debug Z
ifeq L1
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/log Lkellinwood/logging/LoggerInterface;
ldc "Seeking to %d"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getDataPosition()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L1:
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/raf Ljava/io/RandomAccessFile;
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getDataPosition()J
invokevirtual java/io/RandomAccessFile/seek(J)V
return
L0:
aload 1
invokevirtual kellinwood/zipio/ZioEntry/readLocalHeader()V
return
.limit locals 2
.limit stack 7
.end method

.method private readBytes([BII)I
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/size I
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/offset I
isub
ifne L0
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/returnDummyByte Z
ifeq L1
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntryInputStream/returnDummyByte Z
aload 1
iload 2
iconst_0
bastore
iconst_1
istore 4
L2:
iload 4
ireturn
L1:
iconst_m1
ireturn
L0:
iload 3
aload 0
invokevirtual kellinwood/zipio/ZioEntryInputStream/available()I
invokestatic java/lang/Math/min(II)I
istore 4
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/raf Ljava/io/RandomAccessFile;
aload 1
iload 2
iload 4
invokevirtual java/io/RandomAccessFile/read([BII)I
istore 5
iload 5
ifle L3
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/monitor Ljava/io/OutputStream;
ifnull L4
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/monitor Ljava/io/OutputStream;
aload 1
iload 2
iload 5
invokevirtual java/io/OutputStream/write([BII)V
L4:
aload 0
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/offset I
iload 5
iadd
putfield kellinwood/zipio/ZioEntryInputStream/offset I
L3:
iload 5
istore 4
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/debug Z
ifeq L2
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/log Lkellinwood/logging/LoggerInterface;
ldc "Read %d bytes for read(b,%d,%d)"
iconst_3
anewarray java/lang/Object
dup
iconst_0
iload 5
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
iload 5
ireturn
.limit locals 6
.limit stack 6
.end method

.method public available()I
.throws java/io/IOException
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/size I
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/offset I
isub
istore 2
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/debug Z
ifeq L0
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/log Lkellinwood/logging/LoggerInterface;
ldc "Available = %d"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L0:
iload 2
istore 1
iload 2
ifne L1
iload 2
istore 1
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/returnDummyByte Z
ifeq L1
iconst_1
istore 1
L1:
iload 1
ireturn
.limit locals 3
.limit stack 6
.end method

.method public close()V
.throws java/io/IOException
return
.limit locals 1
.limit stack 0
.end method

.method public markSupported()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public read()I
.throws java/io/IOException
iconst_0
istore 1
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/size I
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/offset I
isub
ifne L0
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/returnDummyByte Z
ifeq L1
aload 0
iconst_0
putfield kellinwood/zipio/ZioEntryInputStream/returnDummyByte Z
L2:
iload 1
ireturn
L1:
iconst_m1
ireturn
L0:
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/raf Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/read()I
istore 2
iload 2
iflt L3
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/monitor Ljava/io/OutputStream;
ifnull L4
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/monitor Ljava/io/OutputStream;
iload 2
invokevirtual java/io/OutputStream/write(I)V
L4:
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/debug Z
ifeq L5
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/log Lkellinwood/logging/LoggerInterface;
ldc "Read 1 byte"
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L5:
aload 0
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/offset I
iconst_1
iadd
putfield kellinwood/zipio/ZioEntryInputStream/offset I
iload 2
ireturn
L3:
iload 2
istore 1
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/debug Z
ifeq L2
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/log Lkellinwood/logging/LoggerInterface;
ldc "Read 0 bytes"
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
iload 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public read([B)I
.throws java/io/IOException
aload 0
aload 1
iconst_0
aload 1
arraylength
invokespecial kellinwood/zipio/ZioEntryInputStream/readBytes([BII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method public read([BII)I
.throws java/io/IOException
aload 0
aload 1
iload 2
iload 3
invokespecial kellinwood/zipio/ZioEntryInputStream/readBytes([BII)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method public setMonitorStream(Ljava/io/OutputStream;)V
aload 0
aload 1
putfield kellinwood/zipio/ZioEntryInputStream/monitor Ljava/io/OutputStream;
return
.limit locals 2
.limit stack 2
.end method

.method public setReturnDummyByte(Z)V
aload 0
iload 1
putfield kellinwood/zipio/ZioEntryInputStream/returnDummyByte Z
return
.limit locals 2
.limit stack 2
.end method

.method public skip(J)J
.throws java/io/IOException
lload 1
aload 0
invokevirtual kellinwood/zipio/ZioEntryInputStream/available()I
i2l
invokestatic java/lang/Math/min(JJ)J
lstore 1
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/raf Ljava/io/RandomAccessFile;
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/raf Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/getFilePointer()J
lload 1
ladd
invokevirtual java/io/RandomAccessFile/seek(J)V
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/debug Z
ifeq L0
aload 0
getfield kellinwood/zipio/ZioEntryInputStream/log Lkellinwood/logging/LoggerInterface;
ldc "Skipped %d bytes"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L0:
lload 1
lreturn
.limit locals 3
.limit stack 7
.end method
