.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/optional/CustomKeySigner
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static signZip(Lkellinwood/security/zipsigner/ZipSigner;Ljava/lang/String;[CLjava/lang/String;[CLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.throws java/lang/Exception
aload 0
invokevirtual kellinwood/security/zipsigner/ZipSigner/issueLoadingCertAndKeysProgressEvent()V
aload 1
aload 2
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/loadKeyStore(Ljava/lang/String;[C)Ljava/security/KeyStore;
astore 1
aload 0
ldc "custom"
aload 1
aload 3
invokevirtual java/security/KeyStore/getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;
checkcast java/security/cert/X509Certificate
aload 1
aload 3
aload 4
invokevirtual java/security/KeyStore/getKey(Ljava/lang/String;[C)Ljava/security/Key;
checkcast java/security/PrivateKey
aload 5
aconst_null
invokevirtual kellinwood/security/zipsigner/ZipSigner/setKeys(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;Ljava/lang/String;[B)V
aload 0
aload 6
aload 7
invokevirtual kellinwood/security/zipsigner/ZipSigner/signZip(Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 8
.limit stack 6
.end method
