.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/optional/Fingerprint
.super java/lang/Object

.field static 'logger' Lkellinwood/logging/LoggerInterface;

.method static <clinit>()V
ldc kellinwood/security/zipsigner/optional/Fingerprint
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic kellinwood/logging/LoggerManager/getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
putstatic kellinwood/security/zipsigner/optional/Fingerprint/logger Lkellinwood/logging/LoggerInterface;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static base64Fingerprint(Ljava/lang/String;[B)Ljava/lang/String;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
L0:
aload 0
aload 1
invokestatic kellinwood/security/zipsigner/optional/Fingerprint/calcDigest(Ljava/lang/String;[B)[B
astore 0
L1:
aload 0
ifnonnull L3
aconst_null
areturn
L3:
aload 0
invokestatic kellinwood/security/zipsigner/Base64/encode([B)Ljava/lang/String;
astore 0
L4:
aload 0
areturn
L2:
astore 0
getstatic kellinwood/security/zipsigner/optional/Fingerprint/logger Lkellinwood/logging/LoggerInterface;
aload 0
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
aload 0
invokeinterface kellinwood/logging/LoggerInterface/error(Ljava/lang/String;Ljava/lang/Throwable;)V 2
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method static calcDigest(Ljava/lang/String;[B)[B
.catch java/lang/Exception from L0 to L1 using L2
L0:
aload 0
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 0
aload 0
aload 1
invokevirtual java/security/MessageDigest/update([B)V
aload 0
invokevirtual java/security/MessageDigest/digest()[B
astore 0
L1:
aload 0
areturn
L2:
astore 0
getstatic kellinwood/security/zipsigner/optional/Fingerprint/logger Lkellinwood/logging/LoggerInterface;
aload 0
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
aload 0
invokeinterface kellinwood/logging/LoggerInterface/error(Ljava/lang/String;Ljava/lang/Throwable;)V 2
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method public static hexFingerprint(Ljava/lang/String;[B)Ljava/lang/String;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
L0:
aload 0
aload 1
invokestatic kellinwood/security/zipsigner/optional/Fingerprint/calcDigest(Ljava/lang/String;[B)[B
astore 1
L1:
aload 1
ifnonnull L3
aconst_null
areturn
L3:
new org/spongycastle/util/encoders/HexTranslator
dup
invokespecial org/spongycastle/util/encoders/HexTranslator/<init>()V
astore 3
aload 1
arraylength
iconst_2
imul
newarray byte
astore 0
aload 3
aload 1
iconst_0
aload 1
arraylength
aload 0
iconst_0
invokevirtual org/spongycastle/util/encoders/HexTranslator/encode([BII[BI)I
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 1
L4:
iconst_0
istore 2
L5:
iload 2
aload 0
arraylength
if_icmpge L7
aload 1
aload 0
iload 2
baload
i2c
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 1
aload 0
iload 2
iconst_1
iadd
baload
i2c
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 2
aload 0
arraylength
iconst_2
isub
if_icmpeq L9
aload 1
bipush 58
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L6:
goto L9
L7:
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
astore 0
L8:
aload 0
areturn
L2:
astore 0
getstatic kellinwood/security/zipsigner/optional/Fingerprint/logger Lkellinwood/logging/LoggerInterface;
aload 0
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
aload 0
invokeinterface kellinwood/logging/LoggerInterface/error(Ljava/lang/String;Ljava/lang/Throwable;)V 2
aconst_null
areturn
L9:
iload 2
iconst_2
iadd
istore 2
goto L5
.limit locals 4
.limit stack 6
.end method
