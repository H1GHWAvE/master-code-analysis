.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/optional/CertCreator
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static createKey(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILkellinwood/security/zipsigner/optional/DistinguishedNameValues;)Lkellinwood/security/zipsigner/KeySet;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L4 to L5 using L2
L0:
aload 0
invokestatic java/security/KeyPairGenerator/getInstance(Ljava/lang/String;)Ljava/security/KeyPairGenerator;
astore 0
aload 0
iload 1
invokevirtual java/security/KeyPairGenerator/initialize(I)V
aload 0
invokevirtual java/security/KeyPairGenerator/generateKeyPair()Ljava/security/KeyPair;
astore 6
new org/spongycastle/x509/X509V3CertificateGenerator
dup
invokespecial org/spongycastle/x509/X509V3CertificateGenerator/<init>()V
astore 7
aload 5
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/getPrincipal()Lorg/spongycastle/jce/X509Principal;
astore 5
new java/security/SecureRandom
dup
invokespecial java/security/SecureRandom/<init>()V
invokevirtual java/security/SecureRandom/nextInt()I
i2l
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
astore 0
L1:
aload 0
getstatic java/math/BigInteger/ZERO Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/compareTo(Ljava/math/BigInteger;)I
ifge L4
new java/security/SecureRandom
dup
invokespecial java/security/SecureRandom/<init>()V
invokevirtual java/security/SecureRandom/nextInt()I
i2l
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
astore 0
L3:
goto L1
L4:
aload 7
aload 0
invokevirtual org/spongycastle/x509/X509V3CertificateGenerator/setSerialNumber(Ljava/math/BigInteger;)V
aload 7
aload 5
invokevirtual org/spongycastle/x509/X509V3CertificateGenerator/setIssuerDN(Lorg/spongycastle/asn1/x509/X509Name;)V
aload 7
new java/util/Date
dup
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 2592000000L
lsub
invokespecial java/util/Date/<init>(J)V
invokevirtual org/spongycastle/x509/X509V3CertificateGenerator/setNotBefore(Ljava/util/Date;)V
aload 7
new java/util/Date
dup
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 31622400000L
iload 4
i2l
lmul
ladd
invokespecial java/util/Date/<init>(J)V
invokevirtual org/spongycastle/x509/X509V3CertificateGenerator/setNotAfter(Ljava/util/Date;)V
aload 7
aload 5
invokevirtual org/spongycastle/x509/X509V3CertificateGenerator/setSubjectDN(Lorg/spongycastle/asn1/x509/X509Name;)V
aload 7
aload 6
invokevirtual java/security/KeyPair/getPublic()Ljava/security/PublicKey;
invokevirtual org/spongycastle/x509/X509V3CertificateGenerator/setPublicKey(Ljava/security/PublicKey;)V
aload 7
aload 3
invokevirtual org/spongycastle/x509/X509V3CertificateGenerator/setSignatureAlgorithm(Ljava/lang/String;)V
aload 7
aload 6
invokevirtual java/security/KeyPair/getPrivate()Ljava/security/PrivateKey;
ldc "BC"
invokevirtual org/spongycastle/x509/X509V3CertificateGenerator/generate(Ljava/security/PrivateKey;Ljava/lang/String;)Ljava/security/cert/X509Certificate;
astore 0
new kellinwood/security/zipsigner/KeySet
dup
invokespecial kellinwood/security/zipsigner/KeySet/<init>()V
astore 3
aload 3
aload 2
invokevirtual kellinwood/security/zipsigner/KeySet/setName(Ljava/lang/String;)V
aload 3
aload 6
invokevirtual java/security/KeyPair/getPrivate()Ljava/security/PrivateKey;
invokevirtual kellinwood/security/zipsigner/KeySet/setPrivateKey(Ljava/security/PrivateKey;)V
aload 3
aload 0
invokevirtual kellinwood/security/zipsigner/KeySet/setPublicKey(Ljava/security/cert/X509Certificate;)V
L5:
aload 3
areturn
L2:
astore 0
new java/lang/RuntimeException
dup
aload 0
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 8
.limit stack 9
.end method

.method public static createKey(Ljava/lang/String;[CLjava/lang/String;ILjava/lang/String;[CLjava/lang/String;ILkellinwood/security/zipsigner/optional/DistinguishedNameValues;)Lkellinwood/security/zipsigner/KeySet;
.catch java/lang/RuntimeException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
L0:
aload 2
iload 3
aload 4
aload 6
iload 7
aload 8
invokestatic kellinwood/security/zipsigner/optional/CertCreator/createKey(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILkellinwood/security/zipsigner/optional/DistinguishedNameValues;)Lkellinwood/security/zipsigner/KeySet;
astore 2
aload 0
aload 1
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/loadKeyStore(Ljava/lang/String;[C)Ljava/security/KeyStore;
astore 6
aload 6
aload 4
aload 2
invokevirtual kellinwood/security/zipsigner/KeySet/getPrivateKey()Ljava/security/PrivateKey;
aload 5
iconst_1
anewarray java/security/cert/Certificate
dup
iconst_0
aload 2
invokevirtual kellinwood/security/zipsigner/KeySet/getPublicKey()Ljava/security/cert/X509Certificate;
aastore
invokevirtual java/security/KeyStore/setKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V
aload 6
aload 0
aload 1
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/writeKeyStore(Ljava/security/KeyStore;Ljava/lang/String;[C)V
L1:
aload 2
areturn
L2:
astore 0
aload 0
athrow
L3:
astore 0
new java/lang/RuntimeException
dup
aload 0
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 9
.limit stack 8
.end method

.method public static createKeystoreAndKey(Ljava/lang/String;[CLjava/lang/String;ILjava/lang/String;[CLjava/lang/String;ILkellinwood/security/zipsigner/optional/DistinguishedNameValues;)Lkellinwood/security/zipsigner/KeySet;
.catch java/lang/RuntimeException from L0 to L1 using L1
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/RuntimeException from L3 to L4 using L1
.catch java/lang/Exception from L3 to L4 using L2
L0:
aload 2
iload 3
aload 4
aload 6
iload 7
aload 8
invokestatic kellinwood/security/zipsigner/optional/CertCreator/createKey(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILkellinwood/security/zipsigner/optional/DistinguishedNameValues;)Lkellinwood/security/zipsigner/KeySet;
astore 2
aload 0
aload 1
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/createKeyStore(Ljava/lang/String;[C)Ljava/security/KeyStore;
astore 6
aload 6
aload 4
aload 2
invokevirtual kellinwood/security/zipsigner/KeySet/getPrivateKey()Ljava/security/PrivateKey;
aload 5
iconst_1
anewarray java/security/cert/Certificate
dup
iconst_0
aload 2
invokevirtual kellinwood/security/zipsigner/KeySet/getPublicKey()Ljava/security/cert/X509Certificate;
aastore
invokevirtual java/security/KeyStore/setKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L3
new java/io/IOException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "File already exists: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 0
aload 0
athrow
L3:
aload 6
aload 0
aload 1
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/writeKeyStore(Ljava/security/KeyStore;Ljava/lang/String;[C)V
L4:
aload 2
areturn
L2:
astore 0
new java/lang/RuntimeException
dup
aload 0
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 9
.limit stack 8
.end method

.method public static createKeystoreAndKey(Ljava/lang/String;[CLjava/lang/String;Lkellinwood/security/zipsigner/optional/DistinguishedNameValues;)V
aload 0
aload 1
ldc "RSA"
sipush 2048
aload 2
aload 1
ldc "SHA1withRSA"
bipush 30
aload 3
invokestatic kellinwood/security/zipsigner/optional/CertCreator/createKeystoreAndKey(Ljava/lang/String;[CLjava/lang/String;ILjava/lang/String;[CLjava/lang/String;ILkellinwood/security/zipsigner/optional/DistinguishedNameValues;)Lkellinwood/security/zipsigner/KeySet;
pop
return
.limit locals 4
.limit stack 9
.end method
