.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/optional/SignatureBlockGenerator
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static generate(Lkellinwood/security/zipsigner/KeySet;[B)[B
.catch java/lang/Exception from L0 to L1 using L2
L0:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
new org/spongycastle/cms/CMSProcessableByteArray
dup
aload 1
invokespecial org/spongycastle/cms/CMSProcessableByteArray/<init>([B)V
astore 1
aload 2
aload 0
invokevirtual kellinwood/security/zipsigner/KeySet/getPublicKey()Ljava/security/cert/X509Certificate;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
new org/spongycastle/cert/jcajce/JcaCertStore
dup
aload 2
invokespecial org/spongycastle/cert/jcajce/JcaCertStore/<init>(Ljava/util/Collection;)V
astore 2
new org/spongycastle/cms/CMSSignedDataGenerator
dup
invokespecial org/spongycastle/cms/CMSSignedDataGenerator/<init>()V
astore 3
new org/spongycastle/operator/jcajce/JcaContentSignerBuilder
dup
aload 0
invokevirtual kellinwood/security/zipsigner/KeySet/getSignatureAlgorithm()Ljava/lang/String;
invokespecial org/spongycastle/operator/jcajce/JcaContentSignerBuilder/<init>(Ljava/lang/String;)V
ldc "SC"
invokevirtual org/spongycastle/operator/jcajce/JcaContentSignerBuilder/setProvider(Ljava/lang/String;)Lorg/spongycastle/operator/jcajce/JcaContentSignerBuilder;
aload 0
invokevirtual kellinwood/security/zipsigner/KeySet/getPrivateKey()Ljava/security/PrivateKey;
invokevirtual org/spongycastle/operator/jcajce/JcaContentSignerBuilder/build(Ljava/security/PrivateKey;)Lorg/spongycastle/operator/ContentSigner;
astore 4
new org/spongycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder
dup
new org/spongycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder
dup
invokespecial org/spongycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder/<init>()V
ldc "SC"
invokevirtual org/spongycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder/setProvider(Ljava/lang/String;)Lorg/spongycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;
invokevirtual org/spongycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder/build()Lorg/spongycastle/operator/DigestCalculatorProvider;
invokespecial org/spongycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder/<init>(Lorg/spongycastle/operator/DigestCalculatorProvider;)V
astore 5
aload 5
iconst_1
invokevirtual org/spongycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder/setDirectSignature(Z)Lorg/spongycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;
pop
aload 3
aload 5
aload 4
aload 0
invokevirtual kellinwood/security/zipsigner/KeySet/getPublicKey()Ljava/security/cert/X509Certificate;
invokevirtual org/spongycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder/build(Lorg/spongycastle/operator/ContentSigner;Ljava/security/cert/X509Certificate;)Lorg/spongycastle/cms/SignerInfoGenerator;
invokevirtual org/spongycastle/cms/CMSSignedDataGenerator/addSignerInfoGenerator(Lorg/spongycastle/cms/SignerInfoGenerator;)V
aload 3
aload 2
invokevirtual org/spongycastle/cms/CMSSignedDataGenerator/addCertificates(Lorg/spongycastle/util/Store;)V
aload 3
aload 1
iconst_0
invokevirtual org/spongycastle/cms/CMSSignedDataGenerator/generate(Lorg/spongycastle/cms/CMSTypedData;Z)Lorg/spongycastle/cms/CMSSignedData;
invokevirtual org/spongycastle/cms/CMSSignedData/toASN1Structure()Lorg/spongycastle/asn1/cms/ContentInfo;
ldc "DER"
invokevirtual org/spongycastle/asn1/cms/ContentInfo/getEncoded(Ljava/lang/String;)[B
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/RuntimeException
dup
aload 0
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 6
.limit stack 4
.end method
