.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/optional/PasswordObfuscator
.super java/lang/Object

.field private static 'instance' Lkellinwood/security/zipsigner/optional/PasswordObfuscator;

.field static final 'x' Ljava/lang/String; = "harold-and-maude"

.field 'logger' Lkellinwood/logging/LoggerInterface;

.field 'skeySpec' Ljavax/crypto/spec/SecretKeySpec;

.method static <clinit>()V
aconst_null
putstatic kellinwood/security/zipsigner/optional/PasswordObfuscator/instance Lkellinwood/security/zipsigner/optional/PasswordObfuscator;
return
.limit locals 0
.limit stack 1
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc kellinwood/security/zipsigner/optional/PasswordObfuscator
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic kellinwood/logging/LoggerManager/getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
putfield kellinwood/security/zipsigner/optional/PasswordObfuscator/logger Lkellinwood/logging/LoggerInterface;
aload 0
new javax/crypto/spec/SecretKeySpec
dup
ldc "harold-and-maude"
invokevirtual java/lang/String/getBytes()[B
ldc "AES"
invokespecial javax/crypto/spec/SecretKeySpec/<init>([BLjava/lang/String;)V
putfield kellinwood/security/zipsigner/optional/PasswordObfuscator/skeySpec Ljavax/crypto/spec/SecretKeySpec;
return
.limit locals 1
.limit stack 5
.end method

.method public static flush([B)V
aload 0
ifnonnull L0
L1:
return
L0:
iconst_0
istore 1
L2:
iload 1
aload 0
arraylength
if_icmpge L1
aload 0
iload 1
iconst_0
bastore
iload 1
iconst_1
iadd
istore 1
goto L2
.limit locals 2
.limit stack 3
.end method

.method public static flush([C)V
aload 0
ifnonnull L0
L1:
return
L0:
iconst_0
istore 1
L2:
iload 1
aload 0
arraylength
if_icmpge L1
aload 0
iload 1
iconst_0
castore
iload 1
iconst_1
iadd
istore 1
goto L2
.limit locals 2
.limit stack 3
.end method

.method public static getInstance()Lkellinwood/security/zipsigner/optional/PasswordObfuscator;
getstatic kellinwood/security/zipsigner/optional/PasswordObfuscator/instance Lkellinwood/security/zipsigner/optional/PasswordObfuscator;
ifnonnull L0
new kellinwood/security/zipsigner/optional/PasswordObfuscator
dup
invokespecial kellinwood/security/zipsigner/optional/PasswordObfuscator/<init>()V
putstatic kellinwood/security/zipsigner/optional/PasswordObfuscator/instance Lkellinwood/security/zipsigner/optional/PasswordObfuscator;
L0:
getstatic kellinwood/security/zipsigner/optional/PasswordObfuscator/instance Lkellinwood/security/zipsigner/optional/PasswordObfuscator;
areturn
.limit locals 0
.limit stack 2
.end method

.method public decode(Ljava/lang/String;Ljava/lang/String;)[C
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L11 to L12 using L2
aload 2
ifnonnull L0
aconst_null
areturn
L0:
ldc "AES/ECB/PKCS5Padding"
invokestatic javax/crypto/Cipher/getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
astore 6
aload 6
iconst_2
new javax/crypto/spec/SecretKeySpec
dup
ldc "harold-and-maude"
invokevirtual java/lang/String/getBytes()[B
ldc "AES"
invokespecial javax/crypto/spec/SecretKeySpec/<init>([BLjava/lang/String;)V
invokevirtual javax/crypto/Cipher/init(ILjava/security/Key;)V
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
new java/io/ByteArrayInputStream
dup
aload 6
aload 2
invokevirtual java/lang/String/getBytes()[B
invokestatic kellinwood/security/zipsigner/Base64/decode([B)[B
invokevirtual javax/crypto/Cipher/doFinal([B)[B
invokespecial java/io/ByteArrayInputStream/<init>([B)V
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 6
sipush 128
newarray char
astore 2
L1:
iconst_0
istore 3
L3:
aload 6
aload 2
iload 3
sipush 128
iload 3
isub
invokevirtual java/io/BufferedReader/read([CII)I
istore 4
L4:
iload 4
iconst_m1
if_icmpeq L5
iload 3
iload 4
iadd
istore 3
goto L3
L5:
iload 3
aload 1
invokevirtual java/lang/String/length()I
if_icmpgt L7
L6:
aconst_null
areturn
L7:
iload 3
aload 1
invokevirtual java/lang/String/length()I
isub
newarray char
astore 6
L8:
iconst_0
istore 5
L9:
aload 1
invokevirtual java/lang/String/length()I
istore 4
L10:
goto L13
L11:
aload 2
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/flush([C)V
L12:
aload 6
areturn
L2:
astore 1
aload 0
getfield kellinwood/security/zipsigner/optional/PasswordObfuscator/logger Lkellinwood/logging/LoggerInterface;
ldc "Failed to decode password"
aload 1
invokeinterface kellinwood/logging/LoggerInterface/error(Ljava/lang/String;Ljava/lang/Throwable;)V 2
aconst_null
areturn
L13:
iload 4
iload 3
if_icmpge L11
aload 6
iload 5
aload 2
iload 4
caload
castore
iload 5
iconst_1
iadd
istore 5
iload 4
iconst_1
iadd
istore 4
goto L13
.limit locals 7
.limit stack 8
.end method

.method public decodeAliasPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[C
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokevirtual kellinwood/security/zipsigner/optional/PasswordObfuscator/decode(Ljava/lang/String;Ljava/lang/String;)[C
areturn
.limit locals 4
.limit stack 3
.end method

.method public decodeKeystorePassword(Ljava/lang/String;Ljava/lang/String;)[C
aload 0
aload 1
aload 2
invokevirtual kellinwood/security/zipsigner/optional/PasswordObfuscator/decode(Ljava/lang/String;Ljava/lang/String;)[C
areturn
.limit locals 3
.limit stack 3
.end method

.method public encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
aload 2
ifnonnull L0
aconst_null
areturn
L0:
aload 2
invokevirtual java/lang/String/toCharArray()[C
astore 2
aload 0
aload 1
aload 2
invokevirtual kellinwood/security/zipsigner/optional/PasswordObfuscator/encode(Ljava/lang/String;[C)Ljava/lang/String;
astore 1
aload 2
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/flush([C)V
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method public encode(Ljava/lang/String;[C)Ljava/lang/String;
.catch java/lang/Exception from L0 to L1 using L2
aload 2
ifnonnull L0
aconst_null
areturn
L0:
ldc "AES/ECB/PKCS5Padding"
invokestatic javax/crypto/Cipher/getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
astore 3
aload 3
iconst_1
aload 0
getfield kellinwood/security/zipsigner/optional/PasswordObfuscator/skeySpec Ljavax/crypto/spec/SecretKeySpec;
invokevirtual javax/crypto/Cipher/init(ILjava/security/Key;)V
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 4
new java/io/OutputStreamWriter
dup
aload 4
invokespecial java/io/OutputStreamWriter/<init>(Ljava/io/OutputStream;)V
astore 5
aload 5
aload 1
invokevirtual java/io/Writer/write(Ljava/lang/String;)V
aload 5
aload 2
invokevirtual java/io/Writer/write([C)V
aload 5
invokevirtual java/io/Writer/flush()V
aload 3
aload 4
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
invokevirtual javax/crypto/Cipher/doFinal([B)[B
invokestatic kellinwood/security/zipsigner/Base64/encode([B)Ljava/lang/String;
astore 1
L1:
aload 1
areturn
L2:
astore 1
aload 0
getfield kellinwood/security/zipsigner/optional/PasswordObfuscator/logger Lkellinwood/logging/LoggerInterface;
ldc "Failed to obfuscate password"
aload 1
invokeinterface kellinwood/logging/LoggerInterface/error(Ljava/lang/String;Ljava/lang/Throwable;)V 2
aconst_null
areturn
.limit locals 6
.limit stack 3
.end method

.method public encodeAliasPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokevirtual kellinwood/security/zipsigner/optional/PasswordObfuscator/encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 4
.limit stack 3
.end method

.method public encodeAliasPassword(Ljava/lang/String;Ljava/lang/String;[C)Ljava/lang/String;
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokevirtual kellinwood/security/zipsigner/optional/PasswordObfuscator/encode(Ljava/lang/String;[C)Ljava/lang/String;
areturn
.limit locals 4
.limit stack 3
.end method

.method public encodeKeystorePassword(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
aload 0
aload 1
aload 2
invokevirtual kellinwood/security/zipsigner/optional/PasswordObfuscator/encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 3
.end method

.method public encodeKeystorePassword(Ljava/lang/String;[C)Ljava/lang/String;
aload 0
aload 1
aload 2
invokevirtual kellinwood/security/zipsigner/optional/PasswordObfuscator/encode(Ljava/lang/String;[C)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 3
.end method
