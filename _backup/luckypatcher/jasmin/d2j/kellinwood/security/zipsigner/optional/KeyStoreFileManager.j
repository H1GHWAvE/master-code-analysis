.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/optional/KeyStoreFileManager
.super java/lang/Object

.field static 'logger' Lkellinwood/logging/LoggerInterface;

.field static 'provider' Ljava/security/Provider;

.method static <clinit>()V
new org/spongycastle/jce/provider/BouncyCastleProvider
dup
invokespecial org/spongycastle/jce/provider/BouncyCastleProvider/<init>()V
putstatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/provider Ljava/security/Provider;
ldc kellinwood/security/zipsigner/optional/KeyStoreFileManager
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic kellinwood/logging/LoggerManager/getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
putstatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/logger Lkellinwood/logging/LoggerInterface;
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/getProvider()Ljava/security/Provider;
invokestatic java/security/Security/addProvider(Ljava/security/Provider;)I
pop
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static containsKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.throws java/lang/Exception
aload 0
aload 1
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/loadKeyStore(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;
aload 2
invokevirtual java/security/KeyStore/containsAlias(Ljava/lang/String;)Z
ireturn
.limit locals 3
.limit stack 2
.end method

.method static copyFile(Ljava/io/File;Ljava/io/File;Z)V
.throws java/io/IOException
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L4
.catch all from L5 to L6 using L4
.catch all from L7 to L8 using L4
.catch java/io/IOException from L9 to L10 using L11
.catch all from L9 to L10 using L2
.catch java/io/IOException from L10 to L12 using L13
.catch java/io/IOException from L14 to L15 using L16
.catch all from L14 to L15 using L2
.catch all from L15 to L2 using L2
.catch java/io/IOException from L17 to L18 using L19
aload 1
invokevirtual java/io/File/exists()Z
ifeq L20
aload 1
invokevirtual java/io/File/isDirectory()Z
ifeq L20
new java/io/IOException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Destination '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "' exists but is a directory"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L20:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 6
L0:
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 7
L1:
sipush 4096
newarray byte
astore 8
L3:
lconst_0
lstore 4
L5:
aload 6
aload 8
invokevirtual java/io/FileInputStream/read([B)I
istore 3
L6:
iconst_m1
iload 3
if_icmpeq L9
L7:
aload 7
aload 8
iconst_0
iload 3
invokevirtual java/io/FileOutputStream/write([BII)V
L8:
lload 4
iload 3
i2l
ladd
lstore 4
goto L5
L9:
aload 7
invokevirtual java/io/FileOutputStream/close()V
L10:
aload 6
invokevirtual java/io/FileInputStream/close()V
L12:
aload 0
invokevirtual java/io/File/length()J
aload 1
invokevirtual java/io/File/length()J
lcmp
ifeq L21
new java/io/IOException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Failed to copy full contents from '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "' to '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L4:
astore 0
L14:
aload 7
invokevirtual java/io/FileOutputStream/close()V
L15:
aload 0
athrow
L2:
astore 0
L17:
aload 6
invokevirtual java/io/FileInputStream/close()V
L18:
aload 0
athrow
L21:
iload 2
ifeq L22
aload 1
aload 0
invokevirtual java/io/File/lastModified()J
invokevirtual java/io/File/setLastModified(J)Z
pop
L22:
return
L11:
astore 7
goto L10
L16:
astore 1
goto L15
L13:
astore 6
goto L12
L19:
astore 1
goto L18
.limit locals 9
.limit stack 4
.end method

.method public static createKeyStore(Ljava/lang/String;[C)Ljava/security/KeyStore;
.throws java/lang/Exception
aload 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc ".bks"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L0
ldc "bks"
new org/spongycastle/jce/provider/BouncyCastleProvider
dup
invokespecial org/spongycastle/jce/provider/BouncyCastleProvider/<init>()V
invokestatic java/security/KeyStore/getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyStore;
astore 0
L1:
aload 0
aconst_null
aload 1
invokevirtual java/security/KeyStore/load(Ljava/io/InputStream;[C)V
aload 0
areturn
L0:
new kellinwood/security/zipsigner/optional/JksKeyStore
dup
invokespecial kellinwood/security/zipsigner/optional/JksKeyStore/<init>()V
astore 0
goto L1
.limit locals 2
.limit stack 3
.end method

.method public static deleteKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.throws java/lang/Exception
aload 0
aload 1
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/loadKeyStore(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;
astore 3
aload 3
aload 2
invokevirtual java/security/KeyStore/deleteEntry(Ljava/lang/String;)V
aload 3
aload 0
aload 1
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/writeKeyStore(Ljava/security/KeyStore;Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 4
.limit stack 3
.end method

.method public static getKeyEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore$Entry;
.throws java/lang/Exception
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L6 to L7 using L8
aconst_null
astore 6
aconst_null
astore 5
aload 6
astore 4
L0:
aload 0
aload 1
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/loadKeyStore(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;
astore 7
L1:
aload 6
astore 4
L3:
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/getInstance()Lkellinwood/security/zipsigner/optional/PasswordObfuscator;
aload 0
aload 2
aload 3
invokevirtual kellinwood/security/zipsigner/optional/PasswordObfuscator/decodeAliasPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[C
astore 0
L4:
aload 0
astore 4
L5:
new java/security/KeyStore$PasswordProtection
dup
aload 0
invokespecial java/security/KeyStore$PasswordProtection/<init>([C)V
astore 1
L6:
aload 7
aload 2
aload 1
invokevirtual java/security/KeyStore/getEntry(Ljava/lang/String;Ljava/security/KeyStore$ProtectionParameter;)Ljava/security/KeyStore$Entry;
astore 2
L7:
aload 0
ifnull L9
aload 0
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/flush([C)V
L9:
aload 1
ifnull L10
aload 1
invokevirtual java/security/KeyStore$PasswordProtection/destroy()V
L10:
aload 2
areturn
L2:
astore 2
aload 5
astore 1
aload 4
astore 0
L11:
aload 0
ifnull L12
aload 0
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/flush([C)V
L12:
aload 1
ifnull L13
aload 1
invokevirtual java/security/KeyStore$PasswordProtection/destroy()V
L13:
aload 2
athrow
L8:
astore 2
goto L11
.limit locals 8
.limit stack 4
.end method

.method public static getProvider()Ljava/security/Provider;
getstatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/provider Ljava/security/Provider;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static loadKeyStore(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;
.throws java/lang/Exception
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aconst_null
astore 3
aconst_null
astore 2
aload 1
ifnull L1
L0:
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/getInstance()Lkellinwood/security/zipsigner/optional/PasswordObfuscator;
aload 0
aload 1
invokevirtual kellinwood/security/zipsigner/optional/PasswordObfuscator/decodeKeystorePassword(Ljava/lang/String;Ljava/lang/String;)[C
astore 2
L1:
aload 2
astore 3
L3:
aload 0
aload 2
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/loadKeyStore(Ljava/lang/String;[C)Ljava/security/KeyStore;
astore 0
L4:
aload 2
ifnull L5
aload 2
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/flush([C)V
L5:
aload 0
areturn
L2:
astore 0
aload 3
ifnull L6
aload 3
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/flush([C)V
L6:
aload 0
athrow
.limit locals 4
.limit stack 3
.end method

.method public static loadKeyStore(Ljava/lang/String;[C)Ljava/security/KeyStore;
.throws java/lang/Exception
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L4
.catch java/lang/Exception from L5 to L6 using L7
L0:
new kellinwood/security/zipsigner/optional/JksKeyStore
dup
invokespecial kellinwood/security/zipsigner/optional/JksKeyStore/<init>()V
astore 2
L1:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 3
aload 2
aload 3
aload 1
invokevirtual java/security/KeyStore/load(Ljava/io/InputStream;[C)V
aload 3
invokevirtual java/io/FileInputStream/close()V
L3:
aload 2
areturn
L2:
astore 2
L5:
ldc "bks"
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/getProvider()Ljava/security/Provider;
invokestatic java/security/KeyStore/getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyStore;
astore 2
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 0
aload 2
aload 0
aload 1
invokevirtual java/security/KeyStore/load(Ljava/io/InputStream;[C)V
aload 0
invokevirtual java/io/FileInputStream/close()V
L6:
aload 2
areturn
L7:
astore 0
new java/lang/RuntimeException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Failed to load keystore: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L4:
astore 2
goto L5
.limit locals 4
.limit stack 4
.end method

.method public static renameKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.throws java/lang/Exception
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L2 using L2
.catch all from L10 to L11 using L2
.catch all from L12 to L13 using L2
.catch all from L14 to L15 using L2
.catch all from L16 to L17 using L2
aconst_null
astore 7
aload 7
astore 5
L0:
aload 0
aload 1
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/loadKeyStore(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;
astore 8
L1:
aload 3
astore 6
aload 7
astore 5
L3:
aload 8
instanceof kellinwood/security/zipsigner/optional/JksKeyStore
ifeq L6
L4:
aload 7
astore 5
L5:
aload 3
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 6
L6:
aload 7
astore 5
L7:
aload 8
aload 6
invokevirtual java/security/KeyStore/containsAlias(Ljava/lang/String;)Z
ifeq L18
L8:
aload 7
astore 5
L9:
new kellinwood/security/zipsigner/optional/KeyNameConflictException
dup
invokespecial kellinwood/security/zipsigner/optional/KeyNameConflictException/<init>()V
athrow
L2:
astore 0
aload 5
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/flush([C)V
aload 0
athrow
L18:
aload 7
astore 5
L10:
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/getInstance()Lkellinwood/security/zipsigner/optional/PasswordObfuscator;
aload 0
aload 2
aload 4
invokevirtual kellinwood/security/zipsigner/optional/PasswordObfuscator/decodeAliasPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[C
astore 3
L11:
aload 3
astore 5
L12:
aload 8
aload 6
aload 8
aload 2
aload 3
invokevirtual java/security/KeyStore/getKey(Ljava/lang/String;[C)Ljava/security/Key;
aload 3
iconst_1
anewarray java/security/cert/Certificate
dup
iconst_0
aload 8
aload 2
invokevirtual java/security/KeyStore/getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;
aastore
invokevirtual java/security/KeyStore/setKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V
L13:
aload 3
astore 5
L14:
aload 8
aload 2
invokevirtual java/security/KeyStore/deleteEntry(Ljava/lang/String;)V
L15:
aload 3
astore 5
L16:
aload 8
aload 0
aload 1
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/writeKeyStore(Ljava/security/KeyStore;Ljava/lang/String;Ljava/lang/String;)V
L17:
aload 3
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/flush([C)V
aload 6
areturn
.limit locals 9
.limit stack 9
.end method

.method public static renameTo(Ljava/io/File;Ljava/io/File;)V
.throws java/io/IOException
aload 0
aload 1
iconst_1
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/copyFile(Ljava/io/File;Ljava/io/File;Z)V
aload 0
invokevirtual java/io/File/delete()Z
ifne L0
new java/io/IOException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Failed to delete "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 2
.limit stack 4
.end method

.method public static setProvider(Ljava/security/Provider;)V
getstatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/provider Ljava/security/Provider;
ifnull L0
getstatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/provider Ljava/security/Provider;
invokevirtual java/security/Provider/getName()Ljava/lang/String;
invokestatic java/security/Security/removeProvider(Ljava/lang/String;)V
L0:
aload 0
putstatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/provider Ljava/security/Provider;
aload 0
invokestatic java/security/Security/addProvider(Ljava/security/Provider;)I
pop
return
.limit locals 1
.limit stack 1
.end method

.method public static validateKeyPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.throws java/lang/Exception
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
aconst_null
astore 4
aload 4
astore 3
L0:
aload 0
aconst_null
checkcast [C
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/loadKeyStore(Ljava/lang/String;[C)Ljava/security/KeyStore;
astore 5
L1:
aload 4
astore 3
L3:
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/getInstance()Lkellinwood/security/zipsigner/optional/PasswordObfuscator;
aload 0
aload 1
aload 2
invokevirtual kellinwood/security/zipsigner/optional/PasswordObfuscator/decodeAliasPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[C
astore 0
L4:
aload 0
astore 3
L5:
aload 5
aload 1
aload 0
invokevirtual java/security/KeyStore/getKey(Ljava/lang/String;[C)Ljava/security/Key;
pop
L6:
aload 0
ifnull L7
aload 0
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/flush([C)V
L7:
return
L2:
astore 0
aload 3
ifnull L8
aload 3
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/flush([C)V
L8:
aload 0
athrow
.limit locals 6
.limit stack 4
.end method

.method public static validateKeystorePassword(Ljava/lang/String;Ljava/lang/String;)V
.throws java/lang/Exception
.catch all from L0 to L1 using L2
L0:
aload 0
aload 1
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/loadKeyStore(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;
pop
L1:
iconst_0
ifeq L3
aconst_null
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/flush([C)V
L3:
return
L2:
astore 0
iconst_0
ifeq L4
aconst_null
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/flush([C)V
L4:
aload 0
athrow
.limit locals 2
.limit stack 2
.end method

.method public static writeKeyStore(Ljava/security/KeyStore;Ljava/lang/String;Ljava/lang/String;)V
.throws java/lang/Exception
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aconst_null
astore 3
L0:
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/getInstance()Lkellinwood/security/zipsigner/optional/PasswordObfuscator;
aload 1
aload 2
invokevirtual kellinwood/security/zipsigner/optional/PasswordObfuscator/decodeKeystorePassword(Ljava/lang/String;Ljava/lang/String;)[C
astore 2
L1:
aload 2
astore 3
L3:
aload 0
aload 1
aload 2
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/writeKeyStore(Ljava/security/KeyStore;Ljava/lang/String;[C)V
L4:
aload 2
ifnull L5
aload 2
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/flush([C)V
L5:
return
L2:
astore 0
aload 3
ifnull L6
aload 3
invokestatic kellinwood/security/zipsigner/optional/PasswordObfuscator/flush([C)V
L6:
aload 0
athrow
.limit locals 4
.limit stack 3
.end method

.method public static writeKeyStore(Ljava/security/KeyStore;Ljava/lang/String;[C)V
.throws java/lang/Exception
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L7
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 3
L0:
aload 3
invokevirtual java/io/File/exists()Z
ifeq L3
aload 3
invokevirtual java/io/File/getName()Ljava/lang/String;
aconst_null
aload 3
invokevirtual java/io/File/getParentFile()Ljava/io/File;
invokestatic java/io/File/createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
astore 1
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 4
aload 0
aload 4
aload 2
invokevirtual java/security/KeyStore/store(Ljava/io/OutputStream;[C)V
aload 4
invokevirtual java/io/FileOutputStream/flush()V
aload 4
invokevirtual java/io/FileOutputStream/close()V
aload 1
aload 3
invokestatic kellinwood/security/zipsigner/optional/KeyStoreFileManager/renameTo(Ljava/io/File;Ljava/io/File;)V
L1:
return
L3:
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 1
aload 0
aload 1
aload 2
invokevirtual java/security/KeyStore/store(Ljava/io/OutputStream;[C)V
aload 1
invokevirtual java/io/FileOutputStream/close()V
L4:
return
L2:
astore 0
L5:
new java/io/PrintWriter
dup
new java/io/FileWriter
dup
ldc "zipsigner-error"
ldc ".log"
aload 3
invokevirtual java/io/File/getParentFile()Ljava/io/File;
invokestatic java/io/File/createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
invokespecial java/io/FileWriter/<init>(Ljava/io/File;)V
invokespecial java/io/PrintWriter/<init>(Ljava/io/Writer;)V
astore 1
aload 0
aload 1
invokevirtual java/lang/Exception/printStackTrace(Ljava/io/PrintWriter;)V
aload 1
invokevirtual java/io/PrintWriter/flush()V
aload 1
invokevirtual java/io/PrintWriter/close()V
L6:
aload 0
athrow
L7:
astore 1
goto L6
.limit locals 5
.limit stack 7
.end method
