.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/optional/JKS
.super java/security/KeyStoreSpi

.field private static final 'MAGIC' I = -17957139


.field private static final 'PRIVATE_KEY' I = 1


.field private static final 'TRUSTED_CERT' I = 2


.field private final 'aliases' Ljava/util/Vector;

.field private final 'certChains' Ljava/util/HashMap;

.field private final 'dates' Ljava/util/HashMap;

.field private final 'privateKeys' Ljava/util/HashMap;

.field private final 'trustedCerts' Ljava/util/HashMap;

.method public <init>()V
aload 0
invokespecial java/security/KeyStoreSpi/<init>()V
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield kellinwood/security/zipsigner/optional/JKS/trustedCerts Ljava/util/HashMap;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield kellinwood/security/zipsigner/optional/JKS/privateKeys Ljava/util/HashMap;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield kellinwood/security/zipsigner/optional/JKS/certChains Ljava/util/HashMap;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield kellinwood/security/zipsigner/optional/JKS/dates Ljava/util/HashMap;
return
.limit locals 1
.limit stack 3
.end method

.method private static charsToBytes([C)[B
aload 0
arraylength
iconst_2
imul
newarray byte
astore 4
iconst_0
istore 1
iconst_0
istore 2
L0:
iload 1
aload 0
arraylength
if_icmpge L1
iload 2
iconst_1
iadd
istore 3
aload 4
iload 2
aload 0
iload 1
caload
bipush 8
iushr
i2b
bastore
iload 3
iconst_1
iadd
istore 2
aload 4
iload 3
aload 0
iload 1
caload
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 4
areturn
.limit locals 5
.limit stack 4
.end method

.method private static decryptKey([B[B)[B
.throws java/security/UnrecoverableKeyException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L2 using L2
L0:
new javax/crypto/EncryptedPrivateKeyInfo
dup
aload 0
invokespecial javax/crypto/EncryptedPrivateKeyInfo/<init>([B)V
invokevirtual javax/crypto/EncryptedPrivateKeyInfo/getEncryptedData()[B
astore 0
bipush 20
newarray byte
astore 5
aload 0
iconst_0
aload 5
iconst_0
bipush 20
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
bipush 20
newarray byte
astore 6
aload 0
aload 0
arraylength
bipush 20
isub
aload 6
iconst_0
bipush 20
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
arraylength
bipush 40
isub
newarray byte
astore 7
ldc "SHA1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 8
L1:
iconst_0
istore 2
L3:
iload 2
aload 7
arraylength
if_icmpge L9
aload 8
invokevirtual java/security/MessageDigest/reset()V
aload 8
aload 1
invokevirtual java/security/MessageDigest/update([B)V
aload 8
aload 5
invokevirtual java/security/MessageDigest/update([B)V
aload 8
aload 5
iconst_0
aload 5
arraylength
invokevirtual java/security/MessageDigest/digest([BII)I
pop
L4:
iconst_0
istore 4
iload 2
istore 3
L10:
iload 3
istore 2
L5:
iload 4
aload 5
arraylength
if_icmpge L3
L6:
iload 3
istore 2
L7:
iload 3
aload 7
arraylength
if_icmpge L3
L8:
aload 7
iload 3
aload 5
iload 4
baload
aload 0
iload 3
bipush 20
iadd
baload
ixor
i2b
bastore
iload 3
iconst_1
iadd
istore 3
iload 4
iconst_1
iadd
istore 4
goto L10
L9:
aload 8
invokevirtual java/security/MessageDigest/reset()V
aload 8
aload 1
invokevirtual java/security/MessageDigest/update([B)V
aload 8
aload 7
invokevirtual java/security/MessageDigest/update([B)V
aload 6
aload 8
invokevirtual java/security/MessageDigest/digest()[B
invokestatic java/security/MessageDigest/isEqual([B[B)Z
ifne L11
new java/security/UnrecoverableKeyException
dup
ldc "checksum mismatch"
invokespecial java/security/UnrecoverableKeyException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 0
new java/security/UnrecoverableKeyException
dup
aload 0
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
invokespecial java/security/UnrecoverableKeyException/<init>(Ljava/lang/String;)V
athrow
L11:
aload 7
areturn
.limit locals 9
.limit stack 6
.end method

.method private static encryptKey(Ljava/security/Key;[B)[B
.throws java/security/KeyStoreException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
L0:
ldc "SHA1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 5
ldc "SHA1PRNG"
invokestatic java/security/SecureRandom/getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;
pop
aload 0
invokeinterface java/security/Key/getEncoded()[B 0
astore 0
aload 0
arraylength
bipush 40
iadd
newarray byte
astore 6
bipush 20
invokestatic java/security/SecureRandom/getSeed(I)[B
astore 7
aload 7
iconst_0
aload 6
iconst_0
bipush 20
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L1:
iconst_0
istore 2
L3:
iload 2
aload 0
arraylength
if_icmpge L9
aload 5
invokevirtual java/security/MessageDigest/reset()V
aload 5
aload 1
invokevirtual java/security/MessageDigest/update([B)V
aload 5
aload 7
invokevirtual java/security/MessageDigest/update([B)V
aload 5
aload 7
iconst_0
aload 7
arraylength
invokevirtual java/security/MessageDigest/digest([BII)I
pop
L4:
iconst_0
istore 4
iload 2
istore 3
L11:
iload 3
istore 2
L5:
iload 4
aload 7
arraylength
if_icmpge L3
L6:
iload 3
istore 2
L7:
iload 3
aload 0
arraylength
if_icmpge L3
L8:
aload 6
iload 3
bipush 20
iadd
aload 7
iload 4
baload
aload 0
iload 3
baload
ixor
i2b
bastore
iload 3
iconst_1
iadd
istore 3
iload 4
iconst_1
iadd
istore 4
goto L11
L9:
aload 5
invokevirtual java/security/MessageDigest/reset()V
aload 5
aload 1
invokevirtual java/security/MessageDigest/update([B)V
aload 5
aload 0
invokevirtual java/security/MessageDigest/update([B)V
aload 5
aload 6
aload 6
arraylength
bipush 20
isub
bipush 20
invokevirtual java/security/MessageDigest/digest([BII)I
pop
new javax/crypto/EncryptedPrivateKeyInfo
dup
ldc "1.3.6.1.4.1.42.2.17.1.1"
aload 6
invokespecial javax/crypto/EncryptedPrivateKeyInfo/<init>(Ljava/lang/String;[B)V
invokevirtual javax/crypto/EncryptedPrivateKeyInfo/getEncoded()[B
astore 0
L10:
aload 0
areturn
L2:
astore 0
new java/security/KeyStoreException
dup
aload 0
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
invokespecial java/security/KeyStoreException/<init>(Ljava/lang/String;)V
athrow
.limit locals 8
.limit stack 5
.end method

.method private static readCert(Ljava/io/DataInputStream;)Ljava/security/cert/Certificate;
.throws java/io/IOException
.throws java/security/cert/CertificateException
.throws java/security/NoSuchAlgorithmException
aload 0
invokevirtual java/io/DataInputStream/readUTF()Ljava/lang/String;
astore 1
aload 0
invokevirtual java/io/DataInputStream/readInt()I
newarray byte
astore 2
aload 0
aload 2
invokevirtual java/io/DataInputStream/read([B)I
pop
aload 1
invokestatic java/security/cert/CertificateFactory/getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;
new java/io/ByteArrayInputStream
dup
aload 2
invokespecial java/io/ByteArrayInputStream/<init>([B)V
invokevirtual java/security/cert/CertificateFactory/generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
areturn
.limit locals 3
.limit stack 4
.end method

.method private static writeCert(Ljava/io/DataOutputStream;Ljava/security/cert/Certificate;)V
.throws java/io/IOException
.throws java/security/cert/CertificateException
aload 0
aload 1
invokevirtual java/security/cert/Certificate/getType()Ljava/lang/String;
invokevirtual java/io/DataOutputStream/writeUTF(Ljava/lang/String;)V
aload 1
invokevirtual java/security/cert/Certificate/getEncoded()[B
astore 1
aload 0
aload 1
arraylength
invokevirtual java/io/DataOutputStream/writeInt(I)V
aload 0
aload 1
invokevirtual java/io/DataOutputStream/write([B)V
return
.limit locals 2
.limit stack 2
.end method

.method public engineAliases()Ljava/util/Enumeration;
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
invokevirtual java/util/Vector/elements()Ljava/util/Enumeration;
areturn
.limit locals 1
.limit stack 1
.end method

.method public engineContainsAlias(Ljava/lang/String;)Z
aload 1
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
aload 1
invokevirtual java/util/Vector/contains(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public engineDeleteEntry(Ljava/lang/String;)V
.throws java/security/KeyStoreException
aload 1
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
aload 1
invokevirtual java/util/Vector/remove(Ljava/lang/Object;)Z
pop
return
.limit locals 2
.limit stack 2
.end method

.method public engineGetCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;
aload 1
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 1
aload 0
aload 1
invokevirtual kellinwood/security/zipsigner/optional/JKS/engineIsKeyEntry(Ljava/lang/String;)Z
ifeq L0
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/certChains Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast [Ljava/security/cert/Certificate;
checkcast [Ljava/security/cert/Certificate;
astore 2
aload 2
ifnull L0
aload 2
arraylength
ifle L0
aload 2
iconst_0
aaload
areturn
L0:
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/trustedCerts Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/security/cert/Certificate
areturn
.limit locals 3
.limit stack 2
.end method

.method public engineGetCertificateAlias(Ljava/security/cert/Certificate;)Ljava/lang/String;
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/trustedCerts Ljava/util/HashMap;
invokevirtual java/util/HashMap/keySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 3
aload 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/trustedCerts Ljava/util/HashMap;
aload 3
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
invokevirtual java/security/cert/Certificate/equals(Ljava/lang/Object;)Z
ifeq L0
aload 3
areturn
L1:
aconst_null
areturn
.limit locals 4
.limit stack 3
.end method

.method public engineGetCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;
aload 1
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/certChains Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast [Ljava/security/cert/Certificate;
checkcast [Ljava/security/cert/Certificate;
areturn
.limit locals 2
.limit stack 2
.end method

.method public engineGetCreationDate(Ljava/lang/String;)Ljava/util/Date;
aload 1
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/dates Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Date
areturn
.limit locals 2
.limit stack 2
.end method

.method public engineGetKey(Ljava/lang/String;[C)Ljava/security/Key;
.throws java/security/NoSuchAlgorithmException
.throws java/security/UnrecoverableKeyException
.catch java/security/spec/InvalidKeySpecException from L0 to L1 using L2
aload 1
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/privateKeys Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifne L3
aconst_null
areturn
L3:
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/privateKeys Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast [B
checkcast [B
aload 2
invokestatic kellinwood/security/zipsigner/optional/JKS/charsToBytes([C)[B
invokestatic kellinwood/security/zipsigner/optional/JKS/decryptKey([B[B)[B
astore 2
aload 0
aload 1
invokevirtual kellinwood/security/zipsigner/optional/JKS/engineGetCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;
astore 3
aload 3
arraylength
ifle L4
L0:
aload 3
iconst_0
aaload
invokevirtual java/security/cert/Certificate/getPublicKey()Ljava/security/PublicKey;
invokeinterface java/security/PublicKey/getAlgorithm()Ljava/lang/String; 0
invokestatic java/security/KeyFactory/getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;
new java/security/spec/PKCS8EncodedKeySpec
dup
aload 2
invokespecial java/security/spec/PKCS8EncodedKeySpec/<init>([B)V
invokevirtual java/security/KeyFactory/generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
astore 1
L1:
aload 1
areturn
L2:
astore 1
new java/security/UnrecoverableKeyException
dup
aload 1
invokevirtual java/security/spec/InvalidKeySpecException/getMessage()Ljava/lang/String;
invokespecial java/security/UnrecoverableKeyException/<init>(Ljava/lang/String;)V
athrow
L4:
new javax/crypto/spec/SecretKeySpec
dup
aload 2
aload 1
invokespecial javax/crypto/spec/SecretKeySpec/<init>([BLjava/lang/String;)V
areturn
.limit locals 4
.limit stack 4
.end method

.method public engineIsCertificateEntry(Ljava/lang/String;)Z
aload 1
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/trustedCerts Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public engineIsKeyEntry(Ljava/lang/String;)Z
aload 1
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/privateKeys Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public engineLoad(Ljava/io/InputStream;[C)V
.throws java/io/IOException
.throws java/security/NoSuchAlgorithmException
.throws java/security/cert/CertificateException
ldc "SHA"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 7
aload 2
ifnull L0
aload 7
aload 2
invokestatic kellinwood/security/zipsigner/optional/JKS/charsToBytes([C)[B
invokevirtual java/security/MessageDigest/update([B)V
L0:
aload 7
ldc "Mighty Aphrodite"
ldc "UTF-8"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
invokevirtual java/security/MessageDigest/update([B)V
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
invokevirtual java/util/Vector/clear()V
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/trustedCerts Ljava/util/HashMap;
invokevirtual java/util/HashMap/clear()V
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/privateKeys Ljava/util/HashMap;
invokevirtual java/util/HashMap/clear()V
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/certChains Ljava/util/HashMap;
invokevirtual java/util/HashMap/clear()V
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/dates Ljava/util/HashMap;
invokevirtual java/util/HashMap/clear()V
aload 1
ifnonnull L1
L2:
return
L1:
new java/io/DataInputStream
dup
new java/security/DigestInputStream
dup
aload 1
aload 7
invokespecial java/security/DigestInputStream/<init>(Ljava/io/InputStream;Ljava/security/MessageDigest;)V
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
astore 1
aload 1
invokevirtual java/io/DataInputStream/readInt()I
ldc_w -17957139
if_icmpeq L3
new java/io/IOException
dup
ldc "not a JavaKeyStore"
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 1
invokevirtual java/io/DataInputStream/readInt()I
pop
aload 1
invokevirtual java/io/DataInputStream/readInt()I
istore 5
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
iload 5
invokevirtual java/util/Vector/ensureCapacity(I)V
iload 5
ifge L4
new java/io/IOException
dup
ldc "negative entry count"
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L4:
iconst_0
istore 3
L5:
iload 3
iload 5
if_icmpge L6
aload 1
invokevirtual java/io/DataInputStream/readInt()I
istore 4
aload 1
invokevirtual java/io/DataInputStream/readUTF()Ljava/lang/String;
astore 8
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
aload 8
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/dates Ljava/util/HashMap;
aload 8
new java/util/Date
dup
aload 1
invokevirtual java/io/DataInputStream/readLong()J
invokespecial java/util/Date/<init>(J)V
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 4
tableswitch 1
L7
L8
default : L9
L9:
new java/io/IOException
dup
ldc "malformed key store"
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L7:
aload 1
invokevirtual java/io/DataInputStream/readInt()I
newarray byte
astore 9
aload 1
aload 9
invokevirtual java/io/DataInputStream/read([B)I
pop
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/privateKeys Ljava/util/HashMap;
aload 8
aload 9
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokevirtual java/io/DataInputStream/readInt()I
istore 6
iload 6
anewarray java/security/cert/Certificate
astore 9
iconst_0
istore 4
L10:
iload 4
iload 6
if_icmpge L11
aload 9
iload 4
aload 1
invokestatic kellinwood/security/zipsigner/optional/JKS/readCert(Ljava/io/DataInputStream;)Ljava/security/cert/Certificate;
aastore
iload 4
iconst_1
iadd
istore 4
goto L10
L11:
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/certChains Ljava/util/HashMap;
aload 8
aload 9
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L12:
iload 3
iconst_1
iadd
istore 3
goto L5
L8:
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/trustedCerts Ljava/util/HashMap;
aload 8
aload 1
invokestatic kellinwood/security/zipsigner/optional/JKS/readCert(Ljava/io/DataInputStream;)Ljava/security/cert/Certificate;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
goto L12
L6:
bipush 20
newarray byte
astore 8
aload 1
aload 8
invokevirtual java/io/DataInputStream/read([B)I
pop
aload 2
ifnull L2
aload 8
aload 7
invokevirtual java/security/MessageDigest/digest()[B
invokestatic java/security/MessageDigest/isEqual([B[B)Z
ifeq L2
new java/io/IOException
dup
ldc "signature not verified"
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
.limit locals 10
.limit stack 6
.end method

.method public engineSetCertificateEntry(Ljava/lang/String;Ljava/security/cert/Certificate;)V
.throws java/security/KeyStoreException
aload 1
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/privateKeys Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L0
new java/security/KeyStoreException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "\""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\" is a private key entry"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/security/KeyStoreException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
ifnonnull L1
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L1:
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/trustedCerts Ljava/util/HashMap;
aload 1
aload 2
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
aload 1
invokevirtual java/util/Vector/contains(Ljava/lang/Object;)Z
ifne L2
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/dates Ljava/util/HashMap;
aload 1
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
aload 1
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
L2:
return
.limit locals 3
.limit stack 4
.end method

.method public engineSetKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V
.throws java/security/KeyStoreException
aload 1
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/trustedCerts Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L0
new java/security/KeyStoreException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "\""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " is a trusted certificate entry"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/security/KeyStoreException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/privateKeys Ljava/util/HashMap;
aload 1
aload 2
aload 3
invokestatic kellinwood/security/zipsigner/optional/JKS/charsToBytes([C)[B
invokestatic kellinwood/security/zipsigner/optional/JKS/encryptKey(Ljava/security/Key;[B)[B
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 4
ifnull L1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/certChains Ljava/util/HashMap;
aload 1
aload 4
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L2:
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
aload 1
invokevirtual java/util/Vector/contains(Ljava/lang/Object;)Z
ifne L3
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/dates Ljava/util/HashMap;
aload 1
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
aload 1
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
L3:
return
L1:
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/certChains Ljava/util/HashMap;
aload 1
iconst_0
anewarray java/security/cert/Certificate
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
goto L2
.limit locals 5
.limit stack 4
.end method

.method public engineSetKeyEntry(Ljava/lang/String;[B[Ljava/security/cert/Certificate;)V
.throws java/security/KeyStoreException
.catch java/io/IOException from L0 to L1 using L2
aload 1
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/trustedCerts Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L0
new java/security/KeyStoreException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "\""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\" is a trusted certificate entry"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/security/KeyStoreException/<init>(Ljava/lang/String;)V
athrow
L0:
new javax/crypto/EncryptedPrivateKeyInfo
dup
aload 2
invokespecial javax/crypto/EncryptedPrivateKeyInfo/<init>([B)V
pop
L1:
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/privateKeys Ljava/util/HashMap;
aload 1
aload 2
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 3
ifnull L3
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/certChains Ljava/util/HashMap;
aload 1
aload 3
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
aload 1
invokevirtual java/util/Vector/contains(Ljava/lang/Object;)Z
ifne L5
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/dates Ljava/util/HashMap;
aload 1
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
aload 1
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
L5:
return
L2:
astore 1
new java/security/KeyStoreException
dup
ldc "encoded key is not an EncryptedPrivateKeyInfo"
invokespecial java/security/KeyStoreException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/certChains Ljava/util/HashMap;
aload 1
iconst_0
anewarray java/security/cert/Certificate
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
goto L4
.limit locals 4
.limit stack 4
.end method

.method public engineSize()I
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public engineStore(Ljava/io/OutputStream;[C)V
.throws java/io/IOException
.throws java/security/NoSuchAlgorithmException
.throws java/security/cert/CertificateException
ldc "SHA1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 4
aload 4
aload 2
invokestatic kellinwood/security/zipsigner/optional/JKS/charsToBytes([C)[B
invokevirtual java/security/MessageDigest/update([B)V
aload 4
ldc "Mighty Aphrodite"
ldc "UTF-8"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
invokevirtual java/security/MessageDigest/update([B)V
new java/io/DataOutputStream
dup
new java/security/DigestOutputStream
dup
aload 1
aload 4
invokespecial java/security/DigestOutputStream/<init>(Ljava/io/OutputStream;Ljava/security/MessageDigest;)V
invokespecial java/io/DataOutputStream/<init>(Ljava/io/OutputStream;)V
astore 1
aload 1
ldc_w -17957139
invokevirtual java/io/DataOutputStream/writeInt(I)V
aload 1
iconst_2
invokevirtual java/io/DataOutputStream/writeInt(I)V
aload 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
invokevirtual java/io/DataOutputStream/writeInt(I)V
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/aliases Ljava/util/Vector;
invokevirtual java/util/Vector/elements()Ljava/util/Enumeration;
astore 2
L0:
aload 2
invokeinterface java/util/Enumeration/hasMoreElements()Z 0
ifeq L1
aload 2
invokeinterface java/util/Enumeration/nextElement()Ljava/lang/Object; 0
checkcast java/lang/String
astore 5
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/trustedCerts Ljava/util/HashMap;
aload 5
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L2
aload 1
iconst_2
invokevirtual java/io/DataOutputStream/writeInt(I)V
aload 1
aload 5
invokevirtual java/io/DataOutputStream/writeUTF(Ljava/lang/String;)V
aload 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/dates Ljava/util/HashMap;
aload 5
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Date
invokevirtual java/util/Date/getTime()J
invokevirtual java/io/DataOutputStream/writeLong(J)V
aload 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/trustedCerts Ljava/util/HashMap;
aload 5
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/security/cert/Certificate
invokestatic kellinwood/security/zipsigner/optional/JKS/writeCert(Ljava/io/DataOutputStream;Ljava/security/cert/Certificate;)V
goto L0
L2:
aload 1
iconst_1
invokevirtual java/io/DataOutputStream/writeInt(I)V
aload 1
aload 5
invokevirtual java/io/DataOutputStream/writeUTF(Ljava/lang/String;)V
aload 1
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/dates Ljava/util/HashMap;
aload 5
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Date
invokevirtual java/util/Date/getTime()J
invokevirtual java/io/DataOutputStream/writeLong(J)V
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/privateKeys Ljava/util/HashMap;
aload 5
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast [B
checkcast [B
astore 6
aload 1
aload 6
arraylength
invokevirtual java/io/DataOutputStream/writeInt(I)V
aload 1
aload 6
invokevirtual java/io/DataOutputStream/write([B)V
aload 0
getfield kellinwood/security/zipsigner/optional/JKS/certChains Ljava/util/HashMap;
aload 5
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast [Ljava/security/cert/Certificate;
checkcast [Ljava/security/cert/Certificate;
astore 5
aload 1
aload 5
arraylength
invokevirtual java/io/DataOutputStream/writeInt(I)V
iconst_0
istore 3
L3:
iload 3
aload 5
arraylength
if_icmpge L0
aload 1
aload 5
iload 3
aaload
invokestatic kellinwood/security/zipsigner/optional/JKS/writeCert(Ljava/io/DataOutputStream;Ljava/security/cert/Certificate;)V
iload 3
iconst_1
iadd
istore 3
goto L3
L1:
aload 1
aload 4
invokevirtual java/security/MessageDigest/digest()[B
invokevirtual java/io/DataOutputStream/write([B)V
return
.limit locals 7
.limit stack 6
.end method
