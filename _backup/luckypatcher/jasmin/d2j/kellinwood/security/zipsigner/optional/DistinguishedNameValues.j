.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/optional/DistinguishedNameValues
.super java/util/LinkedHashMap
.signature "Ljava/util/LinkedHashMap<Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;>;"

.method public <init>()V
aload 0
invokespecial java/util/LinkedHashMap/<init>()V
aload 0
getstatic org/spongycastle/asn1/x500/style/BCStyle/C Lorg/spongycastle/asn1/ASN1ObjectIdentifier;
aconst_null
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
pop
aload 0
getstatic org/spongycastle/asn1/x500/style/BCStyle/ST Lorg/spongycastle/asn1/ASN1ObjectIdentifier;
aconst_null
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
pop
aload 0
getstatic org/spongycastle/asn1/x500/style/BCStyle/L Lorg/spongycastle/asn1/ASN1ObjectIdentifier;
aconst_null
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
pop
aload 0
getstatic org/spongycastle/asn1/x500/style/BCStyle/STREET Lorg/spongycastle/asn1/ASN1ObjectIdentifier;
aconst_null
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
pop
aload 0
getstatic org/spongycastle/asn1/x500/style/BCStyle/O Lorg/spongycastle/asn1/ASN1ObjectIdentifier;
aconst_null
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
pop
aload 0
getstatic org/spongycastle/asn1/x500/style/BCStyle/OU Lorg/spongycastle/asn1/ASN1ObjectIdentifier;
aconst_null
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
pop
aload 0
getstatic org/spongycastle/asn1/x500/style/BCStyle/CN Lorg/spongycastle/asn1/ASN1ObjectIdentifier;
aconst_null
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
pop
return
.limit locals 1
.limit stack 3
.end method

.method public getPrincipal()Lorg/spongycastle/jce/X509Principal;
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
astore 1
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
astore 2
aload 0
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/entrySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 4
aload 4
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
ifnull L0
aload 4
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/lang/String
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
aload 1
aload 4
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
aload 2
aload 4
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
goto L0
L1:
new org/spongycastle/jce/X509Principal
dup
aload 1
aload 2
invokespecial org/spongycastle/jce/X509Principal/<init>(Ljava/util/Vector;Ljava/util/Vector;)V
areturn
.limit locals 5
.limit stack 4
.end method

.method public volatile synthetic put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
checkcast org/spongycastle/asn1/ASN1ObjectIdentifier
aload 2
checkcast java/lang/String
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 3
.end method

.method public put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
aload 2
astore 3
aload 2
ifnull L0
aload 2
astore 3
aload 2
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aconst_null
astore 3
L0:
aload 0
aload 1
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/containsKey(Ljava/lang/Object;)Z
ifeq L1
aload 0
aload 1
aload 3
invokespecial java/util/LinkedHashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 3
areturn
L1:
aload 0
aload 1
aload 3
invokespecial java/util/LinkedHashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 3
areturn
.limit locals 4
.limit stack 3
.end method

.method public setCommonName(Ljava/lang/String;)V
aload 0
getstatic org/spongycastle/asn1/x500/style/BCStyle/CN Lorg/spongycastle/asn1/ASN1ObjectIdentifier;
aload 1
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
pop
return
.limit locals 2
.limit stack 3
.end method

.method public setCountry(Ljava/lang/String;)V
aload 0
getstatic org/spongycastle/asn1/x500/style/BCStyle/C Lorg/spongycastle/asn1/ASN1ObjectIdentifier;
aload 1
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
pop
return
.limit locals 2
.limit stack 3
.end method

.method public setLocality(Ljava/lang/String;)V
aload 0
getstatic org/spongycastle/asn1/x500/style/BCStyle/L Lorg/spongycastle/asn1/ASN1ObjectIdentifier;
aload 1
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
pop
return
.limit locals 2
.limit stack 3
.end method

.method public setOrganization(Ljava/lang/String;)V
aload 0
getstatic org/spongycastle/asn1/x500/style/BCStyle/O Lorg/spongycastle/asn1/ASN1ObjectIdentifier;
aload 1
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
pop
return
.limit locals 2
.limit stack 3
.end method

.method public setOrganizationalUnit(Ljava/lang/String;)V
aload 0
getstatic org/spongycastle/asn1/x500/style/BCStyle/OU Lorg/spongycastle/asn1/ASN1ObjectIdentifier;
aload 1
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
pop
return
.limit locals 2
.limit stack 3
.end method

.method public setState(Ljava/lang/String;)V
aload 0
getstatic org/spongycastle/asn1/x500/style/BCStyle/ST Lorg/spongycastle/asn1/ASN1ObjectIdentifier;
aload 1
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
pop
return
.limit locals 2
.limit stack 3
.end method

.method public setStreet(Ljava/lang/String;)V
aload 0
getstatic org/spongycastle/asn1/x500/style/BCStyle/STREET Lorg/spongycastle/asn1/ASN1ObjectIdentifier;
aload 1
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/put(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Ljava/lang/String;
pop
return
.limit locals 2
.limit stack 3
.end method

.method public size()I
iconst_0
istore 1
aload 0
invokevirtual kellinwood/security/zipsigner/optional/DistinguishedNameValues/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
ifnull L0
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 1
ireturn
.limit locals 3
.limit stack 2
.end method
