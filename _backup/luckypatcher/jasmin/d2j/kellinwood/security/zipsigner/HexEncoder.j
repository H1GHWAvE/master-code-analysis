.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/HexEncoder
.super java/lang/Object

.field protected final 'decodingTable' [B

.field protected final 'encodingTable' [B

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
bipush 16
newarray byte
dup
iconst_0
ldc_w 48
bastore
dup
iconst_1
ldc_w 49
bastore
dup
iconst_2
ldc_w 50
bastore
dup
iconst_3
ldc_w 51
bastore
dup
iconst_4
ldc_w 52
bastore
dup
iconst_5
ldc_w 53
bastore
dup
bipush 6
ldc_w 54
bastore
dup
bipush 7
ldc_w 55
bastore
dup
bipush 8
ldc_w 56
bastore
dup
bipush 9
ldc_w 57
bastore
dup
bipush 10
ldc_w 97
bastore
dup
bipush 11
ldc_w 98
bastore
dup
bipush 12
ldc_w 99
bastore
dup
bipush 13
ldc_w 100
bastore
dup
bipush 14
ldc_w 101
bastore
dup
bipush 15
ldc_w 102
bastore
putfield kellinwood/security/zipsigner/HexEncoder/encodingTable [B
aload 0
sipush 128
newarray byte
putfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
aload 0
invokevirtual kellinwood/security/zipsigner/HexEncoder/initialiseDecodingTable()V
return
.limit locals 1
.limit stack 5
.end method

.method private ignore(C)Z
iload 1
bipush 10
if_icmpeq L0
iload 1
bipush 13
if_icmpeq L0
iload 1
bipush 9
if_icmpeq L0
iload 1
bipush 32
if_icmpne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public decode(Ljava/lang/String;Ljava/io/OutputStream;)I
.throws java/io/IOException
iconst_0
istore 5
aload 1
invokevirtual java/lang/String/length()I
istore 4
L0:
iload 4
ifle L1
aload 0
aload 1
iload 4
iconst_1
isub
invokevirtual java/lang/String/charAt(I)C
invokespecial kellinwood/security/zipsigner/HexEncoder/ignore(C)Z
ifne L2
L1:
iconst_0
istore 3
L3:
iload 3
iload 4
if_icmpge L4
L5:
iload 3
iload 4
if_icmpge L6
aload 0
aload 1
iload 3
invokevirtual java/lang/String/charAt(I)C
invokespecial kellinwood/security/zipsigner/HexEncoder/ignore(C)Z
ifeq L6
iload 3
iconst_1
iadd
istore 3
goto L5
L2:
iload 4
iconst_1
isub
istore 4
goto L0
L6:
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
aload 1
iload 3
invokevirtual java/lang/String/charAt(I)C
baload
istore 7
iload 3
iconst_1
iadd
istore 3
L7:
iload 3
iload 4
if_icmpge L8
aload 0
aload 1
iload 3
invokevirtual java/lang/String/charAt(I)C
invokespecial kellinwood/security/zipsigner/HexEncoder/ignore(C)Z
ifeq L8
iload 3
iconst_1
iadd
istore 3
goto L7
L8:
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
astore 8
iload 3
iconst_1
iadd
istore 6
aload 2
iload 7
iconst_4
ishl
aload 8
aload 1
iload 3
invokevirtual java/lang/String/charAt(I)C
baload
ior
invokevirtual java/io/OutputStream/write(I)V
iload 5
iconst_1
iadd
istore 5
iload 6
istore 3
goto L3
L4:
iload 5
ireturn
.limit locals 9
.limit stack 5
.end method

.method public decode([BIILjava/io/OutputStream;)I
.throws java/io/IOException
iconst_0
istore 5
iload 2
iload 3
iadd
istore 3
L0:
iload 3
iload 2
if_icmple L1
aload 0
aload 1
iload 3
iconst_1
isub
baload
i2c
invokespecial kellinwood/security/zipsigner/HexEncoder/ignore(C)Z
ifne L2
L1:
iload 2
iload 3
if_icmpge L3
L4:
iload 2
iload 3
if_icmpge L5
aload 0
aload 1
iload 2
baload
i2c
invokespecial kellinwood/security/zipsigner/HexEncoder/ignore(C)Z
ifeq L5
iload 2
iconst_1
iadd
istore 2
goto L4
L2:
iload 3
iconst_1
isub
istore 3
goto L0
L5:
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
aload 1
iload 2
baload
baload
istore 7
iload 2
iconst_1
iadd
istore 2
L6:
iload 2
iload 3
if_icmpge L7
aload 0
aload 1
iload 2
baload
i2c
invokespecial kellinwood/security/zipsigner/HexEncoder/ignore(C)Z
ifeq L7
iload 2
iconst_1
iadd
istore 2
goto L6
L7:
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
astore 8
iload 2
iconst_1
iadd
istore 6
aload 4
iload 7
iconst_4
ishl
aload 8
aload 1
iload 2
baload
baload
ior
invokevirtual java/io/OutputStream/write(I)V
iload 5
iconst_1
iadd
istore 5
iload 6
istore 2
goto L1
L3:
iload 5
ireturn
.limit locals 9
.limit stack 5
.end method

.method public encode([BIILjava/io/OutputStream;)I
.throws java/io/IOException
iload 2
istore 5
L0:
iload 5
iload 2
iload 3
iadd
if_icmpge L1
aload 1
iload 5
baload
sipush 255
iand
istore 6
aload 4
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/encodingTable [B
iload 6
iconst_4
iushr
baload
invokevirtual java/io/OutputStream/write(I)V
aload 4
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/encodingTable [B
iload 6
bipush 15
iand
baload
invokevirtual java/io/OutputStream/write(I)V
iload 5
iconst_1
iadd
istore 5
goto L0
L1:
iload 3
iconst_2
imul
ireturn
.limit locals 7
.limit stack 4
.end method

.method protected initialiseDecodingTable()V
iconst_0
istore 1
L0:
iload 1
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/encodingTable [B
arraylength
if_icmpge L1
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/encodingTable [B
iload 1
baload
iload 1
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
bipush 65
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
bipush 97
baload
bastore
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
bipush 66
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
bipush 98
baload
bastore
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
bipush 67
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
bipush 99
baload
bastore
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
bipush 68
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
bipush 100
baload
bastore
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
bipush 69
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
bipush 101
baload
bastore
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
bipush 70
aload 0
getfield kellinwood/security/zipsigner/HexEncoder/decodingTable [B
bipush 102
baload
bastore
return
.limit locals 2
.limit stack 4
.end method
