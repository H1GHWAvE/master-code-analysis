.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/ZipSignature
.super java/lang/Object

.field 'afterAlgorithmIdBytes' [B

.field 'algorithmIdBytes' [B

.field 'beforeAlgorithmIdBytes' [B

.field 'cipher' Ljavax/crypto/Cipher;

.field 'md' Ljava/security/MessageDigest;

.method public <init>()V
.throws java/io/IOException
.throws java/security/GeneralSecurityException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_2
newarray byte
dup
iconst_0
ldc_w 48
bastore
dup
iconst_1
ldc_w 33
bastore
putfield kellinwood/security/zipsigner/ZipSignature/beforeAlgorithmIdBytes [B
aload 0
bipush 11
newarray byte
dup
iconst_0
ldc_w 48
bastore
dup
iconst_1
ldc_w 9
bastore
dup
iconst_2
ldc_w 6
bastore
dup
iconst_3
ldc_w 5
bastore
dup
iconst_4
ldc_w 43
bastore
dup
iconst_5
ldc_w 14
bastore
dup
bipush 6
ldc_w 3
bastore
dup
bipush 7
ldc_w 2
bastore
dup
bipush 8
ldc_w 26
bastore
dup
bipush 9
ldc_w 5
bastore
dup
bipush 10
ldc_w 0
bastore
putfield kellinwood/security/zipsigner/ZipSignature/algorithmIdBytes [B
aload 0
iconst_2
newarray byte
dup
iconst_0
ldc_w 4
bastore
dup
iconst_1
ldc_w 20
bastore
putfield kellinwood/security/zipsigner/ZipSignature/afterAlgorithmIdBytes [B
aload 0
ldc "SHA1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
putfield kellinwood/security/zipsigner/ZipSignature/md Ljava/security/MessageDigest;
aload 0
ldc "RSA/ECB/PKCS1Padding"
invokestatic javax/crypto/Cipher/getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
putfield kellinwood/security/zipsigner/ZipSignature/cipher Ljavax/crypto/Cipher;
return
.limit locals 1
.limit stack 5
.end method

.method public initSign(Ljava/security/PrivateKey;)V
.throws java/security/InvalidKeyException
aload 0
getfield kellinwood/security/zipsigner/ZipSignature/cipher Ljavax/crypto/Cipher;
iconst_1
aload 1
invokevirtual javax/crypto/Cipher/init(ILjava/security/Key;)V
return
.limit locals 2
.limit stack 3
.end method

.method public sign()[B
.throws javax/crypto/BadPaddingException
.throws javax/crypto/IllegalBlockSizeException
aload 0
getfield kellinwood/security/zipsigner/ZipSignature/cipher Ljavax/crypto/Cipher;
aload 0
getfield kellinwood/security/zipsigner/ZipSignature/beforeAlgorithmIdBytes [B
invokevirtual javax/crypto/Cipher/update([B)[B
pop
aload 0
getfield kellinwood/security/zipsigner/ZipSignature/cipher Ljavax/crypto/Cipher;
aload 0
getfield kellinwood/security/zipsigner/ZipSignature/algorithmIdBytes [B
invokevirtual javax/crypto/Cipher/update([B)[B
pop
aload 0
getfield kellinwood/security/zipsigner/ZipSignature/cipher Ljavax/crypto/Cipher;
aload 0
getfield kellinwood/security/zipsigner/ZipSignature/afterAlgorithmIdBytes [B
invokevirtual javax/crypto/Cipher/update([B)[B
pop
aload 0
getfield kellinwood/security/zipsigner/ZipSignature/cipher Ljavax/crypto/Cipher;
aload 0
getfield kellinwood/security/zipsigner/ZipSignature/md Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/digest()[B
invokevirtual javax/crypto/Cipher/update([B)[B
pop
aload 0
getfield kellinwood/security/zipsigner/ZipSignature/cipher Ljavax/crypto/Cipher;
invokevirtual javax/crypto/Cipher/doFinal()[B
areturn
.limit locals 1
.limit stack 2
.end method

.method public update([B)V
aload 0
getfield kellinwood/security/zipsigner/ZipSignature/md Ljava/security/MessageDigest;
aload 1
invokevirtual java/security/MessageDigest/update([B)V
return
.limit locals 2
.limit stack 2
.end method

.method public update([BII)V
aload 0
getfield kellinwood/security/zipsigner/ZipSignature/md Ljava/security/MessageDigest;
aload 1
iload 2
iload 3
invokevirtual java/security/MessageDigest/update([BII)V
return
.limit locals 4
.limit stack 4
.end method
