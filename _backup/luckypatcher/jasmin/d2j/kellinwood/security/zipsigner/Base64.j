.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/Base64
.super java/lang/Object

.field static 'aDecodeMethod' Ljava/lang/reflect/Method;

.field static 'aEncodeMethod' Ljava/lang/reflect/Method;

.field static 'bDecodeMethod' Ljava/lang/reflect/Method;

.field static 'bDecoder' Ljava/lang/Object;

.field static 'bEncodeMethod' Ljava/lang/reflect/Method;

.field static 'bEncoder' Ljava/lang/Object;

.field static 'logger' Lkellinwood/logging/LoggerInterface;

.method static <clinit>()V
.catch java/lang/ClassNotFoundException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch java/lang/ClassNotFoundException from L1 to L4 using L5
.catch java/lang/Exception from L1 to L4 using L6
aconst_null
putstatic kellinwood/security/zipsigner/Base64/aEncodeMethod Ljava/lang/reflect/Method;
aconst_null
putstatic kellinwood/security/zipsigner/Base64/aDecodeMethod Ljava/lang/reflect/Method;
aconst_null
putstatic kellinwood/security/zipsigner/Base64/bEncoder Ljava/lang/Object;
aconst_null
putstatic kellinwood/security/zipsigner/Base64/bEncodeMethod Ljava/lang/reflect/Method;
aconst_null
putstatic kellinwood/security/zipsigner/Base64/bDecoder Ljava/lang/Object;
aconst_null
putstatic kellinwood/security/zipsigner/Base64/bDecodeMethod Ljava/lang/reflect/Method;
aconst_null
putstatic kellinwood/security/zipsigner/Base64/logger Lkellinwood/logging/LoggerInterface;
ldc kellinwood/security/zipsigner/Base64
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic kellinwood/logging/LoggerManager/getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
putstatic kellinwood/security/zipsigner/Base64/logger Lkellinwood/logging/LoggerInterface;
L0:
ldc "android.util.Base64"
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
astore 0
aload 0
ldc "encode"
iconst_2
anewarray java/lang/Class
dup
iconst_0
ldc [B
aastore
dup
iconst_1
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putstatic kellinwood/security/zipsigner/Base64/aEncodeMethod Ljava/lang/reflect/Method;
aload 0
ldc "decode"
iconst_2
anewarray java/lang/Class
dup
iconst_0
ldc [B
aastore
dup
iconst_1
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putstatic kellinwood/security/zipsigner/Base64/aDecodeMethod Ljava/lang/reflect/Method;
getstatic kellinwood/security/zipsigner/Base64/logger Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " is available."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/info(Ljava/lang/String;)V 1
L1:
ldc "org.bouncycastle.util.encoders.Base64Encoder"
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
astore 0
aload 0
invokevirtual java/lang/Class/newInstance()Ljava/lang/Object;
putstatic kellinwood/security/zipsigner/Base64/bEncoder Ljava/lang/Object;
aload 0
ldc "encode"
iconst_4
anewarray java/lang/Class
dup
iconst_0
ldc [B
aastore
dup
iconst_1
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
dup
iconst_2
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
dup
iconst_3
ldc java/io/OutputStream
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putstatic kellinwood/security/zipsigner/Base64/bEncodeMethod Ljava/lang/reflect/Method;
getstatic kellinwood/security/zipsigner/Base64/logger Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " is available."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/info(Ljava/lang/String;)V 1
aload 0
ldc "decode"
iconst_4
anewarray java/lang/Class
dup
iconst_0
ldc [B
aastore
dup
iconst_1
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
dup
iconst_2
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
dup
iconst_3
ldc java/io/OutputStream
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putstatic kellinwood/security/zipsigner/Base64/bDecodeMethod Ljava/lang/reflect/Method;
L4:
getstatic kellinwood/security/zipsigner/Base64/aEncodeMethod Ljava/lang/reflect/Method;
ifnonnull L7
getstatic kellinwood/security/zipsigner/Base64/bEncodeMethod Ljava/lang/reflect/Method;
ifnonnull L7
new java/lang/IllegalStateException
dup
ldc "No base64 encoder implementation is available."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L3:
astore 0
getstatic kellinwood/security/zipsigner/Base64/logger Lkellinwood/logging/LoggerInterface;
ldc "Failed to initialize use of android.util.Base64"
aload 0
invokeinterface kellinwood/logging/LoggerInterface/error(Ljava/lang/String;Ljava/lang/Throwable;)V 2
goto L1
L6:
astore 0
getstatic kellinwood/security/zipsigner/Base64/logger Lkellinwood/logging/LoggerInterface;
ldc "Failed to initialize use of org.bouncycastle.util.encoders.Base64Encoder"
aload 0
invokeinterface kellinwood/logging/LoggerInterface/error(Ljava/lang/String;Ljava/lang/Throwable;)V 2
goto L4
L7:
return
L5:
astore 0
goto L4
L2:
astore 0
goto L1
.limit locals 1
.limit stack 6
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static decode([B)[B
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
L0:
getstatic kellinwood/security/zipsigner/Base64/aDecodeMethod Ljava/lang/reflect/Method;
ifnull L1
getstatic kellinwood/security/zipsigner/Base64/aDecodeMethod Ljava/lang/reflect/Method;
aconst_null
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
dup
iconst_1
iconst_2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast [B
checkcast [B
areturn
L1:
getstatic kellinwood/security/zipsigner/Base64/bDecodeMethod Ljava/lang/reflect/Method;
ifnull L4
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 1
getstatic kellinwood/security/zipsigner/Base64/bDecodeMethod Ljava/lang/reflect/Method;
getstatic kellinwood/security/zipsigner/Base64/bEncoder Ljava/lang/Object;
iconst_4
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
dup
iconst_1
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
aload 0
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_3
aload 1
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
astore 0
L3:
aload 0
areturn
L2:
astore 0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L4:
new java/lang/IllegalStateException
dup
ldc "No base64 encoder implementation is available."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 6
.end method

.method public static encode([B)Ljava/lang/String;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
L0:
getstatic kellinwood/security/zipsigner/Base64/aEncodeMethod Ljava/lang/reflect/Method;
ifnull L1
new java/lang/String
dup
getstatic kellinwood/security/zipsigner/Base64/aEncodeMethod Ljava/lang/reflect/Method;
aconst_null
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
dup
iconst_1
iconst_2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast [B
checkcast [B
invokespecial java/lang/String/<init>([B)V
areturn
L1:
getstatic kellinwood/security/zipsigner/Base64/bEncodeMethod Ljava/lang/reflect/Method;
ifnull L4
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 1
getstatic kellinwood/security/zipsigner/Base64/bEncodeMethod Ljava/lang/reflect/Method;
getstatic kellinwood/security/zipsigner/Base64/bEncoder Ljava/lang/Object;
iconst_4
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
dup
iconst_1
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
aload 0
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_3
aload 1
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
new java/lang/String
dup
aload 1
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
invokespecial java/lang/String/<init>([B)V
astore 0
L3:
aload 0
areturn
L2:
astore 0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L4:
new java/lang/IllegalStateException
dup
ldc "No base64 encoder implementation is available."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 8
.end method
