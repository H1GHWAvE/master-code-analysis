.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/ProgressHelper
.super java/lang/Object

.field private 'listeners' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Lkellinwood/security/zipsigner/ProgressListener;>;"

.field private 'progressCurrentItem' I

.field private 'progressEvent' Lkellinwood/security/zipsigner/ProgressEvent;

.field private 'progressTotalItems' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield kellinwood/security/zipsigner/ProgressHelper/progressTotalItems I
aload 0
iconst_0
putfield kellinwood/security/zipsigner/ProgressHelper/progressCurrentItem I
aload 0
new kellinwood/security/zipsigner/ProgressEvent
dup
invokespecial kellinwood/security/zipsigner/ProgressEvent/<init>()V
putfield kellinwood/security/zipsigner/ProgressHelper/progressEvent Lkellinwood/security/zipsigner/ProgressEvent;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield kellinwood/security/zipsigner/ProgressHelper/listeners Ljava/util/ArrayList;
return
.limit locals 1
.limit stack 3
.end method

.method public addProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield kellinwood/security/zipsigner/ProgressHelper/listeners Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clone()Ljava/lang/Object;
checkcast java/util/ArrayList
astore 2
aload 2
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
aload 2
putfield kellinwood/security/zipsigner/ProgressHelper/listeners Ljava/util/ArrayList;
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public getProgressCurrentItem()I
aload 0
getfield kellinwood/security/zipsigner/ProgressHelper/progressCurrentItem I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getProgressTotalItems()I
aload 0
getfield kellinwood/security/zipsigner/ProgressHelper/progressTotalItems I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public initProgress()V
aload 0
sipush 10000
putfield kellinwood/security/zipsigner/ProgressHelper/progressTotalItems I
aload 0
iconst_0
putfield kellinwood/security/zipsigner/ProgressHelper/progressCurrentItem I
return
.limit locals 1
.limit stack 2
.end method

.method public progress(ILjava/lang/String;)V
aload 0
aload 0
getfield kellinwood/security/zipsigner/ProgressHelper/progressCurrentItem I
iconst_1
iadd
putfield kellinwood/security/zipsigner/ProgressHelper/progressCurrentItem I
aload 0
getfield kellinwood/security/zipsigner/ProgressHelper/progressTotalItems I
ifne L0
iconst_0
istore 3
L1:
aload 0
getfield kellinwood/security/zipsigner/ProgressHelper/listeners Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 4
L2:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast kellinwood/security/zipsigner/ProgressListener
astore 5
aload 0
getfield kellinwood/security/zipsigner/ProgressHelper/progressEvent Lkellinwood/security/zipsigner/ProgressEvent;
aload 2
invokevirtual kellinwood/security/zipsigner/ProgressEvent/setMessage(Ljava/lang/String;)V
aload 0
getfield kellinwood/security/zipsigner/ProgressHelper/progressEvent Lkellinwood/security/zipsigner/ProgressEvent;
iload 3
invokevirtual kellinwood/security/zipsigner/ProgressEvent/setPercentDone(I)V
aload 0
getfield kellinwood/security/zipsigner/ProgressHelper/progressEvent Lkellinwood/security/zipsigner/ProgressEvent;
iload 1
invokevirtual kellinwood/security/zipsigner/ProgressEvent/setPriority(I)V
aload 5
aload 0
getfield kellinwood/security/zipsigner/ProgressHelper/progressEvent Lkellinwood/security/zipsigner/ProgressEvent;
invokeinterface kellinwood/security/zipsigner/ProgressListener/onProgress(Lkellinwood/security/zipsigner/ProgressEvent;)V 1
goto L2
L0:
aload 0
getfield kellinwood/security/zipsigner/ProgressHelper/progressCurrentItem I
bipush 100
imul
aload 0
getfield kellinwood/security/zipsigner/ProgressHelper/progressTotalItems I
idiv
istore 3
goto L1
L3:
return
.limit locals 6
.limit stack 3
.end method

.method public removeProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield kellinwood/security/zipsigner/ProgressHelper/listeners Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clone()Ljava/lang/Object;
checkcast java/util/ArrayList
astore 2
aload 2
aload 1
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
aload 0
aload 2
putfield kellinwood/security/zipsigner/ProgressHelper/listeners Ljava/util/ArrayList;
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public setProgressCurrentItem(I)V
aload 0
iload 1
putfield kellinwood/security/zipsigner/ProgressHelper/progressCurrentItem I
return
.limit locals 2
.limit stack 2
.end method

.method public setProgressTotalItems(I)V
aload 0
iload 1
putfield kellinwood/security/zipsigner/ProgressHelper/progressTotalItems I
return
.limit locals 2
.limit stack 2
.end method
