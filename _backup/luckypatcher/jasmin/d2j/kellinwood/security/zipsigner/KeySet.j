.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/KeySet
.super java/lang/Object

.field 'name' Ljava/lang/String;

.field 'privateKey' Ljava/security/PrivateKey;

.field 'publicKey' Ljava/security/cert/X509Certificate;

.field 'sigBlockTemplate' [B

.field 'signatureAlgorithm' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield kellinwood/security/zipsigner/KeySet/publicKey Ljava/security/cert/X509Certificate;
aload 0
aconst_null
putfield kellinwood/security/zipsigner/KeySet/privateKey Ljava/security/PrivateKey;
aload 0
aconst_null
putfield kellinwood/security/zipsigner/KeySet/sigBlockTemplate [B
aload 0
ldc "SHA1withRSA"
putfield kellinwood/security/zipsigner/KeySet/signatureAlgorithm Ljava/lang/String;
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;Ljava/lang/String;[B)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield kellinwood/security/zipsigner/KeySet/publicKey Ljava/security/cert/X509Certificate;
aload 0
aconst_null
putfield kellinwood/security/zipsigner/KeySet/privateKey Ljava/security/PrivateKey;
aload 0
aconst_null
putfield kellinwood/security/zipsigner/KeySet/sigBlockTemplate [B
aload 0
ldc "SHA1withRSA"
putfield kellinwood/security/zipsigner/KeySet/signatureAlgorithm Ljava/lang/String;
aload 0
aload 1
putfield kellinwood/security/zipsigner/KeySet/name Ljava/lang/String;
aload 0
aload 2
putfield kellinwood/security/zipsigner/KeySet/publicKey Ljava/security/cert/X509Certificate;
aload 0
aload 3
putfield kellinwood/security/zipsigner/KeySet/privateKey Ljava/security/PrivateKey;
aload 4
ifnull L0
aload 0
aload 4
putfield kellinwood/security/zipsigner/KeySet/signatureAlgorithm Ljava/lang/String;
L0:
aload 0
aload 5
putfield kellinwood/security/zipsigner/KeySet/sigBlockTemplate [B
return
.limit locals 6
.limit stack 2
.end method

.method public <init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield kellinwood/security/zipsigner/KeySet/publicKey Ljava/security/cert/X509Certificate;
aload 0
aconst_null
putfield kellinwood/security/zipsigner/KeySet/privateKey Ljava/security/PrivateKey;
aload 0
aconst_null
putfield kellinwood/security/zipsigner/KeySet/sigBlockTemplate [B
aload 0
ldc "SHA1withRSA"
putfield kellinwood/security/zipsigner/KeySet/signatureAlgorithm Ljava/lang/String;
aload 0
aload 1
putfield kellinwood/security/zipsigner/KeySet/name Ljava/lang/String;
aload 0
aload 2
putfield kellinwood/security/zipsigner/KeySet/publicKey Ljava/security/cert/X509Certificate;
aload 0
aload 3
putfield kellinwood/security/zipsigner/KeySet/privateKey Ljava/security/PrivateKey;
aload 0
aload 4
putfield kellinwood/security/zipsigner/KeySet/sigBlockTemplate [B
return
.limit locals 5
.limit stack 2
.end method

.method public getName()Ljava/lang/String;
aload 0
getfield kellinwood/security/zipsigner/KeySet/name Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getPrivateKey()Ljava/security/PrivateKey;
aload 0
getfield kellinwood/security/zipsigner/KeySet/privateKey Ljava/security/PrivateKey;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getPublicKey()Ljava/security/cert/X509Certificate;
aload 0
getfield kellinwood/security/zipsigner/KeySet/publicKey Ljava/security/cert/X509Certificate;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSigBlockTemplate()[B
aload 0
getfield kellinwood/security/zipsigner/KeySet/sigBlockTemplate [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSignatureAlgorithm()Ljava/lang/String;
aload 0
getfield kellinwood/security/zipsigner/KeySet/signatureAlgorithm Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public setName(Ljava/lang/String;)V
aload 0
aload 1
putfield kellinwood/security/zipsigner/KeySet/name Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setPrivateKey(Ljava/security/PrivateKey;)V
aload 0
aload 1
putfield kellinwood/security/zipsigner/KeySet/privateKey Ljava/security/PrivateKey;
return
.limit locals 2
.limit stack 2
.end method

.method public setPublicKey(Ljava/security/cert/X509Certificate;)V
aload 0
aload 1
putfield kellinwood/security/zipsigner/KeySet/publicKey Ljava/security/cert/X509Certificate;
return
.limit locals 2
.limit stack 2
.end method

.method public setSigBlockTemplate([B)V
aload 0
aload 1
putfield kellinwood/security/zipsigner/KeySet/sigBlockTemplate [B
return
.limit locals 2
.limit stack 2
.end method

.method public setSignatureAlgorithm(Ljava/lang/String;)V
aload 1
ifnonnull L0
return
L0:
aload 0
aload 1
putfield kellinwood/security/zipsigner/KeySet/signatureAlgorithm Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method
