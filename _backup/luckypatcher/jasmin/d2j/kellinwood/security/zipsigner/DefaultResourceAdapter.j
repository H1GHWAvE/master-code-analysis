.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/DefaultResourceAdapter
.super java/lang/Object
.implements kellinwood/security/zipsigner/ResourceAdapter
.inner class static synthetic inner kellinwood/security/zipsigner/DefaultResourceAdapter$1

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public transient getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String;
getstatic kellinwood/security/zipsigner/DefaultResourceAdapter$1/$SwitchMap$kellinwood$security$zipsigner$ResourceAdapter$Item [I
aload 1
invokevirtual kellinwood/security/zipsigner/ResourceAdapter$Item/ordinal()I
iaload
tableswitch 1
L0
L1
L2
L3
L4
L5
L6
L7
default : L8
L8:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unknown item "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
ldc "Input and output files are the same.  Specify a different name for the output."
areturn
L1:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unable to auto-select key for signing "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
iconst_0
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L2:
ldc "Loading certificate and private key"
areturn
L3:
ldc "Parsing the input's central directory"
areturn
L4:
ldc "Generating manifest"
areturn
L5:
ldc "Generating signature file"
areturn
L6:
ldc "Generating signature block file"
areturn
L7:
ldc "Copying zip entry %d of %d"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 2
iconst_0
aaload
aastore
dup
iconst_1
aload 2
iconst_1
aaload
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 6
.end method
