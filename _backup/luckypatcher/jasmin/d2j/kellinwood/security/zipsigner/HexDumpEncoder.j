.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/HexDumpEncoder
.super java/lang/Object

.field static 'encoder' Lkellinwood/security/zipsigner/HexEncoder;

.method static <clinit>()V
new kellinwood/security/zipsigner/HexEncoder
dup
invokespecial kellinwood/security/zipsigner/HexEncoder/<init>()V
putstatic kellinwood/security/zipsigner/HexDumpEncoder/encoder Lkellinwood/security/zipsigner/HexEncoder;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static encode([B)Ljava/lang/String;
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
.catch java/io/IOException from L7 to L8 using L2
.catch java/io/IOException from L8 to L9 using L2
.catch java/io/IOException from L10 to L11 using L2
.catch java/io/IOException from L12 to L13 using L2
.catch java/io/IOException from L14 to L15 using L2
.catch java/io/IOException from L16 to L17 using L2
.catch java/io/IOException from L18 to L19 using L2
.catch java/io/IOException from L20 to L21 using L2
L0:
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 5
getstatic kellinwood/security/zipsigner/HexDumpEncoder/encoder Lkellinwood/security/zipsigner/HexEncoder;
aload 0
iconst_0
aload 0
arraylength
aload 5
invokevirtual kellinwood/security/zipsigner/HexEncoder/encode([BIILjava/io/OutputStream;)I
pop
aload 5
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
astore 5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 6
L1:
iconst_0
istore 1
L3:
iload 1
aload 5
arraylength
if_icmpge L20
iload 1
bipush 32
iadd
aload 5
arraylength
invokestatic java/lang/Math/min(II)I
istore 3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 7
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 8
aload 7
ldc "%08x: "
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
iconst_2
idiv
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L4:
iload 1
istore 2
L22:
iload 2
iload 3
if_icmpge L14
L5:
aload 7
aload 5
iload 2
baload
i2c
invokestatic java/lang/Character/valueOf(C)Ljava/lang/Character;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
aload 7
aload 5
iload 2
iconst_1
iadd
baload
i2c
invokestatic java/lang/Character/valueOf(C)Ljava/lang/Character;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
L6:
iload 2
iconst_2
iadd
iconst_4
irem
ifne L8
L7:
aload 7
bipush 32
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L8:
aload 0
iload 2
iconst_2
idiv
baload
istore 4
L9:
iload 4
bipush 32
if_icmplt L12
iload 4
bipush 127
if_icmpge L12
L10:
aload 8
iload 4
i2c
invokestatic java/lang/Character/valueOf(C)Ljava/lang/Character;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
L11:
goto L23
L12:
aload 8
bipush 46
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L13:
goto L23
L2:
astore 0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/io/IOException/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L14:
aload 6
aload 7
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 7
invokevirtual java/lang/StringBuilder/length()I
istore 2
L15:
iload 2
bipush 50
if_icmpge L18
L16:
aload 6
bipush 32
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L17:
iload 2
iconst_1
iadd
istore 2
goto L15
L18:
aload 6
ldc "  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 6
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
pop
aload 6
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L19:
iload 1
bipush 32
iadd
istore 1
goto L3
L20:
aload 6
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L21:
aload 0
areturn
L23:
iload 2
iconst_2
iadd
istore 2
goto L22
.limit locals 9
.limit stack 7
.end method
