.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/ZipSigner
.super java/lang/Object
.inner class public static AutoKeyObservable inner kellinwood/security/zipsigner/ZipSigner$AutoKeyObservable outer kellinwood/security/zipsigner/ZipSigner

.field private static final 'CERT_RSA_NAME' Ljava/lang/String; = "META-INF/CERT.RSA"

.field private static final 'CERT_SF_NAME' Ljava/lang/String; = "META-INF/CERT.SF"

.field public static final 'KEY_NONE' Ljava/lang/String; = "none"

.field public static final 'KEY_TESTKEY' Ljava/lang/String; = "testkey"

.field public static final 'MODE_AUTO' Ljava/lang/String; = "auto"

.field public static final 'MODE_AUTO_NONE' Ljava/lang/String; = "auto-none"

.field public static final 'MODE_AUTO_TESTKEY' Ljava/lang/String; = "auto-testkey"

.field public static final 'SUPPORTED_KEY_MODES' [Ljava/lang/String;

.field static 'log' Lkellinwood/logging/LoggerInterface;

.field private static 'stripPattern' Ljava/util/regex/Pattern;

.field 'autoKeyDetect' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"

.field 'autoKeyObservable' Lkellinwood/security/zipsigner/ZipSigner$AutoKeyObservable;

.field private 'canceled' Z

.field 'keySet' Lkellinwood/security/zipsigner/KeySet;

.field 'keymode' Ljava/lang/String;

.field 'loadedKeys' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Lkellinwood/security/zipsigner/KeySet;>;"

.field private 'progressHelper' Lkellinwood/security/zipsigner/ProgressHelper;

.field private 'resourceAdapter' Lkellinwood/security/zipsigner/ResourceAdapter;

.method static <clinit>()V
aconst_null
putstatic kellinwood/security/zipsigner/ZipSigner/log Lkellinwood/logging/LoggerInterface;
ldc "^META-INF/(.*)[.](SF|RSA|DSA)$"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
putstatic kellinwood/security/zipsigner/ZipSigner/stripPattern Ljava/util/regex/Pattern;
bipush 8
anewarray java/lang/String
dup
iconst_0
ldc "auto-testkey"
aastore
dup
iconst_1
ldc "auto"
aastore
dup
iconst_2
ldc "auto-none"
aastore
dup
iconst_3
ldc "media"
aastore
dup
iconst_4
ldc "platform"
aastore
dup
iconst_5
ldc "shared"
aastore
dup
bipush 6
ldc "testkey"
aastore
dup
bipush 7
ldc "none"
aastore
putstatic kellinwood/security/zipsigner/ZipSigner/SUPPORTED_KEY_MODES [Ljava/lang/String;
return
.limit locals 0
.limit stack 4
.end method

.method public <init>()V
.throws java/lang/ClassNotFoundException
.throws java/lang/IllegalAccessException
.throws java/lang/InstantiationException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield kellinwood/security/zipsigner/ZipSigner/canceled Z
aload 0
new kellinwood/security/zipsigner/ProgressHelper
dup
invokespecial kellinwood/security/zipsigner/ProgressHelper/<init>()V
putfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
aload 0
new kellinwood/security/zipsigner/DefaultResourceAdapter
dup
invokespecial kellinwood/security/zipsigner/DefaultResourceAdapter/<init>()V
putfield kellinwood/security/zipsigner/ZipSigner/resourceAdapter Lkellinwood/security/zipsigner/ResourceAdapter;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield kellinwood/security/zipsigner/ZipSigner/loadedKeys Ljava/util/Map;
aload 0
aconst_null
putfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
aload 0
ldc "testkey"
putfield kellinwood/security/zipsigner/ZipSigner/keymode Ljava/lang/String;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield kellinwood/security/zipsigner/ZipSigner/autoKeyDetect Ljava/util/Map;
aload 0
new kellinwood/security/zipsigner/ZipSigner$AutoKeyObservable
dup
invokespecial kellinwood/security/zipsigner/ZipSigner$AutoKeyObservable/<init>()V
putfield kellinwood/security/zipsigner/ZipSigner/autoKeyObservable Lkellinwood/security/zipsigner/ZipSigner$AutoKeyObservable;
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/autoKeyDetect Ljava/util/Map;
ldc "aa9852bc5a53272ac8031d49b65e4b0e"
ldc "media"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/autoKeyDetect Ljava/util/Map;
ldc "e60418c4b638f20d0721e115674ca11f"
ldc "platform"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/autoKeyDetect Ljava/util/Map;
ldc "3e24e49741b60c215c010dc6048fca7d"
ldc "shared"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/autoKeyDetect Ljava/util/Map;
ldc "dab2cead827ef5313f28e22b6fa8479f"
ldc "testkey"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 1
.limit stack 3
.end method

.method private addDigestsToManifest(Ljava/util/Map;)Ljava/util/jar/Manifest;
.signature "(Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;)Ljava/util/jar/Manifest;"
.throws java/io/IOException
.throws java/security/GeneralSecurityException
aconst_null
astore 4
aload 1
ldc "META-INF/MANIFEST.MF"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/zipio/ZioEntry
astore 5
aload 5
ifnull L0
new java/util/jar/Manifest
dup
invokespecial java/util/jar/Manifest/<init>()V
astore 4
aload 4
aload 5
invokevirtual kellinwood/zipio/ZioEntry/getInputStream()Ljava/io/InputStream;
invokevirtual java/util/jar/Manifest/read(Ljava/io/InputStream;)V
L0:
new java/util/jar/Manifest
dup
invokespecial java/util/jar/Manifest/<init>()V
astore 6
aload 6
invokevirtual java/util/jar/Manifest/getMainAttributes()Ljava/util/jar/Attributes;
astore 5
aload 4
ifnull L1
aload 5
aload 4
invokevirtual java/util/jar/Manifest/getMainAttributes()Ljava/util/jar/Attributes;
invokevirtual java/util/jar/Attributes/putAll(Ljava/util/Map;)V
L2:
ldc "SHA1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 7
sipush 512
newarray byte
astore 8
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
astore 5
aload 5
aload 1
invokevirtual java/util/TreeMap/putAll(Ljava/util/Map;)V
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
istore 3
iload 3
ifeq L3
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Manifest entries:"
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L3:
aload 5
invokevirtual java/util/TreeMap/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 9
L4:
aload 9
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 9
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast kellinwood/zipio/ZioEntry
astore 1
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
ifeq L6
L5:
aload 6
areturn
L1:
aload 5
ldc "Manifest-Version"
ldc "1.0"
invokevirtual java/util/jar/Attributes/putValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
pop
aload 5
ldc "Created-By"
ldc "1.0 (Android SignApk)"
invokevirtual java/util/jar/Attributes/putValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
pop
goto L2
L6:
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
astore 10
iload 3
ifeq L7
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
aload 10
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L7:
aload 1
invokevirtual kellinwood/zipio/ZioEntry/isDirectory()Z
ifne L4
aload 10
ldc "META-INF/MANIFEST.MF"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 10
ldc "META-INF/CERT.SF"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 10
ldc "META-INF/CERT.RSA"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
getstatic kellinwood/security/zipsigner/ZipSigner/stripPattern Ljava/util/regex/Pattern;
ifnull L8
getstatic kellinwood/security/zipsigner/ZipSigner/stripPattern Ljava/util/regex/Pattern;
aload 10
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
invokevirtual java/util/regex/Matcher/matches()Z
ifne L4
L8:
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_0
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/resourceAdapter Lkellinwood/security/zipsigner/ResourceAdapter;
getstatic kellinwood/security/zipsigner/ResourceAdapter$Item/GENERATING_MANIFEST Lkellinwood/security/zipsigner/ResourceAdapter$Item;
iconst_0
anewarray java/lang/Object
invokeinterface kellinwood/security/zipsigner/ResourceAdapter/getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String; 2
invokevirtual kellinwood/security/zipsigner/ProgressHelper/progress(ILjava/lang/String;)V
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getInputStream()Ljava/io/InputStream;
astore 1
L9:
aload 1
aload 8
invokevirtual java/io/InputStream/read([B)I
istore 2
iload 2
ifle L10
aload 7
aload 8
iconst_0
iload 2
invokevirtual java/security/MessageDigest/update([BII)V
goto L9
L10:
aconst_null
astore 5
aload 5
astore 1
aload 4
ifnull L11
aload 4
aload 10
invokevirtual java/util/jar/Manifest/getAttributes(Ljava/lang/String;)Ljava/util/jar/Attributes;
astore 11
aload 5
astore 1
aload 11
ifnull L11
new java/util/jar/Attributes
dup
aload 11
invokespecial java/util/jar/Attributes/<init>(Ljava/util/jar/Attributes;)V
astore 1
L11:
aload 1
astore 5
aload 1
ifnonnull L12
new java/util/jar/Attributes
dup
invokespecial java/util/jar/Attributes/<init>()V
astore 5
L12:
aload 5
ldc "SHA1-Digest"
aload 7
invokevirtual java/security/MessageDigest/digest()[B
invokestatic kellinwood/security/zipsigner/Base64/encode([B)Ljava/lang/String;
invokevirtual java/util/jar/Attributes/putValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
pop
aload 6
invokevirtual java/util/jar/Manifest/getEntries()Ljava/util/Map;
aload 10
aload 5
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L4
.limit locals 12
.limit stack 5
.end method

.method private copyFiles(Ljava/util/Map;Lkellinwood/zipio/ZipOutput;)V
.signature "(Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;Lkellinwood/zipio/ZipOutput;)V"
.throws java/io/IOException
iconst_1
istore 3
aload 1
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 4
L0:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast kellinwood/zipio/ZioEntry
astore 5
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
ifeq L2
L1:
return
L2:
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_0
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/resourceAdapter Lkellinwood/security/zipsigner/ResourceAdapter;
getstatic kellinwood/security/zipsigner/ResourceAdapter$Item/COPYING_ZIP_ENTRY Lkellinwood/security/zipsigner/ResourceAdapter$Item;
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 1
invokeinterface java/util/Map/size()I 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokeinterface kellinwood/security/zipsigner/ResourceAdapter/getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String; 2
invokevirtual kellinwood/security/zipsigner/ProgressHelper/progress(ILjava/lang/String;)V
iload 3
iconst_1
iadd
istore 3
aload 2
aload 5
invokevirtual kellinwood/zipio/ZipOutput/write(Lkellinwood/zipio/ZioEntry;)V
goto L0
.limit locals 6
.limit stack 8
.end method

.method private copyFiles(Ljava/util/jar/Manifest;Ljava/util/Map;Lkellinwood/zipio/ZipOutput;J)V
.signature "(Ljava/util/jar/Manifest;Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;Lkellinwood/zipio/ZipOutput;J)V"
.throws java/io/IOException
new java/util/ArrayList
dup
aload 1
invokevirtual java/util/jar/Manifest/getEntries()Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
astore 1
aload 1
invokestatic java/util/Collections/sort(Ljava/util/List;)V
iconst_1
istore 6
aload 1
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 7
L0:
aload 7
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 7
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 8
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
ifeq L2
L1:
return
L2:
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_0
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/resourceAdapter Lkellinwood/security/zipsigner/ResourceAdapter;
getstatic kellinwood/security/zipsigner/ResourceAdapter$Item/COPYING_ZIP_ENTRY Lkellinwood/security/zipsigner/ResourceAdapter$Item;
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 6
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 1
invokeinterface java/util/List/size()I 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokeinterface kellinwood/security/zipsigner/ResourceAdapter/getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String; 2
invokevirtual kellinwood/security/zipsigner/ProgressHelper/progress(ILjava/lang/String;)V
iload 6
iconst_1
iadd
istore 6
aload 2
aload 8
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/zipio/ZioEntry
astore 8
aload 8
lload 4
invokevirtual kellinwood/zipio/ZioEntry/setTime(J)V
aload 3
aload 8
invokevirtual kellinwood/zipio/ZipOutput/write(Lkellinwood/zipio/ZioEntry;)V
goto L0
.limit locals 9
.limit stack 8
.end method

.method private decryptPrivateKey([BLjava/lang/String;)Ljava/security/spec/KeySpec;
.throws java/security/GeneralSecurityException
.catch java/io/IOException from L0 to L1 using L2
.catch java/security/spec/InvalidKeySpecException from L3 to L4 using L5
L0:
new javax/crypto/EncryptedPrivateKeyInfo
dup
aload 1
invokespecial javax/crypto/EncryptedPrivateKeyInfo/<init>([B)V
astore 1
L1:
aload 2
invokevirtual java/lang/String/toCharArray()[C
astore 2
aload 1
invokevirtual javax/crypto/EncryptedPrivateKeyInfo/getAlgName()Ljava/lang/String;
invokestatic javax/crypto/SecretKeyFactory/getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;
new javax/crypto/spec/PBEKeySpec
dup
aload 2
invokespecial javax/crypto/spec/PBEKeySpec/<init>([C)V
invokevirtual javax/crypto/SecretKeyFactory/generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;
astore 2
aload 1
invokevirtual javax/crypto/EncryptedPrivateKeyInfo/getAlgName()Ljava/lang/String;
invokestatic javax/crypto/Cipher/getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
astore 3
aload 3
iconst_2
aload 2
aload 1
invokevirtual javax/crypto/EncryptedPrivateKeyInfo/getAlgParameters()Ljava/security/AlgorithmParameters;
invokevirtual javax/crypto/Cipher/init(ILjava/security/Key;Ljava/security/AlgorithmParameters;)V
L3:
aload 1
aload 3
invokevirtual javax/crypto/EncryptedPrivateKeyInfo/getKeySpec(Ljavax/crypto/Cipher;)Ljava/security/spec/PKCS8EncodedKeySpec;
astore 1
L4:
aload 1
areturn
L2:
astore 1
aconst_null
areturn
L5:
astore 1
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "signapk: Password for private key may be bad."
invokeinterface kellinwood/logging/LoggerInterface/error(Ljava/lang/String;)V 1
aload 1
athrow
.limit locals 4
.limit stack 4
.end method

.method private generateSignatureFile(Ljava/util/jar/Manifest;Ljava/io/OutputStream;)V
.throws java/io/IOException
.throws java/security/GeneralSecurityException
aload 2
ldc "Signature-Version: 1.0\r\n"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/OutputStream/write([B)V
aload 2
ldc "Created-By: 1.0 (Android SignApk)\r\n"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/OutputStream/write([B)V
ldc "SHA1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 3
new java/io/PrintStream
dup
new java/security/DigestOutputStream
dup
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
aload 3
invokespecial java/security/DigestOutputStream/<init>(Ljava/io/OutputStream;Ljava/security/MessageDigest;)V
iconst_1
ldc "UTF-8"
invokespecial java/io/PrintStream/<init>(Ljava/io/OutputStream;ZLjava/lang/String;)V
astore 4
aload 1
aload 4
invokevirtual java/util/jar/Manifest/write(Ljava/io/OutputStream;)V
aload 4
invokevirtual java/io/PrintStream/flush()V
aload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "SHA1-Digest-Manifest: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/security/MessageDigest/digest()[B
invokestatic kellinwood/security/zipsigner/Base64/encode([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\r\n\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/OutputStream/write([B)V
aload 1
invokevirtual java/util/jar/Manifest/getEntries()Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 6
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
ifeq L2
L1:
return
L2:
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_0
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/resourceAdapter Lkellinwood/security/zipsigner/ResourceAdapter;
getstatic kellinwood/security/zipsigner/ResourceAdapter$Item/GENERATING_SIGNATURE_FILE Lkellinwood/security/zipsigner/ResourceAdapter$Item;
iconst_0
anewarray java/lang/Object
invokeinterface kellinwood/security/zipsigner/ResourceAdapter/getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String; 2
invokevirtual kellinwood/security/zipsigner/ProgressHelper/progress(ILjava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Name: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 5
aload 4
aload 5
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
aload 6
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/util/jar/Attributes
invokevirtual java/util/jar/Attributes/entrySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 6
L3:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 7
aload 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 7
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
goto L3
L4:
aload 4
ldc "\r\n"
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
aload 4
invokevirtual java/io/PrintStream/flush()V
aload 2
aload 5
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/OutputStream/write([B)V
aload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "SHA1-Digest: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/security/MessageDigest/digest()[B
invokestatic kellinwood/security/zipsigner/Base64/encode([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\r\n\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/OutputStream/write([B)V
goto L0
.limit locals 8
.limit stack 6
.end method

.method public static getLogger()Lkellinwood/logging/LoggerInterface;
getstatic kellinwood/security/zipsigner/ZipSigner/log Lkellinwood/logging/LoggerInterface;
ifnonnull L0
ldc kellinwood/security/zipsigner/ZipSigner
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic kellinwood/logging/LoggerManager/getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
putstatic kellinwood/security/zipsigner/ZipSigner/log Lkellinwood/logging/LoggerInterface;
L0:
getstatic kellinwood/security/zipsigner/ZipSigner/log Lkellinwood/logging/LoggerInterface;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static getSupportedKeyModes()[Ljava/lang/String;
getstatic kellinwood/security/zipsigner/ZipSigner/SUPPORTED_KEY_MODES [Ljava/lang/String;
areturn
.limit locals 0
.limit stack 1
.end method

.method private writeSignatureBlock(Lkellinwood/security/zipsigner/KeySet;[BLjava/io/OutputStream;)V
.throws java/io/IOException
.throws java/security/GeneralSecurityException
.catch java/lang/Exception from L0 to L1 using L2
aload 1
invokevirtual kellinwood/security/zipsigner/KeySet/getSigBlockTemplate()[B
ifnull L0
new kellinwood/security/zipsigner/ZipSignature
dup
invokespecial kellinwood/security/zipsigner/ZipSignature/<init>()V
astore 4
aload 4
aload 1
invokevirtual kellinwood/security/zipsigner/KeySet/getPrivateKey()Ljava/security/PrivateKey;
invokevirtual kellinwood/security/zipsigner/ZipSignature/initSign(Ljava/security/PrivateKey;)V
aload 4
aload 2
invokevirtual kellinwood/security/zipsigner/ZipSignature/update([B)V
aload 4
invokevirtual kellinwood/security/zipsigner/ZipSignature/sign()[B
astore 4
aload 3
aload 1
invokevirtual kellinwood/security/zipsigner/KeySet/getSigBlockTemplate()[B
invokevirtual java/io/OutputStream/write([B)V
aload 3
aload 4
invokevirtual java/io/OutputStream/write([B)V
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
ifeq L3
ldc "SHA1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 3
aload 3
aload 2
invokevirtual java/security/MessageDigest/update([B)V
aload 3
invokevirtual java/security/MessageDigest/digest()[B
astore 2
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Sig File SHA1: \n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokestatic kellinwood/security/zipsigner/HexDumpEncoder/encode([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Signature: \n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokestatic kellinwood/security/zipsigner/HexDumpEncoder/encode([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
ldc "RSA/ECB/PKCS1Padding"
invokestatic javax/crypto/Cipher/getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
astore 2
aload 2
iconst_2
aload 1
invokevirtual kellinwood/security/zipsigner/KeySet/getPublicKey()Ljava/security/cert/X509Certificate;
invokevirtual javax/crypto/Cipher/init(ILjava/security/cert/Certificate;)V
aload 2
aload 4
invokevirtual javax/crypto/Cipher/doFinal([B)[B
astore 1
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Signature Decrypted: \n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokestatic kellinwood/security/zipsigner/HexDumpEncoder/encode([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L3:
return
L0:
aload 3
ldc "kellinwood.security.zipsigner.optional.SignatureBlockGenerator"
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
ldc "generate"
iconst_2
anewarray java/lang/Class
dup
iconst_0
ldc kellinwood/security/zipsigner/KeySet
aastore
dup
iconst_1
iconst_1
newarray byte
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
aconst_null
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 2
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast [B
checkcast [B
invokevirtual java/io/OutputStream/write([B)V
L1:
return
L2:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 5
.limit stack 7
.end method

.method public addAutoKeyObserver(Ljava/util/Observer;)V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/autoKeyObservable Lkellinwood/security/zipsigner/ZipSigner$AutoKeyObservable;
aload 1
invokevirtual kellinwood/security/zipsigner/ZipSigner$AutoKeyObservable/addObserver(Ljava/util/Observer;)V
return
.limit locals 2
.limit stack 2
.end method

.method public addProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
aload 1
invokevirtual kellinwood/security/zipsigner/ProgressHelper/addProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected autoDetectKey(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
.signature "(Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;)Ljava/lang/String;"
.throws java/security/NoSuchAlgorithmException
.throws java/io/IOException
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
istore 5
aload 1
ldc "auto"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L0
aload 1
areturn
L0:
aconst_null
astore 6
aload 2
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 7
aload 6
astore 2
L1:
aload 7
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 7
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 6
aload 6
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/String
astore 8
aload 8
ldc "META-INF/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L1
aload 8
ldc ".RSA"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L1
ldc "MD5"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 8
aload 6
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast kellinwood/zipio/ZioEntry
invokevirtual kellinwood/zipio/ZioEntry/getData()[B
astore 6
aload 6
arraylength
sipush 1458
if_icmpge L3
L2:
aload 1
ldc "auto-testkey"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
iload 5
ifeq L5
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Falling back to key="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L5:
ldc "testkey"
areturn
L3:
aload 8
aload 6
iconst_0
sipush 1458
invokevirtual java/security/MessageDigest/update([BII)V
aload 8
invokevirtual java/security/MessageDigest/digest()[B
astore 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 6
aload 2
arraylength
istore 4
iconst_0
istore 3
L6:
iload 3
iload 4
if_icmpge L7
aload 6
ldc "%02x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 2
iload 3
baload
invokestatic java/lang/Byte/valueOf(B)Ljava/lang/Byte;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 3
iconst_1
iadd
istore 3
goto L6
L7:
aload 6
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/autoKeyDetect Ljava/util/Map;
aload 2
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 6
iload 5
ifeq L8
aload 6
ifnull L9
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Auto-determined key=%s using md5=%s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 6
aastore
dup
iconst_1
aload 2
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L8:
aload 6
astore 2
aload 6
ifnull L1
aload 6
areturn
L9:
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Auto key determination failed for md5=%s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
goto L8
L4:
aload 1
ldc "auto-none"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L10
iload 5
ifeq L11
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Unable to determine key, returning: none"
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L11:
ldc "none"
areturn
L10:
aconst_null
areturn
.limit locals 9
.limit stack 7
.end method

.method public cancel()V
aload 0
iconst_1
putfield kellinwood/security/zipsigner/ZipSigner/canceled Z
return
.limit locals 1
.limit stack 2
.end method

.method public getKeySet()Lkellinwood/security/zipsigner/KeySet;
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getKeymode()Ljava/lang/String;
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keymode Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getResourceAdapter()Lkellinwood/security/zipsigner/ResourceAdapter;
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/resourceAdapter Lkellinwood/security/zipsigner/ResourceAdapter;
areturn
.limit locals 1
.limit stack 1
.end method

.method public isCanceled()Z
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public issueLoadingCertAndKeysProgressEvent()V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_1
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/resourceAdapter Lkellinwood/security/zipsigner/ResourceAdapter;
getstatic kellinwood/security/zipsigner/ResourceAdapter$Item/LOADING_CERTIFICATE_AND_KEY Lkellinwood/security/zipsigner/ResourceAdapter$Item;
iconst_0
anewarray java/lang/Object
invokeinterface kellinwood/security/zipsigner/ResourceAdapter/getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String; 2
invokevirtual kellinwood/security/zipsigner/ProgressHelper/progress(ILjava/lang/String;)V
return
.limit locals 1
.limit stack 5
.end method

.method public loadKeys(Ljava/lang/String;)V
.throws java/io/IOException
.throws java/security/GeneralSecurityException
aload 0
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/loadedKeys Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/security/zipsigner/KeySet
putfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
ifnull L0
L1:
return
L0:
aload 0
new kellinwood/security/zipsigner/KeySet
dup
invokespecial kellinwood/security/zipsigner/KeySet/<init>()V
putfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
aload 1
invokevirtual kellinwood/security/zipsigner/KeySet/setName(Ljava/lang/String;)V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/loadedKeys Ljava/util/Map;
aload 1
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
ldc "none"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
aload 0
invokevirtual kellinwood/security/zipsigner/ZipSigner/issueLoadingCertAndKeysProgressEvent()V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/keys/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".pk8"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Class/getResource(Ljava/lang/String;)Ljava/net/URL;
astore 2
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
aload 0
aload 2
aconst_null
invokevirtual kellinwood/security/zipsigner/ZipSigner/readPrivateKey(Ljava/net/URL;Ljava/lang/String;)Ljava/security/PrivateKey;
invokevirtual kellinwood/security/zipsigner/KeySet/setPrivateKey(Ljava/security/PrivateKey;)V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/keys/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".x509.pem"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Class/getResource(Ljava/lang/String;)Ljava/net/URL;
astore 2
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
aload 0
aload 2
invokevirtual kellinwood/security/zipsigner/ZipSigner/readPublicKey(Ljava/net/URL;)Ljava/security/cert/X509Certificate;
invokevirtual kellinwood/security/zipsigner/KeySet/setPublicKey(Ljava/security/cert/X509Certificate;)V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/keys/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".sbt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Class/getResource(Ljava/lang/String;)Ljava/net/URL;
astore 1
aload 1
ifnull L1
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
aload 0
aload 1
invokevirtual kellinwood/security/zipsigner/ZipSigner/readContentAsBytes(Ljava/net/URL;)[B
invokevirtual kellinwood/security/zipsigner/KeySet/setSigBlockTemplate([B)V
return
.limit locals 3
.limit stack 4
.end method

.method public loadProvider(Ljava/lang/String;)V
.throws java/lang/ClassNotFoundException
.throws java/lang/IllegalAccessException
.throws java/lang/InstantiationException
aload 1
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
invokevirtual java/lang/Class/newInstance()Ljava/lang/Object;
checkcast java/security/Provider
iconst_1
invokestatic java/security/Security/insertProviderAt(Ljava/security/Provider;I)I
pop
return
.limit locals 2
.limit stack 2
.end method

.method public readContentAsBytes(Ljava/io/InputStream;)[B
.throws java/io/IOException
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 3
sipush 2048
newarray byte
astore 4
aload 1
aload 4
invokevirtual java/io/InputStream/read([B)I
istore 2
L0:
iload 2
iconst_m1
if_icmpeq L1
aload 3
aload 4
iconst_0
iload 2
invokevirtual java/io/ByteArrayOutputStream/write([BII)V
aload 1
aload 4
invokevirtual java/io/InputStream/read([B)I
istore 2
goto L0
L1:
aload 3
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
areturn
.limit locals 5
.limit stack 4
.end method

.method public readContentAsBytes(Ljava/net/URL;)[B
.throws java/io/IOException
aload 0
aload 1
invokevirtual java/net/URL/openStream()Ljava/io/InputStream;
invokevirtual kellinwood/security/zipsigner/ZipSigner/readContentAsBytes(Ljava/io/InputStream;)[B
areturn
.limit locals 2
.limit stack 2
.end method

.method public readPrivateKey(Ljava/net/URL;Ljava/lang/String;)Ljava/security/PrivateKey;
.throws java/io/IOException
.throws java/security/GeneralSecurityException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch java/security/spec/InvalidKeySpecException from L4 to L5 using L6
.catch all from L4 to L5 using L2
.catch all from L7 to L8 using L2
new java/io/DataInputStream
dup
aload 1
invokevirtual java/net/URL/openStream()Ljava/io/InputStream;
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
astore 3
L0:
aload 0
aload 3
invokevirtual kellinwood/security/zipsigner/ZipSigner/readContentAsBytes(Ljava/io/InputStream;)[B
astore 4
aload 0
aload 4
aload 2
invokespecial kellinwood/security/zipsigner/ZipSigner/decryptPrivateKey([BLjava/lang/String;)Ljava/security/spec/KeySpec;
astore 2
L1:
aload 2
astore 1
aload 2
ifnonnull L4
L3:
new java/security/spec/PKCS8EncodedKeySpec
dup
aload 4
invokespecial java/security/spec/PKCS8EncodedKeySpec/<init>([B)V
astore 1
L4:
ldc "RSA"
invokestatic java/security/KeyFactory/getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;
aload 1
invokevirtual java/security/KeyFactory/generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
astore 2
L5:
aload 3
invokevirtual java/io/DataInputStream/close()V
aload 2
areturn
L6:
astore 2
L7:
ldc "DSA"
invokestatic java/security/KeyFactory/getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;
aload 1
invokevirtual java/security/KeyFactory/generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
astore 1
L8:
aload 3
invokevirtual java/io/DataInputStream/close()V
aload 1
areturn
L2:
astore 1
aload 3
invokevirtual java/io/DataInputStream/close()V
aload 1
athrow
.limit locals 5
.limit stack 3
.end method

.method public readPublicKey(Ljava/net/URL;)Ljava/security/cert/X509Certificate;
.throws java/io/IOException
.throws java/security/GeneralSecurityException
.catch all from L0 to L1 using L2
aload 1
invokevirtual java/net/URL/openStream()Ljava/io/InputStream;
astore 1
L0:
ldc "X.509"
invokestatic java/security/cert/CertificateFactory/getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;
aload 1
invokevirtual java/security/cert/CertificateFactory/generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
checkcast java/security/cert/X509Certificate
astore 2
L1:
aload 1
invokevirtual java/io/InputStream/close()V
aload 2
areturn
L2:
astore 2
aload 1
invokevirtual java/io/InputStream/close()V
aload 2
athrow
.limit locals 3
.limit stack 2
.end method

.method public removeProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
aload 1
invokevirtual kellinwood/security/zipsigner/ProgressHelper/removeProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public resetCanceled()V
aload 0
iconst_0
putfield kellinwood/security/zipsigner/ZipSigner/canceled Z
return
.limit locals 1
.limit stack 2
.end method

.method public setKeymode(Ljava/lang/String;)V
.throws java/io/IOException
.throws java/security/GeneralSecurityException
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
ifeq L0
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "setKeymode: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L0:
aload 0
aload 1
putfield kellinwood/security/zipsigner/ZipSigner/keymode Ljava/lang/String;
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keymode Ljava/lang/String;
ldc "auto"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L1
aload 0
aconst_null
putfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
return
L1:
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
invokevirtual kellinwood/security/zipsigner/ProgressHelper/initProgress()V
aload 0
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keymode Ljava/lang/String;
invokevirtual kellinwood/security/zipsigner/ZipSigner/loadKeys(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method public setKeys(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;Ljava/lang/String;[B)V
aload 0
new kellinwood/security/zipsigner/KeySet
dup
aload 1
aload 2
aload 3
aload 4
aload 5
invokespecial kellinwood/security/zipsigner/KeySet/<init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;Ljava/lang/String;[B)V
putfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
return
.limit locals 6
.limit stack 8
.end method

.method public setKeys(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V
aload 0
new kellinwood/security/zipsigner/KeySet
dup
aload 1
aload 2
aload 3
aload 4
invokespecial kellinwood/security/zipsigner/KeySet/<init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V
putfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
return
.limit locals 5
.limit stack 7
.end method

.method public setResourceAdapter(Lkellinwood/security/zipsigner/ResourceAdapter;)V
aload 0
aload 1
putfield kellinwood/security/zipsigner/ZipSigner/resourceAdapter Lkellinwood/security/zipsigner/ResourceAdapter;
return
.limit locals 2
.limit stack 2
.end method

.method public signZip(Ljava/lang/String;Ljava/lang/String;)V
.throws java/io/IOException
.throws java/security/GeneralSecurityException
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getCanonicalFile()Ljava/io/File;
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getCanonicalFile()Ljava/io/File;
invokevirtual java/io/File/equals(Ljava/lang/Object;)Z
ifeq L0
new java/lang/IllegalArgumentException
dup
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/resourceAdapter Lkellinwood/security/zipsigner/ResourceAdapter;
getstatic kellinwood/security/zipsigner/ResourceAdapter$Item/INPUT_SAME_AS_OUTPUT_ERROR Lkellinwood/security/zipsigner/ResourceAdapter$Item;
iconst_0
anewarray java/lang/Object
invokeinterface kellinwood/security/zipsigner/ResourceAdapter/getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String; 2
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
invokevirtual kellinwood/security/zipsigner/ProgressHelper/initProgress()V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_1
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/resourceAdapter Lkellinwood/security/zipsigner/ResourceAdapter;
getstatic kellinwood/security/zipsigner/ResourceAdapter$Item/PARSING_CENTRAL_DIRECTORY Lkellinwood/security/zipsigner/ResourceAdapter$Item;
iconst_0
anewarray java/lang/Object
invokeinterface kellinwood/security/zipsigner/ResourceAdapter/getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String; 2
invokevirtual kellinwood/security/zipsigner/ProgressHelper/progress(ILjava/lang/String;)V
aload 0
aload 1
invokestatic kellinwood/zipio/ZipInput/read(Ljava/lang/String;)Lkellinwood/zipio/ZipInput;
invokevirtual kellinwood/zipio/ZipInput/getEntries()Ljava/util/Map;
new java/io/FileOutputStream
dup
aload 2
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
aload 2
invokevirtual kellinwood/security/zipsigner/ZipSigner/signZip(Ljava/util/Map;Ljava/io/OutputStream;Ljava/lang/String;)V
return
.limit locals 3
.limit stack 5
.end method

.method public signZip(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.throws java/lang/ClassNotFoundException
.throws java/lang/IllegalAccessException
.throws java/lang/InstantiationException
.throws java/io/IOException
.throws java/security/GeneralSecurityException
aload 0
aload 1
aload 2
aload 3
invokevirtual java/lang/String/toCharArray()[C
aload 4
aload 5
invokevirtual java/lang/String/toCharArray()[C
ldc "SHA1withRSA"
aload 6
aload 7
invokevirtual kellinwood/security/zipsigner/ZipSigner/signZip(Ljava/net/URL;Ljava/lang/String;[CLjava/lang/String;[CLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 8
.limit stack 9
.end method

.method public signZip(Ljava/net/URL;Ljava/lang/String;[CLjava/lang/String;[CLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.throws java/lang/ClassNotFoundException
.throws java/lang/IllegalAccessException
.throws java/lang/InstantiationException
.throws java/io/IOException
.throws java/security/GeneralSecurityException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
.catch all from L11 to L12 using L2
aconst_null
astore 10
aload 2
astore 9
aload 2
ifnonnull L1
aload 10
astore 2
L0:
invokestatic java/security/KeyStore/getDefaultType()Ljava/lang/String;
astore 9
L1:
aload 10
astore 2
L3:
aload 9
invokestatic java/security/KeyStore/getInstance(Ljava/lang/String;)Ljava/security/KeyStore;
astore 9
L4:
aload 10
astore 2
L5:
aload 1
invokevirtual java/net/URL/openStream()Ljava/io/InputStream;
astore 1
L6:
aload 1
astore 2
L7:
aload 9
aload 1
aload 3
invokevirtual java/security/KeyStore/load(Ljava/io/InputStream;[C)V
L8:
aload 1
astore 2
L9:
aload 0
ldc "custom"
aload 9
aload 4
invokevirtual java/security/KeyStore/getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;
checkcast java/security/cert/X509Certificate
aload 9
aload 4
aload 5
invokevirtual java/security/KeyStore/getKey(Ljava/lang/String;[C)Ljava/security/Key;
checkcast java/security/PrivateKey
aload 6
aconst_null
invokevirtual kellinwood/security/zipsigner/ZipSigner/setKeys(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;Ljava/lang/String;[B)V
L10:
aload 1
astore 2
L11:
aload 0
aload 7
aload 8
invokevirtual kellinwood/security/zipsigner/ZipSigner/signZip(Ljava/lang/String;Ljava/lang/String;)V
L12:
aload 1
ifnull L13
aload 1
invokevirtual java/io/InputStream/close()V
L13:
return
L2:
astore 1
aload 2
ifnull L14
aload 2
invokevirtual java/io/InputStream/close()V
L14:
aload 1
athrow
.limit locals 11
.limit stack 6
.end method

.method public signZip(Ljava/util/Map;Ljava/io/OutputStream;Ljava/lang/String;)V
.signature "(Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;Ljava/io/OutputStream;Ljava/lang/String;)V"
.throws java/io/IOException
.throws java/security/GeneralSecurityException
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L4
.catch java/lang/Throwable from L5 to L6 using L7
.catch all from L8 to L9 using L4
.catch all from L9 to L10 using L4
.catch all from L11 to L12 using L4
.catch java/lang/Throwable from L13 to L14 using L15
.catch all from L16 to L17 using L4
.catch java/lang/Throwable from L18 to L19 using L20
.catch all from L21 to L22 using L4
.catch all from L23 to L24 using L4
.catch all from L24 to L25 using L4
.catch java/lang/Throwable from L26 to L27 using L28
.catch all from L29 to L30 using L4
.catch java/lang/Throwable from L31 to L32 using L33
.catch java/lang/Throwable from L34 to L35 using L36
.catch java/lang/Throwable from L37 to L38 using L39
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
istore 5
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
invokevirtual kellinwood/security/zipsigner/ProgressHelper/initProgress()V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
ifnonnull L0
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keymode Ljava/lang/String;
ldc "auto"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L40
new java/lang/IllegalStateException
dup
ldc "No keys configured for signing the file!"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L40:
aload 0
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keymode Ljava/lang/String;
aload 1
invokevirtual kellinwood/security/zipsigner/ZipSigner/autoDetectKey(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
astore 9
aload 9
ifnonnull L41
new kellinwood/security/zipsigner/AutoKeyException
dup
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/resourceAdapter Lkellinwood/security/zipsigner/ResourceAdapter;
getstatic kellinwood/security/zipsigner/ResourceAdapter$Item/AUTO_KEY_SELECTION_ERROR Lkellinwood/security/zipsigner/ResourceAdapter$Item;
iconst_1
anewarray java/lang/Object
dup
iconst_0
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getName()Ljava/lang/String;
aastore
invokeinterface kellinwood/security/zipsigner/ResourceAdapter/getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String; 2
invokespecial kellinwood/security/zipsigner/AutoKeyException/<init>(Ljava/lang/String;)V
athrow
L41:
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/autoKeyObservable Lkellinwood/security/zipsigner/ZipSigner$AutoKeyObservable;
aload 9
invokevirtual kellinwood/security/zipsigner/ZipSigner$AutoKeyObservable/notifyObservers(Ljava/lang/Object;)V
aload 0
aload 9
invokevirtual kellinwood/security/zipsigner/ZipSigner/loadKeys(Ljava/lang/String;)V
L0:
new kellinwood/zipio/ZipOutput
dup
aload 2
invokespecial kellinwood/zipio/ZipOutput/<init>(Ljava/io/OutputStream;)V
astore 2
L1:
ldc "none"
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
invokevirtual kellinwood/security/zipsigner/KeySet/getName()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L42
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
aload 1
invokeinterface java/util/Map/size()I 0
invokevirtual kellinwood/security/zipsigner/ProgressHelper/setProgressTotalItems(I)V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_0
invokevirtual kellinwood/security/zipsigner/ProgressHelper/setProgressCurrentItem(I)V
aload 0
aload 1
aload 2
invokespecial kellinwood/security/zipsigner/ZipSigner/copyFiles(Ljava/util/Map;Lkellinwood/zipio/ZipOutput;)V
L3:
aload 2
invokevirtual kellinwood/zipio/ZipOutput/close()V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
ifeq L6
aload 3
ifnull L6
L5:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L6:
return
L7:
astore 1
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
return
L42:
iconst_0
istore 4
L8:
aload 1
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 9
L9:
aload 9
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L11
aload 9
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast kellinwood/zipio/ZioEntry
astore 10
aload 10
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
astore 11
aload 10
invokevirtual kellinwood/zipio/ZioEntry/isDirectory()Z
ifne L9
aload 11
ldc "META-INF/MANIFEST.MF"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L9
aload 11
ldc "META-INF/CERT.SF"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L9
aload 11
ldc "META-INF/CERT.RSA"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L9
getstatic kellinwood/security/zipsigner/ZipSigner/stripPattern Ljava/util/regex/Pattern;
ifnull L43
getstatic kellinwood/security/zipsigner/ZipSigner/stripPattern Ljava/util/regex/Pattern;
aload 11
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
invokevirtual java/util/regex/Matcher/matches()Z
ifne L9
L10:
goto L43
L11:
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iload 4
iconst_1
iadd
invokevirtual kellinwood/security/zipsigner/ProgressHelper/setProgressTotalItems(I)V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_0
invokevirtual kellinwood/security/zipsigner/ProgressHelper/setProgressCurrentItem(I)V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
invokevirtual kellinwood/security/zipsigner/KeySet/getPublicKey()Ljava/security/cert/X509Certificate;
invokevirtual java/security/cert/X509Certificate/getNotBefore()Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
ldc2_w 3600000L
ladd
lstore 7
aload 0
aload 1
invokespecial kellinwood/security/zipsigner/ZipSigner/addDigestsToManifest(Ljava/util/Map;)Ljava/util/jar/Manifest;
astore 9
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
istore 6
L12:
iload 6
ifeq L16
aload 2
invokevirtual kellinwood/zipio/ZipOutput/close()V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
ifeq L6
aload 3
ifnull L6
L13:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L14:
return
L15:
astore 1
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
return
L16:
new kellinwood/zipio/ZioEntry
dup
ldc "META-INF/MANIFEST.MF"
invokespecial kellinwood/zipio/ZioEntry/<init>(Ljava/lang/String;)V
astore 10
aload 10
lload 7
invokevirtual kellinwood/zipio/ZioEntry/setTime(J)V
aload 9
aload 10
invokevirtual kellinwood/zipio/ZioEntry/getOutputStream()Ljava/io/OutputStream;
invokevirtual java/util/jar/Manifest/write(Ljava/io/OutputStream;)V
aload 2
aload 10
invokevirtual kellinwood/zipio/ZipOutput/write(Lkellinwood/zipio/ZioEntry;)V
new kellinwood/zipio/ZioEntry
dup
ldc "META-INF/CERT.SF"
invokespecial kellinwood/zipio/ZioEntry/<init>(Ljava/lang/String;)V
astore 10
aload 10
lload 7
invokevirtual kellinwood/zipio/ZioEntry/setTime(J)V
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 11
aload 0
aload 9
aload 11
invokespecial kellinwood/security/zipsigner/ZipSigner/generateSignatureFile(Ljava/util/jar/Manifest;Ljava/io/OutputStream;)V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
istore 6
L17:
iload 6
ifeq L21
aload 2
invokevirtual kellinwood/zipio/ZipOutput/close()V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
ifeq L6
aload 3
ifnull L6
L18:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L19:
return
L20:
astore 1
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
return
L21:
aload 11
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
astore 11
L22:
iload 5
ifeq L24
L23:
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Signature File: \n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/lang/String
dup
aload 11
invokespecial java/lang/String/<init>([B)V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokestatic kellinwood/security/zipsigner/HexDumpEncoder/encode([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L24:
aload 10
invokevirtual kellinwood/zipio/ZioEntry/getOutputStream()Ljava/io/OutputStream;
aload 11
invokevirtual java/io/OutputStream/write([B)V
aload 2
aload 10
invokevirtual kellinwood/zipio/ZipOutput/write(Lkellinwood/zipio/ZioEntry;)V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_0
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/resourceAdapter Lkellinwood/security/zipsigner/ResourceAdapter;
getstatic kellinwood/security/zipsigner/ResourceAdapter$Item/GENERATING_SIGNATURE_BLOCK Lkellinwood/security/zipsigner/ResourceAdapter$Item;
iconst_0
anewarray java/lang/Object
invokeinterface kellinwood/security/zipsigner/ResourceAdapter/getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String; 2
invokevirtual kellinwood/security/zipsigner/ProgressHelper/progress(ILjava/lang/String;)V
new kellinwood/zipio/ZioEntry
dup
ldc "META-INF/CERT.RSA"
invokespecial kellinwood/zipio/ZioEntry/<init>(Ljava/lang/String;)V
astore 10
aload 10
lload 7
invokevirtual kellinwood/zipio/ZioEntry/setTime(J)V
aload 0
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/keySet Lkellinwood/security/zipsigner/KeySet;
aload 11
aload 10
invokevirtual kellinwood/zipio/ZioEntry/getOutputStream()Ljava/io/OutputStream;
invokespecial kellinwood/security/zipsigner/ZipSigner/writeSignatureBlock(Lkellinwood/security/zipsigner/KeySet;[BLjava/io/OutputStream;)V
aload 2
aload 10
invokevirtual kellinwood/zipio/ZipOutput/write(Lkellinwood/zipio/ZioEntry;)V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
istore 5
L25:
iload 5
ifeq L29
aload 2
invokevirtual kellinwood/zipio/ZipOutput/close()V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
ifeq L6
aload 3
ifnull L6
L26:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L27:
return
L28:
astore 1
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
return
L29:
aload 0
aload 9
aload 1
aload 2
lload 7
invokespecial kellinwood/security/zipsigner/ZipSigner/copyFiles(Ljava/util/jar/Manifest;Ljava/util/Map;Lkellinwood/zipio/ZipOutput;J)V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
istore 5
L30:
iload 5
ifeq L44
aload 2
invokevirtual kellinwood/zipio/ZipOutput/close()V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
ifeq L6
aload 3
ifnull L6
L31:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L32:
return
L33:
astore 1
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
return
L44:
aload 2
invokevirtual kellinwood/zipio/ZipOutput/close()V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
ifeq L6
aload 3
ifnull L6
L34:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L35:
return
L36:
astore 1
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
return
L2:
astore 1
aconst_null
astore 2
L45:
aload 2
invokevirtual kellinwood/zipio/ZipOutput/close()V
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/canceled Z
ifeq L38
aload 3
ifnull L38
L37:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L38:
aload 1
athrow
L39:
astore 2
invokestatic kellinwood/security/zipsigner/ZipSigner/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
goto L38
L4:
astore 1
goto L45
L43:
iload 4
iconst_3
iadd
istore 4
goto L9
.limit locals 12
.limit stack 10
.end method

.method public signZip(Ljava/util/Map;Ljava/lang/String;)V
.signature "(Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;Ljava/lang/String;)V"
.throws java/io/IOException
.throws java/security/GeneralSecurityException
aload 0
getfield kellinwood/security/zipsigner/ZipSigner/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
invokevirtual kellinwood/security/zipsigner/ProgressHelper/initProgress()V
aload 0
aload 1
new java/io/FileOutputStream
dup
aload 2
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
aload 2
invokevirtual kellinwood/security/zipsigner/ZipSigner/signZip(Ljava/util/Map;Ljava/io/OutputStream;Ljava/lang/String;)V
return
.limit locals 3
.limit stack 5
.end method
