.bytecode 50.0
.class public synchronized kellinwood/security/zipsigner/ProgressEvent
.super java/lang/Object

.field public static final 'PRORITY_IMPORTANT' I = 1


.field public static final 'PRORITY_NORMAL' I = 0


.field private 'message' Ljava/lang/String;

.field private 'percentDone' I

.field private 'priority' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public getMessage()Ljava/lang/String;
aload 0
getfield kellinwood/security/zipsigner/ProgressEvent/message Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getPercentDone()I
aload 0
getfield kellinwood/security/zipsigner/ProgressEvent/percentDone I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getPriority()I
aload 0
getfield kellinwood/security/zipsigner/ProgressEvent/priority I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setMessage(Ljava/lang/String;)V
aload 0
aload 1
putfield kellinwood/security/zipsigner/ProgressEvent/message Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setPercentDone(I)V
aload 0
iload 1
putfield kellinwood/security/zipsigner/ProgressEvent/percentDone I
return
.limit locals 2
.limit stack 2
.end method

.method public setPriority(I)V
aload 0
iload 1
putfield kellinwood/security/zipsigner/ProgressEvent/priority I
return
.limit locals 2
.limit stack 2
.end method
