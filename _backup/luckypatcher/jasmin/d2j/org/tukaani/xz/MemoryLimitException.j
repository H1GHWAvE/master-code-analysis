.bytecode 50.0
.class public synchronized org/tukaani/xz/MemoryLimitException
.super org/tukaani/xz/XZIOException

.field private static final 'serialVersionUID' J = 3L


.field private final 'memoryLimit' I

.field private final 'memoryNeeded' I

.method public <init>(II)V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " KiB of memory would be needed; limit was "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " KiB"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
aload 0
iload 1
putfield org/tukaani/xz/MemoryLimitException/memoryNeeded I
aload 0
iload 2
putfield org/tukaani/xz/MemoryLimitException/memoryLimit I
return
.limit locals 3
.limit stack 3
.end method

.method public getMemoryLimit()I
aload 0
getfield org/tukaani/xz/MemoryLimitException/memoryLimit I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMemoryNeeded()I
aload 0
getfield org/tukaani/xz/MemoryLimitException/memoryNeeded I
ireturn
.limit locals 1
.limit stack 1
.end method
