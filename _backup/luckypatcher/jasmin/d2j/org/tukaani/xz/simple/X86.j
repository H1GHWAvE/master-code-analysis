.bytecode 50.0
.class public final synchronized org/tukaani/xz/simple/X86
.super java/lang/Object
.implements org/tukaani/xz/simple/SimpleFilter

.field private static final 'MASK_TO_ALLOWED_STATUS' [Z

.field private static final 'MASK_TO_BIT_NUMBER' [I

.field private final 'isEncoder' Z

.field private 'pos' I

.field private 'prevMask' I

.method static <clinit>()V
bipush 8
newarray boolean
dup
iconst_0
ldc_w 1
bastore
dup
iconst_1
ldc_w 1
bastore
dup
iconst_2
ldc_w 1
bastore
dup
iconst_3
ldc_w 0
bastore
dup
iconst_4
ldc_w 1
bastore
dup
iconst_5
ldc_w 0
bastore
dup
bipush 6
ldc_w 0
bastore
dup
bipush 7
ldc_w 0
bastore
putstatic org/tukaani/xz/simple/X86/MASK_TO_ALLOWED_STATUS [Z
bipush 8
newarray int
dup
iconst_0
iconst_0
iastore
dup
iconst_1
iconst_1
iastore
dup
iconst_2
iconst_2
iastore
dup
iconst_3
iconst_2
iastore
dup
iconst_4
iconst_3
iastore
dup
iconst_5
iconst_3
iastore
dup
bipush 6
iconst_3
iastore
dup
bipush 7
iconst_3
iastore
putstatic org/tukaani/xz/simple/X86/MASK_TO_BIT_NUMBER [I
return
.limit locals 0
.limit stack 4
.end method

.method public <init>(ZI)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield org/tukaani/xz/simple/X86/prevMask I
aload 0
iload 1
putfield org/tukaani/xz/simple/X86/isEncoder Z
aload 0
iload 2
iconst_5
iadd
putfield org/tukaani/xz/simple/X86/pos I
return
.limit locals 3
.limit stack 3
.end method

.method private static test86MSByte(B)Z
iload 0
sipush 255
iand
istore 0
iload 0
ifeq L0
iload 0
sipush 255
if_icmpne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public code([BII)I
iconst_0
istore 7
iload 2
iconst_1
isub
istore 5
iload 2
istore 4
L0:
iload 4
iload 2
iload 3
iadd
iconst_5
isub
if_icmpgt L1
aload 1
iload 4
baload
sipush 254
iand
sipush 232
if_icmpeq L2
L3:
iload 4
iconst_1
iadd
istore 4
goto L0
L2:
iload 4
iload 5
isub
istore 5
iload 5
bipush -4
iand
ifeq L4
aload 0
iconst_0
putfield org/tukaani/xz/simple/X86/prevMask I
L5:
iload 4
istore 5
aload 1
iload 4
iconst_4
iadd
baload
invokestatic org/tukaani/xz/simple/X86/test86MSByte(B)Z
ifeq L6
aload 1
iload 4
iconst_1
iadd
baload
sipush 255
iand
aload 1
iload 4
iconst_2
iadd
baload
sipush 255
iand
bipush 8
ishl
ior
aload 1
iload 4
iconst_3
iadd
baload
sipush 255
iand
bipush 16
ishl
ior
aload 1
iload 4
iconst_4
iadd
baload
sipush 255
iand
bipush 24
ishl
ior
istore 6
L7:
aload 0
getfield org/tukaani/xz/simple/X86/isEncoder Z
ifeq L8
iload 6
aload 0
getfield org/tukaani/xz/simple/X86/pos I
iload 4
iadd
iload 2
isub
iadd
istore 6
L9:
aload 0
getfield org/tukaani/xz/simple/X86/prevMask I
ifne L10
L11:
aload 1
iload 4
iconst_1
iadd
iload 6
i2b
bastore
aload 1
iload 4
iconst_2
iadd
iload 6
bipush 8
iushr
i2b
bastore
aload 1
iload 4
iconst_3
iadd
iload 6
bipush 16
iushr
i2b
bastore
aload 1
iload 4
iconst_4
iadd
iload 6
bipush 24
iushr
iconst_1
iand
iconst_1
isub
iconst_m1
ixor
i2b
bastore
iload 4
iconst_4
iadd
istore 4
goto L3
L4:
aload 0
aload 0
getfield org/tukaani/xz/simple/X86/prevMask I
iload 5
iconst_1
isub
ishl
bipush 7
iand
putfield org/tukaani/xz/simple/X86/prevMask I
aload 0
getfield org/tukaani/xz/simple/X86/prevMask I
ifeq L5
getstatic org/tukaani/xz/simple/X86/MASK_TO_ALLOWED_STATUS [Z
aload 0
getfield org/tukaani/xz/simple/X86/prevMask I
baload
ifeq L12
aload 1
iload 4
iconst_4
iadd
getstatic org/tukaani/xz/simple/X86/MASK_TO_BIT_NUMBER [I
aload 0
getfield org/tukaani/xz/simple/X86/prevMask I
iaload
isub
baload
invokestatic org/tukaani/xz/simple/X86/test86MSByte(B)Z
ifeq L5
L12:
iload 4
istore 5
aload 0
aload 0
getfield org/tukaani/xz/simple/X86/prevMask I
iconst_1
ishl
iconst_1
ior
putfield org/tukaani/xz/simple/X86/prevMask I
goto L3
L8:
iload 6
aload 0
getfield org/tukaani/xz/simple/X86/pos I
iload 4
iadd
iload 2
isub
isub
istore 6
goto L9
L10:
getstatic org/tukaani/xz/simple/X86/MASK_TO_BIT_NUMBER [I
aload 0
getfield org/tukaani/xz/simple/X86/prevMask I
iaload
bipush 8
imul
istore 8
iload 6
bipush 24
iload 8
isub
iushr
i2b
invokestatic org/tukaani/xz/simple/X86/test86MSByte(B)Z
ifeq L11
iload 6
iconst_1
bipush 32
iload 8
isub
ishl
iconst_1
isub
ixor
istore 6
goto L7
L6:
aload 0
aload 0
getfield org/tukaani/xz/simple/X86/prevMask I
iconst_1
ishl
iconst_1
ior
putfield org/tukaani/xz/simple/X86/prevMask I
goto L3
L1:
iload 4
iload 5
isub
istore 3
iload 3
bipush -4
iand
ifeq L13
iload 7
istore 3
L14:
aload 0
iload 3
putfield org/tukaani/xz/simple/X86/prevMask I
iload 4
iload 2
isub
istore 2
aload 0
aload 0
getfield org/tukaani/xz/simple/X86/pos I
iload 2
iadd
putfield org/tukaani/xz/simple/X86/pos I
iload 2
ireturn
L13:
aload 0
getfield org/tukaani/xz/simple/X86/prevMask I
iload 3
iconst_1
isub
ishl
istore 3
goto L14
.limit locals 9
.limit stack 4
.end method
