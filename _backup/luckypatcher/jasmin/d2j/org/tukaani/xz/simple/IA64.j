.bytecode 50.0
.class public final synchronized org/tukaani/xz/simple/IA64
.super java/lang/Object
.implements org/tukaani/xz/simple/SimpleFilter

.field private static final 'BRANCH_TABLE' [I

.field private final 'isEncoder' Z

.field private 'pos' I

.method static <clinit>()V
bipush 32
newarray int
dup
iconst_0
iconst_0
iastore
dup
iconst_1
iconst_0
iastore
dup
iconst_2
iconst_0
iastore
dup
iconst_3
iconst_0
iastore
dup
iconst_4
iconst_0
iastore
dup
iconst_5
iconst_0
iastore
dup
bipush 6
iconst_0
iastore
dup
bipush 7
iconst_0
iastore
dup
bipush 8
iconst_0
iastore
dup
bipush 9
iconst_0
iastore
dup
bipush 10
iconst_0
iastore
dup
bipush 11
iconst_0
iastore
dup
bipush 12
iconst_0
iastore
dup
bipush 13
iconst_0
iastore
dup
bipush 14
iconst_0
iastore
dup
bipush 15
iconst_0
iastore
dup
bipush 16
iconst_4
iastore
dup
bipush 17
iconst_4
iastore
dup
bipush 18
bipush 6
iastore
dup
bipush 19
bipush 6
iastore
dup
bipush 20
iconst_0
iastore
dup
bipush 21
iconst_0
iastore
dup
bipush 22
bipush 7
iastore
dup
bipush 23
bipush 7
iastore
dup
bipush 24
iconst_4
iastore
dup
bipush 25
iconst_4
iastore
dup
bipush 26
iconst_0
iastore
dup
bipush 27
iconst_0
iastore
dup
bipush 28
iconst_4
iastore
dup
bipush 29
iconst_4
iastore
dup
bipush 30
iconst_0
iastore
dup
bipush 31
iconst_0
iastore
putstatic org/tukaani/xz/simple/IA64/BRANCH_TABLE [I
return
.limit locals 0
.limit stack 4
.end method

.method public <init>(ZI)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield org/tukaani/xz/simple/IA64/isEncoder Z
aload 0
iload 2
putfield org/tukaani/xz/simple/IA64/pos I
return
.limit locals 3
.limit stack 2
.end method

.method public code([BII)I
iload 2
istore 4
L0:
iload 4
iload 2
iload 3
iadd
bipush 16
isub
if_icmpgt L1
aload 1
iload 4
baload
istore 5
getstatic org/tukaani/xz/simple/IA64/BRANCH_TABLE [I
iload 5
bipush 31
iand
iaload
istore 8
iconst_0
istore 6
iconst_5
istore 5
L2:
iload 6
iconst_3
if_icmpge L3
iload 8
iload 6
iushr
iconst_1
iand
ifne L4
L5:
iload 6
iconst_1
iadd
istore 6
iload 5
bipush 41
iadd
istore 5
goto L2
L4:
iload 5
iconst_3
iushr
istore 9
iload 5
bipush 7
iand
istore 10
lconst_0
lstore 11
iconst_0
istore 7
L6:
iload 7
bipush 6
if_icmpge L7
lload 11
aload 1
iload 4
iload 9
iadd
iload 7
iadd
baload
i2l
ldc2_w 255L
land
iload 7
bipush 8
imul
lshl
lor
lstore 11
iload 7
iconst_1
iadd
istore 7
goto L6
L7:
lload 11
iload 10
lushr
lstore 13
lload 13
bipush 37
lushr
ldc2_w 15L
land
ldc2_w 5L
lcmp
ifne L5
lload 13
bipush 9
lushr
ldc2_w 7L
land
lconst_0
lcmp
ifne L5
lload 13
bipush 13
lushr
ldc2_w 1048575L
land
l2i
lload 13
bipush 36
lushr
l2i
iconst_1
iand
bipush 20
ishl
ior
iconst_4
ishl
istore 7
aload 0
getfield org/tukaani/xz/simple/IA64/isEncoder Z
ifeq L8
iload 7
aload 0
getfield org/tukaani/xz/simple/IA64/pos I
iload 4
iadd
iload 2
isub
iadd
istore 7
L9:
iload 7
iconst_4
iushr
istore 7
iload 7
i2l
lstore 15
iload 7
i2l
lstore 17
iconst_1
iload 10
ishl
iconst_1
isub
i2l
lstore 19
iconst_0
istore 7
L10:
iload 7
bipush 6
if_icmpge L5
aload 1
iload 4
iload 9
iadd
iload 7
iadd
lload 11
lload 19
land
lload 13
ldc2_w -77309403137L
land
lload 15
ldc2_w 1048575L
land
bipush 13
lshl
lor
lload 17
ldc2_w 1048576L
land
bipush 16
lshl
lor
iload 10
lshl
lor
iload 7
bipush 8
imul
lushr
l2i
i2b
bastore
iload 7
iconst_1
iadd
istore 7
goto L10
L8:
iload 7
aload 0
getfield org/tukaani/xz/simple/IA64/pos I
iload 4
iadd
iload 2
isub
isub
istore 7
goto L9
L3:
iload 4
bipush 16
iadd
istore 4
goto L0
L1:
iload 4
iload 2
isub
istore 2
aload 0
aload 0
getfield org/tukaani/xz/simple/IA64/pos I
iload 2
iadd
putfield org/tukaani/xz/simple/IA64/pos I
iload 2
ireturn
.limit locals 21
.limit stack 10
.end method
