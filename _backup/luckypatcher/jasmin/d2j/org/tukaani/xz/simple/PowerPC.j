.bytecode 50.0
.class public final synchronized org/tukaani/xz/simple/PowerPC
.super java/lang/Object
.implements org/tukaani/xz/simple/SimpleFilter

.field private final 'isEncoder' Z

.field private 'pos' I

.method public <init>(ZI)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield org/tukaani/xz/simple/PowerPC/isEncoder Z
aload 0
iload 2
putfield org/tukaani/xz/simple/PowerPC/pos I
return
.limit locals 3
.limit stack 2
.end method

.method public code([BII)I
iload 2
istore 4
L0:
iload 4
iload 2
iload 3
iadd
iconst_4
isub
if_icmpgt L1
aload 1
iload 4
baload
sipush 252
iand
bipush 72
if_icmpne L2
aload 1
iload 4
iconst_3
iadd
baload
iconst_3
iand
iconst_1
if_icmpne L2
aload 1
iload 4
baload
iconst_3
iand
bipush 24
ishl
aload 1
iload 4
iconst_1
iadd
baload
sipush 255
iand
bipush 16
ishl
ior
aload 1
iload 4
iconst_2
iadd
baload
sipush 255
iand
bipush 8
ishl
ior
aload 1
iload 4
iconst_3
iadd
baload
sipush 252
iand
ior
istore 5
aload 0
getfield org/tukaani/xz/simple/PowerPC/isEncoder Z
ifeq L3
iload 5
aload 0
getfield org/tukaani/xz/simple/PowerPC/pos I
iload 4
iadd
iload 2
isub
iadd
istore 5
L4:
aload 1
iload 4
iload 5
bipush 24
iushr
iconst_3
iand
bipush 72
ior
i2b
bastore
aload 1
iload 4
iconst_1
iadd
iload 5
bipush 16
iushr
i2b
bastore
aload 1
iload 4
iconst_2
iadd
iload 5
bipush 8
iushr
i2b
bastore
aload 1
iload 4
iconst_3
iadd
aload 1
iload 4
iconst_3
iadd
baload
iconst_3
iand
iload 5
ior
i2b
bastore
L2:
iload 4
iconst_4
iadd
istore 4
goto L0
L3:
iload 5
aload 0
getfield org/tukaani/xz/simple/PowerPC/pos I
iload 4
iadd
iload 2
isub
isub
istore 5
goto L4
L1:
iload 4
iload 2
isub
istore 2
aload 0
aload 0
getfield org/tukaani/xz/simple/PowerPC/pos I
iload 2
iadd
putfield org/tukaani/xz/simple/PowerPC/pos I
iload 2
ireturn
.limit locals 6
.limit stack 5
.end method
