.bytecode 50.0
.class public final synchronized org/tukaani/xz/simple/ARMThumb
.super java/lang/Object
.implements org/tukaani/xz/simple/SimpleFilter

.field private final 'isEncoder' Z

.field private 'pos' I

.method public <init>(ZI)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield org/tukaani/xz/simple/ARMThumb/isEncoder Z
aload 0
iload 2
iconst_4
iadd
putfield org/tukaani/xz/simple/ARMThumb/pos I
return
.limit locals 3
.limit stack 3
.end method

.method public code([BII)I
iload 2
istore 4
L0:
iload 4
iload 2
iload 3
iadd
iconst_4
isub
if_icmpgt L1
iload 4
istore 5
aload 1
iload 4
iconst_1
iadd
baload
sipush 248
iand
sipush 240
if_icmpne L2
iload 4
istore 5
aload 1
iload 4
iconst_3
iadd
baload
sipush 248
iand
sipush 248
if_icmpne L2
aload 1
iload 4
iconst_1
iadd
baload
bipush 7
iand
bipush 19
ishl
aload 1
iload 4
baload
sipush 255
iand
bipush 11
ishl
ior
aload 1
iload 4
iconst_3
iadd
baload
bipush 7
iand
bipush 8
ishl
ior
aload 1
iload 4
iconst_2
iadd
baload
sipush 255
iand
ior
iconst_1
ishl
istore 5
aload 0
getfield org/tukaani/xz/simple/ARMThumb/isEncoder Z
ifeq L3
iload 5
aload 0
getfield org/tukaani/xz/simple/ARMThumb/pos I
iload 4
iadd
iload 2
isub
iadd
istore 5
L4:
iload 5
iconst_1
iushr
istore 5
aload 1
iload 4
iconst_1
iadd
iload 5
bipush 19
iushr
bipush 7
iand
sipush 240
ior
i2b
bastore
aload 1
iload 4
iload 5
bipush 11
iushr
i2b
bastore
aload 1
iload 4
iconst_3
iadd
iload 5
bipush 8
iushr
bipush 7
iand
sipush 248
ior
i2b
bastore
aload 1
iload 4
iconst_2
iadd
iload 5
i2b
bastore
iload 4
iconst_2
iadd
istore 5
L2:
iload 5
iconst_2
iadd
istore 4
goto L0
L3:
iload 5
aload 0
getfield org/tukaani/xz/simple/ARMThumb/pos I
iload 4
iadd
iload 2
isub
isub
istore 5
goto L4
L1:
iload 4
iload 2
isub
istore 2
aload 0
aload 0
getfield org/tukaani/xz/simple/ARMThumb/pos I
iload 2
iadd
putfield org/tukaani/xz/simple/ARMThumb/pos I
iload 2
ireturn
.limit locals 6
.limit stack 4
.end method
