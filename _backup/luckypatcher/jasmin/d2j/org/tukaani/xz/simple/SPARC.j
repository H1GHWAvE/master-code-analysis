.bytecode 50.0
.class public final synchronized org/tukaani/xz/simple/SPARC
.super java/lang/Object
.implements org/tukaani/xz/simple/SimpleFilter

.field private final 'isEncoder' Z

.field private 'pos' I

.method public <init>(ZI)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield org/tukaani/xz/simple/SPARC/isEncoder Z
aload 0
iload 2
putfield org/tukaani/xz/simple/SPARC/pos I
return
.limit locals 3
.limit stack 2
.end method

.method public code([BII)I
iload 2
istore 4
L0:
iload 4
iload 2
iload 3
iadd
iconst_4
isub
if_icmpgt L1
aload 1
iload 4
baload
bipush 64
if_icmpne L2
aload 1
iload 4
iconst_1
iadd
baload
sipush 192
iand
ifeq L3
L2:
aload 1
iload 4
baload
bipush 127
if_icmpne L4
aload 1
iload 4
iconst_1
iadd
baload
sipush 192
iand
sipush 192
if_icmpne L4
L3:
aload 1
iload 4
baload
sipush 255
iand
bipush 24
ishl
aload 1
iload 4
iconst_1
iadd
baload
sipush 255
iand
bipush 16
ishl
ior
aload 1
iload 4
iconst_2
iadd
baload
sipush 255
iand
bipush 8
ishl
ior
aload 1
iload 4
iconst_3
iadd
baload
sipush 255
iand
ior
iconst_2
ishl
istore 5
aload 0
getfield org/tukaani/xz/simple/SPARC/isEncoder Z
ifeq L5
iload 5
aload 0
getfield org/tukaani/xz/simple/SPARC/pos I
iload 4
iadd
iload 2
isub
iadd
istore 5
L6:
iload 5
iconst_2
iushr
istore 5
iconst_0
iload 5
bipush 22
iushr
iconst_1
iand
isub
bipush 22
ishl
ldc_w 1073741823
iand
ldc_w 4194303
iload 5
iand
ior
ldc_w 1073741824
ior
istore 5
aload 1
iload 4
iload 5
bipush 24
iushr
i2b
bastore
aload 1
iload 4
iconst_1
iadd
iload 5
bipush 16
iushr
i2b
bastore
aload 1
iload 4
iconst_2
iadd
iload 5
bipush 8
iushr
i2b
bastore
aload 1
iload 4
iconst_3
iadd
iload 5
i2b
bastore
L4:
iload 4
iconst_4
iadd
istore 4
goto L0
L5:
iload 5
aload 0
getfield org/tukaani/xz/simple/SPARC/pos I
iload 4
iadd
iload 2
isub
isub
istore 5
goto L6
L1:
iload 4
iload 2
isub
istore 2
aload 0
aload 0
getfield org/tukaani/xz/simple/SPARC/pos I
iload 2
iadd
putfield org/tukaani/xz/simple/SPARC/pos I
iload 2
ireturn
.limit locals 6
.limit stack 4
.end method
