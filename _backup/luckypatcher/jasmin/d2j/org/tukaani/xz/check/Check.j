.bytecode 50.0
.class public synchronized abstract org/tukaani/xz/check/Check
.super java/lang/Object

.field 'name' Ljava/lang/String;

.field 'size' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getInstance(I)Lorg/tukaani/xz/check/Check;
.throws org/tukaani/xz/UnsupportedOptionsException
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
iload 0
lookupswitch
0 : L3
1 : L4
4 : L5
10 : L0
default : L6
L6:
new org/tukaani/xz/UnsupportedOptionsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unsupported Check ID "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 0
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L3:
new org/tukaani/xz/check/None
dup
invokespecial org/tukaani/xz/check/None/<init>()V
areturn
L4:
new org/tukaani/xz/check/CRC32
dup
invokespecial org/tukaani/xz/check/CRC32/<init>()V
areturn
L5:
new org/tukaani/xz/check/CRC64
dup
invokespecial org/tukaani/xz/check/CRC64/<init>()V
areturn
L0:
new org/tukaani/xz/check/SHA256
dup
invokespecial org/tukaani/xz/check/SHA256/<init>()V
astore 1
L1:
aload 1
areturn
L2:
astore 1
goto L6
.limit locals 2
.limit stack 4
.end method

.method public abstract finish()[B
.end method

.method public getName()Ljava/lang/String;
aload 0
getfield org/tukaani/xz/check/Check/name Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSize()I
aload 0
getfield org/tukaani/xz/check/Check/size I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public update([B)V
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual org/tukaani/xz/check/Check/update([BII)V
return
.limit locals 2
.limit stack 4
.end method

.method public abstract update([BII)V
.end method
