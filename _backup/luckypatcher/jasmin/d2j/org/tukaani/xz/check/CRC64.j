.bytecode 50.0
.class public synchronized org/tukaani/xz/check/CRC64
.super org/tukaani/xz/check/Check

.field private static final 'crcTable' [J

.field private static final 'poly' J = -3932672073523589310L


.field private 'crc' J

.method static <clinit>()V
sipush 256
newarray long
putstatic org/tukaani/xz/check/CRC64/crcTable [J
iconst_0
istore 0
L0:
iload 0
getstatic org/tukaani/xz/check/CRC64/crcTable [J
arraylength
if_icmpge L1
iload 0
i2l
lstore 2
iconst_0
istore 1
L2:
iload 1
bipush 8
if_icmpge L3
lload 2
lconst_1
land
lconst_1
lcmp
ifne L4
lload 2
iconst_1
lushr
ldc2_w -3932672073523589310L
lxor
lstore 2
L5:
iload 1
iconst_1
iadd
istore 1
goto L2
L4:
lload 2
iconst_1
lushr
lstore 2
goto L5
L3:
getstatic org/tukaani/xz/check/CRC64/crcTable [J
iload 0
lload 2
lastore
iload 0
iconst_1
iadd
istore 0
goto L0
L1:
return
.limit locals 4
.limit stack 4
.end method

.method public <init>()V
aload 0
invokespecial org/tukaani/xz/check/Check/<init>()V
aload 0
ldc2_w -1L
putfield org/tukaani/xz/check/CRC64/crc J
aload 0
bipush 8
putfield org/tukaani/xz/check/CRC64/size I
aload 0
ldc "CRC64"
putfield org/tukaani/xz/check/CRC64/name Ljava/lang/String;
return
.limit locals 1
.limit stack 3
.end method

.method public finish()[B
aload 0
getfield org/tukaani/xz/check/CRC64/crc J
lstore 2
aload 0
ldc2_w -1L
putfield org/tukaani/xz/check/CRC64/crc J
bipush 8
newarray byte
astore 4
iconst_0
istore 1
L0:
iload 1
aload 4
arraylength
if_icmpge L1
aload 4
iload 1
lload 2
ldc2_w -1L
lxor
iload 1
bipush 8
imul
lshr
l2i
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 4
areturn
.limit locals 5
.limit stack 6
.end method

.method public update([BII)V
iload 2
istore 4
L0:
iload 4
iload 2
iload 3
iadd
if_icmpge L1
aload 0
getstatic org/tukaani/xz/check/CRC64/crcTable [J
aload 1
iload 4
baload
aload 0
getfield org/tukaani/xz/check/CRC64/crc J
l2i
ixor
sipush 255
iand
laload
aload 0
getfield org/tukaani/xz/check/CRC64/crc J
bipush 8
lushr
lxor
putfield org/tukaani/xz/check/CRC64/crc J
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
return
.limit locals 5
.limit stack 6
.end method
