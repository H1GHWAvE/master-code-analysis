.bytecode 50.0
.class public synchronized org/tukaani/xz/check/CRC32
.super org/tukaani/xz/check/Check

.field private final 'state' Ljava/util/zip/CRC32;

.method public <init>()V
aload 0
invokespecial org/tukaani/xz/check/Check/<init>()V
aload 0
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
putfield org/tukaani/xz/check/CRC32/state Ljava/util/zip/CRC32;
aload 0
iconst_4
putfield org/tukaani/xz/check/CRC32/size I
aload 0
ldc "CRC32"
putfield org/tukaani/xz/check/CRC32/name Ljava/lang/String;
return
.limit locals 1
.limit stack 3
.end method

.method public finish()[B
aload 0
getfield org/tukaani/xz/check/CRC32/state Ljava/util/zip/CRC32;
invokevirtual java/util/zip/CRC32/getValue()J
lstore 5
lload 5
l2i
i2b
istore 1
lload 5
bipush 8
lushr
l2i
i2b
istore 2
lload 5
bipush 16
lushr
l2i
i2b
istore 3
lload 5
bipush 24
lushr
l2i
i2b
istore 4
aload 0
getfield org/tukaani/xz/check/CRC32/state Ljava/util/zip/CRC32;
invokevirtual java/util/zip/CRC32/reset()V
iconst_4
newarray byte
dup
iconst_0
iload 1
bastore
dup
iconst_1
iload 2
bastore
dup
iconst_2
iload 3
bastore
dup
iconst_3
iload 4
bastore
areturn
.limit locals 7
.limit stack 4
.end method

.method public update([BII)V
aload 0
getfield org/tukaani/xz/check/CRC32/state Ljava/util/zip/CRC32;
aload 1
iload 2
iload 3
invokevirtual java/util/zip/CRC32/update([BII)V
return
.limit locals 4
.limit stack 4
.end method
