.bytecode 50.0
.class public synchronized org/tukaani/xz/check/SHA256
.super org/tukaani/xz/check/Check

.field private final 'sha256' Ljava/security/MessageDigest;

.method public <init>()V
.throws java/security/NoSuchAlgorithmException
aload 0
invokespecial org/tukaani/xz/check/Check/<init>()V
aload 0
bipush 32
putfield org/tukaani/xz/check/SHA256/size I
aload 0
ldc "SHA-256"
putfield org/tukaani/xz/check/SHA256/name Ljava/lang/String;
aload 0
ldc "SHA-256"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
putfield org/tukaani/xz/check/SHA256/sha256 Ljava/security/MessageDigest;
return
.limit locals 1
.limit stack 2
.end method

.method public finish()[B
aload 0
getfield org/tukaani/xz/check/SHA256/sha256 Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/digest()[B
astore 1
aload 0
getfield org/tukaani/xz/check/SHA256/sha256 Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/reset()V
aload 1
areturn
.limit locals 2
.limit stack 1
.end method

.method public update([BII)V
aload 0
getfield org/tukaani/xz/check/SHA256/sha256 Ljava/security/MessageDigest;
aload 1
iload 2
iload 3
invokevirtual java/security/MessageDigest/update([BII)V
return
.limit locals 4
.limit stack 4
.end method
