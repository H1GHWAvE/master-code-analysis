.bytecode 50.0
.class synchronized org/tukaani/xz/BlockOutputStream
.super org/tukaani/xz/FinishableOutputStream

.field private final 'check' Lorg/tukaani/xz/check/Check;

.field private final 'compressedSizeLimit' J

.field private 'filterChain' Lorg/tukaani/xz/FinishableOutputStream;

.field private final 'headerSize' I

.field private final 'out' Ljava/io/OutputStream;

.field private final 'outCounted' Lorg/tukaani/xz/CountingOutputStream;

.field private final 'tempBuf' [B

.field private 'uncompressedSize' J

.method public <init>(Ljava/io/OutputStream;[Lorg/tukaani/xz/FilterEncoder;Lorg/tukaani/xz/check/Check;)V
.throws java/io/IOException
aload 0
invokespecial org/tukaani/xz/FinishableOutputStream/<init>()V
aload 0
lconst_0
putfield org/tukaani/xz/BlockOutputStream/uncompressedSize J
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/BlockOutputStream/tempBuf [B
aload 0
aload 1
putfield org/tukaani/xz/BlockOutputStream/out Ljava/io/OutputStream;
aload 0
aload 3
putfield org/tukaani/xz/BlockOutputStream/check Lorg/tukaani/xz/check/Check;
aload 0
new org/tukaani/xz/CountingOutputStream
dup
aload 1
invokespecial org/tukaani/xz/CountingOutputStream/<init>(Ljava/io/OutputStream;)V
putfield org/tukaani/xz/BlockOutputStream/outCounted Lorg/tukaani/xz/CountingOutputStream;
aload 0
aload 0
getfield org/tukaani/xz/BlockOutputStream/outCounted Lorg/tukaani/xz/CountingOutputStream;
putfield org/tukaani/xz/BlockOutputStream/filterChain Lorg/tukaani/xz/FinishableOutputStream;
aload 2
arraylength
iconst_1
isub
istore 4
L0:
iload 4
iflt L1
aload 0
aload 2
iload 4
aaload
aload 0
getfield org/tukaani/xz/BlockOutputStream/filterChain Lorg/tukaani/xz/FinishableOutputStream;
invokeinterface org/tukaani/xz/FilterEncoder/getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream; 1
putfield org/tukaani/xz/BlockOutputStream/filterChain Lorg/tukaani/xz/FinishableOutputStream;
iload 4
iconst_1
isub
istore 4
goto L0
L1:
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 5
aload 5
iconst_0
invokevirtual java/io/ByteArrayOutputStream/write(I)V
aload 5
aload 2
arraylength
iconst_1
isub
invokevirtual java/io/ByteArrayOutputStream/write(I)V
iconst_0
istore 4
L2:
iload 4
aload 2
arraylength
if_icmpge L3
aload 5
aload 2
iload 4
aaload
invokeinterface org/tukaani/xz/FilterEncoder/getFilterID()J 0
invokestatic org/tukaani/xz/common/EncoderUtil/encodeVLI(Ljava/io/OutputStream;J)V
aload 2
iload 4
aaload
invokeinterface org/tukaani/xz/FilterEncoder/getFilterProps()[B 0
astore 6
aload 5
aload 6
arraylength
i2l
invokestatic org/tukaani/xz/common/EncoderUtil/encodeVLI(Ljava/io/OutputStream;J)V
aload 5
aload 6
invokevirtual java/io/ByteArrayOutputStream/write([B)V
iload 4
iconst_1
iadd
istore 4
goto L2
L3:
aload 5
invokevirtual java/io/ByteArrayOutputStream/size()I
iconst_3
iand
ifeq L4
aload 5
iconst_0
invokevirtual java/io/ByteArrayOutputStream/write(I)V
goto L3
L4:
aload 5
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
astore 2
aload 0
aload 2
arraylength
iconst_4
iadd
putfield org/tukaani/xz/BlockOutputStream/headerSize I
aload 0
getfield org/tukaani/xz/BlockOutputStream/headerSize I
sipush 1024
if_icmple L5
new org/tukaani/xz/UnsupportedOptionsException
dup
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>()V
athrow
L5:
aload 2
iconst_0
aload 2
arraylength
iconst_4
idiv
i2b
bastore
aload 1
aload 2
invokevirtual java/io/OutputStream/write([B)V
aload 1
aload 2
invokestatic org/tukaani/xz/common/EncoderUtil/writeCRC32(Ljava/io/OutputStream;[B)V
aload 0
ldc2_w 9223372036854775804L
aload 0
getfield org/tukaani/xz/BlockOutputStream/headerSize I
i2l
lsub
aload 3
invokevirtual org/tukaani/xz/check/Check/getSize()I
i2l
lsub
putfield org/tukaani/xz/BlockOutputStream/compressedSizeLimit J
return
.limit locals 7
.limit stack 5
.end method

.method private validate()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/BlockOutputStream/outCounted Lorg/tukaani/xz/CountingOutputStream;
invokevirtual org/tukaani/xz/CountingOutputStream/getSize()J
lstore 1
lload 1
lconst_0
lcmp
iflt L0
lload 1
aload 0
getfield org/tukaani/xz/BlockOutputStream/compressedSizeLimit J
lcmp
ifgt L0
aload 0
getfield org/tukaani/xz/BlockOutputStream/uncompressedSize J
lconst_0
lcmp
ifge L1
L0:
new org/tukaani/xz/XZIOException
dup
ldc "XZ Stream has grown too big"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L1:
return
.limit locals 3
.limit stack 4
.end method

.method public finish()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/BlockOutputStream/filterChain Lorg/tukaani/xz/FinishableOutputStream;
invokevirtual org/tukaani/xz/FinishableOutputStream/finish()V
aload 0
invokespecial org/tukaani/xz/BlockOutputStream/validate()V
aload 0
getfield org/tukaani/xz/BlockOutputStream/outCounted Lorg/tukaani/xz/CountingOutputStream;
invokevirtual org/tukaani/xz/CountingOutputStream/getSize()J
lstore 1
L0:
ldc2_w 3L
lload 1
land
lconst_0
lcmp
ifeq L1
aload 0
getfield org/tukaani/xz/BlockOutputStream/out Ljava/io/OutputStream;
iconst_0
invokevirtual java/io/OutputStream/write(I)V
lload 1
lconst_1
ladd
lstore 1
goto L0
L1:
aload 0
getfield org/tukaani/xz/BlockOutputStream/out Ljava/io/OutputStream;
aload 0
getfield org/tukaani/xz/BlockOutputStream/check Lorg/tukaani/xz/check/Check;
invokevirtual org/tukaani/xz/check/Check/finish()[B
invokevirtual java/io/OutputStream/write([B)V
return
.limit locals 3
.limit stack 4
.end method

.method public flush()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/BlockOutputStream/filterChain Lorg/tukaani/xz/FinishableOutputStream;
invokevirtual org/tukaani/xz/FinishableOutputStream/flush()V
aload 0
invokespecial org/tukaani/xz/BlockOutputStream/validate()V
return
.limit locals 1
.limit stack 1
.end method

.method public getUncompressedSize()J
aload 0
getfield org/tukaani/xz/BlockOutputStream/uncompressedSize J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getUnpaddedSize()J
aload 0
getfield org/tukaani/xz/BlockOutputStream/headerSize I
i2l
aload 0
getfield org/tukaani/xz/BlockOutputStream/outCounted Lorg/tukaani/xz/CountingOutputStream;
invokevirtual org/tukaani/xz/CountingOutputStream/getSize()J
ladd
aload 0
getfield org/tukaani/xz/BlockOutputStream/check Lorg/tukaani/xz/check/Check;
invokevirtual org/tukaani/xz/check/Check/getSize()I
i2l
ladd
lreturn
.limit locals 1
.limit stack 4
.end method

.method public write(I)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/BlockOutputStream/tempBuf [B
iconst_0
iload 1
i2b
bastore
aload 0
aload 0
getfield org/tukaani/xz/BlockOutputStream/tempBuf [B
iconst_0
iconst_1
invokevirtual org/tukaani/xz/BlockOutputStream/write([BII)V
return
.limit locals 2
.limit stack 4
.end method

.method public write([BII)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/BlockOutputStream/filterChain Lorg/tukaani/xz/FinishableOutputStream;
aload 1
iload 2
iload 3
invokevirtual org/tukaani/xz/FinishableOutputStream/write([BII)V
aload 0
getfield org/tukaani/xz/BlockOutputStream/check Lorg/tukaani/xz/check/Check;
aload 1
iload 2
iload 3
invokevirtual org/tukaani/xz/check/Check/update([BII)V
aload 0
aload 0
getfield org/tukaani/xz/BlockOutputStream/uncompressedSize J
iload 3
i2l
ladd
putfield org/tukaani/xz/BlockOutputStream/uncompressedSize J
aload 0
invokespecial org/tukaani/xz/BlockOutputStream/validate()V
return
.limit locals 4
.limit stack 5
.end method
