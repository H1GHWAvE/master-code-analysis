.bytecode 50.0
.class public synchronized org/tukaani/xz/LZMA2Options
.super org/tukaani/xz/FilterOptions

.field static final synthetic '$assertionsDisabled' Z

.field public static final 'DICT_SIZE_DEFAULT' I = 8388608


.field public static final 'DICT_SIZE_MAX' I = 805306368


.field public static final 'DICT_SIZE_MIN' I = 4096


.field public static final 'LC_DEFAULT' I = 3


.field public static final 'LC_LP_MAX' I = 4


.field public static final 'LP_DEFAULT' I = 0


.field public static final 'MF_BT4' I = 20


.field public static final 'MF_HC4' I = 4


.field public static final 'MODE_FAST' I = 1


.field public static final 'MODE_NORMAL' I = 2


.field public static final 'MODE_UNCOMPRESSED' I = 0


.field public static final 'NICE_LEN_MAX' I = 273


.field public static final 'NICE_LEN_MIN' I = 8


.field public static final 'PB_DEFAULT' I = 2


.field public static final 'PB_MAX' I = 4


.field public static final 'PRESET_DEFAULT' I = 6


.field public static final 'PRESET_MAX' I = 9


.field public static final 'PRESET_MIN' I = 0


.field private static final 'presetToDepthLimit' [I

.field private static final 'presetToDictSize' [I

.field private 'depthLimit' I

.field private 'dictSize' I

.field private 'lc' I

.field private 'lp' I

.field private 'mf' I

.field private 'mode' I

.field private 'niceLen' I

.field private 'pb' I

.field private 'presetDict' [B

.method static <clinit>()V
ldc org/tukaani/xz/LZMA2Options
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/LZMA2Options/$assertionsDisabled Z
bipush 10
newarray int
dup
iconst_0
ldc_w 262144
iastore
dup
iconst_1
ldc_w 1048576
iastore
dup
iconst_2
ldc_w 2097152
iastore
dup
iconst_3
ldc_w 4194304
iastore
dup
iconst_4
ldc_w 4194304
iastore
dup
iconst_5
ldc_w 8388608
iastore
dup
bipush 6
ldc_w 8388608
iastore
dup
bipush 7
ldc_w 16777216
iastore
dup
bipush 8
ldc_w 33554432
iastore
dup
bipush 9
ldc_w 67108864
iastore
putstatic org/tukaani/xz/LZMA2Options/presetToDictSize [I
iconst_4
newarray int
dup
iconst_0
iconst_4
iastore
dup
iconst_1
bipush 8
iastore
dup
iconst_2
bipush 24
iastore
dup
iconst_3
bipush 48
iastore
putstatic org/tukaani/xz/LZMA2Options/presetToDepthLimit [I
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 4
.end method

.method public <init>()V
.catch org/tukaani/xz/UnsupportedOptionsException from L0 to L1 using L2
aload 0
invokespecial org/tukaani/xz/FilterOptions/<init>()V
aload 0
aconst_null
putfield org/tukaani/xz/LZMA2Options/presetDict [B
L0:
aload 0
bipush 6
invokevirtual org/tukaani/xz/LZMA2Options/setPreset(I)V
L1:
return
L2:
astore 1
getstatic org/tukaani/xz/LZMA2Options/$assertionsDisabled Z
ifne L3
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L3:
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public <init>(I)V
.throws org/tukaani/xz/UnsupportedOptionsException
aload 0
invokespecial org/tukaani/xz/FilterOptions/<init>()V
aload 0
aconst_null
putfield org/tukaani/xz/LZMA2Options/presetDict [B
aload 0
iload 1
invokevirtual org/tukaani/xz/LZMA2Options/setPreset(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(IIIIIIII)V
.throws org/tukaani/xz/UnsupportedOptionsException
aload 0
invokespecial org/tukaani/xz/FilterOptions/<init>()V
aload 0
aconst_null
putfield org/tukaani/xz/LZMA2Options/presetDict [B
aload 0
iload 1
invokevirtual org/tukaani/xz/LZMA2Options/setDictSize(I)V
aload 0
iload 2
iload 3
invokevirtual org/tukaani/xz/LZMA2Options/setLcLp(II)V
aload 0
iload 4
invokevirtual org/tukaani/xz/LZMA2Options/setPb(I)V
aload 0
iload 5
invokevirtual org/tukaani/xz/LZMA2Options/setMode(I)V
aload 0
iload 6
invokevirtual org/tukaani/xz/LZMA2Options/setNiceLen(I)V
aload 0
iload 7
invokevirtual org/tukaani/xz/LZMA2Options/setMatchFinder(I)V
aload 0
iload 8
invokevirtual org/tukaani/xz/LZMA2Options/setDepthLimit(I)V
return
.limit locals 9
.limit stack 3
.end method

.method public clone()Ljava/lang/Object;
.catch java/lang/CloneNotSupportedException from L0 to L1 using L2
L0:
aload 0
invokespecial java/lang/Object/clone()Ljava/lang/Object;
astore 1
L1:
aload 1
areturn
L2:
astore 1
getstatic org/tukaani/xz/LZMA2Options/$assertionsDisabled Z
ifne L3
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L3:
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public getDecoderMemoryUsage()I
aload 0
getfield org/tukaani/xz/LZMA2Options/dictSize I
iconst_1
isub
istore 1
iload 1
iload 1
iconst_2
iushr
ior
istore 1
iload 1
iload 1
iconst_3
iushr
ior
istore 1
iload 1
iload 1
iconst_4
iushr
ior
istore 1
iload 1
iload 1
bipush 8
iushr
ior
istore 1
iload 1
iload 1
bipush 16
iushr
ior
iconst_1
iadd
invokestatic org/tukaani/xz/LZMA2InputStream/getMemoryUsage(I)I
ireturn
.limit locals 2
.limit stack 3
.end method

.method public getDepthLimit()I
aload 0
getfield org/tukaani/xz/LZMA2Options/depthLimit I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getDictSize()I
aload 0
getfield org/tukaani/xz/LZMA2Options/dictSize I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getEncoderMemoryUsage()I
aload 0
getfield org/tukaani/xz/LZMA2Options/mode I
ifne L0
invokestatic org/tukaani/xz/UncompressedLZMA2OutputStream/getMemoryUsage()I
ireturn
L0:
aload 0
invokestatic org/tukaani/xz/LZMA2OutputStream/getMemoryUsage(Lorg/tukaani/xz/LZMA2Options;)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method getFilterEncoder()Lorg/tukaani/xz/FilterEncoder;
new org/tukaani/xz/LZMA2Encoder
dup
aload 0
invokespecial org/tukaani/xz/LZMA2Encoder/<init>(Lorg/tukaani/xz/LZMA2Options;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public getInputStream(Ljava/io/InputStream;)Ljava/io/InputStream;
.throws java/io/IOException
new org/tukaani/xz/LZMA2InputStream
dup
aload 1
aload 0
getfield org/tukaani/xz/LZMA2Options/dictSize I
invokespecial org/tukaani/xz/LZMA2InputStream/<init>(Ljava/io/InputStream;I)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public getLc()I
aload 0
getfield org/tukaani/xz/LZMA2Options/lc I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getLp()I
aload 0
getfield org/tukaani/xz/LZMA2Options/lp I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMatchFinder()I
aload 0
getfield org/tukaani/xz/LZMA2Options/mf I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMode()I
aload 0
getfield org/tukaani/xz/LZMA2Options/mode I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getNiceLen()I
aload 0
getfield org/tukaani/xz/LZMA2Options/niceLen I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;
aload 0
getfield org/tukaani/xz/LZMA2Options/mode I
ifne L0
new org/tukaani/xz/UncompressedLZMA2OutputStream
dup
aload 1
invokespecial org/tukaani/xz/UncompressedLZMA2OutputStream/<init>(Lorg/tukaani/xz/FinishableOutputStream;)V
areturn
L0:
new org/tukaani/xz/LZMA2OutputStream
dup
aload 1
aload 0
invokespecial org/tukaani/xz/LZMA2OutputStream/<init>(Lorg/tukaani/xz/FinishableOutputStream;Lorg/tukaani/xz/LZMA2Options;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public getPb()I
aload 0
getfield org/tukaani/xz/LZMA2Options/pb I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getPresetDict()[B
aload 0
getfield org/tukaani/xz/LZMA2Options/presetDict [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public setDepthLimit(I)V
.throws org/tukaani/xz/UnsupportedOptionsException
iload 1
ifge L0
new org/tukaani/xz/UnsupportedOptionsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Depth limit cannot be negative: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
putfield org/tukaani/xz/LZMA2Options/depthLimit I
return
.limit locals 2
.limit stack 4
.end method

.method public setDictSize(I)V
.throws org/tukaani/xz/UnsupportedOptionsException
iload 1
sipush 4096
if_icmpge L0
new org/tukaani/xz/UnsupportedOptionsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LZMA2 dictionary size must be at least 4 KiB: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " B"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 1
ldc_w 805306368
if_icmple L1
new org/tukaani/xz/UnsupportedOptionsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LZMA2 dictionary size must not exceed 768 MiB: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " B"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iload 1
putfield org/tukaani/xz/LZMA2Options/dictSize I
return
.limit locals 2
.limit stack 4
.end method

.method public setLc(I)V
.throws org/tukaani/xz/UnsupportedOptionsException
aload 0
iload 1
aload 0
getfield org/tukaani/xz/LZMA2Options/lp I
invokevirtual org/tukaani/xz/LZMA2Options/setLcLp(II)V
return
.limit locals 2
.limit stack 3
.end method

.method public setLcLp(II)V
.throws org/tukaani/xz/UnsupportedOptionsException
iload 1
iflt L0
iload 2
iflt L0
iload 1
iconst_4
if_icmpgt L0
iload 2
iconst_4
if_icmpgt L0
iload 1
iload 2
iadd
iconst_4
if_icmple L1
L0:
new org/tukaani/xz/UnsupportedOptionsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "lc + lp must not exceed 4: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " + "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iload 1
putfield org/tukaani/xz/LZMA2Options/lc I
aload 0
iload 2
putfield org/tukaani/xz/LZMA2Options/lp I
return
.limit locals 3
.limit stack 4
.end method

.method public setLp(I)V
.throws org/tukaani/xz/UnsupportedOptionsException
aload 0
aload 0
getfield org/tukaani/xz/LZMA2Options/lc I
iload 1
invokevirtual org/tukaani/xz/LZMA2Options/setLcLp(II)V
return
.limit locals 2
.limit stack 3
.end method

.method public setMatchFinder(I)V
.throws org/tukaani/xz/UnsupportedOptionsException
iload 1
iconst_4
if_icmpeq L0
iload 1
bipush 20
if_icmpeq L0
new org/tukaani/xz/UnsupportedOptionsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unsupported match finder: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
putfield org/tukaani/xz/LZMA2Options/mf I
return
.limit locals 2
.limit stack 4
.end method

.method public setMode(I)V
.throws org/tukaani/xz/UnsupportedOptionsException
iload 1
iflt L0
iload 1
iconst_2
if_icmple L1
L0:
new org/tukaani/xz/UnsupportedOptionsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unsupported compression mode: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iload 1
putfield org/tukaani/xz/LZMA2Options/mode I
return
.limit locals 2
.limit stack 4
.end method

.method public setNiceLen(I)V
.throws org/tukaani/xz/UnsupportedOptionsException
iload 1
bipush 8
if_icmpge L0
new org/tukaani/xz/UnsupportedOptionsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Minimum nice length of matches is 8 bytes: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 1
sipush 273
if_icmple L1
new org/tukaani/xz/UnsupportedOptionsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Maximum nice length of matches is 273: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iload 1
putfield org/tukaani/xz/LZMA2Options/niceLen I
return
.limit locals 2
.limit stack 4
.end method

.method public setPb(I)V
.throws org/tukaani/xz/UnsupportedOptionsException
iload 1
iflt L0
iload 1
iconst_4
if_icmple L1
L0:
new org/tukaani/xz/UnsupportedOptionsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "pb must not exceed 4: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iload 1
putfield org/tukaani/xz/LZMA2Options/pb I
return
.limit locals 2
.limit stack 4
.end method

.method public setPreset(I)V
.throws org/tukaani/xz/UnsupportedOptionsException
iload 1
iflt L0
iload 1
bipush 9
if_icmple L1
L0:
new org/tukaani/xz/UnsupportedOptionsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unsupported preset: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iconst_3
putfield org/tukaani/xz/LZMA2Options/lc I
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2Options/lp I
aload 0
iconst_2
putfield org/tukaani/xz/LZMA2Options/pb I
aload 0
getstatic org/tukaani/xz/LZMA2Options/presetToDictSize [I
iload 1
iaload
putfield org/tukaani/xz/LZMA2Options/dictSize I
iload 1
iconst_3
if_icmpgt L2
aload 0
iconst_1
putfield org/tukaani/xz/LZMA2Options/mode I
aload 0
iconst_4
putfield org/tukaani/xz/LZMA2Options/mf I
iload 1
iconst_1
if_icmpgt L3
sipush 128
istore 2
L4:
aload 0
iload 2
putfield org/tukaani/xz/LZMA2Options/niceLen I
aload 0
getstatic org/tukaani/xz/LZMA2Options/presetToDepthLimit [I
iload 1
iaload
putfield org/tukaani/xz/LZMA2Options/depthLimit I
return
L3:
sipush 273
istore 2
goto L4
L2:
aload 0
iconst_2
putfield org/tukaani/xz/LZMA2Options/mode I
aload 0
bipush 20
putfield org/tukaani/xz/LZMA2Options/mf I
iload 1
iconst_4
if_icmpne L5
bipush 16
istore 1
L6:
aload 0
iload 1
putfield org/tukaani/xz/LZMA2Options/niceLen I
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2Options/depthLimit I
return
L5:
iload 1
iconst_5
if_icmpne L7
bipush 32
istore 1
goto L6
L7:
bipush 64
istore 1
goto L6
.limit locals 3
.limit stack 4
.end method

.method public setPresetDict([B)V
aload 0
aload 1
putfield org/tukaani/xz/LZMA2Options/presetDict [B
return
.limit locals 2
.limit stack 2
.end method
