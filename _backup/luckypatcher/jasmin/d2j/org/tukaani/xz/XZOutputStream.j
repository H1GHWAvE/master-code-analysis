.bytecode 50.0
.class public synchronized org/tukaani/xz/XZOutputStream
.super org/tukaani/xz/FinishableOutputStream

.field private 'blockEncoder' Lorg/tukaani/xz/BlockOutputStream;

.field private final 'check' Lorg/tukaani/xz/check/Check;

.field private 'exception' Ljava/io/IOException;

.field private 'filters' [Lorg/tukaani/xz/FilterEncoder;

.field private 'filtersSupportFlushing' Z

.field private 'finished' Z

.field private final 'index' Lorg/tukaani/xz/index/IndexEncoder;

.field private 'out' Ljava/io/OutputStream;

.field private final 'streamFlags' Lorg/tukaani/xz/common/StreamFlags;

.field private final 'tempBuf' [B

.method public <init>(Ljava/io/OutputStream;Lorg/tukaani/xz/FilterOptions;)V
.throws java/io/IOException
aload 0
aload 1
aload 2
iconst_4
invokespecial org/tukaani/xz/XZOutputStream/<init>(Ljava/io/OutputStream;Lorg/tukaani/xz/FilterOptions;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Ljava/io/OutputStream;Lorg/tukaani/xz/FilterOptions;I)V
.throws java/io/IOException
aload 0
aload 1
iconst_1
anewarray org/tukaani/xz/FilterOptions
dup
iconst_0
aload 2
aastore
iload 3
invokespecial org/tukaani/xz/XZOutputStream/<init>(Ljava/io/OutputStream;[Lorg/tukaani/xz/FilterOptions;I)V
return
.limit locals 4
.limit stack 6
.end method

.method public <init>(Ljava/io/OutputStream;[Lorg/tukaani/xz/FilterOptions;)V
.throws java/io/IOException
aload 0
aload 1
aload 2
iconst_4
invokespecial org/tukaani/xz/XZOutputStream/<init>(Ljava/io/OutputStream;[Lorg/tukaani/xz/FilterOptions;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Ljava/io/OutputStream;[Lorg/tukaani/xz/FilterOptions;I)V
.throws java/io/IOException
aload 0
invokespecial org/tukaani/xz/FinishableOutputStream/<init>()V
aload 0
new org/tukaani/xz/common/StreamFlags
dup
invokespecial org/tukaani/xz/common/StreamFlags/<init>()V
putfield org/tukaani/xz/XZOutputStream/streamFlags Lorg/tukaani/xz/common/StreamFlags;
aload 0
new org/tukaani/xz/index/IndexEncoder
dup
invokespecial org/tukaani/xz/index/IndexEncoder/<init>()V
putfield org/tukaani/xz/XZOutputStream/index Lorg/tukaani/xz/index/IndexEncoder;
aload 0
aconst_null
putfield org/tukaani/xz/XZOutputStream/blockEncoder Lorg/tukaani/xz/BlockOutputStream;
aload 0
aconst_null
putfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
aload 0
iconst_0
putfield org/tukaani/xz/XZOutputStream/finished Z
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/XZOutputStream/tempBuf [B
aload 0
aload 1
putfield org/tukaani/xz/XZOutputStream/out Ljava/io/OutputStream;
aload 0
aload 2
invokevirtual org/tukaani/xz/XZOutputStream/updateFilters([Lorg/tukaani/xz/FilterOptions;)V
aload 0
getfield org/tukaani/xz/XZOutputStream/streamFlags Lorg/tukaani/xz/common/StreamFlags;
iload 3
putfield org/tukaani/xz/common/StreamFlags/checkType I
aload 0
iload 3
invokestatic org/tukaani/xz/check/Check/getInstance(I)Lorg/tukaani/xz/check/Check;
putfield org/tukaani/xz/XZOutputStream/check Lorg/tukaani/xz/check/Check;
aload 0
invokespecial org/tukaani/xz/XZOutputStream/encodeStreamHeader()V
return
.limit locals 4
.limit stack 3
.end method

.method private encodeStreamFlags([BI)V
aload 1
iload 2
iconst_0
bastore
aload 1
iload 2
iconst_1
iadd
aload 0
getfield org/tukaani/xz/XZOutputStream/streamFlags Lorg/tukaani/xz/common/StreamFlags;
getfield org/tukaani/xz/common/StreamFlags/checkType I
i2b
bastore
return
.limit locals 3
.limit stack 3
.end method

.method private encodeStreamFooter()V
.throws java/io/IOException
bipush 6
newarray byte
astore 4
aload 0
getfield org/tukaani/xz/XZOutputStream/index Lorg/tukaani/xz/index/IndexEncoder;
invokevirtual org/tukaani/xz/index/IndexEncoder/getIndexSize()J
ldc2_w 4L
ldiv
lstore 2
iconst_0
istore 1
L0:
iload 1
iconst_4
if_icmpge L1
aload 4
iload 1
lload 2
lconst_1
lsub
iload 1
bipush 8
imul
lushr
l2i
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
aload 4
iconst_4
invokespecial org/tukaani/xz/XZOutputStream/encodeStreamFlags([BI)V
aload 0
getfield org/tukaani/xz/XZOutputStream/out Ljava/io/OutputStream;
aload 4
invokestatic org/tukaani/xz/common/EncoderUtil/writeCRC32(Ljava/io/OutputStream;[B)V
aload 0
getfield org/tukaani/xz/XZOutputStream/out Ljava/io/OutputStream;
aload 4
invokevirtual java/io/OutputStream/write([B)V
aload 0
getfield org/tukaani/xz/XZOutputStream/out Ljava/io/OutputStream;
getstatic org/tukaani/xz/XZ/FOOTER_MAGIC [B
invokevirtual java/io/OutputStream/write([B)V
return
.limit locals 5
.limit stack 6
.end method

.method private encodeStreamHeader()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/XZOutputStream/out Ljava/io/OutputStream;
getstatic org/tukaani/xz/XZ/HEADER_MAGIC [B
invokevirtual java/io/OutputStream/write([B)V
iconst_2
newarray byte
astore 1
aload 0
aload 1
iconst_0
invokespecial org/tukaani/xz/XZOutputStream/encodeStreamFlags([BI)V
aload 0
getfield org/tukaani/xz/XZOutputStream/out Ljava/io/OutputStream;
aload 1
invokevirtual java/io/OutputStream/write([B)V
aload 0
getfield org/tukaani/xz/XZOutputStream/out Ljava/io/OutputStream;
aload 1
invokestatic org/tukaani/xz/common/EncoderUtil/writeCRC32(Ljava/io/OutputStream;[B)V
return
.limit locals 2
.limit stack 3
.end method

.method public close()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L4
aload 0
getfield org/tukaani/xz/XZOutputStream/out Ljava/io/OutputStream;
ifnull L5
L0:
aload 0
invokevirtual org/tukaani/xz/XZOutputStream/finish()V
L1:
aload 0
getfield org/tukaani/xz/XZOutputStream/out Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/close()V
L3:
aload 0
aconst_null
putfield org/tukaani/xz/XZOutputStream/out Ljava/io/OutputStream;
L5:
aload 0
getfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
ifnull L6
aload 0
getfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
athrow
L4:
astore 1
aload 0
getfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
ifnonnull L3
aload 0
aload 1
putfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
goto L3
L2:
astore 1
goto L1
L6:
return
.limit locals 2
.limit stack 2
.end method

.method public endBlock()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
ifnull L3
aload 0
getfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
athrow
L3:
aload 0
getfield org/tukaani/xz/XZOutputStream/finished Z
ifeq L4
new org/tukaani/xz/XZIOException
dup
ldc "Stream finished or closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 0
getfield org/tukaani/xz/XZOutputStream/blockEncoder Lorg/tukaani/xz/BlockOutputStream;
ifnull L1
L0:
aload 0
getfield org/tukaani/xz/XZOutputStream/blockEncoder Lorg/tukaani/xz/BlockOutputStream;
invokevirtual org/tukaani/xz/BlockOutputStream/finish()V
aload 0
getfield org/tukaani/xz/XZOutputStream/index Lorg/tukaani/xz/index/IndexEncoder;
aload 0
getfield org/tukaani/xz/XZOutputStream/blockEncoder Lorg/tukaani/xz/BlockOutputStream;
invokevirtual org/tukaani/xz/BlockOutputStream/getUnpaddedSize()J
aload 0
getfield org/tukaani/xz/XZOutputStream/blockEncoder Lorg/tukaani/xz/BlockOutputStream;
invokevirtual org/tukaani/xz/BlockOutputStream/getUncompressedSize()J
invokevirtual org/tukaani/xz/index/IndexEncoder/add(JJ)V
aload 0
aconst_null
putfield org/tukaani/xz/XZOutputStream/blockEncoder Lorg/tukaani/xz/BlockOutputStream;
L1:
return
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
aload 1
athrow
.limit locals 2
.limit stack 5
.end method

.method public finish()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/XZOutputStream/finished Z
ifne L3
aload 0
invokevirtual org/tukaani/xz/XZOutputStream/endBlock()V
L0:
aload 0
getfield org/tukaani/xz/XZOutputStream/index Lorg/tukaani/xz/index/IndexEncoder;
aload 0
getfield org/tukaani/xz/XZOutputStream/out Ljava/io/OutputStream;
invokevirtual org/tukaani/xz/index/IndexEncoder/encode(Ljava/io/OutputStream;)V
aload 0
invokespecial org/tukaani/xz/XZOutputStream/encodeStreamFooter()V
L1:
aload 0
iconst_1
putfield org/tukaani/xz/XZOutputStream/finished Z
L3:
return
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public flush()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
aload 0
getfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
ifnull L7
aload 0
getfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
athrow
L7:
aload 0
getfield org/tukaani/xz/XZOutputStream/finished Z
ifeq L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream finished or closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/tukaani/xz/XZOutputStream/blockEncoder Lorg/tukaani/xz/BlockOutputStream;
ifnull L5
aload 0
getfield org/tukaani/xz/XZOutputStream/filtersSupportFlushing Z
ifeq L3
aload 0
getfield org/tukaani/xz/XZOutputStream/blockEncoder Lorg/tukaani/xz/BlockOutputStream;
invokevirtual org/tukaani/xz/BlockOutputStream/flush()V
L1:
return
L3:
aload 0
invokevirtual org/tukaani/xz/XZOutputStream/endBlock()V
aload 0
getfield org/tukaani/xz/XZOutputStream/out Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/flush()V
L4:
return
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
aload 1
athrow
L5:
aload 0
getfield org/tukaani/xz/XZOutputStream/out Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/flush()V
L6:
return
.limit locals 2
.limit stack 3
.end method

.method public updateFilters(Lorg/tukaani/xz/FilterOptions;)V
.throws org/tukaani/xz/XZIOException
aload 0
iconst_1
anewarray org/tukaani/xz/FilterOptions
dup
iconst_0
aload 1
aastore
invokevirtual org/tukaani/xz/XZOutputStream/updateFilters([Lorg/tukaani/xz/FilterOptions;)V
return
.limit locals 2
.limit stack 5
.end method

.method public updateFilters([Lorg/tukaani/xz/FilterOptions;)V
.throws org/tukaani/xz/XZIOException
aload 0
getfield org/tukaani/xz/XZOutputStream/blockEncoder Lorg/tukaani/xz/BlockOutputStream;
ifnull L0
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "Changing filter options in the middle of a XZ Block not implemented"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
arraylength
iconst_1
if_icmplt L1
aload 1
arraylength
iconst_4
if_icmple L2
L1:
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "XZ filter chain must be 1-4 filters"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
iconst_1
putfield org/tukaani/xz/XZOutputStream/filtersSupportFlushing Z
aload 1
arraylength
anewarray org/tukaani/xz/FilterEncoder
astore 3
iconst_0
istore 2
L3:
iload 2
aload 1
arraylength
if_icmpge L4
aload 3
iload 2
aload 1
iload 2
aaload
invokevirtual org/tukaani/xz/FilterOptions/getFilterEncoder()Lorg/tukaani/xz/FilterEncoder;
aastore
aload 0
aload 0
getfield org/tukaani/xz/XZOutputStream/filtersSupportFlushing Z
aload 3
iload 2
aaload
invokeinterface org/tukaani/xz/FilterEncoder/supportsFlushing()Z 0
iand
putfield org/tukaani/xz/XZOutputStream/filtersSupportFlushing Z
iload 2
iconst_1
iadd
istore 2
goto L3
L4:
aload 3
invokestatic org/tukaani/xz/RawCoder/validate([Lorg/tukaani/xz/FilterCoder;)V
aload 0
aload 3
putfield org/tukaani/xz/XZOutputStream/filters [Lorg/tukaani/xz/FilterEncoder;
return
.limit locals 4
.limit stack 4
.end method

.method public write(I)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/XZOutputStream/tempBuf [B
iconst_0
iload 1
i2b
bastore
aload 0
aload 0
getfield org/tukaani/xz/XZOutputStream/tempBuf [B
iconst_0
iconst_1
invokevirtual org/tukaani/xz/XZOutputStream/write([BII)V
return
.limit locals 2
.limit stack 4
.end method

.method public write([BII)V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L2
iload 2
iflt L4
iload 3
iflt L4
iload 2
iload 3
iadd
iflt L4
iload 2
iload 3
iadd
aload 1
arraylength
if_icmple L5
L4:
new java/lang/IndexOutOfBoundsException
dup
invokespecial java/lang/IndexOutOfBoundsException/<init>()V
athrow
L5:
aload 0
getfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
ifnull L6
aload 0
getfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
athrow
L6:
aload 0
getfield org/tukaani/xz/XZOutputStream/finished Z
ifeq L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream finished or closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/tukaani/xz/XZOutputStream/blockEncoder Lorg/tukaani/xz/BlockOutputStream;
ifnonnull L1
aload 0
new org/tukaani/xz/BlockOutputStream
dup
aload 0
getfield org/tukaani/xz/XZOutputStream/out Ljava/io/OutputStream;
aload 0
getfield org/tukaani/xz/XZOutputStream/filters [Lorg/tukaani/xz/FilterEncoder;
aload 0
getfield org/tukaani/xz/XZOutputStream/check Lorg/tukaani/xz/check/Check;
invokespecial org/tukaani/xz/BlockOutputStream/<init>(Ljava/io/OutputStream;[Lorg/tukaani/xz/FilterEncoder;Lorg/tukaani/xz/check/Check;)V
putfield org/tukaani/xz/XZOutputStream/blockEncoder Lorg/tukaani/xz/BlockOutputStream;
L1:
aload 0
getfield org/tukaani/xz/XZOutputStream/blockEncoder Lorg/tukaani/xz/BlockOutputStream;
aload 1
iload 2
iload 3
invokevirtual org/tukaani/xz/BlockOutputStream/write([BII)V
L3:
return
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/XZOutputStream/exception Ljava/io/IOException;
aload 1
athrow
.limit locals 4
.limit stack 6
.end method
