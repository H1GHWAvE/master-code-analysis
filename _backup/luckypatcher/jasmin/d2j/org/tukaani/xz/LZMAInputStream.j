.bytecode 50.0
.class public synchronized org/tukaani/xz/LZMAInputStream
.super java/io/InputStream

.field static final synthetic '$assertionsDisabled' Z

.field public static final 'DICT_SIZE_MAX' I = 2147483632


.field private 'endReached' Z

.field private 'exception' Ljava/io/IOException;

.field private 'in' Ljava/io/InputStream;

.field private 'lz' Lorg/tukaani/xz/lz/LZDecoder;

.field private 'lzma' Lorg/tukaani/xz/lzma/LZMADecoder;

.field private 'rc' Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;

.field private 'remainingSize' J

.field private final 'tempBuf' [B

.method static <clinit>()V
ldc org/tukaani/xz/LZMAInputStream
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/LZMAInputStream/$assertionsDisabled Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method public <init>(Ljava/io/InputStream;)V
.throws java/io/IOException
aload 0
aload 1
iconst_m1
invokespecial org/tukaani/xz/LZMAInputStream/<init>(Ljava/io/InputStream;I)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/io/InputStream;I)V
.throws java/io/IOException
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
iconst_0
putfield org/tukaani/xz/LZMAInputStream/endReached Z
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/LZMAInputStream/tempBuf [B
aload 0
aconst_null
putfield org/tukaani/xz/LZMAInputStream/exception Ljava/io/IOException;
new java/io/DataInputStream
dup
aload 1
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
astore 8
aload 8
invokevirtual java/io/DataInputStream/readByte()B
istore 3
iconst_0
istore 4
iconst_0
istore 5
L0:
iload 5
iconst_4
if_icmpge L1
iload 4
aload 8
invokevirtual java/io/DataInputStream/readUnsignedByte()I
iload 5
bipush 8
imul
ishl
ior
istore 4
iload 5
iconst_1
iadd
istore 5
goto L0
L1:
lconst_0
lstore 6
iconst_0
istore 5
L2:
iload 5
bipush 8
if_icmpge L3
lload 6
aload 8
invokevirtual java/io/DataInputStream/readUnsignedByte()I
i2l
iload 5
bipush 8
imul
lshl
lor
lstore 6
iload 5
iconst_1
iadd
istore 5
goto L2
L3:
iload 4
iload 3
invokestatic org/tukaani/xz/LZMAInputStream/getMemoryUsage(IB)I
istore 5
iload 2
iconst_m1
if_icmpeq L4
iload 5
iload 2
if_icmple L4
new org/tukaani/xz/MemoryLimitException
dup
iload 5
iload 2
invokespecial org/tukaani/xz/MemoryLimitException/<init>(II)V
athrow
L4:
aload 0
aload 1
lload 6
iload 3
iload 4
aconst_null
invokespecial org/tukaani/xz/LZMAInputStream/initialize(Ljava/io/InputStream;JBI[B)V
return
.limit locals 9
.limit stack 7
.end method

.method public <init>(Ljava/io/InputStream;JBI)V
.throws java/io/IOException
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
iconst_0
putfield org/tukaani/xz/LZMAInputStream/endReached Z
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/LZMAInputStream/tempBuf [B
aload 0
aconst_null
putfield org/tukaani/xz/LZMAInputStream/exception Ljava/io/IOException;
aload 0
aload 1
lload 2
iload 4
iload 5
aconst_null
invokespecial org/tukaani/xz/LZMAInputStream/initialize(Ljava/io/InputStream;JBI[B)V
return
.limit locals 6
.limit stack 7
.end method

.method public <init>(Ljava/io/InputStream;JBI[B)V
.throws java/io/IOException
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
iconst_0
putfield org/tukaani/xz/LZMAInputStream/endReached Z
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/LZMAInputStream/tempBuf [B
aload 0
aconst_null
putfield org/tukaani/xz/LZMAInputStream/exception Ljava/io/IOException;
aload 0
aload 1
lload 2
iload 4
iload 5
aload 6
invokespecial org/tukaani/xz/LZMAInputStream/initialize(Ljava/io/InputStream;JBI[B)V
return
.limit locals 7
.limit stack 7
.end method

.method public <init>(Ljava/io/InputStream;JIIII[B)V
.throws java/io/IOException
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
iconst_0
putfield org/tukaani/xz/LZMAInputStream/endReached Z
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/LZMAInputStream/tempBuf [B
aload 0
aconst_null
putfield org/tukaani/xz/LZMAInputStream/exception Ljava/io/IOException;
aload 0
aload 1
lload 2
iload 4
iload 5
iload 6
iload 7
aload 8
invokespecial org/tukaani/xz/LZMAInputStream/initialize(Ljava/io/InputStream;JIIII[B)V
return
.limit locals 9
.limit stack 9
.end method

.method private static getDictSize(I)I
iload 0
iflt L0
iload 0
ldc_w 2147483632
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
ldc "LZMA dictionary is too big for this implementation"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 0
istore 1
iload 0
sipush 4096
if_icmpge L2
sipush 4096
istore 1
L2:
iload 1
bipush 15
iadd
bipush -16
iand
ireturn
.limit locals 2
.limit stack 3
.end method

.method public static getMemoryUsage(IB)I
.throws org/tukaani/xz/UnsupportedOptionsException
.throws org/tukaani/xz/CorruptedInputException
iload 0
iflt L0
iload 0
ldc_w 2147483632
if_icmple L1
L0:
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "LZMA dictionary is too big for this implementation"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 1
sipush 255
iand
istore 1
iload 1
sipush 224
if_icmple L2
new org/tukaani/xz/CorruptedInputException
dup
ldc "Invalid LZMA properties byte"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L2:
iload 1
bipush 45
irem
istore 1
iload 1
bipush 9
idiv
istore 2
iload 0
iload 1
iload 2
bipush 9
imul
isub
iload 2
invokestatic org/tukaani/xz/LZMAInputStream/getMemoryUsage(III)I
ireturn
.limit locals 3
.limit stack 4
.end method

.method public static getMemoryUsage(III)I
iload 1
iflt L0
iload 1
bipush 8
if_icmpgt L0
iload 2
iflt L0
iload 2
iconst_4
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
ldc "Invalid lc or lp"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 0
invokestatic org/tukaani/xz/LZMAInputStream/getDictSize(I)I
sipush 1024
idiv
bipush 10
iadd
sipush 1536
iload 1
iload 2
iadd
ishl
sipush 1024
idiv
iadd
ireturn
.limit locals 3
.limit stack 4
.end method

.method private initialize(Ljava/io/InputStream;JBI[B)V
.throws java/io/IOException
lload 2
ldc2_w -1L
lcmp
ifge L0
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "Uncompressed size is too big"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 4
sipush 255
iand
istore 7
iload 7
sipush 224
if_icmple L1
new org/tukaani/xz/CorruptedInputException
dup
ldc "Invalid LZMA properties byte"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 7
bipush 45
idiv
istore 4
iload 7
iload 4
bipush 9
imul
iconst_5
imul
isub
istore 7
iload 7
bipush 9
idiv
istore 8
iload 5
iflt L2
iload 5
ldc_w 2147483632
if_icmple L3
L2:
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "LZMA dictionary is too big for this implementation"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
aload 1
lload 2
iload 7
iload 8
bipush 9
imul
isub
iload 8
iload 4
iload 5
aload 6
invokespecial org/tukaani/xz/LZMAInputStream/initialize(Ljava/io/InputStream;JIIII[B)V
return
.limit locals 9
.limit stack 9
.end method

.method private initialize(Ljava/io/InputStream;JIIII[B)V
.throws java/io/IOException
lload 2
ldc2_w -1L
lcmp
iflt L0
iload 4
iflt L0
iload 4
bipush 8
if_icmpgt L0
iload 5
iflt L0
iload 5
iconst_4
if_icmpgt L0
iload 6
iflt L0
iload 6
iconst_4
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L1:
aload 0
aload 1
putfield org/tukaani/xz/LZMAInputStream/in Ljava/io/InputStream;
iload 7
invokestatic org/tukaani/xz/LZMAInputStream/getDictSize(I)I
istore 9
iload 9
istore 7
lload 2
lconst_0
lcmp
iflt L2
iload 9
istore 7
iload 9
i2l
lload 2
lcmp
ifle L2
lload 2
l2i
invokestatic org/tukaani/xz/LZMAInputStream/getDictSize(I)I
istore 7
L2:
aload 0
new org/tukaani/xz/lz/LZDecoder
dup
iload 7
invokestatic org/tukaani/xz/LZMAInputStream/getDictSize(I)I
aload 8
invokespecial org/tukaani/xz/lz/LZDecoder/<init>(I[B)V
putfield org/tukaani/xz/LZMAInputStream/lz Lorg/tukaani/xz/lz/LZDecoder;
aload 0
new org/tukaani/xz/rangecoder/RangeDecoderFromStream
dup
aload 1
invokespecial org/tukaani/xz/rangecoder/RangeDecoderFromStream/<init>(Ljava/io/InputStream;)V
putfield org/tukaani/xz/LZMAInputStream/rc Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;
aload 0
new org/tukaani/xz/lzma/LZMADecoder
dup
aload 0
getfield org/tukaani/xz/LZMAInputStream/lz Lorg/tukaani/xz/lz/LZDecoder;
aload 0
getfield org/tukaani/xz/LZMAInputStream/rc Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;
iload 4
iload 5
iload 6
invokespecial org/tukaani/xz/lzma/LZMADecoder/<init>(Lorg/tukaani/xz/lz/LZDecoder;Lorg/tukaani/xz/rangecoder/RangeDecoder;III)V
putfield org/tukaani/xz/LZMAInputStream/lzma Lorg/tukaani/xz/lzma/LZMADecoder;
aload 0
lload 2
putfield org/tukaani/xz/LZMAInputStream/remainingSize J
return
.limit locals 10
.limit stack 8
.end method

.method public close()V
.throws java/io/IOException
.catch all from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/LZMAInputStream/in Ljava/io/InputStream;
ifnull L3
L0:
aload 0
getfield org/tukaani/xz/LZMAInputStream/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/close()V
L1:
aload 0
aconst_null
putfield org/tukaani/xz/LZMAInputStream/in Ljava/io/InputStream;
L3:
return
L2:
astore 1
aload 0
aconst_null
putfield org/tukaani/xz/LZMAInputStream/in Ljava/io/InputStream;
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public read()I
.throws java/io/IOException
aload 0
aload 0
getfield org/tukaani/xz/LZMAInputStream/tempBuf [B
iconst_0
iconst_1
invokevirtual org/tukaani/xz/LZMAInputStream/read([BII)I
iconst_m1
if_icmpne L0
iconst_m1
ireturn
L0:
aload 0
getfield org/tukaani/xz/LZMAInputStream/tempBuf [B
iconst_0
baload
sipush 255
iand
ireturn
.limit locals 1
.limit stack 4
.end method

.method public read([BII)I
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L4 to L5 using L2
.catch org/tukaani/xz/CorruptedInputException from L5 to L6 using L7
.catch java/io/IOException from L5 to L6 using L2
.catch java/io/IOException from L6 to L8 using L2
.catch java/io/IOException from L9 to L2 using L2
.catch java/io/IOException from L10 to L11 using L2
.catch java/io/IOException from L11 to L12 using L2
.catch java/io/IOException from L12 to L13 using L2
.catch java/io/IOException from L14 to L15 using L2
.catch java/io/IOException from L16 to L17 using L2
.catch java/io/IOException from L17 to L18 using L2
iload 2
iflt L19
iload 3
iflt L19
iload 2
iload 3
iadd
iflt L19
iload 2
iload 3
iadd
aload 1
arraylength
if_icmple L20
L19:
new java/lang/IndexOutOfBoundsException
dup
invokespecial java/lang/IndexOutOfBoundsException/<init>()V
athrow
L20:
iload 3
ifne L21
iconst_0
istore 4
L22:
iload 4
ireturn
L21:
aload 0
getfield org/tukaani/xz/LZMAInputStream/in Ljava/io/InputStream;
ifnonnull L23
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L23:
aload 0
getfield org/tukaani/xz/LZMAInputStream/exception Ljava/io/IOException;
ifnull L24
aload 0
getfield org/tukaani/xz/LZMAInputStream/exception Ljava/io/IOException;
athrow
L24:
aload 0
getfield org/tukaani/xz/LZMAInputStream/endReached Z
ifeq L25
iconst_m1
ireturn
L25:
iconst_0
istore 4
iload 2
istore 5
iload 4
istore 2
L26:
iload 2
istore 4
iload 3
ifle L22
iload 3
istore 4
iload 4
istore 6
L0:
aload 0
getfield org/tukaani/xz/LZMAInputStream/remainingSize J
lconst_0
lcmp
iflt L4
L1:
iload 4
istore 6
L3:
aload 0
getfield org/tukaani/xz/LZMAInputStream/remainingSize J
iload 3
i2l
lcmp
ifge L4
aload 0
getfield org/tukaani/xz/LZMAInputStream/remainingSize J
l2i
istore 6
L4:
aload 0
getfield org/tukaani/xz/LZMAInputStream/lz Lorg/tukaani/xz/lz/LZDecoder;
iload 6
invokevirtual org/tukaani/xz/lz/LZDecoder/setLimit(I)V
L5:
aload 0
getfield org/tukaani/xz/LZMAInputStream/lzma Lorg/tukaani/xz/lzma/LZMADecoder;
invokevirtual org/tukaani/xz/lzma/LZMADecoder/decode()V
L6:
aload 0
getfield org/tukaani/xz/LZMAInputStream/lz Lorg/tukaani/xz/lz/LZDecoder;
aload 1
iload 5
invokevirtual org/tukaani/xz/lz/LZDecoder/flush([BI)I
istore 4
L8:
iload 5
iload 4
iadd
istore 5
iload 3
iload 4
isub
istore 3
iload 2
iload 4
iadd
istore 6
L9:
aload 0
getfield org/tukaani/xz/LZMAInputStream/remainingSize J
lconst_0
lcmp
iflt L15
aload 0
aload 0
getfield org/tukaani/xz/LZMAInputStream/remainingSize J
iload 4
i2l
lsub
putfield org/tukaani/xz/LZMAInputStream/remainingSize J
getstatic org/tukaani/xz/LZMAInputStream/$assertionsDisabled Z
ifne L14
aload 0
getfield org/tukaani/xz/LZMAInputStream/remainingSize J
lconst_0
lcmp
ifge L14
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/LZMAInputStream/exception Ljava/io/IOException;
aload 1
athrow
L7:
astore 7
L10:
aload 0
getfield org/tukaani/xz/LZMAInputStream/remainingSize J
ldc2_w -1L
lcmp
ifne L11
aload 0
getfield org/tukaani/xz/LZMAInputStream/lzma Lorg/tukaani/xz/lzma/LZMADecoder;
invokevirtual org/tukaani/xz/lzma/LZMADecoder/endMarkerDetected()Z
ifne L12
L11:
aload 7
athrow
L12:
aload 0
iconst_1
putfield org/tukaani/xz/LZMAInputStream/endReached Z
aload 0
getfield org/tukaani/xz/LZMAInputStream/rc Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;
invokevirtual org/tukaani/xz/rangecoder/RangeDecoderFromStream/normalize()V
L13:
goto L6
L14:
aload 0
getfield org/tukaani/xz/LZMAInputStream/remainingSize J
lconst_0
lcmp
ifne L15
aload 0
iconst_1
putfield org/tukaani/xz/LZMAInputStream/endReached Z
L15:
iload 6
istore 2
L16:
aload 0
getfield org/tukaani/xz/LZMAInputStream/endReached Z
ifeq L26
aload 0
getfield org/tukaani/xz/LZMAInputStream/rc Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;
invokevirtual org/tukaani/xz/rangecoder/RangeDecoderFromStream/isFinished()Z
ifeq L17
aload 0
getfield org/tukaani/xz/LZMAInputStream/lz Lorg/tukaani/xz/lz/LZDecoder;
invokevirtual org/tukaani/xz/lz/LZDecoder/hasPending()Z
ifeq L18
L17:
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L18:
iload 6
istore 4
iload 6
ifne L22
iconst_m1
ireturn
.limit locals 8
.limit stack 5
.end method
