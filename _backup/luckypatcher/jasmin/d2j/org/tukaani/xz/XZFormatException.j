.bytecode 50.0
.class public synchronized org/tukaani/xz/XZFormatException
.super org/tukaani/xz/XZIOException

.field private static final 'serialVersionUID' J = 3L


.method public <init>()V
aload 0
ldc "Input is not in the XZ format"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
return
.limit locals 1
.limit stack 2
.end method
