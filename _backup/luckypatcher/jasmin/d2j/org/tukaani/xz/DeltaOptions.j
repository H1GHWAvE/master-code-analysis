.bytecode 50.0
.class public synchronized org/tukaani/xz/DeltaOptions
.super org/tukaani/xz/FilterOptions

.field static final synthetic '$assertionsDisabled' Z

.field public static final 'DISTANCE_MAX' I = 256


.field public static final 'DISTANCE_MIN' I = 1


.field private 'distance' I

.method static <clinit>()V
ldc org/tukaani/xz/DeltaOptions
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/DeltaOptions/$assertionsDisabled Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial org/tukaani/xz/FilterOptions/<init>()V
aload 0
iconst_1
putfield org/tukaani/xz/DeltaOptions/distance I
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(I)V
.throws org/tukaani/xz/UnsupportedOptionsException
aload 0
invokespecial org/tukaani/xz/FilterOptions/<init>()V
aload 0
iconst_1
putfield org/tukaani/xz/DeltaOptions/distance I
aload 0
iload 1
invokevirtual org/tukaani/xz/DeltaOptions/setDistance(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public clone()Ljava/lang/Object;
.catch java/lang/CloneNotSupportedException from L0 to L1 using L2
L0:
aload 0
invokespecial java/lang/Object/clone()Ljava/lang/Object;
astore 1
L1:
aload 1
areturn
L2:
astore 1
getstatic org/tukaani/xz/DeltaOptions/$assertionsDisabled Z
ifne L3
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L3:
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public getDecoderMemoryUsage()I
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getDistance()I
aload 0
getfield org/tukaani/xz/DeltaOptions/distance I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getEncoderMemoryUsage()I
invokestatic org/tukaani/xz/DeltaOutputStream/getMemoryUsage()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method getFilterEncoder()Lorg/tukaani/xz/FilterEncoder;
new org/tukaani/xz/DeltaEncoder
dup
aload 0
invokespecial org/tukaani/xz/DeltaEncoder/<init>(Lorg/tukaani/xz/DeltaOptions;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public getInputStream(Ljava/io/InputStream;)Ljava/io/InputStream;
new org/tukaani/xz/DeltaInputStream
dup
aload 1
aload 0
getfield org/tukaani/xz/DeltaOptions/distance I
invokespecial org/tukaani/xz/DeltaInputStream/<init>(Ljava/io/InputStream;I)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;
new org/tukaani/xz/DeltaOutputStream
dup
aload 1
aload 0
invokespecial org/tukaani/xz/DeltaOutputStream/<init>(Lorg/tukaani/xz/FinishableOutputStream;Lorg/tukaani/xz/DeltaOptions;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public setDistance(I)V
.throws org/tukaani/xz/UnsupportedOptionsException
iload 1
iconst_1
if_icmplt L0
iload 1
sipush 256
if_icmple L1
L0:
new org/tukaani/xz/UnsupportedOptionsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Delta distance must be in the range [1, 256]: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iload 1
putfield org/tukaani/xz/DeltaOptions/distance I
return
.limit locals 2
.limit stack 4
.end method
