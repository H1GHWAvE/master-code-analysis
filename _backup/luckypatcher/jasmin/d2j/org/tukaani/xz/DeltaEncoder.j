.bytecode 50.0
.class synchronized org/tukaani/xz/DeltaEncoder
.super org/tukaani/xz/DeltaCoder
.implements org/tukaani/xz/FilterEncoder

.field private final 'options' Lorg/tukaani/xz/DeltaOptions;

.field private final 'props' [B

.method <init>(Lorg/tukaani/xz/DeltaOptions;)V
aload 0
invokespecial org/tukaani/xz/DeltaCoder/<init>()V
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/DeltaEncoder/props [B
aload 0
getfield org/tukaani/xz/DeltaEncoder/props [B
iconst_0
aload 1
invokevirtual org/tukaani/xz/DeltaOptions/getDistance()I
iconst_1
isub
i2b
bastore
aload 0
aload 1
invokevirtual org/tukaani/xz/DeltaOptions/clone()Ljava/lang/Object;
checkcast org/tukaani/xz/DeltaOptions
putfield org/tukaani/xz/DeltaEncoder/options Lorg/tukaani/xz/DeltaOptions;
return
.limit locals 2
.limit stack 4
.end method

.method public getFilterID()J
ldc2_w 3L
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getFilterProps()[B
aload 0
getfield org/tukaani/xz/DeltaEncoder/props [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;
aload 0
getfield org/tukaani/xz/DeltaEncoder/options Lorg/tukaani/xz/DeltaOptions;
aload 1
invokevirtual org/tukaani/xz/DeltaOptions/getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;
areturn
.limit locals 2
.limit stack 2
.end method

.method public supportsFlushing()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method
