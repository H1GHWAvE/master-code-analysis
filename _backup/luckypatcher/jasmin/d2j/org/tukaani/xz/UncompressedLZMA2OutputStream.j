.bytecode 50.0
.class synchronized org/tukaani/xz/UncompressedLZMA2OutputStream
.super org/tukaani/xz/FinishableOutputStream

.field private 'dictResetNeeded' Z

.field private 'exception' Ljava/io/IOException;

.field private 'finished' Z

.field private 'out' Lorg/tukaani/xz/FinishableOutputStream;

.field private final 'outData' Ljava/io/DataOutputStream;

.field private final 'tempBuf' [B

.field private final 'uncompBuf' [B

.field private 'uncompPos' I

.method <init>(Lorg/tukaani/xz/FinishableOutputStream;)V
aload 0
invokespecial org/tukaani/xz/FinishableOutputStream/<init>()V
aload 0
ldc_w 65536
newarray byte
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompBuf [B
aload 0
iconst_0
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompPos I
aload 0
iconst_1
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/dictResetNeeded Z
aload 0
iconst_0
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/finished Z
aload 0
aconst_null
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/tempBuf [B
aload 1
ifnonnull L0
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L0:
aload 0
aload 1
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
aload 0
new java/io/DataOutputStream
dup
aload 1
invokespecial java/io/DataOutputStream/<init>(Ljava/io/OutputStream;)V
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/outData Ljava/io/DataOutputStream;
return
.limit locals 2
.limit stack 4
.end method

.method static getMemoryUsage()I
bipush 70
ireturn
.limit locals 0
.limit stack 1
.end method

.method private writeChunk()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/outData Ljava/io/DataOutputStream;
astore 2
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/dictResetNeeded Z
ifeq L0
iconst_1
istore 1
L1:
aload 2
iload 1
invokevirtual java/io/DataOutputStream/writeByte(I)V
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/outData Ljava/io/DataOutputStream;
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompPos I
iconst_1
isub
invokevirtual java/io/DataOutputStream/writeShort(I)V
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/outData Ljava/io/DataOutputStream;
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompBuf [B
iconst_0
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompPos I
invokevirtual java/io/DataOutputStream/write([BII)V
aload 0
iconst_0
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompPos I
aload 0
iconst_0
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/dictResetNeeded Z
return
L0:
iconst_2
istore 1
goto L1
.limit locals 3
.limit stack 4
.end method

.method private writeEndMarker()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L2
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
ifnull L4
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
athrow
L4:
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/finished Z
ifeq L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream finished or closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompPos I
ifle L1
aload 0
invokespecial org/tukaani/xz/UncompressedLZMA2OutputStream/writeChunk()V
L1:
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
iconst_0
invokevirtual org/tukaani/xz/FinishableOutputStream/write(I)V
L3:
return
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method public close()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L4
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
ifnull L5
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/finished Z
ifne L1
L0:
aload 0
invokespecial org/tukaani/xz/UncompressedLZMA2OutputStream/writeEndMarker()V
L1:
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
invokevirtual org/tukaani/xz/FinishableOutputStream/close()V
L3:
aload 0
aconst_null
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
L5:
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
ifnull L6
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
athrow
L4:
astore 1
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
ifnonnull L3
aload 0
aload 1
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
goto L3
L2:
astore 1
goto L1
L6:
return
.limit locals 2
.limit stack 2
.end method

.method public finish()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/finished Z
ifne L3
aload 0
invokespecial org/tukaani/xz/UncompressedLZMA2OutputStream/writeEndMarker()V
L0:
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
invokevirtual org/tukaani/xz/FinishableOutputStream/finish()V
L1:
aload 0
iconst_1
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/finished Z
L3:
return
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public flush()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L2
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
ifnull L4
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
athrow
L4:
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/finished Z
ifeq L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream finished or closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompPos I
ifle L1
aload 0
invokespecial org/tukaani/xz/UncompressedLZMA2OutputStream/writeChunk()V
L1:
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
invokevirtual org/tukaani/xz/FinishableOutputStream/flush()V
L3:
return
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method public write(I)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/tempBuf [B
iconst_0
iload 1
i2b
bastore
aload 0
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/tempBuf [B
iconst_0
iconst_1
invokevirtual org/tukaani/xz/UncompressedLZMA2OutputStream/write([BII)V
return
.limit locals 2
.limit stack 4
.end method

.method public write([BII)V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
iload 2
iflt L7
iload 3
iflt L7
iload 2
iload 3
iadd
iflt L7
iload 2
iload 3
iadd
aload 1
arraylength
if_icmple L8
L7:
new java/lang/IndexOutOfBoundsException
dup
invokespecial java/lang/IndexOutOfBoundsException/<init>()V
athrow
L8:
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
ifnull L9
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
athrow
L9:
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/finished Z
ifeq L10
new org/tukaani/xz/XZIOException
dup
ldc "Stream finished or closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L10:
iload 3
ifle L11
L0:
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompBuf [B
arraylength
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompPos I
isub
iload 3
invokestatic java/lang/Math/min(II)I
istore 5
aload 1
iload 2
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompBuf [B
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompPos I
iload 5
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L1:
iload 3
iload 5
isub
istore 4
L3:
aload 0
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompPos I
iload 5
iadd
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompPos I
L4:
iload 4
istore 3
L5:
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompPos I
aload 0
getfield org/tukaani/xz/UncompressedLZMA2OutputStream/uncompBuf [B
arraylength
if_icmpne L10
aload 0
invokespecial org/tukaani/xz/UncompressedLZMA2OutputStream/writeChunk()V
L6:
iload 4
istore 3
goto L10
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/UncompressedLZMA2OutputStream/exception Ljava/io/IOException;
aload 1
athrow
L11:
return
.limit locals 6
.limit stack 5
.end method
