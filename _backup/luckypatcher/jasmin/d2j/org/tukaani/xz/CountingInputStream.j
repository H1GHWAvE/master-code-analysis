.bytecode 50.0
.class synchronized org/tukaani/xz/CountingInputStream
.super java/io/FilterInputStream

.field private 'size' J

.method public <init>(Ljava/io/InputStream;)V
aload 0
aload 1
invokespecial java/io/FilterInputStream/<init>(Ljava/io/InputStream;)V
aload 0
lconst_0
putfield org/tukaani/xz/CountingInputStream/size J
return
.limit locals 2
.limit stack 3
.end method

.method public getSize()J
aload 0
getfield org/tukaani/xz/CountingInputStream/size J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public read()I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/CountingInputStream/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/read()I
istore 1
iload 1
iconst_m1
if_icmpeq L0
aload 0
getfield org/tukaani/xz/CountingInputStream/size J
lconst_0
lcmp
iflt L0
aload 0
aload 0
getfield org/tukaani/xz/CountingInputStream/size J
lconst_1
ladd
putfield org/tukaani/xz/CountingInputStream/size J
L0:
iload 1
ireturn
.limit locals 2
.limit stack 5
.end method

.method public read([BII)I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/CountingInputStream/in Ljava/io/InputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/InputStream/read([BII)I
istore 2
iload 2
ifle L0
aload 0
getfield org/tukaani/xz/CountingInputStream/size J
lconst_0
lcmp
iflt L0
aload 0
aload 0
getfield org/tukaani/xz/CountingInputStream/size J
iload 2
i2l
ladd
putfield org/tukaani/xz/CountingInputStream/size J
L0:
iload 2
ireturn
.limit locals 4
.limit stack 5
.end method
