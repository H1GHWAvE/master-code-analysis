.bytecode 50.0
.class public final synchronized org/tukaani/xz/rangecoder/RangeEncoder
.super org/tukaani/xz/rangecoder/RangeCoder

.field static final synthetic '$assertionsDisabled' Z

.field private static final 'BIT_PRICE_SHIFT_BITS' I = 4


.field private static final 'MOVE_REDUCING_BITS' I = 4


.field private static final 'prices' [I

.field private final 'buf' [B

.field private 'bufPos' I

.field private 'cache' B

.field private 'cacheSize' I

.field private 'low' J

.field private 'range' I

.method static <clinit>()V
ldc org/tukaani/xz/rangecoder/RangeEncoder
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 4
L1:
iload 4
putstatic org/tukaani/xz/rangecoder/RangeEncoder/$assertionsDisabled Z
sipush 128
newarray int
putstatic org/tukaani/xz/rangecoder/RangeEncoder/prices [I
bipush 8
istore 0
L2:
iload 0
sipush 2048
if_icmpge L3
iload 0
istore 1
iconst_0
istore 2
iconst_0
istore 3
L4:
iload 3
iconst_4
if_icmpge L5
iload 1
iload 1
imul
istore 1
iload 2
iconst_1
ishl
istore 2
L6:
ldc_w -65536
iload 1
iand
ifeq L7
iload 1
iconst_1
iushr
istore 1
iload 2
iconst_1
iadd
istore 2
goto L6
L0:
iconst_0
istore 4
goto L1
L7:
iload 3
iconst_1
iadd
istore 3
goto L4
L5:
getstatic org/tukaani/xz/rangecoder/RangeEncoder/prices [I
iload 0
iconst_4
ishr
sipush 161
iload 2
isub
iastore
iload 0
bipush 16
iadd
istore 0
goto L2
L3:
return
.limit locals 5
.limit stack 4
.end method

.method public <init>(I)V
aload 0
invokespecial org/tukaani/xz/rangecoder/RangeCoder/<init>()V
aload 0
iload 1
newarray byte
putfield org/tukaani/xz/rangecoder/RangeEncoder/buf [B
aload 0
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/reset()V
return
.limit locals 2
.limit stack 2
.end method

.method public static getBitPrice(II)I
getstatic org/tukaani/xz/rangecoder/RangeEncoder/$assertionsDisabled Z
ifne L0
iload 1
ifeq L0
iload 1
iconst_1
if_icmpeq L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
getstatic org/tukaani/xz/rangecoder/RangeEncoder/prices [I
iload 1
ineg
sipush 2047
iand
iload 0
ixor
iconst_4
iushr
iaload
ireturn
.limit locals 2
.limit stack 3
.end method

.method public static getBitTreePrice([SI)I
iconst_0
istore 2
iload 1
aload 0
arraylength
ior
istore 1
L0:
iload 1
iconst_1
iushr
istore 3
iload 2
aload 0
iload 3
saload
iload 1
iconst_1
iand
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
iadd
istore 4
iload 4
istore 2
iload 3
istore 1
iload 3
iconst_1
if_icmpne L0
iload 4
ireturn
.limit locals 5
.limit stack 4
.end method

.method public static getDirectBitsPrice(I)I
iload 0
iconst_4
ishl
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static getReverseBitTreePrice([SI)I
iconst_0
istore 2
iconst_1
istore 3
iload 1
aload 0
arraylength
ior
istore 1
L0:
iload 1
iconst_1
iand
istore 6
iload 1
iconst_1
iushr
istore 4
iload 2
aload 0
iload 3
saload
iload 6
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
iadd
istore 5
iload 3
iconst_1
ishl
iload 6
ior
istore 3
iload 5
istore 2
iload 4
istore 1
iload 4
iconst_1
if_icmpne L0
iload 5
ireturn
.limit locals 7
.limit stack 3
.end method

.method private shiftLow()V
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/low J
bipush 32
lushr
l2i
istore 2
iload 2
ifne L0
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/low J
ldc2_w 4278190080L
lcmp
ifge L1
L0:
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/cache B
istore 1
L2:
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/buf [B
astore 4
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/bufPos I
istore 3
aload 0
iload 3
iconst_1
iadd
putfield org/tukaani/xz/rangecoder/RangeEncoder/bufPos I
aload 4
iload 3
iload 1
iload 2
iadd
i2b
bastore
sipush 255
istore 1
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/cacheSize I
iconst_1
isub
istore 3
aload 0
iload 3
putfield org/tukaani/xz/rangecoder/RangeEncoder/cacheSize I
iload 3
ifne L2
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/low J
bipush 24
lushr
l2i
i2b
putfield org/tukaani/xz/rangecoder/RangeEncoder/cache B
L1:
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/cacheSize I
iconst_1
iadd
putfield org/tukaani/xz/rangecoder/RangeEncoder/cacheSize I
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/low J
ldc2_w 16777215L
land
bipush 8
lshl
putfield org/tukaani/xz/rangecoder/RangeEncoder/low J
return
.limit locals 5
.limit stack 5
.end method

.method public encodeBit([SII)V
aload 1
iload 2
saload
istore 4
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/range I
bipush 11
iushr
iload 4
imul
istore 5
iload 3
ifne L0
aload 0
iload 5
putfield org/tukaani/xz/rangecoder/RangeEncoder/range I
aload 1
iload 2
sipush 2048
iload 4
isub
iconst_5
iushr
iload 4
iadd
i2s
sastore
L1:
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/range I
ldc_w -16777216
iand
ifne L2
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/range I
bipush 8
ishl
putfield org/tukaani/xz/rangecoder/RangeEncoder/range I
aload 0
invokespecial org/tukaani/xz/rangecoder/RangeEncoder/shiftLow()V
L2:
return
L0:
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/low J
iload 5
i2l
ldc2_w 4294967295L
land
ladd
putfield org/tukaani/xz/rangecoder/RangeEncoder/low J
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/range I
iload 5
isub
putfield org/tukaani/xz/rangecoder/RangeEncoder/range I
aload 1
iload 2
iload 4
iload 4
iconst_5
iushr
isub
i2s
sastore
goto L1
.limit locals 6
.limit stack 7
.end method

.method public encodeBitTree([SI)V
iconst_1
istore 3
aload 1
arraylength
istore 4
L0:
iload 4
iconst_1
iushr
istore 5
iload 2
iload 5
iand
istore 6
aload 0
aload 1
iload 3
iload 6
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
iload 3
iconst_1
ishl
istore 4
iload 4
istore 3
iload 6
ifeq L1
iload 4
iconst_1
ior
istore 3
L1:
iload 5
istore 4
iload 5
iconst_1
if_icmpne L0
return
.limit locals 7
.limit stack 4
.end method

.method public encodeDirectBits(II)V
L0:
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/range I
iconst_1
iushr
putfield org/tukaani/xz/rangecoder/RangeEncoder/range I
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/low J
lstore 5
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/range I
istore 4
iload 2
iconst_1
isub
istore 3
aload 0
lload 5
iload 4
iconst_0
iload 1
iload 3
iushr
iconst_1
iand
isub
iand
i2l
ladd
putfield org/tukaani/xz/rangecoder/RangeEncoder/low J
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/range I
ldc_w -16777216
iand
ifne L1
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/range I
bipush 8
ishl
putfield org/tukaani/xz/rangecoder/RangeEncoder/range I
aload 0
invokespecial org/tukaani/xz/rangecoder/RangeEncoder/shiftLow()V
L1:
iload 3
istore 2
iload 3
ifne L0
return
.limit locals 7
.limit stack 7
.end method

.method public encodeReverseBitTree([SI)V
iconst_1
istore 3
iload 2
aload 1
arraylength
ior
istore 2
L0:
iload 2
iconst_1
iand
istore 5
iload 2
iconst_1
iushr
istore 4
aload 0
aload 1
iload 3
iload 5
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
iload 3
iconst_1
ishl
iload 5
ior
istore 3
iload 4
istore 2
iload 4
iconst_1
if_icmpne L0
return
.limit locals 6
.limit stack 4
.end method

.method public finish()I
iconst_0
istore 1
L0:
iload 1
iconst_5
if_icmpge L1
aload 0
invokespecial org/tukaani/xz/rangecoder/RangeEncoder/shiftLow()V
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/bufPos I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getPendingSize()I
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/bufPos I
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/cacheSize I
iadd
iconst_5
iadd
iconst_1
isub
ireturn
.limit locals 1
.limit stack 2
.end method

.method public reset()V
aload 0
lconst_0
putfield org/tukaani/xz/rangecoder/RangeEncoder/low J
aload 0
iconst_m1
putfield org/tukaani/xz/rangecoder/RangeEncoder/range I
aload 0
iconst_0
putfield org/tukaani/xz/rangecoder/RangeEncoder/cache B
aload 0
iconst_1
putfield org/tukaani/xz/rangecoder/RangeEncoder/cacheSize I
aload 0
iconst_0
putfield org/tukaani/xz/rangecoder/RangeEncoder/bufPos I
return
.limit locals 1
.limit stack 3
.end method

.method public write(Ljava/io/OutputStream;)V
.throws java/io/IOException
aload 1
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/buf [B
iconst_0
aload 0
getfield org/tukaani/xz/rangecoder/RangeEncoder/bufPos I
invokevirtual java/io/OutputStream/write([BII)V
return
.limit locals 2
.limit stack 4
.end method
