.bytecode 50.0
.class public final synchronized org/tukaani/xz/rangecoder/RangeDecoderFromStream
.super org/tukaani/xz/rangecoder/RangeDecoder

.field private final 'inData' Ljava/io/DataInputStream;

.method public <init>(Ljava/io/InputStream;)V
.throws java/io/IOException
aload 0
invokespecial org/tukaani/xz/rangecoder/RangeDecoder/<init>()V
aload 0
new java/io/DataInputStream
dup
aload 1
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
putfield org/tukaani/xz/rangecoder/RangeDecoderFromStream/inData Ljava/io/DataInputStream;
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromStream/inData Ljava/io/DataInputStream;
invokevirtual java/io/DataInputStream/readUnsignedByte()I
ifeq L0
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L0:
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromStream/inData Ljava/io/DataInputStream;
invokevirtual java/io/DataInputStream/readInt()I
putfield org/tukaani/xz/rangecoder/RangeDecoderFromStream/code I
aload 0
iconst_m1
putfield org/tukaani/xz/rangecoder/RangeDecoderFromStream/range I
return
.limit locals 2
.limit stack 4
.end method

.method public isFinished()Z
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromStream/code I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public normalize()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromStream/range I
ldc_w -16777216
iand
ifne L0
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromStream/code I
bipush 8
ishl
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromStream/inData Ljava/io/DataInputStream;
invokevirtual java/io/DataInputStream/readUnsignedByte()I
ior
putfield org/tukaani/xz/rangecoder/RangeDecoderFromStream/code I
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromStream/range I
bipush 8
ishl
putfield org/tukaani/xz/rangecoder/RangeDecoderFromStream/range I
L0:
return
.limit locals 1
.limit stack 3
.end method
