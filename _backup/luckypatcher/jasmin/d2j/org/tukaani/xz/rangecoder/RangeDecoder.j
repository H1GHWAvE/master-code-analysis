.bytecode 50.0
.class public synchronized abstract org/tukaani/xz/rangecoder/RangeDecoder
.super org/tukaani/xz/rangecoder/RangeCoder

.field 'code' I

.field 'range' I

.method public <init>()V
aload 0
invokespecial org/tukaani/xz/rangecoder/RangeCoder/<init>()V
aload 0
iconst_0
putfield org/tukaani/xz/rangecoder/RangeDecoder/range I
aload 0
iconst_0
putfield org/tukaani/xz/rangecoder/RangeDecoder/code I
return
.limit locals 1
.limit stack 2
.end method

.method public decodeBit([SI)I
.throws java/io/IOException
aload 0
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/normalize()V
aload 1
iload 2
saload
istore 3
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoder/range I
bipush 11
iushr
iload 3
imul
istore 4
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoder/code I
ldc_w -2147483648
ixor
ldc_w -2147483648
iload 4
ixor
if_icmpge L0
aload 0
iload 4
putfield org/tukaani/xz/rangecoder/RangeDecoder/range I
aload 1
iload 2
sipush 2048
iload 3
isub
iconst_5
iushr
iload 3
iadd
i2s
sastore
iconst_0
ireturn
L0:
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoder/range I
iload 4
isub
putfield org/tukaani/xz/rangecoder/RangeDecoder/range I
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoder/code I
iload 4
isub
putfield org/tukaani/xz/rangecoder/RangeDecoder/code I
aload 1
iload 2
iload 3
iload 3
iconst_5
iushr
isub
i2s
sastore
iconst_1
ireturn
.limit locals 5
.limit stack 5
.end method

.method public decodeBitTree([S)I
.throws java/io/IOException
iconst_1
istore 2
L0:
iload 2
iconst_1
ishl
aload 0
aload 1
iload 2
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/decodeBit([SI)I
ior
istore 3
iload 3
istore 2
iload 3
aload 1
arraylength
if_icmplt L0
iload 3
aload 1
arraylength
isub
ireturn
.limit locals 4
.limit stack 4
.end method

.method public decodeDirectBits(I)I
.throws java/io/IOException
iconst_0
istore 2
L0:
aload 0
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/normalize()V
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoder/range I
iconst_1
iushr
putfield org/tukaani/xz/rangecoder/RangeDecoder/range I
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoder/code I
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoder/range I
isub
bipush 31
iushr
istore 3
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoder/code I
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoder/range I
iload 3
iconst_1
isub
iand
isub
putfield org/tukaani/xz/rangecoder/RangeDecoder/code I
iload 2
iconst_1
ishl
iconst_1
iload 3
isub
ior
istore 3
iload 1
iconst_1
isub
istore 4
iload 3
istore 2
iload 4
istore 1
iload 4
ifne L0
iload 3
ireturn
.limit locals 5
.limit stack 5
.end method

.method public decodeReverseBitTree([S)I
.throws java/io/IOException
iconst_1
istore 4
iconst_0
istore 2
iconst_0
istore 3
L0:
aload 0
aload 1
iload 4
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/decodeBit([SI)I
istore 5
iload 4
iconst_1
ishl
iload 5
ior
istore 4
iload 3
iload 5
iload 2
ishl
ior
istore 3
iload 4
aload 1
arraylength
if_icmplt L1
iload 3
ireturn
L1:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 6
.limit stack 3
.end method

.method public abstract normalize()V
.throws java/io/IOException
.end method
