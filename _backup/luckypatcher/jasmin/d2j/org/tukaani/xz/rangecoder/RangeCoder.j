.bytecode 50.0
.class public synchronized abstract org/tukaani/xz/rangecoder/RangeCoder
.super java/lang/Object

.field static final 'BIT_MODEL_TOTAL' I = 2048


.field static final 'BIT_MODEL_TOTAL_BITS' I = 11


.field static final 'MOVE_BITS' I = 5


.field static final 'PROB_INIT' S = 1024


.field static final 'SHIFT_BITS' I = 8


.field static final 'TOP_MASK' I = -16777216


.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static final initProbs([S)V
aload 0
sipush 1024
invokestatic java/util/Arrays/fill([SS)V
return
.limit locals 1
.limit stack 2
.end method
