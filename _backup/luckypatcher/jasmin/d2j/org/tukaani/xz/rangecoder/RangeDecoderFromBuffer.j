.bytecode 50.0
.class public final synchronized org/tukaani/xz/rangecoder/RangeDecoderFromBuffer
.super org/tukaani/xz/rangecoder/RangeDecoder

.field private static final 'INIT_SIZE' I = 5


.field private final 'buf' [B

.field private 'end' I

.field private 'pos' I

.method public <init>(I)V
aload 0
invokespecial org/tukaani/xz/rangecoder/RangeDecoder/<init>()V
aload 0
iconst_0
putfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/pos I
aload 0
iconst_0
putfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/end I
aload 0
iload 1
iconst_5
isub
newarray byte
putfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/buf [B
return
.limit locals 2
.limit stack 3
.end method

.method public isFinished()Z
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/pos I
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/end I
if_icmpne L0
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/code I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isInBufferOK()Z
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/pos I
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/end I
if_icmpgt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public normalize()V
.throws java/io/IOException
.catch java/lang/ArrayIndexOutOfBoundsException from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/range I
ldc_w -16777216
iand
ifne L1
L0:
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/code I
istore 1
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/buf [B
astore 3
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/pos I
istore 2
aload 0
iload 2
iconst_1
iadd
putfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/pos I
aload 0
iload 1
bipush 8
ishl
aload 3
iload 2
baload
sipush 255
iand
ior
putfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/code I
aload 0
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/range I
bipush 8
ishl
putfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/range I
L1:
return
L2:
astore 3
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
.limit locals 4
.limit stack 4
.end method

.method public prepareInputBuffer(Ljava/io/DataInputStream;I)V
.throws java/io/IOException
iload 2
iconst_5
if_icmpge L0
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L0:
aload 1
invokevirtual java/io/DataInputStream/readUnsignedByte()I
ifeq L1
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L1:
aload 0
aload 1
invokevirtual java/io/DataInputStream/readInt()I
putfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/code I
aload 0
iconst_m1
putfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/range I
aload 0
iconst_0
putfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/pos I
aload 0
iload 2
iconst_5
isub
putfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/end I
aload 1
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/buf [B
iconst_0
aload 0
getfield org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/end I
invokevirtual java/io/DataInputStream/readFully([BII)V
return
.limit locals 3
.limit stack 4
.end method
