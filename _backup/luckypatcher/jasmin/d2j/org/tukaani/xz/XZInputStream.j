.bytecode 50.0
.class public synchronized org/tukaani/xz/XZInputStream
.super java/io/InputStream

.field private 'endReached' Z

.field private 'exception' Ljava/io/IOException;

.field private 'in' Ljava/io/InputStream;

.field private final 'memoryLimit' I

.field private final 'tempBuf' [B

.field private 'xzIn' Lorg/tukaani/xz/SingleXZInputStream;

.method public <init>(Ljava/io/InputStream;)V
.throws java/io/IOException
aload 0
aload 1
iconst_m1
invokespecial org/tukaani/xz/XZInputStream/<init>(Ljava/io/InputStream;I)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/io/InputStream;I)V
.throws java/io/IOException
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
iconst_0
putfield org/tukaani/xz/XZInputStream/endReached Z
aload 0
aconst_null
putfield org/tukaani/xz/XZInputStream/exception Ljava/io/IOException;
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/XZInputStream/tempBuf [B
aload 0
aload 1
putfield org/tukaani/xz/XZInputStream/in Ljava/io/InputStream;
aload 0
iload 2
putfield org/tukaani/xz/XZInputStream/memoryLimit I
aload 0
new org/tukaani/xz/SingleXZInputStream
dup
aload 1
iload 2
invokespecial org/tukaani/xz/SingleXZInputStream/<init>(Ljava/io/InputStream;I)V
putfield org/tukaani/xz/XZInputStream/xzIn Lorg/tukaani/xz/SingleXZInputStream;
return
.limit locals 3
.limit stack 5
.end method

.method private prepareNextStream()V
.throws java/io/IOException
.catch org/tukaani/xz/XZFormatException from L0 to L1 using L2
new java/io/DataInputStream
dup
aload 0
getfield org/tukaani/xz/XZInputStream/in Ljava/io/InputStream;
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
astore 1
bipush 12
newarray byte
astore 2
L3:
aload 1
aload 2
iconst_0
iconst_1
invokevirtual java/io/DataInputStream/read([BII)I
iconst_m1
if_icmpne L4
aload 0
iconst_1
putfield org/tukaani/xz/XZInputStream/endReached Z
return
L4:
aload 1
aload 2
iconst_1
iconst_3
invokevirtual java/io/DataInputStream/readFully([BII)V
aload 2
iconst_0
baload
ifne L5
aload 2
iconst_1
baload
ifne L5
aload 2
iconst_2
baload
ifne L5
aload 2
iconst_3
baload
ifeq L3
L5:
aload 1
aload 2
iconst_4
bipush 8
invokevirtual java/io/DataInputStream/readFully([BII)V
L0:
aload 0
new org/tukaani/xz/SingleXZInputStream
dup
aload 0
getfield org/tukaani/xz/XZInputStream/in Ljava/io/InputStream;
aload 0
getfield org/tukaani/xz/XZInputStream/memoryLimit I
aload 2
invokespecial org/tukaani/xz/SingleXZInputStream/<init>(Ljava/io/InputStream;I[B)V
putfield org/tukaani/xz/XZInputStream/xzIn Lorg/tukaani/xz/SingleXZInputStream;
L1:
return
L2:
astore 1
new org/tukaani/xz/CorruptedInputException
dup
ldc "Garbage after a valid XZ Stream"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 6
.end method

.method public available()I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/XZInputStream/in Ljava/io/InputStream;
ifnonnull L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/tukaani/xz/XZInputStream/exception Ljava/io/IOException;
ifnull L1
aload 0
getfield org/tukaani/xz/XZInputStream/exception Ljava/io/IOException;
athrow
L1:
aload 0
getfield org/tukaani/xz/XZInputStream/xzIn Lorg/tukaani/xz/SingleXZInputStream;
ifnonnull L2
iconst_0
ireturn
L2:
aload 0
getfield org/tukaani/xz/XZInputStream/xzIn Lorg/tukaani/xz/SingleXZInputStream;
invokevirtual org/tukaani/xz/SingleXZInputStream/available()I
ireturn
.limit locals 1
.limit stack 3
.end method

.method public close()V
.throws java/io/IOException
.catch all from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/XZInputStream/in Ljava/io/InputStream;
ifnull L3
L0:
aload 0
getfield org/tukaani/xz/XZInputStream/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/close()V
L1:
aload 0
aconst_null
putfield org/tukaani/xz/XZInputStream/in Ljava/io/InputStream;
L3:
return
L2:
astore 1
aload 0
aconst_null
putfield org/tukaani/xz/XZInputStream/in Ljava/io/InputStream;
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public read()I
.throws java/io/IOException
aload 0
aload 0
getfield org/tukaani/xz/XZInputStream/tempBuf [B
iconst_0
iconst_1
invokevirtual org/tukaani/xz/XZInputStream/read([BII)I
iconst_m1
if_icmpne L0
iconst_m1
ireturn
L0:
aload 0
getfield org/tukaani/xz/XZInputStream/tempBuf [B
iconst_0
baload
sipush 255
iand
ireturn
.limit locals 1
.limit stack 4
.end method

.method public read([BII)I
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
iload 2
iflt L7
iload 3
iflt L7
iload 2
iload 3
iadd
iflt L7
iload 2
iload 3
iadd
aload 1
arraylength
if_icmple L8
L7:
new java/lang/IndexOutOfBoundsException
dup
invokespecial java/lang/IndexOutOfBoundsException/<init>()V
athrow
L8:
iload 3
ifne L9
iconst_0
istore 5
L10:
iload 5
ireturn
L9:
aload 0
getfield org/tukaani/xz/XZInputStream/in Ljava/io/InputStream;
ifnonnull L11
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L11:
aload 0
getfield org/tukaani/xz/XZInputStream/exception Ljava/io/IOException;
ifnull L12
aload 0
getfield org/tukaani/xz/XZInputStream/exception Ljava/io/IOException;
athrow
L12:
aload 0
getfield org/tukaani/xz/XZInputStream/endReached Z
ifeq L13
iconst_m1
ireturn
L13:
iconst_0
istore 5
iload 2
istore 4
iload 5
istore 2
L14:
iload 2
istore 5
iload 3
ifle L10
L0:
aload 0
getfield org/tukaani/xz/XZInputStream/xzIn Lorg/tukaani/xz/SingleXZInputStream;
ifnonnull L3
aload 0
invokespecial org/tukaani/xz/XZInputStream/prepareNextStream()V
aload 0
getfield org/tukaani/xz/XZInputStream/endReached Z
ifeq L3
L1:
iload 2
istore 5
iload 2
ifne L10
iconst_m1
ireturn
L3:
aload 0
getfield org/tukaani/xz/XZInputStream/xzIn Lorg/tukaani/xz/SingleXZInputStream;
aload 1
iload 4
iload 3
invokevirtual org/tukaani/xz/SingleXZInputStream/read([BII)I
istore 5
L4:
iload 5
ifle L15
iload 2
iload 5
iadd
istore 2
iload 4
iload 5
iadd
istore 4
iload 3
iload 5
isub
istore 3
goto L14
L15:
iload 5
iconst_m1
if_icmpne L14
L5:
aload 0
aconst_null
putfield org/tukaani/xz/XZInputStream/xzIn Lorg/tukaani/xz/SingleXZInputStream;
L6:
goto L14
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/XZInputStream/exception Ljava/io/IOException;
iload 2
istore 5
iload 2
ifne L10
aload 1
athrow
.limit locals 6
.limit stack 4
.end method
