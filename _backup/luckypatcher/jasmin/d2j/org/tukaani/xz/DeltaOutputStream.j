.bytecode 50.0
.class synchronized org/tukaani/xz/DeltaOutputStream
.super org/tukaani/xz/FinishableOutputStream

.field private static final 'FILTER_BUF_SIZE' I = 4096


.field private final 'delta' Lorg/tukaani/xz/delta/DeltaEncoder;

.field private 'exception' Ljava/io/IOException;

.field private final 'filterBuf' [B

.field private 'finished' Z

.field private 'out' Lorg/tukaani/xz/FinishableOutputStream;

.field private final 'tempBuf' [B

.method <init>(Lorg/tukaani/xz/FinishableOutputStream;Lorg/tukaani/xz/DeltaOptions;)V
aload 0
invokespecial org/tukaani/xz/FinishableOutputStream/<init>()V
aload 0
sipush 4096
newarray byte
putfield org/tukaani/xz/DeltaOutputStream/filterBuf [B
aload 0
iconst_0
putfield org/tukaani/xz/DeltaOutputStream/finished Z
aload 0
aconst_null
putfield org/tukaani/xz/DeltaOutputStream/exception Ljava/io/IOException;
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/DeltaOutputStream/tempBuf [B
aload 0
aload 1
putfield org/tukaani/xz/DeltaOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
aload 0
new org/tukaani/xz/delta/DeltaEncoder
dup
aload 2
invokevirtual org/tukaani/xz/DeltaOptions/getDistance()I
invokespecial org/tukaani/xz/delta/DeltaEncoder/<init>(I)V
putfield org/tukaani/xz/DeltaOutputStream/delta Lorg/tukaani/xz/delta/DeltaEncoder;
return
.limit locals 3
.limit stack 4
.end method

.method static getMemoryUsage()I
iconst_5
ireturn
.limit locals 0
.limit stack 1
.end method

.method public close()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/DeltaOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
ifnull L3
L0:
aload 0
getfield org/tukaani/xz/DeltaOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
invokevirtual org/tukaani/xz/FinishableOutputStream/close()V
L1:
aload 0
aconst_null
putfield org/tukaani/xz/DeltaOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
L3:
aload 0
getfield org/tukaani/xz/DeltaOutputStream/exception Ljava/io/IOException;
ifnull L4
aload 0
getfield org/tukaani/xz/DeltaOutputStream/exception Ljava/io/IOException;
athrow
L2:
astore 1
aload 0
getfield org/tukaani/xz/DeltaOutputStream/exception Ljava/io/IOException;
ifnonnull L1
aload 0
aload 1
putfield org/tukaani/xz/DeltaOutputStream/exception Ljava/io/IOException;
goto L1
L4:
return
.limit locals 2
.limit stack 2
.end method

.method public finish()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/DeltaOutputStream/finished Z
ifne L3
aload 0
getfield org/tukaani/xz/DeltaOutputStream/exception Ljava/io/IOException;
ifnull L0
aload 0
getfield org/tukaani/xz/DeltaOutputStream/exception Ljava/io/IOException;
athrow
L0:
aload 0
getfield org/tukaani/xz/DeltaOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
invokevirtual org/tukaani/xz/FinishableOutputStream/finish()V
L1:
aload 0
iconst_1
putfield org/tukaani/xz/DeltaOutputStream/finished Z
L3:
return
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/DeltaOutputStream/exception Ljava/io/IOException;
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public flush()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/DeltaOutputStream/exception Ljava/io/IOException;
ifnull L3
aload 0
getfield org/tukaani/xz/DeltaOutputStream/exception Ljava/io/IOException;
athrow
L3:
aload 0
getfield org/tukaani/xz/DeltaOutputStream/finished Z
ifeq L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream finished or closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/tukaani/xz/DeltaOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
invokevirtual org/tukaani/xz/FinishableOutputStream/flush()V
L1:
return
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/DeltaOutputStream/exception Ljava/io/IOException;
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method public write(I)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/DeltaOutputStream/tempBuf [B
iconst_0
iload 1
i2b
bastore
aload 0
aload 0
getfield org/tukaani/xz/DeltaOutputStream/tempBuf [B
iconst_0
iconst_1
invokevirtual org/tukaani/xz/DeltaOutputStream/write([BII)V
return
.limit locals 2
.limit stack 4
.end method

.method public write([BII)V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
iload 2
iflt L5
iload 3
iflt L5
iload 2
iload 3
iadd
iflt L5
iload 2
iload 3
iadd
aload 1
arraylength
if_icmple L6
L5:
new java/lang/IndexOutOfBoundsException
dup
invokespecial java/lang/IndexOutOfBoundsException/<init>()V
athrow
L6:
aload 0
getfield org/tukaani/xz/DeltaOutputStream/exception Ljava/io/IOException;
ifnull L7
aload 0
getfield org/tukaani/xz/DeltaOutputStream/exception Ljava/io/IOException;
athrow
L7:
aload 0
getfield org/tukaani/xz/DeltaOutputStream/finished Z
ifeq L8
new org/tukaani/xz/XZIOException
dup
ldc "Stream finished"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L8:
iload 3
sipush 4096
if_icmple L3
L0:
aload 0
getfield org/tukaani/xz/DeltaOutputStream/delta Lorg/tukaani/xz/delta/DeltaEncoder;
aload 1
iload 2
sipush 4096
aload 0
getfield org/tukaani/xz/DeltaOutputStream/filterBuf [B
invokevirtual org/tukaani/xz/delta/DeltaEncoder/encode([BII[B)V
aload 0
getfield org/tukaani/xz/DeltaOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
aload 0
getfield org/tukaani/xz/DeltaOutputStream/filterBuf [B
invokevirtual org/tukaani/xz/FinishableOutputStream/write([B)V
L1:
iload 2
sipush 4096
iadd
istore 2
iload 3
sipush 4096
isub
istore 3
goto L8
L3:
aload 0
getfield org/tukaani/xz/DeltaOutputStream/delta Lorg/tukaani/xz/delta/DeltaEncoder;
aload 1
iload 2
iload 3
aload 0
getfield org/tukaani/xz/DeltaOutputStream/filterBuf [B
invokevirtual org/tukaani/xz/delta/DeltaEncoder/encode([BII[B)V
aload 0
getfield org/tukaani/xz/DeltaOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
aload 0
getfield org/tukaani/xz/DeltaOutputStream/filterBuf [B
iconst_0
iload 3
invokevirtual org/tukaani/xz/FinishableOutputStream/write([BII)V
L4:
return
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/DeltaOutputStream/exception Ljava/io/IOException;
aload 1
athrow
.limit locals 4
.limit stack 5
.end method
