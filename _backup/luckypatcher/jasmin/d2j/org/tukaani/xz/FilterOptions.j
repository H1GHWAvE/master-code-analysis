.bytecode 50.0
.class public synchronized abstract org/tukaani/xz/FilterOptions
.super java/lang/Object
.implements java/lang/Cloneable

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getDecoderMemoryUsage([Lorg/tukaani/xz/FilterOptions;)I
iconst_0
istore 2
iconst_0
istore 1
L0:
iload 1
aload 0
arraylength
if_icmpge L1
iload 2
aload 0
iload 1
aaload
invokevirtual org/tukaani/xz/FilterOptions/getDecoderMemoryUsage()I
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public static getEncoderMemoryUsage([Lorg/tukaani/xz/FilterOptions;)I
iconst_0
istore 2
iconst_0
istore 1
L0:
iload 1
aload 0
arraylength
if_icmpge L1
iload 2
aload 0
iload 1
aaload
invokevirtual org/tukaani/xz/FilterOptions/getEncoderMemoryUsage()I
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public abstract getDecoderMemoryUsage()I
.end method

.method public abstract getEncoderMemoryUsage()I
.end method

.method abstract getFilterEncoder()Lorg/tukaani/xz/FilterEncoder;
.end method

.method public abstract getInputStream(Ljava/io/InputStream;)Ljava/io/InputStream;
.throws java/io/IOException
.end method

.method public abstract getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;
.end method
