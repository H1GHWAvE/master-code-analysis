.bytecode 50.0
.class synchronized org/tukaani/xz/BlockInputStream
.super java/io/InputStream

.field private final 'check' Lorg/tukaani/xz/check/Check;

.field private 'compressedSizeInHeader' J

.field private 'compressedSizeLimit' J

.field private 'endReached' Z

.field private 'filterChain' Ljava/io/InputStream;

.field private final 'headerSize' I

.field private final 'inCounted' Lorg/tukaani/xz/CountingInputStream;

.field private final 'inData' Ljava/io/DataInputStream;

.field private final 'tempBuf' [B

.field private 'uncompressedSize' J

.field private 'uncompressedSizeInHeader' J

.method public <init>(Ljava/io/InputStream;Lorg/tukaani/xz/check/Check;IJJ)V
.throws java/io/IOException
.throws org/tukaani/xz/IndexIndicatorException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L4 to L2 using L2
.catch java/io/IOException from L5 to L6 using L2
.catch java/io/IOException from L7 to L8 using L2
.catch java/io/IOException from L9 to L10 using L2
.catch java/io/IOException from L10 to L11 using L2
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
ldc2_w -1L
putfield org/tukaani/xz/BlockInputStream/uncompressedSizeInHeader J
aload 0
ldc2_w -1L
putfield org/tukaani/xz/BlockInputStream/compressedSizeInHeader J
aload 0
lconst_0
putfield org/tukaani/xz/BlockInputStream/uncompressedSize J
aload 0
iconst_0
putfield org/tukaani/xz/BlockInputStream/endReached Z
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/BlockInputStream/tempBuf [B
aload 0
aload 2
putfield org/tukaani/xz/BlockInputStream/check Lorg/tukaani/xz/check/Check;
aload 0
new java/io/DataInputStream
dup
aload 1
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
putfield org/tukaani/xz/BlockInputStream/inData Ljava/io/DataInputStream;
sipush 1024
newarray byte
astore 14
aload 0
getfield org/tukaani/xz/BlockInputStream/inData Ljava/io/DataInputStream;
aload 14
iconst_0
iconst_1
invokevirtual java/io/DataInputStream/readFully([BII)V
aload 14
iconst_0
baload
ifne L12
new org/tukaani/xz/IndexIndicatorException
dup
invokespecial org/tukaani/xz/IndexIndicatorException/<init>()V
athrow
L12:
aload 0
aload 14
iconst_0
baload
sipush 255
iand
iconst_1
iadd
iconst_4
imul
putfield org/tukaani/xz/BlockInputStream/headerSize I
aload 0
getfield org/tukaani/xz/BlockInputStream/inData Ljava/io/DataInputStream;
aload 14
iconst_1
aload 0
getfield org/tukaani/xz/BlockInputStream/headerSize I
iconst_1
isub
invokevirtual java/io/DataInputStream/readFully([BII)V
aload 14
iconst_0
aload 0
getfield org/tukaani/xz/BlockInputStream/headerSize I
iconst_4
isub
aload 0
getfield org/tukaani/xz/BlockInputStream/headerSize I
iconst_4
isub
invokestatic org/tukaani/xz/common/DecoderUtil/isCRC32Valid([BIII)Z
ifne L13
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Block Header is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L13:
aload 14
iconst_1
baload
bipush 60
iand
ifeq L14
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "Unsupported options in XZ Block Header"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L14:
aload 14
iconst_1
baload
iconst_3
iand
iconst_1
iadd
istore 9
iload 9
newarray long
astore 12
iload 9
anewarray [B
astore 13
new java/io/ByteArrayInputStream
dup
aload 14
iconst_2
aload 0
getfield org/tukaani/xz/BlockInputStream/headerSize I
bipush 6
isub
invokespecial java/io/ByteArrayInputStream/<init>([BII)V
astore 15
L0:
aload 0
ldc2_w 9223372036854775804L
aload 0
getfield org/tukaani/xz/BlockInputStream/headerSize I
i2l
lsub
aload 2
invokevirtual org/tukaani/xz/check/Check/getSize()I
i2l
lsub
putfield org/tukaani/xz/BlockInputStream/compressedSizeLimit J
L1:
aload 14
iconst_1
baload
bipush 64
iand
ifeq L6
L3:
aload 0
aload 15
invokestatic org/tukaani/xz/common/DecoderUtil/decodeVLI(Ljava/io/InputStream;)J
putfield org/tukaani/xz/BlockInputStream/compressedSizeInHeader J
aload 0
getfield org/tukaani/xz/BlockInputStream/compressedSizeInHeader J
lconst_0
lcmp
ifeq L4
aload 0
getfield org/tukaani/xz/BlockInputStream/compressedSizeInHeader J
aload 0
getfield org/tukaani/xz/BlockInputStream/compressedSizeLimit J
lcmp
ifle L5
L4:
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L2:
astore 1
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Block Header is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L5:
aload 0
aload 0
getfield org/tukaani/xz/BlockInputStream/compressedSizeInHeader J
putfield org/tukaani/xz/BlockInputStream/compressedSizeLimit J
L6:
aload 14
iconst_1
baload
sipush 128
iand
ifeq L15
L7:
aload 0
aload 15
invokestatic org/tukaani/xz/common/DecoderUtil/decodeVLI(Ljava/io/InputStream;)J
putfield org/tukaani/xz/BlockInputStream/uncompressedSizeInHeader J
L8:
goto L15
L16:
iload 8
iload 9
if_icmpge L17
L9:
aload 12
iload 8
aload 15
invokestatic org/tukaani/xz/common/DecoderUtil/decodeVLI(Ljava/io/InputStream;)J
lastore
aload 15
invokestatic org/tukaani/xz/common/DecoderUtil/decodeVLI(Ljava/io/InputStream;)J
lstore 10
lload 10
aload 15
invokevirtual java/io/ByteArrayInputStream/available()I
i2l
lcmp
ifle L10
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L10:
aload 13
iload 8
lload 10
l2i
newarray byte
aastore
aload 15
aload 13
iload 8
aaload
invokevirtual java/io/ByteArrayInputStream/read([B)I
pop
L11:
iload 8
iconst_1
iadd
istore 8
goto L16
L17:
aload 15
invokevirtual java/io/ByteArrayInputStream/available()I
istore 8
L18:
iload 8
ifle L19
aload 15
invokevirtual java/io/ByteArrayInputStream/read()I
ifeq L20
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "Unsupported options in XZ Block Header"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L20:
iload 8
iconst_1
isub
istore 8
goto L18
L19:
lload 4
ldc2_w -1L
lcmp
ifeq L21
aload 0
getfield org/tukaani/xz/BlockInputStream/headerSize I
aload 2
invokevirtual org/tukaani/xz/check/Check/getSize()I
iadd
istore 8
iload 8
i2l
lload 4
lcmp
iflt L22
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index does not match a Block Header"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L22:
lload 4
iload 8
i2l
lsub
lstore 4
lload 4
aload 0
getfield org/tukaani/xz/BlockInputStream/compressedSizeLimit J
lcmp
ifgt L23
aload 0
getfield org/tukaani/xz/BlockInputStream/compressedSizeInHeader J
ldc2_w -1L
lcmp
ifeq L24
aload 0
getfield org/tukaani/xz/BlockInputStream/compressedSizeInHeader J
lload 4
lcmp
ifeq L24
L23:
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index does not match a Block Header"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L24:
aload 0
getfield org/tukaani/xz/BlockInputStream/uncompressedSizeInHeader J
ldc2_w -1L
lcmp
ifeq L25
aload 0
getfield org/tukaani/xz/BlockInputStream/uncompressedSizeInHeader J
lload 6
lcmp
ifeq L25
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index does not match a Block Header"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L25:
aload 0
lload 4
putfield org/tukaani/xz/BlockInputStream/compressedSizeLimit J
aload 0
lload 4
putfield org/tukaani/xz/BlockInputStream/compressedSizeInHeader J
aload 0
lload 6
putfield org/tukaani/xz/BlockInputStream/uncompressedSizeInHeader J
L21:
aload 12
arraylength
anewarray org/tukaani/xz/FilterDecoder
astore 2
iconst_0
istore 8
L26:
iload 8
aload 2
arraylength
if_icmpge L27
aload 12
iload 8
laload
ldc2_w 33L
lcmp
ifne L28
aload 2
iload 8
new org/tukaani/xz/LZMA2Decoder
dup
aload 13
iload 8
aaload
invokespecial org/tukaani/xz/LZMA2Decoder/<init>([B)V
aastore
L29:
iload 8
iconst_1
iadd
istore 8
goto L26
L28:
aload 12
iload 8
laload
ldc2_w 3L
lcmp
ifne L30
aload 2
iload 8
new org/tukaani/xz/DeltaDecoder
dup
aload 13
iload 8
aaload
invokespecial org/tukaani/xz/DeltaDecoder/<init>([B)V
aastore
goto L29
L30:
aload 12
iload 8
laload
invokestatic org/tukaani/xz/BCJDecoder/isBCJFilterID(J)Z
ifeq L31
aload 2
iload 8
new org/tukaani/xz/BCJDecoder
dup
aload 12
iload 8
laload
aload 13
iload 8
aaload
invokespecial org/tukaani/xz/BCJDecoder/<init>(J[B)V
aastore
goto L29
L31:
new org/tukaani/xz/UnsupportedOptionsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unknown Filter ID "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 12
iload 8
laload
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L27:
aload 2
invokestatic org/tukaani/xz/RawCoder/validate([Lorg/tukaani/xz/FilterCoder;)V
iload 3
iflt L32
iconst_0
istore 9
iconst_0
istore 8
L33:
iload 8
aload 2
arraylength
if_icmpge L34
iload 9
aload 2
iload 8
aaload
invokeinterface org/tukaani/xz/FilterDecoder/getMemoryUsage()I 0
iadd
istore 9
iload 8
iconst_1
iadd
istore 8
goto L33
L34:
iload 9
iload 3
if_icmple L32
new org/tukaani/xz/MemoryLimitException
dup
iload 9
iload 3
invokespecial org/tukaani/xz/MemoryLimitException/<init>(II)V
athrow
L32:
aload 0
new org/tukaani/xz/CountingInputStream
dup
aload 1
invokespecial org/tukaani/xz/CountingInputStream/<init>(Ljava/io/InputStream;)V
putfield org/tukaani/xz/BlockInputStream/inCounted Lorg/tukaani/xz/CountingInputStream;
aload 0
aload 0
getfield org/tukaani/xz/BlockInputStream/inCounted Lorg/tukaani/xz/CountingInputStream;
putfield org/tukaani/xz/BlockInputStream/filterChain Ljava/io/InputStream;
aload 2
arraylength
iconst_1
isub
istore 3
L35:
iload 3
iflt L36
aload 0
aload 2
iload 3
aaload
aload 0
getfield org/tukaani/xz/BlockInputStream/filterChain Ljava/io/InputStream;
invokeinterface org/tukaani/xz/FilterDecoder/getInputStream(Ljava/io/InputStream;)Ljava/io/InputStream; 1
putfield org/tukaani/xz/BlockInputStream/filterChain Ljava/io/InputStream;
iload 3
iconst_1
isub
istore 3
goto L35
L36:
return
L15:
iconst_0
istore 8
goto L16
.limit locals 16
.limit stack 8
.end method

.method private validate()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/BlockInputStream/inCounted Lorg/tukaani/xz/CountingInputStream;
invokevirtual org/tukaani/xz/CountingInputStream/getSize()J
lstore 3
aload 0
getfield org/tukaani/xz/BlockInputStream/compressedSizeInHeader J
ldc2_w -1L
lcmp
ifeq L0
aload 0
getfield org/tukaani/xz/BlockInputStream/compressedSizeInHeader J
lload 3
lcmp
ifne L1
L0:
lload 3
lstore 1
aload 0
getfield org/tukaani/xz/BlockInputStream/uncompressedSizeInHeader J
ldc2_w -1L
lcmp
ifeq L2
lload 3
lstore 1
aload 0
getfield org/tukaani/xz/BlockInputStream/uncompressedSizeInHeader J
aload 0
getfield org/tukaani/xz/BlockInputStream/uncompressedSize J
lcmp
ifeq L2
L1:
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L2:
lload 1
lconst_1
ladd
lstore 3
ldc2_w 3L
lload 1
land
lconst_0
lcmp
ifeq L3
lload 3
lstore 1
aload 0
getfield org/tukaani/xz/BlockInputStream/inData Ljava/io/DataInputStream;
invokevirtual java/io/DataInputStream/readUnsignedByte()I
ifeq L2
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L3:
aload 0
getfield org/tukaani/xz/BlockInputStream/check Lorg/tukaani/xz/check/Check;
invokevirtual org/tukaani/xz/check/Check/getSize()I
newarray byte
astore 5
aload 0
getfield org/tukaani/xz/BlockInputStream/inData Ljava/io/DataInputStream;
aload 5
invokevirtual java/io/DataInputStream/readFully([B)V
aload 0
getfield org/tukaani/xz/BlockInputStream/check Lorg/tukaani/xz/check/Check;
invokevirtual org/tukaani/xz/check/Check/finish()[B
aload 5
invokestatic java/util/Arrays/equals([B[B)Z
ifne L4
new org/tukaani/xz/CorruptedInputException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Integrity check ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield org/tukaani/xz/BlockInputStream/check Lorg/tukaani/xz/check/Check;
invokevirtual org/tukaani/xz/check/Check/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ") does not match"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L4:
return
.limit locals 6
.limit stack 4
.end method

.method public available()I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/BlockInputStream/filterChain Ljava/io/InputStream;
invokevirtual java/io/InputStream/available()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getUncompressedSize()J
aload 0
getfield org/tukaani/xz/BlockInputStream/uncompressedSize J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getUnpaddedSize()J
aload 0
getfield org/tukaani/xz/BlockInputStream/headerSize I
i2l
aload 0
getfield org/tukaani/xz/BlockInputStream/inCounted Lorg/tukaani/xz/CountingInputStream;
invokevirtual org/tukaani/xz/CountingInputStream/getSize()J
ladd
aload 0
getfield org/tukaani/xz/BlockInputStream/check Lorg/tukaani/xz/check/Check;
invokevirtual org/tukaani/xz/check/Check/getSize()I
i2l
ladd
lreturn
.limit locals 1
.limit stack 4
.end method

.method public read()I
.throws java/io/IOException
aload 0
aload 0
getfield org/tukaani/xz/BlockInputStream/tempBuf [B
iconst_0
iconst_1
invokevirtual org/tukaani/xz/BlockInputStream/read([BII)I
iconst_m1
if_icmpne L0
iconst_m1
ireturn
L0:
aload 0
getfield org/tukaani/xz/BlockInputStream/tempBuf [B
iconst_0
baload
sipush 255
iand
ireturn
.limit locals 1
.limit stack 4
.end method

.method public read([BII)I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/BlockInputStream/endReached Z
ifeq L0
iconst_m1
istore 2
L1:
iload 2
ireturn
L0:
aload 0
getfield org/tukaani/xz/BlockInputStream/filterChain Ljava/io/InputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/InputStream/read([BII)I
istore 4
iload 4
ifle L2
aload 0
getfield org/tukaani/xz/BlockInputStream/check Lorg/tukaani/xz/check/Check;
aload 1
iload 2
iload 4
invokevirtual org/tukaani/xz/check/Check/update([BII)V
aload 0
aload 0
getfield org/tukaani/xz/BlockInputStream/uncompressedSize J
iload 4
i2l
ladd
putfield org/tukaani/xz/BlockInputStream/uncompressedSize J
aload 0
getfield org/tukaani/xz/BlockInputStream/inCounted Lorg/tukaani/xz/CountingInputStream;
invokevirtual org/tukaani/xz/CountingInputStream/getSize()J
lstore 5
lload 5
lconst_0
lcmp
iflt L3
lload 5
aload 0
getfield org/tukaani/xz/BlockInputStream/compressedSizeLimit J
lcmp
ifgt L3
aload 0
getfield org/tukaani/xz/BlockInputStream/uncompressedSize J
lconst_0
lcmp
iflt L3
aload 0
getfield org/tukaani/xz/BlockInputStream/uncompressedSizeInHeader J
ldc2_w -1L
lcmp
ifeq L4
aload 0
getfield org/tukaani/xz/BlockInputStream/uncompressedSize J
aload 0
getfield org/tukaani/xz/BlockInputStream/uncompressedSizeInHeader J
lcmp
ifle L4
L3:
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L4:
iload 4
iload 3
if_icmplt L5
iload 4
istore 2
aload 0
getfield org/tukaani/xz/BlockInputStream/uncompressedSize J
aload 0
getfield org/tukaani/xz/BlockInputStream/uncompressedSizeInHeader J
lcmp
ifne L1
L5:
aload 0
getfield org/tukaani/xz/BlockInputStream/filterChain Ljava/io/InputStream;
invokevirtual java/io/InputStream/read()I
iconst_m1
if_icmpeq L6
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L6:
aload 0
invokespecial org/tukaani/xz/BlockInputStream/validate()V
aload 0
iconst_1
putfield org/tukaani/xz/BlockInputStream/endReached Z
iload 4
ireturn
L2:
iload 4
istore 2
iload 4
iconst_m1
if_icmpne L1
aload 0
invokespecial org/tukaani/xz/BlockInputStream/validate()V
aload 0
iconst_1
putfield org/tukaani/xz/BlockInputStream/endReached Z
iload 4
ireturn
.limit locals 7
.limit stack 5
.end method
