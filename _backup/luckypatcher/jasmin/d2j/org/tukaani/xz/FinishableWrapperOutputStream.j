.bytecode 50.0
.class public synchronized org/tukaani/xz/FinishableWrapperOutputStream
.super org/tukaani/xz/FinishableOutputStream

.field protected 'out' Ljava/io/OutputStream;

.method public <init>(Ljava/io/OutputStream;)V
aload 0
invokespecial org/tukaani/xz/FinishableOutputStream/<init>()V
aload 0
aload 1
putfield org/tukaani/xz/FinishableWrapperOutputStream/out Ljava/io/OutputStream;
return
.limit locals 2
.limit stack 2
.end method

.method public close()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/FinishableWrapperOutputStream/out Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/close()V
return
.limit locals 1
.limit stack 1
.end method

.method public flush()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/FinishableWrapperOutputStream/out Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/flush()V
return
.limit locals 1
.limit stack 1
.end method

.method public write(I)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/FinishableWrapperOutputStream/out Ljava/io/OutputStream;
iload 1
invokevirtual java/io/OutputStream/write(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public write([B)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/FinishableWrapperOutputStream/out Ljava/io/OutputStream;
aload 1
invokevirtual java/io/OutputStream/write([B)V
return
.limit locals 2
.limit stack 2
.end method

.method public write([BII)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/FinishableWrapperOutputStream/out Ljava/io/OutputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/OutputStream/write([BII)V
return
.limit locals 4
.limit stack 4
.end method
