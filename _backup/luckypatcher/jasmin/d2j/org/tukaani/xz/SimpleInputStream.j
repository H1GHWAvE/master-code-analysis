.bytecode 50.0
.class synchronized org/tukaani/xz/SimpleInputStream
.super java/io/InputStream

.field static final synthetic '$assertionsDisabled' Z

.field private static final 'FILTER_BUF_SIZE' I = 4096


.field private 'endReached' Z

.field private 'exception' Ljava/io/IOException;

.field private final 'filterBuf' [B

.field private 'filtered' I

.field private 'in' Ljava/io/InputStream;

.field private 'pos' I

.field private final 'simpleFilter' Lorg/tukaani/xz/simple/SimpleFilter;

.field private final 'tempBuf' [B

.field private 'unfiltered' I

.method static <clinit>()V
ldc org/tukaani/xz/SimpleInputStream
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/SimpleInputStream/$assertionsDisabled Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method <init>(Ljava/io/InputStream;Lorg/tukaani/xz/simple/SimpleFilter;)V
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
sipush 4096
newarray byte
putfield org/tukaani/xz/SimpleInputStream/filterBuf [B
aload 0
iconst_0
putfield org/tukaani/xz/SimpleInputStream/pos I
aload 0
iconst_0
putfield org/tukaani/xz/SimpleInputStream/filtered I
aload 0
iconst_0
putfield org/tukaani/xz/SimpleInputStream/unfiltered I
aload 0
iconst_0
putfield org/tukaani/xz/SimpleInputStream/endReached Z
aload 0
aconst_null
putfield org/tukaani/xz/SimpleInputStream/exception Ljava/io/IOException;
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/SimpleInputStream/tempBuf [B
aload 1
ifnonnull L0
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L0:
getstatic org/tukaani/xz/SimpleInputStream/$assertionsDisabled Z
ifne L1
aload 2
ifnonnull L1
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L1:
aload 0
aload 1
putfield org/tukaani/xz/SimpleInputStream/in Ljava/io/InputStream;
aload 0
aload 2
putfield org/tukaani/xz/SimpleInputStream/simpleFilter Lorg/tukaani/xz/simple/SimpleFilter;
return
.limit locals 3
.limit stack 2
.end method

.method static getMemoryUsage()I
iconst_5
ireturn
.limit locals 0
.limit stack 1
.end method

.method public available()I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SimpleInputStream/in Ljava/io/InputStream;
ifnonnull L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/tukaani/xz/SimpleInputStream/exception Ljava/io/IOException;
ifnull L1
aload 0
getfield org/tukaani/xz/SimpleInputStream/exception Ljava/io/IOException;
athrow
L1:
aload 0
getfield org/tukaani/xz/SimpleInputStream/filtered I
ireturn
.limit locals 1
.limit stack 3
.end method

.method public close()V
.throws java/io/IOException
.catch all from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/SimpleInputStream/in Ljava/io/InputStream;
ifnull L3
L0:
aload 0
getfield org/tukaani/xz/SimpleInputStream/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/close()V
L1:
aload 0
aconst_null
putfield org/tukaani/xz/SimpleInputStream/in Ljava/io/InputStream;
L3:
return
L2:
astore 1
aload 0
aconst_null
putfield org/tukaani/xz/SimpleInputStream/in Ljava/io/InputStream;
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public read()I
.throws java/io/IOException
aload 0
aload 0
getfield org/tukaani/xz/SimpleInputStream/tempBuf [B
iconst_0
iconst_1
invokevirtual org/tukaani/xz/SimpleInputStream/read([BII)I
iconst_m1
if_icmpne L0
iconst_m1
ireturn
L0:
aload 0
getfield org/tukaani/xz/SimpleInputStream/tempBuf [B
iconst_0
baload
sipush 255
iand
ireturn
.limit locals 1
.limit stack 4
.end method

.method public read([BII)I
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
.catch java/io/IOException from L7 to L2 using L2
.catch java/io/IOException from L8 to L9 using L2
.catch java/io/IOException from L10 to L11 using L2
.catch java/io/IOException from L12 to L13 using L2
.catch java/io/IOException from L13 to L14 using L2
iconst_0
istore 4
iload 2
iflt L15
iload 3
iflt L15
iload 2
iload 3
iadd
iflt L15
iload 2
iload 3
iadd
aload 1
arraylength
if_icmple L16
L15:
new java/lang/IndexOutOfBoundsException
dup
invokespecial java/lang/IndexOutOfBoundsException/<init>()V
athrow
L16:
iload 3
ifne L17
iload 4
istore 3
L18:
iload 3
ireturn
L17:
aload 0
getfield org/tukaani/xz/SimpleInputStream/in Ljava/io/InputStream;
ifnonnull L19
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L19:
aload 0
getfield org/tukaani/xz/SimpleInputStream/exception Ljava/io/IOException;
ifnull L20
aload 0
getfield org/tukaani/xz/SimpleInputStream/exception Ljava/io/IOException;
athrow
L20:
iconst_0
istore 5
iload 2
istore 4
iload 5
istore 2
L0:
aload 0
getfield org/tukaani/xz/SimpleInputStream/filtered I
iload 3
invokestatic java/lang/Math/min(II)I
istore 5
aload 0
getfield org/tukaani/xz/SimpleInputStream/filterBuf [B
aload 0
getfield org/tukaani/xz/SimpleInputStream/pos I
aload 1
iload 4
iload 5
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 0
getfield org/tukaani/xz/SimpleInputStream/pos I
iload 5
iadd
putfield org/tukaani/xz/SimpleInputStream/pos I
aload 0
aload 0
getfield org/tukaani/xz/SimpleInputStream/filtered I
iload 5
isub
putfield org/tukaani/xz/SimpleInputStream/filtered I
L1:
iload 4
iload 5
iadd
istore 4
iload 3
iload 5
isub
istore 3
iload 2
iload 5
iadd
istore 2
L3:
aload 0
getfield org/tukaani/xz/SimpleInputStream/pos I
aload 0
getfield org/tukaani/xz/SimpleInputStream/filtered I
iadd
aload 0
getfield org/tukaani/xz/SimpleInputStream/unfiltered I
iadd
sipush 4096
if_icmpne L4
aload 0
getfield org/tukaani/xz/SimpleInputStream/filterBuf [B
aload 0
getfield org/tukaani/xz/SimpleInputStream/pos I
aload 0
getfield org/tukaani/xz/SimpleInputStream/filterBuf [B
iconst_0
aload 0
getfield org/tukaani/xz/SimpleInputStream/filtered I
aload 0
getfield org/tukaani/xz/SimpleInputStream/unfiltered I
iadd
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
iconst_0
putfield org/tukaani/xz/SimpleInputStream/pos I
L4:
iload 3
ifeq L21
L5:
aload 0
getfield org/tukaani/xz/SimpleInputStream/endReached Z
ifeq L7
L6:
goto L21
L7:
getstatic org/tukaani/xz/SimpleInputStream/$assertionsDisabled Z
ifne L8
aload 0
getfield org/tukaani/xz/SimpleInputStream/filtered I
ifeq L8
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/SimpleInputStream/exception Ljava/io/IOException;
aload 1
athrow
L8:
aload 0
getfield org/tukaani/xz/SimpleInputStream/pos I
istore 5
aload 0
getfield org/tukaani/xz/SimpleInputStream/filtered I
istore 6
aload 0
getfield org/tukaani/xz/SimpleInputStream/unfiltered I
istore 7
aload 0
getfield org/tukaani/xz/SimpleInputStream/in Ljava/io/InputStream;
aload 0
getfield org/tukaani/xz/SimpleInputStream/filterBuf [B
aload 0
getfield org/tukaani/xz/SimpleInputStream/pos I
aload 0
getfield org/tukaani/xz/SimpleInputStream/filtered I
iadd
aload 0
getfield org/tukaani/xz/SimpleInputStream/unfiltered I
iadd
sipush 4096
iload 5
iload 6
iadd
iload 7
iadd
isub
invokevirtual java/io/InputStream/read([BII)I
istore 5
L9:
iload 5
iconst_m1
if_icmpne L12
L10:
aload 0
iconst_1
putfield org/tukaani/xz/SimpleInputStream/endReached Z
aload 0
aload 0
getfield org/tukaani/xz/SimpleInputStream/unfiltered I
putfield org/tukaani/xz/SimpleInputStream/filtered I
aload 0
iconst_0
putfield org/tukaani/xz/SimpleInputStream/unfiltered I
L11:
goto L0
L12:
aload 0
aload 0
getfield org/tukaani/xz/SimpleInputStream/unfiltered I
iload 5
iadd
putfield org/tukaani/xz/SimpleInputStream/unfiltered I
aload 0
aload 0
getfield org/tukaani/xz/SimpleInputStream/simpleFilter Lorg/tukaani/xz/simple/SimpleFilter;
aload 0
getfield org/tukaani/xz/SimpleInputStream/filterBuf [B
aload 0
getfield org/tukaani/xz/SimpleInputStream/pos I
aload 0
getfield org/tukaani/xz/SimpleInputStream/unfiltered I
invokeinterface org/tukaani/xz/simple/SimpleFilter/code([BII)I 3
putfield org/tukaani/xz/SimpleInputStream/filtered I
getstatic org/tukaani/xz/SimpleInputStream/$assertionsDisabled Z
ifne L13
aload 0
getfield org/tukaani/xz/SimpleInputStream/filtered I
aload 0
getfield org/tukaani/xz/SimpleInputStream/unfiltered I
if_icmple L13
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L13:
aload 0
aload 0
getfield org/tukaani/xz/SimpleInputStream/unfiltered I
aload 0
getfield org/tukaani/xz/SimpleInputStream/filtered I
isub
putfield org/tukaani/xz/SimpleInputStream/unfiltered I
L14:
goto L0
L21:
iload 2
istore 3
iload 2
ifgt L18
iconst_m1
ireturn
.limit locals 8
.limit stack 6
.end method
