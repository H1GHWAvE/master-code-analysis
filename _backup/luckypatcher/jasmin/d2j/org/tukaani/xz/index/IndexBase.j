.bytecode 50.0
.class synchronized abstract org/tukaani/xz/index/IndexBase
.super java/lang/Object

.field 'blocksSum' J

.field 'indexListSize' J

.field private final 'invalidIndexException' Lorg/tukaani/xz/XZIOException;

.field 'recordCount' J

.field 'uncompressedSum' J

.method <init>(Lorg/tukaani/xz/XZIOException;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lconst_0
putfield org/tukaani/xz/index/IndexBase/blocksSum J
aload 0
lconst_0
putfield org/tukaani/xz/index/IndexBase/uncompressedSum J
aload 0
lconst_0
putfield org/tukaani/xz/index/IndexBase/indexListSize J
aload 0
lconst_0
putfield org/tukaani/xz/index/IndexBase/recordCount J
aload 0
aload 1
putfield org/tukaani/xz/index/IndexBase/invalidIndexException Lorg/tukaani/xz/XZIOException;
return
.limit locals 2
.limit stack 3
.end method

.method private getUnpaddedIndexSize()J
aload 0
getfield org/tukaani/xz/index/IndexBase/recordCount J
invokestatic org/tukaani/xz/common/Util/getVLISize(J)I
iconst_1
iadd
i2l
aload 0
getfield org/tukaani/xz/index/IndexBase/indexListSize J
ladd
ldc2_w 4L
ladd
lreturn
.limit locals 1
.limit stack 4
.end method

.method add(JJ)V
.throws org/tukaani/xz/XZIOException
aload 0
aload 0
getfield org/tukaani/xz/index/IndexBase/blocksSum J
ldc2_w 3L
lload 1
ladd
ldc2_w -4L
land
ladd
putfield org/tukaani/xz/index/IndexBase/blocksSum J
aload 0
aload 0
getfield org/tukaani/xz/index/IndexBase/uncompressedSum J
lload 3
ladd
putfield org/tukaani/xz/index/IndexBase/uncompressedSum J
aload 0
aload 0
getfield org/tukaani/xz/index/IndexBase/indexListSize J
lload 1
invokestatic org/tukaani/xz/common/Util/getVLISize(J)I
lload 3
invokestatic org/tukaani/xz/common/Util/getVLISize(J)I
iadd
i2l
ladd
putfield org/tukaani/xz/index/IndexBase/indexListSize J
aload 0
aload 0
getfield org/tukaani/xz/index/IndexBase/recordCount J
lconst_1
ladd
putfield org/tukaani/xz/index/IndexBase/recordCount J
aload 0
getfield org/tukaani/xz/index/IndexBase/blocksSum J
lconst_0
lcmp
iflt L0
aload 0
getfield org/tukaani/xz/index/IndexBase/uncompressedSum J
lconst_0
lcmp
iflt L0
aload 0
invokevirtual org/tukaani/xz/index/IndexBase/getIndexSize()J
ldc2_w 17179869184L
lcmp
ifgt L0
aload 0
invokevirtual org/tukaani/xz/index/IndexBase/getStreamSize()J
lconst_0
lcmp
ifge L1
L0:
aload 0
getfield org/tukaani/xz/index/IndexBase/invalidIndexException Lorg/tukaani/xz/XZIOException;
athrow
L1:
return
.limit locals 5
.limit stack 7
.end method

.method getIndexPaddingSize()I
ldc2_w 4L
aload 0
invokespecial org/tukaani/xz/index/IndexBase/getUnpaddedIndexSize()J
lsub
ldc2_w 3L
land
l2i
ireturn
.limit locals 1
.limit stack 4
.end method

.method public getIndexSize()J
aload 0
invokespecial org/tukaani/xz/index/IndexBase/getUnpaddedIndexSize()J
ldc2_w 3L
ladd
ldc2_w -4L
land
lreturn
.limit locals 1
.limit stack 4
.end method

.method public getStreamSize()J
aload 0
getfield org/tukaani/xz/index/IndexBase/blocksSum J
ldc2_w 12L
ladd
aload 0
invokevirtual org/tukaani/xz/index/IndexBase/getIndexSize()J
ladd
ldc2_w 12L
ladd
lreturn
.limit locals 1
.limit stack 4
.end method
