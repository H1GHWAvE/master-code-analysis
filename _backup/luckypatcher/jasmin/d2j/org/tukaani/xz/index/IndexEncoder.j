.bytecode 50.0
.class public synchronized org/tukaani/xz/index/IndexEncoder
.super org/tukaani/xz/index/IndexBase

.field private final 'records' Ljava/util/ArrayList;

.method public <init>()V
aload 0
new org/tukaani/xz/XZIOException
dup
ldc "XZ Stream or its Index has grown too big"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
invokespecial org/tukaani/xz/index/IndexBase/<init>(Lorg/tukaani/xz/XZIOException;)V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield org/tukaani/xz/index/IndexEncoder/records Ljava/util/ArrayList;
return
.limit locals 1
.limit stack 4
.end method

.method public add(JJ)V
.throws org/tukaani/xz/XZIOException
aload 0
lload 1
lload 3
invokespecial org/tukaani/xz/index/IndexBase/add(JJ)V
aload 0
getfield org/tukaani/xz/index/IndexEncoder/records Ljava/util/ArrayList;
new org/tukaani/xz/index/IndexRecord
dup
lload 1
lload 3
invokespecial org/tukaani/xz/index/IndexRecord/<init>(JJ)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
return
.limit locals 5
.limit stack 7
.end method

.method public encode(Ljava/io/OutputStream;)V
.throws java/io/IOException
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 5
new java/util/zip/CheckedOutputStream
dup
aload 1
aload 5
invokespecial java/util/zip/CheckedOutputStream/<init>(Ljava/io/OutputStream;Ljava/util/zip/Checksum;)V
astore 6
aload 6
iconst_0
invokevirtual java/util/zip/CheckedOutputStream/write(I)V
aload 6
aload 0
getfield org/tukaani/xz/index/IndexEncoder/recordCount J
invokestatic org/tukaani/xz/common/EncoderUtil/encodeVLI(Ljava/io/OutputStream;J)V
aload 0
getfield org/tukaani/xz/index/IndexEncoder/records Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 7
L0:
aload 7
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 7
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/tukaani/xz/index/IndexRecord
astore 8
aload 6
aload 8
getfield org/tukaani/xz/index/IndexRecord/unpadded J
invokestatic org/tukaani/xz/common/EncoderUtil/encodeVLI(Ljava/io/OutputStream;J)V
aload 6
aload 8
getfield org/tukaani/xz/index/IndexRecord/uncompressed J
invokestatic org/tukaani/xz/common/EncoderUtil/encodeVLI(Ljava/io/OutputStream;J)V
goto L0
L1:
aload 0
invokevirtual org/tukaani/xz/index/IndexEncoder/getIndexPaddingSize()I
istore 2
L2:
iload 2
ifle L3
aload 6
iconst_0
invokevirtual java/util/zip/CheckedOutputStream/write(I)V
iload 2
iconst_1
isub
istore 2
goto L2
L3:
aload 5
invokevirtual java/util/zip/CRC32/getValue()J
lstore 3
iconst_0
istore 2
L4:
iload 2
iconst_4
if_icmpge L5
aload 1
lload 3
iload 2
bipush 8
imul
lushr
l2i
i2b
invokevirtual java/io/OutputStream/write(I)V
iload 2
iconst_1
iadd
istore 2
goto L4
L5:
return
.limit locals 9
.limit stack 5
.end method

.method public volatile synthetic getIndexSize()J
aload 0
invokespecial org/tukaani/xz/index/IndexBase/getIndexSize()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public volatile synthetic getStreamSize()J
aload 0
invokespecial org/tukaani/xz/index/IndexBase/getStreamSize()J
lreturn
.limit locals 1
.limit stack 2
.end method
