.bytecode 50.0
.class public synchronized org/tukaani/xz/index/BlockInfo
.super java/lang/Object

.field public 'blockNumber' I

.field public 'compressedOffset' J

.field 'index' Lorg/tukaani/xz/index/IndexDecoder;

.field public 'uncompressedOffset' J

.field public 'uncompressedSize' J

.field public 'unpaddedSize' J

.method public <init>(Lorg/tukaani/xz/index/IndexDecoder;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_m1
putfield org/tukaani/xz/index/BlockInfo/blockNumber I
aload 0
ldc2_w -1L
putfield org/tukaani/xz/index/BlockInfo/compressedOffset J
aload 0
ldc2_w -1L
putfield org/tukaani/xz/index/BlockInfo/uncompressedOffset J
aload 0
ldc2_w -1L
putfield org/tukaani/xz/index/BlockInfo/unpaddedSize J
aload 0
ldc2_w -1L
putfield org/tukaani/xz/index/BlockInfo/uncompressedSize J
aload 0
aload 1
putfield org/tukaani/xz/index/BlockInfo/index Lorg/tukaani/xz/index/IndexDecoder;
return
.limit locals 2
.limit stack 3
.end method

.method public getCheckType()I
aload 0
getfield org/tukaani/xz/index/BlockInfo/index Lorg/tukaani/xz/index/IndexDecoder;
invokevirtual org/tukaani/xz/index/IndexDecoder/getStreamFlags()Lorg/tukaani/xz/common/StreamFlags;
getfield org/tukaani/xz/common/StreamFlags/checkType I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public hasNext()Z
aload 0
getfield org/tukaani/xz/index/BlockInfo/index Lorg/tukaani/xz/index/IndexDecoder;
aload 0
getfield org/tukaani/xz/index/BlockInfo/blockNumber I
iconst_1
iadd
invokevirtual org/tukaani/xz/index/IndexDecoder/hasRecord(I)Z
ireturn
.limit locals 1
.limit stack 3
.end method

.method public setNext()V
aload 0
getfield org/tukaani/xz/index/BlockInfo/index Lorg/tukaani/xz/index/IndexDecoder;
aload 0
aload 0
getfield org/tukaani/xz/index/BlockInfo/blockNumber I
iconst_1
iadd
invokevirtual org/tukaani/xz/index/IndexDecoder/setBlockInfo(Lorg/tukaani/xz/index/BlockInfo;I)V
return
.limit locals 1
.limit stack 4
.end method
