.bytecode 50.0
.class public synchronized org/tukaani/xz/index/IndexDecoder
.super org/tukaani/xz/index/IndexBase

.field static final synthetic '$assertionsDisabled' Z

.field private 'compressedOffset' J

.field private 'largestBlockSize' J

.field private final 'memoryUsage' I

.field private 'recordOffset' I

.field private final 'streamFlags' Lorg/tukaani/xz/common/StreamFlags;

.field private final 'streamPadding' J

.field private final 'uncompressed' [J

.field private 'uncompressedOffset' J

.field private final 'unpadded' [J

.method static <clinit>()V
ldc org/tukaani/xz/index/IndexDecoder
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/index/IndexDecoder/$assertionsDisabled Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/tukaani/xz/SeekableInputStream;Lorg/tukaani/xz/common/StreamFlags;JI)V
.throws java/io/IOException
.catch java/io/EOFException from L0 to L1 using L1
.catch java/io/EOFException from L2 to L3 using L1
.catch java/io/EOFException from L3 to L4 using L1
.catch java/io/EOFException from L5 to L6 using L1
.catch java/io/EOFException from L6 to L7 using L1
.catch java/io/EOFException from L8 to L9 using L1
.catch java/io/EOFException from L9 to L10 using L1
.catch java/io/EOFException from L11 to L12 using L1
.catch java/io/EOFException from L12 to L13 using L1
aload 0
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
invokespecial org/tukaani/xz/index/IndexBase/<init>(Lorg/tukaani/xz/XZIOException;)V
aload 0
lconst_0
putfield org/tukaani/xz/index/IndexDecoder/largestBlockSize J
aload 0
iconst_0
putfield org/tukaani/xz/index/IndexDecoder/recordOffset I
aload 0
lconst_0
putfield org/tukaani/xz/index/IndexDecoder/compressedOffset J
aload 0
lconst_0
putfield org/tukaani/xz/index/IndexDecoder/uncompressedOffset J
aload 0
aload 2
putfield org/tukaani/xz/index/IndexDecoder/streamFlags Lorg/tukaani/xz/common/StreamFlags;
aload 0
lload 3
putfield org/tukaani/xz/index/IndexDecoder/streamPadding J
aload 1
invokevirtual org/tukaani/xz/SeekableInputStream/position()J
aload 2
getfield org/tukaani/xz/common/StreamFlags/backwardSize J
ladd
ldc2_w 4L
lsub
lstore 3
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 11
new java/util/zip/CheckedInputStream
dup
aload 1
aload 11
invokespecial java/util/zip/CheckedInputStream/<init>(Ljava/io/InputStream;Ljava/util/zip/Checksum;)V
astore 12
aload 12
invokevirtual java/util/zip/CheckedInputStream/read()I
ifeq L0
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 12
invokestatic org/tukaani/xz/common/DecoderUtil/decodeVLI(Ljava/io/InputStream;)J
lstore 7
lload 7
aload 2
getfield org/tukaani/xz/common/StreamFlags/backwardSize J
ldc2_w 2L
ldiv
lcmp
iflt L14
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 1
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L14:
lload 7
ldc2_w 2147483647L
lcmp
ifle L3
L2:
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "XZ Index has over 2147483647 Records"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
ldc2_w 16L
lload 7
lmul
ldc2_w 1023L
ladd
ldc2_w 1024L
ldiv
l2i
iconst_1
iadd
putfield org/tukaani/xz/index/IndexDecoder/memoryUsage I
L4:
iload 5
iflt L6
L5:
aload 0
getfield org/tukaani/xz/index/IndexDecoder/memoryUsage I
iload 5
if_icmple L6
new org/tukaani/xz/MemoryLimitException
dup
aload 0
getfield org/tukaani/xz/index/IndexDecoder/memoryUsage I
iload 5
invokespecial org/tukaani/xz/MemoryLimitException/<init>(II)V
athrow
L6:
aload 0
lload 7
l2i
newarray long
putfield org/tukaani/xz/index/IndexDecoder/unpadded [J
aload 0
lload 7
l2i
newarray long
putfield org/tukaani/xz/index/IndexDecoder/uncompressed [J
L7:
iconst_0
istore 6
lload 7
l2i
istore 5
L15:
iload 5
ifle L16
L8:
aload 12
invokestatic org/tukaani/xz/common/DecoderUtil/decodeVLI(Ljava/io/InputStream;)J
lstore 7
aload 12
invokestatic org/tukaani/xz/common/DecoderUtil/decodeVLI(Ljava/io/InputStream;)J
lstore 9
aload 1
invokevirtual org/tukaani/xz/SeekableInputStream/position()J
lload 3
lcmp
ifle L9
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L9:
aload 0
getfield org/tukaani/xz/index/IndexDecoder/unpadded [J
iload 6
aload 0
getfield org/tukaani/xz/index/IndexDecoder/blocksSum J
lload 7
ladd
lastore
aload 0
getfield org/tukaani/xz/index/IndexDecoder/uncompressed [J
iload 6
aload 0
getfield org/tukaani/xz/index/IndexDecoder/uncompressedSum J
lload 9
ladd
lastore
L10:
iload 6
iconst_1
iadd
istore 6
L11:
aload 0
lload 7
lload 9
invokespecial org/tukaani/xz/index/IndexBase/add(JJ)V
getstatic org/tukaani/xz/index/IndexDecoder/$assertionsDisabled Z
ifne L12
iload 6
i2l
aload 0
getfield org/tukaani/xz/index/IndexDecoder/recordCount J
lcmp
ifeq L12
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L12:
aload 0
getfield org/tukaani/xz/index/IndexDecoder/largestBlockSize J
lload 9
lcmp
ifge L13
aload 0
lload 9
putfield org/tukaani/xz/index/IndexDecoder/largestBlockSize J
L13:
iload 5
iconst_1
isub
istore 5
goto L15
L16:
aload 0
invokevirtual org/tukaani/xz/index/IndexDecoder/getIndexPaddingSize()I
istore 6
iload 6
istore 5
aload 1
invokevirtual org/tukaani/xz/SeekableInputStream/position()J
iload 6
i2l
ladd
lload 3
lcmp
ifeq L17
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L17:
iload 5
iconst_1
isub
istore 6
iload 5
ifle L18
iload 6
istore 5
aload 12
invokevirtual java/util/zip/CheckedInputStream/read()I
ifeq L17
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L18:
aload 11
invokevirtual java/util/zip/CRC32/getValue()J
lstore 3
iconst_0
istore 5
L19:
iload 5
iconst_4
if_icmpge L20
lload 3
iload 5
bipush 8
imul
lushr
ldc2_w 255L
land
aload 1
invokevirtual org/tukaani/xz/SeekableInputStream/read()I
i2l
lcmp
ifeq L21
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L21:
iload 5
iconst_1
iadd
istore 5
goto L19
L20:
return
.limit locals 13
.limit stack 6
.end method

.method public volatile synthetic getIndexSize()J
aload 0
invokespecial org/tukaani/xz/index/IndexBase/getIndexSize()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getLargestBlockSize()J
aload 0
getfield org/tukaani/xz/index/IndexDecoder/largestBlockSize J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getMemoryUsage()I
aload 0
getfield org/tukaani/xz/index/IndexDecoder/memoryUsage I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getRecordCount()I
aload 0
getfield org/tukaani/xz/index/IndexDecoder/recordCount J
l2i
ireturn
.limit locals 1
.limit stack 2
.end method

.method public getStreamFlags()Lorg/tukaani/xz/common/StreamFlags;
aload 0
getfield org/tukaani/xz/index/IndexDecoder/streamFlags Lorg/tukaani/xz/common/StreamFlags;
areturn
.limit locals 1
.limit stack 1
.end method

.method public volatile synthetic getStreamSize()J
aload 0
invokespecial org/tukaani/xz/index/IndexBase/getStreamSize()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getUncompressedSize()J
aload 0
getfield org/tukaani/xz/index/IndexDecoder/uncompressedSum J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public hasRecord(I)Z
iload 1
aload 0
getfield org/tukaani/xz/index/IndexDecoder/recordOffset I
if_icmplt L0
iload 1
i2l
aload 0
getfield org/tukaani/xz/index/IndexDecoder/recordOffset I
i2l
aload 0
getfield org/tukaani/xz/index/IndexDecoder/recordCount J
ladd
lcmp
ifge L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 6
.end method

.method public hasUncompressedOffset(J)Z
lload 1
aload 0
getfield org/tukaani/xz/index/IndexDecoder/uncompressedOffset J
lcmp
iflt L0
lload 1
aload 0
getfield org/tukaani/xz/index/IndexDecoder/uncompressedOffset J
aload 0
getfield org/tukaani/xz/index/IndexDecoder/uncompressedSum J
ladd
lcmp
ifge L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 6
.end method

.method public locateBlock(Lorg/tukaani/xz/index/BlockInfo;J)V
getstatic org/tukaani/xz/index/IndexDecoder/$assertionsDisabled Z
ifne L0
lload 2
aload 0
getfield org/tukaani/xz/index/IndexDecoder/uncompressedOffset J
lcmp
ifge L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
lload 2
aload 0
getfield org/tukaani/xz/index/IndexDecoder/uncompressedOffset J
lsub
lstore 2
getstatic org/tukaani/xz/index/IndexDecoder/$assertionsDisabled Z
ifne L1
lload 2
aload 0
getfield org/tukaani/xz/index/IndexDecoder/uncompressedSum J
lcmp
iflt L1
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L1:
iconst_0
istore 5
aload 0
getfield org/tukaani/xz/index/IndexDecoder/unpadded [J
arraylength
iconst_1
isub
istore 4
L2:
iload 5
iload 4
if_icmpge L3
iload 5
iload 4
iload 5
isub
iconst_2
idiv
iadd
istore 6
aload 0
getfield org/tukaani/xz/index/IndexDecoder/uncompressed [J
iload 6
laload
lload 2
lcmp
ifgt L4
iload 6
iconst_1
iadd
istore 5
goto L2
L4:
iload 6
istore 4
goto L2
L3:
aload 0
aload 1
aload 0
getfield org/tukaani/xz/index/IndexDecoder/recordOffset I
iload 5
iadd
invokevirtual org/tukaani/xz/index/IndexDecoder/setBlockInfo(Lorg/tukaani/xz/index/BlockInfo;I)V
return
.limit locals 7
.limit stack 4
.end method

.method public setBlockInfo(Lorg/tukaani/xz/index/BlockInfo;I)V
getstatic org/tukaani/xz/index/IndexDecoder/$assertionsDisabled Z
ifne L0
iload 2
aload 0
getfield org/tukaani/xz/index/IndexDecoder/recordOffset I
if_icmpge L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
getstatic org/tukaani/xz/index/IndexDecoder/$assertionsDisabled Z
ifne L1
iload 2
aload 0
getfield org/tukaani/xz/index/IndexDecoder/recordOffset I
isub
i2l
aload 0
getfield org/tukaani/xz/index/IndexDecoder/recordCount J
lcmp
iflt L1
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L1:
aload 1
aload 0
putfield org/tukaani/xz/index/BlockInfo/index Lorg/tukaani/xz/index/IndexDecoder;
aload 1
iload 2
putfield org/tukaani/xz/index/BlockInfo/blockNumber I
iload 2
aload 0
getfield org/tukaani/xz/index/IndexDecoder/recordOffset I
isub
istore 2
iload 2
ifne L2
aload 1
lconst_0
putfield org/tukaani/xz/index/BlockInfo/compressedOffset J
aload 1
lconst_0
putfield org/tukaani/xz/index/BlockInfo/uncompressedOffset J
L3:
aload 1
aload 0
getfield org/tukaani/xz/index/IndexDecoder/unpadded [J
iload 2
laload
aload 1
getfield org/tukaani/xz/index/BlockInfo/compressedOffset J
lsub
putfield org/tukaani/xz/index/BlockInfo/unpaddedSize J
aload 1
aload 0
getfield org/tukaani/xz/index/IndexDecoder/uncompressed [J
iload 2
laload
aload 1
getfield org/tukaani/xz/index/BlockInfo/uncompressedOffset J
lsub
putfield org/tukaani/xz/index/BlockInfo/uncompressedSize J
aload 1
aload 1
getfield org/tukaani/xz/index/BlockInfo/compressedOffset J
aload 0
getfield org/tukaani/xz/index/IndexDecoder/compressedOffset J
ldc2_w 12L
ladd
ladd
putfield org/tukaani/xz/index/BlockInfo/compressedOffset J
aload 1
aload 1
getfield org/tukaani/xz/index/BlockInfo/uncompressedOffset J
aload 0
getfield org/tukaani/xz/index/IndexDecoder/uncompressedOffset J
ladd
putfield org/tukaani/xz/index/BlockInfo/uncompressedOffset J
return
L2:
aload 1
aload 0
getfield org/tukaani/xz/index/IndexDecoder/unpadded [J
iload 2
iconst_1
isub
laload
ldc2_w 3L
ladd
ldc2_w -4L
land
putfield org/tukaani/xz/index/BlockInfo/compressedOffset J
aload 1
aload 0
getfield org/tukaani/xz/index/IndexDecoder/uncompressed [J
iload 2
iconst_1
isub
laload
putfield org/tukaani/xz/index/BlockInfo/uncompressedOffset J
goto L3
.limit locals 3
.limit stack 7
.end method

.method public setOffsets(Lorg/tukaani/xz/index/IndexDecoder;)V
aload 0
aload 1
getfield org/tukaani/xz/index/IndexDecoder/recordOffset I
aload 1
getfield org/tukaani/xz/index/IndexDecoder/recordCount J
l2i
iadd
putfield org/tukaani/xz/index/IndexDecoder/recordOffset I
aload 0
aload 1
getfield org/tukaani/xz/index/IndexDecoder/compressedOffset J
aload 1
invokevirtual org/tukaani/xz/index/IndexDecoder/getStreamSize()J
ladd
aload 1
getfield org/tukaani/xz/index/IndexDecoder/streamPadding J
ladd
putfield org/tukaani/xz/index/IndexDecoder/compressedOffset J
getstatic org/tukaani/xz/index/IndexDecoder/$assertionsDisabled Z
ifne L0
aload 0
getfield org/tukaani/xz/index/IndexDecoder/compressedOffset J
ldc2_w 3L
land
lconst_0
lcmp
ifeq L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
aload 0
aload 1
getfield org/tukaani/xz/index/IndexDecoder/uncompressedOffset J
aload 1
getfield org/tukaani/xz/index/IndexDecoder/uncompressedSum J
ladd
putfield org/tukaani/xz/index/IndexDecoder/uncompressedOffset J
return
.limit locals 2
.limit stack 5
.end method
