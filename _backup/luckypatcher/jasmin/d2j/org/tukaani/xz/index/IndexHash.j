.bytecode 50.0
.class public synchronized org/tukaani/xz/index/IndexHash
.super org/tukaani/xz/index/IndexBase

.field private 'hash' Lorg/tukaani/xz/check/Check;

.method public <init>()V
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
aload 0
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
invokespecial org/tukaani/xz/index/IndexBase/<init>(Lorg/tukaani/xz/XZIOException;)V
L0:
aload 0
new org/tukaani/xz/check/SHA256
dup
invokespecial org/tukaani/xz/check/SHA256/<init>()V
putfield org/tukaani/xz/index/IndexHash/hash Lorg/tukaani/xz/check/Check;
L1:
return
L2:
astore 1
aload 0
new org/tukaani/xz/check/CRC32
dup
invokespecial org/tukaani/xz/check/CRC32/<init>()V
putfield org/tukaani/xz/index/IndexHash/hash Lorg/tukaani/xz/check/Check;
return
.limit locals 2
.limit stack 3
.end method

.method public add(JJ)V
.throws org/tukaani/xz/XZIOException
aload 0
lload 1
lload 3
invokespecial org/tukaani/xz/index/IndexBase/add(JJ)V
bipush 16
invokestatic java/nio/ByteBuffer/allocate(I)Ljava/nio/ByteBuffer;
astore 5
aload 5
lload 1
invokevirtual java/nio/ByteBuffer/putLong(J)Ljava/nio/ByteBuffer;
pop
aload 5
lload 3
invokevirtual java/nio/ByteBuffer/putLong(J)Ljava/nio/ByteBuffer;
pop
aload 0
getfield org/tukaani/xz/index/IndexHash/hash Lorg/tukaani/xz/check/Check;
aload 5
invokevirtual java/nio/ByteBuffer/array()[B
invokevirtual org/tukaani/xz/check/Check/update([B)V
return
.limit locals 6
.limit stack 5
.end method

.method public volatile synthetic getIndexSize()J
aload 0
invokespecial org/tukaani/xz/index/IndexBase/getIndexSize()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public volatile synthetic getStreamSize()J
aload 0
invokespecial org/tukaani/xz/index/IndexBase/getStreamSize()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public validate(Ljava/io/InputStream;)V
.throws java/io/IOException
.catch org/tukaani/xz/XZIOException from L0 to L1 using L2
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 9
aload 9
iconst_0
invokevirtual java/util/zip/CRC32/update(I)V
new java/util/zip/CheckedInputStream
dup
aload 1
aload 9
invokespecial java/util/zip/CheckedInputStream/<init>(Ljava/io/InputStream;Ljava/util/zip/Checksum;)V
astore 1
aload 1
invokestatic org/tukaani/xz/common/DecoderUtil/decodeVLI(Ljava/io/InputStream;)J
aload 0
getfield org/tukaani/xz/index/IndexHash/recordCount J
lcmp
ifeq L3
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L3:
new org/tukaani/xz/index/IndexHash
dup
invokespecial org/tukaani/xz/index/IndexHash/<init>()V
astore 10
lconst_0
lstore 3
L4:
lload 3
aload 0
getfield org/tukaani/xz/index/IndexHash/recordCount J
lcmp
ifge L5
aload 1
invokestatic org/tukaani/xz/common/DecoderUtil/decodeVLI(Ljava/io/InputStream;)J
lstore 5
aload 1
invokestatic org/tukaani/xz/common/DecoderUtil/decodeVLI(Ljava/io/InputStream;)J
lstore 7
L0:
aload 10
lload 5
lload 7
invokevirtual org/tukaani/xz/index/IndexHash/add(JJ)V
L1:
aload 10
getfield org/tukaani/xz/index/IndexHash/blocksSum J
aload 0
getfield org/tukaani/xz/index/IndexHash/blocksSum J
lcmp
ifgt L6
aload 10
getfield org/tukaani/xz/index/IndexHash/uncompressedSum J
aload 0
getfield org/tukaani/xz/index/IndexHash/uncompressedSum J
lcmp
ifgt L6
aload 10
getfield org/tukaani/xz/index/IndexHash/indexListSize J
aload 0
getfield org/tukaani/xz/index/IndexHash/indexListSize J
lcmp
ifle L7
L6:
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L7:
lload 3
lconst_1
ladd
lstore 3
goto L4
L5:
aload 10
getfield org/tukaani/xz/index/IndexHash/blocksSum J
aload 0
getfield org/tukaani/xz/index/IndexHash/blocksSum J
lcmp
ifne L8
aload 10
getfield org/tukaani/xz/index/IndexHash/uncompressedSum J
aload 0
getfield org/tukaani/xz/index/IndexHash/uncompressedSum J
lcmp
ifne L8
aload 10
getfield org/tukaani/xz/index/IndexHash/indexListSize J
aload 0
getfield org/tukaani/xz/index/IndexHash/indexListSize J
lcmp
ifne L8
aload 10
getfield org/tukaani/xz/index/IndexHash/hash Lorg/tukaani/xz/check/Check;
invokevirtual org/tukaani/xz/check/Check/finish()[B
aload 0
getfield org/tukaani/xz/index/IndexHash/hash Lorg/tukaani/xz/check/Check;
invokevirtual org/tukaani/xz/check/Check/finish()[B
invokestatic java/util/Arrays/equals([B[B)Z
ifne L9
L8:
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L9:
new java/io/DataInputStream
dup
aload 1
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
astore 1
aload 0
invokevirtual org/tukaani/xz/index/IndexHash/getIndexPaddingSize()I
istore 2
L10:
iload 2
ifle L11
aload 1
invokevirtual java/io/DataInputStream/readUnsignedByte()I
ifeq L12
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L12:
iload 2
iconst_1
isub
istore 2
goto L10
L11:
aload 9
invokevirtual java/util/zip/CRC32/getValue()J
lstore 3
iconst_0
istore 2
L13:
iload 2
iconst_4
if_icmpge L14
lload 3
iload 2
bipush 8
imul
lushr
ldc2_w 255L
land
aload 1
invokevirtual java/io/DataInputStream/readUnsignedByte()I
i2l
lcmp
ifeq L15
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L15:
iload 2
iconst_1
iadd
istore 2
goto L13
L14:
return
.limit locals 11
.limit stack 5
.end method
