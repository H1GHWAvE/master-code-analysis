.bytecode 50.0
.class synchronized org/tukaani/xz/CountingOutputStream
.super org/tukaani/xz/FinishableOutputStream

.field private final 'out' Ljava/io/OutputStream;

.field private 'size' J

.method public <init>(Ljava/io/OutputStream;)V
aload 0
invokespecial org/tukaani/xz/FinishableOutputStream/<init>()V
aload 0
lconst_0
putfield org/tukaani/xz/CountingOutputStream/size J
aload 0
aload 1
putfield org/tukaani/xz/CountingOutputStream/out Ljava/io/OutputStream;
return
.limit locals 2
.limit stack 3
.end method

.method public close()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/CountingOutputStream/out Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/close()V
return
.limit locals 1
.limit stack 1
.end method

.method public flush()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/CountingOutputStream/out Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/flush()V
return
.limit locals 1
.limit stack 1
.end method

.method public getSize()J
aload 0
getfield org/tukaani/xz/CountingOutputStream/size J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public write(I)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/CountingOutputStream/out Ljava/io/OutputStream;
iload 1
invokevirtual java/io/OutputStream/write(I)V
aload 0
getfield org/tukaani/xz/CountingOutputStream/size J
lconst_0
lcmp
iflt L0
aload 0
aload 0
getfield org/tukaani/xz/CountingOutputStream/size J
lconst_1
ladd
putfield org/tukaani/xz/CountingOutputStream/size J
L0:
return
.limit locals 2
.limit stack 5
.end method

.method public write([BII)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/CountingOutputStream/out Ljava/io/OutputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/OutputStream/write([BII)V
aload 0
getfield org/tukaani/xz/CountingOutputStream/size J
lconst_0
lcmp
iflt L0
aload 0
aload 0
getfield org/tukaani/xz/CountingOutputStream/size J
iload 3
i2l
ladd
putfield org/tukaani/xz/CountingOutputStream/size J
L0:
return
.limit locals 4
.limit stack 5
.end method
