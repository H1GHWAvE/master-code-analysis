.bytecode 50.0
.class public synchronized org/tukaani/xz/common/Util
.super java/lang/Object

.field public static final 'BACKWARD_SIZE_MAX' J = 17179869184L


.field public static final 'BLOCK_HEADER_SIZE_MAX' I = 1024


.field public static final 'STREAM_HEADER_SIZE' I = 12


.field public static final 'VLI_MAX' J = 9223372036854775807L


.field public static final 'VLI_SIZE_MAX' I = 9


.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getVLISize(J)I
iconst_0
istore 2
L0:
iload 2
iconst_1
iadd
istore 3
lload 0
bipush 7
lshr
lstore 4
iload 3
istore 2
lload 4
lstore 0
lload 4
lconst_0
lcmp
ifne L0
iload 3
ireturn
.limit locals 6
.limit stack 4
.end method
