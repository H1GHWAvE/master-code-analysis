.bytecode 50.0
.class public synchronized org/tukaani/xz/common/EncoderUtil
.super org/tukaani/xz/common/Util

.method public <init>()V
aload 0
invokespecial org/tukaani/xz/common/Util/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static encodeVLI(Ljava/io/OutputStream;J)V
.throws java/io/IOException
L0:
lload 1
ldc2_w 128L
lcmp
iflt L1
aload 0
lload 1
ldc2_w 128L
lor
l2i
i2b
invokevirtual java/io/OutputStream/write(I)V
lload 1
bipush 7
lushr
lstore 1
goto L0
L1:
aload 0
lload 1
l2i
i2b
invokevirtual java/io/OutputStream/write(I)V
return
.limit locals 3
.limit stack 5
.end method

.method public static writeCRC32(Ljava/io/OutputStream;[B)V
.throws java/io/IOException
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 5
aload 5
aload 1
invokevirtual java/util/zip/CRC32/update([B)V
aload 5
invokevirtual java/util/zip/CRC32/getValue()J
lstore 3
iconst_0
istore 2
L0:
iload 2
iconst_4
if_icmpge L1
aload 0
lload 3
iload 2
bipush 8
imul
lushr
l2i
i2b
invokevirtual java/io/OutputStream/write(I)V
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 6
.limit stack 5
.end method
