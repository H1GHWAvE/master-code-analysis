.bytecode 50.0
.class public synchronized org/tukaani/xz/common/DecoderUtil
.super org/tukaani/xz/common/Util

.method public <init>()V
aload 0
invokespecial org/tukaani/xz/common/Util/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static areStreamFlagsEqual(Lorg/tukaani/xz/common/StreamFlags;Lorg/tukaani/xz/common/StreamFlags;)Z
aload 0
getfield org/tukaani/xz/common/StreamFlags/checkType I
aload 1
getfield org/tukaani/xz/common/StreamFlags/checkType I
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static decodeStreamFlags([BI)Lorg/tukaani/xz/common/StreamFlags;
.throws org/tukaani/xz/UnsupportedOptionsException
aload 0
iload 1
baload
ifne L0
aload 0
iload 1
iconst_1
iadd
baload
sipush 255
iand
bipush 16
if_icmplt L1
L0:
new org/tukaani/xz/UnsupportedOptionsException
dup
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>()V
athrow
L1:
new org/tukaani/xz/common/StreamFlags
dup
invokespecial org/tukaani/xz/common/StreamFlags/<init>()V
astore 2
aload 2
aload 0
iload 1
iconst_1
iadd
baload
putfield org/tukaani/xz/common/StreamFlags/checkType I
aload 2
areturn
.limit locals 3
.limit stack 4
.end method

.method public static decodeStreamFooter([B)Lorg/tukaani/xz/common/StreamFlags;
.throws java/io/IOException
.catch org/tukaani/xz/UnsupportedOptionsException from L0 to L1 using L2
aload 0
bipush 10
baload
getstatic org/tukaani/xz/XZ/FOOTER_MAGIC [B
iconst_0
baload
if_icmpne L3
aload 0
bipush 11
baload
getstatic org/tukaani/xz/XZ/FOOTER_MAGIC [B
iconst_1
baload
if_icmpeq L4
L3:
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Stream Footer is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 0
iconst_4
bipush 6
iconst_0
invokestatic org/tukaani/xz/common/DecoderUtil/isCRC32Valid([BIII)Z
ifne L0
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Stream Footer is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
bipush 8
invokestatic org/tukaani/xz/common/DecoderUtil/decodeStreamFlags([BI)Lorg/tukaani/xz/common/StreamFlags;
astore 2
L1:
aload 2
lconst_0
putfield org/tukaani/xz/common/StreamFlags/backwardSize J
iconst_0
istore 1
L5:
iload 1
iconst_4
if_icmpge L6
aload 2
aload 2
getfield org/tukaani/xz/common/StreamFlags/backwardSize J
aload 0
iload 1
iconst_4
iadd
baload
sipush 255
iand
iload 1
bipush 8
imul
ishl
i2l
lor
putfield org/tukaani/xz/common/StreamFlags/backwardSize J
iload 1
iconst_1
iadd
istore 1
goto L5
L2:
astore 0
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "Unsupported options in XZ Stream Footer"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L6:
aload 2
aload 2
getfield org/tukaani/xz/common/StreamFlags/backwardSize J
lconst_1
ladd
ldc2_w 4L
lmul
putfield org/tukaani/xz/common/StreamFlags/backwardSize J
aload 2
areturn
.limit locals 3
.limit stack 6
.end method

.method public static decodeStreamHeader([B)Lorg/tukaani/xz/common/StreamFlags;
.throws java/io/IOException
.catch org/tukaani/xz/UnsupportedOptionsException from L0 to L1 using L2
iconst_0
istore 1
L3:
iload 1
getstatic org/tukaani/xz/XZ/HEADER_MAGIC [B
arraylength
if_icmpge L4
aload 0
iload 1
baload
getstatic org/tukaani/xz/XZ/HEADER_MAGIC [B
iload 1
baload
if_icmpeq L5
new org/tukaani/xz/XZFormatException
dup
invokespecial org/tukaani/xz/XZFormatException/<init>()V
athrow
L5:
iload 1
iconst_1
iadd
istore 1
goto L3
L4:
aload 0
getstatic org/tukaani/xz/XZ/HEADER_MAGIC [B
arraylength
iconst_2
getstatic org/tukaani/xz/XZ/HEADER_MAGIC [B
arraylength
iconst_2
iadd
invokestatic org/tukaani/xz/common/DecoderUtil/isCRC32Valid([BIII)Z
ifne L0
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Stream Header is corrupt"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getstatic org/tukaani/xz/XZ/HEADER_MAGIC [B
arraylength
invokestatic org/tukaani/xz/common/DecoderUtil/decodeStreamFlags([BI)Lorg/tukaani/xz/common/StreamFlags;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "Unsupported options in XZ Stream Header"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 5
.end method

.method public static decodeVLI(Ljava/io/InputStream;)J
.throws java/io/IOException
aload 0
invokevirtual java/io/InputStream/read()I
istore 1
iload 1
iconst_m1
if_icmpne L0
new java/io/EOFException
dup
invokespecial java/io/EOFException/<init>()V
athrow
L0:
iload 1
bipush 127
iand
i2l
lstore 3
iconst_0
istore 2
L1:
iload 1
sipush 128
iand
ifeq L2
iload 2
iconst_1
iadd
istore 2
iload 2
bipush 9
if_icmplt L3
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L3:
aload 0
invokevirtual java/io/InputStream/read()I
istore 1
iload 1
iconst_m1
if_icmpne L4
new java/io/EOFException
dup
invokespecial java/io/EOFException/<init>()V
athrow
L4:
iload 1
ifne L5
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L5:
lload 3
iload 1
bipush 127
iand
i2l
iload 2
bipush 7
imul
lshl
lor
lstore 3
goto L1
L2:
lload 3
lreturn
.limit locals 5
.limit stack 6
.end method

.method public static isCRC32Valid([BIII)Z
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 6
aload 6
aload 0
iload 1
iload 2
invokevirtual java/util/zip/CRC32/update([BII)V
aload 6
invokevirtual java/util/zip/CRC32/getValue()J
lstore 4
iconst_0
istore 1
L0:
iload 1
iconst_4
if_icmpge L1
lload 4
iload 1
bipush 8
imul
lushr
l2i
i2b
aload 0
iload 3
iload 1
iadd
baload
if_icmpeq L2
iconst_0
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iconst_1
ireturn
.limit locals 7
.limit stack 4
.end method
