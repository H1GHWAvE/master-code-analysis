.bytecode 50.0
.class synchronized abstract org/tukaani/xz/BCJCoder
.super java/lang/Object
.implements org/tukaani/xz/FilterCoder

.field public static final 'ARMTHUMB_FILTER_ID' J = 8L


.field public static final 'ARM_FILTER_ID' J = 7L


.field public static final 'IA64_FILTER_ID' J = 6L


.field public static final 'POWERPC_FILTER_ID' J = 5L


.field public static final 'SPARC_FILTER_ID' J = 9L


.field public static final 'X86_FILTER_ID' J = 4L


.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static isBCJFilterID(J)Z
lload 0
ldc2_w 4L
lcmp
iflt L0
lload 0
ldc2_w 9L
lcmp
ifgt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method public changesSize()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public lastOK()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public nonLastOK()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method
