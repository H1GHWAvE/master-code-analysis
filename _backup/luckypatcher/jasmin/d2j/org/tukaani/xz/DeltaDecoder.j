.bytecode 50.0
.class synchronized org/tukaani/xz/DeltaDecoder
.super org/tukaani/xz/DeltaCoder
.implements org/tukaani/xz/FilterDecoder

.field private final 'distance' I

.method <init>([B)V
.throws org/tukaani/xz/UnsupportedOptionsException
aload 0
invokespecial org/tukaani/xz/DeltaCoder/<init>()V
aload 1
arraylength
iconst_1
if_icmpeq L0
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "Unsupported Delta filter properties"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
iconst_0
baload
sipush 255
iand
iconst_1
iadd
putfield org/tukaani/xz/DeltaDecoder/distance I
return
.limit locals 2
.limit stack 3
.end method

.method public getInputStream(Ljava/io/InputStream;)Ljava/io/InputStream;
new org/tukaani/xz/DeltaInputStream
dup
aload 1
aload 0
getfield org/tukaani/xz/DeltaDecoder/distance I
invokespecial org/tukaani/xz/DeltaInputStream/<init>(Ljava/io/InputStream;I)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public getMemoryUsage()I
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method
