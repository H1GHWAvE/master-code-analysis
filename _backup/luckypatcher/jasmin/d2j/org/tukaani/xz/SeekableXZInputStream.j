.bytecode 50.0
.class public synchronized org/tukaani/xz/SeekableXZInputStream
.super org/tukaani/xz/SeekableInputStream

.field static final synthetic '$assertionsDisabled' Z

.field private 'blockCount' I

.field private 'blockDecoder' Lorg/tukaani/xz/BlockInputStream;

.field private 'check' Lorg/tukaani/xz/check/Check;

.field private 'checkTypes' I

.field private final 'curBlockInfo' Lorg/tukaani/xz/index/BlockInfo;

.field private 'curPos' J

.field private 'endReached' Z

.field private 'exception' Ljava/io/IOException;

.field private 'in' Lorg/tukaani/xz/SeekableInputStream;

.field private 'indexMemoryUsage' I

.field private 'largestBlockSize' J

.field private final 'memoryLimit' I

.field private final 'queriedBlockInfo' Lorg/tukaani/xz/index/BlockInfo;

.field private 'seekNeeded' Z

.field private 'seekPos' J

.field private final 'streams' Ljava/util/ArrayList;

.field private final 'tempBuf' [B

.field private 'uncompressedSize' J

.method static <clinit>()V
ldc org/tukaani/xz/SeekableXZInputStream
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/SeekableXZInputStream/$assertionsDisabled Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/tukaani/xz/SeekableInputStream;)V
.throws java/io/IOException
aload 0
aload 1
iconst_m1
invokespecial org/tukaani/xz/SeekableXZInputStream/<init>(Lorg/tukaani/xz/SeekableInputStream;I)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Lorg/tukaani/xz/SeekableInputStream;I)V
.throws java/io/IOException
.catch org/tukaani/xz/MemoryLimitException from L0 to L1 using L2
aload 0
invokespecial org/tukaani/xz/SeekableInputStream/<init>()V
aload 0
iconst_0
putfield org/tukaani/xz/SeekableXZInputStream/indexMemoryUsage I
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield org/tukaani/xz/SeekableXZInputStream/streams Ljava/util/ArrayList;
aload 0
iconst_0
putfield org/tukaani/xz/SeekableXZInputStream/checkTypes I
aload 0
lconst_0
putfield org/tukaani/xz/SeekableXZInputStream/uncompressedSize J
aload 0
lconst_0
putfield org/tukaani/xz/SeekableXZInputStream/largestBlockSize J
aload 0
iconst_0
putfield org/tukaani/xz/SeekableXZInputStream/blockCount I
aload 0
aconst_null
putfield org/tukaani/xz/SeekableXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
aload 0
lconst_0
putfield org/tukaani/xz/SeekableXZInputStream/curPos J
aload 0
iconst_0
putfield org/tukaani/xz/SeekableXZInputStream/seekNeeded Z
aload 0
iconst_0
putfield org/tukaani/xz/SeekableXZInputStream/endReached Z
aload 0
aconst_null
putfield org/tukaani/xz/SeekableXZInputStream/exception Ljava/io/IOException;
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/SeekableXZInputStream/tempBuf [B
aload 0
aload 1
putfield org/tukaani/xz/SeekableXZInputStream/in Lorg/tukaani/xz/SeekableInputStream;
new java/io/DataInputStream
dup
aload 1
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
astore 8
aload 1
lconst_0
invokevirtual org/tukaani/xz/SeekableInputStream/seek(J)V
getstatic org/tukaani/xz/XZ/HEADER_MAGIC [B
arraylength
newarray byte
astore 9
aload 8
aload 9
invokevirtual java/io/DataInputStream/readFully([B)V
aload 9
getstatic org/tukaani/xz/XZ/HEADER_MAGIC [B
invokestatic java/util/Arrays/equals([B[B)Z
ifne L3
new org/tukaani/xz/XZFormatException
dup
invokespecial org/tukaani/xz/XZFormatException/<init>()V
athrow
L3:
aload 1
invokevirtual org/tukaani/xz/SeekableInputStream/length()J
lstore 6
ldc2_w 3L
lload 6
land
lconst_0
lcmp
ifeq L4
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ file size is not a multiple of 4 bytes"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L4:
bipush 12
newarray byte
astore 9
lconst_0
lstore 4
iload 2
istore 3
L5:
lload 6
lconst_0
lcmp
ifle L6
lload 6
ldc2_w 12L
lcmp
ifge L7
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L7:
aload 1
lload 6
ldc2_w 12L
lsub
invokevirtual org/tukaani/xz/SeekableInputStream/seek(J)V
aload 8
aload 9
invokevirtual java/io/DataInputStream/readFully([B)V
aload 9
bipush 8
baload
ifne L8
aload 9
bipush 9
baload
ifne L8
aload 9
bipush 10
baload
ifne L8
aload 9
bipush 11
baload
ifne L8
lload 4
ldc2_w 4L
ladd
lstore 4
lload 6
ldc2_w 4L
lsub
lstore 6
goto L5
L8:
lload 6
ldc2_w 12L
lsub
lstore 6
aload 9
invokestatic org/tukaani/xz/common/DecoderUtil/decodeStreamFooter([B)Lorg/tukaani/xz/common/StreamFlags;
astore 10
aload 10
getfield org/tukaani/xz/common/StreamFlags/backwardSize J
lload 6
lcmp
iflt L9
new org/tukaani/xz/CorruptedInputException
dup
ldc "Backward Size in XZ Stream Footer is too big"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L9:
aload 0
aload 10
getfield org/tukaani/xz/common/StreamFlags/checkType I
invokestatic org/tukaani/xz/check/Check/getInstance(I)Lorg/tukaani/xz/check/Check;
putfield org/tukaani/xz/SeekableXZInputStream/check Lorg/tukaani/xz/check/Check;
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/checkTypes I
iconst_1
aload 10
getfield org/tukaani/xz/common/StreamFlags/checkType I
ishl
ior
putfield org/tukaani/xz/SeekableXZInputStream/checkTypes I
aload 1
lload 6
aload 10
getfield org/tukaani/xz/common/StreamFlags/backwardSize J
lsub
invokevirtual org/tukaani/xz/SeekableInputStream/seek(J)V
L0:
new org/tukaani/xz/index/IndexDecoder
dup
aload 1
aload 10
lload 4
iload 3
invokespecial org/tukaani/xz/index/IndexDecoder/<init>(Lorg/tukaani/xz/SeekableInputStream;Lorg/tukaani/xz/common/StreamFlags;JI)V
astore 11
L1:
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/indexMemoryUsage I
aload 11
invokevirtual org/tukaani/xz/index/IndexDecoder/getMemoryUsage()I
iadd
putfield org/tukaani/xz/SeekableXZInputStream/indexMemoryUsage I
iload 3
istore 2
iload 3
iflt L10
iload 3
aload 11
invokevirtual org/tukaani/xz/index/IndexDecoder/getMemoryUsage()I
isub
istore 3
iload 3
istore 2
getstatic org/tukaani/xz/SeekableXZInputStream/$assertionsDisabled Z
ifne L10
iload 3
istore 2
iload 3
ifge L10
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L2:
astore 1
getstatic org/tukaani/xz/SeekableXZInputStream/$assertionsDisabled Z
ifne L11
iload 3
ifge L11
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L11:
new org/tukaani/xz/MemoryLimitException
dup
aload 1
invokevirtual org/tukaani/xz/MemoryLimitException/getMemoryNeeded()I
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/indexMemoryUsage I
iadd
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/indexMemoryUsage I
iload 3
iadd
invokespecial org/tukaani/xz/MemoryLimitException/<init>(II)V
athrow
L10:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/largestBlockSize J
aload 11
invokevirtual org/tukaani/xz/index/IndexDecoder/getLargestBlockSize()J
lcmp
ifge L12
aload 0
aload 11
invokevirtual org/tukaani/xz/index/IndexDecoder/getLargestBlockSize()J
putfield org/tukaani/xz/SeekableXZInputStream/largestBlockSize J
L12:
aload 11
invokevirtual org/tukaani/xz/index/IndexDecoder/getStreamSize()J
ldc2_w 12L
lsub
lstore 4
lload 6
lload 4
lcmp
ifge L13
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Index indicates too big compressed size for the XZ Stream"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L13:
lload 6
lload 4
lsub
lstore 6
aload 1
lload 6
invokevirtual org/tukaani/xz/SeekableInputStream/seek(J)V
aload 8
aload 9
invokevirtual java/io/DataInputStream/readFully([B)V
aload 9
invokestatic org/tukaani/xz/common/DecoderUtil/decodeStreamHeader([B)Lorg/tukaani/xz/common/StreamFlags;
aload 10
invokestatic org/tukaani/xz/common/DecoderUtil/areStreamFlagsEqual(Lorg/tukaani/xz/common/StreamFlags;Lorg/tukaani/xz/common/StreamFlags;)Z
ifne L14
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Stream Footer does not match Stream Header"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L14:
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/uncompressedSize J
aload 11
invokevirtual org/tukaani/xz/index/IndexDecoder/getUncompressedSize()J
ladd
putfield org/tukaani/xz/SeekableXZInputStream/uncompressedSize J
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/uncompressedSize J
lconst_0
lcmp
ifge L15
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "XZ file is too big"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L15:
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/blockCount I
aload 11
invokevirtual org/tukaani/xz/index/IndexDecoder/getRecordCount()I
iadd
putfield org/tukaani/xz/SeekableXZInputStream/blockCount I
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/blockCount I
ifge L16
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "XZ file has over 2147483647 Blocks"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L16:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/streams Ljava/util/ArrayList;
aload 11
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
lconst_0
lstore 4
iload 2
istore 3
goto L5
L6:
getstatic org/tukaani/xz/SeekableXZInputStream/$assertionsDisabled Z
ifne L17
lload 6
lconst_0
lcmp
ifeq L17
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L17:
aload 0
iload 3
putfield org/tukaani/xz/SeekableXZInputStream/memoryLimit I
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/streams Ljava/util/ArrayList;
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/streams Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast org/tukaani/xz/index/IndexDecoder
astore 1
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/streams Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_2
isub
istore 2
L18:
iload 2
iflt L19
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/streams Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast org/tukaani/xz/index/IndexDecoder
astore 8
aload 8
aload 1
invokevirtual org/tukaani/xz/index/IndexDecoder/setOffsets(Lorg/tukaani/xz/index/IndexDecoder;)V
aload 8
astore 1
iload 2
iconst_1
isub
istore 2
goto L18
L19:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/streams Ljava/util/ArrayList;
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/streams Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast org/tukaani/xz/index/IndexDecoder
astore 1
aload 0
new org/tukaani/xz/index/BlockInfo
dup
aload 1
invokespecial org/tukaani/xz/index/BlockInfo/<init>(Lorg/tukaani/xz/index/IndexDecoder;)V
putfield org/tukaani/xz/SeekableXZInputStream/curBlockInfo Lorg/tukaani/xz/index/BlockInfo;
aload 0
new org/tukaani/xz/index/BlockInfo
dup
aload 1
invokespecial org/tukaani/xz/index/BlockInfo/<init>(Lorg/tukaani/xz/index/IndexDecoder;)V
putfield org/tukaani/xz/SeekableXZInputStream/queriedBlockInfo Lorg/tukaani/xz/index/BlockInfo;
return
.limit locals 12
.limit stack 7
.end method

.method private initBlockDecoder()V
.throws java/io/IOException
.catch org/tukaani/xz/MemoryLimitException from L0 to L1 using L2
.catch org/tukaani/xz/IndexIndicatorException from L0 to L1 using L3
L0:
aload 0
aconst_null
putfield org/tukaani/xz/SeekableXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
aload 0
new org/tukaani/xz/BlockInputStream
dup
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/in Lorg/tukaani/xz/SeekableInputStream;
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/check Lorg/tukaani/xz/check/Check;
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/memoryLimit I
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curBlockInfo Lorg/tukaani/xz/index/BlockInfo;
getfield org/tukaani/xz/index/BlockInfo/unpaddedSize J
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curBlockInfo Lorg/tukaani/xz/index/BlockInfo;
getfield org/tukaani/xz/index/BlockInfo/uncompressedSize J
invokespecial org/tukaani/xz/BlockInputStream/<init>(Ljava/io/InputStream;Lorg/tukaani/xz/check/Check;IJJ)V
putfield org/tukaani/xz/SeekableXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
L1:
return
L2:
astore 1
getstatic org/tukaani/xz/SeekableXZInputStream/$assertionsDisabled Z
ifne L4
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/memoryLimit I
ifge L4
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L4:
new org/tukaani/xz/MemoryLimitException
dup
aload 1
invokevirtual org/tukaani/xz/MemoryLimitException/getMemoryNeeded()I
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/indexMemoryUsage I
iadd
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/memoryLimit I
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/indexMemoryUsage I
iadd
invokespecial org/tukaani/xz/MemoryLimitException/<init>(II)V
athrow
L3:
astore 1
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
.limit locals 2
.limit stack 10
.end method

.method private locateBlockByNumber(Lorg/tukaani/xz/index/BlockInfo;I)V
iload 2
iflt L0
iload 2
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/blockCount I
if_icmplt L1
L0:
new java/lang/IndexOutOfBoundsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Invalid XZ Block number: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IndexOutOfBoundsException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
getfield org/tukaani/xz/index/BlockInfo/blockNumber I
iload 2
if_icmpne L2
return
L2:
iconst_0
istore 3
L3:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/streams Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast org/tukaani/xz/index/IndexDecoder
astore 4
aload 4
iload 2
invokevirtual org/tukaani/xz/index/IndexDecoder/hasRecord(I)Z
ifeq L4
aload 4
aload 1
iload 2
invokevirtual org/tukaani/xz/index/IndexDecoder/setBlockInfo(Lorg/tukaani/xz/index/BlockInfo;I)V
return
L4:
iload 3
iconst_1
iadd
istore 3
goto L3
.limit locals 5
.limit stack 4
.end method

.method private locateBlockByPos(Lorg/tukaani/xz/index/BlockInfo;J)V
lload 2
lconst_0
lcmp
iflt L0
lload 2
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/uncompressedSize J
lcmp
iflt L1
L0:
new java/lang/IndexOutOfBoundsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Invalid uncompressed position: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 2
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IndexOutOfBoundsException/<init>(Ljava/lang/String;)V
athrow
L1:
iconst_0
istore 4
L2:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/streams Ljava/util/ArrayList;
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast org/tukaani/xz/index/IndexDecoder
astore 5
aload 5
lload 2
invokevirtual org/tukaani/xz/index/IndexDecoder/hasUncompressedOffset(J)Z
ifeq L3
aload 5
aload 1
lload 2
invokevirtual org/tukaani/xz/index/IndexDecoder/locateBlock(Lorg/tukaani/xz/index/BlockInfo;J)V
getstatic org/tukaani/xz/SeekableXZInputStream/$assertionsDisabled Z
ifne L4
aload 1
getfield org/tukaani/xz/index/BlockInfo/compressedOffset J
ldc2_w 3L
land
lconst_0
lcmp
ifeq L4
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L3:
iload 4
iconst_1
iadd
istore 4
goto L2
L4:
getstatic org/tukaani/xz/SeekableXZInputStream/$assertionsDisabled Z
ifne L5
aload 1
getfield org/tukaani/xz/index/BlockInfo/uncompressedSize J
lconst_0
lcmp
ifgt L5
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L5:
getstatic org/tukaani/xz/SeekableXZInputStream/$assertionsDisabled Z
ifne L6
lload 2
aload 1
getfield org/tukaani/xz/index/BlockInfo/uncompressedOffset J
lcmp
ifge L6
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L6:
getstatic org/tukaani/xz/SeekableXZInputStream/$assertionsDisabled Z
ifne L7
lload 2
aload 1
getfield org/tukaani/xz/index/BlockInfo/uncompressedOffset J
aload 1
getfield org/tukaani/xz/index/BlockInfo/uncompressedSize J
ladd
lcmp
iflt L7
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L7:
return
.limit locals 6
.limit stack 6
.end method

.method private seek()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/seekNeeded Z
ifne L0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curBlockInfo Lorg/tukaani/xz/index/BlockInfo;
invokevirtual org/tukaani/xz/index/BlockInfo/hasNext()Z
ifeq L1
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curBlockInfo Lorg/tukaani/xz/index/BlockInfo;
invokevirtual org/tukaani/xz/index/BlockInfo/setNext()V
aload 0
invokespecial org/tukaani/xz/SeekableXZInputStream/initBlockDecoder()V
L2:
return
L1:
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curPos J
putfield org/tukaani/xz/SeekableXZInputStream/seekPos J
L0:
aload 0
iconst_0
putfield org/tukaani/xz/SeekableXZInputStream/seekNeeded Z
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/seekPos J
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/uncompressedSize J
lcmp
iflt L3
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/seekPos J
putfield org/tukaani/xz/SeekableXZInputStream/curPos J
aload 0
aconst_null
putfield org/tukaani/xz/SeekableXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
aload 0
iconst_1
putfield org/tukaani/xz/SeekableXZInputStream/endReached Z
return
L3:
aload 0
iconst_0
putfield org/tukaani/xz/SeekableXZInputStream/endReached Z
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curBlockInfo Lorg/tukaani/xz/index/BlockInfo;
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/seekPos J
invokespecial org/tukaani/xz/SeekableXZInputStream/locateBlockByPos(Lorg/tukaani/xz/index/BlockInfo;J)V
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curPos J
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curBlockInfo Lorg/tukaani/xz/index/BlockInfo;
getfield org/tukaani/xz/index/BlockInfo/uncompressedOffset J
lcmp
ifle L4
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curPos J
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/seekPos J
lcmp
ifle L5
L4:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/in Lorg/tukaani/xz/SeekableInputStream;
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curBlockInfo Lorg/tukaani/xz/index/BlockInfo;
getfield org/tukaani/xz/index/BlockInfo/compressedOffset J
invokevirtual org/tukaani/xz/SeekableInputStream/seek(J)V
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curBlockInfo Lorg/tukaani/xz/index/BlockInfo;
invokevirtual org/tukaani/xz/index/BlockInfo/getCheckType()I
invokestatic org/tukaani/xz/check/Check/getInstance(I)Lorg/tukaani/xz/check/Check;
putfield org/tukaani/xz/SeekableXZInputStream/check Lorg/tukaani/xz/check/Check;
aload 0
invokespecial org/tukaani/xz/SeekableXZInputStream/initBlockDecoder()V
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curBlockInfo Lorg/tukaani/xz/index/BlockInfo;
getfield org/tukaani/xz/index/BlockInfo/uncompressedOffset J
putfield org/tukaani/xz/SeekableXZInputStream/curPos J
L5:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/seekPos J
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curPos J
lcmp
ifle L2
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/seekPos J
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curPos J
lsub
lstore 1
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
lload 1
invokevirtual org/tukaani/xz/BlockInputStream/skip(J)J
lload 1
lcmp
ifeq L6
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L6:
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/seekPos J
putfield org/tukaani/xz/SeekableXZInputStream/curPos J
return
.limit locals 3
.limit stack 4
.end method

.method public available()I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/in Lorg/tukaani/xz/SeekableInputStream;
ifnonnull L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/exception Ljava/io/IOException;
ifnull L1
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/exception Ljava/io/IOException;
athrow
L1:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/endReached Z
ifne L2
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/seekNeeded Z
ifne L2
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
ifnonnull L3
L2:
iconst_0
ireturn
L3:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
invokevirtual org/tukaani/xz/BlockInputStream/available()I
ireturn
.limit locals 1
.limit stack 3
.end method

.method public close()V
.throws java/io/IOException
.catch all from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/in Lorg/tukaani/xz/SeekableInputStream;
ifnull L3
L0:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/in Lorg/tukaani/xz/SeekableInputStream;
invokevirtual org/tukaani/xz/SeekableInputStream/close()V
L1:
aload 0
aconst_null
putfield org/tukaani/xz/SeekableXZInputStream/in Lorg/tukaani/xz/SeekableInputStream;
L3:
return
L2:
astore 1
aload 0
aconst_null
putfield org/tukaani/xz/SeekableXZInputStream/in Lorg/tukaani/xz/SeekableInputStream;
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public getBlockCheckType(I)I
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/queriedBlockInfo Lorg/tukaani/xz/index/BlockInfo;
iload 1
invokespecial org/tukaani/xz/SeekableXZInputStream/locateBlockByNumber(Lorg/tukaani/xz/index/BlockInfo;I)V
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/queriedBlockInfo Lorg/tukaani/xz/index/BlockInfo;
invokevirtual org/tukaani/xz/index/BlockInfo/getCheckType()I
ireturn
.limit locals 2
.limit stack 3
.end method

.method public getBlockCompPos(I)J
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/queriedBlockInfo Lorg/tukaani/xz/index/BlockInfo;
iload 1
invokespecial org/tukaani/xz/SeekableXZInputStream/locateBlockByNumber(Lorg/tukaani/xz/index/BlockInfo;I)V
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/queriedBlockInfo Lorg/tukaani/xz/index/BlockInfo;
getfield org/tukaani/xz/index/BlockInfo/compressedOffset J
lreturn
.limit locals 2
.limit stack 3
.end method

.method public getBlockCompSize(I)J
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/queriedBlockInfo Lorg/tukaani/xz/index/BlockInfo;
iload 1
invokespecial org/tukaani/xz/SeekableXZInputStream/locateBlockByNumber(Lorg/tukaani/xz/index/BlockInfo;I)V
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/queriedBlockInfo Lorg/tukaani/xz/index/BlockInfo;
getfield org/tukaani/xz/index/BlockInfo/unpaddedSize J
ldc2_w 3L
ladd
ldc2_w -4L
land
lreturn
.limit locals 2
.limit stack 4
.end method

.method public getBlockCount()I
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/blockCount I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getBlockNumber(J)I
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/queriedBlockInfo Lorg/tukaani/xz/index/BlockInfo;
lload 1
invokespecial org/tukaani/xz/SeekableXZInputStream/locateBlockByPos(Lorg/tukaani/xz/index/BlockInfo;J)V
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/queriedBlockInfo Lorg/tukaani/xz/index/BlockInfo;
getfield org/tukaani/xz/index/BlockInfo/blockNumber I
ireturn
.limit locals 3
.limit stack 4
.end method

.method public getBlockPos(I)J
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/queriedBlockInfo Lorg/tukaani/xz/index/BlockInfo;
iload 1
invokespecial org/tukaani/xz/SeekableXZInputStream/locateBlockByNumber(Lorg/tukaani/xz/index/BlockInfo;I)V
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/queriedBlockInfo Lorg/tukaani/xz/index/BlockInfo;
getfield org/tukaani/xz/index/BlockInfo/uncompressedOffset J
lreturn
.limit locals 2
.limit stack 3
.end method

.method public getBlockSize(I)J
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/queriedBlockInfo Lorg/tukaani/xz/index/BlockInfo;
iload 1
invokespecial org/tukaani/xz/SeekableXZInputStream/locateBlockByNumber(Lorg/tukaani/xz/index/BlockInfo;I)V
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/queriedBlockInfo Lorg/tukaani/xz/index/BlockInfo;
getfield org/tukaani/xz/index/BlockInfo/uncompressedSize J
lreturn
.limit locals 2
.limit stack 3
.end method

.method public getCheckTypes()I
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/checkTypes I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getIndexMemoryUsage()I
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/indexMemoryUsage I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getLargestBlockSize()J
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/largestBlockSize J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getStreamCount()I
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/streams Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public length()J
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/uncompressedSize J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public position()J
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/in Lorg/tukaani/xz/SeekableInputStream;
ifnonnull L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/seekNeeded Z
ifeq L1
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/seekPos J
lreturn
L1:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curPos J
lreturn
.limit locals 1
.limit stack 3
.end method

.method public read()I
.throws java/io/IOException
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/tempBuf [B
iconst_0
iconst_1
invokevirtual org/tukaani/xz/SeekableXZInputStream/read([BII)I
iconst_m1
if_icmpne L0
iconst_m1
ireturn
L0:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/tempBuf [B
iconst_0
baload
sipush 255
iand
ireturn
.limit locals 1
.limit stack 4
.end method

.method public read([BII)I
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
.catch java/io/IOException from L7 to L8 using L2
.catch java/io/IOException from L9 to L10 using L2
.catch java/io/IOException from L11 to L12 using L2
.catch java/io/IOException from L13 to L14 using L2
.catch java/io/IOException from L15 to L16 using L2
.catch java/io/IOException from L17 to L18 using L2
iload 2
iflt L19
iload 3
iflt L19
iload 2
iload 3
iadd
iflt L19
iload 2
iload 3
iadd
aload 1
arraylength
if_icmple L20
L19:
new java/lang/IndexOutOfBoundsException
dup
invokespecial java/lang/IndexOutOfBoundsException/<init>()V
athrow
L20:
iload 3
ifne L21
iconst_0
istore 2
L22:
iload 2
ireturn
L21:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/in Lorg/tukaani/xz/SeekableInputStream;
ifnonnull L23
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L23:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/exception Ljava/io/IOException;
ifnull L24
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/exception Ljava/io/IOException;
athrow
L24:
iconst_0
istore 7
iconst_0
istore 4
iload 7
istore 5
L0:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/seekNeeded Z
ifeq L4
L1:
iload 7
istore 5
L3:
aload 0
invokespecial org/tukaani/xz/SeekableXZInputStream/seek()V
L4:
iload 2
istore 6
iload 7
istore 5
L5:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/endReached Z
ifeq L25
L6:
iconst_m1
ireturn
L26:
iload 4
istore 5
L7:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
aload 1
iload 6
iload 3
invokevirtual org/tukaani/xz/BlockInputStream/read([BII)I
istore 2
L8:
iload 2
ifle L27
iload 4
istore 5
L9:
aload 0
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/curPos J
iload 2
i2l
ladd
putfield org/tukaani/xz/SeekableXZInputStream/curPos J
L10:
iload 4
iload 2
iadd
istore 4
iload 6
iload 2
iadd
istore 6
iload 3
iload 2
isub
istore 3
L25:
iload 4
istore 2
iload 3
ifle L22
iload 4
istore 5
L11:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
ifnonnull L26
L12:
iload 4
istore 5
L13:
aload 0
invokespecial org/tukaani/xz/SeekableXZInputStream/seek()V
L14:
iload 4
istore 5
L15:
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/endReached Z
ifeq L26
L16:
iload 4
ireturn
L27:
iload 2
iconst_m1
if_icmpne L25
iload 4
istore 5
L17:
aload 0
aconst_null
putfield org/tukaani/xz/SeekableXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
L18:
goto L25
L2:
astore 8
aload 8
astore 1
aload 8
instanceof java/io/EOFException
ifeq L28
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
astore 1
L28:
aload 0
aload 1
putfield org/tukaani/xz/SeekableXZInputStream/exception Ljava/io/IOException;
iload 5
istore 2
iload 5
ifne L22
aload 1
athrow
.limit locals 9
.limit stack 5
.end method

.method public seek(J)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/in Lorg/tukaani/xz/SeekableInputStream;
ifnonnull L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
lload 1
lconst_0
lcmp
ifge L1
new org/tukaani/xz/XZIOException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Negative seek position: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
lload 1
putfield org/tukaani/xz/SeekableXZInputStream/seekPos J
aload 0
iconst_1
putfield org/tukaani/xz/SeekableXZInputStream/seekNeeded Z
return
.limit locals 3
.limit stack 5
.end method

.method public seekToBlock(I)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/in Lorg/tukaani/xz/SeekableInputStream;
ifnonnull L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 1
iflt L1
iload 1
aload 0
getfield org/tukaani/xz/SeekableXZInputStream/blockCount I
if_icmplt L2
L1:
new org/tukaani/xz/XZIOException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Invalid XZ Block number: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
aload 0
iload 1
invokevirtual org/tukaani/xz/SeekableXZInputStream/getBlockPos(I)J
putfield org/tukaani/xz/SeekableXZInputStream/seekPos J
aload 0
iconst_1
putfield org/tukaani/xz/SeekableXZInputStream/seekNeeded Z
return
.limit locals 2
.limit stack 4
.end method
