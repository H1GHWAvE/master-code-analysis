.bytecode 50.0
.class public synchronized org/tukaani/xz/SeekableFileInputStream
.super org/tukaani/xz/SeekableInputStream

.field protected 'randomAccessFile' Ljava/io/RandomAccessFile;

.method public <init>(Ljava/io/File;)V
.throws java/io/FileNotFoundException
aload 0
invokespecial org/tukaani/xz/SeekableInputStream/<init>()V
aload 0
new java/io/RandomAccessFile
dup
aload 1
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
putfield org/tukaani/xz/SeekableFileInputStream/randomAccessFile Ljava/io/RandomAccessFile;
return
.limit locals 2
.limit stack 5
.end method

.method public <init>(Ljava/io/RandomAccessFile;)V
aload 0
invokespecial org/tukaani/xz/SeekableInputStream/<init>()V
aload 0
aload 1
putfield org/tukaani/xz/SeekableFileInputStream/randomAccessFile Ljava/io/RandomAccessFile;
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Ljava/lang/String;)V
.throws java/io/FileNotFoundException
aload 0
invokespecial org/tukaani/xz/SeekableInputStream/<init>()V
aload 0
new java/io/RandomAccessFile
dup
aload 1
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/lang/String;Ljava/lang/String;)V
putfield org/tukaani/xz/SeekableFileInputStream/randomAccessFile Ljava/io/RandomAccessFile;
return
.limit locals 2
.limit stack 5
.end method

.method public close()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SeekableFileInputStream/randomAccessFile Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/close()V
return
.limit locals 1
.limit stack 1
.end method

.method public length()J
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SeekableFileInputStream/randomAccessFile Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/length()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public position()J
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SeekableFileInputStream/randomAccessFile Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/getFilePointer()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public read()I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SeekableFileInputStream/randomAccessFile Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/read()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public read([B)I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SeekableFileInputStream/randomAccessFile Ljava/io/RandomAccessFile;
aload 1
invokevirtual java/io/RandomAccessFile/read([B)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public read([BII)I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SeekableFileInputStream/randomAccessFile Ljava/io/RandomAccessFile;
aload 1
iload 2
iload 3
invokevirtual java/io/RandomAccessFile/read([BII)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method public seek(J)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SeekableFileInputStream/randomAccessFile Ljava/io/RandomAccessFile;
lload 1
invokevirtual java/io/RandomAccessFile/seek(J)V
return
.limit locals 3
.limit stack 3
.end method
