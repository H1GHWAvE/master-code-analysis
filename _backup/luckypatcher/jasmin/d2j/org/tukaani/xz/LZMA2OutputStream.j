.bytecode 50.0
.class synchronized org/tukaani/xz/LZMA2OutputStream
.super org/tukaani/xz/FinishableOutputStream

.field static final synthetic '$assertionsDisabled' Z

.field static final 'COMPRESSED_SIZE_MAX' I = 65536


.field private 'dictResetNeeded' Z

.field private 'exception' Ljava/io/IOException;

.field private 'finished' Z

.field private final 'lz' Lorg/tukaani/xz/lz/LZEncoder;

.field private final 'lzma' Lorg/tukaani/xz/lzma/LZMAEncoder;

.field private 'out' Lorg/tukaani/xz/FinishableOutputStream;

.field private final 'outData' Ljava/io/DataOutputStream;

.field private 'pendingSize' I

.field private final 'props' I

.field private 'propsNeeded' Z

.field private final 'rc' Lorg/tukaani/xz/rangecoder/RangeEncoder;

.field private 'stateResetNeeded' Z

.field private final 'tempBuf' [B

.method static <clinit>()V
ldc org/tukaani/xz/LZMA2OutputStream
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/LZMA2OutputStream/$assertionsDisabled Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method <init>(Lorg/tukaani/xz/FinishableOutputStream;Lorg/tukaani/xz/LZMA2Options;)V
aload 0
invokespecial org/tukaani/xz/FinishableOutputStream/<init>()V
aload 0
iconst_1
putfield org/tukaani/xz/LZMA2OutputStream/dictResetNeeded Z
aload 0
iconst_1
putfield org/tukaani/xz/LZMA2OutputStream/stateResetNeeded Z
aload 0
iconst_1
putfield org/tukaani/xz/LZMA2OutputStream/propsNeeded Z
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2OutputStream/pendingSize I
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2OutputStream/finished Z
aload 0
aconst_null
putfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/LZMA2OutputStream/tempBuf [B
aload 1
ifnonnull L0
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L0:
aload 0
aload 1
putfield org/tukaani/xz/LZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
aload 0
new java/io/DataOutputStream
dup
aload 1
invokespecial java/io/DataOutputStream/<init>(Ljava/io/OutputStream;)V
putfield org/tukaani/xz/LZMA2OutputStream/outData Ljava/io/DataOutputStream;
aload 0
new org/tukaani/xz/rangecoder/RangeEncoder
dup
ldc_w 65536
invokespecial org/tukaani/xz/rangecoder/RangeEncoder/<init>(I)V
putfield org/tukaani/xz/LZMA2OutputStream/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 2
invokevirtual org/tukaani/xz/LZMA2Options/getDictSize()I
istore 3
iload 3
invokestatic org/tukaani/xz/LZMA2OutputStream/getExtraSizeBefore(I)I
istore 4
aload 0
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 2
invokevirtual org/tukaani/xz/LZMA2Options/getLc()I
aload 2
invokevirtual org/tukaani/xz/LZMA2Options/getLp()I
aload 2
invokevirtual org/tukaani/xz/LZMA2Options/getPb()I
aload 2
invokevirtual org/tukaani/xz/LZMA2Options/getMode()I
iload 3
iload 4
aload 2
invokevirtual org/tukaani/xz/LZMA2Options/getNiceLen()I
aload 2
invokevirtual org/tukaani/xz/LZMA2Options/getMatchFinder()I
aload 2
invokevirtual org/tukaani/xz/LZMA2Options/getDepthLimit()I
invokestatic org/tukaani/xz/lzma/LZMAEncoder/getInstance(Lorg/tukaani/xz/rangecoder/RangeEncoder;IIIIIIIII)Lorg/tukaani/xz/lzma/LZMAEncoder;
putfield org/tukaani/xz/LZMA2OutputStream/lzma Lorg/tukaani/xz/lzma/LZMAEncoder;
aload 0
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/lzma Lorg/tukaani/xz/lzma/LZMAEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder/getLZEncoder()Lorg/tukaani/xz/lz/LZEncoder;
putfield org/tukaani/xz/LZMA2OutputStream/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 2
invokevirtual org/tukaani/xz/LZMA2Options/getPresetDict()[B
astore 1
aload 1
ifnull L1
aload 1
arraylength
ifle L1
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/lz Lorg/tukaani/xz/lz/LZEncoder;
iload 3
aload 1
invokevirtual org/tukaani/xz/lz/LZEncoder/setPresetDict(I[B)V
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2OutputStream/dictResetNeeded Z
L1:
aload 0
aload 2
invokevirtual org/tukaani/xz/LZMA2Options/getPb()I
iconst_5
imul
aload 2
invokevirtual org/tukaani/xz/LZMA2Options/getLp()I
iadd
bipush 9
imul
aload 2
invokevirtual org/tukaani/xz/LZMA2Options/getLc()I
iadd
putfield org/tukaani/xz/LZMA2OutputStream/props I
return
.limit locals 5
.limit stack 11
.end method

.method private static getExtraSizeBefore(I)I
ldc_w 65536
iload 0
if_icmple L0
ldc_w 65536
iload 0
isub
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method static getMemoryUsage(Lorg/tukaani/xz/LZMA2Options;)I
aload 0
invokevirtual org/tukaani/xz/LZMA2Options/getDictSize()I
istore 1
iload 1
invokestatic org/tukaani/xz/LZMA2OutputStream/getExtraSizeBefore(I)I
istore 2
aload 0
invokevirtual org/tukaani/xz/LZMA2Options/getMode()I
iload 1
iload 2
aload 0
invokevirtual org/tukaani/xz/LZMA2Options/getMatchFinder()I
invokestatic org/tukaani/xz/lzma/LZMAEncoder/getMemoryUsage(IIII)I
bipush 70
iadd
ireturn
.limit locals 3
.limit stack 4
.end method

.method private writeChunk()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/finish()I
istore 2
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/lzma Lorg/tukaani/xz/lzma/LZMAEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder/getUncompressedSize()I
istore 1
getstatic org/tukaani/xz/LZMA2OutputStream/$assertionsDisabled Z
ifne L0
iload 2
ifgt L0
new java/lang/AssertionError
dup
iload 2
invokespecial java/lang/AssertionError/<init>(I)V
athrow
L0:
getstatic org/tukaani/xz/LZMA2OutputStream/$assertionsDisabled Z
ifne L1
iload 1
ifgt L1
new java/lang/AssertionError
dup
iload 1
invokespecial java/lang/AssertionError/<init>(I)V
athrow
L1:
iload 2
iconst_2
iadd
iload 1
if_icmpge L2
aload 0
iload 1
iload 2
invokespecial org/tukaani/xz/LZMA2OutputStream/writeLZMA(II)V
L3:
aload 0
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/pendingSize I
iload 1
isub
putfield org/tukaani/xz/LZMA2OutputStream/pendingSize I
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/lzma Lorg/tukaani/xz/lzma/LZMAEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder/resetUncompressedSize()V
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/reset()V
return
L2:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/lzma Lorg/tukaani/xz/lzma/LZMAEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder/reset()V
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/lzma Lorg/tukaani/xz/lzma/LZMAEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder/getUncompressedSize()I
istore 1
getstatic org/tukaani/xz/LZMA2OutputStream/$assertionsDisabled Z
ifne L4
iload 1
ifgt L4
new java/lang/AssertionError
dup
iload 1
invokespecial java/lang/AssertionError/<init>(I)V
athrow
L4:
aload 0
iload 1
invokespecial org/tukaani/xz/LZMA2OutputStream/writeUncompressed(I)V
goto L3
.limit locals 3
.limit stack 3
.end method

.method private writeEndMarker()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
getstatic org/tukaani/xz/LZMA2OutputStream/$assertionsDisabled Z
ifne L5
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/finished Z
ifeq L5
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L5:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
ifnull L6
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
athrow
L6:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/lz Lorg/tukaani/xz/lz/LZEncoder;
invokevirtual org/tukaani/xz/lz/LZEncoder/setFinishing()V
L0:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/pendingSize I
ifle L3
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/lzma Lorg/tukaani/xz/lzma/LZMAEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder/encodeForLZMA2()Z
pop
aload 0
invokespecial org/tukaani/xz/LZMA2OutputStream/writeChunk()V
L1:
goto L0
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
aload 1
athrow
L3:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
iconst_0
invokevirtual org/tukaani/xz/FinishableOutputStream/write(I)V
L4:
aload 0
iconst_1
putfield org/tukaani/xz/LZMA2OutputStream/finished Z
return
.limit locals 2
.limit stack 2
.end method

.method private writeLZMA(II)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/propsNeeded Z
ifeq L0
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/dictResetNeeded Z
ifeq L1
sipush 224
istore 3
L2:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/outData Ljava/io/DataOutputStream;
iload 3
iload 1
iconst_1
isub
bipush 16
iushr
ior
invokevirtual java/io/DataOutputStream/writeByte(I)V
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/outData Ljava/io/DataOutputStream;
iload 1
iconst_1
isub
invokevirtual java/io/DataOutputStream/writeShort(I)V
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/outData Ljava/io/DataOutputStream;
iload 2
iconst_1
isub
invokevirtual java/io/DataOutputStream/writeShort(I)V
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/propsNeeded Z
ifeq L3
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/outData Ljava/io/DataOutputStream;
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/props I
invokevirtual java/io/DataOutputStream/writeByte(I)V
L3:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/write(Ljava/io/OutputStream;)V
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2OutputStream/propsNeeded Z
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2OutputStream/stateResetNeeded Z
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2OutputStream/dictResetNeeded Z
return
L1:
sipush 192
istore 3
goto L2
L0:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/stateResetNeeded Z
ifeq L4
sipush 160
istore 3
goto L2
L4:
sipush 128
istore 3
goto L2
.limit locals 4
.limit stack 4
.end method

.method private writeUncompressed(I)V
.throws java/io/IOException
L0:
iload 1
ifle L1
iload 1
ldc_w 65536
invokestatic java/lang/Math/min(II)I
istore 3
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/outData Ljava/io/DataOutputStream;
astore 4
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/dictResetNeeded Z
ifeq L2
iconst_1
istore 2
L3:
aload 4
iload 2
invokevirtual java/io/DataOutputStream/writeByte(I)V
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/outData Ljava/io/DataOutputStream;
iload 3
iconst_1
isub
invokevirtual java/io/DataOutputStream/writeShort(I)V
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
iload 1
iload 3
invokevirtual org/tukaani/xz/lz/LZEncoder/copyUncompressed(Ljava/io/OutputStream;II)V
iload 1
iload 3
isub
istore 1
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2OutputStream/dictResetNeeded Z
goto L0
L2:
iconst_2
istore 2
goto L3
L1:
aload 0
iconst_1
putfield org/tukaani/xz/LZMA2OutputStream/stateResetNeeded Z
return
.limit locals 5
.limit stack 4
.end method

.method public close()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L4
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
ifnull L5
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/finished Z
ifne L1
L0:
aload 0
invokespecial org/tukaani/xz/LZMA2OutputStream/writeEndMarker()V
L1:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
invokevirtual org/tukaani/xz/FinishableOutputStream/close()V
L3:
aload 0
aconst_null
putfield org/tukaani/xz/LZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
L5:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
ifnull L6
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
athrow
L4:
astore 1
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
ifnonnull L3
aload 0
aload 1
putfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
goto L3
L2:
astore 1
goto L1
L6:
return
.limit locals 2
.limit stack 2
.end method

.method public finish()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/finished Z
ifne L3
aload 0
invokespecial org/tukaani/xz/LZMA2OutputStream/writeEndMarker()V
L0:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
invokevirtual org/tukaani/xz/FinishableOutputStream/finish()V
L1:
aload 0
iconst_1
putfield org/tukaani/xz/LZMA2OutputStream/finished Z
L3:
return
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public flush()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L2
.catch java/io/IOException from L4 to L5 using L2
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
ifnull L6
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
athrow
L6:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/finished Z
ifeq L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream finished or closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/lz Lorg/tukaani/xz/lz/LZEncoder;
invokevirtual org/tukaani/xz/lz/LZEncoder/setFlushing()V
L1:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/pendingSize I
ifle L4
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/lzma Lorg/tukaani/xz/lzma/LZMAEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder/encodeForLZMA2()Z
pop
aload 0
invokespecial org/tukaani/xz/LZMA2OutputStream/writeChunk()V
L3:
goto L1
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
aload 1
athrow
L4:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
invokevirtual org/tukaani/xz/FinishableOutputStream/flush()V
L5:
return
.limit locals 2
.limit stack 3
.end method

.method public write(I)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/tempBuf [B
iconst_0
iload 1
i2b
bastore
aload 0
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/tempBuf [B
iconst_0
iconst_1
invokevirtual org/tukaani/xz/LZMA2OutputStream/write([BII)V
return
.limit locals 2
.limit stack 4
.end method

.method public write([BII)V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
iload 2
iflt L7
iload 3
iflt L7
iload 2
iload 3
iadd
iflt L7
iload 2
iload 3
iadd
aload 1
arraylength
if_icmple L8
L7:
new java/lang/IndexOutOfBoundsException
dup
invokespecial java/lang/IndexOutOfBoundsException/<init>()V
athrow
L8:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
ifnull L9
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
athrow
L9:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/finished Z
ifeq L10
new org/tukaani/xz/XZIOException
dup
ldc "Stream finished or closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L10:
iload 3
ifle L11
L0:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 1
iload 2
iload 3
invokevirtual org/tukaani/xz/lz/LZEncoder/fillWindow([BII)I
istore 6
L1:
iload 2
iload 6
iadd
istore 4
iload 3
iload 6
isub
istore 5
L3:
aload 0
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/pendingSize I
iload 6
iadd
putfield org/tukaani/xz/LZMA2OutputStream/pendingSize I
L4:
iload 4
istore 2
iload 5
istore 3
L5:
aload 0
getfield org/tukaani/xz/LZMA2OutputStream/lzma Lorg/tukaani/xz/lzma/LZMAEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder/encodeForLZMA2()Z
ifeq L10
aload 0
invokespecial org/tukaani/xz/LZMA2OutputStream/writeChunk()V
L6:
iload 4
istore 2
iload 5
istore 3
goto L10
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/LZMA2OutputStream/exception Ljava/io/IOException;
aload 1
athrow
L11:
return
.limit locals 7
.limit stack 4
.end method
