.bytecode 50.0
.class synchronized org/tukaani/xz/BCJEncoder
.super org/tukaani/xz/BCJCoder
.implements org/tukaani/xz/FilterEncoder

.field static final synthetic '$assertionsDisabled' Z

.field private final 'filterID' J

.field private final 'options' Lorg/tukaani/xz/BCJOptions;

.field private final 'props' [B

.method static <clinit>()V
ldc org/tukaani/xz/BCJEncoder
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/BCJEncoder/$assertionsDisabled Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method <init>(Lorg/tukaani/xz/BCJOptions;J)V
aload 0
invokespecial org/tukaani/xz/BCJCoder/<init>()V
getstatic org/tukaani/xz/BCJEncoder/$assertionsDisabled Z
ifne L0
lload 2
invokestatic org/tukaani/xz/BCJEncoder/isBCJFilterID(J)Z
ifne L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
aload 1
invokevirtual org/tukaani/xz/BCJOptions/getStartOffset()I
istore 5
iload 5
ifne L1
aload 0
iconst_0
newarray byte
putfield org/tukaani/xz/BCJEncoder/props [B
L2:
aload 0
lload 2
putfield org/tukaani/xz/BCJEncoder/filterID J
aload 0
aload 1
invokevirtual org/tukaani/xz/BCJOptions/clone()Ljava/lang/Object;
checkcast org/tukaani/xz/BCJOptions
putfield org/tukaani/xz/BCJEncoder/options Lorg/tukaani/xz/BCJOptions;
return
L1:
aload 0
iconst_4
newarray byte
putfield org/tukaani/xz/BCJEncoder/props [B
iconst_0
istore 4
L3:
iload 4
iconst_4
if_icmpge L2
aload 0
getfield org/tukaani/xz/BCJEncoder/props [B
iload 4
iload 5
iload 4
bipush 8
imul
iushr
i2b
bastore
iload 4
iconst_1
iadd
istore 4
goto L3
.limit locals 6
.limit stack 5
.end method

.method public getFilterID()J
aload 0
getfield org/tukaani/xz/BCJEncoder/filterID J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getFilterProps()[B
aload 0
getfield org/tukaani/xz/BCJEncoder/props [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;
aload 0
getfield org/tukaani/xz/BCJEncoder/options Lorg/tukaani/xz/BCJOptions;
aload 1
invokevirtual org/tukaani/xz/BCJOptions/getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;
areturn
.limit locals 2
.limit stack 2
.end method

.method public supportsFlushing()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method
