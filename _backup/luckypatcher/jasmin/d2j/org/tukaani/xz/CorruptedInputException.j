.bytecode 50.0
.class public synchronized org/tukaani/xz/CorruptedInputException
.super org/tukaani/xz/XZIOException

.field private static final 'serialVersionUID' J = 3L


.method public <init>()V
aload 0
ldc "Compressed data is corrupt"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Ljava/lang/String;)V
aload 0
aload 1
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 2
.end method
