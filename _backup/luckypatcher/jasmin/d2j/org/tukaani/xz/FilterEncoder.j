.bytecode 50.0
.class abstract interface org/tukaani/xz/FilterEncoder
.super java/lang/Object
.implements org/tukaani/xz/FilterCoder

.method public abstract getFilterID()J
.end method

.method public abstract getFilterProps()[B
.end method

.method public abstract getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;
.end method

.method public abstract supportsFlushing()Z
.end method
