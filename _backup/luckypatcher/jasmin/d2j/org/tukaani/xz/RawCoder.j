.bytecode 50.0
.class synchronized org/tukaani/xz/RawCoder
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static validate([Lorg/tukaani/xz/FilterCoder;)V
.throws org/tukaani/xz/UnsupportedOptionsException
iconst_0
istore 1
L0:
iload 1
aload 0
arraylength
iconst_1
isub
if_icmpge L1
aload 0
iload 1
aaload
invokeinterface org/tukaani/xz/FilterCoder/nonLastOK()Z 0
ifne L2
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "Unsupported XZ filter chain"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
aload 0
arraylength
iconst_1
isub
aaload
invokeinterface org/tukaani/xz/FilterCoder/lastOK()Z 0
ifne L3
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "Unsupported XZ filter chain"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L3:
iconst_0
istore 2
iconst_0
istore 1
L4:
iload 1
aload 0
arraylength
if_icmpge L5
iload 2
istore 3
aload 0
iload 1
aaload
invokeinterface org/tukaani/xz/FilterCoder/changesSize()Z 0
ifeq L6
iload 2
iconst_1
iadd
istore 3
L6:
iload 1
iconst_1
iadd
istore 1
iload 3
istore 2
goto L4
L5:
iload 2
iconst_3
if_icmple L7
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "Unsupported XZ filter chain"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L7:
return
.limit locals 4
.limit stack 3
.end method
