.bytecode 50.0
.class public synchronized org/tukaani/xz/delta/DeltaEncoder
.super org/tukaani/xz/delta/DeltaCoder

.method public <init>(I)V
aload 0
iload 1
invokespecial org/tukaani/xz/delta/DeltaCoder/<init>(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public encode([BII[B)V
iconst_0
istore 5
L0:
iload 5
iload 3
if_icmpge L1
aload 0
getfield org/tukaani/xz/delta/DeltaEncoder/history [B
aload 0
getfield org/tukaani/xz/delta/DeltaEncoder/distance I
aload 0
getfield org/tukaani/xz/delta/DeltaEncoder/pos I
iadd
sipush 255
iand
baload
istore 6
aload 0
getfield org/tukaani/xz/delta/DeltaEncoder/history [B
astore 8
aload 0
getfield org/tukaani/xz/delta/DeltaEncoder/pos I
istore 7
aload 0
iload 7
iconst_1
isub
putfield org/tukaani/xz/delta/DeltaEncoder/pos I
aload 8
iload 7
sipush 255
iand
aload 1
iload 2
iload 5
iadd
baload
bastore
aload 4
iload 5
aload 1
iload 2
iload 5
iadd
baload
iload 6
isub
i2b
bastore
iload 5
iconst_1
iadd
istore 5
goto L0
L1:
return
.limit locals 9
.limit stack 5
.end method
