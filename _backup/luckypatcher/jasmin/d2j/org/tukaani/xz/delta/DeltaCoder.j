.bytecode 50.0
.class synchronized abstract org/tukaani/xz/delta/DeltaCoder
.super java/lang/Object

.field static final 'DISTANCE_MASK' I = 255


.field static final 'DISTANCE_MAX' I = 256


.field static final 'DISTANCE_MIN' I = 1


.field final 'distance' I

.field final 'history' [B

.field 'pos' I

.method <init>(I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
sipush 256
newarray byte
putfield org/tukaani/xz/delta/DeltaCoder/history [B
aload 0
iconst_0
putfield org/tukaani/xz/delta/DeltaCoder/pos I
iload 1
iconst_1
if_icmplt L0
iload 1
sipush 256
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L1:
aload 0
iload 1
putfield org/tukaani/xz/delta/DeltaCoder/distance I
return
.limit locals 2
.limit stack 2
.end method
