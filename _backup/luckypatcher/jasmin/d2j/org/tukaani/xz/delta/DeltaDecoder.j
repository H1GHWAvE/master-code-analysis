.bytecode 50.0
.class public synchronized org/tukaani/xz/delta/DeltaDecoder
.super org/tukaani/xz/delta/DeltaCoder

.method public <init>(I)V
aload 0
iload 1
invokespecial org/tukaani/xz/delta/DeltaCoder/<init>(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public decode([BII)V
iload 2
istore 4
L0:
iload 4
iload 2
iload 3
iadd
if_icmpge L1
aload 1
iload 4
aload 1
iload 4
baload
aload 0
getfield org/tukaani/xz/delta/DeltaDecoder/history [B
aload 0
getfield org/tukaani/xz/delta/DeltaDecoder/distance I
aload 0
getfield org/tukaani/xz/delta/DeltaDecoder/pos I
iadd
sipush 255
iand
baload
iadd
i2b
bastore
aload 0
getfield org/tukaani/xz/delta/DeltaDecoder/history [B
astore 6
aload 0
getfield org/tukaani/xz/delta/DeltaDecoder/pos I
istore 5
aload 0
iload 5
iconst_1
isub
putfield org/tukaani/xz/delta/DeltaDecoder/pos I
aload 6
iload 5
sipush 255
iand
aload 1
iload 4
baload
bastore
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
return
.limit locals 7
.limit stack 6
.end method
