.bytecode 50.0
.class public synchronized org/tukaani/xz/LZMA2InputStream
.super java/io/InputStream

.field private static final 'COMPRESSED_SIZE_MAX' I = 65536


.field public static final 'DICT_SIZE_MAX' I = 2147483632


.field public static final 'DICT_SIZE_MIN' I = 4096


.field private 'endReached' Z

.field private 'exception' Ljava/io/IOException;

.field private 'in' Ljava/io/DataInputStream;

.field private 'isLZMAChunk' Z

.field private final 'lz' Lorg/tukaani/xz/lz/LZDecoder;

.field private 'lzma' Lorg/tukaani/xz/lzma/LZMADecoder;

.field private 'needDictReset' Z

.field private 'needProps' Z

.field private final 'rc' Lorg/tukaani/xz/rangecoder/RangeDecoderFromBuffer;

.field private final 'tempBuf' [B

.field private 'uncompressedSize' I

.method public <init>(Ljava/io/InputStream;I)V
aload 0
aload 1
iload 2
aconst_null
invokespecial org/tukaani/xz/LZMA2InputStream/<init>(Ljava/io/InputStream;I[B)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Ljava/io/InputStream;I[B)V
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
new org/tukaani/xz/rangecoder/RangeDecoderFromBuffer
dup
ldc_w 65536
invokespecial org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/<init>(I)V
putfield org/tukaani/xz/LZMA2InputStream/rc Lorg/tukaani/xz/rangecoder/RangeDecoderFromBuffer;
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2InputStream/uncompressedSize I
aload 0
iconst_1
putfield org/tukaani/xz/LZMA2InputStream/needDictReset Z
aload 0
iconst_1
putfield org/tukaani/xz/LZMA2InputStream/needProps Z
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2InputStream/endReached Z
aload 0
aconst_null
putfield org/tukaani/xz/LZMA2InputStream/exception Ljava/io/IOException;
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/LZMA2InputStream/tempBuf [B
aload 1
ifnonnull L0
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L0:
aload 0
new java/io/DataInputStream
dup
aload 1
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
putfield org/tukaani/xz/LZMA2InputStream/in Ljava/io/DataInputStream;
aload 0
new org/tukaani/xz/lz/LZDecoder
dup
iload 2
invokestatic org/tukaani/xz/LZMA2InputStream/getDictSize(I)I
aload 3
invokespecial org/tukaani/xz/lz/LZDecoder/<init>(I[B)V
putfield org/tukaani/xz/LZMA2InputStream/lz Lorg/tukaani/xz/lz/LZDecoder;
aload 3
ifnull L1
aload 3
arraylength
ifle L1
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2InputStream/needDictReset Z
L1:
return
.limit locals 4
.limit stack 5
.end method

.method private decodeChunkHeader()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/LZMA2InputStream/in Ljava/io/DataInputStream;
invokevirtual java/io/DataInputStream/readUnsignedByte()I
istore 1
iload 1
ifne L0
aload 0
iconst_1
putfield org/tukaani/xz/LZMA2InputStream/endReached Z
return
L0:
iload 1
sipush 224
if_icmpge L1
iload 1
iconst_1
if_icmpne L2
L1:
aload 0
iconst_1
putfield org/tukaani/xz/LZMA2InputStream/needProps Z
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2InputStream/needDictReset Z
aload 0
getfield org/tukaani/xz/LZMA2InputStream/lz Lorg/tukaani/xz/lz/LZDecoder;
invokevirtual org/tukaani/xz/lz/LZDecoder/reset()V
L3:
iload 1
sipush 128
if_icmplt L4
aload 0
iconst_1
putfield org/tukaani/xz/LZMA2InputStream/isLZMAChunk Z
aload 0
iload 1
bipush 31
iand
bipush 16
ishl
putfield org/tukaani/xz/LZMA2InputStream/uncompressedSize I
aload 0
aload 0
getfield org/tukaani/xz/LZMA2InputStream/uncompressedSize I
aload 0
getfield org/tukaani/xz/LZMA2InputStream/in Ljava/io/DataInputStream;
invokevirtual java/io/DataInputStream/readUnsignedShort()I
iconst_1
iadd
iadd
putfield org/tukaani/xz/LZMA2InputStream/uncompressedSize I
aload 0
getfield org/tukaani/xz/LZMA2InputStream/in Ljava/io/DataInputStream;
invokevirtual java/io/DataInputStream/readUnsignedShort()I
istore 2
iload 1
sipush 192
if_icmplt L5
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2InputStream/needProps Z
aload 0
invokespecial org/tukaani/xz/LZMA2InputStream/decodeProps()V
L6:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/rc Lorg/tukaani/xz/rangecoder/RangeDecoderFromBuffer;
aload 0
getfield org/tukaani/xz/LZMA2InputStream/in Ljava/io/DataInputStream;
iload 2
iconst_1
iadd
invokevirtual org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/prepareInputBuffer(Ljava/io/DataInputStream;I)V
return
L2:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/needDictReset Z
ifeq L3
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L5:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/needProps Z
ifeq L7
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L7:
iload 1
sipush 160
if_icmplt L6
aload 0
getfield org/tukaani/xz/LZMA2InputStream/lzma Lorg/tukaani/xz/lzma/LZMADecoder;
invokevirtual org/tukaani/xz/lzma/LZMADecoder/reset()V
goto L6
L4:
iload 1
iconst_2
if_icmple L8
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L8:
aload 0
iconst_0
putfield org/tukaani/xz/LZMA2InputStream/isLZMAChunk Z
aload 0
aload 0
getfield org/tukaani/xz/LZMA2InputStream/in Ljava/io/DataInputStream;
invokevirtual java/io/DataInputStream/readUnsignedShort()I
iconst_1
iadd
putfield org/tukaani/xz/LZMA2InputStream/uncompressedSize I
return
.limit locals 3
.limit stack 4
.end method

.method private decodeProps()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/LZMA2InputStream/in Ljava/io/DataInputStream;
invokevirtual java/io/DataInputStream/readUnsignedByte()I
istore 2
iload 2
sipush 224
if_icmple L0
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L0:
iload 2
bipush 45
idiv
istore 1
iload 2
iload 1
bipush 9
imul
iconst_5
imul
isub
istore 3
iload 3
bipush 9
idiv
istore 2
iload 3
iload 2
bipush 9
imul
isub
istore 3
iload 3
iload 2
iadd
iconst_4
if_icmple L1
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L1:
aload 0
new org/tukaani/xz/lzma/LZMADecoder
dup
aload 0
getfield org/tukaani/xz/LZMA2InputStream/lz Lorg/tukaani/xz/lz/LZDecoder;
aload 0
getfield org/tukaani/xz/LZMA2InputStream/rc Lorg/tukaani/xz/rangecoder/RangeDecoderFromBuffer;
iload 3
iload 2
iload 1
invokespecial org/tukaani/xz/lzma/LZMADecoder/<init>(Lorg/tukaani/xz/lz/LZDecoder;Lorg/tukaani/xz/rangecoder/RangeDecoder;III)V
putfield org/tukaani/xz/LZMA2InputStream/lzma Lorg/tukaani/xz/lzma/LZMADecoder;
return
.limit locals 4
.limit stack 8
.end method

.method private static getDictSize(I)I
iload 0
sipush 4096
if_icmplt L0
iload 0
ldc_w 2147483632
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unsupported dictionary size "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 0
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 0
bipush 15
iadd
bipush -16
iand
ireturn
.limit locals 1
.limit stack 4
.end method

.method public static getMemoryUsage(I)I
iload 0
invokestatic org/tukaani/xz/LZMA2InputStream/getDictSize(I)I
sipush 1024
idiv
bipush 104
iadd
ireturn
.limit locals 1
.limit stack 2
.end method

.method public available()I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/LZMA2InputStream/in Ljava/io/DataInputStream;
ifnonnull L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/exception Ljava/io/IOException;
ifnull L1
aload 0
getfield org/tukaani/xz/LZMA2InputStream/exception Ljava/io/IOException;
athrow
L1:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/uncompressedSize I
ireturn
.limit locals 1
.limit stack 3
.end method

.method public close()V
.throws java/io/IOException
.catch all from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/LZMA2InputStream/in Ljava/io/DataInputStream;
ifnull L3
L0:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/in Ljava/io/DataInputStream;
invokevirtual java/io/DataInputStream/close()V
L1:
aload 0
aconst_null
putfield org/tukaani/xz/LZMA2InputStream/in Ljava/io/DataInputStream;
L3:
return
L2:
astore 1
aload 0
aconst_null
putfield org/tukaani/xz/LZMA2InputStream/in Ljava/io/DataInputStream;
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public read()I
.throws java/io/IOException
aload 0
aload 0
getfield org/tukaani/xz/LZMA2InputStream/tempBuf [B
iconst_0
iconst_1
invokevirtual org/tukaani/xz/LZMA2InputStream/read([BII)I
iconst_m1
if_icmpne L0
iconst_m1
ireturn
L0:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/tempBuf [B
iconst_0
baload
sipush 255
iand
ireturn
.limit locals 1
.limit stack 4
.end method

.method public read([BII)I
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L4 to L5 using L2
.catch java/io/IOException from L6 to L7 using L2
.catch java/io/IOException from L8 to L9 using L2
.catch java/io/IOException from L10 to L11 using L2
.catch java/io/IOException from L11 to L2 using L2
.catch java/io/IOException from L12 to L13 using L2
iload 2
iflt L14
iload 3
iflt L14
iload 2
iload 3
iadd
iflt L14
iload 2
iload 3
iadd
aload 1
arraylength
if_icmple L15
L14:
new java/lang/IndexOutOfBoundsException
dup
invokespecial java/lang/IndexOutOfBoundsException/<init>()V
athrow
L15:
iload 3
ifne L16
iconst_0
istore 5
L17:
iload 5
ireturn
L16:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/in Ljava/io/DataInputStream;
ifnonnull L18
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L18:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/exception Ljava/io/IOException;
ifnull L19
aload 0
getfield org/tukaani/xz/LZMA2InputStream/exception Ljava/io/IOException;
athrow
L19:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/endReached Z
ifeq L20
iconst_m1
ireturn
L20:
iconst_0
istore 5
iload 2
istore 4
iload 5
istore 2
L21:
iload 2
istore 5
iload 3
ifle L17
L0:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/uncompressedSize I
ifne L3
aload 0
invokespecial org/tukaani/xz/LZMA2InputStream/decodeChunkHeader()V
aload 0
getfield org/tukaani/xz/LZMA2InputStream/endReached Z
ifeq L3
L1:
iload 2
istore 5
iload 2
ifne L17
iconst_m1
ireturn
L3:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/uncompressedSize I
iload 3
invokestatic java/lang/Math/min(II)I
istore 5
aload 0
getfield org/tukaani/xz/LZMA2InputStream/isLZMAChunk Z
ifne L12
aload 0
getfield org/tukaani/xz/LZMA2InputStream/lz Lorg/tukaani/xz/lz/LZDecoder;
aload 0
getfield org/tukaani/xz/LZMA2InputStream/in Ljava/io/DataInputStream;
iload 5
invokevirtual org/tukaani/xz/lz/LZDecoder/copyUncompressed(Ljava/io/DataInputStream;I)V
L4:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/lz Lorg/tukaani/xz/lz/LZDecoder;
aload 1
iload 4
invokevirtual org/tukaani/xz/lz/LZDecoder/flush([BI)I
istore 8
L5:
iload 4
iload 8
iadd
istore 5
iload 3
iload 8
isub
istore 6
iload 2
iload 8
iadd
istore 7
L6:
aload 0
aload 0
getfield org/tukaani/xz/LZMA2InputStream/uncompressedSize I
iload 8
isub
putfield org/tukaani/xz/LZMA2InputStream/uncompressedSize I
L7:
iload 7
istore 2
iload 5
istore 4
iload 6
istore 3
L8:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/uncompressedSize I
ifne L21
aload 0
getfield org/tukaani/xz/LZMA2InputStream/rc Lorg/tukaani/xz/rangecoder/RangeDecoderFromBuffer;
invokevirtual org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/isFinished()Z
ifeq L11
L9:
iload 7
istore 2
iload 5
istore 4
iload 6
istore 3
L10:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/lz Lorg/tukaani/xz/lz/LZDecoder;
invokevirtual org/tukaani/xz/lz/LZDecoder/hasPending()Z
ifeq L21
L11:
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/LZMA2InputStream/exception Ljava/io/IOException;
aload 1
athrow
L12:
aload 0
getfield org/tukaani/xz/LZMA2InputStream/lz Lorg/tukaani/xz/lz/LZDecoder;
iload 5
invokevirtual org/tukaani/xz/lz/LZDecoder/setLimit(I)V
aload 0
getfield org/tukaani/xz/LZMA2InputStream/lzma Lorg/tukaani/xz/lzma/LZMADecoder;
invokevirtual org/tukaani/xz/lzma/LZMADecoder/decode()V
aload 0
getfield org/tukaani/xz/LZMA2InputStream/rc Lorg/tukaani/xz/rangecoder/RangeDecoderFromBuffer;
invokevirtual org/tukaani/xz/rangecoder/RangeDecoderFromBuffer/isInBufferOK()Z
ifne L4
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L13:
.limit locals 9
.limit stack 3
.end method
