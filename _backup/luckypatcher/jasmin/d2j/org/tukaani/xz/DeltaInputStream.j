.bytecode 50.0
.class public synchronized org/tukaani/xz/DeltaInputStream
.super java/io/InputStream

.field public static final 'DISTANCE_MAX' I = 256


.field public static final 'DISTANCE_MIN' I = 1


.field private final 'delta' Lorg/tukaani/xz/delta/DeltaDecoder;

.field private 'exception' Ljava/io/IOException;

.field private 'in' Ljava/io/InputStream;

.field private final 'tempBuf' [B

.method public <init>(Ljava/io/InputStream;I)V
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
aconst_null
putfield org/tukaani/xz/DeltaInputStream/exception Ljava/io/IOException;
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/DeltaInputStream/tempBuf [B
aload 1
ifnonnull L0
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L0:
aload 0
aload 1
putfield org/tukaani/xz/DeltaInputStream/in Ljava/io/InputStream;
aload 0
new org/tukaani/xz/delta/DeltaDecoder
dup
iload 2
invokespecial org/tukaani/xz/delta/DeltaDecoder/<init>(I)V
putfield org/tukaani/xz/DeltaInputStream/delta Lorg/tukaani/xz/delta/DeltaDecoder;
return
.limit locals 3
.limit stack 4
.end method

.method public available()I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/DeltaInputStream/in Ljava/io/InputStream;
ifnonnull L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/tukaani/xz/DeltaInputStream/exception Ljava/io/IOException;
ifnull L1
aload 0
getfield org/tukaani/xz/DeltaInputStream/exception Ljava/io/IOException;
athrow
L1:
aload 0
getfield org/tukaani/xz/DeltaInputStream/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/available()I
ireturn
.limit locals 1
.limit stack 3
.end method

.method public close()V
.throws java/io/IOException
.catch all from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/DeltaInputStream/in Ljava/io/InputStream;
ifnull L3
L0:
aload 0
getfield org/tukaani/xz/DeltaInputStream/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/close()V
L1:
aload 0
aconst_null
putfield org/tukaani/xz/DeltaInputStream/in Ljava/io/InputStream;
L3:
return
L2:
astore 1
aload 0
aconst_null
putfield org/tukaani/xz/DeltaInputStream/in Ljava/io/InputStream;
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public read()I
.throws java/io/IOException
aload 0
aload 0
getfield org/tukaani/xz/DeltaInputStream/tempBuf [B
iconst_0
iconst_1
invokevirtual org/tukaani/xz/DeltaInputStream/read([BII)I
iconst_m1
if_icmpne L0
iconst_m1
ireturn
L0:
aload 0
getfield org/tukaani/xz/DeltaInputStream/tempBuf [B
iconst_0
baload
sipush 255
iand
ireturn
.limit locals 1
.limit stack 4
.end method

.method public read([BII)I
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
iload 3
ifne L3
iconst_0
ireturn
L3:
aload 0
getfield org/tukaani/xz/DeltaInputStream/in Ljava/io/InputStream;
ifnonnull L4
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 0
getfield org/tukaani/xz/DeltaInputStream/exception Ljava/io/IOException;
ifnull L0
aload 0
getfield org/tukaani/xz/DeltaInputStream/exception Ljava/io/IOException;
athrow
L0:
aload 0
getfield org/tukaani/xz/DeltaInputStream/in Ljava/io/InputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/InputStream/read([BII)I
istore 3
L1:
iload 3
iconst_m1
if_icmpne L5
iconst_m1
ireturn
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/DeltaInputStream/exception Ljava/io/IOException;
aload 1
athrow
L5:
aload 0
getfield org/tukaani/xz/DeltaInputStream/delta Lorg/tukaani/xz/delta/DeltaDecoder;
aload 1
iload 2
iload 3
invokevirtual org/tukaani/xz/delta/DeltaDecoder/decode([BII)V
iload 3
ireturn
.limit locals 4
.limit stack 4
.end method
