.bytecode 50.0
.class final synchronized org/tukaani/xz/lz/Hash234
.super org/tukaani/xz/lz/CRC32Hash

.field private static final 'HASH_2_MASK' I = 1023


.field private static final 'HASH_2_SIZE' I = 1024


.field private static final 'HASH_3_MASK' I = 65535


.field private static final 'HASH_3_SIZE' I = 65536


.field private final 'hash2Table' [I

.field private 'hash2Value' I

.field private final 'hash3Table' [I

.field private 'hash3Value' I

.field private final 'hash4Mask' I

.field private final 'hash4Table' [I

.field private 'hash4Value' I

.method <init>(I)V
aload 0
invokespecial org/tukaani/xz/lz/CRC32Hash/<init>()V
aload 0
sipush 1024
newarray int
putfield org/tukaani/xz/lz/Hash234/hash2Table [I
aload 0
ldc_w 65536
newarray int
putfield org/tukaani/xz/lz/Hash234/hash3Table [I
aload 0
iconst_0
putfield org/tukaani/xz/lz/Hash234/hash2Value I
aload 0
iconst_0
putfield org/tukaani/xz/lz/Hash234/hash3Value I
aload 0
iconst_0
putfield org/tukaani/xz/lz/Hash234/hash4Value I
aload 0
iload 1
invokestatic org/tukaani/xz/lz/Hash234/getHash4Size(I)I
newarray int
putfield org/tukaani/xz/lz/Hash234/hash4Table [I
aload 0
aload 0
getfield org/tukaani/xz/lz/Hash234/hash4Table [I
arraylength
iconst_1
isub
putfield org/tukaani/xz/lz/Hash234/hash4Mask I
return
.limit locals 2
.limit stack 3
.end method

.method static getHash4Size(I)I
iload 0
iconst_1
isub
istore 0
iload 0
iload 0
iconst_1
iushr
ior
istore 0
iload 0
iload 0
iconst_2
iushr
ior
istore 0
iload 0
iload 0
iconst_4
iushr
ior
istore 0
iload 0
iload 0
bipush 8
iushr
ior
iconst_1
iushr
ldc_w 65535
ior
istore 1
iload 1
istore 0
iload 1
ldc_w 16777216
if_icmple L0
iload 1
iconst_1
iushr
istore 0
L0:
iload 0
iconst_1
iadd
ireturn
.limit locals 2
.limit stack 3
.end method

.method static getMemoryUsage(I)I
ldc_w 66560
iload 0
invokestatic org/tukaani/xz/lz/Hash234/getHash4Size(I)I
iadd
sipush 256
idiv
iconst_4
iadd
ireturn
.limit locals 1
.limit stack 2
.end method

.method calcHashes([BI)V
getstatic org/tukaani/xz/lz/Hash234/crcTable [I
aload 1
iload 2
baload
sipush 255
iand
iaload
aload 1
iload 2
iconst_1
iadd
baload
sipush 255
iand
ixor
istore 3
aload 0
iload 3
sipush 1023
iand
putfield org/tukaani/xz/lz/Hash234/hash2Value I
iload 3
aload 1
iload 2
iconst_2
iadd
baload
sipush 255
iand
bipush 8
ishl
ixor
istore 3
aload 0
ldc_w 65535
iload 3
iand
putfield org/tukaani/xz/lz/Hash234/hash3Value I
getstatic org/tukaani/xz/lz/Hash234/crcTable [I
aload 1
iload 2
iconst_3
iadd
baload
sipush 255
iand
iaload
istore 2
aload 0
aload 0
getfield org/tukaani/xz/lz/Hash234/hash4Mask I
iload 3
iload 2
iconst_5
ishl
ixor
iand
putfield org/tukaani/xz/lz/Hash234/hash4Value I
return
.limit locals 4
.limit stack 5
.end method

.method getHash2Pos()I
aload 0
getfield org/tukaani/xz/lz/Hash234/hash2Table [I
aload 0
getfield org/tukaani/xz/lz/Hash234/hash2Value I
iaload
ireturn
.limit locals 1
.limit stack 2
.end method

.method getHash3Pos()I
aload 0
getfield org/tukaani/xz/lz/Hash234/hash3Table [I
aload 0
getfield org/tukaani/xz/lz/Hash234/hash3Value I
iaload
ireturn
.limit locals 1
.limit stack 2
.end method

.method getHash4Pos()I
aload 0
getfield org/tukaani/xz/lz/Hash234/hash4Table [I
aload 0
getfield org/tukaani/xz/lz/Hash234/hash4Value I
iaload
ireturn
.limit locals 1
.limit stack 2
.end method

.method normalize(I)V
aload 0
getfield org/tukaani/xz/lz/Hash234/hash2Table [I
iload 1
invokestatic org/tukaani/xz/lz/LZEncoder/normalize([II)V
aload 0
getfield org/tukaani/xz/lz/Hash234/hash3Table [I
iload 1
invokestatic org/tukaani/xz/lz/LZEncoder/normalize([II)V
aload 0
getfield org/tukaani/xz/lz/Hash234/hash4Table [I
iload 1
invokestatic org/tukaani/xz/lz/LZEncoder/normalize([II)V
return
.limit locals 2
.limit stack 2
.end method

.method updateTables(I)V
aload 0
getfield org/tukaani/xz/lz/Hash234/hash2Table [I
aload 0
getfield org/tukaani/xz/lz/Hash234/hash2Value I
iload 1
iastore
aload 0
getfield org/tukaani/xz/lz/Hash234/hash3Table [I
aload 0
getfield org/tukaani/xz/lz/Hash234/hash3Value I
iload 1
iastore
aload 0
getfield org/tukaani/xz/lz/Hash234/hash4Table [I
aload 0
getfield org/tukaani/xz/lz/Hash234/hash4Value I
iload 1
iastore
return
.limit locals 2
.limit stack 3
.end method
