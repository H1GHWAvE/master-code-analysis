.bytecode 50.0
.class synchronized org/tukaani/xz/lz/CRC32Hash
.super java/lang/Object

.field private static final 'CRC32_POLY' I = -306674912


.field static final 'crcTable' [I

.method static <clinit>()V
sipush 256
newarray int
putstatic org/tukaani/xz/lz/CRC32Hash/crcTable [I
iconst_0
istore 0
L0:
iload 0
sipush 256
if_icmpge L1
iload 0
istore 1
iconst_0
istore 2
L2:
iload 2
bipush 8
if_icmpge L3
iload 1
iconst_1
iand
ifeq L4
iload 1
iconst_1
iushr
ldc_w -306674912
ixor
istore 1
L5:
iload 2
iconst_1
iadd
istore 2
goto L2
L4:
iload 1
iconst_1
iushr
istore 1
goto L5
L3:
getstatic org/tukaani/xz/lz/CRC32Hash/crcTable [I
iload 0
iload 1
iastore
iload 0
iconst_1
iadd
istore 0
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method
