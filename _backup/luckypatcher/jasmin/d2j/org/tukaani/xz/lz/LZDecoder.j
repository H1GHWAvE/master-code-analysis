.bytecode 50.0
.class public final synchronized org/tukaani/xz/lz/LZDecoder
.super java/lang/Object

.field private final 'buf' [B

.field private 'full' I

.field private 'limit' I

.field private 'pendingDist' I

.field private 'pendingLen' I

.field private 'pos' I

.field private 'start' I

.method public <init>(I[B)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZDecoder/start I
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZDecoder/pos I
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZDecoder/full I
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZDecoder/limit I
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZDecoder/pendingLen I
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZDecoder/pendingDist I
aload 0
iload 1
newarray byte
putfield org/tukaani/xz/lz/LZDecoder/buf [B
aload 2
ifnull L0
aload 0
aload 2
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
putfield org/tukaani/xz/lz/LZDecoder/pos I
aload 0
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
putfield org/tukaani/xz/lz/LZDecoder/full I
aload 0
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
putfield org/tukaani/xz/lz/LZDecoder/start I
aload 2
aload 2
arraylength
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
isub
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
iconst_0
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L0:
return
.limit locals 3
.limit stack 5
.end method

.method public copyUncompressed(Ljava/io/DataInputStream;I)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
arraylength
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
isub
iload 2
invokestatic java/lang/Math/min(II)I
istore 2
aload 1
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
iload 2
invokevirtual java/io/DataInputStream/readFully([BII)V
aload 0
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
iload 2
iadd
putfield org/tukaani/xz/lz/LZDecoder/pos I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/full I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
if_icmpge L0
aload 0
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
putfield org/tukaani/xz/lz/LZDecoder/full I
L0:
return
.limit locals 3
.limit stack 4
.end method

.method public flush([BI)I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/start I
isub
istore 3
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
arraylength
if_icmpne L0
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZDecoder/pos I
L0:
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
aload 0
getfield org/tukaani/xz/lz/LZDecoder/start I
aload 1
iload 2
iload 3
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
putfield org/tukaani/xz/lz/LZDecoder/start I
iload 3
ireturn
.limit locals 4
.limit stack 5
.end method

.method public getByte(I)I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
iload 1
isub
iconst_1
isub
istore 3
iload 3
istore 2
iload 1
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
if_icmplt L0
iload 3
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
arraylength
iadd
istore 2
L0:
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
iload 2
baload
sipush 255
iand
ireturn
.limit locals 4
.limit stack 2
.end method

.method public getPos()I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public hasPending()Z
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pendingLen I
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public hasSpace()Z
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/limit I
if_icmpge L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public putByte(B)V
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
astore 3
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
istore 2
aload 0
iload 2
iconst_1
iadd
putfield org/tukaani/xz/lz/LZDecoder/pos I
aload 3
iload 2
iload 1
bastore
aload 0
getfield org/tukaani/xz/lz/LZDecoder/full I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
if_icmpge L0
aload 0
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
putfield org/tukaani/xz/lz/LZDecoder/full I
L0:
return
.limit locals 4
.limit stack 3
.end method

.method public repeat(II)V
.throws java/io/IOException
iload 1
iflt L0
iload 1
aload 0
getfield org/tukaani/xz/lz/LZDecoder/full I
if_icmplt L1
L0:
new org/tukaani/xz/CorruptedInputException
dup
invokespecial org/tukaani/xz/CorruptedInputException/<init>()V
athrow
L1:
aload 0
getfield org/tukaani/xz/lz/LZDecoder/limit I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
isub
iload 2
invokestatic java/lang/Math/min(II)I
istore 4
aload 0
iload 2
iload 4
isub
putfield org/tukaani/xz/lz/LZDecoder/pendingLen I
aload 0
iload 1
putfield org/tukaani/xz/lz/LZDecoder/pendingDist I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
iload 1
isub
iconst_1
isub
istore 5
iload 5
istore 2
iload 4
istore 3
iload 1
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
if_icmplt L2
iload 5
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
arraylength
iadd
istore 2
iload 4
istore 3
L2:
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
astore 6
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
istore 4
aload 0
iload 4
iconst_1
iadd
putfield org/tukaani/xz/lz/LZDecoder/pos I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
astore 7
iload 2
iconst_1
iadd
istore 1
aload 6
iload 4
aload 7
iload 2
baload
bastore
iload 1
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
arraylength
if_icmpne L3
iconst_0
istore 2
L4:
iload 3
iconst_1
isub
istore 1
iload 1
istore 3
iload 1
ifgt L2
aload 0
getfield org/tukaani/xz/lz/LZDecoder/full I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
if_icmpge L5
aload 0
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
putfield org/tukaani/xz/lz/LZDecoder/full I
L5:
return
L3:
iload 1
istore 2
goto L4
.limit locals 8
.limit stack 4
.end method

.method public repeatPending()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pendingLen I
ifle L0
aload 0
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pendingDist I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pendingLen I
invokevirtual org/tukaani/xz/lz/LZDecoder/repeat(II)V
L0:
return
.limit locals 1
.limit stack 3
.end method

.method public reset()V
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZDecoder/start I
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZDecoder/pos I
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZDecoder/full I
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZDecoder/limit I
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
arraylength
iconst_1
isub
iconst_0
bastore
return
.limit locals 1
.limit stack 3
.end method

.method public setLimit(I)V
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
arraylength
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
isub
iload 1
if_icmpgt L0
aload 0
aload 0
getfield org/tukaani/xz/lz/LZDecoder/buf [B
arraylength
putfield org/tukaani/xz/lz/LZDecoder/limit I
return
L0:
aload 0
aload 0
getfield org/tukaani/xz/lz/LZDecoder/pos I
iload 1
iadd
putfield org/tukaani/xz/lz/LZDecoder/limit I
return
.limit locals 2
.limit stack 3
.end method
