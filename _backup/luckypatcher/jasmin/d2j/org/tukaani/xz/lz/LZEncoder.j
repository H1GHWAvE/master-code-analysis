.bytecode 50.0
.class public synchronized abstract org/tukaani/xz/lz/LZEncoder
.super java/lang/Object

.field static final synthetic '$assertionsDisabled' Z

.field public static final 'MF_BT4' I = 20


.field public static final 'MF_HC4' I = 4


.field final 'buf' [B

.field private 'finishing' Z

.field private final 'keepSizeAfter' I

.field private final 'keepSizeBefore' I

.field final 'matchLenMax' I

.field final 'niceLen' I

.field private 'pendingSize' I

.field private 'readLimit' I

.field 'readPos' I

.field private 'writePos' I

.method static <clinit>()V
ldc org/tukaani/xz/lz/LZEncoder
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/lz/LZEncoder/$assertionsDisabled Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method <init>(IIIII)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_m1
putfield org/tukaani/xz/lz/LZEncoder/readPos I
aload 0
iconst_m1
putfield org/tukaani/xz/lz/LZEncoder/readLimit I
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZEncoder/finishing Z
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZEncoder/writePos I
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZEncoder/pendingSize I
aload 0
iload 1
iload 2
iload 3
iload 5
invokestatic org/tukaani/xz/lz/LZEncoder/getBufSize(IIII)I
newarray byte
putfield org/tukaani/xz/lz/LZEncoder/buf [B
aload 0
iload 2
iload 1
iadd
putfield org/tukaani/xz/lz/LZEncoder/keepSizeBefore I
aload 0
iload 3
iload 5
iadd
putfield org/tukaani/xz/lz/LZEncoder/keepSizeAfter I
aload 0
iload 5
putfield org/tukaani/xz/lz/LZEncoder/matchLenMax I
aload 0
iload 4
putfield org/tukaani/xz/lz/LZEncoder/niceLen I
return
.limit locals 6
.limit stack 5
.end method

.method private static getBufSize(IIII)I
iload 1
iload 0
iadd
iload 2
iload 3
iadd
iadd
iload 0
iconst_2
idiv
ldc_w 262144
iadd
ldc_w 536870912
invokestatic java/lang/Math/min(II)I
iadd
ireturn
.limit locals 4
.limit stack 3
.end method

.method public static getInstance(IIIIIII)Lorg/tukaani/xz/lz/LZEncoder;
iload 5
lookupswitch
4 : L0
20 : L1
default : L2
L2:
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L0:
new org/tukaani/xz/lz/HC4
dup
iload 0
iload 1
iload 2
iload 3
iload 4
iload 6
invokespecial org/tukaani/xz/lz/HC4/<init>(IIIIII)V
areturn
L1:
new org/tukaani/xz/lz/BT4
dup
iload 0
iload 1
iload 2
iload 3
iload 4
iload 6
invokespecial org/tukaani/xz/lz/BT4/<init>(IIIIII)V
areturn
.limit locals 7
.limit stack 8
.end method

.method public static getMemoryUsage(IIIII)I
iload 0
iload 1
iload 2
iload 3
invokestatic org/tukaani/xz/lz/LZEncoder/getBufSize(IIII)I
sipush 1024
idiv
bipush 10
iadd
istore 1
iload 4
lookupswitch
4 : L0
20 : L1
default : L2
L2:
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L0:
iload 1
iload 0
invokestatic org/tukaani/xz/lz/HC4/getMemoryUsage(I)I
iadd
ireturn
L1:
iload 1
iload 0
invokestatic org/tukaani/xz/lz/BT4/getMemoryUsage(I)I
iadd
ireturn
.limit locals 5
.limit stack 4
.end method

.method private moveWindow()V
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
iconst_1
iadd
aload 0
getfield org/tukaani/xz/lz/LZEncoder/keepSizeBefore I
isub
bipush -16
iand
istore 1
aload 0
getfield org/tukaani/xz/lz/LZEncoder/writePos I
istore 2
aload 0
getfield org/tukaani/xz/lz/LZEncoder/buf [B
iload 1
aload 0
getfield org/tukaani/xz/lz/LZEncoder/buf [B
iconst_0
iload 2
iload 1
isub
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
iload 1
isub
putfield org/tukaani/xz/lz/LZEncoder/readPos I
aload 0
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readLimit I
iload 1
isub
putfield org/tukaani/xz/lz/LZEncoder/readLimit I
aload 0
aload 0
getfield org/tukaani/xz/lz/LZEncoder/writePos I
iload 1
isub
putfield org/tukaani/xz/lz/LZEncoder/writePos I
return
.limit locals 3
.limit stack 6
.end method

.method static normalize([II)V
iconst_0
istore 2
L0:
iload 2
aload 0
arraylength
if_icmpge L1
aload 0
iload 2
iaload
iload 1
if_icmpgt L2
aload 0
iload 2
iconst_0
iastore
L3:
iload 2
iconst_1
iadd
istore 2
goto L0
L2:
aload 0
iload 2
aload 0
iload 2
iaload
iload 1
isub
iastore
goto L3
L1:
return
.limit locals 3
.limit stack 4
.end method

.method private processPendingBytes()V
aload 0
getfield org/tukaani/xz/lz/LZEncoder/pendingSize I
ifle L0
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readLimit I
if_icmpge L0
aload 0
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/pendingSize I
isub
putfield org/tukaani/xz/lz/LZEncoder/readPos I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/pendingSize I
istore 1
aload 0
iconst_0
putfield org/tukaani/xz/lz/LZEncoder/pendingSize I
aload 0
iload 1
invokevirtual org/tukaani/xz/lz/LZEncoder/skip(I)V
getstatic org/tukaani/xz/lz/LZEncoder/$assertionsDisabled Z
ifne L0
aload 0
getfield org/tukaani/xz/lz/LZEncoder/pendingSize I
iload 1
if_icmplt L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public copyUncompressed(Ljava/io/OutputStream;II)V
.throws java/io/IOException
aload 1
aload 0
getfield org/tukaani/xz/lz/LZEncoder/buf [B
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
iconst_1
iadd
iload 2
isub
iload 3
invokevirtual java/io/OutputStream/write([BII)V
return
.limit locals 4
.limit stack 4
.end method

.method public fillWindow([BII)I
getstatic org/tukaani/xz/lz/LZEncoder/$assertionsDisabled Z
ifne L0
aload 0
getfield org/tukaani/xz/lz/LZEncoder/finishing Z
ifeq L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/buf [B
arraylength
aload 0
getfield org/tukaani/xz/lz/LZEncoder/keepSizeAfter I
isub
if_icmplt L1
aload 0
invokespecial org/tukaani/xz/lz/LZEncoder/moveWindow()V
L1:
iload 3
istore 4
iload 3
aload 0
getfield org/tukaani/xz/lz/LZEncoder/buf [B
arraylength
aload 0
getfield org/tukaani/xz/lz/LZEncoder/writePos I
isub
if_icmple L2
aload 0
getfield org/tukaani/xz/lz/LZEncoder/buf [B
arraylength
aload 0
getfield org/tukaani/xz/lz/LZEncoder/writePos I
isub
istore 4
L2:
aload 1
iload 2
aload 0
getfield org/tukaani/xz/lz/LZEncoder/buf [B
aload 0
getfield org/tukaani/xz/lz/LZEncoder/writePos I
iload 4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 0
getfield org/tukaani/xz/lz/LZEncoder/writePos I
iload 4
iadd
putfield org/tukaani/xz/lz/LZEncoder/writePos I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/writePos I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/keepSizeAfter I
if_icmplt L3
aload 0
aload 0
getfield org/tukaani/xz/lz/LZEncoder/writePos I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/keepSizeAfter I
isub
putfield org/tukaani/xz/lz/LZEncoder/readLimit I
L3:
aload 0
invokespecial org/tukaani/xz/lz/LZEncoder/processPendingBytes()V
iload 4
ireturn
.limit locals 5
.limit stack 5
.end method

.method public getAvail()I
getstatic org/tukaani/xz/lz/LZEncoder/$assertionsDisabled Z
ifne L0
aload 0
invokevirtual org/tukaani/xz/lz/LZEncoder/isStarted()Z
ifne L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
aload 0
getfield org/tukaani/xz/lz/LZEncoder/writePos I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
isub
ireturn
.limit locals 1
.limit stack 2
.end method

.method public getByte(I)I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/buf [B
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
iload 1
isub
baload
sipush 255
iand
ireturn
.limit locals 2
.limit stack 3
.end method

.method public getByte(II)I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/buf [B
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
iload 1
iadd
iload 2
isub
baload
sipush 255
iand
ireturn
.limit locals 3
.limit stack 3
.end method

.method public getMatchLen(II)I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
istore 4
iconst_0
istore 3
L0:
iload 3
iload 2
if_icmpge L1
aload 0
getfield org/tukaani/xz/lz/LZEncoder/buf [B
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
iload 3
iadd
baload
aload 0
getfield org/tukaani/xz/lz/LZEncoder/buf [B
iload 4
iload 1
isub
iconst_1
isub
iload 3
iadd
baload
if_icmpne L1
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
iload 3
ireturn
.limit locals 5
.limit stack 4
.end method

.method public getMatchLen(III)I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
iload 1
iadd
istore 4
iconst_0
istore 1
L0:
iload 1
iload 3
if_icmpge L1
aload 0
getfield org/tukaani/xz/lz/LZEncoder/buf [B
iload 4
iload 1
iadd
baload
aload 0
getfield org/tukaani/xz/lz/LZEncoder/buf [B
iload 4
iload 2
isub
iconst_1
isub
iload 1
iadd
baload
if_icmpne L1
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 1
ireturn
.limit locals 5
.limit stack 4
.end method

.method public abstract getMatches()Lorg/tukaani/xz/lz/Matches;
.end method

.method public getPos()I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public hasEnoughData(I)Z
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
iload 1
isub
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readLimit I
if_icmpge L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public isStarted()Z
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
iconst_m1
if_icmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method movePos(II)I
getstatic org/tukaani/xz/lz/LZEncoder/$assertionsDisabled Z
ifne L0
iload 1
iload 2
if_icmpge L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
aload 0
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
iconst_1
iadd
putfield org/tukaani/xz/lz/LZEncoder/readPos I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/writePos I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/readPos I
isub
istore 4
iload 4
istore 3
iload 4
iload 1
if_icmpge L1
iload 4
iload 2
if_icmplt L2
iload 4
istore 3
aload 0
getfield org/tukaani/xz/lz/LZEncoder/finishing Z
ifne L1
L2:
aload 0
aload 0
getfield org/tukaani/xz/lz/LZEncoder/pendingSize I
iconst_1
iadd
putfield org/tukaani/xz/lz/LZEncoder/pendingSize I
iconst_0
istore 3
L1:
iload 3
ireturn
.limit locals 5
.limit stack 3
.end method

.method public setFinishing()V
aload 0
aload 0
getfield org/tukaani/xz/lz/LZEncoder/writePos I
iconst_1
isub
putfield org/tukaani/xz/lz/LZEncoder/readLimit I
aload 0
iconst_1
putfield org/tukaani/xz/lz/LZEncoder/finishing Z
aload 0
invokespecial org/tukaani/xz/lz/LZEncoder/processPendingBytes()V
return
.limit locals 1
.limit stack 3
.end method

.method public setFlushing()V
aload 0
aload 0
getfield org/tukaani/xz/lz/LZEncoder/writePos I
iconst_1
isub
putfield org/tukaani/xz/lz/LZEncoder/readLimit I
aload 0
invokespecial org/tukaani/xz/lz/LZEncoder/processPendingBytes()V
return
.limit locals 1
.limit stack 3
.end method

.method public setPresetDict(I[B)V
getstatic org/tukaani/xz/lz/LZEncoder/$assertionsDisabled Z
ifne L0
aload 0
invokevirtual org/tukaani/xz/lz/LZEncoder/isStarted()Z
ifeq L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
getstatic org/tukaani/xz/lz/LZEncoder/$assertionsDisabled Z
ifne L1
aload 0
getfield org/tukaani/xz/lz/LZEncoder/writePos I
ifeq L1
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L1:
aload 2
ifnull L2
aload 2
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
istore 1
aload 2
aload 2
arraylength
iload 1
isub
aload 0
getfield org/tukaani/xz/lz/LZEncoder/buf [B
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 0
getfield org/tukaani/xz/lz/LZEncoder/writePos I
iload 1
iadd
putfield org/tukaani/xz/lz/LZEncoder/writePos I
aload 0
iload 1
invokevirtual org/tukaani/xz/lz/LZEncoder/skip(I)V
L2:
return
.limit locals 3
.limit stack 5
.end method

.method public abstract skip(I)V
.end method

.method public verifyMatches(Lorg/tukaani/xz/lz/Matches;)Z
aload 0
invokevirtual org/tukaani/xz/lz/LZEncoder/getAvail()I
aload 0
getfield org/tukaani/xz/lz/LZEncoder/matchLenMax I
invokestatic java/lang/Math/min(II)I
istore 3
iconst_0
istore 2
L0:
iload 2
aload 1
getfield org/tukaani/xz/lz/Matches/count I
if_icmpge L1
aload 0
aload 1
getfield org/tukaani/xz/lz/Matches/dist [I
iload 2
iaload
iload 3
invokevirtual org/tukaani/xz/lz/LZEncoder/getMatchLen(II)I
aload 1
getfield org/tukaani/xz/lz/Matches/len [I
iload 2
iaload
if_icmpeq L2
iconst_0
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_1
ireturn
.limit locals 4
.limit stack 3
.end method
