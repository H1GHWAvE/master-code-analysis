.bytecode 50.0
.class final synchronized org/tukaani/xz/lz/BT4
.super org/tukaani/xz/lz/LZEncoder

.field private 'cyclicPos' I

.field private final 'cyclicSize' I

.field private final 'depthLimit' I

.field private final 'hash' Lorg/tukaani/xz/lz/Hash234;

.field private 'lzPos' I

.field private final 'matches' Lorg/tukaani/xz/lz/Matches;

.field private final 'tree' [I

.method <init>(IIIIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
iload 5
invokespecial org/tukaani/xz/lz/LZEncoder/<init>(IIIII)V
aload 0
iconst_m1
putfield org/tukaani/xz/lz/BT4/cyclicPos I
aload 0
iload 1
iconst_1
iadd
putfield org/tukaani/xz/lz/BT4/cyclicSize I
aload 0
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicSize I
putfield org/tukaani/xz/lz/BT4/lzPos I
aload 0
new org/tukaani/xz/lz/Hash234
dup
iload 1
invokespecial org/tukaani/xz/lz/Hash234/<init>(I)V
putfield org/tukaani/xz/lz/BT4/hash Lorg/tukaani/xz/lz/Hash234;
aload 0
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicSize I
iconst_2
imul
newarray int
putfield org/tukaani/xz/lz/BT4/tree [I
aload 0
new org/tukaani/xz/lz/Matches
dup
iload 4
iconst_1
isub
invokespecial org/tukaani/xz/lz/Matches/<init>(I)V
putfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
iload 6
ifle L0
L1:
aload 0
iload 6
putfield org/tukaani/xz/lz/BT4/depthLimit I
return
L0:
iload 4
iconst_2
idiv
bipush 16
iadd
istore 6
goto L1
.limit locals 7
.limit stack 6
.end method

.method static getMemoryUsage(I)I
iload 0
invokestatic org/tukaani/xz/lz/Hash234/getMemoryUsage(I)I
iload 0
sipush 128
idiv
iadd
bipush 10
iadd
ireturn
.limit locals 1
.limit stack 3
.end method

.method private movePos()I
aload 0
aload 0
getfield org/tukaani/xz/lz/BT4/niceLen I
iconst_4
invokevirtual org/tukaani/xz/lz/BT4/movePos(II)I
istore 1
iload 1
ifeq L0
aload 0
getfield org/tukaani/xz/lz/BT4/lzPos I
iconst_1
iadd
istore 2
aload 0
iload 2
putfield org/tukaani/xz/lz/BT4/lzPos I
iload 2
ldc_w 2147483647
if_icmpne L1
ldc_w 2147483647
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicSize I
isub
istore 2
aload 0
getfield org/tukaani/xz/lz/BT4/hash Lorg/tukaani/xz/lz/Hash234;
iload 2
invokevirtual org/tukaani/xz/lz/Hash234/normalize(I)V
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 2
invokestatic org/tukaani/xz/lz/BT4/normalize([II)V
aload 0
aload 0
getfield org/tukaani/xz/lz/BT4/lzPos I
iload 2
isub
putfield org/tukaani/xz/lz/BT4/lzPos I
L1:
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicPos I
iconst_1
iadd
istore 2
aload 0
iload 2
putfield org/tukaani/xz/lz/BT4/cyclicPos I
iload 2
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicSize I
if_icmpne L0
aload 0
iconst_0
putfield org/tukaani/xz/lz/BT4/cyclicPos I
L0:
iload 1
ireturn
.limit locals 3
.limit stack 3
.end method

.method private skip(II)V
aload 0
getfield org/tukaani/xz/lz/BT4/depthLimit I
istore 3
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicPos I
iconst_1
ishl
iconst_1
iadd
istore 7
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicPos I
iconst_1
ishl
istore 4
iconst_0
istore 6
iconst_0
istore 5
iload 2
istore 8
L0:
aload 0
getfield org/tukaani/xz/lz/BT4/lzPos I
iload 8
isub
istore 11
iload 3
ifeq L1
iload 11
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicSize I
if_icmplt L2
L1:
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 7
iconst_0
iastore
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 4
iconst_0
iastore
return
L2:
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicPos I
istore 9
iload 11
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicPos I
if_icmple L3
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicSize I
istore 2
L4:
iload 2
iload 9
iload 11
isub
iadd
iconst_1
ishl
istore 10
iload 6
iload 5
invokestatic java/lang/Math/min(II)I
istore 9
iload 9
istore 2
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 9
iadd
iload 11
isub
baload
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 9
iadd
baload
if_icmpne L5
L6:
iload 9
iconst_1
iadd
istore 2
iload 2
iload 1
if_icmpne L7
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 4
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 10
iaload
iastore
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 7
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 10
iconst_1
iadd
iaload
iastore
return
L3:
iconst_0
istore 2
goto L4
L7:
iload 2
istore 9
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 2
iadd
iload 11
isub
baload
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 2
iadd
baload
if_icmpeq L6
L5:
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 2
iadd
iload 11
isub
baload
sipush 255
iand
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 2
iadd
baload
sipush 255
iand
if_icmpge L8
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 4
iload 8
iastore
iload 10
iconst_1
iadd
istore 4
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 4
iaload
istore 8
iload 2
istore 5
iload 8
istore 2
L9:
iload 3
iconst_1
isub
istore 3
iload 2
istore 8
goto L0
L8:
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 7
iload 8
iastore
iload 10
istore 7
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 7
iaload
istore 8
iload 2
istore 6
iload 8
istore 2
goto L9
.limit locals 12
.limit stack 5
.end method

.method public getMatches()Lorg/tukaani/xz/lz/Matches;
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
iconst_0
putfield org/tukaani/xz/lz/Matches/count I
aload 0
getfield org/tukaani/xz/lz/BT4/matchLenMax I
istore 2
aload 0
getfield org/tukaani/xz/lz/BT4/niceLen I
istore 5
aload 0
invokespecial org/tukaani/xz/lz/BT4/movePos()I
istore 1
iload 2
istore 3
iload 5
istore 4
iload 1
iload 2
if_icmpge L0
iload 1
ifne L1
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
areturn
L1:
iload 1
istore 2
iload 2
istore 3
iload 5
istore 4
iload 5
iload 1
if_icmple L0
iload 1
istore 4
iload 2
istore 3
L0:
aload 0
getfield org/tukaani/xz/lz/BT4/hash Lorg/tukaani/xz/lz/Hash234;
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
invokevirtual org/tukaani/xz/lz/Hash234/calcHashes([BI)V
aload 0
getfield org/tukaani/xz/lz/BT4/lzPos I
aload 0
getfield org/tukaani/xz/lz/BT4/hash Lorg/tukaani/xz/lz/Hash234;
invokevirtual org/tukaani/xz/lz/Hash234/getHash2Pos()I
isub
istore 7
aload 0
getfield org/tukaani/xz/lz/BT4/lzPos I
aload 0
getfield org/tukaani/xz/lz/BT4/hash Lorg/tukaani/xz/lz/Hash234;
invokevirtual org/tukaani/xz/lz/Hash234/getHash3Pos()I
isub
istore 6
aload 0
getfield org/tukaani/xz/lz/BT4/hash Lorg/tukaani/xz/lz/Hash234;
invokevirtual org/tukaani/xz/lz/Hash234/getHash4Pos()I
istore 11
aload 0
getfield org/tukaani/xz/lz/BT4/hash Lorg/tukaani/xz/lz/Hash234;
aload 0
getfield org/tukaani/xz/lz/BT4/lzPos I
invokevirtual org/tukaani/xz/lz/Hash234/updateTables(I)V
iconst_0
istore 1
iload 1
istore 2
iload 7
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicSize I
if_icmpge L2
iload 1
istore 2
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 7
isub
baload
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
baload
if_icmpne L2
iconst_2
istore 2
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
iconst_0
iconst_2
iastore
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/dist [I
iconst_0
iload 7
iconst_1
isub
iastore
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
iconst_1
putfield org/tukaani/xz/lz/Matches/count I
L2:
iload 7
istore 5
iload 2
istore 1
iload 7
iload 6
if_icmpeq L3
iload 7
istore 5
iload 2
istore 1
iload 6
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicSize I
if_icmpge L3
iload 7
istore 5
iload 2
istore 1
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 6
isub
baload
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
baload
if_icmpne L3
iconst_3
istore 1
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/dist [I
astore 15
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
astore 16
aload 16
getfield org/tukaani/xz/lz/Matches/count I
istore 2
aload 16
iload 2
iconst_1
iadd
putfield org/tukaani/xz/lz/Matches/count I
aload 15
iload 2
iload 6
iconst_1
isub
iastore
iload 6
istore 5
L3:
iload 1
istore 2
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
ifle L4
L5:
iload 1
iload 3
if_icmpge L6
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 1
iadd
iload 5
isub
baload
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 1
iadd
baload
if_icmpne L6
iload 1
iconst_1
iadd
istore 1
goto L5
L6:
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
iload 1
iastore
iload 1
istore 2
iload 1
iload 4
if_icmplt L4
aload 0
iload 4
iload 11
invokespecial org/tukaani/xz/lz/BT4/skip(II)V
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
areturn
L4:
iload 2
istore 1
iload 2
iconst_3
if_icmpge L7
iconst_3
istore 1
L7:
aload 0
getfield org/tukaani/xz/lz/BT4/depthLimit I
istore 6
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicPos I
iconst_1
ishl
iconst_1
iadd
istore 8
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicPos I
iconst_1
ishl
istore 5
iconst_0
istore 9
iconst_0
istore 7
iload 1
istore 12
L8:
aload 0
getfield org/tukaani/xz/lz/BT4/lzPos I
iload 11
isub
istore 14
iload 6
ifeq L9
iload 14
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicSize I
if_icmplt L10
L9:
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 8
iconst_0
iastore
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 5
iconst_0
iastore
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
areturn
L10:
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicPos I
istore 2
iload 14
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicPos I
if_icmple L11
aload 0
getfield org/tukaani/xz/lz/BT4/cyclicSize I
istore 1
L12:
iload 1
iload 2
iload 14
isub
iadd
iconst_1
ishl
istore 13
iload 9
iload 7
invokestatic java/lang/Math/min(II)I
istore 2
iload 2
istore 1
iload 12
istore 10
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 2
iadd
iload 14
isub
baload
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 2
iadd
baload
if_icmpne L13
iload 2
istore 1
L14:
iload 1
iconst_1
iadd
istore 2
iload 2
iload 3
if_icmpge L15
iload 2
istore 1
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 2
iadd
iload 14
isub
baload
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 2
iadd
baload
if_icmpeq L14
L15:
iload 2
istore 1
iload 12
istore 10
iload 2
iload 12
if_icmple L13
iload 2
istore 10
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iload 2
iastore
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/dist [I
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iload 14
iconst_1
isub
iastore
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
astore 15
aload 15
aload 15
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
iadd
putfield org/tukaani/xz/lz/Matches/count I
iload 2
istore 1
iload 2
iload 4
if_icmplt L13
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 5
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 13
iaload
iastore
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 8
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 13
iconst_1
iadd
iaload
iastore
aload 0
getfield org/tukaani/xz/lz/BT4/matches Lorg/tukaani/xz/lz/Matches;
areturn
L11:
iconst_0
istore 1
goto L12
L13:
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 1
iadd
iload 14
isub
baload
sipush 255
iand
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
iload 1
iadd
baload
sipush 255
iand
if_icmpge L16
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 5
iload 11
iastore
iload 13
iconst_1
iadd
istore 5
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 5
iaload
istore 2
iload 1
istore 7
iload 2
istore 1
L17:
iload 6
iconst_1
isub
istore 6
iload 1
istore 11
iload 10
istore 12
goto L8
L16:
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 8
iload 11
iastore
iload 13
istore 8
aload 0
getfield org/tukaani/xz/lz/BT4/tree [I
iload 8
iaload
istore 2
iload 1
istore 9
iload 2
istore 1
goto L17
.limit locals 17
.limit stack 5
.end method

.method public skip(I)V
L0:
iload 1
iconst_1
isub
istore 2
iload 1
ifle L1
aload 0
getfield org/tukaani/xz/lz/BT4/niceLen I
istore 3
aload 0
invokespecial org/tukaani/xz/lz/BT4/movePos()I
istore 4
iload 3
istore 1
iload 4
iload 3
if_icmpge L2
iload 4
ifne L3
iload 2
istore 1
goto L0
L3:
iload 4
istore 1
L2:
aload 0
getfield org/tukaani/xz/lz/BT4/hash Lorg/tukaani/xz/lz/Hash234;
aload 0
getfield org/tukaani/xz/lz/BT4/buf [B
aload 0
getfield org/tukaani/xz/lz/BT4/readPos I
invokevirtual org/tukaani/xz/lz/Hash234/calcHashes([BI)V
aload 0
getfield org/tukaani/xz/lz/BT4/hash Lorg/tukaani/xz/lz/Hash234;
invokevirtual org/tukaani/xz/lz/Hash234/getHash4Pos()I
istore 3
aload 0
getfield org/tukaani/xz/lz/BT4/hash Lorg/tukaani/xz/lz/Hash234;
aload 0
getfield org/tukaani/xz/lz/BT4/lzPos I
invokevirtual org/tukaani/xz/lz/Hash234/updateTables(I)V
aload 0
iload 1
iload 3
invokespecial org/tukaani/xz/lz/BT4/skip(II)V
iload 2
istore 1
goto L0
L1:
return
.limit locals 5
.limit stack 3
.end method
