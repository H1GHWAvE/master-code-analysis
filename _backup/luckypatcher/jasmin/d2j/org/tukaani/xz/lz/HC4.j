.bytecode 50.0
.class final synchronized org/tukaani/xz/lz/HC4
.super org/tukaani/xz/lz/LZEncoder

.field static final synthetic '$assertionsDisabled' Z

.field private final 'chain' [I

.field private 'cyclicPos' I

.field private final 'cyclicSize' I

.field private final 'depthLimit' I

.field private final 'hash' Lorg/tukaani/xz/lz/Hash234;

.field private 'lzPos' I

.field private final 'matches' Lorg/tukaani/xz/lz/Matches;

.method static <clinit>()V
ldc org/tukaani/xz/lz/HC4
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/lz/HC4/$assertionsDisabled Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method <init>(IIIIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
iload 5
invokespecial org/tukaani/xz/lz/LZEncoder/<init>(IIIII)V
aload 0
iconst_m1
putfield org/tukaani/xz/lz/HC4/cyclicPos I
aload 0
new org/tukaani/xz/lz/Hash234
dup
iload 1
invokespecial org/tukaani/xz/lz/Hash234/<init>(I)V
putfield org/tukaani/xz/lz/HC4/hash Lorg/tukaani/xz/lz/Hash234;
aload 0
iload 1
iconst_1
iadd
putfield org/tukaani/xz/lz/HC4/cyclicSize I
aload 0
aload 0
getfield org/tukaani/xz/lz/HC4/cyclicSize I
newarray int
putfield org/tukaani/xz/lz/HC4/chain [I
aload 0
aload 0
getfield org/tukaani/xz/lz/HC4/cyclicSize I
putfield org/tukaani/xz/lz/HC4/lzPos I
aload 0
new org/tukaani/xz/lz/Matches
dup
iload 4
iconst_1
isub
invokespecial org/tukaani/xz/lz/Matches/<init>(I)V
putfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
iload 6
ifle L0
L1:
aload 0
iload 6
putfield org/tukaani/xz/lz/HC4/depthLimit I
return
L0:
iload 4
iconst_4
idiv
iconst_4
iadd
istore 6
goto L1
.limit locals 7
.limit stack 6
.end method

.method static getMemoryUsage(I)I
iload 0
invokestatic org/tukaani/xz/lz/Hash234/getMemoryUsage(I)I
iload 0
sipush 256
idiv
iadd
bipush 10
iadd
ireturn
.limit locals 1
.limit stack 3
.end method

.method private movePos()I
aload 0
iconst_4
iconst_4
invokevirtual org/tukaani/xz/lz/HC4/movePos(II)I
istore 1
iload 1
ifeq L0
aload 0
getfield org/tukaani/xz/lz/HC4/lzPos I
iconst_1
iadd
istore 2
aload 0
iload 2
putfield org/tukaani/xz/lz/HC4/lzPos I
iload 2
ldc_w 2147483647
if_icmpne L1
ldc_w 2147483647
aload 0
getfield org/tukaani/xz/lz/HC4/cyclicSize I
isub
istore 2
aload 0
getfield org/tukaani/xz/lz/HC4/hash Lorg/tukaani/xz/lz/Hash234;
iload 2
invokevirtual org/tukaani/xz/lz/Hash234/normalize(I)V
aload 0
getfield org/tukaani/xz/lz/HC4/chain [I
iload 2
invokestatic org/tukaani/xz/lz/HC4/normalize([II)V
aload 0
aload 0
getfield org/tukaani/xz/lz/HC4/lzPos I
iload 2
isub
putfield org/tukaani/xz/lz/HC4/lzPos I
L1:
aload 0
getfield org/tukaani/xz/lz/HC4/cyclicPos I
iconst_1
iadd
istore 2
aload 0
iload 2
putfield org/tukaani/xz/lz/HC4/cyclicPos I
iload 2
aload 0
getfield org/tukaani/xz/lz/HC4/cyclicSize I
if_icmpne L0
aload 0
iconst_0
putfield org/tukaani/xz/lz/HC4/cyclicPos I
L0:
iload 1
ireturn
.limit locals 3
.limit stack 3
.end method

.method public getMatches()Lorg/tukaani/xz/lz/Matches;
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
iconst_0
putfield org/tukaani/xz/lz/Matches/count I
aload 0
getfield org/tukaani/xz/lz/HC4/matchLenMax I
istore 2
aload 0
getfield org/tukaani/xz/lz/HC4/niceLen I
istore 5
aload 0
invokespecial org/tukaani/xz/lz/HC4/movePos()I
istore 1
iload 2
istore 3
iload 5
istore 4
iload 1
iload 2
if_icmpge L0
iload 1
ifne L1
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
areturn
L1:
iload 1
istore 2
iload 2
istore 3
iload 5
istore 4
iload 5
iload 1
if_icmple L0
iload 1
istore 4
iload 2
istore 3
L0:
aload 0
getfield org/tukaani/xz/lz/HC4/hash Lorg/tukaani/xz/lz/Hash234;
aload 0
getfield org/tukaani/xz/lz/HC4/buf [B
aload 0
getfield org/tukaani/xz/lz/HC4/readPos I
invokevirtual org/tukaani/xz/lz/Hash234/calcHashes([BI)V
aload 0
getfield org/tukaani/xz/lz/HC4/lzPos I
aload 0
getfield org/tukaani/xz/lz/HC4/hash Lorg/tukaani/xz/lz/Hash234;
invokevirtual org/tukaani/xz/lz/Hash234/getHash2Pos()I
isub
istore 8
aload 0
getfield org/tukaani/xz/lz/HC4/lzPos I
aload 0
getfield org/tukaani/xz/lz/HC4/hash Lorg/tukaani/xz/lz/Hash234;
invokevirtual org/tukaani/xz/lz/Hash234/getHash3Pos()I
isub
istore 7
aload 0
getfield org/tukaani/xz/lz/HC4/hash Lorg/tukaani/xz/lz/Hash234;
invokevirtual org/tukaani/xz/lz/Hash234/getHash4Pos()I
istore 6
aload 0
getfield org/tukaani/xz/lz/HC4/hash Lorg/tukaani/xz/lz/Hash234;
aload 0
getfield org/tukaani/xz/lz/HC4/lzPos I
invokevirtual org/tukaani/xz/lz/Hash234/updateTables(I)V
aload 0
getfield org/tukaani/xz/lz/HC4/chain [I
aload 0
getfield org/tukaani/xz/lz/HC4/cyclicPos I
iload 6
iastore
iconst_0
istore 1
iload 1
istore 2
iload 8
aload 0
getfield org/tukaani/xz/lz/HC4/cyclicSize I
if_icmpge L2
iload 1
istore 2
aload 0
getfield org/tukaani/xz/lz/HC4/buf [B
aload 0
getfield org/tukaani/xz/lz/HC4/readPos I
iload 8
isub
baload
aload 0
getfield org/tukaani/xz/lz/HC4/buf [B
aload 0
getfield org/tukaani/xz/lz/HC4/readPos I
baload
if_icmpne L2
iconst_2
istore 2
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
iconst_0
iconst_2
iastore
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/dist [I
iconst_0
iload 8
iconst_1
isub
iastore
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
iconst_1
putfield org/tukaani/xz/lz/Matches/count I
L2:
iload 8
istore 5
iload 2
istore 1
iload 8
iload 7
if_icmpeq L3
iload 8
istore 5
iload 2
istore 1
iload 7
aload 0
getfield org/tukaani/xz/lz/HC4/cyclicSize I
if_icmpge L3
iload 8
istore 5
iload 2
istore 1
aload 0
getfield org/tukaani/xz/lz/HC4/buf [B
aload 0
getfield org/tukaani/xz/lz/HC4/readPos I
iload 7
isub
baload
aload 0
getfield org/tukaani/xz/lz/HC4/buf [B
aload 0
getfield org/tukaani/xz/lz/HC4/readPos I
baload
if_icmpne L3
iconst_3
istore 1
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/dist [I
astore 9
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
astore 10
aload 10
getfield org/tukaani/xz/lz/Matches/count I
istore 2
aload 10
iload 2
iconst_1
iadd
putfield org/tukaani/xz/lz/Matches/count I
aload 9
iload 2
iload 7
iconst_1
isub
iastore
iload 7
istore 5
L3:
iload 1
istore 2
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
ifle L4
L5:
iload 1
iload 3
if_icmpge L6
aload 0
getfield org/tukaani/xz/lz/HC4/buf [B
aload 0
getfield org/tukaani/xz/lz/HC4/readPos I
iload 1
iadd
iload 5
isub
baload
aload 0
getfield org/tukaani/xz/lz/HC4/buf [B
aload 0
getfield org/tukaani/xz/lz/HC4/readPos I
iload 1
iadd
baload
if_icmpne L6
iload 1
iconst_1
iadd
istore 1
goto L5
L6:
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
iload 1
iastore
iload 1
istore 2
iload 1
iload 4
if_icmplt L4
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
areturn
L4:
iload 2
istore 1
iload 2
iconst_3
if_icmpge L7
iconst_3
istore 1
L7:
aload 0
getfield org/tukaani/xz/lz/HC4/depthLimit I
istore 5
iload 1
istore 2
iload 5
istore 1
iload 6
istore 5
L8:
aload 0
getfield org/tukaani/xz/lz/HC4/lzPos I
iload 5
isub
istore 8
iload 1
ifeq L9
iload 8
aload 0
getfield org/tukaani/xz/lz/HC4/cyclicSize I
if_icmplt L10
L9:
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
areturn
L10:
aload 0
getfield org/tukaani/xz/lz/HC4/chain [I
astore 9
aload 0
getfield org/tukaani/xz/lz/HC4/cyclicPos I
istore 6
iload 8
aload 0
getfield org/tukaani/xz/lz/HC4/cyclicPos I
if_icmple L11
aload 0
getfield org/tukaani/xz/lz/HC4/cyclicSize I
istore 5
L12:
aload 9
iload 5
iload 6
iload 8
isub
iadd
iaload
istore 7
iload 2
istore 6
aload 0
getfield org/tukaani/xz/lz/HC4/buf [B
aload 0
getfield org/tukaani/xz/lz/HC4/readPos I
iload 2
iadd
iload 8
isub
baload
aload 0
getfield org/tukaani/xz/lz/HC4/buf [B
aload 0
getfield org/tukaani/xz/lz/HC4/readPos I
iload 2
iadd
baload
if_icmpne L13
iload 2
istore 6
aload 0
getfield org/tukaani/xz/lz/HC4/buf [B
aload 0
getfield org/tukaani/xz/lz/HC4/readPos I
iload 8
isub
baload
aload 0
getfield org/tukaani/xz/lz/HC4/buf [B
aload 0
getfield org/tukaani/xz/lz/HC4/readPos I
baload
if_icmpne L13
iconst_0
istore 6
L14:
iload 6
iconst_1
iadd
istore 5
iload 5
iload 3
if_icmpge L15
iload 5
istore 6
aload 0
getfield org/tukaani/xz/lz/HC4/buf [B
aload 0
getfield org/tukaani/xz/lz/HC4/readPos I
iload 5
iadd
iload 8
isub
baload
aload 0
getfield org/tukaani/xz/lz/HC4/buf [B
aload 0
getfield org/tukaani/xz/lz/HC4/readPos I
iload 5
iadd
baload
if_icmpeq L14
L15:
iload 2
istore 6
iload 5
iload 2
if_icmple L13
iload 5
istore 6
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iload 5
iastore
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/dist [I
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iload 8
iconst_1
isub
iastore
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
astore 9
aload 9
aload 9
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
iadd
putfield org/tukaani/xz/lz/Matches/count I
iload 5
iload 4
if_icmplt L13
aload 0
getfield org/tukaani/xz/lz/HC4/matches Lorg/tukaani/xz/lz/Matches;
areturn
L11:
iconst_0
istore 5
goto L12
L13:
iload 1
iconst_1
isub
istore 1
iload 7
istore 5
iload 6
istore 2
goto L8
.limit locals 11
.limit stack 4
.end method

.method public skip(I)V
iload 1
istore 2
getstatic org/tukaani/xz/lz/HC4/$assertionsDisabled Z
ifne L0
iload 1
istore 2
iload 1
ifge L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L1:
iload 2
iconst_1
isub
istore 1
iload 2
ifle L2
iload 1
istore 2
aload 0
invokespecial org/tukaani/xz/lz/HC4/movePos()I
ifeq L0
aload 0
getfield org/tukaani/xz/lz/HC4/hash Lorg/tukaani/xz/lz/Hash234;
aload 0
getfield org/tukaani/xz/lz/HC4/buf [B
aload 0
getfield org/tukaani/xz/lz/HC4/readPos I
invokevirtual org/tukaani/xz/lz/Hash234/calcHashes([BI)V
aload 0
getfield org/tukaani/xz/lz/HC4/chain [I
aload 0
getfield org/tukaani/xz/lz/HC4/cyclicPos I
aload 0
getfield org/tukaani/xz/lz/HC4/hash Lorg/tukaani/xz/lz/Hash234;
invokevirtual org/tukaani/xz/lz/Hash234/getHash4Pos()I
iastore
aload 0
getfield org/tukaani/xz/lz/HC4/hash Lorg/tukaani/xz/lz/Hash234;
aload 0
getfield org/tukaani/xz/lz/HC4/lzPos I
invokevirtual org/tukaani/xz/lz/Hash234/updateTables(I)V
iload 1
istore 2
goto L1
L2:
return
L0:
goto L1
.limit locals 3
.limit stack 3
.end method
