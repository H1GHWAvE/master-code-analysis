.bytecode 50.0
.class synchronized org/tukaani/xz/LZMA2Encoder
.super org/tukaani/xz/LZMA2Coder
.implements org/tukaani/xz/FilterEncoder

.field private final 'options' Lorg/tukaani/xz/LZMA2Options;

.field private final 'props' [B

.method <init>(Lorg/tukaani/xz/LZMA2Options;)V
aload 0
invokespecial org/tukaani/xz/LZMA2Coder/<init>()V
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/LZMA2Encoder/props [B
aload 1
invokevirtual org/tukaani/xz/LZMA2Options/getPresetDict()[B
ifnull L0
new java/lang/IllegalArgumentException
dup
ldc "XZ doesn't support a preset dictionary for now"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual org/tukaani/xz/LZMA2Options/getMode()I
ifne L1
aload 0
getfield org/tukaani/xz/LZMA2Encoder/props [B
iconst_0
iconst_0
bastore
L2:
aload 0
aload 1
invokevirtual org/tukaani/xz/LZMA2Options/clone()Ljava/lang/Object;
checkcast org/tukaani/xz/LZMA2Options
putfield org/tukaani/xz/LZMA2Encoder/options Lorg/tukaani/xz/LZMA2Options;
return
L1:
aload 1
invokevirtual org/tukaani/xz/LZMA2Options/getDictSize()I
sipush 4096
invokestatic java/lang/Math/max(II)I
istore 2
aload 0
getfield org/tukaani/xz/LZMA2Encoder/props [B
iconst_0
iload 2
iconst_1
isub
invokestatic org/tukaani/xz/lzma/LZMAEncoder/getDistSlot(I)I
bipush 23
isub
i2b
bastore
goto L2
.limit locals 3
.limit stack 4
.end method

.method public getFilterID()J
ldc2_w 33L
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getFilterProps()[B
aload 0
getfield org/tukaani/xz/LZMA2Encoder/props [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;
aload 0
getfield org/tukaani/xz/LZMA2Encoder/options Lorg/tukaani/xz/LZMA2Options;
aload 1
invokevirtual org/tukaani/xz/LZMA2Options/getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;
areturn
.limit locals 2
.limit stack 2
.end method

.method public supportsFlushing()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method
