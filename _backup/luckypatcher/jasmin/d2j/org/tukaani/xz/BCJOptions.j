.bytecode 50.0
.class synchronized abstract org/tukaani/xz/BCJOptions
.super org/tukaani/xz/FilterOptions

.field static final synthetic '$assertionsDisabled' Z

.field private final 'alignment' I

.field 'startOffset' I

.method static <clinit>()V
ldc org/tukaani/xz/BCJOptions
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/BCJOptions/$assertionsDisabled Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method <init>(I)V
aload 0
invokespecial org/tukaani/xz/FilterOptions/<init>()V
aload 0
iconst_0
putfield org/tukaani/xz/BCJOptions/startOffset I
aload 0
iload 1
putfield org/tukaani/xz/BCJOptions/alignment I
return
.limit locals 2
.limit stack 2
.end method

.method public clone()Ljava/lang/Object;
.catch java/lang/CloneNotSupportedException from L0 to L1 using L2
L0:
aload 0
invokespecial java/lang/Object/clone()Ljava/lang/Object;
astore 1
L1:
aload 1
areturn
L2:
astore 1
getstatic org/tukaani/xz/BCJOptions/$assertionsDisabled Z
ifne L3
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L3:
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public getDecoderMemoryUsage()I
invokestatic org/tukaani/xz/SimpleInputStream/getMemoryUsage()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getEncoderMemoryUsage()I
invokestatic org/tukaani/xz/SimpleOutputStream/getMemoryUsage()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getStartOffset()I
aload 0
getfield org/tukaani/xz/BCJOptions/startOffset I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setStartOffset(I)V
.throws org/tukaani/xz/UnsupportedOptionsException
aload 0
getfield org/tukaani/xz/BCJOptions/alignment I
iconst_1
isub
iload 1
iand
ifeq L0
new org/tukaani/xz/UnsupportedOptionsException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Start offset must be a multiple of "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield org/tukaani/xz/BCJOptions/alignment I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
putfield org/tukaani/xz/BCJOptions/startOffset I
return
.limit locals 2
.limit stack 4
.end method
