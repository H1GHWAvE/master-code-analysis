.bytecode 50.0
.class public synchronized org/tukaani/xz/SingleXZInputStream
.super java/io/InputStream

.field private 'blockDecoder' Lorg/tukaani/xz/BlockInputStream;

.field private 'check' Lorg/tukaani/xz/check/Check;

.field private 'endReached' Z

.field private 'exception' Ljava/io/IOException;

.field private 'in' Ljava/io/InputStream;

.field private final 'indexHash' Lorg/tukaani/xz/index/IndexHash;

.field private 'memoryLimit' I

.field private 'streamHeaderFlags' Lorg/tukaani/xz/common/StreamFlags;

.field private final 'tempBuf' [B

.method public <init>(Ljava/io/InputStream;)V
.throws java/io/IOException
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
aconst_null
putfield org/tukaani/xz/SingleXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
aload 0
new org/tukaani/xz/index/IndexHash
dup
invokespecial org/tukaani/xz/index/IndexHash/<init>()V
putfield org/tukaani/xz/SingleXZInputStream/indexHash Lorg/tukaani/xz/index/IndexHash;
aload 0
iconst_0
putfield org/tukaani/xz/SingleXZInputStream/endReached Z
aload 0
aconst_null
putfield org/tukaani/xz/SingleXZInputStream/exception Ljava/io/IOException;
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/SingleXZInputStream/tempBuf [B
aload 0
aload 1
iconst_m1
invokespecial org/tukaani/xz/SingleXZInputStream/initialize(Ljava/io/InputStream;I)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/io/InputStream;I)V
.throws java/io/IOException
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
aconst_null
putfield org/tukaani/xz/SingleXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
aload 0
new org/tukaani/xz/index/IndexHash
dup
invokespecial org/tukaani/xz/index/IndexHash/<init>()V
putfield org/tukaani/xz/SingleXZInputStream/indexHash Lorg/tukaani/xz/index/IndexHash;
aload 0
iconst_0
putfield org/tukaani/xz/SingleXZInputStream/endReached Z
aload 0
aconst_null
putfield org/tukaani/xz/SingleXZInputStream/exception Ljava/io/IOException;
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/SingleXZInputStream/tempBuf [B
aload 0
aload 1
iload 2
invokespecial org/tukaani/xz/SingleXZInputStream/initialize(Ljava/io/InputStream;I)V
return
.limit locals 3
.limit stack 3
.end method

.method <init>(Ljava/io/InputStream;I[B)V
.throws java/io/IOException
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
aconst_null
putfield org/tukaani/xz/SingleXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
aload 0
new org/tukaani/xz/index/IndexHash
dup
invokespecial org/tukaani/xz/index/IndexHash/<init>()V
putfield org/tukaani/xz/SingleXZInputStream/indexHash Lorg/tukaani/xz/index/IndexHash;
aload 0
iconst_0
putfield org/tukaani/xz/SingleXZInputStream/endReached Z
aload 0
aconst_null
putfield org/tukaani/xz/SingleXZInputStream/exception Ljava/io/IOException;
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/SingleXZInputStream/tempBuf [B
aload 0
aload 1
iload 2
aload 3
invokespecial org/tukaani/xz/SingleXZInputStream/initialize(Ljava/io/InputStream;I[B)V
return
.limit locals 4
.limit stack 4
.end method

.method private initialize(Ljava/io/InputStream;I)V
.throws java/io/IOException
bipush 12
newarray byte
astore 3
new java/io/DataInputStream
dup
aload 1
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
aload 3
invokevirtual java/io/DataInputStream/readFully([B)V
aload 0
aload 1
iload 2
aload 3
invokespecial org/tukaani/xz/SingleXZInputStream/initialize(Ljava/io/InputStream;I[B)V
return
.limit locals 4
.limit stack 4
.end method

.method private initialize(Ljava/io/InputStream;I[B)V
.throws java/io/IOException
aload 0
aload 1
putfield org/tukaani/xz/SingleXZInputStream/in Ljava/io/InputStream;
aload 0
iload 2
putfield org/tukaani/xz/SingleXZInputStream/memoryLimit I
aload 0
aload 3
invokestatic org/tukaani/xz/common/DecoderUtil/decodeStreamHeader([B)Lorg/tukaani/xz/common/StreamFlags;
putfield org/tukaani/xz/SingleXZInputStream/streamHeaderFlags Lorg/tukaani/xz/common/StreamFlags;
aload 0
aload 0
getfield org/tukaani/xz/SingleXZInputStream/streamHeaderFlags Lorg/tukaani/xz/common/StreamFlags;
getfield org/tukaani/xz/common/StreamFlags/checkType I
invokestatic org/tukaani/xz/check/Check/getInstance(I)Lorg/tukaani/xz/check/Check;
putfield org/tukaani/xz/SingleXZInputStream/check Lorg/tukaani/xz/check/Check;
return
.limit locals 4
.limit stack 2
.end method

.method private validateStreamFooter()V
.throws java/io/IOException
bipush 12
newarray byte
astore 1
new java/io/DataInputStream
dup
aload 0
getfield org/tukaani/xz/SingleXZInputStream/in Ljava/io/InputStream;
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
aload 1
invokevirtual java/io/DataInputStream/readFully([B)V
aload 1
invokestatic org/tukaani/xz/common/DecoderUtil/decodeStreamFooter([B)Lorg/tukaani/xz/common/StreamFlags;
astore 1
aload 0
getfield org/tukaani/xz/SingleXZInputStream/streamHeaderFlags Lorg/tukaani/xz/common/StreamFlags;
aload 1
invokestatic org/tukaani/xz/common/DecoderUtil/areStreamFlagsEqual(Lorg/tukaani/xz/common/StreamFlags;Lorg/tukaani/xz/common/StreamFlags;)Z
ifeq L0
aload 0
getfield org/tukaani/xz/SingleXZInputStream/indexHash Lorg/tukaani/xz/index/IndexHash;
invokevirtual org/tukaani/xz/index/IndexHash/getIndexSize()J
aload 1
getfield org/tukaani/xz/common/StreamFlags/backwardSize J
lcmp
ifeq L1
L0:
new org/tukaani/xz/CorruptedInputException
dup
ldc "XZ Stream Footer does not match Stream Header"
invokespecial org/tukaani/xz/CorruptedInputException/<init>(Ljava/lang/String;)V
athrow
L1:
return
.limit locals 2
.limit stack 4
.end method

.method public available()I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SingleXZInputStream/in Ljava/io/InputStream;
ifnonnull L0
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/tukaani/xz/SingleXZInputStream/exception Ljava/io/IOException;
ifnull L1
aload 0
getfield org/tukaani/xz/SingleXZInputStream/exception Ljava/io/IOException;
athrow
L1:
aload 0
getfield org/tukaani/xz/SingleXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
ifnonnull L2
iconst_0
ireturn
L2:
aload 0
getfield org/tukaani/xz/SingleXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
invokevirtual org/tukaani/xz/BlockInputStream/available()I
ireturn
.limit locals 1
.limit stack 3
.end method

.method public close()V
.throws java/io/IOException
.catch all from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/SingleXZInputStream/in Ljava/io/InputStream;
ifnull L3
L0:
aload 0
getfield org/tukaani/xz/SingleXZInputStream/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/close()V
L1:
aload 0
aconst_null
putfield org/tukaani/xz/SingleXZInputStream/in Ljava/io/InputStream;
L3:
return
L2:
astore 1
aload 0
aconst_null
putfield org/tukaani/xz/SingleXZInputStream/in Ljava/io/InputStream;
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public getCheckName()Ljava/lang/String;
aload 0
getfield org/tukaani/xz/SingleXZInputStream/check Lorg/tukaani/xz/check/Check;
invokevirtual org/tukaani/xz/check/Check/getName()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getCheckType()I
aload 0
getfield org/tukaani/xz/SingleXZInputStream/streamHeaderFlags Lorg/tukaani/xz/common/StreamFlags;
getfield org/tukaani/xz/common/StreamFlags/checkType I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public read()I
.throws java/io/IOException
aload 0
aload 0
getfield org/tukaani/xz/SingleXZInputStream/tempBuf [B
iconst_0
iconst_1
invokevirtual org/tukaani/xz/SingleXZInputStream/read([BII)I
iconst_m1
if_icmpne L0
iconst_m1
ireturn
L0:
aload 0
getfield org/tukaani/xz/SingleXZInputStream/tempBuf [B
iconst_0
baload
sipush 255
iand
ireturn
.limit locals 1
.limit stack 4
.end method

.method public read([BII)I
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch org/tukaani/xz/IndexIndicatorException from L3 to L4 using L5
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L4 to L6 using L2
.catch java/io/IOException from L7 to L8 using L2
.catch java/io/IOException from L9 to L10 using L2
iload 2
iflt L11
iload 3
iflt L11
iload 2
iload 3
iadd
iflt L11
iload 2
iload 3
iadd
aload 1
arraylength
if_icmple L12
L11:
new java/lang/IndexOutOfBoundsException
dup
invokespecial java/lang/IndexOutOfBoundsException/<init>()V
athrow
L12:
iload 3
ifne L13
iconst_0
istore 5
L14:
iload 5
ireturn
L13:
aload 0
getfield org/tukaani/xz/SingleXZInputStream/in Ljava/io/InputStream;
ifnonnull L15
new org/tukaani/xz/XZIOException
dup
ldc "Stream closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L15:
aload 0
getfield org/tukaani/xz/SingleXZInputStream/exception Ljava/io/IOException;
ifnull L16
aload 0
getfield org/tukaani/xz/SingleXZInputStream/exception Ljava/io/IOException;
athrow
L16:
aload 0
getfield org/tukaani/xz/SingleXZInputStream/endReached Z
ifeq L17
iconst_m1
ireturn
L17:
iconst_0
istore 5
iload 2
istore 4
iload 5
istore 2
L18:
iload 2
istore 5
iload 3
ifle L14
L0:
aload 0
getfield org/tukaani/xz/SingleXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
astore 6
L1:
aload 6
ifnonnull L4
L3:
aload 0
new org/tukaani/xz/BlockInputStream
dup
aload 0
getfield org/tukaani/xz/SingleXZInputStream/in Ljava/io/InputStream;
aload 0
getfield org/tukaani/xz/SingleXZInputStream/check Lorg/tukaani/xz/check/Check;
aload 0
getfield org/tukaani/xz/SingleXZInputStream/memoryLimit I
ldc2_w -1L
ldc2_w -1L
invokespecial org/tukaani/xz/BlockInputStream/<init>(Ljava/io/InputStream;Lorg/tukaani/xz/check/Check;IJJ)V
putfield org/tukaani/xz/SingleXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
L4:
aload 0
getfield org/tukaani/xz/SingleXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
aload 1
iload 4
iload 3
invokevirtual org/tukaani/xz/BlockInputStream/read([BII)I
istore 5
L6:
iload 5
ifle L19
iload 2
iload 5
iadd
istore 2
iload 4
iload 5
iadd
istore 4
iload 3
iload 5
isub
istore 3
goto L18
L5:
astore 1
L7:
aload 0
getfield org/tukaani/xz/SingleXZInputStream/indexHash Lorg/tukaani/xz/index/IndexHash;
aload 0
getfield org/tukaani/xz/SingleXZInputStream/in Ljava/io/InputStream;
invokevirtual org/tukaani/xz/index/IndexHash/validate(Ljava/io/InputStream;)V
aload 0
invokespecial org/tukaani/xz/SingleXZInputStream/validateStreamFooter()V
aload 0
iconst_1
putfield org/tukaani/xz/SingleXZInputStream/endReached Z
L8:
iload 2
istore 5
iload 2
ifgt L14
iconst_m1
ireturn
L19:
iload 5
iconst_m1
if_icmpne L18
L9:
aload 0
getfield org/tukaani/xz/SingleXZInputStream/indexHash Lorg/tukaani/xz/index/IndexHash;
aload 0
getfield org/tukaani/xz/SingleXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
invokevirtual org/tukaani/xz/BlockInputStream/getUnpaddedSize()J
aload 0
getfield org/tukaani/xz/SingleXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
invokevirtual org/tukaani/xz/BlockInputStream/getUncompressedSize()J
invokevirtual org/tukaani/xz/index/IndexHash/add(JJ)V
aload 0
aconst_null
putfield org/tukaani/xz/SingleXZInputStream/blockDecoder Lorg/tukaani/xz/BlockInputStream;
L10:
goto L18
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/SingleXZInputStream/exception Ljava/io/IOException;
iload 2
istore 5
iload 2
ifne L14
aload 1
athrow
.limit locals 7
.limit stack 10
.end method
