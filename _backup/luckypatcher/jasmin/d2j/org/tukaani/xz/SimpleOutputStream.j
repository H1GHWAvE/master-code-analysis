.bytecode 50.0
.class synchronized org/tukaani/xz/SimpleOutputStream
.super org/tukaani/xz/FinishableOutputStream

.field static final synthetic '$assertionsDisabled' Z

.field private static final 'FILTER_BUF_SIZE' I = 4096


.field private 'exception' Ljava/io/IOException;

.field private final 'filterBuf' [B

.field private 'finished' Z

.field private 'out' Lorg/tukaani/xz/FinishableOutputStream;

.field private 'pos' I

.field private final 'simpleFilter' Lorg/tukaani/xz/simple/SimpleFilter;

.field private final 'tempBuf' [B

.field private 'unfiltered' I

.method static <clinit>()V
ldc org/tukaani/xz/SimpleOutputStream
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/SimpleOutputStream/$assertionsDisabled Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method <init>(Lorg/tukaani/xz/FinishableOutputStream;Lorg/tukaani/xz/simple/SimpleFilter;)V
aload 0
invokespecial org/tukaani/xz/FinishableOutputStream/<init>()V
aload 0
sipush 4096
newarray byte
putfield org/tukaani/xz/SimpleOutputStream/filterBuf [B
aload 0
iconst_0
putfield org/tukaani/xz/SimpleOutputStream/pos I
aload 0
iconst_0
putfield org/tukaani/xz/SimpleOutputStream/unfiltered I
aload 0
aconst_null
putfield org/tukaani/xz/SimpleOutputStream/exception Ljava/io/IOException;
aload 0
iconst_0
putfield org/tukaani/xz/SimpleOutputStream/finished Z
aload 0
iconst_1
newarray byte
putfield org/tukaani/xz/SimpleOutputStream/tempBuf [B
aload 1
ifnonnull L0
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L0:
aload 0
aload 1
putfield org/tukaani/xz/SimpleOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
aload 0
aload 2
putfield org/tukaani/xz/SimpleOutputStream/simpleFilter Lorg/tukaani/xz/simple/SimpleFilter;
return
.limit locals 3
.limit stack 2
.end method

.method static getMemoryUsage()I
iconst_5
ireturn
.limit locals 0
.limit stack 1
.end method

.method private writePending()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
getstatic org/tukaani/xz/SimpleOutputStream/$assertionsDisabled Z
ifne L3
aload 0
getfield org/tukaani/xz/SimpleOutputStream/finished Z
ifeq L3
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L3:
aload 0
getfield org/tukaani/xz/SimpleOutputStream/exception Ljava/io/IOException;
ifnull L0
aload 0
getfield org/tukaani/xz/SimpleOutputStream/exception Ljava/io/IOException;
athrow
L0:
aload 0
getfield org/tukaani/xz/SimpleOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
aload 0
getfield org/tukaani/xz/SimpleOutputStream/filterBuf [B
aload 0
getfield org/tukaani/xz/SimpleOutputStream/pos I
aload 0
getfield org/tukaani/xz/SimpleOutputStream/unfiltered I
invokevirtual org/tukaani/xz/FinishableOutputStream/write([BII)V
L1:
aload 0
iconst_1
putfield org/tukaani/xz/SimpleOutputStream/finished Z
return
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/SimpleOutputStream/exception Ljava/io/IOException;
aload 1
athrow
.limit locals 2
.limit stack 4
.end method

.method public close()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L4
aload 0
getfield org/tukaani/xz/SimpleOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
ifnull L5
aload 0
getfield org/tukaani/xz/SimpleOutputStream/finished Z
ifne L1
L0:
aload 0
invokespecial org/tukaani/xz/SimpleOutputStream/writePending()V
L1:
aload 0
getfield org/tukaani/xz/SimpleOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
invokevirtual org/tukaani/xz/FinishableOutputStream/close()V
L3:
aload 0
aconst_null
putfield org/tukaani/xz/SimpleOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
L5:
aload 0
getfield org/tukaani/xz/SimpleOutputStream/exception Ljava/io/IOException;
ifnull L6
aload 0
getfield org/tukaani/xz/SimpleOutputStream/exception Ljava/io/IOException;
athrow
L4:
astore 1
aload 0
getfield org/tukaani/xz/SimpleOutputStream/exception Ljava/io/IOException;
ifnonnull L3
aload 0
aload 1
putfield org/tukaani/xz/SimpleOutputStream/exception Ljava/io/IOException;
goto L3
L2:
astore 1
goto L1
L6:
return
.limit locals 2
.limit stack 2
.end method

.method public finish()V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
aload 0
getfield org/tukaani/xz/SimpleOutputStream/finished Z
ifne L1
aload 0
invokespecial org/tukaani/xz/SimpleOutputStream/writePending()V
L0:
aload 0
getfield org/tukaani/xz/SimpleOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
invokevirtual org/tukaani/xz/FinishableOutputStream/finish()V
L1:
return
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/SimpleOutputStream/exception Ljava/io/IOException;
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public flush()V
.throws java/io/IOException
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "Flushing is not supported"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public write(I)V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/SimpleOutputStream/tempBuf [B
iconst_0
iload 1
i2b
bastore
aload 0
aload 0
getfield org/tukaani/xz/SimpleOutputStream/tempBuf [B
iconst_0
iconst_1
invokevirtual org/tukaani/xz/SimpleOutputStream/write([BII)V
return
.limit locals 2
.limit stack 4
.end method

.method public write([BII)V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
iload 2
iflt L3
iload 3
iflt L3
iload 2
iload 3
iadd
iflt L3
iload 2
iload 3
iadd
aload 1
arraylength
if_icmple L4
L3:
new java/lang/IndexOutOfBoundsException
dup
invokespecial java/lang/IndexOutOfBoundsException/<init>()V
athrow
L4:
aload 0
getfield org/tukaani/xz/SimpleOutputStream/exception Ljava/io/IOException;
ifnull L5
aload 0
getfield org/tukaani/xz/SimpleOutputStream/exception Ljava/io/IOException;
athrow
L5:
aload 0
getfield org/tukaani/xz/SimpleOutputStream/finished Z
ifeq L6
new org/tukaani/xz/XZIOException
dup
ldc "Stream finished or closed"
invokespecial org/tukaani/xz/XZIOException/<init>(Ljava/lang/String;)V
athrow
L7:
aload 0
aload 0
getfield org/tukaani/xz/SimpleOutputStream/unfiltered I
iload 2
isub
putfield org/tukaani/xz/SimpleOutputStream/unfiltered I
L0:
aload 0
getfield org/tukaani/xz/SimpleOutputStream/out Lorg/tukaani/xz/FinishableOutputStream;
aload 0
getfield org/tukaani/xz/SimpleOutputStream/filterBuf [B
aload 0
getfield org/tukaani/xz/SimpleOutputStream/pos I
iload 2
invokevirtual org/tukaani/xz/FinishableOutputStream/write([BII)V
L1:
aload 0
aload 0
getfield org/tukaani/xz/SimpleOutputStream/pos I
iload 2
iadd
putfield org/tukaani/xz/SimpleOutputStream/pos I
iload 4
istore 2
iload 5
istore 3
aload 0
getfield org/tukaani/xz/SimpleOutputStream/pos I
aload 0
getfield org/tukaani/xz/SimpleOutputStream/unfiltered I
iadd
sipush 4096
if_icmpne L6
aload 0
getfield org/tukaani/xz/SimpleOutputStream/filterBuf [B
aload 0
getfield org/tukaani/xz/SimpleOutputStream/pos I
aload 0
getfield org/tukaani/xz/SimpleOutputStream/filterBuf [B
iconst_0
aload 0
getfield org/tukaani/xz/SimpleOutputStream/unfiltered I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
iconst_0
putfield org/tukaani/xz/SimpleOutputStream/pos I
iload 5
istore 3
iload 4
istore 2
L6:
iload 3
ifle L8
iload 3
sipush 4096
aload 0
getfield org/tukaani/xz/SimpleOutputStream/pos I
aload 0
getfield org/tukaani/xz/SimpleOutputStream/unfiltered I
iadd
isub
invokestatic java/lang/Math/min(II)I
istore 6
aload 1
iload 2
aload 0
getfield org/tukaani/xz/SimpleOutputStream/filterBuf [B
aload 0
getfield org/tukaani/xz/SimpleOutputStream/pos I
aload 0
getfield org/tukaani/xz/SimpleOutputStream/unfiltered I
iadd
iload 6
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iload 2
iload 6
iadd
istore 4
iload 3
iload 6
isub
istore 5
aload 0
aload 0
getfield org/tukaani/xz/SimpleOutputStream/unfiltered I
iload 6
iadd
putfield org/tukaani/xz/SimpleOutputStream/unfiltered I
aload 0
getfield org/tukaani/xz/SimpleOutputStream/simpleFilter Lorg/tukaani/xz/simple/SimpleFilter;
aload 0
getfield org/tukaani/xz/SimpleOutputStream/filterBuf [B
aload 0
getfield org/tukaani/xz/SimpleOutputStream/pos I
aload 0
getfield org/tukaani/xz/SimpleOutputStream/unfiltered I
invokeinterface org/tukaani/xz/simple/SimpleFilter/code([BII)I 3
istore 2
getstatic org/tukaani/xz/SimpleOutputStream/$assertionsDisabled Z
ifne L7
iload 2
aload 0
getfield org/tukaani/xz/SimpleOutputStream/unfiltered I
if_icmple L7
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L2:
astore 1
aload 0
aload 1
putfield org/tukaani/xz/SimpleOutputStream/exception Ljava/io/IOException;
aload 1
athrow
L8:
return
.limit locals 7
.limit stack 5
.end method
