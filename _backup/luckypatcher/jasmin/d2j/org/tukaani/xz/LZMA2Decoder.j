.bytecode 50.0
.class synchronized org/tukaani/xz/LZMA2Decoder
.super org/tukaani/xz/LZMA2Coder
.implements org/tukaani/xz/FilterDecoder

.field private 'dictSize' I

.method <init>([B)V
.throws org/tukaani/xz/UnsupportedOptionsException
aload 0
invokespecial org/tukaani/xz/LZMA2Coder/<init>()V
aload 1
arraylength
iconst_1
if_icmpne L0
aload 1
iconst_0
baload
sipush 255
iand
bipush 37
if_icmple L1
L0:
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "Unsupported LZMA2 properties"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 1
iconst_0
baload
iconst_1
iand
iconst_2
ior
putfield org/tukaani/xz/LZMA2Decoder/dictSize I
aload 0
aload 0
getfield org/tukaani/xz/LZMA2Decoder/dictSize I
aload 1
iconst_0
baload
iconst_1
iushr
bipush 11
iadd
ishl
putfield org/tukaani/xz/LZMA2Decoder/dictSize I
return
.limit locals 2
.limit stack 4
.end method

.method public getInputStream(Ljava/io/InputStream;)Ljava/io/InputStream;
new org/tukaani/xz/LZMA2InputStream
dup
aload 1
aload 0
getfield org/tukaani/xz/LZMA2Decoder/dictSize I
invokespecial org/tukaani/xz/LZMA2InputStream/<init>(Ljava/io/InputStream;I)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public getMemoryUsage()I
aload 0
getfield org/tukaani/xz/LZMA2Decoder/dictSize I
invokestatic org/tukaani/xz/LZMA2InputStream/getMemoryUsage(I)I
ireturn
.limit locals 1
.limit stack 1
.end method
