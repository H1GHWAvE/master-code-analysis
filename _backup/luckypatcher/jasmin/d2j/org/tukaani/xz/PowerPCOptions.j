.bytecode 50.0
.class public synchronized org/tukaani/xz/PowerPCOptions
.super org/tukaani/xz/BCJOptions

.field private static final 'ALIGNMENT' I = 4


.method public <init>()V
aload 0
iconst_4
invokespecial org/tukaani/xz/BCJOptions/<init>(I)V
return
.limit locals 1
.limit stack 2
.end method

.method public volatile synthetic clone()Ljava/lang/Object;
aload 0
invokespecial org/tukaani/xz/BCJOptions/clone()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public volatile synthetic getDecoderMemoryUsage()I
aload 0
invokespecial org/tukaani/xz/BCJOptions/getDecoderMemoryUsage()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public volatile synthetic getEncoderMemoryUsage()I
aload 0
invokespecial org/tukaani/xz/BCJOptions/getEncoderMemoryUsage()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method getFilterEncoder()Lorg/tukaani/xz/FilterEncoder;
new org/tukaani/xz/BCJEncoder
dup
aload 0
ldc2_w 5L
invokespecial org/tukaani/xz/BCJEncoder/<init>(Lorg/tukaani/xz/BCJOptions;J)V
areturn
.limit locals 1
.limit stack 5
.end method

.method public getInputStream(Ljava/io/InputStream;)Ljava/io/InputStream;
new org/tukaani/xz/SimpleInputStream
dup
aload 1
new org/tukaani/xz/simple/PowerPC
dup
iconst_0
aload 0
getfield org/tukaani/xz/PowerPCOptions/startOffset I
invokespecial org/tukaani/xz/simple/PowerPC/<init>(ZI)V
invokespecial org/tukaani/xz/SimpleInputStream/<init>(Ljava/io/InputStream;Lorg/tukaani/xz/simple/SimpleFilter;)V
areturn
.limit locals 2
.limit stack 7
.end method

.method public getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;
new org/tukaani/xz/SimpleOutputStream
dup
aload 1
new org/tukaani/xz/simple/PowerPC
dup
iconst_1
aload 0
getfield org/tukaani/xz/PowerPCOptions/startOffset I
invokespecial org/tukaani/xz/simple/PowerPC/<init>(ZI)V
invokespecial org/tukaani/xz/SimpleOutputStream/<init>(Lorg/tukaani/xz/FinishableOutputStream;Lorg/tukaani/xz/simple/SimpleFilter;)V
areturn
.limit locals 2
.limit stack 7
.end method

.method public volatile synthetic getStartOffset()I
aload 0
invokespecial org/tukaani/xz/BCJOptions/getStartOffset()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public volatile synthetic setStartOffset(I)V
.throws org/tukaani/xz/UnsupportedOptionsException
aload 0
iload 1
invokespecial org/tukaani/xz/BCJOptions/setStartOffset(I)V
return
.limit locals 2
.limit stack 2
.end method
