.bytecode 50.0
.class synchronized org/tukaani/xz/BCJDecoder
.super org/tukaani/xz/BCJCoder
.implements org/tukaani/xz/FilterDecoder

.field static final synthetic '$assertionsDisabled' Z

.field private final 'filterID' J

.field private final 'startOffset' I

.method static <clinit>()V
ldc org/tukaani/xz/BCJDecoder
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/BCJDecoder/$assertionsDisabled Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method <init>(J[B)V
.throws org/tukaani/xz/UnsupportedOptionsException
aload 0
invokespecial org/tukaani/xz/BCJCoder/<init>()V
getstatic org/tukaani/xz/BCJDecoder/$assertionsDisabled Z
ifne L0
lload 1
invokestatic org/tukaani/xz/BCJDecoder/isBCJFilterID(J)Z
ifne L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
aload 0
lload 1
putfield org/tukaani/xz/BCJDecoder/filterID J
aload 3
arraylength
ifne L1
aload 0
iconst_0
putfield org/tukaani/xz/BCJDecoder/startOffset I
return
L1:
aload 3
arraylength
iconst_4
if_icmpne L2
iconst_0
istore 5
iconst_0
istore 4
L3:
iload 4
iconst_4
if_icmpge L4
iload 5
aload 3
iload 4
baload
sipush 255
iand
iload 4
bipush 8
imul
ishl
ior
istore 5
iload 4
iconst_1
iadd
istore 4
goto L3
L4:
aload 0
iload 5
putfield org/tukaani/xz/BCJDecoder/startOffset I
return
L2:
new org/tukaani/xz/UnsupportedOptionsException
dup
ldc "Unsupported BCJ filter properties"
invokespecial org/tukaani/xz/UnsupportedOptionsException/<init>(Ljava/lang/String;)V
athrow
.limit locals 6
.limit stack 4
.end method

.method public getInputStream(Ljava/io/InputStream;)Ljava/io/InputStream;
aconst_null
astore 2
aload 0
getfield org/tukaani/xz/BCJDecoder/filterID J
ldc2_w 4L
lcmp
ifne L0
new org/tukaani/xz/simple/X86
dup
iconst_0
aload 0
getfield org/tukaani/xz/BCJDecoder/startOffset I
invokespecial org/tukaani/xz/simple/X86/<init>(ZI)V
astore 2
L1:
new org/tukaani/xz/SimpleInputStream
dup
aload 1
aload 2
invokespecial org/tukaani/xz/SimpleInputStream/<init>(Ljava/io/InputStream;Lorg/tukaani/xz/simple/SimpleFilter;)V
areturn
L0:
aload 0
getfield org/tukaani/xz/BCJDecoder/filterID J
ldc2_w 5L
lcmp
ifne L2
new org/tukaani/xz/simple/PowerPC
dup
iconst_0
aload 0
getfield org/tukaani/xz/BCJDecoder/startOffset I
invokespecial org/tukaani/xz/simple/PowerPC/<init>(ZI)V
astore 2
goto L1
L2:
aload 0
getfield org/tukaani/xz/BCJDecoder/filterID J
ldc2_w 6L
lcmp
ifne L3
new org/tukaani/xz/simple/IA64
dup
iconst_0
aload 0
getfield org/tukaani/xz/BCJDecoder/startOffset I
invokespecial org/tukaani/xz/simple/IA64/<init>(ZI)V
astore 2
goto L1
L3:
aload 0
getfield org/tukaani/xz/BCJDecoder/filterID J
ldc2_w 7L
lcmp
ifne L4
new org/tukaani/xz/simple/ARM
dup
iconst_0
aload 0
getfield org/tukaani/xz/BCJDecoder/startOffset I
invokespecial org/tukaani/xz/simple/ARM/<init>(ZI)V
astore 2
goto L1
L4:
aload 0
getfield org/tukaani/xz/BCJDecoder/filterID J
ldc2_w 8L
lcmp
ifne L5
new org/tukaani/xz/simple/ARMThumb
dup
iconst_0
aload 0
getfield org/tukaani/xz/BCJDecoder/startOffset I
invokespecial org/tukaani/xz/simple/ARMThumb/<init>(ZI)V
astore 2
goto L1
L5:
aload 0
getfield org/tukaani/xz/BCJDecoder/filterID J
ldc2_w 9L
lcmp
ifne L6
new org/tukaani/xz/simple/SPARC
dup
iconst_0
aload 0
getfield org/tukaani/xz/BCJDecoder/startOffset I
invokespecial org/tukaani/xz/simple/SPARC/<init>(ZI)V
astore 2
goto L1
L6:
getstatic org/tukaani/xz/BCJDecoder/$assertionsDisabled Z
ifne L1
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
.limit locals 3
.limit stack 4
.end method

.method public getMemoryUsage()I
invokestatic org/tukaani/xz/SimpleInputStream/getMemoryUsage()I
ireturn
.limit locals 1
.limit stack 1
.end method
