.bytecode 50.0
.class synchronized org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder
.super org/tukaani/xz/lzma/LZMACoder$LiteralCoder$LiteralSubcoder
.inner class LiteralEncoder inner org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder outer org/tukaani/xz/lzma/LZMAEncoder
.inner class private LiteralSubencoder inner org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder outer org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder

.field final synthetic 'this$1' Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

.method private <init>(Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;)V
aload 0
aload 1
putfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/this$1 Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
aload 0
aload 1
invokespecial org/tukaani/xz/lzma/LZMACoder$LiteralCoder$LiteralSubcoder/<init>(Lorg/tukaani/xz/lzma/LZMACoder$LiteralCoder;)V
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;Lorg/tukaani/xz/lzma/LZMAEncoder$1;)V
aload 0
aload 1
invokespecial org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/<init>(Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;)V
return
.limit locals 3
.limit stack 2
.end method

.method encode()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/this$1 Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/this$1 Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(I)I
sipush 256
ior
istore 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/this$1 Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/isLiteral()Z
ifeq L0
L1:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/this$1 Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
invokestatic org/tukaani/xz/lzma/LZMAEncoder/access$100(Lorg/tukaani/xz/lzma/LZMAEncoder;)Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/probs [S
iload 1
bipush 8
iushr
iload 1
bipush 7
iushr
iconst_1
iand
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
iload 1
iconst_1
ishl
istore 2
iload 2
istore 1
iload 2
ldc_w 65536
if_icmplt L1
L2:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/this$1 Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateLiteral()V
return
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/this$1 Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/this$1 Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_0
iaload
iconst_1
iadd
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/this$1 Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
iadd
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(I)I
istore 3
sipush 256
istore 2
L3:
iload 3
iconst_1
ishl
istore 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/this$1 Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
invokestatic org/tukaani/xz/lzma/LZMAEncoder/access$100(Lorg/tukaani/xz/lzma/LZMAEncoder;)Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/probs [S
iload 2
iload 3
iload 2
iand
iadd
iload 1
bipush 8
iushr
iadd
iload 1
bipush 7
iushr
iconst_1
iand
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
iload 1
iconst_1
ishl
istore 4
iload 2
iload 3
iload 4
ixor
iconst_m1
ixor
iand
istore 2
iload 4
istore 1
iload 4
ldc_w 65536
if_icmplt L3
goto L2
.limit locals 5
.limit stack 5
.end method

.method getMatchedPrice(II)I
iconst_0
istore 3
sipush 256
istore 4
iload 1
sipush 256
ior
istore 1
L0:
iload 2
iconst_1
ishl
istore 2
iload 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/probs [S
iload 4
iload 2
iload 4
iand
iadd
iload 1
bipush 8
iushr
iadd
saload
iload 1
bipush 7
iushr
iconst_1
iand
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
iadd
istore 5
iload 1
iconst_1
ishl
istore 6
iload 4
iload 2
iload 6
ixor
iconst_m1
ixor
iand
istore 4
iload 5
istore 3
iload 6
istore 1
iload 6
ldc_w 65536
if_icmplt L0
iload 5
ireturn
.limit locals 7
.limit stack 5
.end method

.method getNormalPrice(I)I
iconst_0
istore 2
iload 1
sipush 256
ior
istore 1
L0:
iload 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/probs [S
iload 1
bipush 8
iushr
saload
iload 1
bipush 7
iushr
iconst_1
iand
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
iadd
istore 3
iload 1
iconst_1
ishl
istore 4
iload 3
istore 2
iload 4
istore 1
iload 4
ldc_w 65536
if_icmplt L0
iload 3
ireturn
.limit locals 5
.limit stack 4
.end method
