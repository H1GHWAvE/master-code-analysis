.bytecode 50.0
.class public synchronized abstract org/tukaani/xz/lzma/LZMAEncoder
.super org/tukaani/xz/lzma/LZMACoder
.inner class static synthetic inner org/tukaani/xz/lzma/LZMAEncoder$1
.inner class LengthEncoder inner org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder outer org/tukaani/xz/lzma/LZMAEncoder
.inner class LiteralEncoder inner org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder outer org/tukaani/xz/lzma/LZMAEncoder
.inner class private LiteralSubencoder inner org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder outer org/tukaani/xz/lzma/LZMAEncoder

.field static final synthetic '$assertionsDisabled' Z

.field private static final 'ALIGN_PRICE_UPDATE_INTERVAL' I = 16


.field private static final 'DIST_PRICE_UPDATE_INTERVAL' I = 128


.field private static final 'LZMA2_COMPRESSED_LIMIT' I = 65510


.field private static final 'LZMA2_UNCOMPRESSED_LIMIT' I = 2096879


.field public static final 'MODE_FAST' I = 1


.field public static final 'MODE_NORMAL' I = 2


.field private 'alignPriceCount' I

.field private final 'alignPrices' [I

.field 'back' I

.field private 'distPriceCount' I

.field private final 'distSlotPrices' [[I

.field private final 'distSlotPricesSize' I

.field private final 'fullDistPrices' [[I

.field final 'literalEncoder' Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;

.field final 'lz' Lorg/tukaani/xz/lz/LZEncoder;

.field final 'matchLenEncoder' Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

.field final 'niceLen' I

.field private final 'rc' Lorg/tukaani/xz/rangecoder/RangeEncoder;

.field 'readAhead' I

.field final 'repLenEncoder' Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;

.field private 'uncompressedSize' I

.method static <clinit>()V
ldc org/tukaani/xz/lzma/LZMAEncoder
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/lzma/LZMAEncoder/$assertionsDisabled Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method <init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;Lorg/tukaani/xz/lz/LZEncoder;IIIII)V
aload 0
iload 5
invokespecial org/tukaani/xz/lzma/LZMACoder/<init>(I)V
aload 0
iconst_0
putfield org/tukaani/xz/lzma/LZMAEncoder/distPriceCount I
aload 0
iconst_0
putfield org/tukaani/xz/lzma/LZMAEncoder/alignPriceCount I
aload 0
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
iconst_2
newarray int
dup
iconst_0
iconst_4
iastore
dup
iconst_1
sipush 128
iastore
invokestatic java/lang/reflect/Array/newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;
checkcast [[I
putfield org/tukaani/xz/lzma/LZMAEncoder/fullDistPrices [[I
aload 0
bipush 16
newarray int
putfield org/tukaani/xz/lzma/LZMAEncoder/alignPrices [I
aload 0
iconst_0
putfield org/tukaani/xz/lzma/LZMAEncoder/back I
aload 0
iconst_m1
putfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
aload 0
iconst_0
putfield org/tukaani/xz/lzma/LZMAEncoder/uncompressedSize I
aload 0
aload 1
putfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
aload 2
putfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 0
iload 7
putfield org/tukaani/xz/lzma/LZMAEncoder/niceLen I
aload 0
new org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder
dup
aload 0
iload 3
iload 4
invokespecial org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/<init>(Lorg/tukaani/xz/lzma/LZMAEncoder;II)V
putfield org/tukaani/xz/lzma/LZMAEncoder/literalEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
aload 0
new org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder
dup
aload 0
iload 5
iload 7
invokespecial org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/<init>(Lorg/tukaani/xz/lzma/LZMAEncoder;II)V
putfield org/tukaani/xz/lzma/LZMAEncoder/matchLenEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;
aload 0
new org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder
dup
aload 0
iload 5
iload 7
invokespecial org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/<init>(Lorg/tukaani/xz/lzma/LZMAEncoder;II)V
putfield org/tukaani/xz/lzma/LZMAEncoder/repLenEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;
aload 0
iload 6
iconst_1
isub
invokestatic org/tukaani/xz/lzma/LZMAEncoder/getDistSlot(I)I
iconst_1
iadd
putfield org/tukaani/xz/lzma/LZMAEncoder/distSlotPricesSize I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distSlotPricesSize I
istore 3
aload 0
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
iconst_2
newarray int
dup
iconst_0
iconst_4
iastore
dup
iconst_1
iload 3
iastore
invokestatic java/lang/reflect/Array/newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;
checkcast [[I
putfield org/tukaani/xz/lzma/LZMAEncoder/distSlotPrices [[I
aload 0
invokevirtual org/tukaani/xz/lzma/LZMAEncoder/reset()V
return
.limit locals 8
.limit stack 6
.end method

.method static synthetic access$100(Lorg/tukaani/xz/lzma/LZMAEncoder;)Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
areturn
.limit locals 1
.limit stack 1
.end method

.method private encodeInit()Z
getstatic org/tukaani/xz/lzma/LZMAEncoder/$assertionsDisabled Z
ifne L0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
iconst_m1
if_icmpeq L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
iconst_0
invokevirtual org/tukaani/xz/lz/LZEncoder/hasEnoughData(I)Z
ifne L1
iconst_0
ireturn
L1:
aload 0
iconst_1
invokevirtual org/tukaani/xz/lzma/LZMAEncoder/skip(I)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isMatch [[S
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
aaload
iconst_0
iconst_0
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/literalEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/encodeInit()V
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
iconst_1
isub
putfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
getstatic org/tukaani/xz/lzma/LZMAEncoder/$assertionsDisabled Z
ifne L2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
iconst_m1
if_icmpeq L2
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L2:
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/uncompressedSize I
iconst_1
iadd
putfield org/tukaani/xz/lzma/LZMAEncoder/uncompressedSize I
getstatic org/tukaani/xz/lzma/LZMAEncoder/$assertionsDisabled Z
ifne L3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/uncompressedSize I
iconst_1
if_icmpeq L3
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L3:
iconst_1
ireturn
.limit locals 1
.limit stack 4
.end method

.method private encodeMatch(III)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateMatch()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/matchLenEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;
iload 2
iload 3
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/encode(II)V
iload 1
invokestatic org/tukaani/xz/lzma/LZMAEncoder/getDistSlot(I)I
istore 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distSlots [[S
iload 2
invokestatic org/tukaani/xz/lzma/LZMAEncoder/getDistState(I)I
aaload
iload 3
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBitTree([SI)V
iload 3
iconst_4
if_icmplt L0
iload 3
iconst_1
iushr
iconst_1
isub
istore 2
iload 1
iload 3
iconst_1
iand
iconst_2
ior
iload 2
ishl
isub
istore 4
iload 3
bipush 14
if_icmpge L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distSpecial [[S
iload 3
iconst_4
isub
aaload
iload 4
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeReverseBitTree([SI)V
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_2
iaload
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_1
iaload
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_0
iaload
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_0
iload 1
iastore
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distPriceCount I
iconst_1
isub
putfield org/tukaani/xz/lzma/LZMAEncoder/distPriceCount I
return
L1:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
iload 4
iconst_4
iushr
iload 2
iconst_4
isub
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeDirectBits(II)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distAlign [S
iload 4
bipush 15
iand
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeReverseBitTree([SI)V
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/alignPriceCount I
iconst_1
isub
putfield org/tukaani/xz/lzma/LZMAEncoder/alignPriceCount I
goto L0
.limit locals 5
.limit stack 4
.end method

.method private encodeRepMatch(III)V
iconst_0
istore 4
iload 1
ifne L0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep0 [S
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
iconst_0
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
astore 5
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep0Long [[S
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
aaload
astore 6
iload 2
iconst_1
if_icmpne L1
iload 4
istore 1
L2:
aload 5
aload 6
iload 3
iload 1
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
L3:
iload 2
iconst_1
if_icmpne L4
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateShortRep()V
return
L1:
iconst_1
istore 1
goto L2
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iload 1
iaload
istore 4
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep0 [S
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
iconst_1
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
iload 1
iconst_1
if_icmpne L5
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep1 [S
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
iconst_0
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
L6:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_0
iaload
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_0
iload 4
iastore
goto L3
L5:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep1 [S
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
iconst_1
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep2 [S
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
iload 1
iconst_2
isub
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
iload 1
iconst_3
if_icmpne L7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_2
iaload
iastore
L7:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
iconst_1
iaload
iastore
goto L6
L4:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/repLenEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;
iload 2
iload 3
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/encode(II)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateLongRep()V
return
.limit locals 7
.limit stack 5
.end method

.method private encodeSymbol()Z
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
iconst_1
iadd
invokevirtual org/tukaani/xz/lz/LZEncoder/hasEnoughData(I)Z
ifne L0
iconst_0
ireturn
L0:
aload 0
invokevirtual org/tukaani/xz/lzma/LZMAEncoder/getNextSymbol()I
istore 1
getstatic org/tukaani/xz/lzma/LZMAEncoder/$assertionsDisabled Z
ifne L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
ifge L1
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L1:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
invokevirtual org/tukaani/xz/lz/LZEncoder/getPos()I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
isub
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/posMask I
iand
istore 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/back I
iconst_m1
if_icmpne L2
getstatic org/tukaani/xz/lzma/LZMAEncoder/$assertionsDisabled Z
ifne L3
iload 1
iconst_1
if_icmpeq L3
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L3:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isMatch [[S
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
aaload
iload 2
iconst_0
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/literalEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/encode()V
L4:
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
iload 1
isub
putfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/uncompressedSize I
iload 1
iadd
putfield org/tukaani/xz/lzma/LZMAEncoder/uncompressedSize I
iconst_1
ireturn
L2:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isMatch [[S
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
aaload
iload 2
iconst_1
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/back I
iconst_4
if_icmpge L5
getstatic org/tukaani/xz/lzma/LZMAEncoder/$assertionsDisabled Z
ifne L6
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
ineg
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/reps [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/back I
iaload
iload 1
invokevirtual org/tukaani/xz/lz/LZEncoder/getMatchLen(III)I
iload 1
if_icmpeq L6
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L6:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep [S
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
iconst_1
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/back I
iload 1
iload 2
invokespecial org/tukaani/xz/lzma/LZMAEncoder/encodeRepMatch(III)V
goto L4
L5:
getstatic org/tukaani/xz/lzma/LZMAEncoder/$assertionsDisabled Z
ifne L7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
ineg
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/back I
iconst_4
isub
iload 1
invokevirtual org/tukaani/xz/lz/LZEncoder/getMatchLen(III)I
iload 1
if_icmpeq L7
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L7:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep [S
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
iconst_0
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/back I
iconst_4
isub
iload 1
iload 2
invokespecial org/tukaani/xz/lzma/LZMAEncoder/encodeMatch(III)V
goto L4
.limit locals 3
.limit stack 4
.end method

.method public static getDistSlot(I)I
iload 0
iconst_4
if_icmpgt L0
iload 0
ireturn
L0:
iload 0
istore 1
bipush 31
istore 3
iload 1
istore 4
ldc_w -65536
iload 1
iand
ifne L1
iload 1
bipush 16
ishl
istore 4
bipush 15
istore 3
L1:
iload 3
istore 1
iload 4
istore 2
ldc_w -16777216
iload 4
iand
ifne L2
iload 4
bipush 8
ishl
istore 2
iload 3
bipush 8
isub
istore 1
L2:
iload 1
istore 3
iload 2
istore 4
ldc_w -268435456
iload 2
iand
ifne L3
iload 2
iconst_4
ishl
istore 4
iload 1
iconst_4
isub
istore 3
L3:
iload 3
istore 1
iload 4
istore 2
ldc_w -1073741824
iload 4
iand
ifne L4
iload 4
iconst_2
ishl
istore 2
iload 3
iconst_2
isub
istore 1
L4:
iload 1
istore 3
ldc_w -2147483648
iload 2
iand
ifne L5
iload 1
iconst_1
isub
istore 3
L5:
iload 3
iconst_1
ishl
iload 0
iload 3
iconst_1
isub
iushr
iconst_1
iand
iadd
ireturn
.limit locals 5
.limit stack 4
.end method

.method public static getInstance(Lorg/tukaani/xz/rangecoder/RangeEncoder;IIIIIIIII)Lorg/tukaani/xz/lzma/LZMAEncoder;
iload 4
tableswitch 1
L0
L1
default : L2
L2:
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L0:
new org/tukaani/xz/lzma/LZMAEncoderFast
dup
aload 0
iload 1
iload 2
iload 3
iload 5
iload 6
iload 7
iload 8
iload 9
invokespecial org/tukaani/xz/lzma/LZMAEncoderFast/<init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;IIIIIIII)V
areturn
L1:
new org/tukaani/xz/lzma/LZMAEncoderNormal
dup
aload 0
iload 1
iload 2
iload 3
iload 5
iload 6
iload 7
iload 8
iload 9
invokespecial org/tukaani/xz/lzma/LZMAEncoderNormal/<init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;IIIIIIII)V
areturn
.limit locals 10
.limit stack 11
.end method

.method public static getMemoryUsage(IIII)I
iload 0
tableswitch 1
L0
L1
default : L2
L2:
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L0:
bipush 80
iload 1
iload 2
iload 3
invokestatic org/tukaani/xz/lzma/LZMAEncoderFast/getMemoryUsage(III)I
iadd
ireturn
L1:
bipush 80
iload 1
iload 2
iload 3
invokestatic org/tukaani/xz/lzma/LZMAEncoderNormal/getMemoryUsage(III)I
iadd
ireturn
.limit locals 4
.limit stack 4
.end method

.method private updateAlignPrices()V
aload 0
bipush 16
putfield org/tukaani/xz/lzma/LZMAEncoder/alignPriceCount I
iconst_0
istore 1
L0:
iload 1
bipush 16
if_icmpge L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/alignPrices [I
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distAlign [S
iload 1
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getReverseBitTreePrice([SI)I
iastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
return
.limit locals 2
.limit stack 4
.end method

.method private updateDistPrices()V
aload 0
sipush 128
putfield org/tukaani/xz/lzma/LZMAEncoder/distPriceCount I
iconst_0
istore 1
L0:
iload 1
iconst_4
if_icmpge L1
iconst_0
istore 2
L2:
iload 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distSlotPricesSize I
if_icmpge L3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distSlotPrices [[I
iload 1
aaload
iload 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distSlots [[S
iload 1
aaload
iload 2
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitTreePrice([SI)I
iastore
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
bipush 14
istore 2
L4:
iload 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distSlotPricesSize I
if_icmpge L5
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distSlotPrices [[I
iload 1
aaload
astore 7
aload 7
iload 2
aload 7
iload 2
iaload
iload 2
iconst_1
iushr
iconst_1
isub
iconst_4
isub
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getDirectBitsPrice(I)I
iadd
iastore
iload 2
iconst_1
iadd
istore 2
goto L4
L5:
iconst_0
istore 2
L6:
iload 2
iconst_4
if_icmpge L7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/fullDistPrices [[I
iload 1
aaload
iload 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distSlotPrices [[I
iload 1
aaload
iload 2
iaload
iastore
iload 2
iconst_1
iadd
istore 2
goto L6
L7:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iconst_4
istore 2
iconst_4
istore 1
L8:
iload 1
bipush 14
if_icmpge L9
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distSpecial [[S
iload 1
iconst_4
isub
aaload
arraylength
istore 5
iconst_0
istore 3
L10:
iload 3
iload 5
if_icmpge L11
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distSpecial [[S
iload 1
iconst_4
isub
aaload
iload 2
iload 1
iconst_1
iand
iconst_2
ior
iload 1
iconst_1
iushr
iconst_1
isub
ishl
isub
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getReverseBitTreePrice([SI)I
istore 6
iconst_0
istore 4
L12:
iload 4
iconst_4
if_icmpge L13
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/fullDistPrices [[I
iload 4
aaload
iload 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distSlotPrices [[I
iload 4
aaload
iload 1
iaload
iload 6
iadd
iastore
iload 4
iconst_1
iadd
istore 4
goto L12
L13:
iload 2
iconst_1
iadd
istore 2
iload 3
iconst_1
iadd
istore 3
goto L10
L11:
iload 1
iconst_1
iadd
istore 1
goto L8
L9:
getstatic org/tukaani/xz/lzma/LZMAEncoder/$assertionsDisabled Z
ifne L14
iload 2
sipush 128
if_icmpeq L14
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L14:
return
.limit locals 8
.limit stack 5
.end method

.method public encodeForLZMA2()Z
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
invokevirtual org/tukaani/xz/lz/LZEncoder/isStarted()Z
ifne L0
aload 0
invokespecial org/tukaani/xz/lzma/LZMAEncoder/encodeInit()Z
ifne L0
iconst_0
ireturn
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/uncompressedSize I
ldc_w 2096879
if_icmpgt L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/rc Lorg/tukaani/xz/rangecoder/RangeEncoder;
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/getPendingSize()I
ldc_w 65510
if_icmpgt L1
aload 0
invokespecial org/tukaani/xz/lzma/LZMAEncoder/encodeSymbol()Z
ifne L0
iconst_0
ireturn
L1:
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method getAnyMatchPrice(Lorg/tukaani/xz/lzma/State;I)I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isMatch [[S
aload 1
invokevirtual org/tukaani/xz/lzma/State/get()I
aaload
iload 2
iaload
iconst_1
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
ireturn
.limit locals 3
.limit stack 2
.end method

.method getAnyRepPrice(ILorg/tukaani/xz/lzma/State;)I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep [S
aload 2
invokevirtual org/tukaani/xz/lzma/State/get()I
saload
iconst_1
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
iload 1
iadd
ireturn
.limit locals 3
.limit stack 2
.end method

.method public getLZEncoder()Lorg/tukaani/xz/lz/LZEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
areturn
.limit locals 1
.limit stack 1
.end method

.method getLongRepAndLenPrice(IILorg/tukaani/xz/lzma/State;I)I
aload 0
aload 0
aload 0
aload 3
iload 4
invokevirtual org/tukaani/xz/lzma/LZMAEncoder/getAnyMatchPrice(Lorg/tukaani/xz/lzma/State;I)I
aload 3
invokevirtual org/tukaani/xz/lzma/LZMAEncoder/getAnyRepPrice(ILorg/tukaani/xz/lzma/State;)I
iload 1
aload 3
iload 4
invokevirtual org/tukaani/xz/lzma/LZMAEncoder/getLongRepPrice(IILorg/tukaani/xz/lzma/State;I)I
istore 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/repLenEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;
iload 2
iload 4
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/getPrice(II)I
iload 1
iadd
ireturn
.limit locals 5
.limit stack 5
.end method

.method getLongRepPrice(IILorg/tukaani/xz/lzma/State;I)I
iload 2
ifne L0
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep0 [S
aload 3
invokevirtual org/tukaani/xz/lzma/State/get()I
saload
iconst_0
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep0Long [[S
aload 3
invokevirtual org/tukaani/xz/lzma/State/get()I
aaload
iload 4
iaload
iconst_1
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
iadd
iadd
ireturn
L0:
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep0 [S
aload 3
invokevirtual org/tukaani/xz/lzma/State/get()I
saload
iconst_1
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
iadd
istore 1
iload 2
iconst_1
if_icmpne L1
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep1 [S
aload 3
invokevirtual org/tukaani/xz/lzma/State/get()I
saload
iconst_0
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
iadd
ireturn
L1:
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep1 [S
aload 3
invokevirtual org/tukaani/xz/lzma/State/get()I
saload
iconst_1
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep2 [S
aload 3
invokevirtual org/tukaani/xz/lzma/State/get()I
saload
iload 2
iconst_2
isub
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
iadd
iadd
ireturn
.limit locals 5
.limit stack 5
.end method

.method getMatchAndLenPrice(IIII)I
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/matchLenEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;
iload 3
iload 4
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/getPrice(II)I
iadd
istore 1
iload 3
invokestatic org/tukaani/xz/lzma/LZMAEncoder/getDistState(I)I
istore 3
iload 2
sipush 128
if_icmpge L0
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/fullDistPrices [[I
iload 3
aaload
iload 2
iaload
iadd
ireturn
L0:
iload 2
invokestatic org/tukaani/xz/lzma/LZMAEncoder/getDistSlot(I)I
istore 4
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distSlotPrices [[I
iload 3
aaload
iload 4
iaload
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/alignPrices [I
iload 2
bipush 15
iand
iaload
iadd
iadd
ireturn
.limit locals 5
.limit stack 5
.end method

.method getMatches()Lorg/tukaani/xz/lz/Matches;
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
iconst_1
iadd
putfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
invokevirtual org/tukaani/xz/lz/LZEncoder/getMatches()Lorg/tukaani/xz/lz/Matches;
astore 1
getstatic org/tukaani/xz/lzma/LZMAEncoder/$assertionsDisabled Z
ifne L0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 1
invokevirtual org/tukaani/xz/lz/LZEncoder/verifyMatches(Lorg/tukaani/xz/lz/Matches;)Z
ifne L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method abstract getNextSymbol()I
.end method

.method getNormalMatchPrice(ILorg/tukaani/xz/lzma/State;)I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep [S
aload 2
invokevirtual org/tukaani/xz/lzma/State/get()I
saload
iconst_0
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
iload 1
iadd
ireturn
.limit locals 3
.limit stack 2
.end method

.method getShortRepPrice(ILorg/tukaani/xz/lzma/State;I)I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep0 [S
aload 2
invokevirtual org/tukaani/xz/lzma/State/get()I
saload
iconst_0
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
iload 1
iadd
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/isRep0Long [[S
aload 2
invokevirtual org/tukaani/xz/lzma/State/get()I
aaload
iload 3
iaload
iconst_0
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
iadd
ireturn
.limit locals 4
.limit stack 3
.end method

.method public getUncompressedSize()I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/uncompressedSize I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public reset()V
aload 0
invokespecial org/tukaani/xz/lzma/LZMACoder/reset()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/literalEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/reset()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/matchLenEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/reset()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/repLenEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/reset()V
aload 0
iconst_0
putfield org/tukaani/xz/lzma/LZMAEncoder/distPriceCount I
aload 0
iconst_0
putfield org/tukaani/xz/lzma/LZMAEncoder/alignPriceCount I
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/uncompressedSize I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
iconst_1
iadd
iadd
putfield org/tukaani/xz/lzma/LZMAEncoder/uncompressedSize I
aload 0
iconst_m1
putfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
return
.limit locals 1
.limit stack 4
.end method

.method public resetUncompressedSize()V
aload 0
iconst_0
putfield org/tukaani/xz/lzma/LZMAEncoder/uncompressedSize I
return
.limit locals 1
.limit stack 2
.end method

.method skip(I)V
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
iload 1
iadd
putfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
iload 1
invokevirtual org/tukaani/xz/lz/LZEncoder/skip(I)V
return
.limit locals 2
.limit stack 3
.end method

.method updatePrices()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/distPriceCount I
ifgt L0
aload 0
invokespecial org/tukaani/xz/lzma/LZMAEncoder/updateDistPrices()V
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/alignPriceCount I
ifgt L1
aload 0
invokespecial org/tukaani/xz/lzma/LZMAEncoder/updateAlignPrices()V
L1:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/matchLenEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/updatePrices()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder/repLenEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/updatePrices()V
return
.limit locals 1
.limit stack 1
.end method
