.bytecode 50.0
.class synchronized abstract org/tukaani/xz/lzma/LZMACoder$LengthCoder
.super java/lang/Object
.inner class abstract LengthCoder inner org/tukaani/xz/lzma/LZMACoder$LengthCoder outer org/tukaani/xz/lzma/LZMACoder

.field static final 'HIGH_SYMBOLS' I = 256


.field static final 'LOW_SYMBOLS' I = 8


.field static final 'MID_SYMBOLS' I = 8


.field final 'choice' [S

.field final 'high' [S

.field final 'low' [[S

.field final 'mid' [[S

.field final synthetic 'this$0' Lorg/tukaani/xz/lzma/LZMACoder;

.method <init>(Lorg/tukaani/xz/lzma/LZMACoder;)V
aload 0
aload 1
putfield org/tukaani/xz/lzma/LZMACoder$LengthCoder/this$0 Lorg/tukaani/xz/lzma/LZMACoder;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_2
newarray short
putfield org/tukaani/xz/lzma/LZMACoder$LengthCoder/choice [S
aload 0
getstatic java/lang/Short/TYPE Ljava/lang/Class;
iconst_2
newarray int
dup
iconst_0
bipush 16
iastore
dup
iconst_1
bipush 8
iastore
invokestatic java/lang/reflect/Array/newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;
checkcast [[S
putfield org/tukaani/xz/lzma/LZMACoder$LengthCoder/low [[S
aload 0
getstatic java/lang/Short/TYPE Ljava/lang/Class;
iconst_2
newarray int
dup
iconst_0
bipush 16
iastore
dup
iconst_1
bipush 8
iastore
invokestatic java/lang/reflect/Array/newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;
checkcast [[S
putfield org/tukaani/xz/lzma/LZMACoder$LengthCoder/mid [[S
aload 0
sipush 256
newarray short
putfield org/tukaani/xz/lzma/LZMACoder$LengthCoder/high [S
return
.limit locals 2
.limit stack 6
.end method

.method reset()V
aload 0
getfield org/tukaani/xz/lzma/LZMACoder$LengthCoder/choice [S
invokestatic org/tukaani/xz/rangecoder/RangeCoder/initProbs([S)V
iconst_0
istore 1
L0:
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMACoder$LengthCoder/low [[S
arraylength
if_icmpge L1
aload 0
getfield org/tukaani/xz/lzma/LZMACoder$LengthCoder/low [[S
iload 1
aaload
invokestatic org/tukaani/xz/rangecoder/RangeCoder/initProbs([S)V
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iconst_0
istore 1
L2:
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMACoder$LengthCoder/low [[S
arraylength
if_icmpge L3
aload 0
getfield org/tukaani/xz/lzma/LZMACoder$LengthCoder/mid [[S
iload 1
aaload
invokestatic org/tukaani/xz/rangecoder/RangeCoder/initProbs([S)V
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
aload 0
getfield org/tukaani/xz/lzma/LZMACoder$LengthCoder/high [S
invokestatic org/tukaani/xz/rangecoder/RangeCoder/initProbs([S)V
return
.limit locals 2
.limit stack 2
.end method
