.bytecode 50.0
.class final synchronized org/tukaani/xz/lzma/LZMAEncoderFast
.super org/tukaani/xz/lzma/LZMAEncoder

.field private static 'EXTRA_SIZE_AFTER' I

.field private static 'EXTRA_SIZE_BEFORE' I

.field private 'matches' Lorg/tukaani/xz/lz/Matches;

.method static <clinit>()V
iconst_1
putstatic org/tukaani/xz/lzma/LZMAEncoderFast/EXTRA_SIZE_BEFORE I
sipush 272
putstatic org/tukaani/xz/lzma/LZMAEncoderFast/EXTRA_SIZE_AFTER I
return
.limit locals 0
.limit stack 1
.end method

.method <init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;IIIIIIII)V
aload 0
aload 1
iload 5
iload 6
getstatic org/tukaani/xz/lzma/LZMAEncoderFast/EXTRA_SIZE_BEFORE I
invokestatic java/lang/Math/max(II)I
getstatic org/tukaani/xz/lzma/LZMAEncoderFast/EXTRA_SIZE_AFTER I
iload 7
sipush 273
iload 8
iload 9
invokestatic org/tukaani/xz/lz/LZEncoder/getInstance(IIIIIII)Lorg/tukaani/xz/lz/LZEncoder;
iload 2
iload 3
iload 4
iload 5
iload 7
invokespecial org/tukaani/xz/lzma/LZMAEncoder/<init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;Lorg/tukaani/xz/lz/LZEncoder;IIIII)V
aload 0
aconst_null
putfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
return
.limit locals 10
.limit stack 9
.end method

.method private changePair(II)Z
iload 1
iload 2
bipush 7
iushr
if_icmpge L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method static getMemoryUsage(III)I
iload 0
iload 1
getstatic org/tukaani/xz/lzma/LZMAEncoderFast/EXTRA_SIZE_BEFORE I
invokestatic java/lang/Math/max(II)I
getstatic org/tukaani/xz/lzma/LZMAEncoderFast/EXTRA_SIZE_AFTER I
sipush 273
iload 2
invokestatic org/tukaani/xz/lz/LZEncoder/getMemoryUsage(IIIII)I
ireturn
.limit locals 3
.limit stack 5
.end method

.method getNextSymbol()I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/readAhead I
iconst_m1
if_icmpne L0
aload 0
aload 0
invokevirtual org/tukaani/xz/lzma/LZMAEncoderFast/getMatches()Lorg/tukaani/xz/lz/Matches;
putfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
L0:
aload 0
iconst_m1
putfield org/tukaani/xz/lzma/LZMAEncoderFast/back I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/lz Lorg/tukaani/xz/lz/LZEncoder;
invokevirtual org/tukaani/xz/lz/LZEncoder/getAvail()I
sipush 273
invokestatic java/lang/Math/min(II)I
istore 7
iload 7
iconst_2
if_icmpge L1
iconst_1
ireturn
L1:
iconst_0
istore 4
iconst_0
istore 5
iconst_0
istore 1
L2:
iload 1
iconst_4
if_icmpge L3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/reps [I
iload 1
iaload
iload 7
invokevirtual org/tukaani/xz/lz/LZEncoder/getMatchLen(II)I
istore 3
iload 3
iconst_2
if_icmpge L4
iload 4
istore 2
L5:
iload 1
iconst_1
iadd
istore 1
iload 2
istore 4
goto L2
L4:
iload 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/niceLen I
if_icmplt L6
aload 0
iload 1
putfield org/tukaani/xz/lzma/LZMAEncoderFast/back I
aload 0
iload 3
iconst_1
isub
invokevirtual org/tukaani/xz/lzma/LZMAEncoderFast/skip(I)V
iload 3
ireturn
L6:
iload 4
istore 2
iload 3
iload 4
if_icmple L5
iload 1
istore 5
iload 3
istore 2
goto L5
L3:
iconst_0
istore 1
iconst_0
istore 6
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
ifle L7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
iaload
istore 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/dist [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
iaload
istore 6
iload 6
istore 2
iload 1
istore 3
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/niceLen I
if_icmplt L8
aload 0
iload 6
iconst_4
iadd
putfield org/tukaani/xz/lzma/LZMAEncoderFast/back I
aload 0
iload 1
iconst_1
isub
invokevirtual org/tukaani/xz/lzma/LZMAEncoderFast/skip(I)V
iload 1
ireturn
L9:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
astore 8
aload 8
aload 8
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
putfield org/tukaani/xz/lz/Matches/count I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
iaload
istore 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/dist [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
iaload
istore 2
L8:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
if_icmple L10
iload 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_2
isub
iaload
iconst_1
iadd
if_icmpne L10
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/dist [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_2
isub
iaload
iload 2
invokespecial org/tukaani/xz/lzma/LZMAEncoderFast/changePair(II)Z
ifne L9
L10:
iload 2
istore 6
iload 3
istore 1
iload 3
iconst_2
if_icmpne L7
iload 2
istore 6
iload 3
istore 1
iload 2
sipush 128
if_icmplt L7
iconst_1
istore 1
iload 2
istore 6
L7:
iload 4
iconst_2
if_icmplt L11
iload 4
iconst_1
iadd
iload 1
if_icmpge L12
iload 4
iconst_2
iadd
iload 1
if_icmplt L13
iload 6
sipush 512
if_icmpge L12
L13:
iload 4
iconst_3
iadd
iload 1
if_icmplt L11
iload 6
ldc_w 32768
if_icmplt L11
L12:
aload 0
iload 5
putfield org/tukaani/xz/lzma/LZMAEncoderFast/back I
aload 0
iload 4
iconst_1
isub
invokevirtual org/tukaani/xz/lzma/LZMAEncoderFast/skip(I)V
iload 4
ireturn
L11:
iload 1
iconst_2
if_icmplt L14
iload 7
iconst_2
if_icmpgt L15
L14:
iconst_1
ireturn
L15:
aload 0
aload 0
invokevirtual org/tukaani/xz/lzma/LZMAEncoderFast/getMatches()Lorg/tukaani/xz/lz/Matches;
putfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
ifle L16
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
iaload
istore 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/dist [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
iaload
istore 3
iload 2
iload 1
if_icmplt L17
iload 3
iload 6
if_icmplt L18
L17:
iload 2
iload 1
iconst_1
iadd
if_icmpne L19
aload 0
iload 6
iload 3
invokespecial org/tukaani/xz/lzma/LZMAEncoderFast/changePair(II)Z
ifeq L18
L19:
iload 2
iload 1
iconst_1
iadd
if_icmpgt L18
iload 2
iconst_1
iadd
iload 1
if_icmplt L16
iload 1
iconst_3
if_icmplt L16
aload 0
iload 3
iload 6
invokespecial org/tukaani/xz/lzma/LZMAEncoderFast/changePair(II)Z
ifeq L16
L18:
iconst_1
ireturn
L16:
iload 1
iconst_1
isub
iconst_2
invokestatic java/lang/Math/max(II)I
istore 3
iconst_0
istore 2
L20:
iload 2
iconst_4
if_icmpge L21
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderFast/reps [I
iload 2
iaload
iload 3
invokevirtual org/tukaani/xz/lz/LZEncoder/getMatchLen(II)I
iload 3
if_icmpne L22
iconst_1
ireturn
L22:
iload 2
iconst_1
iadd
istore 2
goto L20
L21:
aload 0
iload 6
iconst_4
iadd
putfield org/tukaani/xz/lzma/LZMAEncoderFast/back I
aload 0
iload 1
iconst_2
isub
invokevirtual org/tukaani/xz/lzma/LZMAEncoderFast/skip(I)V
iload 1
ireturn
.limit locals 9
.limit stack 4
.end method
