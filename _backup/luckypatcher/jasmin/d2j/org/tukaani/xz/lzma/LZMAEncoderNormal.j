.bytecode 50.0
.class final synchronized org/tukaani/xz/lzma/LZMAEncoderNormal
.super org/tukaani/xz/lzma/LZMAEncoder

.field static final synthetic '$assertionsDisabled' Z

.field private static 'EXTRA_SIZE_AFTER' I = 0


.field private static 'EXTRA_SIZE_BEFORE' I = 0


.field private static final 'OPTS' I = 4096


.field private 'matches' Lorg/tukaani/xz/lz/Matches;

.field private final 'nextState' Lorg/tukaani/xz/lzma/State;

.field private 'optCur' I

.field private 'optEnd' I

.field private final 'opts' [Lorg/tukaani/xz/lzma/Optimum;

.field private final 'repLens' [I

.method static <clinit>()V
ldc org/tukaani/xz/lzma/LZMAEncoderNormal
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/lzma/LZMAEncoderNormal/$assertionsDisabled Z
sipush 4096
putstatic org/tukaani/xz/lzma/LZMAEncoderNormal/EXTRA_SIZE_BEFORE I
sipush 4096
putstatic org/tukaani/xz/lzma/LZMAEncoderNormal/EXTRA_SIZE_AFTER I
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method <init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;IIIIIIII)V
aload 0
aload 1
iload 5
iload 6
getstatic org/tukaani/xz/lzma/LZMAEncoderNormal/EXTRA_SIZE_BEFORE I
invokestatic java/lang/Math/max(II)I
getstatic org/tukaani/xz/lzma/LZMAEncoderNormal/EXTRA_SIZE_AFTER I
iload 7
sipush 273
iload 8
iload 9
invokestatic org/tukaani/xz/lz/LZEncoder/getInstance(IIIIIII)Lorg/tukaani/xz/lz/LZEncoder;
iload 2
iload 3
iload 4
iload 5
iload 7
invokespecial org/tukaani/xz/lzma/LZMAEncoder/<init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;Lorg/tukaani/xz/lz/LZEncoder;IIIII)V
aload 0
sipush 4096
anewarray org/tukaani/xz/lzma/Optimum
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
iconst_0
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aload 0
iconst_0
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
aload 0
iconst_4
newarray int
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLens [I
aload 0
new org/tukaani/xz/lzma/State
dup
invokespecial org/tukaani/xz/lzma/State/<init>()V
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/nextState Lorg/tukaani/xz/lzma/State;
iconst_0
istore 2
L0:
iload 2
sipush 4096
if_icmpge L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 2
new org/tukaani/xz/lzma/Optimum
dup
invokespecial org/tukaani/xz/lzma/Optimum/<init>()V
aastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 10
.limit stack 9
.end method

.method private calc1BytePrices(IIII)V
iconst_0
istore 5
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
iconst_0
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(I)I
istore 8
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iconst_0
iaload
iconst_1
iadd
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(I)I
istore 9
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/price I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/literalEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
iload 8
iload 9
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
iconst_1
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(I)I
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/getPrice(IIIILorg/tukaani/xz/lzma/State;)I
iadd
istore 7
iload 7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iconst_1
iadd
aaload
getfield org/tukaani/xz/lzma/Optimum/price I
if_icmpge L0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iconst_1
iadd
aaload
iload 7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iconst_m1
invokevirtual org/tukaani/xz/lzma/Optimum/set1(III)V
iconst_1
istore 5
L0:
iload 5
istore 6
iload 9
iload 8
if_icmpne L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iconst_1
iadd
aaload
getfield org/tukaani/xz/lzma/Optimum/optPrev I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
if_icmpeq L2
iload 5
istore 6
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iconst_1
iadd
aaload
getfield org/tukaani/xz/lzma/Optimum/backPrev I
ifeq L1
L2:
aload 0
iload 4
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
iload 2
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getShortRepPrice(ILorg/tukaani/xz/lzma/State;I)I
istore 2
iload 5
istore 6
iload 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iconst_1
iadd
aaload
getfield org/tukaani/xz/lzma/Optimum/price I
if_icmpgt L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iconst_1
iadd
aaload
iload 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iconst_0
invokevirtual org/tukaani/xz/lzma/Optimum/set1(III)V
iconst_1
istore 6
L1:
iload 6
ifne L3
iload 9
iload 8
if_icmpeq L3
iload 3
iconst_2
if_icmple L3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/niceLen I
iload 3
iconst_1
isub
invokestatic java/lang/Math/min(II)I
istore 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
iconst_1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iconst_0
iaload
iload 2
invokevirtual org/tukaani/xz/lz/LZEncoder/getMatchLen(III)I
istore 2
iload 2
iconst_2
if_icmplt L3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/nextState Lorg/tukaani/xz/lzma/State;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/set(Lorg/tukaani/xz/lzma/State;)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/nextState Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateLiteral()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/posMask I
istore 3
iload 7
aload 0
iconst_0
iload 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/nextState Lorg/tukaani/xz/lzma/State;
iload 1
iconst_1
iadd
iload 3
iand
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getLongRepAndLenPrice(IILorg/tukaani/xz/lzma/State;I)I
iadd
istore 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iconst_1
iadd
iload 2
iadd
istore 2
L4:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
iload 2
if_icmpge L5
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
astore 10
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
iconst_1
iadd
istore 3
aload 0
iload 3
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
aload 10
iload 3
aaload
invokevirtual org/tukaani/xz/lzma/Optimum/reset()V
goto L4
L5:
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 2
aaload
getfield org/tukaani/xz/lzma/Optimum/price I
if_icmpge L3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 2
aaload
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iconst_0
invokevirtual org/tukaani/xz/lzma/Optimum/set2(III)V
L3:
return
.limit locals 11
.limit stack 8
.end method

.method private calcLongRepPrices(IIII)I
iconst_2
istore 5
iload 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/niceLen I
invokestatic java/lang/Math/min(II)I
istore 9
iconst_0
istore 6
L0:
iload 6
iconst_4
if_icmpge L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iload 6
iaload
iload 9
invokevirtual org/tukaani/xz/lz/LZEncoder/getMatchLen(II)I
istore 8
iload 8
iconst_2
if_icmpge L2
iload 5
istore 7
L3:
iload 6
iconst_1
iadd
istore 6
iload 7
istore 5
goto L0
L2:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iload 8
iadd
if_icmpge L4
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
astore 15
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
iconst_1
iadd
istore 7
aload 0
iload 7
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
aload 15
iload 7
aaload
invokevirtual org/tukaani/xz/lzma/Optimum/reset()V
goto L2
L4:
aload 0
iload 4
iload 6
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
iload 2
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getLongRepPrice(IILorg/tukaani/xz/lzma/State;I)I
istore 10
iload 8
istore 7
L5:
iload 7
iconst_2
if_icmplt L6
iload 10
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLenEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;
iload 7
iload 2
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/getPrice(II)I
iadd
istore 11
iload 11
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iload 7
iadd
aaload
getfield org/tukaani/xz/lzma/Optimum/price I
if_icmpge L7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iload 7
iadd
aaload
iload 11
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iload 6
invokevirtual org/tukaani/xz/lzma/Optimum/set1(III)V
L7:
iload 7
iconst_1
isub
istore 7
goto L5
L6:
iload 6
ifne L8
iload 8
iconst_1
iadd
istore 5
L8:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/niceLen I
iload 3
iload 8
isub
iconst_1
isub
invokestatic java/lang/Math/min(II)I
istore 7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
iload 8
iconst_1
iadd
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iload 6
iaload
iload 7
invokevirtual org/tukaani/xz/lz/LZEncoder/getMatchLen(III)I
istore 11
iload 5
istore 7
iload 11
iconst_2
if_icmplt L3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLenEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;
iload 8
iload 2
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/getPrice(II)I
istore 7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/nextState Lorg/tukaani/xz/lzma/State;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/set(Lorg/tukaani/xz/lzma/State;)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/nextState Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateLongRep()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
iload 8
iconst_0
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(II)I
istore 12
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
iconst_0
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(I)I
istore 13
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
iload 8
iconst_1
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(II)I
istore 14
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/literalEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
iload 12
iload 13
iload 14
iload 1
iload 8
iadd
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/nextState Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/getPrice(IIIILorg/tukaani/xz/lzma/State;)I
istore 12
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/nextState Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateLiteral()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/posMask I
istore 13
iload 10
iload 7
iadd
iload 12
iadd
aload 0
iconst_0
iload 11
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/nextState Lorg/tukaani/xz/lzma/State;
iload 1
iload 8
iadd
iconst_1
iadd
iload 13
iand
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getLongRepAndLenPrice(IILorg/tukaani/xz/lzma/State;I)I
iadd
istore 10
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iload 8
iadd
iconst_1
iadd
iload 11
iadd
istore 11
L9:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
iload 11
if_icmpge L10
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
astore 15
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
iconst_1
iadd
istore 7
aload 0
iload 7
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
aload 15
iload 7
aaload
invokevirtual org/tukaani/xz/lzma/Optimum/reset()V
goto L9
L10:
iload 5
istore 7
iload 10
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 11
aaload
getfield org/tukaani/xz/lzma/Optimum/price I
if_icmpge L3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 11
aaload
iload 10
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iload 6
iload 8
iconst_0
invokevirtual org/tukaani/xz/lzma/Optimum/set3(IIIII)V
iload 5
istore 7
goto L3
L1:
iload 5
ireturn
.limit locals 16
.limit stack 7
.end method

.method private calcNormalMatchPrices(IIIII)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
iaload
iload 3
if_icmple L0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
iconst_0
putfield org/tukaani/xz/lz/Matches/count I
L1:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iaload
iload 3
if_icmpge L2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
astore 13
aload 13
aload 13
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
iadd
putfield org/tukaani/xz/lz/Matches/count I
goto L1
L2:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
astore 13
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
astore 14
aload 14
getfield org/tukaani/xz/lz/Matches/count I
istore 6
aload 14
iload 6
iconst_1
iadd
putfield org/tukaani/xz/lz/Matches/count I
aload 13
iload 6
iload 3
iastore
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
iaload
iload 5
if_icmpge L3
return
L3:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
iaload
iadd
if_icmpge L4
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
astore 13
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
iconst_1
iadd
istore 6
aload 0
iload 6
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
aload 13
iload 6
aaload
invokevirtual org/tukaani/xz/lzma/Optimum/reset()V
goto L3
L4:
aload 0
iload 4
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getNormalMatchPrice(ILorg/tukaani/xz/lzma/State;)I
istore 7
iconst_0
istore 4
L5:
iload 5
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
iload 4
iaload
if_icmple L6
iload 4
iconst_1
iadd
istore 4
goto L5
L6:
iload 5
istore 6
iload 4
istore 5
iload 6
istore 4
L7:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/dist [I
iload 5
iaload
istore 6
aload 0
iload 7
iload 6
iload 4
iload 2
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getMatchAndLenPrice(IIII)I
istore 8
iload 8
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iload 4
iadd
aaload
getfield org/tukaani/xz/lzma/Optimum/price I
if_icmpge L8
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iload 4
iadd
aaload
iload 8
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iload 6
iconst_4
iadd
invokevirtual org/tukaani/xz/lzma/Optimum/set1(III)V
L8:
iload 4
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
iload 5
iaload
if_icmpeq L9
L10:
iload 4
iconst_1
iadd
istore 4
goto L7
L9:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/niceLen I
iload 3
iload 4
isub
iconst_1
isub
invokestatic java/lang/Math/min(II)I
istore 9
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
iload 4
iconst_1
iadd
iload 6
iload 9
invokevirtual org/tukaani/xz/lz/LZEncoder/getMatchLen(III)I
istore 9
iload 9
iconst_2
if_icmplt L11
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/nextState Lorg/tukaani/xz/lzma/State;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/set(Lorg/tukaani/xz/lzma/State;)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/nextState Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateMatch()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
iload 4
iconst_0
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(II)I
istore 10
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
iconst_0
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(I)I
istore 11
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
iload 4
iconst_1
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(II)I
istore 12
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/literalEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
iload 10
iload 11
iload 12
iload 1
iload 4
iadd
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/nextState Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/getPrice(IIIILorg/tukaani/xz/lzma/State;)I
istore 10
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/nextState Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateLiteral()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/posMask I
istore 11
iload 8
iload 10
iadd
aload 0
iconst_0
iload 9
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/nextState Lorg/tukaani/xz/lzma/State;
iload 1
iload 4
iadd
iconst_1
iadd
iload 11
iand
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getLongRepAndLenPrice(IILorg/tukaani/xz/lzma/State;I)I
iadd
istore 8
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iload 4
iadd
iconst_1
iadd
iload 9
iadd
istore 9
L12:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
iload 9
if_icmpge L13
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
astore 13
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
iconst_1
iadd
istore 10
aload 0
iload 10
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
aload 13
iload 10
aaload
invokevirtual org/tukaani/xz/lzma/Optimum/reset()V
goto L12
L13:
iload 8
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 9
aaload
getfield org/tukaani/xz/lzma/Optimum/price I
if_icmpge L11
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 9
aaload
iload 8
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iload 6
iconst_4
iadd
iload 4
iconst_0
invokevirtual org/tukaani/xz/lzma/Optimum/set3(IIIII)V
L11:
iload 5
iconst_1
iadd
istore 6
iload 6
istore 5
iload 6
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
if_icmpne L10
return
.limit locals 15
.limit stack 7
.end method

.method private convertOpts()I
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/optPrev I
istore 1
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
astore 3
iload 1
istore 2
aload 3
getfield org/tukaani/xz/lzma/Optimum/prev1IsLiteral Z
ifeq L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 1
aaload
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
putfield org/tukaani/xz/lzma/Optimum/optPrev I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 1
aaload
iconst_m1
putfield org/tukaani/xz/lzma/Optimum/backPrev I
iload 1
iconst_1
isub
istore 2
aload 0
iload 1
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aload 3
getfield org/tukaani/xz/lzma/Optimum/hasPrev2 Z
ifeq L2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 2
aaload
iload 2
iconst_1
iadd
putfield org/tukaani/xz/lzma/Optimum/optPrev I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 2
aaload
aload 3
getfield org/tukaani/xz/lzma/Optimum/backPrev2 I
putfield org/tukaani/xz/lzma/Optimum/backPrev I
aload 0
iload 2
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aload 3
getfield org/tukaani/xz/lzma/Optimum/optPrev2 I
istore 2
L1:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 2
aaload
getfield org/tukaani/xz/lzma/Optimum/optPrev I
istore 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 2
aaload
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
putfield org/tukaani/xz/lzma/Optimum/optPrev I
aload 0
iload 2
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
ifgt L0
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iconst_0
aaload
getfield org/tukaani/xz/lzma/Optimum/optPrev I
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/backPrev I
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/back I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
ireturn
L2:
goto L1
.limit locals 4
.limit stack 3
.end method

.method static getMemoryUsage(III)I
iload 0
iload 1
getstatic org/tukaani/xz/lzma/LZMAEncoderNormal/EXTRA_SIZE_BEFORE I
invokestatic java/lang/Math/max(II)I
getstatic org/tukaani/xz/lzma/LZMAEncoderNormal/EXTRA_SIZE_AFTER I
sipush 273
iload 2
invokestatic org/tukaani/xz/lz/LZEncoder/getMemoryUsage(IIIII)I
sipush 256
iadd
ireturn
.limit locals 3
.limit stack 5
.end method

.method private updateOptStateAndReps()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/optPrev I
istore 1
getstatic org/tukaani/xz/lzma/LZMAEncoderNormal/$assertionsDisabled Z
ifne L0
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
if_icmplt L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/prev1IsLiteral Z
ifeq L1
iload 1
iconst_1
isub
istore 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/hasPrev2 Z
ifeq L2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/optPrev2 I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/set(Lorg/tukaani/xz/lzma/State;)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/backPrev2 I
iconst_4
if_icmpge L3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateLongRep()V
L4:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateLiteral()V
L5:
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iconst_1
isub
if_icmpne L6
getstatic org/tukaani/xz/lzma/LZMAEncoderNormal/$assertionsDisabled Z
ifne L7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/backPrev I
ifeq L7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/backPrev I
iconst_m1
if_icmpeq L7
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L3:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateMatch()V
goto L4
L2:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 1
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/set(Lorg/tukaani/xz/lzma/State;)V
goto L4
L1:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 1
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/set(Lorg/tukaani/xz/lzma/State;)V
goto L5
L7:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/backPrev I
ifne L8
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateShortRep()V
L9:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 1
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iconst_0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iconst_0
iconst_4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L10:
return
L8:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateLiteral()V
goto L9
L6:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/prev1IsLiteral Z
ifeq L11
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/hasPrev2 Z
ifeq L11
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/optPrev2 I
istore 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/backPrev2 I
istore 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateLongRep()V
L12:
iload 3
iconst_4
if_icmpge L13
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iconst_0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 2
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iload 3
iaload
iastore
iconst_1
istore 1
L14:
iload 1
istore 4
iload 1
iload 3
if_icmpgt L15
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 2
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iload 1
iconst_1
isub
iaload
iastore
iload 1
iconst_1
iadd
istore 1
goto L14
L11:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/backPrev I
istore 3
iload 3
iconst_4
if_icmpge L16
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateLongRep()V
iload 1
istore 2
goto L12
L16:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateMatch()V
iload 1
istore 2
goto L12
L15:
iload 4
iconst_4
if_icmpge L10
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iload 4
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 2
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iload 4
iaload
iastore
iload 4
iconst_1
iadd
istore 4
goto L15
L13:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iconst_0
iload 3
iconst_4
isub
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 2
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iconst_0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iconst_1
iconst_3
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
return
.limit locals 5
.limit stack 5
.end method

.method getNextSymbol()I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
if_icmpge L0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/optPrev I
istore 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
istore 2
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/optPrev I
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/backPrev I
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/back I
iload 1
iload 2
isub
ireturn
L0:
getstatic org/tukaani/xz/lzma/LZMAEncoderNormal/$assertionsDisabled Z
ifne L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
if_icmpeq L1
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L1:
aload 0
iconst_0
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aload 0
iconst_0
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
aload 0
iconst_m1
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/back I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/readAhead I
iconst_m1
if_icmpne L2
aload 0
aload 0
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getMatches()Lorg/tukaani/xz/lz/Matches;
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
L2:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
invokevirtual org/tukaani/xz/lz/LZEncoder/getAvail()I
sipush 273
invokestatic java/lang/Math/min(II)I
istore 4
iload 4
iconst_2
if_icmpge L3
iconst_1
ireturn
L3:
iconst_0
istore 2
iconst_0
istore 1
L4:
iload 1
iconst_4
if_icmpge L5
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLens [I
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/reps [I
iload 1
iaload
iload 4
invokevirtual org/tukaani/xz/lz/LZEncoder/getMatchLen(II)I
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLens [I
iload 1
iaload
iconst_2
if_icmpge L6
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLens [I
iload 1
iconst_0
iastore
iload 2
istore 3
L7:
iload 1
iconst_1
iadd
istore 1
iload 3
istore 2
goto L4
L6:
iload 2
istore 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLens [I
iload 1
iaload
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLens [I
iload 2
iaload
if_icmple L7
iload 1
istore 3
goto L7
L5:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLens [I
iload 2
iaload
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/niceLen I
if_icmplt L8
aload 0
iload 2
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/back I
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLens [I
iload 2
iaload
iconst_1
isub
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/skip(I)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLens [I
iload 2
iaload
ireturn
L8:
iconst_0
istore 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
ifle L9
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
iaload
istore 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/dist [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
iaload
istore 4
iload 3
istore 1
iload 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/niceLen I
if_icmplt L9
aload 0
iload 4
iconst_4
iadd
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/back I
aload 0
iload 3
iconst_1
isub
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/skip(I)V
iload 3
ireturn
L9:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
iconst_0
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(I)I
istore 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/reps [I
iconst_0
iaload
iconst_1
iadd
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(I)I
istore 5
iload 1
iconst_2
if_icmpge L10
iload 3
iload 5
if_icmpeq L10
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLens [I
iload 2
iaload
iconst_2
if_icmpge L10
iconst_1
ireturn
L10:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
invokevirtual org/tukaani/xz/lz/LZEncoder/getPos()I
istore 4
iload 4
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/posMask I
iand
istore 6
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
iconst_1
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(I)I
istore 7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/literalEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;
iload 3
iload 5
iload 7
iload 4
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/getPrice(IIIILorg/tukaani/xz/lzma/State;)I
istore 7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iconst_1
aaload
iload 7
iconst_0
iconst_m1
invokevirtual org/tukaani/xz/lzma/Optimum/set1(III)V
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/state Lorg/tukaani/xz/lzma/State;
iload 6
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getAnyMatchPrice(Lorg/tukaani/xz/lzma/State;I)I
istore 7
aload 0
iload 7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getAnyRepPrice(ILorg/tukaani/xz/lzma/State;)I
istore 8
iload 5
iload 3
if_icmpne L11
aload 0
iload 8
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/state Lorg/tukaani/xz/lzma/State;
iload 6
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getShortRepPrice(ILorg/tukaani/xz/lzma/State;I)I
istore 3
iload 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iconst_1
aaload
getfield org/tukaani/xz/lzma/Optimum/price I
if_icmpge L11
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iconst_1
aaload
iload 3
iconst_0
iconst_0
invokevirtual org/tukaani/xz/lzma/Optimum/set1(III)V
L11:
aload 0
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLens [I
iload 2
iaload
invokestatic java/lang/Math/max(II)I
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
iconst_2
if_icmpge L12
getstatic org/tukaani/xz/lzma/LZMAEncoderNormal/$assertionsDisabled Z
ifne L13
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
ifeq L13
new java/lang/AssertionError
dup
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
invokespecial java/lang/AssertionError/<init>(I)V
athrow
L13:
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iconst_1
aaload
getfield org/tukaani/xz/lzma/Optimum/backPrev I
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/back I
iconst_1
ireturn
L12:
aload 0
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/updatePrices()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iconst_0
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/set(Lorg/tukaani/xz/lzma/State;)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/reps [I
iconst_0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iconst_0
aaload
getfield org/tukaani/xz/lzma/Optimum/reps [I
iconst_0
iconst_4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
istore 2
L14:
iload 2
iconst_2
if_icmplt L15
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 2
aaload
invokevirtual org/tukaani/xz/lzma/Optimum/reset()V
iload 2
iconst_1
isub
istore 2
goto L14
L15:
iconst_0
istore 2
L16:
iload 2
iconst_4
if_icmpge L17
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLens [I
iload 2
iaload
istore 3
iload 3
iconst_2
if_icmpge L18
L19:
iload 2
iconst_1
iadd
istore 2
goto L16
L18:
aload 0
iload 8
iload 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/state Lorg/tukaani/xz/lzma/State;
iload 6
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getLongRepPrice(IILorg/tukaani/xz/lzma/State;I)I
istore 9
L20:
iload 9
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLenEncoder Lorg/tukaani/xz/lzma/LZMAEncoder$LengthEncoder;
iload 3
iload 6
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/getPrice(II)I
iadd
istore 5
iload 5
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 3
aaload
getfield org/tukaani/xz/lzma/Optimum/price I
if_icmpge L21
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 3
aaload
iload 5
iconst_0
iload 2
invokevirtual org/tukaani/xz/lzma/Optimum/set1(III)V
L21:
iload 3
iconst_1
isub
istore 5
iload 5
istore 3
iload 5
iconst_2
if_icmpge L20
goto L19
L17:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/repLens [I
iconst_0
iaload
iconst_1
iadd
iconst_2
invokestatic java/lang/Math/max(II)I
istore 5
iload 5
iload 1
if_icmpgt L22
aload 0
iload 7
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getNormalMatchPrice(ILorg/tukaani/xz/lzma/State;)I
istore 7
iconst_0
istore 1
L23:
iload 1
istore 3
iload 5
istore 2
iload 5
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
iload 1
iaload
if_icmple L24
iload 1
iconst_1
iadd
istore 1
goto L23
L25:
iload 2
iconst_1
iadd
istore 2
iload 1
istore 3
L24:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/dist [I
iload 3
iaload
istore 1
aload 0
iload 7
iload 1
iload 2
iload 6
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getMatchAndLenPrice(IIII)I
istore 5
iload 5
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 2
aaload
getfield org/tukaani/xz/lzma/Optimum/price I
if_icmpge L26
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
iload 2
aaload
iload 5
iconst_0
iload 1
iconst_4
iadd
invokevirtual org/tukaani/xz/lzma/Optimum/set1(III)V
L26:
iload 3
istore 1
iload 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
iload 3
iaload
if_icmpne L25
iload 3
iconst_1
iadd
istore 3
iload 3
istore 1
iload 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
if_icmpne L25
L22:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/lz Lorg/tukaani/xz/lz/LZEncoder;
invokevirtual org/tukaani/xz/lz/LZEncoder/getAvail()I
sipush 4095
invokestatic java/lang/Math/min(II)I
istore 1
iload 4
istore 2
L27:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iconst_1
iadd
istore 3
aload 0
iload 3
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
iload 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
if_icmpge L28
aload 0
aload 0
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getMatches()Lorg/tukaani/xz/lz/Matches;
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
ifle L29
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/len [I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
iconst_1
isub
iaload
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/niceLen I
if_icmplt L29
L28:
aload 0
invokespecial org/tukaani/xz/lzma/LZMAEncoderNormal/convertOpts()I
ireturn
L29:
iload 1
iconst_1
isub
istore 3
iload 2
iconst_1
iadd
istore 4
iload 4
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/posMask I
iand
istore 5
aload 0
invokespecial org/tukaani/xz/lzma/LZMAEncoderNormal/updateOptStateAndReps()V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/price I
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
iload 5
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getAnyMatchPrice(Lorg/tukaani/xz/lzma/State;I)I
iadd
istore 6
aload 0
iload 6
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/opts [Lorg/tukaani/xz/lzma/Optimum;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aaload
getfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/LZMAEncoderNormal/getAnyRepPrice(ILorg/tukaani/xz/lzma/State;)I
istore 7
aload 0
iload 4
iload 5
iload 3
iload 7
invokespecial org/tukaani/xz/lzma/LZMAEncoderNormal/calc1BytePrices(IIII)V
iload 4
istore 2
iload 3
istore 1
iload 3
iconst_2
if_icmplt L27
aload 0
iload 4
iload 5
iload 3
iload 7
invokespecial org/tukaani/xz/lzma/LZMAEncoderNormal/calcLongRepPrices(IIII)I
istore 7
iload 4
istore 2
iload 3
istore 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoderNormal/matches Lorg/tukaani/xz/lz/Matches;
getfield org/tukaani/xz/lz/Matches/count I
ifle L27
aload 0
iload 4
iload 5
iload 3
iload 6
iload 7
invokespecial org/tukaani/xz/lzma/LZMAEncoderNormal/calcNormalMatchPrices(IIIII)V
iload 4
istore 2
iload 3
istore 1
goto L27
.limit locals 10
.limit stack 6
.end method

.method public reset()V
aload 0
iconst_0
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optCur I
aload 0
iconst_0
putfield org/tukaani/xz/lzma/LZMAEncoderNormal/optEnd I
aload 0
invokespecial org/tukaani/xz/lzma/LZMAEncoder/reset()V
return
.limit locals 1
.limit stack 2
.end method
