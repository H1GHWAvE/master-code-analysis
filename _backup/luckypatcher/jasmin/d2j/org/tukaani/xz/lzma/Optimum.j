.bytecode 50.0
.class final synchronized org/tukaani/xz/lzma/Optimum
.super java/lang/Object

.field private static final 'INFINITY_PRICE' I = 1073741824


.field 'backPrev' I

.field 'backPrev2' I

.field 'hasPrev2' Z

.field 'optPrev' I

.field 'optPrev2' I

.field 'prev1IsLiteral' Z

.field 'price' I

.field final 'reps' [I

.field final 'state' Lorg/tukaani/xz/lzma/State;

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new org/tukaani/xz/lzma/State
dup
invokespecial org/tukaani/xz/lzma/State/<init>()V
putfield org/tukaani/xz/lzma/Optimum/state Lorg/tukaani/xz/lzma/State;
aload 0
iconst_4
newarray int
putfield org/tukaani/xz/lzma/Optimum/reps [I
return
.limit locals 1
.limit stack 3
.end method

.method reset()V
aload 0
ldc_w 1073741824
putfield org/tukaani/xz/lzma/Optimum/price I
return
.limit locals 1
.limit stack 2
.end method

.method set1(III)V
aload 0
iload 1
putfield org/tukaani/xz/lzma/Optimum/price I
aload 0
iload 2
putfield org/tukaani/xz/lzma/Optimum/optPrev I
aload 0
iload 3
putfield org/tukaani/xz/lzma/Optimum/backPrev I
aload 0
iconst_0
putfield org/tukaani/xz/lzma/Optimum/prev1IsLiteral Z
return
.limit locals 4
.limit stack 2
.end method

.method set2(III)V
aload 0
iload 1
putfield org/tukaani/xz/lzma/Optimum/price I
aload 0
iload 2
iconst_1
iadd
putfield org/tukaani/xz/lzma/Optimum/optPrev I
aload 0
iload 3
putfield org/tukaani/xz/lzma/Optimum/backPrev I
aload 0
iconst_1
putfield org/tukaani/xz/lzma/Optimum/prev1IsLiteral Z
aload 0
iconst_0
putfield org/tukaani/xz/lzma/Optimum/hasPrev2 Z
return
.limit locals 4
.limit stack 3
.end method

.method set3(IIIII)V
aload 0
iload 1
putfield org/tukaani/xz/lzma/Optimum/price I
aload 0
iload 2
iload 4
iadd
iconst_1
iadd
putfield org/tukaani/xz/lzma/Optimum/optPrev I
aload 0
iload 5
putfield org/tukaani/xz/lzma/Optimum/backPrev I
aload 0
iconst_1
putfield org/tukaani/xz/lzma/Optimum/prev1IsLiteral Z
aload 0
iconst_1
putfield org/tukaani/xz/lzma/Optimum/hasPrev2 Z
aload 0
iload 2
putfield org/tukaani/xz/lzma/Optimum/optPrev2 I
aload 0
iload 3
putfield org/tukaani/xz/lzma/Optimum/backPrev2 I
return
.limit locals 6
.limit stack 3
.end method
