.bytecode 50.0
.class public final synchronized org/tukaani/xz/lzma/LZMADecoder
.super org/tukaani/xz/lzma/LZMACoder
.inner class static synthetic inner org/tukaani/xz/lzma/LZMADecoder$1
.inner class private LengthDecoder inner org/tukaani/xz/lzma/LZMADecoder$LengthDecoder outer org/tukaani/xz/lzma/LZMADecoder
.inner class private LiteralDecoder inner org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder outer org/tukaani/xz/lzma/LZMADecoder
.inner class private LiteralSubdecoder inner org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder$LiteralSubdecoder outer org/tukaani/xz/lzma/LZMADecoder

.field private final 'literalDecoder' Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;

.field private final 'lz' Lorg/tukaani/xz/lz/LZDecoder;

.field private final 'matchLenDecoder' Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;

.field private final 'rc' Lorg/tukaani/xz/rangecoder/RangeDecoder;

.field private final 'repLenDecoder' Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;

.method public <init>(Lorg/tukaani/xz/lz/LZDecoder;Lorg/tukaani/xz/rangecoder/RangeDecoder;III)V
aload 0
iload 5
invokespecial org/tukaani/xz/lzma/LZMACoder/<init>(I)V
aload 0
new org/tukaani/xz/lzma/LZMADecoder$LengthDecoder
dup
aload 0
aconst_null
invokespecial org/tukaani/xz/lzma/LZMADecoder$LengthDecoder/<init>(Lorg/tukaani/xz/lzma/LZMADecoder;Lorg/tukaani/xz/lzma/LZMADecoder$1;)V
putfield org/tukaani/xz/lzma/LZMADecoder/matchLenDecoder Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;
aload 0
new org/tukaani/xz/lzma/LZMADecoder$LengthDecoder
dup
aload 0
aconst_null
invokespecial org/tukaani/xz/lzma/LZMADecoder$LengthDecoder/<init>(Lorg/tukaani/xz/lzma/LZMADecoder;Lorg/tukaani/xz/lzma/LZMADecoder$1;)V
putfield org/tukaani/xz/lzma/LZMADecoder/repLenDecoder Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;
aload 0
aload 1
putfield org/tukaani/xz/lzma/LZMADecoder/lz Lorg/tukaani/xz/lz/LZDecoder;
aload 0
aload 2
putfield org/tukaani/xz/lzma/LZMADecoder/rc Lorg/tukaani/xz/rangecoder/RangeDecoder;
aload 0
new org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder
dup
aload 0
iload 3
iload 4
invokespecial org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder/<init>(Lorg/tukaani/xz/lzma/LZMADecoder;II)V
putfield org/tukaani/xz/lzma/LZMADecoder/literalDecoder Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;
aload 0
invokevirtual org/tukaani/xz/lzma/LZMADecoder/reset()V
return
.limit locals 6
.limit stack 6
.end method

.method static synthetic access$200(Lorg/tukaani/xz/lzma/LZMADecoder;)Lorg/tukaani/xz/lz/LZDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/lz Lorg/tukaani/xz/lz/LZDecoder;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$300(Lorg/tukaani/xz/lzma/LZMADecoder;)Lorg/tukaani/xz/rangecoder/RangeDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/rc Lorg/tukaani/xz/rangecoder/RangeDecoder;
areturn
.limit locals 1
.limit stack 1
.end method

.method private decodeMatch(I)I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateMatch()V
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_3
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_2
iaload
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_2
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_1
iaload
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_1
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_0
iaload
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/matchLenDecoder Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;
iload 1
invokevirtual org/tukaani/xz/lzma/LZMADecoder$LengthDecoder/decode(I)I
istore 1
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/rc Lorg/tukaani/xz/rangecoder/RangeDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/distSlots [[S
iload 1
invokestatic org/tukaani/xz/lzma/LZMADecoder/getDistState(I)I
aaload
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/decodeBitTree([S)I
istore 2
iload 2
iconst_4
if_icmpge L0
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_0
iload 2
iastore
iload 1
ireturn
L0:
iload 2
iconst_1
ishr
iconst_1
isub
istore 3
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_0
iload 2
iconst_1
iand
iconst_2
ior
iload 3
ishl
iastore
iload 2
bipush 14
if_icmpge L1
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
astore 4
aload 4
iconst_0
aload 4
iconst_0
iaload
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/rc Lorg/tukaani/xz/rangecoder/RangeDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/distSpecial [[S
iload 2
iconst_4
isub
aaload
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/decodeReverseBitTree([S)I
ior
iastore
iload 1
ireturn
L1:
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
astore 4
aload 4
iconst_0
aload 4
iconst_0
iaload
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/rc Lorg/tukaani/xz/rangecoder/RangeDecoder;
iload 3
iconst_4
isub
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/decodeDirectBits(I)I
iconst_4
ishl
ior
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
astore 4
aload 4
iconst_0
aload 4
iconst_0
iaload
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/rc Lorg/tukaani/xz/rangecoder/RangeDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/distAlign [S
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/decodeReverseBitTree([S)I
ior
iastore
iload 1
ireturn
.limit locals 5
.limit stack 7
.end method

.method private decodeRepMatch(I)I
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/rc Lorg/tukaani/xz/rangecoder/RangeDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/isRep0 [S
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/decodeBit([SI)I
ifne L0
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/rc Lorg/tukaani/xz/rangecoder/RangeDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/isRep0Long [[S
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
aaload
iload 1
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/decodeBit([SI)I
ifne L1
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateShortRep()V
iconst_1
ireturn
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/rc Lorg/tukaani/xz/rangecoder/RangeDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/isRep1 [S
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/decodeBit([SI)I
ifne L2
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_1
iaload
istore 2
L3:
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_1
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_0
iaload
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_0
iload 2
iastore
L1:
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateLongRep()V
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/repLenDecoder Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;
iload 1
invokevirtual org/tukaani/xz/lzma/LZMADecoder$LengthDecoder/decode(I)I
ireturn
L2:
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/rc Lorg/tukaani/xz/rangecoder/RangeDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/isRep2 [S
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/decodeBit([SI)I
ifne L4
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_2
iaload
istore 2
L5:
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_2
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_1
iaload
iastore
goto L3
L4:
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_3
iaload
istore 2
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_3
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_2
iaload
iastore
goto L5
.limit locals 3
.limit stack 4
.end method

.method public decode()V
.throws java/io/IOException
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/lz Lorg/tukaani/xz/lz/LZDecoder;
invokevirtual org/tukaani/xz/lz/LZDecoder/repeatPending()V
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/lz Lorg/tukaani/xz/lz/LZDecoder;
invokevirtual org/tukaani/xz/lz/LZDecoder/hasSpace()Z
ifeq L1
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/lz Lorg/tukaani/xz/lz/LZDecoder;
invokevirtual org/tukaani/xz/lz/LZDecoder/getPos()I
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/posMask I
iand
istore 1
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/rc Lorg/tukaani/xz/rangecoder/RangeDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/isMatch [[S
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
aaload
iload 1
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/decodeBit([SI)I
ifne L2
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/literalDecoder Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;
invokevirtual org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder/decode()V
goto L0
L2:
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/rc Lorg/tukaani/xz/rangecoder/RangeDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/isRep [S
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/get()I
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/decodeBit([SI)I
ifne L3
aload 0
iload 1
invokespecial org/tukaani/xz/lzma/LZMADecoder/decodeMatch(I)I
istore 1
L4:
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/lz Lorg/tukaani/xz/lz/LZDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_0
iaload
iload 1
invokevirtual org/tukaani/xz/lz/LZDecoder/repeat(II)V
goto L0
L3:
aload 0
iload 1
invokespecial org/tukaani/xz/lzma/LZMADecoder/decodeRepMatch(I)I
istore 1
goto L4
L1:
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/rc Lorg/tukaani/xz/rangecoder/RangeDecoder;
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/normalize()V
return
.limit locals 2
.limit stack 3
.end method

.method public endMarkerDetected()Z
iconst_0
istore 1
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_0
iaload
iconst_m1
if_icmpne L0
iconst_1
istore 1
L0:
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public reset()V
aload 0
invokespecial org/tukaani/xz/lzma/LZMACoder/reset()V
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/literalDecoder Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;
invokevirtual org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder/reset()V
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/matchLenDecoder Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;
invokevirtual org/tukaani/xz/lzma/LZMADecoder$LengthDecoder/reset()V
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder/repLenDecoder Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;
invokevirtual org/tukaani/xz/lzma/LZMADecoder$LengthDecoder/reset()V
return
.limit locals 1
.limit stack 1
.end method
