.bytecode 50.0
.class synchronized abstract org/tukaani/xz/lzma/LZMACoder
.super java/lang/Object
.inner class abstract LengthCoder inner org/tukaani/xz/lzma/LZMACoder$LengthCoder outer org/tukaani/xz/lzma/LZMACoder
.inner class abstract LiteralCoder inner org/tukaani/xz/lzma/LZMACoder$LiteralCoder outer org/tukaani/xz/lzma/LZMACoder
.inner class abstract LiteralSubcoder inner org/tukaani/xz/lzma/LZMACoder$LiteralCoder$LiteralSubcoder outer org/tukaani/xz/lzma/LZMACoder

.field static final 'ALIGN_BITS' I = 4


.field static final 'ALIGN_MASK' I = 15


.field static final 'ALIGN_SIZE' I = 16


.field static final 'DIST_MODEL_END' I = 14


.field static final 'DIST_MODEL_START' I = 4


.field static final 'DIST_SLOTS' I = 64


.field static final 'DIST_STATES' I = 4


.field static final 'FULL_DISTANCES' I = 128


.field static final 'MATCH_LEN_MAX' I = 273


.field static final 'MATCH_LEN_MIN' I = 2


.field static final 'POS_STATES_MAX' I = 16


.field static final 'REPS' I = 4


.field final 'distAlign' [S

.field final 'distSlots' [[S

.field final 'distSpecial' [[S

.field final 'isMatch' [[S

.field final 'isRep' [S

.field final 'isRep0' [S

.field final 'isRep0Long' [[S

.field final 'isRep1' [S

.field final 'isRep2' [S

.field final 'posMask' I

.field final 'reps' [I

.field final 'state' Lorg/tukaani/xz/lzma/State;

.method <init>(I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_4
newarray int
putfield org/tukaani/xz/lzma/LZMACoder/reps [I
aload 0
new org/tukaani/xz/lzma/State
dup
invokespecial org/tukaani/xz/lzma/State/<init>()V
putfield org/tukaani/xz/lzma/LZMACoder/state Lorg/tukaani/xz/lzma/State;
aload 0
getstatic java/lang/Short/TYPE Ljava/lang/Class;
iconst_2
newarray int
dup
iconst_0
bipush 12
iastore
dup
iconst_1
bipush 16
iastore
invokestatic java/lang/reflect/Array/newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;
checkcast [[S
putfield org/tukaani/xz/lzma/LZMACoder/isMatch [[S
aload 0
bipush 12
newarray short
putfield org/tukaani/xz/lzma/LZMACoder/isRep [S
aload 0
bipush 12
newarray short
putfield org/tukaani/xz/lzma/LZMACoder/isRep0 [S
aload 0
bipush 12
newarray short
putfield org/tukaani/xz/lzma/LZMACoder/isRep1 [S
aload 0
bipush 12
newarray short
putfield org/tukaani/xz/lzma/LZMACoder/isRep2 [S
aload 0
getstatic java/lang/Short/TYPE Ljava/lang/Class;
iconst_2
newarray int
dup
iconst_0
bipush 12
iastore
dup
iconst_1
bipush 16
iastore
invokestatic java/lang/reflect/Array/newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;
checkcast [[S
putfield org/tukaani/xz/lzma/LZMACoder/isRep0Long [[S
aload 0
getstatic java/lang/Short/TYPE Ljava/lang/Class;
iconst_2
newarray int
dup
iconst_0
iconst_4
iastore
dup
iconst_1
bipush 64
iastore
invokestatic java/lang/reflect/Array/newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;
checkcast [[S
putfield org/tukaani/xz/lzma/LZMACoder/distSlots [[S
aload 0
bipush 10
anewarray [S
dup
iconst_0
iconst_2
newarray short
aastore
dup
iconst_1
iconst_2
newarray short
aastore
dup
iconst_2
iconst_4
newarray short
aastore
dup
iconst_3
iconst_4
newarray short
aastore
dup
iconst_4
bipush 8
newarray short
aastore
dup
iconst_5
bipush 8
newarray short
aastore
dup
bipush 6
bipush 16
newarray short
aastore
dup
bipush 7
bipush 16
newarray short
aastore
dup
bipush 8
bipush 32
newarray short
aastore
dup
bipush 9
bipush 32
newarray short
aastore
putfield org/tukaani/xz/lzma/LZMACoder/distSpecial [[S
aload 0
bipush 16
newarray short
putfield org/tukaani/xz/lzma/LZMACoder/distAlign [S
aload 0
iconst_1
iload 1
ishl
iconst_1
isub
putfield org/tukaani/xz/lzma/LZMACoder/posMask I
return
.limit locals 2
.limit stack 6
.end method

.method static final getDistState(I)I
iload 0
bipush 6
if_icmpge L0
iload 0
iconst_2
isub
ireturn
L0:
iconst_3
ireturn
.limit locals 1
.limit stack 2
.end method

.method reset()V
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/reps [I
iconst_0
iconst_0
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/reps [I
iconst_1
iconst_0
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/reps [I
iconst_2
iconst_0
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/reps [I
iconst_3
iconst_0
iastore
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/reset()V
iconst_0
istore 1
L0:
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/isMatch [[S
arraylength
if_icmpge L1
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/isMatch [[S
iload 1
aaload
invokestatic org/tukaani/xz/rangecoder/RangeCoder/initProbs([S)V
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/isRep [S
invokestatic org/tukaani/xz/rangecoder/RangeCoder/initProbs([S)V
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/isRep0 [S
invokestatic org/tukaani/xz/rangecoder/RangeCoder/initProbs([S)V
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/isRep1 [S
invokestatic org/tukaani/xz/rangecoder/RangeCoder/initProbs([S)V
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/isRep2 [S
invokestatic org/tukaani/xz/rangecoder/RangeCoder/initProbs([S)V
iconst_0
istore 1
L2:
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/isRep0Long [[S
arraylength
if_icmpge L3
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/isRep0Long [[S
iload 1
aaload
invokestatic org/tukaani/xz/rangecoder/RangeCoder/initProbs([S)V
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
iconst_0
istore 1
L4:
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/distSlots [[S
arraylength
if_icmpge L5
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/distSlots [[S
iload 1
aaload
invokestatic org/tukaani/xz/rangecoder/RangeCoder/initProbs([S)V
iload 1
iconst_1
iadd
istore 1
goto L4
L5:
iconst_0
istore 1
L6:
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/distSpecial [[S
arraylength
if_icmpge L7
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/distSpecial [[S
iload 1
aaload
invokestatic org/tukaani/xz/rangecoder/RangeCoder/initProbs([S)V
iload 1
iconst_1
iadd
istore 1
goto L6
L7:
aload 0
getfield org/tukaani/xz/lzma/LZMACoder/distAlign [S
invokestatic org/tukaani/xz/rangecoder/RangeCoder/initProbs([S)V
return
.limit locals 2
.limit stack 3
.end method
