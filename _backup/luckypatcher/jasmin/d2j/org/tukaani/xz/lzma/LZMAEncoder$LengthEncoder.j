.bytecode 50.0
.class synchronized org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder
.super org/tukaani/xz/lzma/LZMACoder$LengthCoder
.inner class LengthEncoder inner org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder outer org/tukaani/xz/lzma/LZMAEncoder

.field private static final 'PRICE_UPDATE_INTERVAL' I = 32


.field private final 'counters' [I

.field private final 'prices' [[I

.field final synthetic 'this$0' Lorg/tukaani/xz/lzma/LZMAEncoder;

.method <init>(Lorg/tukaani/xz/lzma/LZMAEncoder;II)V
aload 0
aload 1
putfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
aload 0
aload 1
invokespecial org/tukaani/xz/lzma/LZMACoder$LengthCoder/<init>(Lorg/tukaani/xz/lzma/LZMACoder;)V
iconst_1
iload 2
ishl
istore 2
aload 0
iload 2
newarray int
putfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/counters [I
iload 3
iconst_2
isub
iconst_1
iadd
bipush 16
invokestatic java/lang/Math/max(II)I
istore 3
aload 0
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
iconst_2
newarray int
dup
iconst_0
iload 2
iastore
dup
iconst_1
iload 3
iastore
invokestatic java/lang/reflect/Array/newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;
checkcast [[I
putfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/prices [[I
return
.limit locals 4
.limit stack 6
.end method

.method private updatePrices(I)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/choice [S
iconst_0
saload
iconst_0
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
istore 3
iconst_0
istore 2
L0:
iload 2
bipush 8
if_icmpge L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/prices [[I
iload 1
aaload
iload 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/low [[S
iload 1
aaload
iload 2
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitTreePrice([SI)I
iload 3
iadd
iastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/choice [S
iconst_0
saload
iconst_1
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
istore 3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/choice [S
iconst_1
saload
iconst_0
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
istore 4
L2:
iload 2
bipush 16
if_icmpge L3
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/prices [[I
iload 1
aaload
iload 2
iload 3
iload 4
iadd
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/mid [[S
iload 1
aaload
iload 2
bipush 8
isub
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitTreePrice([SI)I
iadd
iastore
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/choice [S
iconst_1
saload
iconst_1
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
istore 4
L4:
iload 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/prices [[I
iload 1
aaload
arraylength
if_icmpge L5
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/prices [[I
iload 1
aaload
iload 2
iload 3
iload 4
iadd
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/high [S
iload 2
bipush 8
isub
bipush 8
isub
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitTreePrice([SI)I
iadd
iastore
iload 2
iconst_1
iadd
istore 2
goto L4
L5:
return
.limit locals 5
.limit stack 6
.end method

.method encode(II)V
iload 1
iconst_2
isub
istore 1
iload 1
bipush 8
if_icmpge L0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
invokestatic org/tukaani/xz/lzma/LZMAEncoder/access$100(Lorg/tukaani/xz/lzma/LZMAEncoder;)Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/choice [S
iconst_0
iconst_0
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
invokestatic org/tukaani/xz/lzma/LZMAEncoder/access$100(Lorg/tukaani/xz/lzma/LZMAEncoder;)Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/low [[S
iload 2
aaload
iload 1
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBitTree([SI)V
L1:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/counters [I
astore 3
aload 3
iload 2
aload 3
iload 2
iaload
iconst_1
isub
iastore
return
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
invokestatic org/tukaani/xz/lzma/LZMAEncoder/access$100(Lorg/tukaani/xz/lzma/LZMAEncoder;)Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/choice [S
iconst_0
iconst_1
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
iload 1
bipush 8
isub
istore 1
iload 1
bipush 8
if_icmpge L2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
invokestatic org/tukaani/xz/lzma/LZMAEncoder/access$100(Lorg/tukaani/xz/lzma/LZMAEncoder;)Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/choice [S
iconst_1
iconst_0
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
invokestatic org/tukaani/xz/lzma/LZMAEncoder/access$100(Lorg/tukaani/xz/lzma/LZMAEncoder;)Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/mid [[S
iload 2
aaload
iload 1
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBitTree([SI)V
goto L1
L2:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
invokestatic org/tukaani/xz/lzma/LZMAEncoder/access$100(Lorg/tukaani/xz/lzma/LZMAEncoder;)Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/choice [S
iconst_1
iconst_1
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBit([SII)V
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
invokestatic org/tukaani/xz/lzma/LZMAEncoder/access$100(Lorg/tukaani/xz/lzma/LZMAEncoder;)Lorg/tukaani/xz/rangecoder/RangeEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/high [S
iload 1
bipush 8
isub
invokevirtual org/tukaani/xz/rangecoder/RangeEncoder/encodeBitTree([SI)V
goto L1
.limit locals 4
.limit stack 4
.end method

.method getPrice(II)I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/prices [[I
iload 2
aaload
iload 1
iconst_2
isub
iaload
ireturn
.limit locals 3
.limit stack 3
.end method

.method reset()V
aload 0
invokespecial org/tukaani/xz/lzma/LZMACoder$LengthCoder/reset()V
iconst_0
istore 1
L0:
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/counters [I
arraylength
if_icmpge L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/counters [I
iload 1
iconst_0
iastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
return
.limit locals 2
.limit stack 3
.end method

.method updatePrices()V
iconst_0
istore 1
L0:
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/counters [I
arraylength
if_icmpge L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/counters [I
iload 1
iaload
ifgt L2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/counters [I
iload 1
bipush 32
iastore
aload 0
iload 1
invokespecial org/tukaani/xz/lzma/LZMAEncoder$LengthEncoder/updatePrices(I)V
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
return
.limit locals 2
.limit stack 3
.end method
