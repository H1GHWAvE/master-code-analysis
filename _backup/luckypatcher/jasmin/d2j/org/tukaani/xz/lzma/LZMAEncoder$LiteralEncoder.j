.bytecode 50.0
.class synchronized org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder
.super org/tukaani/xz/lzma/LZMACoder$LiteralCoder
.inner class LiteralEncoder inner org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder outer org/tukaani/xz/lzma/LZMAEncoder
.inner class private LiteralSubencoder inner org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder outer org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder

.field static final synthetic '$assertionsDisabled' Z

.field 'subencoders' [Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;

.field final synthetic 'this$0' Lorg/tukaani/xz/lzma/LZMAEncoder;

.method static <clinit>()V
ldc org/tukaani/xz/lzma/LZMAEncoder
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/$assertionsDisabled Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method <init>(Lorg/tukaani/xz/lzma/LZMAEncoder;II)V
aload 0
aload 1
putfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
aload 0
aload 1
iload 2
iload 3
invokespecial org/tukaani/xz/lzma/LZMACoder$LiteralCoder/<init>(Lorg/tukaani/xz/lzma/LZMACoder;II)V
aload 0
iconst_1
iload 2
iload 3
iadd
ishl
anewarray org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder
putfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/subencoders [Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;
iconst_0
istore 2
L0:
iload 2
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/subencoders [Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;
arraylength
if_icmpge L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/subencoders [Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;
iload 2
new org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder
dup
aload 0
aconst_null
invokespecial org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/<init>(Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder;Lorg/tukaani/xz/lzma/LZMAEncoder$1;)V
aastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 4
.limit stack 6
.end method

.method encode()V
getstatic org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/$assertionsDisabled Z
ifne L0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
ifge L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
aload 0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
iconst_1
iadd
invokevirtual org/tukaani/xz/lz/LZEncoder/getByte(I)I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/lz Lorg/tukaani/xz/lz/LZEncoder;
invokevirtual org/tukaani/xz/lz/LZEncoder/getPos()I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
isub
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/getSubcoderIndex(II)I
istore 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/subencoders [Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;
iload 1
aaload
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/encode()V
return
.limit locals 2
.limit stack 4
.end method

.method encodeInit()V
getstatic org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/$assertionsDisabled Z
ifne L0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/readAhead I
ifge L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/subencoders [Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;
iconst_0
aaload
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/encode()V
return
.limit locals 1
.limit stack 2
.end method

.method getPrice(IIIILorg/tukaani/xz/lzma/State;)I
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/isMatch [[S
aload 5
invokevirtual org/tukaani/xz/lzma/State/get()I
aaload
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/this$0 Lorg/tukaani/xz/lzma/LZMAEncoder;
getfield org/tukaani/xz/lzma/LZMAEncoder/posMask I
iload 4
iand
iaload
iconst_0
invokestatic org/tukaani/xz/rangecoder/RangeEncoder/getBitPrice(II)I
istore 6
aload 0
iload 3
iload 4
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/getSubcoderIndex(II)I
istore 3
aload 5
invokevirtual org/tukaani/xz/lzma/State/isLiteral()Z
ifeq L0
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/subencoders [Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;
iload 3
aaload
iload 1
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/getNormalPrice(I)I
istore 1
L1:
iload 6
iload 1
iadd
ireturn
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/subencoders [Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;
iload 3
aaload
iload 1
iload 2
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/getMatchedPrice(II)I
istore 1
goto L1
.limit locals 7
.limit stack 3
.end method

.method reset()V
iconst_0
istore 1
L0:
iload 1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/subencoders [Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;
arraylength
if_icmpge L1
aload 0
getfield org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder/subencoders [Lorg/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder;
iload 1
aaload
invokevirtual org/tukaani/xz/lzma/LZMAEncoder$LiteralEncoder$LiteralSubencoder/reset()V
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method
