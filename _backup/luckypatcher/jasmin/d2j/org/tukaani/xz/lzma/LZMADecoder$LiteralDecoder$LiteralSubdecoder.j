.bytecode 50.0
.class synchronized org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder$LiteralSubdecoder
.super org/tukaani/xz/lzma/LZMACoder$LiteralCoder$LiteralSubcoder
.inner class private LiteralDecoder inner org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder outer org/tukaani/xz/lzma/LZMADecoder
.inner class private LiteralSubdecoder inner org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder$LiteralSubdecoder outer org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder

.field final synthetic 'this$1' Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;

.method private <init>(Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;)V
aload 0
aload 1
putfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder$LiteralSubdecoder/this$1 Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;
aload 0
aload 1
invokespecial org/tukaani/xz/lzma/LZMACoder$LiteralCoder$LiteralSubcoder/<init>(Lorg/tukaani/xz/lzma/LZMACoder$LiteralCoder;)V
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;Lorg/tukaani/xz/lzma/LZMADecoder$1;)V
aload 0
aload 1
invokespecial org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder$LiteralSubdecoder/<init>(Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;)V
return
.limit locals 3
.limit stack 2
.end method

.method decode()V
.throws java/io/IOException
iconst_1
istore 1
iconst_1
istore 2
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder$LiteralSubdecoder/this$1 Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder/this$0 Lorg/tukaani/xz/lzma/LZMADecoder;
getfield org/tukaani/xz/lzma/LZMADecoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/isLiteral()Z
ifeq L0
iload 2
istore 1
L1:
iload 1
iconst_1
ishl
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder$LiteralSubdecoder/this$1 Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder/this$0 Lorg/tukaani/xz/lzma/LZMADecoder;
invokestatic org/tukaani/xz/lzma/LZMADecoder/access$300(Lorg/tukaani/xz/lzma/LZMADecoder;)Lorg/tukaani/xz/rangecoder/RangeDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder$LiteralSubdecoder/probs [S
iload 1
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/decodeBit([SI)I
ior
istore 2
iload 2
istore 1
iload 2
sipush 256
if_icmplt L1
L2:
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder$LiteralSubdecoder/this$1 Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder/this$0 Lorg/tukaani/xz/lzma/LZMADecoder;
invokestatic org/tukaani/xz/lzma/LZMADecoder/access$200(Lorg/tukaani/xz/lzma/LZMADecoder;)Lorg/tukaani/xz/lz/LZDecoder;
iload 2
i2b
invokevirtual org/tukaani/xz/lz/LZDecoder/putByte(B)V
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder$LiteralSubdecoder/this$1 Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder/this$0 Lorg/tukaani/xz/lzma/LZMADecoder;
getfield org/tukaani/xz/lzma/LZMADecoder/state Lorg/tukaani/xz/lzma/State;
invokevirtual org/tukaani/xz/lzma/State/updateLiteral()V
return
L0:
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder$LiteralSubdecoder/this$1 Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder/this$0 Lorg/tukaani/xz/lzma/LZMADecoder;
invokestatic org/tukaani/xz/lzma/LZMADecoder/access$200(Lorg/tukaani/xz/lzma/LZMADecoder;)Lorg/tukaani/xz/lz/LZDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder$LiteralSubdecoder/this$1 Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder/this$0 Lorg/tukaani/xz/lzma/LZMADecoder;
getfield org/tukaani/xz/lzma/LZMADecoder/reps [I
iconst_0
iaload
invokevirtual org/tukaani/xz/lz/LZDecoder/getByte(I)I
istore 4
sipush 256
istore 2
L3:
iload 4
iconst_1
ishl
istore 4
iload 4
iload 2
iand
istore 5
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder$LiteralSubdecoder/this$1 Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder/this$0 Lorg/tukaani/xz/lzma/LZMADecoder;
invokestatic org/tukaani/xz/lzma/LZMADecoder/access$300(Lorg/tukaani/xz/lzma/LZMADecoder;)Lorg/tukaani/xz/rangecoder/RangeDecoder;
aload 0
getfield org/tukaani/xz/lzma/LZMADecoder$LiteralDecoder$LiteralSubdecoder/probs [S
iload 2
iload 5
iadd
iload 1
iadd
invokevirtual org/tukaani/xz/rangecoder/RangeDecoder/decodeBit([SI)I
istore 6
iload 1
iconst_1
ishl
iload 6
ior
istore 3
iload 2
iconst_0
iload 6
isub
iload 5
iconst_m1
ixor
ixor
iand
istore 2
iload 3
istore 1
iload 3
sipush 256
if_icmplt L3
iload 3
istore 2
goto L2
.limit locals 7
.limit stack 4
.end method
