.bytecode 50.0
.class final synchronized org/tukaani/xz/lzma/State
.super java/lang/Object

.field private static final 'LIT_LIT' I = 0


.field private static final 'LIT_LONGREP' I = 8


.field private static final 'LIT_MATCH' I = 7


.field private static final 'LIT_SHORTREP' I = 9


.field private static final 'LIT_STATES' I = 7


.field private static final 'MATCH_LIT' I = 4


.field private static final 'MATCH_LIT_LIT' I = 1


.field private static final 'NONLIT_MATCH' I = 10


.field private static final 'NONLIT_REP' I = 11


.field private static final 'REP_LIT' I = 5


.field private static final 'REP_LIT_LIT' I = 2


.field private static final 'SHORTREP_LIT' I = 6


.field private static final 'SHORTREP_LIT_LIT' I = 3


.field static final 'STATES' I = 12


.field private 'state' I

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method <init>(Lorg/tukaani/xz/lzma/State;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
getfield org/tukaani/xz/lzma/State/state I
putfield org/tukaani/xz/lzma/State/state I
return
.limit locals 2
.limit stack 2
.end method

.method get()I
aload 0
getfield org/tukaani/xz/lzma/State/state I
ireturn
.limit locals 1
.limit stack 1
.end method

.method isLiteral()Z
aload 0
getfield org/tukaani/xz/lzma/State/state I
bipush 7
if_icmpge L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method reset()V
aload 0
iconst_0
putfield org/tukaani/xz/lzma/State/state I
return
.limit locals 1
.limit stack 2
.end method

.method set(Lorg/tukaani/xz/lzma/State;)V
aload 0
aload 1
getfield org/tukaani/xz/lzma/State/state I
putfield org/tukaani/xz/lzma/State/state I
return
.limit locals 2
.limit stack 2
.end method

.method updateLiteral()V
aload 0
getfield org/tukaani/xz/lzma/State/state I
iconst_3
if_icmpgt L0
aload 0
iconst_0
putfield org/tukaani/xz/lzma/State/state I
return
L0:
aload 0
getfield org/tukaani/xz/lzma/State/state I
bipush 9
if_icmpgt L1
aload 0
aload 0
getfield org/tukaani/xz/lzma/State/state I
iconst_3
isub
putfield org/tukaani/xz/lzma/State/state I
return
L1:
aload 0
aload 0
getfield org/tukaani/xz/lzma/State/state I
bipush 6
isub
putfield org/tukaani/xz/lzma/State/state I
return
.limit locals 1
.limit stack 3
.end method

.method updateLongRep()V
aload 0
getfield org/tukaani/xz/lzma/State/state I
bipush 7
if_icmpge L0
bipush 8
istore 1
L1:
aload 0
iload 1
putfield org/tukaani/xz/lzma/State/state I
return
L0:
bipush 11
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method updateMatch()V
bipush 7
istore 1
aload 0
getfield org/tukaani/xz/lzma/State/state I
bipush 7
if_icmpge L0
L1:
aload 0
iload 1
putfield org/tukaani/xz/lzma/State/state I
return
L0:
bipush 10
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method updateShortRep()V
aload 0
getfield org/tukaani/xz/lzma/State/state I
bipush 7
if_icmpge L0
bipush 9
istore 1
L1:
aload 0
iload 1
putfield org/tukaani/xz/lzma/State/state I
return
L0:
bipush 11
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method
