.bytecode 50.0
.class public synchronized abstract org/tukaani/xz/SeekableInputStream
.super java/io/InputStream

.method public <init>()V
aload 0
invokespecial java/io/InputStream/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public abstract length()J
.throws java/io/IOException
.end method

.method public abstract position()J
.throws java/io/IOException
.end method

.method public abstract seek(J)V
.throws java/io/IOException
.end method

.method public skip(J)J
.throws java/io/IOException
lload 1
lconst_0
lcmp
ifgt L0
L1:
lconst_0
lreturn
L0:
aload 0
invokevirtual org/tukaani/xz/SeekableInputStream/length()J
lstore 7
aload 0
invokevirtual org/tukaani/xz/SeekableInputStream/position()J
lstore 5
lload 5
lload 7
lcmp
ifge L1
lload 1
lstore 3
lload 7
lload 5
lsub
lload 1
lcmp
ifge L2
lload 7
lload 5
lsub
lstore 3
L2:
aload 0
lload 5
lload 3
ladd
invokevirtual org/tukaani/xz/SeekableInputStream/seek(J)V
lload 3
lreturn
.limit locals 9
.limit stack 5
.end method
