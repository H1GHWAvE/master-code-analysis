.bytecode 50.0
.class public synchronized abstract pxb/android/axml/NodeVisitor
.super java/lang/Object

.field public static final 'TYPE_FIRST_INT' I = 16


.field public static final 'TYPE_INT_BOOLEAN' I = 18


.field public static final 'TYPE_INT_HEX' I = 17


.field public static final 'TYPE_REFERENCE' I = 1


.field public static final 'TYPE_STRING' I = 3


.field protected 'nv' Lpxb/android/axml/NodeVisitor;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lpxb/android/axml/NodeVisitor;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield pxb/android/axml/NodeVisitor/nv Lpxb/android/axml/NodeVisitor;
return
.limit locals 2
.limit stack 2
.end method

.method public attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
aload 0
getfield pxb/android/axml/NodeVisitor/nv Lpxb/android/axml/NodeVisitor;
ifnull L0
aload 0
getfield pxb/android/axml/NodeVisitor/nv Lpxb/android/axml/NodeVisitor;
aload 1
aload 2
iload 3
iload 4
aload 5
invokevirtual pxb/android/axml/NodeVisitor/attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
L0:
return
.limit locals 6
.limit stack 6
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
aload 0
getfield pxb/android/axml/NodeVisitor/nv Lpxb/android/axml/NodeVisitor;
ifnull L0
aload 0
getfield pxb/android/axml/NodeVisitor/nv Lpxb/android/axml/NodeVisitor;
aload 1
aload 2
invokevirtual pxb/android/axml/NodeVisitor/child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
areturn
L0:
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method public end()V
aload 0
getfield pxb/android/axml/NodeVisitor/nv Lpxb/android/axml/NodeVisitor;
ifnull L0
aload 0
getfield pxb/android/axml/NodeVisitor/nv Lpxb/android/axml/NodeVisitor;
invokevirtual pxb/android/axml/NodeVisitor/end()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method public line(I)V
aload 0
getfield pxb/android/axml/NodeVisitor/nv Lpxb/android/axml/NodeVisitor;
ifnull L0
aload 0
getfield pxb/android/axml/NodeVisitor/nv Lpxb/android/axml/NodeVisitor;
iload 1
invokevirtual pxb/android/axml/NodeVisitor/line(I)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public text(ILjava/lang/String;)V
aload 0
getfield pxb/android/axml/NodeVisitor/nv Lpxb/android/axml/NodeVisitor;
ifnull L0
aload 0
getfield pxb/android/axml/NodeVisitor/nv Lpxb/android/axml/NodeVisitor;
iload 1
aload 2
invokevirtual pxb/android/axml/NodeVisitor/text(ILjava/lang/String;)V
L0:
return
.limit locals 3
.limit stack 3
.end method
