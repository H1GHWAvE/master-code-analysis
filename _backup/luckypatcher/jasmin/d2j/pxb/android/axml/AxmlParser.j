.bytecode 50.0
.class public synchronized pxb/android/axml/AxmlParser
.super java/lang/Object
.implements pxb/android/ResConst

.field public static final 'END_FILE' I = 7


.field public static final 'END_NS' I = 5


.field public static final 'END_TAG' I = 3


.field public static final 'START_FILE' I = 1


.field public static final 'START_NS' I = 4


.field public static final 'START_TAG' I = 2


.field public static final 'TEXT' I = 6


.field private 'attributeCount' I

.field private 'attrs' Ljava/nio/IntBuffer;

.field private 'classAttribute' I

.field private 'fileSize' I

.field private 'idAttribute' I

.field private 'in' Ljava/nio/ByteBuffer;

.field private 'lineNumber' I

.field private 'nameIdx' I

.field private 'nsIdx' I

.field private 'prefixIdx' I

.field private 'resourceIds' [I

.field private 'strings' [Ljava/lang/String;

.field private 'styleAttribute' I

.field private 'textIdx' I

.method public <init>(Ljava/nio/ByteBuffer;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_m1
putfield pxb/android/axml/AxmlParser/fileSize I
aload 0
aload 1
getstatic java/nio/ByteOrder/LITTLE_ENDIAN Ljava/nio/ByteOrder;
invokevirtual java/nio/ByteBuffer/order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
putfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
return
.limit locals 2
.limit stack 3
.end method

.method public <init>([B)V
aload 0
aload 1
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
invokespecial pxb/android/axml/AxmlParser/<init>(Ljava/nio/ByteBuffer;)V
return
.limit locals 2
.limit stack 2
.end method

.method public getAttrCount()I
aload 0
getfield pxb/android/axml/AxmlParser/attributeCount I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getAttrName(I)Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlParser/attrs Ljava/nio/IntBuffer;
iload 1
iconst_5
imul
iconst_1
iadd
invokevirtual java/nio/IntBuffer/get(I)I
istore 1
aload 0
getfield pxb/android/axml/AxmlParser/strings [Ljava/lang/String;
iload 1
aaload
areturn
.limit locals 2
.limit stack 3
.end method

.method public getAttrNs(I)Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlParser/attrs Ljava/nio/IntBuffer;
iload 1
iconst_5
imul
iconst_0
iadd
invokevirtual java/nio/IntBuffer/get(I)I
istore 1
iload 1
iflt L0
aload 0
getfield pxb/android/axml/AxmlParser/strings [Ljava/lang/String;
iload 1
aaload
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method getAttrRawString(I)Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlParser/attrs Ljava/nio/IntBuffer;
iload 1
iconst_5
imul
iconst_2
iadd
invokevirtual java/nio/IntBuffer/get(I)I
istore 1
iload 1
iflt L0
aload 0
getfield pxb/android/axml/AxmlParser/strings [Ljava/lang/String;
iload 1
aaload
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method public getAttrResId(I)I
aload 0
getfield pxb/android/axml/AxmlParser/resourceIds [I
ifnull L0
aload 0
getfield pxb/android/axml/AxmlParser/attrs Ljava/nio/IntBuffer;
iload 1
iconst_5
imul
iconst_1
iadd
invokevirtual java/nio/IntBuffer/get(I)I
istore 1
iload 1
iflt L0
iload 1
aload 0
getfield pxb/android/axml/AxmlParser/resourceIds [I
arraylength
if_icmpge L0
aload 0
getfield pxb/android/axml/AxmlParser/resourceIds [I
iload 1
iaload
ireturn
L0:
iconst_m1
ireturn
.limit locals 2
.limit stack 3
.end method

.method public getAttrType(I)I
aload 0
getfield pxb/android/axml/AxmlParser/attrs Ljava/nio/IntBuffer;
iload 1
iconst_5
imul
iconst_3
iadd
invokevirtual java/nio/IntBuffer/get(I)I
bipush 24
ishr
ireturn
.limit locals 2
.limit stack 3
.end method

.method public getAttrValue(I)Ljava/lang/Object;
aload 0
getfield pxb/android/axml/AxmlParser/attrs Ljava/nio/IntBuffer;
iload 1
iconst_5
imul
iconst_4
iadd
invokevirtual java/nio/IntBuffer/get(I)I
istore 2
iload 1
aload 0
getfield pxb/android/axml/AxmlParser/idAttribute I
if_icmpne L0
iload 2
aload 0
iload 1
invokevirtual pxb/android/axml/AxmlParser/getAttrRawString(I)Ljava/lang/String;
invokestatic pxb/android/axml/ValueWrapper/wrapId(ILjava/lang/String;)Lpxb/android/axml/ValueWrapper;
areturn
L0:
iload 1
aload 0
getfield pxb/android/axml/AxmlParser/styleAttribute I
if_icmpne L1
iload 2
aload 0
iload 1
invokevirtual pxb/android/axml/AxmlParser/getAttrRawString(I)Ljava/lang/String;
invokestatic pxb/android/axml/ValueWrapper/wrapStyle(ILjava/lang/String;)Lpxb/android/axml/ValueWrapper;
areturn
L1:
iload 1
aload 0
getfield pxb/android/axml/AxmlParser/classAttribute I
if_icmpne L2
iload 2
aload 0
iload 1
invokevirtual pxb/android/axml/AxmlParser/getAttrRawString(I)Ljava/lang/String;
invokestatic pxb/android/axml/ValueWrapper/wrapClass(ILjava/lang/String;)Lpxb/android/axml/ValueWrapper;
areturn
L2:
aload 0
iload 1
invokevirtual pxb/android/axml/AxmlParser/getAttrType(I)I
lookupswitch
3 : L3
18 : L4
default : L5
L5:
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
areturn
L3:
aload 0
getfield pxb/android/axml/AxmlParser/strings [Ljava/lang/String;
iload 2
aaload
areturn
L4:
iload 2
ifeq L6
iconst_1
istore 3
L7:
iload 3
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L6:
iconst_0
istore 3
goto L7
.limit locals 4
.limit stack 3
.end method

.method public getAttributeCount()I
aload 0
getfield pxb/android/axml/AxmlParser/attributeCount I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getLineNumber()I
aload 0
getfield pxb/android/axml/AxmlParser/lineNumber I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getName()Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlParser/strings [Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlParser/nameIdx I
aaload
areturn
.limit locals 1
.limit stack 2
.end method

.method public getNamespacePrefix()Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlParser/strings [Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlParser/prefixIdx I
aaload
areturn
.limit locals 1
.limit stack 2
.end method

.method public getNamespaceUri()Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlParser/nsIdx I
iflt L0
aload 0
getfield pxb/android/axml/AxmlParser/strings [Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlParser/nsIdx I
aaload
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 2
.end method

.method public getText()Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlParser/strings [Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlParser/textIdx I
aaload
areturn
.limit locals 1
.limit stack 2
.end method

.method public next()I
.throws java/io/IOException
aload 0
getfield pxb/android/axml/AxmlParser/fileSize I
ifge L0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
ldc_w 65535
iand
iconst_3
if_icmpeq L1
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L1:
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
putfield pxb/android/axml/AxmlParser/fileSize I
iconst_1
ireturn
L0:
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/position()I
istore 2
L2:
iload 2
aload 0
getfield pxb/android/axml/AxmlParser/fileSize I
if_icmpge L3
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
istore 1
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
istore 3
iload 1
ldc_w 65535
iand
lookupswitch
1 : L4
256 : L5
257 : L6
258 : L7
259 : L8
260 : L9
384 : L10
default : L11
L11:
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L7:
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
putfield pxb/android/axml/AxmlParser/lineNumber I
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
pop
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
putfield pxb/android/axml/AxmlParser/nsIdx I
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
putfield pxb/android/axml/AxmlParser/nameIdx I
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
ldc_w 1310740
if_icmpeq L12
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L12:
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getShort()S
ldc_w 65535
iand
putfield pxb/android/axml/AxmlParser/attributeCount I
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getShort()S
ldc_w 65535
iand
iconst_1
isub
putfield pxb/android/axml/AxmlParser/idAttribute I
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getShort()S
ldc_w 65535
iand
iconst_1
isub
putfield pxb/android/axml/AxmlParser/classAttribute I
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getShort()S
ldc_w 65535
iand
iconst_1
isub
putfield pxb/android/axml/AxmlParser/styleAttribute I
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/asIntBuffer()Ljava/nio/IntBuffer;
putfield pxb/android/axml/AxmlParser/attrs Ljava/nio/IntBuffer;
iconst_2
istore 1
L13:
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
iload 2
iload 3
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
iload 1
ireturn
L8:
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
iload 2
iload 3
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
iconst_3
istore 1
goto L13
L5:
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
putfield pxb/android/axml/AxmlParser/lineNumber I
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
pop
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
putfield pxb/android/axml/AxmlParser/prefixIdx I
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
putfield pxb/android/axml/AxmlParser/nsIdx I
iconst_4
istore 1
goto L13
L6:
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
iload 2
iload 3
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
iconst_5
istore 1
goto L13
L4:
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokestatic pxb/android/StringItems/read(Ljava/nio/ByteBuffer;)[Ljava/lang/String;
putfield pxb/android/axml/AxmlParser/strings [Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
iload 2
iload 3
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
L14:
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/position()I
istore 2
goto L2
L10:
iload 3
iconst_4
idiv
iconst_2
isub
istore 4
aload 0
iload 4
newarray int
putfield pxb/android/axml/AxmlParser/resourceIds [I
iconst_0
istore 1
L15:
iload 1
iload 4
if_icmpge L16
aload 0
getfield pxb/android/axml/AxmlParser/resourceIds [I
iload 1
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
iastore
iload 1
iconst_1
iadd
istore 1
goto L15
L16:
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
iload 2
iload 3
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
goto L14
L9:
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
putfield pxb/android/axml/AxmlParser/lineNumber I
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
pop
aload 0
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
putfield pxb/android/axml/AxmlParser/textIdx I
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
pop
aload 0
getfield pxb/android/axml/AxmlParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
pop
bipush 6
istore 1
goto L13
L3:
bipush 7
ireturn
.limit locals 5
.limit stack 3
.end method
