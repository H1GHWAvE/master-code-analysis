.bytecode 50.0
.class public synchronized pxb/android/axml/Axml
.super pxb/android/axml/AxmlVisitor
.inner class public static Node inner pxb/android/axml/Axml$Node outer pxb/android/axml/Axml
.inner class public static Attr inner pxb/android/axml/Axml$Node$Attr outer pxb/android/axml/Axml
.inner class public static Text inner pxb/android/axml/Axml$Node$Text outer pxb/android/axml/Axml
.inner class public static Ns inner pxb/android/axml/Axml$Ns outer pxb/android/axml/Axml

.field public 'firsts' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/axml/Axml$Node;>;"

.field public 'nses' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/axml/Axml$Ns;>;"

.method public <init>()V
aload 0
invokespecial pxb/android/axml/AxmlVisitor/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/axml/Axml/firsts Ljava/util/List;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/axml/Axml/nses Ljava/util/List;
return
.limit locals 1
.limit stack 3
.end method

.method public accept(Lpxb/android/axml/AxmlVisitor;)V
aload 0
getfield pxb/android/axml/Axml/nses Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axml/Axml$Ns
aload 1
invokevirtual pxb/android/axml/Axml$Ns/accept(Lpxb/android/axml/AxmlVisitor;)V
goto L0
L1:
aload 0
getfield pxb/android/axml/Axml/firsts Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L2:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axml/Axml$Node
aload 1
invokevirtual pxb/android/axml/Axml$Node/accept(Lpxb/android/axml/NodeVisitor;)V
goto L2
L3:
return
.limit locals 3
.limit stack 2
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
new pxb/android/axml/Axml$Node
dup
invokespecial pxb/android/axml/Axml$Node/<init>()V
astore 3
aload 3
aload 2
putfield pxb/android/axml/Axml$Node/name Ljava/lang/String;
aload 3
aload 1
putfield pxb/android/axml/Axml$Node/ns Ljava/lang/String;
aload 0
getfield pxb/android/axml/Axml/firsts Ljava/util/List;
aload 3
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 3
areturn
.limit locals 4
.limit stack 2
.end method

.method public ns(Ljava/lang/String;Ljava/lang/String;I)V
new pxb/android/axml/Axml$Ns
dup
invokespecial pxb/android/axml/Axml$Ns/<init>()V
astore 4
aload 4
aload 1
putfield pxb/android/axml/Axml$Ns/prefix Ljava/lang/String;
aload 4
aload 2
putfield pxb/android/axml/Axml$Ns/uri Ljava/lang/String;
aload 4
iload 3
putfield pxb/android/axml/Axml$Ns/ln I
aload 0
getfield pxb/android/axml/Axml/nses Ljava/util/List;
aload 4
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 5
.limit stack 2
.end method
