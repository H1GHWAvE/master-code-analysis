.bytecode 50.0
.class public synchronized pxb/android/axml/DumpAdapter
.super pxb/android/axml/AxmlVisitor

.field protected 'deep' I

.field protected 'nses' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"

.method public <init>()V
aload 0
aconst_null
invokespecial pxb/android/axml/DumpAdapter/<init>(Lpxb/android/axml/NodeVisitor;)V
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Lpxb/android/axml/NodeVisitor;)V
aload 0
aload 1
iconst_0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
invokespecial pxb/android/axml/DumpAdapter/<init>(Lpxb/android/axml/NodeVisitor;ILjava/util/Map;)V
return
.limit locals 2
.limit stack 5
.end method

.method public <init>(Lpxb/android/axml/NodeVisitor;ILjava/util/Map;)V
.signature "(Lpxb/android/axml/NodeVisitor;ILjava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)V"
aload 0
aload 1
invokespecial pxb/android/axml/AxmlVisitor/<init>(Lpxb/android/axml/NodeVisitor;)V
aload 0
iload 2
putfield pxb/android/axml/DumpAdapter/deep I
aload 0
aload 3
putfield pxb/android/axml/DumpAdapter/nses Ljava/util/Map;
return
.limit locals 4
.limit stack 2
.end method

.method public attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
iconst_0
istore 6
L0:
iload 6
aload 0
getfield pxb/android/axml/DumpAdapter/deep I
if_icmpge L1
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "  "
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
iload 6
iconst_1
iadd
istore 6
goto L0
L1:
aload 1
ifnull L2
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "%s:"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aload 1
invokevirtual pxb/android/axml/DumpAdapter/getPrefix(Ljava/lang/String;)Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
L2:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 2
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
iload 3
iconst_m1
if_icmpeq L3
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "(%08x)"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
L3:
aload 5
instanceof java/lang/String
ifeq L4
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "=[%08x]\"%s\""
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 5
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
L5:
getstatic java/lang/System/out Ljava/io/PrintStream;
invokevirtual java/io/PrintStream/println()V
aload 0
aload 1
aload 2
iload 3
iload 4
aload 5
invokespecial pxb/android/axml/AxmlVisitor/attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
return
L4:
aload 5
instanceof java/lang/Boolean
ifeq L6
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "=[%08x]\"%b\""
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 5
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
goto L5
L6:
aload 5
instanceof pxb/android/axml/ValueWrapper
ifeq L7
aload 5
checkcast pxb/android/axml/ValueWrapper
astore 7
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "=[%08x]@%08x, raw: \"%s\""
iconst_3
anewarray java/lang/Object
dup
iconst_0
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 7
getfield pxb/android/axml/ValueWrapper/ref I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
aload 7
getfield pxb/android/axml/ValueWrapper/raw Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
goto L5
L7:
iload 4
iconst_1
if_icmpne L8
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "=[%08x]@%08x"
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 5
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
goto L5
L8:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "=[%08x]%08x"
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 5
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
goto L5
.limit locals 8
.limit stack 7
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
iconst_0
istore 3
L0:
iload 3
aload 0
getfield pxb/android/axml/DumpAdapter/deep I
if_icmpge L1
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "  "
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "<"
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
aload 1
ifnull L2
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
aload 1
invokevirtual pxb/android/axml/DumpAdapter/getPrefix(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
L2:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 2
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
aload 1
aload 2
invokespecial pxb/android/axml/AxmlVisitor/child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
astore 1
aload 1
ifnull L3
new pxb/android/axml/DumpAdapter
dup
aload 1
aload 0
getfield pxb/android/axml/DumpAdapter/deep I
iconst_1
iadd
aload 0
getfield pxb/android/axml/DumpAdapter/nses Ljava/util/Map;
invokespecial pxb/android/axml/DumpAdapter/<init>(Lpxb/android/axml/NodeVisitor;ILjava/util/Map;)V
areturn
L3:
aconst_null
areturn
.limit locals 4
.limit stack 5
.end method

.method protected getPrefix(Ljava/lang/String;)Ljava/lang/String;
aload 0
getfield pxb/android/axml/DumpAdapter/nses Ljava/util/Map;
ifnull L0
aload 0
getfield pxb/android/axml/DumpAdapter/nses Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 2
aload 2
ifnull L0
aload 2
areturn
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method public ns(Ljava/lang/String;Ljava/lang/String;I)V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
getfield pxb/android/axml/DumpAdapter/nses Ljava/util/Map;
aload 2
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
aload 1
aload 2
iload 3
invokespecial pxb/android/axml/AxmlVisitor/ns(Ljava/lang/String;Ljava/lang/String;I)V
return
.limit locals 4
.limit stack 4
.end method

.method public text(ILjava/lang/String;)V
iconst_0
istore 3
L0:
iload 3
aload 0
getfield pxb/android/axml/DumpAdapter/deep I
iconst_1
iadd
if_icmpge L1
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "  "
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "T: "
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 2
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
iload 1
aload 2
invokespecial pxb/android/axml/AxmlVisitor/text(ILjava/lang/String;)V
return
.limit locals 4
.limit stack 3
.end method
