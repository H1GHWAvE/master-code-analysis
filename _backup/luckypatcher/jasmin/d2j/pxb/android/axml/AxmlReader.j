.bytecode 50.0
.class public synchronized pxb/android/axml/AxmlReader
.super java/lang/Object
.inner class static final inner pxb/android/axml/AxmlReader$1

.field public static final 'EMPTY_VISITOR' Lpxb/android/axml/NodeVisitor;

.field final 'parser' Lpxb/android/axml/AxmlParser;

.method static <clinit>()V
new pxb/android/axml/AxmlReader$1
dup
invokespecial pxb/android/axml/AxmlReader$1/<init>()V
putstatic pxb/android/axml/AxmlReader/EMPTY_VISITOR Lpxb/android/axml/NodeVisitor;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>([B)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new pxb/android/axml/AxmlParser
dup
aload 1
invokespecial pxb/android/axml/AxmlParser/<init>([B)V
putfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
return
.limit locals 2
.limit stack 4
.end method

.method public accept(Lpxb/android/axml/AxmlVisitor;)V
.throws java/io/IOException
new java/util/Stack
dup
invokespecial java/util/Stack/<init>()V
astore 5
aload 1
astore 3
L0:
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
invokevirtual pxb/android/axml/AxmlParser/next()I
tableswitch 2
L1
L2
L3
L0
L4
L5
default : L6
L6:
goto L0
L1:
aload 5
aload 3
invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 3
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
invokevirtual pxb/android/axml/AxmlParser/getNamespaceUri()Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
invokevirtual pxb/android/axml/AxmlParser/getName()Ljava/lang/String;
invokevirtual pxb/android/axml/NodeVisitor/child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
astore 4
aload 4
ifnull L7
aload 4
astore 3
aload 4
getstatic pxb/android/axml/AxmlReader/EMPTY_VISITOR Lpxb/android/axml/NodeVisitor;
if_acmpeq L0
aload 4
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
invokevirtual pxb/android/axml/AxmlParser/getLineNumber()I
invokevirtual pxb/android/axml/NodeVisitor/line(I)V
iconst_0
istore 2
L8:
aload 4
astore 3
iload 2
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
invokevirtual pxb/android/axml/AxmlParser/getAttrCount()I
if_icmpge L0
aload 4
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
iload 2
invokevirtual pxb/android/axml/AxmlParser/getAttrNs(I)Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
iload 2
invokevirtual pxb/android/axml/AxmlParser/getAttrName(I)Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
iload 2
invokevirtual pxb/android/axml/AxmlParser/getAttrResId(I)I
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
iload 2
invokevirtual pxb/android/axml/AxmlParser/getAttrType(I)I
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
iload 2
invokevirtual pxb/android/axml/AxmlParser/getAttrValue(I)Ljava/lang/Object;
invokevirtual pxb/android/axml/NodeVisitor/attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
iload 2
iconst_1
iadd
istore 2
goto L8
L7:
getstatic pxb/android/axml/AxmlReader/EMPTY_VISITOR Lpxb/android/axml/NodeVisitor;
astore 3
goto L0
L2:
aload 3
invokevirtual pxb/android/axml/NodeVisitor/end()V
aload 5
invokevirtual java/util/Stack/pop()Ljava/lang/Object;
checkcast pxb/android/axml/NodeVisitor
astore 3
goto L0
L3:
aload 1
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
invokevirtual pxb/android/axml/AxmlParser/getNamespacePrefix()Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
invokevirtual pxb/android/axml/AxmlParser/getNamespaceUri()Ljava/lang/String;
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
invokevirtual pxb/android/axml/AxmlParser/getLineNumber()I
invokevirtual pxb/android/axml/AxmlVisitor/ns(Ljava/lang/String;Ljava/lang/String;I)V
goto L0
L4:
aload 3
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
invokevirtual pxb/android/axml/AxmlParser/getLineNumber()I
aload 0
getfield pxb/android/axml/AxmlReader/parser Lpxb/android/axml/AxmlParser;
invokevirtual pxb/android/axml/AxmlParser/getText()Ljava/lang/String;
invokevirtual pxb/android/axml/NodeVisitor/text(ILjava/lang/String;)V
goto L0
L5:
return
.limit locals 6
.limit stack 7
.end method
