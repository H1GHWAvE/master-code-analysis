.bytecode 50.0
.class synchronized pxb/android/axml/AxmlWriter$NodeImpl
.super pxb/android/axml/NodeVisitor
.inner class static NodeImpl inner pxb/android/axml/AxmlWriter$NodeImpl outer pxb/android/axml/AxmlWriter

.field private 'attrs' Ljava/util/Set; signature "Ljava/util/Set<Lpxb/android/axml/AxmlWriter$Attr;>;"

.field private 'children' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/axml/AxmlWriter$NodeImpl;>;"

.field 'clz' Lpxb/android/axml/AxmlWriter$Attr;

.field 'id' Lpxb/android/axml/AxmlWriter$Attr;

.field private 'line' I

.field private 'name' Lpxb/android/StringItem;

.field private 'ns' Lpxb/android/StringItem;

.field 'style' Lpxb/android/axml/AxmlWriter$Attr;

.field private 'text' Lpxb/android/StringItem;

.field private 'textLineNumber' I

.method public <init>(Ljava/lang/String;Ljava/lang/String;)V
aconst_null
astore 3
aload 0
aconst_null
invokespecial pxb/android/axml/NodeVisitor/<init>(Lpxb/android/axml/NodeVisitor;)V
aload 0
new java/util/TreeSet
dup
getstatic pxb/android/axml/AxmlWriter/ATTR_CMP Ljava/util/Comparator;
invokespecial java/util/TreeSet/<init>(Ljava/util/Comparator;)V
putfield pxb/android/axml/AxmlWriter$NodeImpl/attrs Ljava/util/Set;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/axml/AxmlWriter$NodeImpl/children Ljava/util/List;
aload 1
ifnonnull L0
aconst_null
astore 1
L1:
aload 0
aload 1
putfield pxb/android/axml/AxmlWriter$NodeImpl/ns Lpxb/android/StringItem;
aload 2
ifnonnull L2
aload 3
astore 1
L3:
aload 0
aload 1
putfield pxb/android/axml/AxmlWriter$NodeImpl/name Lpxb/android/StringItem;
return
L0:
new pxb/android/StringItem
dup
aload 1
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
astore 1
goto L1
L2:
new pxb/android/StringItem
dup
aload 2
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
astore 1
goto L3
.limit locals 4
.limit stack 4
.end method

.method public attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
aload 2
ifnonnull L0
new java/lang/RuntimeException
dup
ldc "name can't be null"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
ifnonnull L1
aconst_null
astore 1
L2:
new pxb/android/axml/AxmlWriter$Attr
dup
aload 1
new pxb/android/StringItem
dup
aload 2
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
iload 3
invokespecial pxb/android/axml/AxmlWriter$Attr/<init>(Lpxb/android/StringItem;Lpxb/android/StringItem;I)V
astore 1
aload 1
iload 4
putfield pxb/android/axml/AxmlWriter$Attr/type I
aload 5
instanceof pxb/android/axml/ValueWrapper
ifeq L3
aload 5
checkcast pxb/android/axml/ValueWrapper
astore 2
aload 2
getfield pxb/android/axml/ValueWrapper/raw Ljava/lang/String;
ifnull L4
aload 1
new pxb/android/StringItem
dup
aload 2
getfield pxb/android/axml/ValueWrapper/raw Ljava/lang/String;
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
putfield pxb/android/axml/AxmlWriter$Attr/raw Lpxb/android/StringItem;
L4:
aload 1
aload 2
getfield pxb/android/axml/ValueWrapper/ref I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
putfield pxb/android/axml/AxmlWriter$Attr/value Ljava/lang/Object;
aload 2
getfield pxb/android/axml/ValueWrapper/type I
tableswitch 1
L5
L6
L7
default : L8
L8:
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/attrs Ljava/util/Set;
aload 1
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
return
L1:
new pxb/android/StringItem
dup
aload 1
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
astore 1
goto L2
L7:
aload 0
aload 1
putfield pxb/android/axml/AxmlWriter$NodeImpl/clz Lpxb/android/axml/AxmlWriter$Attr;
goto L8
L5:
aload 0
aload 1
putfield pxb/android/axml/AxmlWriter$NodeImpl/id Lpxb/android/axml/AxmlWriter$Attr;
goto L8
L6:
aload 0
aload 1
putfield pxb/android/axml/AxmlWriter$NodeImpl/style Lpxb/android/axml/AxmlWriter$Attr;
goto L8
L3:
iload 4
iconst_3
if_icmpne L9
new pxb/android/StringItem
dup
aload 5
checkcast java/lang/String
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
astore 2
aload 1
aload 2
putfield pxb/android/axml/AxmlWriter$Attr/raw Lpxb/android/StringItem;
aload 1
aload 2
putfield pxb/android/axml/AxmlWriter$Attr/value Ljava/lang/Object;
goto L8
L9:
aload 1
aconst_null
putfield pxb/android/axml/AxmlWriter$Attr/raw Lpxb/android/StringItem;
aload 1
aload 5
putfield pxb/android/axml/AxmlWriter$Attr/value Ljava/lang/Object;
goto L8
.limit locals 6
.limit stack 6
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
new pxb/android/axml/AxmlWriter$NodeImpl
dup
aload 1
aload 2
invokespecial pxb/android/axml/AxmlWriter$NodeImpl/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 1
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/children Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method public end()V
return
.limit locals 1
.limit stack 0
.end method

.method public line(I)V
aload 0
iload 1
putfield pxb/android/axml/AxmlWriter$NodeImpl/line I
return
.limit locals 2
.limit stack 2
.end method

.method public prepare(Lpxb/android/axml/AxmlWriter;)I
aload 0
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/ns Lpxb/android/StringItem;
invokevirtual pxb/android/axml/AxmlWriter/updateNs(Lpxb/android/StringItem;)Lpxb/android/StringItem;
putfield pxb/android/axml/AxmlWriter$NodeImpl/ns Lpxb/android/StringItem;
aload 0
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/name Lpxb/android/StringItem;
invokevirtual pxb/android/axml/AxmlWriter/update(Lpxb/android/StringItem;)Lpxb/android/StringItem;
putfield pxb/android/axml/AxmlWriter$NodeImpl/name Lpxb/android/StringItem;
iconst_0
istore 2
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/attrs Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 4
L0:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axml/AxmlWriter$Attr
astore 5
aload 5
iload 2
putfield pxb/android/axml/AxmlWriter$Attr/index I
aload 5
aload 1
invokevirtual pxb/android/axml/AxmlWriter$Attr/prepare(Lpxb/android/axml/AxmlWriter;)V
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/text Lpxb/android/StringItem;
invokevirtual pxb/android/axml/AxmlWriter/update(Lpxb/android/StringItem;)Lpxb/android/StringItem;
putfield pxb/android/axml/AxmlWriter$NodeImpl/text Lpxb/android/StringItem;
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/attrs Ljava/util/Set;
invokeinterface java/util/Set/size()I 0
bipush 20
imul
bipush 60
iadd
istore 2
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/children Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L2:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
iload 2
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axml/AxmlWriter$NodeImpl
aload 1
invokevirtual pxb/android/axml/AxmlWriter$NodeImpl/prepare(Lpxb/android/axml/AxmlWriter;)I
iadd
istore 2
goto L2
L3:
iload 2
istore 3
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/text Lpxb/android/StringItem;
ifnull L4
iload 2
bipush 28
iadd
istore 3
L4:
iload 3
ireturn
.limit locals 6
.limit stack 3
.end method

.method public text(ILjava/lang/String;)V
aload 0
new pxb/android/StringItem
dup
aload 2
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
putfield pxb/android/axml/AxmlWriter$NodeImpl/text Lpxb/android/StringItem;
aload 0
iload 1
putfield pxb/android/axml/AxmlWriter$NodeImpl/textLineNumber I
return
.limit locals 3
.limit stack 4
.end method

.method write(Ljava/nio/ByteBuffer;)V
.throws java/io/IOException
iconst_m1
istore 3
aload 1
ldc_w 1048834
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/attrs Ljava/util/Set;
invokeinterface java/util/Set/size()I 0
bipush 20
imul
bipush 36
iadd
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/line I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
iconst_m1
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/ns Lpxb/android/StringItem;
ifnull L0
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/ns Lpxb/android/StringItem;
getfield pxb/android/StringItem/index I
istore 2
L1:
aload 1
iload 2
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/name Lpxb/android/StringItem;
getfield pxb/android/StringItem/index I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
ldc_w 1310740
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/attrs Ljava/util/Set;
invokeinterface java/util/Set/size()I 0
i2s
invokevirtual java/nio/ByteBuffer/putShort(S)Ljava/nio/ByteBuffer;
pop
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/id Lpxb/android/axml/AxmlWriter$Attr;
ifnonnull L2
iconst_0
istore 2
L3:
aload 1
iload 2
i2s
invokevirtual java/nio/ByteBuffer/putShort(S)Ljava/nio/ByteBuffer;
pop
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/clz Lpxb/android/axml/AxmlWriter$Attr;
ifnonnull L4
iconst_0
istore 2
L5:
aload 1
iload 2
i2s
invokevirtual java/nio/ByteBuffer/putShort(S)Ljava/nio/ByteBuffer;
pop
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/style Lpxb/android/axml/AxmlWriter$Attr;
ifnonnull L6
iconst_0
istore 2
L7:
aload 1
iload 2
i2s
invokevirtual java/nio/ByteBuffer/putShort(S)Ljava/nio/ByteBuffer;
pop
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/attrs Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 4
L8:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L9
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axml/AxmlWriter$Attr
astore 5
aload 5
getfield pxb/android/axml/AxmlWriter$Attr/ns Lpxb/android/StringItem;
ifnonnull L10
iconst_m1
istore 2
L11:
aload 1
iload 2
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 5
getfield pxb/android/axml/AxmlWriter$Attr/name Lpxb/android/StringItem;
getfield pxb/android/StringItem/index I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 5
getfield pxb/android/axml/AxmlWriter$Attr/raw Lpxb/android/StringItem;
ifnull L12
aload 5
getfield pxb/android/axml/AxmlWriter$Attr/raw Lpxb/android/StringItem;
getfield pxb/android/StringItem/index I
istore 2
L13:
aload 1
iload 2
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 5
getfield pxb/android/axml/AxmlWriter$Attr/type I
bipush 24
ishl
bipush 8
ior
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 5
getfield pxb/android/axml/AxmlWriter$Attr/value Ljava/lang/Object;
astore 6
aload 6
instanceof pxb/android/StringItem
ifeq L14
aload 1
aload 5
getfield pxb/android/axml/AxmlWriter$Attr/value Ljava/lang/Object;
checkcast pxb/android/StringItem
getfield pxb/android/StringItem/index I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
goto L8
L0:
iconst_m1
istore 2
goto L1
L2:
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/id Lpxb/android/axml/AxmlWriter$Attr;
getfield pxb/android/axml/AxmlWriter$Attr/index I
iconst_1
iadd
istore 2
goto L3
L4:
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/clz Lpxb/android/axml/AxmlWriter$Attr;
getfield pxb/android/axml/AxmlWriter$Attr/index I
iconst_1
iadd
istore 2
goto L5
L6:
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/style Lpxb/android/axml/AxmlWriter$Attr;
getfield pxb/android/axml/AxmlWriter$Attr/index I
iconst_1
iadd
istore 2
goto L7
L10:
aload 5
getfield pxb/android/axml/AxmlWriter$Attr/ns Lpxb/android/StringItem;
getfield pxb/android/StringItem/index I
istore 2
goto L11
L12:
iconst_m1
istore 2
goto L13
L14:
aload 6
instanceof java/lang/Boolean
ifeq L15
getstatic java/lang/Boolean/TRUE Ljava/lang/Boolean;
aload 6
invokevirtual java/lang/Boolean/equals(Ljava/lang/Object;)Z
ifeq L16
iconst_m1
istore 2
L17:
aload 1
iload 2
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
goto L8
L16:
iconst_0
istore 2
goto L17
L15:
aload 1
aload 5
getfield pxb/android/axml/AxmlWriter$Attr/value Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
goto L8
L9:
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/text Lpxb/android/StringItem;
ifnull L18
aload 1
ldc_w 1048836
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
bipush 28
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/textLineNumber I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
iconst_m1
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/text Lpxb/android/StringItem;
getfield pxb/android/StringItem/index I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
bipush 8
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
iconst_0
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
L18:
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/children Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L19:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L20
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axml/AxmlWriter$NodeImpl
aload 1
invokevirtual pxb/android/axml/AxmlWriter$NodeImpl/write(Ljava/nio/ByteBuffer;)V
goto L19
L20:
aload 1
ldc_w 1048835
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
bipush 24
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
iconst_m1
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
iconst_m1
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
iload 3
istore 2
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/ns Lpxb/android/StringItem;
ifnull L21
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/ns Lpxb/android/StringItem;
getfield pxb/android/StringItem/index I
istore 2
L21:
aload 1
iload 2
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$NodeImpl/name Lpxb/android/StringItem;
getfield pxb/android/StringItem/index I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
return
.limit locals 7
.limit stack 3
.end method
