.bytecode 50.0
.class public synchronized pxb/android/axml/AxmlWriter
.super pxb/android/axml/AxmlVisitor
.inner class static final inner pxb/android/axml/AxmlWriter$1
.inner class static Attr inner pxb/android/axml/AxmlWriter$Attr outer pxb/android/axml/AxmlWriter
.inner class static NodeImpl inner pxb/android/axml/AxmlWriter$NodeImpl outer pxb/android/axml/AxmlWriter
.inner class static Ns inner pxb/android/axml/AxmlWriter$Ns outer pxb/android/axml/AxmlWriter

.field static final 'ATTR_CMP' Ljava/util/Comparator; signature "Ljava/util/Comparator<Lpxb/android/axml/AxmlWriter$Attr;>;"

.field private 'firsts' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/axml/AxmlWriter$NodeImpl;>;"

.field private 'nses' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Lpxb/android/axml/AxmlWriter$Ns;>;"

.field private 'otherString' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/StringItem;>;"

.field private 'resourceId2Str' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Lpxb/android/StringItem;>;"

.field private 'resourceIds' Ljava/util/List; signature "Ljava/util/List<Ljava/lang/Integer;>;"

.field private 'resourceString' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/StringItem;>;"

.field private 'stringItems' Lpxb/android/StringItems;

.method static <clinit>()V
new pxb/android/axml/AxmlWriter$1
dup
invokespecial pxb/android/axml/AxmlWriter$1/<init>()V
putstatic pxb/android/axml/AxmlWriter/ATTR_CMP Ljava/util/Comparator;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial pxb/android/axml/AxmlVisitor/<init>()V
aload 0
new java/util/ArrayList
dup
iconst_3
invokespecial java/util/ArrayList/<init>(I)V
putfield pxb/android/axml/AxmlWriter/firsts Ljava/util/List;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield pxb/android/axml/AxmlWriter/nses Ljava/util/Map;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/axml/AxmlWriter/otherString Ljava/util/List;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield pxb/android/axml/AxmlWriter/resourceId2Str Ljava/util/Map;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/axml/AxmlWriter/resourceIds Ljava/util/List;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/axml/AxmlWriter/resourceString Ljava/util/List;
aload 0
new pxb/android/StringItems
dup
invokespecial pxb/android/StringItems/<init>()V
putfield pxb/android/axml/AxmlWriter/stringItems Lpxb/android/StringItems;
return
.limit locals 1
.limit stack 4
.end method

.method private prepare()I
.throws java/io/IOException
iconst_0
istore 1
aload 0
getfield pxb/android/axml/AxmlWriter/firsts Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 5
L0:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
iload 1
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axml/AxmlWriter$NodeImpl
aload 0
invokevirtual pxb/android/axml/AxmlWriter$NodeImpl/prepare(Lpxb/android/axml/AxmlWriter;)I
iadd
istore 1
goto L0
L1:
iconst_0
istore 2
aload 0
getfield pxb/android/axml/AxmlWriter/nses Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 7
L2:
aload 7
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 7
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 8
aload 8
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast pxb/android/axml/AxmlWriter$Ns
astore 6
aload 6
astore 5
aload 6
ifnonnull L4
new pxb/android/axml/AxmlWriter$Ns
dup
aconst_null
new pxb/android/StringItem
dup
aload 8
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/String
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
iconst_0
invokespecial pxb/android/axml/AxmlWriter$Ns/<init>(Lpxb/android/StringItem;Lpxb/android/StringItem;I)V
astore 5
aload 8
aload 5
invokeinterface java/util/Map$Entry/setValue(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
L4:
iload 2
istore 3
aload 5
getfield pxb/android/axml/AxmlWriter$Ns/prefix Lpxb/android/StringItem;
ifnonnull L5
aload 5
new pxb/android/StringItem
dup
ldc "axml_auto_%02d"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
putfield pxb/android/axml/AxmlWriter$Ns/prefix Lpxb/android/StringItem;
iload 2
iconst_1
iadd
istore 3
L5:
aload 5
aload 0
aload 5
getfield pxb/android/axml/AxmlWriter$Ns/prefix Lpxb/android/StringItem;
invokevirtual pxb/android/axml/AxmlWriter/update(Lpxb/android/StringItem;)Lpxb/android/StringItem;
putfield pxb/android/axml/AxmlWriter$Ns/prefix Lpxb/android/StringItem;
aload 5
aload 0
aload 5
getfield pxb/android/axml/AxmlWriter$Ns/uri Lpxb/android/StringItem;
invokevirtual pxb/android/axml/AxmlWriter/update(Lpxb/android/StringItem;)Lpxb/android/StringItem;
putfield pxb/android/axml/AxmlWriter$Ns/uri Lpxb/android/StringItem;
iload 3
istore 2
goto L2
L3:
aload 0
getfield pxb/android/axml/AxmlWriter/nses Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
istore 4
aload 0
getfield pxb/android/axml/AxmlWriter/stringItems Lpxb/android/StringItems;
aload 0
getfield pxb/android/axml/AxmlWriter/resourceString Ljava/util/List;
invokevirtual pxb/android/StringItems/addAll(Ljava/util/Collection;)Z
pop
aload 0
aconst_null
putfield pxb/android/axml/AxmlWriter/resourceString Ljava/util/List;
aload 0
getfield pxb/android/axml/AxmlWriter/stringItems Lpxb/android/StringItems;
aload 0
getfield pxb/android/axml/AxmlWriter/otherString Ljava/util/List;
invokevirtual pxb/android/StringItems/addAll(Ljava/util/Collection;)Z
pop
aload 0
aconst_null
putfield pxb/android/axml/AxmlWriter/otherString Ljava/util/List;
aload 0
getfield pxb/android/axml/AxmlWriter/stringItems Lpxb/android/StringItems;
invokevirtual pxb/android/StringItems/prepare()V
aload 0
getfield pxb/android/axml/AxmlWriter/stringItems Lpxb/android/StringItems;
invokevirtual pxb/android/StringItems/getSize()I
istore 3
iload 3
istore 2
iload 3
iconst_4
irem
ifeq L6
iload 3
iconst_4
iload 3
iconst_4
irem
isub
iadd
istore 2
L6:
iload 1
iload 4
bipush 24
imul
iconst_2
imul
iadd
iload 2
bipush 8
iadd
iadd
aload 0
getfield pxb/android/axml/AxmlWriter/resourceIds Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_4
imul
bipush 8
iadd
iadd
ireturn
.limit locals 9
.limit stack 8
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
new pxb/android/axml/AxmlWriter$NodeImpl
dup
aload 1
aload 2
invokespecial pxb/android/axml/AxmlWriter$NodeImpl/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 1
aload 0
getfield pxb/android/axml/AxmlWriter/firsts Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method public end()V
return
.limit locals 1
.limit stack 0
.end method

.method public ns(Ljava/lang/String;Ljava/lang/String;I)V
aload 0
getfield pxb/android/axml/AxmlWriter/nses Ljava/util/Map;
astore 4
aload 1
ifnonnull L0
aconst_null
astore 1
L1:
aload 4
aload 2
new pxb/android/axml/AxmlWriter$Ns
dup
aload 1
new pxb/android/StringItem
dup
aload 2
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
iload 3
invokespecial pxb/android/axml/AxmlWriter$Ns/<init>(Lpxb/android/StringItem;Lpxb/android/StringItem;I)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
L0:
new pxb/android/StringItem
dup
aload 1
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
astore 1
goto L1
.limit locals 5
.limit stack 8
.end method

.method public toByteArray()[B
.throws java/io/IOException
aload 0
invokespecial pxb/android/axml/AxmlWriter/prepare()I
bipush 8
iadd
istore 1
iload 1
invokestatic java/nio/ByteBuffer/allocate(I)Ljava/nio/ByteBuffer;
getstatic java/nio/ByteOrder/LITTLE_ENDIAN Ljava/nio/ByteOrder;
invokevirtual java/nio/ByteBuffer/order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
astore 3
aload 3
ldc_w 524291
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 3
iload 1
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 0
getfield pxb/android/axml/AxmlWriter/stringItems Lpxb/android/StringItems;
invokevirtual pxb/android/StringItems/getSize()I
istore 2
iconst_0
istore 1
iload 2
iconst_4
irem
ifeq L0
iconst_4
iload 2
iconst_4
irem
isub
istore 1
L0:
aload 3
ldc_w 1835009
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 3
iload 2
iload 1
iadd
bipush 8
iadd
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 0
getfield pxb/android/axml/AxmlWriter/stringItems Lpxb/android/StringItems;
aload 3
invokevirtual pxb/android/StringItems/write(Ljava/nio/ByteBuffer;)V
aload 3
iload 1
newarray byte
invokevirtual java/nio/ByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 3
ldc_w 524672
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 3
aload 0
getfield pxb/android/axml/AxmlWriter/resourceIds Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_4
imul
bipush 8
iadd
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 0
getfield pxb/android/axml/AxmlWriter/resourceIds Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L1:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 3
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
goto L1
L2:
new java/util/Stack
dup
invokespecial java/util/Stack/<init>()V
astore 4
aload 0
getfield pxb/android/axml/AxmlWriter/nses Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 5
L3:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast pxb/android/axml/AxmlWriter$Ns
astore 6
aload 4
aload 6
invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 3
ldc_w 1048832
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 3
bipush 24
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 3
iconst_m1
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 3
iconst_m1
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 3
aload 6
getfield pxb/android/axml/AxmlWriter$Ns/prefix Lpxb/android/StringItem;
getfield pxb/android/StringItem/index I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 3
aload 6
getfield pxb/android/axml/AxmlWriter$Ns/uri Lpxb/android/StringItem;
getfield pxb/android/StringItem/index I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
goto L3
L4:
aload 0
getfield pxb/android/axml/AxmlWriter/firsts Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 5
L5:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L6
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axml/AxmlWriter$NodeImpl
aload 3
invokevirtual pxb/android/axml/AxmlWriter$NodeImpl/write(Ljava/nio/ByteBuffer;)V
goto L5
L6:
aload 4
invokevirtual java/util/Stack/size()I
ifle L7
aload 4
invokevirtual java/util/Stack/pop()Ljava/lang/Object;
checkcast pxb/android/axml/AxmlWriter$Ns
astore 5
aload 3
ldc_w 1048833
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 3
bipush 24
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 3
aload 5
getfield pxb/android/axml/AxmlWriter$Ns/ln I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 3
iconst_m1
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 3
aload 5
getfield pxb/android/axml/AxmlWriter$Ns/prefix Lpxb/android/StringItem;
getfield pxb/android/StringItem/index I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 3
aload 5
getfield pxb/android/axml/AxmlWriter$Ns/uri Lpxb/android/StringItem;
getfield pxb/android/StringItem/index I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
goto L6
L7:
aload 3
invokevirtual java/nio/ByteBuffer/array()[B
areturn
.limit locals 7
.limit stack 3
.end method

.method update(Lpxb/android/StringItem;)Lpxb/android/StringItem;
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield pxb/android/axml/AxmlWriter/otherString Ljava/util/List;
aload 1
invokeinterface java/util/List/indexOf(Ljava/lang/Object;)I 1
istore 2
iload 2
ifge L1
new pxb/android/StringItem
dup
aload 1
getfield pxb/android/StringItem/data Ljava/lang/String;
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
astore 1
aload 0
getfield pxb/android/axml/AxmlWriter/otherString Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 1
areturn
L1:
aload 0
getfield pxb/android/axml/AxmlWriter/otherString Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast pxb/android/StringItem
areturn
.limit locals 3
.limit stack 3
.end method

.method updateNs(Lpxb/android/StringItem;)Lpxb/android/StringItem;
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 1
getfield pxb/android/StringItem/data Ljava/lang/String;
astore 2
aload 0
getfield pxb/android/axml/AxmlWriter/nses Ljava/util/Map;
aload 2
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L1
aload 0
getfield pxb/android/axml/AxmlWriter/nses Ljava/util/Map;
aload 2
aconst_null
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L1:
aload 0
aload 1
invokevirtual pxb/android/axml/AxmlWriter/update(Lpxb/android/StringItem;)Lpxb/android/StringItem;
areturn
.limit locals 3
.limit stack 3
.end method

.method updateWithResourceId(Lpxb/android/StringItem;I)Lpxb/android/StringItem;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
getfield pxb/android/StringItem/data Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
aload 0
getfield pxb/android/axml/AxmlWriter/resourceId2Str Ljava/util/Map;
aload 3
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast pxb/android/StringItem
astore 4
aload 4
ifnull L0
aload 4
areturn
L0:
new pxb/android/StringItem
dup
aload 1
getfield pxb/android/StringItem/data Ljava/lang/String;
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
astore 1
aload 0
getfield pxb/android/axml/AxmlWriter/resourceIds Ljava/util/List;
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield pxb/android/axml/AxmlWriter/resourceString Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield pxb/android/axml/AxmlWriter/resourceId2Str Ljava/util/Map;
aload 3
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 1
areturn
.limit locals 5
.limit stack 3
.end method
