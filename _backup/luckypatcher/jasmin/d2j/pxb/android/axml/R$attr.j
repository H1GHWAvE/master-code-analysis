.bytecode 50.0
.class public final synchronized pxb/android/axml/R$attr
.super java/lang/Object
.inner class public static final attr inner pxb/android/axml/R$attr outer pxb/android/axml/R

.field public static final 'absListViewStyle' I = 16842858


.field public static final 'accessibilityEventTypes' I = 16843648


.field public static final 'accessibilityFeedbackType' I = 16843650


.field public static final 'accessibilityFlags' I = 16843652


.field public static final 'accountPreferences' I = 16843423


.field public static final 'accountType' I = 16843407


.field public static final 'action' I = 16842797


.field public static final 'actionBarDivider' I = 16843675


.field public static final 'actionBarItemBackground' I = 16843676


.field public static final 'actionBarSize' I = 16843499


.field public static final 'actionBarSplitStyle' I = 16843656


.field public static final 'actionBarStyle' I = 16843470


.field public static final 'actionBarTabBarStyle' I = 16843508


.field public static final 'actionBarTabStyle' I = 16843507


.field public static final 'actionBarTabTextStyle' I = 16843509


.field public static final 'actionBarWidgetTheme' I = 16843671


.field public static final 'actionButtonStyle' I = 16843480


.field public static final 'actionDropDownStyle' I = 16843479


.field public static final 'actionLayout' I = 16843515


.field public static final 'actionMenuTextAppearance' I = 16843616


.field public static final 'actionMenuTextColor' I = 16843617


.field public static final 'actionModeBackground' I = 16843483


.field public static final 'actionModeCloseButtonStyle' I = 16843511


.field public static final 'actionModeCloseDrawable' I = 16843484


.field public static final 'actionModeCopyDrawable' I = 16843538


.field public static final 'actionModeCutDrawable' I = 16843537


.field public static final 'actionModePasteDrawable' I = 16843539


.field public static final 'actionModeSelectAllDrawable' I = 16843646


.field public static final 'actionModeSplitBackground' I = 16843677


.field public static final 'actionModeStyle' I = 16843668


.field public static final 'actionOverflowButtonStyle' I = 16843510


.field public static final 'actionProviderClass' I = 16843657


.field public static final 'actionViewClass' I = 16843516


.field public static final 'activatedBackgroundIndicator' I = 16843517


.field public static final 'activityCloseEnterAnimation' I = 16842938


.field public static final 'activityCloseExitAnimation' I = 16842939


.field public static final 'activityOpenEnterAnimation' I = 16842936


.field public static final 'activityOpenExitAnimation' I = 16842937


.field public static final 'addStatesFromChildren' I = 16842992


.field public static final 'adjustViewBounds' I = 16843038


.field public static final 'alertDialogIcon' I = 16843605


.field public static final 'alertDialogStyle' I = 16842845


.field public static final 'alertDialogTheme' I = 16843529


.field public static final 'alignmentMode' I = 16843642


.field public static final 'allContactsName' I = 16843468


.field public static final 'allowBackup' I = 16843392


.field public static final 'allowClearUserData' I = 16842757


.field public static final 'allowParallelSyncs' I = 16843570


.field public static final 'allowSingleTap' I = 16843353


.field public static final 'allowTaskReparenting' I = 16843268


.field public static final 'alpha' I = 16843551


.field public static final 'alphabeticShortcut' I = 16843235


.field public static final 'alwaysDrawnWithCache' I = 16842991


.field public static final 'alwaysRetainTaskState' I = 16843267


.field public static final 'angle' I = 16843168


.field public static final 'animateFirstView' I = 16843477


.field public static final 'animateLayoutChanges' I = 16843506


.field public static final 'animateOnClick' I = 16843356


.field public static final 'animation' I = 16843213


.field public static final 'animationCache' I = 16842989


.field public static final 'animationDuration' I = 16843026


.field public static final 'animationOrder' I = 16843214


.field public static final 'animationResolution' I = 16843546

.annotation visible Ljava/lang/Deprecated;
.end annotation
.end field

.field public static final 'antialias' I = 16843034


.field public static final 'anyDensity' I = 16843372


.field public static final 'apiKey' I = 16843281


.field public static final 'author' I = 16843444


.field public static final 'authorities' I = 16842776


.field public static final 'autoAdvanceViewId' I = 16843535


.field public static final 'autoCompleteTextViewStyle' I = 16842859


.field public static final 'autoLink' I = 16842928


.field public static final 'autoStart' I = 16843445


.field public static final 'autoText' I = 16843114

.annotation visible Ljava/lang/Deprecated;
.end annotation
.end field

.field public static final 'autoUrlDetect' I = 16843404


.field public static final 'background' I = 16842964


.field public static final 'backgroundDimAmount' I = 16842802


.field public static final 'backgroundDimEnabled' I = 16843295


.field public static final 'backgroundSplit' I = 16843659


.field public static final 'backgroundStacked' I = 16843658


.field public static final 'backupAgent' I = 16843391


.field public static final 'baseline' I = 16843548


.field public static final 'baselineAlignBottom' I = 16843042


.field public static final 'baselineAligned' I = 16843046


.field public static final 'baselineAlignedChildIndex' I = 16843047


.field public static final 'borderlessButtonStyle' I = 16843563


.field public static final 'bottom' I = 16843184


.field public static final 'bottomBright' I = 16842957


.field public static final 'bottomDark' I = 16842953


.field public static final 'bottomLeftRadius' I = 16843179


.field public static final 'bottomMedium' I = 16842958


.field public static final 'bottomOffset' I = 16843351


.field public static final 'bottomRightRadius' I = 16843180


.field public static final 'breadCrumbShortTitle' I = 16843524


.field public static final 'breadCrumbTitle' I = 16843523


.field public static final 'bufferType' I = 16843086


.field public static final 'button' I = 16843015


.field public static final 'buttonBarButtonStyle' I = 16843567


.field public static final 'buttonBarStyle' I = 16843566


.field public static final 'buttonStyle' I = 16842824


.field public static final 'buttonStyleInset' I = 16842826


.field public static final 'buttonStyleSmall' I = 16842825


.field public static final 'buttonStyleToggle' I = 16842827


.field public static final 'cacheColorHint' I = 16843009


.field public static final 'calendarViewShown' I = 16843596


.field public static final 'calendarViewStyle' I = 16843613


.field public static final 'canRetrieveWindowContent' I = 16843653


.field public static final 'candidatesTextStyleSpans' I = 16843312


.field public static final 'capitalize' I = 16843113

.annotation visible Ljava/lang/Deprecated;
.end annotation
.end field

.field public static final 'centerBright' I = 16842956


.field public static final 'centerColor' I = 16843275


.field public static final 'centerDark' I = 16842952


.field public static final 'centerMedium' I = 16842959


.field public static final 'centerX' I = 16843170


.field public static final 'centerY' I = 16843171


.field public static final 'checkBoxPreferenceStyle' I = 16842895


.field public static final 'checkMark' I = 16843016


.field public static final 'checkable' I = 16843237


.field public static final 'checkableBehavior' I = 16843232


.field public static final 'checkboxStyle' I = 16842860


.field public static final 'checked' I = 16843014


.field public static final 'checkedButton' I = 16843080


.field public static final 'checkedTextViewStyle' I = 16843720


.field public static final 'childDivider' I = 16843025


.field public static final 'childIndicator' I = 16843020


.field public static final 'childIndicatorLeft' I = 16843023


.field public static final 'childIndicatorRight' I = 16843024


.field public static final 'choiceMode' I = 16843051


.field public static final 'clearTaskOnLaunch' I = 16842773


.field public static final 'clickable' I = 16842981


.field public static final 'clipChildren' I = 16842986


.field public static final 'clipOrientation' I = 16843274


.field public static final 'clipToPadding' I = 16842987


.field public static final 'codes' I = 16843330


.field public static final 'collapseColumns' I = 16843083


.field public static final 'color' I = 16843173


.field public static final 'colorActivatedHighlight' I = 16843664


.field public static final 'colorBackground' I = 16842801


.field public static final 'colorBackgroundCacheHint' I = 16843435


.field public static final 'colorFocusedHighlight' I = 16843663


.field public static final 'colorForeground' I = 16842800


.field public static final 'colorForegroundInverse' I = 16843270


.field public static final 'colorLongPressedHighlight' I = 16843662


.field public static final 'colorMultiSelectHighlight' I = 16843665


.field public static final 'colorPressedHighlight' I = 16843661


.field public static final 'columnCount' I = 16843639


.field public static final 'columnDelay' I = 16843215


.field public static final 'columnOrderPreserved' I = 16843640


.field public static final 'columnWidth' I = 16843031


.field public static final 'compatibleWidthLimitDp' I = 16843621


.field public static final 'completionHint' I = 16843122


.field public static final 'completionHintView' I = 16843123


.field public static final 'completionThreshold' I = 16843124


.field public static final 'configChanges' I = 16842783


.field public static final 'configure' I = 16843357


.field public static final 'constantSize' I = 16843158


.field public static final 'content' I = 16843355


.field public static final 'contentAuthority' I = 16843408


.field public static final 'contentDescription' I = 16843379


.field public static final 'cropToPadding' I = 16843043


.field public static final 'cursorVisible' I = 16843090


.field public static final 'customNavigationLayout' I = 16843474


.field public static final 'customTokens' I = 16843579


.field public static final 'cycles' I = 16843220


.field public static final 'dashGap' I = 16843175


.field public static final 'dashWidth' I = 16843174


.field public static final 'data' I = 16842798


.field public static final 'datePickerStyle' I = 16843612


.field public static final 'dateTextAppearance' I = 16843593


.field public static final 'debuggable' I = 16842767


.field public static final 'defaultValue' I = 16843245


.field public static final 'delay' I = 16843212


.field public static final 'dependency' I = 16843244


.field public static final 'descendantFocusability' I = 16842993


.field public static final 'description' I = 16842784


.field public static final 'detachWallpaper' I = 16843430


.field public static final 'detailColumn' I = 16843427


.field public static final 'detailSocialSummary' I = 16843428


.field public static final 'detailsElementBackground' I = 16843598


.field public static final 'dial' I = 16843010


.field public static final 'dialogIcon' I = 16843252


.field public static final 'dialogLayout' I = 16843255


.field public static final 'dialogMessage' I = 16843251


.field public static final 'dialogPreferenceStyle' I = 16842897


.field public static final 'dialogTheme' I = 16843528


.field public static final 'dialogTitle' I = 16843250


.field public static final 'digits' I = 16843110


.field public static final 'direction' I = 16843217


.field public static final 'directionDescriptions' I = 16843681


.field public static final 'directionPriority' I = 16843218


.field public static final 'disableDependentsState' I = 16843249


.field public static final 'disabledAlpha' I = 16842803


.field public static final 'displayOptions' I = 16843472


.field public static final 'dither' I = 16843036


.field public static final 'divider' I = 16843049


.field public static final 'dividerHeight' I = 16843050


.field public static final 'dividerHorizontal' I = 16843564


.field public static final 'dividerPadding' I = 16843562


.field public static final 'dividerVertical' I = 16843530


.field public static final 'drawSelectorOnTop' I = 16843004


.field public static final 'drawable' I = 16843161


.field public static final 'drawableBottom' I = 16843118


.field public static final 'drawableEnd' I = 16843667


.field public static final 'drawableLeft' I = 16843119


.field public static final 'drawablePadding' I = 16843121


.field public static final 'drawableRight' I = 16843120


.field public static final 'drawableStart' I = 16843666


.field public static final 'drawableTop' I = 16843117


.field public static final 'drawingCacheQuality' I = 16842984


.field public static final 'dropDownAnchor' I = 16843363


.field public static final 'dropDownHeight' I = 16843395


.field public static final 'dropDownHintAppearance' I = 16842888


.field public static final 'dropDownHorizontalOffset' I = 16843436


.field public static final 'dropDownItemStyle' I = 16842886


.field public static final 'dropDownListViewStyle' I = 16842861


.field public static final 'dropDownSelector' I = 16843125


.field public static final 'dropDownSpinnerStyle' I = 16843478


.field public static final 'dropDownVerticalOffset' I = 16843437


.field public static final 'dropDownWidth' I = 16843362


.field public static final 'duplicateParentState' I = 16842985


.field public static final 'duration' I = 16843160


.field public static final 'editTextBackground' I = 16843602


.field public static final 'editTextColor' I = 16843601


.field public static final 'editTextPreferenceStyle' I = 16842898


.field public static final 'editTextStyle' I = 16842862


.field public static final 'editable' I = 16843115

.annotation visible Ljava/lang/Deprecated;
.end annotation
.end field

.field public static final 'editorExtras' I = 16843300


.field public static final 'ellipsize' I = 16842923


.field public static final 'ems' I = 16843096


.field public static final 'enabled' I = 16842766


.field public static final 'endColor' I = 16843166


.field public static final 'endYear' I = 16843133

.annotation visible Ljava/lang/Deprecated;
.end annotation
.end field

.field public static final 'enterFadeDuration' I = 16843532


.field public static final 'entries' I = 16842930


.field public static final 'entryValues' I = 16843256


.field public static final 'eventsInterceptionEnabled' I = 16843389


.field public static final 'excludeFromRecents' I = 16842775


.field public static final 'exitFadeDuration' I = 16843533


.field public static final 'expandableListPreferredChildIndicatorLeft' I = 16842834


.field public static final 'expandableListPreferredChildIndicatorRight' I = 16842835


.field public static final 'expandableListPreferredChildPaddingLeft' I = 16842831


.field public static final 'expandableListPreferredItemIndicatorLeft' I = 16842832


.field public static final 'expandableListPreferredItemIndicatorRight' I = 16842833


.field public static final 'expandableListPreferredItemPaddingLeft' I = 16842830


.field public static final 'expandableListViewStyle' I = 16842863


.field public static final 'expandableListViewWhiteStyle' I = 16843446


.field public static final 'exported' I = 16842768


.field public static final 'extraTension' I = 16843371


.field public static final 'factor' I = 16843219


.field public static final 'fadeDuration' I = 16843384


.field public static final 'fadeEnabled' I = 16843390


.field public static final 'fadeOffset' I = 16843383


.field public static final 'fadeScrollbars' I = 16843434


.field public static final 'fadingEdge' I = 16842975


.field public static final 'fadingEdgeLength' I = 16842976


.field public static final 'fastScrollAlwaysVisible' I = 16843573


.field public static final 'fastScrollEnabled' I = 16843302


.field public static final 'fastScrollOverlayPosition' I = 16843578


.field public static final 'fastScrollPreviewBackgroundLeft' I = 16843575


.field public static final 'fastScrollPreviewBackgroundRight' I = 16843576


.field public static final 'fastScrollTextColor' I = 16843609


.field public static final 'fastScrollThumbDrawable' I = 16843574


.field public static final 'fastScrollTrackDrawable' I = 16843577


.field public static final 'fillAfter' I = 16843197


.field public static final 'fillBefore' I = 16843196


.field public static final 'fillEnabled' I = 16843343


.field public static final 'fillViewport' I = 16843130


.field public static final 'filter' I = 16843035


.field public static final 'filterTouchesWhenObscured' I = 16843460


.field public static final 'finishOnCloseSystemDialogs' I = 16843431


.field public static final 'finishOnTaskLaunch' I = 16842772


.field public static final 'firstDayOfWeek' I = 16843581


.field public static final 'fitsSystemWindows' I = 16842973


.field public static final 'flipInterval' I = 16843129


.field public static final 'focusable' I = 16842970


.field public static final 'focusableInTouchMode' I = 16842971


.field public static final 'focusedMonthDateColor' I = 16843587


.field public static final 'fontFamily' I = 16843692


.field public static final 'footerDividersEnabled' I = 16843311


.field public static final 'foreground' I = 16843017


.field public static final 'foregroundGravity' I = 16843264


.field public static final 'format' I = 16843013


.field public static final 'format12Hour' I = 16843722


.field public static final 'format24Hour' I = 16843723


.field public static final 'fragment' I = 16843491


.field public static final 'fragmentCloseEnterAnimation' I = 16843495


.field public static final 'fragmentCloseExitAnimation' I = 16843496


.field public static final 'fragmentFadeEnterAnimation' I = 16843497


.field public static final 'fragmentFadeExitAnimation' I = 16843498


.field public static final 'fragmentOpenEnterAnimation' I = 16843493


.field public static final 'fragmentOpenExitAnimation' I = 16843494


.field public static final 'freezesText' I = 16843116


.field public static final 'fromAlpha' I = 16843210


.field public static final 'fromDegrees' I = 16843187


.field public static final 'fromXDelta' I = 16843206


.field public static final 'fromXScale' I = 16843202


.field public static final 'fromYDelta' I = 16843208


.field public static final 'fromYScale' I = 16843204


.field public static final 'fullBright' I = 16842954


.field public static final 'fullDark' I = 16842950


.field public static final 'functionalTest' I = 16842787


.field public static final 'galleryItemBackground' I = 16842828


.field public static final 'galleryStyle' I = 16842864


.field public static final 'gestureColor' I = 16843381


.field public static final 'gestureStrokeAngleThreshold' I = 16843388


.field public static final 'gestureStrokeLengthThreshold' I = 16843386


.field public static final 'gestureStrokeSquarenessThreshold' I = 16843387


.field public static final 'gestureStrokeType' I = 16843385


.field public static final 'gestureStrokeWidth' I = 16843380


.field public static final 'glEsVersion' I = 16843393


.field public static final 'gradientRadius' I = 16843172


.field public static final 'grantUriPermissions' I = 16842779


.field public static final 'gravity' I = 16842927


.field public static final 'gridViewStyle' I = 16842865


.field public static final 'groupIndicator' I = 16843019


.field public static final 'hand_hour' I = 16843011


.field public static final 'hand_minute' I = 16843012


.field public static final 'handle' I = 16843354


.field public static final 'handleProfiling' I = 16842786


.field public static final 'hapticFeedbackEnabled' I = 16843358


.field public static final 'hardwareAccelerated' I = 16843475


.field public static final 'hasCode' I = 16842764


.field public static final 'headerBackground' I = 16843055


.field public static final 'headerDividersEnabled' I = 16843310


.field public static final 'height' I = 16843093


.field public static final 'hint' I = 16843088


.field public static final 'homeAsUpIndicator' I = 16843531


.field public static final 'homeLayout' I = 16843549


.field public static final 'horizontalDivider' I = 16843053


.field public static final 'horizontalGap' I = 16843327


.field public static final 'horizontalScrollViewStyle' I = 16843603


.field public static final 'horizontalSpacing' I = 16843028


.field public static final 'host' I = 16842792


.field public static final 'icon' I = 16842754


.field public static final 'iconPreview' I = 16843337


.field public static final 'iconifiedByDefault' I = 16843514


.field public static final 'id' I = 16842960


.field public static final 'ignoreGravity' I = 16843263


.field public static final 'imageButtonStyle' I = 16842866


.field public static final 'imageWellStyle' I = 16842867


.field public static final 'imeActionId' I = 16843366


.field public static final 'imeActionLabel' I = 16843365


.field public static final 'imeExtractEnterAnimation' I = 16843368


.field public static final 'imeExtractExitAnimation' I = 16843369


.field public static final 'imeFullscreenBackground' I = 16843308


.field public static final 'imeOptions' I = 16843364


.field public static final 'imeSubtypeExtraValue' I = 16843502


.field public static final 'imeSubtypeLocale' I = 16843500


.field public static final 'imeSubtypeMode' I = 16843501


.field public static final 'immersive' I = 16843456


.field public static final 'importantForAccessibility' I = 16843690


.field public static final 'inAnimation' I = 16843127


.field public static final 'includeFontPadding' I = 16843103


.field public static final 'includeInGlobalSearch' I = 16843374


.field public static final 'indeterminate' I = 16843065


.field public static final 'indeterminateBehavior' I = 16843070


.field public static final 'indeterminateDrawable' I = 16843067


.field public static final 'indeterminateDuration' I = 16843069


.field public static final 'indeterminateOnly' I = 16843066


.field public static final 'indeterminateProgressStyle' I = 16843544


.field public static final 'indicatorLeft' I = 16843021


.field public static final 'indicatorRight' I = 16843022


.field public static final 'inflatedId' I = 16842995


.field public static final 'initOrder' I = 16842778


.field public static final 'initialKeyguardLayout' I = 16843714


.field public static final 'initialLayout' I = 16843345


.field public static final 'innerRadius' I = 16843359


.field public static final 'innerRadiusRatio' I = 16843163


.field public static final 'inputMethod' I = 16843112

.annotation visible Ljava/lang/Deprecated;
.end annotation
.end field

.field public static final 'inputType' I = 16843296


.field public static final 'insetBottom' I = 16843194


.field public static final 'insetLeft' I = 16843191


.field public static final 'insetRight' I = 16843192


.field public static final 'insetTop' I = 16843193


.field public static final 'installLocation' I = 16843447


.field public static final 'interpolator' I = 16843073


.field public static final 'isAlwaysSyncable' I = 16843571


.field public static final 'isAuxiliary' I = 16843647


.field public static final 'isDefault' I = 16843297


.field public static final 'isIndicator' I = 16843079


.field public static final 'isModifier' I = 16843334


.field public static final 'isRepeatable' I = 16843336


.field public static final 'isScrollContainer' I = 16843342


.field public static final 'isSticky' I = 16843335


.field public static final 'isolatedProcess' I = 16843689


.field public static final 'itemBackground' I = 16843056


.field public static final 'itemIconDisabledAlpha' I = 16843057


.field public static final 'itemPadding' I = 16843565


.field public static final 'itemTextAppearance' I = 16843052


.field public static final 'keepScreenOn' I = 16843286


.field public static final 'key' I = 16843240


.field public static final 'keyBackground' I = 16843315


.field public static final 'keyEdgeFlags' I = 16843333


.field public static final 'keyHeight' I = 16843326


.field public static final 'keyIcon' I = 16843340


.field public static final 'keyLabel' I = 16843339


.field public static final 'keyOutputText' I = 16843338


.field public static final 'keyPreviewHeight' I = 16843321


.field public static final 'keyPreviewLayout' I = 16843319


.field public static final 'keyPreviewOffset' I = 16843320


.field public static final 'keyTextColor' I = 16843318


.field public static final 'keyTextSize' I = 16843316


.field public static final 'keyWidth' I = 16843325


.field public static final 'keyboardLayout' I = 16843691


.field public static final 'keyboardMode' I = 16843341


.field public static final 'keycode' I = 16842949


.field public static final 'killAfterRestore' I = 16843420


.field public static final 'label' I = 16842753


.field public static final 'labelFor' I = 16843718


.field public static final 'labelTextSize' I = 16843317


.field public static final 'largeHeap' I = 16843610


.field public static final 'largeScreens' I = 16843398


.field public static final 'largestWidthLimitDp' I = 16843622


.field public static final 'launchMode' I = 16842781


.field public static final 'layerType' I = 16843604


.field public static final 'layout' I = 16842994


.field public static final 'layoutAnimation' I = 16842988


.field public static final 'layoutDirection' I = 16843698


.field public static final 'layout_above' I = 16843140


.field public static final 'layout_alignBaseline' I = 16843142


.field public static final 'layout_alignBottom' I = 16843146


.field public static final 'layout_alignEnd' I = 16843706


.field public static final 'layout_alignLeft' I = 16843143


.field public static final 'layout_alignParentBottom' I = 16843150


.field public static final 'layout_alignParentEnd' I = 16843708


.field public static final 'layout_alignParentLeft' I = 16843147


.field public static final 'layout_alignParentRight' I = 16843149


.field public static final 'layout_alignParentStart' I = 16843707


.field public static final 'layout_alignParentTop' I = 16843148


.field public static final 'layout_alignRight' I = 16843145


.field public static final 'layout_alignStart' I = 16843705


.field public static final 'layout_alignTop' I = 16843144


.field public static final 'layout_alignWithParentIfMissing' I = 16843154


.field public static final 'layout_below' I = 16843141


.field public static final 'layout_centerHorizontal' I = 16843152


.field public static final 'layout_centerInParent' I = 16843151


.field public static final 'layout_centerVertical' I = 16843153


.field public static final 'layout_column' I = 16843084


.field public static final 'layout_columnSpan' I = 16843645


.field public static final 'layout_gravity' I = 16842931


.field public static final 'layout_height' I = 16842997


.field public static final 'layout_margin' I = 16842998


.field public static final 'layout_marginBottom' I = 16843002


.field public static final 'layout_marginEnd' I = 16843702


.field public static final 'layout_marginLeft' I = 16842999


.field public static final 'layout_marginRight' I = 16843001


.field public static final 'layout_marginStart' I = 16843701


.field public static final 'layout_marginTop' I = 16843000


.field public static final 'layout_row' I = 16843643


.field public static final 'layout_rowSpan' I = 16843644


.field public static final 'layout_scale' I = 16843155


.field public static final 'layout_span' I = 16843085


.field public static final 'layout_toEndOf' I = 16843704


.field public static final 'layout_toLeftOf' I = 16843138


.field public static final 'layout_toRightOf' I = 16843139


.field public static final 'layout_toStartOf' I = 16843703


.field public static final 'layout_weight' I = 16843137


.field public static final 'layout_width' I = 16842996


.field public static final 'layout_x' I = 16843135


.field public static final 'layout_y' I = 16843136


.field public static final 'left' I = 16843181


.field public static final 'lineSpacingExtra' I = 16843287


.field public static final 'lineSpacingMultiplier' I = 16843288


.field public static final 'lines' I = 16843092


.field public static final 'linksClickable' I = 16842929


.field public static final 'listChoiceBackgroundIndicator' I = 16843504


.field public static final 'listChoiceIndicatorMultiple' I = 16843290


.field public static final 'listChoiceIndicatorSingle' I = 16843289


.field public static final 'listDivider' I = 16843284


.field public static final 'listDividerAlertDialog' I = 16843525


.field public static final 'listPopupWindowStyle' I = 16843519


.field public static final 'listPreferredItemHeight' I = 16842829


.field public static final 'listPreferredItemHeightLarge' I = 16843654


.field public static final 'listPreferredItemHeightSmall' I = 16843655


.field public static final 'listPreferredItemPaddingEnd' I = 16843710


.field public static final 'listPreferredItemPaddingLeft' I = 16843683


.field public static final 'listPreferredItemPaddingRight' I = 16843684


.field public static final 'listPreferredItemPaddingStart' I = 16843709


.field public static final 'listSelector' I = 16843003


.field public static final 'listSeparatorTextViewStyle' I = 16843272


.field public static final 'listViewStyle' I = 16842868


.field public static final 'listViewWhiteStyle' I = 16842869


.field public static final 'logo' I = 16843454


.field public static final 'longClickable' I = 16842982


.field public static final 'loopViews' I = 16843527


.field public static final 'manageSpaceActivity' I = 16842756


.field public static final 'mapViewStyle' I = 16842890


.field public static final 'marqueeRepeatLimit' I = 16843293


.field public static final 'max' I = 16843062


.field public static final 'maxDate' I = 16843584


.field public static final 'maxEms' I = 16843095


.field public static final 'maxHeight' I = 16843040


.field public static final 'maxItemsPerRow' I = 16843060


.field public static final 'maxLength' I = 16843104


.field public static final 'maxLevel' I = 16843186


.field public static final 'maxLines' I = 16843091


.field public static final 'maxRows' I = 16843059


.field public static final 'maxSdkVersion' I = 16843377


.field public static final 'maxWidth' I = 16843039


.field public static final 'measureAllChildren' I = 16843018


.field public static final 'measureWithLargestChild' I = 16843476


.field public static final 'mediaRouteButtonStyle' I = 16843693


.field public static final 'mediaRouteTypes' I = 16843694


.field public static final 'menuCategory' I = 16843230


.field public static final 'mimeType' I = 16842790


.field public static final 'minDate' I = 16843583


.field public static final 'minEms' I = 16843098


.field public static final 'minHeight' I = 16843072


.field public static final 'minLevel' I = 16843185


.field public static final 'minLines' I = 16843094


.field public static final 'minResizeHeight' I = 16843670


.field public static final 'minResizeWidth' I = 16843669


.field public static final 'minSdkVersion' I = 16843276


.field public static final 'minWidth' I = 16843071


.field public static final 'mode' I = 16843134


.field public static final 'moreIcon' I = 16843061


.field public static final 'multiprocess' I = 16842771


.field public static final 'name' I = 16842755


.field public static final 'navigationMode' I = 16843471


.field public static final 'negativeButtonText' I = 16843254


.field public static final 'nextFocusDown' I = 16842980


.field public static final 'nextFocusForward' I = 16843580


.field public static final 'nextFocusLeft' I = 16842977


.field public static final 'nextFocusRight' I = 16842978


.field public static final 'nextFocusUp' I = 16842979


.field public static final 'noHistory' I = 16843309


.field public static final 'normalScreens' I = 16843397


.field public static final 'notificationTimeout' I = 16843651


.field public static final 'numColumns' I = 16843032


.field public static final 'numStars' I = 16843076


.field public static final 'numeric' I = 16843109

.annotation visible Ljava/lang/Deprecated;
.end annotation
.end field

.field public static final 'numericShortcut' I = 16843236


.field public static final 'onClick' I = 16843375


.field public static final 'oneshot' I = 16843159


.field public static final 'opacity' I = 16843550


.field public static final 'order' I = 16843242


.field public static final 'orderInCategory' I = 16843231


.field public static final 'ordering' I = 16843490


.field public static final 'orderingFromXml' I = 16843239


.field public static final 'orientation' I = 16842948


.field public static final 'outAnimation' I = 16843128


.field public static final 'overScrollFooter' I = 16843459


.field public static final 'overScrollHeader' I = 16843458


.field public static final 'overScrollMode' I = 16843457


.field public static final 'overridesImplicitlyEnabledSubtype' I = 16843682


.field public static final 'packageNames' I = 16843649


.field public static final 'padding' I = 16842965


.field public static final 'paddingBottom' I = 16842969


.field public static final 'paddingEnd' I = 16843700


.field public static final 'paddingLeft' I = 16842966


.field public static final 'paddingRight' I = 16842968


.field public static final 'paddingStart' I = 16843699


.field public static final 'paddingTop' I = 16842967


.field public static final 'panelBackground' I = 16842846


.field public static final 'panelColorBackground' I = 16842849


.field public static final 'panelColorForeground' I = 16842848


.field public static final 'panelFullBackground' I = 16842847


.field public static final 'panelTextAppearance' I = 16842850


.field public static final 'parentActivityName' I = 16843687


.field public static final 'password' I = 16843100

.annotation visible Ljava/lang/Deprecated;
.end annotation
.end field

.field public static final 'path' I = 16842794


.field public static final 'pathPattern' I = 16842796


.field public static final 'pathPrefix' I = 16842795


.field public static final 'permission' I = 16842758


.field public static final 'permissionFlags' I = 16843719


.field public static final 'permissionGroup' I = 16842762


.field public static final 'permissionGroupFlags' I = 16843717


.field public static final 'persistent' I = 16842765


.field public static final 'persistentDrawingCache' I = 16842990


.field public static final 'phoneNumber' I = 16843111

.annotation visible Ljava/lang/Deprecated;
.end annotation
.end field

.field public static final 'pivotX' I = 16843189


.field public static final 'pivotY' I = 16843190


.field public static final 'popupAnimationStyle' I = 16843465


.field public static final 'popupBackground' I = 16843126


.field public static final 'popupCharacters' I = 16843332


.field public static final 'popupKeyboard' I = 16843331


.field public static final 'popupLayout' I = 16843323


.field public static final 'popupMenuStyle' I = 16843520


.field public static final 'popupWindowStyle' I = 16842870


.field public static final 'port' I = 16842793


.field public static final 'positiveButtonText' I = 16843253


.field public static final 'preferenceCategoryStyle' I = 16842892


.field public static final 'preferenceInformationStyle' I = 16842893


.field public static final 'preferenceLayoutChild' I = 16842900


.field public static final 'preferenceScreenStyle' I = 16842891


.field public static final 'preferenceStyle' I = 16842894


.field public static final 'presentationTheme' I = 16843712


.field public static final 'previewImage' I = 16843482


.field public static final 'priority' I = 16842780


.field public static final 'privateImeOptions' I = 16843299


.field public static final 'process' I = 16842769


.field public static final 'progress' I = 16843063


.field public static final 'progressBarPadding' I = 16843545


.field public static final 'progressBarStyle' I = 16842871


.field public static final 'progressBarStyleHorizontal' I = 16842872


.field public static final 'progressBarStyleInverse' I = 16843399


.field public static final 'progressBarStyleLarge' I = 16842874


.field public static final 'progressBarStyleLargeInverse' I = 16843401


.field public static final 'progressBarStyleSmall' I = 16842873


.field public static final 'progressBarStyleSmallInverse' I = 16843400


.field public static final 'progressBarStyleSmallTitle' I = 16843279


.field public static final 'progressDrawable' I = 16843068


.field public static final 'prompt' I = 16843131


.field public static final 'propertyName' I = 16843489


.field public static final 'protectionLevel' I = 16842761


.field public static final 'publicKey' I = 16843686


.field public static final 'queryActionMsg' I = 16843227


.field public static final 'queryAfterZeroResults' I = 16843394


.field public static final 'queryHint' I = 16843608


.field public static final 'quickContactBadgeStyleSmallWindowLarge' I = 16843443


.field public static final 'quickContactBadgeStyleSmallWindowMedium' I = 16843442


.field public static final 'quickContactBadgeStyleSmallWindowSmall' I = 16843441


.field public static final 'quickContactBadgeStyleWindowLarge' I = 16843440


.field public static final 'quickContactBadgeStyleWindowMedium' I = 16843439


.field public static final 'quickContactBadgeStyleWindowSmall' I = 16843438


.field public static final 'radioButtonStyle' I = 16842878


.field public static final 'radius' I = 16843176


.field public static final 'rating' I = 16843077


.field public static final 'ratingBarStyle' I = 16842876


.field public static final 'ratingBarStyleIndicator' I = 16843280


.field public static final 'ratingBarStyleSmall' I = 16842877


.field public static final 'readPermission' I = 16842759


.field public static final 'repeatCount' I = 16843199


.field public static final 'repeatMode' I = 16843200


.field public static final 'reqFiveWayNav' I = 16843314


.field public static final 'reqHardKeyboard' I = 16843305


.field public static final 'reqKeyboardType' I = 16843304


.field public static final 'reqNavigation' I = 16843306


.field public static final 'reqTouchScreen' I = 16843303


.field public static final 'required' I = 16843406


.field public static final 'requiresFadingEdge' I = 16843685


.field public static final 'requiresSmallestWidthDp' I = 16843620


.field public static final 'resizeMode' I = 16843619


.field public static final 'resizeable' I = 16843405


.field public static final 'resource' I = 16842789


.field public static final 'restoreAnyVersion' I = 16843450


.field public static final 'restoreNeedsApplication' I = 16843421

.annotation visible Ljava/lang/Deprecated;
.end annotation
.end field

.field public static final 'right' I = 16843183


.field public static final 'ringtonePreferenceStyle' I = 16842899


.field public static final 'ringtoneType' I = 16843257


.field public static final 'rotation' I = 16843558


.field public static final 'rotationX' I = 16843559


.field public static final 'rotationY' I = 16843560


.field public static final 'rowCount' I = 16843637


.field public static final 'rowDelay' I = 16843216


.field public static final 'rowEdgeFlags' I = 16843329


.field public static final 'rowHeight' I = 16843058


.field public static final 'rowOrderPreserved' I = 16843638


.field public static final 'saveEnabled' I = 16842983


.field public static final 'scaleGravity' I = 16843262


.field public static final 'scaleHeight' I = 16843261


.field public static final 'scaleType' I = 16843037


.field public static final 'scaleWidth' I = 16843260


.field public static final 'scaleX' I = 16843556


.field public static final 'scaleY' I = 16843557


.field public static final 'scheme' I = 16842791


.field public static final 'screenDensity' I = 16843467


.field public static final 'screenOrientation' I = 16842782


.field public static final 'screenSize' I = 16843466


.field public static final 'scrollHorizontally' I = 16843099


.field public static final 'scrollViewStyle' I = 16842880


.field public static final 'scrollX' I = 16842962


.field public static final 'scrollY' I = 16842963


.field public static final 'scrollbarAlwaysDrawHorizontalTrack' I = 16842856


.field public static final 'scrollbarAlwaysDrawVerticalTrack' I = 16842857


.field public static final 'scrollbarDefaultDelayBeforeFade' I = 16843433


.field public static final 'scrollbarFadeDuration' I = 16843432


.field public static final 'scrollbarSize' I = 16842851


.field public static final 'scrollbarStyle' I = 16842879


.field public static final 'scrollbarThumbHorizontal' I = 16842852


.field public static final 'scrollbarThumbVertical' I = 16842853


.field public static final 'scrollbarTrackHorizontal' I = 16842854


.field public static final 'scrollbarTrackVertical' I = 16842855


.field public static final 'scrollbars' I = 16842974


.field public static final 'scrollingCache' I = 16843006


.field public static final 'searchButtonText' I = 16843269

.annotation visible Ljava/lang/Deprecated;
.end annotation
.end field

.field public static final 'searchMode' I = 16843221


.field public static final 'searchSettingsDescription' I = 16843402


.field public static final 'searchSuggestAuthority' I = 16843222


.field public static final 'searchSuggestIntentAction' I = 16843225


.field public static final 'searchSuggestIntentData' I = 16843226


.field public static final 'searchSuggestPath' I = 16843223


.field public static final 'searchSuggestSelection' I = 16843224


.field public static final 'searchSuggestThreshold' I = 16843373


.field public static final 'secondaryProgress' I = 16843064


.field public static final 'seekBarStyle' I = 16842875


.field public static final 'segmentedButtonStyle' I = 16843568


.field public static final 'selectAllOnFocus' I = 16843102


.field public static final 'selectable' I = 16843238


.field public static final 'selectableItemBackground' I = 16843534


.field public static final 'selectedDateVerticalBar' I = 16843591


.field public static final 'selectedWeekBackgroundColor' I = 16843586


.field public static final 'settingsActivity' I = 16843301


.field public static final 'shadowColor' I = 16843105


.field public static final 'shadowDx' I = 16843106


.field public static final 'shadowDy' I = 16843107


.field public static final 'shadowRadius' I = 16843108


.field public static final 'shape' I = 16843162


.field public static final 'shareInterpolator' I = 16843195


.field public static final 'sharedUserId' I = 16842763


.field public static final 'sharedUserLabel' I = 16843361


.field public static final 'shouldDisableView' I = 16843246


.field public static final 'showAsAction' I = 16843481


.field public static final 'showDefault' I = 16843258


.field public static final 'showDividers' I = 16843561


.field public static final 'showOnLockScreen' I = 16843721


.field public static final 'showSilent' I = 16843259


.field public static final 'showWeekNumber' I = 16843582


.field public static final 'shownWeekCount' I = 16843585


.field public static final 'shrinkColumns' I = 16843082


.field public static final 'singleLine' I = 16843101

.annotation visible Ljava/lang/Deprecated;
.end annotation
.end field

.field public static final 'singleUser' I = 16843711


.field public static final 'smallIcon' I = 16843422


.field public static final 'smallScreens' I = 16843396


.field public static final 'smoothScrollbar' I = 16843313


.field public static final 'soundEffectsEnabled' I = 16843285


.field public static final 'spacing' I = 16843027


.field public static final 'spinnerDropDownItemStyle' I = 16842887


.field public static final 'spinnerItemStyle' I = 16842889


.field public static final 'spinnerMode' I = 16843505


.field public static final 'spinnerStyle' I = 16842881


.field public static final 'spinnersShown' I = 16843595


.field public static final 'splitMotionEvents' I = 16843503


.field public static final 'src' I = 16843033


.field public static final 'stackFromBottom' I = 16843005


.field public static final 'starStyle' I = 16842882


.field public static final 'startColor' I = 16843165


.field public static final 'startOffset' I = 16843198


.field public static final 'startYear' I = 16843132

.annotation visible Ljava/lang/Deprecated;
.end annotation
.end field

.field public static final 'stateNotNeeded' I = 16842774


.field public static final 'state_above_anchor' I = 16842922


.field public static final 'state_accelerated' I = 16843547


.field public static final 'state_activated' I = 16843518


.field public static final 'state_active' I = 16842914


.field public static final 'state_checkable' I = 16842911


.field public static final 'state_checked' I = 16842912


.field public static final 'state_drag_can_accept' I = 16843624


.field public static final 'state_drag_hovered' I = 16843625


.field public static final 'state_empty' I = 16842921


.field public static final 'state_enabled' I = 16842910


.field public static final 'state_expanded' I = 16842920


.field public static final 'state_first' I = 16842916


.field public static final 'state_focused' I = 16842908


.field public static final 'state_hovered' I = 16843623


.field public static final 'state_last' I = 16842918


.field public static final 'state_long_pressable' I = 16843324


.field public static final 'state_middle' I = 16842917


.field public static final 'state_multiline' I = 16843597


.field public static final 'state_pressed' I = 16842919


.field public static final 'state_selected' I = 16842913


.field public static final 'state_single' I = 16842915


.field public static final 'state_window_focused' I = 16842909


.field public static final 'staticWallpaperPreview' I = 16843569


.field public static final 'stepSize' I = 16843078


.field public static final 'stopWithTask' I = 16843626


.field public static final 'streamType' I = 16843273


.field public static final 'stretchColumns' I = 16843081


.field public static final 'stretchMode' I = 16843030


.field public static final 'subtitle' I = 16843473


.field public static final 'subtitleTextStyle' I = 16843513


.field public static final 'subtypeExtraValue' I = 16843674


.field public static final 'subtypeId' I = 16843713


.field public static final 'subtypeLocale' I = 16843673


.field public static final 'suggestActionMsg' I = 16843228


.field public static final 'suggestActionMsgColumn' I = 16843229


.field public static final 'summary' I = 16843241


.field public static final 'summaryColumn' I = 16843426


.field public static final 'summaryOff' I = 16843248


.field public static final 'summaryOn' I = 16843247


.field public static final 'supportsRtl' I = 16843695


.field public static final 'supportsUploading' I = 16843419


.field public static final 'switchMinWidth' I = 16843632


.field public static final 'switchPadding' I = 16843633


.field public static final 'switchPreferenceStyle' I = 16843629


.field public static final 'switchTextAppearance' I = 16843630


.field public static final 'switchTextOff' I = 16843628


.field public static final 'switchTextOn' I = 16843627


.field public static final 'syncable' I = 16842777


.field public static final 'tabStripEnabled' I = 16843453


.field public static final 'tabStripLeft' I = 16843451


.field public static final 'tabStripRight' I = 16843452


.field public static final 'tabWidgetStyle' I = 16842883


.field public static final 'tag' I = 16842961


.field public static final 'targetActivity' I = 16843266


.field public static final 'targetClass' I = 16842799


.field public static final 'targetDescriptions' I = 16843680


.field public static final 'targetPackage' I = 16842785


.field public static final 'targetSdkVersion' I = 16843376


.field public static final 'taskAffinity' I = 16842770


.field public static final 'taskCloseEnterAnimation' I = 16842942


.field public static final 'taskCloseExitAnimation' I = 16842943


.field public static final 'taskOpenEnterAnimation' I = 16842940


.field public static final 'taskOpenExitAnimation' I = 16842941


.field public static final 'taskToBackEnterAnimation' I = 16842946


.field public static final 'taskToBackExitAnimation' I = 16842947


.field public static final 'taskToFrontEnterAnimation' I = 16842944


.field public static final 'taskToFrontExitAnimation' I = 16842945


.field public static final 'tension' I = 16843370


.field public static final 'testOnly' I = 16843378


.field public static final 'text' I = 16843087


.field public static final 'textAlignment' I = 16843697


.field public static final 'textAllCaps' I = 16843660


.field public static final 'textAppearance' I = 16842804


.field public static final 'textAppearanceButton' I = 16843271


.field public static final 'textAppearanceInverse' I = 16842805


.field public static final 'textAppearanceLarge' I = 16842816


.field public static final 'textAppearanceLargeInverse' I = 16842819


.field public static final 'textAppearanceLargePopupMenu' I = 16843521


.field public static final 'textAppearanceListItem' I = 16843678


.field public static final 'textAppearanceListItemSmall' I = 16843679


.field public static final 'textAppearanceMedium' I = 16842817


.field public static final 'textAppearanceMediumInverse' I = 16842820


.field public static final 'textAppearanceSearchResultSubtitle' I = 16843424


.field public static final 'textAppearanceSearchResultTitle' I = 16843425


.field public static final 'textAppearanceSmall' I = 16842818


.field public static final 'textAppearanceSmallInverse' I = 16842821


.field public static final 'textAppearanceSmallPopupMenu' I = 16843522


.field public static final 'textCheckMark' I = 16842822


.field public static final 'textCheckMarkInverse' I = 16842823


.field public static final 'textColor' I = 16842904


.field public static final 'textColorAlertDialogListItem' I = 16843526


.field public static final 'textColorHighlight' I = 16842905


.field public static final 'textColorHighlightInverse' I = 16843599


.field public static final 'textColorHint' I = 16842906


.field public static final 'textColorHintInverse' I = 16842815


.field public static final 'textColorLink' I = 16842907


.field public static final 'textColorLinkInverse' I = 16843600


.field public static final 'textColorPrimary' I = 16842806


.field public static final 'textColorPrimaryDisableOnly' I = 16842807


.field public static final 'textColorPrimaryInverse' I = 16842809


.field public static final 'textColorPrimaryInverseDisableOnly' I = 16843403


.field public static final 'textColorPrimaryInverseNoDisable' I = 16842813


.field public static final 'textColorPrimaryNoDisable' I = 16842811


.field public static final 'textColorSecondary' I = 16842808


.field public static final 'textColorSecondaryInverse' I = 16842810


.field public static final 'textColorSecondaryInverseNoDisable' I = 16842814


.field public static final 'textColorSecondaryNoDisable' I = 16842812


.field public static final 'textColorTertiary' I = 16843282


.field public static final 'textColorTertiaryInverse' I = 16843283


.field public static final 'textCursorDrawable' I = 16843618


.field public static final 'textDirection' I = 16843696


.field public static final 'textEditNoPasteWindowLayout' I = 16843541


.field public static final 'textEditPasteWindowLayout' I = 16843540


.field public static final 'textEditSideNoPasteWindowLayout' I = 16843615


.field public static final 'textEditSidePasteWindowLayout' I = 16843614


.field public static final 'textEditSuggestionItemLayout' I = 16843636


.field public static final 'textFilterEnabled' I = 16843007


.field public static final 'textIsSelectable' I = 16843542


.field public static final 'textOff' I = 16843045


.field public static final 'textOn' I = 16843044


.field public static final 'textScaleX' I = 16843089


.field public static final 'textSelectHandle' I = 16843463


.field public static final 'textSelectHandleLeft' I = 16843461


.field public static final 'textSelectHandleRight' I = 16843462


.field public static final 'textSelectHandleWindowStyle' I = 16843464


.field public static final 'textSize' I = 16842901


.field public static final 'textStyle' I = 16842903


.field public static final 'textSuggestionsWindowStyle' I = 16843635


.field public static final 'textViewStyle' I = 16842884


.field public static final 'theme' I = 16842752


.field public static final 'thickness' I = 16843360


.field public static final 'thicknessRatio' I = 16843164


.field public static final 'thumb' I = 16843074


.field public static final 'thumbOffset' I = 16843075


.field public static final 'thumbTextPadding' I = 16843634


.field public static final 'thumbnail' I = 16843429


.field public static final 'tileMode' I = 16843265


.field public static final 'timeZone' I = 16843724


.field public static final 'tint' I = 16843041


.field public static final 'title' I = 16843233


.field public static final 'titleCondensed' I = 16843234


.field public static final 'titleTextStyle' I = 16843512


.field public static final 'toAlpha' I = 16843211


.field public static final 'toDegrees' I = 16843188


.field public static final 'toXDelta' I = 16843207


.field public static final 'toXScale' I = 16843203


.field public static final 'toYDelta' I = 16843209


.field public static final 'toYScale' I = 16843205


.field public static final 'top' I = 16843182


.field public static final 'topBright' I = 16842955


.field public static final 'topDark' I = 16842951


.field public static final 'topLeftRadius' I = 16843177


.field public static final 'topOffset' I = 16843352


.field public static final 'topRightRadius' I = 16843178


.field public static final 'track' I = 16843631


.field public static final 'transcriptMode' I = 16843008


.field public static final 'transformPivotX' I = 16843552


.field public static final 'transformPivotY' I = 16843553


.field public static final 'translationX' I = 16843554


.field public static final 'translationY' I = 16843555


.field public static final 'type' I = 16843169


.field public static final 'typeface' I = 16842902


.field public static final 'uiOptions' I = 16843672


.field public static final 'uncertainGestureColor' I = 16843382


.field public static final 'unfocusedMonthDateColor' I = 16843588


.field public static final 'unselectedAlpha' I = 16843278


.field public static final 'updatePeriodMillis' I = 16843344


.field public static final 'useDefaultMargins' I = 16843641


.field public static final 'useIntrinsicSizeAsMinimum' I = 16843536


.field public static final 'useLevel' I = 16843167


.field public static final 'userVisible' I = 16843409


.field public static final 'value' I = 16842788


.field public static final 'valueFrom' I = 16843486


.field public static final 'valueTo' I = 16843487


.field public static final 'valueType' I = 16843488


.field public static final 'variablePadding' I = 16843157


.field public static final 'versionCode' I = 16843291


.field public static final 'versionName' I = 16843292


.field public static final 'verticalCorrection' I = 16843322


.field public static final 'verticalDivider' I = 16843054


.field public static final 'verticalGap' I = 16843328


.field public static final 'verticalScrollbarPosition' I = 16843572


.field public static final 'verticalSpacing' I = 16843029


.field public static final 'visibility' I = 16842972


.field public static final 'visible' I = 16843156


.field public static final 'vmSafeMode' I = 16843448


.field public static final 'voiceLanguage' I = 16843349


.field public static final 'voiceLanguageModel' I = 16843347


.field public static final 'voiceMaxResults' I = 16843350


.field public static final 'voicePromptText' I = 16843348


.field public static final 'voiceSearchMode' I = 16843346


.field public static final 'wallpaperCloseEnterAnimation' I = 16843413


.field public static final 'wallpaperCloseExitAnimation' I = 16843414


.field public static final 'wallpaperIntraCloseEnterAnimation' I = 16843417


.field public static final 'wallpaperIntraCloseExitAnimation' I = 16843418


.field public static final 'wallpaperIntraOpenEnterAnimation' I = 16843415


.field public static final 'wallpaperIntraOpenExitAnimation' I = 16843416


.field public static final 'wallpaperOpenEnterAnimation' I = 16843411


.field public static final 'wallpaperOpenExitAnimation' I = 16843412


.field public static final 'webTextViewStyle' I = 16843449


.field public static final 'webViewStyle' I = 16842885


.field public static final 'weekDayTextAppearance' I = 16843592


.field public static final 'weekNumberColor' I = 16843589


.field public static final 'weekSeparatorLineColor' I = 16843590


.field public static final 'weightSum' I = 16843048


.field public static final 'widgetCategory' I = 16843716


.field public static final 'widgetLayout' I = 16843243


.field public static final 'width' I = 16843097


.field public static final 'windowActionBar' I = 16843469


.field public static final 'windowActionBarOverlay' I = 16843492


.field public static final 'windowActionModeOverlay' I = 16843485


.field public static final 'windowAnimationStyle' I = 16842926


.field public static final 'windowBackground' I = 16842836


.field public static final 'windowCloseOnTouchOutside' I = 16843611


.field public static final 'windowContentOverlay' I = 16842841


.field public static final 'windowDisablePreview' I = 16843298


.field public static final 'windowEnableSplitTouch' I = 16843543


.field public static final 'windowEnterAnimation' I = 16842932


.field public static final 'windowExitAnimation' I = 16842933


.field public static final 'windowFrame' I = 16842837


.field public static final 'windowFullscreen' I = 16843277


.field public static final 'windowHideAnimation' I = 16842935


.field public static final 'windowIsFloating' I = 16842839


.field public static final 'windowIsTranslucent' I = 16842840


.field public static final 'windowMinWidthMajor' I = 16843606


.field public static final 'windowMinWidthMinor' I = 16843607


.field public static final 'windowNoDisplay' I = 16843294


.field public static final 'windowNoTitle' I = 16842838


.field public static final 'windowShowAnimation' I = 16842934


.field public static final 'windowShowWallpaper' I = 16843410


.field public static final 'windowSoftInputMode' I = 16843307


.field public static final 'windowTitleBackgroundStyle' I = 16842844


.field public static final 'windowTitleSize' I = 16842842


.field public static final 'windowTitleStyle' I = 16842843


.field public static final 'writePermission' I = 16842760


.field public static final 'x' I = 16842924


.field public static final 'xlargeScreens' I = 16843455


.field public static final 'y' I = 16842925


.field public static final 'yesNoPreferenceStyle' I = 16842896


.field public static final 'zAdjustment' I = 16843201


.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method
