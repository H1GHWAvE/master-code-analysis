.bytecode 50.0
.class public synchronized pxb/android/axml/Util
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V
.throws java/io/IOException
sipush 10240
newarray byte
astore 3
aload 0
aload 3
invokevirtual java/io/InputStream/read([B)I
istore 2
L0:
iload 2
ifle L1
aload 1
aload 3
iconst_0
iload 2
invokevirtual java/io/OutputStream/write([BII)V
aload 0
aload 3
invokevirtual java/io/InputStream/read([B)I
istore 2
goto L0
L1:
return
.limit locals 4
.limit stack 4
.end method

.method public static readFile(Ljava/io/File;)[B
.throws java/io/IOException
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 0
aload 0
invokevirtual java/io/InputStream/available()I
newarray byte
astore 1
aload 0
aload 1
invokevirtual java/io/InputStream/read([B)I
pop
aload 0
invokevirtual java/io/InputStream/close()V
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public static readIs(Ljava/io/InputStream;)[B
.throws java/io/IOException
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 1
aload 0
aload 1
invokestatic pxb/android/axml/Util/copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V
aload 1
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
areturn
.limit locals 2
.limit stack 2
.end method

.method public static readProguardConfig(Ljava/io/File;)Ljava/util/Map;
.signature "(Ljava/io/File;)Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
.throws java/io/IOException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 2
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
ldc "utf8"
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;Ljava/lang/String;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 3
L0:
aload 3
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 0
L1:
aload 0
ifnull L10
L3:
aload 0
ldc "#"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L4
aload 0
ldc " "
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L6
L4:
aload 3
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 0
L5:
goto L1
L6:
aload 0
ldc "->"
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
istore 1
L7:
iload 1
ifle L4
L8:
aload 2
aload 0
iconst_0
iload 1
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
aload 0
iload 1
iconst_2
iadd
aload 0
invokevirtual java/lang/String/length()I
iconst_1
isub
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L9:
goto L4
L2:
astore 0
aload 3
invokevirtual java/io/BufferedReader/close()V
aload 0
athrow
L10:
aload 3
invokevirtual java/io/BufferedReader/close()V
aload 2
areturn
.limit locals 4
.limit stack 7
.end method

.method public static writeFile([BLjava/io/File;)V
.throws java/io/IOException
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 1
aload 1
aload 0
invokevirtual java/io/FileOutputStream/write([B)V
aload 1
invokevirtual java/io/FileOutputStream/close()V
return
.limit locals 2
.limit stack 3
.end method
