.bytecode 50.0
.class public synchronized pxb/android/axml/Axml$Node
.super pxb/android/axml/NodeVisitor
.inner class public static Node inner pxb/android/axml/Axml$Node outer pxb/android/axml/Axml
.inner class public static Attr inner pxb/android/axml/Axml$Node$Attr outer pxb/android/axml/Axml$Node
.inner class public static Text inner pxb/android/axml/Axml$Node$Text outer pxb/android/axml/Axml$Node

.field public 'attrs' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/axml/Axml$Node$Attr;>;"

.field public 'children' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/axml/Axml$Node;>;"

.field public 'ln' Ljava/lang/Integer;

.field public 'name' Ljava/lang/String;

.field public 'ns' Ljava/lang/String;

.field public 'text' Lpxb/android/axml/Axml$Node$Text;

.method public <init>()V
aload 0
invokespecial pxb/android/axml/NodeVisitor/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/axml/Axml$Node/attrs Ljava/util/List;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/axml/Axml$Node/children Ljava/util/List;
return
.limit locals 1
.limit stack 3
.end method

.method public accept(Lpxb/android/axml/NodeVisitor;)V
aload 1
aload 0
getfield pxb/android/axml/Axml$Node/ns Ljava/lang/String;
aload 0
getfield pxb/android/axml/Axml$Node/name Ljava/lang/String;
invokevirtual pxb/android/axml/NodeVisitor/child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
astore 1
aload 0
aload 1
invokevirtual pxb/android/axml/Axml$Node/acceptB(Lpxb/android/axml/NodeVisitor;)V
aload 1
invokevirtual pxb/android/axml/NodeVisitor/end()V
return
.limit locals 2
.limit stack 3
.end method

.method public acceptB(Lpxb/android/axml/NodeVisitor;)V
aload 0
getfield pxb/android/axml/Axml$Node/text Lpxb/android/axml/Axml$Node$Text;
ifnull L0
aload 0
getfield pxb/android/axml/Axml$Node/text Lpxb/android/axml/Axml$Node$Text;
aload 1
invokevirtual pxb/android/axml/Axml$Node$Text/accept(Lpxb/android/axml/NodeVisitor;)V
L0:
aload 0
getfield pxb/android/axml/Axml$Node/attrs Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L1:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axml/Axml$Node$Attr
aload 1
invokevirtual pxb/android/axml/Axml$Node$Attr/accept(Lpxb/android/axml/NodeVisitor;)V
goto L1
L2:
aload 0
getfield pxb/android/axml/Axml$Node/ln Ljava/lang/Integer;
ifnull L3
aload 1
aload 0
getfield pxb/android/axml/Axml$Node/ln Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
invokevirtual pxb/android/axml/NodeVisitor/line(I)V
L3:
aload 0
getfield pxb/android/axml/Axml$Node/children Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L4:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axml/Axml$Node
aload 1
invokevirtual pxb/android/axml/Axml$Node/accept(Lpxb/android/axml/NodeVisitor;)V
goto L4
L5:
return
.limit locals 3
.limit stack 2
.end method

.method public attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
new pxb/android/axml/Axml$Node$Attr
dup
invokespecial pxb/android/axml/Axml$Node$Attr/<init>()V
astore 6
aload 6
aload 2
putfield pxb/android/axml/Axml$Node$Attr/name Ljava/lang/String;
aload 6
aload 1
putfield pxb/android/axml/Axml$Node$Attr/ns Ljava/lang/String;
aload 6
iload 3
putfield pxb/android/axml/Axml$Node$Attr/resourceId I
aload 6
iload 4
putfield pxb/android/axml/Axml$Node$Attr/type I
aload 6
aload 5
putfield pxb/android/axml/Axml$Node$Attr/value Ljava/lang/Object;
aload 0
getfield pxb/android/axml/Axml$Node/attrs Ljava/util/List;
aload 6
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 7
.limit stack 2
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
new pxb/android/axml/Axml$Node
dup
invokespecial pxb/android/axml/Axml$Node/<init>()V
astore 3
aload 3
aload 2
putfield pxb/android/axml/Axml$Node/name Ljava/lang/String;
aload 3
aload 1
putfield pxb/android/axml/Axml$Node/ns Ljava/lang/String;
aload 0
getfield pxb/android/axml/Axml$Node/children Ljava/util/List;
aload 3
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 3
areturn
.limit locals 4
.limit stack 2
.end method

.method public line(I)V
aload 0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
putfield pxb/android/axml/Axml$Node/ln Ljava/lang/Integer;
return
.limit locals 2
.limit stack 2
.end method

.method public text(ILjava/lang/String;)V
new pxb/android/axml/Axml$Node$Text
dup
invokespecial pxb/android/axml/Axml$Node$Text/<init>()V
astore 3
aload 3
iload 1
putfield pxb/android/axml/Axml$Node$Text/ln I
aload 3
aload 2
putfield pxb/android/axml/Axml$Node$Text/text Ljava/lang/String;
aload 0
aload 3
putfield pxb/android/axml/Axml$Node/text Lpxb/android/axml/Axml$Node$Text;
return
.limit locals 4
.limit stack 2
.end method
