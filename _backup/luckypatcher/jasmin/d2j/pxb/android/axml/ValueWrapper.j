.bytecode 50.0
.class public synchronized pxb/android/axml/ValueWrapper
.super java/lang/Object

.field public static final 'CLASS' I = 3


.field public static final 'ID' I = 1


.field public static final 'STYLE' I = 2


.field public final 'raw' Ljava/lang/String;

.field public final 'ref' I

.field public final 'type' I

.method private <init>(IILjava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield pxb/android/axml/ValueWrapper/type I
aload 0
aload 3
putfield pxb/android/axml/ValueWrapper/raw Ljava/lang/String;
aload 0
iload 2
putfield pxb/android/axml/ValueWrapper/ref I
return
.limit locals 4
.limit stack 2
.end method

.method public static wrapClass(ILjava/lang/String;)Lpxb/android/axml/ValueWrapper;
new pxb/android/axml/ValueWrapper
dup
iconst_3
iload 0
aload 1
invokespecial pxb/android/axml/ValueWrapper/<init>(IILjava/lang/String;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method public static wrapId(ILjava/lang/String;)Lpxb/android/axml/ValueWrapper;
new pxb/android/axml/ValueWrapper
dup
iconst_1
iload 0
aload 1
invokespecial pxb/android/axml/ValueWrapper/<init>(IILjava/lang/String;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method public static wrapStyle(ILjava/lang/String;)Lpxb/android/axml/ValueWrapper;
new pxb/android/axml/ValueWrapper
dup
iconst_2
iload 0
aload 1
invokespecial pxb/android/axml/ValueWrapper/<init>(IILjava/lang/String;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method public replaceRaw(Ljava/lang/String;)Lpxb/android/axml/ValueWrapper;
new pxb/android/axml/ValueWrapper
dup
aload 0
getfield pxb/android/axml/ValueWrapper/type I
aload 0
getfield pxb/android/axml/ValueWrapper/ref I
aload 1
invokespecial pxb/android/axml/ValueWrapper/<init>(IILjava/lang/String;)V
areturn
.limit locals 2
.limit stack 5
.end method
