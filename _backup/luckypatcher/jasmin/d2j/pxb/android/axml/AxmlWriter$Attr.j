.bytecode 50.0
.class synchronized pxb/android/axml/AxmlWriter$Attr
.super java/lang/Object
.inner class static Attr inner pxb/android/axml/AxmlWriter$Attr outer pxb/android/axml/AxmlWriter

.field public 'index' I

.field public 'name' Lpxb/android/StringItem;

.field public 'ns' Lpxb/android/StringItem;

.field public 'raw' Lpxb/android/StringItem;

.field public 'resourceId' I

.field public 'type' I

.field public 'value' Ljava/lang/Object;

.method public <init>(Lpxb/android/StringItem;Lpxb/android/StringItem;I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield pxb/android/axml/AxmlWriter$Attr/ns Lpxb/android/StringItem;
aload 0
aload 2
putfield pxb/android/axml/AxmlWriter$Attr/name Lpxb/android/StringItem;
aload 0
iload 3
putfield pxb/android/axml/AxmlWriter$Attr/resourceId I
return
.limit locals 4
.limit stack 2
.end method

.method public prepare(Lpxb/android/axml/AxmlWriter;)V
aload 0
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$Attr/ns Lpxb/android/StringItem;
invokevirtual pxb/android/axml/AxmlWriter/updateNs(Lpxb/android/StringItem;)Lpxb/android/StringItem;
putfield pxb/android/axml/AxmlWriter$Attr/ns Lpxb/android/StringItem;
aload 0
getfield pxb/android/axml/AxmlWriter$Attr/name Lpxb/android/StringItem;
ifnull L0
aload 0
getfield pxb/android/axml/AxmlWriter$Attr/resourceId I
iconst_m1
if_icmpeq L1
aload 0
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$Attr/name Lpxb/android/StringItem;
aload 0
getfield pxb/android/axml/AxmlWriter$Attr/resourceId I
invokevirtual pxb/android/axml/AxmlWriter/updateWithResourceId(Lpxb/android/StringItem;I)Lpxb/android/StringItem;
putfield pxb/android/axml/AxmlWriter$Attr/name Lpxb/android/StringItem;
L0:
aload 0
getfield pxb/android/axml/AxmlWriter$Attr/value Ljava/lang/Object;
instanceof pxb/android/StringItem
ifeq L2
aload 0
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$Attr/value Ljava/lang/Object;
checkcast pxb/android/StringItem
invokevirtual pxb/android/axml/AxmlWriter/update(Lpxb/android/StringItem;)Lpxb/android/StringItem;
putfield pxb/android/axml/AxmlWriter$Attr/value Ljava/lang/Object;
L2:
aload 0
getfield pxb/android/axml/AxmlWriter$Attr/raw Lpxb/android/StringItem;
ifnull L3
aload 0
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$Attr/raw Lpxb/android/StringItem;
invokevirtual pxb/android/axml/AxmlWriter/update(Lpxb/android/StringItem;)Lpxb/android/StringItem;
putfield pxb/android/axml/AxmlWriter$Attr/raw Lpxb/android/StringItem;
L3:
return
L1:
aload 0
aload 1
aload 0
getfield pxb/android/axml/AxmlWriter$Attr/name Lpxb/android/StringItem;
invokevirtual pxb/android/axml/AxmlWriter/update(Lpxb/android/StringItem;)Lpxb/android/StringItem;
putfield pxb/android/axml/AxmlWriter$Attr/name Lpxb/android/StringItem;
goto L0
.limit locals 2
.limit stack 4
.end method
