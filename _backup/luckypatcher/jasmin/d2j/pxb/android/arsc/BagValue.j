.bytecode 50.0
.class public synchronized pxb/android/arsc/BagValue
.super java/lang/Object

.field public 'map' Ljava/util/List; signature "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/Integer;Lpxb/android/arsc/Value;>;>;"

.field public final 'parent' I

.method public <init>(I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/arsc/BagValue/map Ljava/util/List;
aload 0
iload 1
putfield pxb/android/arsc/BagValue/parent I
return
.limit locals 2
.limit stack 3
.end method

.method public equals(Ljava/lang/Object;)Z
aload 0
aload 1
if_acmpne L0
L1:
iconst_1
ireturn
L0:
aload 1
ifnonnull L2
iconst_0
ireturn
L2:
aload 1
instanceof pxb/android/arsc/BagValue
ifne L3
iconst_0
ireturn
L3:
aload 1
checkcast pxb/android/arsc/BagValue
astore 1
aload 0
getfield pxb/android/arsc/BagValue/map Ljava/util/List;
ifnonnull L4
aload 1
getfield pxb/android/arsc/BagValue/map Ljava/util/List;
ifnull L5
iconst_0
ireturn
L4:
aload 0
getfield pxb/android/arsc/BagValue/map Ljava/util/List;
aload 1
getfield pxb/android/arsc/BagValue/map Ljava/util/List;
invokeinterface java/util/List/equals(Ljava/lang/Object;)Z 1
ifne L5
iconst_0
ireturn
L5:
aload 0
getfield pxb/android/arsc/BagValue/parent I
aload 1
getfield pxb/android/arsc/BagValue/parent I
if_icmpeq L1
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public hashCode()I
aload 0
getfield pxb/android/arsc/BagValue/map Ljava/util/List;
ifnonnull L0
iconst_0
istore 1
L1:
iload 1
bipush 31
iadd
bipush 31
imul
aload 0
getfield pxb/android/arsc/BagValue/parent I
iadd
ireturn
L0:
aload 0
getfield pxb/android/arsc/BagValue/map Ljava/util/List;
invokeinterface java/util/List/hashCode()I 0
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 1
aload 1
ldc "{bag%08x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield pxb/android/arsc/BagValue/parent I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
getfield pxb/android/arsc/BagValue/map Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 1
ldc ","
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "0x%08x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
ldc "="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
goto L0
L1:
aload 1
ldc "}"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 6
.end method
