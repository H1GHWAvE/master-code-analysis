.bytecode 50.0
.class public synchronized pxb/android/arsc/Type
.super java/lang/Object

.field public 'configs' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/arsc/Config;>;"

.field public 'id' I

.field public 'name' Ljava/lang/String;

.field public 'specs' [Lpxb/android/arsc/ResSpec;

.field 'wPosition' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/arsc/Type/configs Ljava/util/List;
return
.limit locals 1
.limit stack 3
.end method

.method public addConfig(Lpxb/android/arsc/Config;)V
aload 1
getfield pxb/android/arsc/Config/entryCount I
aload 0
getfield pxb/android/arsc/Type/specs [Lpxb/android/arsc/ResSpec;
arraylength
if_icmpeq L0
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L0:
aload 0
getfield pxb/android/arsc/Type/configs Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 2
.end method

.method public getSpec(I)Lpxb/android/arsc/ResSpec;
aload 0
getfield pxb/android/arsc/Type/specs [Lpxb/android/arsc/ResSpec;
iload 1
aaload
astore 3
aload 3
astore 2
aload 3
ifnonnull L0
new pxb/android/arsc/ResSpec
dup
iload 1
invokespecial pxb/android/arsc/ResSpec/<init>(I)V
astore 2
aload 0
getfield pxb/android/arsc/Type/specs [Lpxb/android/arsc/ResSpec;
iload 1
aload 2
aastore
L0:
aload 2
areturn
.limit locals 4
.limit stack 3
.end method
