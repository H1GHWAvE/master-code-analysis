.bytecode 50.0
.class synchronized pxb/android/arsc/ArscWriter$PkgCtx
.super java/lang/Object
.inner class private static PkgCtx inner pxb/android/arsc/ArscWriter$PkgCtx outer pxb/android/arsc/ArscWriter

.field 'keyNames' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Lpxb/android/StringItem;>;"

.field 'keyNames0' Lpxb/android/StringItems;

.field public 'keyStringOff' I

.field 'offset' I

.field 'pkg' Lpxb/android/arsc/Pkg;

.field 'pkgSize' I

.field 'typeNames' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/StringItem;>;"

.field 'typeNames0' Lpxb/android/StringItems;

.field 'typeStringOff' I

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield pxb/android/arsc/ArscWriter$PkgCtx/keyNames Ljava/util/Map;
aload 0
new pxb/android/StringItems
dup
invokespecial pxb/android/StringItems/<init>()V
putfield pxb/android/arsc/ArscWriter$PkgCtx/keyNames0 Lpxb/android/StringItems;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/arsc/ArscWriter$PkgCtx/typeNames Ljava/util/List;
aload 0
new pxb/android/StringItems
dup
invokespecial pxb/android/StringItems/<init>()V
putfield pxb/android/arsc/ArscWriter$PkgCtx/typeNames0 Lpxb/android/StringItems;
return
.limit locals 1
.limit stack 3
.end method

.method synthetic <init>(Lpxb/android/arsc/ArscWriter$1;)V
aload 0
invokespecial pxb/android/arsc/ArscWriter$PkgCtx/<init>()V
return
.limit locals 2
.limit stack 1
.end method

.method public addKeyName(Ljava/lang/String;)V
aload 0
getfield pxb/android/arsc/ArscWriter$PkgCtx/keyNames Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L0
return
L0:
new pxb/android/StringItem
dup
aload 1
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
astore 2
aload 0
getfield pxb/android/arsc/ArscWriter$PkgCtx/keyNames Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
getfield pxb/android/arsc/ArscWriter$PkgCtx/keyNames0 Lpxb/android/StringItems;
aload 2
invokevirtual pxb/android/StringItems/add(Ljava/lang/Object;)Z
pop
return
.limit locals 3
.limit stack 3
.end method

.method public addTypeName(ILjava/lang/String;)V
L0:
aload 0
getfield pxb/android/arsc/ArscWriter$PkgCtx/typeNames Ljava/util/List;
invokeinterface java/util/List/size()I 0
iload 1
if_icmpgt L1
aload 0
getfield pxb/android/arsc/ArscWriter$PkgCtx/typeNames Ljava/util/List;
aconst_null
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L0
L1:
aload 0
getfield pxb/android/arsc/ArscWriter$PkgCtx/typeNames Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast pxb/android/StringItem
ifnonnull L2
aload 0
getfield pxb/android/arsc/ArscWriter$PkgCtx/typeNames Ljava/util/List;
iload 1
new pxb/android/StringItem
dup
aload 2
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
invokeinterface java/util/List/set(ILjava/lang/Object;)Ljava/lang/Object; 2
pop
return
L2:
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
.limit locals 3
.limit stack 5
.end method
