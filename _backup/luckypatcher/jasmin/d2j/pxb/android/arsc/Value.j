.bytecode 50.0
.class public synchronized pxb/android/arsc/Value
.super java/lang/Object

.field public final 'data' I

.field public 'raw' Ljava/lang/String;

.field public final 'type' I

.method public <init>(IILjava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield pxb/android/arsc/Value/type I
aload 0
iload 2
putfield pxb/android/arsc/Value/data I
aload 0
aload 3
putfield pxb/android/arsc/Value/raw Ljava/lang/String;
return
.limit locals 4
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield pxb/android/arsc/Value/type I
iconst_3
if_icmpne L0
aload 0
getfield pxb/android/arsc/Value/raw Ljava/lang/String;
areturn
L0:
ldc "{t=0x%02x d=0x%08x}"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield pxb/android/arsc/Value/type I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 0
getfield pxb/android/arsc/Value/data I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 5
.end method
