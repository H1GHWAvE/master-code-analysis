.bytecode 50.0
.class public synchronized pxb/android/arsc/ArscDumper
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static dump(Ljava/util/List;)V
.signature "(Ljava/util/List<Lpxb/android/arsc/Pkg;>;)V"
iconst_0
istore 1
L0:
iload 1
aload 0
invokeinterface java/util/List/size()I 0
if_icmpge L1
aload 0
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast pxb/android/arsc/Pkg
astore 5
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "  Package %d id=%d name=%s typeCount=%d"
iconst_4
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 5
getfield pxb/android/arsc/Pkg/id I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
aload 5
getfield pxb/android/arsc/Pkg/name Ljava/lang/String;
aastore
dup
iconst_3
aload 5
getfield pxb/android/arsc/Pkg/types Ljava/util/TreeMap;
invokevirtual java/util/TreeMap/size()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 5
getfield pxb/android/arsc/Pkg/types Ljava/util/TreeMap;
invokevirtual java/util/TreeMap/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 6
L2:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/arsc/Type
astore 7
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "    type %d %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 7
getfield pxb/android/arsc/Type/id I
iconst_1
isub
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 7
getfield pxb/android/arsc/Type/name Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 5
getfield pxb/android/arsc/Pkg/id I
bipush 24
ishl
aload 7
getfield pxb/android/arsc/Type/id I
bipush 16
ishl
ior
istore 4
iconst_0
istore 2
L4:
iload 2
aload 7
getfield pxb/android/arsc/Type/specs [Lpxb/android/arsc/ResSpec;
arraylength
if_icmpge L5
aload 7
iload 2
invokevirtual pxb/android/arsc/Type/getSpec(I)Lpxb/android/arsc/ResSpec;
astore 8
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "      spec 0x%08x 0x%08x %s"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 8
getfield pxb/android/arsc/ResSpec/id I
iload 4
ior
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 8
getfield pxb/android/arsc/ResSpec/flags I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
aload 8
getfield pxb/android/arsc/ResSpec/name Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iload 2
iconst_1
iadd
istore 2
goto L4
L5:
iconst_0
istore 2
L6:
iload 2
aload 7
getfield pxb/android/arsc/Type/configs Ljava/util/List;
invokeinterface java/util/List/size()I 0
if_icmpge L2
aload 7
getfield pxb/android/arsc/Type/configs Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast pxb/android/arsc/Config
astore 8
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "      config"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/util/ArrayList
dup
aload 8
getfield pxb/android/arsc/Config/resources Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
astore 8
iconst_0
istore 3
L7:
iload 3
aload 8
invokeinterface java/util/List/size()I 0
if_icmpge L8
aload 8
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast pxb/android/arsc/ResEntry
astore 9
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "        resource 0x%08x %-20s: %s"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 9
getfield pxb/android/arsc/ResEntry/spec Lpxb/android/arsc/ResSpec;
getfield pxb/android/arsc/ResSpec/id I
iload 4
ior
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 9
getfield pxb/android/arsc/ResEntry/spec Lpxb/android/arsc/ResSpec;
getfield pxb/android/arsc/ResSpec/name Ljava/lang/String;
aastore
dup
iconst_2
aload 9
getfield pxb/android/arsc/ResEntry/value Ljava/lang/Object;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iload 3
iconst_1
iadd
istore 3
goto L7
L8:
iload 2
iconst_1
iadd
istore 2
goto L6
L3:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
return
.limit locals 10
.limit stack 7
.end method

.method public static transient main([Ljava/lang/String;)V
.throws java/io/IOException
aload 0
arraylength
ifne L0
getstatic java/lang/System/err Ljava/io/PrintStream;
ldc "asrc-dump file.arsc"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
L0:
new pxb/android/arsc/ArscParser
dup
new java/io/File
dup
aload 0
iconst_0
aaload
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic pxb/android/axml/Util/readFile(Ljava/io/File;)[B
invokespecial pxb/android/arsc/ArscParser/<init>([B)V
invokevirtual pxb/android/arsc/ArscParser/parse()Ljava/util/List;
invokestatic pxb/android/arsc/ArscDumper/dump(Ljava/util/List;)V
return
.limit locals 1
.limit stack 6
.end method
