.bytecode 50.0
.class public synchronized pxb/android/arsc/ArscWriter
.super java/lang/Object
.implements pxb/android/ResConst
.inner class static synthetic inner pxb/android/arsc/ArscWriter$1
.inner class private static PkgCtx inner pxb/android/arsc/ArscWriter$PkgCtx outer pxb/android/arsc/ArscWriter

.field private 'ctxs' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/arsc/ArscWriter$PkgCtx;>;"

.field private 'pkgs' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/arsc/Pkg;>;"

.field private 'strTable' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Lpxb/android/StringItem;>;"

.field private 'strTable0' Lpxb/android/StringItems;

.method public <init>(Ljava/util/List;)V
.signature "(Ljava/util/List<Lpxb/android/arsc/Pkg;>;)V"
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
iconst_5
invokespecial java/util/ArrayList/<init>(I)V
putfield pxb/android/arsc/ArscWriter/ctxs Ljava/util/List;
aload 0
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
putfield pxb/android/arsc/ArscWriter/strTable Ljava/util/Map;
aload 0
new pxb/android/StringItems
dup
invokespecial pxb/android/StringItems/<init>()V
putfield pxb/android/arsc/ArscWriter/strTable0 Lpxb/android/StringItems;
aload 0
aload 1
putfield pxb/android/arsc/ArscWriter/pkgs Ljava/util/List;
return
.limit locals 2
.limit stack 4
.end method

.method private static transient D(Ljava/lang/String;[Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 0
.end method

.method private addString(Ljava/lang/String;)V
aload 0
getfield pxb/android/arsc/ArscWriter/strTable Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L0
return
L0:
new pxb/android/StringItem
dup
aload 1
invokespecial pxb/android/StringItem/<init>(Ljava/lang/String;)V
astore 2
aload 0
getfield pxb/android/arsc/ArscWriter/strTable Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
getfield pxb/android/arsc/ArscWriter/strTable0 Lpxb/android/StringItems;
aload 2
invokevirtual pxb/android/StringItems/add(Ljava/lang/Object;)Z
pop
return
.limit locals 3
.limit stack 3
.end method

.method private count()I
aload 0
getfield pxb/android/arsc/ArscWriter/strTable0 Lpxb/android/StringItems;
invokevirtual pxb/android/StringItems/getSize()I
istore 2
iload 2
istore 1
iload 2
iconst_4
irem
ifeq L0
iload 2
iconst_4
iload 2
iconst_4
irem
isub
iadd
istore 1
L0:
iload 1
bipush 8
iadd
bipush 12
iadd
istore 2
aload 0
getfield pxb/android/arsc/ArscWriter/ctxs Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 5
L1:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/arsc/ArscWriter$PkgCtx
astore 6
aload 6
iload 2
putfield pxb/android/arsc/ArscWriter$PkgCtx/offset I
aload 6
iconst_0
sipush 268
iadd
bipush 16
iadd
putfield pxb/android/arsc/ArscWriter$PkgCtx/typeStringOff I
aload 6
getfield pxb/android/arsc/ArscWriter$PkgCtx/typeNames0 Lpxb/android/StringItems;
invokevirtual pxb/android/StringItems/getSize()I
istore 3
iload 3
istore 1
iload 3
iconst_4
irem
ifeq L3
iload 3
iconst_4
iload 3
iconst_4
irem
isub
iadd
istore 1
L3:
iload 1
bipush 8
iadd
sipush 284
iadd
istore 4
aload 6
iload 4
putfield pxb/android/arsc/ArscWriter$PkgCtx/keyStringOff I
aload 6
getfield pxb/android/arsc/ArscWriter$PkgCtx/keyNames0 Lpxb/android/StringItems;
invokevirtual pxb/android/StringItems/getSize()I
istore 3
iload 3
istore 1
iload 3
iconst_4
irem
ifeq L4
iload 3
iconst_4
iload 3
iconst_4
irem
isub
iadd
istore 1
L4:
iload 4
iload 1
bipush 8
iadd
iadd
istore 1
aload 6
getfield pxb/android/arsc/ArscWriter$PkgCtx/pkg Lpxb/android/arsc/Pkg;
getfield pxb/android/arsc/Pkg/types Ljava/util/TreeMap;
invokevirtual java/util/TreeMap/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 7
L5:
aload 7
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L6
aload 7
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/arsc/Type
astore 8
aload 8
iload 2
iload 1
iadd
putfield pxb/android/arsc/Type/wPosition I
iload 1
aload 8
getfield pxb/android/arsc/Type/specs [Lpxb/android/arsc/ResSpec;
arraylength
iconst_4
imul
bipush 16
iadd
iadd
istore 1
aload 8
getfield pxb/android/arsc/Type/configs Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 8
L7:
iload 1
istore 3
iload 3
istore 1
aload 8
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 8
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/arsc/Config
astore 9
aload 9
iload 3
iload 2
iadd
putfield pxb/android/arsc/Config/wPosition I
aload 9
getfield pxb/android/arsc/Config/id [B
arraylength
istore 4
iload 4
istore 1
iload 4
iconst_4
irem
ifeq L8
iload 4
iconst_4
iload 4
iconst_4
irem
isub
iadd
istore 1
L8:
iload 3
bipush 20
iadd
iload 1
iadd
iload 3
isub
bipush 56
if_icmple L9
new java/lang/RuntimeException
dup
ldc "config id  too big"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L9:
iload 3
bipush 56
iadd
aload 9
getfield pxb/android/arsc/Config/entryCount I
iconst_4
imul
iadd
istore 4
aload 9
iload 4
iload 3
isub
putfield pxb/android/arsc/Config/wEntryStart I
aload 9
getfield pxb/android/arsc/Config/resources Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 10
iload 4
istore 1
L10:
aload 10
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L11
aload 10
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/arsc/ResEntry
astore 11
aload 11
iload 1
iload 4
isub
putfield pxb/android/arsc/ResEntry/wOffset I
iload 1
bipush 8
iadd
istore 1
aload 11
getfield pxb/android/arsc/ResEntry/value Ljava/lang/Object;
instanceof pxb/android/arsc/BagValue
ifeq L12
iload 1
aload 11
getfield pxb/android/arsc/ResEntry/value Ljava/lang/Object;
checkcast pxb/android/arsc/BagValue
getfield pxb/android/arsc/BagValue/map Ljava/util/List;
invokeinterface java/util/List/size()I 0
bipush 12
imul
bipush 8
iadd
iadd
istore 1
goto L10
L12:
iload 1
bipush 8
iadd
istore 1
goto L10
L11:
aload 9
iload 1
iload 3
isub
putfield pxb/android/arsc/Config/wChunkSize I
goto L7
L6:
aload 6
iload 1
putfield pxb/android/arsc/ArscWriter$PkgCtx/pkgSize I
iload 2
iload 1
iadd
istore 2
goto L1
L2:
iload 2
ireturn
.limit locals 12
.limit stack 4
.end method

.method public static transient main([Ljava/lang/String;)V
.throws java/io/IOException
aload 0
arraylength
iconst_2
if_icmpge L0
getstatic java/lang/System/err Ljava/io/PrintStream;
ldc "asrc-write-test in.arsc out.arsc"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
L0:
new pxb/android/arsc/ArscWriter
dup
new pxb/android/arsc/ArscParser
dup
new java/io/File
dup
aload 0
iconst_0
aaload
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic pxb/android/axml/Util/readFile(Ljava/io/File;)[B
invokespecial pxb/android/arsc/ArscParser/<init>([B)V
invokevirtual pxb/android/arsc/ArscParser/parse()Ljava/util/List;
invokespecial pxb/android/arsc/ArscWriter/<init>(Ljava/util/List;)V
invokevirtual pxb/android/arsc/ArscWriter/toByteArray()[B
new java/io/File
dup
aload 0
iconst_1
aaload
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic pxb/android/axml/Util/writeFile([BLjava/io/File;)V
return
.limit locals 1
.limit stack 8
.end method

.method private prepare()Ljava/util/List;
.signature "()Ljava/util/List<Lpxb/android/arsc/ArscWriter$PkgCtx;>;"
.throws java/io/IOException
aload 0
getfield pxb/android/arsc/ArscWriter/pkgs Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/arsc/Pkg
astore 5
new pxb/android/arsc/ArscWriter$PkgCtx
dup
aconst_null
invokespecial pxb/android/arsc/ArscWriter$PkgCtx/<init>(Lpxb/android/arsc/ArscWriter$1;)V
astore 4
aload 4
aload 5
putfield pxb/android/arsc/ArscWriter$PkgCtx/pkg Lpxb/android/arsc/Pkg;
aload 0
getfield pxb/android/arsc/ArscWriter/ctxs Ljava/util/List;
aload 4
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 5
getfield pxb/android/arsc/Pkg/types Ljava/util/TreeMap;
invokevirtual java/util/TreeMap/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 5
L2:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/arsc/Type
astore 6
aload 4
aload 6
getfield pxb/android/arsc/Type/id I
iconst_1
isub
aload 6
getfield pxb/android/arsc/Type/name Ljava/lang/String;
invokevirtual pxb/android/arsc/ArscWriter$PkgCtx/addTypeName(ILjava/lang/String;)V
aload 6
getfield pxb/android/arsc/Type/specs [Lpxb/android/arsc/ResSpec;
astore 7
aload 7
arraylength
istore 2
iconst_0
istore 1
L4:
iload 1
iload 2
if_icmpge L5
aload 4
aload 7
iload 1
aaload
getfield pxb/android/arsc/ResSpec/name Ljava/lang/String;
invokevirtual pxb/android/arsc/ArscWriter$PkgCtx/addKeyName(Ljava/lang/String;)V
iload 1
iconst_1
iadd
istore 1
goto L4
L5:
aload 6
getfield pxb/android/arsc/Type/configs Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 6
L6:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/arsc/Config
getfield pxb/android/arsc/Config/resources Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 7
L7:
aload 7
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L6
aload 7
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/arsc/ResEntry
getfield pxb/android/arsc/ResEntry/value Ljava/lang/Object;
astore 8
aload 8
instanceof pxb/android/arsc/BagValue
ifeq L8
aload 0
aload 8
checkcast pxb/android/arsc/BagValue
invokespecial pxb/android/arsc/ArscWriter/travelBagValue(Lpxb/android/arsc/BagValue;)V
goto L7
L8:
aload 0
aload 8
checkcast pxb/android/arsc/Value
invokespecial pxb/android/arsc/ArscWriter/travelValue(Lpxb/android/arsc/Value;)V
goto L7
L3:
aload 4
getfield pxb/android/arsc/ArscWriter$PkgCtx/keyNames0 Lpxb/android/StringItems;
invokevirtual pxb/android/StringItems/prepare()V
aload 4
getfield pxb/android/arsc/ArscWriter$PkgCtx/typeNames0 Lpxb/android/StringItems;
aload 4
getfield pxb/android/arsc/ArscWriter$PkgCtx/typeNames Ljava/util/List;
invokevirtual pxb/android/StringItems/addAll(Ljava/util/Collection;)Z
pop
aload 4
getfield pxb/android/arsc/ArscWriter$PkgCtx/typeNames0 Lpxb/android/StringItems;
invokevirtual pxb/android/StringItems/prepare()V
goto L0
L1:
aload 0
getfield pxb/android/arsc/ArscWriter/strTable0 Lpxb/android/StringItems;
invokevirtual pxb/android/StringItems/prepare()V
aload 0
getfield pxb/android/arsc/ArscWriter/ctxs Ljava/util/List;
areturn
.limit locals 9
.limit stack 3
.end method

.method private travelBagValue(Lpxb/android/arsc/BagValue;)V
aload 1
getfield pxb/android/arsc/BagValue/map Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast pxb/android/arsc/Value
invokespecial pxb/android/arsc/ArscWriter/travelValue(Lpxb/android/arsc/Value;)V
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method private travelValue(Lpxb/android/arsc/Value;)V
aload 1
getfield pxb/android/arsc/Value/raw Ljava/lang/String;
ifnull L0
aload 0
aload 1
getfield pxb/android/arsc/Value/raw Ljava/lang/String;
invokespecial pxb/android/arsc/ArscWriter/addString(Ljava/lang/String;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private write(Ljava/nio/ByteBuffer;I)V
.throws java/io/IOException
aload 1
ldc_w 786434
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
iload 2
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 0
getfield pxb/android/arsc/ArscWriter/ctxs Ljava/util/List;
invokeinterface java/util/List/size()I 0
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 0
getfield pxb/android/arsc/ArscWriter/strTable0 Lpxb/android/StringItems;
invokevirtual pxb/android/StringItems/getSize()I
istore 3
iconst_0
istore 2
iload 3
iconst_4
irem
ifeq L0
iconst_4
iload 3
iconst_4
irem
isub
istore 2
L0:
aload 1
ldc_w 1835009
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
iload 3
iload 2
iadd
bipush 8
iadd
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 0
getfield pxb/android/arsc/ArscWriter/strTable0 Lpxb/android/StringItems;
aload 1
invokevirtual pxb/android/StringItems/write(Ljava/nio/ByteBuffer;)V
aload 1
iload 2
newarray byte
invokevirtual java/nio/ByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 0
getfield pxb/android/arsc/ArscWriter/ctxs Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 6
L1:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/arsc/ArscWriter$PkgCtx
astore 7
aload 1
invokevirtual java/nio/ByteBuffer/position()I
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/offset I
if_icmpeq L3
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L3:
aload 1
invokevirtual java/nio/ByteBuffer/position()I
istore 3
aload 1
ldc_w 18612736
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/pkgSize I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/pkg Lpxb/android/arsc/Pkg;
getfield pxb/android/arsc/Pkg/id I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
invokevirtual java/nio/ByteBuffer/position()I
istore 2
aload 1
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/pkg Lpxb/android/arsc/Pkg;
getfield pxb/android/arsc/Pkg/name Ljava/lang/String;
ldc "UTF-16LE"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
invokevirtual java/nio/ByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 1
iload 2
sipush 256
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 1
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/typeStringOff I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/typeNames0 Lpxb/android/StringItems;
invokevirtual pxb/android/StringItems/size()I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/keyStringOff I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/keyNames0 Lpxb/android/StringItems;
invokevirtual pxb/android/StringItems/size()I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
invokevirtual java/nio/ByteBuffer/position()I
iload 3
isub
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/typeStringOff I
if_icmpeq L4
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L4:
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/typeNames0 Lpxb/android/StringItems;
invokevirtual pxb/android/StringItems/getSize()I
istore 4
iconst_0
istore 2
iload 4
iconst_4
irem
ifeq L5
iconst_4
iload 4
iconst_4
irem
isub
istore 2
L5:
aload 1
ldc_w 1835009
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
iload 4
iload 2
iadd
bipush 8
iadd
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/typeNames0 Lpxb/android/StringItems;
aload 1
invokevirtual pxb/android/StringItems/write(Ljava/nio/ByteBuffer;)V
aload 1
iload 2
newarray byte
invokevirtual java/nio/ByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 1
invokevirtual java/nio/ByteBuffer/position()I
iload 3
isub
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/keyStringOff I
if_icmpeq L6
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L6:
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/keyNames0 Lpxb/android/StringItems;
invokevirtual pxb/android/StringItems/getSize()I
istore 3
iconst_0
istore 2
iload 3
iconst_4
irem
ifeq L7
iconst_4
iload 3
iconst_4
irem
isub
istore 2
L7:
aload 1
ldc_w 1835009
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
iload 3
iload 2
iadd
bipush 8
iadd
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/keyNames0 Lpxb/android/StringItems;
aload 1
invokevirtual pxb/android/StringItems/write(Ljava/nio/ByteBuffer;)V
aload 1
iload 2
newarray byte
invokevirtual java/nio/ByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/pkg Lpxb/android/arsc/Pkg;
getfield pxb/android/arsc/Pkg/types Ljava/util/TreeMap;
invokevirtual java/util/TreeMap/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 8
L8:
aload 8
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 8
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/arsc/Type
astore 9
ldc "[%08x]write spec"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual java/nio/ByteBuffer/position()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 9
getfield pxb/android/arsc/Type/name Ljava/lang/String;
aastore
invokestatic pxb/android/arsc/ArscWriter/D(Ljava/lang/String;[Ljava/lang/Object;)V
aload 9
getfield pxb/android/arsc/Type/wPosition I
aload 1
invokevirtual java/nio/ByteBuffer/position()I
if_icmpeq L9
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L9:
aload 1
ldc_w 1049090
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 9
getfield pxb/android/arsc/Type/specs [Lpxb/android/arsc/ResSpec;
arraylength
iconst_4
imul
bipush 16
iadd
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 9
getfield pxb/android/arsc/Type/id I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 9
getfield pxb/android/arsc/Type/specs [Lpxb/android/arsc/ResSpec;
arraylength
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 9
getfield pxb/android/arsc/Type/specs [Lpxb/android/arsc/ResSpec;
astore 10
aload 10
arraylength
istore 3
iconst_0
istore 2
L10:
iload 2
iload 3
if_icmpge L11
aload 1
aload 10
iload 2
aaload
getfield pxb/android/arsc/ResSpec/flags I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
iload 2
iconst_1
iadd
istore 2
goto L10
L11:
aload 9
getfield pxb/android/arsc/Type/configs Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 10
L12:
aload 10
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L8
aload 10
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/arsc/Config
astore 11
ldc "[%08x]write config"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual java/nio/ByteBuffer/position()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic pxb/android/arsc/ArscWriter/D(Ljava/lang/String;[Ljava/lang/Object;)V
aload 1
invokevirtual java/nio/ByteBuffer/position()I
istore 3
aload 11
getfield pxb/android/arsc/Config/wPosition I
iload 3
if_icmpeq L13
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L13:
aload 1
ldc_w 3670529
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 11
getfield pxb/android/arsc/Config/wChunkSize I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 9
getfield pxb/android/arsc/Type/id I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 9
getfield pxb/android/arsc/Type/specs [Lpxb/android/arsc/ResSpec;
arraylength
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 11
getfield pxb/android/arsc/Config/wEntryStart I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
ldc "[%08x]write config ids"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual java/nio/ByteBuffer/position()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic pxb/android/arsc/ArscWriter/D(Ljava/lang/String;[Ljava/lang/Object;)V
aload 1
aload 11
getfield pxb/android/arsc/Config/id [B
invokevirtual java/nio/ByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 11
getfield pxb/android/arsc/Config/id [B
arraylength
istore 4
iconst_0
istore 2
iload 4
iconst_4
irem
ifeq L14
iconst_4
iload 4
iconst_4
irem
isub
istore 2
L14:
aload 1
iload 2
newarray byte
invokevirtual java/nio/ByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 1
iload 3
bipush 56
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
ldc "[%08x]write config entry offsets"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual java/nio/ByteBuffer/position()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic pxb/android/arsc/ArscWriter/D(Ljava/lang/String;[Ljava/lang/Object;)V
iconst_0
istore 2
L15:
iload 2
aload 11
getfield pxb/android/arsc/Config/entryCount I
if_icmpge L16
aload 11
getfield pxb/android/arsc/Config/resources Ljava/util/Map;
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast pxb/android/arsc/ResEntry
astore 12
aload 12
ifnonnull L17
aload 1
iconst_m1
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
L18:
iload 2
iconst_1
iadd
istore 2
goto L15
L17:
aload 1
aload 12
getfield pxb/android/arsc/ResEntry/wOffset I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
goto L18
L16:
aload 1
invokevirtual java/nio/ByteBuffer/position()I
iload 3
isub
aload 11
getfield pxb/android/arsc/Config/wEntryStart I
if_icmpeq L19
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L19:
ldc "[%08x]write config entrys"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual java/nio/ByteBuffer/position()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic pxb/android/arsc/ArscWriter/D(Ljava/lang/String;[Ljava/lang/Object;)V
aload 11
getfield pxb/android/arsc/Config/resources Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 11
L20:
aload 11
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L12
aload 11
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/arsc/ResEntry
astore 12
ldc "[%08x]ResTable_entry"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual java/nio/ByteBuffer/position()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic pxb/android/arsc/ArscWriter/D(Ljava/lang/String;[Ljava/lang/Object;)V
aload 12
getfield pxb/android/arsc/ResEntry/value Ljava/lang/Object;
instanceof pxb/android/arsc/BagValue
istore 5
iload 5
ifeq L21
bipush 16
istore 2
L22:
aload 1
iload 2
i2s
invokevirtual java/nio/ByteBuffer/putShort(S)Ljava/nio/ByteBuffer;
pop
aload 12
getfield pxb/android/arsc/ResEntry/flag I
istore 2
iload 5
ifeq L23
iload 2
iconst_1
ior
istore 2
L24:
aload 1
iload 2
i2s
invokevirtual java/nio/ByteBuffer/putShort(S)Ljava/nio/ByteBuffer;
pop
aload 1
aload 7
getfield pxb/android/arsc/ArscWriter$PkgCtx/keyNames Ljava/util/Map;
aload 12
getfield pxb/android/arsc/ResEntry/spec Lpxb/android/arsc/ResSpec;
getfield pxb/android/arsc/ResSpec/name Ljava/lang/String;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast pxb/android/StringItem
getfield pxb/android/StringItem/index I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
iload 5
ifeq L25
aload 12
getfield pxb/android/arsc/ResEntry/value Ljava/lang/Object;
checkcast pxb/android/arsc/BagValue
astore 12
aload 1
aload 12
getfield pxb/android/arsc/BagValue/parent I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 12
getfield pxb/android/arsc/BagValue/map Ljava/util/List;
invokeinterface java/util/List/size()I 0
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 12
getfield pxb/android/arsc/BagValue/map Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 12
L26:
aload 12
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L20
aload 12
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 13
aload 1
aload 13
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 0
aload 13
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast pxb/android/arsc/Value
aload 1
invokespecial pxb/android/arsc/ArscWriter/writeValue(Lpxb/android/arsc/Value;Ljava/nio/ByteBuffer;)V
goto L26
L21:
bipush 8
istore 2
goto L22
L23:
iload 2
bipush -2
iand
istore 2
goto L24
L25:
aload 0
aload 12
getfield pxb/android/arsc/ResEntry/value Ljava/lang/Object;
checkcast pxb/android/arsc/Value
aload 1
invokespecial pxb/android/arsc/ArscWriter/writeValue(Lpxb/android/arsc/Value;Ljava/nio/ByteBuffer;)V
goto L20
L2:
return
.limit locals 14
.limit stack 5
.end method

.method private writeValue(Lpxb/android/arsc/Value;Ljava/nio/ByteBuffer;)V
aload 2
bipush 8
invokevirtual java/nio/ByteBuffer/putShort(S)Ljava/nio/ByteBuffer;
pop
aload 2
iconst_0
invokevirtual java/nio/ByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 2
aload 1
getfield pxb/android/arsc/Value/type I
i2b
invokevirtual java/nio/ByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 1
getfield pxb/android/arsc/Value/type I
iconst_3
if_icmpne L0
aload 2
aload 0
getfield pxb/android/arsc/ArscWriter/strTable Ljava/util/Map;
aload 1
getfield pxb/android/arsc/Value/raw Ljava/lang/String;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast pxb/android/StringItem
getfield pxb/android/StringItem/index I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
return
L0:
aload 2
aload 1
getfield pxb/android/arsc/Value/data I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
return
.limit locals 3
.limit stack 3
.end method

.method public toByteArray()[B
.throws java/io/IOException
aload 0
invokespecial pxb/android/arsc/ArscWriter/prepare()Ljava/util/List;
pop
aload 0
invokespecial pxb/android/arsc/ArscWriter/count()I
istore 1
iload 1
invokestatic java/nio/ByteBuffer/allocate(I)Ljava/nio/ByteBuffer;
getstatic java/nio/ByteOrder/LITTLE_ENDIAN Ljava/nio/ByteOrder;
invokevirtual java/nio/ByteBuffer/order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
astore 2
aload 0
aload 2
iload 1
invokespecial pxb/android/arsc/ArscWriter/write(Ljava/nio/ByteBuffer;I)V
aload 2
invokevirtual java/nio/ByteBuffer/array()[B
areturn
.limit locals 3
.limit stack 3
.end method
