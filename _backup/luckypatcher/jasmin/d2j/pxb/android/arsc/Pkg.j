.bytecode 50.0
.class public synchronized pxb/android/arsc/Pkg
.super java/lang/Object

.field public final 'id' I

.field public 'name' Ljava/lang/String;

.field public 'types' Ljava/util/TreeMap; signature "Ljava/util/TreeMap<Ljava/lang/Integer;Lpxb/android/arsc/Type;>;"

.method public <init>(ILjava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
putfield pxb/android/arsc/Pkg/types Ljava/util/TreeMap;
aload 0
iload 1
putfield pxb/android/arsc/Pkg/id I
aload 0
aload 2
putfield pxb/android/arsc/Pkg/name Ljava/lang/String;
return
.limit locals 3
.limit stack 3
.end method

.method public getType(ILjava/lang/String;I)Lpxb/android/arsc/Type;
aload 0
getfield pxb/android/arsc/Pkg/types Ljava/util/TreeMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/TreeMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast pxb/android/arsc/Type
astore 5
aload 5
ifnull L0
aload 5
astore 4
aload 2
ifnull L1
aload 5
getfield pxb/android/arsc/Type/name Ljava/lang/String;
ifnonnull L2
aload 5
aload 2
putfield pxb/android/arsc/Type/name Ljava/lang/String;
L3:
aload 5
astore 4
aload 5
getfield pxb/android/arsc/Type/specs [Lpxb/android/arsc/ResSpec;
arraylength
iload 3
if_icmpeq L1
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L2:
aload 2
aload 5
getfield pxb/android/arsc/Type/name Ljava/lang/String;
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L3
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L0:
new pxb/android/arsc/Type
dup
invokespecial pxb/android/arsc/Type/<init>()V
astore 4
aload 4
iload 1
putfield pxb/android/arsc/Type/id I
aload 4
aload 2
putfield pxb/android/arsc/Type/name Ljava/lang/String;
aload 4
iload 3
anewarray pxb/android/arsc/ResSpec
putfield pxb/android/arsc/Type/specs [Lpxb/android/arsc/ResSpec;
aload 0
getfield pxb/android/arsc/Pkg/types Ljava/util/TreeMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 4
invokevirtual java/util/TreeMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
aload 4
areturn
.limit locals 6
.limit stack 3
.end method
