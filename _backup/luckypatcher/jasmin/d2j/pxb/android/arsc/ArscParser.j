.bytecode 50.0
.class public synchronized pxb/android/arsc/ArscParser
.super java/lang/Object
.implements pxb/android/ResConst
.inner class Chunk inner pxb/android/arsc/ArscParser$Chunk outer pxb/android/arsc/ArscParser

.field private static final 'DEBUG' Z = 0


.field static final 'ENGRY_FLAG_PUBLIC' I = 2


.field static final 'ENTRY_FLAG_COMPLEX' S = 1


.field public static final 'TYPE_STRING' I = 3


.field private 'fileSize' I

.field private 'in' Ljava/nio/ByteBuffer;

.field private 'keyNamesX' [Ljava/lang/String;

.field private 'pkg' Lpxb/android/arsc/Pkg;

.field private 'pkgs' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/arsc/Pkg;>;"

.field private 'strings' [Ljava/lang/String;

.field private 'typeNamesX' [Ljava/lang/String;

.method public <init>([B)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_m1
putfield pxb/android/arsc/ArscParser/fileSize I
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/arsc/ArscParser/pkgs Ljava/util/List;
aload 0
aload 1
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
getstatic java/nio/ByteOrder/LITTLE_ENDIAN Ljava/nio/ByteOrder;
invokevirtual java/nio/ByteBuffer/order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
putfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
return
.limit locals 2
.limit stack 3
.end method

.method private static transient D(Ljava/lang/String;[Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 0
.end method

.method static synthetic access$000(Lpxb/android/arsc/ArscParser;)Ljava/nio/ByteBuffer;
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$100(Ljava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 1
invokestatic pxb/android/arsc/ArscParser/D(Ljava/lang/String;[Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method private readEntry(Lpxb/android/arsc/Config;Lpxb/android/arsc/ResSpec;)V
ldc "[%08x]read ResTable_entry"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/position()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic pxb/android/arsc/ArscParser/D(Ljava/lang/String;[Ljava/lang/Object;)V
ldc "ResTable_entry %d"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getShort()S
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic pxb/android/arsc/ArscParser/D(Ljava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getShort()S
istore 3
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
istore 4
aload 2
aload 0
getfield pxb/android/arsc/ArscParser/keyNamesX [Ljava/lang/String;
iload 4
aaload
invokevirtual pxb/android/arsc/ResSpec/updateName(Ljava/lang/String;)V
new pxb/android/arsc/ResEntry
dup
iload 3
aload 2
invokespecial pxb/android/arsc/ResEntry/<init>(ILpxb/android/arsc/ResSpec;)V
astore 5
iload 3
iconst_1
iand
ifeq L0
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
istore 3
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
istore 4
new pxb/android/arsc/BagValue
dup
iload 3
invokespecial pxb/android/arsc/BagValue/<init>(I)V
astore 6
iconst_0
istore 3
L1:
iload 3
iload 4
if_icmpge L2
new java/util/AbstractMap$SimpleEntry
dup
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 0
invokespecial pxb/android/arsc/ArscParser/readValue()Ljava/lang/Object;
invokespecial java/util/AbstractMap$SimpleEntry/<init>(Ljava/lang/Object;Ljava/lang/Object;)V
astore 7
aload 6
getfield pxb/android/arsc/BagValue/map Ljava/util/List;
aload 7
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
aload 5
aload 6
putfield pxb/android/arsc/ResEntry/value Ljava/lang/Object;
L3:
aload 1
getfield pxb/android/arsc/Config/resources Ljava/util/Map;
aload 2
getfield pxb/android/arsc/ResSpec/id I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 5
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
L0:
aload 5
aload 0
invokespecial pxb/android/arsc/ArscParser/readValue()Ljava/lang/Object;
putfield pxb/android/arsc/ResEntry/value Ljava/lang/Object;
goto L3
.limit locals 8
.limit stack 5
.end method

.method private readPackage(Ljava/nio/ByteBuffer;)V
.throws java/io/IOException
aload 1
invokevirtual java/nio/ByteBuffer/getInt()I
istore 3
aload 1
invokevirtual java/nio/ByteBuffer/position()I
istore 4
new java/lang/StringBuilder
dup
bipush 32
invokespecial java/lang/StringBuilder/<init>(I)V
astore 6
iconst_0
istore 2
L0:
iload 2
sipush 128
if_icmpge L1
aload 1
invokevirtual java/nio/ByteBuffer/getShort()S
istore 5
iload 5
ifne L2
L1:
aload 6
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 6
aload 1
iload 4
sipush 256
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 0
new pxb/android/arsc/Pkg
dup
iload 3
sipush 255
irem
aload 6
invokespecial pxb/android/arsc/Pkg/<init>(ILjava/lang/String;)V
putfield pxb/android/arsc/ArscParser/pkg Lpxb/android/arsc/Pkg;
aload 0
getfield pxb/android/arsc/ArscParser/pkgs Ljava/util/List;
aload 0
getfield pxb/android/arsc/ArscParser/pkg Lpxb/android/arsc/Pkg;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 1
invokevirtual java/nio/ByteBuffer/getInt()I
pop
aload 1
invokevirtual java/nio/ByteBuffer/getInt()I
pop
aload 1
invokevirtual java/nio/ByteBuffer/getInt()I
pop
aload 1
invokevirtual java/nio/ByteBuffer/getInt()I
pop
new pxb/android/arsc/ArscParser$Chunk
dup
aload 0
invokespecial pxb/android/arsc/ArscParser$Chunk/<init>(Lpxb/android/arsc/ArscParser;)V
astore 6
aload 6
getfield pxb/android/arsc/ArscParser$Chunk/type I
iconst_1
if_icmpeq L3
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L2:
aload 6
iload 5
i2c
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L3:
aload 0
aload 1
invokestatic pxb/android/StringItems/read(Ljava/nio/ByteBuffer;)[Ljava/lang/String;
putfield pxb/android/arsc/ArscParser/typeNamesX [Ljava/lang/String;
aload 1
aload 6
getfield pxb/android/arsc/ArscParser$Chunk/location I
aload 6
getfield pxb/android/arsc/ArscParser$Chunk/size I
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
new pxb/android/arsc/ArscParser$Chunk
dup
aload 0
invokespecial pxb/android/arsc/ArscParser$Chunk/<init>(Lpxb/android/arsc/ArscParser;)V
astore 6
aload 6
getfield pxb/android/arsc/ArscParser$Chunk/type I
iconst_1
if_icmpeq L4
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L4:
aload 0
aload 1
invokestatic pxb/android/StringItems/read(Ljava/nio/ByteBuffer;)[Ljava/lang/String;
putfield pxb/android/arsc/ArscParser/keyNamesX [Ljava/lang/String;
aload 1
aload 6
getfield pxb/android/arsc/ArscParser$Chunk/location I
aload 6
getfield pxb/android/arsc/ArscParser$Chunk/size I
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
L5:
aload 1
invokevirtual java/nio/ByteBuffer/hasRemaining()Z
ifeq L6
new pxb/android/arsc/ArscParser$Chunk
dup
aload 0
invokespecial pxb/android/arsc/ArscParser$Chunk/<init>(Lpxb/android/arsc/ArscParser;)V
astore 6
aload 6
getfield pxb/android/arsc/ArscParser$Chunk/type I
tableswitch 513
L7
L8
default : L6
L6:
return
L8:
ldc "[%08x]read spec"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual java/nio/ByteBuffer/position()I
bipush 8
isub
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic pxb/android/arsc/ArscParser/D(Ljava/lang/String;[Ljava/lang/Object;)V
aload 1
invokevirtual java/nio/ByteBuffer/get()B
sipush 255
iand
istore 2
aload 1
invokevirtual java/nio/ByteBuffer/get()B
pop
aload 1
invokevirtual java/nio/ByteBuffer/getShort()S
pop
aload 1
invokevirtual java/nio/ByteBuffer/getInt()I
istore 3
aload 0
getfield pxb/android/arsc/ArscParser/pkg Lpxb/android/arsc/Pkg;
iload 2
aload 0
getfield pxb/android/arsc/ArscParser/typeNamesX [Ljava/lang/String;
iload 2
iconst_1
isub
aaload
iload 3
invokevirtual pxb/android/arsc/Pkg/getType(ILjava/lang/String;I)Lpxb/android/arsc/Type;
astore 7
iconst_0
istore 2
L9:
iload 2
iload 3
if_icmpge L10
aload 7
iload 2
invokevirtual pxb/android/arsc/Type/getSpec(I)Lpxb/android/arsc/ResSpec;
aload 1
invokevirtual java/nio/ByteBuffer/getInt()I
putfield pxb/android/arsc/ResSpec/flags I
iload 2
iconst_1
iadd
istore 2
goto L9
L7:
ldc "[%08x]read config"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual java/nio/ByteBuffer/position()I
bipush 8
isub
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic pxb/android/arsc/ArscParser/D(Ljava/lang/String;[Ljava/lang/Object;)V
aload 1
invokevirtual java/nio/ByteBuffer/get()B
sipush 255
iand
istore 2
aload 1
invokevirtual java/nio/ByteBuffer/get()B
pop
aload 1
invokevirtual java/nio/ByteBuffer/getShort()S
pop
aload 1
invokevirtual java/nio/ByteBuffer/getInt()I
istore 4
aload 0
getfield pxb/android/arsc/ArscParser/pkg Lpxb/android/arsc/Pkg;
iload 2
aload 0
getfield pxb/android/arsc/ArscParser/typeNamesX [Ljava/lang/String;
iload 2
iconst_1
isub
aaload
iload 4
invokevirtual pxb/android/arsc/Pkg/getType(ILjava/lang/String;I)Lpxb/android/arsc/Type;
astore 7
aload 1
invokevirtual java/nio/ByteBuffer/getInt()I
istore 3
ldc "[%08x]read config id"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual java/nio/ByteBuffer/position()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic pxb/android/arsc/ArscParser/D(Ljava/lang/String;[Ljava/lang/Object;)V
aload 1
invokevirtual java/nio/ByteBuffer/position()I
istore 2
aload 1
invokevirtual java/nio/ByteBuffer/getInt()I
newarray byte
astore 8
aload 1
iload 2
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 1
aload 8
invokevirtual java/nio/ByteBuffer/get([B)Ljava/nio/ByteBuffer;
pop
new pxb/android/arsc/Config
dup
aload 8
iload 4
invokespecial pxb/android/arsc/Config/<init>([BI)V
astore 8
aload 1
aload 6
getfield pxb/android/arsc/ArscParser$Chunk/location I
aload 6
getfield pxb/android/arsc/ArscParser$Chunk/headSize I
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
ldc "[%08x]read config entry offset"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual java/nio/ByteBuffer/position()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic pxb/android/arsc/ArscParser/D(Ljava/lang/String;[Ljava/lang/Object;)V
iload 4
newarray int
astore 9
iconst_0
istore 2
L11:
iload 2
iload 4
if_icmpge L12
aload 9
iload 2
aload 1
invokevirtual java/nio/ByteBuffer/getInt()I
iastore
iload 2
iconst_1
iadd
istore 2
goto L11
L12:
ldc "[%08x]read config entrys"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual java/nio/ByteBuffer/position()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic pxb/android/arsc/ArscParser/D(Ljava/lang/String;[Ljava/lang/Object;)V
iconst_0
istore 2
L13:
iload 2
aload 9
arraylength
if_icmpge L14
aload 9
iload 2
iaload
iconst_m1
if_icmpeq L15
aload 1
aload 6
getfield pxb/android/arsc/ArscParser$Chunk/location I
iload 3
iadd
aload 9
iload 2
iaload
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 0
aload 8
aload 7
iload 2
invokevirtual pxb/android/arsc/Type/getSpec(I)Lpxb/android/arsc/ResSpec;
invokespecial pxb/android/arsc/ArscParser/readEntry(Lpxb/android/arsc/Config;Lpxb/android/arsc/ResSpec;)V
L15:
iload 2
iconst_1
iadd
istore 2
goto L13
L14:
aload 7
aload 8
invokevirtual pxb/android/arsc/Type/addConfig(Lpxb/android/arsc/Config;)V
L10:
aload 1
aload 6
getfield pxb/android/arsc/ArscParser$Chunk/location I
aload 6
getfield pxb/android/arsc/ArscParser$Chunk/size I
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
goto L5
.limit locals 10
.limit stack 6
.end method

.method private readValue()Ljava/lang/Object;
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getShort()S
pop
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/get()B
pop
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/get()B
sipush 255
iand
istore 1
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
istore 2
aconst_null
astore 3
iload 1
iconst_3
if_icmpne L0
aload 0
getfield pxb/android/arsc/ArscParser/strings [Ljava/lang/String;
iload 2
aaload
astore 3
L0:
new pxb/android/arsc/Value
dup
iload 1
iload 2
aload 3
invokespecial pxb/android/arsc/Value/<init>(IILjava/lang/String;)V
areturn
.limit locals 4
.limit stack 5
.end method

.method public parse()Ljava/util/List;
.signature "()Ljava/util/List<Lpxb/android/arsc/Pkg;>;"
.throws java/io/IOException
aload 0
getfield pxb/android/arsc/ArscParser/fileSize I
ifge L0
new pxb/android/arsc/ArscParser$Chunk
dup
aload 0
invokespecial pxb/android/arsc/ArscParser$Chunk/<init>(Lpxb/android/arsc/ArscParser;)V
astore 1
aload 1
getfield pxb/android/arsc/ArscParser$Chunk/type I
iconst_2
if_icmpeq L1
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L1:
aload 0
aload 1
getfield pxb/android/arsc/ArscParser$Chunk/size I
putfield pxb/android/arsc/ArscParser/fileSize I
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getInt()I
pop
L0:
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/hasRemaining()Z
ifeq L2
new pxb/android/arsc/ArscParser$Chunk
dup
aload 0
invokespecial pxb/android/arsc/ArscParser$Chunk/<init>(Lpxb/android/arsc/ArscParser;)V
astore 1
aload 1
getfield pxb/android/arsc/ArscParser$Chunk/type I
lookupswitch
1 : L3
512 : L4
default : L5
L5:
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
aload 1
getfield pxb/android/arsc/ArscParser$Chunk/location I
aload 1
getfield pxb/android/arsc/ArscParser$Chunk/size I
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
goto L0
L3:
aload 0
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokestatic pxb/android/StringItems/read(Ljava/nio/ByteBuffer;)[Ljava/lang/String;
putfield pxb/android/arsc/ArscParser/strings [Ljava/lang/String;
goto L5
L4:
aload 0
aload 0
getfield pxb/android/arsc/ArscParser/in Ljava/nio/ByteBuffer;
invokespecial pxb/android/arsc/ArscParser/readPackage(Ljava/nio/ByteBuffer;)V
goto L5
L2:
aload 0
getfield pxb/android/arsc/ArscParser/pkgs Ljava/util/List;
areturn
.limit locals 2
.limit stack 3
.end method
