.bytecode 50.0
.class public abstract interface pxb/android/ResConst
.super java/lang/Object

.field public static final 'RES_STRING_POOL_TYPE' I = 1


.field public static final 'RES_TABLE_PACKAGE_TYPE' I = 512


.field public static final 'RES_TABLE_TYPE' I = 2


.field public static final 'RES_TABLE_TYPE_SPEC_TYPE' I = 514


.field public static final 'RES_TABLE_TYPE_TYPE' I = 513


.field public static final 'RES_XML_CDATA_TYPE' I = 260


.field public static final 'RES_XML_END_ELEMENT_TYPE' I = 259


.field public static final 'RES_XML_END_NAMESPACE_TYPE' I = 257


.field public static final 'RES_XML_RESOURCE_MAP_TYPE' I = 384


.field public static final 'RES_XML_START_ELEMENT_TYPE' I = 258


.field public static final 'RES_XML_START_NAMESPACE_TYPE' I = 256


.field public static final 'RES_XML_TYPE' I = 3

