.bytecode 50.0
.class public synchronized pxb/android/axmlLP/DumpAdapter
.super pxb/android/axmlLP/AxmlVisitor
.inner class public static DumpNodeAdapter inner pxb/android/axmlLP/DumpAdapter$DumpNodeAdapter outer pxb/android/axmlLP/DumpAdapter

.field private 'nses' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"

.method public <init>()V
aload 0
invokespecial pxb/android/axmlLP/AxmlVisitor/<init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield pxb/android/axmlLP/DumpAdapter/nses Ljava/util/Map;
return
.limit locals 1
.limit stack 3
.end method

.method public <init>(Lpxb/android/axmlLP/AxmlVisitor;)V
aload 0
aload 1
invokespecial pxb/android/axmlLP/AxmlVisitor/<init>(Lpxb/android/axmlLP/AxmlVisitor;)V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield pxb/android/axmlLP/DumpAdapter/nses Ljava/util/Map;
return
.limit locals 2
.limit stack 3
.end method

.method public end()V
aload 0
invokespecial pxb/android/axmlLP/AxmlVisitor/end()V
return
.limit locals 1
.limit stack 1
.end method

.method public first(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "<"
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
aload 1
ifnull L0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield pxb/android/axmlLP/DumpAdapter/nses Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L0:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 2
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
aload 1
aload 2
invokespecial pxb/android/axmlLP/AxmlVisitor/first(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
astore 1
aload 1
ifnull L1
new pxb/android/axmlLP/DumpAdapter$DumpNodeAdapter
dup
aload 1
iconst_1
aload 0
getfield pxb/android/axmlLP/DumpAdapter/nses Ljava/util/Map;
invokespecial pxb/android/axmlLP/DumpAdapter$DumpNodeAdapter/<init>(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;ILjava/util/Map;)V
areturn
L1:
aconst_null
areturn
.limit locals 3
.limit stack 5
.end method

.method public ns(Ljava/lang/String;Ljava/lang/String;I)V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
getfield pxb/android/axmlLP/DumpAdapter/nses Ljava/util/Map;
aload 2
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
aload 1
aload 2
iload 3
invokespecial pxb/android/axmlLP/AxmlVisitor/ns(Ljava/lang/String;Ljava/lang/String;I)V
return
.limit locals 4
.limit stack 4
.end method
