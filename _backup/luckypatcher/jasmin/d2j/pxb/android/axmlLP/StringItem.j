.bytecode 50.0
.class synchronized pxb/android/axmlLP/StringItem
.super java/lang/Object

.field public 'data' Ljava/lang/String;

.field public 'dataOffset' I

.field public 'index' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public equals(Ljava/lang/Object;)Z
aload 1
checkcast pxb/android/axmlLP/StringItem
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
aload 0
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public hashCode()I
aload 0
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
invokevirtual java/lang/String/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
ldc "S%04d %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield pxb/android/axmlLP/StringItem/index I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 0
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 5
.end method
