.bytecode 50.0
.class synchronized pxb/android/axmlLP/AxmlWriter$NodeImpl
.super pxb/android/axmlLP/AxmlVisitor$NodeVisitor
.inner class static NodeImpl inner pxb/android/axmlLP/AxmlWriter$NodeImpl outer pxb/android/axmlLP/AxmlWriter
.inner class inner pxb/android/axmlLP/AxmlWriter$NodeImpl$1

.field private 'attrs' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Lpxb/android/axmlLP/AxmlWriter$Attr;>;"

.field private 'children' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/axmlLP/AxmlWriter$NodeImpl;>;"

.field private 'line' I

.field private 'name' Lpxb/android/axmlLP/StringItem;

.field private 'ns' Lpxb/android/axmlLP/StringItem;

.field private 'text' Lpxb/android/axmlLP/StringItem;

.field private 'textLineNumber' I

.method public <init>(Ljava/lang/String;Ljava/lang/String;)V
aconst_null
astore 3
aload 0
aconst_null
invokespecial pxb/android/axmlLP/AxmlVisitor$NodeVisitor/<init>(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;)V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield pxb/android/axmlLP/AxmlWriter$NodeImpl/attrs Ljava/util/Map;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/axmlLP/AxmlWriter$NodeImpl/children Ljava/util/List;
aload 1
ifnonnull L0
aconst_null
astore 1
L1:
aload 0
aload 1
putfield pxb/android/axmlLP/AxmlWriter$NodeImpl/ns Lpxb/android/axmlLP/StringItem;
aload 2
ifnonnull L2
aload 3
astore 1
L3:
aload 0
aload 1
putfield pxb/android/axmlLP/AxmlWriter$NodeImpl/name Lpxb/android/axmlLP/StringItem;
return
L0:
new pxb/android/axmlLP/StringItem
dup
aload 1
invokespecial pxb/android/axmlLP/StringItem/<init>(Ljava/lang/String;)V
astore 1
goto L1
L2:
new pxb/android/axmlLP/StringItem
dup
aload 2
invokespecial pxb/android/axmlLP/StringItem/<init>(Ljava/lang/String;)V
astore 1
goto L3
.limit locals 4
.limit stack 3
.end method

.method public attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
aload 2
ifnonnull L0
new java/lang/RuntimeException
dup
ldc "name can't be null"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/attrs Ljava/util/Map;
astore 7
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 8
aload 1
ifnonnull L1
ldc "zzz"
astore 6
L2:
aload 8
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 6
aload 1
ifnonnull L3
aconst_null
astore 1
L4:
new pxb/android/axmlLP/StringItem
dup
aload 2
invokespecial pxb/android/axmlLP/StringItem/<init>(Ljava/lang/String;)V
astore 8
iload 4
iconst_3
if_icmpne L5
new pxb/android/axmlLP/StringItem
dup
aload 5
checkcast java/lang/String
invokespecial pxb/android/axmlLP/StringItem/<init>(Ljava/lang/String;)V
astore 2
L6:
aload 7
aload 6
new pxb/android/axmlLP/AxmlWriter$Attr
dup
aload 1
aload 8
iload 3
iload 4
aload 2
invokespecial pxb/android/axmlLP/AxmlWriter$Attr/<init>(Lpxb/android/axmlLP/StringItem;Lpxb/android/axmlLP/StringItem;IILjava/lang/Object;)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
L1:
aload 1
astore 6
goto L2
L3:
new pxb/android/axmlLP/StringItem
dup
aload 1
invokespecial pxb/android/axmlLP/StringItem/<init>(Ljava/lang/String;)V
astore 1
goto L4
L5:
aload 5
astore 2
goto L6
.limit locals 9
.limit stack 9
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
new pxb/android/axmlLP/AxmlWriter$NodeImpl
dup
aload 1
aload 2
invokespecial pxb/android/axmlLP/AxmlWriter$NodeImpl/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/children Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method public end()V
return
.limit locals 1
.limit stack 0
.end method

.method public line(I)V
aload 0
iload 1
putfield pxb/android/axmlLP/AxmlWriter$NodeImpl/line I
return
.limit locals 2
.limit stack 2
.end method

.method public prepare(Lpxb/android/axmlLP/AxmlWriter;)I
aload 0
aload 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/ns Lpxb/android/axmlLP/StringItem;
invokevirtual pxb/android/axmlLP/AxmlWriter/updateNs(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;
putfield pxb/android/axmlLP/AxmlWriter$NodeImpl/ns Lpxb/android/axmlLP/StringItem;
aload 0
aload 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/name Lpxb/android/axmlLP/StringItem;
invokevirtual pxb/android/axmlLP/AxmlWriter/update(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;
putfield pxb/android/axmlLP/AxmlWriter$NodeImpl/name Lpxb/android/axmlLP/StringItem;
aload 0
invokevirtual pxb/android/axmlLP/AxmlWriter$NodeImpl/sortedAttrs()Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L0:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axmlLP/AxmlWriter$Attr
aload 1
invokevirtual pxb/android/axmlLP/AxmlWriter$Attr/prepare(Lpxb/android/axmlLP/AxmlWriter;)V
goto L0
L1:
aload 0
aload 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/text Lpxb/android/axmlLP/StringItem;
invokevirtual pxb/android/axmlLP/AxmlWriter/update(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;
putfield pxb/android/axmlLP/AxmlWriter$NodeImpl/text Lpxb/android/axmlLP/StringItem;
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/attrs Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
bipush 20
imul
bipush 60
iadd
istore 2
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/children Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L2:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
iload 2
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axmlLP/AxmlWriter$NodeImpl
aload 1
invokevirtual pxb/android/axmlLP/AxmlWriter$NodeImpl/prepare(Lpxb/android/axmlLP/AxmlWriter;)I
iadd
istore 2
goto L2
L3:
iload 2
istore 3
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/text Lpxb/android/axmlLP/StringItem;
ifnull L4
iload 2
bipush 28
iadd
istore 3
L4:
iload 3
ireturn
.limit locals 5
.limit stack 3
.end method

.method sortedAttrs()Ljava/util/List;
.signature "()Ljava/util/List<Lpxb/android/axmlLP/AxmlWriter$Attr;>;"
new java/util/ArrayList
dup
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/attrs Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
astore 1
aload 1
new pxb/android/axmlLP/AxmlWriter$NodeImpl$1
dup
aload 0
invokespecial pxb/android/axmlLP/AxmlWriter$NodeImpl$1/<init>(Lpxb/android/axmlLP/AxmlWriter$NodeImpl;)V
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
aload 1
areturn
.limit locals 2
.limit stack 4
.end method

.method public text(ILjava/lang/String;)V
aload 0
new pxb/android/axmlLP/StringItem
dup
aload 2
invokespecial pxb/android/axmlLP/StringItem/<init>(Ljava/lang/String;)V
putfield pxb/android/axmlLP/AxmlWriter$NodeImpl/text Lpxb/android/axmlLP/StringItem;
aload 0
iload 1
putfield pxb/android/axmlLP/AxmlWriter$NodeImpl/textLineNumber I
return
.limit locals 3
.limit stack 4
.end method

.method write(Lcom/googlecode/dex2jar/reader/io/DataOut;)V
.throws java/io/IOException
iconst_m1
istore 3
aload 1
ldc_w 1048834
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/attrs Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
bipush 20
imul
bipush 36
iadd
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/line I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
iconst_m1
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/ns Lpxb/android/axmlLP/StringItem;
ifnull L0
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/ns Lpxb/android/axmlLP/StringItem;
getfield pxb/android/axmlLP/StringItem/index I
istore 2
L1:
aload 1
iload 2
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/name Lpxb/android/axmlLP/StringItem;
getfield pxb/android/axmlLP/StringItem/index I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
ldc_w 1310740
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/attrs Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeShort(I)V 1
aload 1
iconst_0
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeShort(I)V 1
aload 1
iconst_0
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeShort(I)V 1
aload 1
iconst_0
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeShort(I)V 1
aload 0
invokevirtual pxb/android/axmlLP/AxmlWriter$NodeImpl/sortedAttrs()Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L2:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axmlLP/AxmlWriter$Attr
astore 5
aload 5
getfield pxb/android/axmlLP/AxmlWriter$Attr/ns Lpxb/android/axmlLP/StringItem;
ifnonnull L4
iconst_m1
istore 2
L5:
aload 1
iload 2
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
aload 5
getfield pxb/android/axmlLP/AxmlWriter$Attr/name Lpxb/android/axmlLP/StringItem;
getfield pxb/android/axmlLP/StringItem/index I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 5
getfield pxb/android/axmlLP/AxmlWriter$Attr/value Ljava/lang/Object;
instanceof pxb/android/axmlLP/StringItem
ifeq L6
aload 5
getfield pxb/android/axmlLP/AxmlWriter$Attr/value Ljava/lang/Object;
checkcast pxb/android/axmlLP/StringItem
getfield pxb/android/axmlLP/StringItem/index I
istore 2
L7:
aload 1
iload 2
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
aload 5
getfield pxb/android/axmlLP/AxmlWriter$Attr/type I
bipush 24
ishl
bipush 8
ior
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 5
getfield pxb/android/axmlLP/AxmlWriter$Attr/value Ljava/lang/Object;
astore 6
aload 6
instanceof pxb/android/axmlLP/StringItem
ifeq L8
aload 1
aload 5
getfield pxb/android/axmlLP/AxmlWriter$Attr/value Ljava/lang/Object;
checkcast pxb/android/axmlLP/StringItem
getfield pxb/android/axmlLP/StringItem/index I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
goto L2
L0:
iconst_m1
istore 2
goto L1
L4:
aload 5
getfield pxb/android/axmlLP/AxmlWriter$Attr/ns Lpxb/android/axmlLP/StringItem;
getfield pxb/android/axmlLP/StringItem/index I
istore 2
goto L5
L6:
iconst_m1
istore 2
goto L7
L8:
aload 6
instanceof java/lang/Boolean
ifeq L9
getstatic java/lang/Boolean/TRUE Ljava/lang/Boolean;
aload 6
invokevirtual java/lang/Boolean/equals(Ljava/lang/Object;)Z
ifeq L10
iconst_m1
istore 2
L11:
aload 1
iload 2
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
goto L2
L10:
iconst_0
istore 2
goto L11
L9:
aload 1
aload 5
getfield pxb/android/axmlLP/AxmlWriter$Attr/value Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
goto L2
L3:
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/text Lpxb/android/axmlLP/StringItem;
ifnull L12
aload 1
ldc_w 1048836
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
bipush 28
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/textLineNumber I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
iconst_m1
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/text Lpxb/android/axmlLP/StringItem;
getfield pxb/android/axmlLP/StringItem/index I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
bipush 8
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
iconst_0
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
L12:
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/children Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L13:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L14
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axmlLP/AxmlWriter$NodeImpl
aload 1
invokevirtual pxb/android/axmlLP/AxmlWriter$NodeImpl/write(Lcom/googlecode/dex2jar/reader/io/DataOut;)V
goto L13
L14:
aload 1
ldc_w 1048835
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
bipush 24
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
iconst_m1
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
iconst_m1
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
iload 3
istore 2
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/ns Lpxb/android/axmlLP/StringItem;
ifnull L15
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/ns Lpxb/android/axmlLP/StringItem;
getfield pxb/android/axmlLP/StringItem/index I
istore 2
L15:
aload 1
iload 2
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter$NodeImpl/name Lpxb/android/axmlLP/StringItem;
getfield pxb/android/axmlLP/StringItem/index I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
return
.limit locals 7
.limit stack 3
.end method
