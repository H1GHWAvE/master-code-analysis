.bytecode 50.0
.class synchronized pxb/android/axmlLP/StringItems
.super java/util/ArrayList
.signature "Ljava/util/ArrayList<Lpxb/android/axmlLP/StringItem;>;"

.field 'stringData' [B

.method <init>()V
aload 0
invokespecial java/util/ArrayList/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public getSize()I
aload 0
invokevirtual pxb/android/axmlLP/StringItems/size()I
iconst_4
imul
bipush 20
iadd
aload 0
getfield pxb/android/axmlLP/StringItems/stringData [B
arraylength
iadd
iconst_0
iadd
ireturn
.limit locals 1
.limit stack 2
.end method

.method public prepare()V
.throws java/io/IOException
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 4
iconst_0
istore 1
iconst_0
istore 2
aload 4
invokevirtual java/io/ByteArrayOutputStream/reset()V
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 5
aload 0
invokevirtual pxb/android/axmlLP/StringItems/iterator()Ljava/util/Iterator;
astore 6
L0:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axmlLP/StringItem
astore 7
aload 7
iload 1
putfield pxb/android/axmlLP/StringItem/index I
aload 7
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
astore 8
aload 5
aload 8
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/Integer
astore 9
aload 9
ifnull L2
aload 7
aload 9
invokevirtual java/lang/Integer/intValue()I
putfield pxb/android/axmlLP/StringItem/dataOffset I
L3:
iload 1
iconst_1
iadd
istore 1
goto L0
L2:
aload 7
iload 2
putfield pxb/android/axmlLP/StringItem/dataOffset I
aload 5
aload 8
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 8
invokevirtual java/lang/String/length()I
istore 3
aload 8
ldc "UTF-16LE"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
astore 7
aload 4
iload 3
invokevirtual java/io/ByteArrayOutputStream/write(I)V
aload 4
iload 3
bipush 8
ishr
invokevirtual java/io/ByteArrayOutputStream/write(I)V
aload 4
aload 7
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 4
iconst_0
invokevirtual java/io/ByteArrayOutputStream/write(I)V
aload 4
iconst_0
invokevirtual java/io/ByteArrayOutputStream/write(I)V
iload 2
aload 7
arraylength
iconst_4
iadd
iadd
istore 2
goto L3
L1:
aload 0
aload 4
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
putfield pxb/android/axmlLP/StringItems/stringData [B
return
.limit locals 10
.limit stack 3
.end method

.method public read(Lcom/googlecode/dex2jar/reader/io/DataIn;I)V
.throws java/io/IOException
aload 1
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/getCurrentPosition()I 0
pop
aload 1
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 5
aload 1
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 7
aload 1
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 4
aload 1
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
pop
aload 1
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 6
iconst_0
istore 3
L0:
iload 3
iload 5
if_icmpge L1
new pxb/android/axmlLP/StringItem
dup
invokespecial pxb/android/axmlLP/StringItem/<init>()V
astore 8
aload 8
iload 3
putfield pxb/android/axmlLP/StringItem/index I
aload 8
aload 1
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
putfield pxb/android/axmlLP/StringItem/dataOffset I
aload 0
aload 8
invokevirtual pxb/android/axmlLP/StringItems/add(Ljava/lang/Object;)Z
pop
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
astore 8
iload 7
ifeq L2
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L2:
iload 6
ifne L3
L4:
aload 1
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/getCurrentPosition()I 0
istore 3
iload 4
sipush 256
iand
ifeq L5
iload 3
istore 4
L6:
iload 4
iload 2
if_icmpge L7
new java/io/ByteArrayOutputStream
dup
aload 1
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readLeb128()J 0
l2i
bipush 10
iadd
invokespecial java/io/ByteArrayOutputStream/<init>(I)V
astore 9
aload 1
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readByte()I 0
istore 5
L8:
iload 5
ifeq L9
aload 9
iload 5
invokevirtual java/io/ByteArrayOutputStream/write(I)V
aload 1
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readByte()I 0
istore 5
goto L8
L3:
iload 6
istore 2
goto L4
L9:
aload 8
iload 4
iload 3
isub
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
new java/lang/String
dup
aload 9
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
ldc "UTF-8"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 1
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/getCurrentPosition()I 0
istore 4
goto L6
L5:
iload 3
istore 4
L10:
iload 4
iload 2
if_icmpge L7
aload 1
aload 1
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readShortx()I 0
iconst_2
imul
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readBytes(I)[B 1
astore 9
aload 1
iconst_2
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/skip(I)V 1
aload 8
iload 4
iload 3
isub
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
new java/lang/String
dup
aload 9
ldc "UTF-16LE"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 1
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/getCurrentPosition()I 0
istore 4
goto L10
L7:
iload 6
ifeq L11
L11:
aload 0
invokevirtual pxb/android/axmlLP/StringItems/iterator()Ljava/util/Iterator;
astore 1
L12:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L13
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axmlLP/StringItem
astore 9
aload 9
aload 8
aload 9
getfield pxb/android/axmlLP/StringItem/dataOffset I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
putfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
goto L12
L13:
return
.limit locals 10
.limit stack 6
.end method

.method public write(Lcom/googlecode/dex2jar/reader/io/DataOut;)V
.throws java/io/IOException
aload 1
aload 0
invokevirtual pxb/android/axmlLP/StringItems/size()I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
iconst_0
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
iconst_0
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
aload 0
invokevirtual pxb/android/axmlLP/StringItems/size()I
iconst_4
imul
bipush 28
iadd
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 1
iconst_0
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 0
invokevirtual pxb/android/axmlLP/StringItems/iterator()Ljava/util/Iterator;
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axmlLP/StringItem
getfield pxb/android/axmlLP/StringItem/dataOffset I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
goto L0
L1:
aload 1
aload 0
getfield pxb/android/axmlLP/StringItems/stringData [B
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeBytes([B)V 1
return
.limit locals 3
.limit stack 3
.end method
