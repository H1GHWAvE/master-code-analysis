.bytecode 50.0
.class public synchronized pxb/android/axmlLP/AxmlWriter
.super pxb/android/axmlLP/AxmlVisitor
.inner class static Attr inner pxb/android/axmlLP/AxmlWriter$Attr outer pxb/android/axmlLP/AxmlWriter
.inner class static NodeImpl inner pxb/android/axmlLP/AxmlWriter$NodeImpl outer pxb/android/axmlLP/AxmlWriter
.inner class inner pxb/android/axmlLP/AxmlWriter$NodeImpl$1
.inner class static Ns inner pxb/android/axmlLP/AxmlWriter$Ns outer pxb/android/axmlLP/AxmlWriter

.field private 'firsts' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/axmlLP/AxmlWriter$NodeImpl;>;"

.field private 'nses' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Lpxb/android/axmlLP/AxmlWriter$Ns;>;"

.field private 'otherString' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/axmlLP/StringItem;>;"

.field private 'resourceId2Str' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/Integer;Lpxb/android/axmlLP/StringItem;>;"

.field private 'resourceIds' Ljava/util/List; signature "Ljava/util/List<Ljava/lang/Integer;>;"

.field private 'resourceString' Ljava/util/List; signature "Ljava/util/List<Lpxb/android/axmlLP/StringItem;>;"

.field private 'stringItems' Lpxb/android/axmlLP/StringItems;

.method public <init>()V
aload 0
invokespecial pxb/android/axmlLP/AxmlVisitor/<init>()V
aload 0
new java/util/ArrayList
dup
iconst_3
invokespecial java/util/ArrayList/<init>(I)V
putfield pxb/android/axmlLP/AxmlWriter/firsts Ljava/util/List;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield pxb/android/axmlLP/AxmlWriter/nses Ljava/util/Map;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/axmlLP/AxmlWriter/otherString Ljava/util/List;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield pxb/android/axmlLP/AxmlWriter/resourceId2Str Ljava/util/Map;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/axmlLP/AxmlWriter/resourceIds Ljava/util/List;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/axmlLP/AxmlWriter/resourceString Ljava/util/List;
aload 0
new pxb/android/axmlLP/StringItems
dup
invokespecial pxb/android/axmlLP/StringItems/<init>()V
putfield pxb/android/axmlLP/AxmlWriter/stringItems Lpxb/android/axmlLP/StringItems;
return
.limit locals 1
.limit stack 4
.end method

.method private prepare()I
.throws java/io/IOException
aload 0
getfield pxb/android/axmlLP/AxmlWriter/nses Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
bipush 24
imul
iconst_2
imul
istore 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter/firsts Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L0:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
iload 1
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axmlLP/AxmlWriter$NodeImpl
aload 0
invokevirtual pxb/android/axmlLP/AxmlWriter$NodeImpl/prepare(Lpxb/android/axmlLP/AxmlWriter;)I
iadd
istore 1
goto L0
L1:
iconst_0
istore 2
aload 0
getfield pxb/android/axmlLP/AxmlWriter/nses Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 6
L2:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 7
aload 7
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast pxb/android/axmlLP/AxmlWriter$Ns
astore 5
iload 2
istore 3
aload 5
astore 4
aload 5
ifnonnull L4
new pxb/android/axmlLP/AxmlWriter$Ns
dup
new pxb/android/axmlLP/StringItem
dup
ldc "axml_auto_%02d"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial pxb/android/axmlLP/StringItem/<init>(Ljava/lang/String;)V
new pxb/android/axmlLP/StringItem
dup
aload 7
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/String
invokespecial pxb/android/axmlLP/StringItem/<init>(Ljava/lang/String;)V
iconst_0
invokespecial pxb/android/axmlLP/AxmlWriter$Ns/<init>(Lpxb/android/axmlLP/StringItem;Lpxb/android/axmlLP/StringItem;I)V
astore 4
aload 7
aload 4
invokeinterface java/util/Map$Entry/setValue(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
iload 2
iconst_1
iadd
istore 3
L4:
aload 4
aload 0
aload 4
getfield pxb/android/axmlLP/AxmlWriter$Ns/prefix Lpxb/android/axmlLP/StringItem;
invokevirtual pxb/android/axmlLP/AxmlWriter/update(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;
putfield pxb/android/axmlLP/AxmlWriter$Ns/prefix Lpxb/android/axmlLP/StringItem;
aload 4
aload 0
aload 4
getfield pxb/android/axmlLP/AxmlWriter$Ns/uri Lpxb/android/axmlLP/StringItem;
invokevirtual pxb/android/axmlLP/AxmlWriter/update(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;
putfield pxb/android/axmlLP/AxmlWriter$Ns/uri Lpxb/android/axmlLP/StringItem;
iload 3
istore 2
goto L2
L3:
aload 0
getfield pxb/android/axmlLP/AxmlWriter/stringItems Lpxb/android/axmlLP/StringItems;
aload 0
getfield pxb/android/axmlLP/AxmlWriter/resourceString Ljava/util/List;
invokevirtual pxb/android/axmlLP/StringItems/addAll(Ljava/util/Collection;)Z
pop
aload 0
aconst_null
putfield pxb/android/axmlLP/AxmlWriter/resourceString Ljava/util/List;
aload 0
getfield pxb/android/axmlLP/AxmlWriter/stringItems Lpxb/android/axmlLP/StringItems;
aload 0
getfield pxb/android/axmlLP/AxmlWriter/otherString Ljava/util/List;
invokevirtual pxb/android/axmlLP/StringItems/addAll(Ljava/util/Collection;)Z
pop
aload 0
aconst_null
putfield pxb/android/axmlLP/AxmlWriter/otherString Ljava/util/List;
aload 0
getfield pxb/android/axmlLP/AxmlWriter/stringItems Lpxb/android/axmlLP/StringItems;
invokevirtual pxb/android/axmlLP/StringItems/prepare()V
aload 0
getfield pxb/android/axmlLP/AxmlWriter/stringItems Lpxb/android/axmlLP/StringItems;
invokevirtual pxb/android/axmlLP/StringItems/getSize()I
istore 3
iload 3
istore 2
iload 3
iconst_4
irem
ifeq L5
iload 3
iconst_4
iload 3
iconst_4
irem
isub
iadd
istore 2
L5:
iload 1
iload 2
bipush 8
iadd
iadd
aload 0
getfield pxb/android/axmlLP/AxmlWriter/resourceIds Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_4
imul
bipush 8
iadd
iadd
ireturn
.limit locals 8
.limit stack 9
.end method

.method public end()V
return
.limit locals 1
.limit stack 0
.end method

.method public first(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
new pxb/android/axmlLP/AxmlWriter$NodeImpl
dup
aload 1
aload 2
invokespecial pxb/android/axmlLP/AxmlWriter$NodeImpl/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter/firsts Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method public ns(Ljava/lang/String;Ljava/lang/String;I)V
aload 0
getfield pxb/android/axmlLP/AxmlWriter/nses Ljava/util/Map;
aload 2
new pxb/android/axmlLP/AxmlWriter$Ns
dup
new pxb/android/axmlLP/StringItem
dup
aload 1
invokespecial pxb/android/axmlLP/StringItem/<init>(Ljava/lang/String;)V
new pxb/android/axmlLP/StringItem
dup
aload 2
invokespecial pxb/android/axmlLP/StringItem/<init>(Ljava/lang/String;)V
iload 3
invokespecial pxb/android/axmlLP/AxmlWriter$Ns/<init>(Lpxb/android/axmlLP/StringItem;Lpxb/android/axmlLP/StringItem;I)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 4
.limit stack 8
.end method

.method public toByteArray()[B
.throws java/io/IOException
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 3
new com/googlecode/dex2jar/reader/io/LeDataOut
dup
aload 3
invokespecial com/googlecode/dex2jar/reader/io/LeDataOut/<init>(Ljava/io/OutputStream;)V
astore 4
aload 0
invokespecial pxb/android/axmlLP/AxmlWriter/prepare()I
istore 1
aload 4
ldc_w 524291
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 4
iload 1
bipush 8
iadd
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter/stringItems Lpxb/android/axmlLP/StringItems;
invokevirtual pxb/android/axmlLP/StringItems/getSize()I
istore 2
iconst_0
istore 1
iload 2
iconst_4
irem
ifeq L0
iconst_4
iload 2
iconst_4
irem
isub
istore 1
L0:
aload 4
ldc_w 1835009
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 4
iload 2
iload 1
iadd
bipush 8
iadd
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter/stringItems Lpxb/android/axmlLP/StringItems;
aload 4
invokevirtual pxb/android/axmlLP/StringItems/write(Lcom/googlecode/dex2jar/reader/io/DataOut;)V
aload 4
iload 1
newarray byte
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeBytes([B)V 1
aload 4
ldc_w 524672
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 4
aload 0
getfield pxb/android/axmlLP/AxmlWriter/resourceIds Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_4
imul
bipush 8
iadd
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter/resourceIds Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 5
L1:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 4
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
goto L1
L2:
new java/util/Stack
dup
invokespecial java/util/Stack/<init>()V
astore 5
aload 0
getfield pxb/android/axmlLP/AxmlWriter/nses Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 6
L3:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast pxb/android/axmlLP/AxmlWriter$Ns
astore 7
aload 5
aload 7
invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 4
ldc_w 1048832
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 4
bipush 24
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 4
iconst_m1
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 4
iconst_m1
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 4
aload 7
getfield pxb/android/axmlLP/AxmlWriter$Ns/prefix Lpxb/android/axmlLP/StringItem;
getfield pxb/android/axmlLP/StringItem/index I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 4
aload 7
getfield pxb/android/axmlLP/AxmlWriter$Ns/uri Lpxb/android/axmlLP/StringItem;
getfield pxb/android/axmlLP/StringItem/index I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
goto L3
L4:
aload 0
getfield pxb/android/axmlLP/AxmlWriter/firsts Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 6
L5:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L6
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/axmlLP/AxmlWriter$NodeImpl
aload 4
invokevirtual pxb/android/axmlLP/AxmlWriter$NodeImpl/write(Lcom/googlecode/dex2jar/reader/io/DataOut;)V
goto L5
L6:
aload 5
invokevirtual java/util/Stack/size()I
ifle L7
aload 5
invokevirtual java/util/Stack/pop()Ljava/lang/Object;
checkcast pxb/android/axmlLP/AxmlWriter$Ns
astore 6
aload 4
ldc_w 1048833
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 4
bipush 24
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 4
aload 6
getfield pxb/android/axmlLP/AxmlWriter$Ns/ln I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 4
iconst_m1
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 4
aload 6
getfield pxb/android/axmlLP/AxmlWriter$Ns/prefix Lpxb/android/axmlLP/StringItem;
getfield pxb/android/axmlLP/StringItem/index I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
aload 4
aload 6
getfield pxb/android/axmlLP/AxmlWriter$Ns/uri Lpxb/android/axmlLP/StringItem;
getfield pxb/android/axmlLP/StringItem/index I
invokeinterface com/googlecode/dex2jar/reader/io/DataOut/writeInt(I)V 1
goto L6
L7:
aload 3
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
areturn
.limit locals 8
.limit stack 3
.end method

.method update(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield pxb/android/axmlLP/AxmlWriter/otherString Ljava/util/List;
aload 1
invokeinterface java/util/List/indexOf(Ljava/lang/Object;)I 1
istore 2
iload 2
ifge L1
new pxb/android/axmlLP/StringItem
dup
aload 1
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
invokespecial pxb/android/axmlLP/StringItem/<init>(Ljava/lang/String;)V
astore 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter/otherString Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 1
areturn
L1:
aload 0
getfield pxb/android/axmlLP/AxmlWriter/otherString Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast pxb/android/axmlLP/StringItem
areturn
.limit locals 3
.limit stack 3
.end method

.method updateNs(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 1
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
astore 2
aload 0
getfield pxb/android/axmlLP/AxmlWriter/nses Ljava/util/Map;
aload 2
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L1
aload 0
getfield pxb/android/axmlLP/AxmlWriter/nses Ljava/util/Map;
aload 2
aconst_null
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L1:
aload 0
aload 1
invokevirtual pxb/android/axmlLP/AxmlWriter/update(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;
areturn
.limit locals 3
.limit stack 3
.end method

.method updateWithResourceId(Lpxb/android/axmlLP/StringItem;I)Lpxb/android/axmlLP/StringItem;
aload 0
getfield pxb/android/axmlLP/AxmlWriter/resourceId2Str Ljava/util/Map;
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast pxb/android/axmlLP/StringItem
astore 3
aload 3
ifnull L0
aload 3
areturn
L0:
new pxb/android/axmlLP/StringItem
dup
aload 1
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
invokespecial pxb/android/axmlLP/StringItem/<init>(Ljava/lang/String;)V
astore 1
aload 0
getfield pxb/android/axmlLP/AxmlWriter/resourceIds Ljava/util/List;
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield pxb/android/axmlLP/AxmlWriter/resourceString Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield pxb/android/axmlLP/AxmlWriter/resourceId2Str Ljava/util/Map;
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 1
areturn
.limit locals 4
.limit stack 3
.end method
