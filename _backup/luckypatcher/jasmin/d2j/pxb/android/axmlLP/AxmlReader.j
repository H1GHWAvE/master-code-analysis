.bytecode 50.0
.class public synchronized pxb/android/axmlLP/AxmlReader
.super java/lang/Object
.inner class static final inner pxb/android/axmlLP/AxmlReader$1
.inner class inner pxb/android/axmlLP/AxmlReader$2

.field static final 'CHUNK_AXML_FILE' I = 524291


.field static final 'CHUNK_RESOURCEIDS' I = 524672


.field static final 'CHUNK_STRINGS' I = 1835009


.field static final 'CHUNK_XML_END_NAMESPACE' I = 1048833


.field static final 'CHUNK_XML_END_TAG' I = 1048835


.field static final 'CHUNK_XML_START_NAMESPACE' I = 1048832


.field static final 'CHUNK_XML_START_TAG' I = 1048834


.field static final 'CHUNK_XML_TEXT' I = 1048836


.field public static final 'EMPTY_VISITOR' Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

.field static final 'UTF8_FLAG' I = 256


.field private 'in' Lcom/googlecode/dex2jar/reader/io/DataIn;

.field private 'resourceIds' Ljava/util/List; signature "Ljava/util/List<Ljava/lang/Integer;>;"

.field private 'stringItems' Lpxb/android/axmlLP/StringItems;

.method static <clinit>()V
new pxb/android/axmlLP/AxmlReader$1
dup
invokespecial pxb/android/axmlLP/AxmlReader$1/<init>()V
putstatic pxb/android/axmlLP/AxmlReader/EMPTY_VISITOR Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Lcom/googlecode/dex2jar/reader/io/DataIn;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield pxb/android/axmlLP/AxmlReader/resourceIds Ljava/util/List;
aload 0
new pxb/android/axmlLP/StringItems
dup
invokespecial pxb/android/axmlLP/StringItems/<init>()V
putfield pxb/android/axmlLP/AxmlReader/stringItems Lpxb/android/axmlLP/StringItems;
aload 0
aload 1
putfield pxb/android/axmlLP/AxmlReader/in Lcom/googlecode/dex2jar/reader/io/DataIn;
return
.limit locals 2
.limit stack 3
.end method

.method public <init>([B)V
aload 0
aload 1
invokestatic com/googlecode/dex2jar/reader/io/ArrayDataIn/le([B)Lcom/googlecode/dex2jar/reader/io/ArrayDataIn;
invokespecial pxb/android/axmlLP/AxmlReader/<init>(Lcom/googlecode/dex2jar/reader/io/DataIn;)V
return
.limit locals 2
.limit stack 2
.end method

.method public accept(Lpxb/android/axmlLP/AxmlVisitor;)V
.throws java/io/IOException
aload 0
getfield pxb/android/axmlLP/AxmlReader/in Lcom/googlecode/dex2jar/reader/io/DataIn;
astore 15
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
ldc_w 524291
if_icmpeq L0
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L0:
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 5
aload 1
ifnonnull L1
getstatic pxb/android/axmlLP/AxmlReader/EMPTY_VISITOR Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
astore 12
L2:
aload 12
astore 13
new java/util/Stack
dup
invokespecial java/util/Stack/<init>()V
astore 16
aload 16
aload 12
invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/getCurrentPosition()I 0
istore 2
aload 13
astore 12
L3:
iload 2
iload 5
if_icmpge L4
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 3
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 6
iload 3
lookupswitch
524672 : L5
1048832 : L6
1048833 : L7
1048834 : L8
1048835 : L9
1048836 : L10
1835009 : L11
default : L12
L12:
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L1:
new pxb/android/axmlLP/AxmlReader$2
dup
aload 0
aload 1
invokespecial pxb/android/axmlLP/AxmlReader$2/<init>(Lpxb/android/axmlLP/AxmlReader;Lpxb/android/axmlLP/AxmlVisitor;)V
astore 12
goto L2
L8:
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 3
aload 15
iconst_4
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/skip(I)V 1
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 4
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 7
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
ldc_w 1310740
if_icmpeq L13
new java/lang/RuntimeException
dup
invokespecial java/lang/RuntimeException/<init>()V
athrow
L13:
aload 0
getfield pxb/android/axmlLP/AxmlReader/stringItems Lpxb/android/axmlLP/StringItems;
iload 7
invokevirtual pxb/android/axmlLP/StringItems/get(I)Ljava/lang/Object;
checkcast pxb/android/axmlLP/StringItem
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
astore 14
iload 4
iflt L14
aload 0
getfield pxb/android/axmlLP/AxmlReader/stringItems Lpxb/android/axmlLP/StringItems;
iload 4
invokevirtual pxb/android/axmlLP/StringItems/get(I)Ljava/lang/Object;
checkcast pxb/android/axmlLP/StringItem
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
astore 13
L15:
aload 12
aload 13
aload 14
invokevirtual pxb/android/axmlLP/AxmlVisitor$NodeVisitor/child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
astore 13
aload 13
astore 12
aload 13
ifnonnull L16
getstatic pxb/android/axmlLP/AxmlReader/EMPTY_VISITOR Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
astore 12
L16:
aload 16
aload 12
invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 12
iload 3
invokevirtual pxb/android/axmlLP/AxmlVisitor$NodeVisitor/line(I)V
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readUShortx()I 0
istore 7
aload 15
bipush 6
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/skip(I)V 1
aload 12
getstatic pxb/android/axmlLP/AxmlReader/EMPTY_VISITOR Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
if_acmpeq L17
iconst_0
istore 3
L18:
aload 12
astore 13
iload 3
iload 7
if_icmpge L19
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 4
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 9
aload 15
iconst_4
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/skip(I)V 1
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
bipush 24
iushr
istore 8
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 10
aload 0
getfield pxb/android/axmlLP/AxmlReader/stringItems Lpxb/android/axmlLP/StringItems;
iload 9
invokevirtual pxb/android/axmlLP/StringItems/get(I)Ljava/lang/Object;
checkcast pxb/android/axmlLP/StringItem
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
astore 17
iload 4
iflt L20
aload 0
getfield pxb/android/axmlLP/AxmlReader/stringItems Lpxb/android/axmlLP/StringItems;
iload 4
invokevirtual pxb/android/axmlLP/StringItems/get(I)Ljava/lang/Object;
checkcast pxb/android/axmlLP/StringItem
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
astore 14
L21:
iload 8
lookupswitch
3 : L22
18 : L23
default : L24
L24:
iload 10
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
astore 13
L25:
iload 9
aload 0
getfield pxb/android/axmlLP/AxmlReader/resourceIds Ljava/util/List;
invokeinterface java/util/List/size()I 0
if_icmpge L26
aload 0
getfield pxb/android/axmlLP/AxmlReader/resourceIds Ljava/util/List;
iload 9
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
istore 4
L27:
aload 12
aload 14
aload 17
iload 4
iload 8
aload 13
invokevirtual pxb/android/axmlLP/AxmlVisitor$NodeVisitor/attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
iload 3
iconst_1
iadd
istore 3
goto L18
L14:
aconst_null
astore 13
goto L15
L20:
aconst_null
astore 14
goto L21
L22:
aload 0
getfield pxb/android/axmlLP/AxmlReader/stringItems Lpxb/android/axmlLP/StringItems;
iload 10
invokevirtual pxb/android/axmlLP/StringItems/get(I)Ljava/lang/Object;
checkcast pxb/android/axmlLP/StringItem
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
astore 13
goto L25
L23:
iload 10
ifeq L28
iconst_1
istore 11
L29:
iload 11
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
astore 13
goto L25
L28:
iconst_0
istore 11
goto L29
L26:
iconst_m1
istore 4
goto L27
L17:
aload 15
bipush 20
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/skip(I)V 1
aload 12
astore 13
L19:
aload 15
iload 2
iload 6
iadd
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/move(I)V 1
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/getCurrentPosition()I 0
istore 2
aload 13
astore 12
goto L3
L9:
aload 15
iload 6
bipush 8
isub
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/skip(I)V 1
aload 12
invokevirtual pxb/android/axmlLP/AxmlVisitor$NodeVisitor/end()V
aload 16
invokevirtual java/util/Stack/pop()Ljava/lang/Object;
pop
aload 16
invokevirtual java/util/Stack/peek()Ljava/lang/Object;
checkcast pxb/android/axmlLP/AxmlVisitor$NodeVisitor
astore 13
goto L19
L6:
aload 1
ifnonnull L30
aload 15
bipush 16
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/skip(I)V 1
aload 12
astore 13
goto L19
L30:
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 3
aload 15
iconst_4
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/skip(I)V 1
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 4
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 7
aload 1
aload 0
getfield pxb/android/axmlLP/AxmlReader/stringItems Lpxb/android/axmlLP/StringItems;
iload 4
invokevirtual pxb/android/axmlLP/StringItems/get(I)Ljava/lang/Object;
checkcast pxb/android/axmlLP/StringItem
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
aload 0
getfield pxb/android/axmlLP/AxmlReader/stringItems Lpxb/android/axmlLP/StringItems;
iload 7
invokevirtual pxb/android/axmlLP/StringItems/get(I)Ljava/lang/Object;
checkcast pxb/android/axmlLP/StringItem
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
iload 3
invokevirtual pxb/android/axmlLP/AxmlVisitor/ns(Ljava/lang/String;Ljava/lang/String;I)V
aload 12
astore 13
goto L19
L7:
aload 15
iload 6
bipush 8
isub
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/skip(I)V 1
aload 12
astore 13
goto L19
L11:
aload 0
getfield pxb/android/axmlLP/AxmlReader/stringItems Lpxb/android/axmlLP/StringItems;
aload 15
iload 6
invokevirtual pxb/android/axmlLP/StringItems/read(Lcom/googlecode/dex2jar/reader/io/DataIn;I)V
aload 12
astore 13
goto L19
L5:
iload 6
iconst_4
idiv
istore 4
iconst_0
istore 3
L31:
aload 12
astore 13
iload 3
iload 4
iconst_2
isub
if_icmpge L19
aload 0
getfield pxb/android/axmlLP/AxmlReader/resourceIds Ljava/util/List;
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 3
iconst_1
iadd
istore 3
goto L31
L10:
aload 12
getstatic pxb/android/axmlLP/AxmlReader/EMPTY_VISITOR Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
if_acmpne L32
aload 15
bipush 20
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/skip(I)V 1
aload 12
astore 13
goto L19
L32:
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 3
aload 15
iconst_4
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/skip(I)V 1
aload 15
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/readIntx()I 0
istore 4
aload 15
bipush 8
invokeinterface com/googlecode/dex2jar/reader/io/DataIn/skip(I)V 1
aload 12
iload 3
aload 0
getfield pxb/android/axmlLP/AxmlReader/stringItems Lpxb/android/axmlLP/StringItems;
iload 4
invokevirtual pxb/android/axmlLP/StringItems/get(I)Ljava/lang/Object;
checkcast pxb/android/axmlLP/StringItem
getfield pxb/android/axmlLP/StringItem/data Ljava/lang/String;
invokevirtual pxb/android/axmlLP/AxmlVisitor$NodeVisitor/text(ILjava/lang/String;)V
aload 12
astore 13
goto L19
L4:
return
.limit locals 18
.limit stack 6
.end method
