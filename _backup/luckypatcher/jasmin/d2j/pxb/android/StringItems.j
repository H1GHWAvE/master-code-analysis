.bytecode 50.0
.class public synchronized pxb/android/StringItems
.super java/util/ArrayList
.signature "Ljava/util/ArrayList<Lpxb/android/StringItem;>;"

.field static final 'UTF8_FLAG' I = 256


.field 'stringData' [B

.field private 'useUTF8' Z

.method public <init>()V
aload 0
invokespecial java/util/ArrayList/<init>()V
aload 0
iconst_1
putfield pxb/android/StringItems/useUTF8 Z
return
.limit locals 1
.limit stack 2
.end method

.method public static read(Ljava/nio/ByteBuffer;)[Ljava/lang/String;
.throws java/io/IOException
aload 0
invokevirtual java/nio/ByteBuffer/position()I
bipush 8
isub
istore 3
aload 0
invokevirtual java/nio/ByteBuffer/getInt()I
istore 2
aload 0
invokevirtual java/nio/ByteBuffer/getInt()I
pop
aload 0
invokevirtual java/nio/ByteBuffer/getInt()I
istore 4
aload 0
invokevirtual java/nio/ByteBuffer/getInt()I
istore 5
aload 0
invokevirtual java/nio/ByteBuffer/getInt()I
istore 6
iload 2
newarray int
astore 8
iload 2
anewarray java/lang/String
astore 9
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 8
iload 1
aload 0
invokevirtual java/nio/ByteBuffer/getInt()I
iastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 6
ifeq L2
getstatic java/lang/System/err Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "ignore style offset at 0x"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L2:
iconst_0
istore 1
L3:
iload 1
aload 8
arraylength
if_icmpge L4
aload 0
aload 8
iload 1
iaload
iload 3
iload 5
iadd
iadd
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
iload 4
sipush 256
iand
ifeq L5
aload 0
invokestatic pxb/android/StringItems/u8length(Ljava/nio/ByteBuffer;)I
pop
aload 0
invokestatic pxb/android/StringItems/u8length(Ljava/nio/ByteBuffer;)I
istore 2
aload 0
invokevirtual java/nio/ByteBuffer/position()I
istore 6
L6:
aload 0
iload 6
iload 2
iadd
invokevirtual java/nio/ByteBuffer/get(I)B
ifeq L7
iload 2
iconst_1
iadd
istore 2
goto L6
L7:
new java/lang/String
dup
aload 0
invokevirtual java/nio/ByteBuffer/array()[B
iload 6
iload 2
ldc "UTF-8"
invokespecial java/lang/String/<init>([BIILjava/lang/String;)V
astore 7
L8:
aload 9
iload 1
aload 7
aastore
iload 1
iconst_1
iadd
istore 1
goto L3
L5:
aload 0
invokestatic pxb/android/StringItems/u16length(Ljava/nio/ByteBuffer;)I
istore 2
new java/lang/String
dup
aload 0
invokevirtual java/nio/ByteBuffer/array()[B
aload 0
invokevirtual java/nio/ByteBuffer/position()I
iload 2
iconst_2
imul
ldc "UTF-16LE"
invokespecial java/lang/String/<init>([BIILjava/lang/String;)V
astore 7
goto L8
L4:
aload 9
areturn
.limit locals 10
.limit stack 6
.end method

.method static u16length(Ljava/nio/ByteBuffer;)I
aload 0
invokevirtual java/nio/ByteBuffer/getShort()S
ldc_w 65535
iand
istore 2
iload 2
istore 1
iload 2
sipush 32767
if_icmple L0
iload 2
sipush 32767
iand
bipush 8
ishl
aload 0
invokevirtual java/nio/ByteBuffer/getShort()S
ldc_w 65535
iand
ior
istore 1
L0:
iload 1
ireturn
.limit locals 3
.limit stack 3
.end method

.method static u8length(Ljava/nio/ByteBuffer;)I
aload 0
invokevirtual java/nio/ByteBuffer/get()B
sipush 255
iand
istore 2
iload 2
istore 1
iload 2
sipush 128
iand
ifeq L0
iload 2
bipush 127
iand
bipush 8
ishl
aload 0
invokevirtual java/nio/ByteBuffer/get()B
sipush 255
iand
ior
istore 1
L0:
iload 1
ireturn
.limit locals 3
.limit stack 3
.end method

.method public getSize()I
aload 0
invokevirtual pxb/android/StringItems/size()I
iconst_4
imul
bipush 20
iadd
aload 0
getfield pxb/android/StringItems/stringData [B
arraylength
iadd
iconst_0
iadd
ireturn
.limit locals 1
.limit stack 2
.end method

.method public prepare()V
.throws java/io/IOException
aload 0
invokevirtual pxb/android/StringItems/iterator()Ljava/util/Iterator;
astore 6
L0:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/StringItem
getfield pxb/android/StringItem/data Ljava/lang/String;
invokevirtual java/lang/String/length()I
sipush 32767
if_icmple L0
aload 0
iconst_0
putfield pxb/android/StringItems/useUTF8 Z
goto L0
L1:
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 6
iconst_0
istore 3
iconst_0
istore 1
aload 6
invokevirtual java/io/ByteArrayOutputStream/reset()V
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 7
aload 0
invokevirtual pxb/android/StringItems/iterator()Ljava/util/Iterator;
astore 8
L2:
aload 8
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 8
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/StringItem
astore 9
aload 9
iload 3
putfield pxb/android/StringItem/index I
aload 9
getfield pxb/android/StringItem/data Ljava/lang/String;
astore 10
aload 7
aload 10
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/Integer
astore 11
aload 11
ifnull L4
aload 9
aload 11
invokevirtual java/lang/Integer/intValue()I
putfield pxb/android/StringItem/dataOffset I
L5:
iload 3
iconst_1
iadd
istore 3
goto L2
L4:
aload 9
iload 1
putfield pxb/android/StringItem/dataOffset I
aload 7
aload 10
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
getfield pxb/android/StringItems/useUTF8 Z
ifeq L6
aload 10
invokevirtual java/lang/String/length()I
istore 5
aload 10
ldc "UTF-8"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
astore 9
aload 9
arraylength
istore 4
iload 1
istore 2
iload 5
bipush 127
if_icmple L7
iload 1
iconst_1
iadd
istore 2
aload 6
iload 5
bipush 8
ishr
sipush 128
ior
invokevirtual java/io/ByteArrayOutputStream/write(I)V
L7:
aload 6
iload 5
invokevirtual java/io/ByteArrayOutputStream/write(I)V
iload 2
istore 1
iload 4
bipush 127
if_icmple L8
iload 2
iconst_1
iadd
istore 1
aload 6
iload 4
bipush 8
ishr
sipush 128
ior
invokevirtual java/io/ByteArrayOutputStream/write(I)V
L8:
aload 6
iload 4
invokevirtual java/io/ByteArrayOutputStream/write(I)V
aload 6
aload 9
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 6
iconst_0
invokevirtual java/io/ByteArrayOutputStream/write(I)V
iload 1
iload 4
iconst_3
iadd
iadd
istore 1
goto L5
L6:
aload 10
invokevirtual java/lang/String/length()I
istore 4
aload 10
ldc "UTF-16LE"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
astore 9
iload 1
istore 2
iload 4
sipush 32767
if_icmple L9
iload 4
bipush 16
ishr
ldc_w 32768
ior
istore 2
aload 6
iload 2
invokevirtual java/io/ByteArrayOutputStream/write(I)V
aload 6
iload 2
bipush 8
ishr
invokevirtual java/io/ByteArrayOutputStream/write(I)V
iload 1
iconst_2
iadd
istore 2
L9:
aload 6
iload 4
invokevirtual java/io/ByteArrayOutputStream/write(I)V
aload 6
iload 4
bipush 8
ishr
invokevirtual java/io/ByteArrayOutputStream/write(I)V
aload 6
aload 9
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 6
iconst_0
invokevirtual java/io/ByteArrayOutputStream/write(I)V
aload 6
iconst_0
invokevirtual java/io/ByteArrayOutputStream/write(I)V
iload 2
aload 9
arraylength
iconst_4
iadd
iadd
istore 1
goto L5
L3:
aload 0
aload 6
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
putfield pxb/android/StringItems/stringData [B
return
.limit locals 12
.limit stack 3
.end method

.method public write(Ljava/nio/ByteBuffer;)V
.throws java/io/IOException
aload 1
aload 0
invokevirtual pxb/android/StringItems/size()I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
iconst_0
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 0
getfield pxb/android/StringItems/useUTF8 Z
ifeq L0
sipush 256
istore 2
L1:
aload 1
iload 2
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
aload 0
invokevirtual pxb/android/StringItems/size()I
iconst_4
imul
bipush 28
iadd
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 1
iconst_0
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 0
invokevirtual pxb/android/StringItems/iterator()Ljava/util/Iterator;
astore 3
L2:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast pxb/android/StringItem
getfield pxb/android/StringItem/dataOffset I
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
goto L2
L0:
iconst_0
istore 2
goto L1
L3:
aload 1
aload 0
getfield pxb/android/StringItems/stringData [B
invokevirtual java/nio/ByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
return
.limit locals 4
.limit stack 3
.end method
