.bytecode 50.0
.class public synchronized android/support/v4/view/ViewGroupCompat
.super java/lang/Object
.inner class static ViewGroupCompatHCImpl inner android/support/v4/view/ViewGroupCompat$ViewGroupCompatHCImpl outer android/support/v4/view/ViewGroupCompat
.inner class static ViewGroupCompatIcsImpl inner android/support/v4/view/ViewGroupCompat$ViewGroupCompatIcsImpl outer android/support/v4/view/ViewGroupCompat
.inner class static abstract interface ViewGroupCompatImpl inner android/support/v4/view/ViewGroupCompat$ViewGroupCompatImpl outer android/support/v4/view/ViewGroupCompat
.inner class static ViewGroupCompatStubImpl inner android/support/v4/view/ViewGroupCompat$ViewGroupCompatStubImpl outer android/support/v4/view/ViewGroupCompat

.field static final 'IMPL' Landroid/support/v4/view/ViewGroupCompat$ViewGroupCompatImpl;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
istore 0
iload 0
bipush 14
if_icmplt L0
new android/support/v4/view/ViewGroupCompat$ViewGroupCompatIcsImpl
dup
invokespecial android/support/v4/view/ViewGroupCompat$ViewGroupCompatIcsImpl/<init>()V
putstatic android/support/v4/view/ViewGroupCompat/IMPL Landroid/support/v4/view/ViewGroupCompat$ViewGroupCompatImpl;
return
L0:
iload 0
bipush 11
if_icmplt L1
new android/support/v4/view/ViewGroupCompat$ViewGroupCompatHCImpl
dup
invokespecial android/support/v4/view/ViewGroupCompat$ViewGroupCompatHCImpl/<init>()V
putstatic android/support/v4/view/ViewGroupCompat/IMPL Landroid/support/v4/view/ViewGroupCompat$ViewGroupCompatImpl;
return
L1:
new android/support/v4/view/ViewGroupCompat$ViewGroupCompatStubImpl
dup
invokespecial android/support/v4/view/ViewGroupCompat$ViewGroupCompatStubImpl/<init>()V
putstatic android/support/v4/view/ViewGroupCompat/IMPL Landroid/support/v4/view/ViewGroupCompat$ViewGroupCompatImpl;
return
.limit locals 1
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
getstatic android/support/v4/view/ViewGroupCompat/IMPL Landroid/support/v4/view/ViewGroupCompat$ViewGroupCompatImpl;
aload 0
aload 1
aload 2
invokeinterface android/support/v4/view/ViewGroupCompat$ViewGroupCompatImpl/onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z 3
ireturn
.limit locals 3
.limit stack 4
.end method

.method public static setMotionEventSplittingEnabled(Landroid/view/ViewGroup;Z)V
getstatic android/support/v4/view/ViewGroupCompat/IMPL Landroid/support/v4/view/ViewGroupCompat$ViewGroupCompatImpl;
aload 0
iload 1
invokeinterface android/support/v4/view/ViewGroupCompat$ViewGroupCompatImpl/setMotionEventSplittingEnabled(Landroid/view/ViewGroup;Z)V 2
return
.limit locals 2
.limit stack 3
.end method
