.bytecode 50.0
.class synchronized android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImplJbMr1
.super java/lang/Object
.implements android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl
.inner class static MarginLayoutParamsCompatImplJbMr1 inner android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImplJbMr1 outer android/support/v4/view/MarginLayoutParamsCompat

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public getLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;)I
aload 1
invokestatic android/support/v4/view/MarginLayoutParamsCompatJellybeanMr1/getLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;)I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I
aload 1
invokestatic android/support/v4/view/MarginLayoutParamsCompatJellybeanMr1/getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I
aload 1
invokestatic android/support/v4/view/MarginLayoutParamsCompatJellybeanMr1/getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public isMarginRelative(Landroid/view/ViewGroup$MarginLayoutParams;)Z
aload 1
invokestatic android/support/v4/view/MarginLayoutParamsCompatJellybeanMr1/isMarginRelative(Landroid/view/ViewGroup$MarginLayoutParams;)Z
ireturn
.limit locals 2
.limit stack 1
.end method

.method public resolveLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;I)V
aload 1
iload 2
invokestatic android/support/v4/view/MarginLayoutParamsCompatJellybeanMr1/resolveLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;I)V
return
.limit locals 3
.limit stack 2
.end method

.method public setLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;I)V
aload 1
iload 2
invokestatic android/support/v4/view/MarginLayoutParamsCompatJellybeanMr1/setLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;I)V
return
.limit locals 3
.limit stack 2
.end method

.method public setMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;I)V
aload 1
iload 2
invokestatic android/support/v4/view/MarginLayoutParamsCompatJellybeanMr1/setMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;I)V
return
.limit locals 3
.limit stack 2
.end method

.method public setMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;I)V
aload 1
iload 2
invokestatic android/support/v4/view/MarginLayoutParamsCompatJellybeanMr1/setMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;I)V
return
.limit locals 3
.limit stack 2
.end method
