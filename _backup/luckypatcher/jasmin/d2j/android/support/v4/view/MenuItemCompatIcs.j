.bytecode 50.0
.class synchronized android/support/v4/view/MenuItemCompatIcs
.super java/lang/Object
.inner class static OnActionExpandListenerWrapper inner android/support/v4/view/MenuItemCompatIcs$OnActionExpandListenerWrapper outer android/support/v4/view/MenuItemCompatIcs
.inner class static abstract interface SupportActionExpandProxy inner android/support/v4/view/MenuItemCompatIcs$SupportActionExpandProxy outer android/support/v4/view/MenuItemCompatIcs

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static collapseActionView(Landroid/view/MenuItem;)Z
aload 0
invokeinterface android/view/MenuItem/collapseActionView()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static expandActionView(Landroid/view/MenuItem;)Z
aload 0
invokeinterface android/view/MenuItem/expandActionView()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static isActionViewExpanded(Landroid/view/MenuItem;)Z
aload 0
invokeinterface android/view/MenuItem/isActionViewExpanded()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static setOnActionExpandListener(Landroid/view/MenuItem;Landroid/support/v4/view/MenuItemCompatIcs$SupportActionExpandProxy;)Landroid/view/MenuItem;
aload 0
new android/support/v4/view/MenuItemCompatIcs$OnActionExpandListenerWrapper
dup
aload 1
invokespecial android/support/v4/view/MenuItemCompatIcs$OnActionExpandListenerWrapper/<init>(Landroid/support/v4/view/MenuItemCompatIcs$SupportActionExpandProxy;)V
invokeinterface android/view/MenuItem/setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem; 1
areturn
.limit locals 2
.limit stack 4
.end method
