.bytecode 50.0
.class abstract interface android/support/v4/view/MenuItemCompat$MenuVersionImpl
.super java/lang/Object
.inner class static abstract interface MenuVersionImpl inner android/support/v4/view/MenuItemCompat$MenuVersionImpl outer android/support/v4/view/MenuItemCompat

.method public abstract collapseActionView(Landroid/view/MenuItem;)Z
.end method

.method public abstract expandActionView(Landroid/view/MenuItem;)Z
.end method

.method public abstract getActionView(Landroid/view/MenuItem;)Landroid/view/View;
.end method

.method public abstract isActionViewExpanded(Landroid/view/MenuItem;)Z
.end method

.method public abstract setActionView(Landroid/view/MenuItem;I)Landroid/view/MenuItem;
.end method

.method public abstract setActionView(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;
.end method

.method public abstract setOnActionExpandListener(Landroid/view/MenuItem;Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/view/MenuItem;
.end method

.method public abstract setShowAsAction(Landroid/view/MenuItem;I)V
.end method
