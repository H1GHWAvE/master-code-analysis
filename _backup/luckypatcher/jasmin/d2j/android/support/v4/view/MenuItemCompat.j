.bytecode 50.0
.class public synchronized android/support/v4/view/MenuItemCompat
.super java/lang/Object
.inner class static BaseMenuVersionImpl inner android/support/v4/view/MenuItemCompat$BaseMenuVersionImpl outer android/support/v4/view/MenuItemCompat
.inner class static HoneycombMenuVersionImpl inner android/support/v4/view/MenuItemCompat$HoneycombMenuVersionImpl outer android/support/v4/view/MenuItemCompat
.inner class static IcsMenuVersionImpl inner android/support/v4/view/MenuItemCompat$IcsMenuVersionImpl outer android/support/v4/view/MenuItemCompat
.inner class inner android/support/v4/view/MenuItemCompat$IcsMenuVersionImpl$1
.inner class static abstract interface MenuVersionImpl inner android/support/v4/view/MenuItemCompat$MenuVersionImpl outer android/support/v4/view/MenuItemCompat
.inner class public static abstract interface OnActionExpandListener inner android/support/v4/view/MenuItemCompat$OnActionExpandListener outer android/support/v4/view/MenuItemCompat

.field static final 'IMPL' Landroid/support/v4/view/MenuItemCompat$MenuVersionImpl;

.field public static final 'SHOW_AS_ACTION_ALWAYS' I = 2


.field public static final 'SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW' I = 8


.field public static final 'SHOW_AS_ACTION_IF_ROOM' I = 1


.field public static final 'SHOW_AS_ACTION_NEVER' I = 0


.field public static final 'SHOW_AS_ACTION_WITH_TEXT' I = 4


.field private static final 'TAG' Ljava/lang/String; = "MenuItemCompat"

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
istore 0
iload 0
bipush 14
if_icmplt L0
new android/support/v4/view/MenuItemCompat$IcsMenuVersionImpl
dup
invokespecial android/support/v4/view/MenuItemCompat$IcsMenuVersionImpl/<init>()V
putstatic android/support/v4/view/MenuItemCompat/IMPL Landroid/support/v4/view/MenuItemCompat$MenuVersionImpl;
return
L0:
iload 0
bipush 11
if_icmplt L1
new android/support/v4/view/MenuItemCompat$HoneycombMenuVersionImpl
dup
invokespecial android/support/v4/view/MenuItemCompat$HoneycombMenuVersionImpl/<init>()V
putstatic android/support/v4/view/MenuItemCompat/IMPL Landroid/support/v4/view/MenuItemCompat$MenuVersionImpl;
return
L1:
new android/support/v4/view/MenuItemCompat$BaseMenuVersionImpl
dup
invokespecial android/support/v4/view/MenuItemCompat$BaseMenuVersionImpl/<init>()V
putstatic android/support/v4/view/MenuItemCompat/IMPL Landroid/support/v4/view/MenuItemCompat$MenuVersionImpl;
return
.limit locals 1
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static collapseActionView(Landroid/view/MenuItem;)Z
aload 0
instanceof android/support/v4/internal/view/SupportMenuItem
ifeq L0
aload 0
checkcast android/support/v4/internal/view/SupportMenuItem
invokeinterface android/support/v4/internal/view/SupportMenuItem/collapseActionView()Z 0
ireturn
L0:
getstatic android/support/v4/view/MenuItemCompat/IMPL Landroid/support/v4/view/MenuItemCompat$MenuVersionImpl;
aload 0
invokeinterface android/support/v4/view/MenuItemCompat$MenuVersionImpl/collapseActionView(Landroid/view/MenuItem;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static expandActionView(Landroid/view/MenuItem;)Z
aload 0
instanceof android/support/v4/internal/view/SupportMenuItem
ifeq L0
aload 0
checkcast android/support/v4/internal/view/SupportMenuItem
invokeinterface android/support/v4/internal/view/SupportMenuItem/expandActionView()Z 0
ireturn
L0:
getstatic android/support/v4/view/MenuItemCompat/IMPL Landroid/support/v4/view/MenuItemCompat$MenuVersionImpl;
aload 0
invokeinterface android/support/v4/view/MenuItemCompat$MenuVersionImpl/expandActionView(Landroid/view/MenuItem;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static getActionProvider(Landroid/view/MenuItem;)Landroid/support/v4/view/ActionProvider;
aload 0
instanceof android/support/v4/internal/view/SupportMenuItem
ifeq L0
aload 0
checkcast android/support/v4/internal/view/SupportMenuItem
invokeinterface android/support/v4/internal/view/SupportMenuItem/getSupportActionProvider()Landroid/support/v4/view/ActionProvider; 0
areturn
L0:
ldc "MenuItemCompat"
ldc "getActionProvider: item does not implement SupportMenuItem; returning null"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
.limit locals 1
.limit stack 2
.end method

.method public static getActionView(Landroid/view/MenuItem;)Landroid/view/View;
aload 0
instanceof android/support/v4/internal/view/SupportMenuItem
ifeq L0
aload 0
checkcast android/support/v4/internal/view/SupportMenuItem
invokeinterface android/support/v4/internal/view/SupportMenuItem/getActionView()Landroid/view/View; 0
areturn
L0:
getstatic android/support/v4/view/MenuItemCompat/IMPL Landroid/support/v4/view/MenuItemCompat$MenuVersionImpl;
aload 0
invokeinterface android/support/v4/view/MenuItemCompat$MenuVersionImpl/getActionView(Landroid/view/MenuItem;)Landroid/view/View; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method public static isActionViewExpanded(Landroid/view/MenuItem;)Z
aload 0
instanceof android/support/v4/internal/view/SupportMenuItem
ifeq L0
aload 0
checkcast android/support/v4/internal/view/SupportMenuItem
invokeinterface android/support/v4/internal/view/SupportMenuItem/isActionViewExpanded()Z 0
ireturn
L0:
getstatic android/support/v4/view/MenuItemCompat/IMPL Landroid/support/v4/view/MenuItemCompat$MenuVersionImpl;
aload 0
invokeinterface android/support/v4/view/MenuItemCompat$MenuVersionImpl/isActionViewExpanded(Landroid/view/MenuItem;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static setActionProvider(Landroid/view/MenuItem;Landroid/support/v4/view/ActionProvider;)Landroid/view/MenuItem;
aload 0
instanceof android/support/v4/internal/view/SupportMenuItem
ifeq L0
aload 0
checkcast android/support/v4/internal/view/SupportMenuItem
aload 1
invokeinterface android/support/v4/internal/view/SupportMenuItem/setSupportActionProvider(Landroid/support/v4/view/ActionProvider;)Landroid/support/v4/internal/view/SupportMenuItem; 1
areturn
L0:
ldc "MenuItemCompat"
ldc "setActionProvider: item does not implement SupportMenuItem; ignoring"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public static setActionView(Landroid/view/MenuItem;I)Landroid/view/MenuItem;
aload 0
instanceof android/support/v4/internal/view/SupportMenuItem
ifeq L0
aload 0
checkcast android/support/v4/internal/view/SupportMenuItem
iload 1
invokeinterface android/support/v4/internal/view/SupportMenuItem/setActionView(I)Landroid/view/MenuItem; 1
areturn
L0:
getstatic android/support/v4/view/MenuItemCompat/IMPL Landroid/support/v4/view/MenuItemCompat$MenuVersionImpl;
aload 0
iload 1
invokeinterface android/support/v4/view/MenuItemCompat$MenuVersionImpl/setActionView(Landroid/view/MenuItem;I)Landroid/view/MenuItem; 2
areturn
.limit locals 2
.limit stack 3
.end method

.method public static setActionView(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;
aload 0
instanceof android/support/v4/internal/view/SupportMenuItem
ifeq L0
aload 0
checkcast android/support/v4/internal/view/SupportMenuItem
aload 1
invokeinterface android/support/v4/internal/view/SupportMenuItem/setActionView(Landroid/view/View;)Landroid/view/MenuItem; 1
areturn
L0:
getstatic android/support/v4/view/MenuItemCompat/IMPL Landroid/support/v4/view/MenuItemCompat$MenuVersionImpl;
aload 0
aload 1
invokeinterface android/support/v4/view/MenuItemCompat$MenuVersionImpl/setActionView(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem; 2
areturn
.limit locals 2
.limit stack 3
.end method

.method public static setOnActionExpandListener(Landroid/view/MenuItem;Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/view/MenuItem;
aload 0
instanceof android/support/v4/internal/view/SupportMenuItem
ifeq L0
aload 0
checkcast android/support/v4/internal/view/SupportMenuItem
aload 1
invokeinterface android/support/v4/internal/view/SupportMenuItem/setSupportOnActionExpandListener(Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/support/v4/internal/view/SupportMenuItem; 1
areturn
L0:
getstatic android/support/v4/view/MenuItemCompat/IMPL Landroid/support/v4/view/MenuItemCompat$MenuVersionImpl;
aload 0
aload 1
invokeinterface android/support/v4/view/MenuItemCompat$MenuVersionImpl/setOnActionExpandListener(Landroid/view/MenuItem;Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/view/MenuItem; 2
areturn
.limit locals 2
.limit stack 3
.end method

.method public static setShowAsAction(Landroid/view/MenuItem;I)V
aload 0
instanceof android/support/v4/internal/view/SupportMenuItem
ifeq L0
aload 0
checkcast android/support/v4/internal/view/SupportMenuItem
iload 1
invokeinterface android/support/v4/internal/view/SupportMenuItem/setShowAsAction(I)V 1
return
L0:
getstatic android/support/v4/view/MenuItemCompat/IMPL Landroid/support/v4/view/MenuItemCompat$MenuVersionImpl;
aload 0
iload 1
invokeinterface android/support/v4/view/MenuItemCompat$MenuVersionImpl/setShowAsAction(Landroid/view/MenuItem;I)V 2
return
.limit locals 2
.limit stack 3
.end method
