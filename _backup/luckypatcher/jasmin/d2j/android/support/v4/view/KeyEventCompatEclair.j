.bytecode 50.0
.class synchronized android/support/v4/view/KeyEventCompatEclair
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static dispatch(Landroid/view/KeyEvent;Landroid/view/KeyEvent$Callback;Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
checkcast android/view/KeyEvent$DispatcherState
aload 3
invokevirtual android/view/KeyEvent/dispatch(Landroid/view/KeyEvent$Callback;Landroid/view/KeyEvent$DispatcherState;Ljava/lang/Object;)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method public static getKeyDispatcherState(Landroid/view/View;)Ljava/lang/Object;
aload 0
invokevirtual android/view/View/getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static isTracking(Landroid/view/KeyEvent;)Z
aload 0
invokevirtual android/view/KeyEvent/isTracking()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static startTracking(Landroid/view/KeyEvent;)V
aload 0
invokevirtual android/view/KeyEvent/startTracking()V
return
.limit locals 1
.limit stack 1
.end method
