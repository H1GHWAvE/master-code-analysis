.bytecode 50.0
.class synchronized android/support/v4/view/MarginLayoutParamsCompatJellybeanMr1
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;)I
aload 0
invokevirtual android/view/ViewGroup$MarginLayoutParams/getLayoutDirection()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I
aload 0
invokevirtual android/view/ViewGroup$MarginLayoutParams/getMarginEnd()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I
aload 0
invokevirtual android/view/ViewGroup$MarginLayoutParams/getMarginStart()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static isMarginRelative(Landroid/view/ViewGroup$MarginLayoutParams;)Z
aload 0
invokevirtual android/view/ViewGroup$MarginLayoutParams/isMarginRelative()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static resolveLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;I)V
aload 0
iload 1
invokevirtual android/view/ViewGroup$MarginLayoutParams/resolveLayoutDirection(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public static setLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;I)V
aload 0
iload 1
invokevirtual android/view/ViewGroup$MarginLayoutParams/setLayoutDirection(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public static setMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;I)V
aload 0
iload 1
invokevirtual android/view/ViewGroup$MarginLayoutParams/setMarginEnd(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public static setMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;I)V
aload 0
iload 1
invokevirtual android/view/ViewGroup$MarginLayoutParams/setMarginStart(I)V
return
.limit locals 2
.limit stack 2
.end method
