.bytecode 50.0
.class abstract interface android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl
.super java/lang/Object
.inner class static abstract interface MarginLayoutParamsCompatImpl inner android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl outer android/support/v4/view/MarginLayoutParamsCompat

.method public abstract getLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;)I
.end method

.method public abstract getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I
.end method

.method public abstract getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I
.end method

.method public abstract isMarginRelative(Landroid/view/ViewGroup$MarginLayoutParams;)Z
.end method

.method public abstract resolveLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;I)V
.end method

.method public abstract setLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;I)V
.end method

.method public abstract setMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;I)V
.end method

.method public abstract setMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;I)V
.end method
