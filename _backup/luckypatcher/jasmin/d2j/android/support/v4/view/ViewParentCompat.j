.bytecode 50.0
.class public synchronized android/support/v4/view/ViewParentCompat
.super java/lang/Object
.inner class static ViewParentCompatICSImpl inner android/support/v4/view/ViewParentCompat$ViewParentCompatICSImpl outer android/support/v4/view/ViewParentCompat
.inner class static abstract interface ViewParentCompatImpl inner android/support/v4/view/ViewParentCompat$ViewParentCompatImpl outer android/support/v4/view/ViewParentCompat
.inner class static ViewParentCompatStubImpl inner android/support/v4/view/ViewParentCompat$ViewParentCompatStubImpl outer android/support/v4/view/ViewParentCompat

.field static final 'IMPL' Landroid/support/v4/view/ViewParentCompat$ViewParentCompatImpl;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L0
new android/support/v4/view/ViewParentCompat$ViewParentCompatICSImpl
dup
invokespecial android/support/v4/view/ViewParentCompat$ViewParentCompatICSImpl/<init>()V
putstatic android/support/v4/view/ViewParentCompat/IMPL Landroid/support/v4/view/ViewParentCompat$ViewParentCompatImpl;
return
L0:
new android/support/v4/view/ViewParentCompat$ViewParentCompatStubImpl
dup
invokespecial android/support/v4/view/ViewParentCompat$ViewParentCompatStubImpl/<init>()V
putstatic android/support/v4/view/ViewParentCompat/IMPL Landroid/support/v4/view/ViewParentCompat$ViewParentCompatImpl;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static requestSendAccessibilityEvent(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
getstatic android/support/v4/view/ViewParentCompat/IMPL Landroid/support/v4/view/ViewParentCompat$ViewParentCompatImpl;
aload 0
aload 1
aload 2
invokeinterface android/support/v4/view/ViewParentCompat$ViewParentCompatImpl/requestSendAccessibilityEvent(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z 3
ireturn
.limit locals 3
.limit stack 4
.end method
