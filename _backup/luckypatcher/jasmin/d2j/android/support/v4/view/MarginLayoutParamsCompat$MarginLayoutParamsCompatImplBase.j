.bytecode 50.0
.class synchronized android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImplBase
.super java/lang/Object
.implements android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl
.inner class static MarginLayoutParamsCompatImplBase inner android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImplBase outer android/support/v4/view/MarginLayoutParamsCompat

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public getLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;)I
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I
aload 1
getfield android/view/ViewGroup$MarginLayoutParams/rightMargin I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I
aload 1
getfield android/view/ViewGroup$MarginLayoutParams/leftMargin I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public isMarginRelative(Landroid/view/ViewGroup$MarginLayoutParams;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public resolveLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;I)V
return
.limit locals 3
.limit stack 0
.end method

.method public setLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;I)V
return
.limit locals 3
.limit stack 0
.end method

.method public setMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;I)V
aload 1
iload 2
putfield android/view/ViewGroup$MarginLayoutParams/rightMargin I
return
.limit locals 3
.limit stack 2
.end method

.method public setMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;I)V
aload 1
iload 2
putfield android/view/ViewGroup$MarginLayoutParams/leftMargin I
return
.limit locals 3
.limit stack 2
.end method
