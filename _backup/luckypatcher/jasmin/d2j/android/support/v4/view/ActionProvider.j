.bytecode 50.0
.class public synchronized abstract android/support/v4/view/ActionProvider
.super java/lang/Object
.inner class public static abstract interface SubUiVisibilityListener inner android/support/v4/view/ActionProvider$SubUiVisibilityListener outer android/support/v4/view/ActionProvider
.inner class public static abstract interface VisibilityListener inner android/support/v4/view/ActionProvider$VisibilityListener outer android/support/v4/view/ActionProvider

.field private static final 'TAG' Ljava/lang/String; = "ActionProvider(support)"

.field private final 'mContext' Landroid/content/Context;

.field private 'mSubUiVisibilityListener' Landroid/support/v4/view/ActionProvider$SubUiVisibilityListener;

.field private 'mVisibilityListener' Landroid/support/v4/view/ActionProvider$VisibilityListener;

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/view/ActionProvider/mContext Landroid/content/Context;
return
.limit locals 2
.limit stack 2
.end method

.method public getContext()Landroid/content/Context;
aload 0
getfield android/support/v4/view/ActionProvider/mContext Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method public hasSubMenu()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isVisible()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public abstract onCreateActionView()Landroid/view/View;
.end method

.method public onCreateActionView(Landroid/view/MenuItem;)Landroid/view/View;
aload 0
invokevirtual android/support/v4/view/ActionProvider/onCreateActionView()Landroid/view/View;
areturn
.limit locals 2
.limit stack 1
.end method

.method public onPerformDefaultAction()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public onPrepareSubMenu(Landroid/view/SubMenu;)V
return
.limit locals 2
.limit stack 0
.end method

.method public overridesItemVisibility()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public refreshVisibility()V
aload 0
getfield android/support/v4/view/ActionProvider/mVisibilityListener Landroid/support/v4/view/ActionProvider$VisibilityListener;
ifnull L0
aload 0
invokevirtual android/support/v4/view/ActionProvider/overridesItemVisibility()Z
ifeq L0
aload 0
getfield android/support/v4/view/ActionProvider/mVisibilityListener Landroid/support/v4/view/ActionProvider$VisibilityListener;
aload 0
invokevirtual android/support/v4/view/ActionProvider/isVisible()Z
invokeinterface android/support/v4/view/ActionProvider$VisibilityListener/onActionProviderVisibilityChanged(Z)V 1
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public setSubUiVisibilityListener(Landroid/support/v4/view/ActionProvider$SubUiVisibilityListener;)V
aload 0
aload 1
putfield android/support/v4/view/ActionProvider/mSubUiVisibilityListener Landroid/support/v4/view/ActionProvider$SubUiVisibilityListener;
return
.limit locals 2
.limit stack 2
.end method

.method public setVisibilityListener(Landroid/support/v4/view/ActionProvider$VisibilityListener;)V
aload 0
getfield android/support/v4/view/ActionProvider/mVisibilityListener Landroid/support/v4/view/ActionProvider$VisibilityListener;
ifnull L0
aload 1
ifnull L0
ldc "ActionProvider(support)"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " instance while it is still in use somewhere else?"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
aload 1
putfield android/support/v4/view/ActionProvider/mVisibilityListener Landroid/support/v4/view/ActionProvider$VisibilityListener;
return
.limit locals 2
.limit stack 3
.end method

.method public subUiVisibilityChanged(Z)V
aload 0
getfield android/support/v4/view/ActionProvider/mSubUiVisibilityListener Landroid/support/v4/view/ActionProvider$SubUiVisibilityListener;
ifnull L0
aload 0
getfield android/support/v4/view/ActionProvider/mSubUiVisibilityListener Landroid/support/v4/view/ActionProvider$SubUiVisibilityListener;
iload 1
invokeinterface android/support/v4/view/ActionProvider$SubUiVisibilityListener/onSubUiVisibilityChanged(Z)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method
