.bytecode 50.0
.class synchronized android/support/v4/view/ViewCompatJellybeanMr1
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getLabelFor(Landroid/view/View;)I
aload 0
invokevirtual android/view/View/getLabelFor()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static getLayoutDirection(Landroid/view/View;)I
aload 0
invokevirtual android/view/View/getLayoutDirection()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static setLabelFor(Landroid/view/View;I)V
aload 0
iload 1
invokevirtual android/view/View/setLabelFor(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public static setLayerPaint(Landroid/view/View;Landroid/graphics/Paint;)V
aload 0
aload 1
invokevirtual android/view/View/setLayerPaint(Landroid/graphics/Paint;)V
return
.limit locals 2
.limit stack 2
.end method

.method public static setLayoutDirection(Landroid/view/View;I)V
aload 0
iload 1
invokevirtual android/view/View/setLayoutDirection(I)V
return
.limit locals 2
.limit stack 2
.end method
