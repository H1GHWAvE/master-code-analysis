.bytecode 50.0
.class public synchronized android/support/v4/view/MarginLayoutParamsCompat
.super java/lang/Object
.inner class static abstract interface MarginLayoutParamsCompatImpl inner android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl outer android/support/v4/view/MarginLayoutParamsCompat
.inner class static MarginLayoutParamsCompatImplBase inner android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImplBase outer android/support/v4/view/MarginLayoutParamsCompat
.inner class static MarginLayoutParamsCompatImplJbMr1 inner android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImplJbMr1 outer android/support/v4/view/MarginLayoutParamsCompat

.field static final 'IMPL' Landroid/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 17
if_icmplt L0
new android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImplJbMr1
dup
invokespecial android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImplJbMr1/<init>()V
putstatic android/support/v4/view/MarginLayoutParamsCompat/IMPL Landroid/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl;
return
L0:
new android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImplBase
dup
invokespecial android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImplBase/<init>()V
putstatic android/support/v4/view/MarginLayoutParamsCompat/IMPL Landroid/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;)I
getstatic android/support/v4/view/MarginLayoutParamsCompat/IMPL Landroid/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl;
aload 0
invokeinterface android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl/getLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I
getstatic android/support/v4/view/MarginLayoutParamsCompat/IMPL Landroid/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl;
aload 0
invokeinterface android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl/getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I
getstatic android/support/v4/view/MarginLayoutParamsCompat/IMPL Landroid/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl;
aload 0
invokeinterface android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl/getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static isMarginRelative(Landroid/view/ViewGroup$MarginLayoutParams;)Z
getstatic android/support/v4/view/MarginLayoutParamsCompat/IMPL Landroid/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl;
aload 0
invokeinterface android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl/isMarginRelative(Landroid/view/ViewGroup$MarginLayoutParams;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static resolveLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;I)V
getstatic android/support/v4/view/MarginLayoutParamsCompat/IMPL Landroid/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl;
aload 0
iload 1
invokeinterface android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl/resolveLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;I)V 2
return
.limit locals 2
.limit stack 3
.end method

.method public static setLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;I)V
getstatic android/support/v4/view/MarginLayoutParamsCompat/IMPL Landroid/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl;
aload 0
iload 1
invokeinterface android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl/setLayoutDirection(Landroid/view/ViewGroup$MarginLayoutParams;I)V 2
return
.limit locals 2
.limit stack 3
.end method

.method public static setMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;I)V
getstatic android/support/v4/view/MarginLayoutParamsCompat/IMPL Landroid/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl;
aload 0
iload 1
invokeinterface android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl/setMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;I)V 2
return
.limit locals 2
.limit stack 3
.end method

.method public static setMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;I)V
getstatic android/support/v4/view/MarginLayoutParamsCompat/IMPL Landroid/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl;
aload 0
iload 1
invokeinterface android/support/v4/view/MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl/setMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;I)V 2
return
.limit locals 2
.limit stack 3
.end method
