.bytecode 50.0
.class abstract interface android/support/v4/view/KeyEventCompat$KeyEventVersionImpl
.super java/lang/Object
.inner class static abstract interface KeyEventVersionImpl inner android/support/v4/view/KeyEventCompat$KeyEventVersionImpl outer android/support/v4/view/KeyEventCompat

.method public abstract dispatch(Landroid/view/KeyEvent;Landroid/view/KeyEvent$Callback;Ljava/lang/Object;Ljava/lang/Object;)Z
.end method

.method public abstract getKeyDispatcherState(Landroid/view/View;)Ljava/lang/Object;
.end method

.method public abstract isTracking(Landroid/view/KeyEvent;)Z
.end method

.method public abstract metaStateHasModifiers(II)Z
.end method

.method public abstract metaStateHasNoModifiers(I)Z
.end method

.method public abstract normalizeMetaState(I)I
.end method

.method public abstract startTracking(Landroid/view/KeyEvent;)V
.end method
