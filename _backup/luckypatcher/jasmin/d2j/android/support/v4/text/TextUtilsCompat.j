.bytecode 50.0
.class public synchronized android/support/v4/text/TextUtilsCompat
.super java/lang/Object

.field private static 'ARAB_SCRIPT_SUBTAG' Ljava/lang/String;

.field private static 'HEBR_SCRIPT_SUBTAG' Ljava/lang/String;

.field public static final 'ROOT' Ljava/util/Locale;

.method static <clinit>()V
new java/util/Locale
dup
ldc ""
ldc ""
invokespecial java/util/Locale/<init>(Ljava/lang/String;Ljava/lang/String;)V
putstatic android/support/v4/text/TextUtilsCompat/ROOT Ljava/util/Locale;
ldc "Arab"
putstatic android/support/v4/text/TextUtilsCompat/ARAB_SCRIPT_SUBTAG Ljava/lang/String;
ldc "Hebr"
putstatic android/support/v4/text/TextUtilsCompat/HEBR_SCRIPT_SUBTAG Ljava/lang/String;
return
.limit locals 0
.limit stack 4
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static getLayoutDirectionFromFirstChar(Ljava/util/Locale;)I
aload 0
aload 0
invokevirtual java/util/Locale/getDisplayName(Ljava/util/Locale;)Ljava/lang/String;
iconst_0
invokevirtual java/lang/String/charAt(I)C
invokestatic java/lang/Character/getDirectionality(C)B
tableswitch 1
L0
L0
default : L1
L1:
iconst_0
ireturn
L0:
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static getLayoutDirectionFromLocale(Ljava/util/Locale;)I
aload 0
ifnull L0
aload 0
getstatic android/support/v4/text/TextUtilsCompat/ROOT Ljava/util/Locale;
invokevirtual java/util/Locale/equals(Ljava/lang/Object;)Z
ifne L0
aload 0
invokevirtual java/util/Locale/toString()Ljava/lang/String;
invokestatic android/support/v4/text/ICUCompat/addLikelySubtags(Ljava/lang/String;)Ljava/lang/String;
invokestatic android/support/v4/text/ICUCompat/getScript(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 1
ifnonnull L1
aload 0
invokestatic android/support/v4/text/TextUtilsCompat/getLayoutDirectionFromFirstChar(Ljava/util/Locale;)I
ireturn
L1:
aload 1
getstatic android/support/v4/text/TextUtilsCompat/ARAB_SCRIPT_SUBTAG Ljava/lang/String;
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifne L2
aload 1
getstatic android/support/v4/text/TextUtilsCompat/HEBR_SCRIPT_SUBTAG Ljava/lang/String;
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L0
L2:
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static htmlEncode(Ljava/lang/String;)Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 3
iconst_0
istore 2
L0:
iload 2
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L1
aload 0
iload 2
invokevirtual java/lang/String/charAt(I)C
istore 1
iload 1
lookupswitch
34 : L2
38 : L3
39 : L4
60 : L5
62 : L6
default : L7
L7:
aload 3
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L8:
iload 2
iconst_1
iadd
istore 2
goto L0
L5:
aload 3
ldc "&lt;"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L8
L6:
aload 3
ldc "&gt;"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L8
L3:
aload 3
ldc "&amp;"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L8
L4:
aload 3
ldc "&#39;"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L8
L2:
aload 3
ldc "&quot;"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L8
L1:
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 2
.end method
