.bytecode 50.0
.class synchronized android/support/v4/text/ICUCompatIcs
.super java/lang/Object

.field private static final 'TAG' Ljava/lang/String; = "ICUCompatIcs"

.field private static 'sAddLikelySubtagsMethod' Ljava/lang/reflect/Method;

.field private static 'sGetScriptMethod' Ljava/lang/reflect/Method;

.method static <clinit>()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
L0:
ldc "libcore.icu.ICU"
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
astore 0
L1:
aload 0
ifnull L4
L3:
aload 0
ldc "getScript"
iconst_1
anewarray java/lang/Class
dup
iconst_0
ldc java/lang/String
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putstatic android/support/v4/text/ICUCompatIcs/sGetScriptMethod Ljava/lang/reflect/Method;
aload 0
ldc "addLikelySubtags"
iconst_1
anewarray java/lang/Class
dup
iconst_0
ldc java/lang/String
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putstatic android/support/v4/text/ICUCompatIcs/sAddLikelySubtagsMethod Ljava/lang/reflect/Method;
L4:
return
L2:
astore 0
ldc "ICUCompatIcs"
aload 0
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 1
.limit stack 6
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static addLikelySubtags(Ljava/lang/String;)Ljava/lang/String;
.catch java/lang/IllegalAccessException from L0 to L1 using L2
.catch java/lang/reflect/InvocationTargetException from L0 to L1 using L3
L0:
getstatic android/support/v4/text/ICUCompatIcs/sAddLikelySubtagsMethod Ljava/lang/reflect/Method;
ifnull L4
getstatic android/support/v4/text/ICUCompatIcs/sAddLikelySubtagsMethod Ljava/lang/reflect/Method;
aconst_null
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "ICUCompatIcs"
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L4:
aload 0
areturn
L3:
astore 1
ldc "ICUCompatIcs"
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L4
.limit locals 2
.limit stack 6
.end method

.method public static getScript(Ljava/lang/String;)Ljava/lang/String;
.catch java/lang/IllegalAccessException from L0 to L1 using L2
.catch java/lang/reflect/InvocationTargetException from L0 to L1 using L3
L0:
getstatic android/support/v4/text/ICUCompatIcs/sGetScriptMethod Ljava/lang/reflect/Method;
ifnull L4
getstatic android/support/v4/text/ICUCompatIcs/sGetScriptMethod Ljava/lang/reflect/Method;
aconst_null
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
astore 0
L1:
aload 0
areturn
L2:
astore 0
ldc "ICUCompatIcs"
aload 0
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L4:
aconst_null
areturn
L3:
astore 0
ldc "ICUCompatIcs"
aload 0
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L4
.limit locals 1
.limit stack 6
.end method
