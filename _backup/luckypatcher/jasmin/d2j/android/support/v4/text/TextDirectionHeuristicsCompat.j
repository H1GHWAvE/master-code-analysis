.bytecode 50.0
.class public synchronized android/support/v4/text/TextDirectionHeuristicsCompat
.super java/lang/Object
.inner class static synthetic inner android/support/v4/text/TextDirectionHeuristicsCompat$1
.inner class private static AnyStrong inner android/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong outer android/support/v4/text/TextDirectionHeuristicsCompat
.inner class private static FirstStrong inner android/support/v4/text/TextDirectionHeuristicsCompat$FirstStrong outer android/support/v4/text/TextDirectionHeuristicsCompat
.inner class private static abstract interface TextDirectionAlgorithm inner android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionAlgorithm outer android/support/v4/text/TextDirectionHeuristicsCompat
.inner class private static abstract TextDirectionHeuristicImpl inner android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicImpl outer android/support/v4/text/TextDirectionHeuristicsCompat
.inner class private static TextDirectionHeuristicInternal inner android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicInternal outer android/support/v4/text/TextDirectionHeuristicsCompat
.inner class private static TextDirectionHeuristicLocale inner android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicLocale outer android/support/v4/text/TextDirectionHeuristicsCompat

.field public static final 'ANYRTL_LTR' Landroid/support/v4/text/TextDirectionHeuristicCompat;

.field public static final 'FIRSTSTRONG_LTR' Landroid/support/v4/text/TextDirectionHeuristicCompat;

.field public static final 'FIRSTSTRONG_RTL' Landroid/support/v4/text/TextDirectionHeuristicCompat;

.field public static final 'LOCALE' Landroid/support/v4/text/TextDirectionHeuristicCompat;

.field public static final 'LTR' Landroid/support/v4/text/TextDirectionHeuristicCompat;

.field public static final 'RTL' Landroid/support/v4/text/TextDirectionHeuristicCompat;

.field private static final 'STATE_FALSE' I = 1


.field private static final 'STATE_TRUE' I = 0


.field private static final 'STATE_UNKNOWN' I = 2


.method static <clinit>()V
new android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicInternal
dup
aconst_null
iconst_0
aconst_null
invokespecial android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicInternal/<init>(Landroid/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionAlgorithm;ZLandroid/support/v4/text/TextDirectionHeuristicsCompat$1;)V
putstatic android/support/v4/text/TextDirectionHeuristicsCompat/LTR Landroid/support/v4/text/TextDirectionHeuristicCompat;
new android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicInternal
dup
aconst_null
iconst_1
aconst_null
invokespecial android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicInternal/<init>(Landroid/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionAlgorithm;ZLandroid/support/v4/text/TextDirectionHeuristicsCompat$1;)V
putstatic android/support/v4/text/TextDirectionHeuristicsCompat/RTL Landroid/support/v4/text/TextDirectionHeuristicCompat;
new android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicInternal
dup
getstatic android/support/v4/text/TextDirectionHeuristicsCompat$FirstStrong/INSTANCE Landroid/support/v4/text/TextDirectionHeuristicsCompat$FirstStrong;
iconst_0
aconst_null
invokespecial android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicInternal/<init>(Landroid/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionAlgorithm;ZLandroid/support/v4/text/TextDirectionHeuristicsCompat$1;)V
putstatic android/support/v4/text/TextDirectionHeuristicsCompat/FIRSTSTRONG_LTR Landroid/support/v4/text/TextDirectionHeuristicCompat;
new android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicInternal
dup
getstatic android/support/v4/text/TextDirectionHeuristicsCompat$FirstStrong/INSTANCE Landroid/support/v4/text/TextDirectionHeuristicsCompat$FirstStrong;
iconst_1
aconst_null
invokespecial android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicInternal/<init>(Landroid/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionAlgorithm;ZLandroid/support/v4/text/TextDirectionHeuristicsCompat$1;)V
putstatic android/support/v4/text/TextDirectionHeuristicsCompat/FIRSTSTRONG_RTL Landroid/support/v4/text/TextDirectionHeuristicCompat;
new android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicInternal
dup
getstatic android/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong/INSTANCE_RTL Landroid/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong;
iconst_0
aconst_null
invokespecial android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicInternal/<init>(Landroid/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionAlgorithm;ZLandroid/support/v4/text/TextDirectionHeuristicsCompat$1;)V
putstatic android/support/v4/text/TextDirectionHeuristicsCompat/ANYRTL_LTR Landroid/support/v4/text/TextDirectionHeuristicCompat;
getstatic android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicLocale/INSTANCE Landroid/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionHeuristicLocale;
putstatic android/support/v4/text/TextDirectionHeuristicsCompat/LOCALE Landroid/support/v4/text/TextDirectionHeuristicCompat;
return
.limit locals 0
.limit stack 5
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$100(I)I
iload 0
invokestatic android/support/v4/text/TextDirectionHeuristicsCompat/isRtlTextOrFormat(I)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$200(I)I
iload 0
invokestatic android/support/v4/text/TextDirectionHeuristicsCompat/isRtlText(I)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static isRtlText(I)I
iload 0
tableswitch 0
L0
L1
L1
default : L2
L2:
iconst_2
ireturn
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static isRtlTextOrFormat(I)I
iload 0
lookupswitch
0 : L0
1 : L1
2 : L1
14 : L0
15 : L0
16 : L1
17 : L1
default : L2
L2:
iconst_2
ireturn
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method
