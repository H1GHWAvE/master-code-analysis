.bytecode 50.0
.class synchronized android/support/v4/text/BidiFormatter$DirectionalityEstimator
.super java/lang/Object
.inner class private static DirectionalityEstimator inner android/support/v4/text/BidiFormatter$DirectionalityEstimator outer android/support/v4/text/BidiFormatter

.field private static final 'DIR_TYPE_CACHE' [B

.field private static final 'DIR_TYPE_CACHE_SIZE' I = 1792


.field private 'charIndex' I

.field private final 'isHtml' Z

.field private 'lastChar' C

.field private final 'length' I

.field private final 'text' Ljava/lang/String;

.method static <clinit>()V
sipush 1792
newarray byte
putstatic android/support/v4/text/BidiFormatter$DirectionalityEstimator/DIR_TYPE_CACHE [B
iconst_0
istore 0
L0:
iload 0
sipush 1792
if_icmpge L1
getstatic android/support/v4/text/BidiFormatter$DirectionalityEstimator/DIR_TYPE_CACHE [B
iload 0
iload 0
invokestatic java/lang/Character/getDirectionality(I)B
bastore
iload 0
iconst_1
iadd
istore 0
goto L0
L1:
return
.limit locals 1
.limit stack 3
.end method

.method <init>(Ljava/lang/String;Z)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/text Ljava/lang/String;
aload 0
iload 2
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/isHtml Z
aload 0
aload 1
invokevirtual java/lang/String/length()I
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/length I
return
.limit locals 3
.limit stack 2
.end method

.method private static getCachedDirectionality(C)B
iload 0
sipush 1792
if_icmpge L0
getstatic android/support/v4/text/BidiFormatter$DirectionalityEstimator/DIR_TYPE_CACHE [B
iload 0
baload
ireturn
L0:
iload 0
invokestatic java/lang/Character/getDirectionality(C)B
ireturn
.limit locals 1
.limit stack 2
.end method

.method private skipEntityBackward()B
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
istore 1
L0:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
ifle L1
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/text Ljava/lang/String;
astore 3
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
iconst_1
isub
istore 2
aload 0
iload 2
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 0
aload 3
iload 2
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 38
if_icmpne L2
bipush 12
ireturn
L2:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 59
if_icmpne L0
L1:
aload 0
iload 1
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 0
bipush 59
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 13
ireturn
.limit locals 4
.limit stack 3
.end method

.method private skipEntityForward()B
L0:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/length I
if_icmpge L1
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/text Ljava/lang/String;
astore 3
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
istore 2
aload 0
iload 2
iconst_1
iadd
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 3
iload 2
invokevirtual java/lang/String/charAt(I)C
istore 1
aload 0
iload 1
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
iload 1
bipush 59
if_icmpne L0
L1:
bipush 12
ireturn
.limit locals 4
.limit stack 3
.end method

.method private skipTagBackward()B
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
istore 2
L0:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
ifle L1
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/text Ljava/lang/String;
astore 5
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
iconst_1
isub
istore 3
aload 0
iload 3
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 0
aload 5
iload 3
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 60
if_icmpne L2
bipush 12
ireturn
L2:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 62
if_icmpne L3
L1:
aload 0
iload 2
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 0
bipush 62
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 13
ireturn
L3:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 34
if_icmpeq L4
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 39
if_icmpne L0
L4:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
istore 3
L5:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
ifle L0
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/text Ljava/lang/String;
astore 5
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
iconst_1
isub
istore 4
aload 0
iload 4
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 5
iload 4
invokevirtual java/lang/String/charAt(I)C
istore 1
aload 0
iload 1
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
iload 1
iload 3
if_icmpeq L0
goto L5
.limit locals 6
.limit stack 3
.end method

.method private skipTagForward()B
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
istore 2
L0:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/length I
if_icmpge L1
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/text Ljava/lang/String;
astore 5
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
istore 3
aload 0
iload 3
iconst_1
iadd
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 0
aload 5
iload 3
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 62
if_icmpne L2
bipush 12
ireturn
L2:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 34
if_icmpeq L3
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 39
if_icmpne L0
L3:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
istore 3
L4:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/length I
if_icmpge L0
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/text Ljava/lang/String;
astore 5
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
istore 4
aload 0
iload 4
iconst_1
iadd
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 5
iload 4
invokevirtual java/lang/String/charAt(I)C
istore 1
aload 0
iload 1
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
iload 1
iload 3
if_icmpeq L0
goto L4
L1:
aload 0
iload 2
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 0
bipush 60
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 13
ireturn
.limit locals 6
.limit stack 3
.end method

.method dirTypeBackward()B
aload 0
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/text Ljava/lang/String;
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
iconst_1
isub
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
invokestatic java/lang/Character/isLowSurrogate(C)Z
ifeq L0
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/text Ljava/lang/String;
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
invokestatic java/lang/Character/codePointBefore(Ljava/lang/CharSequence;I)I
istore 3
aload 0
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
iload 3
invokestatic java/lang/Character/charCount(I)I
isub
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
iload 3
invokestatic java/lang/Character/getDirectionality(I)B
istore 1
L1:
iload 1
ireturn
L0:
aload 0
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
iconst_1
isub
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
invokestatic android/support/v4/text/BidiFormatter$DirectionalityEstimator/getCachedDirectionality(C)B
istore 2
iload 2
istore 1
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/isHtml Z
ifeq L1
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 62
if_icmpne L2
aload 0
invokespecial android/support/v4/text/BidiFormatter$DirectionalityEstimator/skipTagBackward()B
ireturn
L2:
iload 2
istore 1
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 59
if_icmpne L1
aload 0
invokespecial android/support/v4/text/BidiFormatter$DirectionalityEstimator/skipEntityBackward()B
ireturn
.limit locals 4
.limit stack 4
.end method

.method dirTypeForward()B
aload 0
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/text Ljava/lang/String;
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
invokestatic java/lang/Character/isHighSurrogate(C)Z
ifeq L0
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/text Ljava/lang/String;
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
invokestatic java/lang/Character/codePointAt(Ljava/lang/CharSequence;I)I
istore 3
aload 0
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
iload 3
invokestatic java/lang/Character/charCount(I)I
iadd
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
iload 3
invokestatic java/lang/Character/getDirectionality(I)B
istore 1
L1:
iload 1
ireturn
L0:
aload 0
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
iconst_1
iadd
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
invokestatic android/support/v4/text/BidiFormatter$DirectionalityEstimator/getCachedDirectionality(C)B
istore 2
iload 2
istore 1
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/isHtml Z
ifeq L1
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 60
if_icmpne L2
aload 0
invokespecial android/support/v4/text/BidiFormatter$DirectionalityEstimator/skipTagForward()B
ireturn
L2:
iload 2
istore 1
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/lastChar C
bipush 38
if_icmpne L1
aload 0
invokespecial android/support/v4/text/BidiFormatter$DirectionalityEstimator/skipEntityForward()B
ireturn
.limit locals 4
.limit stack 3
.end method

.method getEntryDir()I
aload 0
iconst_0
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
iconst_0
istore 1
iconst_0
istore 2
iconst_0
istore 3
L0:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/length I
if_icmpge L1
iload 3
ifne L1
aload 0
invokevirtual android/support/v4/text/BidiFormatter$DirectionalityEstimator/dirTypeForward()B
tableswitch 0
L2
L3
L3
L4
L4
L4
L4
L4
L4
L0
L4
L4
L4
L4
L5
L5
L6
L6
L7
default : L4
L4:
iload 1
istore 3
goto L0
L5:
iload 1
iconst_1
iadd
istore 1
iconst_m1
istore 2
goto L0
L6:
iload 1
iconst_1
iadd
istore 1
iconst_1
istore 2
goto L0
L7:
iload 1
iconst_1
isub
istore 1
iconst_0
istore 2
goto L0
L2:
iload 1
ifne L8
iconst_m1
istore 4
L9:
iload 4
ireturn
L8:
iload 1
istore 3
goto L0
L3:
iload 1
ifne L10
iconst_1
ireturn
L10:
iload 1
istore 3
goto L0
L1:
iload 3
ifne L11
iconst_0
ireturn
L11:
iload 2
istore 4
iload 2
ifne L9
L12:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
ifle L13
aload 0
invokevirtual android/support/v4/text/BidiFormatter$DirectionalityEstimator/dirTypeBackward()B
tableswitch 14
L14
L14
L15
L15
L16
default : L17
L17:
goto L12
L14:
iload 3
iload 1
if_icmpne L18
iconst_m1
ireturn
L18:
iload 1
iconst_1
isub
istore 1
goto L12
L15:
iload 3
iload 1
if_icmpne L19
iconst_1
ireturn
L19:
iload 1
iconst_1
isub
istore 1
goto L12
L16:
iload 1
iconst_1
iadd
istore 1
goto L12
L13:
iconst_0
ireturn
.limit locals 5
.limit stack 2
.end method

.method getExitDir()I
aload 0
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/length I
putfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
iconst_0
istore 1
iconst_0
istore 2
L0:
aload 0
getfield android/support/v4/text/BidiFormatter$DirectionalityEstimator/charIndex I
ifle L1
aload 0
invokevirtual android/support/v4/text/BidiFormatter$DirectionalityEstimator/dirTypeBackward()B
tableswitch 0
L2
L3
L3
L4
L4
L4
L4
L4
L4
L0
L4
L4
L4
L4
L5
L5
L6
L6
L7
default : L4
L4:
iload 2
ifne L0
iload 1
istore 2
goto L0
L2:
iload 1
ifne L8
L9:
iconst_m1
ireturn
L8:
iload 2
ifne L0
iload 1
istore 2
goto L0
L5:
iload 2
iload 1
if_icmpeq L9
iload 1
iconst_1
isub
istore 1
goto L0
L3:
iload 1
ifne L10
iconst_1
ireturn
L10:
iload 2
ifne L0
iload 1
istore 2
goto L0
L6:
iload 2
iload 1
if_icmpne L11
iconst_1
ireturn
L11:
iload 1
iconst_1
isub
istore 1
goto L0
L7:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method
