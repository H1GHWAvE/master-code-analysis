.bytecode 50.0
.class public synchronized android/support/v4/text/ICUCompat
.super java/lang/Object
.inner class static abstract interface ICUCompatImpl inner android/support/v4/text/ICUCompat$ICUCompatImpl outer android/support/v4/text/ICUCompat
.inner class static ICUCompatImplBase inner android/support/v4/text/ICUCompat$ICUCompatImplBase outer android/support/v4/text/ICUCompat
.inner class static ICUCompatImplIcs inner android/support/v4/text/ICUCompat$ICUCompatImplIcs outer android/support/v4/text/ICUCompat

.field private static final 'IMPL' Landroid/support/v4/text/ICUCompat$ICUCompatImpl;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L0
new android/support/v4/text/ICUCompat$ICUCompatImplIcs
dup
invokespecial android/support/v4/text/ICUCompat$ICUCompatImplIcs/<init>()V
putstatic android/support/v4/text/ICUCompat/IMPL Landroid/support/v4/text/ICUCompat$ICUCompatImpl;
return
L0:
new android/support/v4/text/ICUCompat$ICUCompatImplBase
dup
invokespecial android/support/v4/text/ICUCompat$ICUCompatImplBase/<init>()V
putstatic android/support/v4/text/ICUCompat/IMPL Landroid/support/v4/text/ICUCompat$ICUCompatImpl;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static addLikelySubtags(Ljava/lang/String;)Ljava/lang/String;
getstatic android/support/v4/text/ICUCompat/IMPL Landroid/support/v4/text/ICUCompat$ICUCompatImpl;
aload 0
invokeinterface android/support/v4/text/ICUCompat$ICUCompatImpl/addLikelySubtags(Ljava/lang/String;)Ljava/lang/String; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method public static getScript(Ljava/lang/String;)Ljava/lang/String;
getstatic android/support/v4/text/ICUCompat/IMPL Landroid/support/v4/text/ICUCompat$ICUCompatImpl;
aload 0
invokeinterface android/support/v4/text/ICUCompat$ICUCompatImpl/getScript(Ljava/lang/String;)Ljava/lang/String; 1
areturn
.limit locals 1
.limit stack 2
.end method
