.bytecode 50.0
.class public final synchronized android/support/v4/text/BidiFormatter$Builder
.super java/lang/Object
.inner class public static final Builder inner android/support/v4/text/BidiFormatter$Builder outer android/support/v4/text/BidiFormatter

.field private 'mFlags' I

.field private 'mIsRtlContext' Z

.field private 'mTextDirectionHeuristicCompat' Landroid/support/v4/text/TextDirectionHeuristicCompat;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
invokestatic java/util/Locale/getDefault()Ljava/util/Locale;
invokestatic android/support/v4/text/BidiFormatter/access$000(Ljava/util/Locale;)Z
invokespecial android/support/v4/text/BidiFormatter$Builder/initialize(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Ljava/util/Locale;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic android/support/v4/text/BidiFormatter/access$000(Ljava/util/Locale;)Z
invokespecial android/support/v4/text/BidiFormatter$Builder/initialize(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Z)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
invokespecial android/support/v4/text/BidiFormatter$Builder/initialize(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method private static getDefaultInstanceFromContext(Z)Landroid/support/v4/text/BidiFormatter;
iload 0
ifeq L0
invokestatic android/support/v4/text/BidiFormatter/access$200()Landroid/support/v4/text/BidiFormatter;
areturn
L0:
invokestatic android/support/v4/text/BidiFormatter/access$300()Landroid/support/v4/text/BidiFormatter;
areturn
.limit locals 1
.limit stack 1
.end method

.method private initialize(Z)V
aload 0
iload 1
putfield android/support/v4/text/BidiFormatter$Builder/mIsRtlContext Z
aload 0
invokestatic android/support/v4/text/BidiFormatter/access$100()Landroid/support/v4/text/TextDirectionHeuristicCompat;
putfield android/support/v4/text/BidiFormatter$Builder/mTextDirectionHeuristicCompat Landroid/support/v4/text/TextDirectionHeuristicCompat;
aload 0
iconst_2
putfield android/support/v4/text/BidiFormatter$Builder/mFlags I
return
.limit locals 2
.limit stack 2
.end method

.method public build()Landroid/support/v4/text/BidiFormatter;
aload 0
getfield android/support/v4/text/BidiFormatter$Builder/mFlags I
iconst_2
if_icmpne L0
aload 0
getfield android/support/v4/text/BidiFormatter$Builder/mTextDirectionHeuristicCompat Landroid/support/v4/text/TextDirectionHeuristicCompat;
invokestatic android/support/v4/text/BidiFormatter/access$100()Landroid/support/v4/text/TextDirectionHeuristicCompat;
if_acmpne L0
aload 0
getfield android/support/v4/text/BidiFormatter$Builder/mIsRtlContext Z
invokestatic android/support/v4/text/BidiFormatter$Builder/getDefaultInstanceFromContext(Z)Landroid/support/v4/text/BidiFormatter;
areturn
L0:
new android/support/v4/text/BidiFormatter
dup
aload 0
getfield android/support/v4/text/BidiFormatter$Builder/mIsRtlContext Z
aload 0
getfield android/support/v4/text/BidiFormatter$Builder/mFlags I
aload 0
getfield android/support/v4/text/BidiFormatter$Builder/mTextDirectionHeuristicCompat Landroid/support/v4/text/TextDirectionHeuristicCompat;
aconst_null
invokespecial android/support/v4/text/BidiFormatter/<init>(ZILandroid/support/v4/text/TextDirectionHeuristicCompat;Landroid/support/v4/text/BidiFormatter$1;)V
areturn
.limit locals 1
.limit stack 6
.end method

.method public setTextDirectionHeuristic(Landroid/support/v4/text/TextDirectionHeuristicCompat;)Landroid/support/v4/text/BidiFormatter$Builder;
aload 0
aload 1
putfield android/support/v4/text/BidiFormatter$Builder/mTextDirectionHeuristicCompat Landroid/support/v4/text/TextDirectionHeuristicCompat;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public stereoReset(Z)Landroid/support/v4/text/BidiFormatter$Builder;
iload 1
ifeq L0
aload 0
aload 0
getfield android/support/v4/text/BidiFormatter$Builder/mFlags I
iconst_2
ior
putfield android/support/v4/text/BidiFormatter$Builder/mFlags I
aload 0
areturn
L0:
aload 0
aload 0
getfield android/support/v4/text/BidiFormatter$Builder/mFlags I
bipush -3
iand
putfield android/support/v4/text/BidiFormatter$Builder/mFlags I
aload 0
areturn
.limit locals 2
.limit stack 3
.end method
