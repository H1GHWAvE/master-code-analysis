.bytecode 50.0
.class public final synchronized android/support/v4/text/BidiFormatter
.super java/lang/Object
.inner class static synthetic inner android/support/v4/text/BidiFormatter$1
.inner class public static final Builder inner android/support/v4/text/BidiFormatter$Builder outer android/support/v4/text/BidiFormatter
.inner class private static DirectionalityEstimator inner android/support/v4/text/BidiFormatter$DirectionalityEstimator outer android/support/v4/text/BidiFormatter

.field private static final 'DEFAULT_FLAGS' I = 2


.field private static final 'DEFAULT_LTR_INSTANCE' Landroid/support/v4/text/BidiFormatter;

.field private static final 'DEFAULT_RTL_INSTANCE' Landroid/support/v4/text/BidiFormatter;

.field private static 'DEFAULT_TEXT_DIRECTION_HEURISTIC' Landroid/support/v4/text/TextDirectionHeuristicCompat;

.field private static final 'DIR_LTR' I = -1


.field private static final 'DIR_RTL' I = 1


.field private static final 'DIR_UNKNOWN' I = 0


.field private static final 'EMPTY_STRING' Ljava/lang/String; = ""

.field private static final 'FLAG_STEREO_RESET' I = 2


.field private static final 'LRE' C = 8234


.field private static final 'LRM' C = 8206


.field private static final 'LRM_STRING' Ljava/lang/String;

.field private static final 'PDF' C = 8236


.field private static final 'RLE' C = 8235


.field private static final 'RLM' C = 8207


.field private static final 'RLM_STRING' Ljava/lang/String;

.field private final 'mDefaultTextDirectionHeuristicCompat' Landroid/support/v4/text/TextDirectionHeuristicCompat;

.field private final 'mFlags' I

.field private final 'mIsRtlContext' Z

.method static <clinit>()V
getstatic android/support/v4/text/TextDirectionHeuristicsCompat/FIRSTSTRONG_LTR Landroid/support/v4/text/TextDirectionHeuristicCompat;
putstatic android/support/v4/text/BidiFormatter/DEFAULT_TEXT_DIRECTION_HEURISTIC Landroid/support/v4/text/TextDirectionHeuristicCompat;
sipush 8206
invokestatic java/lang/Character/toString(C)Ljava/lang/String;
putstatic android/support/v4/text/BidiFormatter/LRM_STRING Ljava/lang/String;
sipush 8207
invokestatic java/lang/Character/toString(C)Ljava/lang/String;
putstatic android/support/v4/text/BidiFormatter/RLM_STRING Ljava/lang/String;
new android/support/v4/text/BidiFormatter
dup
iconst_0
iconst_2
getstatic android/support/v4/text/BidiFormatter/DEFAULT_TEXT_DIRECTION_HEURISTIC Landroid/support/v4/text/TextDirectionHeuristicCompat;
invokespecial android/support/v4/text/BidiFormatter/<init>(ZILandroid/support/v4/text/TextDirectionHeuristicCompat;)V
putstatic android/support/v4/text/BidiFormatter/DEFAULT_LTR_INSTANCE Landroid/support/v4/text/BidiFormatter;
new android/support/v4/text/BidiFormatter
dup
iconst_1
iconst_2
getstatic android/support/v4/text/BidiFormatter/DEFAULT_TEXT_DIRECTION_HEURISTIC Landroid/support/v4/text/TextDirectionHeuristicCompat;
invokespecial android/support/v4/text/BidiFormatter/<init>(ZILandroid/support/v4/text/TextDirectionHeuristicCompat;)V
putstatic android/support/v4/text/BidiFormatter/DEFAULT_RTL_INSTANCE Landroid/support/v4/text/BidiFormatter;
return
.limit locals 0
.limit stack 5
.end method

.method private <init>(ZILandroid/support/v4/text/TextDirectionHeuristicCompat;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield android/support/v4/text/BidiFormatter/mIsRtlContext Z
aload 0
iload 2
putfield android/support/v4/text/BidiFormatter/mFlags I
aload 0
aload 3
putfield android/support/v4/text/BidiFormatter/mDefaultTextDirectionHeuristicCompat Landroid/support/v4/text/TextDirectionHeuristicCompat;
return
.limit locals 4
.limit stack 2
.end method

.method synthetic <init>(ZILandroid/support/v4/text/TextDirectionHeuristicCompat;Landroid/support/v4/text/BidiFormatter$1;)V
aload 0
iload 1
iload 2
aload 3
invokespecial android/support/v4/text/BidiFormatter/<init>(ZILandroid/support/v4/text/TextDirectionHeuristicCompat;)V
return
.limit locals 5
.limit stack 4
.end method

.method static synthetic access$000(Ljava/util/Locale;)Z
aload 0
invokestatic android/support/v4/text/BidiFormatter/isRtlLocale(Ljava/util/Locale;)Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$100()Landroid/support/v4/text/TextDirectionHeuristicCompat;
getstatic android/support/v4/text/BidiFormatter/DEFAULT_TEXT_DIRECTION_HEURISTIC Landroid/support/v4/text/TextDirectionHeuristicCompat;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic access$200()Landroid/support/v4/text/BidiFormatter;
getstatic android/support/v4/text/BidiFormatter/DEFAULT_RTL_INSTANCE Landroid/support/v4/text/BidiFormatter;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic access$300()Landroid/support/v4/text/BidiFormatter;
getstatic android/support/v4/text/BidiFormatter/DEFAULT_LTR_INSTANCE Landroid/support/v4/text/BidiFormatter;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static getEntryDir(Ljava/lang/String;)I
new android/support/v4/text/BidiFormatter$DirectionalityEstimator
dup
aload 0
iconst_0
invokespecial android/support/v4/text/BidiFormatter$DirectionalityEstimator/<init>(Ljava/lang/String;Z)V
invokevirtual android/support/v4/text/BidiFormatter$DirectionalityEstimator/getEntryDir()I
ireturn
.limit locals 1
.limit stack 4
.end method

.method private static getExitDir(Ljava/lang/String;)I
new android/support/v4/text/BidiFormatter$DirectionalityEstimator
dup
aload 0
iconst_0
invokespecial android/support/v4/text/BidiFormatter$DirectionalityEstimator/<init>(Ljava/lang/String;Z)V
invokevirtual android/support/v4/text/BidiFormatter$DirectionalityEstimator/getExitDir()I
ireturn
.limit locals 1
.limit stack 4
.end method

.method public static getInstance()Landroid/support/v4/text/BidiFormatter;
new android/support/v4/text/BidiFormatter$Builder
dup
invokespecial android/support/v4/text/BidiFormatter$Builder/<init>()V
invokevirtual android/support/v4/text/BidiFormatter$Builder/build()Landroid/support/v4/text/BidiFormatter;
areturn
.limit locals 0
.limit stack 2
.end method

.method public static getInstance(Ljava/util/Locale;)Landroid/support/v4/text/BidiFormatter;
new android/support/v4/text/BidiFormatter$Builder
dup
aload 0
invokespecial android/support/v4/text/BidiFormatter$Builder/<init>(Ljava/util/Locale;)V
invokevirtual android/support/v4/text/BidiFormatter$Builder/build()Landroid/support/v4/text/BidiFormatter;
areturn
.limit locals 1
.limit stack 3
.end method

.method public static getInstance(Z)Landroid/support/v4/text/BidiFormatter;
new android/support/v4/text/BidiFormatter$Builder
dup
iload 0
invokespecial android/support/v4/text/BidiFormatter$Builder/<init>(Z)V
invokevirtual android/support/v4/text/BidiFormatter$Builder/build()Landroid/support/v4/text/BidiFormatter;
areturn
.limit locals 1
.limit stack 3
.end method

.method private static isRtlLocale(Ljava/util/Locale;)Z
aload 0
invokestatic android/support/v4/text/TextUtilsCompat/getLayoutDirectionFromLocale(Ljava/util/Locale;)I
iconst_1
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private markAfter(Ljava/lang/String;Landroid/support/v4/text/TextDirectionHeuristicCompat;)Ljava/lang/String;
aload 2
aload 1
iconst_0
aload 1
invokevirtual java/lang/String/length()I
invokeinterface android/support/v4/text/TextDirectionHeuristicCompat/isRtl(Ljava/lang/CharSequence;II)Z 3
istore 3
aload 0
getfield android/support/v4/text/BidiFormatter/mIsRtlContext Z
ifne L0
iload 3
ifne L1
aload 1
invokestatic android/support/v4/text/BidiFormatter/getExitDir(Ljava/lang/String;)I
iconst_1
if_icmpne L0
L1:
getstatic android/support/v4/text/BidiFormatter/LRM_STRING Ljava/lang/String;
areturn
L0:
aload 0
getfield android/support/v4/text/BidiFormatter/mIsRtlContext Z
ifeq L2
iload 3
ifeq L3
aload 1
invokestatic android/support/v4/text/BidiFormatter/getExitDir(Ljava/lang/String;)I
iconst_m1
if_icmpne L2
L3:
getstatic android/support/v4/text/BidiFormatter/RLM_STRING Ljava/lang/String;
areturn
L2:
ldc ""
areturn
.limit locals 4
.limit stack 4
.end method

.method private markBefore(Ljava/lang/String;Landroid/support/v4/text/TextDirectionHeuristicCompat;)Ljava/lang/String;
aload 2
aload 1
iconst_0
aload 1
invokevirtual java/lang/String/length()I
invokeinterface android/support/v4/text/TextDirectionHeuristicCompat/isRtl(Ljava/lang/CharSequence;II)Z 3
istore 3
aload 0
getfield android/support/v4/text/BidiFormatter/mIsRtlContext Z
ifne L0
iload 3
ifne L1
aload 1
invokestatic android/support/v4/text/BidiFormatter/getEntryDir(Ljava/lang/String;)I
iconst_1
if_icmpne L0
L1:
getstatic android/support/v4/text/BidiFormatter/LRM_STRING Ljava/lang/String;
areturn
L0:
aload 0
getfield android/support/v4/text/BidiFormatter/mIsRtlContext Z
ifeq L2
iload 3
ifeq L3
aload 1
invokestatic android/support/v4/text/BidiFormatter/getEntryDir(Ljava/lang/String;)I
iconst_m1
if_icmpne L2
L3:
getstatic android/support/v4/text/BidiFormatter/RLM_STRING Ljava/lang/String;
areturn
L2:
ldc ""
areturn
.limit locals 4
.limit stack 4
.end method

.method public getStereoReset()Z
aload 0
getfield android/support/v4/text/BidiFormatter/mFlags I
iconst_2
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isRtl(Ljava/lang/String;)Z
aload 0
getfield android/support/v4/text/BidiFormatter/mDefaultTextDirectionHeuristicCompat Landroid/support/v4/text/TextDirectionHeuristicCompat;
aload 1
iconst_0
aload 1
invokevirtual java/lang/String/length()I
invokeinterface android/support/v4/text/TextDirectionHeuristicCompat/isRtl(Ljava/lang/CharSequence;II)Z 3
ireturn
.limit locals 2
.limit stack 4
.end method

.method public isRtlContext()Z
aload 0
getfield android/support/v4/text/BidiFormatter/mIsRtlContext Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public unicodeWrap(Ljava/lang/String;)Ljava/lang/String;
aload 0
aload 1
aload 0
getfield android/support/v4/text/BidiFormatter/mDefaultTextDirectionHeuristicCompat Landroid/support/v4/text/TextDirectionHeuristicCompat;
iconst_1
invokevirtual android/support/v4/text/BidiFormatter/unicodeWrap(Ljava/lang/String;Landroid/support/v4/text/TextDirectionHeuristicCompat;Z)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method

.method public unicodeWrap(Ljava/lang/String;Landroid/support/v4/text/TextDirectionHeuristicCompat;)Ljava/lang/String;
aload 0
aload 1
aload 2
iconst_1
invokevirtual android/support/v4/text/BidiFormatter/unicodeWrap(Ljava/lang/String;Landroid/support/v4/text/TextDirectionHeuristicCompat;Z)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method

.method public unicodeWrap(Ljava/lang/String;Landroid/support/v4/text/TextDirectionHeuristicCompat;Z)Ljava/lang/String;
aload 2
aload 1
iconst_0
aload 1
invokevirtual java/lang/String/length()I
invokeinterface android/support/v4/text/TextDirectionHeuristicCompat/isRtl(Ljava/lang/CharSequence;II)Z 3
istore 5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 6
aload 0
invokevirtual android/support/v4/text/BidiFormatter/getStereoReset()Z
ifeq L0
iload 3
ifeq L0
iload 5
ifeq L1
getstatic android/support/v4/text/TextDirectionHeuristicsCompat/RTL Landroid/support/v4/text/TextDirectionHeuristicCompat;
astore 2
L2:
aload 6
aload 0
aload 1
aload 2
invokespecial android/support/v4/text/BidiFormatter/markBefore(Ljava/lang/String;Landroid/support/v4/text/TextDirectionHeuristicCompat;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L0:
iload 5
aload 0
getfield android/support/v4/text/BidiFormatter/mIsRtlContext Z
if_icmpeq L3
iload 5
ifeq L4
sipush 8235
istore 4
L5:
aload 6
iload 4
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 6
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 6
sipush 8236
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L6:
iload 3
ifeq L7
iload 5
ifeq L8
getstatic android/support/v4/text/TextDirectionHeuristicsCompat/RTL Landroid/support/v4/text/TextDirectionHeuristicCompat;
astore 2
L9:
aload 6
aload 0
aload 1
aload 2
invokespecial android/support/v4/text/BidiFormatter/markAfter(Ljava/lang/String;Landroid/support/v4/text/TextDirectionHeuristicCompat;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L7:
aload 6
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L1:
getstatic android/support/v4/text/TextDirectionHeuristicsCompat/LTR Landroid/support/v4/text/TextDirectionHeuristicCompat;
astore 2
goto L2
L4:
sipush 8234
istore 4
goto L5
L3:
aload 6
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L6
L8:
getstatic android/support/v4/text/TextDirectionHeuristicsCompat/LTR Landroid/support/v4/text/TextDirectionHeuristicCompat;
astore 2
goto L9
.limit locals 7
.limit stack 4
.end method

.method public unicodeWrap(Ljava/lang/String;Z)Ljava/lang/String;
aload 0
aload 1
aload 0
getfield android/support/v4/text/BidiFormatter/mDefaultTextDirectionHeuristicCompat Landroid/support/v4/text/TextDirectionHeuristicCompat;
iload 2
invokevirtual android/support/v4/text/BidiFormatter/unicodeWrap(Ljava/lang/String;Landroid/support/v4/text/TextDirectionHeuristicCompat;Z)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method
