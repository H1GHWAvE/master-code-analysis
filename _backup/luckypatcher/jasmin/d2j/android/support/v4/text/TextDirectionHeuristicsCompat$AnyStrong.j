.bytecode 50.0
.class synchronized android/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong
.super java/lang/Object
.implements android/support/v4/text/TextDirectionHeuristicsCompat$TextDirectionAlgorithm
.inner class private static AnyStrong inner android/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong outer android/support/v4/text/TextDirectionHeuristicsCompat

.field public static final 'INSTANCE_LTR' Landroid/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong;

.field public static final 'INSTANCE_RTL' Landroid/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong;

.field private final 'mLookForRtl' Z

.method static <clinit>()V
new android/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong
dup
iconst_1
invokespecial android/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong/<init>(Z)V
putstatic android/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong/INSTANCE_RTL Landroid/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong;
new android/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong
dup
iconst_0
invokespecial android/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong/<init>(Z)V
putstatic android/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong/INSTANCE_LTR Landroid/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong;
return
.limit locals 0
.limit stack 3
.end method

.method private <init>(Z)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield android/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong/mLookForRtl Z
return
.limit locals 2
.limit stack 2
.end method

.method public checkRtl(Ljava/lang/CharSequence;II)I
iconst_1
istore 6
iconst_0
istore 4
iload 2
istore 5
L0:
iload 5
iload 2
iload 3
iadd
if_icmpge L1
aload 1
iload 5
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokestatic java/lang/Character/getDirectionality(C)B
invokestatic android/support/v4/text/TextDirectionHeuristicsCompat/access$200(I)I
tableswitch 0
L2
L3
default : L4
L4:
iload 5
iconst_1
iadd
istore 5
goto L0
L2:
aload 0
getfield android/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong/mLookForRtl Z
ifeq L5
iconst_0
istore 4
L6:
iload 4
ireturn
L5:
iconst_1
istore 4
goto L4
L3:
iload 6
istore 4
aload 0
getfield android/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong/mLookForRtl Z
ifeq L6
iconst_1
istore 4
goto L4
L1:
iload 4
ifeq L7
iload 6
istore 4
aload 0
getfield android/support/v4/text/TextDirectionHeuristicsCompat$AnyStrong/mLookForRtl Z
ifne L6
iconst_0
ireturn
L7:
iconst_2
ireturn
.limit locals 7
.limit stack 3
.end method
