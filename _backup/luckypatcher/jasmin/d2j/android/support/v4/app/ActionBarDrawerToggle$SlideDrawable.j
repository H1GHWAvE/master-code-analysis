.bytecode 50.0
.class synchronized android/support/v4/app/ActionBarDrawerToggle$SlideDrawable
.super android/graphics/drawable/Drawable
.implements android/graphics/drawable/Drawable$Callback
.inner class private static SlideDrawable inner android/support/v4/app/ActionBarDrawerToggle$SlideDrawable outer android/support/v4/app/ActionBarDrawerToggle

.field private 'mOffset' F

.field private 'mOffsetBy' F

.field private final 'mTmpRect' Landroid/graphics/Rect;

.field private 'mWrapped' Landroid/graphics/drawable/Drawable;

.method public <init>(Landroid/graphics/drawable/Drawable;)V
aload 0
invokespecial android/graphics/drawable/Drawable/<init>()V
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mTmpRect Landroid/graphics/Rect;
aload 0
aload 1
putfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
return
.limit locals 2
.limit stack 3
.end method

.method public clearColorFilter()V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/clearColorFilter()V
return
.limit locals 1
.limit stack 1
.end method

.method public draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mTmpRect Landroid/graphics/Rect;
invokevirtual android/graphics/drawable/Drawable/copyBounds(Landroid/graphics/Rect;)V
aload 1
invokevirtual android/graphics/Canvas/save()I
pop
aload 1
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mOffsetBy F
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mTmpRect Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/width()I
i2f
fmul
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mOffset F
fneg
fmul
fconst_0
invokevirtual android/graphics/Canvas/translate(FF)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
aload 1
invokevirtual android/graphics/Canvas/restore()V
return
.limit locals 2
.limit stack 3
.end method

.method public getChangingConfigurations()I
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getChangingConfigurations()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
aload 0
invokespecial android/graphics/drawable/Drawable/getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getCurrent()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getCurrent()Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getIntrinsicHeight()I
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getIntrinsicWidth()I
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMinimumHeight()I
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getMinimumHeight()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMinimumWidth()I
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getMinimumWidth()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOffset()F
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mOffset F
freturn
.limit locals 1
.limit stack 1
.end method

.method public getOpacity()I
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getOpacity()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getPadding(Landroid/graphics/Rect;)Z
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getState()[I
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getState()[I
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTransparentRegion()Landroid/graphics/Region;
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getTransparentRegion()Landroid/graphics/Region;
areturn
.limit locals 1
.limit stack 1
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
if_acmpne L0
aload 0
invokevirtual android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public isStateful()Z
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/isStateful()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
aload 0
aload 1
invokespecial android/graphics/drawable/Drawable/onBoundsChange(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/setBounds(Landroid/graphics/Rect;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected onStateChange([I)Z
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
aload 0
aload 1
invokespecial android/graphics/drawable/Drawable/onStateChange([I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
aload 1
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
if_acmpne L0
aload 0
aload 2
lload 3
invokevirtual android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/scheduleSelf(Ljava/lang/Runnable;J)V
L0:
return
.limit locals 5
.limit stack 4
.end method

.method public setAlpha(I)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setAlpha(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public setChangingConfigurations(I)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setChangingConfigurations(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
iload 1
aload 2
invokevirtual android/graphics/drawable/Drawable/setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V
return
.limit locals 3
.limit stack 3
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/setColorFilter(Landroid/graphics/ColorFilter;)V
return
.limit locals 2
.limit stack 2
.end method

.method public setDither(Z)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setDither(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public setFilterBitmap(Z)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setFilterBitmap(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public setOffset(F)V
aload 0
fload 1
putfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mOffset F
aload 0
invokevirtual android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method

.method public setOffsetBy(F)V
aload 0
fload 1
putfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mOffsetBy F
aload 0
invokevirtual android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method

.method public setState([I)Z
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public setVisible(ZZ)Z
aload 0
iload 1
iload 2
invokespecial android/graphics/drawable/Drawable/setVisible(ZZ)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
aload 1
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/mWrapped Landroid/graphics/drawable/Drawable;
if_acmpne L0
aload 0
aload 2
invokevirtual android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/unscheduleSelf(Ljava/lang/Runnable;)V
L0:
return
.limit locals 3
.limit stack 2
.end method
