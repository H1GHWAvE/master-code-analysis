.bytecode 50.0
.class public synchronized android/support/v4/app/ActivityOptionsCompat
.super java/lang/Object
.inner class private static ActivityOptionsImplJB inner android/support/v4/app/ActivityOptionsCompat$ActivityOptionsImplJB outer android/support/v4/app/ActivityOptionsCompat

.method protected <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static makeCustomAnimation(Landroid/content/Context;II)Landroid/support/v4/app/ActivityOptionsCompat;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
new android/support/v4/app/ActivityOptionsCompat$ActivityOptionsImplJB
dup
aload 0
iload 1
iload 2
invokestatic android/support/v4/app/ActivityOptionsCompatJB/makeCustomAnimation(Landroid/content/Context;II)Landroid/support/v4/app/ActivityOptionsCompatJB;
invokespecial android/support/v4/app/ActivityOptionsCompat$ActivityOptionsImplJB/<init>(Landroid/support/v4/app/ActivityOptionsCompatJB;)V
areturn
L0:
new android/support/v4/app/ActivityOptionsCompat
dup
invokespecial android/support/v4/app/ActivityOptionsCompat/<init>()V
areturn
.limit locals 3
.limit stack 5
.end method

.method public static makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/support/v4/app/ActivityOptionsCompat;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
new android/support/v4/app/ActivityOptionsCompat$ActivityOptionsImplJB
dup
aload 0
iload 1
iload 2
iload 3
iload 4
invokestatic android/support/v4/app/ActivityOptionsCompatJB/makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/support/v4/app/ActivityOptionsCompatJB;
invokespecial android/support/v4/app/ActivityOptionsCompat$ActivityOptionsImplJB/<init>(Landroid/support/v4/app/ActivityOptionsCompatJB;)V
areturn
L0:
new android/support/v4/app/ActivityOptionsCompat
dup
invokespecial android/support/v4/app/ActivityOptionsCompat/<init>()V
areturn
.limit locals 5
.limit stack 7
.end method

.method public static makeThumbnailScaleUpAnimation(Landroid/view/View;Landroid/graphics/Bitmap;II)Landroid/support/v4/app/ActivityOptionsCompat;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
new android/support/v4/app/ActivityOptionsCompat$ActivityOptionsImplJB
dup
aload 0
aload 1
iload 2
iload 3
invokestatic android/support/v4/app/ActivityOptionsCompatJB/makeThumbnailScaleUpAnimation(Landroid/view/View;Landroid/graphics/Bitmap;II)Landroid/support/v4/app/ActivityOptionsCompatJB;
invokespecial android/support/v4/app/ActivityOptionsCompat$ActivityOptionsImplJB/<init>(Landroid/support/v4/app/ActivityOptionsCompatJB;)V
areturn
L0:
new android/support/v4/app/ActivityOptionsCompat
dup
invokespecial android/support/v4/app/ActivityOptionsCompat/<init>()V
areturn
.limit locals 4
.limit stack 6
.end method

.method public toBundle()Landroid/os/Bundle;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public update(Landroid/support/v4/app/ActivityOptionsCompat;)V
return
.limit locals 2
.limit stack 0
.end method
