.bytecode 50.0
.class public synchronized android/support/v4/app/ActionBarDrawerToggle
.super java/lang/Object
.implements android/support/v4/widget/DrawerLayout$DrawerListener
.inner class static synthetic inner android/support/v4/app/ActionBarDrawerToggle$1
.inner class private static abstract interface ActionBarDrawerToggleImpl inner android/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImpl outer android/support/v4/app/ActionBarDrawerToggle
.inner class private static ActionBarDrawerToggleImplBase inner android/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImplBase outer android/support/v4/app/ActionBarDrawerToggle
.inner class private static ActionBarDrawerToggleImplHC inner android/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImplHC outer android/support/v4/app/ActionBarDrawerToggle
.inner class public static abstract interface Delegate inner android/support/v4/app/ActionBarDrawerToggle$Delegate outer android/support/v4/app/ActionBarDrawerToggle
.inner class public static abstract interface DelegateProvider inner android/support/v4/app/ActionBarDrawerToggle$DelegateProvider outer android/support/v4/app/ActionBarDrawerToggle
.inner class private static SlideDrawable inner android/support/v4/app/ActionBarDrawerToggle$SlideDrawable outer android/support/v4/app/ActionBarDrawerToggle

.field private static final 'ID_HOME' I = 16908332


.field private static final 'IMPL' Landroid/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;

.field private final 'mActivity' Landroid/app/Activity;

.field private final 'mActivityImpl' Landroid/support/v4/app/ActionBarDrawerToggle$Delegate;

.field private final 'mCloseDrawerContentDescRes' I

.field private 'mDrawerImage' Landroid/graphics/drawable/Drawable;

.field private final 'mDrawerImageResource' I

.field private 'mDrawerIndicatorEnabled' Z

.field private final 'mDrawerLayout' Landroid/support/v4/widget/DrawerLayout;

.field private final 'mOpenDrawerContentDescRes' I

.field private 'mSetIndicatorInfo' Ljava/lang/Object;

.field private 'mSlider' Landroid/support/v4/app/ActionBarDrawerToggle$SlideDrawable;

.field private 'mThemeImage' Landroid/graphics/drawable/Drawable;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
new android/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImplHC
dup
aconst_null
invokespecial android/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImplHC/<init>(Landroid/support/v4/app/ActionBarDrawerToggle$1;)V
putstatic android/support/v4/app/ActionBarDrawerToggle/IMPL Landroid/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;
return
L0:
new android/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImplBase
dup
aconst_null
invokespecial android/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImplBase/<init>(Landroid/support/v4/app/ActionBarDrawerToggle$1;)V
putstatic android/support/v4/app/ActionBarDrawerToggle/IMPL Landroid/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_1
putfield android/support/v4/app/ActionBarDrawerToggle/mDrawerIndicatorEnabled Z
aload 0
aload 1
putfield android/support/v4/app/ActionBarDrawerToggle/mActivity Landroid/app/Activity;
aload 0
aload 2
putfield android/support/v4/app/ActionBarDrawerToggle/mDrawerLayout Landroid/support/v4/widget/DrawerLayout;
aload 0
iload 3
putfield android/support/v4/app/ActionBarDrawerToggle/mDrawerImageResource I
aload 0
iload 4
putfield android/support/v4/app/ActionBarDrawerToggle/mOpenDrawerContentDescRes I
aload 0
iload 5
putfield android/support/v4/app/ActionBarDrawerToggle/mCloseDrawerContentDescRes I
aload 0
aload 0
invokevirtual android/support/v4/app/ActionBarDrawerToggle/getThemeUpIndicator()Landroid/graphics/drawable/Drawable;
putfield android/support/v4/app/ActionBarDrawerToggle/mThemeImage Landroid/graphics/drawable/Drawable;
aload 0
aload 1
invokevirtual android/app/Activity/getResources()Landroid/content/res/Resources;
iload 3
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield android/support/v4/app/ActionBarDrawerToggle/mDrawerImage Landroid/graphics/drawable/Drawable;
aload 0
new android/support/v4/app/ActionBarDrawerToggle$SlideDrawable
dup
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mDrawerImage Landroid/graphics/drawable/Drawable;
invokespecial android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/<init>(Landroid/graphics/drawable/Drawable;)V
putfield android/support/v4/app/ActionBarDrawerToggle/mSlider Landroid/support/v4/app/ActionBarDrawerToggle$SlideDrawable;
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mSlider Landroid/support/v4/app/ActionBarDrawerToggle$SlideDrawable;
ldc_w 0.33333334F
invokevirtual android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/setOffsetBy(F)V
aload 1
instanceof android/support/v4/app/ActionBarDrawerToggle$DelegateProvider
ifeq L0
aload 0
aload 1
checkcast android/support/v4/app/ActionBarDrawerToggle$DelegateProvider
invokeinterface android/support/v4/app/ActionBarDrawerToggle$DelegateProvider/getDrawerToggleDelegate()Landroid/support/v4/app/ActionBarDrawerToggle$Delegate; 0
putfield android/support/v4/app/ActionBarDrawerToggle/mActivityImpl Landroid/support/v4/app/ActionBarDrawerToggle$Delegate;
return
L0:
aload 0
aconst_null
putfield android/support/v4/app/ActionBarDrawerToggle/mActivityImpl Landroid/support/v4/app/ActionBarDrawerToggle$Delegate;
return
.limit locals 6
.limit stack 4
.end method

.method getThemeUpIndicator()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mActivityImpl Landroid/support/v4/app/ActionBarDrawerToggle$Delegate;
ifnull L0
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mActivityImpl Landroid/support/v4/app/ActionBarDrawerToggle$Delegate;
invokeinterface android/support/v4/app/ActionBarDrawerToggle$Delegate/getThemeUpIndicator()Landroid/graphics/drawable/Drawable; 0
areturn
L0:
getstatic android/support/v4/app/ActionBarDrawerToggle/IMPL Landroid/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mActivity Landroid/app/Activity;
invokeinterface android/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImpl/getThemeUpIndicator(Landroid/app/Activity;)Landroid/graphics/drawable/Drawable; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method public isDrawerIndicatorEnabled()Z
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mDrawerIndicatorEnabled Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
aload 0
aload 0
invokevirtual android/support/v4/app/ActionBarDrawerToggle/getThemeUpIndicator()Landroid/graphics/drawable/Drawable;
putfield android/support/v4/app/ActionBarDrawerToggle/mThemeImage Landroid/graphics/drawable/Drawable;
aload 0
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mActivity Landroid/app/Activity;
invokevirtual android/app/Activity/getResources()Landroid/content/res/Resources;
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mDrawerImageResource I
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield android/support/v4/app/ActionBarDrawerToggle/mDrawerImage Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/support/v4/app/ActionBarDrawerToggle/syncState()V
return
.limit locals 2
.limit stack 3
.end method

.method public onDrawerClosed(Landroid/view/View;)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mSlider Landroid/support/v4/app/ActionBarDrawerToggle$SlideDrawable;
fconst_0
invokevirtual android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/setOffset(F)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mDrawerIndicatorEnabled Z
ifeq L0
aload 0
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mCloseDrawerContentDescRes I
invokevirtual android/support/v4/app/ActionBarDrawerToggle/setActionBarDescription(I)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public onDrawerOpened(Landroid/view/View;)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mSlider Landroid/support/v4/app/ActionBarDrawerToggle$SlideDrawable;
fconst_1
invokevirtual android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/setOffset(F)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mDrawerIndicatorEnabled Z
ifeq L0
aload 0
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mOpenDrawerContentDescRes I
invokevirtual android/support/v4/app/ActionBarDrawerToggle/setActionBarDescription(I)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mSlider Landroid/support/v4/app/ActionBarDrawerToggle$SlideDrawable;
invokevirtual android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/getOffset()F
fstore 3
fload 2
ldc_w 0.5F
fcmpl
ifle L0
fload 3
fconst_0
fload 2
ldc_w 0.5F
fsub
invokestatic java/lang/Math/max(FF)F
fconst_2
fmul
invokestatic java/lang/Math/max(FF)F
fstore 2
L1:
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mSlider Landroid/support/v4/app/ActionBarDrawerToggle$SlideDrawable;
fload 2
invokevirtual android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/setOffset(F)V
return
L0:
fload 3
fload 2
fconst_2
fmul
invokestatic java/lang/Math/min(FF)F
fstore 2
goto L1
.limit locals 4
.limit stack 4
.end method

.method public onDrawerStateChanged(I)V
return
.limit locals 2
.limit stack 0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
aload 1
ifnull L0
aload 1
invokeinterface android/view/MenuItem/getItemId()I 0
ldc_w 16908332
if_icmpne L0
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mDrawerIndicatorEnabled Z
ifeq L0
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mDrawerLayout Landroid/support/v4/widget/DrawerLayout;
ldc_w 8388611
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerVisible(I)Z
ifeq L1
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mDrawerLayout Landroid/support/v4/widget/DrawerLayout;
ldc_w 8388611
invokevirtual android/support/v4/widget/DrawerLayout/closeDrawer(I)V
L2:
iconst_1
ireturn
L1:
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mDrawerLayout Landroid/support/v4/widget/DrawerLayout;
ldc_w 8388611
invokevirtual android/support/v4/widget/DrawerLayout/openDrawer(I)V
goto L2
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method setActionBarDescription(I)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mActivityImpl Landroid/support/v4/app/ActionBarDrawerToggle$Delegate;
ifnull L0
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mActivityImpl Landroid/support/v4/app/ActionBarDrawerToggle$Delegate;
iload 1
invokeinterface android/support/v4/app/ActionBarDrawerToggle$Delegate/setActionBarDescription(I)V 1
return
L0:
aload 0
getstatic android/support/v4/app/ActionBarDrawerToggle/IMPL Landroid/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mSetIndicatorInfo Ljava/lang/Object;
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mActivity Landroid/app/Activity;
iload 1
invokeinterface android/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImpl/setActionBarDescription(Ljava/lang/Object;Landroid/app/Activity;I)Ljava/lang/Object; 3
putfield android/support/v4/app/ActionBarDrawerToggle/mSetIndicatorInfo Ljava/lang/Object;
return
.limit locals 2
.limit stack 5
.end method

.method setActionBarUpIndicator(Landroid/graphics/drawable/Drawable;I)V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mActivityImpl Landroid/support/v4/app/ActionBarDrawerToggle$Delegate;
ifnull L0
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mActivityImpl Landroid/support/v4/app/ActionBarDrawerToggle$Delegate;
aload 1
iload 2
invokeinterface android/support/v4/app/ActionBarDrawerToggle$Delegate/setActionBarUpIndicator(Landroid/graphics/drawable/Drawable;I)V 2
return
L0:
aload 0
getstatic android/support/v4/app/ActionBarDrawerToggle/IMPL Landroid/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mSetIndicatorInfo Ljava/lang/Object;
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mActivity Landroid/app/Activity;
aload 1
iload 2
invokeinterface android/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImpl/setActionBarUpIndicator(Ljava/lang/Object;Landroid/app/Activity;Landroid/graphics/drawable/Drawable;I)Ljava/lang/Object; 4
putfield android/support/v4/app/ActionBarDrawerToggle/mSetIndicatorInfo Ljava/lang/Object;
return
.limit locals 3
.limit stack 6
.end method

.method public setDrawerIndicatorEnabled(Z)V
iload 1
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mDrawerIndicatorEnabled Z
if_icmpeq L0
iload 1
ifeq L1
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mSlider Landroid/support/v4/app/ActionBarDrawerToggle$SlideDrawable;
astore 3
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mDrawerLayout Landroid/support/v4/widget/DrawerLayout;
ldc_w 8388611
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerOpen(I)Z
ifeq L2
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mOpenDrawerContentDescRes I
istore 2
L3:
aload 0
aload 3
iload 2
invokevirtual android/support/v4/app/ActionBarDrawerToggle/setActionBarUpIndicator(Landroid/graphics/drawable/Drawable;I)V
L4:
aload 0
iload 1
putfield android/support/v4/app/ActionBarDrawerToggle/mDrawerIndicatorEnabled Z
L0:
return
L2:
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mCloseDrawerContentDescRes I
istore 2
goto L3
L1:
aload 0
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mThemeImage Landroid/graphics/drawable/Drawable;
iconst_0
invokevirtual android/support/v4/app/ActionBarDrawerToggle/setActionBarUpIndicator(Landroid/graphics/drawable/Drawable;I)V
goto L4
.limit locals 4
.limit stack 3
.end method

.method public syncState()V
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mDrawerLayout Landroid/support/v4/widget/DrawerLayout;
ldc_w 8388611
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerOpen(I)Z
ifeq L0
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mSlider Landroid/support/v4/app/ActionBarDrawerToggle$SlideDrawable;
fconst_1
invokevirtual android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/setOffset(F)V
L1:
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mDrawerIndicatorEnabled Z
ifeq L2
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mSlider Landroid/support/v4/app/ActionBarDrawerToggle$SlideDrawable;
astore 2
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mDrawerLayout Landroid/support/v4/widget/DrawerLayout;
ldc_w 8388611
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerOpen(I)Z
ifeq L3
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mOpenDrawerContentDescRes I
istore 1
L4:
aload 0
aload 2
iload 1
invokevirtual android/support/v4/app/ActionBarDrawerToggle/setActionBarUpIndicator(Landroid/graphics/drawable/Drawable;I)V
L2:
return
L0:
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mSlider Landroid/support/v4/app/ActionBarDrawerToggle$SlideDrawable;
fconst_0
invokevirtual android/support/v4/app/ActionBarDrawerToggle$SlideDrawable/setOffset(F)V
goto L1
L3:
aload 0
getfield android/support/v4/app/ActionBarDrawerToggle/mCloseDrawerContentDescRes I
istore 1
goto L4
.limit locals 3
.limit stack 3
.end method
