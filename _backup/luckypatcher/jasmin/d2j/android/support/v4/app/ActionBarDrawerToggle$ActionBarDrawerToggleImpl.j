.bytecode 50.0
.class abstract interface android/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImpl
.super java/lang/Object
.inner class private static abstract interface ActionBarDrawerToggleImpl inner android/support/v4/app/ActionBarDrawerToggle$ActionBarDrawerToggleImpl outer android/support/v4/app/ActionBarDrawerToggle

.method public abstract getThemeUpIndicator(Landroid/app/Activity;)Landroid/graphics/drawable/Drawable;
.end method

.method public abstract setActionBarDescription(Ljava/lang/Object;Landroid/app/Activity;I)Ljava/lang/Object;
.end method

.method public abstract setActionBarUpIndicator(Ljava/lang/Object;Landroid/app/Activity;Landroid/graphics/drawable/Drawable;I)Ljava/lang/Object;
.end method
