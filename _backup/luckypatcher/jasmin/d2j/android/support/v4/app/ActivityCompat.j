.bytecode 50.0
.class public synchronized android/support/v4/app/ActivityCompat
.super android/support/v4/content/ContextCompat

.method public <init>()V
aload 0
invokespecial android/support/v4/content/ContextCompat/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static finishAffinity(Landroid/app/Activity;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
aload 0
invokestatic android/support/v4/app/ActivityCompatJB/finishAffinity(Landroid/app/Activity;)V
return
L0:
aload 0
invokevirtual android/app/Activity/finish()V
return
.limit locals 1
.limit stack 2
.end method

.method public static invalidateOptionsMenu(Landroid/app/Activity;)Z
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
aload 0
invokestatic android/support/v4/app/ActivityCompatHoneycomb/invalidateOptionsMenu(Landroid/app/Activity;)V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static startActivity(Landroid/app/Activity;Landroid/content/Intent;Landroid/os/Bundle;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
aload 0
aload 1
aload 2
invokestatic android/support/v4/app/ActivityCompatJB/startActivity(Landroid/content/Context;Landroid/content/Intent;Landroid/os/Bundle;)V
return
L0:
aload 0
aload 1
invokevirtual android/app/Activity/startActivity(Landroid/content/Intent;)V
return
.limit locals 3
.limit stack 3
.end method

.method public static startActivityForResult(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
aload 0
aload 1
iload 2
aload 3
invokestatic android/support/v4/app/ActivityCompatJB/startActivityForResult(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V
return
L0:
aload 0
aload 1
iload 2
invokevirtual android/app/Activity/startActivityForResult(Landroid/content/Intent;I)V
return
.limit locals 4
.limit stack 4
.end method
