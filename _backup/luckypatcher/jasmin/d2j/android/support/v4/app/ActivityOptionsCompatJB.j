.bytecode 50.0
.class synchronized android/support/v4/app/ActivityOptionsCompatJB
.super java/lang/Object

.field private final 'mActivityOptions' Landroid/app/ActivityOptions;

.method private <init>(Landroid/app/ActivityOptions;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/app/ActivityOptionsCompatJB/mActivityOptions Landroid/app/ActivityOptions;
return
.limit locals 2
.limit stack 2
.end method

.method public static makeCustomAnimation(Landroid/content/Context;II)Landroid/support/v4/app/ActivityOptionsCompatJB;
new android/support/v4/app/ActivityOptionsCompatJB
dup
aload 0
iload 1
iload 2
invokestatic android/app/ActivityOptions/makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;
invokespecial android/support/v4/app/ActivityOptionsCompatJB/<init>(Landroid/app/ActivityOptions;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method public static makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/support/v4/app/ActivityOptionsCompatJB;
new android/support/v4/app/ActivityOptionsCompatJB
dup
aload 0
iload 1
iload 2
iload 3
iload 4
invokestatic android/app/ActivityOptions/makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/app/ActivityOptions;
invokespecial android/support/v4/app/ActivityOptionsCompatJB/<init>(Landroid/app/ActivityOptions;)V
areturn
.limit locals 5
.limit stack 7
.end method

.method public static makeThumbnailScaleUpAnimation(Landroid/view/View;Landroid/graphics/Bitmap;II)Landroid/support/v4/app/ActivityOptionsCompatJB;
new android/support/v4/app/ActivityOptionsCompatJB
dup
aload 0
aload 1
iload 2
iload 3
invokestatic android/app/ActivityOptions/makeThumbnailScaleUpAnimation(Landroid/view/View;Landroid/graphics/Bitmap;II)Landroid/app/ActivityOptions;
invokespecial android/support/v4/app/ActivityOptionsCompatJB/<init>(Landroid/app/ActivityOptions;)V
areturn
.limit locals 4
.limit stack 6
.end method

.method public toBundle()Landroid/os/Bundle;
aload 0
getfield android/support/v4/app/ActivityOptionsCompatJB/mActivityOptions Landroid/app/ActivityOptions;
invokevirtual android/app/ActivityOptions/toBundle()Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method public update(Landroid/support/v4/app/ActivityOptionsCompatJB;)V
aload 0
getfield android/support/v4/app/ActivityOptionsCompatJB/mActivityOptions Landroid/app/ActivityOptions;
aload 1
getfield android/support/v4/app/ActivityOptionsCompatJB/mActivityOptions Landroid/app/ActivityOptions;
invokevirtual android/app/ActivityOptions/update(Landroid/app/ActivityOptions;)V
return
.limit locals 2
.limit stack 2
.end method
