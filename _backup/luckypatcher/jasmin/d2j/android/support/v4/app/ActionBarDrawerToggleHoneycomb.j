.bytecode 50.0
.class synchronized android/support/v4/app/ActionBarDrawerToggleHoneycomb
.super java/lang/Object
.inner class private static SetIndicatorInfo inner android/support/v4/app/ActionBarDrawerToggleHoneycomb$SetIndicatorInfo outer android/support/v4/app/ActionBarDrawerToggleHoneycomb

.field private static final 'TAG' Ljava/lang/String; = "ActionBarDrawerToggleHoneycomb"

.field private static final 'THEME_ATTRS' [I

.method static <clinit>()V
iconst_1
newarray int
dup
iconst_0
ldc_w 16843531
iastore
putstatic android/support/v4/app/ActionBarDrawerToggleHoneycomb/THEME_ATTRS [I
return
.limit locals 0
.limit stack 4
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getThemeUpIndicator(Landroid/app/Activity;)Landroid/graphics/drawable/Drawable;
aload 0
getstatic android/support/v4/app/ActionBarDrawerToggleHoneycomb/THEME_ATTRS [I
invokevirtual android/app/Activity/obtainStyledAttributes([I)Landroid/content/res/TypedArray;
astore 0
aload 0
iconst_0
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 1
aload 0
invokevirtual android/content/res/TypedArray/recycle()V
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public static setActionBarDescription(Ljava/lang/Object;Landroid/app/Activity;I)Ljava/lang/Object;
.catch java/lang/Exception from L0 to L1 using L2
aload 0
astore 3
aload 0
ifnonnull L3
new android/support/v4/app/ActionBarDrawerToggleHoneycomb$SetIndicatorInfo
dup
aload 1
invokespecial android/support/v4/app/ActionBarDrawerToggleHoneycomb$SetIndicatorInfo/<init>(Landroid/app/Activity;)V
astore 3
L3:
aload 3
checkcast android/support/v4/app/ActionBarDrawerToggleHoneycomb$SetIndicatorInfo
astore 0
aload 0
getfield android/support/v4/app/ActionBarDrawerToggleHoneycomb$SetIndicatorInfo/setHomeAsUpIndicator Ljava/lang/reflect/Method;
ifnull L1
L0:
aload 1
invokevirtual android/app/Activity/getActionBar()Landroid/app/ActionBar;
astore 1
aload 0
getfield android/support/v4/app/ActionBarDrawerToggleHoneycomb$SetIndicatorInfo/setHomeActionContentDescription Ljava/lang/reflect/Method;
aload 1
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
aload 3
areturn
L2:
astore 0
ldc "ActionBarDrawerToggleHoneycomb"
ldc "Couldn't set content description via JB-MR2 API"
aload 0
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aload 3
areturn
.limit locals 4
.limit stack 6
.end method

.method public static setActionBarUpIndicator(Ljava/lang/Object;Landroid/app/Activity;Landroid/graphics/drawable/Drawable;I)Ljava/lang/Object;
.catch java/lang/Exception from L0 to L1 using L2
aload 0
astore 4
aload 0
ifnonnull L3
new android/support/v4/app/ActionBarDrawerToggleHoneycomb$SetIndicatorInfo
dup
aload 1
invokespecial android/support/v4/app/ActionBarDrawerToggleHoneycomb$SetIndicatorInfo/<init>(Landroid/app/Activity;)V
astore 4
L3:
aload 4
checkcast android/support/v4/app/ActionBarDrawerToggleHoneycomb$SetIndicatorInfo
astore 0
aload 0
getfield android/support/v4/app/ActionBarDrawerToggleHoneycomb$SetIndicatorInfo/setHomeAsUpIndicator Ljava/lang/reflect/Method;
ifnull L4
L0:
aload 1
invokevirtual android/app/Activity/getActionBar()Landroid/app/ActionBar;
astore 1
aload 0
getfield android/support/v4/app/ActionBarDrawerToggleHoneycomb$SetIndicatorInfo/setHomeAsUpIndicator Ljava/lang/reflect/Method;
aload 1
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield android/support/v4/app/ActionBarDrawerToggleHoneycomb$SetIndicatorInfo/setHomeActionContentDescription Ljava/lang/reflect/Method;
aload 1
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
aload 4
areturn
L2:
astore 0
ldc "ActionBarDrawerToggleHoneycomb"
ldc "Couldn't set home-as-up indicator via JB-MR2 API"
aload 0
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aload 4
areturn
L4:
aload 0
getfield android/support/v4/app/ActionBarDrawerToggleHoneycomb$SetIndicatorInfo/upIndicatorView Landroid/widget/ImageView;
ifnull L5
aload 0
getfield android/support/v4/app/ActionBarDrawerToggleHoneycomb$SetIndicatorInfo/upIndicatorView Landroid/widget/ImageView;
aload 2
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 4
areturn
L5:
ldc "ActionBarDrawerToggleHoneycomb"
ldc "Couldn't set home-as-up indicator"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 4
areturn
.limit locals 5
.limit stack 6
.end method
