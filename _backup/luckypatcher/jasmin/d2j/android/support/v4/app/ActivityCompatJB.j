.bytecode 50.0
.class synchronized android/support/v4/app/ActivityCompatJB
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static finishAffinity(Landroid/app/Activity;)V
aload 0
invokevirtual android/app/Activity/finishAffinity()V
return
.limit locals 1
.limit stack 1
.end method

.method public static startActivity(Landroid/content/Context;Landroid/content/Intent;Landroid/os/Bundle;)V
aload 0
aload 1
aload 2
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method

.method public static startActivityForResult(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V
aload 0
aload 1
iload 2
aload 3
invokevirtual android/app/Activity/startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
return
.limit locals 4
.limit stack 4
.end method
