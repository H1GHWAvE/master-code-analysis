.bytecode 50.0
.class synchronized android/support/v4/content/FileProvider$SimplePathStrategy
.super java/lang/Object
.implements android/support/v4/content/FileProvider$PathStrategy
.inner class static SimplePathStrategy inner android/support/v4/content/FileProvider$SimplePathStrategy outer android/support/v4/content/FileProvider

.field private final 'mAuthority' Ljava/lang/String;

.field private final 'mRoots' Ljava/util/HashMap; signature "Ljava/util/HashMap<Ljava/lang/String;Ljava/io/File;>;"

.method public <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield android/support/v4/content/FileProvider$SimplePathStrategy/mRoots Ljava/util/HashMap;
aload 0
aload 1
putfield android/support/v4/content/FileProvider$SimplePathStrategy/mAuthority Ljava/lang/String;
return
.limit locals 2
.limit stack 3
.end method

.method public addRoot(Ljava/lang/String;Ljava/io/File;)V
.catch java/io/IOException from L0 to L1 using L2
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L0
new java/lang/IllegalArgumentException
dup
ldc "Name must not be empty"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
invokevirtual java/io/File/getCanonicalFile()Ljava/io/File;
astore 3
L1:
aload 0
getfield android/support/v4/content/FileProvider$SimplePathStrategy/mRoots Ljava/util/HashMap;
aload 1
aload 3
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
return
L2:
astore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Failed to resolve canonical path for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 4
.limit stack 4
.end method

.method public getFileForUri(Landroid/net/Uri;)Ljava/io/File;
.catch java/io/IOException from L0 to L1 using L2
aload 1
invokevirtual android/net/Uri/getEncodedPath()Ljava/lang/String;
astore 4
aload 4
bipush 47
iconst_1
invokevirtual java/lang/String/indexOf(II)I
istore 2
aload 4
iconst_1
iload 2
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokestatic android/net/Uri/decode(Ljava/lang/String;)Ljava/lang/String;
astore 3
aload 4
iload 2
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic android/net/Uri/decode(Ljava/lang/String;)Ljava/lang/String;
astore 4
aload 0
getfield android/support/v4/content/FileProvider$SimplePathStrategy/mRoots Ljava/util/HashMap;
aload 3
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/io/File
astore 3
aload 3
ifnonnull L3
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unable to find configured root for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L3:
new java/io/File
dup
aload 3
aload 4
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 1
L0:
aload 1
invokevirtual java/io/File/getCanonicalFile()Ljava/io/File;
astore 4
L1:
aload 4
invokevirtual java/io/File/getPath()Ljava/lang/String;
aload 3
invokevirtual java/io/File/getPath()Ljava/lang/String;
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L4
new java/lang/SecurityException
dup
ldc "Resolved path jumped beyond configured root"
invokespecial java/lang/SecurityException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 3
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Failed to resolve canonical path for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 4
areturn
.limit locals 5
.limit stack 4
.end method

.method public getUriForFile(Ljava/io/File;)Landroid/net/Uri;
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 1
invokevirtual java/io/File/getCanonicalPath()Ljava/lang/String;
astore 3
L1:
aconst_null
astore 1
aload 0
getfield android/support/v4/content/FileProvider$SimplePathStrategy/mRoots Ljava/util/HashMap;
invokevirtual java/util/HashMap/entrySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 4
L3:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 2
aload 2
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/io/File
invokevirtual java/io/File/getPath()Ljava/lang/String;
astore 5
aload 3
aload 5
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L3
aload 1
ifnull L5
aload 5
invokevirtual java/lang/String/length()I
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/io/File
invokevirtual java/io/File/getPath()Ljava/lang/String;
invokevirtual java/lang/String/length()I
if_icmple L3
L5:
aload 2
astore 1
goto L3
L2:
astore 2
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Failed to resolve canonical path for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 1
ifnonnull L6
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Failed to find configured root that contains "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L6:
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/io/File
invokevirtual java/io/File/getPath()Ljava/lang/String;
astore 2
aload 2
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L7
aload 3
aload 2
invokevirtual java/lang/String/length()I
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 2
L8:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/String
invokestatic android/net/Uri/encode(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
bipush 47
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
aload 2
ldc "/"
invokestatic android/net/Uri/encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
new android/net/Uri$Builder
dup
invokespecial android/net/Uri$Builder/<init>()V
ldc "content"
invokevirtual android/net/Uri$Builder/scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;
aload 0
getfield android/support/v4/content/FileProvider$SimplePathStrategy/mAuthority Ljava/lang/String;
invokevirtual android/net/Uri$Builder/authority(Ljava/lang/String;)Landroid/net/Uri$Builder;
aload 1
invokevirtual android/net/Uri$Builder/encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
invokevirtual android/net/Uri$Builder/build()Landroid/net/Uri;
areturn
L7:
aload 3
aload 2
invokevirtual java/lang/String/length()I
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 2
goto L8
.limit locals 6
.limit stack 4
.end method
