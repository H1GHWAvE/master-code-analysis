.bytecode 50.0
.class public synchronized abstract android/support/v4/content/WakefulBroadcastReceiver
.super android/content/BroadcastReceiver

.field private static final 'EXTRA_WAKE_LOCK_ID' Ljava/lang/String; = "android.support.content.wakelockid"

.field private static final 'mActiveWakeLocks' Landroid/util/SparseArray; signature "Landroid/util/SparseArray<Landroid/os/PowerManager$WakeLock;>;"

.field private static 'mNextId' I

.method static <clinit>()V
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
putstatic android/support/v4/content/WakefulBroadcastReceiver/mActiveWakeLocks Landroid/util/SparseArray;
iconst_1
putstatic android/support/v4/content/WakefulBroadcastReceiver/mNextId I
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial android/content/BroadcastReceiver/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static completeWakefulIntent(Landroid/content/Intent;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
aload 0
ldc "android.support.content.wakelockid"
iconst_0
invokevirtual android/content/Intent/getIntExtra(Ljava/lang/String;I)I
istore 1
iload 1
ifne L9
iconst_0
ireturn
L9:
getstatic android/support/v4/content/WakefulBroadcastReceiver/mActiveWakeLocks Landroid/util/SparseArray;
astore 0
aload 0
monitorenter
L0:
getstatic android/support/v4/content/WakefulBroadcastReceiver/mActiveWakeLocks Landroid/util/SparseArray;
iload 1
invokevirtual android/util/SparseArray/get(I)Ljava/lang/Object;
checkcast android/os/PowerManager$WakeLock
astore 2
L1:
aload 2
ifnull L5
L3:
aload 2
invokevirtual android/os/PowerManager$WakeLock/release()V
getstatic android/support/v4/content/WakefulBroadcastReceiver/mActiveWakeLocks Landroid/util/SparseArray;
iload 1
invokevirtual android/util/SparseArray/remove(I)V
aload 0
monitorexit
L4:
iconst_1
ireturn
L5:
ldc "WakefulBroadcastReceiver"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "No active wake lock id #"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
monitorexit
L6:
iconst_1
ireturn
L2:
astore 2
L7:
aload 0
monitorexit
L8:
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public static startWakefulService(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
getstatic android/support/v4/content/WakefulBroadcastReceiver/mActiveWakeLocks Landroid/util/SparseArray;
astore 3
aload 3
monitorenter
L0:
getstatic android/support/v4/content/WakefulBroadcastReceiver/mNextId I
istore 2
getstatic android/support/v4/content/WakefulBroadcastReceiver/mNextId I
iconst_1
iadd
putstatic android/support/v4/content/WakefulBroadcastReceiver/mNextId I
getstatic android/support/v4/content/WakefulBroadcastReceiver/mNextId I
ifgt L1
iconst_1
putstatic android/support/v4/content/WakefulBroadcastReceiver/mNextId I
L1:
aload 1
ldc "android.support.content.wakelockid"
iload 2
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;I)Landroid/content/Intent;
pop
aload 0
aload 1
invokevirtual android/content/Context/startService(Landroid/content/Intent;)Landroid/content/ComponentName;
astore 1
L3:
aload 1
ifnonnull L6
L4:
aload 3
monitorexit
L5:
aconst_null
areturn
L6:
aload 0
ldc "power"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/os/PowerManager
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "wake:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual android/content/ComponentName/flattenToShortString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/os/PowerManager/newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
astore 0
aload 0
iconst_0
invokevirtual android/os/PowerManager$WakeLock/setReferenceCounted(Z)V
aload 0
ldc2_w 60000L
invokevirtual android/os/PowerManager$WakeLock/acquire(J)V
getstatic android/support/v4/content/WakefulBroadcastReceiver/mActiveWakeLocks Landroid/util/SparseArray;
iload 2
aload 0
invokevirtual android/util/SparseArray/put(ILjava/lang/Object;)V
aload 3
monitorexit
L7:
aload 1
areturn
L2:
astore 0
L8:
aload 3
monitorexit
L9:
aload 0
athrow
.limit locals 4
.limit stack 4
.end method
