.bytecode 50.0
.class abstract interface android/support/v4/content/FileProvider$PathStrategy
.super java/lang/Object
.inner class static abstract interface PathStrategy inner android/support/v4/content/FileProvider$PathStrategy outer android/support/v4/content/FileProvider

.method public abstract getFileForUri(Landroid/net/Uri;)Ljava/io/File;
.end method

.method public abstract getUriForFile(Ljava/io/File;)Landroid/net/Uri;
.end method
