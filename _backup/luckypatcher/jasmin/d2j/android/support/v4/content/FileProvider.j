.bytecode 50.0
.class public synchronized android/support/v4/content/FileProvider
.super android/content/ContentProvider
.inner class static abstract interface PathStrategy inner android/support/v4/content/FileProvider$PathStrategy outer android/support/v4/content/FileProvider
.inner class static SimplePathStrategy inner android/support/v4/content/FileProvider$SimplePathStrategy outer android/support/v4/content/FileProvider

.field private static final 'ATTR_NAME' Ljava/lang/String; = "name"

.field private static final 'ATTR_PATH' Ljava/lang/String; = "path"

.field private static final 'COLUMNS' [Ljava/lang/String;

.field private static final 'DEVICE_ROOT' Ljava/io/File;

.field private static final 'META_DATA_FILE_PROVIDER_PATHS' Ljava/lang/String; = "android.support.FILE_PROVIDER_PATHS"

.field private static final 'TAG_CACHE_PATH' Ljava/lang/String; = "cache-path"

.field private static final 'TAG_EXTERNAL' Ljava/lang/String; = "external-path"

.field private static final 'TAG_FILES_PATH' Ljava/lang/String; = "files-path"

.field private static final 'TAG_ROOT_PATH' Ljava/lang/String; = "root-path"

.field private static 'sCache' Ljava/util/HashMap; signature "Ljava/util/HashMap<Ljava/lang/String;Landroid/support/v4/content/FileProvider$PathStrategy;>;"

.field private 'mStrategy' Landroid/support/v4/content/FileProvider$PathStrategy;

.method static <clinit>()V
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "_display_name"
aastore
dup
iconst_1
ldc "_size"
aastore
putstatic android/support/v4/content/FileProvider/COLUMNS [Ljava/lang/String;
new java/io/File
dup
ldc "/"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic android/support/v4/content/FileProvider/DEVICE_ROOT Ljava/io/File;
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putstatic android/support/v4/content/FileProvider/sCache Ljava/util/HashMap;
return
.limit locals 0
.limit stack 4
.end method

.method public <init>()V
aload 0
invokespecial android/content/ContentProvider/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static transient buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
aload 1
arraylength
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 1
iload 2
aaload
astore 4
aload 4
ifnull L2
new java/io/File
dup
aload 0
aload 4
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 0
L3:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
areturn
L2:
goto L3
.limit locals 5
.limit stack 4
.end method

.method private static copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;
iload 1
anewarray java/lang/Object
astore 2
aload 0
iconst_0
aload 2
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method private static copyOf([Ljava/lang/String;I)[Ljava/lang/String;
iload 1
anewarray java/lang/String
astore 2
aload 0
iconst_0
aload 2
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method private static getPathStrategy(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/content/FileProvider$PathStrategy;
.catch all from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L5
.catch org/xmlpull/v1/XmlPullParserException from L3 to L4 using L6
.catch all from L3 to L4 using L2
.catch all from L4 to L7 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L2 using L2
.catch all from L10 to L11 using L2
.catch all from L12 to L13 using L2
getstatic android/support/v4/content/FileProvider/sCache Ljava/util/HashMap;
astore 4
aload 4
monitorenter
L0:
getstatic android/support/v4/content/FileProvider/sCache Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/support/v4/content/FileProvider$PathStrategy
astore 3
L1:
aload 3
astore 2
aload 3
ifnonnull L7
L3:
aload 0
aload 1
invokestatic android/support/v4/content/FileProvider/parsePathStrategy(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/content/FileProvider$PathStrategy;
astore 2
L4:
getstatic android/support/v4/content/FileProvider/sCache Ljava/util/HashMap;
aload 1
aload 2
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L7:
aload 4
monitorexit
L8:
aload 2
areturn
L5:
astore 0
L9:
new java/lang/IllegalArgumentException
dup
ldc "Failed to parse android.support.FILE_PROVIDER_PATHS meta-data"
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L2:
astore 0
L10:
aload 4
monitorexit
L11:
aload 0
athrow
L6:
astore 0
L12:
new java/lang/IllegalArgumentException
dup
ldc "Failed to parse android.support.FILE_PROVIDER_PATHS meta-data"
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L13:
.limit locals 5
.limit stack 4
.end method

.method public static getUriForFile(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;
aload 0
aload 1
invokestatic android/support/v4/content/FileProvider/getPathStrategy(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/content/FileProvider$PathStrategy;
aload 2
invokeinterface android/support/v4/content/FileProvider$PathStrategy/getUriForFile(Ljava/io/File;)Landroid/net/Uri; 1
areturn
.limit locals 3
.limit stack 2
.end method

.method private static modeToMode(Ljava/lang/String;)I
ldc "r"
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
ldc_w 268435456
ireturn
L0:
ldc "w"
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
ldc "wt"
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
L1:
ldc_w 738197504
ireturn
L2:
ldc "wa"
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
ldc_w 704643072
ireturn
L3:
ldc "rw"
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
ldc_w 939524096
ireturn
L4:
ldc "rwt"
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
ldc_w 1006632960
ireturn
L5:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Invalid mode: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 4
.end method

.method private static parsePathStrategy(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/content/FileProvider$PathStrategy;
.throws java/io/IOException
.throws org/xmlpull/v1/XmlPullParserException
new android/support/v4/content/FileProvider$SimplePathStrategy
dup
aload 1
invokespecial android/support/v4/content/FileProvider$SimplePathStrategy/<init>(Ljava/lang/String;)V
astore 3
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 1
sipush 128
invokevirtual android/content/pm/PackageManager/resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
ldc "android.support.FILE_PROVIDER_PATHS"
invokevirtual android/content/pm/ProviderInfo/loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
astore 4
aload 4
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "Missing android.support.FILE_PROVIDER_PATHS meta-data"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 4
invokeinterface android/content/res/XmlResourceParser/next()I 0
istore 2
iload 2
iconst_1
if_icmpeq L1
iload 2
iconst_2
if_icmpne L0
aload 4
invokeinterface android/content/res/XmlResourceParser/getName()Ljava/lang/String; 0
astore 6
aload 4
aconst_null
ldc "name"
invokeinterface android/content/res/XmlResourceParser/getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 5
aload 4
aconst_null
ldc "path"
invokeinterface android/content/res/XmlResourceParser/getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 7
aconst_null
astore 1
ldc "root-path"
aload 6
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
getstatic android/support/v4/content/FileProvider/DEVICE_ROOT Ljava/io/File;
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 7
aastore
invokestatic android/support/v4/content/FileProvider/buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 1
L3:
aload 1
ifnull L0
aload 3
aload 5
aload 1
invokevirtual android/support/v4/content/FileProvider$SimplePathStrategy/addRoot(Ljava/lang/String;Ljava/io/File;)V
goto L0
L2:
ldc "files-path"
aload 6
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
aload 0
invokevirtual android/content/Context/getFilesDir()Ljava/io/File;
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 7
aastore
invokestatic android/support/v4/content/FileProvider/buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 1
goto L3
L4:
ldc "cache-path"
aload 6
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
aload 0
invokevirtual android/content/Context/getCacheDir()Ljava/io/File;
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 7
aastore
invokestatic android/support/v4/content/FileProvider/buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 1
goto L3
L5:
ldc "external-path"
aload 6
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 7
aastore
invokestatic android/support/v4/content/FileProvider/buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 1
goto L3
L1:
aload 3
areturn
.limit locals 8
.limit stack 5
.end method

.method public attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
aload 0
aload 1
aload 2
invokespecial android/content/ContentProvider/attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
aload 2
getfield android/content/pm/ProviderInfo/exported Z
ifeq L0
new java/lang/SecurityException
dup
ldc "Provider must not be exported"
invokespecial java/lang/SecurityException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
getfield android/content/pm/ProviderInfo/grantUriPermissions Z
ifne L1
new java/lang/SecurityException
dup
ldc "Provider must grant uri permissions"
invokespecial java/lang/SecurityException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 1
aload 2
getfield android/content/pm/ProviderInfo/authority Ljava/lang/String;
invokestatic android/support/v4/content/FileProvider/getPathStrategy(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/content/FileProvider$PathStrategy;
putfield android/support/v4/content/FileProvider/mStrategy Landroid/support/v4/content/FileProvider$PathStrategy;
return
.limit locals 3
.limit stack 3
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
aload 0
getfield android/support/v4/content/FileProvider/mStrategy Landroid/support/v4/content/FileProvider$PathStrategy;
aload 1
invokeinterface android/support/v4/content/FileProvider$PathStrategy/getFileForUri(Landroid/net/Uri;)Ljava/io/File; 1
invokevirtual java/io/File/delete()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
aload 0
getfield android/support/v4/content/FileProvider/mStrategy Landroid/support/v4/content/FileProvider$PathStrategy;
aload 1
invokeinterface android/support/v4/content/FileProvider$PathStrategy/getFileForUri(Landroid/net/Uri;)Ljava/io/File; 1
astore 1
aload 1
invokevirtual java/io/File/getName()Ljava/lang/String;
bipush 46
invokevirtual java/lang/String/lastIndexOf(I)I
istore 2
iload 2
iflt L0
aload 1
invokevirtual java/io/File/getName()Ljava/lang/String;
iload 2
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 1
invokestatic android/webkit/MimeTypeMap/getSingleton()Landroid/webkit/MimeTypeMap;
aload 1
invokevirtual android/webkit/MimeTypeMap/getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 1
ifnull L0
aload 1
areturn
L0:
ldc "application/octet-stream"
areturn
.limit locals 3
.limit stack 3
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
new java/lang/UnsupportedOperationException
dup
ldc "No external inserts"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public onCreate()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
.throws java/io/FileNotFoundException
aload 0
getfield android/support/v4/content/FileProvider/mStrategy Landroid/support/v4/content/FileProvider$PathStrategy;
aload 1
invokeinterface android/support/v4/content/FileProvider$PathStrategy/getFileForUri(Landroid/net/Uri;)Ljava/io/File; 1
aload 2
invokestatic android/support/v4/content/FileProvider/modeToMode(Ljava/lang/String;)I
invokestatic android/os/ParcelFileDescriptor/open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
areturn
.limit locals 3
.limit stack 2
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
aload 0
getfield android/support/v4/content/FileProvider/mStrategy Landroid/support/v4/content/FileProvider$PathStrategy;
aload 1
invokeinterface android/support/v4/content/FileProvider$PathStrategy/getFileForUri(Landroid/net/Uri;)Ljava/io/File; 1
astore 3
aload 2
astore 1
aload 2
ifnonnull L0
getstatic android/support/v4/content/FileProvider/COLUMNS [Ljava/lang/String;
astore 1
L0:
aload 1
arraylength
anewarray java/lang/String
astore 4
aload 1
arraylength
anewarray java/lang/Object
astore 2
aload 1
arraylength
istore 9
iconst_0
istore 7
iconst_0
istore 6
L1:
iload 7
iload 9
if_icmpge L2
aload 1
iload 7
aaload
astore 5
ldc "_display_name"
aload 5
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
aload 4
iload 6
ldc "_display_name"
aastore
iload 6
iconst_1
iadd
istore 8
aload 2
iload 6
aload 3
invokevirtual java/io/File/getName()Ljava/lang/String;
aastore
iload 8
istore 6
L4:
iload 7
iconst_1
iadd
istore 7
goto L1
L3:
ldc "_size"
aload 5
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
aload 4
iload 6
ldc "_size"
aastore
iload 6
iconst_1
iadd
istore 8
aload 2
iload 6
aload 3
invokevirtual java/io/File/length()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
iload 8
istore 6
goto L4
L2:
aload 4
iload 6
invokestatic android/support/v4/content/FileProvider/copyOf([Ljava/lang/String;I)[Ljava/lang/String;
astore 1
aload 2
iload 6
invokestatic android/support/v4/content/FileProvider/copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;
astore 2
new android/database/MatrixCursor
dup
aload 1
iconst_1
invokespecial android/database/MatrixCursor/<init>([Ljava/lang/String;I)V
astore 1
aload 1
aload 2
invokevirtual android/database/MatrixCursor/addRow([Ljava/lang/Object;)V
aload 1
areturn
L5:
goto L4
.limit locals 10
.limit stack 4
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
new java/lang/UnsupportedOperationException
dup
ldc "No external updates"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 5
.limit stack 3
.end method
