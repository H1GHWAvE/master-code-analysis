.bytecode 50.0
.class public abstract interface android/support/v4/internal/view/SupportContextMenu
.super java/lang/Object
.implements android/support/v4/internal/view/SupportMenu
.implements android/view/ContextMenu

.method public abstract clearHeader()V
.end method

.method public abstract setHeaderIcon(I)Landroid/support/v4/internal/view/SupportContextMenu;
.end method

.method public abstract setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/support/v4/internal/view/SupportContextMenu;
.end method

.method public abstract setHeaderTitle(I)Landroid/support/v4/internal/view/SupportContextMenu;
.end method

.method public abstract setHeaderTitle(Ljava/lang/CharSequence;)Landroid/support/v4/internal/view/SupportContextMenu;
.end method

.method public abstract setHeaderView(Landroid/view/View;)Landroid/support/v4/internal/view/SupportContextMenu;
.end method
