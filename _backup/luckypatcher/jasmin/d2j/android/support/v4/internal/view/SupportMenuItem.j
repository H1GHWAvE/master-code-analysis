.bytecode 50.0
.class public abstract interface android/support/v4/internal/view/SupportMenuItem
.super java/lang/Object
.implements android/view/MenuItem

.field public static final 'SHOW_AS_ACTION_ALWAYS' I = 2


.field public static final 'SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW' I = 8


.field public static final 'SHOW_AS_ACTION_IF_ROOM' I = 1


.field public static final 'SHOW_AS_ACTION_NEVER' I = 0


.field public static final 'SHOW_AS_ACTION_WITH_TEXT' I = 4


.method public abstract collapseActionView()Z
.end method

.method public abstract expandActionView()Z
.end method

.method public abstract getActionView()Landroid/view/View;
.end method

.method public abstract getSupportActionProvider()Landroid/support/v4/view/ActionProvider;
.end method

.method public abstract isActionViewExpanded()Z
.end method

.method public abstract setActionView(I)Landroid/view/MenuItem;
.end method

.method public abstract setActionView(Landroid/view/View;)Landroid/view/MenuItem;
.end method

.method public abstract setShowAsAction(I)V
.end method

.method public abstract setShowAsActionFlags(I)Landroid/view/MenuItem;
.end method

.method public abstract setSupportActionProvider(Landroid/support/v4/view/ActionProvider;)Landroid/support/v4/internal/view/SupportMenuItem;
.end method

.method public abstract setSupportOnActionExpandListener(Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/support/v4/internal/view/SupportMenuItem;
.end method
