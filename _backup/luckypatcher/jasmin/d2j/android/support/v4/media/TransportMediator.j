.bytecode 50.0
.class public synchronized android/support/v4/media/TransportMediator
.super android/support/v4/media/TransportController
.inner class inner android/support/v4/media/TransportMediator$1
.inner class inner android/support/v4/media/TransportMediator$2

.field public static final 'FLAG_KEY_MEDIA_FAST_FORWARD' I = 64


.field public static final 'FLAG_KEY_MEDIA_NEXT' I = 128


.field public static final 'FLAG_KEY_MEDIA_PAUSE' I = 16


.field public static final 'FLAG_KEY_MEDIA_PLAY' I = 4


.field public static final 'FLAG_KEY_MEDIA_PLAY_PAUSE' I = 8


.field public static final 'FLAG_KEY_MEDIA_PREVIOUS' I = 1


.field public static final 'FLAG_KEY_MEDIA_REWIND' I = 2


.field public static final 'FLAG_KEY_MEDIA_STOP' I = 32


.field public static final 'KEYCODE_MEDIA_PAUSE' I = 127


.field public static final 'KEYCODE_MEDIA_PLAY' I = 126


.field public static final 'KEYCODE_MEDIA_RECORD' I = 130


.field final 'mAudioManager' Landroid/media/AudioManager;

.field final 'mCallbacks' Landroid/support/v4/media/TransportPerformer;

.field final 'mContext' Landroid/content/Context;

.field final 'mController' Landroid/support/v4/media/TransportMediatorJellybeanMR2;

.field final 'mDispatcherState' Ljava/lang/Object;

.field final 'mKeyEventCallback' Landroid/view/KeyEvent$Callback;

.field final 'mListeners' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Landroid/support/v4/media/TransportStateListener;>;"

.field final 'mTransportKeyCallback' Landroid/support/v4/media/TransportMediatorCallback;

.field final 'mView' Landroid/view/View;

.method public <init>(Landroid/app/Activity;Landroid/support/v4/media/TransportPerformer;)V
aload 0
aload 1
aconst_null
aload 2
invokespecial android/support/v4/media/TransportMediator/<init>(Landroid/app/Activity;Landroid/view/View;Landroid/support/v4/media/TransportPerformer;)V
return
.limit locals 3
.limit stack 4
.end method

.method private <init>(Landroid/app/Activity;Landroid/view/View;Landroid/support/v4/media/TransportPerformer;)V
aload 0
invokespecial android/support/v4/media/TransportController/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/media/TransportMediator/mListeners Ljava/util/ArrayList;
aload 0
new android/support/v4/media/TransportMediator$1
dup
aload 0
invokespecial android/support/v4/media/TransportMediator$1/<init>(Landroid/support/v4/media/TransportMediator;)V
putfield android/support/v4/media/TransportMediator/mTransportKeyCallback Landroid/support/v4/media/TransportMediatorCallback;
aload 0
new android/support/v4/media/TransportMediator$2
dup
aload 0
invokespecial android/support/v4/media/TransportMediator$2/<init>(Landroid/support/v4/media/TransportMediator;)V
putfield android/support/v4/media/TransportMediator/mKeyEventCallback Landroid/view/KeyEvent$Callback;
aload 1
ifnull L0
aload 1
astore 4
L1:
aload 0
aload 4
putfield android/support/v4/media/TransportMediator/mContext Landroid/content/Context;
aload 0
aload 3
putfield android/support/v4/media/TransportMediator/mCallbacks Landroid/support/v4/media/TransportPerformer;
aload 0
aload 0
getfield android/support/v4/media/TransportMediator/mContext Landroid/content/Context;
ldc "audio"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/media/AudioManager
putfield android/support/v4/media/TransportMediator/mAudioManager Landroid/media/AudioManager;
aload 1
ifnull L2
aload 1
invokevirtual android/app/Activity/getWindow()Landroid/view/Window;
invokevirtual android/view/Window/getDecorView()Landroid/view/View;
astore 2
L2:
aload 0
aload 2
putfield android/support/v4/media/TransportMediator/mView Landroid/view/View;
aload 0
aload 0
getfield android/support/v4/media/TransportMediator/mView Landroid/view/View;
invokestatic android/support/v4/view/KeyEventCompat/getKeyDispatcherState(Landroid/view/View;)Ljava/lang/Object;
putfield android/support/v4/media/TransportMediator/mDispatcherState Ljava/lang/Object;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L3
aload 0
new android/support/v4/media/TransportMediatorJellybeanMR2
dup
aload 0
getfield android/support/v4/media/TransportMediator/mContext Landroid/content/Context;
aload 0
getfield android/support/v4/media/TransportMediator/mAudioManager Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/TransportMediator/mView Landroid/view/View;
aload 0
getfield android/support/v4/media/TransportMediator/mTransportKeyCallback Landroid/support/v4/media/TransportMediatorCallback;
invokespecial android/support/v4/media/TransportMediatorJellybeanMR2/<init>(Landroid/content/Context;Landroid/media/AudioManager;Landroid/view/View;Landroid/support/v4/media/TransportMediatorCallback;)V
putfield android/support/v4/media/TransportMediator/mController Landroid/support/v4/media/TransportMediatorJellybeanMR2;
return
L0:
aload 2
invokevirtual android/view/View/getContext()Landroid/content/Context;
astore 4
goto L1
L3:
aload 0
aconst_null
putfield android/support/v4/media/TransportMediator/mController Landroid/support/v4/media/TransportMediatorJellybeanMR2;
return
.limit locals 5
.limit stack 7
.end method

.method public <init>(Landroid/view/View;Landroid/support/v4/media/TransportPerformer;)V
aload 0
aconst_null
aload 1
aload 2
invokespecial android/support/v4/media/TransportMediator/<init>(Landroid/app/Activity;Landroid/view/View;Landroid/support/v4/media/TransportPerformer;)V
return
.limit locals 3
.limit stack 4
.end method

.method private getListeners()[Landroid/support/v4/media/TransportStateListener;
aload 0
getfield android/support/v4/media/TransportMediator/mListeners Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifgt L0
aconst_null
areturn
L0:
aload 0
getfield android/support/v4/media/TransportMediator/mListeners Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
anewarray android/support/v4/media/TransportStateListener
astore 1
aload 0
getfield android/support/v4/media/TransportMediator/mListeners Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static isMediaKey(I)Z
iload 0
lookupswitch
79 : L0
85 : L0
86 : L0
87 : L0
88 : L0
89 : L0
90 : L0
91 : L0
126 : L0
127 : L0
130 : L0
default : L1
L1:
iconst_0
ireturn
L0:
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method private pushControllerState()V
aload 0
getfield android/support/v4/media/TransportMediator/mController Landroid/support/v4/media/TransportMediatorJellybeanMR2;
ifnull L0
aload 0
getfield android/support/v4/media/TransportMediator/mController Landroid/support/v4/media/TransportMediatorJellybeanMR2;
aload 0
getfield android/support/v4/media/TransportMediator/mCallbacks Landroid/support/v4/media/TransportPerformer;
invokevirtual android/support/v4/media/TransportPerformer/onIsPlaying()Z
aload 0
getfield android/support/v4/media/TransportMediator/mCallbacks Landroid/support/v4/media/TransportPerformer;
invokevirtual android/support/v4/media/TransportPerformer/onGetCurrentPosition()J
aload 0
getfield android/support/v4/media/TransportMediator/mCallbacks Landroid/support/v4/media/TransportPerformer;
invokevirtual android/support/v4/media/TransportPerformer/onGetTransportControlFlags()I
invokevirtual android/support/v4/media/TransportMediatorJellybeanMR2/refreshState(ZJI)V
L0:
return
.limit locals 1
.limit stack 5
.end method

.method private reportPlayingChanged()V
aload 0
invokespecial android/support/v4/media/TransportMediator/getListeners()[Landroid/support/v4/media/TransportStateListener;
astore 3
aload 3
ifnull L0
aload 3
arraylength
istore 2
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L0
aload 3
iload 1
aaload
aload 0
invokevirtual android/support/v4/media/TransportStateListener/onPlayingChanged(Landroid/support/v4/media/TransportController;)V
iload 1
iconst_1
iadd
istore 1
goto L1
L0:
return
.limit locals 4
.limit stack 2
.end method

.method private reportTransportControlsChanged()V
aload 0
invokespecial android/support/v4/media/TransportMediator/getListeners()[Landroid/support/v4/media/TransportStateListener;
astore 3
aload 3
ifnull L0
aload 3
arraylength
istore 2
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L0
aload 3
iload 1
aaload
aload 0
invokevirtual android/support/v4/media/TransportStateListener/onTransportControlsChanged(Landroid/support/v4/media/TransportController;)V
iload 1
iconst_1
iadd
istore 1
goto L1
L0:
return
.limit locals 4
.limit stack 2
.end method

.method public destroy()V
aload 0
getfield android/support/v4/media/TransportMediator/mController Landroid/support/v4/media/TransportMediatorJellybeanMR2;
invokevirtual android/support/v4/media/TransportMediatorJellybeanMR2/destroy()V
return
.limit locals 1
.limit stack 1
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
aload 1
aload 0
getfield android/support/v4/media/TransportMediator/mKeyEventCallback Landroid/view/KeyEvent$Callback;
aload 0
getfield android/support/v4/media/TransportMediator/mDispatcherState Ljava/lang/Object;
aload 0
invokestatic android/support/v4/view/KeyEventCompat/dispatch(Landroid/view/KeyEvent;Landroid/view/KeyEvent$Callback;Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 4
.end method

.method public getBufferPercentage()I
aload 0
getfield android/support/v4/media/TransportMediator/mCallbacks Landroid/support/v4/media/TransportPerformer;
invokevirtual android/support/v4/media/TransportPerformer/onGetBufferPercentage()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getCurrentPosition()J
aload 0
getfield android/support/v4/media/TransportMediator/mCallbacks Landroid/support/v4/media/TransportPerformer;
invokevirtual android/support/v4/media/TransportPerformer/onGetCurrentPosition()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getDuration()J
aload 0
getfield android/support/v4/media/TransportMediator/mCallbacks Landroid/support/v4/media/TransportPerformer;
invokevirtual android/support/v4/media/TransportPerformer/onGetDuration()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getRemoteControlClient()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/TransportMediator/mController Landroid/support/v4/media/TransportMediatorJellybeanMR2;
ifnull L0
aload 0
getfield android/support/v4/media/TransportMediator/mController Landroid/support/v4/media/TransportMediatorJellybeanMR2;
invokevirtual android/support/v4/media/TransportMediatorJellybeanMR2/getRemoteControlClient()Ljava/lang/Object;
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTransportControlFlags()I
aload 0
getfield android/support/v4/media/TransportMediator/mCallbacks Landroid/support/v4/media/TransportPerformer;
invokevirtual android/support/v4/media/TransportPerformer/onGetTransportControlFlags()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isPlaying()Z
aload 0
getfield android/support/v4/media/TransportMediator/mCallbacks Landroid/support/v4/media/TransportPerformer;
invokevirtual android/support/v4/media/TransportPerformer/onIsPlaying()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public pausePlaying()V
aload 0
getfield android/support/v4/media/TransportMediator/mController Landroid/support/v4/media/TransportMediatorJellybeanMR2;
ifnull L0
aload 0
getfield android/support/v4/media/TransportMediator/mController Landroid/support/v4/media/TransportMediatorJellybeanMR2;
invokevirtual android/support/v4/media/TransportMediatorJellybeanMR2/pausePlaying()V
L0:
aload 0
getfield android/support/v4/media/TransportMediator/mCallbacks Landroid/support/v4/media/TransportPerformer;
invokevirtual android/support/v4/media/TransportPerformer/onPause()V
aload 0
invokespecial android/support/v4/media/TransportMediator/pushControllerState()V
aload 0
invokespecial android/support/v4/media/TransportMediator/reportPlayingChanged()V
return
.limit locals 1
.limit stack 1
.end method

.method public refreshState()V
aload 0
invokespecial android/support/v4/media/TransportMediator/pushControllerState()V
aload 0
invokespecial android/support/v4/media/TransportMediator/reportPlayingChanged()V
aload 0
invokespecial android/support/v4/media/TransportMediator/reportTransportControlsChanged()V
return
.limit locals 1
.limit stack 1
.end method

.method public registerStateListener(Landroid/support/v4/media/TransportStateListener;)V
aload 0
getfield android/support/v4/media/TransportMediator/mListeners Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
return
.limit locals 2
.limit stack 2
.end method

.method public seekTo(J)V
aload 0
getfield android/support/v4/media/TransportMediator/mCallbacks Landroid/support/v4/media/TransportPerformer;
lload 1
invokevirtual android/support/v4/media/TransportPerformer/onSeekTo(J)V
return
.limit locals 3
.limit stack 3
.end method

.method public startPlaying()V
aload 0
getfield android/support/v4/media/TransportMediator/mController Landroid/support/v4/media/TransportMediatorJellybeanMR2;
ifnull L0
aload 0
getfield android/support/v4/media/TransportMediator/mController Landroid/support/v4/media/TransportMediatorJellybeanMR2;
invokevirtual android/support/v4/media/TransportMediatorJellybeanMR2/startPlaying()V
L0:
aload 0
getfield android/support/v4/media/TransportMediator/mCallbacks Landroid/support/v4/media/TransportPerformer;
invokevirtual android/support/v4/media/TransportPerformer/onStart()V
aload 0
invokespecial android/support/v4/media/TransportMediator/pushControllerState()V
aload 0
invokespecial android/support/v4/media/TransportMediator/reportPlayingChanged()V
return
.limit locals 1
.limit stack 1
.end method

.method public stopPlaying()V
aload 0
getfield android/support/v4/media/TransportMediator/mController Landroid/support/v4/media/TransportMediatorJellybeanMR2;
ifnull L0
aload 0
getfield android/support/v4/media/TransportMediator/mController Landroid/support/v4/media/TransportMediatorJellybeanMR2;
invokevirtual android/support/v4/media/TransportMediatorJellybeanMR2/stopPlaying()V
L0:
aload 0
getfield android/support/v4/media/TransportMediator/mCallbacks Landroid/support/v4/media/TransportPerformer;
invokevirtual android/support/v4/media/TransportPerformer/onStop()V
aload 0
invokespecial android/support/v4/media/TransportMediator/pushControllerState()V
aload 0
invokespecial android/support/v4/media/TransportMediator/reportPlayingChanged()V
return
.limit locals 1
.limit stack 1
.end method

.method public unregisterStateListener(Landroid/support/v4/media/TransportStateListener;)V
aload 0
getfield android/support/v4/media/TransportMediator/mListeners Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
return
.limit locals 2
.limit stack 2
.end method
