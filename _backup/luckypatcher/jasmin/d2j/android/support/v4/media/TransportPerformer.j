.bytecode 50.0
.class public synchronized abstract android/support/v4/media/TransportPerformer
.super java/lang/Object

.field static final 'AUDIOFOCUS_GAIN' I = 1


.field static final 'AUDIOFOCUS_GAIN_TRANSIENT' I = 2


.field static final 'AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK' I = 3


.field static final 'AUDIOFOCUS_LOSS' I = -1


.field static final 'AUDIOFOCUS_LOSS_TRANSIENT' I = -2


.field static final 'AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK' I = -3


.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public onAudioFocusChange(I)V
iconst_0
istore 2
iload 1
tableswitch -1
L0
default : L1
L1:
iload 2
istore 1
L2:
iload 1
ifeq L3
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 3
aload 0
iload 1
new android/view/KeyEvent
dup
lload 3
lload 3
iconst_0
iload 1
iconst_0
invokespecial android/view/KeyEvent/<init>(JJIII)V
invokevirtual android/support/v4/media/TransportPerformer/onMediaButtonDown(ILandroid/view/KeyEvent;)Z
pop
aload 0
iload 1
new android/view/KeyEvent
dup
lload 3
lload 3
iconst_1
iload 1
iconst_0
invokespecial android/view/KeyEvent/<init>(JJIII)V
invokevirtual android/support/v4/media/TransportPerformer/onMediaButtonUp(ILandroid/view/KeyEvent;)Z
pop
L3:
return
L0:
bipush 127
istore 1
goto L2
.limit locals 5
.limit stack 11
.end method

.method public onGetBufferPercentage()I
bipush 100
ireturn
.limit locals 1
.limit stack 1
.end method

.method public abstract onGetCurrentPosition()J
.end method

.method public abstract onGetDuration()J
.end method

.method public onGetTransportControlFlags()I
bipush 60
ireturn
.limit locals 1
.limit stack 1
.end method

.method public abstract onIsPlaying()Z
.end method

.method public onMediaButtonDown(ILandroid/view/KeyEvent;)Z
iload 1
lookupswitch
79 : L0
85 : L0
86 : L1
126 : L2
127 : L3
default : L4
L4:
iconst_1
ireturn
L2:
aload 0
invokevirtual android/support/v4/media/TransportPerformer/onStart()V
iconst_1
ireturn
L3:
aload 0
invokevirtual android/support/v4/media/TransportPerformer/onPause()V
iconst_1
ireturn
L1:
aload 0
invokevirtual android/support/v4/media/TransportPerformer/onStop()V
iconst_1
ireturn
L0:
aload 0
invokevirtual android/support/v4/media/TransportPerformer/onIsPlaying()Z
ifeq L5
aload 0
invokevirtual android/support/v4/media/TransportPerformer/onPause()V
iconst_1
ireturn
L5:
aload 0
invokevirtual android/support/v4/media/TransportPerformer/onStart()V
iconst_1
ireturn
.limit locals 3
.limit stack 1
.end method

.method public onMediaButtonUp(ILandroid/view/KeyEvent;)Z
iconst_1
ireturn
.limit locals 3
.limit stack 1
.end method

.method public abstract onPause()V
.end method

.method public abstract onSeekTo(J)V
.end method

.method public abstract onStart()V
.end method

.method public abstract onStop()V
.end method
