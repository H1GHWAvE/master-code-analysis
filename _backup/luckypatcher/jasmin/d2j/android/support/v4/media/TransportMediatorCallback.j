.bytecode 50.0
.class abstract interface android/support/v4/media/TransportMediatorCallback
.super java/lang/Object

.method public abstract getPlaybackPosition()J
.end method

.method public abstract handleAudioFocusChange(I)V
.end method

.method public abstract handleKey(Landroid/view/KeyEvent;)V
.end method

.method public abstract playbackPositionUpdate(J)V
.end method
