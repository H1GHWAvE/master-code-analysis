.bytecode 50.0
.class synchronized android/support/v4/media/TransportMediatorJellybeanMR2
.super java/lang/Object
.implements android/media/RemoteControlClient$OnGetPlaybackPositionListener
.implements android/media/RemoteControlClient$OnPlaybackPositionUpdateListener
.inner class inner android/support/v4/media/TransportMediatorJellybeanMR2$1
.inner class inner android/support/v4/media/TransportMediatorJellybeanMR2$2
.inner class inner android/support/v4/media/TransportMediatorJellybeanMR2$3
.inner class inner android/support/v4/media/TransportMediatorJellybeanMR2$4

.field 'mAudioFocusChangeListener' Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field 'mAudioFocused' Z

.field final 'mAudioManager' Landroid/media/AudioManager;

.field final 'mContext' Landroid/content/Context;

.field 'mFocused' Z

.field final 'mIntent' Landroid/content/Intent;

.field final 'mMediaButtonReceiver' Landroid/content/BroadcastReceiver;

.field 'mPendingIntent' Landroid/app/PendingIntent;

.field 'mPlayState' I

.field final 'mReceiverAction' Ljava/lang/String;

.field final 'mReceiverFilter' Landroid/content/IntentFilter;

.field 'mRemoteControl' Landroid/media/RemoteControlClient;

.field final 'mTargetView' Landroid/view/View;

.field final 'mTransportCallback' Landroid/support/v4/media/TransportMediatorCallback;

.field final 'mWindowAttachListener' Landroid/view/ViewTreeObserver$OnWindowAttachListener;

.field final 'mWindowFocusListener' Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;

.method public <init>(Landroid/content/Context;Landroid/media/AudioManager;Landroid/view/View;Landroid/support/v4/media/TransportMediatorCallback;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new android/support/v4/media/TransportMediatorJellybeanMR2$1
dup
aload 0
invokespecial android/support/v4/media/TransportMediatorJellybeanMR2$1/<init>(Landroid/support/v4/media/TransportMediatorJellybeanMR2;)V
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mWindowAttachListener Landroid/view/ViewTreeObserver$OnWindowAttachListener;
aload 0
new android/support/v4/media/TransportMediatorJellybeanMR2$2
dup
aload 0
invokespecial android/support/v4/media/TransportMediatorJellybeanMR2$2/<init>(Landroid/support/v4/media/TransportMediatorJellybeanMR2;)V
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mWindowFocusListener Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;
aload 0
new android/support/v4/media/TransportMediatorJellybeanMR2$3
dup
aload 0
invokespecial android/support/v4/media/TransportMediatorJellybeanMR2$3/<init>(Landroid/support/v4/media/TransportMediatorJellybeanMR2;)V
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mMediaButtonReceiver Landroid/content/BroadcastReceiver;
aload 0
new android/support/v4/media/TransportMediatorJellybeanMR2$4
dup
aload 0
invokespecial android/support/v4/media/TransportMediatorJellybeanMR2$4/<init>(Landroid/support/v4/media/TransportMediatorJellybeanMR2;)V
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mAudioFocusChangeListener Landroid/media/AudioManager$OnAudioFocusChangeListener;
aload 0
iconst_0
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mPlayState I
aload 0
aload 1
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mContext Landroid/content/Context;
aload 0
aload 2
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mAudioManager Landroid/media/AudioManager;
aload 0
aload 3
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mTargetView Landroid/view/View;
aload 0
aload 4
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mTransportCallback Landroid/support/v4/media/TransportMediatorCallback;
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":transport:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokestatic java/lang/System/identityHashCode(Ljava/lang/Object;)I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mReceiverAction Ljava/lang/String;
aload 0
new android/content/Intent
dup
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mReceiverAction Ljava/lang/String;
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mIntent Landroid/content/Intent;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mIntent Landroid/content/Intent;
aload 1
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual android/content/Intent/setPackage(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
new android/content/IntentFilter
dup
invokespecial android/content/IntentFilter/<init>()V
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mReceiverFilter Landroid/content/IntentFilter;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mReceiverFilter Landroid/content/IntentFilter;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mReceiverAction Ljava/lang/String;
invokevirtual android/content/IntentFilter/addAction(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mTargetView Landroid/view/View;
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mWindowAttachListener Landroid/view/ViewTreeObserver$OnWindowAttachListener;
invokevirtual android/view/ViewTreeObserver/addOnWindowAttachListener(Landroid/view/ViewTreeObserver$OnWindowAttachListener;)V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mTargetView Landroid/view/View;
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mWindowFocusListener Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;
invokevirtual android/view/ViewTreeObserver/addOnWindowFocusChangeListener(Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;)V
return
.limit locals 5
.limit stack 4
.end method

.method public destroy()V
aload 0
invokevirtual android/support/v4/media/TransportMediatorJellybeanMR2/windowDetached()V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mTargetView Landroid/view/View;
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mWindowAttachListener Landroid/view/ViewTreeObserver$OnWindowAttachListener;
invokevirtual android/view/ViewTreeObserver/removeOnWindowAttachListener(Landroid/view/ViewTreeObserver$OnWindowAttachListener;)V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mTargetView Landroid/view/View;
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mWindowFocusListener Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;
invokevirtual android/view/ViewTreeObserver/removeOnWindowFocusChangeListener(Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;)V
return
.limit locals 1
.limit stack 2
.end method

.method dropAudioFocus()V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mAudioFocused Z
ifeq L0
aload 0
iconst_0
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mAudioFocused Z
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mAudioManager Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mAudioFocusChangeListener Landroid/media/AudioManager$OnAudioFocusChangeListener;
invokevirtual android/media/AudioManager/abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I
pop
L0:
return
.limit locals 1
.limit stack 2
.end method

.method gainFocus()V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mFocused Z
ifne L0
aload 0
iconst_1
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mFocused Z
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mAudioManager Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mPendingIntent Landroid/app/PendingIntent;
invokevirtual android/media/AudioManager/registerMediaButtonEventReceiver(Landroid/app/PendingIntent;)V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mAudioManager Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mRemoteControl Landroid/media/RemoteControlClient;
invokevirtual android/media/AudioManager/registerRemoteControlClient(Landroid/media/RemoteControlClient;)V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mPlayState I
iconst_3
if_icmpne L0
aload 0
invokevirtual android/support/v4/media/TransportMediatorJellybeanMR2/takeAudioFocus()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public getRemoteControlClient()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mRemoteControl Landroid/media/RemoteControlClient;
areturn
.limit locals 1
.limit stack 1
.end method

.method loseFocus()V
aload 0
invokevirtual android/support/v4/media/TransportMediatorJellybeanMR2/dropAudioFocus()V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mFocused Z
ifeq L0
aload 0
iconst_0
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mFocused Z
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mAudioManager Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mRemoteControl Landroid/media/RemoteControlClient;
invokevirtual android/media/AudioManager/unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mAudioManager Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mPendingIntent Landroid/app/PendingIntent;
invokevirtual android/media/AudioManager/unregisterMediaButtonEventReceiver(Landroid/app/PendingIntent;)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public onGetPlaybackPosition()J
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mTransportCallback Landroid/support/v4/media/TransportMediatorCallback;
invokeinterface android/support/v4/media/TransportMediatorCallback/getPlaybackPosition()J 0
lreturn
.limit locals 1
.limit stack 2
.end method

.method public onPlaybackPositionUpdate(J)V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mTransportCallback Landroid/support/v4/media/TransportMediatorCallback;
lload 1
invokeinterface android/support/v4/media/TransportMediatorCallback/playbackPositionUpdate(J)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public pausePlaying()V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mPlayState I
iconst_3
if_icmpne L0
aload 0
iconst_2
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mPlayState I
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mRemoteControl Landroid/media/RemoteControlClient;
iconst_2
invokevirtual android/media/RemoteControlClient/setPlaybackState(I)V
L0:
aload 0
invokevirtual android/support/v4/media/TransportMediatorJellybeanMR2/dropAudioFocus()V
return
.limit locals 1
.limit stack 2
.end method

.method public refreshState(ZJI)V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mRemoteControl Landroid/media/RemoteControlClient;
ifnull L0
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mRemoteControl Landroid/media/RemoteControlClient;
astore 7
iload 1
ifeq L1
iconst_3
istore 6
L2:
iload 1
ifeq L3
fconst_1
fstore 5
L4:
aload 7
iload 6
lload 2
fload 5
invokevirtual android/media/RemoteControlClient/setPlaybackState(IJF)V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mRemoteControl Landroid/media/RemoteControlClient;
iload 4
invokevirtual android/media/RemoteControlClient/setTransportControlFlags(I)V
L0:
return
L1:
iconst_1
istore 6
goto L2
L3:
fconst_0
fstore 5
goto L4
.limit locals 8
.limit stack 5
.end method

.method public startPlaying()V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mPlayState I
iconst_3
if_icmpeq L0
aload 0
iconst_3
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mPlayState I
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mRemoteControl Landroid/media/RemoteControlClient;
iconst_3
invokevirtual android/media/RemoteControlClient/setPlaybackState(I)V
L0:
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mFocused Z
ifeq L1
aload 0
invokevirtual android/support/v4/media/TransportMediatorJellybeanMR2/takeAudioFocus()V
L1:
return
.limit locals 1
.limit stack 2
.end method

.method public stopPlaying()V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mPlayState I
iconst_1
if_icmpeq L0
aload 0
iconst_1
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mPlayState I
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mRemoteControl Landroid/media/RemoteControlClient;
iconst_1
invokevirtual android/media/RemoteControlClient/setPlaybackState(I)V
L0:
aload 0
invokevirtual android/support/v4/media/TransportMediatorJellybeanMR2/dropAudioFocus()V
return
.limit locals 1
.limit stack 2
.end method

.method takeAudioFocus()V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mAudioFocused Z
ifne L0
aload 0
iconst_1
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mAudioFocused Z
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mAudioManager Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mAudioFocusChangeListener Landroid/media/AudioManager$OnAudioFocusChangeListener;
iconst_3
iconst_1
invokevirtual android/media/AudioManager/requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I
pop
L0:
return
.limit locals 1
.limit stack 4
.end method

.method windowAttached()V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mContext Landroid/content/Context;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mMediaButtonReceiver Landroid/content/BroadcastReceiver;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mReceiverFilter Landroid/content/IntentFilter;
invokevirtual android/content/Context/registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
pop
aload 0
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mContext Landroid/content/Context;
iconst_0
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mIntent Landroid/content/Intent;
ldc_w 268435456
invokestatic android/app/PendingIntent/getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mPendingIntent Landroid/app/PendingIntent;
aload 0
new android/media/RemoteControlClient
dup
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mPendingIntent Landroid/app/PendingIntent;
invokespecial android/media/RemoteControlClient/<init>(Landroid/app/PendingIntent;)V
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mRemoteControl Landroid/media/RemoteControlClient;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mRemoteControl Landroid/media/RemoteControlClient;
aload 0
invokevirtual android/media/RemoteControlClient/setOnGetPlaybackPositionListener(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mRemoteControl Landroid/media/RemoteControlClient;
aload 0
invokevirtual android/media/RemoteControlClient/setPlaybackPositionUpdateListener(Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;)V
return
.limit locals 1
.limit stack 5
.end method

.method windowDetached()V
aload 0
invokevirtual android/support/v4/media/TransportMediatorJellybeanMR2/loseFocus()V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mPendingIntent Landroid/app/PendingIntent;
ifnull L0
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mContext Landroid/content/Context;
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mMediaButtonReceiver Landroid/content/BroadcastReceiver;
invokevirtual android/content/Context/unregisterReceiver(Landroid/content/BroadcastReceiver;)V
aload 0
getfield android/support/v4/media/TransportMediatorJellybeanMR2/mPendingIntent Landroid/app/PendingIntent;
invokevirtual android/app/PendingIntent/cancel()V
aload 0
aconst_null
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mPendingIntent Landroid/app/PendingIntent;
aload 0
aconst_null
putfield android/support/v4/media/TransportMediatorJellybeanMR2/mRemoteControl Landroid/media/RemoteControlClient;
L0:
return
.limit locals 1
.limit stack 2
.end method
