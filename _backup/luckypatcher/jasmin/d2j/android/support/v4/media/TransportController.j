.bytecode 50.0
.class public synchronized abstract android/support/v4/media/TransportController
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public abstract getBufferPercentage()I
.end method

.method public abstract getCurrentPosition()J
.end method

.method public abstract getDuration()J
.end method

.method public abstract getTransportControlFlags()I
.end method

.method public abstract isPlaying()Z
.end method

.method public abstract pausePlaying()V
.end method

.method public abstract registerStateListener(Landroid/support/v4/media/TransportStateListener;)V
.end method

.method public abstract seekTo(J)V
.end method

.method public abstract startPlaying()V
.end method

.method public abstract stopPlaying()V
.end method

.method public abstract unregisterStateListener(Landroid/support/v4/media/TransportStateListener;)V
.end method
