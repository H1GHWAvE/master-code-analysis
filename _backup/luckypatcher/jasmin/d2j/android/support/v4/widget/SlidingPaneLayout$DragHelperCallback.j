.bytecode 50.0
.class synchronized android/support/v4/widget/SlidingPaneLayout$DragHelperCallback
.super android/support/v4/widget/ViewDragHelper$Callback
.inner class private DragHelperCallback inner android/support/v4/widget/SlidingPaneLayout$DragHelperCallback outer android/support/v4/widget/SlidingPaneLayout

.field final synthetic 'this$0' Landroid/support/v4/widget/SlidingPaneLayout;

.method private <init>(Landroid/support/v4/widget/SlidingPaneLayout;)V
aload 0
aload 1
putfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
aload 0
invokespecial android/support/v4/widget/ViewDragHelper$Callback/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Landroid/support/v4/widget/SlidingPaneLayout;Landroid/support/v4/widget/SlidingPaneLayout$1;)V
aload 0
aload 1
invokespecial android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/<init>(Landroid/support/v4/widget/SlidingPaneLayout;)V
return
.limit locals 3
.limit stack 2
.end method

.method public clampViewPositionHorizontal(Landroid/view/View;II)I
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/access$400(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/SlidingPaneLayout$LayoutParams
astore 1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
aload 1
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/leftMargin I
iadd
istore 3
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/access$700(Landroid/support/v4/widget/SlidingPaneLayout;)I
istore 4
iload 2
iload 3
invokestatic java/lang/Math/max(II)I
iload 3
iload 4
iadd
invokestatic java/lang/Math/min(II)I
ireturn
.limit locals 5
.limit stack 3
.end method

.method public getViewHorizontalDragRange(Landroid/view/View;)I
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/access$700(Landroid/support/v4/widget/SlidingPaneLayout;)I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public onEdgeDragStarted(II)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/access$200(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/support/v4/widget/ViewDragHelper;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/access$400(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/view/View;
iload 2
invokevirtual android/support/v4/widget/ViewDragHelper/captureChildView(Landroid/view/View;I)V
return
.limit locals 3
.limit stack 3
.end method

.method public onViewCaptured(Landroid/view/View;I)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/setAllChildrenVisible()V
return
.limit locals 3
.limit stack 1
.end method

.method public onViewDragStateChanged(I)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/access$200(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/getViewDragState()I
ifne L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/access$300(Landroid/support/v4/widget/SlidingPaneLayout;)F
fconst_0
fcmpl
ifne L1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/access$400(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/view/View;
invokevirtual android/support/v4/widget/SlidingPaneLayout/updateObscuredViewsVisibility(Landroid/view/View;)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/access$400(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/view/View;
invokevirtual android/support/v4/widget/SlidingPaneLayout/dispatchOnPanelClosed(Landroid/view/View;)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
iconst_0
invokestatic android/support/v4/widget/SlidingPaneLayout/access$502(Landroid/support/v4/widget/SlidingPaneLayout;Z)Z
pop
L0:
return
L1:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/access$400(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/view/View;
invokevirtual android/support/v4/widget/SlidingPaneLayout/dispatchOnPanelOpened(Landroid/view/View;)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
iconst_1
invokestatic android/support/v4/widget/SlidingPaneLayout/access$502(Landroid/support/v4/widget/SlidingPaneLayout;Z)Z
pop
return
.limit locals 2
.limit stack 2
.end method

.method public onViewPositionChanged(Landroid/view/View;IIII)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
iload 2
invokestatic android/support/v4/widget/SlidingPaneLayout/access$600(Landroid/support/v4/widget/SlidingPaneLayout;I)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/invalidate()V
return
.limit locals 6
.limit stack 2
.end method

.method public onViewReleased(Landroid/view/View;FF)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/SlidingPaneLayout$LayoutParams
astore 6
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
aload 6
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/leftMargin I
iadd
istore 5
fload 2
fconst_0
fcmpl
ifgt L0
iload 5
istore 4
fload 2
fconst_0
fcmpl
ifne L1
iload 5
istore 4
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/access$300(Landroid/support/v4/widget/SlidingPaneLayout;)F
ldc_w 0.5F
fcmpl
ifle L1
L0:
iload 5
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/access$700(Landroid/support/v4/widget/SlidingPaneLayout;)I
iadd
istore 4
L1:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/access$200(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/support/v4/widget/ViewDragHelper;
iload 4
aload 1
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/ViewDragHelper/settleCapturedViewAt(II)Z
pop
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/invalidate()V
return
.limit locals 7
.limit stack 3
.end method

.method public tryCaptureView(Landroid/view/View;I)Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/access$100(Landroid/support/v4/widget/SlidingPaneLayout;)Z
ifeq L0
iconst_0
ireturn
L0:
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/SlidingPaneLayout$LayoutParams
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/slideable Z
ireturn
.limit locals 3
.limit stack 1
.end method
