.bytecode 50.0
.class public synchronized android/support/v4/widget/DrawerLayout$SavedState
.super android/view/View$BaseSavedState
.inner class protected static SavedState inner android/support/v4/widget/DrawerLayout$SavedState outer android/support/v4/widget/DrawerLayout
.inner class static final inner android/support/v4/widget/DrawerLayout$SavedState$1

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator; signature "Landroid/os/Parcelable$Creator<Landroid/support/v4/widget/DrawerLayout$SavedState;>;"

.field 'lockModeLeft' I

.field 'lockModeRight' I

.field 'openDrawerGravity' I

.method static <clinit>()V
new android/support/v4/widget/DrawerLayout$SavedState$1
dup
invokespecial android/support/v4/widget/DrawerLayout$SavedState$1/<init>()V
putstatic android/support/v4/widget/DrawerLayout$SavedState/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Landroid/os/Parcel;)V
aload 0
aload 1
invokespecial android/view/View$BaseSavedState/<init>(Landroid/os/Parcel;)V
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout$SavedState/openDrawerGravity I
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout$SavedState/lockModeLeft I
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout$SavedState/lockModeRight I
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/widget/DrawerLayout$SavedState/openDrawerGravity I
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/os/Parcelable;)V
aload 0
aload 1
invokespecial android/view/View$BaseSavedState/<init>(Landroid/os/Parcelable;)V
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout$SavedState/openDrawerGravity I
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout$SavedState/lockModeLeft I
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout$SavedState/lockModeRight I
return
.limit locals 2
.limit stack 2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
aload 0
aload 1
iload 2
invokespecial android/view/View$BaseSavedState/writeToParcel(Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v4/widget/DrawerLayout$SavedState/openDrawerGravity I
invokevirtual android/os/Parcel/writeInt(I)V
return
.limit locals 3
.limit stack 3
.end method
