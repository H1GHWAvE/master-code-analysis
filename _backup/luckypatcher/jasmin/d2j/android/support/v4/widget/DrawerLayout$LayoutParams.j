.bytecode 50.0
.class public synchronized android/support/v4/widget/DrawerLayout$LayoutParams
.super android/view/ViewGroup$MarginLayoutParams
.inner class public static LayoutParams inner android/support/v4/widget/DrawerLayout$LayoutParams outer android/support/v4/widget/DrawerLayout

.field public 'gravity' I

.field 'isPeeking' Z

.field 'knownOpen' Z

.field 'onScreen' F

.method public <init>(II)V
aload 0
iload 1
iload 2
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(II)V
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout$LayoutParams/gravity I
return
.limit locals 3
.limit stack 3
.end method

.method public <init>(III)V
aload 0
iload 1
iload 2
invokespecial android/support/v4/widget/DrawerLayout$LayoutParams/<init>(II)V
aload 0
iload 3
putfield android/support/v4/widget/DrawerLayout$LayoutParams/gravity I
return
.limit locals 4
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout$LayoutParams/gravity I
aload 1
aload 2
invokestatic android/support/v4/widget/DrawerLayout/access$100()[I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
iconst_0
iconst_0
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/v4/widget/DrawerLayout$LayoutParams/gravity I
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/support/v4/widget/DrawerLayout$LayoutParams;)V
aload 0
aload 1
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout$LayoutParams/gravity I
aload 0
aload 1
getfield android/support/v4/widget/DrawerLayout$LayoutParams/gravity I
putfield android/support/v4/widget/DrawerLayout$LayoutParams/gravity I
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 1
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout$LayoutParams/gravity I
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
aload 1
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout$LayoutParams/gravity I
return
.limit locals 2
.limit stack 2
.end method
