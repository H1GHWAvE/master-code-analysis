.bytecode 50.0
.class synchronized android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB
.super android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplBase
.inner class static SlidingPanelLayoutImplJB inner android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB outer android/support/v4/widget/SlidingPaneLayout

.field private 'mGetDisplayList' Ljava/lang/reflect/Method;

.field private 'mRecreateDisplayList' Ljava/lang/reflect/Field;

.method <init>()V
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
.catch java/lang/NoSuchFieldException from L1 to L3 using L4
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplBase/<init>()V
L0:
aload 0
ldc android/view/View
ldc "getDisplayList"
aconst_null
checkcast [Ljava/lang/Class;
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putfield android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB/mGetDisplayList Ljava/lang/reflect/Method;
L1:
aload 0
ldc android/view/View
ldc "mRecreateDisplayList"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
putfield android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB/mRecreateDisplayList Ljava/lang/reflect/Field;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB/mRecreateDisplayList Ljava/lang/reflect/Field;
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
L3:
return
L2:
astore 1
ldc "SlidingPaneLayout"
ldc "Couldn't fetch getDisplayList method; dimming won't work right."
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
L4:
astore 1
ldc "SlidingPaneLayout"
ldc "Couldn't fetch mRecreateDisplayList field; dimming will be slow."
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 2
.limit stack 4
.end method

.method public invalidateChildRegion(Landroid/support/v4/widget/SlidingPaneLayout;Landroid/view/View;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB/mGetDisplayList Ljava/lang/reflect/Method;
ifnull L3
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB/mRecreateDisplayList Ljava/lang/reflect/Field;
ifnull L3
L0:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB/mRecreateDisplayList Ljava/lang/reflect/Field;
aload 2
iconst_1
invokevirtual java/lang/reflect/Field/setBoolean(Ljava/lang/Object;Z)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB/mGetDisplayList Ljava/lang/reflect/Method;
aload 2
aconst_null
checkcast [Ljava/lang/Object;
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
aload 0
aload 1
aload 2
invokespecial android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplBase/invalidateChildRegion(Landroid/support/v4/widget/SlidingPaneLayout;Landroid/view/View;)V
return
L2:
astore 3
ldc "SlidingPaneLayout"
ldc "Error refreshing display list state"
aload 3
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
L3:
aload 2
invokevirtual android/view/View/invalidate()V
return
.limit locals 4
.limit stack 3
.end method
