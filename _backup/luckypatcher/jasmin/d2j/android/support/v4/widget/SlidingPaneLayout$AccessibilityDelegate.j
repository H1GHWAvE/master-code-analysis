.bytecode 50.0
.class synchronized android/support/v4/widget/SlidingPaneLayout$AccessibilityDelegate
.super android/support/v4/view/AccessibilityDelegateCompat
.inner class AccessibilityDelegate inner android/support/v4/widget/SlidingPaneLayout$AccessibilityDelegate outer android/support/v4/widget/SlidingPaneLayout

.field private final 'mTmpRect' Landroid/graphics/Rect;

.field final synthetic 'this$0' Landroid/support/v4/widget/SlidingPaneLayout;

.method <init>(Landroid/support/v4/widget/SlidingPaneLayout;)V
aload 0
aload 1
putfield android/support/v4/widget/SlidingPaneLayout$AccessibilityDelegate/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
aload 0
invokespecial android/support/v4/view/AccessibilityDelegateCompat/<init>()V
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/widget/SlidingPaneLayout$AccessibilityDelegate/mTmpRect Landroid/graphics/Rect;
return
.limit locals 2
.limit stack 3
.end method

.method private copyNodeInfoNoChildren(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$AccessibilityDelegate/mTmpRect Landroid/graphics/Rect;
astore 3
aload 2
aload 3
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/getBoundsInParent(Landroid/graphics/Rect;)V
aload 1
aload 3
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setBoundsInParent(Landroid/graphics/Rect;)V
aload 2
aload 3
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/getBoundsInScreen(Landroid/graphics/Rect;)V
aload 1
aload 3
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setBoundsInScreen(Landroid/graphics/Rect;)V
aload 1
aload 2
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/isVisibleToUser()Z
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setVisibleToUser(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/getPackageName()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setPackageName(Ljava/lang/CharSequence;)V
aload 1
aload 2
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/getClassName()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setClassName(Ljava/lang/CharSequence;)V
aload 1
aload 2
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/getContentDescription()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setContentDescription(Ljava/lang/CharSequence;)V
aload 1
aload 2
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/isEnabled()Z
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setEnabled(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/isClickable()Z
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setClickable(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/isFocusable()Z
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setFocusable(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/isFocused()Z
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setFocused(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/isAccessibilityFocused()Z
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setAccessibilityFocused(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/isSelected()Z
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setSelected(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/isLongClickable()Z
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setLongClickable(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/getActions()I
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/addAction(I)V
aload 1
aload 2
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/getMovementGranularities()I
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setMovementGranularities(I)V
return
.limit locals 4
.limit stack 2
.end method

.method public filter(Landroid/view/View;)Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$AccessibilityDelegate/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
aload 1
invokevirtual android/support/v4/widget/SlidingPaneLayout/isDimmed(Landroid/view/View;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
aload 1
aload 2
invokespecial android/support/v4/view/AccessibilityDelegateCompat/onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 2
ldc android/support/v4/widget/SlidingPaneLayout
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setClassName(Ljava/lang/CharSequence;)V
return
.limit locals 3
.limit stack 3
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
aload 2
invokestatic android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
astore 5
aload 0
aload 1
aload 5
invokespecial android/support/v4/view/AccessibilityDelegateCompat/onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
aload 0
aload 2
aload 5
invokespecial android/support/v4/widget/SlidingPaneLayout$AccessibilityDelegate/copyNodeInfoNoChildren(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
aload 5
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/recycle()V
aload 2
ldc android/support/v4/widget/SlidingPaneLayout
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setClassName(Ljava/lang/CharSequence;)V
aload 2
aload 1
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setSource(Landroid/view/View;)V
aload 1
invokestatic android/support/v4/view/ViewCompat/getParentForAccessibility(Landroid/view/View;)Landroid/view/ViewParent;
astore 1
aload 1
instanceof android/view/View
ifeq L0
aload 2
aload 1
checkcast android/view/View
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setParent(Landroid/view/View;)V
L0:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$AccessibilityDelegate/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
istore 4
iconst_0
istore 3
L1:
iload 3
iload 4
if_icmpge L2
aload 0
getfield android/support/v4/widget/SlidingPaneLayout$AccessibilityDelegate/this$0 Landroid/support/v4/widget/SlidingPaneLayout;
iload 3
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 1
aload 0
aload 1
invokevirtual android/support/v4/widget/SlidingPaneLayout$AccessibilityDelegate/filter(Landroid/view/View;)Z
ifne L3
aload 1
invokevirtual android/view/View/getVisibility()I
ifne L3
aload 1
iconst_1
invokestatic android/support/v4/view/ViewCompat/setImportantForAccessibility(Landroid/view/View;I)V
aload 2
aload 1
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/addChild(Landroid/view/View;)V
L3:
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
return
.limit locals 6
.limit stack 3
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
aload 0
aload 2
invokevirtual android/support/v4/widget/SlidingPaneLayout$AccessibilityDelegate/filter(Landroid/view/View;)Z
ifne L0
aload 0
aload 1
aload 2
aload 3
invokespecial android/support/v4/view/AccessibilityDelegateCompat/onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 4
.end method
