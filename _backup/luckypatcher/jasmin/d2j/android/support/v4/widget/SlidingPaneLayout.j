.bytecode 50.0
.class public synchronized android/support/v4/widget/SlidingPaneLayout
.super android/view/ViewGroup
.inner class static synthetic inner android/support/v4/widget/SlidingPaneLayout$1
.inner class AccessibilityDelegate inner android/support/v4/widget/SlidingPaneLayout$AccessibilityDelegate outer android/support/v4/widget/SlidingPaneLayout
.inner class private DisableLayerRunnable inner android/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable outer android/support/v4/widget/SlidingPaneLayout
.inner class private DragHelperCallback inner android/support/v4/widget/SlidingPaneLayout$DragHelperCallback outer android/support/v4/widget/SlidingPaneLayout
.inner class public static LayoutParams inner android/support/v4/widget/SlidingPaneLayout$LayoutParams outer android/support/v4/widget/SlidingPaneLayout
.inner class public static abstract interface PanelSlideListener inner android/support/v4/widget/SlidingPaneLayout$PanelSlideListener outer android/support/v4/widget/SlidingPaneLayout
.inner class static SavedState inner android/support/v4/widget/SlidingPaneLayout$SavedState outer android/support/v4/widget/SlidingPaneLayout
.inner class static final inner android/support/v4/widget/SlidingPaneLayout$SavedState$1
.inner class public static SimplePanelSlideListener inner android/support/v4/widget/SlidingPaneLayout$SimplePanelSlideListener outer android/support/v4/widget/SlidingPaneLayout
.inner class static abstract interface SlidingPanelLayoutImpl inner android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImpl outer android/support/v4/widget/SlidingPaneLayout
.inner class static SlidingPanelLayoutImplBase inner android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplBase outer android/support/v4/widget/SlidingPaneLayout
.inner class static SlidingPanelLayoutImplJB inner android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB outer android/support/v4/widget/SlidingPaneLayout
.inner class static SlidingPanelLayoutImplJBMR1 inner android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJBMR1 outer android/support/v4/widget/SlidingPaneLayout

.field private static final 'DEFAULT_FADE_COLOR' I = -858993460


.field private static final 'DEFAULT_OVERHANG_SIZE' I = 32


.field static final 'IMPL' Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImpl;

.field private static final 'MIN_FLING_VELOCITY' I = 400


.field private static final 'TAG' Ljava/lang/String; = "SlidingPaneLayout"

.field private 'mCanSlide' Z

.field private 'mCoveredFadeColor' I

.field private final 'mDragHelper' Landroid/support/v4/widget/ViewDragHelper;

.field private 'mFirstLayout' Z

.field private 'mInitialMotionX' F

.field private 'mInitialMotionY' F

.field private 'mIsUnableToDrag' Z

.field private final 'mOverhangSize' I

.field private 'mPanelSlideListener' Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;

.field private 'mParallaxBy' I

.field private 'mParallaxOffset' F

.field private final 'mPostedRunnables' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Landroid/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable;>;"

.field private 'mPreservedOpenState' Z

.field private 'mShadowDrawable' Landroid/graphics/drawable/Drawable;

.field private 'mSlideOffset' F

.field private 'mSlideRange' I

.field private 'mSlideableView' Landroid/view/View;

.field private 'mSliderFadeColor' I

.field private final 'mTmpRect' Landroid/graphics/Rect;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
istore 0
iload 0
bipush 17
if_icmplt L0
new android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJBMR1
dup
invokespecial android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJBMR1/<init>()V
putstatic android/support/v4/widget/SlidingPaneLayout/IMPL Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImpl;
return
L0:
iload 0
bipush 16
if_icmplt L1
new android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB
dup
invokespecial android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplJB/<init>()V
putstatic android/support/v4/widget/SlidingPaneLayout/IMPL Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImpl;
return
L1:
new android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplBase
dup
invokespecial android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImplBase/<init>()V
putstatic android/support/v4/widget/SlidingPaneLayout/IMPL Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImpl;
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v4/widget/SlidingPaneLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/v4/widget/SlidingPaneLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
ldc_w -858993460
putfield android/support/v4/widget/SlidingPaneLayout/mSliderFadeColor I
aload 0
iconst_1
putfield android/support/v4/widget/SlidingPaneLayout/mFirstLayout Z
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/widget/SlidingPaneLayout/mTmpRect Landroid/graphics/Rect;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/widget/SlidingPaneLayout/mPostedRunnables Ljava/util/ArrayList;
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fstore 4
aload 0
ldc_w 32.0F
fload 4
fmul
ldc_w 0.5F
fadd
f2i
putfield android/support/v4/widget/SlidingPaneLayout/mOverhangSize I
aload 1
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
pop
aload 0
iconst_0
invokevirtual android/support/v4/widget/SlidingPaneLayout/setWillNotDraw(Z)V
aload 0
new android/support/v4/widget/SlidingPaneLayout$AccessibilityDelegate
dup
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout$AccessibilityDelegate/<init>(Landroid/support/v4/widget/SlidingPaneLayout;)V
invokestatic android/support/v4/view/ViewCompat/setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V
aload 0
iconst_1
invokestatic android/support/v4/view/ViewCompat/setImportantForAccessibility(Landroid/view/View;I)V
aload 0
aload 0
ldc_w 0.5F
new android/support/v4/widget/SlidingPaneLayout$DragHelperCallback
dup
aload 0
aconst_null
invokespecial android/support/v4/widget/SlidingPaneLayout$DragHelperCallback/<init>(Landroid/support/v4/widget/SlidingPaneLayout;Landroid/support/v4/widget/SlidingPaneLayout$1;)V
invokestatic android/support/v4/widget/ViewDragHelper/create(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ViewDragHelper$Callback;)Landroid/support/v4/widget/ViewDragHelper;
putfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
iconst_1
invokevirtual android/support/v4/widget/ViewDragHelper/setEdgeTrackingEnabled(I)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
ldc_w 400.0F
fload 4
fmul
invokevirtual android/support/v4/widget/ViewDragHelper/setMinVelocity(F)V
return
.limit locals 5
.limit stack 7
.end method

.method static synthetic access$100(Landroid/support/v4/widget/SlidingPaneLayout;)Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mIsUnableToDrag Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$1000(Landroid/support/v4/widget/SlidingPaneLayout;)Ljava/util/ArrayList;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mPostedRunnables Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$200(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/support/v4/widget/ViewDragHelper;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$300(Landroid/support/v4/widget/SlidingPaneLayout;)F
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideOffset F
freturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$400(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/view/View;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$502(Landroid/support/v4/widget/SlidingPaneLayout;Z)Z
aload 0
iload 1
putfield android/support/v4/widget/SlidingPaneLayout/mPreservedOpenState Z
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$600(Landroid/support/v4/widget/SlidingPaneLayout;I)V
aload 0
iload 1
invokespecial android/support/v4/widget/SlidingPaneLayout/onPanelDragged(I)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$700(Landroid/support/v4/widget/SlidingPaneLayout;)I
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideRange I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$900(Landroid/support/v4/widget/SlidingPaneLayout;Landroid/view/View;)V
aload 0
aload 1
invokespecial android/support/v4/widget/SlidingPaneLayout/invalidateChildRegion(Landroid/view/View;)V
return
.limit locals 2
.limit stack 2
.end method

.method private closePane(Landroid/view/View;I)Z
iconst_0
istore 3
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mFirstLayout Z
ifne L0
aload 0
fconst_0
iload 2
invokevirtual android/support/v4/widget/SlidingPaneLayout/smoothSlideTo(FI)Z
ifeq L1
L0:
aload 0
iconst_0
putfield android/support/v4/widget/SlidingPaneLayout/mPreservedOpenState Z
iconst_1
istore 3
L1:
iload 3
ireturn
.limit locals 4
.limit stack 3
.end method

.method private dimChildView(Landroid/view/View;FI)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/SlidingPaneLayout$LayoutParams
astore 5
fload 2
fconst_0
fcmpl
ifle L0
iload 3
ifeq L0
ldc_w -16777216
iload 3
iand
bipush 24
iushr
i2f
fload 2
fmul
f2i
istore 4
aload 5
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/dimPaint Landroid/graphics/Paint;
ifnonnull L1
aload 5
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
putfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/dimPaint Landroid/graphics/Paint;
L1:
aload 5
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/dimPaint Landroid/graphics/Paint;
new android/graphics/PorterDuffColorFilter
dup
iload 4
bipush 24
ishl
ldc_w 16777215
iload 3
iand
ior
getstatic android/graphics/PorterDuff$Mode/SRC_OVER Landroid/graphics/PorterDuff$Mode;
invokespecial android/graphics/PorterDuffColorFilter/<init>(ILandroid/graphics/PorterDuff$Mode;)V
invokevirtual android/graphics/Paint/setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
pop
aload 1
invokestatic android/support/v4/view/ViewCompat/getLayerType(Landroid/view/View;)I
iconst_2
if_icmpeq L2
aload 1
iconst_2
aload 5
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/dimPaint Landroid/graphics/Paint;
invokestatic android/support/v4/view/ViewCompat/setLayerType(Landroid/view/View;ILandroid/graphics/Paint;)V
L2:
aload 0
aload 1
invokespecial android/support/v4/widget/SlidingPaneLayout/invalidateChildRegion(Landroid/view/View;)V
L3:
return
L0:
aload 1
invokestatic android/support/v4/view/ViewCompat/getLayerType(Landroid/view/View;)I
ifeq L3
aload 5
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/dimPaint Landroid/graphics/Paint;
ifnull L4
aload 5
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/dimPaint Landroid/graphics/Paint;
aconst_null
invokevirtual android/graphics/Paint/setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
pop
L4:
new android/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable
dup
aload 0
aload 1
invokespecial android/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable/<init>(Landroid/support/v4/widget/SlidingPaneLayout;Landroid/view/View;)V
astore 1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mPostedRunnables Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
aload 1
invokestatic android/support/v4/view/ViewCompat/postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V
return
.limit locals 6
.limit stack 6
.end method

.method private invalidateChildRegion(Landroid/view/View;)V
getstatic android/support/v4/widget/SlidingPaneLayout/IMPL Landroid/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImpl;
aload 0
aload 1
invokeinterface android/support/v4/widget/SlidingPaneLayout$SlidingPanelLayoutImpl/invalidateChildRegion(Landroid/support/v4/widget/SlidingPaneLayout;Landroid/view/View;)V 2
return
.limit locals 2
.limit stack 3
.end method

.method private onPanelDragged(I)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/SlidingPaneLayout$LayoutParams
astore 2
aload 0
iload 1
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
aload 2
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/leftMargin I
iadd
isub
i2f
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideRange I
i2f
fdiv
putfield android/support/v4/widget/SlidingPaneLayout/mSlideOffset F
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mParallaxBy I
ifeq L0
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideOffset F
invokespecial android/support/v4/widget/SlidingPaneLayout/parallaxOtherViews(F)V
L0:
aload 2
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/dimWhenOffset Z
ifeq L1
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideOffset F
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSliderFadeColor I
invokespecial android/support/v4/widget/SlidingPaneLayout/dimChildView(Landroid/view/View;FI)V
L1:
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
invokevirtual android/support/v4/widget/SlidingPaneLayout/dispatchOnPanelSlide(Landroid/view/View;)V
return
.limit locals 3
.limit stack 4
.end method

.method private openPane(Landroid/view/View;I)Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mFirstLayout Z
ifne L0
aload 0
fconst_1
iload 2
invokevirtual android/support/v4/widget/SlidingPaneLayout/smoothSlideTo(FI)Z
ifeq L1
L0:
aload 0
iconst_1
putfield android/support/v4/widget/SlidingPaneLayout/mPreservedOpenState Z
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method private parallaxOtherViews(F)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/SlidingPaneLayout$LayoutParams
astore 6
aload 6
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/dimWhenOffset Z
ifeq L0
aload 6
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/leftMargin I
ifgt L0
iconst_1
istore 2
L1:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
istore 4
iconst_0
istore 3
L2:
iload 3
iload 4
if_icmpge L3
aload 0
iload 3
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 6
aload 6
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
if_acmpne L4
L5:
iload 3
iconst_1
iadd
istore 3
goto L2
L0:
iconst_0
istore 2
goto L1
L4:
fconst_1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mParallaxOffset F
fsub
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mParallaxBy I
i2f
fmul
f2i
istore 5
aload 0
fload 1
putfield android/support/v4/widget/SlidingPaneLayout/mParallaxOffset F
aload 6
iload 5
fconst_1
fload 1
fsub
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mParallaxBy I
i2f
fmul
f2i
isub
invokevirtual android/view/View/offsetLeftAndRight(I)V
iload 2
ifeq L5
aload 0
aload 6
fconst_1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mParallaxOffset F
fsub
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCoveredFadeColor I
invokespecial android/support/v4/widget/SlidingPaneLayout/dimChildView(Landroid/view/View;FI)V
goto L5
L3:
return
.limit locals 7
.limit stack 4
.end method

.method private static viewIsOpaque(Landroid/view/View;)Z
aload 0
invokestatic android/support/v4/view/ViewCompat/isOpaque(Landroid/view/View;)Z
ifeq L0
L1:
iconst_1
ireturn
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L2
iconst_0
ireturn
L2:
aload 0
invokevirtual android/view/View/getBackground()Landroid/graphics/drawable/Drawable;
astore 0
aload 0
ifnull L3
aload 0
invokevirtual android/graphics/drawable/Drawable/getOpacity()I
iconst_m1
if_icmpeq L1
iconst_0
ireturn
L3:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method protected canScroll(Landroid/view/View;ZIII)Z
aload 1
instanceof android/view/ViewGroup
ifeq L0
aload 1
checkcast android/view/ViewGroup
astore 9
aload 1
invokevirtual android/view/View/getScrollX()I
istore 7
aload 1
invokevirtual android/view/View/getScrollY()I
istore 8
aload 9
invokevirtual android/view/ViewGroup/getChildCount()I
iconst_1
isub
istore 6
L1:
iload 6
iflt L0
aload 9
iload 6
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
astore 10
iload 4
iload 7
iadd
aload 10
invokevirtual android/view/View/getLeft()I
if_icmplt L2
iload 4
iload 7
iadd
aload 10
invokevirtual android/view/View/getRight()I
if_icmpge L2
iload 5
iload 8
iadd
aload 10
invokevirtual android/view/View/getTop()I
if_icmplt L2
iload 5
iload 8
iadd
aload 10
invokevirtual android/view/View/getBottom()I
if_icmpge L2
aload 0
aload 10
iconst_1
iload 3
iload 4
iload 7
iadd
aload 10
invokevirtual android/view/View/getLeft()I
isub
iload 5
iload 8
iadd
aload 10
invokevirtual android/view/View/getTop()I
isub
invokevirtual android/support/v4/widget/SlidingPaneLayout/canScroll(Landroid/view/View;ZIII)Z
ifeq L2
iconst_1
ireturn
L2:
iload 6
iconst_1
isub
istore 6
goto L1
L0:
iload 2
ifeq L3
aload 1
iload 3
ineg
invokestatic android/support/v4/view/ViewCompat/canScrollHorizontally(Landroid/view/View;I)Z
ifeq L3
iconst_1
ireturn
L3:
iconst_0
ireturn
.limit locals 11
.limit stack 7
.end method

.method public canSlide()Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
aload 1
instanceof android/support/v4/widget/SlidingPaneLayout$LayoutParams
ifeq L0
aload 0
aload 1
invokespecial android/view/ViewGroup/checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public closePane()Z
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
iconst_0
invokespecial android/support/v4/widget/SlidingPaneLayout/closePane(Landroid/view/View;I)Z
ireturn
.limit locals 1
.limit stack 3
.end method

.method public computeScroll()V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
iconst_1
invokevirtual android/support/v4/widget/ViewDragHelper/continueSettling(Z)Z
ifeq L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
ifne L1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/abort()V
L0:
return
L1:
aload 0
invokestatic android/support/v4/view/ViewCompat/postInvalidateOnAnimation(Landroid/view/View;)V
return
.limit locals 1
.limit stack 2
.end method

.method dispatchOnPanelClosed(Landroid/view/View;)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mPanelSlideListener Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;
ifnull L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mPanelSlideListener Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;
aload 1
invokeinterface android/support/v4/widget/SlidingPaneLayout$PanelSlideListener/onPanelClosed(Landroid/view/View;)V 1
L0:
aload 0
bipush 32
invokevirtual android/support/v4/widget/SlidingPaneLayout/sendAccessibilityEvent(I)V
return
.limit locals 2
.limit stack 2
.end method

.method dispatchOnPanelOpened(Landroid/view/View;)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mPanelSlideListener Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;
ifnull L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mPanelSlideListener Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;
aload 1
invokeinterface android/support/v4/widget/SlidingPaneLayout$PanelSlideListener/onPanelOpened(Landroid/view/View;)V 1
L0:
aload 0
bipush 32
invokevirtual android/support/v4/widget/SlidingPaneLayout/sendAccessibilityEvent(I)V
return
.limit locals 2
.limit stack 2
.end method

.method dispatchOnPanelSlide(Landroid/view/View;)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mPanelSlideListener Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;
ifnull L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mPanelSlideListener Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;
aload 1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideOffset F
invokeinterface android/support/v4/widget/SlidingPaneLayout$PanelSlideListener/onPanelSlide(Landroid/view/View;F)V 2
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public draw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/view/ViewGroup/draw(Landroid/graphics/Canvas;)V
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
iconst_1
if_icmple L0
aload 0
iconst_1
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 6
L1:
aload 6
ifnull L2
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mShadowDrawable Landroid/graphics/drawable/Drawable;
ifnonnull L3
L2:
return
L0:
aconst_null
astore 6
goto L1
L3:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mShadowDrawable Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
istore 2
aload 6
invokevirtual android/view/View/getLeft()I
istore 3
aload 6
invokevirtual android/view/View/getTop()I
istore 4
aload 6
invokevirtual android/view/View/getBottom()I
istore 5
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mShadowDrawable Landroid/graphics/drawable/Drawable;
iload 3
iload 2
isub
iload 4
iload 3
iload 5
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mShadowDrawable Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
return
.limit locals 7
.limit stack 5
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
aload 2
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/SlidingPaneLayout$LayoutParams
astore 7
aload 1
iconst_2
invokevirtual android/graphics/Canvas/save(I)I
istore 5
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
ifeq L0
aload 7
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/slideable Z
ifne L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
ifnull L0
aload 1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mTmpRect Landroid/graphics/Rect;
invokevirtual android/graphics/Canvas/getClipBounds(Landroid/graphics/Rect;)Z
pop
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mTmpRect Landroid/graphics/Rect;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mTmpRect Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
invokevirtual android/view/View/getLeft()I
invokestatic java/lang/Math/min(II)I
putfield android/graphics/Rect/right I
aload 1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mTmpRect Landroid/graphics/Rect;
invokevirtual android/graphics/Canvas/clipRect(Landroid/graphics/Rect;)Z
pop
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L1
aload 0
aload 1
aload 2
lload 3
invokespecial android/view/ViewGroup/drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
istore 6
L2:
aload 1
iload 5
invokevirtual android/graphics/Canvas/restoreToCount(I)V
iload 6
ireturn
L1:
aload 7
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/dimWhenOffset Z
ifeq L3
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideOffset F
fconst_0
fcmpl
ifle L3
aload 2
invokevirtual android/view/View/isDrawingCacheEnabled()Z
ifne L4
aload 2
iconst_1
invokevirtual android/view/View/setDrawingCacheEnabled(Z)V
L4:
aload 2
invokevirtual android/view/View/getDrawingCache()Landroid/graphics/Bitmap;
astore 8
aload 8
ifnull L5
aload 1
aload 8
aload 2
invokevirtual android/view/View/getLeft()I
i2f
aload 2
invokevirtual android/view/View/getTop()I
i2f
aload 7
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/dimPaint Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
iconst_0
istore 6
goto L2
L5:
ldc "SlidingPaneLayout"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "drawChild: child view "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " returned null drawing cache"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
aload 1
aload 2
lload 3
invokespecial android/view/ViewGroup/drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
istore 6
goto L2
L3:
aload 2
invokevirtual android/view/View/isDrawingCacheEnabled()Z
ifeq L6
aload 2
iconst_0
invokevirtual android/view/View/setDrawingCacheEnabled(Z)V
L6:
aload 0
aload 1
aload 2
lload 3
invokespecial android/view/ViewGroup/drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
istore 6
goto L2
.limit locals 9
.limit stack 5
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
new android/support/v4/widget/SlidingPaneLayout$LayoutParams
dup
invokespecial android/support/v4/widget/SlidingPaneLayout$LayoutParams/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
new android/support/v4/widget/SlidingPaneLayout$LayoutParams
dup
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/v4/widget/SlidingPaneLayout$LayoutParams/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
aload 1
instanceof android/view/ViewGroup$MarginLayoutParams
ifeq L0
new android/support/v4/widget/SlidingPaneLayout$LayoutParams
dup
aload 1
checkcast android/view/ViewGroup$MarginLayoutParams
invokespecial android/support/v4/widget/SlidingPaneLayout$LayoutParams/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
areturn
L0:
new android/support/v4/widget/SlidingPaneLayout$LayoutParams
dup
aload 1
invokespecial android/support/v4/widget/SlidingPaneLayout$LayoutParams/<init>(Landroid/view/ViewGroup$LayoutParams;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public getCoveredFadeColor()I
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCoveredFadeColor I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getParallaxDistance()I
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mParallaxBy I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSliderFadeColor()I
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSliderFadeColor I
ireturn
.limit locals 1
.limit stack 1
.end method

.method isDimmed(Landroid/view/View;)Z
aload 1
ifnonnull L0
L1:
iconst_0
ireturn
L0:
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/SlidingPaneLayout$LayoutParams
astore 1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
ifeq L1
aload 1
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/dimWhenOffset Z
ifeq L1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideOffset F
fconst_0
fcmpl
ifle L1
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public isOpen()Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
ifeq L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideOffset F
fconst_1
fcmpl
ifne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isSlideable()Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected onAttachedToWindow()V
aload 0
invokespecial android/view/ViewGroup/onAttachedToWindow()V
aload 0
iconst_1
putfield android/support/v4/widget/SlidingPaneLayout/mFirstLayout Z
return
.limit locals 1
.limit stack 2
.end method

.method protected onDetachedFromWindow()V
aload 0
invokespecial android/view/ViewGroup/onDetachedFromWindow()V
aload 0
iconst_1
putfield android/support/v4/widget/SlidingPaneLayout/mFirstLayout Z
iconst_0
istore 1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mPostedRunnables Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 2
L0:
iload 1
iload 2
if_icmpge L1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mPostedRunnables Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable
invokevirtual android/support/v4/widget/SlidingPaneLayout$DisableLayerRunnable/run()V
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mPostedRunnables Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
return
.limit locals 3
.limit stack 2
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
aload 1
invokestatic android/support/v4/view/MotionEventCompat/getActionMasked(Landroid/view/MotionEvent;)I
istore 6
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
ifne L0
iload 6
ifne L0
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
iconst_1
if_icmple L0
aload 0
iconst_1
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 8
aload 8
ifnull L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
aload 8
aload 1
invokevirtual android/view/MotionEvent/getX()F
f2i
aload 1
invokevirtual android/view/MotionEvent/getY()F
f2i
invokevirtual android/support/v4/widget/ViewDragHelper/isViewUnder(Landroid/view/View;II)Z
ifne L1
iconst_1
istore 7
L2:
aload 0
iload 7
putfield android/support/v4/widget/SlidingPaneLayout/mPreservedOpenState Z
L0:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
ifeq L3
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mIsUnableToDrag Z
ifeq L4
iload 6
ifeq L4
L3:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/cancel()V
aload 0
aload 1
invokespecial android/view/ViewGroup/onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
ireturn
L1:
iconst_0
istore 7
goto L2
L4:
iload 6
iconst_3
if_icmpeq L5
iload 6
iconst_1
if_icmpne L6
L5:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/cancel()V
iconst_0
ireturn
L6:
iconst_0
istore 5
iload 5
istore 4
iload 6
tableswitch 0
L7
L8
L9
default : L10
L10:
iload 5
istore 4
L8:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
aload 1
invokevirtual android/support/v4/widget/ViewDragHelper/shouldInterceptTouchEvent(Landroid/view/MotionEvent;)Z
ifne L11
iload 4
ifeq L12
L11:
iconst_1
ireturn
L7:
aload 0
iconst_0
putfield android/support/v4/widget/SlidingPaneLayout/mIsUnableToDrag Z
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
aload 0
fload 2
putfield android/support/v4/widget/SlidingPaneLayout/mInitialMotionX F
aload 0
fload 3
putfield android/support/v4/widget/SlidingPaneLayout/mInitialMotionY F
iload 5
istore 4
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
fload 2
f2i
fload 3
f2i
invokevirtual android/support/v4/widget/ViewDragHelper/isViewUnder(Landroid/view/View;II)Z
ifeq L8
iload 5
istore 4
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
invokevirtual android/support/v4/widget/SlidingPaneLayout/isDimmed(Landroid/view/View;)Z
ifeq L8
iconst_1
istore 4
goto L8
L9:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 3
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 2
fload 3
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mInitialMotionX F
fsub
invokestatic java/lang/Math/abs(F)F
fstore 3
fload 2
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mInitialMotionY F
fsub
invokestatic java/lang/Math/abs(F)F
fstore 2
iload 5
istore 4
fload 3
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/getTouchSlop()I
i2f
fcmpl
ifle L8
iload 5
istore 4
fload 2
fload 3
fcmpl
ifle L8
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/cancel()V
aload 0
iconst_1
putfield android/support/v4/widget/SlidingPaneLayout/mIsUnableToDrag Z
iconst_0
ireturn
L12:
iconst_0
ireturn
.limit locals 9
.limit stack 4
.end method

.method protected onLayout(ZIIII)V
iload 4
iload 2
isub
istore 8
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
istore 2
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingRight()I
istore 9
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingTop()I
istore 10
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
istore 7
iload 2
istore 4
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mFirstLayout Z
ifeq L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
ifeq L1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mPreservedOpenState Z
ifeq L1
fconst_1
fstore 6
L2:
aload 0
fload 6
putfield android/support/v4/widget/SlidingPaneLayout/mSlideOffset F
L0:
iconst_0
istore 5
iload 2
istore 3
iload 4
istore 2
iload 5
istore 4
L3:
iload 4
iload 7
if_icmpge L4
aload 0
iload 4
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 14
aload 14
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpne L5
L6:
iload 4
iconst_1
iadd
istore 4
goto L3
L1:
fconst_0
fstore 6
goto L2
L5:
aload 14
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/SlidingPaneLayout$LayoutParams
astore 15
aload 14
invokevirtual android/view/View/getMeasuredWidth()I
istore 11
iconst_0
istore 5
aload 15
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/slideable Z
ifeq L7
aload 15
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/leftMargin I
istore 12
aload 15
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/rightMargin I
istore 13
iload 2
iload 8
iload 9
isub
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mOverhangSize I
isub
invokestatic java/lang/Math/min(II)I
iload 3
isub
iload 12
iload 13
iadd
isub
istore 12
aload 0
iload 12
putfield android/support/v4/widget/SlidingPaneLayout/mSlideRange I
aload 15
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/leftMargin I
iload 3
iadd
iload 12
iadd
iload 11
iconst_2
idiv
iadd
iload 8
iload 9
isub
if_icmple L8
iconst_1
istore 1
L9:
aload 15
iload 1
putfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/dimWhenOffset Z
iload 3
iload 12
i2f
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideOffset F
fmul
f2i
aload 15
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/leftMargin I
iadd
iadd
istore 3
L10:
iload 3
iload 5
isub
istore 5
aload 14
iload 5
iload 10
iload 5
iload 11
iadd
iload 10
aload 14
invokevirtual android/view/View/getMeasuredHeight()I
iadd
invokevirtual android/view/View/layout(IIII)V
iload 2
aload 14
invokevirtual android/view/View/getWidth()I
iadd
istore 2
goto L6
L8:
iconst_0
istore 1
goto L9
L7:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
ifeq L11
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mParallaxBy I
ifeq L11
fconst_1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideOffset F
fsub
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mParallaxBy I
i2f
fmul
f2i
istore 5
iload 2
istore 3
goto L10
L11:
iload 2
istore 3
goto L10
L4:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mFirstLayout Z
ifeq L12
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
ifeq L13
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mParallaxBy I
ifeq L14
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideOffset F
invokespecial android/support/v4/widget/SlidingPaneLayout/parallaxOtherViews(F)V
L14:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/SlidingPaneLayout$LayoutParams
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/dimWhenOffset Z
ifeq L15
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideOffset F
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSliderFadeColor I
invokespecial android/support/v4/widget/SlidingPaneLayout/dimChildView(Landroid/view/View;FI)V
L15:
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
invokevirtual android/support/v4/widget/SlidingPaneLayout/updateObscuredViewsVisibility(Landroid/view/View;)V
L12:
aload 0
iconst_0
putfield android/support/v4/widget/SlidingPaneLayout/mFirstLayout Z
return
L13:
iconst_0
istore 2
L16:
iload 2
iload 7
if_icmpge L15
aload 0
aload 0
iload 2
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
fconst_0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSliderFadeColor I
invokespecial android/support/v4/widget/SlidingPaneLayout/dimChildView(Landroid/view/View;FI)V
iload 2
iconst_1
iadd
istore 2
goto L16
.limit locals 16
.limit stack 6
.end method

.method protected onMeasure(II)V
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 8
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 5
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 6
iload 2
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 2
iload 8
ldc_w 1073741824
if_icmpeq L0
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/isInEditMode()Z
ifeq L1
iload 8
ldc_w -2147483648
if_icmpne L2
iload 5
istore 7
iload 2
istore 1
iload 6
istore 9
L3:
iconst_0
istore 5
iconst_m1
istore 2
iload 9
lookupswitch
-2147483648 : L4
1073741824 : L5
default : L6
L6:
fconst_0
fstore 3
iconst_0
istore 14
iload 7
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
isub
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingRight()I
isub
istore 8
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
istore 12
iload 12
iconst_2
if_icmple L7
ldc "SlidingPaneLayout"
ldc "onMeasure: More than two child views are not supported."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
L7:
aload 0
aconst_null
putfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
iconst_0
istore 10
L8:
iload 10
iload 12
if_icmpge L9
aload 0
iload 10
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 16
aload 16
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/SlidingPaneLayout$LayoutParams
astore 17
aload 16
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpne L10
aload 17
iconst_0
putfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/dimWhenOffset Z
iload 8
istore 11
iload 5
istore 6
iload 14
istore 15
L11:
iload 10
iconst_1
iadd
istore 10
iload 15
istore 14
iload 6
istore 5
iload 11
istore 8
goto L8
L2:
iload 6
istore 9
iload 2
istore 1
iload 5
istore 7
iload 8
ifne L3
sipush 300
istore 7
iload 6
istore 9
iload 2
istore 1
goto L3
L1:
new java/lang/IllegalStateException
dup
ldc "Width must have an exact value or MATCH_PARENT"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 6
istore 9
iload 2
istore 1
iload 5
istore 7
iload 6
ifne L3
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/isInEditMode()Z
ifeq L12
iload 6
istore 9
iload 2
istore 1
iload 5
istore 7
iload 6
ifne L3
ldc_w -2147483648
istore 9
sipush 300
istore 1
iload 5
istore 7
goto L3
L12:
new java/lang/IllegalStateException
dup
ldc "Height must not be UNSPECIFIED"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L5:
iload 1
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingTop()I
isub
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingBottom()I
isub
istore 2
iload 2
istore 5
goto L6
L4:
iload 1
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingTop()I
isub
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingBottom()I
isub
istore 2
goto L6
L10:
fload 3
fstore 4
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/weight F
fconst_0
fcmpl
ifle L13
fload 3
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/weight F
fadd
fstore 4
iload 14
istore 15
iload 5
istore 6
fload 4
fstore 3
iload 8
istore 11
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/width I
ifeq L11
L13:
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/leftMargin I
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/rightMargin I
iadd
istore 1
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/width I
bipush -2
if_icmpne L14
iload 7
iload 1
isub
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 1
L15:
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/height I
bipush -2
if_icmpne L16
iload 2
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 6
L17:
aload 16
iload 1
iload 6
invokevirtual android/view/View/measure(II)V
aload 16
invokevirtual android/view/View/getMeasuredWidth()I
istore 6
aload 16
invokevirtual android/view/View/getMeasuredHeight()I
istore 11
iload 5
istore 1
iload 9
ldc_w -2147483648
if_icmpne L18
iload 5
istore 1
iload 11
iload 5
if_icmple L18
iload 11
iload 2
invokestatic java/lang/Math/min(II)I
istore 1
L18:
iload 8
iload 6
isub
istore 5
iload 5
ifge L19
iconst_1
istore 15
L20:
aload 17
iload 15
putfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/slideable Z
iload 14
iload 15
ior
istore 14
iload 14
istore 15
iload 1
istore 6
fload 4
fstore 3
iload 5
istore 11
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/slideable Z
ifeq L11
aload 0
aload 16
putfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
iload 14
istore 15
iload 1
istore 6
fload 4
fstore 3
iload 5
istore 11
goto L11
L14:
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/width I
iconst_m1
if_icmpne L21
iload 7
iload 1
isub
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 1
goto L15
L21:
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/width I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 1
goto L15
L16:
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/height I
iconst_m1
if_icmpne L22
iload 2
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 6
goto L17
L22:
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/height I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 6
goto L17
L19:
iconst_0
istore 15
goto L20
L9:
iload 14
ifne L23
fload 3
fconst_0
fcmpl
ifle L24
L23:
iload 7
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mOverhangSize I
isub
istore 10
iconst_0
istore 6
L25:
iload 6
iload 12
if_icmpge L24
aload 0
iload 6
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 16
aload 16
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpne L26
L27:
iload 6
iconst_1
iadd
istore 6
goto L25
L26:
aload 16
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/SlidingPaneLayout$LayoutParams
astore 17
aload 16
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L27
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/width I
ifne L28
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/weight F
fconst_0
fcmpl
ifle L28
iconst_1
istore 1
L29:
iload 1
ifeq L30
iconst_0
istore 9
L31:
iload 14
ifeq L32
aload 16
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
if_acmpeq L32
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/width I
ifge L27
iload 9
iload 10
if_icmpgt L33
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/weight F
fconst_0
fcmpl
ifle L27
L33:
iload 1
ifeq L34
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/height I
bipush -2
if_icmpne L35
iload 2
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 1
L36:
aload 16
iload 10
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 1
invokevirtual android/view/View/measure(II)V
goto L27
L28:
iconst_0
istore 1
goto L29
L30:
aload 16
invokevirtual android/view/View/getMeasuredWidth()I
istore 9
goto L31
L35:
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/height I
iconst_m1
if_icmpne L37
iload 2
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 1
goto L36
L37:
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/height I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 1
goto L36
L34:
aload 16
invokevirtual android/view/View/getMeasuredHeight()I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 1
goto L36
L32:
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/weight F
fconst_0
fcmpl
ifle L27
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/width I
ifne L38
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/height I
bipush -2
if_icmpne L39
iload 2
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 1
L40:
iload 14
ifeq L41
iload 7
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/leftMargin I
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/rightMargin I
iadd
isub
istore 11
iload 11
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 13
iload 9
iload 11
if_icmpeq L27
aload 16
iload 13
iload 1
invokevirtual android/view/View/measure(II)V
goto L27
L39:
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/height I
iconst_m1
if_icmpne L42
iload 2
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 1
goto L40
L42:
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/height I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 1
goto L40
L38:
aload 16
invokevirtual android/view/View/getMeasuredHeight()I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 1
goto L40
L41:
iconst_0
iload 8
invokestatic java/lang/Math/max(II)I
istore 11
aload 16
iload 9
aload 17
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/weight F
iload 11
i2f
fmul
fload 3
fdiv
f2i
iadd
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 1
invokevirtual android/view/View/measure(II)V
goto L27
L24:
aload 0
iload 7
iload 5
invokevirtual android/support/v4/widget/SlidingPaneLayout/setMeasuredDimension(II)V
aload 0
iload 14
putfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/getViewDragState()I
ifeq L43
iload 14
ifne L43
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/abort()V
L43:
return
.limit locals 18
.limit stack 4
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
checkcast android/support/v4/widget/SlidingPaneLayout$SavedState
astore 1
aload 0
aload 1
invokevirtual android/support/v4/widget/SlidingPaneLayout$SavedState/getSuperState()Landroid/os/Parcelable;
invokespecial android/view/ViewGroup/onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
getfield android/support/v4/widget/SlidingPaneLayout$SavedState/isOpen Z
ifeq L0
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/openPane()Z
pop
L1:
aload 0
aload 1
getfield android/support/v4/widget/SlidingPaneLayout$SavedState/isOpen Z
putfield android/support/v4/widget/SlidingPaneLayout/mPreservedOpenState Z
return
L0:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/closePane()Z
pop
goto L1
.limit locals 2
.limit stack 2
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
new android/support/v4/widget/SlidingPaneLayout$SavedState
dup
aload 0
invokespecial android/view/ViewGroup/onSaveInstanceState()Landroid/os/Parcelable;
invokespecial android/support/v4/widget/SlidingPaneLayout$SavedState/<init>(Landroid/os/Parcelable;)V
astore 2
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/isSlideable()Z
ifeq L0
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/isOpen()Z
istore 1
L1:
aload 2
iload 1
putfield android/support/v4/widget/SlidingPaneLayout$SavedState/isOpen Z
aload 2
areturn
L0:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mPreservedOpenState Z
istore 1
goto L1
.limit locals 3
.limit stack 3
.end method

.method protected onSizeChanged(IIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
invokespecial android/view/ViewGroup/onSizeChanged(IIII)V
iload 1
iload 3
if_icmpeq L0
aload 0
iconst_1
putfield android/support/v4/widget/SlidingPaneLayout/mFirstLayout Z
L0:
return
.limit locals 5
.limit stack 5
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
ifne L0
aload 0
aload 1
invokespecial android/view/ViewGroup/onTouchEvent(Landroid/view/MotionEvent;)Z
istore 7
L1:
iload 7
ireturn
L0:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
aload 1
invokevirtual android/support/v4/widget/ViewDragHelper/processTouchEvent(Landroid/view/MotionEvent;)V
aload 1
invokevirtual android/view/MotionEvent/getAction()I
istore 6
iconst_1
istore 8
iload 6
sipush 255
iand
tableswitch 0
L2
L3
default : L4
L4:
iconst_1
ireturn
L2:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
aload 0
fload 2
putfield android/support/v4/widget/SlidingPaneLayout/mInitialMotionX F
aload 0
fload 3
putfield android/support/v4/widget/SlidingPaneLayout/mInitialMotionY F
iconst_1
ireturn
L3:
iload 8
istore 7
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
invokevirtual android/support/v4/widget/SlidingPaneLayout/isDimmed(Landroid/view/View;)Z
ifeq L1
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
fload 2
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mInitialMotionX F
fsub
fstore 4
fload 3
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mInitialMotionY F
fsub
fstore 5
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/getTouchSlop()I
istore 6
iload 8
istore 7
fload 4
fload 4
fmul
fload 5
fload 5
fmul
fadd
iload 6
iload 6
imul
i2f
fcmpg
ifge L1
iload 8
istore 7
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
fload 2
f2i
fload 3
f2i
invokevirtual android/support/v4/widget/ViewDragHelper/isViewUnder(Landroid/view/View;II)Z
ifeq L1
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
iconst_0
invokespecial android/support/v4/widget/SlidingPaneLayout/closePane(Landroid/view/View;I)Z
pop
iconst_1
ireturn
.limit locals 9
.limit stack 4
.end method

.method public openPane()Z
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
iconst_0
invokespecial android/support/v4/widget/SlidingPaneLayout/openPane(Landroid/view/View;I)Z
ireturn
.limit locals 1
.limit stack 3
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
aload 0
aload 1
aload 2
invokespecial android/view/ViewGroup/requestChildFocus(Landroid/view/View;Landroid/view/View;)V
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/isInTouchMode()Z
ifne L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
ifne L0
aload 1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
if_acmpne L1
iconst_1
istore 3
L2:
aload 0
iload 3
putfield android/support/v4/widget/SlidingPaneLayout/mPreservedOpenState Z
L0:
return
L1:
iconst_0
istore 3
goto L2
.limit locals 4
.limit stack 3
.end method

.method setAllChildrenVisible()V
iconst_0
istore 1
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
istore 2
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 3
aload 3
invokevirtual android/view/View/getVisibility()I
iconst_4
if_icmpne L2
aload 3
iconst_0
invokevirtual android/view/View/setVisibility(I)V
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
return
.limit locals 4
.limit stack 2
.end method

.method public setCoveredFadeColor(I)V
aload 0
iload 1
putfield android/support/v4/widget/SlidingPaneLayout/mCoveredFadeColor I
return
.limit locals 2
.limit stack 2
.end method

.method public setPanelSlideListener(Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;)V
aload 0
aload 1
putfield android/support/v4/widget/SlidingPaneLayout/mPanelSlideListener Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;
return
.limit locals 2
.limit stack 2
.end method

.method public setParallaxDistance(I)V
aload 0
iload 1
putfield android/support/v4/widget/SlidingPaneLayout/mParallaxBy I
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public setShadowDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
putfield android/support/v4/widget/SlidingPaneLayout/mShadowDrawable Landroid/graphics/drawable/Drawable;
return
.limit locals 2
.limit stack 2
.end method

.method public setShadowResource(I)V
aload 0
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v4/widget/SlidingPaneLayout/setShadowDrawable(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 3
.end method

.method public setSliderFadeColor(I)V
aload 0
iload 1
putfield android/support/v4/widget/SlidingPaneLayout/mSliderFadeColor I
return
.limit locals 2
.limit stack 2
.end method

.method public smoothSlideClosed()V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/closePane()Z
pop
return
.limit locals 1
.limit stack 1
.end method

.method public smoothSlideOpen()V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/openPane()Z
pop
return
.limit locals 1
.limit stack 1
.end method

.method smoothSlideTo(FI)Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mCanSlide Z
ifne L0
L1:
iconst_0
ireturn
L0:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/SlidingPaneLayout$LayoutParams
astore 3
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
aload 3
getfield android/support/v4/widget/SlidingPaneLayout$LayoutParams/leftMargin I
iadd
i2f
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideRange I
i2f
fload 1
fmul
fadd
f2i
istore 2
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mDragHelper Landroid/support/v4/widget/ViewDragHelper;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
iload 2
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/mSlideableView Landroid/view/View;
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/ViewDragHelper/smoothSlideViewTo(Landroid/view/View;II)Z
ifeq L1
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/setAllChildrenVisible()V
aload 0
invokestatic android/support/v4/view/ViewCompat/postInvalidateOnAnimation(Landroid/view/View;)V
iconst_1
ireturn
.limit locals 4
.limit stack 4
.end method

.method updateObscuredViewsVisibility(Landroid/view/View;)V
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
istore 8
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getWidth()I
istore 9
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingRight()I
istore 10
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingTop()I
istore 11
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getHeight()I
istore 12
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingBottom()I
istore 13
aload 1
ifnull L0
aload 1
invokestatic android/support/v4/widget/SlidingPaneLayout/viewIsOpaque(Landroid/view/View;)Z
ifeq L0
aload 1
invokevirtual android/view/View/getLeft()I
istore 3
aload 1
invokevirtual android/view/View/getRight()I
istore 4
aload 1
invokevirtual android/view/View/getTop()I
istore 5
aload 1
invokevirtual android/view/View/getBottom()I
istore 2
L1:
iconst_0
istore 6
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
istore 14
L2:
iload 6
iload 14
if_icmpge L3
aload 0
iload 6
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 18
aload 18
aload 1
if_acmpne L4
L3:
return
L0:
iconst_0
istore 2
iconst_0
istore 5
iconst_0
istore 4
iconst_0
istore 3
goto L1
L4:
iload 8
aload 18
invokevirtual android/view/View/getLeft()I
invokestatic java/lang/Math/max(II)I
istore 7
iload 11
aload 18
invokevirtual android/view/View/getTop()I
invokestatic java/lang/Math/max(II)I
istore 15
iload 9
iload 10
isub
aload 18
invokevirtual android/view/View/getRight()I
invokestatic java/lang/Math/min(II)I
istore 16
iload 12
iload 13
isub
aload 18
invokevirtual android/view/View/getBottom()I
invokestatic java/lang/Math/min(II)I
istore 17
iload 7
iload 3
if_icmplt L5
iload 15
iload 5
if_icmplt L5
iload 16
iload 4
if_icmpgt L5
iload 17
iload 2
if_icmpgt L5
iconst_4
istore 7
L6:
aload 18
iload 7
invokevirtual android/view/View/setVisibility(I)V
iload 6
iconst_1
iadd
istore 6
goto L2
L5:
iconst_0
istore 7
goto L6
.limit locals 19
.limit stack 2
.end method
