.bytecode 50.0
.class synchronized android/support/v4/widget/DrawerLayout$ViewDragCallback
.super android/support/v4/widget/ViewDragHelper$Callback
.inner class private ViewDragCallback inner android/support/v4/widget/DrawerLayout$ViewDragCallback outer android/support/v4/widget/DrawerLayout
.inner class inner android/support/v4/widget/DrawerLayout$ViewDragCallback$1

.field private 'mDragger' Landroid/support/v4/widget/ViewDragHelper;

.field private final 'mGravity' I

.field private final 'mPeekRunnable' Ljava/lang/Runnable;

.field final synthetic 'this$0' Landroid/support/v4/widget/DrawerLayout;

.method public <init>(Landroid/support/v4/widget/DrawerLayout;I)V
aload 0
aload 1
putfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 0
invokespecial android/support/v4/widget/ViewDragHelper$Callback/<init>()V
aload 0
new android/support/v4/widget/DrawerLayout$ViewDragCallback$1
dup
aload 0
invokespecial android/support/v4/widget/DrawerLayout$ViewDragCallback$1/<init>(Landroid/support/v4/widget/DrawerLayout$ViewDragCallback;)V
putfield android/support/v4/widget/DrawerLayout$ViewDragCallback/mPeekRunnable Ljava/lang/Runnable;
aload 0
iload 2
putfield android/support/v4/widget/DrawerLayout$ViewDragCallback/mGravity I
return
.limit locals 3
.limit stack 4
.end method

.method static synthetic access$000(Landroid/support/v4/widget/DrawerLayout$ViewDragCallback;)V
aload 0
invokespecial android/support/v4/widget/DrawerLayout$ViewDragCallback/peekDrawer()V
return
.limit locals 1
.limit stack 1
.end method

.method private closeOtherDrawer()V
iconst_3
istore 1
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/mGravity I
iconst_3
if_icmpne L0
iconst_5
istore 1
L0:
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/findDrawerWithGravity(I)Landroid/view/View;
astore 2
aload 2
ifnull L1
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 2
invokevirtual android/support/v4/widget/DrawerLayout/closeDrawer(Landroid/view/View;)V
L1:
return
.limit locals 3
.limit stack 2
.end method

.method private peekDrawer()V
iconst_0
istore 2
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/mDragger Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/getEdgeSize()I
istore 3
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/mGravity I
iconst_3
if_icmpne L0
iconst_1
istore 1
L1:
iload 1
ifeq L2
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/findDrawerWithGravity(I)Landroid/view/View;
astore 4
aload 4
ifnull L3
aload 4
invokevirtual android/view/View/getWidth()I
ineg
istore 2
L3:
iload 2
iload 3
iadd
istore 2
L4:
aload 4
ifnull L5
iload 1
ifeq L6
aload 4
invokevirtual android/view/View/getLeft()I
iload 2
if_icmplt L7
L6:
iload 1
ifne L5
aload 4
invokevirtual android/view/View/getLeft()I
iload 2
if_icmple L5
L7:
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 4
invokevirtual android/support/v4/widget/DrawerLayout/getDrawerLockMode(Landroid/view/View;)I
ifne L5
aload 4
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
astore 5
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/mDragger Landroid/support/v4/widget/ViewDragHelper;
aload 4
iload 2
aload 4
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/ViewDragHelper/smoothSlideViewTo(Landroid/view/View;II)Z
pop
aload 5
iconst_1
putfield android/support/v4/widget/DrawerLayout$LayoutParams/isPeeking Z
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
aload 0
invokespecial android/support/v4/widget/DrawerLayout$ViewDragCallback/closeOtherDrawer()V
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/cancelChildViewTouch()V
L5:
return
L0:
iconst_0
istore 1
goto L1
L2:
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
iconst_5
invokevirtual android/support/v4/widget/DrawerLayout/findDrawerWithGravity(I)Landroid/view/View;
astore 4
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
iload 3
isub
istore 2
goto L4
.limit locals 6
.limit stack 4
.end method

.method public clampViewPositionHorizontal(Landroid/view/View;II)I
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 1
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/checkDrawerViewGravity(Landroid/view/View;I)Z
ifeq L0
aload 1
invokevirtual android/view/View/getWidth()I
ineg
iload 2
iconst_0
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/Math/max(II)I
ireturn
L0:
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
istore 3
iload 3
aload 1
invokevirtual android/view/View/getWidth()I
isub
iload 2
iload 3
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/Math/max(II)I
ireturn
.limit locals 4
.limit stack 3
.end method

.method public clampViewPositionVertical(Landroid/view/View;II)I
aload 1
invokevirtual android/view/View/getTop()I
ireturn
.limit locals 4
.limit stack 1
.end method

.method public getViewHorizontalDragRange(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getWidth()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public onEdgeDragStarted(II)V
iload 1
iconst_1
iand
iconst_1
if_icmpne L0
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/findDrawerWithGravity(I)Landroid/view/View;
astore 3
L1:
aload 3
ifnull L2
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 3
invokevirtual android/support/v4/widget/DrawerLayout/getDrawerLockMode(Landroid/view/View;)I
ifne L2
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/mDragger Landroid/support/v4/widget/ViewDragHelper;
aload 3
iload 2
invokevirtual android/support/v4/widget/ViewDragHelper/captureChildView(Landroid/view/View;I)V
L2:
return
L0:
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
iconst_5
invokevirtual android/support/v4/widget/DrawerLayout/findDrawerWithGravity(I)Landroid/view/View;
astore 3
goto L1
.limit locals 4
.limit stack 3
.end method

.method public onEdgeLock(I)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public onEdgeTouched(II)V
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/mPeekRunnable Ljava/lang/Runnable;
ldc2_w 160L
invokevirtual android/support/v4/widget/DrawerLayout/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 3
.limit stack 4
.end method

.method public onViewCaptured(Landroid/view/View;I)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
iconst_0
putfield android/support/v4/widget/DrawerLayout$LayoutParams/isPeeking Z
aload 0
invokespecial android/support/v4/widget/DrawerLayout$ViewDragCallback/closeOtherDrawer()V
return
.limit locals 3
.limit stack 2
.end method

.method public onViewDragStateChanged(I)V
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/mGravity I
iload 1
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/mDragger Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/getCapturedView()Landroid/view/View;
invokevirtual android/support/v4/widget/DrawerLayout/updateDrawerState(IILandroid/view/View;)V
return
.limit locals 2
.limit stack 4
.end method

.method public onViewPositionChanged(Landroid/view/View;IIII)V
aload 1
invokevirtual android/view/View/getWidth()I
istore 3
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 1
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/checkDrawerViewGravity(Landroid/view/View;I)Z
ifeq L0
iload 3
iload 2
iadd
i2f
iload 3
i2f
fdiv
fstore 6
L1:
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 1
fload 6
invokevirtual android/support/v4/widget/DrawerLayout/setDrawerViewOffset(Landroid/view/View;F)V
fload 6
fconst_0
fcmpl
ifne L2
iconst_4
istore 2
L3:
aload 1
iload 2
invokevirtual android/view/View/setVisibility(I)V
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
return
L0:
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
iload 2
isub
i2f
iload 3
i2f
fdiv
fstore 6
goto L1
L2:
iconst_0
istore 2
goto L3
.limit locals 7
.limit stack 3
.end method

.method public onViewReleased(Landroid/view/View;FF)V
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/getDrawerViewOffset(Landroid/view/View;)F
fstore 3
aload 1
invokevirtual android/view/View/getWidth()I
istore 5
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 1
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/checkDrawerViewGravity(Landroid/view/View;I)Z
ifeq L0
fload 2
fconst_0
fcmpl
ifgt L1
fload 2
fconst_0
fcmpl
ifne L2
fload 3
ldc_w 0.5F
fcmpl
ifle L2
L1:
iconst_0
istore 4
L3:
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/mDragger Landroid/support/v4/widget/ViewDragHelper;
iload 4
aload 1
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/ViewDragHelper/settleCapturedViewAt(II)Z
pop
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
return
L2:
iload 5
ineg
istore 4
goto L3
L0:
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
istore 4
fload 2
fconst_0
fcmpg
iflt L4
fload 2
fconst_0
fcmpl
ifne L5
fload 3
ldc_w 0.5F
fcmpg
ifge L5
L4:
iload 4
iload 5
isub
istore 4
L6:
goto L3
L5:
goto L6
.limit locals 6
.limit stack 3
.end method

.method public removeCallbacks()V
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/mPeekRunnable Ljava/lang/Runnable;
invokevirtual android/support/v4/widget/DrawerLayout/removeCallbacks(Ljava/lang/Runnable;)Z
pop
return
.limit locals 1
.limit stack 2
.end method

.method public setDragger(Landroid/support/v4/widget/ViewDragHelper;)V
aload 0
aload 1
putfield android/support/v4/widget/DrawerLayout$ViewDragCallback/mDragger Landroid/support/v4/widget/ViewDragHelper;
return
.limit locals 2
.limit stack 2
.end method

.method public tryCaptureView(Landroid/view/View;I)Z
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerView(Landroid/view/View;)Z
ifeq L0
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 1
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/mGravity I
invokevirtual android/support/v4/widget/DrawerLayout/checkDrawerViewGravity(Landroid/view/View;I)Z
ifeq L0
aload 0
getfield android/support/v4/widget/DrawerLayout$ViewDragCallback/this$0 Landroid/support/v4/widget/DrawerLayout;
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/getDrawerLockMode(Landroid/view/View;)I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method
