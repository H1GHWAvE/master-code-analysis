.bytecode 50.0
.class public abstract interface android/support/v4/widget/SlidingPaneLayout$PanelSlideListener
.super java/lang/Object
.inner class public static abstract interface PanelSlideListener inner android/support/v4/widget/SlidingPaneLayout$PanelSlideListener outer android/support/v4/widget/SlidingPaneLayout

.method public abstract onPanelClosed(Landroid/view/View;)V
.end method

.method public abstract onPanelOpened(Landroid/view/View;)V
.end method

.method public abstract onPanelSlide(Landroid/view/View;F)V
.end method
