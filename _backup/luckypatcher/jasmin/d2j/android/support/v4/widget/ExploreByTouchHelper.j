.bytecode 50.0
.class public synchronized abstract android/support/v4/widget/ExploreByTouchHelper
.super android/support/v4/view/AccessibilityDelegateCompat
.inner class static synthetic inner android/support/v4/widget/ExploreByTouchHelper$1
.inner class private ExploreByTouchNodeProvider inner android/support/v4/widget/ExploreByTouchHelper$ExploreByTouchNodeProvider outer android/support/v4/widget/ExploreByTouchHelper

.field private static final 'DEFAULT_CLASS_NAME' Ljava/lang/String;

.field public static final 'INVALID_ID' I = -2147483648


.field private 'mFocusedVirtualViewId' I

.field private 'mHoveredVirtualViewId' I

.field private final 'mManager' Landroid/view/accessibility/AccessibilityManager;

.field private 'mNodeProvider' Landroid/support/v4/widget/ExploreByTouchHelper$ExploreByTouchNodeProvider;

.field private final 'mTempGlobalRect' [I

.field private final 'mTempParentRect' Landroid/graphics/Rect;

.field private final 'mTempScreenRect' Landroid/graphics/Rect;

.field private final 'mTempVisibleRect' Landroid/graphics/Rect;

.field private final 'mView' Landroid/view/View;

.method static <clinit>()V
ldc android/view/View
invokevirtual java/lang/Class/getName()Ljava/lang/String;
putstatic android/support/v4/widget/ExploreByTouchHelper/DEFAULT_CLASS_NAME Ljava/lang/String;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>(Landroid/view/View;)V
aload 0
invokespecial android/support/v4/view/AccessibilityDelegateCompat/<init>()V
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/widget/ExploreByTouchHelper/mTempScreenRect Landroid/graphics/Rect;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/widget/ExploreByTouchHelper/mTempParentRect Landroid/graphics/Rect;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/widget/ExploreByTouchHelper/mTempVisibleRect Landroid/graphics/Rect;
aload 0
iconst_2
newarray int
putfield android/support/v4/widget/ExploreByTouchHelper/mTempGlobalRect [I
aload 0
ldc_w -2147483648
putfield android/support/v4/widget/ExploreByTouchHelper/mFocusedVirtualViewId I
aload 0
ldc_w -2147483648
putfield android/support/v4/widget/ExploreByTouchHelper/mHoveredVirtualViewId I
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "View may not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
aload 0
aload 1
invokevirtual android/view/View/getContext()Landroid/content/Context;
ldc "accessibility"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/accessibility/AccessibilityManager
putfield android/support/v4/widget/ExploreByTouchHelper/mManager Landroid/view/accessibility/AccessibilityManager;
return
.limit locals 2
.limit stack 3
.end method

.method static synthetic access$100(Landroid/support/v4/widget/ExploreByTouchHelper;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
aload 0
iload 1
invokespecial android/support/v4/widget/ExploreByTouchHelper/createNode(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$200(Landroid/support/v4/widget/ExploreByTouchHelper;IILandroid/os/Bundle;)Z
aload 0
iload 1
iload 2
aload 3
invokespecial android/support/v4/widget/ExploreByTouchHelper/performAction(IILandroid/os/Bundle;)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method private clearAccessibilityFocus(I)Z
aload 0
iload 1
invokespecial android/support/v4/widget/ExploreByTouchHelper/isAccessibilityFocused(I)Z
ifeq L0
aload 0
ldc_w -2147483648
putfield android/support/v4/widget/ExploreByTouchHelper/mFocusedVirtualViewId I
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
invokevirtual android/view/View/invalidate()V
aload 0
iload 1
ldc_w 65536
invokevirtual android/support/v4/widget/ExploreByTouchHelper/sendEventForVirtualView(II)Z
pop
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method private createEvent(II)Landroid/view/accessibility/AccessibilityEvent;
iload 1
tableswitch -1
L0
default : L1
L1:
aload 0
iload 1
iload 2
invokespecial android/support/v4/widget/ExploreByTouchHelper/createEventForChild(II)Landroid/view/accessibility/AccessibilityEvent;
areturn
L0:
aload 0
iload 2
invokespecial android/support/v4/widget/ExploreByTouchHelper/createEventForHost(I)Landroid/view/accessibility/AccessibilityEvent;
areturn
.limit locals 3
.limit stack 3
.end method

.method private createEventForChild(II)Landroid/view/accessibility/AccessibilityEvent;
iload 2
invokestatic android/view/accessibility/AccessibilityEvent/obtain(I)Landroid/view/accessibility/AccessibilityEvent;
astore 3
aload 3
iconst_1
invokevirtual android/view/accessibility/AccessibilityEvent/setEnabled(Z)V
aload 3
getstatic android/support/v4/widget/ExploreByTouchHelper/DEFAULT_CLASS_NAME Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setClassName(Ljava/lang/CharSequence;)V
aload 0
iload 1
aload 3
invokevirtual android/support/v4/widget/ExploreByTouchHelper/onPopulateEventForVirtualView(ILandroid/view/accessibility/AccessibilityEvent;)V
aload 3
invokevirtual android/view/accessibility/AccessibilityEvent/getText()Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifeq L0
aload 3
invokevirtual android/view/accessibility/AccessibilityEvent/getContentDescription()Ljava/lang/CharSequence;
ifnonnull L0
new java/lang/RuntimeException
dup
ldc "Callbacks must add text or a content description in populateEventForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 3
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
invokevirtual android/view/View/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setPackageName(Ljava/lang/CharSequence;)V
aload 3
invokestatic android/support/v4/view/accessibility/AccessibilityEventCompat/asRecord(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
iload 1
invokevirtual android/support/v4/view/accessibility/AccessibilityRecordCompat/setSource(Landroid/view/View;I)V
aload 3
areturn
.limit locals 4
.limit stack 3
.end method

.method private createEventForHost(I)Landroid/view/accessibility/AccessibilityEvent;
iload 1
invokestatic android/view/accessibility/AccessibilityEvent/obtain(I)Landroid/view/accessibility/AccessibilityEvent;
astore 2
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
aload 2
invokestatic android/support/v4/view/ViewCompat/onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 2
areturn
.limit locals 3
.limit stack 2
.end method

.method private createNode(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
iload 1
tableswitch -1
L0
default : L1
L1:
aload 0
iload 1
invokespecial android/support/v4/widget/ExploreByTouchHelper/createNodeForChild(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
areturn
L0:
aload 0
invokespecial android/support/v4/widget/ExploreByTouchHelper/createNodeForHost()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
areturn
.limit locals 2
.limit stack 2
.end method

.method private createNodeForChild(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
invokestatic android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/obtain()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
astore 3
aload 3
iconst_1
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setEnabled(Z)V
aload 3
getstatic android/support/v4/widget/ExploreByTouchHelper/DEFAULT_CLASS_NAME Ljava/lang/String;
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setClassName(Ljava/lang/CharSequence;)V
aload 0
iload 1
aload 3
invokevirtual android/support/v4/widget/ExploreByTouchHelper/onPopulateNodeForVirtualView(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
aload 3
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/getText()Ljava/lang/CharSequence;
ifnonnull L0
aload 3
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/getContentDescription()Ljava/lang/CharSequence;
ifnonnull L0
new java/lang/RuntimeException
dup
ldc "Callbacks must add text or a content description in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 3
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mTempParentRect Landroid/graphics/Rect;
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/getBoundsInParent(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mTempParentRect Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/isEmpty()Z
ifeq L1
new java/lang/RuntimeException
dup
ldc "Callbacks must set parent bounds in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 3
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/getActions()I
istore 2
iload 2
bipush 64
iand
ifeq L2
new java/lang/RuntimeException
dup
ldc "Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L2:
iload 2
sipush 128
iand
ifeq L3
new java/lang/RuntimeException
dup
ldc "Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 3
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
invokevirtual android/view/View/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setPackageName(Ljava/lang/CharSequence;)V
aload 3
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
iload 1
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setSource(Landroid/view/View;I)V
aload 3
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setParent(Landroid/view/View;)V
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mFocusedVirtualViewId I
iload 1
if_icmpne L4
aload 3
iconst_1
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setAccessibilityFocused(Z)V
aload 3
sipush 128
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/addAction(I)V
L5:
aload 0
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mTempParentRect Landroid/graphics/Rect;
invokespecial android/support/v4/widget/ExploreByTouchHelper/intersectVisibleToUser(Landroid/graphics/Rect;)Z
ifeq L6
aload 3
iconst_1
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setVisibleToUser(Z)V
aload 3
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mTempParentRect Landroid/graphics/Rect;
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setBoundsInParent(Landroid/graphics/Rect;)V
L6:
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mTempGlobalRect [I
invokevirtual android/view/View/getLocationOnScreen([I)V
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mTempGlobalRect [I
iconst_0
iaload
istore 1
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mTempGlobalRect [I
iconst_1
iaload
istore 2
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mTempScreenRect Landroid/graphics/Rect;
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mTempParentRect Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/set(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mTempScreenRect Landroid/graphics/Rect;
iload 1
iload 2
invokevirtual android/graphics/Rect/offset(II)V
aload 3
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mTempScreenRect Landroid/graphics/Rect;
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setBoundsInScreen(Landroid/graphics/Rect;)V
aload 3
areturn
L4:
aload 3
iconst_0
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/setAccessibilityFocused(Z)V
aload 3
bipush 64
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/addAction(I)V
goto L5
.limit locals 4
.limit stack 3
.end method

.method private createNodeForHost()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
invokestatic android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/obtain(Landroid/view/View;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
astore 1
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
aload 1
invokestatic android/support/v4/view/ViewCompat/onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
astore 2
aload 0
aload 2
invokevirtual android/support/v4/widget/ExploreByTouchHelper/getVisibleVirtualViews(Ljava/util/List;)V
aload 2
invokevirtual java/util/LinkedList/iterator()Ljava/util/Iterator;
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
astore 3
aload 1
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
aload 3
invokevirtual java/lang/Integer/intValue()I
invokevirtual android/support/v4/view/accessibility/AccessibilityNodeInfoCompat/addChild(Landroid/view/View;I)V
goto L0
L1:
aload 1
areturn
.limit locals 4
.limit stack 3
.end method

.method private intersectVisibleToUser(Landroid/graphics/Rect;)Z
aload 1
ifnull L0
aload 1
invokevirtual android/graphics/Rect/isEmpty()Z
ifeq L1
L0:
iconst_0
ireturn
L1:
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
invokevirtual android/view/View/getWindowVisibility()I
ifne L0
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 2
L2:
aload 2
instanceof android/view/View
ifeq L3
aload 2
checkcast android/view/View
astore 2
aload 2
invokestatic android/support/v4/view/ViewCompat/getAlpha(Landroid/view/View;)F
fconst_0
fcmpg
ifle L0
aload 2
invokevirtual android/view/View/getVisibility()I
ifne L0
aload 2
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 2
goto L2
L3:
aload 2
ifnull L0
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mTempVisibleRect Landroid/graphics/Rect;
invokevirtual android/view/View/getLocalVisibleRect(Landroid/graphics/Rect;)Z
ifeq L0
aload 1
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mTempVisibleRect Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/intersect(Landroid/graphics/Rect;)Z
ireturn
.limit locals 3
.limit stack 2
.end method

.method private isAccessibilityFocused(I)Z
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mFocusedVirtualViewId I
iload 1
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private manageFocusForChild(IILandroid/os/Bundle;)Z
iload 2
lookupswitch
64 : L0
128 : L1
default : L2
L2:
iconst_0
ireturn
L0:
aload 0
iload 1
invokespecial android/support/v4/widget/ExploreByTouchHelper/requestAccessibilityFocus(I)Z
ireturn
L1:
aload 0
iload 1
invokespecial android/support/v4/widget/ExploreByTouchHelper/clearAccessibilityFocus(I)Z
ireturn
.limit locals 4
.limit stack 2
.end method

.method private performAction(IILandroid/os/Bundle;)Z
iload 1
tableswitch -1
L0
default : L1
L1:
aload 0
iload 1
iload 2
aload 3
invokespecial android/support/v4/widget/ExploreByTouchHelper/performActionForChild(IILandroid/os/Bundle;)Z
ireturn
L0:
aload 0
iload 2
aload 3
invokespecial android/support/v4/widget/ExploreByTouchHelper/performActionForHost(ILandroid/os/Bundle;)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method private performActionForChild(IILandroid/os/Bundle;)Z
iload 2
lookupswitch
64 : L0
128 : L0
default : L1
L1:
aload 0
iload 1
iload 2
aload 3
invokevirtual android/support/v4/widget/ExploreByTouchHelper/onPerformActionForVirtualView(IILandroid/os/Bundle;)Z
ireturn
L0:
aload 0
iload 1
iload 2
aload 3
invokespecial android/support/v4/widget/ExploreByTouchHelper/manageFocusForChild(IILandroid/os/Bundle;)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method private performActionForHost(ILandroid/os/Bundle;)Z
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
iload 1
aload 2
invokestatic android/support/v4/view/ViewCompat/performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method private requestAccessibilityFocus(I)Z
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mManager Landroid/view/accessibility/AccessibilityManager;
invokevirtual android/view/accessibility/AccessibilityManager/isEnabled()Z
ifeq L0
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mManager Landroid/view/accessibility/AccessibilityManager;
invokestatic android/support/v4/view/accessibility/AccessibilityManagerCompat/isTouchExplorationEnabled(Landroid/view/accessibility/AccessibilityManager;)Z
ifne L1
L0:
iconst_0
ireturn
L1:
aload 0
iload 1
invokespecial android/support/v4/widget/ExploreByTouchHelper/isAccessibilityFocused(I)Z
ifne L0
aload 0
iload 1
putfield android/support/v4/widget/ExploreByTouchHelper/mFocusedVirtualViewId I
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
invokevirtual android/view/View/invalidate()V
aload 0
iload 1
ldc_w 32768
invokevirtual android/support/v4/widget/ExploreByTouchHelper/sendEventForVirtualView(II)Z
pop
iconst_1
ireturn
.limit locals 2
.limit stack 3
.end method

.method private updateHoveredVirtualView(I)V
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mHoveredVirtualViewId I
iload 1
if_icmpne L0
return
L0:
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mHoveredVirtualViewId I
istore 2
aload 0
iload 1
putfield android/support/v4/widget/ExploreByTouchHelper/mHoveredVirtualViewId I
aload 0
iload 1
sipush 128
invokevirtual android/support/v4/widget/ExploreByTouchHelper/sendEventForVirtualView(II)Z
pop
aload 0
iload 2
sipush 256
invokevirtual android/support/v4/widget/ExploreByTouchHelper/sendEventForVirtualView(II)Z
pop
return
.limit locals 3
.limit stack 3
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
iconst_1
istore 3
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mManager Landroid/view/accessibility/AccessibilityManager;
invokevirtual android/view/accessibility/AccessibilityManager/isEnabled()Z
ifeq L0
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mManager Landroid/view/accessibility/AccessibilityManager;
invokestatic android/support/v4/view/accessibility/AccessibilityManagerCompat/isTouchExplorationEnabled(Landroid/view/accessibility/AccessibilityManager;)Z
ifne L1
L0:
iconst_0
ireturn
L1:
aload 1
invokevirtual android/view/MotionEvent/getAction()I
tableswitch 7
L2
L3
L2
L4
default : L3
L3:
iconst_0
ireturn
L2:
aload 0
aload 1
invokevirtual android/view/MotionEvent/getX()F
aload 1
invokevirtual android/view/MotionEvent/getY()F
invokevirtual android/support/v4/widget/ExploreByTouchHelper/getVirtualViewAt(FF)I
istore 2
aload 0
iload 2
invokespecial android/support/v4/widget/ExploreByTouchHelper/updateHoveredVirtualView(I)V
iload 2
ldc_w -2147483648
if_icmpeq L5
L6:
iload 3
ireturn
L5:
iconst_0
istore 3
goto L6
L4:
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mFocusedVirtualViewId I
ldc_w -2147483648
if_icmpeq L0
aload 0
ldc_w -2147483648
invokespecial android/support/v4/widget/ExploreByTouchHelper/updateHoveredVirtualView(I)V
iconst_1
ireturn
.limit locals 4
.limit stack 3
.end method

.method public getAccessibilityNodeProvider(Landroid/view/View;)Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mNodeProvider Landroid/support/v4/widget/ExploreByTouchHelper$ExploreByTouchNodeProvider;
ifnonnull L0
aload 0
new android/support/v4/widget/ExploreByTouchHelper$ExploreByTouchNodeProvider
dup
aload 0
aconst_null
invokespecial android/support/v4/widget/ExploreByTouchHelper$ExploreByTouchNodeProvider/<init>(Landroid/support/v4/widget/ExploreByTouchHelper;Landroid/support/v4/widget/ExploreByTouchHelper$1;)V
putfield android/support/v4/widget/ExploreByTouchHelper/mNodeProvider Landroid/support/v4/widget/ExploreByTouchHelper$ExploreByTouchNodeProvider;
L0:
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mNodeProvider Landroid/support/v4/widget/ExploreByTouchHelper$ExploreByTouchNodeProvider;
areturn
.limit locals 2
.limit stack 5
.end method

.method public getFocusedVirtualView()I
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mFocusedVirtualViewId I
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected abstract getVirtualViewAt(FF)I
.end method

.method protected abstract getVisibleVirtualViews(Ljava/util/List;)V
.signature "(Ljava/util/List<Ljava/lang/Integer;>;)V"
.end method

.method public invalidateRoot()V
aload 0
iconst_m1
invokevirtual android/support/v4/widget/ExploreByTouchHelper/invalidateVirtualView(I)V
return
.limit locals 1
.limit stack 2
.end method

.method public invalidateVirtualView(I)V
aload 0
iload 1
sipush 2048
invokevirtual android/support/v4/widget/ExploreByTouchHelper/sendEventForVirtualView(II)Z
pop
return
.limit locals 2
.limit stack 3
.end method

.method protected abstract onPerformActionForVirtualView(IILandroid/os/Bundle;)Z
.end method

.method protected abstract onPopulateEventForVirtualView(ILandroid/view/accessibility/AccessibilityEvent;)V
.end method

.method protected abstract onPopulateNodeForVirtualView(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
.end method

.method public sendEventForVirtualView(II)Z
iload 1
ldc_w -2147483648
if_icmpeq L0
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mManager Landroid/view/accessibility/AccessibilityManager;
invokevirtual android/view/accessibility/AccessibilityManager/isEnabled()Z
ifne L1
L0:
iconst_0
ireturn
L1:
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 3
aload 3
ifnull L0
aload 0
iload 1
iload 2
invokespecial android/support/v4/widget/ExploreByTouchHelper/createEvent(II)Landroid/view/accessibility/AccessibilityEvent;
astore 4
aload 3
aload 0
getfield android/support/v4/widget/ExploreByTouchHelper/mView Landroid/view/View;
aload 4
invokestatic android/support/v4/view/ViewParentCompat/requestSendAccessibilityEvent(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
ireturn
.limit locals 5
.limit stack 3
.end method
