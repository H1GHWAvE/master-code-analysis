.bytecode 50.0
.class public synchronized android/support/v4/widget/DrawerLayout
.super android/view/ViewGroup
.inner class AccessibilityDelegate inner android/support/v4/widget/DrawerLayout$AccessibilityDelegate outer android/support/v4/widget/DrawerLayout
.inner class public static abstract interface DrawerListener inner android/support/v4/widget/DrawerLayout$DrawerListener outer android/support/v4/widget/DrawerLayout
.inner class public static LayoutParams inner android/support/v4/widget/DrawerLayout$LayoutParams outer android/support/v4/widget/DrawerLayout
.inner class protected static SavedState inner android/support/v4/widget/DrawerLayout$SavedState outer android/support/v4/widget/DrawerLayout
.inner class static final inner android/support/v4/widget/DrawerLayout$SavedState$1
.inner class public static abstract SimpleDrawerListener inner android/support/v4/widget/DrawerLayout$SimpleDrawerListener outer android/support/v4/widget/DrawerLayout
.inner class private ViewDragCallback inner android/support/v4/widget/DrawerLayout$ViewDragCallback outer android/support/v4/widget/DrawerLayout
.inner class inner android/support/v4/widget/DrawerLayout$ViewDragCallback$1

.field private static final 'ALLOW_EDGE_LOCK' Z = 0


.field private static final 'CHILDREN_DISALLOW_INTERCEPT' Z = 1


.field private static final 'DEFAULT_SCRIM_COLOR' I = -1728053248


.field private static final 'LAYOUT_ATTRS' [I

.field public static final 'LOCK_MODE_LOCKED_CLOSED' I = 1


.field public static final 'LOCK_MODE_LOCKED_OPEN' I = 2


.field public static final 'LOCK_MODE_UNLOCKED' I = 0


.field private static final 'MIN_DRAWER_MARGIN' I = 64


.field private static final 'MIN_FLING_VELOCITY' I = 400


.field private static final 'PEEK_DELAY' I = 160


.field public static final 'STATE_DRAGGING' I = 1


.field public static final 'STATE_IDLE' I = 0


.field public static final 'STATE_SETTLING' I = 2


.field private static final 'TAG' Ljava/lang/String; = "DrawerLayout"

.field private static final 'TOUCH_SLOP_SENSITIVITY' F = 1.0F


.field private 'mChildrenCanceledTouch' Z

.field private 'mDisallowInterceptRequested' Z

.field private 'mDrawerState' I

.field private 'mFirstLayout' Z

.field private 'mInLayout' Z

.field private 'mInitialMotionX' F

.field private 'mInitialMotionY' F

.field private final 'mLeftCallback' Landroid/support/v4/widget/DrawerLayout$ViewDragCallback;

.field private final 'mLeftDragger' Landroid/support/v4/widget/ViewDragHelper;

.field private 'mListener' Landroid/support/v4/widget/DrawerLayout$DrawerListener;

.field private 'mLockModeLeft' I

.field private 'mLockModeRight' I

.field private 'mMinDrawerMargin' I

.field private final 'mRightCallback' Landroid/support/v4/widget/DrawerLayout$ViewDragCallback;

.field private final 'mRightDragger' Landroid/support/v4/widget/ViewDragHelper;

.field private 'mScrimColor' I

.field private 'mScrimOpacity' F

.field private 'mScrimPaint' Landroid/graphics/Paint;

.field private 'mShadowLeft' Landroid/graphics/drawable/Drawable;

.field private 'mShadowRight' Landroid/graphics/drawable/Drawable;

.method static <clinit>()V
iconst_1
newarray int
dup
iconst_0
ldc_w 16842931
iastore
putstatic android/support/v4/widget/DrawerLayout/LAYOUT_ATTRS [I
return
.limit locals 0
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v4/widget/DrawerLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/v4/widget/DrawerLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
ldc_w -1728053248
putfield android/support/v4/widget/DrawerLayout/mScrimColor I
aload 0
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
putfield android/support/v4/widget/DrawerLayout/mScrimPaint Landroid/graphics/Paint;
aload 0
iconst_1
putfield android/support/v4/widget/DrawerLayout/mFirstLayout Z
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fstore 4
aload 0
ldc_w 64.0F
fload 4
fmul
ldc_w 0.5F
fadd
f2i
putfield android/support/v4/widget/DrawerLayout/mMinDrawerMargin I
ldc_w 400.0F
fload 4
fmul
fstore 4
aload 0
new android/support/v4/widget/DrawerLayout$ViewDragCallback
dup
aload 0
iconst_3
invokespecial android/support/v4/widget/DrawerLayout$ViewDragCallback/<init>(Landroid/support/v4/widget/DrawerLayout;I)V
putfield android/support/v4/widget/DrawerLayout/mLeftCallback Landroid/support/v4/widget/DrawerLayout$ViewDragCallback;
aload 0
new android/support/v4/widget/DrawerLayout$ViewDragCallback
dup
aload 0
iconst_5
invokespecial android/support/v4/widget/DrawerLayout$ViewDragCallback/<init>(Landroid/support/v4/widget/DrawerLayout;I)V
putfield android/support/v4/widget/DrawerLayout/mRightCallback Landroid/support/v4/widget/DrawerLayout$ViewDragCallback;
aload 0
aload 0
fconst_1
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftCallback Landroid/support/v4/widget/DrawerLayout$ViewDragCallback;
invokestatic android/support/v4/widget/ViewDragHelper/create(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ViewDragHelper$Callback;)Landroid/support/v4/widget/ViewDragHelper;
putfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
iconst_1
invokevirtual android/support/v4/widget/ViewDragHelper/setEdgeTrackingEnabled(I)V
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
fload 4
invokevirtual android/support/v4/widget/ViewDragHelper/setMinVelocity(F)V
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftCallback Landroid/support/v4/widget/DrawerLayout$ViewDragCallback;
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/DrawerLayout$ViewDragCallback/setDragger(Landroid/support/v4/widget/ViewDragHelper;)V
aload 0
aload 0
fconst_1
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightCallback Landroid/support/v4/widget/DrawerLayout$ViewDragCallback;
invokestatic android/support/v4/widget/ViewDragHelper/create(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ViewDragHelper$Callback;)Landroid/support/v4/widget/ViewDragHelper;
putfield android/support/v4/widget/DrawerLayout/mRightDragger Landroid/support/v4/widget/ViewDragHelper;
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightDragger Landroid/support/v4/widget/ViewDragHelper;
iconst_2
invokevirtual android/support/v4/widget/ViewDragHelper/setEdgeTrackingEnabled(I)V
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightDragger Landroid/support/v4/widget/ViewDragHelper;
fload 4
invokevirtual android/support/v4/widget/ViewDragHelper/setMinVelocity(F)V
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightCallback Landroid/support/v4/widget/DrawerLayout$ViewDragCallback;
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightDragger Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/DrawerLayout$ViewDragCallback/setDragger(Landroid/support/v4/widget/ViewDragHelper;)V
aload 0
iconst_1
invokevirtual android/support/v4/widget/DrawerLayout/setFocusableInTouchMode(Z)V
aload 0
new android/support/v4/widget/DrawerLayout$AccessibilityDelegate
dup
aload 0
invokespecial android/support/v4/widget/DrawerLayout$AccessibilityDelegate/<init>(Landroid/support/v4/widget/DrawerLayout;)V
invokestatic android/support/v4/view/ViewCompat/setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V
aload 0
iconst_0
invokestatic android/support/v4/view/ViewGroupCompat/setMotionEventSplittingEnabled(Landroid/view/ViewGroup;Z)V
return
.limit locals 5
.limit stack 5
.end method

.method static synthetic access$100()[I
getstatic android/support/v4/widget/DrawerLayout/LAYOUT_ATTRS [I
areturn
.limit locals 0
.limit stack 1
.end method

.method private findVisibleDrawer()Landroid/view/View;
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 3
aload 0
aload 3
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerView(Landroid/view/View;)Z
ifeq L2
aload 0
aload 3
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerVisible(Landroid/view/View;)Z
ifeq L2
aload 3
areturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aconst_null
areturn
.limit locals 4
.limit stack 2
.end method

.method static gravityToString(I)Ljava/lang/String;
iload 0
iconst_3
iand
iconst_3
if_icmpne L0
ldc "LEFT"
areturn
L0:
iload 0
iconst_5
iand
iconst_5
if_icmpne L1
ldc "RIGHT"
areturn
L1:
iload 0
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static hasOpaqueBackground(Landroid/view/View;)Z
iconst_0
istore 2
aload 0
invokevirtual android/view/View/getBackground()Landroid/graphics/drawable/Drawable;
astore 0
iload 2
istore 1
aload 0
ifnull L0
iload 2
istore 1
aload 0
invokevirtual android/graphics/drawable/Drawable/getOpacity()I
iconst_m1
if_icmpne L0
iconst_1
istore 1
L0:
iload 1
ireturn
.limit locals 3
.limit stack 2
.end method

.method private hasPeekingDrawer()Z
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
getfield android/support/v4/widget/DrawerLayout$LayoutParams/isPeeking Z
ifeq L2
iconst_1
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method private hasVisibleDrawer()Z
aload 0
invokespecial android/support/v4/widget/DrawerLayout/findVisibleDrawer()Landroid/view/View;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method cancelChildViewTouch()V
aload 0
getfield android/support/v4/widget/DrawerLayout/mChildrenCanceledTouch Z
ifne L0
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 3
lload 3
lload 3
iconst_3
fconst_0
fconst_0
iconst_0
invokestatic android/view/MotionEvent/obtain(JJIFFI)Landroid/view/MotionEvent;
astore 5
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 2
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L2
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
aload 5
invokevirtual android/view/View/dispatchTouchEvent(Landroid/view/MotionEvent;)Z
pop
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 5
invokevirtual android/view/MotionEvent/recycle()V
aload 0
iconst_1
putfield android/support/v4/widget/DrawerLayout/mChildrenCanceledTouch Z
L0:
return
.limit locals 6
.limit stack 8
.end method

.method checkDrawerViewGravity(Landroid/view/View;I)Z
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/getDrawerViewGravity(Landroid/view/View;)I
iload 2
iand
iload 2
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
aload 1
instanceof android/support/v4/widget/DrawerLayout$LayoutParams
ifeq L0
aload 0
aload 1
invokespecial android/view/ViewGroup/checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public closeDrawer(I)V
iload 1
aload 0
invokestatic android/support/v4/view/ViewCompat/getLayoutDirection(Landroid/view/View;)I
invokestatic android/support/v4/view/GravityCompat/getAbsoluteGravity(II)I
istore 1
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/findDrawerWithGravity(I)Landroid/view/View;
astore 2
aload 2
ifnonnull L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "No drawer view found with absolute gravity "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokestatic android/support/v4/widget/DrawerLayout/gravityToString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 2
invokevirtual android/support/v4/widget/DrawerLayout/closeDrawer(Landroid/view/View;)V
return
.limit locals 3
.limit stack 4
.end method

.method public closeDrawer(Landroid/view/View;)V
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerView(Landroid/view/View;)Z
ifne L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "View "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " is not a sliding drawer"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/widget/DrawerLayout/mFirstLayout Z
ifeq L1
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
astore 1
aload 1
fconst_0
putfield android/support/v4/widget/DrawerLayout$LayoutParams/onScreen F
aload 1
iconst_0
putfield android/support/v4/widget/DrawerLayout$LayoutParams/knownOpen Z
L2:
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
return
L1:
aload 0
aload 1
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/checkDrawerViewGravity(Landroid/view/View;I)Z
ifeq L3
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
aload 1
aload 1
invokevirtual android/view/View/getWidth()I
ineg
aload 1
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/ViewDragHelper/smoothSlideViewTo(Landroid/view/View;II)Z
pop
goto L2
L3:
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightDragger Landroid/support/v4/widget/ViewDragHelper;
aload 1
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
aload 1
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/ViewDragHelper/smoothSlideViewTo(Landroid/view/View;II)Z
pop
goto L2
.limit locals 2
.limit stack 4
.end method

.method public closeDrawers()V
aload 0
iconst_0
invokevirtual android/support/v4/widget/DrawerLayout/closeDrawers(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method closeDrawers(Z)V
iconst_0
istore 2
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 5
iconst_0
istore 3
L0:
iload 3
iload 5
if_icmpge L1
aload 0
iload 3
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 6
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
astore 7
iload 2
istore 4
aload 0
aload 6
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerView(Landroid/view/View;)Z
ifeq L2
iload 1
ifeq L3
aload 7
getfield android/support/v4/widget/DrawerLayout$LayoutParams/isPeeking Z
ifne L3
iload 2
istore 4
L2:
iload 3
iconst_1
iadd
istore 3
iload 4
istore 2
goto L0
L3:
aload 6
invokevirtual android/view/View/getWidth()I
istore 4
aload 0
aload 6
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/checkDrawerViewGravity(Landroid/view/View;I)Z
ifeq L4
iload 2
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
aload 6
iload 4
ineg
aload 6
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/ViewDragHelper/smoothSlideViewTo(Landroid/view/View;II)Z
ior
istore 2
L5:
aload 7
iconst_0
putfield android/support/v4/widget/DrawerLayout$LayoutParams/isPeeking Z
iload 2
istore 4
goto L2
L4:
iload 2
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightDragger Landroid/support/v4/widget/ViewDragHelper;
aload 6
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
aload 6
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/ViewDragHelper/smoothSlideViewTo(Landroid/view/View;II)Z
ior
istore 2
goto L5
L1:
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftCallback Landroid/support/v4/widget/DrawerLayout$ViewDragCallback;
invokevirtual android/support/v4/widget/DrawerLayout$ViewDragCallback/removeCallbacks()V
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightCallback Landroid/support/v4/widget/DrawerLayout$ViewDragCallback;
invokevirtual android/support/v4/widget/DrawerLayout$ViewDragCallback/removeCallbacks()V
iload 2
ifeq L6
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
L6:
return
.limit locals 8
.limit stack 5
.end method

.method public computeScroll()V
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 3
fconst_0
fstore 1
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
fload 1
aload 0
iload 2
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
getfield android/support/v4/widget/DrawerLayout$LayoutParams/onScreen F
invokestatic java/lang/Math/max(FF)F
fstore 1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
fload 1
putfield android/support/v4/widget/DrawerLayout/mScrimOpacity F
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
iconst_1
invokevirtual android/support/v4/widget/ViewDragHelper/continueSettling(Z)Z
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightDragger Landroid/support/v4/widget/ViewDragHelper;
iconst_1
invokevirtual android/support/v4/widget/ViewDragHelper/continueSettling(Z)Z
ior
ifeq L2
aload 0
invokestatic android/support/v4/view/ViewCompat/postInvalidateOnAnimation(Landroid/view/View;)V
L2:
return
.limit locals 4
.limit stack 3
.end method

.method dispatchOnDrawerClosed(Landroid/view/View;)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
astore 2
aload 2
getfield android/support/v4/widget/DrawerLayout$LayoutParams/knownOpen Z
ifeq L0
aload 2
iconst_0
putfield android/support/v4/widget/DrawerLayout$LayoutParams/knownOpen Z
aload 0
getfield android/support/v4/widget/DrawerLayout/mListener Landroid/support/v4/widget/DrawerLayout$DrawerListener;
ifnull L1
aload 0
getfield android/support/v4/widget/DrawerLayout/mListener Landroid/support/v4/widget/DrawerLayout$DrawerListener;
aload 1
invokeinterface android/support/v4/widget/DrawerLayout$DrawerListener/onDrawerClosed(Landroid/view/View;)V 1
L1:
aload 0
bipush 32
invokevirtual android/support/v4/widget/DrawerLayout/sendAccessibilityEvent(I)V
L0:
return
.limit locals 3
.limit stack 2
.end method

.method dispatchOnDrawerOpened(Landroid/view/View;)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
astore 2
aload 2
getfield android/support/v4/widget/DrawerLayout$LayoutParams/knownOpen Z
ifne L0
aload 2
iconst_1
putfield android/support/v4/widget/DrawerLayout$LayoutParams/knownOpen Z
aload 0
getfield android/support/v4/widget/DrawerLayout/mListener Landroid/support/v4/widget/DrawerLayout$DrawerListener;
ifnull L1
aload 0
getfield android/support/v4/widget/DrawerLayout/mListener Landroid/support/v4/widget/DrawerLayout$DrawerListener;
aload 1
invokeinterface android/support/v4/widget/DrawerLayout$DrawerListener/onDrawerOpened(Landroid/view/View;)V 1
L1:
aload 1
bipush 32
invokevirtual android/view/View/sendAccessibilityEvent(I)V
L0:
return
.limit locals 3
.limit stack 2
.end method

.method dispatchOnDrawerSlide(Landroid/view/View;F)V
aload 0
getfield android/support/v4/widget/DrawerLayout/mListener Landroid/support/v4/widget/DrawerLayout$DrawerListener;
ifnull L0
aload 0
getfield android/support/v4/widget/DrawerLayout/mListener Landroid/support/v4/widget/DrawerLayout$DrawerListener;
aload 1
fload 2
invokeinterface android/support/v4/widget/DrawerLayout$DrawerListener/onDrawerSlide(Landroid/view/View;F)V 2
L0:
return
.limit locals 3
.limit stack 3
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getHeight()I
istore 12
aload 0
aload 2
invokevirtual android/support/v4/widget/DrawerLayout/isContentView(Landroid/view/View;)Z
istore 15
iconst_0
istore 6
iconst_0
istore 9
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
istore 7
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 13
iload 7
istore 8
iload 15
ifeq L0
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 14
iconst_0
istore 8
iload 9
istore 6
L1:
iload 8
iload 14
if_icmpge L2
aload 0
iload 8
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 17
iload 6
istore 9
iload 7
istore 10
aload 17
aload 2
if_acmpeq L3
iload 6
istore 9
iload 7
istore 10
aload 17
invokevirtual android/view/View/getVisibility()I
ifne L3
iload 6
istore 9
iload 7
istore 10
aload 17
invokestatic android/support/v4/widget/DrawerLayout/hasOpaqueBackground(Landroid/view/View;)Z
ifeq L3
iload 6
istore 9
iload 7
istore 10
aload 0
aload 17
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerView(Landroid/view/View;)Z
ifeq L3
aload 17
invokevirtual android/view/View/getHeight()I
iload 12
if_icmpge L4
iload 7
istore 10
iload 6
istore 9
L3:
iload 8
iconst_1
iadd
istore 8
iload 9
istore 6
iload 10
istore 7
goto L1
L4:
aload 0
aload 17
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/checkDrawerViewGravity(Landroid/view/View;I)Z
ifeq L5
aload 17
invokevirtual android/view/View/getRight()I
istore 11
iload 6
istore 9
iload 7
istore 10
iload 11
iload 6
if_icmple L3
iload 11
istore 9
iload 7
istore 10
goto L3
L5:
aload 17
invokevirtual android/view/View/getLeft()I
istore 11
iload 6
istore 9
iload 7
istore 10
iload 11
iload 7
if_icmpge L3
iload 11
istore 10
iload 6
istore 9
goto L3
L2:
aload 1
iload 6
iconst_0
iload 7
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getHeight()I
invokevirtual android/graphics/Canvas/clipRect(IIII)Z
pop
iload 7
istore 8
L0:
aload 0
aload 1
aload 2
lload 3
invokespecial android/view/ViewGroup/drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
istore 16
aload 1
iload 13
invokevirtual android/graphics/Canvas/restoreToCount(I)V
aload 0
getfield android/support/v4/widget/DrawerLayout/mScrimOpacity F
fconst_0
fcmpl
ifle L6
iload 15
ifeq L6
aload 0
getfield android/support/v4/widget/DrawerLayout/mScrimColor I
ldc_w -16777216
iand
bipush 24
iushr
i2f
aload 0
getfield android/support/v4/widget/DrawerLayout/mScrimOpacity F
fmul
f2i
istore 7
aload 0
getfield android/support/v4/widget/DrawerLayout/mScrimColor I
istore 9
aload 0
getfield android/support/v4/widget/DrawerLayout/mScrimPaint Landroid/graphics/Paint;
iload 7
bipush 24
ishl
iload 9
ldc_w 16777215
iand
ior
invokevirtual android/graphics/Paint/setColor(I)V
aload 1
iload 6
i2f
fconst_0
iload 8
i2f
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getHeight()I
i2f
aload 0
getfield android/support/v4/widget/DrawerLayout/mScrimPaint Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRect(FFFFLandroid/graphics/Paint;)V
L7:
iload 16
ireturn
L6:
aload 0
getfield android/support/v4/widget/DrawerLayout/mShadowLeft Landroid/graphics/drawable/Drawable;
ifnull L8
aload 0
aload 2
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/checkDrawerViewGravity(Landroid/view/View;I)Z
ifeq L8
aload 0
getfield android/support/v4/widget/DrawerLayout/mShadowLeft Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
istore 6
aload 2
invokevirtual android/view/View/getRight()I
istore 7
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/getEdgeSize()I
istore 8
fconst_0
iload 7
i2f
iload 8
i2f
fdiv
fconst_1
invokestatic java/lang/Math/min(FF)F
invokestatic java/lang/Math/max(FF)F
fstore 5
aload 0
getfield android/support/v4/widget/DrawerLayout/mShadowLeft Landroid/graphics/drawable/Drawable;
iload 7
aload 2
invokevirtual android/view/View/getTop()I
iload 7
iload 6
iadd
aload 2
invokevirtual android/view/View/getBottom()I
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield android/support/v4/widget/DrawerLayout/mShadowLeft Landroid/graphics/drawable/Drawable;
ldc_w 255.0F
fload 5
fmul
f2i
invokevirtual android/graphics/drawable/Drawable/setAlpha(I)V
aload 0
getfield android/support/v4/widget/DrawerLayout/mShadowLeft Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
iload 16
ireturn
L8:
aload 0
getfield android/support/v4/widget/DrawerLayout/mShadowRight Landroid/graphics/drawable/Drawable;
ifnull L7
aload 0
aload 2
iconst_5
invokevirtual android/support/v4/widget/DrawerLayout/checkDrawerViewGravity(Landroid/view/View;I)Z
ifeq L7
aload 0
getfield android/support/v4/widget/DrawerLayout/mShadowRight Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
istore 6
aload 2
invokevirtual android/view/View/getLeft()I
istore 7
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
istore 8
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightDragger Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/getEdgeSize()I
istore 9
fconst_0
iload 8
iload 7
isub
i2f
iload 9
i2f
fdiv
fconst_1
invokestatic java/lang/Math/min(FF)F
invokestatic java/lang/Math/max(FF)F
fstore 5
aload 0
getfield android/support/v4/widget/DrawerLayout/mShadowRight Landroid/graphics/drawable/Drawable;
iload 7
iload 6
isub
aload 2
invokevirtual android/view/View/getTop()I
iload 7
aload 2
invokevirtual android/view/View/getBottom()I
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield android/support/v4/widget/DrawerLayout/mShadowRight Landroid/graphics/drawable/Drawable;
ldc_w 255.0F
fload 5
fmul
f2i
invokevirtual android/graphics/drawable/Drawable/setAlpha(I)V
aload 0
getfield android/support/v4/widget/DrawerLayout/mShadowRight Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
iload 16
ireturn
.limit locals 18
.limit stack 6
.end method

.method findDrawerWithGravity(I)Landroid/view/View;
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 4
aload 0
aload 4
invokevirtual android/support/v4/widget/DrawerLayout/getDrawerViewGravity(Landroid/view/View;)I
bipush 7
iand
iload 1
bipush 7
iand
if_icmpne L2
aload 4
areturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aconst_null
areturn
.limit locals 5
.limit stack 3
.end method

.method findOpenDrawer()Landroid/view/View;
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 3
aload 3
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
getfield android/support/v4/widget/DrawerLayout$LayoutParams/knownOpen Z
ifeq L2
aload 3
areturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aconst_null
areturn
.limit locals 4
.limit stack 2
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
new android/support/v4/widget/DrawerLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/support/v4/widget/DrawerLayout$LayoutParams/<init>(II)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
new android/support/v4/widget/DrawerLayout$LayoutParams
dup
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/v4/widget/DrawerLayout$LayoutParams/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
aload 1
instanceof android/support/v4/widget/DrawerLayout$LayoutParams
ifeq L0
new android/support/v4/widget/DrawerLayout$LayoutParams
dup
aload 1
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
invokespecial android/support/v4/widget/DrawerLayout$LayoutParams/<init>(Landroid/support/v4/widget/DrawerLayout$LayoutParams;)V
areturn
L0:
aload 1
instanceof android/view/ViewGroup$MarginLayoutParams
ifeq L1
new android/support/v4/widget/DrawerLayout$LayoutParams
dup
aload 1
checkcast android/view/ViewGroup$MarginLayoutParams
invokespecial android/support/v4/widget/DrawerLayout$LayoutParams/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
areturn
L1:
new android/support/v4/widget/DrawerLayout$LayoutParams
dup
aload 1
invokespecial android/support/v4/widget/DrawerLayout$LayoutParams/<init>(Landroid/view/ViewGroup$LayoutParams;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public getDrawerLockMode(I)I
iload 1
aload 0
invokestatic android/support/v4/view/ViewCompat/getLayoutDirection(Landroid/view/View;)I
invokestatic android/support/v4/view/GravityCompat/getAbsoluteGravity(II)I
istore 1
iload 1
iconst_3
if_icmpne L0
aload 0
getfield android/support/v4/widget/DrawerLayout/mLockModeLeft I
ireturn
L0:
iload 1
iconst_5
if_icmpne L1
aload 0
getfield android/support/v4/widget/DrawerLayout/mLockModeRight I
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getDrawerLockMode(Landroid/view/View;)I
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/getDrawerViewGravity(Landroid/view/View;)I
istore 2
iload 2
iconst_3
if_icmpne L0
aload 0
getfield android/support/v4/widget/DrawerLayout/mLockModeLeft I
ireturn
L0:
iload 2
iconst_5
if_icmpne L1
aload 0
getfield android/support/v4/widget/DrawerLayout/mLockModeRight I
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method getDrawerViewGravity(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
getfield android/support/v4/widget/DrawerLayout$LayoutParams/gravity I
aload 1
invokestatic android/support/v4/view/ViewCompat/getLayoutDirection(Landroid/view/View;)I
invokestatic android/support/v4/view/GravityCompat/getAbsoluteGravity(II)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method getDrawerViewOffset(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
getfield android/support/v4/widget/DrawerLayout$LayoutParams/onScreen F
freturn
.limit locals 2
.limit stack 1
.end method

.method isContentView(Landroid/view/View;)Z
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
getfield android/support/v4/widget/DrawerLayout$LayoutParams/gravity I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public isDrawerOpen(I)Z
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/findDrawerWithGravity(I)Landroid/view/View;
astore 2
aload 2
ifnull L0
aload 0
aload 2
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerOpen(Landroid/view/View;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public isDrawerOpen(Landroid/view/View;)Z
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerView(Landroid/view/View;)Z
ifne L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "View "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " is not a drawer"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
getfield android/support/v4/widget/DrawerLayout$LayoutParams/knownOpen Z
ireturn
.limit locals 2
.limit stack 4
.end method

.method isDrawerView(Landroid/view/View;)Z
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
getfield android/support/v4/widget/DrawerLayout$LayoutParams/gravity I
aload 1
invokestatic android/support/v4/view/ViewCompat/getLayoutDirection(Landroid/view/View;)I
invokestatic android/support/v4/view/GravityCompat/getAbsoluteGravity(II)I
bipush 7
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public isDrawerVisible(I)Z
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/findDrawerWithGravity(I)Landroid/view/View;
astore 2
aload 2
ifnull L0
aload 0
aload 2
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerVisible(Landroid/view/View;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public isDrawerVisible(Landroid/view/View;)Z
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerView(Landroid/view/View;)Z
ifne L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "View "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " is not a drawer"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
getfield android/support/v4/widget/DrawerLayout$LayoutParams/onScreen F
fconst_0
fcmpl
ifle L1
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method moveDrawerToOffset(Landroid/view/View;F)V
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/getDrawerViewOffset(Landroid/view/View;)F
fstore 3
aload 1
invokevirtual android/view/View/getWidth()I
istore 4
iload 4
i2f
fload 3
fmul
f2i
istore 5
iload 4
i2f
fload 2
fmul
f2i
iload 5
isub
istore 4
aload 0
aload 1
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/checkDrawerViewGravity(Landroid/view/View;I)Z
ifeq L0
L1:
aload 1
iload 4
invokevirtual android/view/View/offsetLeftAndRight(I)V
aload 0
aload 1
fload 2
invokevirtual android/support/v4/widget/DrawerLayout/setDrawerViewOffset(Landroid/view/View;F)V
return
L0:
iload 4
ineg
istore 4
goto L1
.limit locals 6
.limit stack 3
.end method

.method protected onAttachedToWindow()V
aload 0
invokespecial android/view/ViewGroup/onAttachedToWindow()V
aload 0
iconst_1
putfield android/support/v4/widget/DrawerLayout/mFirstLayout Z
return
.limit locals 1
.limit stack 2
.end method

.method protected onDetachedFromWindow()V
aload 0
invokespecial android/view/ViewGroup/onDetachedFromWindow()V
aload 0
iconst_1
putfield android/support/v4/widget/DrawerLayout/mFirstLayout Z
return
.limit locals 1
.limit stack 2
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
iconst_0
istore 7
aload 1
invokestatic android/support/v4/view/MotionEventCompat/getActionMasked(Landroid/view/MotionEvent;)I
istore 4
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
aload 1
invokevirtual android/support/v4/widget/ViewDragHelper/shouldInterceptTouchEvent(Landroid/view/MotionEvent;)Z
istore 8
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightDragger Landroid/support/v4/widget/ViewDragHelper;
aload 1
invokevirtual android/support/v4/widget/ViewDragHelper/shouldInterceptTouchEvent(Landroid/view/MotionEvent;)Z
istore 9
iconst_0
istore 6
iconst_0
istore 5
iload 4
tableswitch 0
L0
L1
L2
L1
default : L3
L3:
iload 5
istore 4
L4:
iload 8
iload 9
ior
ifne L5
iload 4
ifne L5
aload 0
invokespecial android/support/v4/widget/DrawerLayout/hasPeekingDrawer()Z
ifne L5
aload 0
getfield android/support/v4/widget/DrawerLayout/mChildrenCanceledTouch Z
ifeq L6
L5:
iconst_1
istore 7
L6:
iload 7
ireturn
L0:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
aload 0
fload 2
putfield android/support/v4/widget/DrawerLayout/mInitialMotionX F
aload 0
fload 3
putfield android/support/v4/widget/DrawerLayout/mInitialMotionY F
iload 6
istore 4
aload 0
getfield android/support/v4/widget/DrawerLayout/mScrimOpacity F
fconst_0
fcmpl
ifle L7
iload 6
istore 4
aload 0
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
fload 2
f2i
fload 3
f2i
invokevirtual android/support/v4/widget/ViewDragHelper/findTopChildUnder(II)Landroid/view/View;
invokevirtual android/support/v4/widget/DrawerLayout/isContentView(Landroid/view/View;)Z
ifeq L7
iconst_1
istore 4
L7:
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/mDisallowInterceptRequested Z
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/mChildrenCanceledTouch Z
goto L4
L2:
iload 5
istore 4
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
iconst_3
invokevirtual android/support/v4/widget/ViewDragHelper/checkTouchSlop(I)Z
ifeq L4
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftCallback Landroid/support/v4/widget/DrawerLayout$ViewDragCallback;
invokevirtual android/support/v4/widget/DrawerLayout$ViewDragCallback/removeCallbacks()V
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightCallback Landroid/support/v4/widget/DrawerLayout$ViewDragCallback;
invokevirtual android/support/v4/widget/DrawerLayout$ViewDragCallback/removeCallbacks()V
iload 5
istore 4
goto L4
L1:
aload 0
iconst_1
invokevirtual android/support/v4/widget/DrawerLayout/closeDrawers(Z)V
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/mDisallowInterceptRequested Z
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/mChildrenCanceledTouch Z
iload 5
istore 4
goto L4
.limit locals 10
.limit stack 4
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
iload 1
iconst_4
if_icmpne L0
aload 0
invokespecial android/support/v4/widget/DrawerLayout/hasVisibleDrawer()Z
ifeq L0
aload 2
invokestatic android/support/v4/view/KeyEventCompat/startTracking(Landroid/view/KeyEvent;)V
iconst_1
ireturn
L0:
aload 0
iload 1
aload 2
invokespecial android/view/ViewGroup/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
iload 1
iconst_4
if_icmpne L0
aload 0
invokespecial android/support/v4/widget/DrawerLayout/findVisibleDrawer()Landroid/view/View;
astore 2
aload 2
ifnull L1
aload 0
aload 2
invokevirtual android/support/v4/widget/DrawerLayout/getDrawerLockMode(Landroid/view/View;)I
ifne L1
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/closeDrawers()V
L1:
aload 2
ifnull L2
iconst_1
ireturn
L2:
iconst_0
ireturn
L0:
aload 0
iload 1
aload 2
invokespecial android/view/ViewGroup/onKeyUp(ILandroid/view/KeyEvent;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method protected onLayout(ZIIII)V
aload 0
iconst_1
putfield android/support/v4/widget/DrawerLayout/mInLayout Z
iload 4
iload 2
isub
istore 10
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 11
iconst_0
istore 4
L0:
iload 4
iload 11
if_icmpge L1
aload 0
iload 4
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 15
aload 15
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpne L2
L3:
iload 4
iconst_1
iadd
istore 4
goto L0
L2:
aload 15
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
astore 16
aload 0
aload 15
invokevirtual android/support/v4/widget/DrawerLayout/isContentView(Landroid/view/View;)Z
ifeq L4
aload 15
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/leftMargin I
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/topMargin I
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/leftMargin I
aload 15
invokevirtual android/view/View/getMeasuredWidth()I
iadd
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/topMargin I
aload 15
invokevirtual android/view/View/getMeasuredHeight()I
iadd
invokevirtual android/view/View/layout(IIII)V
goto L3
L4:
aload 15
invokevirtual android/view/View/getMeasuredWidth()I
istore 12
aload 15
invokevirtual android/view/View/getMeasuredHeight()I
istore 13
aload 0
aload 15
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/checkDrawerViewGravity(Landroid/view/View;I)Z
ifeq L5
iload 12
ineg
iload 12
i2f
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/onScreen F
fmul
f2i
iadd
istore 7
iload 12
iload 7
iadd
i2f
iload 12
i2f
fdiv
fstore 6
L6:
fload 6
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/onScreen F
fcmpl
ifeq L7
iconst_1
istore 8
L8:
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/gravity I
bipush 112
iand
lookupswitch
16 : L9
80 : L10
default : L11
L11:
aload 15
iload 7
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/topMargin I
iload 7
iload 12
iadd
iload 13
invokevirtual android/view/View/layout(IIII)V
L12:
iload 8
ifeq L13
aload 0
aload 15
fload 6
invokevirtual android/support/v4/widget/DrawerLayout/setDrawerViewOffset(Landroid/view/View;F)V
L13:
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/onScreen F
fconst_0
fcmpl
ifle L14
iconst_0
istore 2
L15:
aload 15
invokevirtual android/view/View/getVisibility()I
iload 2
if_icmpeq L3
aload 15
iload 2
invokevirtual android/view/View/setVisibility(I)V
goto L3
L5:
iload 10
iload 12
i2f
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/onScreen F
fmul
f2i
isub
istore 7
iload 10
iload 7
isub
i2f
iload 12
i2f
fdiv
fstore 6
goto L6
L7:
iconst_0
istore 8
goto L8
L10:
iload 5
iload 3
isub
istore 2
aload 15
iload 7
iload 2
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/bottomMargin I
isub
aload 15
invokevirtual android/view/View/getMeasuredHeight()I
isub
iload 7
iload 12
iadd
iload 2
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/bottomMargin I
isub
invokevirtual android/view/View/layout(IIII)V
goto L12
L9:
iload 5
iload 3
isub
istore 14
iload 14
iload 13
isub
iconst_2
idiv
istore 9
iload 9
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/topMargin I
if_icmpge L16
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/topMargin I
istore 2
L17:
aload 15
iload 7
iload 2
iload 7
iload 12
iadd
iload 2
iload 13
iadd
invokevirtual android/view/View/layout(IIII)V
goto L12
L16:
iload 9
istore 2
iload 9
iload 13
iadd
iload 14
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/bottomMargin I
isub
if_icmple L17
iload 14
aload 16
getfield android/support/v4/widget/DrawerLayout$LayoutParams/bottomMargin I
isub
iload 13
isub
istore 2
goto L17
L14:
iconst_4
istore 2
goto L15
L1:
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/mInLayout Z
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/mFirstLayout Z
return
.limit locals 17
.limit stack 6
.end method

.method protected onMeasure(II)V
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 8
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 7
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 3
iload 2
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 6
iload 8
ldc_w 1073741824
if_icmpne L0
iload 6
istore 4
iload 3
istore 5
iload 7
ldc_w 1073741824
if_icmpeq L1
L0:
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/isInEditMode()Z
ifeq L2
iload 8
ldc_w -2147483648
if_icmpne L3
L4:
iload 7
ldc_w -2147483648
if_icmpne L5
iload 3
istore 5
iload 6
istore 4
L1:
aload 0
iload 5
iload 4
invokevirtual android/support/v4/widget/DrawerLayout/setMeasuredDimension(II)V
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 6
iconst_0
istore 3
L6:
iload 3
iload 6
if_icmpge L7
aload 0
iload 3
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 9
aload 9
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpne L8
L9:
iload 3
iconst_1
iadd
istore 3
goto L6
L3:
iload 8
ifne L4
sipush 300
istore 3
goto L4
L5:
iload 6
istore 4
iload 3
istore 5
iload 7
ifne L1
sipush 300
istore 4
iload 3
istore 5
goto L1
L2:
new java/lang/IllegalArgumentException
dup
ldc "DrawerLayout must be measured with MeasureSpec.EXACTLY."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L8:
aload 9
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
astore 10
aload 0
aload 9
invokevirtual android/support/v4/widget/DrawerLayout/isContentView(Landroid/view/View;)Z
ifeq L10
aload 9
iload 5
aload 10
getfield android/support/v4/widget/DrawerLayout$LayoutParams/leftMargin I
isub
aload 10
getfield android/support/v4/widget/DrawerLayout$LayoutParams/rightMargin I
isub
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 4
aload 10
getfield android/support/v4/widget/DrawerLayout$LayoutParams/topMargin I
isub
aload 10
getfield android/support/v4/widget/DrawerLayout$LayoutParams/bottomMargin I
isub
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
goto L9
L10:
aload 0
aload 9
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerView(Landroid/view/View;)Z
ifeq L11
aload 0
aload 9
invokevirtual android/support/v4/widget/DrawerLayout/getDrawerViewGravity(Landroid/view/View;)I
bipush 7
iand
istore 7
iconst_0
iload 7
iand
ifeq L12
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Child drawer has absolute gravity "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 7
invokestatic android/support/v4/widget/DrawerLayout/gravityToString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " but this "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "DrawerLayout"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " already has a "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "drawer view along that edge"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L12:
aload 9
iload 1
aload 0
getfield android/support/v4/widget/DrawerLayout/mMinDrawerMargin I
aload 10
getfield android/support/v4/widget/DrawerLayout$LayoutParams/leftMargin I
iadd
aload 10
getfield android/support/v4/widget/DrawerLayout$LayoutParams/rightMargin I
iadd
aload 10
getfield android/support/v4/widget/DrawerLayout$LayoutParams/width I
invokestatic android/support/v4/widget/DrawerLayout/getChildMeasureSpec(III)I
iload 2
aload 10
getfield android/support/v4/widget/DrawerLayout$LayoutParams/topMargin I
aload 10
getfield android/support/v4/widget/DrawerLayout$LayoutParams/bottomMargin I
iadd
aload 10
getfield android/support/v4/widget/DrawerLayout$LayoutParams/height I
invokestatic android/support/v4/widget/DrawerLayout/getChildMeasureSpec(III)I
invokevirtual android/view/View/measure(II)V
goto L9
L11:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Child "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " at index "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " does not have a valid layout_gravity - must be Gravity.LEFT, "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "Gravity.RIGHT or Gravity.NO_GRAVITY"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L7:
return
.limit locals 11
.limit stack 5
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
checkcast android/support/v4/widget/DrawerLayout$SavedState
astore 1
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout$SavedState/getSuperState()Landroid/os/Parcelable;
invokespecial android/view/ViewGroup/onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
getfield android/support/v4/widget/DrawerLayout$SavedState/openDrawerGravity I
ifeq L0
aload 0
aload 1
getfield android/support/v4/widget/DrawerLayout$SavedState/openDrawerGravity I
invokevirtual android/support/v4/widget/DrawerLayout/findDrawerWithGravity(I)Landroid/view/View;
astore 2
aload 2
ifnull L0
aload 0
aload 2
invokevirtual android/support/v4/widget/DrawerLayout/openDrawer(Landroid/view/View;)V
L0:
aload 0
aload 1
getfield android/support/v4/widget/DrawerLayout$SavedState/lockModeLeft I
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/setDrawerLockMode(II)V
aload 0
aload 1
getfield android/support/v4/widget/DrawerLayout$SavedState/lockModeRight I
iconst_5
invokevirtual android/support/v4/widget/DrawerLayout/setDrawerLockMode(II)V
return
.limit locals 3
.limit stack 3
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
new android/support/v4/widget/DrawerLayout$SavedState
dup
aload 0
invokespecial android/view/ViewGroup/onSaveInstanceState()Landroid/os/Parcelable;
invokespecial android/support/v4/widget/DrawerLayout$SavedState/<init>(Landroid/os/Parcelable;)V
astore 3
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 4
aload 0
aload 4
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerView(Landroid/view/View;)Z
ifne L2
L3:
iload 1
iconst_1
iadd
istore 1
goto L0
L2:
aload 4
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
astore 4
aload 4
getfield android/support/v4/widget/DrawerLayout$LayoutParams/knownOpen Z
ifeq L3
aload 3
aload 4
getfield android/support/v4/widget/DrawerLayout$LayoutParams/gravity I
putfield android/support/v4/widget/DrawerLayout$SavedState/openDrawerGravity I
L1:
aload 3
aload 0
getfield android/support/v4/widget/DrawerLayout/mLockModeLeft I
putfield android/support/v4/widget/DrawerLayout$SavedState/lockModeLeft I
aload 3
aload 0
getfield android/support/v4/widget/DrawerLayout/mLockModeRight I
putfield android/support/v4/widget/DrawerLayout$SavedState/lockModeRight I
aload 3
areturn
.limit locals 5
.limit stack 3
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
aload 1
invokevirtual android/support/v4/widget/ViewDragHelper/processTouchEvent(Landroid/view/MotionEvent;)V
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightDragger Landroid/support/v4/widget/ViewDragHelper;
aload 1
invokevirtual android/support/v4/widget/ViewDragHelper/processTouchEvent(Landroid/view/MotionEvent;)V
aload 1
invokevirtual android/view/MotionEvent/getAction()I
sipush 255
iand
tableswitch 0
L0
L1
L2
L3
default : L2
L2:
iconst_1
ireturn
L0:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
aload 0
fload 2
putfield android/support/v4/widget/DrawerLayout/mInitialMotionX F
aload 0
fload 3
putfield android/support/v4/widget/DrawerLayout/mInitialMotionY F
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/mDisallowInterceptRequested Z
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/mChildrenCanceledTouch Z
iconst_1
ireturn
L1:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 3
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 2
iconst_1
istore 6
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
fload 3
f2i
fload 2
f2i
invokevirtual android/support/v4/widget/ViewDragHelper/findTopChildUnder(II)Landroid/view/View;
astore 1
iload 6
istore 5
aload 1
ifnull L4
iload 6
istore 5
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/isContentView(Landroid/view/View;)Z
ifeq L4
fload 3
aload 0
getfield android/support/v4/widget/DrawerLayout/mInitialMotionX F
fsub
fstore 3
fload 2
aload 0
getfield android/support/v4/widget/DrawerLayout/mInitialMotionY F
fsub
fstore 2
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/getTouchSlop()I
istore 4
iload 6
istore 5
fload 3
fload 3
fmul
fload 2
fload 2
fmul
fadd
iload 4
iload 4
imul
i2f
fcmpg
ifge L4
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/findOpenDrawer()Landroid/view/View;
astore 1
iload 6
istore 5
aload 1
ifnull L4
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/getDrawerLockMode(Landroid/view/View;)I
iconst_2
if_icmpne L5
iconst_1
istore 5
L4:
aload 0
iload 5
invokevirtual android/support/v4/widget/DrawerLayout/closeDrawers(Z)V
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/mDisallowInterceptRequested Z
iconst_1
ireturn
L5:
iconst_0
istore 5
goto L4
L3:
aload 0
iconst_1
invokevirtual android/support/v4/widget/DrawerLayout/closeDrawers(Z)V
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/mDisallowInterceptRequested Z
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/mChildrenCanceledTouch Z
iconst_1
ireturn
.limit locals 7
.limit stack 3
.end method

.method public openDrawer(I)V
iload 1
aload 0
invokestatic android/support/v4/view/ViewCompat/getLayoutDirection(Landroid/view/View;)I
invokestatic android/support/v4/view/GravityCompat/getAbsoluteGravity(II)I
istore 1
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/findDrawerWithGravity(I)Landroid/view/View;
astore 2
aload 2
ifnonnull L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "No drawer view found with absolute gravity "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokestatic android/support/v4/widget/DrawerLayout/gravityToString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 2
invokevirtual android/support/v4/widget/DrawerLayout/openDrawer(Landroid/view/View;)V
return
.limit locals 3
.limit stack 4
.end method

.method public openDrawer(Landroid/view/View;)V
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerView(Landroid/view/View;)Z
ifne L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "View "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " is not a sliding drawer"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/widget/DrawerLayout/mFirstLayout Z
ifeq L1
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
astore 1
aload 1
fconst_1
putfield android/support/v4/widget/DrawerLayout$LayoutParams/onScreen F
aload 1
iconst_1
putfield android/support/v4/widget/DrawerLayout$LayoutParams/knownOpen Z
L2:
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
return
L1:
aload 0
aload 1
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/checkDrawerViewGravity(Landroid/view/View;I)Z
ifeq L3
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
aload 1
iconst_0
aload 1
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/ViewDragHelper/smoothSlideViewTo(Landroid/view/View;II)Z
pop
goto L2
L3:
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightDragger Landroid/support/v4/widget/ViewDragHelper;
aload 1
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
aload 1
invokevirtual android/view/View/getWidth()I
isub
aload 1
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/ViewDragHelper/smoothSlideViewTo(Landroid/view/View;II)Z
pop
goto L2
.limit locals 2
.limit stack 4
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
aload 0
iload 1
invokespecial android/view/ViewGroup/requestDisallowInterceptTouchEvent(Z)V
aload 0
iload 1
putfield android/support/v4/widget/DrawerLayout/mDisallowInterceptRequested Z
iload 1
ifeq L0
aload 0
iconst_1
invokevirtual android/support/v4/widget/DrawerLayout/closeDrawers(Z)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public requestLayout()V
aload 0
getfield android/support/v4/widget/DrawerLayout/mInLayout Z
ifne L0
aload 0
invokespecial android/view/ViewGroup/requestLayout()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method public setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V
aload 0
aload 1
putfield android/support/v4/widget/DrawerLayout/mListener Landroid/support/v4/widget/DrawerLayout$DrawerListener;
return
.limit locals 2
.limit stack 2
.end method

.method public setDrawerLockMode(I)V
aload 0
iload 1
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/setDrawerLockMode(II)V
aload 0
iload 1
iconst_5
invokevirtual android/support/v4/widget/DrawerLayout/setDrawerLockMode(II)V
return
.limit locals 2
.limit stack 3
.end method

.method public setDrawerLockMode(II)V
iload 2
aload 0
invokestatic android/support/v4/view/ViewCompat/getLayoutDirection(Landroid/view/View;)I
invokestatic android/support/v4/view/GravityCompat/getAbsoluteGravity(II)I
istore 2
iload 2
iconst_3
if_icmpne L0
aload 0
iload 1
putfield android/support/v4/widget/DrawerLayout/mLockModeLeft I
L1:
iload 1
ifeq L2
iload 2
iconst_3
if_icmpne L3
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
astore 3
L4:
aload 3
invokevirtual android/support/v4/widget/ViewDragHelper/cancel()V
L2:
iload 1
tableswitch 1
L5
L6
default : L7
L7:
return
L0:
iload 2
iconst_5
if_icmpne L1
aload 0
iload 1
putfield android/support/v4/widget/DrawerLayout/mLockModeRight I
goto L1
L3:
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightDragger Landroid/support/v4/widget/ViewDragHelper;
astore 3
goto L4
L6:
aload 0
iload 2
invokevirtual android/support/v4/widget/DrawerLayout/findDrawerWithGravity(I)Landroid/view/View;
astore 3
aload 3
ifnull L7
aload 0
aload 3
invokevirtual android/support/v4/widget/DrawerLayout/openDrawer(Landroid/view/View;)V
return
L5:
aload 0
iload 2
invokevirtual android/support/v4/widget/DrawerLayout/findDrawerWithGravity(I)Landroid/view/View;
astore 3
aload 3
ifnull L7
aload 0
aload 3
invokevirtual android/support/v4/widget/DrawerLayout/closeDrawer(Landroid/view/View;)V
return
.limit locals 4
.limit stack 2
.end method

.method public setDrawerLockMode(ILandroid/view/View;)V
aload 0
aload 2
invokevirtual android/support/v4/widget/DrawerLayout/isDrawerView(Landroid/view/View;)Z
ifne L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "View "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " is not a "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "drawer with appropriate layout_gravity"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
aload 0
aload 2
invokevirtual android/support/v4/widget/DrawerLayout/getDrawerViewGravity(Landroid/view/View;)I
invokevirtual android/support/v4/widget/DrawerLayout/setDrawerLockMode(II)V
return
.limit locals 3
.limit stack 4
.end method

.method public setDrawerShadow(II)V
aload 0
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
iload 2
invokevirtual android/support/v4/widget/DrawerLayout/setDrawerShadow(Landroid/graphics/drawable/Drawable;I)V
return
.limit locals 3
.limit stack 3
.end method

.method public setDrawerShadow(Landroid/graphics/drawable/Drawable;I)V
iload 2
aload 0
invokestatic android/support/v4/view/ViewCompat/getLayoutDirection(Landroid/view/View;)I
invokestatic android/support/v4/view/GravityCompat/getAbsoluteGravity(II)I
istore 2
iload 2
iconst_3
iand
iconst_3
if_icmpne L0
aload 0
aload 1
putfield android/support/v4/widget/DrawerLayout/mShadowLeft Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
L0:
iload 2
iconst_5
iand
iconst_5
if_icmpne L1
aload 0
aload 1
putfield android/support/v4/widget/DrawerLayout/mShadowRight Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
L1:
return
.limit locals 3
.limit stack 2
.end method

.method setDrawerViewOffset(Landroid/view/View;F)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
astore 3
fload 2
aload 3
getfield android/support/v4/widget/DrawerLayout$LayoutParams/onScreen F
fcmpl
ifne L0
return
L0:
aload 3
fload 2
putfield android/support/v4/widget/DrawerLayout$LayoutParams/onScreen F
aload 0
aload 1
fload 2
invokevirtual android/support/v4/widget/DrawerLayout/dispatchOnDrawerSlide(Landroid/view/View;F)V
return
.limit locals 4
.limit stack 3
.end method

.method public setScrimColor(I)V
aload 0
iload 1
putfield android/support/v4/widget/DrawerLayout/mScrimColor I
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
return
.limit locals 2
.limit stack 2
.end method

.method updateDrawerState(IILandroid/view/View;)V
aload 0
getfield android/support/v4/widget/DrawerLayout/mLeftDragger Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/getViewDragState()I
istore 1
aload 0
getfield android/support/v4/widget/DrawerLayout/mRightDragger Landroid/support/v4/widget/ViewDragHelper;
invokevirtual android/support/v4/widget/ViewDragHelper/getViewDragState()I
istore 4
iload 1
iconst_1
if_icmpeq L0
iload 4
iconst_1
if_icmpne L1
L0:
iconst_1
istore 1
L2:
aload 3
ifnull L3
iload 2
ifne L3
aload 3
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/DrawerLayout$LayoutParams
astore 5
aload 5
getfield android/support/v4/widget/DrawerLayout$LayoutParams/onScreen F
fconst_0
fcmpl
ifne L4
aload 0
aload 3
invokevirtual android/support/v4/widget/DrawerLayout/dispatchOnDrawerClosed(Landroid/view/View;)V
L3:
iload 1
aload 0
getfield android/support/v4/widget/DrawerLayout/mDrawerState I
if_icmpeq L5
aload 0
iload 1
putfield android/support/v4/widget/DrawerLayout/mDrawerState I
aload 0
getfield android/support/v4/widget/DrawerLayout/mListener Landroid/support/v4/widget/DrawerLayout$DrawerListener;
ifnull L5
aload 0
getfield android/support/v4/widget/DrawerLayout/mListener Landroid/support/v4/widget/DrawerLayout$DrawerListener;
iload 1
invokeinterface android/support/v4/widget/DrawerLayout$DrawerListener/onDrawerStateChanged(I)V 1
L5:
return
L1:
iload 1
iconst_2
if_icmpeq L6
iload 4
iconst_2
if_icmpne L7
L6:
iconst_2
istore 1
goto L2
L7:
iconst_0
istore 1
goto L2
L4:
aload 5
getfield android/support/v4/widget/DrawerLayout$LayoutParams/onScreen F
fconst_1
fcmpl
ifne L3
aload 0
aload 3
invokevirtual android/support/v4/widget/DrawerLayout/dispatchOnDrawerOpened(Landroid/view/View;)V
goto L3
.limit locals 6
.limit stack 2
.end method
