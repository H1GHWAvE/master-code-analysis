.bytecode 50.0
.class public synchronized android/support/v4/widget/ScrollerCompat
.super java/lang/Object
.inner class static abstract interface ScrollerCompatImpl inner android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl outer android/support/v4/widget/ScrollerCompat
.inner class static ScrollerCompatImplBase inner android/support/v4/widget/ScrollerCompat$ScrollerCompatImplBase outer android/support/v4/widget/ScrollerCompat
.inner class static ScrollerCompatImplGingerbread inner android/support/v4/widget/ScrollerCompat$ScrollerCompatImplGingerbread outer android/support/v4/widget/ScrollerCompat
.inner class static ScrollerCompatImplIcs inner android/support/v4/widget/ScrollerCompat$ScrollerCompatImplIcs outer android/support/v4/widget/ScrollerCompat

.field static final 'IMPL' Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;

.field 'mScroller' Ljava/lang/Object;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
istore 0
iload 0
bipush 14
if_icmplt L0
new android/support/v4/widget/ScrollerCompat$ScrollerCompatImplIcs
dup
invokespecial android/support/v4/widget/ScrollerCompat$ScrollerCompatImplIcs/<init>()V
putstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
return
L0:
iload 0
bipush 9
if_icmplt L1
new android/support/v4/widget/ScrollerCompat$ScrollerCompatImplGingerbread
dup
invokespecial android/support/v4/widget/ScrollerCompat$ScrollerCompatImplGingerbread/<init>()V
putstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
return
L1:
new android/support/v4/widget/ScrollerCompat$ScrollerCompatImplBase
dup
invokespecial android/support/v4/widget/ScrollerCompat$ScrollerCompatImplBase/<init>()V
putstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
return
.limit locals 1
.limit stack 2
.end method

.method <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 1
aload 2
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/createScroller(Landroid/content/Context;Landroid/view/animation/Interpolator;)Ljava/lang/Object; 2
putfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
return
.limit locals 3
.limit stack 4
.end method

.method public static create(Landroid/content/Context;)Landroid/support/v4/widget/ScrollerCompat;
aload 0
aconst_null
invokestatic android/support/v4/widget/ScrollerCompat/create(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ScrollerCompat;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static create(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ScrollerCompat;
new android/support/v4/widget/ScrollerCompat
dup
aload 0
aload 1
invokespecial android/support/v4/widget/ScrollerCompat/<init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public abortAnimation()V
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/abortAnimation(Ljava/lang/Object;)V 1
return
.limit locals 1
.limit stack 2
.end method

.method public computeScrollOffset()Z
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/computeScrollOffset(Ljava/lang/Object;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public fling(IIIIIIII)V
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
iload 1
iload 2
iload 3
iload 4
iload 5
iload 6
iload 7
iload 8
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/fling(Ljava/lang/Object;IIIIIIII)V 9
return
.limit locals 9
.limit stack 10
.end method

.method public fling(IIIIIIIIII)V
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
iload 1
iload 2
iload 3
iload 4
iload 5
iload 6
iload 7
iload 8
iload 9
iload 10
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/fling(Ljava/lang/Object;IIIIIIIIII)V 11
return
.limit locals 11
.limit stack 12
.end method

.method public getCurrVelocity()F
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/getCurrVelocity(Ljava/lang/Object;)F 1
freturn
.limit locals 1
.limit stack 2
.end method

.method public getCurrX()I
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/getCurrX(Ljava/lang/Object;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public getCurrY()I
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/getCurrY(Ljava/lang/Object;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public getFinalX()I
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/getFinalX(Ljava/lang/Object;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public getFinalY()I
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/getFinalY(Ljava/lang/Object;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isFinished()Z
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/isFinished(Ljava/lang/Object;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isOverScrolled()Z
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/isOverScrolled(Ljava/lang/Object;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public notifyHorizontalEdgeReached(III)V
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
iload 1
iload 2
iload 3
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/notifyHorizontalEdgeReached(Ljava/lang/Object;III)V 4
return
.limit locals 4
.limit stack 5
.end method

.method public notifyVerticalEdgeReached(III)V
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
iload 1
iload 2
iload 3
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/notifyVerticalEdgeReached(Ljava/lang/Object;III)V 4
return
.limit locals 4
.limit stack 5
.end method

.method public startScroll(IIII)V
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
iload 1
iload 2
iload 3
iload 4
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/startScroll(Ljava/lang/Object;IIII)V 5
return
.limit locals 5
.limit stack 6
.end method

.method public startScroll(IIIII)V
getstatic android/support/v4/widget/ScrollerCompat/IMPL Landroid/support/v4/widget/ScrollerCompat$ScrollerCompatImpl;
aload 0
getfield android/support/v4/widget/ScrollerCompat/mScroller Ljava/lang/Object;
iload 1
iload 2
iload 3
iload 4
iload 5
invokeinterface android/support/v4/widget/ScrollerCompat$ScrollerCompatImpl/startScroll(Ljava/lang/Object;IIIII)V 6
return
.limit locals 6
.limit stack 7
.end method
