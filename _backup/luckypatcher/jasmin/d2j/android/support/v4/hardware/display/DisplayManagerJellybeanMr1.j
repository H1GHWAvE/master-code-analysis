.bytecode 50.0
.class final synchronized android/support/v4/hardware/display/DisplayManagerJellybeanMr1
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getDisplay(Ljava/lang/Object;I)Landroid/view/Display;
aload 0
checkcast android/hardware/display/DisplayManager
iload 1
invokevirtual android/hardware/display/DisplayManager/getDisplay(I)Landroid/view/Display;
areturn
.limit locals 2
.limit stack 2
.end method

.method public static getDisplayManager(Landroid/content/Context;)Ljava/lang/Object;
aload 0
ldc "display"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static getDisplays(Ljava/lang/Object;)[Landroid/view/Display;
aload 0
checkcast android/hardware/display/DisplayManager
invokevirtual android/hardware/display/DisplayManager/getDisplays()[Landroid/view/Display;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static getDisplays(Ljava/lang/Object;Ljava/lang/String;)[Landroid/view/Display;
aload 0
checkcast android/hardware/display/DisplayManager
aload 1
invokevirtual android/hardware/display/DisplayManager/getDisplays(Ljava/lang/String;)[Landroid/view/Display;
areturn
.limit locals 2
.limit stack 2
.end method
