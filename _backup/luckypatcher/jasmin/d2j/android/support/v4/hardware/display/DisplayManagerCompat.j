.bytecode 50.0
.class public synchronized abstract android/support/v4/hardware/display/DisplayManagerCompat
.super java/lang/Object
.inner class private static JellybeanMr1Impl inner android/support/v4/hardware/display/DisplayManagerCompat$JellybeanMr1Impl outer android/support/v4/hardware/display/DisplayManagerCompat
.inner class private static LegacyImpl inner android/support/v4/hardware/display/DisplayManagerCompat$LegacyImpl outer android/support/v4/hardware/display/DisplayManagerCompat

.field public static final 'DISPLAY_CATEGORY_PRESENTATION' Ljava/lang/String; = "android.hardware.display.category.PRESENTATION"

.field private static final 'sInstances' Ljava/util/WeakHashMap; signature "Ljava/util/WeakHashMap<Landroid/content/Context;Landroid/support/v4/hardware/display/DisplayManagerCompat;>;"

.method static <clinit>()V
new java/util/WeakHashMap
dup
invokespecial java/util/WeakHashMap/<init>()V
putstatic android/support/v4/hardware/display/DisplayManagerCompat/sInstances Ljava/util/WeakHashMap;
return
.limit locals 0
.limit stack 2
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/support/v4/hardware/display/DisplayManagerCompat;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
getstatic android/support/v4/hardware/display/DisplayManagerCompat/sInstances Ljava/util/WeakHashMap;
astore 3
aload 3
monitorenter
L0:
getstatic android/support/v4/hardware/display/DisplayManagerCompat/sInstances Ljava/util/WeakHashMap;
aload 0
invokevirtual java/util/WeakHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/support/v4/hardware/display/DisplayManagerCompat
astore 2
L1:
aload 2
astore 1
aload 2
ifnonnull L5
L3:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 17
if_icmplt L7
new android/support/v4/hardware/display/DisplayManagerCompat$JellybeanMr1Impl
dup
aload 0
invokespecial android/support/v4/hardware/display/DisplayManagerCompat$JellybeanMr1Impl/<init>(Landroid/content/Context;)V
astore 1
L4:
getstatic android/support/v4/hardware/display/DisplayManagerCompat/sInstances Ljava/util/WeakHashMap;
aload 0
aload 1
invokevirtual java/util/WeakHashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L5:
aload 3
monitorexit
L6:
aload 1
areturn
L7:
new android/support/v4/hardware/display/DisplayManagerCompat$LegacyImpl
dup
aload 0
invokespecial android/support/v4/hardware/display/DisplayManagerCompat$LegacyImpl/<init>(Landroid/content/Context;)V
astore 1
L8:
goto L4
L2:
astore 0
L9:
aload 3
monitorexit
L10:
aload 0
athrow
.limit locals 4
.limit stack 3
.end method

.method public abstract getDisplay(I)Landroid/view/Display;
.end method

.method public abstract getDisplays()[Landroid/view/Display;
.end method

.method public abstract getDisplays(Ljava/lang/String;)[Landroid/view/Display;
.end method
