.bytecode 50.0
.class synchronized android/support/v4/hardware/display/DisplayManagerCompat$JellybeanMr1Impl
.super android/support/v4/hardware/display/DisplayManagerCompat
.inner class private static JellybeanMr1Impl inner android/support/v4/hardware/display/DisplayManagerCompat$JellybeanMr1Impl outer android/support/v4/hardware/display/DisplayManagerCompat

.field private final 'mDisplayManagerObj' Ljava/lang/Object;

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial android/support/v4/hardware/display/DisplayManagerCompat/<init>()V
aload 0
aload 1
invokestatic android/support/v4/hardware/display/DisplayManagerJellybeanMr1/getDisplayManager(Landroid/content/Context;)Ljava/lang/Object;
putfield android/support/v4/hardware/display/DisplayManagerCompat$JellybeanMr1Impl/mDisplayManagerObj Ljava/lang/Object;
return
.limit locals 2
.limit stack 2
.end method

.method public getDisplay(I)Landroid/view/Display;
aload 0
getfield android/support/v4/hardware/display/DisplayManagerCompat$JellybeanMr1Impl/mDisplayManagerObj Ljava/lang/Object;
iload 1
invokestatic android/support/v4/hardware/display/DisplayManagerJellybeanMr1/getDisplay(Ljava/lang/Object;I)Landroid/view/Display;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getDisplays()[Landroid/view/Display;
aload 0
getfield android/support/v4/hardware/display/DisplayManagerCompat$JellybeanMr1Impl/mDisplayManagerObj Ljava/lang/Object;
invokestatic android/support/v4/hardware/display/DisplayManagerJellybeanMr1/getDisplays(Ljava/lang/Object;)[Landroid/view/Display;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getDisplays(Ljava/lang/String;)[Landroid/view/Display;
aload 0
getfield android/support/v4/hardware/display/DisplayManagerCompat$JellybeanMr1Impl/mDisplayManagerObj Ljava/lang/Object;
aload 1
invokestatic android/support/v4/hardware/display/DisplayManagerJellybeanMr1/getDisplays(Ljava/lang/Object;Ljava/lang/String;)[Landroid/view/Display;
areturn
.limit locals 2
.limit stack 2
.end method
