.bytecode 50.0
.class synchronized android/support/v4/hardware/display/DisplayManagerCompat$LegacyImpl
.super android/support/v4/hardware/display/DisplayManagerCompat
.inner class private static LegacyImpl inner android/support/v4/hardware/display/DisplayManagerCompat$LegacyImpl outer android/support/v4/hardware/display/DisplayManagerCompat

.field private final 'mWindowManager' Landroid/view/WindowManager;

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial android/support/v4/hardware/display/DisplayManagerCompat/<init>()V
aload 0
aload 1
ldc "window"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/WindowManager
putfield android/support/v4/hardware/display/DisplayManagerCompat$LegacyImpl/mWindowManager Landroid/view/WindowManager;
return
.limit locals 2
.limit stack 3
.end method

.method public getDisplay(I)Landroid/view/Display;
aload 0
getfield android/support/v4/hardware/display/DisplayManagerCompat$LegacyImpl/mWindowManager Landroid/view/WindowManager;
invokeinterface android/view/WindowManager/getDefaultDisplay()Landroid/view/Display; 0
astore 2
aload 2
invokevirtual android/view/Display/getDisplayId()I
iload 1
if_icmpne L0
aload 2
areturn
L0:
aconst_null
areturn
.limit locals 3
.limit stack 2
.end method

.method public getDisplays()[Landroid/view/Display;
iconst_1
anewarray android/view/Display
dup
iconst_0
aload 0
getfield android/support/v4/hardware/display/DisplayManagerCompat$LegacyImpl/mWindowManager Landroid/view/WindowManager;
invokeinterface android/view/WindowManager/getDefaultDisplay()Landroid/view/Display; 0
aastore
areturn
.limit locals 1
.limit stack 4
.end method

.method public getDisplays(Ljava/lang/String;)[Landroid/view/Display;
aload 1
ifnonnull L0
aload 0
invokevirtual android/support/v4/hardware/display/DisplayManagerCompat$LegacyImpl/getDisplays()[Landroid/view/Display;
areturn
L0:
iconst_0
anewarray android/view/Display
areturn
.limit locals 2
.limit stack 1
.end method
