.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/FileHeader
.super java/lang/Object

.field private 'aesExtraDataRecord' Lnet/lingala/zip4j/model/AESExtraDataRecord;

.field private 'compressedSize' J

.field private 'compressionMethod' I

.field private 'crc32' J

.field private 'crcBuff' [B

.field private 'dataDescriptorExists' Z

.field private 'diskNumberStart' I

.field private 'encryptionMethod' I

.field private 'externalFileAttr' [B

.field private 'extraDataRecords' Ljava/util/ArrayList;

.field private 'extraFieldLength' I

.field private 'fileComment' Ljava/lang/String;

.field private 'fileCommentLength' I

.field private 'fileName' Ljava/lang/String;

.field private 'fileNameLength' I

.field private 'fileNameUTF8Encoded' Z

.field private 'generalPurposeFlag' [B

.field private 'internalFileAttr' [B

.field private 'isDirectory' Z

.field private 'isEncrypted' Z

.field private 'lastModFileTime' I

.field private 'offsetLocalHeader' J

.field private 'password' [C

.field private 'signature' I

.field private 'uncompressedSize' J

.field private 'versionMadeBy' I

.field private 'versionNeededToExtract' I

.field private 'zip64ExtendedInfo' Lnet/lingala/zip4j/model/Zip64ExtendedInfo;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_m1
putfield net/lingala/zip4j/model/FileHeader/encryptionMethod I
aload 0
lconst_0
putfield net/lingala/zip4j/model/FileHeader/crc32 J
aload 0
lconst_0
putfield net/lingala/zip4j/model/FileHeader/uncompressedSize J
return
.limit locals 1
.limit stack 3
.end method

.method public extractFile(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input zipModel is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkOutputFolder(Ljava/lang/String;)Z
ifne L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "Invalid output path"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
ifnonnull L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid file header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
new net/lingala/zip4j/unzip/Unzip
dup
aload 1
invokespecial net/lingala/zip4j/unzip/Unzip/<init>(Lnet/lingala/zip4j/model/ZipModel;)V
aload 0
aload 2
aload 3
aload 4
aload 5
iload 6
invokevirtual net/lingala/zip4j/unzip/Unzip/extractFile(Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
return
.limit locals 7
.limit stack 7
.end method

.method public extractFile(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
aload 2
aload 3
aconst_null
aload 4
iload 5
invokevirtual net/lingala/zip4j/model/FileHeader/extractFile(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
return
.limit locals 6
.limit stack 7
.end method

.method public extractFile(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
aload 2
aconst_null
aload 3
iload 4
invokevirtual net/lingala/zip4j/model/FileHeader/extractFile(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
return
.limit locals 5
.limit stack 6
.end method

.method public getAesExtraDataRecord()Lnet/lingala/zip4j/model/AESExtraDataRecord;
aload 0
getfield net/lingala/zip4j/model/FileHeader/aesExtraDataRecord Lnet/lingala/zip4j/model/AESExtraDataRecord;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getCompressedSize()J
aload 0
getfield net/lingala/zip4j/model/FileHeader/compressedSize J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getCompressionMethod()I
aload 0
getfield net/lingala/zip4j/model/FileHeader/compressionMethod I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getCrc32()J
aload 0
getfield net/lingala/zip4j/model/FileHeader/crc32 J
ldc2_w 4294967295L
land
lreturn
.limit locals 1
.limit stack 4
.end method

.method public getCrcBuff()[B
aload 0
getfield net/lingala/zip4j/model/FileHeader/crcBuff [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getDiskNumberStart()I
aload 0
getfield net/lingala/zip4j/model/FileHeader/diskNumberStart I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getEncryptionMethod()I
aload 0
getfield net/lingala/zip4j/model/FileHeader/encryptionMethod I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getExternalFileAttr()[B
aload 0
getfield net/lingala/zip4j/model/FileHeader/externalFileAttr [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getExtraDataRecords()Ljava/util/ArrayList;
aload 0
getfield net/lingala/zip4j/model/FileHeader/extraDataRecords Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getExtraFieldLength()I
aload 0
getfield net/lingala/zip4j/model/FileHeader/extraFieldLength I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getFileComment()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/model/FileHeader/fileComment Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getFileCommentLength()I
aload 0
getfield net/lingala/zip4j/model/FileHeader/fileCommentLength I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getFileName()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/model/FileHeader/fileName Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getFileNameLength()I
aload 0
getfield net/lingala/zip4j/model/FileHeader/fileNameLength I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getGeneralPurposeFlag()[B
aload 0
getfield net/lingala/zip4j/model/FileHeader/generalPurposeFlag [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getInternalFileAttr()[B
aload 0
getfield net/lingala/zip4j/model/FileHeader/internalFileAttr [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getLastModFileTime()I
aload 0
getfield net/lingala/zip4j/model/FileHeader/lastModFileTime I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOffsetLocalHeader()J
aload 0
getfield net/lingala/zip4j/model/FileHeader/offsetLocalHeader J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getPassword()[C
aload 0
getfield net/lingala/zip4j/model/FileHeader/password [C
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSignature()I
aload 0
getfield net/lingala/zip4j/model/FileHeader/signature I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getUncompressedSize()J
aload 0
getfield net/lingala/zip4j/model/FileHeader/uncompressedSize J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getVersionMadeBy()I
aload 0
getfield net/lingala/zip4j/model/FileHeader/versionMadeBy I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getVersionNeededToExtract()I
aload 0
getfield net/lingala/zip4j/model/FileHeader/versionNeededToExtract I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
aload 0
getfield net/lingala/zip4j/model/FileHeader/zip64ExtendedInfo Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
areturn
.limit locals 1
.limit stack 1
.end method

.method public isDataDescriptorExists()Z
aload 0
getfield net/lingala/zip4j/model/FileHeader/dataDescriptorExists Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isDirectory()Z
aload 0
getfield net/lingala/zip4j/model/FileHeader/isDirectory Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isEncrypted()Z
aload 0
getfield net/lingala/zip4j/model/FileHeader/isEncrypted Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isFileNameUTF8Encoded()Z
aload 0
getfield net/lingala/zip4j/model/FileHeader/fileNameUTF8Encoded Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setAesExtraDataRecord(Lnet/lingala/zip4j/model/AESExtraDataRecord;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/FileHeader/aesExtraDataRecord Lnet/lingala/zip4j/model/AESExtraDataRecord;
return
.limit locals 2
.limit stack 2
.end method

.method public setCompressedSize(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/FileHeader/compressedSize J
return
.limit locals 3
.limit stack 3
.end method

.method public setCompressionMethod(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/FileHeader/compressionMethod I
return
.limit locals 2
.limit stack 2
.end method

.method public setCrc32(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/FileHeader/crc32 J
return
.limit locals 3
.limit stack 3
.end method

.method public setCrcBuff([B)V
aload 0
aload 1
putfield net/lingala/zip4j/model/FileHeader/crcBuff [B
return
.limit locals 2
.limit stack 2
.end method

.method public setDataDescriptorExists(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/FileHeader/dataDescriptorExists Z
return
.limit locals 2
.limit stack 2
.end method

.method public setDirectory(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/FileHeader/isDirectory Z
return
.limit locals 2
.limit stack 2
.end method

.method public setDiskNumberStart(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/FileHeader/diskNumberStart I
return
.limit locals 2
.limit stack 2
.end method

.method public setEncrypted(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/FileHeader/isEncrypted Z
return
.limit locals 2
.limit stack 2
.end method

.method public setEncryptionMethod(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/FileHeader/encryptionMethod I
return
.limit locals 2
.limit stack 2
.end method

.method public setExternalFileAttr([B)V
aload 0
aload 1
putfield net/lingala/zip4j/model/FileHeader/externalFileAttr [B
return
.limit locals 2
.limit stack 2
.end method

.method public setExtraDataRecords(Ljava/util/ArrayList;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/FileHeader/extraDataRecords Ljava/util/ArrayList;
return
.limit locals 2
.limit stack 2
.end method

.method public setExtraFieldLength(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/FileHeader/extraFieldLength I
return
.limit locals 2
.limit stack 2
.end method

.method public setFileComment(Ljava/lang/String;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/FileHeader/fileComment Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setFileCommentLength(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/FileHeader/fileCommentLength I
return
.limit locals 2
.limit stack 2
.end method

.method public setFileName(Ljava/lang/String;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/FileHeader/fileName Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setFileNameLength(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/FileHeader/fileNameLength I
return
.limit locals 2
.limit stack 2
.end method

.method public setFileNameUTF8Encoded(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/FileHeader/fileNameUTF8Encoded Z
return
.limit locals 2
.limit stack 2
.end method

.method public setGeneralPurposeFlag([B)V
aload 0
aload 1
putfield net/lingala/zip4j/model/FileHeader/generalPurposeFlag [B
return
.limit locals 2
.limit stack 2
.end method

.method public setInternalFileAttr([B)V
aload 0
aload 1
putfield net/lingala/zip4j/model/FileHeader/internalFileAttr [B
return
.limit locals 2
.limit stack 2
.end method

.method public setLastModFileTime(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/FileHeader/lastModFileTime I
return
.limit locals 2
.limit stack 2
.end method

.method public setOffsetLocalHeader(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/FileHeader/offsetLocalHeader J
return
.limit locals 3
.limit stack 3
.end method

.method public setPassword([C)V
aload 0
aload 1
putfield net/lingala/zip4j/model/FileHeader/password [C
return
.limit locals 2
.limit stack 2
.end method

.method public setSignature(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/FileHeader/signature I
return
.limit locals 2
.limit stack 2
.end method

.method public setUncompressedSize(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/FileHeader/uncompressedSize J
return
.limit locals 3
.limit stack 3
.end method

.method public setVersionMadeBy(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/FileHeader/versionMadeBy I
return
.limit locals 2
.limit stack 2
.end method

.method public setVersionNeededToExtract(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/FileHeader/versionNeededToExtract I
return
.limit locals 2
.limit stack 2
.end method

.method public setZip64ExtendedInfo(Lnet/lingala/zip4j/model/Zip64ExtendedInfo;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/FileHeader/zip64ExtendedInfo Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
return
.limit locals 2
.limit stack 2
.end method
