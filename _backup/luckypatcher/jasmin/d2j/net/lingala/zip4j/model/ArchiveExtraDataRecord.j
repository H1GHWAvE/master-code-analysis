.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/ArchiveExtraDataRecord
.super java/lang/Object

.field private 'extraFieldData' Ljava/lang/String;

.field private 'extraFieldLength' I

.field private 'signature' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public getExtraFieldData()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/model/ArchiveExtraDataRecord/extraFieldData Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getExtraFieldLength()I
aload 0
getfield net/lingala/zip4j/model/ArchiveExtraDataRecord/extraFieldLength I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSignature()I
aload 0
getfield net/lingala/zip4j/model/ArchiveExtraDataRecord/signature I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setExtraFieldData(Ljava/lang/String;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ArchiveExtraDataRecord/extraFieldData Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setExtraFieldLength(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ArchiveExtraDataRecord/extraFieldLength I
return
.limit locals 2
.limit stack 2
.end method

.method public setSignature(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ArchiveExtraDataRecord/signature I
return
.limit locals 2
.limit stack 2
.end method
