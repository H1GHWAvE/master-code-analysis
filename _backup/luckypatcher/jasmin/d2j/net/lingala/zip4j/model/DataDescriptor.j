.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/DataDescriptor
.super java/lang/Object

.field private 'compressedSize' I

.field private 'crc32' Ljava/lang/String;

.field private 'uncompressedSize' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public getCompressedSize()I
aload 0
getfield net/lingala/zip4j/model/DataDescriptor/compressedSize I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getCrc32()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/model/DataDescriptor/crc32 Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getUncompressedSize()I
aload 0
getfield net/lingala/zip4j/model/DataDescriptor/uncompressedSize I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setCompressedSize(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/DataDescriptor/compressedSize I
return
.limit locals 2
.limit stack 2
.end method

.method public setCrc32(Ljava/lang/String;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/DataDescriptor/crc32 Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setUncompressedSize(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/DataDescriptor/uncompressedSize I
return
.limit locals 2
.limit stack 2
.end method
