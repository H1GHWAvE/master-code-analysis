.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/AESExtraDataRecord
.super java/lang/Object

.field private 'aesStrength' I

.field private 'compressionMethod' I

.field private 'dataSize' I

.field private 'signature' J

.field private 'vendorID' Ljava/lang/String;

.field private 'versionNumber' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc2_w -1L
putfield net/lingala/zip4j/model/AESExtraDataRecord/signature J
aload 0
iconst_m1
putfield net/lingala/zip4j/model/AESExtraDataRecord/dataSize I
aload 0
iconst_m1
putfield net/lingala/zip4j/model/AESExtraDataRecord/versionNumber I
aload 0
aconst_null
putfield net/lingala/zip4j/model/AESExtraDataRecord/vendorID Ljava/lang/String;
aload 0
iconst_m1
putfield net/lingala/zip4j/model/AESExtraDataRecord/aesStrength I
aload 0
iconst_m1
putfield net/lingala/zip4j/model/AESExtraDataRecord/compressionMethod I
return
.limit locals 1
.limit stack 3
.end method

.method public getAesStrength()I
aload 0
getfield net/lingala/zip4j/model/AESExtraDataRecord/aesStrength I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getCompressionMethod()I
aload 0
getfield net/lingala/zip4j/model/AESExtraDataRecord/compressionMethod I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getDataSize()I
aload 0
getfield net/lingala/zip4j/model/AESExtraDataRecord/dataSize I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSignature()J
aload 0
getfield net/lingala/zip4j/model/AESExtraDataRecord/signature J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getVendorID()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/model/AESExtraDataRecord/vendorID Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getVersionNumber()I
aload 0
getfield net/lingala/zip4j/model/AESExtraDataRecord/versionNumber I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setAesStrength(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/AESExtraDataRecord/aesStrength I
return
.limit locals 2
.limit stack 2
.end method

.method public setCompressionMethod(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/AESExtraDataRecord/compressionMethod I
return
.limit locals 2
.limit stack 2
.end method

.method public setDataSize(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/AESExtraDataRecord/dataSize I
return
.limit locals 2
.limit stack 2
.end method

.method public setSignature(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/AESExtraDataRecord/signature J
return
.limit locals 3
.limit stack 3
.end method

.method public setVendorID(Ljava/lang/String;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/AESExtraDataRecord/vendorID Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setVersionNumber(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/AESExtraDataRecord/versionNumber I
return
.limit locals 2
.limit stack 2
.end method
