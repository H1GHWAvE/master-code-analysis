.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/UnzipEngineParameters
.super java/lang/Object

.field private 'fileHeader' Lnet/lingala/zip4j/model/FileHeader;

.field private 'iDecryptor' Lnet/lingala/zip4j/crypto/IDecrypter;

.field private 'localFileHeader' Lnet/lingala/zip4j/model/LocalFileHeader;

.field private 'outputStream' Ljava/io/FileOutputStream;

.field private 'unzipEngine' Lnet/lingala/zip4j/unzip/UnzipEngine;

.field private 'zipModel' Lnet/lingala/zip4j/model/ZipModel;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public getFileHeader()Lnet/lingala/zip4j/model/FileHeader;
aload 0
getfield net/lingala/zip4j/model/UnzipEngineParameters/fileHeader Lnet/lingala/zip4j/model/FileHeader;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getIDecryptor()Lnet/lingala/zip4j/crypto/IDecrypter;
aload 0
getfield net/lingala/zip4j/model/UnzipEngineParameters/iDecryptor Lnet/lingala/zip4j/crypto/IDecrypter;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getLocalFileHeader()Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/model/UnzipEngineParameters/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getOutputStream()Ljava/io/FileOutputStream;
aload 0
getfield net/lingala/zip4j/model/UnzipEngineParameters/outputStream Ljava/io/FileOutputStream;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getUnzipEngine()Lnet/lingala/zip4j/unzip/UnzipEngine;
aload 0
getfield net/lingala/zip4j/model/UnzipEngineParameters/unzipEngine Lnet/lingala/zip4j/unzip/UnzipEngine;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getZipModel()Lnet/lingala/zip4j/model/ZipModel;
aload 0
getfield net/lingala/zip4j/model/UnzipEngineParameters/zipModel Lnet/lingala/zip4j/model/ZipModel;
areturn
.limit locals 1
.limit stack 1
.end method

.method public setFileHeader(Lnet/lingala/zip4j/model/FileHeader;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/UnzipEngineParameters/fileHeader Lnet/lingala/zip4j/model/FileHeader;
return
.limit locals 2
.limit stack 2
.end method

.method public setIDecryptor(Lnet/lingala/zip4j/crypto/IDecrypter;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/UnzipEngineParameters/iDecryptor Lnet/lingala/zip4j/crypto/IDecrypter;
return
.limit locals 2
.limit stack 2
.end method

.method public setLocalFileHeader(Lnet/lingala/zip4j/model/LocalFileHeader;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/UnzipEngineParameters/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
return
.limit locals 2
.limit stack 2
.end method

.method public setOutputStream(Ljava/io/FileOutputStream;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/UnzipEngineParameters/outputStream Ljava/io/FileOutputStream;
return
.limit locals 2
.limit stack 2
.end method

.method public setUnzipEngine(Lnet/lingala/zip4j/unzip/UnzipEngine;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/UnzipEngineParameters/unzipEngine Lnet/lingala/zip4j/unzip/UnzipEngine;
return
.limit locals 2
.limit stack 2
.end method

.method public setZipModel(Lnet/lingala/zip4j/model/ZipModel;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/UnzipEngineParameters/zipModel Lnet/lingala/zip4j/model/ZipModel;
return
.limit locals 2
.limit stack 2
.end method
