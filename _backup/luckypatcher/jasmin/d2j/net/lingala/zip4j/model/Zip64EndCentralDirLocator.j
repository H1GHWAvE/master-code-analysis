.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/Zip64EndCentralDirLocator
.super java/lang/Object

.field private 'noOfDiskStartOfZip64EndOfCentralDirRec' I

.field private 'offsetZip64EndOfCentralDirRec' J

.field private 'signature' J

.field private 'totNumberOfDiscs' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public getNoOfDiskStartOfZip64EndOfCentralDirRec()I
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirLocator/noOfDiskStartOfZip64EndOfCentralDirRec I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOffsetZip64EndOfCentralDirRec()J
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirLocator/offsetZip64EndOfCentralDirRec J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getSignature()J
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirLocator/signature J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getTotNumberOfDiscs()I
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirLocator/totNumberOfDiscs I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setNoOfDiskStartOfZip64EndOfCentralDirRec(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirLocator/noOfDiskStartOfZip64EndOfCentralDirRec I
return
.limit locals 2
.limit stack 2
.end method

.method public setOffsetZip64EndOfCentralDirRec(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirLocator/offsetZip64EndOfCentralDirRec J
return
.limit locals 3
.limit stack 3
.end method

.method public setSignature(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirLocator/signature J
return
.limit locals 3
.limit stack 3
.end method

.method public setTotNumberOfDiscs(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirLocator/totNumberOfDiscs I
return
.limit locals 2
.limit stack 2
.end method
