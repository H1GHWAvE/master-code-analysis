.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/DigitalSignature
.super java/lang/Object

.field private 'headerSignature' I

.field private 'signatureData' Ljava/lang/String;

.field private 'sizeOfData' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public getHeaderSignature()I
aload 0
getfield net/lingala/zip4j/model/DigitalSignature/headerSignature I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSignatureData()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/model/DigitalSignature/signatureData Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSizeOfData()I
aload 0
getfield net/lingala/zip4j/model/DigitalSignature/sizeOfData I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setHeaderSignature(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/DigitalSignature/headerSignature I
return
.limit locals 2
.limit stack 2
.end method

.method public setSignatureData(Ljava/lang/String;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/DigitalSignature/signatureData Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setSizeOfData(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/DigitalSignature/sizeOfData I
return
.limit locals 2
.limit stack 2
.end method
