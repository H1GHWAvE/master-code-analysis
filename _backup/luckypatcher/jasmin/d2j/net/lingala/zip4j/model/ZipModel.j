.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/ZipModel
.super java/lang/Object
.implements java/lang/Cloneable

.field private 'archiveExtraDataRecord' Lnet/lingala/zip4j/model/ArchiveExtraDataRecord;

.field private 'centralDirectory' Lnet/lingala/zip4j/model/CentralDirectory;

.field private 'dataDescriptorList' Ljava/util/List;

.field private 'end' J

.field private 'endCentralDirRecord' Lnet/lingala/zip4j/model/EndCentralDirRecord;

.field private 'fileNameCharset' Ljava/lang/String;

.field private 'isNestedZipFile' Z

.field private 'isZip64Format' Z

.field private 'localFileHeaderList' Ljava/util/List;

.field private 'splitArchive' Z

.field private 'splitLength' J

.field private 'start' J

.field private 'zip64EndCentralDirLocator' Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;

.field private 'zip64EndCentralDirRecord' Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;

.field private 'zipFile' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc2_w -1L
putfield net/lingala/zip4j/model/ZipModel/splitLength J
return
.limit locals 1
.limit stack 3
.end method

.method public clone()Ljava/lang/Object;
.throws java/lang/CloneNotSupportedException
aload 0
invokespecial java/lang/Object/clone()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getArchiveExtraDataRecord()Lnet/lingala/zip4j/model/ArchiveExtraDataRecord;
aload 0
getfield net/lingala/zip4j/model/ZipModel/archiveExtraDataRecord Lnet/lingala/zip4j/model/ArchiveExtraDataRecord;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
aload 0
getfield net/lingala/zip4j/model/ZipModel/centralDirectory Lnet/lingala/zip4j/model/CentralDirectory;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getDataDescriptorList()Ljava/util/List;
aload 0
getfield net/lingala/zip4j/model/ZipModel/dataDescriptorList Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getEnd()J
aload 0
getfield net/lingala/zip4j/model/ZipModel/end J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
aload 0
getfield net/lingala/zip4j/model/ZipModel/endCentralDirRecord Lnet/lingala/zip4j/model/EndCentralDirRecord;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getFileNameCharset()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/model/ZipModel/fileNameCharset Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getLocalFileHeaderList()Ljava/util/List;
aload 0
getfield net/lingala/zip4j/model/ZipModel/localFileHeaderList Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSplitLength()J
aload 0
getfield net/lingala/zip4j/model/ZipModel/splitLength J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getStart()J
aload 0
getfield net/lingala/zip4j/model/ZipModel/start J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
aload 0
getfield net/lingala/zip4j/model/ZipModel/zip64EndCentralDirLocator Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
aload 0
getfield net/lingala/zip4j/model/ZipModel/zip64EndCentralDirRecord Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getZipFile()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/model/ZipModel/zipFile Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public isNestedZipFile()Z
aload 0
getfield net/lingala/zip4j/model/ZipModel/isNestedZipFile Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isSplitArchive()Z
aload 0
getfield net/lingala/zip4j/model/ZipModel/splitArchive Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isZip64Format()Z
aload 0
getfield net/lingala/zip4j/model/ZipModel/isZip64Format Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setArchiveExtraDataRecord(Lnet/lingala/zip4j/model/ArchiveExtraDataRecord;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ZipModel/archiveExtraDataRecord Lnet/lingala/zip4j/model/ArchiveExtraDataRecord;
return
.limit locals 2
.limit stack 2
.end method

.method public setCentralDirectory(Lnet/lingala/zip4j/model/CentralDirectory;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ZipModel/centralDirectory Lnet/lingala/zip4j/model/CentralDirectory;
return
.limit locals 2
.limit stack 2
.end method

.method public setDataDescriptorList(Ljava/util/List;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ZipModel/dataDescriptorList Ljava/util/List;
return
.limit locals 2
.limit stack 2
.end method

.method public setEnd(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/ZipModel/end J
return
.limit locals 3
.limit stack 3
.end method

.method public setEndCentralDirRecord(Lnet/lingala/zip4j/model/EndCentralDirRecord;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ZipModel/endCentralDirRecord Lnet/lingala/zip4j/model/EndCentralDirRecord;
return
.limit locals 2
.limit stack 2
.end method

.method public setFileNameCharset(Ljava/lang/String;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ZipModel/fileNameCharset Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setLocalFileHeaderList(Ljava/util/List;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ZipModel/localFileHeaderList Ljava/util/List;
return
.limit locals 2
.limit stack 2
.end method

.method public setNestedZipFile(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ZipModel/isNestedZipFile Z
return
.limit locals 2
.limit stack 2
.end method

.method public setSplitArchive(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ZipModel/splitArchive Z
return
.limit locals 2
.limit stack 2
.end method

.method public setSplitLength(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/ZipModel/splitLength J
return
.limit locals 3
.limit stack 3
.end method

.method public setStart(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/ZipModel/start J
return
.limit locals 3
.limit stack 3
.end method

.method public setZip64EndCentralDirLocator(Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ZipModel/zip64EndCentralDirLocator Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
return
.limit locals 2
.limit stack 2
.end method

.method public setZip64EndCentralDirRecord(Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ZipModel/zip64EndCentralDirRecord Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
return
.limit locals 2
.limit stack 2
.end method

.method public setZip64Format(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ZipModel/isZip64Format Z
return
.limit locals 2
.limit stack 2
.end method

.method public setZipFile(Ljava/lang/String;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ZipModel/zipFile Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method
