.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/UnzipParameters
.super java/lang/Object

.field private 'ignoreAllFileAttributes' Z

.field private 'ignoreArchiveFileAttribute' Z

.field private 'ignoreDateTimeAttributes' Z

.field private 'ignoreHiddenFileAttribute' Z

.field private 'ignoreReadOnlyFileAttribute' Z

.field private 'ignoreSystemFileAttribute' Z

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public isIgnoreAllFileAttributes()Z
aload 0
getfield net/lingala/zip4j/model/UnzipParameters/ignoreAllFileAttributes Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isIgnoreArchiveFileAttribute()Z
aload 0
getfield net/lingala/zip4j/model/UnzipParameters/ignoreArchiveFileAttribute Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isIgnoreDateTimeAttributes()Z
aload 0
getfield net/lingala/zip4j/model/UnzipParameters/ignoreDateTimeAttributes Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isIgnoreHiddenFileAttribute()Z
aload 0
getfield net/lingala/zip4j/model/UnzipParameters/ignoreHiddenFileAttribute Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isIgnoreReadOnlyFileAttribute()Z
aload 0
getfield net/lingala/zip4j/model/UnzipParameters/ignoreReadOnlyFileAttribute Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isIgnoreSystemFileAttribute()Z
aload 0
getfield net/lingala/zip4j/model/UnzipParameters/ignoreSystemFileAttribute Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setIgnoreAllFileAttributes(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/UnzipParameters/ignoreAllFileAttributes Z
return
.limit locals 2
.limit stack 2
.end method

.method public setIgnoreArchiveFileAttribute(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/UnzipParameters/ignoreArchiveFileAttribute Z
return
.limit locals 2
.limit stack 2
.end method

.method public setIgnoreDateTimeAttributes(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/UnzipParameters/ignoreDateTimeAttributes Z
return
.limit locals 2
.limit stack 2
.end method

.method public setIgnoreHiddenFileAttribute(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/UnzipParameters/ignoreHiddenFileAttribute Z
return
.limit locals 2
.limit stack 2
.end method

.method public setIgnoreReadOnlyFileAttribute(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/UnzipParameters/ignoreReadOnlyFileAttribute Z
return
.limit locals 2
.limit stack 2
.end method

.method public setIgnoreSystemFileAttribute(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/UnzipParameters/ignoreSystemFileAttribute Z
return
.limit locals 2
.limit stack 2
.end method
