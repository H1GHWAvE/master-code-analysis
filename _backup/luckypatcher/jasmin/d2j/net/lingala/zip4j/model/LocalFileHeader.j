.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/LocalFileHeader
.super java/lang/Object

.field private 'aesExtraDataRecord' Lnet/lingala/zip4j/model/AESExtraDataRecord;

.field private 'compressedSize' J

.field private 'compressionMethod' I

.field private 'crc32' J

.field private 'crcBuff' [B

.field private 'dataDescriptorExists' Z

.field private 'encryptionMethod' I

.field private 'extraDataRecords' Ljava/util/ArrayList;

.field private 'extraField' [B

.field private 'extraFieldLength' I

.field private 'fileName' Ljava/lang/String;

.field private 'fileNameLength' I

.field private 'fileNameUTF8Encoded' Z

.field private 'generalPurposeFlag' [B

.field private 'isEncrypted' Z

.field private 'lastModFileTime' I

.field private 'offsetStartOfData' J

.field private 'password' [C

.field private 'signature' I

.field private 'uncompressedSize' J

.field private 'versionNeededToExtract' I

.field private 'writeComprSizeInZip64ExtraRecord' Z

.field private 'zip64ExtendedInfo' Lnet/lingala/zip4j/model/Zip64ExtendedInfo;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_m1
putfield net/lingala/zip4j/model/LocalFileHeader/encryptionMethod I
aload 0
iconst_0
putfield net/lingala/zip4j/model/LocalFileHeader/writeComprSizeInZip64ExtraRecord Z
aload 0
lconst_0
putfield net/lingala/zip4j/model/LocalFileHeader/crc32 J
aload 0
lconst_0
putfield net/lingala/zip4j/model/LocalFileHeader/uncompressedSize J
return
.limit locals 1
.limit stack 3
.end method

.method public getAesExtraDataRecord()Lnet/lingala/zip4j/model/AESExtraDataRecord;
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/aesExtraDataRecord Lnet/lingala/zip4j/model/AESExtraDataRecord;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getCompressedSize()J
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/compressedSize J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getCompressionMethod()I
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/compressionMethod I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getCrc32()J
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/crc32 J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getCrcBuff()[B
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/crcBuff [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getEncryptionMethod()I
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/encryptionMethod I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getExtraDataRecords()Ljava/util/ArrayList;
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/extraDataRecords Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getExtraField()[B
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/extraField [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getExtraFieldLength()I
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/extraFieldLength I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getFileName()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/fileName Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getFileNameLength()I
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/fileNameLength I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getGeneralPurposeFlag()[B
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/generalPurposeFlag [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getLastModFileTime()I
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/lastModFileTime I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOffsetStartOfData()J
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/offsetStartOfData J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getPassword()[C
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/password [C
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSignature()I
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/signature I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getUncompressedSize()J
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/uncompressedSize J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getVersionNeededToExtract()I
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/versionNeededToExtract I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/zip64ExtendedInfo Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
areturn
.limit locals 1
.limit stack 1
.end method

.method public isDataDescriptorExists()Z
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/dataDescriptorExists Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isEncrypted()Z
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/isEncrypted Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isFileNameUTF8Encoded()Z
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/fileNameUTF8Encoded Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isWriteComprSizeInZip64ExtraRecord()Z
aload 0
getfield net/lingala/zip4j/model/LocalFileHeader/writeComprSizeInZip64ExtraRecord Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setAesExtraDataRecord(Lnet/lingala/zip4j/model/AESExtraDataRecord;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/LocalFileHeader/aesExtraDataRecord Lnet/lingala/zip4j/model/AESExtraDataRecord;
return
.limit locals 2
.limit stack 2
.end method

.method public setCompressedSize(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/LocalFileHeader/compressedSize J
return
.limit locals 3
.limit stack 3
.end method

.method public setCompressionMethod(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/LocalFileHeader/compressionMethod I
return
.limit locals 2
.limit stack 2
.end method

.method public setCrc32(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/LocalFileHeader/crc32 J
return
.limit locals 3
.limit stack 3
.end method

.method public setCrcBuff([B)V
aload 0
aload 1
putfield net/lingala/zip4j/model/LocalFileHeader/crcBuff [B
return
.limit locals 2
.limit stack 2
.end method

.method public setDataDescriptorExists(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/LocalFileHeader/dataDescriptorExists Z
return
.limit locals 2
.limit stack 2
.end method

.method public setEncrypted(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/LocalFileHeader/isEncrypted Z
return
.limit locals 2
.limit stack 2
.end method

.method public setEncryptionMethod(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/LocalFileHeader/encryptionMethod I
return
.limit locals 2
.limit stack 2
.end method

.method public setExtraDataRecords(Ljava/util/ArrayList;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/LocalFileHeader/extraDataRecords Ljava/util/ArrayList;
return
.limit locals 2
.limit stack 2
.end method

.method public setExtraField([B)V
aload 0
aload 1
putfield net/lingala/zip4j/model/LocalFileHeader/extraField [B
return
.limit locals 2
.limit stack 2
.end method

.method public setExtraFieldLength(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/LocalFileHeader/extraFieldLength I
return
.limit locals 2
.limit stack 2
.end method

.method public setFileName(Ljava/lang/String;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/LocalFileHeader/fileName Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setFileNameLength(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/LocalFileHeader/fileNameLength I
return
.limit locals 2
.limit stack 2
.end method

.method public setFileNameUTF8Encoded(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/LocalFileHeader/fileNameUTF8Encoded Z
return
.limit locals 2
.limit stack 2
.end method

.method public setGeneralPurposeFlag([B)V
aload 0
aload 1
putfield net/lingala/zip4j/model/LocalFileHeader/generalPurposeFlag [B
return
.limit locals 2
.limit stack 2
.end method

.method public setLastModFileTime(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/LocalFileHeader/lastModFileTime I
return
.limit locals 2
.limit stack 2
.end method

.method public setOffsetStartOfData(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/LocalFileHeader/offsetStartOfData J
return
.limit locals 3
.limit stack 3
.end method

.method public setPassword([C)V
aload 0
aload 1
putfield net/lingala/zip4j/model/LocalFileHeader/password [C
return
.limit locals 2
.limit stack 2
.end method

.method public setSignature(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/LocalFileHeader/signature I
return
.limit locals 2
.limit stack 2
.end method

.method public setUncompressedSize(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/LocalFileHeader/uncompressedSize J
return
.limit locals 3
.limit stack 3
.end method

.method public setVersionNeededToExtract(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/LocalFileHeader/versionNeededToExtract I
return
.limit locals 2
.limit stack 2
.end method

.method public setWriteComprSizeInZip64ExtraRecord(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/LocalFileHeader/writeComprSizeInZip64ExtraRecord Z
return
.limit locals 2
.limit stack 2
.end method

.method public setZip64ExtendedInfo(Lnet/lingala/zip4j/model/Zip64ExtendedInfo;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/LocalFileHeader/zip64ExtendedInfo Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
return
.limit locals 2
.limit stack 2
.end method
