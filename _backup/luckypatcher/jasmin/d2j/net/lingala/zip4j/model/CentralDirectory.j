.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/CentralDirectory
.super java/lang/Object

.field private 'digitalSignature' Lnet/lingala/zip4j/model/DigitalSignature;

.field private 'fileHeaders' Ljava/util/ArrayList;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public getDigitalSignature()Lnet/lingala/zip4j/model/DigitalSignature;
aload 0
getfield net/lingala/zip4j/model/CentralDirectory/digitalSignature Lnet/lingala/zip4j/model/DigitalSignature;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getFileHeaders()Ljava/util/ArrayList;
aload 0
getfield net/lingala/zip4j/model/CentralDirectory/fileHeaders Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method public setDigitalSignature(Lnet/lingala/zip4j/model/DigitalSignature;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/CentralDirectory/digitalSignature Lnet/lingala/zip4j/model/DigitalSignature;
return
.limit locals 2
.limit stack 2
.end method

.method public setFileHeaders(Ljava/util/ArrayList;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/CentralDirectory/fileHeaders Ljava/util/ArrayList;
return
.limit locals 2
.limit stack 2
.end method
