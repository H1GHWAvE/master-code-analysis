.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/Zip64EndCentralDirRecord
.super java/lang/Object

.field private 'extensibleDataSector' [B

.field private 'noOfThisDisk' I

.field private 'noOfThisDiskStartOfCentralDir' I

.field private 'offsetStartCenDirWRTStartDiskNo' J

.field private 'signature' J

.field private 'sizeOfCentralDir' J

.field private 'sizeOfZip64EndCentralDirRec' J

.field private 'totNoOfEntriesInCentralDir' J

.field private 'totNoOfEntriesInCentralDirOnThisDisk' J

.field private 'versionMadeBy' I

.field private 'versionNeededToExtract' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public getExtensibleDataSector()[B
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/extensibleDataSector [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getNoOfThisDisk()I
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/noOfThisDisk I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getNoOfThisDiskStartOfCentralDir()I
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/noOfThisDiskStartOfCentralDir I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOffsetStartCenDirWRTStartDiskNo()J
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/offsetStartCenDirWRTStartDiskNo J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getSignature()J
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/signature J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getSizeOfCentralDir()J
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/sizeOfCentralDir J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getSizeOfZip64EndCentralDirRec()J
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/sizeOfZip64EndCentralDirRec J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getTotNoOfEntriesInCentralDir()J
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/totNoOfEntriesInCentralDir J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getTotNoOfEntriesInCentralDirOnThisDisk()J
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/totNoOfEntriesInCentralDirOnThisDisk J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getVersionMadeBy()I
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/versionMadeBy I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getVersionNeededToExtract()I
aload 0
getfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/versionNeededToExtract I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setExtensibleDataSector([B)V
aload 0
aload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/extensibleDataSector [B
return
.limit locals 2
.limit stack 2
.end method

.method public setNoOfThisDisk(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/noOfThisDisk I
return
.limit locals 2
.limit stack 2
.end method

.method public setNoOfThisDiskStartOfCentralDir(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/noOfThisDiskStartOfCentralDir I
return
.limit locals 2
.limit stack 2
.end method

.method public setOffsetStartCenDirWRTStartDiskNo(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/offsetStartCenDirWRTStartDiskNo J
return
.limit locals 3
.limit stack 3
.end method

.method public setSignature(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/signature J
return
.limit locals 3
.limit stack 3
.end method

.method public setSizeOfCentralDir(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/sizeOfCentralDir J
return
.limit locals 3
.limit stack 3
.end method

.method public setSizeOfZip64EndCentralDirRec(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/sizeOfZip64EndCentralDirRec J
return
.limit locals 3
.limit stack 3
.end method

.method public setTotNoOfEntriesInCentralDir(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/totNoOfEntriesInCentralDir J
return
.limit locals 3
.limit stack 3
.end method

.method public setTotNoOfEntriesInCentralDirOnThisDisk(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/totNoOfEntriesInCentralDirOnThisDisk J
return
.limit locals 3
.limit stack 3
.end method

.method public setVersionMadeBy(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/versionMadeBy I
return
.limit locals 2
.limit stack 2
.end method

.method public setVersionNeededToExtract(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/Zip64EndCentralDirRecord/versionNeededToExtract I
return
.limit locals 2
.limit stack 2
.end method
