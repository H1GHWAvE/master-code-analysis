.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/ZipParameters
.super java/lang/Object
.implements java/lang/Cloneable

.field private 'aesKeyStrength' I

.field private 'compressionLevel' I

.field private 'compressionMethod' I

.field private 'defaultFolderPath' Ljava/lang/String;

.field private 'encryptFiles' Z

.field private 'encryptionMethod' I

.field private 'fileNameInZip' Ljava/lang/String;

.field private 'includeRootFolder' Z

.field private 'isSourceExternalStream' Z

.field private 'password' [C

.field private 'readHiddenFiles' Z

.field private 'rootFolderInZip' Ljava/lang/String;

.field private 'sourceFileCRC' I

.field private 'timeZone' Ljava/util/TimeZone;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
bipush 8
putfield net/lingala/zip4j/model/ZipParameters/compressionMethod I
aload 0
iconst_0
putfield net/lingala/zip4j/model/ZipParameters/encryptFiles Z
aload 0
iconst_1
putfield net/lingala/zip4j/model/ZipParameters/readHiddenFiles Z
aload 0
iconst_m1
putfield net/lingala/zip4j/model/ZipParameters/encryptionMethod I
aload 0
iconst_m1
putfield net/lingala/zip4j/model/ZipParameters/aesKeyStrength I
aload 0
iconst_1
putfield net/lingala/zip4j/model/ZipParameters/includeRootFolder Z
aload 0
invokestatic java/util/TimeZone/getDefault()Ljava/util/TimeZone;
putfield net/lingala/zip4j/model/ZipParameters/timeZone Ljava/util/TimeZone;
return
.limit locals 1
.limit stack 2
.end method

.method public clone()Ljava/lang/Object;
.throws java/lang/CloneNotSupportedException
aload 0
invokespecial java/lang/Object/clone()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getAesKeyStrength()I
aload 0
getfield net/lingala/zip4j/model/ZipParameters/aesKeyStrength I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getCompressionLevel()I
aload 0
getfield net/lingala/zip4j/model/ZipParameters/compressionLevel I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getCompressionMethod()I
aload 0
getfield net/lingala/zip4j/model/ZipParameters/compressionMethod I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getDefaultFolderPath()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/model/ZipParameters/defaultFolderPath Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getEncryptionMethod()I
aload 0
getfield net/lingala/zip4j/model/ZipParameters/encryptionMethod I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getFileNameInZip()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/model/ZipParameters/fileNameInZip Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getPassword()[C
aload 0
getfield net/lingala/zip4j/model/ZipParameters/password [C
areturn
.limit locals 1
.limit stack 1
.end method

.method public getRootFolderInZip()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/model/ZipParameters/rootFolderInZip Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSourceFileCRC()I
aload 0
getfield net/lingala/zip4j/model/ZipParameters/sourceFileCRC I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getTimeZone()Ljava/util/TimeZone;
aload 0
getfield net/lingala/zip4j/model/ZipParameters/timeZone Ljava/util/TimeZone;
areturn
.limit locals 1
.limit stack 1
.end method

.method public isEncryptFiles()Z
aload 0
getfield net/lingala/zip4j/model/ZipParameters/encryptFiles Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isIncludeRootFolder()Z
aload 0
getfield net/lingala/zip4j/model/ZipParameters/includeRootFolder Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isReadHiddenFiles()Z
aload 0
getfield net/lingala/zip4j/model/ZipParameters/readHiddenFiles Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isSourceExternalStream()Z
aload 0
getfield net/lingala/zip4j/model/ZipParameters/isSourceExternalStream Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setAesKeyStrength(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ZipParameters/aesKeyStrength I
return
.limit locals 2
.limit stack 2
.end method

.method public setCompressionLevel(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ZipParameters/compressionLevel I
return
.limit locals 2
.limit stack 2
.end method

.method public setCompressionMethod(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ZipParameters/compressionMethod I
return
.limit locals 2
.limit stack 2
.end method

.method public setDefaultFolderPath(Ljava/lang/String;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ZipParameters/defaultFolderPath Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setEncryptFiles(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ZipParameters/encryptFiles Z
return
.limit locals 2
.limit stack 2
.end method

.method public setEncryptionMethod(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ZipParameters/encryptionMethod I
return
.limit locals 2
.limit stack 2
.end method

.method public setFileNameInZip(Ljava/lang/String;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ZipParameters/fileNameInZip Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setIncludeRootFolder(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ZipParameters/includeRootFolder Z
return
.limit locals 2
.limit stack 2
.end method

.method public setPassword(Ljava/lang/String;)V
aload 1
ifnonnull L0
return
L0:
aload 0
aload 1
invokevirtual java/lang/String/toCharArray()[C
invokevirtual net/lingala/zip4j/model/ZipParameters/setPassword([C)V
return
.limit locals 2
.limit stack 2
.end method

.method public setPassword([C)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ZipParameters/password [C
return
.limit locals 2
.limit stack 2
.end method

.method public setReadHiddenFiles(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ZipParameters/readHiddenFiles Z
return
.limit locals 2
.limit stack 2
.end method

.method public setRootFolderInZip(Ljava/lang/String;)V
aload 1
astore 2
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifeq L0
aload 1
astore 2
aload 1
ldc "\\"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L1
aload 1
astore 2
aload 1
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic net/lingala/zip4j/util/InternalZipConstants/FILE_SEPARATOR Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
L1:
aload 2
ldc "\\\\"
ldc "/"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 2
L0:
aload 0
aload 2
putfield net/lingala/zip4j/model/ZipParameters/rootFolderInZip Ljava/lang/String;
return
.limit locals 3
.limit stack 3
.end method

.method public setSourceExternalStream(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ZipParameters/isSourceExternalStream Z
return
.limit locals 2
.limit stack 2
.end method

.method public setSourceFileCRC(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ZipParameters/sourceFileCRC I
return
.limit locals 2
.limit stack 2
.end method

.method public setTimeZone(Ljava/util/TimeZone;)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ZipParameters/timeZone Ljava/util/TimeZone;
return
.limit locals 2
.limit stack 2
.end method
