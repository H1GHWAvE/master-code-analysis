.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/Zip64ExtendedInfo
.super java/lang/Object

.field private 'compressedSize' J

.field private 'diskNumberStart' I

.field private 'header' I

.field private 'offsetLocalHeader' J

.field private 'size' I

.field private 'unCompressedSize' J

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc2_w -1L
putfield net/lingala/zip4j/model/Zip64ExtendedInfo/compressedSize J
aload 0
ldc2_w -1L
putfield net/lingala/zip4j/model/Zip64ExtendedInfo/unCompressedSize J
aload 0
ldc2_w -1L
putfield net/lingala/zip4j/model/Zip64ExtendedInfo/offsetLocalHeader J
aload 0
iconst_m1
putfield net/lingala/zip4j/model/Zip64ExtendedInfo/diskNumberStart I
return
.limit locals 1
.limit stack 3
.end method

.method public getCompressedSize()J
aload 0
getfield net/lingala/zip4j/model/Zip64ExtendedInfo/compressedSize J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getDiskNumberStart()I
aload 0
getfield net/lingala/zip4j/model/Zip64ExtendedInfo/diskNumberStart I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getHeader()I
aload 0
getfield net/lingala/zip4j/model/Zip64ExtendedInfo/header I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOffsetLocalHeader()J
aload 0
getfield net/lingala/zip4j/model/Zip64ExtendedInfo/offsetLocalHeader J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getSize()I
aload 0
getfield net/lingala/zip4j/model/Zip64ExtendedInfo/size I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getUnCompressedSize()J
aload 0
getfield net/lingala/zip4j/model/Zip64ExtendedInfo/unCompressedSize J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public setCompressedSize(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/Zip64ExtendedInfo/compressedSize J
return
.limit locals 3
.limit stack 3
.end method

.method public setDiskNumberStart(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/Zip64ExtendedInfo/diskNumberStart I
return
.limit locals 2
.limit stack 2
.end method

.method public setHeader(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/Zip64ExtendedInfo/header I
return
.limit locals 2
.limit stack 2
.end method

.method public setOffsetLocalHeader(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/Zip64ExtendedInfo/offsetLocalHeader J
return
.limit locals 3
.limit stack 3
.end method

.method public setSize(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/Zip64ExtendedInfo/size I
return
.limit locals 2
.limit stack 2
.end method

.method public setUnCompressedSize(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/Zip64ExtendedInfo/unCompressedSize J
return
.limit locals 3
.limit stack 3
.end method
