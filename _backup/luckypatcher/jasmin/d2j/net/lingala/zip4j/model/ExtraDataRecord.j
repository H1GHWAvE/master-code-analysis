.bytecode 50.0
.class public synchronized net/lingala/zip4j/model/ExtraDataRecord
.super java/lang/Object

.field private 'data' [B

.field private 'header' J

.field private 'sizeOfData' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public getData()[B
aload 0
getfield net/lingala/zip4j/model/ExtraDataRecord/data [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getHeader()J
aload 0
getfield net/lingala/zip4j/model/ExtraDataRecord/header J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getSizeOfData()I
aload 0
getfield net/lingala/zip4j/model/ExtraDataRecord/sizeOfData I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setData([B)V
aload 0
aload 1
putfield net/lingala/zip4j/model/ExtraDataRecord/data [B
return
.limit locals 2
.limit stack 2
.end method

.method public setHeader(J)V
aload 0
lload 1
putfield net/lingala/zip4j/model/ExtraDataRecord/header J
return
.limit locals 3
.limit stack 3
.end method

.method public setSizeOfData(I)V
aload 0
iload 1
putfield net/lingala/zip4j/model/ExtraDataRecord/sizeOfData I
return
.limit locals 2
.limit stack 2
.end method
