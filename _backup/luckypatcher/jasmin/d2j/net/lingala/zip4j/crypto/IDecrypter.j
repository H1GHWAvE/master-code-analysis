.bytecode 50.0
.class public abstract interface net/lingala/zip4j/crypto/IDecrypter
.super java/lang/Object

.method public abstract decryptData([B)I
.throws net/lingala/zip4j/exception/ZipException
.end method

.method public abstract decryptData([BII)I
.throws net/lingala/zip4j/exception/ZipException
.end method
