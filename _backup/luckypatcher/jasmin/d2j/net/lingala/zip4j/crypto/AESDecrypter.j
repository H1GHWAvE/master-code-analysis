.bytecode 50.0
.class public synchronized net/lingala/zip4j/crypto/AESDecrypter
.super java/lang/Object
.implements net/lingala/zip4j/crypto/IDecrypter

.field private 'KEY_LENGTH' I

.field private 'MAC_LENGTH' I

.field private final 'PASSWORD_VERIFIER_LENGTH' I

.field private 'SALT_LENGTH' I

.field private 'aesEngine' Lnet/lingala/zip4j/crypto/engine/AESEngine;

.field private 'aesKey' [B

.field private 'counterBlock' [B

.field private 'derivedPasswordVerifier' [B

.field private 'iv' [B

.field private 'localFileHeader' Lnet/lingala/zip4j/model/LocalFileHeader;

.field private 'loopCount' I

.field private 'mac' Lnet/lingala/zip4j/crypto/PBKDF2/MacBasedPRF;

.field private 'macKey' [B

.field private 'nonce' I

.field private 'storedMac' [B

.method public <init>(Lnet/lingala/zip4j/model/LocalFileHeader;[B[B)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_2
putfield net/lingala/zip4j/crypto/AESDecrypter/PASSWORD_VERIFIER_LENGTH I
aload 0
iconst_1
putfield net/lingala/zip4j/crypto/AESDecrypter/nonce I
aload 0
iconst_0
putfield net/lingala/zip4j/crypto/AESDecrypter/loopCount I
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "one of the input parameters is null in AESDecryptor Constructor"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield net/lingala/zip4j/crypto/AESDecrypter/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
aconst_null
putfield net/lingala/zip4j/crypto/AESDecrypter/storedMac [B
aload 0
bipush 16
newarray byte
putfield net/lingala/zip4j/crypto/AESDecrypter/iv [B
aload 0
bipush 16
newarray byte
putfield net/lingala/zip4j/crypto/AESDecrypter/counterBlock [B
aload 0
aload 2
aload 3
invokespecial net/lingala/zip4j/crypto/AESDecrypter/init([B[B)V
return
.limit locals 4
.limit stack 3
.end method

.method private deriveKey([B[C)[B
.throws net/lingala/zip4j/exception/ZipException
.catch java/lang/Exception from L0 to L1 using L2
L0:
new net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine
dup
new net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters
dup
ldc "HmacSHA1"
ldc "ISO-8859-1"
aload 1
sipush 1000
invokespecial net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/<init>(Ljava/lang/String;Ljava/lang/String;[BI)V
invokespecial net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/<init>(Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;)V
aload 2
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/KEY_LENGTH I
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/MAC_LENGTH I
iadd
iconst_2
iadd
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/deriveKey([CI)[B
astore 1
L1:
aload 1
areturn
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 8
.end method

.method private init([B[B)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid file header in init method of AESDecryptor"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getAesExtraDataRecord()Lnet/lingala/zip4j/model/AESExtraDataRecord;
astore 3
aload 3
ifnonnull L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid aes extra data record - in init method of AESDecryptor"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 3
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getAesStrength()I
tableswitch 1
L2
L3
L4
default : L5
L5:
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "invalid aes key strength for file: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getFileName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
bipush 16
putfield net/lingala/zip4j/crypto/AESDecrypter/KEY_LENGTH I
aload 0
bipush 16
putfield net/lingala/zip4j/crypto/AESDecrypter/MAC_LENGTH I
aload 0
bipush 8
putfield net/lingala/zip4j/crypto/AESDecrypter/SALT_LENGTH I
L6:
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getPassword()[C
ifnull L7
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getPassword()[C
arraylength
ifgt L8
L7:
new net/lingala/zip4j/exception/ZipException
dup
ldc "empty or null password provided for AES Decryptor"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
bipush 24
putfield net/lingala/zip4j/crypto/AESDecrypter/KEY_LENGTH I
aload 0
bipush 24
putfield net/lingala/zip4j/crypto/AESDecrypter/MAC_LENGTH I
aload 0
bipush 12
putfield net/lingala/zip4j/crypto/AESDecrypter/SALT_LENGTH I
goto L6
L4:
aload 0
bipush 32
putfield net/lingala/zip4j/crypto/AESDecrypter/KEY_LENGTH I
aload 0
bipush 32
putfield net/lingala/zip4j/crypto/AESDecrypter/MAC_LENGTH I
aload 0
bipush 16
putfield net/lingala/zip4j/crypto/AESDecrypter/SALT_LENGTH I
goto L6
L8:
aload 0
aload 1
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getPassword()[C
invokespecial net/lingala/zip4j/crypto/AESDecrypter/deriveKey([B[C)[B
astore 1
aload 1
ifnull L9
aload 1
arraylength
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/KEY_LENGTH I
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/MAC_LENGTH I
iadd
iconst_2
iadd
if_icmpeq L10
L9:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid derived key"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L10:
aload 0
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/KEY_LENGTH I
newarray byte
putfield net/lingala/zip4j/crypto/AESDecrypter/aesKey [B
aload 0
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/MAC_LENGTH I
newarray byte
putfield net/lingala/zip4j/crypto/AESDecrypter/macKey [B
aload 0
iconst_2
newarray byte
putfield net/lingala/zip4j/crypto/AESDecrypter/derivedPasswordVerifier [B
aload 1
iconst_0
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/aesKey [B
iconst_0
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/KEY_LENGTH I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/KEY_LENGTH I
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/macKey [B
iconst_0
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/MAC_LENGTH I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/KEY_LENGTH I
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/MAC_LENGTH I
iadd
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/derivedPasswordVerifier [B
iconst_0
iconst_2
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/derivedPasswordVerifier [B
ifnonnull L11
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid derived password verifier for AES"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L11:
aload 2
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/derivedPasswordVerifier [B
invokestatic java/util/Arrays/equals([B[B)Z
ifne L12
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Wrong Password for file: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getFileName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iconst_5
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;I)V
athrow
L12:
aload 0
new net/lingala/zip4j/crypto/engine/AESEngine
dup
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/aesKey [B
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/<init>([B)V
putfield net/lingala/zip4j/crypto/AESDecrypter/aesEngine Lnet/lingala/zip4j/crypto/engine/AESEngine;
aload 0
new net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF
dup
ldc "HmacSHA1"
invokespecial net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/<init>(Ljava/lang/String;)V
putfield net/lingala/zip4j/crypto/AESDecrypter/mac Lnet/lingala/zip4j/crypto/PBKDF2/MacBasedPRF;
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/mac Lnet/lingala/zip4j/crypto/PBKDF2/MacBasedPRF;
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/macKey [B
invokevirtual net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/init([B)V
return
.limit locals 4
.limit stack 5
.end method

.method public decryptData([B)I
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual net/lingala/zip4j/crypto/AESDecrypter/decryptData([BII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method public decryptData([BII)I
.throws net/lingala/zip4j/exception/ZipException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch net/lingala/zip4j/exception/ZipException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
.catch net/lingala/zip4j/exception/ZipException from L6 to L7 using L2
.catch java/lang/Exception from L6 to L7 using L3
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/aesEngine Lnet/lingala/zip4j/crypto/engine/AESEngine;
ifnonnull L8
new net/lingala/zip4j/exception/ZipException
dup
ldc "AES not initialized properly"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L8:
iload 2
istore 4
L9:
iload 4
iload 2
iload 3
iadd
if_icmpge L10
iload 4
bipush 16
iadd
iload 2
iload 3
iadd
if_icmpgt L11
bipush 16
istore 5
L0:
aload 0
iload 5
putfield net/lingala/zip4j/crypto/AESDecrypter/loopCount I
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/mac Lnet/lingala/zip4j/crypto/PBKDF2/MacBasedPRF;
aload 1
iload 4
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/loopCount I
invokevirtual net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/update([BII)V
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/iv [B
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/nonce I
bipush 16
invokestatic net/lingala/zip4j/util/Raw/prepareBuffAESIVBytes([BII)V
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/aesEngine Lnet/lingala/zip4j/crypto/engine/AESEngine;
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/iv [B
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/counterBlock [B
invokevirtual net/lingala/zip4j/crypto/engine/AESEngine/processBlock([B[B)I
pop
L1:
iconst_0
istore 5
L4:
iload 5
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/loopCount I
if_icmpge L6
aload 1
iload 4
iload 5
iadd
aload 1
iload 4
iload 5
iadd
baload
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/counterBlock [B
iload 5
baload
ixor
i2b
bastore
L5:
iload 5
iconst_1
iadd
istore 5
goto L4
L6:
aload 0
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/nonce I
iconst_1
iadd
putfield net/lingala/zip4j/crypto/AESDecrypter/nonce I
L7:
iload 4
bipush 16
iadd
istore 4
goto L9
L2:
astore 1
aload 1
athrow
L3:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L10:
iload 3
ireturn
L11:
iload 2
iload 3
iadd
iload 4
isub
istore 5
goto L0
.limit locals 6
.limit stack 5
.end method

.method public getCalculatedAuthenticationBytes()[B
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/mac Lnet/lingala/zip4j/crypto/PBKDF2/MacBasedPRF;
invokevirtual net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/doFinal()[B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getPasswordVerifierLength()I
iconst_2
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSaltLength()I
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/SALT_LENGTH I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getStoredMac()[B
aload 0
getfield net/lingala/zip4j/crypto/AESDecrypter/storedMac [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public setStoredMac([B)V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/AESDecrypter/storedMac [B
return
.limit locals 2
.limit stack 2
.end method
