.bytecode 50.0
.class synchronized net/lingala/zip4j/crypto/PBKDF2/BinTools
.super java/lang/Object

.field public static final 'hex' Ljava/lang/String; = "0123456789ABCDEF"

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static bin2hex([B)Ljava/lang/String;
aload 0
ifnonnull L0
ldc ""
areturn
L0:
new java/lang/StringBuffer
dup
aload 0
arraylength
iconst_2
imul
invokespecial java/lang/StringBuffer/<init>(I)V
astore 4
aload 0
arraylength
istore 2
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L2
aload 0
iload 1
baload
sipush 256
iadd
sipush 256
irem
istore 3
aload 4
ldc "0123456789ABCDEF"
iload 3
bipush 16
idiv
bipush 15
iand
invokevirtual java/lang/String/charAt(I)C
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 4
ldc "0123456789ABCDEF"
iload 3
bipush 16
irem
bipush 15
iand
invokevirtual java/lang/String/charAt(I)C
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 4
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 5
.limit stack 4
.end method

.method public static hex2bin(C)I
iload 0
bipush 48
if_icmplt L0
iload 0
bipush 57
if_icmpgt L0
iload 0
bipush 48
isub
ireturn
L0:
iload 0
bipush 65
if_icmplt L1
iload 0
bipush 70
if_icmpgt L1
iload 0
bipush 65
isub
bipush 10
iadd
ireturn
L1:
iload 0
bipush 97
if_icmplt L2
iload 0
bipush 102
if_icmpgt L2
iload 0
bipush 97
isub
bipush 10
iadd
ireturn
L2:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Input string may only contain hex digits, but found '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 0
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 4
.end method

.method public static hex2bin(Ljava/lang/String;)[B
aload 0
astore 6
aload 0
ifnonnull L0
ldc ""
astore 6
L1:
aload 6
invokevirtual java/lang/String/length()I
iconst_2
idiv
newarray byte
astore 0
iconst_0
istore 4
iconst_0
istore 3
L2:
iload 4
aload 6
invokevirtual java/lang/String/length()I
if_icmpge L3
iload 4
iconst_1
iadd
istore 5
aload 6
iload 4
invokevirtual java/lang/String/charAt(I)C
istore 1
iload 5
iconst_1
iadd
istore 4
aload 6
iload 5
invokevirtual java/lang/String/charAt(I)C
istore 2
aload 0
iload 3
iload 1
invokestatic net/lingala/zip4j/crypto/PBKDF2/BinTools/hex2bin(C)I
bipush 16
imul
iload 2
invokestatic net/lingala/zip4j/crypto/PBKDF2/BinTools/hex2bin(C)I
iadd
i2b
bastore
iload 3
iconst_1
iadd
istore 3
goto L2
L0:
aload 0
invokevirtual java/lang/String/length()I
iconst_2
irem
ifeq L1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 6
goto L1
L3:
aload 0
areturn
.limit locals 7
.limit stack 4
.end method
