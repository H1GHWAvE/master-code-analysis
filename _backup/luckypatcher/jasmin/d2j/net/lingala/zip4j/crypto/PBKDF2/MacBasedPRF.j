.bytecode 50.0
.class public synchronized net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF
.super java/lang/Object
.implements net/lingala/zip4j/crypto/PBKDF2/PRF

.field protected 'hLen' I

.field protected 'mac' Ljavax/crypto/Mac;

.field protected 'macAlgorithm' Ljava/lang/String;

.method public <init>(Ljava/lang/String;)V
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/macAlgorithm Ljava/lang/String;
L0:
aload 0
aload 1
invokestatic javax/crypto/Mac/getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;
putfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/mac Ljavax/crypto/Mac;
aload 0
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/mac Ljavax/crypto/Mac;
invokevirtual javax/crypto/Mac/getMacLength()I
putfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/hLen I
L1:
return
L2:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/String;)V
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/security/NoSuchProviderException from L0 to L1 using L3
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/macAlgorithm Ljava/lang/String;
L0:
aload 0
aload 1
aload 2
invokestatic javax/crypto/Mac/getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Mac;
putfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/mac Ljavax/crypto/Mac;
aload 0
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/mac Ljavax/crypto/Mac;
invokevirtual javax/crypto/Mac/getMacLength()I
putfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/hLen I
L1:
return
L2:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public doFinal()[B
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/mac Ljavax/crypto/Mac;
invokevirtual javax/crypto/Mac/doFinal()[B
areturn
.limit locals 1
.limit stack 1
.end method

.method public doFinal([B)[B
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/mac Ljavax/crypto/Mac;
aload 1
invokevirtual javax/crypto/Mac/doFinal([B)[B
areturn
.limit locals 2
.limit stack 2
.end method

.method public getHLen()I
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/hLen I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public init([B)V
.catch java/security/InvalidKeyException from L0 to L1 using L2
L0:
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/mac Ljavax/crypto/Mac;
new javax/crypto/spec/SecretKeySpec
dup
aload 1
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/macAlgorithm Ljava/lang/String;
invokespecial javax/crypto/spec/SecretKeySpec/<init>([BLjava/lang/String;)V
invokevirtual javax/crypto/Mac/init(Ljava/security/Key;)V
L1:
return
L2:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 5
.end method

.method public update([B)V
.catch java/lang/IllegalStateException from L0 to L1 using L2
L0:
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/mac Ljavax/crypto/Mac;
aload 1
invokevirtual javax/crypto/Mac/update([B)V
L1:
return
L2:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public update([BII)V
.catch java/lang/IllegalStateException from L0 to L1 using L2
L0:
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/mac Ljavax/crypto/Mac;
aload 1
iload 2
iload 3
invokevirtual javax/crypto/Mac/update([BII)V
L1:
return
L2:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 4
.limit stack 4
.end method
