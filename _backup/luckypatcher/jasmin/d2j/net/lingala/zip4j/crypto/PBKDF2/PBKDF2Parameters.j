.bytecode 50.0
.class public synchronized net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters
.super java/lang/Object

.field protected 'derivedKey' [B

.field protected 'hashAlgorithm' Ljava/lang/String;

.field protected 'hashCharset' Ljava/lang/String;

.field protected 'iterationCount' I

.field protected 'salt' [B

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/hashAlgorithm Ljava/lang/String;
aload 0
ldc "UTF-8"
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/hashCharset Ljava/lang/String;
aload 0
aconst_null
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/salt [B
aload 0
sipush 1000
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/iterationCount I
aload 0
aconst_null
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/derivedKey [B
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/String;[BI)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/hashAlgorithm Ljava/lang/String;
aload 0
aload 2
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/hashCharset Ljava/lang/String;
aload 0
aload 3
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/salt [B
aload 0
iload 4
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/iterationCount I
aload 0
aconst_null
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/derivedKey [B
return
.limit locals 5
.limit stack 2
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/String;[BI[B)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/hashAlgorithm Ljava/lang/String;
aload 0
aload 2
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/hashCharset Ljava/lang/String;
aload 0
aload 3
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/salt [B
aload 0
iload 4
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/iterationCount I
aload 0
aload 5
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/derivedKey [B
return
.limit locals 6
.limit stack 2
.end method

.method public getDerivedKey()[B
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/derivedKey [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getHashAlgorithm()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/hashAlgorithm Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getHashCharset()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/hashCharset Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getIterationCount()I
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/iterationCount I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSalt()[B
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/salt [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public setDerivedKey([B)V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/derivedKey [B
return
.limit locals 2
.limit stack 2
.end method

.method public setHashAlgorithm(Ljava/lang/String;)V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/hashAlgorithm Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setHashCharset(Ljava/lang/String;)V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/hashCharset Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setIterationCount(I)V
aload 0
iload 1
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/iterationCount I
return
.limit locals 2
.limit stack 2
.end method

.method public setSalt([B)V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/salt [B
return
.limit locals 2
.limit stack 2
.end method
