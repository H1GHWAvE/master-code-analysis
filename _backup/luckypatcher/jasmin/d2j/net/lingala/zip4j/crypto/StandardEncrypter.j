.bytecode 50.0
.class public synchronized net/lingala/zip4j/crypto/StandardEncrypter
.super java/lang/Object
.implements net/lingala/zip4j/crypto/IEncrypter

.field private 'headerBytes' [B

.field private 'zipCryptoEngine' Lnet/lingala/zip4j/crypto/engine/ZipCryptoEngine;

.method public <init>([CI)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
ifnull L0
aload 1
arraylength
ifgt L1
L0:
new net/lingala/zip4j/exception/ZipException
dup
ldc "input password is null or empty in standard encrpyter constructor"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
new net/lingala/zip4j/crypto/engine/ZipCryptoEngine
dup
invokespecial net/lingala/zip4j/crypto/engine/ZipCryptoEngine/<init>()V
putfield net/lingala/zip4j/crypto/StandardEncrypter/zipCryptoEngine Lnet/lingala/zip4j/crypto/engine/ZipCryptoEngine;
aload 0
bipush 12
newarray byte
putfield net/lingala/zip4j/crypto/StandardEncrypter/headerBytes [B
aload 0
aload 1
iload 2
invokespecial net/lingala/zip4j/crypto/StandardEncrypter/init([CI)V
return
.limit locals 3
.limit stack 3
.end method

.method private init([CI)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnull L0
aload 1
arraylength
ifgt L1
L0:
new net/lingala/zip4j/exception/ZipException
dup
ldc "input password is null or empty, cannot initialize standard encrypter"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield net/lingala/zip4j/crypto/StandardEncrypter/zipCryptoEngine Lnet/lingala/zip4j/crypto/engine/ZipCryptoEngine;
aload 1
invokevirtual net/lingala/zip4j/crypto/engine/ZipCryptoEngine/initKeys([C)V
aload 0
aload 0
bipush 12
invokevirtual net/lingala/zip4j/crypto/StandardEncrypter/generateRandomBytes(I)[B
putfield net/lingala/zip4j/crypto/StandardEncrypter/headerBytes [B
aload 0
getfield net/lingala/zip4j/crypto/StandardEncrypter/zipCryptoEngine Lnet/lingala/zip4j/crypto/engine/ZipCryptoEngine;
aload 1
invokevirtual net/lingala/zip4j/crypto/engine/ZipCryptoEngine/initKeys([C)V
aload 0
getfield net/lingala/zip4j/crypto/StandardEncrypter/headerBytes [B
bipush 11
iload 2
bipush 24
iushr
i2b
bastore
aload 0
getfield net/lingala/zip4j/crypto/StandardEncrypter/headerBytes [B
bipush 10
iload 2
bipush 16
iushr
i2b
bastore
aload 0
getfield net/lingala/zip4j/crypto/StandardEncrypter/headerBytes [B
arraylength
bipush 12
if_icmpge L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid header bytes generated, cannot perform standard encryption"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
aload 0
getfield net/lingala/zip4j/crypto/StandardEncrypter/headerBytes [B
invokevirtual net/lingala/zip4j/crypto/StandardEncrypter/encryptData([B)I
pop
return
.limit locals 3
.limit stack 4
.end method

.method protected encryptByte(B)B
aload 0
getfield net/lingala/zip4j/crypto/StandardEncrypter/zipCryptoEngine Lnet/lingala/zip4j/crypto/engine/ZipCryptoEngine;
invokevirtual net/lingala/zip4j/crypto/engine/ZipCryptoEngine/decryptByte()B
sipush 255
iand
iload 1
ixor
i2b
istore 2
aload 0
getfield net/lingala/zip4j/crypto/StandardEncrypter/zipCryptoEngine Lnet/lingala/zip4j/crypto/engine/ZipCryptoEngine;
iload 1
invokevirtual net/lingala/zip4j/crypto/engine/ZipCryptoEngine/updateKeys(B)V
iload 2
ireturn
.limit locals 3
.limit stack 2
.end method

.method public encryptData([B)I
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L0:
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual net/lingala/zip4j/crypto/StandardEncrypter/encryptData([BII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method public encryptData([BII)I
.throws net/lingala/zip4j/exception/ZipException
.catch java/lang/Exception from L0 to L1 using L2
iload 3
ifge L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid length specified to decrpyt data"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
iload 2
istore 4
L4:
iload 4
iload 2
iload 3
iadd
if_icmpge L5
L0:
aload 1
iload 4
aload 0
aload 1
iload 4
baload
invokevirtual net/lingala/zip4j/crypto/StandardEncrypter/encryptByte(B)B
bastore
L1:
iload 4
iconst_1
iadd
istore 4
goto L4
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L5:
iload 3
ireturn
.limit locals 5
.limit stack 5
.end method

.method protected generateRandomBytes(I)[B
.throws net/lingala/zip4j/exception/ZipException
iload 1
ifgt L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "size is either 0 or less than 0, cannot generate header for standard encryptor"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 1
newarray byte
astore 2
new java/util/Random
dup
invokespecial java/util/Random/<init>()V
astore 3
iconst_0
istore 1
L1:
iload 1
aload 2
arraylength
if_icmpge L2
aload 2
iload 1
aload 0
aload 3
sipush 256
invokevirtual java/util/Random/nextInt(I)I
i2b
invokevirtual net/lingala/zip4j/crypto/StandardEncrypter/encryptByte(B)B
bastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 2
areturn
.limit locals 4
.limit stack 5
.end method

.method public getHeaderBytes()[B
aload 0
getfield net/lingala/zip4j/crypto/StandardEncrypter/headerBytes [B
areturn
.limit locals 1
.limit stack 1
.end method
