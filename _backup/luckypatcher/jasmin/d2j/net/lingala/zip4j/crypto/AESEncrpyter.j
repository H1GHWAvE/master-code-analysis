.bytecode 50.0
.class public synchronized net/lingala/zip4j/crypto/AESEncrpyter
.super java/lang/Object
.implements net/lingala/zip4j/crypto/IEncrypter

.field private 'KEY_LENGTH' I

.field private 'MAC_LENGTH' I

.field private final 'PASSWORD_VERIFIER_LENGTH' I

.field private 'SALT_LENGTH' I

.field private 'aesEngine' Lnet/lingala/zip4j/crypto/engine/AESEngine;

.field private 'aesKey' [B

.field private 'counterBlock' [B

.field private 'derivedPasswordVerifier' [B

.field private 'finished' Z

.field private 'iv' [B

.field private 'keyStrength' I

.field private 'loopCount' I

.field private 'mac' Lnet/lingala/zip4j/crypto/PBKDF2/MacBasedPRF;

.field private 'macKey' [B

.field private 'nonce' I

.field private 'password' [C

.field private 'saltBytes' [B

.method public <init>([CI)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_2
putfield net/lingala/zip4j/crypto/AESEncrpyter/PASSWORD_VERIFIER_LENGTH I
aload 0
iconst_1
putfield net/lingala/zip4j/crypto/AESEncrpyter/nonce I
aload 0
iconst_0
putfield net/lingala/zip4j/crypto/AESEncrpyter/loopCount I
aload 1
ifnull L0
aload 1
arraylength
ifne L1
L0:
new net/lingala/zip4j/exception/ZipException
dup
ldc "input password is empty or null in AES encrypter constructor"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 2
iconst_1
if_icmpeq L2
iload 2
iconst_3
if_icmpeq L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "Invalid key strength in AES encrypter constructor"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
aload 1
putfield net/lingala/zip4j/crypto/AESEncrpyter/password [C
aload 0
iload 2
putfield net/lingala/zip4j/crypto/AESEncrpyter/keyStrength I
aload 0
iconst_0
putfield net/lingala/zip4j/crypto/AESEncrpyter/finished Z
aload 0
bipush 16
newarray byte
putfield net/lingala/zip4j/crypto/AESEncrpyter/counterBlock [B
aload 0
bipush 16
newarray byte
putfield net/lingala/zip4j/crypto/AESEncrpyter/iv [B
aload 0
invokespecial net/lingala/zip4j/crypto/AESEncrpyter/init()V
return
.limit locals 3
.limit stack 3
.end method

.method private deriveKey([B[C)[B
.throws net/lingala/zip4j/exception/ZipException
.catch java/lang/Exception from L0 to L1 using L2
L0:
new net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine
dup
new net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters
dup
ldc "HmacSHA1"
ldc "ISO-8859-1"
aload 1
sipush 1000
invokespecial net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/<init>(Ljava/lang/String;Ljava/lang/String;[BI)V
invokespecial net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/<init>(Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;)V
aload 2
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/KEY_LENGTH I
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/MAC_LENGTH I
iadd
iconst_2
iadd
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/deriveKey([CI)[B
astore 1
L1:
aload 1
areturn
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 8
.end method

.method private static generateSalt(I)[B
.throws net/lingala/zip4j/exception/ZipException
iload 0
bipush 8
if_icmpeq L0
iload 0
bipush 16
if_icmpeq L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid salt size, cannot generate salt"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
iconst_0
istore 1
iload 0
bipush 8
if_icmpne L1
iconst_2
istore 1
L1:
iload 0
bipush 16
if_icmpne L2
iconst_4
istore 1
L2:
iload 0
newarray byte
astore 3
iconst_0
istore 0
L3:
iload 0
iload 1
if_icmpge L4
new java/util/Random
dup
invokespecial java/util/Random/<init>()V
invokevirtual java/util/Random/nextInt()I
istore 2
aload 3
iload 0
iconst_4
imul
iconst_0
iadd
iload 2
bipush 24
ishr
i2b
bastore
aload 3
iload 0
iconst_4
imul
iconst_1
iadd
iload 2
bipush 16
ishr
i2b
bastore
aload 3
iload 0
iconst_4
imul
iconst_2
iadd
iload 2
bipush 8
ishr
i2b
bastore
aload 3
iload 0
iconst_4
imul
iconst_3
iadd
iload 2
i2b
bastore
iload 0
iconst_1
iadd
istore 0
goto L3
L4:
aload 3
areturn
.limit locals 4
.limit stack 4
.end method

.method private init()V
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/keyStrength I
tableswitch 1
L0
L1
L2
default : L1
L1:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid aes key strength, cannot determine key sizes"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
bipush 16
putfield net/lingala/zip4j/crypto/AESEncrpyter/KEY_LENGTH I
aload 0
bipush 16
putfield net/lingala/zip4j/crypto/AESEncrpyter/MAC_LENGTH I
aload 0
bipush 8
putfield net/lingala/zip4j/crypto/AESEncrpyter/SALT_LENGTH I
L3:
aload 0
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/SALT_LENGTH I
invokestatic net/lingala/zip4j/crypto/AESEncrpyter/generateSalt(I)[B
putfield net/lingala/zip4j/crypto/AESEncrpyter/saltBytes [B
aload 0
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/saltBytes [B
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/password [C
invokespecial net/lingala/zip4j/crypto/AESEncrpyter/deriveKey([B[C)[B
astore 1
aload 1
ifnull L4
aload 1
arraylength
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/KEY_LENGTH I
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/MAC_LENGTH I
iadd
iconst_2
iadd
if_icmpeq L5
L4:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid key generated, cannot decrypt file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
bipush 32
putfield net/lingala/zip4j/crypto/AESEncrpyter/KEY_LENGTH I
aload 0
bipush 32
putfield net/lingala/zip4j/crypto/AESEncrpyter/MAC_LENGTH I
aload 0
bipush 16
putfield net/lingala/zip4j/crypto/AESEncrpyter/SALT_LENGTH I
goto L3
L5:
aload 0
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/KEY_LENGTH I
newarray byte
putfield net/lingala/zip4j/crypto/AESEncrpyter/aesKey [B
aload 0
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/MAC_LENGTH I
newarray byte
putfield net/lingala/zip4j/crypto/AESEncrpyter/macKey [B
aload 0
iconst_2
newarray byte
putfield net/lingala/zip4j/crypto/AESEncrpyter/derivedPasswordVerifier [B
aload 1
iconst_0
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/aesKey [B
iconst_0
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/KEY_LENGTH I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/KEY_LENGTH I
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/macKey [B
iconst_0
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/MAC_LENGTH I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/KEY_LENGTH I
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/MAC_LENGTH I
iadd
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/derivedPasswordVerifier [B
iconst_0
iconst_2
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
new net/lingala/zip4j/crypto/engine/AESEngine
dup
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/aesKey [B
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/<init>([B)V
putfield net/lingala/zip4j/crypto/AESEncrpyter/aesEngine Lnet/lingala/zip4j/crypto/engine/AESEngine;
aload 0
new net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF
dup
ldc "HmacSHA1"
invokespecial net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/<init>(Ljava/lang/String;)V
putfield net/lingala/zip4j/crypto/AESEncrpyter/mac Lnet/lingala/zip4j/crypto/PBKDF2/MacBasedPRF;
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/mac Lnet/lingala/zip4j/crypto/PBKDF2/MacBasedPRF;
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/macKey [B
invokevirtual net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/init([B)V
return
.limit locals 2
.limit stack 5
.end method

.method public encryptData([B)I
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input bytes are null, cannot perform AES encrpytion"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual net/lingala/zip4j/crypto/AESEncrpyter/encryptData([BII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method public encryptData([BII)I
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/finished Z
ifeq L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "AES Encrypter is in finished state (A non 16 byte block has already been passed to encrypter)"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 3
bipush 16
irem
ifeq L1
aload 0
iconst_1
putfield net/lingala/zip4j/crypto/AESEncrpyter/finished Z
L1:
iload 2
istore 4
L2:
iload 4
iload 2
iload 3
iadd
if_icmpge L3
iload 4
bipush 16
iadd
iload 2
iload 3
iadd
if_icmpgt L4
bipush 16
istore 5
L5:
aload 0
iload 5
putfield net/lingala/zip4j/crypto/AESEncrpyter/loopCount I
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/iv [B
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/nonce I
bipush 16
invokestatic net/lingala/zip4j/util/Raw/prepareBuffAESIVBytes([BII)V
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/aesEngine Lnet/lingala/zip4j/crypto/engine/AESEngine;
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/iv [B
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/counterBlock [B
invokevirtual net/lingala/zip4j/crypto/engine/AESEngine/processBlock([B[B)I
pop
iconst_0
istore 5
L6:
iload 5
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/loopCount I
if_icmpge L7
aload 1
iload 4
iload 5
iadd
aload 1
iload 4
iload 5
iadd
baload
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/counterBlock [B
iload 5
baload
ixor
i2b
bastore
iload 5
iconst_1
iadd
istore 5
goto L6
L4:
iload 2
iload 3
iadd
iload 4
isub
istore 5
goto L5
L7:
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/mac Lnet/lingala/zip4j/crypto/PBKDF2/MacBasedPRF;
aload 1
iload 4
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/loopCount I
invokevirtual net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/update([BII)V
aload 0
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/nonce I
iconst_1
iadd
putfield net/lingala/zip4j/crypto/AESEncrpyter/nonce I
iload 4
bipush 16
iadd
istore 4
goto L2
L3:
iload 3
ireturn
.limit locals 6
.limit stack 5
.end method

.method public getDerivedPasswordVerifier()[B
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/derivedPasswordVerifier [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getFinalMac()[B
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/mac Lnet/lingala/zip4j/crypto/PBKDF2/MacBasedPRF;
invokevirtual net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/doFinal()[B
astore 1
bipush 10
newarray byte
astore 2
aload 1
iconst_0
aload 2
iconst_0
bipush 10
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method public getPasswordVeriifierLength()I
iconst_2
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSaltBytes()[B
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/saltBytes [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSaltLength()I
aload 0
getfield net/lingala/zip4j/crypto/AESEncrpyter/SALT_LENGTH I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setDerivedPasswordVerifier([B)V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/AESEncrpyter/derivedPasswordVerifier [B
return
.limit locals 2
.limit stack 2
.end method

.method public setSaltBytes([B)V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/AESEncrpyter/saltBytes [B
return
.limit locals 2
.limit stack 2
.end method
