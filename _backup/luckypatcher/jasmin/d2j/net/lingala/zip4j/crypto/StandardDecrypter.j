.bytecode 50.0
.class public synchronized net/lingala/zip4j/crypto/StandardDecrypter
.super java/lang/Object
.implements net/lingala/zip4j/crypto/IDecrypter

.field private 'crc' [B

.field private 'fileHeader' Lnet/lingala/zip4j/model/FileHeader;

.field private 'zipCryptoEngine' Lnet/lingala/zip4j/crypto/engine/ZipCryptoEngine;

.method public <init>(Lnet/lingala/zip4j/model/FileHeader;[B)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_4
newarray byte
putfield net/lingala/zip4j/crypto/StandardDecrypter/crc [B
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "one of more of the input parameters were null in StandardDecryptor"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield net/lingala/zip4j/crypto/StandardDecrypter/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
new net/lingala/zip4j/crypto/engine/ZipCryptoEngine
dup
invokespecial net/lingala/zip4j/crypto/engine/ZipCryptoEngine/<init>()V
putfield net/lingala/zip4j/crypto/StandardDecrypter/zipCryptoEngine Lnet/lingala/zip4j/crypto/engine/ZipCryptoEngine;
aload 0
aload 2
invokevirtual net/lingala/zip4j/crypto/StandardDecrypter/init([B)V
return
.limit locals 3
.limit stack 3
.end method

.method public decryptData([B)I
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual net/lingala/zip4j/crypto/StandardDecrypter/decryptData([BII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method public decryptData([BII)I
.throws net/lingala/zip4j/exception/ZipException
.catch java/lang/Exception from L0 to L1 using L2
iload 2
iflt L3
iload 3
ifge L4
L3:
new net/lingala/zip4j/exception/ZipException
dup
ldc "one of the input parameters were null in standard decrpyt data"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L4:
iload 2
istore 4
L5:
iload 4
iload 2
iload 3
iadd
if_icmpge L6
aload 1
iload 4
baload
istore 5
L0:
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/zipCryptoEngine Lnet/lingala/zip4j/crypto/engine/ZipCryptoEngine;
invokevirtual net/lingala/zip4j/crypto/engine/ZipCryptoEngine/decryptByte()B
iload 5
sipush 255
iand
ixor
sipush 255
iand
istore 5
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/zipCryptoEngine Lnet/lingala/zip4j/crypto/engine/ZipCryptoEngine;
iload 5
i2b
invokevirtual net/lingala/zip4j/crypto/engine/ZipCryptoEngine/updateKeys(B)V
L1:
aload 1
iload 4
iload 5
i2b
bastore
iload 4
iconst_1
iadd
istore 4
goto L5
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L6:
iload 3
ireturn
.limit locals 6
.limit stack 3
.end method

.method public init([B)V
.throws net/lingala/zip4j/exception/ZipException
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getCrcBuff()[B
astore 4
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/crc [B
iconst_3
aload 4
iconst_3
baload
sipush 255
iand
i2b
bastore
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/crc [B
iconst_2
aload 4
iconst_3
baload
bipush 8
ishr
sipush 255
iand
i2b
bastore
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/crc [B
iconst_1
aload 4
iconst_3
baload
bipush 16
ishr
sipush 255
iand
i2b
bastore
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/crc [B
iconst_0
aload 4
iconst_3
baload
bipush 24
ishr
sipush 255
iand
i2b
bastore
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/crc [B
iconst_2
baload
ifgt L3
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/crc [B
iconst_1
baload
ifgt L3
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/crc [B
iconst_0
baload
ifle L4
L3:
new java/lang/IllegalStateException
dup
ldc "Invalid CRC in File Header"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getPassword()[C
ifnull L5
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getPassword()[C
arraylength
ifgt L6
L5:
new net/lingala/zip4j/exception/ZipException
dup
ldc "Wrong password!"
iconst_5
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;I)V
athrow
L6:
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/zipCryptoEngine Lnet/lingala/zip4j/crypto/engine/ZipCryptoEngine;
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getPassword()[C
invokevirtual net/lingala/zip4j/crypto/engine/ZipCryptoEngine/initKeys([C)V
aload 1
iconst_0
baload
istore 3
iconst_0
istore 2
L7:
iload 2
bipush 12
if_icmpge L8
L0:
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/zipCryptoEngine Lnet/lingala/zip4j/crypto/engine/ZipCryptoEngine;
aload 0
getfield net/lingala/zip4j/crypto/StandardDecrypter/zipCryptoEngine Lnet/lingala/zip4j/crypto/engine/ZipCryptoEngine;
invokevirtual net/lingala/zip4j/crypto/engine/ZipCryptoEngine/decryptByte()B
iload 3
ixor
i2b
invokevirtual net/lingala/zip4j/crypto/engine/ZipCryptoEngine/updateKeys(B)V
L1:
iload 2
iconst_1
iadd
bipush 12
if_icmpeq L9
aload 1
iload 2
iconst_1
iadd
baload
istore 3
L9:
iload 2
iconst_1
iadd
istore 2
goto L7
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L8:
return
.limit locals 5
.limit stack 4
.end method
