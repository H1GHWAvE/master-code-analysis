.bytecode 50.0
.class synchronized net/lingala/zip4j/crypto/PBKDF2/PBKDF2HexFormatter
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public fromString(Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;Ljava/lang/String;)Z
aload 1
ifnull L0
aload 2
ifnonnull L1
L0:
iconst_1
ireturn
L1:
aload 2
ldc ":"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 4
aload 4
ifnull L2
aload 4
arraylength
iconst_3
if_icmpeq L3
L2:
iconst_1
ireturn
L3:
aload 4
iconst_0
aaload
invokestatic net/lingala/zip4j/crypto/PBKDF2/BinTools/hex2bin(Ljava/lang/String;)[B
astore 2
aload 4
iconst_1
aaload
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 3
aload 4
iconst_2
aaload
invokestatic net/lingala/zip4j/crypto/PBKDF2/BinTools/hex2bin(Ljava/lang/String;)[B
astore 4
aload 1
aload 2
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/setSalt([B)V
aload 1
iload 3
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/setIterationCount(I)V
aload 1
aload 4
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/setDerivedKey([B)V
iconst_0
ireturn
.limit locals 5
.limit stack 2
.end method

.method public toString(Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;)Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/getSalt()[B
invokestatic net/lingala/zip4j/crypto/PBKDF2/BinTools/bin2hex([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/getIterationCount()I
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/getDerivedKey()[B
invokestatic net/lingala/zip4j/crypto/PBKDF2/BinTools/bin2hex([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method
