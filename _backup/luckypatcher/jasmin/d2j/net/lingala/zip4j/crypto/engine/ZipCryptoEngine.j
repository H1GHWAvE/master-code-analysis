.bytecode 50.0
.class public synchronized net/lingala/zip4j/crypto/engine/ZipCryptoEngine
.super java/lang/Object

.field private static final 'CRC_TABLE' [I

.field private final 'keys' [I

.method static <clinit>()V
sipush 256
newarray int
putstatic net/lingala/zip4j/crypto/engine/ZipCryptoEngine/CRC_TABLE [I
iconst_0
istore 0
L0:
iload 0
sipush 256
if_icmpge L1
iload 0
istore 1
iconst_0
istore 2
L2:
iload 2
bipush 8
if_icmpge L3
iload 1
iconst_1
iand
iconst_1
if_icmpne L4
iload 1
iconst_1
iushr
ldc_w -306674912
ixor
istore 1
L5:
iload 2
iconst_1
iadd
istore 2
goto L2
L4:
iload 1
iconst_1
iushr
istore 1
goto L5
L3:
getstatic net/lingala/zip4j/crypto/engine/ZipCryptoEngine/CRC_TABLE [I
iload 0
iload 1
iastore
iload 0
iconst_1
iadd
istore 0
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_3
newarray int
putfield net/lingala/zip4j/crypto/engine/ZipCryptoEngine/keys [I
return
.limit locals 1
.limit stack 2
.end method

.method private crc32(IB)I
iload 1
bipush 8
iushr
getstatic net/lingala/zip4j/crypto/engine/ZipCryptoEngine/CRC_TABLE [I
iload 1
iload 2
ixor
sipush 255
iand
iaload
ixor
ireturn
.limit locals 3
.limit stack 4
.end method

.method public decryptByte()B
aload 0
getfield net/lingala/zip4j/crypto/engine/ZipCryptoEngine/keys [I
iconst_2
iaload
iconst_2
ior
istore 1
iload 1
iconst_1
ixor
iload 1
imul
bipush 8
iushr
i2b
ireturn
.limit locals 2
.limit stack 2
.end method

.method public initKeys([C)V
aload 0
getfield net/lingala/zip4j/crypto/engine/ZipCryptoEngine/keys [I
iconst_0
ldc_w 305419896
iastore
aload 0
getfield net/lingala/zip4j/crypto/engine/ZipCryptoEngine/keys [I
iconst_1
ldc_w 591751049
iastore
aload 0
getfield net/lingala/zip4j/crypto/engine/ZipCryptoEngine/keys [I
iconst_2
ldc_w 878082192
iastore
iconst_0
istore 2
L0:
iload 2
aload 1
arraylength
if_icmpge L1
aload 0
aload 1
iload 2
caload
sipush 255
iand
i2b
invokevirtual net/lingala/zip4j/crypto/engine/ZipCryptoEngine/updateKeys(B)V
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method

.method public updateKeys(B)V
aload 0
getfield net/lingala/zip4j/crypto/engine/ZipCryptoEngine/keys [I
iconst_0
aload 0
aload 0
getfield net/lingala/zip4j/crypto/engine/ZipCryptoEngine/keys [I
iconst_0
iaload
iload 1
invokespecial net/lingala/zip4j/crypto/engine/ZipCryptoEngine/crc32(IB)I
iastore
aload 0
getfield net/lingala/zip4j/crypto/engine/ZipCryptoEngine/keys [I
astore 2
aload 2
iconst_1
aload 2
iconst_1
iaload
aload 0
getfield net/lingala/zip4j/crypto/engine/ZipCryptoEngine/keys [I
iconst_0
iaload
sipush 255
iand
iadd
iastore
aload 0
getfield net/lingala/zip4j/crypto/engine/ZipCryptoEngine/keys [I
iconst_1
aload 0
getfield net/lingala/zip4j/crypto/engine/ZipCryptoEngine/keys [I
iconst_1
iaload
ldc_w 134775813
imul
iconst_1
iadd
iastore
aload 0
getfield net/lingala/zip4j/crypto/engine/ZipCryptoEngine/keys [I
iconst_2
aload 0
aload 0
getfield net/lingala/zip4j/crypto/engine/ZipCryptoEngine/keys [I
iconst_2
iaload
aload 0
getfield net/lingala/zip4j/crypto/engine/ZipCryptoEngine/keys [I
iconst_1
iaload
bipush 24
ishr
i2b
invokespecial net/lingala/zip4j/crypto/engine/ZipCryptoEngine/crc32(IB)I
iastore
return
.limit locals 3
.limit stack 6
.end method
