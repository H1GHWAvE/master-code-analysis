.bytecode 50.0
.class public synchronized net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine
.super java/lang/Object

.field protected 'parameters' Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;

.field protected 'prf' Lnet/lingala/zip4j/crypto/PBKDF2/PRF;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/parameters Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;
aload 0
aconst_null
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/prf Lnet/lingala/zip4j/crypto/PBKDF2/PRF;
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/parameters Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;
aload 0
aconst_null
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/prf Lnet/lingala/zip4j/crypto/PBKDF2/PRF;
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;Lnet/lingala/zip4j/crypto/PBKDF2/PRF;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/parameters Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;
aload 0
aload 2
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/prf Lnet/lingala/zip4j/crypto/PBKDF2/PRF;
return
.limit locals 3
.limit stack 2
.end method

.method protected INT([BII)V
aload 1
iload 2
iconst_0
iadd
iload 3
ldc_w 16777216
idiv
i2b
bastore
aload 1
iload 2
iconst_1
iadd
iload 3
ldc_w 65536
idiv
i2b
bastore
aload 1
iload 2
iconst_2
iadd
iload 3
sipush 256
idiv
i2b
bastore
aload 1
iload 2
iconst_3
iadd
iload 3
i2b
bastore
return
.limit locals 4
.limit stack 4
.end method

.method protected PBKDF2(Lnet/lingala/zip4j/crypto/PBKDF2/PRF;[BII)[B
aload 2
astore 9
aload 2
ifnonnull L0
iconst_0
newarray byte
astore 9
L0:
aload 1
invokeinterface net/lingala/zip4j/crypto/PBKDF2/PRF/getHLen()I 0
istore 7
aload 0
iload 4
iload 7
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/ceil(II)I
istore 8
iload 8
iload 7
imul
newarray byte
astore 2
iconst_0
istore 6
iconst_1
istore 5
L1:
iload 5
iload 8
if_icmpgt L2
aload 0
aload 2
iload 6
aload 1
aload 9
iload 3
iload 5
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/_F([BILnet/lingala/zip4j/crypto/PBKDF2/PRF;[BII)V
iload 6
iload 7
iadd
istore 6
iload 5
iconst_1
iadd
istore 5
goto L1
L2:
iload 4
iload 8
iconst_1
isub
iload 7
imul
isub
iload 7
if_icmpge L3
iload 4
newarray byte
astore 1
aload 2
iconst_0
aload 1
iconst_0
iload 4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
areturn
L3:
aload 2
areturn
.limit locals 10
.limit stack 7
.end method

.method protected _F([BILnet/lingala/zip4j/crypto/PBKDF2/PRF;[BII)V
aload 3
invokeinterface net/lingala/zip4j/crypto/PBKDF2/PRF/getHLen()I 0
istore 7
iload 7
newarray byte
astore 9
aload 4
arraylength
iconst_4
iadd
newarray byte
astore 8
aload 4
iconst_0
aload 8
iconst_0
aload 4
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 8
aload 4
arraylength
iload 6
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/INT([BII)V
iconst_0
istore 6
aload 8
astore 4
L0:
iload 6
iload 5
if_icmpge L1
aload 3
aload 4
invokeinterface net/lingala/zip4j/crypto/PBKDF2/PRF/doFinal([B)[B 1
astore 4
aload 0
aload 9
aload 4
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/xor([B[B)V
iload 6
iconst_1
iadd
istore 6
goto L0
L1:
aload 9
iconst_0
aload 1
iload 2
iload 7
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
return
.limit locals 10
.limit stack 5
.end method

.method protected assertPRF([B)V
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/prf Lnet/lingala/zip4j/crypto/PBKDF2/PRF;
ifnonnull L0
aload 0
new net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF
dup
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/parameters Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/getHashAlgorithm()Ljava/lang/String;
invokespecial net/lingala/zip4j/crypto/PBKDF2/MacBasedPRF/<init>(Ljava/lang/String;)V
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/prf Lnet/lingala/zip4j/crypto/PBKDF2/PRF;
L0:
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/prf Lnet/lingala/zip4j/crypto/PBKDF2/PRF;
aload 1
invokeinterface net/lingala/zip4j/crypto/PBKDF2/PRF/init([B)V 1
return
.limit locals 2
.limit stack 4
.end method

.method protected ceil(II)I
iconst_0
istore 3
iload 1
iload 2
irem
ifle L0
iconst_1
istore 3
L0:
iload 1
iload 2
idiv
iload 3
iadd
ireturn
.limit locals 4
.limit stack 2
.end method

.method public deriveKey([C)[B
aload 0
aload 1
iconst_0
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/deriveKey([CI)[B
areturn
.limit locals 2
.limit stack 3
.end method

.method public deriveKey([CI)[B
aload 1
ifnonnull L0
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L0:
aload 0
aload 1
invokestatic net/lingala/zip4j/util/Raw/convertCharArrayToByteArray([C)[B
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/assertPRF([B)V
iload 2
istore 3
iload 2
ifne L1
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/prf Lnet/lingala/zip4j/crypto/PBKDF2/PRF;
invokeinterface net/lingala/zip4j/crypto/PBKDF2/PRF/getHLen()I 0
istore 3
L1:
aload 0
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/prf Lnet/lingala/zip4j/crypto/PBKDF2/PRF;
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/parameters Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/getSalt()[B
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/parameters Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/getIterationCount()I
iload 3
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/PBKDF2(Lnet/lingala/zip4j/crypto/PBKDF2/PRF;[BII)[B
areturn
.limit locals 4
.limit stack 5
.end method

.method public getParameters()Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/parameters Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getPseudoRandomFunction()Lnet/lingala/zip4j/crypto/PBKDF2/PRF;
aload 0
getfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/prf Lnet/lingala/zip4j/crypto/PBKDF2/PRF;
areturn
.limit locals 1
.limit stack 1
.end method

.method public setParameters(Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;)V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/parameters Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;
return
.limit locals 2
.limit stack 2
.end method

.method public setPseudoRandomFunction(Lnet/lingala/zip4j/crypto/PBKDF2/PRF;)V
aload 0
aload 1
putfield net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/prf Lnet/lingala/zip4j/crypto/PBKDF2/PRF;
return
.limit locals 2
.limit stack 2
.end method

.method public verifyKey([C)Z
aload 0
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/getParameters()Lnet/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters;
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Parameters/getDerivedKey()[B
astore 3
aload 3
ifnull L0
aload 3
arraylength
ifne L1
L0:
iconst_0
ireturn
L1:
aload 0
aload 1
aload 3
arraylength
invokevirtual net/lingala/zip4j/crypto/PBKDF2/PBKDF2Engine/deriveKey([CI)[B
astore 1
aload 1
ifnull L0
aload 1
arraylength
aload 3
arraylength
if_icmpne L0
iconst_0
istore 2
L2:
iload 2
aload 1
arraylength
if_icmpge L3
aload 1
iload 2
baload
aload 3
iload 2
baload
if_icmpne L0
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
iconst_1
ireturn
.limit locals 4
.limit stack 3
.end method

.method protected xor([B[B)V
iconst_0
istore 3
L0:
iload 3
aload 1
arraylength
if_icmpge L1
aload 1
iload 3
aload 1
iload 3
baload
aload 2
iload 3
baload
ixor
i2b
bastore
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
return
.limit locals 4
.limit stack 5
.end method
