.bytecode 50.0
.class public synchronized net/lingala/zip4j/crypto/engine/AESEngine
.super java/lang/Object

.field private static final 'S' [B

.field private static final 'T0' [I

.field private static final 'rcon' [I

.field private 'C0' I

.field private 'C1' I

.field private 'C2' I

.field private 'C3' I

.field private 'rounds' I

.field private 'workingKey' [[I

.method static <clinit>()V
sipush 256
newarray byte
dup
iconst_0
ldc_w 99
bastore
dup
iconst_1
ldc_w 124
bastore
dup
iconst_2
ldc_w 119
bastore
dup
iconst_3
ldc_w 123
bastore
dup
iconst_4
ldc_w -14
bastore
dup
iconst_5
ldc_w 107
bastore
dup
bipush 6
ldc_w 111
bastore
dup
bipush 7
ldc_w -59
bastore
dup
bipush 8
ldc_w 48
bastore
dup
bipush 9
ldc_w 1
bastore
dup
bipush 10
ldc_w 103
bastore
dup
bipush 11
ldc_w 43
bastore
dup
bipush 12
ldc_w -2
bastore
dup
bipush 13
ldc_w -41
bastore
dup
bipush 14
ldc_w -85
bastore
dup
bipush 15
ldc_w 118
bastore
dup
bipush 16
ldc_w -54
bastore
dup
bipush 17
ldc_w -126
bastore
dup
bipush 18
ldc_w -55
bastore
dup
bipush 19
ldc_w 125
bastore
dup
bipush 20
ldc_w -6
bastore
dup
bipush 21
ldc_w 89
bastore
dup
bipush 22
ldc_w 71
bastore
dup
bipush 23
ldc_w -16
bastore
dup
bipush 24
ldc_w -83
bastore
dup
bipush 25
ldc_w -44
bastore
dup
bipush 26
ldc_w -94
bastore
dup
bipush 27
ldc_w -81
bastore
dup
bipush 28
ldc_w -100
bastore
dup
bipush 29
ldc_w -92
bastore
dup
bipush 30
ldc_w 114
bastore
dup
bipush 31
ldc_w -64
bastore
dup
bipush 32
ldc_w -73
bastore
dup
bipush 33
ldc_w -3
bastore
dup
bipush 34
ldc_w -109
bastore
dup
bipush 35
ldc_w 38
bastore
dup
bipush 36
ldc_w 54
bastore
dup
bipush 37
ldc_w 63
bastore
dup
bipush 38
ldc_w -9
bastore
dup
bipush 39
ldc_w -52
bastore
dup
bipush 40
ldc_w 52
bastore
dup
bipush 41
ldc_w -91
bastore
dup
bipush 42
ldc_w -27
bastore
dup
bipush 43
ldc_w -15
bastore
dup
bipush 44
ldc_w 113
bastore
dup
bipush 45
ldc_w -40
bastore
dup
bipush 46
ldc_w 49
bastore
dup
bipush 47
ldc_w 21
bastore
dup
bipush 48
ldc_w 4
bastore
dup
bipush 49
ldc_w -57
bastore
dup
bipush 50
ldc_w 35
bastore
dup
bipush 51
ldc_w -61
bastore
dup
bipush 52
ldc_w 24
bastore
dup
bipush 53
ldc_w -106
bastore
dup
bipush 54
ldc_w 5
bastore
dup
bipush 55
ldc_w -102
bastore
dup
bipush 56
ldc_w 7
bastore
dup
bipush 57
ldc_w 18
bastore
dup
bipush 58
ldc_w -128
bastore
dup
bipush 59
ldc_w -30
bastore
dup
bipush 60
ldc_w -21
bastore
dup
bipush 61
ldc_w 39
bastore
dup
bipush 62
ldc_w -78
bastore
dup
bipush 63
ldc_w 117
bastore
dup
bipush 64
ldc_w 9
bastore
dup
bipush 65
ldc_w -125
bastore
dup
bipush 66
ldc_w 44
bastore
dup
bipush 67
ldc_w 26
bastore
dup
bipush 68
ldc_w 27
bastore
dup
bipush 69
ldc_w 110
bastore
dup
bipush 70
ldc_w 90
bastore
dup
bipush 71
ldc_w -96
bastore
dup
bipush 72
ldc_w 82
bastore
dup
bipush 73
ldc_w 59
bastore
dup
bipush 74
ldc_w -42
bastore
dup
bipush 75
ldc_w -77
bastore
dup
bipush 76
ldc_w 41
bastore
dup
bipush 77
ldc_w -29
bastore
dup
bipush 78
ldc_w 47
bastore
dup
bipush 79
ldc_w -124
bastore
dup
bipush 80
ldc_w 83
bastore
dup
bipush 81
ldc_w -47
bastore
dup
bipush 82
ldc_w 0
bastore
dup
bipush 83
ldc_w -19
bastore
dup
bipush 84
ldc_w 32
bastore
dup
bipush 85
ldc_w -4
bastore
dup
bipush 86
ldc_w -79
bastore
dup
bipush 87
ldc_w 91
bastore
dup
bipush 88
ldc_w 106
bastore
dup
bipush 89
ldc_w -53
bastore
dup
bipush 90
ldc_w -66
bastore
dup
bipush 91
ldc_w 57
bastore
dup
bipush 92
ldc_w 74
bastore
dup
bipush 93
ldc_w 76
bastore
dup
bipush 94
ldc_w 88
bastore
dup
bipush 95
ldc_w -49
bastore
dup
bipush 96
ldc_w -48
bastore
dup
bipush 97
ldc_w -17
bastore
dup
bipush 98
ldc_w -86
bastore
dup
bipush 99
ldc_w -5
bastore
dup
bipush 100
ldc_w 67
bastore
dup
bipush 101
ldc_w 77
bastore
dup
bipush 102
ldc_w 51
bastore
dup
bipush 103
ldc_w -123
bastore
dup
bipush 104
ldc_w 69
bastore
dup
bipush 105
ldc_w -7
bastore
dup
bipush 106
ldc_w 2
bastore
dup
bipush 107
ldc_w 127
bastore
dup
bipush 108
ldc_w 80
bastore
dup
bipush 109
ldc_w 60
bastore
dup
bipush 110
ldc_w -97
bastore
dup
bipush 111
ldc_w -88
bastore
dup
bipush 112
ldc_w 81
bastore
dup
bipush 113
ldc_w -93
bastore
dup
bipush 114
ldc_w 64
bastore
dup
bipush 115
ldc_w -113
bastore
dup
bipush 116
ldc_w -110
bastore
dup
bipush 117
ldc_w -99
bastore
dup
bipush 118
ldc_w 56
bastore
dup
bipush 119
ldc_w -11
bastore
dup
bipush 120
ldc_w -68
bastore
dup
bipush 121
ldc_w -74
bastore
dup
bipush 122
ldc_w -38
bastore
dup
bipush 123
ldc_w 33
bastore
dup
bipush 124
ldc_w 16
bastore
dup
bipush 125
ldc_w -1
bastore
dup
bipush 126
ldc_w -13
bastore
dup
bipush 127
ldc_w -46
bastore
dup
sipush 128
ldc_w -51
bastore
dup
sipush 129
ldc_w 12
bastore
dup
sipush 130
ldc_w 19
bastore
dup
sipush 131
ldc_w -20
bastore
dup
sipush 132
ldc_w 95
bastore
dup
sipush 133
ldc_w -105
bastore
dup
sipush 134
ldc_w 68
bastore
dup
sipush 135
ldc_w 23
bastore
dup
sipush 136
ldc_w -60
bastore
dup
sipush 137
ldc_w -89
bastore
dup
sipush 138
ldc_w 126
bastore
dup
sipush 139
ldc_w 61
bastore
dup
sipush 140
ldc_w 100
bastore
dup
sipush 141
ldc_w 93
bastore
dup
sipush 142
ldc_w 25
bastore
dup
sipush 143
ldc_w 115
bastore
dup
sipush 144
ldc_w 96
bastore
dup
sipush 145
ldc_w -127
bastore
dup
sipush 146
ldc_w 79
bastore
dup
sipush 147
ldc_w -36
bastore
dup
sipush 148
ldc_w 34
bastore
dup
sipush 149
ldc_w 42
bastore
dup
sipush 150
ldc_w -112
bastore
dup
sipush 151
ldc_w -120
bastore
dup
sipush 152
ldc_w 70
bastore
dup
sipush 153
ldc_w -18
bastore
dup
sipush 154
ldc_w -72
bastore
dup
sipush 155
ldc_w 20
bastore
dup
sipush 156
ldc_w -34
bastore
dup
sipush 157
ldc_w 94
bastore
dup
sipush 158
ldc_w 11
bastore
dup
sipush 159
ldc_w -37
bastore
dup
sipush 160
ldc_w -32
bastore
dup
sipush 161
ldc_w 50
bastore
dup
sipush 162
ldc_w 58
bastore
dup
sipush 163
ldc_w 10
bastore
dup
sipush 164
ldc_w 73
bastore
dup
sipush 165
ldc_w 6
bastore
dup
sipush 166
ldc_w 36
bastore
dup
sipush 167
ldc_w 92
bastore
dup
sipush 168
ldc_w -62
bastore
dup
sipush 169
ldc_w -45
bastore
dup
sipush 170
ldc_w -84
bastore
dup
sipush 171
ldc_w 98
bastore
dup
sipush 172
ldc_w -111
bastore
dup
sipush 173
ldc_w -107
bastore
dup
sipush 174
ldc_w -28
bastore
dup
sipush 175
ldc_w 121
bastore
dup
sipush 176
ldc_w -25
bastore
dup
sipush 177
ldc_w -56
bastore
dup
sipush 178
ldc_w 55
bastore
dup
sipush 179
ldc_w 109
bastore
dup
sipush 180
ldc_w -115
bastore
dup
sipush 181
ldc_w -43
bastore
dup
sipush 182
ldc_w 78
bastore
dup
sipush 183
ldc_w -87
bastore
dup
sipush 184
ldc_w 108
bastore
dup
sipush 185
ldc_w 86
bastore
dup
sipush 186
ldc_w -12
bastore
dup
sipush 187
ldc_w -22
bastore
dup
sipush 188
ldc_w 101
bastore
dup
sipush 189
ldc_w 122
bastore
dup
sipush 190
ldc_w -82
bastore
dup
sipush 191
ldc_w 8
bastore
dup
sipush 192
ldc_w -70
bastore
dup
sipush 193
ldc_w 120
bastore
dup
sipush 194
ldc_w 37
bastore
dup
sipush 195
ldc_w 46
bastore
dup
sipush 196
ldc_w 28
bastore
dup
sipush 197
ldc_w -90
bastore
dup
sipush 198
ldc_w -76
bastore
dup
sipush 199
ldc_w -58
bastore
dup
sipush 200
ldc_w -24
bastore
dup
sipush 201
ldc_w -35
bastore
dup
sipush 202
ldc_w 116
bastore
dup
sipush 203
ldc_w 31
bastore
dup
sipush 204
ldc_w 75
bastore
dup
sipush 205
ldc_w -67
bastore
dup
sipush 206
ldc_w -117
bastore
dup
sipush 207
ldc_w -118
bastore
dup
sipush 208
ldc_w 112
bastore
dup
sipush 209
ldc_w 62
bastore
dup
sipush 210
ldc_w -75
bastore
dup
sipush 211
ldc_w 102
bastore
dup
sipush 212
ldc_w 72
bastore
dup
sipush 213
ldc_w 3
bastore
dup
sipush 214
ldc_w -10
bastore
dup
sipush 215
ldc_w 14
bastore
dup
sipush 216
ldc_w 97
bastore
dup
sipush 217
ldc_w 53
bastore
dup
sipush 218
ldc_w 87
bastore
dup
sipush 219
ldc_w -71
bastore
dup
sipush 220
ldc_w -122
bastore
dup
sipush 221
ldc_w -63
bastore
dup
sipush 222
ldc_w 29
bastore
dup
sipush 223
ldc_w -98
bastore
dup
sipush 224
ldc_w -31
bastore
dup
sipush 225
ldc_w -8
bastore
dup
sipush 226
ldc_w -104
bastore
dup
sipush 227
ldc_w 17
bastore
dup
sipush 228
ldc_w 105
bastore
dup
sipush 229
ldc_w -39
bastore
dup
sipush 230
ldc_w -114
bastore
dup
sipush 231
ldc_w -108
bastore
dup
sipush 232
ldc_w -101
bastore
dup
sipush 233
ldc_w 30
bastore
dup
sipush 234
ldc_w -121
bastore
dup
sipush 235
ldc_w -23
bastore
dup
sipush 236
ldc_w -50
bastore
dup
sipush 237
ldc_w 85
bastore
dup
sipush 238
ldc_w 40
bastore
dup
sipush 239
ldc_w -33
bastore
dup
sipush 240
ldc_w -116
bastore
dup
sipush 241
ldc_w -95
bastore
dup
sipush 242
ldc_w -119
bastore
dup
sipush 243
ldc_w 13
bastore
dup
sipush 244
ldc_w -65
bastore
dup
sipush 245
ldc_w -26
bastore
dup
sipush 246
ldc_w 66
bastore
dup
sipush 247
ldc_w 104
bastore
dup
sipush 248
ldc_w 65
bastore
dup
sipush 249
ldc_w -103
bastore
dup
sipush 250
ldc_w 45
bastore
dup
sipush 251
ldc_w 15
bastore
dup
sipush 252
ldc_w -80
bastore
dup
sipush 253
ldc_w 84
bastore
dup
sipush 254
ldc_w -69
bastore
dup
sipush 255
ldc_w 22
bastore
putstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
bipush 30
newarray int
dup
iconst_0
iconst_1
iastore
dup
iconst_1
iconst_2
iastore
dup
iconst_2
iconst_4
iastore
dup
iconst_3
bipush 8
iastore
dup
iconst_4
bipush 16
iastore
dup
iconst_5
bipush 32
iastore
dup
bipush 6
bipush 64
iastore
dup
bipush 7
sipush 128
iastore
dup
bipush 8
bipush 27
iastore
dup
bipush 9
bipush 54
iastore
dup
bipush 10
bipush 108
iastore
dup
bipush 11
sipush 216
iastore
dup
bipush 12
sipush 171
iastore
dup
bipush 13
bipush 77
iastore
dup
bipush 14
sipush 154
iastore
dup
bipush 15
bipush 47
iastore
dup
bipush 16
bipush 94
iastore
dup
bipush 17
sipush 188
iastore
dup
bipush 18
bipush 99
iastore
dup
bipush 19
sipush 198
iastore
dup
bipush 20
sipush 151
iastore
dup
bipush 21
bipush 53
iastore
dup
bipush 22
bipush 106
iastore
dup
bipush 23
sipush 212
iastore
dup
bipush 24
sipush 179
iastore
dup
bipush 25
bipush 125
iastore
dup
bipush 26
sipush 250
iastore
dup
bipush 27
sipush 239
iastore
dup
bipush 28
sipush 197
iastore
dup
bipush 29
sipush 145
iastore
putstatic net/lingala/zip4j/crypto/engine/AESEngine/rcon [I
sipush 256
newarray int
dup
iconst_0
ldc_w -1520213050
iastore
dup
iconst_1
ldc_w -2072216328
iastore
dup
iconst_2
ldc_w -1720223762
iastore
dup
iconst_3
ldc_w -1921287178
iastore
dup
iconst_4
ldc_w 234025727
iastore
dup
iconst_5
ldc_w -1117033514
iastore
dup
bipush 6
ldc_w -1318096930
iastore
dup
bipush 7
ldc_w 1422247313
iastore
dup
bipush 8
ldc_w 1345335392
iastore
dup
bipush 9
ldc_w 50397442
iastore
dup
bipush 10
ldc_w -1452841010
iastore
dup
bipush 11
ldc_w 2099981142
iastore
dup
bipush 12
ldc_w 436141799
iastore
dup
bipush 13
ldc_w 1658312629
iastore
dup
bipush 14
ldc_w -424957107
iastore
dup
bipush 15
ldc_w -1703512340
iastore
dup
bipush 16
ldc_w 1170918031
iastore
dup
bipush 17
ldc_w -1652391393
iastore
dup
bipush 18
ldc_w 1086966153
iastore
dup
bipush 19
ldc_w -2021818886
iastore
dup
bipush 20
ldc_w 368769775
iastore
dup
bipush 21
ldc_w -346465870
iastore
dup
bipush 22
ldc_w -918075506
iastore
dup
bipush 23
ldc_w 200339707
iastore
dup
bipush 24
ldc_w -324162239
iastore
dup
bipush 25
ldc_w 1742001331
iastore
dup
bipush 26
ldc_w -39673249
iastore
dup
bipush 27
ldc_w -357585083
iastore
dup
bipush 28
ldc_w -1080255453
iastore
dup
bipush 29
ldc_w -140204973
iastore
dup
bipush 30
ldc_w -1770884380
iastore
dup
bipush 31
ldc_w 1539358875
iastore
dup
bipush 32
ldc_w -1028147339
iastore
dup
bipush 33
ldc_w 486407649
iastore
dup
bipush 34
ldc_w -1366060227
iastore
dup
bipush 35
ldc_w 1780885068
iastore
dup
bipush 36
ldc_w 1513502316
iastore
dup
bipush 37
ldc_w 1094664062
iastore
dup
bipush 38
ldc_w 49805301
iastore
dup
bipush 39
ldc_w 1338821763
iastore
dup
bipush 40
ldc_w 1546925160
iastore
dup
bipush 41
ldc_w -190470831
iastore
dup
bipush 42
ldc_w 887481809
iastore
dup
bipush 43
ldc_w 150073849
iastore
dup
bipush 44
ldc_w -1821281822
iastore
dup
bipush 45
ldc_w 1943591083
iastore
dup
bipush 46
ldc_w 1395732834
iastore
dup
bipush 47
ldc_w 1058346282
iastore
dup
bipush 48
ldc_w 201589768
iastore
dup
bipush 49
ldc_w 1388824469
iastore
dup
bipush 50
ldc_w 1696801606
iastore
dup
bipush 51
ldc_w 1589887901
iastore
dup
bipush 52
ldc_w 672667696
iastore
dup
bipush 53
ldc_w -1583966665
iastore
dup
bipush 54
ldc_w 251987210
iastore
dup
bipush 55
ldc_w -1248159185
iastore
dup
bipush 56
ldc_w 151455502
iastore
dup
bipush 57
ldc_w 907153956
iastore
dup
bipush 58
ldc_w -1686077413
iastore
dup
bipush 59
ldc_w 1038279391
iastore
dup
bipush 60
ldc_w 652995533
iastore
dup
bipush 61
ldc_w 1764173646
iastore
dup
bipush 62
ldc_w -843926913
iastore
dup
bipush 63
ldc_w -1619692054
iastore
dup
bipush 64
ldc_w 453576978
iastore
dup
bipush 65
ldc_w -1635548387
iastore
dup
bipush 66
ldc_w 1949051992
iastore
dup
bipush 67
ldc_w 773462580
iastore
dup
bipush 68
ldc_w 756751158
iastore
dup
bipush 69
ldc_w -1301385508
iastore
dup
bipush 70
ldc_w -296068428
iastore
dup
bipush 71
ldc_w -73359269
iastore
dup
bipush 72
ldc_w -162377052
iastore
dup
bipush 73
ldc_w 1295727478
iastore
dup
bipush 74
ldc_w 1641469623
iastore
dup
bipush 75
ldc_w -827083907
iastore
dup
bipush 76
ldc_w 2066295122
iastore
dup
bipush 77
ldc_w 1055122397
iastore
dup
bipush 78
ldc_w 1898917726
iastore
dup
bipush 79
ldc_w -1752923117
iastore
dup
bipush 80
ldc_w -179088474
iastore
dup
bipush 81
ldc_w 1758581177
iastore
dup
bipush 82
iconst_0
iastore
dup
bipush 83
ldc_w 753790401
iastore
dup
bipush 84
ldc_w 1612718144
iastore
dup
bipush 85
ldc_w 536673507
iastore
dup
bipush 86
ldc_w -927878791
iastore
dup
bipush 87
ldc_w -312779850
iastore
dup
bipush 88
ldc_w -1100322092
iastore
dup
bipush 89
ldc_w 1187761037
iastore
dup
bipush 90
ldc_w -641810841
iastore
dup
bipush 91
ldc_w 1262041458
iastore
dup
bipush 92
ldc_w -565556588
iastore
dup
bipush 93
ldc_w -733197160
iastore
dup
bipush 94
ldc_w -396863312
iastore
dup
bipush 95
ldc_w 1255133061
iastore
dup
bipush 96
ldc_w 1808847035
iastore
dup
bipush 97
ldc_w 720367557
iastore
dup
bipush 98
ldc_w -441800113
iastore
dup
bipush 99
ldc_w 385612781
iastore
dup
bipush 100
ldc_w -985447546
iastore
dup
bipush 101
ldc_w -682799718
iastore
dup
bipush 102
ldc_w 1429418854
iastore
dup
bipush 103
ldc_w -1803188975
iastore
dup
bipush 104
ldc_w -817543798
iastore
dup
bipush 105
ldc_w 284817897
iastore
dup
bipush 106
ldc_w 100794884
iastore
dup
bipush 107
ldc_w -2122350594
iastore
dup
bipush 108
ldc_w -263171936
iastore
dup
bipush 109
ldc_w 1144798328
iastore
dup
bipush 110
ldc_w -1163944155
iastore
dup
bipush 111
ldc_w -475486133
iastore
dup
bipush 112
ldc_w -212774494
iastore
dup
bipush 113
ldc_w -22830243
iastore
dup
bipush 114
ldc_w -1069531008
iastore
dup
bipush 115
ldc_w -1970303227
iastore
dup
bipush 116
ldc_w -1382903233
iastore
dup
bipush 117
ldc_w -1130521311
iastore
dup
bipush 118
ldc_w 1211644016
iastore
dup
bipush 119
ldc_w 83228145
iastore
dup
bipush 120
ldc_w -541279133
iastore
dup
bipush 121
ldc_w -1044990345
iastore
dup
bipush 122
ldc_w 1977277103
iastore
dup
bipush 123
ldc_w 1663115586
iastore
dup
bipush 124
ldc_w 806359072
iastore
dup
bipush 125
ldc_w 452984805
iastore
dup
bipush 126
ldc_w 250868733
iastore
dup
bipush 127
ldc_w 1842533055
iastore
dup
sipush 128
ldc_w 1288555905
iastore
dup
sipush 129
ldc_w 336333848
iastore
dup
sipush 130
ldc_w 890442534
iastore
dup
sipush 131
ldc_w 804056259
iastore
dup
sipush 132
ldc_w -513843266
iastore
dup
sipush 133
ldc_w -1567123659
iastore
dup
sipush 134
ldc_w -867941240
iastore
dup
sipush 135
ldc_w 957814574
iastore
dup
sipush 136
ldc_w 1472513171
iastore
dup
sipush 137
ldc_w -223893675
iastore
dup
sipush 138
ldc_w -2105639172
iastore
dup
sipush 139
ldc_w 1195195770
iastore
dup
sipush 140
ldc_w -1402706744
iastore
dup
sipush 141
ldc_w -413311558
iastore
dup
sipush 142
ldc_w 723065138
iastore
dup
sipush 143
ldc_w -1787595802
iastore
dup
sipush 144
ldc_w -1604296512
iastore
dup
sipush 145
ldc_w -1736343271
iastore
dup
sipush 146
ldc_w -783331426
iastore
dup
sipush 147
ldc_w 2145180835
iastore
dup
sipush 148
ldc_w 1713513028
iastore
dup
sipush 149
ldc_w 2116692564
iastore
dup
sipush 150
ldc_w -1416589253
iastore
dup
sipush 151
ldc_w -2088204277
iastore
dup
sipush 152
ldc_w -901364084
iastore
dup
sipush 153
ldc_w 703524551
iastore
dup
sipush 154
ldc_w -742868885
iastore
dup
sipush 155
ldc_w 1007948840
iastore
dup
sipush 156
ldc_w 2044649127
iastore
dup
sipush 157
ldc_w -497131844
iastore
dup
sipush 158
ldc_w 487262998
iastore
dup
sipush 159
ldc_w 1994120109
iastore
dup
sipush 160
ldc_w 1004593371
iastore
dup
sipush 161
ldc_w 1446130276
iastore
dup
sipush 162
ldc_w 1312438900
iastore
dup
sipush 163
ldc_w 503974420
iastore
dup
sipush 164
ldc_w -615954030
iastore
dup
sipush 165
ldc_w 168166924
iastore
dup
sipush 166
ldc_w 1814307912
iastore
dup
sipush 167
ldc_w -463709000
iastore
dup
sipush 168
ldc_w 1573044895
iastore
dup
sipush 169
ldc_w 1859376061
iastore
dup
sipush 170
ldc_w -273896381
iastore
dup
sipush 171
ldc_w -1503501628
iastore
dup
sipush 172
ldc_w -1466855111
iastore
dup
sipush 173
ldc_w -1533700815
iastore
dup
sipush 174
ldc_w 937747667
iastore
dup
sipush 175
ldc_w -1954973198
iastore
dup
sipush 176
ldc_w 854058965
iastore
dup
sipush 177
ldc_w 1137232011
iastore
dup
sipush 178
ldc_w 1496790894
iastore
dup
sipush 179
ldc_w -1217565222
iastore
dup
sipush 180
ldc_w -1936880383
iastore
dup
sipush 181
ldc_w 1691735473
iastore
dup
sipush 182
ldc_w -766620004
iastore
dup
sipush 183
ldc_w -525751991
iastore
dup
sipush 184
ldc_w -1267962664
iastore
dup
sipush 185
ldc_w -95005012
iastore
dup
sipush 186
ldc_w 133494003
iastore
dup
sipush 187
ldc_w 636152527
iastore
dup
sipush 188
ldc_w -1352309302
iastore
dup
sipush 189
ldc_w -1904575756
iastore
dup
sipush 190
ldc_w -374428089
iastore
dup
sipush 191
ldc_w 403179536
iastore
dup
sipush 192
ldc_w -709182865
iastore
dup
sipush 193
ldc_w -2005370640
iastore
dup
sipush 194
ldc_w 1864705354
iastore
dup
sipush 195
ldc_w 1915629148
iastore
dup
sipush 196
ldc_w 605822008
iastore
dup
sipush 197
ldc_w -240736681
iastore
dup
sipush 198
ldc_w -944458637
iastore
dup
sipush 199
ldc_w 1371981463
iastore
dup
sipush 200
ldc_w 602466507
iastore
dup
sipush 201
ldc_w 2094914977
iastore
dup
sipush 202
ldc_w -1670089496
iastore
dup
sipush 203
ldc_w 555687742
iastore
dup
sipush 204
ldc_w -582268010
iastore
dup
sipush 205
ldc_w -591544991
iastore
dup
sipush 206
ldc_w -2037675251
iastore
dup
sipush 207
ldc_w -2054518257
iastore
dup
sipush 208
ldc_w -1871679264
iastore
dup
sipush 209
ldc_w 1111375484
iastore
dup
sipush 210
ldc_w -994724495
iastore
dup
sipush 211
ldc_w -1436129588
iastore
dup
sipush 212
ldc_w -666351472
iastore
dup
sipush 213
ldc_w 84083462
iastore
dup
sipush 214
ldc_w 32962295
iastore
dup
sipush 215
ldc_w 302911004
iastore
dup
sipush 216
ldc_w -1553899070
iastore
dup
sipush 217
ldc_w 1597322602
iastore
dup
sipush 218
ldc_w -111716434
iastore
dup
sipush 219
ldc_w -793134743
iastore
dup
sipush 220
ldc_w -1853454825
iastore
dup
sipush 221
ldc_w 1489093017
iastore
dup
sipush 222
ldc_w 656219450
iastore
dup
sipush 223
ldc_w -1180787161
iastore
dup
sipush 224
ldc_w 954327513
iastore
dup
sipush 225
ldc_w 335083755
iastore
dup
sipush 226
ldc_w -1281845205
iastore
dup
sipush 227
ldc_w 856756514
iastore
dup
sipush 228
ldc_w -1150719534
iastore
dup
sipush 229
ldc_w 1893325225
iastore
dup
sipush 230
ldc_w -1987146233
iastore
dup
sipush 231
ldc_w -1483434957
iastore
dup
sipush 232
ldc_w -1231316179
iastore
dup
sipush 233
ldc_w 572399164
iastore
dup
sipush 234
ldc_w -1836611819
iastore
dup
sipush 235
ldc_w 552200649
iastore
dup
sipush 236
ldc_w 1238290055
iastore
dup
sipush 237
ldc_w -11184726
iastore
dup
sipush 238
ldc_w 2015897680
iastore
dup
sipush 239
ldc_w 2061492133
iastore
dup
sipush 240
ldc_w -1886614525
iastore
dup
sipush 241
ldc_w -123625127
iastore
dup
sipush 242
ldc_w -2138470135
iastore
dup
sipush 243
ldc_w 386731290
iastore
dup
sipush 244
ldc_w -624967835
iastore
dup
sipush 245
ldc_w 837215959
iastore
dup
sipush 246
ldc_w -968736124
iastore
dup
sipush 247
ldc_w -1201116976
iastore
dup
sipush 248
ldc_w -1019133566
iastore
dup
sipush 249
ldc_w -1332111063
iastore
dup
sipush 250
ldc_w 1999449434
iastore
dup
sipush 251
ldc_w 286199582
iastore
dup
sipush 252
ldc_w -877612933
iastore
dup
sipush 253
ldc_w -61582168
iastore
dup
sipush 254
ldc_w -692339859
iastore
dup
sipush 255
ldc_w 974525996
iastore
putstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
return
.limit locals 0
.limit stack 4
.end method

.method public <init>([B)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
checkcast [[I
putfield net/lingala/zip4j/crypto/engine/AESEngine/workingKey [[I
aload 0
aload 1
invokevirtual net/lingala/zip4j/crypto/engine/AESEngine/init([B)V
return
.limit locals 2
.limit stack 2
.end method

.method private final encryptBlock([[I)V
aload 0
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
aload 1
iconst_0
aaload
iconst_0
iaload
ixor
putfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
aload 0
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
aload 1
iconst_0
aaload
iconst_1
iaload
ixor
putfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
aload 0
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
aload 1
iconst_0
aaload
iconst_2
iaload
ixor
putfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
aload 0
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
aload 1
iconst_0
aaload
iconst_3
iaload
ixor
putfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
iconst_1
istore 2
L0:
iload 2
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/rounds I
iconst_1
isub
if_icmpge L1
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
sipush 255
iand
iaload
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
bipush 8
ishr
sipush 255
iand
iaload
bipush 24
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
bipush 16
ishr
sipush 255
iand
iaload
bipush 16
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
bipush 24
ishr
sipush 255
iand
iaload
bipush 8
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 1
iload 2
aaload
iconst_0
iaload
ixor
istore 6
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
sipush 255
iand
iaload
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
bipush 8
ishr
sipush 255
iand
iaload
bipush 24
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
bipush 16
ishr
sipush 255
iand
iaload
bipush 16
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
bipush 24
ishr
sipush 255
iand
iaload
bipush 8
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 1
iload 2
aaload
iconst_1
iaload
ixor
istore 5
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
sipush 255
iand
iaload
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
bipush 8
ishr
sipush 255
iand
iaload
bipush 24
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
bipush 16
ishr
sipush 255
iand
iaload
bipush 16
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
bipush 24
ishr
sipush 255
iand
iaload
bipush 8
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 1
iload 2
aaload
iconst_2
iaload
ixor
istore 4
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
sipush 255
iand
iaload
istore 7
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
bipush 8
ishr
sipush 255
iand
iaload
bipush 24
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
istore 8
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
bipush 16
ishr
sipush 255
iand
iaload
bipush 16
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
istore 9
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
bipush 24
ishr
sipush 255
iand
iaload
bipush 8
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
istore 10
iload 2
iconst_1
iadd
istore 3
iload 7
iload 8
ixor
iload 9
ixor
iload 10
ixor
aload 1
iload 2
aaload
iconst_3
iaload
ixor
istore 2
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 6
sipush 255
iand
iaload
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 5
bipush 8
ishr
sipush 255
iand
iaload
bipush 24
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 4
bipush 16
ishr
sipush 255
iand
iaload
bipush 16
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 2
bipush 24
ishr
sipush 255
iand
iaload
bipush 8
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 1
iload 3
aaload
iconst_0
iaload
ixor
putfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 5
sipush 255
iand
iaload
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 4
bipush 8
ishr
sipush 255
iand
iaload
bipush 24
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 2
bipush 16
ishr
sipush 255
iand
iaload
bipush 16
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 6
bipush 24
ishr
sipush 255
iand
iaload
bipush 8
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 1
iload 3
aaload
iconst_1
iaload
ixor
putfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 4
sipush 255
iand
iaload
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 2
bipush 8
ishr
sipush 255
iand
iaload
bipush 24
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 6
bipush 16
ishr
sipush 255
iand
iaload
bipush 16
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 5
bipush 24
ishr
sipush 255
iand
iaload
bipush 8
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 1
iload 3
aaload
iconst_2
iaload
ixor
putfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 2
sipush 255
iand
iaload
istore 7
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 6
bipush 8
ishr
sipush 255
iand
iaload
bipush 24
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
istore 6
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 5
bipush 16
ishr
sipush 255
iand
iaload
bipush 16
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
istore 5
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
iload 4
bipush 24
ishr
sipush 255
iand
iaload
bipush 8
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
istore 4
iload 3
iconst_1
iadd
istore 2
aload 0
iload 7
iload 6
ixor
iload 5
ixor
iload 4
ixor
aload 1
iload 3
aaload
iconst_3
iaload
ixor
putfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
goto L0
L1:
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
sipush 255
iand
iaload
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
bipush 8
ishr
sipush 255
iand
iaload
bipush 24
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
bipush 16
ishr
sipush 255
iand
iaload
bipush 16
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
bipush 24
ishr
sipush 255
iand
iaload
bipush 8
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 1
iload 2
aaload
iconst_0
iaload
ixor
istore 3
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
sipush 255
iand
iaload
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
bipush 8
ishr
sipush 255
iand
iaload
bipush 24
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
bipush 16
ishr
sipush 255
iand
iaload
bipush 16
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
bipush 24
ishr
sipush 255
iand
iaload
bipush 8
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 1
iload 2
aaload
iconst_1
iaload
ixor
istore 4
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
sipush 255
iand
iaload
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
bipush 8
ishr
sipush 255
iand
iaload
bipush 24
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
bipush 16
ishr
sipush 255
iand
iaload
bipush 16
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
bipush 24
ishr
sipush 255
iand
iaload
bipush 8
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
ixor
aload 1
iload 2
aaload
iconst_2
iaload
ixor
istore 5
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
sipush 255
iand
iaload
istore 7
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
bipush 8
ishr
sipush 255
iand
iaload
bipush 24
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
istore 8
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
bipush 16
ishr
sipush 255
iand
iaload
bipush 16
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
istore 9
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/T0 [I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
bipush 24
ishr
sipush 255
iand
iaload
bipush 8
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
istore 10
iload 2
iconst_1
iadd
istore 6
iload 7
iload 8
ixor
iload 9
ixor
iload 10
ixor
aload 1
iload 2
aaload
iconst_3
iaload
ixor
istore 2
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 3
sipush 255
iand
baload
sipush 255
iand
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 4
bipush 8
ishr
sipush 255
iand
baload
sipush 255
iand
bipush 8
ishl
ixor
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 5
bipush 16
ishr
sipush 255
iand
baload
sipush 255
iand
bipush 16
ishl
ixor
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 2
bipush 24
ishr
sipush 255
iand
baload
bipush 24
ishl
ixor
aload 1
iload 6
aaload
iconst_0
iaload
ixor
putfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 4
sipush 255
iand
baload
sipush 255
iand
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 5
bipush 8
ishr
sipush 255
iand
baload
sipush 255
iand
bipush 8
ishl
ixor
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 2
bipush 16
ishr
sipush 255
iand
baload
sipush 255
iand
bipush 16
ishl
ixor
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 3
bipush 24
ishr
sipush 255
iand
baload
bipush 24
ishl
ixor
aload 1
iload 6
aaload
iconst_1
iaload
ixor
putfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 5
sipush 255
iand
baload
sipush 255
iand
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 2
bipush 8
ishr
sipush 255
iand
baload
sipush 255
iand
bipush 8
ishl
ixor
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 3
bipush 16
ishr
sipush 255
iand
baload
sipush 255
iand
bipush 16
ishl
ixor
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 4
bipush 24
ishr
sipush 255
iand
baload
bipush 24
ishl
ixor
aload 1
iload 6
aaload
iconst_2
iaload
ixor
putfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
aload 0
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 2
sipush 255
iand
baload
sipush 255
iand
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 3
bipush 8
ishr
sipush 255
iand
baload
sipush 255
iand
bipush 8
ishl
ixor
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 4
bipush 16
ishr
sipush 255
iand
baload
sipush 255
iand
bipush 16
ishl
ixor
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 5
bipush 24
ishr
sipush 255
iand
baload
bipush 24
ishl
ixor
aload 1
iload 6
aaload
iconst_3
iaload
ixor
putfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
return
.limit locals 11
.limit stack 6
.end method

.method private generateWorkingKey([B)[[I
.throws net/lingala/zip4j/exception/ZipException
aload 1
arraylength
iconst_4
idiv
istore 4
iload 4
iconst_4
if_icmpeq L0
iload 4
bipush 6
if_icmpeq L0
iload 4
bipush 8
if_icmpne L1
L0:
iload 4
iconst_4
imul
aload 1
arraylength
if_icmpeq L2
L1:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid key length (not 128/192/256)"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
iload 4
bipush 6
iadd
putfield net/lingala/zip4j/crypto/engine/AESEngine/rounds I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/rounds I
istore 2
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
iconst_2
newarray int
dup
iconst_0
iload 2
iconst_1
iadd
iastore
dup
iconst_1
iconst_4
iastore
invokestatic java/lang/reflect/Array/newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;
checkcast [[I
astore 7
iconst_0
istore 2
iconst_0
istore 3
L3:
iload 3
aload 1
arraylength
if_icmpge L4
aload 7
iload 2
iconst_2
ishr
aaload
iload 2
iconst_3
iand
aload 1
iload 3
baload
sipush 255
iand
aload 1
iload 3
iconst_1
iadd
baload
sipush 255
iand
bipush 8
ishl
ior
aload 1
iload 3
iconst_2
iadd
baload
sipush 255
iand
bipush 16
ishl
ior
aload 1
iload 3
iconst_3
iadd
baload
bipush 24
ishl
ior
iastore
iload 3
iconst_4
iadd
istore 3
iload 2
iconst_1
iadd
istore 2
goto L3
L4:
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/rounds I
istore 6
iload 4
istore 3
L5:
iload 3
iload 6
iconst_1
iadd
iconst_2
ishl
if_icmpge L6
aload 7
iload 3
iconst_1
isub
iconst_2
ishr
aaload
iload 3
iconst_1
isub
iconst_3
iand
iaload
istore 5
iload 3
iload 4
irem
ifne L7
aload 0
aload 0
iload 5
bipush 8
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/shift(II)I
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/subWord(I)I
getstatic net/lingala/zip4j/crypto/engine/AESEngine/rcon [I
iload 3
iload 4
idiv
iconst_1
isub
iaload
ixor
istore 2
L8:
aload 7
iload 3
iconst_2
ishr
aaload
iload 3
iconst_3
iand
aload 7
iload 3
iload 4
isub
iconst_2
ishr
aaload
iload 3
iload 4
isub
iconst_3
iand
iaload
iload 2
ixor
iastore
iload 3
iconst_1
iadd
istore 3
goto L5
L7:
iload 5
istore 2
iload 4
bipush 6
if_icmple L8
iload 5
istore 2
iload 3
iload 4
irem
iconst_4
if_icmpne L8
aload 0
iload 5
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/subWord(I)I
istore 2
goto L8
L6:
aload 7
areturn
.limit locals 8
.limit stack 6
.end method

.method private shift(II)I
iload 1
iload 2
iushr
iload 1
iload 2
ineg
ishl
ior
ireturn
.limit locals 3
.limit stack 3
.end method

.method private final stateIn([BI)V
iload 2
iconst_1
iadd
istore 3
aload 0
aload 1
iload 2
baload
sipush 255
iand
putfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
istore 4
iload 3
iconst_1
iadd
istore 2
aload 0
iload 4
aload 1
iload 3
baload
sipush 255
iand
bipush 8
ishl
ior
putfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
istore 4
iload 2
iconst_1
iadd
istore 3
aload 0
iload 4
aload 1
iload 2
baload
sipush 255
iand
bipush 16
ishl
ior
putfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
istore 4
iload 3
iconst_1
iadd
istore 2
aload 0
iload 4
aload 1
iload 3
baload
bipush 24
ishl
ior
putfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
iload 2
iconst_1
iadd
istore 3
aload 0
aload 1
iload 2
baload
sipush 255
iand
putfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
istore 4
iload 3
iconst_1
iadd
istore 2
aload 0
iload 4
aload 1
iload 3
baload
sipush 255
iand
bipush 8
ishl
ior
putfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
istore 4
iload 2
iconst_1
iadd
istore 3
aload 0
iload 4
aload 1
iload 2
baload
sipush 255
iand
bipush 16
ishl
ior
putfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
istore 4
iload 3
iconst_1
iadd
istore 2
aload 0
iload 4
aload 1
iload 3
baload
bipush 24
ishl
ior
putfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
iload 2
iconst_1
iadd
istore 3
aload 0
aload 1
iload 2
baload
sipush 255
iand
putfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
istore 4
iload 3
iconst_1
iadd
istore 2
aload 0
iload 4
aload 1
iload 3
baload
sipush 255
iand
bipush 8
ishl
ior
putfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
istore 4
iload 2
iconst_1
iadd
istore 3
aload 0
iload 4
aload 1
iload 2
baload
sipush 255
iand
bipush 16
ishl
ior
putfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
istore 4
iload 3
iconst_1
iadd
istore 2
aload 0
iload 4
aload 1
iload 3
baload
bipush 24
ishl
ior
putfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
iload 2
iconst_1
iadd
istore 3
aload 0
aload 1
iload 2
baload
sipush 255
iand
putfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
istore 4
iload 3
iconst_1
iadd
istore 2
aload 0
iload 4
aload 1
iload 3
baload
sipush 255
iand
bipush 8
ishl
ior
putfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
istore 3
iload 2
iconst_1
iadd
istore 4
aload 0
iload 3
aload 1
iload 2
baload
sipush 255
iand
bipush 16
ishl
ior
putfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
aload 0
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
aload 1
iload 4
baload
bipush 24
ishl
ior
putfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
return
.limit locals 5
.limit stack 4
.end method

.method private final stateOut([BI)V
iload 2
iconst_1
iadd
istore 3
aload 1
iload 2
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
i2b
bastore
iload 3
iconst_1
iadd
istore 2
aload 1
iload 3
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
bipush 8
ishr
i2b
bastore
iload 2
iconst_1
iadd
istore 3
aload 1
iload 2
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
bipush 16
ishr
i2b
bastore
iload 3
iconst_1
iadd
istore 2
aload 1
iload 3
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C0 I
bipush 24
ishr
i2b
bastore
iload 2
iconst_1
iadd
istore 3
aload 1
iload 2
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
i2b
bastore
iload 3
iconst_1
iadd
istore 2
aload 1
iload 3
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
bipush 8
ishr
i2b
bastore
iload 2
iconst_1
iadd
istore 3
aload 1
iload 2
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
bipush 16
ishr
i2b
bastore
iload 3
iconst_1
iadd
istore 2
aload 1
iload 3
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C1 I
bipush 24
ishr
i2b
bastore
iload 2
iconst_1
iadd
istore 3
aload 1
iload 2
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
i2b
bastore
iload 3
iconst_1
iadd
istore 2
aload 1
iload 3
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
bipush 8
ishr
i2b
bastore
iload 2
iconst_1
iadd
istore 3
aload 1
iload 2
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
bipush 16
ishr
i2b
bastore
iload 3
iconst_1
iadd
istore 2
aload 1
iload 3
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C2 I
bipush 24
ishr
i2b
bastore
iload 2
iconst_1
iadd
istore 3
aload 1
iload 2
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
i2b
bastore
iload 3
iconst_1
iadd
istore 2
aload 1
iload 3
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
bipush 8
ishr
i2b
bastore
iload 2
iconst_1
iadd
istore 3
aload 1
iload 2
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
bipush 16
ishr
i2b
bastore
aload 1
iload 3
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/C3 I
bipush 24
ishr
i2b
bastore
return
.limit locals 4
.limit stack 4
.end method

.method private subWord(I)I
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 1
sipush 255
iand
baload
sipush 255
iand
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 1
bipush 8
ishr
sipush 255
iand
baload
sipush 255
iand
bipush 8
ishl
ior
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 1
bipush 16
ishr
sipush 255
iand
baload
sipush 255
iand
bipush 16
ishl
ior
getstatic net/lingala/zip4j/crypto/engine/AESEngine/S [B
iload 1
bipush 24
ishr
sipush 255
iand
baload
bipush 24
ishl
ior
ireturn
.limit locals 2
.limit stack 4
.end method

.method public init([B)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 0
aload 1
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/generateWorkingKey([B)[[I
putfield net/lingala/zip4j/crypto/engine/AESEngine/workingKey [[I
return
.limit locals 2
.limit stack 3
.end method

.method public processBlock([BI[BI)I
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/workingKey [[I
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "AES engine not initialised"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 2
bipush 16
iadd
aload 1
arraylength
if_icmple L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "input buffer too short"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 4
bipush 16
iadd
aload 3
arraylength
if_icmple L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "output buffer too short"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
aload 1
iload 2
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/stateIn([BI)V
aload 0
aload 0
getfield net/lingala/zip4j/crypto/engine/AESEngine/workingKey [[I
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/encryptBlock([[I)V
aload 0
aload 3
iload 4
invokespecial net/lingala/zip4j/crypto/engine/AESEngine/stateOut([BI)V
bipush 16
ireturn
.limit locals 5
.limit stack 3
.end method

.method public processBlock([B[B)I
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
iconst_0
aload 2
iconst_0
invokevirtual net/lingala/zip4j/crypto/engine/AESEngine/processBlock([BI[BI)I
ireturn
.limit locals 3
.limit stack 5
.end method
