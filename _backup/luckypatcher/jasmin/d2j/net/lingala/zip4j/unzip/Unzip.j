.bytecode 50.0
.class public synchronized net/lingala/zip4j/unzip/Unzip
.super java/lang/Object
.inner class inner net/lingala/zip4j/unzip/Unzip$1
.inner class inner net/lingala/zip4j/unzip/Unzip$2

.field private 'zipModel' Lnet/lingala/zip4j/model/ZipModel;

.method public <init>(Lnet/lingala/zip4j/model/ZipModel;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "ZipModel is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield net/lingala/zip4j/unzip/Unzip/zipModel Lnet/lingala/zip4j/model/ZipModel;
return
.limit locals 2
.limit stack 3
.end method

.method static synthetic access$000(Lnet/lingala/zip4j/unzip/Unzip;Ljava/util/ArrayList;Lnet/lingala/zip4j/model/UnzipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
aload 2
aload 3
aload 4
invokespecial net/lingala/zip4j/unzip/Unzip/initExtractAll(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/UnzipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Ljava/lang/String;)V
return
.limit locals 5
.limit stack 5
.end method

.method static synthetic access$100(Lnet/lingala/zip4j/unzip/Unzip;Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
aload 2
aload 3
aload 4
aload 5
invokespecial net/lingala/zip4j/unzip/Unzip/initExtractFile(Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
return
.limit locals 6
.limit stack 6
.end method

.method private calculateTotalWork(Ljava/util/ArrayList;)J
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "fileHeaders is null, cannot calculate total work"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
lconst_0
lstore 3
iconst_0
istore 2
L1:
iload 2
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpge L2
aload 1
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
astore 5
aload 5
invokevirtual net/lingala/zip4j/model/FileHeader/getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
ifnull L3
aload 5
invokevirtual net/lingala/zip4j/model/FileHeader/getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getUnCompressedSize()J
lconst_0
lcmp
ifle L3
lload 3
aload 5
invokevirtual net/lingala/zip4j/model/FileHeader/getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getCompressedSize()J
ladd
lstore 3
L4:
iload 2
iconst_1
iadd
istore 2
goto L1
L3:
lload 3
aload 5
invokevirtual net/lingala/zip4j/model/FileHeader/getCompressedSize()J
ladd
lstore 3
goto L4
L2:
lload 3
lreturn
.limit locals 6
.limit stack 4
.end method

.method private checkOutputDirectoryStructure(Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
.catch java/lang/Exception from L0 to L1 using L2
aload 1
ifnull L3
aload 2
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L4
L3:
new net/lingala/zip4j/exception/ZipException
dup
ldc "Cannot check output directory structure...one of the parameters was null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
astore 1
aload 3
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifeq L5
aload 3
astore 1
L5:
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L6
L7:
return
L6:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L0:
new java/io/File
dup
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getParent()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifne L7
aload 1
invokevirtual java/io/File/mkdirs()Z
pop
L1:
return
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 4
.limit stack 5
.end method

.method private initExtractAll(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/UnzipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
iconst_0
istore 5
L0:
iload 5
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpge L1
aload 0
aload 1
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
aload 4
aload 2
aconst_null
aload 3
invokespecial net/lingala/zip4j/unzip/Unzip/initExtractFile(Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
aload 3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/isCancelAllTasks()Z
ifeq L2
aload 3
iconst_3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setResult(I)V
aload 3
iconst_0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
L1:
return
L2:
iload 5
iconst_1
iadd
istore 5
goto L0
.limit locals 6
.limit stack 6
.end method

.method private initExtractFile(Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
.throws net/lingala/zip4j/exception/ZipException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch net/lingala/zip4j/exception/ZipException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
.catch net/lingala/zip4j/exception/ZipException from L5 to L6 using L2
.catch java/lang/Exception from L5 to L6 using L3
.catch java/lang/Exception from L7 to L8 using L9
.catch net/lingala/zip4j/exception/ZipException from L7 to L8 using L2
.catch java/lang/Exception from L10 to L11 using L9
.catch net/lingala/zip4j/exception/ZipException from L10 to L11 using L2
.catch net/lingala/zip4j/exception/ZipException from L12 to L2 using L2
.catch java/lang/Exception from L12 to L2 using L3
.catch net/lingala/zip4j/exception/ZipException from L13 to L14 using L2
.catch java/lang/Exception from L13 to L14 using L3
.catch java/lang/Exception from L14 to L15 using L16
.catch net/lingala/zip4j/exception/ZipException from L14 to L15 using L2
.catch net/lingala/zip4j/exception/ZipException from L17 to L3 using L2
.catch java/lang/Exception from L17 to L3 using L3
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "fileHeader is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 5
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setFileName(Ljava/lang/String;)V
L1:
aload 2
astore 7
L4:
aload 2
getstatic net/lingala/zip4j/util/InternalZipConstants/FILE_SEPARATOR Ljava/lang/String;
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic net/lingala/zip4j/util/InternalZipConstants/FILE_SEPARATOR Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 7
L5:
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/isDirectory()Z
istore 6
L6:
iload 6
ifeq L13
L7:
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
astore 1
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L10
L8:
return
L10:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifne L18
aload 1
invokevirtual java/io/File/mkdirs()Z
pop
L11:
return
L9:
astore 1
L12:
aload 5
aload 1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorError(Ljava/lang/Throwable;)V
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L2:
astore 1
aload 5
aload 1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorError(Ljava/lang/Throwable;)V
aload 1
athrow
L13:
aload 0
aload 1
aload 7
aload 4
invokespecial net/lingala/zip4j/unzip/Unzip/checkOutputDirectoryStructure(Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Ljava/lang/String;)V
new net/lingala/zip4j/unzip/UnzipEngine
dup
aload 0
getfield net/lingala/zip4j/unzip/Unzip/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 1
invokespecial net/lingala/zip4j/unzip/UnzipEngine/<init>(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;)V
astore 1
L14:
aload 1
aload 5
aload 7
aload 4
aload 3
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/unzipFile(Lnet/lingala/zip4j/progress/ProgressMonitor;Ljava/lang/String;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;)V
L15:
return
L16:
astore 1
L17:
aload 5
aload 1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorError(Ljava/lang/Throwable;)V
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 1
aload 5
aload 1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorError(Ljava/lang/Throwable;)V
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L18:
return
.limit locals 8
.limit stack 5
.end method

.method public extractAll(Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/unzip/Unzip/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
astore 5
aload 5
ifnull L0
aload 5
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
ifnonnull L1
L0:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid central directory in zipModel"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 5
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
astore 5
aload 3
iconst_1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setCurrentOperation(I)V
aload 3
aload 0
aload 5
invokespecial net/lingala/zip4j/unzip/Unzip/calculateTotalWork(Ljava/util/ArrayList;)J
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setTotalWork(J)V
aload 3
iconst_1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
iload 4
ifeq L2
new net/lingala/zip4j/unzip/Unzip$1
dup
aload 0
ldc "Zip4j"
aload 5
aload 1
aload 3
aload 2
invokespecial net/lingala/zip4j/unzip/Unzip$1/<init>(Lnet/lingala/zip4j/unzip/Unzip;Ljava/lang/String;Ljava/util/ArrayList;Lnet/lingala/zip4j/model/UnzipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Ljava/lang/String;)V
invokevirtual java/lang/Thread/start()V
return
L2:
aload 0
aload 5
aload 1
aload 3
aload 2
invokespecial net/lingala/zip4j/unzip/Unzip/initExtractAll(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/UnzipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Ljava/lang/String;)V
return
.limit locals 6
.limit stack 8
.end method

.method public extractFile(Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "fileHeader is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 5
iconst_1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setCurrentOperation(I)V
aload 5
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getCompressedSize()J
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setTotalWork(J)V
aload 5
iconst_1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
aload 5
iconst_0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setPercentDone(I)V
aload 5
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setFileName(Ljava/lang/String;)V
iload 6
ifeq L1
new net/lingala/zip4j/unzip/Unzip$2
dup
aload 0
ldc "Zip4j"
aload 1
aload 2
aload 3
aload 4
aload 5
invokespecial net/lingala/zip4j/unzip/Unzip$2/<init>(Lnet/lingala/zip4j/unzip/Unzip;Ljava/lang/String;Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
invokevirtual java/lang/Thread/start()V
return
L1:
aload 0
aload 1
aload 2
aload 3
aload 4
aload 5
invokespecial net/lingala/zip4j/unzip/Unzip/initExtractFile(Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
aload 5
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorSuccess()V
return
.limit locals 7
.limit stack 9
.end method

.method public getInputStream(Lnet/lingala/zip4j/model/FileHeader;)Lnet/lingala/zip4j/io/ZipInputStream;
.throws net/lingala/zip4j/exception/ZipException
new net/lingala/zip4j/unzip/UnzipEngine
dup
aload 0
getfield net/lingala/zip4j/unzip/Unzip/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 1
invokespecial net/lingala/zip4j/unzip/UnzipEngine/<init>(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;)V
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/getInputStream()Lnet/lingala/zip4j/io/ZipInputStream;
areturn
.limit locals 2
.limit stack 4
.end method
