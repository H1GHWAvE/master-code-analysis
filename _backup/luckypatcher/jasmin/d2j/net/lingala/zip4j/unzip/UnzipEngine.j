.bytecode 50.0
.class public synchronized net/lingala/zip4j/unzip/UnzipEngine
.super java/lang/Object

.field private 'crc' Ljava/util/zip/CRC32;

.field private 'currSplitFileCounter' I

.field private 'decrypter' Lnet/lingala/zip4j/crypto/IDecrypter;

.field private 'fileHeader' Lnet/lingala/zip4j/model/FileHeader;

.field private 'localFileHeader' Lnet/lingala/zip4j/model/LocalFileHeader;

.field private 'zipModel' Lnet/lingala/zip4j/model/ZipModel;

.method public <init>(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield net/lingala/zip4j/unzip/UnzipEngine/currSplitFileCounter I
aload 1
ifnull L0
aload 2
ifnonnull L1
L0:
new net/lingala/zip4j/exception/ZipException
dup
ldc "Invalid parameters passed to StoreUnzip. One or more of the parameters were null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 1
putfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
aload 2
putfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
putfield net/lingala/zip4j/unzip/UnzipEngine/crc Ljava/util/zip/CRC32;
return
.limit locals 3
.limit stack 3
.end method

.method private calculateAESSaltLength(Lnet/lingala/zip4j/model/AESExtraDataRecord;)I
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "unable to determine salt length: AESExtraDataRecord is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getAesStrength()I
tableswitch 1
L1
L2
L3
default : L4
L4:
new net/lingala/zip4j/exception/ZipException
dup
ldc "unable to determine salt length: invalid aes key strength"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
bipush 8
ireturn
L2:
bipush 12
ireturn
L3:
bipush 16
ireturn
.limit locals 2
.limit stack 3
.end method

.method private checkLocalHeader()Z
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/FileNotFoundException from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch java/io/FileNotFoundException from L6 to L7 using L2
.catch all from L6 to L7 using L3
.catch java/io/FileNotFoundException from L8 to L9 using L2
.catch all from L8 to L9 using L3
.catch java/io/FileNotFoundException from L10 to L2 using L2
.catch all from L10 to L2 using L3
.catch all from L11 to L3 using L3
.catch java/io/IOException from L12 to L13 using L14
.catch java/lang/Exception from L12 to L13 using L15
.catch java/io/FileNotFoundException from L16 to L17 using L2
.catch all from L16 to L17 using L3
.catch java/io/FileNotFoundException from L18 to L19 using L2
.catch all from L18 to L19 using L3
.catch java/io/IOException from L20 to L21 using L22
.catch java/lang/Exception from L20 to L21 using L23
.catch java/io/IOException from L24 to L25 using L26
.catch java/lang/Exception from L24 to L25 using L27
aconst_null
astore 6
aconst_null
astore 5
L0:
aload 0
invokespecial net/lingala/zip4j/unzip/UnzipEngine/checkSplitFile()Ljava/io/RandomAccessFile;
astore 8
L1:
aload 8
astore 7
aload 8
ifnonnull L5
aload 8
astore 5
aload 8
astore 6
L4:
new java/io/RandomAccessFile
dup
new java/io/File
dup
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 7
L5:
aload 7
astore 5
aload 7
astore 6
L6:
aload 0
new net/lingala/zip4j/core/HeaderReader
dup
aload 7
invokespecial net/lingala/zip4j/core/HeaderReader/<init>(Ljava/io/RandomAccessFile;)V
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/core/HeaderReader/readLocalFileHeader(Lnet/lingala/zip4j/model/FileHeader;)Lnet/lingala/zip4j/model/LocalFileHeader;
putfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
L7:
aload 7
astore 5
aload 7
astore 6
L8:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
ifnonnull L28
L9:
aload 7
astore 5
aload 7
astore 6
L10:
new net/lingala/zip4j/exception/ZipException
dup
ldc "error reading local file header. Is this a valid zip file?"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 7
aload 5
astore 6
L11:
new net/lingala/zip4j/exception/ZipException
dup
aload 7
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 5
aload 6
ifnull L13
L12:
aload 6
invokevirtual java/io/RandomAccessFile/close()V
L13:
aload 5
athrow
L28:
aload 7
astore 5
aload 7
astore 6
L16:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getCompressionMethod()I
istore 1
L17:
aload 7
astore 5
aload 7
astore 6
L18:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getCompressionMethod()I
istore 2
L19:
iload 1
iload 2
if_icmpeq L29
iconst_0
istore 4
iload 4
istore 3
aload 7
ifnull L30
L20:
aload 7
invokevirtual java/io/RandomAccessFile/close()V
L21:
iload 4
istore 3
L30:
iload 3
ireturn
L29:
iconst_1
istore 3
aload 7
ifnull L30
L24:
aload 7
invokevirtual java/io/RandomAccessFile/close()V
L25:
iconst_1
ireturn
L26:
astore 5
iconst_1
ireturn
L22:
astore 5
iconst_0
ireturn
L23:
astore 5
iconst_0
ireturn
L27:
astore 5
iconst_1
ireturn
L14:
astore 6
goto L13
L15:
astore 6
goto L13
.limit locals 9
.limit stack 5
.end method

.method private checkSplitFile()Ljava/io/RandomAccessFile;
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch java/io/FileNotFoundException from L4 to L5 using L2
.catch java/io/IOException from L4 to L5 using L3
.catch java/io/FileNotFoundException from L6 to L2 using L2
.catch java/io/IOException from L6 to L2 using L3
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifeq L7
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getDiskNumberStart()I
istore 1
aload 0
iload 1
iconst_1
iadd
putfield net/lingala/zip4j/unzip/UnzipEngine/currSplitFileCounter I
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
astore 2
iload 1
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getNoOfThisDisk()I
if_icmpne L8
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
astore 2
L0:
new java/io/RandomAccessFile
dup
aload 2
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 3
L1:
aload 3
astore 2
L4:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/currSplitFileCounter I
iconst_1
if_icmpne L9
iconst_4
newarray byte
astore 4
aload 3
aload 4
invokevirtual java/io/RandomAccessFile/read([B)I
pop
L5:
aload 3
astore 2
L6:
aload 4
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
i2l
ldc2_w 134695760L
lcmp
ifeq L9
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid first part split file signature"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 2
new net/lingala/zip4j/exception/ZipException
dup
aload 2
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L8:
iload 1
bipush 9
if_icmplt L10
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
iconst_0
aload 2
ldc "."
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".z"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
goto L0
L10:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
iconst_0
aload 2
ldc "."
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".z0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
goto L0
L3:
astore 2
new net/lingala/zip4j/exception/ZipException
dup
aload 2
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L7:
aconst_null
astore 2
L9:
aload 2
areturn
.limit locals 5
.limit stack 5
.end method

.method private closeStreams(Ljava/io/InputStream;Ljava/io/OutputStream;)V
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L4 to L5 using L6
.catch all from L7 to L3 using L3
.catch java/io/IOException from L8 to L9 using L10
.catch java/io/IOException from L11 to L12 using L13
aload 1
ifnull L1
L0:
aload 1
invokevirtual java/io/InputStream/close()V
L1:
aload 2
ifnull L5
L4:
aload 2
invokevirtual java/io/OutputStream/close()V
L5:
return
L2:
astore 1
aload 1
ifnull L14
L7:
aload 1
invokevirtual java/io/IOException/getMessage()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifeq L14
aload 1
invokevirtual java/io/IOException/getMessage()Ljava/lang/String;
ldc " - Wrong Password?"
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iflt L14
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokevirtual java/io/IOException/getMessage()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
astore 1
aload 2
ifnull L9
L8:
aload 2
invokevirtual java/io/OutputStream/close()V
L9:
aload 1
athrow
L14:
aload 2
ifnull L5
L11:
aload 2
invokevirtual java/io/OutputStream/close()V
L12:
return
L6:
astore 1
return
L13:
astore 1
return
L10:
astore 2
goto L9
.limit locals 3
.limit stack 3
.end method

.method private createFileHandler(Ljava/lang/String;)Ljava/io/RandomAccessFile;
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch java/io/FileNotFoundException from L1 to L4 using L2
.catch java/lang/Exception from L1 to L4 using L3
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnull L5
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
L5:
new net/lingala/zip4j/exception/ZipException
dup
ldc "input parameter is null in getFilePointer"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifeq L1
aload 0
invokespecial net/lingala/zip4j/unzip/UnzipEngine/checkSplitFile()Ljava/io/RandomAccessFile;
areturn
L1:
new java/io/RandomAccessFile
dup
new java/io/File
dup
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 1
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 1
L4:
aload 1
areturn
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 5
.end method

.method private getAESPasswordVerifier(Ljava/io/RandomAccessFile;)[B
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
L0:
iconst_2
newarray byte
astore 2
aload 1
aload 2
invokevirtual java/io/RandomAccessFile/read([B)I
pop
L1:
aload 2
areturn
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method private getAESSalt(Ljava/io/RandomAccessFile;)[B
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getAesExtraDataRecord()Lnet/lingala/zip4j/model/AESExtraDataRecord;
ifnonnull L0
aconst_null
areturn
L0:
aload 0
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getAesExtraDataRecord()Lnet/lingala/zip4j/model/AESExtraDataRecord;
invokespecial net/lingala/zip4j/unzip/UnzipEngine/calculateAESSaltLength(Lnet/lingala/zip4j/model/AESExtraDataRecord;)I
newarray byte
astore 2
aload 1
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getOffsetStartOfData()J
invokevirtual java/io/RandomAccessFile/seek(J)V
aload 1
aload 2
invokevirtual java/io/RandomAccessFile/read([B)I
pop
L1:
aload 2
areturn
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method private getOutputFileNameWithPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.throws net/lingala/zip4j/exception/ZipException
aload 2
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifeq L0
L1:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "file.separator"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
astore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method private getOutputStream(Ljava/lang/String;Ljava/lang/String;)Ljava/io/FileOutputStream;
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/FileNotFoundException from L1 to L3 using L2
.catch java/io/FileNotFoundException from L3 to L4 using L2
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid output path"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/io/File
dup
aload 0
aload 1
aload 2
invokespecial net/lingala/zip4j/unzip/UnzipEngine/getOutputFileNameWithPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/getParentFile()Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifne L1
aload 1
invokevirtual java/io/File/getParentFile()Ljava/io/File;
invokevirtual java/io/File/mkdirs()Z
pop
L1:
aload 1
invokevirtual java/io/File/exists()Z
ifeq L3
aload 1
invokevirtual java/io/File/delete()Z
pop
L3:
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 1
L4:
aload 1
areturn
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 5
.end method

.method private getStandardDecrypterHeaderBytes(Ljava/io/RandomAccessFile;)[B
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
L0:
bipush 12
newarray byte
astore 2
aload 1
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getOffsetStartOfData()J
invokevirtual java/io/RandomAccessFile/seek(J)V
aload 1
aload 2
iconst_0
bipush 12
invokevirtual java/io/RandomAccessFile/read([BII)I
pop
L1:
aload 2
areturn
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 4
.end method

.method private init(Ljava/io/RandomAccessFile;)V
.throws net/lingala/zip4j/exception/ZipException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "local file header is null, cannot initialize input stream"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
invokespecial net/lingala/zip4j/unzip/UnzipEngine/initDecrypter(Ljava/io/RandomAccessFile;)V
L1:
return
L2:
astore 1
aload 1
athrow
L3:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method private initDecrypter(Ljava/io/RandomAccessFile;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "local file header is null, cannot init decrypter"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/isEncrypted()Z
ifeq L1
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getEncryptionMethod()I
ifne L2
aload 0
new net/lingala/zip4j/crypto/StandardDecrypter
dup
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
aload 1
invokespecial net/lingala/zip4j/unzip/UnzipEngine/getStandardDecrypterHeaderBytes(Ljava/io/RandomAccessFile;)[B
invokespecial net/lingala/zip4j/crypto/StandardDecrypter/<init>(Lnet/lingala/zip4j/model/FileHeader;[B)V
putfield net/lingala/zip4j/unzip/UnzipEngine/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
L1:
return
L2:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getEncryptionMethod()I
bipush 99
if_icmpne L3
aload 0
new net/lingala/zip4j/crypto/AESDecrypter
dup
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
aload 1
invokespecial net/lingala/zip4j/unzip/UnzipEngine/getAESSalt(Ljava/io/RandomAccessFile;)[B
aload 0
aload 1
invokespecial net/lingala/zip4j/unzip/UnzipEngine/getAESPasswordVerifier(Ljava/io/RandomAccessFile;)[B
invokespecial net/lingala/zip4j/crypto/AESDecrypter/<init>(Lnet/lingala/zip4j/model/LocalFileHeader;[B[B)V
putfield net/lingala/zip4j/unzip/UnzipEngine/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
return
L3:
new net/lingala/zip4j/exception/ZipException
dup
ldc "unsupported encryption method"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 7
.end method

.method public checkCRC()V
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
ifnull L0
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getEncryptionMethod()I
bipush 99
if_icmpne L1
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
ifnull L0
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
instanceof net/lingala/zip4j/crypto/AESDecrypter
ifeq L0
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
checkcast net/lingala/zip4j/crypto/AESDecrypter
invokevirtual net/lingala/zip4j/crypto/AESDecrypter/getCalculatedAuthenticationBytes()[B
astore 1
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
checkcast net/lingala/zip4j/crypto/AESDecrypter
invokevirtual net/lingala/zip4j/crypto/AESDecrypter/getStoredMac()[B
astore 2
bipush 10
newarray byte
astore 3
aload 3
ifnull L2
aload 2
ifnonnull L3
L2:
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "CRC (MAC) check failed for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 1
iconst_0
aload 3
iconst_0
bipush 10
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
aload 2
invokestatic java/util/Arrays/equals([B[B)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "invalid CRC (MAC) for file: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/crc Ljava/util/zip/CRC32;
invokevirtual java/util/zip/CRC32/getValue()J
ldc2_w 4294967295L
land
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getCrc32()J
lcmp
ifeq L0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "invalid CRC for file: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
aload 2
astore 1
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/isEncrypted()Z
ifeq L4
aload 2
astore 1
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getEncryptionMethod()I
ifne L4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " - Wrong Password?"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L4:
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 4
.limit stack 5
.end method

.method public getDecrypter()Lnet/lingala/zip4j/crypto/IDecrypter;
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getFileHeader()Lnet/lingala/zip4j/model/FileHeader;
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getInputStream()Lnet/lingala/zip4j/io/ZipInputStream;
.throws net/lingala/zip4j/exception/ZipException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch net/lingala/zip4j/exception/ZipException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
.catch net/lingala/zip4j/exception/ZipException from L6 to L2 using L2
.catch java/lang/Exception from L6 to L2 using L3
.catch java/io/IOException from L7 to L8 using L9
.catch net/lingala/zip4j/exception/ZipException from L10 to L11 using L2
.catch java/lang/Exception from L10 to L11 using L3
.catch net/lingala/zip4j/exception/ZipException from L12 to L13 using L2
.catch java/lang/Exception from L12 to L13 using L3
.catch net/lingala/zip4j/exception/ZipException from L14 to L15 using L2
.catch java/lang/Exception from L14 to L15 using L3
.catch net/lingala/zip4j/exception/ZipException from L16 to L17 using L2
.catch java/lang/Exception from L16 to L17 using L3
.catch net/lingala/zip4j/exception/ZipException from L18 to L19 using L2
.catch java/lang/Exception from L18 to L19 using L3
.catch net/lingala/zip4j/exception/ZipException from L20 to L21 using L2
.catch java/lang/Exception from L20 to L21 using L3
.catch net/lingala/zip4j/exception/ZipException from L22 to L23 using L2
.catch java/lang/Exception from L22 to L23 using L3
.catch net/lingala/zip4j/exception/ZipException from L24 to L25 using L2
.catch java/lang/Exception from L24 to L25 using L3
.catch net/lingala/zip4j/exception/ZipException from L26 to L27 using L2
.catch java/lang/Exception from L26 to L27 using L3
.catch net/lingala/zip4j/exception/ZipException from L28 to L29 using L2
.catch java/lang/Exception from L28 to L29 using L3
.catch net/lingala/zip4j/exception/ZipException from L30 to L31 using L2
.catch java/lang/Exception from L30 to L31 using L3
.catch net/lingala/zip4j/exception/ZipException from L32 to L33 using L2
.catch java/lang/Exception from L32 to L33 using L3
.catch net/lingala/zip4j/exception/ZipException from L34 to L35 using L2
.catch java/lang/Exception from L34 to L35 using L3
.catch net/lingala/zip4j/exception/ZipException from L36 to L37 using L2
.catch java/lang/Exception from L36 to L37 using L3
.catch net/lingala/zip4j/exception/ZipException from L38 to L39 using L2
.catch java/lang/Exception from L38 to L39 using L3
.catch net/lingala/zip4j/exception/ZipException from L40 to L3 using L2
.catch java/lang/Exception from L40 to L3 using L3
.catch java/io/IOException from L41 to L42 using L43
.catch net/lingala/zip4j/exception/ZipException from L44 to L45 using L2
.catch java/lang/Exception from L44 to L45 using L3
.catch net/lingala/zip4j/exception/ZipException from L46 to L47 using L2
.catch java/lang/Exception from L46 to L47 using L3
.catch net/lingala/zip4j/exception/ZipException from L48 to L49 using L2
.catch java/lang/Exception from L48 to L49 using L3
.catch net/lingala/zip4j/exception/ZipException from L50 to L51 using L2
.catch java/lang/Exception from L50 to L51 using L3
.catch net/lingala/zip4j/exception/ZipException from L52 to L53 using L2
.catch java/lang/Exception from L52 to L53 using L3
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
ifnonnull L54
new net/lingala/zip4j/exception/ZipException
dup
ldc "file header is null, cannot get inputstream"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L54:
aconst_null
astore 11
aconst_null
astore 10
L0:
aload 0
ldc "r"
invokespecial net/lingala/zip4j/unzip/UnzipEngine/createFileHandler(Ljava/lang/String;)Ljava/io/RandomAccessFile;
astore 12
L1:
aload 12
astore 10
aload 12
astore 11
L4:
aload 0
invokespecial net/lingala/zip4j/unzip/UnzipEngine/checkLocalHeader()Z
ifne L55
L5:
aload 12
astore 10
aload 12
astore 11
L6:
new net/lingala/zip4j/exception/ZipException
dup
ldc "local header and file header do not match"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 11
aload 10
ifnull L8
L7:
aload 10
invokevirtual java/io/RandomAccessFile/close()V
L8:
aload 11
athrow
L55:
aload 12
astore 10
aload 12
astore 11
L10:
aload 0
aload 12
invokespecial net/lingala/zip4j/unzip/UnzipEngine/init(Ljava/io/RandomAccessFile;)V
L11:
aload 12
astore 10
aload 12
astore 11
L12:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getCompressedSize()J
lstore 8
L13:
aload 12
astore 10
aload 12
astore 11
L14:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getOffsetStartOfData()J
lstore 6
L15:
aload 12
astore 10
lload 6
lstore 2
lload 8
lstore 4
aload 12
astore 11
L16:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/isEncrypted()Z
ifeq L29
L17:
aload 12
astore 10
aload 12
astore 11
L18:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getEncryptionMethod()I
bipush 99
if_icmpne L45
L19:
aload 12
astore 10
aload 12
astore 11
L20:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
instanceof net/lingala/zip4j/crypto/AESDecrypter
ifeq L56
L21:
aload 12
astore 10
aload 12
astore 11
L22:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
checkcast net/lingala/zip4j/crypto/AESDecrypter
invokevirtual net/lingala/zip4j/crypto/AESDecrypter/getSaltLength()I
istore 1
L23:
aload 12
astore 10
aload 12
astore 11
L24:
lload 8
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
checkcast net/lingala/zip4j/crypto/AESDecrypter
invokevirtual net/lingala/zip4j/crypto/AESDecrypter/getPasswordVerifierLength()I
iload 1
iadd
bipush 10
iadd
i2l
lsub
lstore 4
L25:
aload 12
astore 10
aload 12
astore 11
L26:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
checkcast net/lingala/zip4j/crypto/AESDecrypter
invokevirtual net/lingala/zip4j/crypto/AESDecrypter/getSaltLength()I
istore 1
L27:
aload 12
astore 10
aload 12
astore 11
L28:
lload 6
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
checkcast net/lingala/zip4j/crypto/AESDecrypter
invokevirtual net/lingala/zip4j/crypto/AESDecrypter/getPasswordVerifierLength()I
iload 1
iadd
i2l
ladd
lstore 2
L29:
aload 12
astore 10
aload 12
astore 11
L30:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getCompressionMethod()I
istore 1
L31:
aload 12
astore 10
aload 12
astore 11
L32:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getEncryptionMethod()I
bipush 99
if_icmpne L37
L33:
aload 12
astore 10
aload 12
astore 11
L34:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getAesExtraDataRecord()Lnet/lingala/zip4j/model/AESExtraDataRecord;
ifnull L57
L35:
aload 12
astore 10
aload 12
astore 11
L36:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getAesExtraDataRecord()Lnet/lingala/zip4j/model/AESExtraDataRecord;
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getCompressionMethod()I
istore 1
L37:
aload 12
astore 10
aload 12
astore 11
L38:
aload 12
lload 2
invokevirtual java/io/RandomAccessFile/seek(J)V
L39:
iload 1
lookupswitch
0 : L49
8 : L51
default : L58
L59:
aload 12
astore 10
aload 12
astore 11
L40:
new net/lingala/zip4j/exception/ZipException
dup
ldc "compression type not supported"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
astore 10
aload 11
ifnull L42
L41:
aload 11
invokevirtual java/io/RandomAccessFile/close()V
L42:
new net/lingala/zip4j/exception/ZipException
dup
aload 10
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L56:
aload 12
astore 10
aload 12
astore 11
L44:
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "invalid decryptor when trying to calculate compressed size for AES encrypted file: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L45:
aload 12
astore 10
lload 6
lstore 2
lload 8
lstore 4
aload 12
astore 11
L46:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getEncryptionMethod()I
ifne L29
L47:
lload 8
ldc2_w 12L
lsub
lstore 4
lload 6
ldc2_w 12L
ladd
lstore 2
goto L29
L57:
aload 12
astore 10
aload 12
astore 11
L48:
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "AESExtraDataRecord does not exist for AES encrypted file: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L49:
aload 12
astore 10
aload 12
astore 11
L50:
new net/lingala/zip4j/io/ZipInputStream
dup
new net/lingala/zip4j/io/PartInputStream
dup
aload 12
lload 2
lload 4
aload 0
invokespecial net/lingala/zip4j/io/PartInputStream/<init>(Ljava/io/RandomAccessFile;JJLnet/lingala/zip4j/unzip/UnzipEngine;)V
invokespecial net/lingala/zip4j/io/ZipInputStream/<init>(Lnet/lingala/zip4j/io/BaseInputStream;)V
areturn
L51:
aload 12
astore 10
aload 12
astore 11
L52:
new net/lingala/zip4j/io/ZipInputStream
dup
new net/lingala/zip4j/io/InflaterInputStream
dup
aload 12
lload 2
lload 4
aload 0
invokespecial net/lingala/zip4j/io/InflaterInputStream/<init>(Ljava/io/RandomAccessFile;JJLnet/lingala/zip4j/unzip/UnzipEngine;)V
invokespecial net/lingala/zip4j/io/ZipInputStream/<init>(Lnet/lingala/zip4j/io/BaseInputStream;)V
astore 12
L53:
aload 12
areturn
L9:
astore 10
goto L8
L43:
astore 11
goto L42
L58:
goto L59
.limit locals 13
.limit stack 10
.end method

.method public getLocalFileHeader()Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getZipModel()Lnet/lingala/zip4j/model/ZipModel;
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
areturn
.limit locals 1
.limit stack 1
.end method

.method public startNextSplitFile()Ljava/io/RandomAccessFile;
.throws java/io/IOException
.throws java/io/FileNotFoundException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L1
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
astore 1
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/currSplitFileCounter I
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getNoOfThisDisk()I
if_icmpne L2
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
astore 1
L3:
aload 0
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/currSplitFileCounter I
iconst_1
iadd
putfield net/lingala/zip4j/unzip/UnzipEngine/currSplitFileCounter I
L0:
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
ifne L4
new java/io/IOException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "zip split file does not exist: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 1
new java/io/IOException
dup
aload 1
invokevirtual net/lingala/zip4j/exception/ZipException/getMessage()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/currSplitFileCounter I
bipush 9
if_icmplt L5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
iconst_0
aload 1
ldc "."
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".z"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/currSplitFileCounter I
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
goto L3
L5:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
iconst_0
aload 1
ldc "."
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".z0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/currSplitFileCounter I
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
goto L3
L4:
new java/io/RandomAccessFile
dup
aload 1
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/lang/String;Ljava/lang/String;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method public unzipFile(Lnet/lingala/zip4j/progress/ProgressMonitor;Ljava/lang/String;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch all from L0 to L1 using L4
.catch java/io/IOException from L5 to L6 using L2
.catch java/lang/Exception from L5 to L6 using L3
.catch all from L5 to L6 using L4
.catch java/io/IOException from L7 to L8 using L2
.catch java/lang/Exception from L7 to L8 using L3
.catch all from L7 to L8 using L4
.catch java/io/IOException from L9 to L10 using L2
.catch java/lang/Exception from L9 to L10 using L3
.catch all from L9 to L10 using L4
.catch java/io/IOException from L11 to L12 using L2
.catch java/lang/Exception from L11 to L12 using L3
.catch all from L11 to L12 using L4
.catch java/io/IOException from L13 to L14 using L2
.catch java/lang/Exception from L13 to L14 using L3
.catch all from L13 to L14 using L4
.catch java/io/IOException from L15 to L16 using L2
.catch java/lang/Exception from L15 to L16 using L3
.catch all from L15 to L16 using L4
.catch java/io/IOException from L17 to L18 using L2
.catch java/lang/Exception from L17 to L18 using L3
.catch all from L17 to L18 using L4
.catch java/io/IOException from L19 to L20 using L2
.catch java/lang/Exception from L19 to L20 using L3
.catch all from L19 to L20 using L4
.catch java/io/IOException from L21 to L22 using L2
.catch java/lang/Exception from L21 to L22 using L3
.catch all from L21 to L22 using L4
.catch java/io/IOException from L23 to L24 using L2
.catch java/lang/Exception from L23 to L24 using L3
.catch all from L23 to L24 using L4
.catch all from L25 to L4 using L4
.catch all from L26 to L27 using L4
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnull L28
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
ifnull L28
aload 2
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L29
L28:
new net/lingala/zip4j/exception/ZipException
dup
ldc "Invalid parameters passed during unzipping file. One or more of the parameters were null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L29:
aconst_null
astore 16
aconst_null
astore 17
aconst_null
astore 12
aconst_null
astore 14
aconst_null
astore 15
aconst_null
astore 13
aload 12
astore 8
aload 13
astore 9
aload 16
astore 6
aload 14
astore 7
aload 17
astore 10
aload 15
astore 11
L0:
sipush 4096
newarray byte
astore 18
L1:
aload 12
astore 8
aload 13
astore 9
aload 16
astore 6
aload 14
astore 7
aload 17
astore 10
aload 15
astore 11
L5:
aload 0
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/getInputStream()Lnet/lingala/zip4j/io/ZipInputStream;
astore 12
L6:
aload 12
astore 8
aload 13
astore 9
aload 12
astore 6
aload 14
astore 7
aload 12
astore 10
aload 15
astore 11
L7:
aload 0
aload 2
aload 3
invokespecial net/lingala/zip4j/unzip/UnzipEngine/getOutputStream(Ljava/lang/String;Ljava/lang/String;)Ljava/io/FileOutputStream;
astore 13
L8:
aload 12
astore 8
aload 13
astore 9
aload 12
astore 6
aload 13
astore 7
aload 12
astore 10
aload 13
astore 11
L9:
aload 12
aload 18
invokevirtual java/io/InputStream/read([B)I
istore 5
L10:
iload 5
iconst_m1
if_icmpeq L30
aload 12
astore 8
aload 13
astore 9
aload 12
astore 6
aload 13
astore 7
aload 12
astore 10
aload 13
astore 11
L11:
aload 13
aload 18
iconst_0
iload 5
invokevirtual java/io/OutputStream/write([BII)V
L12:
aload 12
astore 8
aload 13
astore 9
aload 12
astore 6
aload 13
astore 7
aload 12
astore 10
aload 13
astore 11
L13:
aload 1
iload 5
i2l
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/updateWorkCompleted(J)V
L14:
aload 12
astore 8
aload 13
astore 9
aload 12
astore 6
aload 13
astore 7
aload 12
astore 10
aload 13
astore 11
L15:
aload 1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/isCancelAllTasks()Z
ifeq L8
L16:
aload 12
astore 8
aload 13
astore 9
aload 12
astore 6
aload 13
astore 7
aload 12
astore 10
aload 13
astore 11
L17:
aload 1
iconst_3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setResult(I)V
L18:
aload 12
astore 8
aload 13
astore 9
aload 12
astore 6
aload 13
astore 7
aload 12
astore 10
aload 13
astore 11
L19:
aload 1
iconst_0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
L20:
aload 0
aload 12
aload 13
invokespecial net/lingala/zip4j/unzip/UnzipEngine/closeStreams(Ljava/io/InputStream;Ljava/io/OutputStream;)V
return
L30:
aload 12
astore 8
aload 13
astore 9
aload 12
astore 6
aload 13
astore 7
aload 12
astore 10
aload 13
astore 11
L21:
aload 0
aload 12
aload 13
invokespecial net/lingala/zip4j/unzip/UnzipEngine/closeStreams(Ljava/io/InputStream;Ljava/io/OutputStream;)V
L22:
aload 12
astore 8
aload 13
astore 9
aload 12
astore 6
aload 13
astore 7
aload 12
astore 10
aload 13
astore 11
L23:
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/fileHeader Lnet/lingala/zip4j/model/FileHeader;
new java/io/File
dup
aload 0
aload 2
aload 3
invokespecial net/lingala/zip4j/unzip/UnzipEngine/getOutputFileNameWithPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 4
invokestatic net/lingala/zip4j/unzip/UnzipUtil/applyFileAttributes(Lnet/lingala/zip4j/model/FileHeader;Ljava/io/File;Lnet/lingala/zip4j/model/UnzipParameters;)V
L24:
aload 0
aload 12
aload 13
invokespecial net/lingala/zip4j/unzip/UnzipEngine/closeStreams(Ljava/io/InputStream;Ljava/io/OutputStream;)V
return
L2:
astore 1
aload 8
astore 6
aload 9
astore 7
L25:
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L4:
astore 1
aload 0
aload 6
aload 7
invokespecial net/lingala/zip4j/unzip/UnzipEngine/closeStreams(Ljava/io/InputStream;Ljava/io/OutputStream;)V
aload 1
athrow
L3:
astore 1
aload 10
astore 6
aload 11
astore 7
L26:
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L27:
.limit locals 19
.limit stack 6
.end method

.method public updateCRC(I)V
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/crc Ljava/util/zip/CRC32;
iload 1
invokevirtual java/util/zip/CRC32/update(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public updateCRC([BII)V
aload 1
ifnull L0
aload 0
getfield net/lingala/zip4j/unzip/UnzipEngine/crc Ljava/util/zip/CRC32;
aload 1
iload 2
iload 3
invokevirtual java/util/zip/CRC32/update([BII)V
L0:
return
.limit locals 4
.limit stack 4
.end method
