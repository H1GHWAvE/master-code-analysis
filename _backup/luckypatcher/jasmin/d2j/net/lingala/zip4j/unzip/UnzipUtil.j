.bytecode 50.0
.class public synchronized net/lingala/zip4j/unzip/UnzipUtil
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static applyFileAttributes(Lnet/lingala/zip4j/model/FileHeader;Ljava/io/File;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
aconst_null
invokestatic net/lingala/zip4j/unzip/UnzipUtil/applyFileAttributes(Lnet/lingala/zip4j/model/FileHeader;Ljava/io/File;Lnet/lingala/zip4j/model/UnzipParameters;)V
return
.limit locals 2
.limit stack 3
.end method

.method public static applyFileAttributes(Lnet/lingala/zip4j/model/FileHeader;Ljava/io/File;Lnet/lingala/zip4j/model/UnzipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "cannot set file properties: file header is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
ifnonnull L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "cannot set file properties: output file is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/io/File;)Z
ifne L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "cannot set file properties: file doesnot exist"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 2
ifnull L3
aload 2
invokevirtual net/lingala/zip4j/model/UnzipParameters/isIgnoreDateTimeAttributes()Z
ifne L4
L3:
aload 0
aload 1
invokestatic net/lingala/zip4j/unzip/UnzipUtil/setFileLastModifiedTime(Lnet/lingala/zip4j/model/FileHeader;Ljava/io/File;)V
L4:
aload 2
ifnonnull L5
aload 0
aload 1
iconst_1
iconst_1
iconst_1
iconst_1
invokestatic net/lingala/zip4j/unzip/UnzipUtil/setFileAttributes(Lnet/lingala/zip4j/model/FileHeader;Ljava/io/File;ZZZZ)V
return
L5:
aload 2
invokevirtual net/lingala/zip4j/model/UnzipParameters/isIgnoreAllFileAttributes()Z
ifeq L6
aload 0
aload 1
iconst_0
iconst_0
iconst_0
iconst_0
invokestatic net/lingala/zip4j/unzip/UnzipUtil/setFileAttributes(Lnet/lingala/zip4j/model/FileHeader;Ljava/io/File;ZZZZ)V
return
L6:
aload 2
invokevirtual net/lingala/zip4j/model/UnzipParameters/isIgnoreReadOnlyFileAttribute()Z
ifne L7
iconst_1
istore 3
L8:
aload 2
invokevirtual net/lingala/zip4j/model/UnzipParameters/isIgnoreHiddenFileAttribute()Z
ifne L9
iconst_1
istore 4
L10:
aload 2
invokevirtual net/lingala/zip4j/model/UnzipParameters/isIgnoreArchiveFileAttribute()Z
ifne L11
iconst_1
istore 5
L12:
aload 2
invokevirtual net/lingala/zip4j/model/UnzipParameters/isIgnoreSystemFileAttribute()Z
ifne L13
iconst_1
istore 6
L14:
aload 0
aload 1
iload 3
iload 4
iload 5
iload 6
invokestatic net/lingala/zip4j/unzip/UnzipUtil/setFileAttributes(Lnet/lingala/zip4j/model/FileHeader;Ljava/io/File;ZZZZ)V
return
L7:
iconst_0
istore 3
goto L8
L9:
iconst_0
istore 4
goto L10
L11:
iconst_0
istore 5
goto L12
L13:
iconst_0
istore 6
goto L14
.limit locals 7
.limit stack 6
.end method

.method private static setFileAttributes(Lnet/lingala/zip4j/model/FileHeader;Ljava/io/File;ZZZZ)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid file header. cannot set file attributes"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual net/lingala/zip4j/model/FileHeader/getExternalFileAttr()[B
astore 0
aload 0
ifnonnull L1
L2:
return
L1:
aload 0
iconst_0
baload
lookupswitch
1 : L3
2 : L4
3 : L5
18 : L4
32 : L6
33 : L7
34 : L8
35 : L9
38 : L10
48 : L6
50 : L8
default : L11
L11:
return
L3:
iload 2
ifeq L2
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileReadOnly(Ljava/io/File;)V
return
L4:
iload 3
ifeq L2
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileHidden(Ljava/io/File;)V
return
L6:
iload 4
ifeq L2
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileArchive(Ljava/io/File;)V
return
L5:
iload 2
ifeq L12
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileReadOnly(Ljava/io/File;)V
L12:
iload 3
ifeq L2
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileHidden(Ljava/io/File;)V
return
L7:
iload 4
ifeq L13
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileArchive(Ljava/io/File;)V
L13:
iload 2
ifeq L2
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileReadOnly(Ljava/io/File;)V
return
L8:
iload 4
ifeq L14
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileArchive(Ljava/io/File;)V
L14:
iload 3
ifeq L2
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileHidden(Ljava/io/File;)V
return
L9:
iload 4
ifeq L15
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileArchive(Ljava/io/File;)V
L15:
iload 2
ifeq L16
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileReadOnly(Ljava/io/File;)V
L16:
iload 3
ifeq L2
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileHidden(Ljava/io/File;)V
return
L10:
iload 2
ifeq L17
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileReadOnly(Ljava/io/File;)V
L17:
iload 3
ifeq L18
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileHidden(Ljava/io/File;)V
L18:
iload 5
ifeq L2
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/setFileSystemMode(Ljava/io/File;)V
return
.limit locals 6
.limit stack 3
.end method

.method private static setFileLastModifiedTime(Lnet/lingala/zip4j/model/FileHeader;Ljava/io/File;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokevirtual net/lingala/zip4j/model/FileHeader/getLastModFileTime()I
ifgt L0
L1:
return
L0:
aload 1
invokevirtual java/io/File/exists()Z
ifeq L1
aload 1
aload 0
invokevirtual net/lingala/zip4j/model/FileHeader/getLastModFileTime()I
invokestatic net/lingala/zip4j/util/Zip4jUtil/dosToJavaTme(I)J
invokevirtual java/io/File/setLastModified(J)Z
pop
return
.limit locals 2
.limit stack 3
.end method
