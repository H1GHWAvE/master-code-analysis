.bytecode 50.0
.class public synchronized net/lingala/zip4j/zip/ZipEngine
.super java/lang/Object
.inner class inner net/lingala/zip4j/zip/ZipEngine$1

.field private 'zipModel' Lnet/lingala/zip4j/model/ZipModel;

.method public <init>(Lnet/lingala/zip4j/model/ZipModel;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip model is null in ZipEngine constructor"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
return
.limit locals 2
.limit stack 3
.end method

.method static synthetic access$000(Lnet/lingala/zip4j/zip/ZipEngine;Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
aload 2
aload 3
invokespecial net/lingala/zip4j/zip/ZipEngine/initAddFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
return
.limit locals 4
.limit stack 4
.end method

.method private calculateTotalWork(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;)J
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "file list is null, cannot calculate total work"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
lconst_0
lstore 6
iconst_0
istore 3
L1:
iload 3
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpge L2
lload 6
lstore 4
aload 1
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
instanceof java/io/File
ifeq L3
lload 6
lstore 4
aload 1
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokevirtual java/io/File/exists()Z
ifeq L3
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/isEncryptFiles()Z
ifeq L4
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
ifne L4
lload 6
aload 1
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileLengh(Ljava/io/File;)J
ldc2_w 2L
lmul
ladd
lstore 6
L5:
lload 6
lstore 4
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnull L3
lload 6
lstore 4
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
ifnull L3
lload 6
lstore 4
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L3
aload 1
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/getRootFolderInZip()Ljava/lang/String;
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/getDefaultFolderPath()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/getRelativeFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 8
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 8
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileHeader(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Lnet/lingala/zip4j/model/FileHeader;
astore 8
lload 6
lstore 4
aload 8
ifnull L3
lload 6
new java/io/File
dup
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileLengh(Ljava/io/File;)J
aload 8
invokevirtual net/lingala/zip4j/model/FileHeader/getCompressedSize()J
lsub
ladd
lstore 4
L3:
iload 3
iconst_1
iadd
istore 3
lload 4
lstore 6
goto L1
L4:
lload 6
aload 1
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileLengh(Ljava/io/File;)J
ladd
lstore 6
goto L5
L2:
lload 6
lreturn
.limit locals 9
.limit stack 6
.end method

.method private checkParameters(Lnet/lingala/zip4j/model/ZipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "cannot validate zip parameters"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionMethod()I
ifeq L1
aload 1
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionMethod()I
bipush 8
if_icmpeq L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "unsupported compression type"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionMethod()I
bipush 8
if_icmpne L2
aload 1
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionLevel()I
ifge L2
aload 1
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionLevel()I
bipush 9
if_icmple L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid compression level. compression level dor deflate should be in the range of 0-9"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 1
invokevirtual net/lingala/zip4j/model/ZipParameters/isEncryptFiles()Z
ifeq L3
aload 1
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
ifeq L4
aload 1
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
bipush 99
if_icmpeq L4
new net/lingala/zip4j/exception/ZipException
dup
ldc "unsupported encryption method"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 1
invokevirtual net/lingala/zip4j/model/ZipParameters/getPassword()[C
ifnull L5
aload 1
invokevirtual net/lingala/zip4j/model/ZipParameters/getPassword()[C
arraylength
ifgt L6
L5:
new net/lingala/zip4j/exception/ZipException
dup
ldc "input password is empty or null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 1
iconst_m1
invokevirtual net/lingala/zip4j/model/ZipParameters/setAesKeyStrength(I)V
aload 1
iconst_m1
invokevirtual net/lingala/zip4j/model/ZipParameters/setEncryptionMethod(I)V
L6:
return
.limit locals 2
.limit stack 3
.end method

.method private createEndOfCentralDirectoryRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
new net/lingala/zip4j/model/EndCentralDirRecord
dup
invokespecial net/lingala/zip4j/model/EndCentralDirRecord/<init>()V
astore 1
aload 1
ldc2_w 101010256L
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setSignature(J)V
aload 1
iconst_0
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setNoOfThisDisk(I)V
aload 1
iconst_0
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setTotNoOfEntriesInCentralDir(I)V
aload 1
iconst_0
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setTotNoOfEntriesInCentralDirOnThisDisk(I)V
aload 1
lconst_0
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setOffsetOfStartOfCentralDir(J)V
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private initAddFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
.throws net/lingala/zip4j/exception/ZipException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch all from L0 to L1 using L4
.catch net/lingala/zip4j/exception/ZipException from L5 to L6 using L2
.catch java/lang/Exception from L5 to L6 using L3
.catch all from L5 to L6 using L4
.catch net/lingala/zip4j/exception/ZipException from L7 to L8 using L2
.catch java/lang/Exception from L7 to L8 using L3
.catch all from L7 to L8 using L4
.catch net/lingala/zip4j/exception/ZipException from L9 to L10 using L2
.catch java/lang/Exception from L9 to L10 using L3
.catch all from L9 to L10 using L4
.catch net/lingala/zip4j/exception/ZipException from L11 to L12 using L2
.catch java/lang/Exception from L11 to L12 using L3
.catch all from L11 to L12 using L4
.catch net/lingala/zip4j/exception/ZipException from L13 to L14 using L15
.catch java/lang/Exception from L13 to L14 using L16
.catch all from L13 to L14 using L17
.catch net/lingala/zip4j/exception/ZipException from L18 to L15 using L15
.catch java/lang/Exception from L18 to L15 using L16
.catch all from L18 to L15 using L17
.catch all from L19 to L20 using L4
.catch all from L21 to L4 using L4
.catch java/io/IOException from L22 to L23 using L24
.catch java/io/IOException from L25 to L26 using L27
.catch net/lingala/zip4j/exception/ZipException from L28 to L29 using L15
.catch java/lang/Exception from L28 to L29 using L16
.catch all from L28 to L29 using L17
.catch net/lingala/zip4j/exception/ZipException from L30 to L31 using L15
.catch java/lang/Exception from L30 to L31 using L16
.catch all from L30 to L31 using L17
.catch net/lingala/zip4j/exception/ZipException from L32 to L33 using L34
.catch java/lang/Exception from L32 to L33 using L35
.catch all from L32 to L33 using L36
.catch java/io/IOException from L37 to L38 using L39
.catch java/io/IOException from L40 to L41 using L42
.catch net/lingala/zip4j/exception/ZipException from L43 to L44 using L34
.catch java/lang/Exception from L43 to L44 using L35
.catch all from L43 to L44 using L36
.catch java/io/IOException from L45 to L46 using L47
.catch java/io/IOException from L48 to L49 using L50
.catch net/lingala/zip4j/exception/ZipException from L51 to L52 using L34
.catch java/lang/Exception from L51 to L52 using L35
.catch all from L51 to L52 using L36
.catch net/lingala/zip4j/exception/ZipException from L52 to L53 using L34
.catch java/lang/Exception from L52 to L53 using L35
.catch all from L52 to L53 using L36
.catch net/lingala/zip4j/exception/ZipException from L54 to L55 using L34
.catch java/lang/Exception from L54 to L55 using L35
.catch all from L54 to L55 using L36
.catch net/lingala/zip4j/exception/ZipException from L56 to L57 using L34
.catch java/lang/Exception from L56 to L57 using L35
.catch all from L56 to L57 using L36
.catch net/lingala/zip4j/exception/ZipException from L58 to L59 using L15
.catch java/lang/Exception from L58 to L59 using L16
.catch all from L58 to L59 using L17
.catch net/lingala/zip4j/exception/ZipException from L60 to L61 using L15
.catch java/lang/Exception from L60 to L61 using L16
.catch all from L60 to L61 using L17
.catch net/lingala/zip4j/exception/ZipException from L62 to L63 using L15
.catch java/lang/Exception from L62 to L63 using L16
.catch all from L62 to L63 using L17
.catch net/lingala/zip4j/exception/ZipException from L64 to L65 using L15
.catch java/lang/Exception from L64 to L65 using L16
.catch all from L64 to L65 using L17
.catch java/io/IOException from L66 to L67 using L68
.catch java/io/IOException from L69 to L70 using L71
.catch net/lingala/zip4j/exception/ZipException from L72 to L73 using L15
.catch java/lang/Exception from L72 to L73 using L16
.catch all from L72 to L73 using L17
.catch net/lingala/zip4j/exception/ZipException from L74 to L75 using L15
.catch java/lang/Exception from L74 to L75 using L16
.catch all from L74 to L75 using L17
.catch all from L76 to L77 using L4
.catch all from L78 to L79 using L4
.catch net/lingala/zip4j/exception/ZipException from L80 to L81 using L15
.catch java/lang/Exception from L80 to L81 using L16
.catch all from L80 to L81 using L17
.catch net/lingala/zip4j/exception/ZipException from L82 to L83 using L15
.catch java/lang/Exception from L82 to L83 using L16
.catch all from L82 to L83 using L17
.catch net/lingala/zip4j/exception/ZipException from L84 to L85 using L34
.catch java/lang/Exception from L84 to L85 using L35
.catch all from L84 to L85 using L36
.catch java/io/IOException from L86 to L87 using L88
.catch java/io/IOException from L89 to L90 using L91
aload 1
ifnull L92
aload 2
ifnonnull L93
L92:
new net/lingala/zip4j/exception/ZipException
dup
ldc "one of the input parameters is null when adding files"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L93:
aload 1
invokevirtual java/util/ArrayList/size()I
ifgt L94
new net/lingala/zip4j/exception/ZipException
dup
ldc "no files to add"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L94:
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
ifnonnull L95
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
invokespecial net/lingala/zip4j/zip/ZipEngine/createEndOfCentralDirectoryRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/ZipModel/setEndCentralDirRecord(Lnet/lingala/zip4j/model/EndCentralDirRecord;)V
L95:
aconst_null
astore 8
aconst_null
astore 11
aconst_null
astore 10
aconst_null
astore 15
aconst_null
astore 17
aconst_null
astore 13
aconst_null
astore 16
aconst_null
astore 14
aconst_null
astore 12
aload 17
astore 7
aload 8
astore 9
L0:
aload 0
aload 2
invokespecial net/lingala/zip4j/zip/ZipEngine/checkParameters(Lnet/lingala/zip4j/model/ZipParameters;)V
L1:
aload 17
astore 7
aload 8
astore 9
L5:
aload 0
aload 1
aload 2
aload 3
invokespecial net/lingala/zip4j/zip/ZipEngine/removeFilesIfExists(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
L6:
aload 17
astore 7
aload 8
astore 9
L7:
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
istore 6
L8:
aload 17
astore 7
aload 8
astore 9
L9:
new net/lingala/zip4j/io/SplitOutputStream
dup
new java/io/File
dup
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getSplitLength()J
invokespecial net/lingala/zip4j/io/SplitOutputStream/<init>(Ljava/io/File;J)V
astore 18
L10:
aload 17
astore 7
aload 8
astore 9
L11:
new net/lingala/zip4j/io/ZipOutputStream
dup
aload 18
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokespecial net/lingala/zip4j/io/ZipOutputStream/<init>(Ljava/io/OutputStream;Lnet/lingala/zip4j/model/ZipModel;)V
astore 8
L12:
iload 6
ifeq L29
aload 12
astore 7
aload 13
astore 10
aload 14
astore 11
L13:
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
ifnonnull L96
L14:
aload 12
astore 7
aload 13
astore 10
aload 14
astore 11
L18:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid end of central directory record"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L15:
astore 9
aload 8
astore 2
aload 7
astore 1
aload 9
astore 8
L97:
aload 1
astore 7
aload 2
astore 9
L19:
aload 3
aload 8
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorError(Ljava/lang/Throwable;)V
L20:
aload 1
astore 7
aload 2
astore 9
L21:
aload 8
athrow
L4:
astore 1
L98:
aload 7
ifnull L23
L22:
aload 7
invokevirtual java/io/InputStream/close()V
L23:
aload 9
ifnull L26
L25:
aload 9
invokevirtual net/lingala/zip4j/io/ZipOutputStream/close()V
L26:
aload 1
athrow
L96:
aload 12
astore 7
aload 13
astore 10
aload 14
astore 11
L28:
aload 18
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getOffsetOfStartOfCentralDir()J
invokevirtual net/lingala/zip4j/io/SplitOutputStream/seek(J)V
L29:
aload 12
astore 7
aload 13
astore 10
aload 14
astore 11
L30:
sipush 4096
newarray byte
astore 12
L31:
iconst_0
istore 4
aconst_null
astore 7
L32:
iload 4
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpge L84
aload 3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/isCancelAllTasks()Z
ifeq L43
aload 3
iconst_3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setResult(I)V
aload 3
iconst_0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
L33:
aload 7
ifnull L38
L37:
aload 7
invokevirtual java/io/InputStream/close()V
L38:
aload 8
ifnull L41
L40:
aload 8
invokevirtual net/lingala/zip4j/io/ZipOutputStream/close()V
L41:
return
L43:
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/clone()Ljava/lang/Object;
checkcast net/lingala/zip4j/model/ZipParameters
astore 9
aload 3
aload 1
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setFileName(Ljava/lang/String;)V
aload 1
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokevirtual java/io/File/isDirectory()Z
ifne L52
aload 9
invokevirtual net/lingala/zip4j/model/ZipParameters/isEncryptFiles()Z
ifeq L51
aload 9
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
ifne L51
aload 3
iconst_3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setCurrentOperation(I)V
aload 9
aload 1
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 3
invokestatic net/lingala/zip4j/util/CRCUtil/computeFileCRC(Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;)J
l2i
invokevirtual net/lingala/zip4j/model/ZipParameters/setSourceFileCRC(I)V
aload 3
iconst_0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setCurrentOperation(I)V
aload 3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/isCancelAllTasks()Z
ifeq L51
aload 3
iconst_3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setResult(I)V
aload 3
iconst_0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
L44:
aload 7
ifnull L46
L45:
aload 7
invokevirtual java/io/InputStream/close()V
L46:
aload 8
ifnull L49
L48:
aload 8
invokevirtual net/lingala/zip4j/io/ZipOutputStream/close()V
L49:
return
L51:
aload 1
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileLengh(Ljava/io/File;)J
lconst_0
lcmp
ifne L52
aload 9
iconst_0
invokevirtual net/lingala/zip4j/model/ZipParameters/setCompressionMethod(I)V
L52:
aload 1
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
astore 10
aload 10
invokevirtual java/io/File/isFile()Z
ifne L54
aload 10
invokevirtual java/io/File/isDirectory()Z
ifne L54
L53:
goto L99
L54:
aload 8
aload 1
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
aload 9
invokevirtual net/lingala/zip4j/io/ZipOutputStream/putNextEntry(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
aload 1
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokevirtual java/io/File/isDirectory()Z
ifeq L56
aload 8
invokevirtual net/lingala/zip4j/io/ZipOutputStream/closeEntry()V
L55:
goto L99
L56:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 1
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/FileInputStream
dup
aload 1
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 9
L57:
aload 9
astore 7
aload 9
astore 10
aload 9
astore 11
L58:
aload 9
aload 12
invokevirtual java/io/InputStream/read([B)I
istore 5
L59:
iload 5
iconst_m1
if_icmpeq L79
aload 9
astore 7
aload 9
astore 10
aload 9
astore 11
L60:
aload 3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/isCancelAllTasks()Z
ifeq L100
L61:
aload 9
astore 7
aload 9
astore 10
aload 9
astore 11
L62:
aload 3
iconst_3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setResult(I)V
L63:
aload 9
astore 7
aload 9
astore 10
aload 9
astore 11
L64:
aload 3
iconst_0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
L65:
aload 9
ifnull L67
L66:
aload 9
invokevirtual java/io/InputStream/close()V
L67:
aload 8
ifnull L41
L69:
aload 8
invokevirtual net/lingala/zip4j/io/ZipOutputStream/close()V
L70:
return
L71:
astore 1
return
L100:
aload 9
astore 7
aload 9
astore 10
aload 9
astore 11
L72:
aload 8
aload 12
iconst_0
iload 5
invokevirtual net/lingala/zip4j/io/ZipOutputStream/write([BII)V
L73:
aload 9
astore 7
aload 9
astore 10
aload 9
astore 11
L74:
aload 3
iload 5
i2l
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/updateWorkCompleted(J)V
L75:
goto L57
L16:
astore 7
aload 8
astore 2
aload 10
astore 1
aload 7
astore 8
L101:
aload 1
astore 7
aload 2
astore 9
L76:
aload 3
aload 8
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorError(Ljava/lang/Throwable;)V
L77:
aload 1
astore 7
aload 2
astore 9
L78:
new net/lingala/zip4j/exception/ZipException
dup
aload 8
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L79:
aload 9
astore 7
aload 9
astore 10
aload 9
astore 11
L80:
aload 8
invokevirtual net/lingala/zip4j/io/ZipOutputStream/closeEntry()V
L81:
aload 9
astore 7
aload 9
ifnull L99
aload 9
astore 7
aload 9
astore 10
aload 9
astore 11
L82:
aload 9
invokevirtual java/io/InputStream/close()V
L83:
aload 9
astore 7
goto L99
L17:
astore 1
aload 11
astore 7
aload 8
astore 9
goto L98
L84:
aload 8
invokevirtual net/lingala/zip4j/io/ZipOutputStream/finish()V
aload 3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorSuccess()V
L85:
aload 7
ifnull L87
L86:
aload 7
invokevirtual java/io/InputStream/close()V
L87:
aload 8
ifnull L90
L89:
aload 8
invokevirtual net/lingala/zip4j/io/ZipOutputStream/close()V
L90:
return
L39:
astore 1
goto L38
L42:
astore 1
goto L41
L47:
astore 1
goto L46
L50:
astore 1
goto L49
L68:
astore 1
goto L67
L88:
astore 1
goto L87
L91:
astore 1
goto L90
L24:
astore 2
goto L23
L27:
astore 2
goto L26
L36:
astore 1
aload 8
astore 9
goto L98
L3:
astore 8
aload 16
astore 1
aload 11
astore 2
goto L101
L35:
astore 9
aload 7
astore 1
aload 8
astore 2
aload 9
astore 8
goto L101
L2:
astore 8
aload 15
astore 1
aload 10
astore 2
goto L97
L34:
astore 9
aload 7
astore 1
aload 8
astore 2
aload 9
astore 8
goto L97
L99:
iload 4
iconst_1
iadd
istore 4
goto L32
.limit locals 19
.limit stack 5
.end method

.method private prepareFileOutputStream()Ljava/io/RandomAccessFile;
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/FileNotFoundException from L1 to L3 using L2
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
astore 1
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid output path"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/getParentFile()Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifne L1
aload 1
invokevirtual java/io/File/getParentFile()Ljava/io/File;
invokevirtual java/io/File/mkdirs()Z
pop
L1:
new java/io/RandomAccessFile
dup
aload 1
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 1
L3:
aload 1
areturn
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 4
.end method

.method private removeFilesIfExists(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch java/io/IOException from L6 to L7 using L2
.catch all from L6 to L7 using L3
.catch java/io/IOException from L8 to L9 using L2
.catch all from L8 to L9 using L3
.catch java/io/IOException from L10 to L11 using L2
.catch all from L10 to L11 using L3
.catch java/io/IOException from L12 to L13 using L2
.catch all from L12 to L13 using L3
.catch java/io/IOException from L14 to L15 using L2
.catch all from L14 to L15 using L3
.catch java/io/IOException from L16 to L17 using L2
.catch all from L16 to L17 using L3
.catch java/io/IOException from L18 to L19 using L2
.catch all from L18 to L19 using L3
.catch java/io/IOException from L20 to L21 using L2
.catch all from L20 to L21 using L3
.catch java/io/IOException from L22 to L23 using L24
.catch java/io/IOException from L25 to L26 using L2
.catch all from L25 to L26 using L3
.catch java/io/IOException from L27 to L28 using L2
.catch all from L27 to L28 using L3
.catch java/io/IOException from L29 to L30 using L2
.catch all from L29 to L30 using L3
.catch java/lang/NumberFormatException from L31 to L32 using L33
.catch java/lang/Exception from L31 to L32 using L34
.catch java/io/IOException from L31 to L32 using L2
.catch all from L31 to L32 using L3
.catch java/io/IOException from L35 to L36 using L2
.catch all from L35 to L36 using L3
.catch java/io/IOException from L37 to L2 using L2
.catch all from L37 to L2 using L3
.catch all from L38 to L3 using L3
.catch java/io/IOException from L39 to L40 using L41
.catch java/io/IOException from L42 to L43 using L2
.catch all from L42 to L43 using L3
.catch java/io/IOException from L44 to L45 using L46
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnull L47
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnull L47
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
ifnull L47
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifgt L48
L47:
return
L48:
aconst_null
astore 8
iconst_0
istore 4
L49:
aload 8
astore 10
aload 8
astore 9
L0:
iload 4
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpge L43
L1:
aload 8
astore 10
aload 8
astore 9
L4:
aload 1
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/getRootFolderInZip()Ljava/lang/String;
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/getDefaultFolderPath()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/getRelativeFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 7
L5:
aload 8
astore 10
aload 8
astore 9
L6:
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 7
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileHeader(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Lnet/lingala/zip4j/model/FileHeader;
astore 11
L7:
aload 8
astore 9
aload 11
ifnull L50
aload 8
astore 7
aload 8
ifnull L51
aload 8
astore 10
aload 8
astore 9
L8:
aload 8
invokevirtual java/io/RandomAccessFile/close()V
L9:
aconst_null
astore 7
L51:
aload 7
astore 10
aload 7
astore 9
L10:
new net/lingala/zip4j/util/ArchiveMaintainer
dup
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/<init>()V
astore 8
L11:
aload 7
astore 10
aload 7
astore 9
L12:
aload 3
iconst_2
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setCurrentOperation(I)V
L13:
aload 7
astore 10
aload 7
astore 9
L14:
aload 8
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 11
aload 3
invokevirtual net/lingala/zip4j/util/ArchiveMaintainer/initRemoveZipFile(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;Lnet/lingala/zip4j/progress/ProgressMonitor;)Ljava/util/HashMap;
astore 8
L15:
aload 7
astore 10
aload 7
astore 9
L16:
aload 3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/isCancelAllTasks()Z
ifeq L52
L17:
aload 7
astore 10
aload 7
astore 9
L18:
aload 3
iconst_3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setResult(I)V
L19:
aload 7
astore 10
aload 7
astore 9
L20:
aload 3
iconst_0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
L21:
aload 7
ifnull L47
L22:
aload 7
invokevirtual java/io/RandomAccessFile/close()V
L23:
return
L24:
astore 1
return
L52:
aload 7
astore 10
aload 7
astore 9
L25:
aload 3
iconst_0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setCurrentOperation(I)V
L26:
aload 7
astore 9
aload 7
ifnonnull L50
aload 7
astore 10
aload 7
astore 9
L27:
aload 0
invokespecial net/lingala/zip4j/zip/ZipEngine/prepareFileOutputStream()Ljava/io/RandomAccessFile;
astore 7
L28:
aload 7
astore 9
aload 8
ifnull L50
aload 7
astore 10
aload 7
astore 9
L29:
aload 8
ldc "offsetCentralDir"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
astore 11
L30:
aload 7
astore 9
aload 11
ifnull L50
aload 7
astore 10
aload 7
astore 9
L31:
aload 8
ldc "offsetCentralDir"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 5
L32:
aload 7
astore 9
lload 5
lconst_0
lcmp
iflt L50
aload 7
astore 10
aload 7
astore 9
L35:
aload 7
lload 5
invokevirtual java/io/RandomAccessFile/seek(J)V
L36:
aload 7
astore 9
goto L50
L33:
astore 1
aload 7
astore 10
aload 7
astore 9
L37:
new net/lingala/zip4j/exception/ZipException
dup
ldc "NumberFormatException while parsing offset central directory. Cannot update already existing file header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
aload 10
astore 9
L38:
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 1
aload 9
ifnull L40
L39:
aload 9
invokevirtual java/io/RandomAccessFile/close()V
L40:
aload 1
athrow
L34:
astore 1
aload 7
astore 10
aload 7
astore 9
L42:
new net/lingala/zip4j/exception/ZipException
dup
ldc "Error while parsing offset central directory. Cannot update already existing file header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L43:
aload 8
ifnull L47
L44:
aload 8
invokevirtual java/io/RandomAccessFile/close()V
L45:
return
L46:
astore 1
return
L41:
astore 2
goto L40
L50:
iload 4
iconst_1
iadd
istore 4
aload 9
astore 8
goto L49
.limit locals 12
.limit stack 4
.end method

.method public addFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnull L0
aload 2
ifnonnull L1
L0:
new net/lingala/zip4j/exception/ZipException
dup
ldc "one of the input parameters is null when adding files"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
invokevirtual java/util/ArrayList/size()I
ifgt L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "no files to add"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 3
iconst_0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setCurrentOperation(I)V
aload 3
iconst_1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
aload 3
iconst_1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setResult(I)V
iload 4
ifeq L3
aload 3
aload 0
aload 1
aload 2
invokespecial net/lingala/zip4j/zip/ZipEngine/calculateTotalWork(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;)J
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setTotalWork(J)V
aload 3
aload 1
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setFileName(Ljava/lang/String;)V
new net/lingala/zip4j/zip/ZipEngine$1
dup
aload 0
ldc "Zip4j"
aload 1
aload 2
aload 3
invokespecial net/lingala/zip4j/zip/ZipEngine$1/<init>(Lnet/lingala/zip4j/zip/ZipEngine;Ljava/lang/String;Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
invokevirtual java/lang/Thread/start()V
return
L3:
aload 0
aload 1
aload 2
aload 3
invokespecial net/lingala/zip4j/zip/ZipEngine/initAddFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
return
.limit locals 5
.limit stack 7
.end method

.method public addFolderToZip(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnull L0
aload 2
ifnonnull L1
L0:
new net/lingala/zip4j/exception/ZipException
dup
ldc "one of the input parameters is null, cannot add folder to zip"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
ifne L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "input folder does not exist"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 1
invokevirtual java/io/File/isDirectory()Z
ifne L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file is not a folder, user addFileToZip method to add files"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 1
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileReadAccess(Ljava/lang/String;)Z
ifne L4
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "cannot read folder: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/isIncludeRootFolder()Z
ifeq L5
aload 1
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ifnull L6
aload 1
invokevirtual java/io/File/getAbsoluteFile()Ljava/io/File;
invokevirtual java/io/File/getParentFile()Ljava/io/File;
ifnull L7
aload 1
invokevirtual java/io/File/getAbsoluteFile()Ljava/io/File;
invokevirtual java/io/File/getParentFile()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
astore 5
L8:
aload 2
aload 5
invokevirtual net/lingala/zip4j/model/ZipParameters/setDefaultFolderPath(Ljava/lang/String;)V
aload 1
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/isReadHiddenFiles()Z
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFilesInDirectoryRec(Ljava/io/File;Z)Ljava/util/ArrayList;
astore 6
aload 6
astore 5
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/isIncludeRootFolder()Z
ifeq L9
aload 6
astore 5
aload 6
ifnonnull L10
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 5
L10:
aload 5
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L9:
aload 0
aload 5
aload 2
aload 3
iload 4
invokevirtual net/lingala/zip4j/zip/ZipEngine/addFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
return
L7:
ldc ""
astore 5
goto L8
L6:
aload 1
invokevirtual java/io/File/getParentFile()Ljava/io/File;
ifnull L11
aload 1
invokevirtual java/io/File/getParentFile()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
astore 5
L12:
goto L8
L11:
ldc ""
astore 5
goto L12
L5:
aload 1
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
astore 5
goto L8
.limit locals 7
.limit stack 5
.end method

.method public addStreamToZip(Ljava/io/InputStream;Lnet/lingala/zip4j/model/ZipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch all from L0 to L1 using L4
.catch net/lingala/zip4j/exception/ZipException from L5 to L6 using L2
.catch java/lang/Exception from L5 to L6 using L3
.catch all from L5 to L6 using L4
.catch net/lingala/zip4j/exception/ZipException from L7 to L8 using L2
.catch java/lang/Exception from L7 to L8 using L3
.catch all from L7 to L8 using L4
.catch net/lingala/zip4j/exception/ZipException from L9 to L10 using L2
.catch java/lang/Exception from L9 to L10 using L3
.catch all from L9 to L10 using L4
.catch net/lingala/zip4j/exception/ZipException from L11 to L12 using L12
.catch java/lang/Exception from L11 to L12 using L13
.catch all from L11 to L12 using L14
.catch all from L15 to L4 using L4
.catch java/io/IOException from L16 to L17 using L18
.catch net/lingala/zip4j/exception/ZipException from L19 to L20 using L12
.catch java/lang/Exception from L19 to L20 using L13
.catch all from L19 to L20 using L14
.catch net/lingala/zip4j/exception/ZipException from L20 to L21 using L12
.catch java/lang/Exception from L20 to L21 using L13
.catch all from L20 to L21 using L14
.catch net/lingala/zip4j/exception/ZipException from L21 to L22 using L12
.catch java/lang/Exception from L21 to L22 using L13
.catch all from L21 to L22 using L14
.catch net/lingala/zip4j/exception/ZipException from L23 to L24 using L12
.catch java/lang/Exception from L23 to L24 using L13
.catch all from L23 to L24 using L14
.catch all from L25 to L26 using L4
.catch net/lingala/zip4j/exception/ZipException from L26 to L27 using L12
.catch java/lang/Exception from L26 to L27 using L13
.catch all from L26 to L27 using L14
.catch java/io/IOException from L28 to L29 using L30
aload 1
ifnull L31
aload 2
ifnonnull L32
L31:
new net/lingala/zip4j/exception/ZipException
dup
ldc "one of the input parameters is null, cannot add stream to zip"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L32:
aconst_null
astore 6
aconst_null
astore 8
aconst_null
astore 7
aload 6
astore 5
L0:
aload 0
aload 2
invokespecial net/lingala/zip4j/zip/ZipEngine/checkParameters(Lnet/lingala/zip4j/model/ZipParameters;)V
L1:
aload 6
astore 5
L5:
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
istore 4
L6:
aload 6
astore 5
L7:
new net/lingala/zip4j/io/SplitOutputStream
dup
new java/io/File
dup
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getSplitLength()J
invokespecial net/lingala/zip4j/io/SplitOutputStream/<init>(Ljava/io/File;J)V
astore 9
L8:
aload 6
astore 5
L9:
new net/lingala/zip4j/io/ZipOutputStream
dup
aload 9
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokespecial net/lingala/zip4j/io/ZipOutputStream/<init>(Ljava/io/OutputStream;Lnet/lingala/zip4j/model/ZipModel;)V
astore 6
L10:
iload 4
ifeq L20
L11:
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
ifnonnull L19
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid end of central directory record"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L12:
astore 1
aload 6
astore 5
L15:
aload 1
athrow
L4:
astore 1
L33:
aload 5
ifnull L17
L16:
aload 5
invokevirtual net/lingala/zip4j/io/ZipOutputStream/close()V
L17:
aload 1
athrow
L19:
aload 9
aload 0
getfield net/lingala/zip4j/zip/ZipEngine/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getOffsetOfStartOfCentralDir()J
invokevirtual net/lingala/zip4j/io/SplitOutputStream/seek(J)V
L20:
sipush 4096
newarray byte
astore 5
aload 6
aconst_null
aload 2
invokevirtual net/lingala/zip4j/io/ZipOutputStream/putNextEntry(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/getFileNameInZip()Ljava/lang/String;
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L26
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/getFileNameInZip()Ljava/lang/String;
ldc "\\"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L26
L21:
aload 1
aload 5
invokevirtual java/io/InputStream/read([B)I
istore 3
L22:
iload 3
iconst_m1
if_icmpeq L26
L23:
aload 6
aload 5
iconst_0
iload 3
invokevirtual net/lingala/zip4j/io/ZipOutputStream/write([BII)V
L24:
goto L21
L13:
astore 1
aload 6
astore 5
L25:
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L26:
aload 6
invokevirtual net/lingala/zip4j/io/ZipOutputStream/closeEntry()V
aload 6
invokevirtual net/lingala/zip4j/io/ZipOutputStream/finish()V
L27:
aload 6
ifnull L29
L28:
aload 6
invokevirtual net/lingala/zip4j/io/ZipOutputStream/close()V
L29:
return
L30:
astore 1
return
L18:
astore 2
goto L17
L14:
astore 1
aload 6
astore 5
goto L33
L3:
astore 1
aload 8
astore 5
goto L25
L2:
astore 1
aload 7
astore 5
goto L15
.limit locals 10
.limit stack 5
.end method
