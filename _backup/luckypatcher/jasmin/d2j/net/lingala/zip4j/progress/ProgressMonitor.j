.bytecode 50.0
.class public synchronized net/lingala/zip4j/progress/ProgressMonitor
.super java/lang/Object

.field public static final 'OPERATION_ADD' I = 0


.field public static final 'OPERATION_CALC_CRC' I = 3


.field public static final 'OPERATION_EXTRACT' I = 1


.field public static final 'OPERATION_MERGE' I = 4


.field public static final 'OPERATION_NONE' I = -1


.field public static final 'OPERATION_REMOVE' I = 2


.field public static final 'RESULT_CANCELLED' I = 3


.field public static final 'RESULT_ERROR' I = 2


.field public static final 'RESULT_SUCCESS' I = 0


.field public static final 'RESULT_WORKING' I = 1


.field public static final 'STATE_BUSY' I = 1


.field public static final 'STATE_READY' I = 0


.field private 'cancelAllTasks' Z

.field private 'currentOperation' I

.field private 'exception' Ljava/lang/Throwable;

.field private 'fileName' Ljava/lang/String;

.field private 'pause' Z

.field private 'percentDone' I

.field private 'result' I

.field private 'state' I

.field private 'totalWork' J

.field private 'workCompleted' J

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/reset()V
aload 0
iconst_0
putfield net/lingala/zip4j/progress/ProgressMonitor/percentDone I
return
.limit locals 1
.limit stack 2
.end method

.method public cancelAllTasks()V
aload 0
iconst_1
putfield net/lingala/zip4j/progress/ProgressMonitor/cancelAllTasks Z
return
.limit locals 1
.limit stack 2
.end method

.method public endProgressMonitorError(Ljava/lang/Throwable;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/reset()V
aload 0
iconst_2
putfield net/lingala/zip4j/progress/ProgressMonitor/result I
aload 0
aload 1
putfield net/lingala/zip4j/progress/ProgressMonitor/exception Ljava/lang/Throwable;
return
.limit locals 2
.limit stack 2
.end method

.method public endProgressMonitorSuccess()V
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/reset()V
aload 0
iconst_0
putfield net/lingala/zip4j/progress/ProgressMonitor/result I
return
.limit locals 1
.limit stack 2
.end method

.method public fullReset()V
aload 0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/reset()V
aload 0
aconst_null
putfield net/lingala/zip4j/progress/ProgressMonitor/exception Ljava/lang/Throwable;
aload 0
iconst_0
putfield net/lingala/zip4j/progress/ProgressMonitor/result I
return
.limit locals 1
.limit stack 2
.end method

.method public getCurrentOperation()I
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/currentOperation I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getException()Ljava/lang/Throwable;
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/exception Ljava/lang/Throwable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getFileName()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/fileName Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getPercentDone()I
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/percentDone I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getResult()I
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/result I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getState()I
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/state I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getTotalWork()J
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/totalWork J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getWorkCompleted()J
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/workCompleted J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public isCancelAllTasks()Z
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/cancelAllTasks Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isPause()Z
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/pause Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public reset()V
aload 0
iconst_m1
putfield net/lingala/zip4j/progress/ProgressMonitor/currentOperation I
aload 0
iconst_0
putfield net/lingala/zip4j/progress/ProgressMonitor/state I
aload 0
aconst_null
putfield net/lingala/zip4j/progress/ProgressMonitor/fileName Ljava/lang/String;
aload 0
lconst_0
putfield net/lingala/zip4j/progress/ProgressMonitor/totalWork J
aload 0
lconst_0
putfield net/lingala/zip4j/progress/ProgressMonitor/workCompleted J
aload 0
iconst_0
putfield net/lingala/zip4j/progress/ProgressMonitor/percentDone I
return
.limit locals 1
.limit stack 3
.end method

.method public setCurrentOperation(I)V
aload 0
iload 1
putfield net/lingala/zip4j/progress/ProgressMonitor/currentOperation I
return
.limit locals 2
.limit stack 2
.end method

.method public setException(Ljava/lang/Throwable;)V
aload 0
aload 1
putfield net/lingala/zip4j/progress/ProgressMonitor/exception Ljava/lang/Throwable;
return
.limit locals 2
.limit stack 2
.end method

.method public setFileName(Ljava/lang/String;)V
aload 0
aload 1
putfield net/lingala/zip4j/progress/ProgressMonitor/fileName Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setPause(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/progress/ProgressMonitor/pause Z
return
.limit locals 2
.limit stack 2
.end method

.method public setPercentDone(I)V
aload 0
iload 1
putfield net/lingala/zip4j/progress/ProgressMonitor/percentDone I
return
.limit locals 2
.limit stack 2
.end method

.method public setResult(I)V
aload 0
iload 1
putfield net/lingala/zip4j/progress/ProgressMonitor/result I
return
.limit locals 2
.limit stack 2
.end method

.method public setState(I)V
aload 0
iload 1
putfield net/lingala/zip4j/progress/ProgressMonitor/state I
return
.limit locals 2
.limit stack 2
.end method

.method public setTotalWork(J)V
aload 0
lload 1
putfield net/lingala/zip4j/progress/ProgressMonitor/totalWork J
return
.limit locals 3
.limit stack 3
.end method

.method public updateWorkCompleted(J)V
aload 0
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/workCompleted J
lload 1
ladd
putfield net/lingala/zip4j/progress/ProgressMonitor/workCompleted J
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/totalWork J
lconst_0
lcmp
ifle L0
aload 0
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/workCompleted J
ldc2_w 100L
lmul
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/totalWork J
ldiv
l2i
putfield net/lingala/zip4j/progress/ProgressMonitor/percentDone I
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/percentDone I
bipush 100
if_icmple L0
aload 0
bipush 100
putfield net/lingala/zip4j/progress/ProgressMonitor/percentDone I
L0:
aload 0
getfield net/lingala/zip4j/progress/ProgressMonitor/pause Z
ifeq L1
new com/chelpus/Utils
dup
ldc "w"
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
ldc2_w 150L
invokevirtual com/chelpus/Utils/waitLP(J)V
goto L0
L1:
return
.limit locals 3
.limit stack 5
.end method
