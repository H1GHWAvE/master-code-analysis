.bytecode 50.0
.class public synchronized net/lingala/zip4j/io/DeflaterOutputStream
.super net/lingala/zip4j/io/CipherOutputStream

.field private 'buff' [B

.field protected 'deflater' Ljava/util/zip/Deflater;

.field private 'firstBytesRead' Z

.method public <init>(Ljava/io/OutputStream;Lnet/lingala/zip4j/model/ZipModel;)V
aload 0
aload 1
aload 2
invokespecial net/lingala/zip4j/io/CipherOutputStream/<init>(Ljava/io/OutputStream;Lnet/lingala/zip4j/model/ZipModel;)V
aload 0
new java/util/zip/Deflater
dup
invokespecial java/util/zip/Deflater/<init>()V
putfield net/lingala/zip4j/io/DeflaterOutputStream/deflater Ljava/util/zip/Deflater;
aload 0
sipush 4096
newarray byte
putfield net/lingala/zip4j/io/DeflaterOutputStream/buff [B
aload 0
iconst_0
putfield net/lingala/zip4j/io/DeflaterOutputStream/firstBytesRead Z
return
.limit locals 3
.limit stack 3
.end method

.method private deflate()V
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/deflater Ljava/util/zip/Deflater;
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/buff [B
iconst_0
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/buff [B
arraylength
invokevirtual java/util/zip/Deflater/deflate([BII)I
istore 2
iload 2
ifle L0
iload 2
istore 1
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/deflater Ljava/util/zip/Deflater;
invokevirtual java/util/zip/Deflater/finished()Z
ifeq L1
iload 2
iconst_4
if_icmpne L2
L0:
return
L2:
iload 2
iconst_4
if_icmpge L3
aload 0
iconst_4
iload 2
isub
invokevirtual net/lingala/zip4j/io/DeflaterOutputStream/decrementCompressedFileSize(I)V
return
L3:
iload 2
iconst_4
isub
istore 1
L1:
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/firstBytesRead Z
ifne L4
aload 0
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/buff [B
iconst_2
iload 1
iconst_2
isub
invokespecial net/lingala/zip4j/io/CipherOutputStream/write([BII)V
aload 0
iconst_1
putfield net/lingala/zip4j/io/DeflaterOutputStream/firstBytesRead Z
return
L4:
aload 0
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/buff [B
iconst_0
iload 1
invokespecial net/lingala/zip4j/io/CipherOutputStream/write([BII)V
return
.limit locals 3
.limit stack 5
.end method

.method public closeEntry()V
.throws java/io/IOException
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionMethod()I
bipush 8
if_icmpne L0
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/deflater Ljava/util/zip/Deflater;
invokevirtual java/util/zip/Deflater/finished()Z
ifne L1
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/deflater Ljava/util/zip/Deflater;
invokevirtual java/util/zip/Deflater/finish()V
L2:
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/deflater Ljava/util/zip/Deflater;
invokevirtual java/util/zip/Deflater/finished()Z
ifne L1
aload 0
invokespecial net/lingala/zip4j/io/DeflaterOutputStream/deflate()V
goto L2
L1:
aload 0
iconst_0
putfield net/lingala/zip4j/io/DeflaterOutputStream/firstBytesRead Z
L0:
aload 0
invokespecial net/lingala/zip4j/io/CipherOutputStream/closeEntry()V
return
.limit locals 1
.limit stack 2
.end method

.method public finish()V
.throws java/io/IOException
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial net/lingala/zip4j/io/CipherOutputStream/finish()V
return
.limit locals 1
.limit stack 1
.end method

.method public putNextEntry(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
aload 2
invokespecial net/lingala/zip4j/io/CipherOutputStream/putNextEntry(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionMethod()I
bipush 8
if_icmpne L0
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/deflater Ljava/util/zip/Deflater;
invokevirtual java/util/zip/Deflater/reset()V
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionLevel()I
iflt L1
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionLevel()I
bipush 9
if_icmple L2
L1:
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionLevel()I
iconst_m1
if_icmpeq L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid compression level for deflater. compression level should be in the range of 0-9"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/deflater Ljava/util/zip/Deflater;
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionLevel()I
invokevirtual java/util/zip/Deflater/setLevel(I)V
L0:
return
.limit locals 3
.limit stack 3
.end method

.method public write(I)V
.throws java/io/IOException
aload 0
iconst_1
newarray byte
dup
iconst_0
iload 1
i2b
bastore
iconst_0
iconst_1
invokevirtual net/lingala/zip4j/io/DeflaterOutputStream/write([BII)V
return
.limit locals 2
.limit stack 5
.end method

.method public write([B)V
.throws java/io/IOException
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual net/lingala/zip4j/io/DeflaterOutputStream/write([BII)V
return
.limit locals 2
.limit stack 4
.end method

.method public write([BII)V
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionMethod()I
bipush 8
if_icmpeq L0
aload 0
aload 1
iload 2
iload 3
invokespecial net/lingala/zip4j/io/CipherOutputStream/write([BII)V
L1:
return
L0:
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/deflater Ljava/util/zip/Deflater;
aload 1
iload 2
iload 3
invokevirtual java/util/zip/Deflater/setInput([BII)V
L2:
aload 0
getfield net/lingala/zip4j/io/DeflaterOutputStream/deflater Ljava/util/zip/Deflater;
invokevirtual java/util/zip/Deflater/needsInput()Z
ifne L1
aload 0
invokespecial net/lingala/zip4j/io/DeflaterOutputStream/deflate()V
goto L2
.limit locals 4
.limit stack 4
.end method
