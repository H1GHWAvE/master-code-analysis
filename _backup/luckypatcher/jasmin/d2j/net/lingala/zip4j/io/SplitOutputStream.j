.bytecode 50.0
.class public synchronized net/lingala/zip4j/io/SplitOutputStream
.super java/io/OutputStream

.field private 'bytesWrittenForThisPart' J

.field private 'currSplitFileCounter' I

.field private 'outFile' Ljava/io/File;

.field private 'raf' Ljava/io/RandomAccessFile;

.field private 'splitLength' J

.field private 'zipFile' Ljava/io/File;

.method public <init>(Ljava/io/File;)V
.throws java/io/FileNotFoundException
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
ldc2_w -1L
invokespecial net/lingala/zip4j/io/SplitOutputStream/<init>(Ljava/io/File;J)V
return
.limit locals 2
.limit stack 4
.end method

.method public <init>(Ljava/io/File;J)V
.throws java/io/FileNotFoundException
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial java/io/OutputStream/<init>()V
lload 2
lconst_0
lcmp
iflt L0
lload 2
ldc2_w 65536L
lcmp
ifge L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "split length less than minimum allowed split length of 65536 Bytes"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
new java/io/RandomAccessFile
dup
aload 1
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
putfield net/lingala/zip4j/io/SplitOutputStream/raf Ljava/io/RandomAccessFile;
aload 0
lload 2
putfield net/lingala/zip4j/io/SplitOutputStream/splitLength J
aload 0
aload 1
putfield net/lingala/zip4j/io/SplitOutputStream/outFile Ljava/io/File;
aload 0
aload 1
putfield net/lingala/zip4j/io/SplitOutputStream/zipFile Ljava/io/File;
aload 0
iconst_0
putfield net/lingala/zip4j/io/SplitOutputStream/currSplitFileCounter I
aload 0
lconst_0
putfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
return
.limit locals 4
.limit stack 5
.end method

.method public <init>(Ljava/lang/String;)V
.throws java/io/FileNotFoundException
.throws net/lingala/zip4j/exception/ZipException
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifeq L0
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
L1:
aload 0
aload 1
invokespecial net/lingala/zip4j/io/SplitOutputStream/<init>(Ljava/io/File;)V
return
L0:
aconst_null
astore 1
goto L1
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/lang/String;J)V
.throws java/io/FileNotFoundException
.throws net/lingala/zip4j/exception/ZipException
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
L1:
aload 0
aload 1
lload 2
invokespecial net/lingala/zip4j/io/SplitOutputStream/<init>(Ljava/io/File;J)V
return
L0:
aconst_null
astore 1
goto L1
.limit locals 4
.limit stack 4
.end method

.method private isHeaderData([B)Z
aload 1
ifnull L0
aload 1
arraylength
iconst_4
if_icmpge L1
L0:
iconst_0
ireturn
L1:
aload 1
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
istore 3
invokestatic net/lingala/zip4j/util/Zip4jUtil/getAllHeaderSignatures()[J
astore 1
aload 1
ifnull L0
aload 1
arraylength
ifle L0
iconst_0
istore 2
L2:
iload 2
aload 1
arraylength
if_icmpge L0
aload 1
iload 2
laload
ldc2_w 134695760L
lcmp
ifeq L3
aload 1
iload 2
laload
iload 3
i2l
lcmp
ifne L3
iconst_1
ireturn
L3:
iload 2
iconst_1
iadd
istore 2
goto L2
.limit locals 4
.limit stack 4
.end method

.method private startNextSplitFile()V
.throws java/io/IOException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch net/lingala/zip4j/exception/ZipException from L3 to L4 using L2
.catch net/lingala/zip4j/exception/ZipException from L4 to L2 using L2
.catch net/lingala/zip4j/exception/ZipException from L5 to L6 using L2
.catch net/lingala/zip4j/exception/ZipException from L7 to L8 using L2
.catch net/lingala/zip4j/exception/ZipException from L9 to L10 using L2
.catch net/lingala/zip4j/exception/ZipException from L10 to L11 using L2
L0:
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/outFile Ljava/io/File;
invokevirtual java/io/File/getName()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/getZipFileNameWithoutExt(Ljava/lang/String;)Ljava/lang/String;
astore 3
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/zipFile Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
astore 2
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/outFile Ljava/io/File;
invokevirtual java/io/File/getParent()Ljava/lang/String;
ifnonnull L5
L1:
ldc ""
astore 1
L3:
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/currSplitFileCounter I
bipush 9
if_icmpge L7
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".z0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/currSplitFileCounter I
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
L4:
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/raf Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/close()V
aload 1
invokevirtual java/io/File/exists()Z
ifeq L9
new java/io/IOException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "split file: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " already exists in the current directory, cannot rename this file"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
new java/io/IOException
dup
aload 1
invokevirtual net/lingala/zip4j/exception/ZipException/getMessage()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L5:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/outFile Ljava/io/File;
invokevirtual java/io/File/getParent()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "file.separator"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L6:
goto L3
L7:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".z"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/currSplitFileCounter I
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
L8:
goto L4
L9:
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/zipFile Ljava/io/File;
aload 1
invokevirtual java/io/File/renameTo(Ljava/io/File;)Z
ifne L10
new java/io/IOException
dup
ldc "cannot rename newly created split file"
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L10:
aload 0
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putfield net/lingala/zip4j/io/SplitOutputStream/zipFile Ljava/io/File;
aload 0
new java/io/RandomAccessFile
dup
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/zipFile Ljava/io/File;
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
putfield net/lingala/zip4j/io/SplitOutputStream/raf Ljava/io/RandomAccessFile;
aload 0
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/currSplitFileCounter I
iconst_1
iadd
putfield net/lingala/zip4j/io/SplitOutputStream/currSplitFileCounter I
L11:
return
.limit locals 4
.limit stack 5
.end method

.method public checkBuffSizeAndStartNextSplitFile(I)Z
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
iload 1
ifge L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "negative buffersize for checkBuffSizeAndStartNextSplitFile"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
iload 1
invokevirtual net/lingala/zip4j/io/SplitOutputStream/isBuffSizeFitForCurrSplitFile(I)Z
ifne L4
L0:
aload 0
invokespecial net/lingala/zip4j/io/SplitOutputStream/startNextSplitFile()V
aload 0
lconst_0
putfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
L1:
iconst_1
ireturn
L2:
astore 2
new net/lingala/zip4j/exception/ZipException
dup
aload 2
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L4:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public close()V
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/raf Ljava/io/RandomAccessFile;
ifnull L0
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/raf Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/close()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method public flush()V
.throws java/io/IOException
return
.limit locals 1
.limit stack 0
.end method

.method public getCurrSplitFileCounter()I
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/currSplitFileCounter I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getFilePointer()J
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/raf Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/getFilePointer()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getSplitLength()J
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/splitLength J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public isBuffSizeFitForCurrSplitFile(I)Z
.throws net/lingala/zip4j/exception/ZipException
iload 1
ifge L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "negative buffersize for isBuffSizeFitForCurrSplitFile"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/splitLength J
ldc2_w 65536L
lcmp
iflt L1
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
iload 1
i2l
ladd
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/splitLength J
lcmp
ifgt L2
L1:
iconst_1
ireturn
L2:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method public isSplitZipFile()Z
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/splitLength J
ldc2_w -1L
lcmp
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 4
.end method

.method public seek(J)V
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/raf Ljava/io/RandomAccessFile;
lload 1
invokevirtual java/io/RandomAccessFile/seek(J)V
return
.limit locals 3
.limit stack 3
.end method

.method public write(I)V
.throws java/io/IOException
aload 0
iconst_1
newarray byte
dup
iconst_0
iload 1
i2b
bastore
iconst_0
iconst_1
invokevirtual net/lingala/zip4j/io/SplitOutputStream/write([BII)V
return
.limit locals 2
.limit stack 5
.end method

.method public write([B)V
.throws java/io/IOException
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual net/lingala/zip4j/io/SplitOutputStream/write([BII)V
return
.limit locals 2
.limit stack 4
.end method

.method public write([BII)V
.throws java/io/IOException
iload 3
ifgt L0
return
L0:
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/splitLength J
ldc2_w -1L
lcmp
ifeq L1
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/splitLength J
ldc2_w 65536L
lcmp
ifge L2
new java/io/IOException
dup
ldc "split length less than minimum allowed split length of 65536 Bytes"
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/splitLength J
lcmp
iflt L3
aload 0
invokespecial net/lingala/zip4j/io/SplitOutputStream/startNextSplitFile()V
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/raf Ljava/io/RandomAccessFile;
aload 1
iload 2
iload 3
invokevirtual java/io/RandomAccessFile/write([BII)V
aload 0
iload 3
i2l
putfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
return
L3:
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
iload 3
i2l
ladd
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/splitLength J
lcmp
ifle L4
aload 0
aload 1
invokespecial net/lingala/zip4j/io/SplitOutputStream/isHeaderData([B)Z
ifeq L5
aload 0
invokespecial net/lingala/zip4j/io/SplitOutputStream/startNextSplitFile()V
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/raf Ljava/io/RandomAccessFile;
aload 1
iload 2
iload 3
invokevirtual java/io/RandomAccessFile/write([BII)V
aload 0
iload 3
i2l
putfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
return
L5:
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/raf Ljava/io/RandomAccessFile;
aload 1
iload 2
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/splitLength J
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
lsub
l2i
invokevirtual java/io/RandomAccessFile/write([BII)V
aload 0
invokespecial net/lingala/zip4j/io/SplitOutputStream/startNextSplitFile()V
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/raf Ljava/io/RandomAccessFile;
aload 1
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/splitLength J
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
lsub
l2i
iload 2
iadd
iload 3
i2l
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/splitLength J
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
lsub
lsub
l2i
invokevirtual java/io/RandomAccessFile/write([BII)V
aload 0
iload 3
i2l
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/splitLength J
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
lsub
lsub
putfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
return
L4:
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/raf Ljava/io/RandomAccessFile;
aload 1
iload 2
iload 3
invokevirtual java/io/RandomAccessFile/write([BII)V
aload 0
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
iload 3
i2l
ladd
putfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
return
L1:
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/raf Ljava/io/RandomAccessFile;
aload 1
iload 2
iload 3
invokevirtual java/io/RandomAccessFile/write([BII)V
aload 0
aload 0
getfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
iload 3
i2l
ladd
putfield net/lingala/zip4j/io/SplitOutputStream/bytesWrittenForThisPart J
return
.limit locals 4
.limit stack 9
.end method
