.bytecode 50.0
.class public synchronized net/lingala/zip4j/io/CipherOutputStream
.super net/lingala/zip4j/io/BaseOutputStream

.field private 'bytesWrittenForThisFile' J

.field protected 'crc' Ljava/util/zip/CRC32;

.field private 'encrypter' Lnet/lingala/zip4j/crypto/IEncrypter;

.field protected 'fileHeader' Lnet/lingala/zip4j/model/FileHeader;

.field protected 'localFileHeader' Lnet/lingala/zip4j/model/LocalFileHeader;

.field protected 'outputStream' Ljava/io/OutputStream;

.field private 'pendingBuffer' [B

.field private 'pendingBufferLength' I

.field private 'sourceFile' Ljava/io/File;

.field private 'totalBytesRead' J

.field private 'totalBytesWritten' J

.field protected 'zipModel' Lnet/lingala/zip4j/model/ZipModel;

.field protected 'zipParameters' Lnet/lingala/zip4j/model/ZipParameters;

.method public <init>(Ljava/io/OutputStream;Lnet/lingala/zip4j/model/ZipModel;)V
aload 0
invokespecial net/lingala/zip4j/io/BaseOutputStream/<init>()V
aload 0
aload 1
putfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
aload 0
aload 2
invokespecial net/lingala/zip4j/io/CipherOutputStream/initZipModel(Lnet/lingala/zip4j/model/ZipModel;)V
aload 0
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
putfield net/lingala/zip4j/io/CipherOutputStream/crc Ljava/util/zip/CRC32;
aload 0
lconst_0
putfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
aload 0
lconst_0
putfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
aload 0
bipush 16
newarray byte
putfield net/lingala/zip4j/io/CipherOutputStream/pendingBuffer [B
aload 0
iconst_0
putfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
aload 0
lconst_0
putfield net/lingala/zip4j/io/CipherOutputStream/totalBytesRead J
return
.limit locals 3
.limit stack 3
.end method

.method private createFileHeader()V
.throws net/lingala/zip4j/exception/ZipException
aload 0
new net/lingala/zip4j/model/FileHeader
dup
invokespecial net/lingala/zip4j/model/FileHeader/<init>()V
putfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
ldc_w 33639248
invokevirtual net/lingala/zip4j/model/FileHeader/setSignature(I)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
bipush 20
invokevirtual net/lingala/zip4j/model/FileHeader/setVersionMadeBy(I)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
bipush 20
invokevirtual net/lingala/zip4j/model/FileHeader/setVersionNeededToExtract(I)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/isEncryptFiles()Z
ifeq L0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
bipush 99
if_icmpne L0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
bipush 99
invokevirtual net/lingala/zip4j/model/FileHeader/setCompressionMethod(I)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokespecial net/lingala/zip4j/io/CipherOutputStream/generateAESExtraDataRecord(Lnet/lingala/zip4j/model/ZipParameters;)Lnet/lingala/zip4j/model/AESExtraDataRecord;
invokevirtual net/lingala/zip4j/model/FileHeader/setAesExtraDataRecord(Lnet/lingala/zip4j/model/AESExtraDataRecord;)V
L1:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/isEncryptFiles()Z
ifeq L2
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
iconst_1
invokevirtual net/lingala/zip4j/model/FileHeader/setEncrypted(Z)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
invokevirtual net/lingala/zip4j/model/FileHeader/setEncryptionMethod(I)V
L2:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/isSourceExternalStream()Z
ifeq L3
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokestatic java/lang/System/currentTimeMillis()J
invokestatic net/lingala/zip4j/util/Zip4jUtil/javaToDosTime(J)J
l2i
invokevirtual net/lingala/zip4j/model/FileHeader/setLastModFileTime(I)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getFileNameInZip()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L4
new net/lingala/zip4j/exception/ZipException
dup
ldc "fileNameInZip is null or empty"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionMethod()I
invokevirtual net/lingala/zip4j/model/FileHeader/setCompressionMethod(I)V
goto L1
L4:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getFileNameInZip()Ljava/lang/String;
astore 6
L5:
aload 6
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L6
new net/lingala/zip4j/exception/ZipException
dup
ldc "fileName is null or empty. unable to create file header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/sourceFile Ljava/io/File;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getTimeZone()Ljava/util/TimeZone;
invokestatic net/lingala/zip4j/util/Zip4jUtil/getLastModifiedFileTime(Ljava/io/File;Ljava/util/TimeZone;)J
invokestatic net/lingala/zip4j/util/Zip4jUtil/javaToDosTime(J)J
l2i
invokevirtual net/lingala/zip4j/model/FileHeader/setLastModFileTime(I)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/sourceFile Ljava/io/File;
invokevirtual java/io/File/length()J
invokevirtual net/lingala/zip4j/model/FileHeader/setUncompressedSize(J)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/sourceFile Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getRootFolderInZip()Ljava/lang/String;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getDefaultFolderPath()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/getRelativeFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 6
goto L5
L6:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 6
invokevirtual net/lingala/zip4j/model/FileHeader/setFileName(Ljava/lang/String;)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getFileNameCharset()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifeq L7
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 6
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getFileNameCharset()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/getEncodedStringLength(Ljava/lang/String;Ljava/lang/String;)I
invokevirtual net/lingala/zip4j/model/FileHeader/setFileNameLength(I)V
L8:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
instanceof net/lingala/zip4j/io/SplitOutputStream
ifeq L9
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
checkcast net/lingala/zip4j/io/SplitOutputStream
invokevirtual net/lingala/zip4j/io/SplitOutputStream/getCurrSplitFileCounter()I
invokevirtual net/lingala/zip4j/model/FileHeader/setDiskNumberStart(I)V
L10:
iconst_0
istore 2
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/isSourceExternalStream()Z
ifne L11
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/sourceFile Ljava/io/File;
invokespecial net/lingala/zip4j/io/CipherOutputStream/getFileAttributes(Ljava/io/File;)I
istore 2
L11:
iload 2
i2b
istore 1
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
iconst_4
newarray byte
dup
iconst_0
iload 1
bastore
dup
iconst_1
iconst_0
bastore
dup
iconst_2
iconst_0
bastore
dup
iconst_3
iconst_0
bastore
invokevirtual net/lingala/zip4j/model/FileHeader/setExternalFileAttr([B)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/isSourceExternalStream()Z
ifeq L12
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
astore 7
aload 6
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L13
aload 6
ldc "\\"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L14
L13:
iconst_1
istore 3
L15:
aload 7
iload 3
invokevirtual net/lingala/zip4j/model/FileHeader/setDirectory(Z)V
L16:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/isDirectory()Z
ifeq L17
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
lconst_0
invokevirtual net/lingala/zip4j/model/FileHeader/setCompressedSize(J)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
lconst_0
invokevirtual net/lingala/zip4j/model/FileHeader/setUncompressedSize(J)V
L18:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/isEncryptFiles()Z
ifeq L19
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
ifne L19
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getSourceFileCRC()I
i2l
invokevirtual net/lingala/zip4j/model/FileHeader/setCrc32(J)V
L19:
iconst_2
newarray byte
astore 6
aload 6
iconst_0
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/isEncrypted()Z
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionMethod()I
invokespecial net/lingala/zip4j/io/CipherOutputStream/generateGeneralPurposeBitArray(ZI)[I
invokestatic net/lingala/zip4j/util/Raw/bitArrayToByte([I)B
bastore
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getFileNameCharset()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
istore 3
iload 3
ifeq L20
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getFileNameCharset()Ljava/lang/String;
ldc "UTF8"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifne L21
L20:
iload 3
ifne L22
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/detectCharSet(Ljava/lang/String;)Ljava/lang/String;
ldc "UTF8"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L22
L21:
aload 6
iconst_1
bipush 8
bastore
L23:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 6
invokevirtual net/lingala/zip4j/model/FileHeader/setGeneralPurposeFlag([B)V
return
L7:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 6
invokestatic net/lingala/zip4j/util/Zip4jUtil/getEncodedStringLength(Ljava/lang/String;)I
invokevirtual net/lingala/zip4j/model/FileHeader/setFileNameLength(I)V
goto L8
L9:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
iconst_0
invokevirtual net/lingala/zip4j/model/FileHeader/setDiskNumberStart(I)V
goto L10
L14:
iconst_0
istore 3
goto L15
L12:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/sourceFile Ljava/io/File;
invokevirtual java/io/File/isDirectory()Z
invokevirtual net/lingala/zip4j/model/FileHeader/setDirectory(Z)V
goto L16
L17:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/isSourceExternalStream()Z
ifne L18
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/sourceFile Ljava/io/File;
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileLengh(Ljava/io/File;)J
lstore 4
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionMethod()I
ifne L24
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
ifne L25
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
ldc2_w 12L
lload 4
ladd
invokevirtual net/lingala/zip4j/model/FileHeader/setCompressedSize(J)V
L26:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
lload 4
invokevirtual net/lingala/zip4j/model/FileHeader/setUncompressedSize(J)V
goto L18
L25:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
bipush 99
if_icmpne L27
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getAesKeyStrength()I
tableswitch 1
L28
L29
L30
default : L29
L29:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid aes key strength, cannot determine key sizes"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L28:
bipush 8
istore 2
L31:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
iload 2
i2l
lload 4
ladd
ldc2_w 10L
ladd
ldc2_w 2L
ladd
invokevirtual net/lingala/zip4j/model/FileHeader/setCompressedSize(J)V
goto L26
L30:
bipush 16
istore 2
goto L31
L27:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
lconst_0
invokevirtual net/lingala/zip4j/model/FileHeader/setCompressedSize(J)V
goto L26
L24:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
lconst_0
invokevirtual net/lingala/zip4j/model/FileHeader/setCompressedSize(J)V
goto L26
L22:
aload 6
iconst_1
iconst_0
bastore
goto L23
.limit locals 8
.limit stack 5
.end method

.method private createLocalFileHeader()V
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "file header is null, cannot create local file header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
new net/lingala/zip4j/model/LocalFileHeader
dup
invokespecial net/lingala/zip4j/model/LocalFileHeader/<init>()V
putfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
ldc_w 67324752
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setSignature(I)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getVersionNeededToExtract()I
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setVersionNeededToExtract(I)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getCompressionMethod()I
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setCompressionMethod(I)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getLastModFileTime()I
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setLastModFileTime(I)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getUncompressedSize()J
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setUncompressedSize(J)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getFileNameLength()I
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setFileNameLength(I)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setFileName(Ljava/lang/String;)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/isEncrypted()Z
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setEncrypted(Z)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getEncryptionMethod()I
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setEncryptionMethod(I)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getAesExtraDataRecord()Lnet/lingala/zip4j/model/AESExtraDataRecord;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setAesExtraDataRecord(Lnet/lingala/zip4j/model/AESExtraDataRecord;)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getCrc32()J
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setCrc32(J)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getCompressedSize()J
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setCompressedSize(J)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getGeneralPurposeFlag()[B
invokevirtual [B/clone()Ljava/lang/Object;
checkcast [B
checkcast [B
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setGeneralPurposeFlag([B)V
return
.limit locals 1
.limit stack 3
.end method

.method private encryptAndWrite([BII)V
.throws java/io/IOException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/encrypter Lnet/lingala/zip4j/crypto/IEncrypter;
ifnull L1
L0:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/encrypter Lnet/lingala/zip4j/crypto/IEncrypter;
aload 1
iload 2
iload 3
invokeinterface net/lingala/zip4j/crypto/IEncrypter/encryptData([BII)I 3
pop
L1:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/OutputStream/write([BII)V
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
iload 3
i2l
ladd
putfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
iload 3
i2l
ladd
putfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
return
L2:
astore 1
new java/io/IOException
dup
aload 1
invokevirtual net/lingala/zip4j/exception/ZipException/getMessage()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
.limit locals 4
.limit stack 5
.end method

.method private generateAESExtraDataRecord(Lnet/lingala/zip4j/model/ZipParameters;)Lnet/lingala/zip4j/model/AESExtraDataRecord;
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip parameters are null, cannot generate AES Extra Data record"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new net/lingala/zip4j/model/AESExtraDataRecord
dup
invokespecial net/lingala/zip4j/model/AESExtraDataRecord/<init>()V
astore 2
aload 2
ldc2_w 39169L
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/setSignature(J)V
aload 2
bipush 7
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/setDataSize(I)V
aload 2
ldc "AE"
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/setVendorID(Ljava/lang/String;)V
aload 2
iconst_2
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/setVersionNumber(I)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipParameters/getAesKeyStrength()I
iconst_1
if_icmpne L1
aload 2
iconst_1
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/setAesStrength(I)V
L2:
aload 2
aload 1
invokevirtual net/lingala/zip4j/model/ZipParameters/getCompressionMethod()I
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/setCompressionMethod(I)V
aload 2
areturn
L1:
aload 1
invokevirtual net/lingala/zip4j/model/ZipParameters/getAesKeyStrength()I
iconst_3
if_icmpne L3
aload 2
iconst_3
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/setAesStrength(I)V
goto L2
L3:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid AES key strength, cannot generate AES Extra data record"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method private generateGeneralPurposeBitArray(ZI)[I
bipush 8
newarray int
astore 3
iload 1
ifeq L0
aload 3
iconst_0
iconst_1
iastore
L1:
iload 2
bipush 8
if_icmpne L2
L3:
aload 3
iconst_3
iconst_1
iastore
aload 3
areturn
L0:
aload 3
iconst_0
iconst_0
iastore
goto L1
L2:
aload 3
iconst_1
iconst_0
iastore
aload 3
iconst_2
iconst_0
iastore
goto L3
.limit locals 4
.limit stack 3
.end method

.method private getFileAttributes(Ljava/io/File;)I
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file is null, cannot get file attributes"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual java/io/File/exists()Z
ifne L1
L2:
iconst_0
ireturn
L1:
aload 1
invokevirtual java/io/File/isDirectory()Z
ifeq L3
aload 1
invokevirtual java/io/File/isHidden()Z
ifeq L4
bipush 18
ireturn
L4:
bipush 16
ireturn
L3:
aload 1
invokevirtual java/io/File/canWrite()Z
ifne L5
aload 1
invokevirtual java/io/File/isHidden()Z
ifeq L5
iconst_3
ireturn
L5:
aload 1
invokevirtual java/io/File/canWrite()Z
ifne L6
iconst_1
ireturn
L6:
aload 1
invokevirtual java/io/File/isHidden()Z
ifeq L2
iconst_2
ireturn
.limit locals 2
.limit stack 3
.end method

.method private initEncrypter()V
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/isEncryptFiles()Z
ifne L0
aload 0
aconst_null
putfield net/lingala/zip4j/io/CipherOutputStream/encrypter Lnet/lingala/zip4j/crypto/IEncrypter;
return
L0:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
lookupswitch
0 : L1
99 : L2
default : L3
L3:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid encprytion method"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
new net/lingala/zip4j/crypto/StandardEncrypter
dup
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getPassword()[C
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getLastModFileTime()I
ldc_w 65535
iand
bipush 16
ishl
invokespecial net/lingala/zip4j/crypto/StandardEncrypter/<init>([CI)V
putfield net/lingala/zip4j/io/CipherOutputStream/encrypter Lnet/lingala/zip4j/crypto/IEncrypter;
return
L2:
aload 0
new net/lingala/zip4j/crypto/AESEncrpyter
dup
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getPassword()[C
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getAesKeyStrength()I
invokespecial net/lingala/zip4j/crypto/AESEncrpyter/<init>([CI)V
putfield net/lingala/zip4j/io/CipherOutputStream/encrypter Lnet/lingala/zip4j/crypto/IEncrypter;
return
.limit locals 1
.limit stack 6
.end method

.method private initZipModel(Lnet/lingala/zip4j/model/ZipModel;)V
aload 1
ifnonnull L0
aload 0
new net/lingala/zip4j/model/ZipModel
dup
invokespecial net/lingala/zip4j/model/ZipModel/<init>()V
putfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
L1:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
ifnonnull L2
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
new net/lingala/zip4j/model/EndCentralDirRecord
dup
invokespecial net/lingala/zip4j/model/EndCentralDirRecord/<init>()V
invokevirtual net/lingala/zip4j/model/ZipModel/setEndCentralDirRecord(Lnet/lingala/zip4j/model/EndCentralDirRecord;)V
L2:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnonnull L3
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
new net/lingala/zip4j/model/CentralDirectory
dup
invokespecial net/lingala/zip4j/model/CentralDirectory/<init>()V
invokevirtual net/lingala/zip4j/model/ZipModel/setCentralDirectory(Lnet/lingala/zip4j/model/CentralDirectory;)V
L3:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
ifnonnull L4
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
invokevirtual net/lingala/zip4j/model/CentralDirectory/setFileHeaders(Ljava/util/ArrayList;)V
L4:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getLocalFileHeaderList()Ljava/util/List;
ifnonnull L5
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
invokevirtual net/lingala/zip4j/model/ZipModel/setLocalFileHeaderList(Ljava/util/List;)V
L5:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
instanceof net/lingala/zip4j/io/SplitOutputStream
ifeq L6
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
checkcast net/lingala/zip4j/io/SplitOutputStream
invokevirtual net/lingala/zip4j/io/SplitOutputStream/isSplitZipFile()Z
ifeq L6
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
iconst_1
invokevirtual net/lingala/zip4j/model/ZipModel/setSplitArchive(Z)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
checkcast net/lingala/zip4j/io/SplitOutputStream
invokevirtual net/lingala/zip4j/io/SplitOutputStream/getSplitLength()J
invokevirtual net/lingala/zip4j/model/ZipModel/setSplitLength(J)V
L6:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
ldc2_w 101010256L
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setSignature(J)V
return
L0:
aload 0
aload 1
putfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
goto L1
.limit locals 2
.limit stack 3
.end method

.method public close()V
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
ifnull L0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/close()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method public closeEntry()V
.throws java/io/IOException
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
ifeq L0
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBuffer [B
iconst_0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
invokespecial net/lingala/zip4j/io/CipherOutputStream/encryptAndWrite([BII)V
aload 0
iconst_0
putfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
L0:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/isEncryptFiles()Z
ifeq L1
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
bipush 99
if_icmpne L1
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/encrypter Lnet/lingala/zip4j/crypto/IEncrypter;
instanceof net/lingala/zip4j/crypto/AESEncrpyter
ifeq L2
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/encrypter Lnet/lingala/zip4j/crypto/IEncrypter;
checkcast net/lingala/zip4j/crypto/AESEncrpyter
invokevirtual net/lingala/zip4j/crypto/AESEncrpyter/getFinalMac()[B
invokevirtual java/io/OutputStream/write([B)V
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
ldc2_w 10L
ladd
putfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
ldc2_w 10L
ladd
putfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
L1:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
invokevirtual net/lingala/zip4j/model/FileHeader/setCompressedSize(J)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setCompressedSize(J)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/isSourceExternalStream()Z
ifeq L3
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesRead J
invokevirtual net/lingala/zip4j/model/FileHeader/setUncompressedSize(J)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getUncompressedSize()J
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesRead J
lcmp
ifeq L3
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesRead J
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setUncompressedSize(J)V
L3:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/crc Ljava/util/zip/CRC32;
invokevirtual java/util/zip/CRC32/getValue()J
lstore 3
lload 3
lstore 1
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/isEncrypted()Z
ifeq L4
lload 3
lstore 1
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getEncryptionMethod()I
bipush 99
if_icmpne L4
lconst_0
lstore 1
L4:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/isEncryptFiles()Z
ifeq L5
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
bipush 99
if_icmpne L5
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
lconst_0
invokevirtual net/lingala/zip4j/model/FileHeader/setCrc32(J)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
lconst_0
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setCrc32(J)V
L6:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getLocalFileHeaderList()Ljava/util/List;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
new net/lingala/zip4j/core/HeaderWriter
dup
invokespecial net/lingala/zip4j/core/HeaderWriter/<init>()V
astore 5
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
aload 5
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
invokevirtual net/lingala/zip4j/core/HeaderWriter/writeExtendedLocalHeader(Lnet/lingala/zip4j/model/LocalFileHeader;Ljava/io/OutputStream;)I
i2l
ladd
putfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/crc Ljava/util/zip/CRC32;
invokevirtual java/util/zip/CRC32/reset()V
aload 0
lconst_0
putfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
aload 0
aconst_null
putfield net/lingala/zip4j/io/CipherOutputStream/encrypter Lnet/lingala/zip4j/crypto/IEncrypter;
aload 0
lconst_0
putfield net/lingala/zip4j/io/CipherOutputStream/totalBytesRead J
return
L2:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid encrypter for AES encrypted file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L5:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
lload 1
invokevirtual net/lingala/zip4j/model/FileHeader/setCrc32(J)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
lload 1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setCrc32(J)V
goto L6
.limit locals 6
.limit stack 6
.end method

.method public decrementCompressedFileSize(I)V
iload 1
ifgt L0
L1:
return
L0:
iload 1
i2l
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
lcmp
ifgt L1
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
iload 1
i2l
lsub
putfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
return
.limit locals 2
.limit stack 5
.end method

.method public finish()V
.throws java/io/IOException
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setOffsetOfStartOfCentralDir(J)V
new net/lingala/zip4j/core/HeaderWriter
dup
invokespecial net/lingala/zip4j/core/HeaderWriter/<init>()V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
invokevirtual net/lingala/zip4j/core/HeaderWriter/finalizeZipFile(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;)V
return
.limit locals 1
.limit stack 3
.end method

.method public getSourceFile()Ljava/io/File;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/sourceFile Ljava/io/File;
areturn
.limit locals 1
.limit stack 1
.end method

.method public putNextEntry(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
.catch java/lang/CloneNotSupportedException from L0 to L1 using L2
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L3
.catch java/lang/Exception from L0 to L1 using L4
.catch java/lang/CloneNotSupportedException from L1 to L5 using L2
.catch net/lingala/zip4j/exception/ZipException from L1 to L5 using L3
.catch java/lang/Exception from L1 to L5 using L4
.catch java/lang/CloneNotSupportedException from L5 to L6 using L2
.catch net/lingala/zip4j/exception/ZipException from L5 to L6 using L3
.catch java/lang/Exception from L5 to L6 using L4
.catch java/lang/CloneNotSupportedException from L6 to L7 using L2
.catch net/lingala/zip4j/exception/ZipException from L6 to L7 using L3
.catch java/lang/Exception from L6 to L7 using L4
.catch java/lang/CloneNotSupportedException from L7 to L8 using L2
.catch net/lingala/zip4j/exception/ZipException from L7 to L8 using L3
.catch java/lang/Exception from L7 to L8 using L4
.catch java/lang/CloneNotSupportedException from L8 to L9 using L2
.catch net/lingala/zip4j/exception/ZipException from L8 to L9 using L3
.catch java/lang/Exception from L8 to L9 using L4
.catch java/lang/CloneNotSupportedException from L10 to L2 using L2
.catch net/lingala/zip4j/exception/ZipException from L10 to L2 using L3
.catch java/lang/Exception from L10 to L2 using L4
.catch java/lang/CloneNotSupportedException from L11 to L12 using L2
.catch net/lingala/zip4j/exception/ZipException from L11 to L12 using L3
.catch java/lang/Exception from L11 to L12 using L4
.catch java/lang/CloneNotSupportedException from L12 to L13 using L2
.catch net/lingala/zip4j/exception/ZipException from L12 to L13 using L3
.catch java/lang/Exception from L12 to L13 using L4
.catch java/lang/CloneNotSupportedException from L14 to L15 using L2
.catch net/lingala/zip4j/exception/ZipException from L14 to L15 using L3
.catch java/lang/Exception from L14 to L15 using L4
.catch java/lang/CloneNotSupportedException from L16 to L17 using L2
.catch net/lingala/zip4j/exception/ZipException from L16 to L17 using L3
.catch java/lang/Exception from L16 to L17 using L4
.catch java/lang/CloneNotSupportedException from L18 to L19 using L2
.catch net/lingala/zip4j/exception/ZipException from L18 to L19 using L3
.catch java/lang/Exception from L18 to L19 using L4
.catch java/lang/CloneNotSupportedException from L20 to L21 using L2
.catch net/lingala/zip4j/exception/ZipException from L20 to L21 using L3
.catch java/lang/Exception from L20 to L21 using L4
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/isSourceExternalStream()Z
ifne L22
aload 1
ifnonnull L22
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L22:
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/isSourceExternalStream()Z
ifne L0
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/io/File;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file does not exist"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield net/lingala/zip4j/io/CipherOutputStream/sourceFile Ljava/io/File;
aload 0
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/clone()Ljava/lang/Object;
checkcast net/lingala/zip4j/model/ZipParameters
putfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/isSourceExternalStream()Z
ifne L10
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/sourceFile Ljava/io/File;
invokevirtual java/io/File/isDirectory()Z
ifeq L1
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
iconst_0
invokevirtual net/lingala/zip4j/model/ZipParameters/setEncryptFiles(Z)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
iconst_m1
invokevirtual net/lingala/zip4j/model/ZipParameters/setEncryptionMethod(I)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
iconst_0
invokevirtual net/lingala/zip4j/model/ZipParameters/setCompressionMethod(I)V
L1:
aload 0
invokespecial net/lingala/zip4j/io/CipherOutputStream/createFileHeader()V
aload 0
invokespecial net/lingala/zip4j/io/CipherOutputStream/createLocalFileHeader()V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifeq L6
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnull L5
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
ifnull L5
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L6
L5:
iconst_4
newarray byte
astore 1
aload 1
iconst_0
ldc_w 134695760
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
aload 1
invokevirtual java/io/OutputStream/write([B)V
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
ldc2_w 4L
ladd
putfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
L6:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
instanceof net/lingala/zip4j/io/SplitOutputStream
ifeq L16
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
ldc2_w 4L
lcmp
ifne L14
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
ldc2_w 4L
invokevirtual net/lingala/zip4j/model/FileHeader/setOffsetLocalHeader(J)V
L7:
new net/lingala/zip4j/core/HeaderWriter
dup
invokespecial net/lingala/zip4j/core/HeaderWriter/<init>()V
astore 1
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
aload 1
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/localFileHeader Lnet/lingala/zip4j/model/LocalFileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
invokevirtual net/lingala/zip4j/core/HeaderWriter/writeLocalFileHeader(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/LocalFileHeader;Ljava/io/OutputStream;)I
i2l
ladd
putfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/isEncryptFiles()Z
ifeq L8
aload 0
invokespecial net/lingala/zip4j/io/CipherOutputStream/initEncrypter()V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/encrypter Lnet/lingala/zip4j/crypto/IEncrypter;
ifnull L8
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
ifne L20
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/encrypter Lnet/lingala/zip4j/crypto/IEncrypter;
checkcast net/lingala/zip4j/crypto/StandardEncrypter
invokevirtual net/lingala/zip4j/crypto/StandardEncrypter/getHeaderBytes()[B
astore 1
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
aload 1
invokevirtual java/io/OutputStream/write([B)V
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
aload 1
arraylength
i2l
ladd
putfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
aload 1
arraylength
i2l
ladd
putfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
L8:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/crc Ljava/util/zip/CRC32;
invokevirtual java/util/zip/CRC32/reset()V
L9:
return
L10:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getFileNameInZip()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L11
new net/lingala/zip4j/exception/ZipException
dup
ldc "file name is empty for external stream"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L11:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getFileNameInZip()Ljava/lang/String;
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L12
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getFileNameInZip()Ljava/lang/String;
ldc "\\"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L1
L12:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
iconst_0
invokevirtual net/lingala/zip4j/model/ZipParameters/setEncryptFiles(Z)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
iconst_m1
invokevirtual net/lingala/zip4j/model/ZipParameters/setEncryptionMethod(I)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
iconst_0
invokevirtual net/lingala/zip4j/model/ZipParameters/setCompressionMethod(I)V
L13:
goto L1
L3:
astore 1
aload 1
athrow
L14:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
checkcast net/lingala/zip4j/io/SplitOutputStream
invokevirtual net/lingala/zip4j/io/SplitOutputStream/getFilePointer()J
invokevirtual net/lingala/zip4j/model/FileHeader/setOffsetLocalHeader(J)V
L15:
goto L7
L4:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L16:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
ldc2_w 4L
lcmp
ifne L18
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
ldc2_w 4L
invokevirtual net/lingala/zip4j/model/FileHeader/setOffsetLocalHeader(J)V
L17:
goto L7
L18:
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/fileHeader Lnet/lingala/zip4j/model/FileHeader;
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
invokevirtual net/lingala/zip4j/model/FileHeader/setOffsetLocalHeader(J)V
L19:
goto L7
L20:
aload 2
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
bipush 99
if_icmpne L8
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/encrypter Lnet/lingala/zip4j/crypto/IEncrypter;
checkcast net/lingala/zip4j/crypto/AESEncrpyter
invokevirtual net/lingala/zip4j/crypto/AESEncrpyter/getSaltBytes()[B
astore 1
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/encrypter Lnet/lingala/zip4j/crypto/IEncrypter;
checkcast net/lingala/zip4j/crypto/AESEncrpyter
invokevirtual net/lingala/zip4j/crypto/AESEncrpyter/getDerivedPasswordVerifier()[B
astore 2
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
aload 1
invokevirtual java/io/OutputStream/write([B)V
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/outputStream Ljava/io/OutputStream;
aload 2
invokevirtual java/io/OutputStream/write([B)V
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
aload 1
arraylength
aload 2
arraylength
iadd
i2l
ladd
putfield net/lingala/zip4j/io/CipherOutputStream/totalBytesWritten J
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
aload 1
arraylength
aload 2
arraylength
iadd
i2l
ladd
putfield net/lingala/zip4j/io/CipherOutputStream/bytesWrittenForThisFile J
L21:
goto L8
.limit locals 3
.limit stack 7
.end method

.method public setSourceFile(Ljava/io/File;)V
aload 0
aload 1
putfield net/lingala/zip4j/io/CipherOutputStream/sourceFile Ljava/io/File;
return
.limit locals 2
.limit stack 2
.end method

.method protected updateTotalBytesRead(I)V
iload 1
ifle L0
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/totalBytesRead J
iload 1
i2l
ladd
putfield net/lingala/zip4j/io/CipherOutputStream/totalBytesRead J
L0:
return
.limit locals 2
.limit stack 5
.end method

.method public write(I)V
.throws java/io/IOException
aload 0
iconst_1
newarray byte
dup
iconst_0
iload 1
i2b
bastore
iconst_0
iconst_1
invokevirtual net/lingala/zip4j/io/CipherOutputStream/write([BII)V
return
.limit locals 2
.limit stack 5
.end method

.method public write([B)V
.throws java/io/IOException
aload 1
ifnonnull L0
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L0:
aload 1
arraylength
ifne L1
return
L1:
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual net/lingala/zip4j/io/CipherOutputStream/write([BII)V
return
.limit locals 2
.limit stack 4
.end method

.method public write([BII)V
.throws java/io/IOException
iload 3
ifne L0
L1:
return
L0:
iload 2
istore 7
iload 3
istore 6
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/isEncryptFiles()Z
ifeq L2
iload 2
istore 7
iload 3
istore 6
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/zipParameters Lnet/lingala/zip4j/model/ZipParameters;
invokevirtual net/lingala/zip4j/model/ZipParameters/getEncryptionMethod()I
bipush 99
if_icmpne L2
iload 2
istore 4
iload 3
istore 5
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
ifeq L3
iload 3
bipush 16
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
isub
if_icmplt L4
aload 1
iload 2
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBuffer [B
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
bipush 16
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
isub
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBuffer [B
iconst_0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBuffer [B
arraylength
invokespecial net/lingala/zip4j/io/CipherOutputStream/encryptAndWrite([BII)V
bipush 16
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
isub
istore 4
iload 3
iload 4
isub
istore 5
aload 0
iconst_0
putfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
L3:
iload 4
istore 7
iload 5
istore 6
iload 5
ifeq L2
iload 4
istore 7
iload 5
istore 6
iload 5
bipush 16
irem
ifeq L2
aload 1
iload 5
iload 4
iadd
iload 5
bipush 16
irem
isub
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBuffer [B
iconst_0
iload 5
bipush 16
irem
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
iload 5
bipush 16
irem
putfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
iload 5
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
isub
istore 6
iload 4
istore 7
L2:
iload 6
ifeq L1
aload 0
aload 1
iload 7
iload 6
invokespecial net/lingala/zip4j/io/CipherOutputStream/encryptAndWrite([BII)V
return
L4:
aload 1
iload 2
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBuffer [B
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
iload 3
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 0
getfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
iload 3
iadd
putfield net/lingala/zip4j/io/CipherOutputStream/pendingBufferLength I
return
.limit locals 8
.limit stack 6
.end method
