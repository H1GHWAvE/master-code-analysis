.bytecode 50.0
.class public synchronized net/lingala/zip4j/io/InflaterInputStream
.super net/lingala/zip4j/io/PartInputStream

.field private 'buff' [B

.field private 'bytesWritten' J

.field private 'inflater' Ljava/util/zip/Inflater;

.field private 'oneByteBuff' [B

.field private 'uncompressedSize' J

.field private 'unzipEngine' Lnet/lingala/zip4j/unzip/UnzipEngine;

.method public <init>(Ljava/io/RandomAccessFile;JJLnet/lingala/zip4j/unzip/UnzipEngine;)V
aload 0
aload 1
lload 2
lload 4
aload 6
invokespecial net/lingala/zip4j/io/PartInputStream/<init>(Ljava/io/RandomAccessFile;JJLnet/lingala/zip4j/unzip/UnzipEngine;)V
aload 0
iconst_1
newarray byte
putfield net/lingala/zip4j/io/InflaterInputStream/oneByteBuff [B
aload 0
new java/util/zip/Inflater
dup
iconst_1
invokespecial java/util/zip/Inflater/<init>(Z)V
putfield net/lingala/zip4j/io/InflaterInputStream/inflater Ljava/util/zip/Inflater;
aload 0
sipush 4096
newarray byte
putfield net/lingala/zip4j/io/InflaterInputStream/buff [B
aload 0
aload 6
putfield net/lingala/zip4j/io/InflaterInputStream/unzipEngine Lnet/lingala/zip4j/unzip/UnzipEngine;
aload 0
lconst_0
putfield net/lingala/zip4j/io/InflaterInputStream/bytesWritten J
aload 0
aload 6
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/getFileHeader()Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getUncompressedSize()J
putfield net/lingala/zip4j/io/InflaterInputStream/uncompressedSize J
return
.limit locals 7
.limit stack 7
.end method

.method private fill()V
.throws java/io/IOException
aload 0
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/buff [B
iconst_0
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/buff [B
arraylength
invokespecial net/lingala/zip4j/io/PartInputStream/read([BII)I
istore 1
iload 1
iconst_m1
if_icmpne L0
new java/io/EOFException
dup
ldc "Unexpected end of ZLIB input stream"
invokespecial java/io/EOFException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/inflater Ljava/util/zip/Inflater;
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/buff [B
iconst_0
iload 1
invokevirtual java/util/zip/Inflater/setInput([BII)V
return
.limit locals 2
.limit stack 4
.end method

.method private finishInflating()V
.throws java/io/IOException
sipush 1024
newarray byte
astore 1
L0:
aload 0
aload 1
iconst_0
sipush 1024
invokespecial net/lingala/zip4j/io/PartInputStream/read([BII)I
iconst_m1
if_icmpne L0
aload 0
invokevirtual net/lingala/zip4j/io/InflaterInputStream/checkAndReadAESMacBytes()V
return
.limit locals 2
.limit stack 4
.end method

.method public available()I
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/inflater Ljava/util/zip/Inflater;
invokevirtual java/util/zip/Inflater/finished()Z
ifeq L0
iconst_0
ireturn
L0:
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public close()V
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/inflater Ljava/util/zip/Inflater;
invokevirtual java/util/zip/Inflater/end()V
aload 0
invokespecial net/lingala/zip4j/io/PartInputStream/close()V
return
.limit locals 1
.limit stack 1
.end method

.method public getUnzipEngine()Lnet/lingala/zip4j/unzip/UnzipEngine;
aload 0
invokespecial net/lingala/zip4j/io/PartInputStream/getUnzipEngine()Lnet/lingala/zip4j/unzip/UnzipEngine;
areturn
.limit locals 1
.limit stack 1
.end method

.method public read()I
.throws java/io/IOException
aload 0
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/oneByteBuff [B
iconst_0
iconst_1
invokevirtual net/lingala/zip4j/io/InflaterInputStream/read([BII)I
iconst_m1
if_icmpne L0
iconst_m1
ireturn
L0:
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/oneByteBuff [B
iconst_0
baload
sipush 255
iand
ireturn
.limit locals 1
.limit stack 4
.end method

.method public read([B)I
.throws java/io/IOException
aload 1
ifnonnull L0
new java/lang/NullPointerException
dup
ldc "input buffer is null"
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual net/lingala/zip4j/io/InflaterInputStream/read([BII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method public read([BII)I
.throws java/io/IOException
.catch java/util/zip/DataFormatException from L0 to L1 using L2
.catch java/util/zip/DataFormatException from L3 to L4 using L2
.catch java/util/zip/DataFormatException from L4 to L5 using L2
.catch java/util/zip/DataFormatException from L6 to L7 using L2
.catch java/util/zip/DataFormatException from L7 to L8 using L2
.catch java/util/zip/DataFormatException from L9 to L10 using L2
aload 1
ifnonnull L11
new java/lang/NullPointerException
dup
ldc "input buffer is null"
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
athrow
L11:
iload 2
iflt L12
iload 3
iflt L12
iload 3
aload 1
arraylength
iload 2
isub
if_icmple L13
L12:
new java/lang/IndexOutOfBoundsException
dup
invokespecial java/lang/IndexOutOfBoundsException/<init>()V
athrow
L13:
iload 3
ifne L0
iconst_0
ireturn
L0:
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/bytesWritten J
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/uncompressedSize J
lcmp
iflt L4
aload 0
invokespecial net/lingala/zip4j/io/InflaterInputStream/finishInflating()V
L1:
iconst_m1
ireturn
L3:
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/inflater Ljava/util/zip/Inflater;
invokevirtual java/util/zip/Inflater/needsInput()Z
ifeq L4
aload 0
invokespecial net/lingala/zip4j/io/InflaterInputStream/fill()V
L4:
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/inflater Ljava/util/zip/Inflater;
aload 1
iload 2
iload 3
invokevirtual java/util/zip/Inflater/inflate([BII)I
istore 4
L5:
iload 4
ifne L9
L6:
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/inflater Ljava/util/zip/Inflater;
invokevirtual java/util/zip/Inflater/finished()Z
ifne L7
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/inflater Ljava/util/zip/Inflater;
invokevirtual java/util/zip/Inflater/needsDictionary()Z
ifeq L3
L7:
aload 0
invokespecial net/lingala/zip4j/io/InflaterInputStream/finishInflating()V
L8:
iconst_m1
ireturn
L9:
aload 0
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/bytesWritten J
iload 4
i2l
ladd
putfield net/lingala/zip4j/io/InflaterInputStream/bytesWritten J
L10:
iload 4
ireturn
L2:
astore 5
ldc "Invalid ZLIB data format"
astore 1
aload 5
invokevirtual java/util/zip/DataFormatException/getMessage()Ljava/lang/String;
ifnull L14
aload 5
invokevirtual java/util/zip/DataFormatException/getMessage()Ljava/lang/String;
astore 1
L14:
aload 1
astore 5
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/unzipEngine Lnet/lingala/zip4j/unzip/UnzipEngine;
ifnull L15
aload 1
astore 5
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/unzipEngine Lnet/lingala/zip4j/unzip/UnzipEngine;
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/getLocalFileHeader()Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/isEncrypted()Z
ifeq L15
aload 1
astore 5
aload 0
getfield net/lingala/zip4j/io/InflaterInputStream/unzipEngine Lnet/lingala/zip4j/unzip/UnzipEngine;
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/getLocalFileHeader()Lnet/lingala/zip4j/model/LocalFileHeader;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getEncryptionMethod()I
ifne L15
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " - Wrong Password?"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 5
L15:
new java/io/IOException
dup
aload 5
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
.limit locals 6
.limit stack 5
.end method

.method public seek(J)V
.throws java/io/IOException
aload 0
lload 1
invokespecial net/lingala/zip4j/io/PartInputStream/seek(J)V
return
.limit locals 3
.limit stack 3
.end method

.method public skip(J)J
.throws java/io/IOException
lload 1
lconst_0
lcmp
ifge L0
new java/lang/IllegalArgumentException
dup
ldc "negative skip length"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
lload 1
ldc2_w 2147483647L
invokestatic java/lang/Math/min(JJ)J
l2i
istore 6
iconst_0
istore 3
sipush 512
newarray byte
astore 7
L1:
iload 3
iload 6
if_icmpge L2
iload 6
iload 3
isub
istore 5
iload 5
istore 4
iload 5
aload 7
arraylength
if_icmple L3
aload 7
arraylength
istore 4
L3:
aload 0
aload 7
iconst_0
iload 4
invokevirtual net/lingala/zip4j/io/InflaterInputStream/read([BII)I
istore 4
iload 4
iconst_m1
if_icmpne L4
L2:
iload 3
i2l
lreturn
L4:
iload 3
iload 4
iadd
istore 3
goto L1
.limit locals 8
.limit stack 4
.end method
