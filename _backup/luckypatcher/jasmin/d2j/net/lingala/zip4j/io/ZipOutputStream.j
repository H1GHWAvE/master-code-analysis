.bytecode 50.0
.class public synchronized net/lingala/zip4j/io/ZipOutputStream
.super net/lingala/zip4j/io/DeflaterOutputStream

.method public <init>(Ljava/io/OutputStream;)V
aload 0
aload 1
aconst_null
invokespecial net/lingala/zip4j/io/ZipOutputStream/<init>(Ljava/io/OutputStream;Lnet/lingala/zip4j/model/ZipModel;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/io/OutputStream;Lnet/lingala/zip4j/model/ZipModel;)V
aload 0
aload 1
aload 2
invokespecial net/lingala/zip4j/io/DeflaterOutputStream/<init>(Ljava/io/OutputStream;Lnet/lingala/zip4j/model/ZipModel;)V
return
.limit locals 3
.limit stack 3
.end method

.method public write(I)V
.throws java/io/IOException
aload 0
iconst_1
newarray byte
dup
iconst_0
iload 1
i2b
bastore
iconst_0
iconst_1
invokevirtual net/lingala/zip4j/io/ZipOutputStream/write([BII)V
return
.limit locals 2
.limit stack 5
.end method

.method public write([B)V
.throws java/io/IOException
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual net/lingala/zip4j/io/ZipOutputStream/write([BII)V
return
.limit locals 2
.limit stack 4
.end method

.method public write([BII)V
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/ZipOutputStream/crc Ljava/util/zip/CRC32;
aload 1
iload 2
iload 3
invokevirtual java/util/zip/CRC32/update([BII)V
aload 0
iload 3
invokevirtual net/lingala/zip4j/io/ZipOutputStream/updateTotalBytesRead(I)V
aload 0
aload 1
iload 2
iload 3
invokespecial net/lingala/zip4j/io/DeflaterOutputStream/write([BII)V
return
.limit locals 4
.limit stack 4
.end method
