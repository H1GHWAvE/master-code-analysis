.bytecode 50.0
.class public synchronized net/lingala/zip4j/io/PartInputStream
.super net/lingala/zip4j/io/BaseInputStream

.field private 'aesBlockByte' [B

.field private 'aesBytesReturned' I

.field private 'bytesRead' J

.field private 'count' I

.field private 'decrypter' Lnet/lingala/zip4j/crypto/IDecrypter;

.field private 'isAESEncryptedFile' Z

.field private 'length' J

.field private 'oneByteBuff' [B

.field private 'raf' Ljava/io/RandomAccessFile;

.field private 'unzipEngine' Lnet/lingala/zip4j/unzip/UnzipEngine;

.method public <init>(Ljava/io/RandomAccessFile;JJLnet/lingala/zip4j/unzip/UnzipEngine;)V
iconst_1
istore 7
aload 0
invokespecial net/lingala/zip4j/io/BaseInputStream/<init>()V
aload 0
iconst_1
newarray byte
putfield net/lingala/zip4j/io/PartInputStream/oneByteBuff [B
aload 0
bipush 16
newarray byte
putfield net/lingala/zip4j/io/PartInputStream/aesBlockByte [B
aload 0
iconst_0
putfield net/lingala/zip4j/io/PartInputStream/aesBytesReturned I
aload 0
iconst_0
putfield net/lingala/zip4j/io/PartInputStream/isAESEncryptedFile Z
aload 0
iconst_m1
putfield net/lingala/zip4j/io/PartInputStream/count I
aload 0
aload 1
putfield net/lingala/zip4j/io/PartInputStream/raf Ljava/io/RandomAccessFile;
aload 0
aload 6
putfield net/lingala/zip4j/io/PartInputStream/unzipEngine Lnet/lingala/zip4j/unzip/UnzipEngine;
aload 0
aload 6
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/getDecrypter()Lnet/lingala/zip4j/crypto/IDecrypter;
putfield net/lingala/zip4j/io/PartInputStream/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
aload 0
lconst_0
putfield net/lingala/zip4j/io/PartInputStream/bytesRead J
aload 0
lload 4
putfield net/lingala/zip4j/io/PartInputStream/length J
aload 6
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/getFileHeader()Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/isEncrypted()Z
ifeq L0
aload 6
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/getFileHeader()Lnet/lingala/zip4j/model/FileHeader;
invokevirtual net/lingala/zip4j/model/FileHeader/getEncryptionMethod()I
bipush 99
if_icmpne L0
L1:
aload 0
iload 7
putfield net/lingala/zip4j/io/PartInputStream/isAESEncryptedFile Z
return
L0:
iconst_0
istore 7
goto L1
.limit locals 8
.limit stack 3
.end method

.method public available()I
aload 0
getfield net/lingala/zip4j/io/PartInputStream/length J
aload 0
getfield net/lingala/zip4j/io/PartInputStream/bytesRead J
lsub
lstore 1
lload 1
ldc2_w 2147483647L
lcmp
ifle L0
ldc_w 2147483647
ireturn
L0:
lload 1
l2i
ireturn
.limit locals 3
.limit stack 4
.end method

.method protected checkAndReadAESMacBytes()V
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/PartInputStream/isAESEncryptedFile Z
ifeq L0
aload 0
getfield net/lingala/zip4j/io/PartInputStream/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
ifnull L0
aload 0
getfield net/lingala/zip4j/io/PartInputStream/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
instanceof net/lingala/zip4j/crypto/AESDecrypter
ifeq L0
aload 0
getfield net/lingala/zip4j/io/PartInputStream/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
checkcast net/lingala/zip4j/crypto/AESDecrypter
invokevirtual net/lingala/zip4j/crypto/AESDecrypter/getStoredMac()[B
ifnull L1
L0:
return
L1:
bipush 10
newarray byte
astore 2
aload 0
getfield net/lingala/zip4j/io/PartInputStream/raf Ljava/io/RandomAccessFile;
aload 2
invokevirtual java/io/RandomAccessFile/read([B)I
istore 1
iload 1
bipush 10
if_icmpeq L2
aload 0
getfield net/lingala/zip4j/io/PartInputStream/unzipEngine Lnet/lingala/zip4j/unzip/UnzipEngine;
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/getZipModel()Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifeq L3
aload 0
getfield net/lingala/zip4j/io/PartInputStream/raf Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/close()V
aload 0
aload 0
getfield net/lingala/zip4j/io/PartInputStream/unzipEngine Lnet/lingala/zip4j/unzip/UnzipEngine;
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/startNextSplitFile()Ljava/io/RandomAccessFile;
putfield net/lingala/zip4j/io/PartInputStream/raf Ljava/io/RandomAccessFile;
aload 0
getfield net/lingala/zip4j/io/PartInputStream/raf Ljava/io/RandomAccessFile;
aload 2
iload 1
bipush 10
iload 1
isub
invokevirtual java/io/RandomAccessFile/read([BII)I
pop
L2:
aload 0
getfield net/lingala/zip4j/io/PartInputStream/unzipEngine Lnet/lingala/zip4j/unzip/UnzipEngine;
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/getDecrypter()Lnet/lingala/zip4j/crypto/IDecrypter;
checkcast net/lingala/zip4j/crypto/AESDecrypter
aload 2
invokevirtual net/lingala/zip4j/crypto/AESDecrypter/setStoredMac([B)V
return
L3:
new java/io/IOException
dup
ldc "Error occured while reading stored AES authentication bytes"
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 5
.end method

.method public close()V
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/PartInputStream/raf Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/close()V
return
.limit locals 1
.limit stack 1
.end method

.method public getUnzipEngine()Lnet/lingala/zip4j/unzip/UnzipEngine;
aload 0
getfield net/lingala/zip4j/io/PartInputStream/unzipEngine Lnet/lingala/zip4j/unzip/UnzipEngine;
areturn
.limit locals 1
.limit stack 1
.end method

.method public read()I
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/PartInputStream/bytesRead J
aload 0
getfield net/lingala/zip4j/io/PartInputStream/length J
lcmp
iflt L0
L1:
iconst_m1
ireturn
L0:
aload 0
getfield net/lingala/zip4j/io/PartInputStream/isAESEncryptedFile Z
ifeq L2
aload 0
getfield net/lingala/zip4j/io/PartInputStream/aesBytesReturned I
ifeq L3
aload 0
getfield net/lingala/zip4j/io/PartInputStream/aesBytesReturned I
bipush 16
if_icmpne L4
L3:
aload 0
aload 0
getfield net/lingala/zip4j/io/PartInputStream/aesBlockByte [B
invokevirtual net/lingala/zip4j/io/PartInputStream/read([B)I
iconst_m1
if_icmpeq L1
aload 0
iconst_0
putfield net/lingala/zip4j/io/PartInputStream/aesBytesReturned I
L4:
aload 0
getfield net/lingala/zip4j/io/PartInputStream/aesBlockByte [B
astore 2
aload 0
getfield net/lingala/zip4j/io/PartInputStream/aesBytesReturned I
istore 1
aload 0
iload 1
iconst_1
iadd
putfield net/lingala/zip4j/io/PartInputStream/aesBytesReturned I
aload 2
iload 1
baload
sipush 255
iand
ireturn
L2:
aload 0
aload 0
getfield net/lingala/zip4j/io/PartInputStream/oneByteBuff [B
iconst_0
iconst_1
invokevirtual net/lingala/zip4j/io/PartInputStream/read([BII)I
iconst_m1
if_icmpeq L1
aload 0
getfield net/lingala/zip4j/io/PartInputStream/oneByteBuff [B
iconst_0
baload
sipush 255
iand
ireturn
.limit locals 3
.limit stack 4
.end method

.method public read([B)I
.throws java/io/IOException
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual net/lingala/zip4j/io/PartInputStream/read([BII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method public read([BII)I
.throws java/io/IOException
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
.catch all from L5 to L6 using L2
.catch net/lingala/zip4j/exception/ZipException from L7 to L8 using L9
.catch all from L10 to L11 using L2
iload 3
istore 4
iload 3
i2l
aload 0
getfield net/lingala/zip4j/io/PartInputStream/length J
aload 0
getfield net/lingala/zip4j/io/PartInputStream/bytesRead J
lsub
lcmp
ifle L12
aload 0
getfield net/lingala/zip4j/io/PartInputStream/length J
aload 0
getfield net/lingala/zip4j/io/PartInputStream/bytesRead J
lsub
l2i
istore 3
iload 3
istore 4
iload 3
ifne L12
aload 0
invokevirtual net/lingala/zip4j/io/PartInputStream/checkAndReadAESMacBytes()V
iconst_m1
ireturn
L12:
iload 4
istore 3
aload 0
getfield net/lingala/zip4j/io/PartInputStream/unzipEngine Lnet/lingala/zip4j/unzip/UnzipEngine;
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/getDecrypter()Lnet/lingala/zip4j/crypto/IDecrypter;
instanceof net/lingala/zip4j/crypto/AESDecrypter
ifeq L13
iload 4
istore 3
aload 0
getfield net/lingala/zip4j/io/PartInputStream/bytesRead J
iload 4
i2l
ladd
aload 0
getfield net/lingala/zip4j/io/PartInputStream/length J
lcmp
ifge L13
iload 4
istore 3
iload 4
bipush 16
irem
ifeq L13
iload 4
iload 4
bipush 16
irem
isub
istore 3
L13:
aload 0
getfield net/lingala/zip4j/io/PartInputStream/raf Ljava/io/RandomAccessFile;
astore 5
aload 5
monitorenter
L0:
aload 0
aload 0
getfield net/lingala/zip4j/io/PartInputStream/raf Ljava/io/RandomAccessFile;
aload 1
iload 2
iload 3
invokevirtual java/io/RandomAccessFile/read([BII)I
putfield net/lingala/zip4j/io/PartInputStream/count I
aload 0
getfield net/lingala/zip4j/io/PartInputStream/count I
iload 3
if_icmpge L5
aload 0
getfield net/lingala/zip4j/io/PartInputStream/unzipEngine Lnet/lingala/zip4j/unzip/UnzipEngine;
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/getZipModel()Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifeq L5
aload 0
getfield net/lingala/zip4j/io/PartInputStream/raf Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/close()V
aload 0
aload 0
getfield net/lingala/zip4j/io/PartInputStream/unzipEngine Lnet/lingala/zip4j/unzip/UnzipEngine;
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/startNextSplitFile()Ljava/io/RandomAccessFile;
putfield net/lingala/zip4j/io/PartInputStream/raf Ljava/io/RandomAccessFile;
aload 0
getfield net/lingala/zip4j/io/PartInputStream/count I
ifge L1
aload 0
iconst_0
putfield net/lingala/zip4j/io/PartInputStream/count I
L1:
aload 0
getfield net/lingala/zip4j/io/PartInputStream/raf Ljava/io/RandomAccessFile;
aload 1
aload 0
getfield net/lingala/zip4j/io/PartInputStream/count I
iload 3
aload 0
getfield net/lingala/zip4j/io/PartInputStream/count I
isub
invokevirtual java/io/RandomAccessFile/read([BII)I
istore 3
L3:
iload 3
ifle L5
L4:
aload 0
aload 0
getfield net/lingala/zip4j/io/PartInputStream/count I
iload 3
iadd
putfield net/lingala/zip4j/io/PartInputStream/count I
L5:
aload 5
monitorexit
L6:
aload 0
getfield net/lingala/zip4j/io/PartInputStream/count I
ifle L14
aload 0
getfield net/lingala/zip4j/io/PartInputStream/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
ifnull L8
L7:
aload 0
getfield net/lingala/zip4j/io/PartInputStream/decrypter Lnet/lingala/zip4j/crypto/IDecrypter;
aload 1
iload 2
aload 0
getfield net/lingala/zip4j/io/PartInputStream/count I
invokeinterface net/lingala/zip4j/crypto/IDecrypter/decryptData([BII)I 3
pop
L8:
aload 0
aload 0
getfield net/lingala/zip4j/io/PartInputStream/bytesRead J
aload 0
getfield net/lingala/zip4j/io/PartInputStream/count I
i2l
ladd
putfield net/lingala/zip4j/io/PartInputStream/bytesRead J
L14:
aload 0
getfield net/lingala/zip4j/io/PartInputStream/bytesRead J
aload 0
getfield net/lingala/zip4j/io/PartInputStream/length J
lcmp
iflt L15
aload 0
invokevirtual net/lingala/zip4j/io/PartInputStream/checkAndReadAESMacBytes()V
L15:
aload 0
getfield net/lingala/zip4j/io/PartInputStream/count I
ireturn
L2:
astore 1
L10:
aload 5
monitorexit
L11:
aload 1
athrow
L9:
astore 1
new java/io/IOException
dup
aload 1
invokevirtual net/lingala/zip4j/exception/ZipException/getMessage()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
.limit locals 6
.limit stack 6
.end method

.method public seek(J)V
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/PartInputStream/raf Ljava/io/RandomAccessFile;
lload 1
invokevirtual java/io/RandomAccessFile/seek(J)V
return
.limit locals 3
.limit stack 3
.end method

.method public skip(J)J
.throws java/io/IOException
lload 1
lconst_0
lcmp
ifge L0
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L0:
lload 1
lstore 3
lload 1
aload 0
getfield net/lingala/zip4j/io/PartInputStream/length J
aload 0
getfield net/lingala/zip4j/io/PartInputStream/bytesRead J
lsub
lcmp
ifle L1
aload 0
getfield net/lingala/zip4j/io/PartInputStream/length J
aload 0
getfield net/lingala/zip4j/io/PartInputStream/bytesRead J
lsub
lstore 3
L1:
aload 0
aload 0
getfield net/lingala/zip4j/io/PartInputStream/bytesRead J
lload 3
ladd
putfield net/lingala/zip4j/io/PartInputStream/bytesRead J
lload 3
lreturn
.limit locals 5
.limit stack 6
.end method
