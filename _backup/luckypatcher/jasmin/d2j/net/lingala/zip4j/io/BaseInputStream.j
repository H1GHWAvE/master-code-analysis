.bytecode 50.0
.class public synchronized abstract net/lingala/zip4j/io/BaseInputStream
.super java/io/InputStream

.method public <init>()V
aload 0
invokespecial java/io/InputStream/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public available()I
.throws java/io/IOException
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getUnzipEngine()Lnet/lingala/zip4j/unzip/UnzipEngine;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public read()I
.throws java/io/IOException
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public seek(J)V
.throws java/io/IOException
return
.limit locals 3
.limit stack 0
.end method
