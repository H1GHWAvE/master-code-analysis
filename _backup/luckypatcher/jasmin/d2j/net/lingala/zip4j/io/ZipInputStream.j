.bytecode 50.0
.class public synchronized net/lingala/zip4j/io/ZipInputStream
.super java/io/InputStream

.field private 'is' Lnet/lingala/zip4j/io/BaseInputStream;

.method public <init>(Lnet/lingala/zip4j/io/BaseInputStream;)V
aload 0
invokespecial java/io/InputStream/<init>()V
aload 0
aload 1
putfield net/lingala/zip4j/io/ZipInputStream/is Lnet/lingala/zip4j/io/BaseInputStream;
return
.limit locals 2
.limit stack 2
.end method

.method public available()I
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/ZipInputStream/is Lnet/lingala/zip4j/io/BaseInputStream;
invokevirtual net/lingala/zip4j/io/BaseInputStream/available()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public close()V
.throws java/io/IOException
aload 0
iconst_0
invokevirtual net/lingala/zip4j/io/ZipInputStream/close(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method public close(Z)V
.throws java/io/IOException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch net/lingala/zip4j/exception/ZipException from L3 to L4 using L2
L0:
aload 0
getfield net/lingala/zip4j/io/ZipInputStream/is Lnet/lingala/zip4j/io/BaseInputStream;
invokevirtual net/lingala/zip4j/io/BaseInputStream/close()V
L1:
iload 1
ifne L4
L3:
aload 0
getfield net/lingala/zip4j/io/ZipInputStream/is Lnet/lingala/zip4j/io/BaseInputStream;
invokevirtual net/lingala/zip4j/io/BaseInputStream/getUnzipEngine()Lnet/lingala/zip4j/unzip/UnzipEngine;
ifnull L4
aload 0
getfield net/lingala/zip4j/io/ZipInputStream/is Lnet/lingala/zip4j/io/BaseInputStream;
invokevirtual net/lingala/zip4j/io/BaseInputStream/getUnzipEngine()Lnet/lingala/zip4j/unzip/UnzipEngine;
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/checkCRC()V
L4:
return
L2:
astore 2
new java/io/IOException
dup
aload 2
invokevirtual net/lingala/zip4j/exception/ZipException/getMessage()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public read()I
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/ZipInputStream/is Lnet/lingala/zip4j/io/BaseInputStream;
invokevirtual net/lingala/zip4j/io/BaseInputStream/read()I
istore 1
iload 1
iconst_m1
if_icmpeq L0
aload 0
getfield net/lingala/zip4j/io/ZipInputStream/is Lnet/lingala/zip4j/io/BaseInputStream;
invokevirtual net/lingala/zip4j/io/BaseInputStream/getUnzipEngine()Lnet/lingala/zip4j/unzip/UnzipEngine;
iload 1
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/updateCRC(I)V
L0:
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public read([B)I
.throws java/io/IOException
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual net/lingala/zip4j/io/ZipInputStream/read([BII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method public read([BII)I
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/ZipInputStream/is Lnet/lingala/zip4j/io/BaseInputStream;
aload 1
iload 2
iload 3
invokevirtual net/lingala/zip4j/io/BaseInputStream/read([BII)I
istore 3
iload 3
ifle L0
aload 0
getfield net/lingala/zip4j/io/ZipInputStream/is Lnet/lingala/zip4j/io/BaseInputStream;
invokevirtual net/lingala/zip4j/io/BaseInputStream/getUnzipEngine()Lnet/lingala/zip4j/unzip/UnzipEngine;
ifnull L0
aload 0
getfield net/lingala/zip4j/io/ZipInputStream/is Lnet/lingala/zip4j/io/BaseInputStream;
invokevirtual net/lingala/zip4j/io/BaseInputStream/getUnzipEngine()Lnet/lingala/zip4j/unzip/UnzipEngine;
aload 1
iload 2
iload 3
invokevirtual net/lingala/zip4j/unzip/UnzipEngine/updateCRC([BII)V
L0:
iload 3
ireturn
.limit locals 4
.limit stack 4
.end method

.method public skip(J)J
.throws java/io/IOException
aload 0
getfield net/lingala/zip4j/io/ZipInputStream/is Lnet/lingala/zip4j/io/BaseInputStream;
lload 1
invokevirtual net/lingala/zip4j/io/BaseInputStream/skip(J)J
lreturn
.limit locals 3
.limit stack 3
.end method
