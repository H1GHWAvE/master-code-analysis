.bytecode 50.0
.class public synchronized net/lingala/zip4j/exception/ZipException
.super java/lang/Exception

.field private static final 'serialVersionUID' J = 1L


.field private 'code' I

.method public <init>()V
aload 0
invokespecial java/lang/Exception/<init>()V
aload 0
iconst_m1
putfield net/lingala/zip4j/exception/ZipException/code I
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Ljava/lang/String;)V
aload 0
aload 1
invokespecial java/lang/Exception/<init>(Ljava/lang/String;)V
aload 0
iconst_m1
putfield net/lingala/zip4j/exception/ZipException/code I
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Ljava/lang/String;I)V
aload 0
aload 1
invokespecial java/lang/Exception/<init>(Ljava/lang/String;)V
aload 0
iconst_m1
putfield net/lingala/zip4j/exception/ZipException/code I
aload 0
iload 2
putfield net/lingala/zip4j/exception/ZipException/code I
return
.limit locals 3
.limit stack 2
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
aload 1
aload 2
invokespecial java/lang/Exception/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
iconst_m1
putfield net/lingala/zip4j/exception/ZipException/code I
return
.limit locals 3
.limit stack 3
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/Throwable;I)V
aload 0
aload 1
aload 2
invokespecial java/lang/Exception/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
iconst_m1
putfield net/lingala/zip4j/exception/ZipException/code I
aload 0
iload 3
putfield net/lingala/zip4j/exception/ZipException/code I
return
.limit locals 4
.limit stack 3
.end method

.method public <init>(Ljava/lang/Throwable;)V
aload 0
aload 1
invokespecial java/lang/Exception/<init>(Ljava/lang/Throwable;)V
aload 0
iconst_m1
putfield net/lingala/zip4j/exception/ZipException/code I
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Ljava/lang/Throwable;I)V
aload 0
aload 1
invokespecial java/lang/Exception/<init>(Ljava/lang/Throwable;)V
aload 0
iconst_m1
putfield net/lingala/zip4j/exception/ZipException/code I
aload 0
iload 2
putfield net/lingala/zip4j/exception/ZipException/code I
return
.limit locals 3
.limit stack 2
.end method

.method public getCode()I
aload 0
getfield net/lingala/zip4j/exception/ZipException/code I
ireturn
.limit locals 1
.limit stack 1
.end method
