.bytecode 50.0
.class public synchronized net/lingala/zip4j/core/ZipFile
.super java/lang/Object

.field private 'file' Ljava/lang/String;

.field private 'fileNameCharset' Ljava/lang/String;

.field private 'isEncrypted' Z

.field private 'mode' I

.field private 'progressMonitor' Lnet/lingala/zip4j/progress/ProgressMonitor;

.field private 'runInThread' Z

.field private 'zipModel' Lnet/lingala/zip4j/model/ZipModel;

.method public <init>(Ljava/io/File;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "Input zip file parameter is not null"
iconst_1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;I)V
athrow
L0:
aload 0
aload 1
invokevirtual java/io/File/getPath()Ljava/lang/String;
putfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
aload 0
iconst_2
putfield net/lingala/zip4j/core/ZipFile/mode I
aload 0
new net/lingala/zip4j/progress/ProgressMonitor
dup
invokespecial net/lingala/zip4j/progress/ProgressMonitor/<init>()V
putfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
aload 0
iconst_0
putfield net/lingala/zip4j/core/ZipFile/runInThread Z
return
.limit locals 2
.limit stack 4
.end method

.method public <init>(Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/io/File;)V
return
.limit locals 2
.limit stack 4
.end method

.method private addFolder(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;Z)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/checkZipModel()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "internal error: zip model is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 3
ifeq L1
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifeq L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "This is a split archive. Zip file format does not allow updating split/spanned files"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
new net/lingala/zip4j/zip/ZipEngine
dup
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokespecial net/lingala/zip4j/zip/ZipEngine/<init>(Lnet/lingala/zip4j/model/ZipModel;)V
aload 1
aload 2
aload 0
getfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
aload 0
getfield net/lingala/zip4j/core/ZipFile/runInThread Z
invokevirtual net/lingala/zip4j/zip/ZipEngine/addFolderToZip(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
return
.limit locals 4
.limit stack 5
.end method

.method private checkZipModel()V
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L0
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
ifeq L1
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/readZipInfo()V
L0:
return
L1:
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/createNewZipModel()V
return
.limit locals 1
.limit stack 1
.end method

.method private createNewZipModel()V
aload 0
new net/lingala/zip4j/model/ZipModel
dup
invokespecial net/lingala/zip4j/model/ZipModel/<init>()V
putfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokevirtual net/lingala/zip4j/model/ZipModel/setZipFile(Ljava/lang/String;)V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
getfield net/lingala/zip4j/core/ZipFile/fileNameCharset Ljava/lang/String;
invokevirtual net/lingala/zip4j/model/ZipModel/setFileNameCharset(Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method private readZipInfo()V
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/FileNotFoundException from L1 to L4 using L5
.catch all from L1 to L4 using L6
.catch java/io/IOException from L7 to L8 using L9
.catch all from L10 to L3 using L3
.catch java/io/IOException from L11 to L12 using L13
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
ifne L14
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip file does not exist"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L14:
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileReadAccess(Ljava/lang/String;)Z
ifne L15
new net/lingala/zip4j/exception/ZipException
dup
ldc "no read access for the input zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L15:
aload 0
getfield net/lingala/zip4j/core/ZipFile/mode I
iconst_2
if_icmpeq L16
new net/lingala/zip4j/exception/ZipException
dup
ldc "Invalid mode"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L16:
aconst_null
astore 1
aconst_null
astore 3
L0:
new java/io/RandomAccessFile
dup
new java/io/File
dup
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 2
L1:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L4
aload 0
new net/lingala/zip4j/core/HeaderReader
dup
aload 2
invokespecial net/lingala/zip4j/core/HeaderReader/<init>(Ljava/io/RandomAccessFile;)V
aload 0
getfield net/lingala/zip4j/core/ZipFile/fileNameCharset Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/HeaderReader/readAllHeaders(Ljava/lang/String;)Lnet/lingala/zip4j/model/ZipModel;
putfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnull L4
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokevirtual net/lingala/zip4j/model/ZipModel/setZipFile(Ljava/lang/String;)V
L4:
aload 2
ifnull L8
L7:
aload 2
invokevirtual java/io/RandomAccessFile/close()V
L8:
return
L2:
astore 2
aload 3
astore 1
L10:
new net/lingala/zip4j/exception/ZipException
dup
aload 2
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 2
L17:
aload 1
ifnull L12
L11:
aload 1
invokevirtual java/io/RandomAccessFile/close()V
L12:
aload 2
athrow
L9:
astore 1
return
L13:
astore 1
goto L12
L6:
astore 3
aload 2
astore 1
aload 3
astore 2
goto L17
L5:
astore 3
aload 2
astore 1
aload 3
astore 2
goto L10
.limit locals 4
.limit stack 5
.end method

.method public addFile(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 3
aload 3
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
aload 3
aload 2
invokevirtual net/lingala/zip4j/core/ZipFile/addFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;)V
return
.limit locals 4
.limit stack 3
.end method

.method public addFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/checkZipModel()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "internal error: zip model is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
ifnonnull L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file ArrayList is null, cannot add files"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
iconst_1
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkArrayListTypes(Ljava/util/ArrayList;I)Z
ifne L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "One or more elements in the input ArrayList is not of type File"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 2
ifnonnull L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "input parameters are null, cannot add files to zip"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
getfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/getState()I
iconst_1
if_icmpne L4
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid operation - Zip4j is in busy state"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
ifeq L5
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifeq L5
new net/lingala/zip4j/exception/ZipException
dup
ldc "Zip file already exists. Zip file format does not allow updating split/spanned files"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L5:
new net/lingala/zip4j/zip/ZipEngine
dup
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokespecial net/lingala/zip4j/zip/ZipEngine/<init>(Lnet/lingala/zip4j/model/ZipModel;)V
aload 1
aload 2
aload 0
getfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
aload 0
getfield net/lingala/zip4j/core/ZipFile/runInThread Z
invokevirtual net/lingala/zip4j/zip/ZipEngine/addFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
return
.limit locals 3
.limit stack 5
.end method

.method public addFolder(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input path is null, cannot add folder to zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
ifnonnull L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "input parameters are null, cannot add folder to zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 1
aload 2
iconst_1
invokespecial net/lingala/zip4j/core/ZipFile/addFolder(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;Z)V
return
.limit locals 3
.limit stack 4
.end method

.method public addFolder(Ljava/lang/String;Lnet/lingala/zip4j/model/ZipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input path is null or empty, cannot add folder to zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 2
invokevirtual net/lingala/zip4j/core/ZipFile/addFolder(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
return
.limit locals 3
.limit stack 4
.end method

.method public addStream(Ljava/io/InputStream;Lnet/lingala/zip4j/model/ZipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "inputstream is null, cannot add file to zip"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
ifnonnull L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip parameters are null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iconst_0
invokevirtual net/lingala/zip4j/core/ZipFile/setRunInThread(Z)V
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/checkZipModel()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "internal error: zip model is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
ifeq L3
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifeq L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "Zip file already exists. Zip file format does not allow updating split/spanned files"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
new net/lingala/zip4j/zip/ZipEngine
dup
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokespecial net/lingala/zip4j/zip/ZipEngine/<init>(Lnet/lingala/zip4j/model/ZipModel;)V
aload 1
aload 2
invokevirtual net/lingala/zip4j/zip/ZipEngine/addStreamToZip(Ljava/io/InputStream;Lnet/lingala/zip4j/model/ZipParameters;)V
return
.limit locals 3
.limit stack 3
.end method

.method public createZipFile(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 3
aload 3
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
aload 3
aload 2
iconst_0
ldc2_w -1L
invokevirtual net/lingala/zip4j/core/ZipFile/createZipFile(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;ZJ)V
return
.limit locals 4
.limit stack 6
.end method

.method public createZipFile(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;ZJ)V
.throws net/lingala/zip4j/exception/ZipException
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 6
aload 6
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
aload 6
aload 2
iload 3
lload 4
invokevirtual net/lingala/zip4j/core/ZipFile/createZipFile(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;ZJ)V
return
.limit locals 7
.limit stack 6
.end method

.method public createZipFile(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
aload 2
iconst_0
ldc2_w -1L
invokevirtual net/lingala/zip4j/core/ZipFile/createZipFile(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;ZJ)V
return
.limit locals 3
.limit stack 6
.end method

.method public createZipFile(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;ZJ)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip file path is empty"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
ifeq L1
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "zip file: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " already exists. To add files to existing zip file use addFile method"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
ifnonnull L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file ArrayList is null, cannot create zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 1
iconst_1
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkArrayListTypes(Ljava/util/ArrayList;I)Z
ifne L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "One or more elements in the input ArrayList is not of type File"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/createNewZipModel()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
iload 3
invokevirtual net/lingala/zip4j/model/ZipModel/setSplitArchive(Z)V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
lload 4
invokevirtual net/lingala/zip4j/model/ZipModel/setSplitLength(J)V
aload 0
aload 1
aload 2
invokevirtual net/lingala/zip4j/core/ZipFile/addFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;)V
return
.limit locals 6
.limit stack 4
.end method

.method public createZipFileFromFolder(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;ZJ)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "folderToAdd is null, cannot create zip file from folder"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
ifnonnull L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "input parameters are null, cannot create zip file from folder"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
ifeq L2
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "zip file: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " already exists. To add files to existing zip file use addFolder method"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/createNewZipModel()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
iload 3
invokevirtual net/lingala/zip4j/model/ZipModel/setSplitArchive(Z)V
iload 3
ifeq L3
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
lload 4
invokevirtual net/lingala/zip4j/model/ZipModel/setSplitLength(J)V
L3:
aload 0
aload 1
aload 2
iconst_0
invokespecial net/lingala/zip4j/core/ZipFile/addFolder(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;Z)V
return
.limit locals 6
.limit stack 4
.end method

.method public createZipFileFromFolder(Ljava/lang/String;Lnet/lingala/zip4j/model/ZipParameters;ZJ)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "folderToAdd is empty or null, cannot create Zip File from folder"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 2
iload 3
lload 4
invokevirtual net/lingala/zip4j/core/ZipFile/createZipFileFromFolder(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;ZJ)V
return
.limit locals 6
.limit stack 6
.end method

.method public extractAll(Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
aconst_null
invokevirtual net/lingala/zip4j/core/ZipFile/extractAll(Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;)V
return
.limit locals 2
.limit stack 3
.end method

.method public extractAll(Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "output path is null or invalid"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkOutputFolder(Ljava/lang/String;)Z
ifne L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid output path"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L2
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/readZipInfo()V
L2:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "Internal error occurred when extracting zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
getfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/getState()I
iconst_1
if_icmpne L4
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid operation - Zip4j is in busy state"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L4:
new net/lingala/zip4j/unzip/Unzip
dup
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokespecial net/lingala/zip4j/unzip/Unzip/<init>(Lnet/lingala/zip4j/model/ZipModel;)V
aload 2
aload 1
aload 0
getfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
aload 0
getfield net/lingala/zip4j/core/ZipFile/runInThread Z
invokevirtual net/lingala/zip4j/unzip/Unzip/extractAll(Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
return
.limit locals 3
.limit stack 5
.end method

.method public extractFile(Ljava/lang/String;Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
aload 2
aconst_null
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;)V
return
.limit locals 3
.limit stack 4
.end method

.method public extractFile(Ljava/lang/String;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
aload 2
aload 3
aconst_null
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;)V
return
.limit locals 4
.limit stack 5
.end method

.method public extractFile(Ljava/lang/String;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "file to extract is null or empty, cannot extract file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "destination string path is empty or null, cannot extract file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/readZipInfo()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileHeader(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Lnet/lingala/zip4j/model/FileHeader;
astore 1
aload 1
ifnonnull L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "file header not found for given file name, cannot extract file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
getfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/getState()I
iconst_1
if_icmpne L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid operation - Zip4j is in busy state"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 1
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 2
aload 3
aload 4
aload 0
getfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
aload 0
getfield net/lingala/zip4j/core/ZipFile/runInThread Z
invokevirtual net/lingala/zip4j/model/FileHeader/extractFile(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
return
.limit locals 5
.limit stack 7
.end method

.method public extractFile(Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
aload 2
aconst_null
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;)V
return
.limit locals 3
.limit stack 4
.end method

.method public extractFile(Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
aload 2
aload 3
aconst_null
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;)V
return
.limit locals 4
.limit stack 5
.end method

.method public extractFile(Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file header is null, cannot extract file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "destination path is empty or null, cannot extract file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/readZipInfo()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/getState()I
iconst_1
if_icmpne L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid operation - Zip4j is in busy state"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 1
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 2
aload 3
aload 4
aload 0
getfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
aload 0
getfield net/lingala/zip4j/core/ZipFile/runInThread Z
invokevirtual net/lingala/zip4j/model/FileHeader/extractFile(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
return
.limit locals 5
.limit stack 7
.end method

.method public getComment()Ljava/lang/String;
.throws net/lingala/zip4j/exception/ZipException
aload 0
aconst_null
invokevirtual net/lingala/zip4j/core/ZipFile/getComment(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public getComment(Ljava/lang/String;)Ljava/lang/String;
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
aload 1
astore 2
aload 1
ifnonnull L3
ldc "windows-1254"
invokestatic net/lingala/zip4j/util/Zip4jUtil/isSupportedCharset(Ljava/lang/String;)Z
ifeq L4
ldc "windows-1254"
astore 2
L3:
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
ifeq L5
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/checkZipModel()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L6
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip model is null, cannot read comment"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L4:
getstatic net/lingala/zip4j/util/InternalZipConstants/CHARSET_DEFAULT Ljava/lang/String;
astore 2
goto L3
L5:
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip file does not exist, cannot read comment"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L6:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
ifnonnull L7
new net/lingala/zip4j/exception/ZipException
dup
ldc "end of central directory record is null, cannot read comment"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L7:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getCommentBytes()[B
ifnull L8
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getCommentBytes()[B
arraylength
ifgt L0
L8:
aconst_null
areturn
L0:
new java/lang/String
dup
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getCommentBytes()[B
aload 2
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
astore 1
L1:
aload 1
areturn
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 4
.end method

.method public getFile()Ljava/io/File;
new java/io/File
dup
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public getFileHeader(Ljava/lang/String;)Lnet/lingala/zip4j/model/FileHeader;
.throws net/lingala/zip4j/exception/ZipException
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file name is emtpy or null, cannot get FileHeader"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/readZipInfo()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnull L1
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnonnull L2
L1:
aconst_null
areturn
L2:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileHeader(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Lnet/lingala/zip4j/model/FileHeader;
areturn
.limit locals 2
.limit stack 3
.end method

.method public getFileHeaders()Ljava/util/List;
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/readZipInfo()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnull L0
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnonnull L1
L0:
aconst_null
areturn
L1:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getInputStream(Lnet/lingala/zip4j/model/FileHeader;)Lnet/lingala/zip4j/io/ZipInputStream;
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "FileHeader is null, cannot get InputStream"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/checkZipModel()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip model is null, cannot get inputstream"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
new net/lingala/zip4j/unzip/Unzip
dup
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokespecial net/lingala/zip4j/unzip/Unzip/<init>(Lnet/lingala/zip4j/model/ZipModel;)V
aload 1
invokevirtual net/lingala/zip4j/unzip/Unzip/getInputStream(Lnet/lingala/zip4j/model/FileHeader;)Lnet/lingala/zip4j/io/ZipInputStream;
areturn
.limit locals 2
.limit stack 3
.end method

.method public getProgressMonitor()Lnet/lingala/zip4j/progress/ProgressMonitor;
aload 0
getfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSplitZipFiles()Ljava/util/ArrayList;
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/checkZipModel()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokestatic net/lingala/zip4j/util/Zip4jUtil/getSplitZipFiles(Lnet/lingala/zip4j/model/ZipModel;)Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method public isEncrypted()Z
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L0
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/readZipInfo()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "Zip Model is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnull L1
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
ifnonnull L2
L1:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
astore 2
iconst_0
istore 1
L3:
iload 1
aload 2
invokevirtual java/util/ArrayList/size()I
if_icmpge L4
aload 2
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
astore 3
aload 3
ifnull L5
aload 3
invokevirtual net/lingala/zip4j/model/FileHeader/isEncrypted()Z
ifeq L5
aload 0
iconst_1
putfield net/lingala/zip4j/core/ZipFile/isEncrypted Z
L4:
aload 0
getfield net/lingala/zip4j/core/ZipFile/isEncrypted Z
ireturn
L5:
iload 1
iconst_1
iadd
istore 1
goto L3
.limit locals 4
.limit stack 3
.end method

.method public isRunInThread()Z
aload 0
getfield net/lingala/zip4j/core/ZipFile/runInThread Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isSplitArchive()Z
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L0
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/readZipInfo()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "Zip Model is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ireturn
.limit locals 1
.limit stack 3
.end method

.method public isValidZipFile()Z
.catch java/lang/Exception from L0 to L1 using L2
L0:
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/readZipInfo()V
L1:
iconst_1
ireturn
L2:
astore 1
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public mergeSplitFiles(Ljava/io/File;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "outputZipFile is null, cannot merge split files"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual java/io/File/exists()Z
ifeq L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "output Zip File already exists"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/checkZipModel()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip model is null, corrupt zip file?"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
new net/lingala/zip4j/util/ArchiveMaintainer
dup
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/<init>()V
astore 2
aload 2
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
getfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
invokevirtual net/lingala/zip4j/util/ArchiveMaintainer/initProgressMonitorForMergeOp(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
aload 2
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 1
aload 0
getfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
aload 0
getfield net/lingala/zip4j/core/ZipFile/runInThread Z
invokevirtual net/lingala/zip4j/util/ArchiveMaintainer/mergeSplitZipFiles(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/File;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
return
.limit locals 3
.limit stack 5
.end method

.method public removeFile(Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "file name is empty or null, cannot remove file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L1
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
ifeq L1
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/readZipInfo()V
L1:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifeq L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "Zip file format does not allow updating split/spanned files"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileHeader(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Lnet/lingala/zip4j/model/FileHeader;
astore 2
aload 2
ifnonnull L3
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "could not find file header for file: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
aload 2
invokevirtual net/lingala/zip4j/core/ZipFile/removeFile(Lnet/lingala/zip4j/model/FileHeader;)V
return
.limit locals 3
.limit stack 4
.end method

.method public removeFile(Lnet/lingala/zip4j/model/FileHeader;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "file header is null, cannot remove file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L1
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
ifeq L1
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/readZipInfo()V
L1:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifeq L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "Zip file format does not allow updating split/spanned files"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
new net/lingala/zip4j/util/ArchiveMaintainer
dup
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/<init>()V
astore 2
aload 2
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 1
aload 0
getfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
invokevirtual net/lingala/zip4j/util/ArchiveMaintainer/initProgressMonitorForRemoveOp(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
aload 2
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 1
aload 0
getfield net/lingala/zip4j/core/ZipFile/progressMonitor Lnet/lingala/zip4j/progress/ProgressMonitor;
aload 0
getfield net/lingala/zip4j/core/ZipFile/runInThread Z
invokevirtual net/lingala/zip4j/util/ArchiveMaintainer/removeZipFile(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)Ljava/util/HashMap;
pop
return
.limit locals 3
.limit stack 5
.end method

.method public setComment(Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input comment is null, cannot update zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/core/ZipFile/file Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
ifne L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip file does not exist, cannot set comment for zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/readZipInfo()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "zipModel is null, cannot update zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
ifnonnull L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "end of central directory is null, cannot set comment"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
new net/lingala/zip4j/util/ArchiveMaintainer
dup
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/<init>()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 1
invokevirtual net/lingala/zip4j/util/ArchiveMaintainer/setComment(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method public setFileNameCharset(Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "null or empty charset name"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isSupportedCharset(Ljava/lang/String;)Z
ifne L1
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "unsupported charset: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 1
putfield net/lingala/zip4j/core/ZipFile/fileNameCharset Ljava/lang/String;
return
.limit locals 2
.limit stack 4
.end method

.method public setPassword(Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L0:
aload 0
aload 1
invokevirtual java/lang/String/toCharArray()[C
invokevirtual net/lingala/zip4j/core/ZipFile/setPassword([C)V
return
.limit locals 2
.limit stack 2
.end method

.method public setPassword([C)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L0
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/readZipInfo()V
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "Zip Model is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnull L1
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
ifnonnull L2
L1:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
iconst_0
istore 2
L3:
iload 2
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L4
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
ifnull L5
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
invokevirtual net/lingala/zip4j/model/FileHeader/isEncrypted()Z
ifeq L5
aload 0
getfield net/lingala/zip4j/core/ZipFile/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/setPassword([C)V
L5:
iload 2
iconst_1
iadd
istore 2
goto L3
L4:
return
.limit locals 3
.limit stack 3
.end method

.method public setRunInThread(Z)V
aload 0
iload 1
putfield net/lingala/zip4j/core/ZipFile/runInThread Z
return
.limit locals 2
.limit stack 2
.end method
