.bytecode 50.0
.class public synchronized net/lingala/zip4j/core/HeaderReader
.super java/lang/Object

.field private 'zip4jRaf' Ljava/io/RandomAccessFile;

.field private 'zipModel' Lnet/lingala/zip4j/model/ZipModel;

.method public <init>(Ljava/io/RandomAccessFile;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 0
aload 1
putfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
return
.limit locals 2
.limit stack 2
.end method

.method private getLongByteFromIntByte([B)[B
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input parameter is null, cannot expand to 8 bytes"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
arraylength
iconst_4
if_icmpeq L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid byte length, cannot expand to 8 bytes"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
bipush 8
newarray byte
dup
iconst_0
aload 1
iconst_0
baload
bastore
dup
iconst_1
aload 1
iconst_1
baload
bastore
dup
iconst_2
aload 1
iconst_2
baload
bastore
dup
iconst_3
aload 1
iconst_3
baload
bastore
dup
iconst_4
iconst_0
bastore
dup
iconst_5
iconst_0
bastore
dup
bipush 6
iconst_0
bastore
dup
bipush 7
iconst_0
bastore
areturn
.limit locals 2
.limit stack 5
.end method

.method private readAESExtraDataRecord(Ljava/util/ArrayList;)Lnet/lingala/zip4j/model/AESExtraDataRecord;
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
L1:
aconst_null
areturn
L0:
iconst_0
istore 2
L2:
iload 2
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpge L1
aload 1
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/ExtraDataRecord
astore 3
aload 3
ifnonnull L3
L4:
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
aload 3
invokevirtual net/lingala/zip4j/model/ExtraDataRecord/getHeader()J
ldc2_w 39169L
lcmp
ifne L4
aload 3
invokevirtual net/lingala/zip4j/model/ExtraDataRecord/getData()[B
ifnonnull L5
new net/lingala/zip4j/exception/ZipException
dup
ldc "corrput AES extra data records"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L5:
new net/lingala/zip4j/model/AESExtraDataRecord
dup
invokespecial net/lingala/zip4j/model/AESExtraDataRecord/<init>()V
astore 1
aload 1
ldc2_w 39169L
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/setSignature(J)V
aload 1
aload 3
invokevirtual net/lingala/zip4j/model/ExtraDataRecord/getSizeOfData()I
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/setDataSize(I)V
aload 3
invokevirtual net/lingala/zip4j/model/ExtraDataRecord/getData()[B
astore 3
aload 1
aload 3
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/setVersionNumber(I)V
iconst_2
newarray byte
astore 4
aload 3
iconst_2
aload 4
iconst_0
iconst_2
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
new java/lang/String
dup
aload 4
invokespecial java/lang/String/<init>([B)V
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/setVendorID(Ljava/lang/String;)V
aload 1
aload 3
iconst_4
baload
sipush 255
iand
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/setAesStrength(I)V
aload 1
aload 3
iconst_5
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/setCompressionMethod(I)V
aload 1
areturn
.limit locals 5
.limit stack 5
.end method

.method private readAndSaveAESExtraDataRecord(Lnet/lingala/zip4j/model/FileHeader;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "file header is null in reading Zip64 Extended Info"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getExtraDataRecords()Ljava/util/ArrayList;
ifnull L1
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getExtraDataRecords()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifgt L2
L1:
return
L2:
aload 0
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getExtraDataRecords()Ljava/util/ArrayList;
invokespecial net/lingala/zip4j/core/HeaderReader/readAESExtraDataRecord(Ljava/util/ArrayList;)Lnet/lingala/zip4j/model/AESExtraDataRecord;
astore 2
aload 2
ifnull L1
aload 1
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/setAesExtraDataRecord(Lnet/lingala/zip4j/model/AESExtraDataRecord;)V
aload 1
bipush 99
invokevirtual net/lingala/zip4j/model/FileHeader/setEncryptionMethod(I)V
return
.limit locals 3
.limit stack 3
.end method

.method private readAndSaveAESExtraDataRecord(Lnet/lingala/zip4j/model/LocalFileHeader;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "file header is null in reading Zip64 Extended Info"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getExtraDataRecords()Ljava/util/ArrayList;
ifnull L1
aload 1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getExtraDataRecords()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifgt L2
L1:
return
L2:
aload 0
aload 1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getExtraDataRecords()Ljava/util/ArrayList;
invokespecial net/lingala/zip4j/core/HeaderReader/readAESExtraDataRecord(Ljava/util/ArrayList;)Lnet/lingala/zip4j/model/AESExtraDataRecord;
astore 2
aload 2
ifnull L1
aload 1
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setAesExtraDataRecord(Lnet/lingala/zip4j/model/AESExtraDataRecord;)V
aload 1
bipush 99
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setEncryptionMethod(I)V
return
.limit locals 3
.limit stack 3
.end method

.method private readAndSaveExtraDataRecord(Lnet/lingala/zip4j/model/FileHeader;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid file handler when trying to read extra data record"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
ifnonnull L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "file header is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getExtraFieldLength()I
istore 2
iload 2
ifgt L2
return
L2:
aload 1
aload 0
iload 2
invokespecial net/lingala/zip4j/core/HeaderReader/readExtraDataRecords(I)Ljava/util/ArrayList;
invokevirtual net/lingala/zip4j/model/FileHeader/setExtraDataRecords(Ljava/util/ArrayList;)V
return
.limit locals 3
.limit stack 3
.end method

.method private readAndSaveExtraDataRecord(Lnet/lingala/zip4j/model/LocalFileHeader;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid file handler when trying to read extra data record"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
ifnonnull L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "file header is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getExtraFieldLength()I
istore 2
iload 2
ifgt L2
return
L2:
aload 1
aload 0
iload 2
invokespecial net/lingala/zip4j/core/HeaderReader/readExtraDataRecords(I)Ljava/util/ArrayList;
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setExtraDataRecords(Ljava/util/ArrayList;)V
return
.limit locals 3
.limit stack 3
.end method

.method private readAndSaveZip64ExtendedInfo(Lnet/lingala/zip4j/model/FileHeader;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "file header is null in reading Zip64 Extended Info"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getExtraDataRecords()Ljava/util/ArrayList;
ifnull L1
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getExtraDataRecords()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifgt L2
L1:
return
L2:
aload 0
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getExtraDataRecords()Ljava/util/ArrayList;
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getUncompressedSize()J
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getCompressedSize()J
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getOffsetLocalHeader()J
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getDiskNumberStart()I
invokespecial net/lingala/zip4j/core/HeaderReader/readZip64ExtendedInfo(Ljava/util/ArrayList;JJJI)Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
astore 2
aload 2
ifnull L1
aload 1
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/setZip64ExtendedInfo(Lnet/lingala/zip4j/model/Zip64ExtendedInfo;)V
aload 2
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getUnCompressedSize()J
ldc2_w -1L
lcmp
ifeq L3
aload 1
aload 2
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getUnCompressedSize()J
invokevirtual net/lingala/zip4j/model/FileHeader/setUncompressedSize(J)V
L3:
aload 2
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getCompressedSize()J
ldc2_w -1L
lcmp
ifeq L4
aload 1
aload 2
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getCompressedSize()J
invokevirtual net/lingala/zip4j/model/FileHeader/setCompressedSize(J)V
L4:
aload 2
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getOffsetLocalHeader()J
ldc2_w -1L
lcmp
ifeq L5
aload 1
aload 2
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getOffsetLocalHeader()J
invokevirtual net/lingala/zip4j/model/FileHeader/setOffsetLocalHeader(J)V
L5:
aload 2
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getDiskNumberStart()I
iconst_m1
if_icmpeq L1
aload 1
aload 2
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getDiskNumberStart()I
invokevirtual net/lingala/zip4j/model/FileHeader/setDiskNumberStart(I)V
return
.limit locals 3
.limit stack 9
.end method

.method private readAndSaveZip64ExtendedInfo(Lnet/lingala/zip4j/model/LocalFileHeader;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "file header is null in reading Zip64 Extended Info"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getExtraDataRecords()Ljava/util/ArrayList;
ifnull L1
aload 1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getExtraDataRecords()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifgt L2
L1:
return
L2:
aload 0
aload 1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getExtraDataRecords()Ljava/util/ArrayList;
aload 1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getUncompressedSize()J
aload 1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getCompressedSize()J
ldc2_w -1L
iconst_m1
invokespecial net/lingala/zip4j/core/HeaderReader/readZip64ExtendedInfo(Ljava/util/ArrayList;JJJI)Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
astore 2
aload 2
ifnull L1
aload 1
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setZip64ExtendedInfo(Lnet/lingala/zip4j/model/Zip64ExtendedInfo;)V
aload 2
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getUnCompressedSize()J
ldc2_w -1L
lcmp
ifeq L3
aload 1
aload 2
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getUnCompressedSize()J
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setUncompressedSize(J)V
L3:
aload 2
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getCompressedSize()J
ldc2_w -1L
lcmp
ifeq L1
aload 1
aload 2
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getCompressedSize()J
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setCompressedSize(J)V
return
.limit locals 3
.limit stack 9
.end method

.method private readCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
.catch java/io/IOException from L7 to L2 using L2
.catch java/io/IOException from L8 to L9 using L2
.catch java/io/IOException from L10 to L11 using L2
.catch java/io/IOException from L12 to L13 using L2
.catch java/io/IOException from L13 to L14 using L2
.catch java/io/IOException from L15 to L16 using L2
.catch java/io/IOException from L17 to L18 using L2
.catch java/io/IOException from L19 to L20 using L2
.catch java/io/IOException from L20 to L21 using L2
.catch java/io/IOException from L22 to L23 using L2
.catch java/io/IOException from L23 to L24 using L2
.catch java/io/IOException from L25 to L26 using L2
.catch java/io/IOException from L26 to L27 using L2
.catch java/io/IOException from L28 to L29 using L2
.catch java/io/IOException from L29 to L30 using L2
.catch java/io/IOException from L31 to L32 using L2
.catch java/io/IOException from L33 to L34 using L2
.catch java/io/IOException from L35 to L36 using L2
.catch java/io/IOException from L37 to L38 using L2
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
ifnonnull L39
new net/lingala/zip4j/exception/ZipException
dup
ldc "random access file was null"
iconst_3
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;I)V
athrow
L39:
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "EndCentralRecord was null, maybe a corrupt zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new net/lingala/zip4j/model/CentralDirectory
dup
invokespecial net/lingala/zip4j/model/CentralDirectory/<init>()V
astore 10
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 11
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
astore 8
aload 8
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getOffsetOfStartOfCentralDir()J
lstore 5
aload 8
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getTotNoOfEntriesInCentralDir()I
istore 1
L1:
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/isZip64Format()Z
ifeq L3
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/getOffsetStartCenDirWRTStartDiskNo()J
lstore 5
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/getTotNoOfEntriesInCentralDir()J
l2i
istore 1
L3:
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
lload 5
invokevirtual java/io/RandomAccessFile/seek(J)V
iconst_4
newarray byte
astore 12
iconst_2
newarray byte
astore 13
bipush 8
newarray byte
astore 8
L4:
iconst_0
istore 2
L40:
iload 2
iload 1
if_icmpge L33
L5:
new net/lingala/zip4j/model/FileHeader
dup
invokespecial net/lingala/zip4j/model/FileHeader/<init>()V
astore 14
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 12
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
istore 3
L6:
iload 3
i2l
ldc2_w 33639248L
lcmp
ifeq L8
L7:
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Expected central directory entry not found (#"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 8
new net/lingala/zip4j/exception/ZipException
dup
aload 8
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L8:
aload 14
iload 3
invokevirtual net/lingala/zip4j/model/FileHeader/setSignature(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 13
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 14
aload 13
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/FileHeader/setVersionMadeBy(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 13
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 14
aload 13
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/FileHeader/setVersionNeededToExtract(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 13
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 13
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
sipush 2048
iand
ifeq L41
L9:
iconst_1
istore 7
L10:
aload 14
iload 7
invokevirtual net/lingala/zip4j/model/FileHeader/setFileNameUTF8Encoded(Z)V
L11:
aload 13
iconst_0
baload
istore 3
iload 3
iconst_1
iand
ifeq L13
L12:
aload 14
iconst_1
invokevirtual net/lingala/zip4j/model/FileHeader/setEncrypted(Z)V
L13:
aload 14
aload 13
invokevirtual [B/clone()Ljava/lang/Object;
checkcast [B
checkcast [B
invokevirtual net/lingala/zip4j/model/FileHeader/setGeneralPurposeFlag([B)V
L14:
iload 3
iconst_3
ishr
iconst_1
if_icmpne L42
iconst_1
istore 7
L15:
aload 14
iload 7
invokevirtual net/lingala/zip4j/model/FileHeader/setDataDescriptorExists(Z)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 13
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 14
aload 13
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/FileHeader/setCompressionMethod(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 14
aload 12
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/FileHeader/setLastModFileTime(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 14
aload 12
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
i2l
invokevirtual net/lingala/zip4j/model/FileHeader/setCrc32(J)V
aload 14
aload 12
invokevirtual [B/clone()Ljava/lang/Object;
checkcast [B
checkcast [B
invokevirtual net/lingala/zip4j/model/FileHeader/setCrcBuff([B)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 14
aload 0
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/getLongByteFromIntByte([B)[B
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
invokevirtual net/lingala/zip4j/model/FileHeader/setCompressedSize(J)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 14
aload 0
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/getLongByteFromIntByte([B)[B
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
invokevirtual net/lingala/zip4j/model/FileHeader/setUncompressedSize(J)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 13
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 13
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
istore 3
aload 14
iload 3
invokevirtual net/lingala/zip4j/model/FileHeader/setFileNameLength(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 13
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 14
aload 13
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/FileHeader/setExtraFieldLength(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 13
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 13
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
istore 4
aload 14
new java/lang/String
dup
aload 13
invokespecial java/lang/String/<init>([B)V
invokevirtual net/lingala/zip4j/model/FileHeader/setFileComment(Ljava/lang/String;)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 13
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 14
aload 13
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/FileHeader/setDiskNumberStart(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 13
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 14
aload 13
invokevirtual [B/clone()Ljava/lang/Object;
checkcast [B
checkcast [B
invokevirtual net/lingala/zip4j/model/FileHeader/setInternalFileAttr([B)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 14
aload 12
invokevirtual [B/clone()Ljava/lang/Object;
checkcast [B
checkcast [B
invokevirtual net/lingala/zip4j/model/FileHeader/setExternalFileAttr([B)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 14
aload 0
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/getLongByteFromIntByte([B)[B
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
ldc2_w 4294967295L
land
invokevirtual net/lingala/zip4j/model/FileHeader/setOffsetLocalHeader(J)V
L16:
iload 3
ifle L31
L17:
iload 3
newarray byte
astore 8
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 8
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getFileNameCharset()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifeq L20
new java/lang/String
dup
aload 8
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getFileNameCharset()Ljava/lang/String;
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
astore 8
L18:
aload 8
ifnonnull L43
L19:
new net/lingala/zip4j/exception/ZipException
dup
ldc "fileName is null when reading central directory"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L20:
aload 8
aload 14
invokevirtual net/lingala/zip4j/model/FileHeader/isFileNameUTF8Encoded()Z
invokestatic net/lingala/zip4j/util/Zip4jUtil/decodeFileName([BZ)Ljava/lang/String;
astore 8
L21:
goto L18
L43:
aload 8
astore 9
L22:
aload 8
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "file.separator"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iflt L23
aload 8
aload 8
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "file.separator"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iconst_2
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 9
L23:
aload 14
aload 9
invokevirtual net/lingala/zip4j/model/FileHeader/setFileName(Ljava/lang/String;)V
aload 9
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L44
aload 9
ldc "\\"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L45
L24:
goto L44
L25:
aload 14
iload 7
invokevirtual net/lingala/zip4j/model/FileHeader/setDirectory(Z)V
L26:
aload 0
aload 14
invokespecial net/lingala/zip4j/core/HeaderReader/readAndSaveExtraDataRecord(Lnet/lingala/zip4j/model/FileHeader;)V
aload 0
aload 14
invokespecial net/lingala/zip4j/core/HeaderReader/readAndSaveZip64ExtendedInfo(Lnet/lingala/zip4j/model/FileHeader;)V
aload 0
aload 14
invokespecial net/lingala/zip4j/core/HeaderReader/readAndSaveAESExtraDataRecord(Lnet/lingala/zip4j/model/FileHeader;)V
L27:
iload 4
ifle L29
L28:
iload 4
newarray byte
astore 8
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 8
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 14
new java/lang/String
dup
aload 8
invokespecial java/lang/String/<init>([B)V
invokevirtual net/lingala/zip4j/model/FileHeader/setFileComment(Ljava/lang/String;)V
L29:
aload 11
aload 14
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L30:
iload 2
iconst_1
iadd
istore 2
goto L40
L31:
aload 14
aconst_null
invokevirtual net/lingala/zip4j/model/FileHeader/setFileName(Ljava/lang/String;)V
L32:
goto L26
L33:
aload 10
aload 11
invokevirtual net/lingala/zip4j/model/CentralDirectory/setFileHeaders(Ljava/util/ArrayList;)V
new net/lingala/zip4j/model/DigitalSignature
dup
invokespecial net/lingala/zip4j/model/DigitalSignature/<init>()V
astore 8
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 12
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
istore 1
L34:
iload 1
i2l
ldc2_w 84233040L
lcmp
ifeq L35
aload 10
areturn
L35:
aload 8
iload 1
invokevirtual net/lingala/zip4j/model/DigitalSignature/setHeaderSignature(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 13
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 13
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
istore 1
aload 8
iload 1
invokevirtual net/lingala/zip4j/model/DigitalSignature/setSizeOfData(I)V
L36:
iload 1
ifle L46
L37:
iload 1
newarray byte
astore 9
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 9
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 8
new java/lang/String
dup
aload 9
invokespecial java/lang/String/<init>([B)V
invokevirtual net/lingala/zip4j/model/DigitalSignature/setSignatureData(Ljava/lang/String;)V
L38:
aload 10
areturn
L41:
iconst_0
istore 7
goto L10
L42:
iconst_0
istore 7
goto L15
L44:
iconst_1
istore 7
goto L25
L45:
iconst_0
istore 7
goto L25
L46:
aload 10
areturn
.limit locals 15
.limit stack 5
.end method

.method private readEndOfCentralDirectoryRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
.catch java/io/IOException from L7 to L2 using L2
.catch java/io/IOException from L8 to L9 using L2
.catch java/io/IOException from L10 to L11 using L2
.catch java/io/IOException from L11 to L12 using L2
.catch java/io/IOException from L13 to L14 using L2
.catch java/io/IOException from L15 to L16 using L2
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "random access file was null"
iconst_3
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;I)V
athrow
L0:
iconst_4
newarray byte
astore 5
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/length()J
lstore 2
new net/lingala/zip4j/model/EndCentralDirRecord
dup
invokespecial net/lingala/zip4j/model/EndCentralDirRecord/<init>()V
astore 4
L1:
iconst_0
istore 1
lload 2
ldc2_w 22L
lsub
lstore 2
L3:
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
lload 2
invokevirtual java/io/RandomAccessFile/seek(J)V
L4:
iload 1
iconst_1
iadd
istore 1
L5:
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 5
invokestatic net/lingala/zip4j/util/Raw/readLeInt(Ljava/io/DataInput;[B)I
i2l
ldc2_w 101010256L
lcmp
ifeq L7
L6:
iload 1
sipush 3000
if_icmple L17
L7:
aload 5
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
i2l
ldc2_w 101010256L
lcmp
ifeq L8
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip headers not found. probably not a zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 4
new net/lingala/zip4j/exception/ZipException
dup
ldc "Probably not a zip file or a corrupted zip file"
aload 4
iconst_4
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;Ljava/lang/Throwable;I)V
athrow
L8:
iconst_4
newarray byte
astore 5
iconst_2
newarray byte
astore 6
aload 4
ldc2_w 101010256L
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setSignature(J)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 6
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 6
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setNoOfThisDisk(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 6
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 6
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setNoOfThisDiskStartOfCentralDir(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 6
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 6
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setTotNoOfEntriesInCentralDirOnThisDisk(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 6
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 6
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setTotNoOfEntriesInCentralDir(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 5
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 5
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setSizeOfCentralDir(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 5
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 0
aload 5
invokespecial net/lingala/zip4j/core/HeaderReader/getLongByteFromIntByte([B)[B
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setOffsetOfStartOfCentralDir(J)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 6
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 6
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
istore 1
aload 4
iload 1
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setCommentLength(I)V
L9:
iload 1
ifle L13
L10:
iload 1
newarray byte
astore 5
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 5
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
new java/lang/String
dup
aload 5
invokespecial java/lang/String/<init>([B)V
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setComment(Ljava/lang/String;)V
aload 4
aload 5
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setCommentBytes([B)V
L11:
aload 4
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getNoOfThisDisk()I
ifle L15
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
iconst_1
invokevirtual net/lingala/zip4j/model/ZipModel/setSplitArchive(Z)V
L12:
aload 4
areturn
L13:
aload 4
aconst_null
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setComment(Ljava/lang/String;)V
L14:
goto L11
L15:
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
iconst_0
invokevirtual net/lingala/zip4j/model/ZipModel/setSplitArchive(Z)V
L16:
aload 4
areturn
L17:
lload 2
lconst_1
lsub
lstore 2
goto L3
.limit locals 7
.limit stack 5
.end method

.method private readExtraDataRecords(I)Ljava/util/ArrayList;
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
.catch java/io/IOException from L7 to L8 using L2
.catch java/io/IOException from L9 to L10 using L2
.catch java/io/IOException from L11 to L12 using L2
.catch java/io/IOException from L13 to L14 using L2
.catch java/io/IOException from L15 to L16 using L2
.catch java/io/IOException from L17 to L18 using L2
iload 1
ifgt L0
aconst_null
astore 5
L19:
aload 5
areturn
L0:
iload 1
newarray byte
astore 5
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 5
invokevirtual java/io/RandomAccessFile/read([B)I
pop
L1:
iconst_0
istore 2
L3:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 6
L4:
iload 2
iload 1
if_icmpge L20
L5:
new net/lingala/zip4j/model/ExtraDataRecord
dup
invokespecial net/lingala/zip4j/model/ExtraDataRecord/<init>()V
astore 7
aload 7
aload 5
iload 2
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
i2l
invokevirtual net/lingala/zip4j/model/ExtraDataRecord/setHeader(J)V
L6:
iload 2
iconst_2
iadd
istore 4
L7:
aload 5
iload 4
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
istore 3
L8:
iload 3
istore 2
iload 3
iconst_2
iadd
iload 1
if_icmple L13
L9:
aload 5
iload 4
invokestatic net/lingala/zip4j/util/Raw/readShortBigEndian([BI)S
istore 3
L10:
iload 3
istore 2
iload 3
iconst_2
iadd
iload 1
if_icmple L13
L20:
aload 6
astore 5
L11:
aload 6
invokevirtual java/util/ArrayList/size()I
ifgt L19
L12:
aconst_null
areturn
L13:
aload 7
iload 2
invokevirtual net/lingala/zip4j/model/ExtraDataRecord/setSizeOfData(I)V
L14:
iload 4
iconst_2
iadd
istore 3
iload 2
ifle L16
L15:
iload 2
newarray byte
astore 8
aload 5
iload 3
aload 8
iconst_0
iload 2
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 7
aload 8
invokevirtual net/lingala/zip4j/model/ExtraDataRecord/setData([B)V
L16:
iload 3
iload 2
iadd
istore 2
L17:
aload 6
aload 7
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L18:
goto L4
L2:
astore 5
new net/lingala/zip4j/exception/ZipException
dup
aload 5
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 9
.limit stack 5
.end method

.method private readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L2 using L2
L0:
aload 1
aload 2
iconst_0
aload 2
arraylength
invokevirtual java/io/RandomAccessFile/read([BII)I
iconst_m1
if_icmpeq L3
L1:
aload 2
areturn
L3:
new net/lingala/zip4j/exception/ZipException
dup
ldc "unexpected end of file when reading short buff"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
ldc "IOException when reading short buff"
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 4
.end method

.method private readZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
.throws net/lingala/zip4j/exception/ZipException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid file handler when trying to read Zip64EndCentralDirLocator"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new net/lingala/zip4j/model/Zip64EndCentralDirLocator
dup
invokespecial net/lingala/zip4j/model/Zip64EndCentralDirLocator/<init>()V
astore 2
aload 0
invokespecial net/lingala/zip4j/core/HeaderReader/setFilePointerToReadZip64EndCentralDirLoc()V
iconst_4
newarray byte
astore 3
bipush 8
newarray byte
astore 4
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 3
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 3
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
istore 1
L1:
iload 1
i2l
ldc2_w 117853008L
lcmp
ifne L5
L3:
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
iconst_1
invokevirtual net/lingala/zip4j/model/ZipModel/setZip64Format(Z)V
aload 2
iload 1
i2l
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setSignature(J)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 3
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 2
aload 3
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setNoOfDiskStartOfZip64EndOfCentralDirRec(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 4
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 2
aload 4
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setOffsetZip64EndOfCentralDirRec(J)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 3
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 2
aload 3
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setTotNumberOfDiscs(I)V
L4:
aload 2
areturn
L5:
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
iconst_0
invokevirtual net/lingala/zip4j/model/ZipModel/setZip64Format(Z)V
L6:
aconst_null
areturn
L2:
astore 2
new net/lingala/zip4j/exception/ZipException
dup
aload 2
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 5
.limit stack 4
.end method

.method private readZip64EndCentralDirRec()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L2 using L2
.catch java/io/IOException from L4 to L5 using L2
.catch java/io/IOException from L6 to L7 using L2
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
ifnonnull L8
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid zip64 end of central directory locator"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L8:
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/getOffsetZip64EndOfCentralDirRec()J
lstore 2
lload 2
lconst_0
lcmp
ifge L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid offset for start of end of central directory record"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
lload 2
invokevirtual java/io/RandomAccessFile/seek(J)V
new net/lingala/zip4j/model/Zip64EndCentralDirRecord
dup
invokespecial net/lingala/zip4j/model/Zip64EndCentralDirRecord/<init>()V
astore 4
iconst_2
newarray byte
astore 5
iconst_4
newarray byte
astore 6
bipush 8
newarray byte
astore 7
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 6
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 6
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
istore 1
L1:
iload 1
i2l
ldc2_w 101075792L
lcmp
ifeq L9
L3:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid signature for zip64 end of central directory record"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 4
new net/lingala/zip4j/exception/ZipException
dup
aload 4
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L9:
iload 1
i2l
lstore 2
L4:
aload 4
lload 2
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setSignature(J)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 7
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 7
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setSizeOfZip64EndCentralDirRec(J)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 5
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 5
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setVersionMadeBy(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 5
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 5
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setVersionNeededToExtract(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 6
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 6
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setNoOfThisDisk(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 6
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 6
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setNoOfThisDiskStartOfCentralDir(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 7
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 7
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setTotNoOfEntriesInCentralDirOnThisDisk(J)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 7
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 7
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setTotNoOfEntriesInCentralDir(J)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 7
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 7
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setSizeOfCentralDir(J)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 7
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 7
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setOffsetStartCenDirWRTStartDiskNo(J)V
aload 4
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/getSizeOfZip64EndCentralDirRec()J
ldc2_w 44L
lsub
lstore 2
L5:
lload 2
lconst_0
lcmp
ifle L7
L6:
lload 2
l2i
newarray byte
astore 5
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 5
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 4
aload 5
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setExtensibleDataSector([B)V
L7:
aload 4
areturn
.limit locals 8
.limit stack 4
.end method

.method private readZip64ExtendedInfo(Ljava/util/ArrayList;JJJI)Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
.throws net/lingala/zip4j/exception/ZipException
iconst_0
istore 9
L0:
iload 9
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpge L1
aload 1
iload 9
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/ExtraDataRecord
astore 13
aload 13
ifnonnull L2
L3:
iload 9
iconst_1
iadd
istore 9
goto L0
L2:
aload 13
invokevirtual net/lingala/zip4j/model/ExtraDataRecord/getHeader()J
lconst_1
lcmp
ifne L3
new net/lingala/zip4j/model/Zip64ExtendedInfo
dup
invokespecial net/lingala/zip4j/model/Zip64ExtendedInfo/<init>()V
astore 1
aload 13
invokevirtual net/lingala/zip4j/model/ExtraDataRecord/getData()[B
astore 14
aload 13
invokevirtual net/lingala/zip4j/model/ExtraDataRecord/getSizeOfData()I
ifgt L4
L1:
aconst_null
areturn
L4:
bipush 8
newarray byte
astore 15
iconst_4
newarray byte
astore 16
iconst_0
istore 10
iconst_0
istore 12
iload 10
istore 11
iload 12
istore 9
ldc2_w 65535L
lload 2
land
ldc2_w 65535L
lcmp
ifne L5
iload 10
istore 11
iload 12
istore 9
aload 13
invokevirtual net/lingala/zip4j/model/ExtraDataRecord/getSizeOfData()I
ifge L5
aload 14
iconst_0
aload 15
iconst_0
bipush 8
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
aload 15
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/setUnCompressedSize(J)V
iconst_0
bipush 8
iadd
istore 11
iconst_1
istore 9
L5:
iload 11
istore 12
iload 9
istore 10
ldc2_w 65535L
lload 4
land
ldc2_w 65535L
lcmp
ifne L6
iload 11
istore 12
iload 9
istore 10
iload 11
aload 13
invokevirtual net/lingala/zip4j/model/ExtraDataRecord/getSizeOfData()I
if_icmpge L6
aload 14
iload 11
aload 15
iconst_0
bipush 8
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
aload 15
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/setCompressedSize(J)V
iload 11
bipush 8
iadd
istore 12
iconst_1
istore 10
L6:
iload 12
istore 11
iload 10
istore 9
ldc2_w 65535L
lload 6
land
ldc2_w 65535L
lcmp
ifne L7
iload 12
istore 11
iload 10
istore 9
iload 12
aload 13
invokevirtual net/lingala/zip4j/model/ExtraDataRecord/getSizeOfData()I
if_icmpge L7
aload 14
iload 12
aload 15
iconst_0
bipush 8
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
aload 15
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/setOffsetLocalHeader(J)V
iload 12
bipush 8
iadd
istore 11
iconst_1
istore 9
L7:
iload 9
istore 10
ldc_w 65535
iload 8
iand
ldc_w 65535
if_icmpne L8
iload 9
istore 10
iload 11
aload 13
invokevirtual net/lingala/zip4j/model/ExtraDataRecord/getSizeOfData()I
if_icmpge L8
aload 14
iload 11
aload 16
iconst_0
iconst_4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
aload 16
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/setDiskNumberStart(I)V
iconst_1
istore 10
L8:
iload 10
ifeq L1
aload 1
areturn
.limit locals 17
.limit stack 5
.end method

.method private setFilePointerToReadZip64EndCentralDirLoc()V
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L2
L0:
iconst_4
newarray byte
astore 3
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/length()J
ldc2_w 22L
lsub
lstore 1
L1:
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
lload 1
invokevirtual java/io/RandomAccessFile/seek(J)V
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 3
invokestatic net/lingala/zip4j/util/Raw/readLeInt(Ljava/io/DataInput;[B)I
i2l
ldc2_w 101010256L
lcmp
ifne L4
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
invokevirtual java/io/RandomAccessFile/getFilePointer()J
ldc2_w 4L
lsub
ldc2_w 4L
lsub
ldc2_w 8L
lsub
ldc2_w 4L
lsub
ldc2_w 4L
lsub
invokevirtual java/io/RandomAccessFile/seek(J)V
L3:
return
L2:
astore 3
new net/lingala/zip4j/exception/ZipException
dup
aload 3
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L4:
lload 1
lconst_1
lsub
lstore 1
goto L1
.limit locals 4
.limit stack 5
.end method

.method public readAllHeaders()Lnet/lingala/zip4j/model/ZipModel;
.throws net/lingala/zip4j/exception/ZipException
aload 0
aconst_null
invokevirtual net/lingala/zip4j/core/HeaderReader/readAllHeaders(Ljava/lang/String;)Lnet/lingala/zip4j/model/ZipModel;
areturn
.limit locals 1
.limit stack 2
.end method

.method public readAllHeaders(Ljava/lang/String;)Lnet/lingala/zip4j/model/ZipModel;
.throws net/lingala/zip4j/exception/ZipException
aload 0
new net/lingala/zip4j/model/ZipModel
dup
invokespecial net/lingala/zip4j/model/ZipModel/<init>()V
putfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/setFileNameCharset(Ljava/lang/String;)V
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
invokespecial net/lingala/zip4j/core/HeaderReader/readEndOfCentralDirectoryRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/ZipModel/setEndCentralDirRecord(Lnet/lingala/zip4j/model/EndCentralDirRecord;)V
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
invokespecial net/lingala/zip4j/core/HeaderReader/readZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
invokevirtual net/lingala/zip4j/model/ZipModel/setZip64EndCentralDirLocator(Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;)V
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/isZip64Format()Z
ifeq L0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
invokespecial net/lingala/zip4j/core/HeaderReader/readZip64EndCentralDirRec()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/ZipModel/setZip64EndCentralDirRecord(Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;)V
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
ifnull L1
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/getNoOfThisDisk()I
ifle L1
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
iconst_1
invokevirtual net/lingala/zip4j/model/ZipModel/setSplitArchive(Z)V
L0:
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
aload 0
invokespecial net/lingala/zip4j/core/HeaderReader/readCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/ZipModel/setCentralDirectory(Lnet/lingala/zip4j/model/CentralDirectory;)V
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
areturn
L1:
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zipModel Lnet/lingala/zip4j/model/ZipModel;
iconst_0
invokevirtual net/lingala/zip4j/model/ZipModel/setSplitArchive(Z)V
goto L0
.limit locals 2
.limit stack 3
.end method

.method public readLocalFileHeader(Lnet/lingala/zip4j/model/FileHeader;)Lnet/lingala/zip4j/model/LocalFileHeader;
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L2 using L2
.catch java/io/IOException from L4 to L5 using L2
.catch java/io/IOException from L6 to L7 using L2
.catch java/io/IOException from L8 to L9 using L2
.catch java/io/IOException from L9 to L10 using L2
.catch java/io/IOException from L11 to L12 using L2
.catch java/io/IOException from L12 to L13 using L2
.catch java/io/IOException from L14 to L15 using L2
.catch java/io/IOException from L16 to L17 using L2
.catch java/io/IOException from L18 to L19 using L2
.catch java/io/IOException from L19 to L20 using L2
.catch java/io/IOException from L21 to L22 using L2
.catch java/io/IOException from L22 to L23 using L2
.catch java/io/IOException from L23 to L24 using L2
.catch java/io/IOException from L24 to L25 using L2
.catch java/io/IOException from L26 to L27 using L2
.catch java/io/IOException from L28 to L29 using L2
.catch java/io/IOException from L30 to L31 using L2
aload 1
ifnull L32
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
ifnonnull L33
L32:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid read parameters for local header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L33:
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getOffsetLocalHeader()J
lstore 8
lload 8
lstore 6
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
ifnull L34
lload 8
lstore 6
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getOffsetLocalHeader()J
lconst_0
lcmp
ifle L34
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getOffsetLocalHeader()J
lstore 6
L34:
lload 6
lconst_0
lcmp
ifge L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid local header offset"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
lload 6
invokevirtual java/io/RandomAccessFile/seek(J)V
new net/lingala/zip4j/model/LocalFileHeader
dup
invokespecial net/lingala/zip4j/model/LocalFileHeader/<init>()V
astore 13
iconst_2
newarray byte
astore 11
iconst_4
newarray byte
astore 12
bipush 8
newarray byte
astore 14
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 12
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
istore 2
L1:
iload 2
i2l
ldc2_w 67324752L
lcmp
ifeq L4
L3:
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "invalid local header signature for file: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L4:
aload 13
iload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setSignature(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 11
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 13
aload 11
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setVersionNeededToExtract(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 11
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 11
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
sipush 2048
iand
ifeq L35
L5:
iconst_1
istore 10
L6:
aload 13
iload 10
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setFileNameUTF8Encoded(Z)V
L7:
aload 11
iconst_0
baload
istore 3
iload 3
iconst_1
iand
ifeq L9
L8:
aload 13
iconst_1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setEncrypted(Z)V
L9:
aload 13
aload 11
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setGeneralPurposeFlag([B)V
iload 3
invokestatic java/lang/Integer/toBinaryString(I)Ljava/lang/String;
astore 14
aload 14
invokevirtual java/lang/String/length()I
iconst_4
if_icmplt L12
aload 14
iconst_3
invokevirtual java/lang/String/charAt(I)C
bipush 49
if_icmpne L36
L10:
iconst_1
istore 10
L11:
aload 13
iload 10
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setDataDescriptorExists(Z)V
L12:
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 11
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 13
aload 11
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setCompressionMethod(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 13
aload 12
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setLastModFileTime(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 13
aload 12
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
i2l
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setCrc32(J)V
aload 13
aload 12
invokevirtual [B/clone()Ljava/lang/Object;
checkcast [B
checkcast [B
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setCrcBuff([B)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 13
aload 0
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/getLongByteFromIntByte([B)[B
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setCompressedSize(J)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 13
aload 0
aload 12
invokespecial net/lingala/zip4j/core/HeaderReader/getLongByteFromIntByte([B)[B
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readLongLittleEndian([BI)J
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setUncompressedSize(J)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 11
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 11
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
istore 5
aload 13
iload 5
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setFileNameLength(I)V
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 11
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 11
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readShortLittleEndian([BI)I
istore 4
aload 13
iload 4
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setExtraFieldLength(I)V
L13:
iconst_0
iconst_4
iadd
iconst_2
iadd
iconst_2
iadd
iconst_2
iadd
iconst_4
iadd
iconst_4
iadd
iconst_4
iadd
iconst_4
iadd
iconst_2
iadd
iconst_2
iadd
istore 2
iload 5
ifle L26
L14:
iload 5
newarray byte
astore 11
aload 0
aload 0
getfield net/lingala/zip4j/core/HeaderReader/zip4jRaf Ljava/io/RandomAccessFile;
aload 11
invokespecial net/lingala/zip4j/core/HeaderReader/readIntoBuff(Ljava/io/RandomAccessFile;[B)[B
pop
aload 11
aload 13
invokevirtual net/lingala/zip4j/model/LocalFileHeader/isFileNameUTF8Encoded()Z
invokestatic net/lingala/zip4j/util/Zip4jUtil/decodeFileName([BZ)Ljava/lang/String;
astore 12
L15:
aload 12
ifnonnull L17
L16:
new net/lingala/zip4j/exception/ZipException
dup
ldc "file name is null, cannot assign file name to local file header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L17:
aload 12
astore 11
L18:
aload 12
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "file.separator"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iflt L19
aload 12
aload 12
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "file.separator"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iconst_2
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 11
L19:
aload 13
aload 11
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setFileName(Ljava/lang/String;)V
L20:
iload 5
bipush 30
iadd
istore 2
L21:
aload 0
aload 13
invokespecial net/lingala/zip4j/core/HeaderReader/readAndSaveExtraDataRecord(Lnet/lingala/zip4j/model/LocalFileHeader;)V
aload 13
iload 2
iload 4
iadd
i2l
lload 6
ladd
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setOffsetStartOfData(J)V
aload 13
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getPassword()[C
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setPassword([C)V
aload 0
aload 13
invokespecial net/lingala/zip4j/core/HeaderReader/readAndSaveZip64ExtendedInfo(Lnet/lingala/zip4j/model/LocalFileHeader;)V
aload 0
aload 13
invokespecial net/lingala/zip4j/core/HeaderReader/readAndSaveAESExtraDataRecord(Lnet/lingala/zip4j/model/LocalFileHeader;)V
aload 13
invokevirtual net/lingala/zip4j/model/LocalFileHeader/isEncrypted()Z
ifeq L22
aload 13
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getEncryptionMethod()I
bipush 99
if_icmpne L37
L22:
aload 13
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getCrc32()J
lconst_0
lcmp
ifgt L23
aload 13
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getCrc32()J
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setCrc32(J)V
aload 13
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getCrcBuff()[B
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setCrcBuff([B)V
L23:
aload 13
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getCompressedSize()J
lconst_0
lcmp
ifgt L24
aload 13
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getCompressedSize()J
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setCompressedSize(J)V
L24:
aload 13
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getUncompressedSize()J
lconst_0
lcmp
ifgt L38
aload 13
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getUncompressedSize()J
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setUncompressedSize(J)V
L25:
aload 13
areturn
L26:
aload 13
aconst_null
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setFileName(Ljava/lang/String;)V
L27:
goto L21
L37:
iload 3
bipush 64
iand
bipush 64
if_icmpne L30
L28:
aload 13
iconst_1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setEncryptionMethod(I)V
L29:
goto L22
L30:
aload 13
iconst_0
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setEncryptionMethod(I)V
L31:
goto L22
L35:
iconst_0
istore 10
goto L6
L36:
iconst_0
istore 10
goto L11
L38:
aload 13
areturn
.limit locals 15
.limit stack 5
.end method
