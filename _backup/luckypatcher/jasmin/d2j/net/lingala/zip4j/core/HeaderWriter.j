.bytecode 50.0
.class public synchronized net/lingala/zip4j/core/HeaderWriter
.super java/lang/Object

.field private final 'ZIP64_EXTRA_BUF' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
bipush 50
putfield net/lingala/zip4j/core/HeaderWriter/ZIP64_EXTRA_BUF I
return
.limit locals 1
.limit stack 2
.end method

.method private byteArrayListToByteArray(Ljava/util/List;)[B
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input byte array list is null, cannot conver to byte array"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokeinterface java/util/List/size()I 0
ifgt L1
aconst_null
astore 3
L2:
aload 3
areturn
L1:
aload 1
invokeinterface java/util/List/size()I 0
newarray byte
astore 4
iconst_0
istore 2
L3:
aload 4
astore 3
iload 2
aload 1
invokeinterface java/util/List/size()I 0
if_icmpge L2
aload 4
iload 2
aload 1
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/lang/String
invokestatic java/lang/Byte/parseByte(Ljava/lang/String;)B
bastore
iload 2
iconst_1
iadd
istore 2
goto L3
.limit locals 5
.limit stack 4
.end method

.method private copyByteArrayToArrayList([BLjava/util/List;)V
.throws net/lingala/zip4j/exception/ZipException
aload 2
ifnull L0
aload 1
ifnonnull L1
L0:
new net/lingala/zip4j/exception/ZipException
dup
ldc "one of the input parameters is null, cannot copy byte array to array list"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
iconst_0
istore 3
L2:
iload 3
aload 1
arraylength
if_icmpge L3
aload 2
aload 1
iload 3
baload
invokestatic java/lang/Byte/toString(B)Ljava/lang/String;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
return
.limit locals 4
.limit stack 3
.end method

.method private countNumberOfFileHeaderEntriesOnDisk(Ljava/util/ArrayList;I)I
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "file headers are null, cannot calculate number of entries on this disk"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
iconst_0
istore 4
iconst_0
istore 3
L1:
iload 3
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpge L2
iload 4
istore 5
aload 1
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
invokevirtual net/lingala/zip4j/model/FileHeader/getDiskNumberStart()I
iload 2
if_icmpne L3
iload 4
iconst_1
iadd
istore 5
L3:
iload 3
iconst_1
iadd
istore 3
iload 5
istore 4
goto L1
L2:
iload 4
ireturn
.limit locals 6
.limit stack 3
.end method

.method private processHeaderData(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;)V
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L4 to L5 using L2
.catch java/io/IOException from L5 to L6 using L2
iconst_0
istore 3
L0:
aload 2
instanceof net/lingala/zip4j/io/SplitOutputStream
ifeq L1
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
aload 2
checkcast net/lingala/zip4j/io/SplitOutputStream
invokevirtual net/lingala/zip4j/io/SplitOutputStream/getFilePointer()J
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setOffsetOfStartOfCentralDir(J)V
aload 2
checkcast net/lingala/zip4j/io/SplitOutputStream
invokevirtual net/lingala/zip4j/io/SplitOutputStream/getCurrSplitFileCounter()I
istore 3
L1:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/isZip64Format()Z
ifeq L5
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
ifnonnull L3
aload 1
new net/lingala/zip4j/model/Zip64EndCentralDirRecord
dup
invokespecial net/lingala/zip4j/model/Zip64EndCentralDirRecord/<init>()V
invokevirtual net/lingala/zip4j/model/ZipModel/setZip64EndCentralDirRecord(Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;)V
L3:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
ifnonnull L4
aload 1
new net/lingala/zip4j/model/Zip64EndCentralDirLocator
dup
invokespecial net/lingala/zip4j/model/Zip64EndCentralDirLocator/<init>()V
invokevirtual net/lingala/zip4j/model/ZipModel/setZip64EndCentralDirLocator(Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;)V
L4:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
iload 3
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setNoOfDiskStartOfZip64EndOfCentralDirRec(I)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
iload 3
iconst_1
iadd
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setTotNumberOfDiscs(I)V
L5:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
iload 3
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setNoOfThisDisk(I)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
iload 3
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setNoOfThisDiskStartOfCentralDir(I)V
L6:
return
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 4
.limit stack 3
.end method

.method private updateCompressedSizeInLocalFileHeader(Lnet/lingala/zip4j/io/SplitOutputStream;Lnet/lingala/zip4j/model/LocalFileHeader;JJ[BZ)V
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L1
.catch java/io/IOException from L2 to L3 using L1
.catch java/io/IOException from L4 to L5 using L1
.catch java/io/IOException from L6 to L7 using L1
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid output stream, cannot update compressed size for local file header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/isWriteComprSizeInZip64ExtraRecord()Z
ifeq L6
aload 7
arraylength
bipush 8
if_icmpeq L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "attempting to write a non 8-byte compressed size block for a zip64 file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L2:
lload 3
lload 5
ladd
ldc2_w 4L
ladd
ldc2_w 4L
ladd
ldc2_w 2L
ladd
ldc2_w 2L
ladd
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getFileNameLength()I
i2l
ladd
ldc2_w 2L
ladd
ldc2_w 2L
ladd
ldc2_w 8L
ladd
lstore 9
L3:
lload 9
lstore 3
lload 5
ldc2_w 22L
lcmp
ifne L4
lload 9
ldc2_w 8L
ladd
lstore 3
L4:
aload 1
lload 3
invokevirtual net/lingala/zip4j/io/SplitOutputStream/seek(J)V
aload 1
aload 7
invokevirtual net/lingala/zip4j/io/SplitOutputStream/write([B)V
L5:
return
L6:
aload 1
lload 3
lload 5
ladd
invokevirtual net/lingala/zip4j/io/SplitOutputStream/seek(J)V
aload 1
aload 7
invokevirtual net/lingala/zip4j/io/SplitOutputStream/write([B)V
L7:
return
.limit locals 11
.limit stack 5
.end method

.method private writeCentralDirectory(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;Ljava/util/List;)I
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnull L0
aload 2
ifnonnull L1
L0:
new net/lingala/zip4j/exception/ZipException
dup
ldc "input parameters is null, cannot write central directory"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnull L2
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
ifnull L2
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifgt L3
L2:
iconst_0
istore 6
L4:
iload 6
ireturn
L3:
iconst_0
istore 4
iconst_0
istore 5
L5:
iload 4
istore 6
iload 5
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L4
iload 4
aload 0
aload 1
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
aload 2
aload 3
invokespecial net/lingala/zip4j/core/HeaderWriter/writeFileHeader(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;Ljava/io/OutputStream;Ljava/util/List;)I
iadd
istore 4
iload 5
iconst_1
iadd
istore 5
goto L5
.limit locals 7
.limit stack 6
.end method

.method private writeEndOfCentralDirectoryRecord(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;IJLjava/util/List;)V
.throws net/lingala/zip4j/exception/ZipException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L2 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch java/lang/Exception from L8 to L9 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L11 to L12 using L2
.catch java/lang/Exception from L13 to L14 using L2
aload 1
ifnull L15
aload 2
ifnonnull L0
L15:
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip model or output stream is null, cannot write end of central directory record"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
iconst_2
newarray byte
astore 2
iconst_4
newarray byte
astore 9
bipush 8
newarray byte
astore 10
aload 9
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getSignature()J
l2i
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 9
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 2
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getNoOfThisDisk()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 2
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 2
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getNoOfThisDiskStartOfCentralDir()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 2
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnull L1
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
ifnonnull L3
L1:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid central directory/file headers, cannot write end of central directory record"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 8
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifeq L16
aload 0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getNoOfThisDisk()I
invokespecial net/lingala/zip4j/core/HeaderWriter/countNumberOfFileHeaderEntriesOnDisk(Ljava/util/ArrayList;I)I
istore 7
L4:
aload 2
iconst_0
iload 7
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 2
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 2
iconst_0
iload 8
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 2
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 9
iconst_0
iload 3
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 9
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L5:
lload 4
ldc2_w 4294967295L
lcmp
ifle L13
L6:
aload 10
iconst_0
ldc2_w 4294967295L
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 10
iconst_0
aload 9
iconst_0
iconst_4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 9
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L7:
iconst_0
istore 3
L8:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getComment()Ljava/lang/String;
ifnull L9
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getCommentLength()I
istore 3
L9:
aload 2
iconst_0
iload 3
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 2
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L10:
iload 3
ifle L17
L11:
aload 0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getCommentBytes()[B
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L12:
return
L13:
aload 10
iconst_0
lload 4
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 10
iconst_0
aload 9
iconst_0
iconst_4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 9
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L14:
goto L7
L17:
return
L16:
iload 8
istore 7
goto L4
.limit locals 11
.limit stack 5
.end method

.method private writeFileHeader(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;Ljava/io/OutputStream;Ljava/util/List;)I
.throws net/lingala/zip4j/exception/ZipException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch java/lang/Exception from L8 to L9 using L2
.catch java/lang/Exception from L10 to L11 using L2
.catch java/lang/Exception from L12 to L13 using L2
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L15 to L16 using L2
.catch java/lang/Exception from L17 to L18 using L2
.catch java/lang/Exception from L19 to L20 using L2
.catch java/lang/Exception from L21 to L22 using L2
.catch java/lang/Exception from L23 to L24 using L2
.catch java/lang/Exception from L25 to L26 using L2
.catch java/lang/Exception from L27 to L28 using L2
.catch java/lang/Exception from L29 to L30 using L2
.catch java/lang/Exception from L31 to L32 using L2
aload 2
ifnull L33
aload 3
ifnonnull L0
L33:
new net/lingala/zip4j/exception/ZipException
dup
ldc "input parameters is null, cannot write local file header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
iconst_2
newarray byte
astore 3
iconst_4
newarray byte
astore 12
bipush 8
newarray byte
astore 10
iconst_2
newarray byte
astore 11
aload 11
dup
iconst_0
ldc_w 0
bastore
dup
iconst_1
ldc_w 0
bastore
pop
L1:
iconst_0
istore 7
iconst_0
istore 8
L3:
aload 12
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getSignature()I
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 12
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 3
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getVersionMadeBy()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 3
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 3
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getVersionNeededToExtract()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 3
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getGeneralPurposeFlag()[B
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 3
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getCompressionMethod()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 3
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 12
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getLastModFileTime()I
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 12
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 12
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getCrc32()J
l2i
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 12
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L4:
iconst_0
iconst_4
iadd
iconst_2
iadd
iconst_2
iadd
iconst_2
iadd
iconst_2
iadd
iconst_4
iadd
iconst_4
iadd
istore 5
L5:
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getCompressedSize()J
ldc2_w 4294967295L
lcmp
ifge L6
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getUncompressedSize()J
ldc2_w 50L
ladd
ldc2_w 4294967295L
lcmp
iflt L25
L6:
aload 10
iconst_0
ldc2_w 4294967295L
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 10
iconst_0
aload 12
iconst_0
iconst_4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 12
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 0
aload 12
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L7:
iload 5
iconst_4
iadd
iconst_4
iadd
istore 5
iconst_1
istore 7
L8:
aload 3
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getFileNameLength()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 3
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
iconst_4
newarray byte
astore 12
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getOffsetLocalHeader()J
ldc2_w 4294967295L
lcmp
ifle L27
aload 10
iconst_0
ldc2_w 4294967295L
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 10
iconst_0
aload 12
iconst_0
iconst_4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L9:
iconst_1
istore 8
goto L34
L35:
iload 5
istore 6
L10:
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getAesExtraDataRecord()Lnet/lingala/zip4j/model/AESExtraDataRecord;
ifnull L12
L11:
iload 5
bipush 11
iadd
istore 6
L12:
aload 3
iconst_0
iload 6
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 3
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 0
aload 11
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 3
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getDiskNumberStart()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 3
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 0
aload 11
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getExternalFileAttr()[B
ifnull L29
aload 0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getExternalFileAttr()[B
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L13:
aload 0
aload 12
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getFileNameCharset()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifeq L31
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getFileNameCharset()Ljava/lang/String;
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
astore 11
aload 0
aload 11
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 11
arraylength
bipush 46
iadd
istore 5
L14:
goto L36
L15:
aload 1
iconst_1
invokevirtual net/lingala/zip4j/model/ZipModel/setZip64Format(Z)V
aload 3
iconst_0
iconst_1
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 3
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L16:
iconst_0
istore 6
iload 7
ifeq L37
iconst_0
bipush 16
iadd
istore 6
goto L37
L17:
aload 3
iconst_0
iload 9
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 3
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L18:
iload 5
iconst_2
iadd
iconst_2
iadd
istore 6
iload 6
istore 5
iload 7
ifeq L38
L19:
aload 10
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getUncompressedSize()J
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 0
aload 10
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 10
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getCompressedSize()J
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 0
aload 10
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L20:
iload 6
bipush 8
iadd
bipush 8
iadd
istore 5
L38:
iload 5
istore 6
iload 8
ifeq L23
L21:
aload 10
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getOffsetLocalHeader()J
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 0
aload 10
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L22:
iload 5
bipush 8
iadd
istore 6
L23:
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getAesExtraDataRecord()Lnet/lingala/zip4j/model/AESExtraDataRecord;
ifnull L39
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getAesExtraDataRecord()Lnet/lingala/zip4j/model/AESExtraDataRecord;
astore 1
aload 3
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getSignature()J
l2i
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 3
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 3
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getDataSize()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 3
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 3
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getVersionNumber()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 3
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 0
aload 1
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getVendorID()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 0
iconst_1
newarray byte
dup
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getAesStrength()I
i2b
bastore
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 3
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getCompressionMethod()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 3
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L24:
iload 6
bipush 11
iadd
ireturn
L25:
aload 10
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getCompressedSize()J
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 10
iconst_0
aload 12
iconst_0
iconst_4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 12
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 10
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getUncompressedSize()J
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 10
iconst_0
aload 12
iconst_0
iconst_4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 12
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L26:
iload 5
iconst_4
iadd
iconst_4
iadd
istore 5
goto L8
L27:
aload 10
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getOffsetLocalHeader()J
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 10
iconst_0
aload 12
iconst_0
iconst_4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L28:
goto L34
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L29:
aload 0
iconst_4
newarray byte
dup
iconst_0
ldc_w 0
bastore
dup
iconst_1
ldc_w 0
bastore
dup
iconst_2
ldc_w 0
bastore
dup
iconst_3
ldc_w 0
bastore
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L30:
goto L13
L31:
aload 0
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/convertCharset(Ljava/lang/String;)[B
aload 4
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/getEncodedStringLength(Ljava/lang/String;)I
istore 5
L32:
iload 5
bipush 46
iadd
istore 5
goto L36
L34:
iconst_0
istore 5
iload 7
ifne L40
iload 8
ifeq L35
L40:
iconst_0
iconst_4
iadd
istore 5
iload 5
istore 6
iload 7
ifeq L41
iload 5
bipush 16
iadd
istore 6
L41:
iload 6
istore 5
iload 8
ifeq L35
iload 6
bipush 8
iadd
istore 5
goto L35
L36:
iload 7
ifne L15
iload 5
istore 6
iload 8
ifeq L23
goto L15
L37:
iload 6
istore 9
iload 8
ifeq L17
iload 6
bipush 8
iadd
istore 9
goto L17
L39:
iload 6
ireturn
.limit locals 13
.limit stack 5
.end method

.method private writeZip64EndOfCentralDirectoryLocator(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;Ljava/util/List;)V
.throws net/lingala/zip4j/exception/ZipException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
aload 1
ifnull L4
aload 2
ifnonnull L0
L4:
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip model or output stream is null, cannot write zip64 end of central directory locator"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
iconst_4
newarray byte
astore 2
bipush 8
newarray byte
astore 4
aload 2
iconst_0
ldc_w 117853008
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 2
aload 3
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 2
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/getNoOfDiskStartOfZip64EndOfCentralDirRec()I
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 2
aload 3
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 4
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/getOffsetZip64EndOfCentralDirRec()J
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 0
aload 4
aload 3
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 2
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/getTotNumberOfDiscs()I
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 2
aload 3
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L1:
return
L2:
astore 1
aload 1
athrow
L3:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 5
.limit stack 4
.end method

.method private writeZip64EndOfCentralDirectoryRecord(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;IJLjava/util/List;)V
.throws net/lingala/zip4j/exception/ZipException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch net/lingala/zip4j/exception/ZipException from L1 to L4 using L2
.catch java/lang/Exception from L1 to L4 using L3
.catch net/lingala/zip4j/exception/ZipException from L5 to L6 using L2
.catch java/lang/Exception from L5 to L6 using L3
.catch net/lingala/zip4j/exception/ZipException from L6 to L2 using L2
.catch java/lang/Exception from L6 to L2 using L3
.catch net/lingala/zip4j/exception/ZipException from L7 to L8 using L2
.catch java/lang/Exception from L7 to L8 using L3
.catch net/lingala/zip4j/exception/ZipException from L9 to L10 using L2
.catch java/lang/Exception from L9 to L10 using L3
.catch net/lingala/zip4j/exception/ZipException from L10 to L11 using L2
.catch java/lang/Exception from L10 to L11 using L3
aload 1
ifnull L12
aload 2
ifnonnull L0
L12:
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip model or output stream is null, cannot write zip64 end of central directory record"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
iconst_2
newarray byte
astore 2
iconst_2
newarray byte
astore 9
aload 9
dup
iconst_0
ldc_w 0
bastore
dup
iconst_1
ldc_w 0
bastore
pop
iconst_4
newarray byte
astore 10
bipush 8
newarray byte
astore 11
aload 10
iconst_0
ldc_w 101075792
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 10
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 11
iconst_0
ldc2_w 44L
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 0
aload 11
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnull L7
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
ifnull L7
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L7
aload 2
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
invokevirtual net/lingala/zip4j/model/FileHeader/getVersionMadeBy()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 2
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 2
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
invokevirtual net/lingala/zip4j/model/FileHeader/getVersionNeededToExtract()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 2
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L1:
aload 10
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getNoOfThisDisk()I
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 10
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 10
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getNoOfThisDiskStartOfCentralDir()I
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 10
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L4:
iconst_0
istore 7
L5:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnull L6
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
ifnonnull L9
L6:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid central directory/file headers, cannot write end of central directory record"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
aload 1
athrow
L7:
aload 0
aload 9
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 0
aload 9
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L8:
goto L1
L3:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L9:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 8
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifeq L13
aload 0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getNoOfThisDisk()I
invokespecial net/lingala/zip4j/core/HeaderWriter/countNumberOfFileHeaderEntriesOnDisk(Ljava/util/ArrayList;I)I
pop
L10:
aload 11
iconst_0
iload 7
i2l
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 0
aload 11
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 11
iconst_0
iload 8
i2l
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 0
aload 11
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 11
iconst_0
iload 3
i2l
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 0
aload 11
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 11
iconst_0
lload 4
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 0
aload 11
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L11:
return
L13:
iload 8
istore 7
goto L10
.limit locals 12
.limit stack 4
.end method

.method private writeZipHeaderBytes(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;[B)V
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
aload 3
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid buff to write as zip headers"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
instanceof net/lingala/zip4j/io/SplitOutputStream
ifeq L3
aload 2
checkcast net/lingala/zip4j/io/SplitOutputStream
aload 3
arraylength
invokevirtual net/lingala/zip4j/io/SplitOutputStream/checkBuffSizeAndStartNextSplitFile(I)Z
ifeq L3
aload 0
aload 1
aload 2
invokevirtual net/lingala/zip4j/core/HeaderWriter/finalizeZipFile(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;)V
L1:
return
L3:
aload 2
aload 3
invokevirtual java/io/OutputStream/write([B)V
L4:
return
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 4
.limit stack 3
.end method

.method public finalizeZipFile(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;)V
.throws net/lingala/zip4j/exception/ZipException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch net/lingala/zip4j/exception/ZipException from L1 to L4 using L2
.catch java/lang/Exception from L1 to L4 using L3
.catch net/lingala/zip4j/exception/ZipException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
.catch net/lingala/zip4j/exception/ZipException from L5 to L6 using L2
.catch java/lang/Exception from L5 to L6 using L3
.catch net/lingala/zip4j/exception/ZipException from L6 to L7 using L2
.catch java/lang/Exception from L6 to L7 using L3
.catch net/lingala/zip4j/exception/ZipException from L8 to L9 using L2
.catch java/lang/Exception from L8 to L9 using L3
aload 1
ifnull L10
aload 2
ifnonnull L0
L10:
new net/lingala/zip4j/exception/ZipException
dup
ldc "input parameters is null, cannot finalize zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
aload 2
invokespecial net/lingala/zip4j/core/HeaderWriter/processHeaderData(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getOffsetOfStartOfCentralDir()J
lstore 4
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 6
aload 0
aload 1
aload 2
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/writeCentralDirectory(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;Ljava/util/List;)I
istore 3
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/isZip64Format()Z
ifeq L6
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
ifnonnull L1
aload 1
new net/lingala/zip4j/model/Zip64EndCentralDirRecord
dup
invokespecial net/lingala/zip4j/model/Zip64EndCentralDirRecord/<init>()V
invokevirtual net/lingala/zip4j/model/ZipModel/setZip64EndCentralDirRecord(Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;)V
L1:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
ifnonnull L4
aload 1
new net/lingala/zip4j/model/Zip64EndCentralDirLocator
dup
invokespecial net/lingala/zip4j/model/Zip64EndCentralDirLocator/<init>()V
invokevirtual net/lingala/zip4j/model/ZipModel/setZip64EndCentralDirLocator(Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;)V
L4:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
iload 3
i2l
lload 4
ladd
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setOffsetZip64EndOfCentralDirRec(J)V
aload 2
instanceof net/lingala/zip4j/io/SplitOutputStream
ifeq L8
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
aload 2
checkcast net/lingala/zip4j/io/SplitOutputStream
invokevirtual net/lingala/zip4j/io/SplitOutputStream/getCurrSplitFileCounter()I
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setNoOfDiskStartOfZip64EndOfCentralDirRec(I)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
aload 2
checkcast net/lingala/zip4j/io/SplitOutputStream
invokevirtual net/lingala/zip4j/io/SplitOutputStream/getCurrSplitFileCounter()I
iconst_1
iadd
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setTotNumberOfDiscs(I)V
L5:
aload 0
aload 1
aload 2
iload 3
lload 4
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/writeZip64EndOfCentralDirectoryRecord(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;IJLjava/util/List;)V
aload 0
aload 1
aload 2
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/writeZip64EndOfCentralDirectoryLocator(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;Ljava/util/List;)V
L6:
aload 0
aload 1
aload 2
iload 3
lload 4
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/writeEndOfCentralDirectoryRecord(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;IJLjava/util/List;)V
aload 0
aload 1
aload 2
aload 0
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/byteArrayListToByteArray(Ljava/util/List;)[B
invokespecial net/lingala/zip4j/core/HeaderWriter/writeZipHeaderBytes(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;[B)V
L7:
return
L8:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
iconst_0
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setNoOfDiskStartOfZip64EndOfCentralDirRec(I)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
iconst_1
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setTotNumberOfDiscs(I)V
L9:
goto L5
L2:
astore 1
aload 1
athrow
L3:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 7
.limit stack 7
.end method

.method public finalizeZipFileWithoutValidations(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;)V
.throws net/lingala/zip4j/exception/ZipException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch net/lingala/zip4j/exception/ZipException from L1 to L4 using L2
.catch java/lang/Exception from L1 to L4 using L3
.catch net/lingala/zip4j/exception/ZipException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
.catch net/lingala/zip4j/exception/ZipException from L5 to L6 using L2
.catch java/lang/Exception from L5 to L6 using L3
aload 1
ifnull L7
aload 2
ifnonnull L0
L7:
new net/lingala/zip4j/exception/ZipException
dup
ldc "input parameters is null, cannot finalize zip file without validations"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 6
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getOffsetOfStartOfCentralDir()J
lstore 4
aload 0
aload 1
aload 2
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/writeCentralDirectory(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;Ljava/util/List;)I
istore 3
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/isZip64Format()Z
ifeq L5
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
ifnonnull L1
aload 1
new net/lingala/zip4j/model/Zip64EndCentralDirRecord
dup
invokespecial net/lingala/zip4j/model/Zip64EndCentralDirRecord/<init>()V
invokevirtual net/lingala/zip4j/model/ZipModel/setZip64EndCentralDirRecord(Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;)V
L1:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
ifnonnull L4
aload 1
new net/lingala/zip4j/model/Zip64EndCentralDirLocator
dup
invokespecial net/lingala/zip4j/model/Zip64EndCentralDirLocator/<init>()V
invokevirtual net/lingala/zip4j/model/ZipModel/setZip64EndCentralDirLocator(Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;)V
L4:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
iload 3
i2l
lload 4
ladd
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setOffsetZip64EndOfCentralDirRec(J)V
aload 0
aload 1
aload 2
iload 3
lload 4
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/writeZip64EndOfCentralDirectoryRecord(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;IJLjava/util/List;)V
aload 0
aload 1
aload 2
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/writeZip64EndOfCentralDirectoryLocator(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;Ljava/util/List;)V
L5:
aload 0
aload 1
aload 2
iload 3
lload 4
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/writeEndOfCentralDirectoryRecord(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;IJLjava/util/List;)V
aload 0
aload 1
aload 2
aload 0
aload 6
invokespecial net/lingala/zip4j/core/HeaderWriter/byteArrayListToByteArray(Ljava/util/List;)[B
invokespecial net/lingala/zip4j/core/HeaderWriter/writeZipHeaderBytes(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;[B)V
L6:
return
L2:
astore 1
aload 1
athrow
L3:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 7
.limit stack 7
.end method

.method public updateLocalFileHeader(Lnet/lingala/zip4j/model/LocalFileHeader;JILnet/lingala/zip4j/model/ZipModel;[BILnet/lingala/zip4j/io/SplitOutputStream;)V
.throws net/lingala/zip4j/exception/ZipException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch java/lang/Exception from L8 to L9 using L2
.catch java/lang/Exception from L10 to L11 using L2
.catch java/lang/Exception from L12 to L13 using L2
.catch java/lang/Exception from L14 to L15 using L2
.catch java/lang/Exception from L16 to L17 using L2
aload 1
ifnull L18
lload 2
lconst_0
lcmp
iflt L18
aload 5
ifnonnull L19
L18:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid input parameters, cannot update local file header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L19:
iconst_0
istore 9
L0:
iload 7
aload 8
invokevirtual net/lingala/zip4j/io/SplitOutputStream/getCurrSplitFileCounter()I
if_icmpeq L20
new java/io/File
dup
aload 5
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 15
aload 15
invokevirtual java/io/File/getParent()Ljava/lang/String;
astore 14
aload 15
invokevirtual java/io/File/getName()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/getZipFileNameWithoutExt(Ljava/lang/String;)Ljava/lang/String;
astore 15
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 14
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "file.separator"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 14
L1:
iload 7
bipush 9
if_icmpge L10
L3:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 14
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 15
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".z0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 7
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 14
L4:
new net/lingala/zip4j/io/SplitOutputStream
dup
new java/io/File
dup
aload 14
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokespecial net/lingala/zip4j/io/SplitOutputStream/<init>(Ljava/io/File;)V
astore 14
L5:
iconst_1
istore 7
L6:
aload 14
invokevirtual net/lingala/zip4j/io/SplitOutputStream/getFilePointer()J
lstore 10
L7:
iload 4
lookupswitch
14 : L12
18 : L21
22 : L21
default : L22
L23:
iload 7
ifeq L16
L8:
aload 14
invokevirtual net/lingala/zip4j/io/SplitOutputStream/close()V
L9:
return
L10:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 14
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 15
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".z"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 7
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 14
L11:
goto L4
L12:
aload 14
iload 4
i2l
lload 2
ladd
invokevirtual net/lingala/zip4j/io/SplitOutputStream/seek(J)V
aload 14
aload 6
invokevirtual net/lingala/zip4j/io/SplitOutputStream/write([B)V
L13:
goto L23
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L21:
iload 4
i2l
lstore 12
L14:
aload 0
aload 14
aload 1
lload 2
lload 12
aload 6
aload 5
invokevirtual net/lingala/zip4j/model/ZipModel/isZip64Format()Z
invokespecial net/lingala/zip4j/core/HeaderWriter/updateCompressedSizeInLocalFileHeader(Lnet/lingala/zip4j/io/SplitOutputStream;Lnet/lingala/zip4j/model/LocalFileHeader;JJ[BZ)V
L15:
goto L23
L16:
aload 8
lload 10
invokevirtual net/lingala/zip4j/io/SplitOutputStream/seek(J)V
L17:
return
L22:
goto L23
L20:
aload 8
astore 14
iload 9
istore 7
goto L6
.limit locals 16
.limit stack 9
.end method

.method public writeExtendedLocalHeader(Lnet/lingala/zip4j/model/LocalFileHeader;Ljava/io/OutputStream;)I
.throws net/lingala/zip4j/exception/ZipException
.throws java/io/IOException
aload 1
ifnull L0
aload 2
ifnonnull L1
L0:
new net/lingala/zip4j/exception/ZipException
dup
ldc "input parameters is null, cannot write extended local header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 7
iconst_4
newarray byte
astore 8
aload 8
iconst_0
ldc_w 134695760
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 8
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 8
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getCrc32()J
l2i
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 8
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getCompressedSize()J
lstore 5
lload 5
lstore 3
lload 5
ldc2_w 2147483647L
lcmp
iflt L2
ldc2_w 2147483647L
lstore 3
L2:
aload 8
iconst_0
lload 3
l2i
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 8
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getUncompressedSize()J
lstore 5
lload 5
lstore 3
lload 5
ldc2_w 2147483647L
lcmp
iflt L3
ldc2_w 2147483647L
lstore 3
L3:
aload 8
iconst_0
lload 3
l2i
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 8
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 0
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/byteArrayListToByteArray(Ljava/util/List;)[B
astore 1
aload 2
aload 1
invokevirtual java/io/OutputStream/write([B)V
aload 1
arraylength
ireturn
.limit locals 9
.limit stack 4
.end method

.method public writeLocalFileHeader(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/LocalFileHeader;Ljava/io/OutputStream;)I
.throws net/lingala/zip4j/exception/ZipException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch net/lingala/zip4j/exception/ZipException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
.catch net/lingala/zip4j/exception/ZipException from L6 to L7 using L2
.catch java/lang/Exception from L6 to L7 using L3
.catch net/lingala/zip4j/exception/ZipException from L7 to L8 using L2
.catch java/lang/Exception from L7 to L8 using L3
.catch net/lingala/zip4j/exception/ZipException from L9 to L10 using L2
.catch java/lang/Exception from L9 to L10 using L3
.catch net/lingala/zip4j/exception/ZipException from L11 to L12 using L2
.catch java/lang/Exception from L11 to L12 using L3
.catch net/lingala/zip4j/exception/ZipException from L13 to L14 using L2
.catch java/lang/Exception from L13 to L14 using L3
.catch net/lingala/zip4j/exception/ZipException from L14 to L15 using L2
.catch java/lang/Exception from L14 to L15 using L3
.catch net/lingala/zip4j/exception/ZipException from L15 to L16 using L2
.catch java/lang/Exception from L15 to L16 using L3
.catch net/lingala/zip4j/exception/ZipException from L16 to L17 using L2
.catch java/lang/Exception from L16 to L17 using L3
.catch net/lingala/zip4j/exception/ZipException from L18 to L19 using L2
.catch java/lang/Exception from L18 to L19 using L3
aload 2
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input parameters are null, cannot write local file header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 7
iconst_2
newarray byte
astore 8
iconst_4
newarray byte
astore 9
bipush 8
newarray byte
astore 10
aload 9
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getSignature()I
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 9
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 8
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getVersionNeededToExtract()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 8
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 0
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getGeneralPurposeFlag()[B
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 8
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getCompressionMethod()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 8
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 9
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getLastModFileTime()I
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 9
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 9
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getCrc32()J
l2i
invokestatic net/lingala/zip4j/util/Raw/writeIntLittleEndian([BII)V
aload 0
aload 9
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L1:
iconst_0
istore 5
L4:
ldc2_w 50L
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getUncompressedSize()J
ladd
ldc2_w 4294967295L
lcmp
iflt L16
aload 10
iconst_0
ldc2_w 4294967295L
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 10
iconst_0
aload 9
iconst_0
iconst_4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 9
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 0
aload 9
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 1
iconst_1
invokevirtual net/lingala/zip4j/model/ZipModel/setZip64Format(Z)V
L5:
iconst_1
istore 5
L6:
aload 2
iconst_1
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setWriteComprSizeInZip64ExtraRecord(Z)V
L7:
aload 8
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getFileNameLength()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 8
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L8:
iconst_0
istore 4
iload 5
ifeq L20
iconst_0
bipush 20
iadd
istore 4
L20:
iload 4
istore 6
L9:
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getAesExtraDataRecord()Lnet/lingala/zip4j/model/AESExtraDataRecord;
ifnull L11
L10:
iload 4
bipush 11
iadd
istore 6
L11:
aload 8
iconst_0
iload 6
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 8
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getFileNameCharset()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifeq L18
aload 0
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getFileName()Ljava/lang/String;
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getFileNameCharset()Ljava/lang/String;
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L12:
iload 5
ifeq L14
L13:
aload 8
iconst_0
iconst_1
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 8
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 8
iconst_0
bipush 16
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 8
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 10
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getUncompressedSize()J
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 0
aload 10
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 0
bipush 8
newarray byte
dup
iconst_0
ldc_w 0
bastore
dup
iconst_1
ldc_w 0
bastore
dup
iconst_2
ldc_w 0
bastore
dup
iconst_3
ldc_w 0
bastore
dup
iconst_4
ldc_w 0
bastore
dup
iconst_5
ldc_w 0
bastore
dup
bipush 6
ldc_w 0
bastore
dup
bipush 7
ldc_w 0
bastore
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L14:
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getAesExtraDataRecord()Lnet/lingala/zip4j/model/AESExtraDataRecord;
ifnull L15
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getAesExtraDataRecord()Lnet/lingala/zip4j/model/AESExtraDataRecord;
astore 1
aload 8
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getSignature()J
l2i
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 8
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 8
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getDataSize()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 8
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 8
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getVersionNumber()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 8
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 0
aload 1
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getVendorID()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 0
iconst_1
newarray byte
dup
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getAesStrength()I
i2b
bastore
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 8
iconst_0
aload 1
invokevirtual net/lingala/zip4j/model/AESExtraDataRecord/getCompressionMethod()I
i2s
invokestatic net/lingala/zip4j/util/Raw/writeShortLittleEndian([BIS)V
aload 0
aload 8
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L15:
aload 0
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/byteArrayListToByteArray(Ljava/util/List;)[B
astore 1
aload 3
aload 1
invokevirtual java/io/OutputStream/write([B)V
aload 1
arraylength
ireturn
L16:
aload 10
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getCompressedSize()J
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 10
iconst_0
aload 9
iconst_0
iconst_4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 9
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 10
iconst_0
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getUncompressedSize()J
invokestatic net/lingala/zip4j/util/Raw/writeLongLittleEndian([BIJ)V
aload 10
iconst_0
aload 9
iconst_0
iconst_4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 9
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
aload 2
iconst_0
invokevirtual net/lingala/zip4j/model/LocalFileHeader/setWriteComprSizeInZip64ExtraRecord(Z)V
L17:
goto L7
L2:
astore 1
aload 1
athrow
L18:
aload 0
aload 2
invokevirtual net/lingala/zip4j/model/LocalFileHeader/getFileName()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/convertCharset(Ljava/lang/String;)[B
aload 7
invokespecial net/lingala/zip4j/core/HeaderWriter/copyByteArrayToArrayList([BLjava/util/List;)V
L19:
goto L12
L3:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 11
.limit stack 5
.end method
