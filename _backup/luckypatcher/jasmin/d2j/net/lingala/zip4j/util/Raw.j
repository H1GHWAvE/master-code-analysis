.bytecode 50.0
.class public synchronized net/lingala/zip4j/util/Raw
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static bitArrayToByte([I)B
.throws net/lingala/zip4j/exception/ZipException
aload 0
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "bit array is null, cannot calculate byte from bits"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
arraylength
bipush 8
if_icmpeq L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid bit array length, cannot calculate byte"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
invokestatic net/lingala/zip4j/util/Raw/checkBits([I)Z
ifne L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid bits provided, bits contain other values than 0 or 1"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
iconst_0
istore 2
iconst_0
istore 1
L3:
iload 1
aload 0
arraylength
if_icmpge L4
iload 2
i2d
ldc2_w 2.0D
iload 1
i2d
invokestatic java/lang/Math/pow(DD)D
aload 0
iload 1
iaload
i2d
dmul
dadd
d2i
istore 2
iload 1
iconst_1
iadd
istore 1
goto L3
L4:
iload 2
i2b
ireturn
.limit locals 3
.limit stack 6
.end method

.method private static checkBits([I)Z
iconst_1
istore 3
iconst_0
istore 1
L0:
iload 3
istore 2
iload 1
aload 0
arraylength
if_icmpge L1
aload 0
iload 1
iaload
ifeq L2
aload 0
iload 1
iaload
iconst_1
if_icmpeq L2
iconst_0
istore 2
L1:
iload 2
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
.limit locals 4
.limit stack 2
.end method

.method public static convertCharArrayToByteArray([C)[B
aload 0
ifnonnull L0
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L0:
aload 0
arraylength
newarray byte
astore 2
iconst_0
istore 1
L1:
iload 1
aload 0
arraylength
if_icmpge L2
aload 2
iload 1
aload 0
iload 1
caload
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 2
areturn
.limit locals 3
.limit stack 4
.end method

.method public static prepareBuffAESIVBytes([BII)V
aload 0
iconst_0
iload 1
i2b
bastore
aload 0
iconst_1
iload 1
bipush 8
ishr
i2b
bastore
aload 0
iconst_2
iload 1
bipush 16
ishr
i2b
bastore
aload 0
iconst_3
iload 1
bipush 24
ishr
i2b
bastore
aload 0
iconst_4
iconst_0
bastore
aload 0
iconst_5
iconst_0
bastore
aload 0
bipush 6
iconst_0
bastore
aload 0
bipush 7
iconst_0
bastore
aload 0
bipush 8
iconst_0
bastore
aload 0
bipush 9
iconst_0
bastore
aload 0
bipush 10
iconst_0
bastore
aload 0
bipush 11
iconst_0
bastore
aload 0
bipush 12
iconst_0
bastore
aload 0
bipush 13
iconst_0
bastore
aload 0
bipush 14
iconst_0
bastore
aload 0
bipush 15
iconst_0
bastore
return
.limit locals 3
.limit stack 4
.end method

.method public static readIntLittleEndian([BI)I
aload 0
iload 1
baload
sipush 255
iand
aload 0
iload 1
iconst_1
iadd
baload
sipush 255
iand
bipush 8
ishl
ior
aload 0
iload 1
iconst_2
iadd
baload
sipush 255
iand
aload 0
iload 1
iconst_3
iadd
baload
sipush 255
iand
bipush 8
ishl
ior
bipush 16
ishl
ior
ireturn
.limit locals 2
.limit stack 5
.end method

.method public static readLeInt(Ljava/io/DataInput;[B)I
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
aload 1
iconst_0
iconst_4
invokeinterface java/io/DataInput/readFully([BII)V 3
L1:
aload 1
iconst_0
baload
sipush 255
iand
aload 1
iconst_1
baload
sipush 255
iand
bipush 8
ishl
ior
aload 1
iconst_2
baload
sipush 255
iand
aload 1
iconst_3
baload
sipush 255
iand
bipush 8
ishl
ior
bipush 16
ishl
ior
ireturn
L2:
astore 0
new net/lingala/zip4j/exception/ZipException
dup
aload 0
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 4
.end method

.method public static readLongLittleEndian([BI)J
lconst_0
aload 0
iload 1
bipush 7
iadd
baload
sipush 255
iand
i2l
lor
bipush 8
lshl
aload 0
iload 1
bipush 6
iadd
baload
sipush 255
iand
i2l
lor
bipush 8
lshl
aload 0
iload 1
iconst_5
iadd
baload
sipush 255
iand
i2l
lor
bipush 8
lshl
aload 0
iload 1
iconst_4
iadd
baload
sipush 255
iand
i2l
lor
bipush 8
lshl
aload 0
iload 1
iconst_3
iadd
baload
sipush 255
iand
i2l
lor
bipush 8
lshl
aload 0
iload 1
iconst_2
iadd
baload
sipush 255
iand
i2l
lor
bipush 8
lshl
aload 0
iload 1
iconst_1
iadd
baload
sipush 255
iand
i2l
lor
bipush 8
lshl
aload 0
iload 1
baload
sipush 255
iand
i2l
lor
lreturn
.limit locals 2
.limit stack 5
.end method

.method public static final readShortBigEndian([BI)S
aload 0
iload 1
baload
sipush 255
iand
iconst_0
ior
i2s
bipush 8
ishl
i2s
istore 2
aload 0
iload 1
iconst_1
iadd
baload
sipush 255
iand
iload 2
ior
i2s
ireturn
.limit locals 3
.limit stack 3
.end method

.method public static readShortLittleEndian([BI)I
aload 0
iload 1
baload
sipush 255
iand
aload 0
iload 1
iconst_1
iadd
baload
sipush 255
iand
bipush 8
ishl
ior
ireturn
.limit locals 2
.limit stack 4
.end method

.method public static toByteArray(I)[B
iconst_4
newarray byte
dup
iconst_0
iload 0
i2b
bastore
dup
iconst_1
iload 0
bipush 8
ishr
i2b
bastore
dup
iconst_2
iload 0
bipush 16
ishr
i2b
bastore
dup
iconst_3
iload 0
bipush 24
ishr
i2b
bastore
areturn
.limit locals 1
.limit stack 5
.end method

.method public static toByteArray(II)[B
iload 1
newarray byte
astore 2
iload 0
invokestatic net/lingala/zip4j/util/Raw/toByteArray(I)[B
astore 3
iconst_0
istore 0
L0:
iload 0
aload 3
arraylength
if_icmpge L1
iload 0
iload 1
if_icmpge L1
aload 2
iload 0
aload 3
iload 0
baload
bastore
iload 0
iconst_1
iadd
istore 0
goto L0
L1:
aload 2
areturn
.limit locals 4
.limit stack 4
.end method

.method public static final writeIntLittleEndian([BII)V
aload 0
iload 1
iconst_3
iadd
iload 2
bipush 24
iushr
i2b
bastore
aload 0
iload 1
iconst_2
iadd
iload 2
bipush 16
iushr
i2b
bastore
aload 0
iload 1
iconst_1
iadd
iload 2
bipush 8
iushr
i2b
bastore
aload 0
iload 1
iload 2
sipush 255
iand
i2b
bastore
return
.limit locals 3
.limit stack 4
.end method

.method public static writeLongLittleEndian([BIJ)V
aload 0
iload 1
bipush 7
iadd
lload 2
bipush 56
lushr
l2i
i2b
bastore
aload 0
iload 1
bipush 6
iadd
lload 2
bipush 48
lushr
l2i
i2b
bastore
aload 0
iload 1
iconst_5
iadd
lload 2
bipush 40
lushr
l2i
i2b
bastore
aload 0
iload 1
iconst_4
iadd
lload 2
bipush 32
lushr
l2i
i2b
bastore
aload 0
iload 1
iconst_3
iadd
lload 2
bipush 24
lushr
l2i
i2b
bastore
aload 0
iload 1
iconst_2
iadd
lload 2
bipush 16
lushr
l2i
i2b
bastore
aload 0
iload 1
iconst_1
iadd
lload 2
bipush 8
lushr
l2i
i2b
bastore
aload 0
iload 1
ldc2_w 255L
lload 2
land
l2i
i2b
bastore
return
.limit locals 4
.limit stack 6
.end method

.method public static final writeShortLittleEndian([BIS)V
aload 0
iload 1
iconst_1
iadd
iload 2
bipush 8
iushr
i2b
bastore
aload 0
iload 1
iload 2
sipush 255
iand
i2b
bastore
return
.limit locals 3
.limit stack 4
.end method
