.bytecode 50.0
.class public synchronized net/lingala/zip4j/util/CRCUtil
.super java/lang/Object

.field private static final 'BUF_SIZE' I = 16384


.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static computeFileCRC(Ljava/lang/String;)J
.throws net/lingala/zip4j/exception/ZipException
aload 0
aconst_null
invokestatic net/lingala/zip4j/util/CRCUtil/computeFileCRC(Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;)J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public static computeFileCRC(Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;)J
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch all from L0 to L1 using L4
.catch java/io/IOException from L5 to L6 using L2
.catch java/lang/Exception from L5 to L6 using L3
.catch all from L5 to L6 using L4
.catch java/io/IOException from L6 to L7 using L8
.catch java/lang/Exception from L6 to L7 using L9
.catch all from L6 to L7 using L10
.catch java/io/IOException from L7 to L11 using L8
.catch java/lang/Exception from L7 to L11 using L9
.catch all from L7 to L11 using L10
.catch java/io/IOException from L12 to L13 using L8
.catch java/lang/Exception from L12 to L13 using L9
.catch all from L12 to L13 using L10
.catch java/io/IOException from L14 to L15 using L8
.catch java/lang/Exception from L14 to L15 using L9
.catch all from L14 to L15 using L10
.catch java/io/IOException from L16 to L17 using L18
.catch java/io/IOException from L19 to L20 using L8
.catch java/lang/Exception from L19 to L20 using L9
.catch all from L19 to L20 using L10
.catch java/io/IOException from L21 to L22 using L23
.catch all from L24 to L4 using L4
.catch java/io/IOException from L25 to L26 using L27
.catch all from L28 to L27 using L4
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L29
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file is null or empty, cannot calculate CRC for the file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L29:
aconst_null
astore 10
aconst_null
astore 9
aconst_null
astore 8
aload 10
astore 7
L0:
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileReadAccess(Ljava/lang/String;)Z
pop
L1:
aload 10
astore 7
L5:
new java/io/FileInputStream
dup
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 0
L6:
sipush 16384
newarray byte
astore 7
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 8
L7:
aload 0
aload 7
invokevirtual java/io/InputStream/read([B)I
istore 2
L11:
iload 2
iconst_m1
if_icmpeq L19
L12:
aload 8
aload 7
iconst_0
iload 2
invokevirtual java/util/zip/CRC32/update([BII)V
L13:
aload 1
ifnull L7
L14:
aload 1
iload 2
i2l
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/updateWorkCompleted(J)V
aload 1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/isCancelAllTasks()Z
ifeq L7
aload 1
iconst_3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setResult(I)V
aload 1
iconst_0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
L15:
lconst_0
lstore 5
lload 5
lstore 3
aload 0
ifnull L30
L16:
aload 0
invokevirtual java/io/InputStream/close()V
L17:
lload 5
lstore 3
L30:
lload 3
lreturn
L18:
astore 0
new net/lingala/zip4j/exception/ZipException
dup
ldc "error while closing the file after calculating crc"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L19:
aload 8
invokevirtual java/util/zip/CRC32/getValue()J
lstore 5
L20:
lload 5
lstore 3
aload 0
ifnull L30
L21:
aload 0
invokevirtual java/io/InputStream/close()V
L22:
lload 5
lreturn
L23:
astore 0
new net/lingala/zip4j/exception/ZipException
dup
ldc "error while closing the file after calculating crc"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 0
aload 8
astore 7
L24:
new net/lingala/zip4j/exception/ZipException
dup
aload 0
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L4:
astore 0
L31:
aload 7
ifnull L26
L25:
aload 7
invokevirtual java/io/InputStream/close()V
L26:
aload 0
athrow
L3:
astore 0
aload 9
astore 7
L28:
new net/lingala/zip4j/exception/ZipException
dup
aload 0
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L27:
astore 0
new net/lingala/zip4j/exception/ZipException
dup
ldc "error while closing the file after calculating crc"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L10:
astore 1
aload 0
astore 7
aload 1
astore 0
goto L31
L9:
astore 1
aload 0
astore 7
aload 1
astore 0
goto L28
L8:
astore 1
aload 0
astore 7
aload 1
astore 0
goto L24
.limit locals 11
.limit stack 5
.end method
