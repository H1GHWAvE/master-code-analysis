.bytecode 50.0
.class public abstract interface net/lingala/zip4j/util/InternalZipConstants
.super java/lang/Object

.field public static final 'AESSIG' I = 39169


.field public static final 'AES_AUTH_LENGTH' I = 10


.field public static final 'AES_BLOCK_SIZE' I = 16


.field public static final 'ARCEXTDATREC' J = 134630224L


.field public static final 'BUFF_SIZE' I = 4096


.field public static final 'CENATT' I = 36


.field public static final 'CENATX' I = 38


.field public static final 'CENCOM' I = 32


.field public static final 'CENCRC' I = 16


.field public static final 'CENDSK' I = 34


.field public static final 'CENEXT' I = 30


.field public static final 'CENFLG' I = 8


.field public static final 'CENHDR' I = 46


.field public static final 'CENHOW' I = 10


.field public static final 'CENLEN' I = 24


.field public static final 'CENNAM' I = 28


.field public static final 'CENOFF' I = 42


.field public static final 'CENSIG' J = 33639248L


.field public static final 'CENSIZ' I = 20


.field public static final 'CENTIM' I = 12


.field public static final 'CENVEM' I = 4


.field public static final 'CENVER' I = 6


.field public static final 'CHARSET_COMMENTS_DEFAULT' Ljava/lang/String; = "windows-1254"

.field public static final 'CHARSET_CP850' Ljava/lang/String; = "Cp850"

.field public static final 'CHARSET_DEFAULT' Ljava/lang/String;

.field public static final 'CHARSET_UTF8' Ljava/lang/String; = "UTF8"

.field public static final 'DIGSIG' J = 84233040L


.field public static final 'ENDCOM' I = 20


.field public static final 'ENDHDR' I = 22


.field public static final 'ENDOFF' I = 16


.field public static final 'ENDSIG' J = 101010256L


.field public static final 'ENDSIZ' I = 12


.field public static final 'ENDSUB' I = 8


.field public static final 'ENDTOT' I = 10


.field public static final 'EXTCRC' I = 4


.field public static final 'EXTHDR' I = 16


.field public static final 'EXTLEN' I = 12


.field public static final 'EXTRAFIELDZIP64LENGTH' I = 1


.field public static final 'EXTSIG' J = 134695760L


.field public static final 'EXTSIZ' I = 8


.field public static final 'FILE_MODE_ARCHIVE' I = 32


.field public static final 'FILE_MODE_HIDDEN' I = 2


.field public static final 'FILE_MODE_HIDDEN_ARCHIVE' I = 34


.field public static final 'FILE_MODE_NONE' I = 0


.field public static final 'FILE_MODE_READ_ONLY' I = 1


.field public static final 'FILE_MODE_READ_ONLY_ARCHIVE' I = 33


.field public static final 'FILE_MODE_READ_ONLY_HIDDEN' I = 3


.field public static final 'FILE_MODE_READ_ONLY_HIDDEN_ARCHIVE' I = 35


.field public static final 'FILE_MODE_SYSTEM' I = 38


.field public static final 'FILE_SEPARATOR' Ljava/lang/String;

.field public static final 'FOLDER_MODE_ARCHIVE' I = 48


.field public static final 'FOLDER_MODE_HIDDEN' I = 18


.field public static final 'FOLDER_MODE_HIDDEN_ARCHIVE' I = 50


.field public static final 'FOLDER_MODE_NONE' I = 16


.field public static final 'LIST_TYPE_FILE' I = 1


.field public static final 'LIST_TYPE_STRING' I = 2


.field public static final 'LOCCRC' I = 14


.field public static final 'LOCEXT' I = 28


.field public static final 'LOCFLG' I = 6


.field public static final 'LOCHDR' I = 30


.field public static final 'LOCHOW' I = 8


.field public static final 'LOCLEN' I = 22


.field public static final 'LOCNAM' I = 26


.field public static final 'LOCSIG' J = 67324752L


.field public static final 'LOCSIZ' I = 18


.field public static final 'LOCTIM' I = 10


.field public static final 'LOCVER' I = 4


.field public static final 'MAX_ALLOWED_ZIP_COMMENT_LENGTH' I = 65535


.field public static final 'MIN_SPLIT_LENGTH' I = 65536


.field public static final 'MODE_UNZIP' I = 2


.field public static final 'MODE_ZIP' I = 1


.field public static final 'OFFSET_CENTRAL_DIR' Ljava/lang/String; = "offsetCentralDir"

.field public static final 'READ_MODE' Ljava/lang/String; = "r"

.field public static final 'SPLITSIG' J = 134695760L


.field public static final 'STD_DEC_HDR_SIZE' I = 12


.field public static final 'THREAD_NAME' Ljava/lang/String; = "Zip4j"

.field public static final 'UFT8_NAMES_FLAG' I = 2048


.field public static final 'UPDATE_LFH_COMP_SIZE' I = 18


.field public static final 'UPDATE_LFH_CRC' I = 14


.field public static final 'UPDATE_LFH_UNCOMP_SIZE' I = 22


.field public static final 'VERSION' Ljava/lang/String; = "1.3.2"

.field public static final 'WRITE_MODE' Ljava/lang/String; = "rw"

.field public static final 'ZIP64ENDCENDIRLOC' J = 117853008L


.field public static final 'ZIP64ENDCENDIRREC' J = 101075792L


.field public static final 'ZIP_64_LIMIT' J = 4294967295L


.field public static final 'ZIP_FILE_SEPARATOR' Ljava/lang/String; = "/"

.method static <clinit>()V
ldc "file.encoding"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
putstatic net/lingala/zip4j/util/InternalZipConstants/CHARSET_DEFAULT Ljava/lang/String;
ldc "file.separator"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
putstatic net/lingala/zip4j/util/InternalZipConstants/FILE_SEPARATOR Ljava/lang/String;
return
.limit locals 0
.limit stack 1
.end method
