.bytecode 50.0
.class public synchronized net/lingala/zip4j/util/Zip4jUtil
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static checkArrayListTypes(Ljava/util/ArrayList;I)Z
.throws net/lingala/zip4j/exception/ZipException
aload 0
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input arraylist is null, cannot check types"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual java/util/ArrayList/size()I
ifgt L1
L2:
iconst_1
ireturn
L1:
iconst_0
istore 3
iload 1
tableswitch 1
L3
L4
default : L5
L5:
iload 3
istore 1
L6:
iload 1
ifeq L2
iconst_0
ireturn
L3:
iconst_0
istore 2
L7:
iload 3
istore 1
iload 2
aload 0
invokevirtual java/util/ArrayList/size()I
if_icmpge L6
aload 0
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
instanceof java/io/File
ifne L8
iconst_1
istore 1
goto L6
L8:
iload 2
iconst_1
iadd
istore 2
goto L7
L4:
iconst_0
istore 2
L9:
iload 3
istore 1
iload 2
aload 0
invokevirtual java/util/ArrayList/size()I
if_icmpge L6
aload 0
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
instanceof java/lang/String
ifne L10
iconst_1
istore 1
goto L6
L10:
iload 2
iconst_1
iadd
istore 2
goto L9
.limit locals 4
.limit stack 3
.end method

.method public static checkFileExists(Ljava/io/File;)Z
.throws net/lingala/zip4j/exception/ZipException
aload 0
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "cannot check if file exists: input file is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual java/io/File/exists()Z
ireturn
.limit locals 1
.limit stack 3
.end method

.method public static checkFileExists(Ljava/lang/String;)Z
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "path is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/io/File;)Z
ireturn
.limit locals 1
.limit stack 3
.end method

.method public static checkFileReadAccess(Ljava/lang/String;)Z
.throws net/lingala/zip4j/exception/ZipException
.catch java/lang/Exception from L0 to L1 using L2
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "path is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "file does not exist: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/canRead()Z
istore 1
L1:
iload 1
ireturn
L2:
astore 0
new net/lingala/zip4j/exception/ZipException
dup
ldc "cannot read zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 4
.end method

.method public static checkFileWriteAccess(Ljava/lang/String;)Z
.throws net/lingala/zip4j/exception/ZipException
.catch java/lang/Exception from L0 to L1 using L2
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "path is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "file does not exist: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/canWrite()Z
istore 1
L1:
iload 1
ireturn
L2:
astore 0
new net/lingala/zip4j/exception/ZipException
dup
ldc "cannot read zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 4
.end method

.method public static checkOutputFolder(Ljava/lang/String;)Z
.throws net/lingala/zip4j/exception/ZipException
.catch java/lang/Exception from L0 to L1 using L1
.catch java/lang/Exception from L2 to L3 using L1
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L4
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/NullPointerException
dup
ldc "output path is null"
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L4:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L0
aload 0
invokevirtual java/io/File/isDirectory()Z
ifne L5
new net/lingala/zip4j/exception/ZipException
dup
ldc "output folder is not valid"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L5:
aload 0
invokevirtual java/io/File/canWrite()Z
ifne L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "no write access to output folder"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual java/io/File/mkdirs()Z
pop
aload 0
invokevirtual java/io/File/isDirectory()Z
ifne L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "output folder is not valid"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 0
new net/lingala/zip4j/exception/ZipException
dup
ldc "Cannot create destination folder"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
invokevirtual java/io/File/canWrite()Z
ifne L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "no write access to destination folder"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
iconst_1
ireturn
.limit locals 1
.limit stack 5
.end method

.method public static convertCharset(Ljava/lang/String;)[B
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch java/io/UnsupportedEncodingException from L1 to L4 using L2
.catch java/lang/Exception from L1 to L4 using L3
.catch java/io/UnsupportedEncodingException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
L0:
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/detectCharSet(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 1
ldc "Cp850"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 0
ldc "Cp850"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
areturn
L1:
aload 1
ldc "UTF8"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
aload 0
ldc "UTF8"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
areturn
L4:
aload 0
invokevirtual java/lang/String/getBytes()[B
astore 1
L5:
aload 1
areturn
L2:
astore 1
aload 0
invokevirtual java/lang/String/getBytes()[B
areturn
L3:
astore 0
new net/lingala/zip4j/exception/ZipException
dup
aload 0
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public static decodeFileName([BZ)Ljava/lang/String;
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
iload 1
ifeq L3
L0:
new java/lang/String
dup
aload 0
ldc "UTF8"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
astore 2
L1:
aload 2
areturn
L2:
astore 2
new java/lang/String
dup
aload 0
invokespecial java/lang/String/<init>([B)V
areturn
L3:
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/getCp850EncodedString([B)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method

.method public static detectCharSet(Ljava/lang/String;)Ljava/lang/String;
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch java/io/UnsupportedEncodingException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
.catch java/io/UnsupportedEncodingException from L6 to L7 using L2
.catch java/lang/Exception from L6 to L7 using L3
aload 0
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input string is null, cannot detect charset"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
new java/lang/String
dup
aload 0
ldc "Cp850"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
ldc "Cp850"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
L1:
ldc "Cp850"
areturn
L4:
aload 0
new java/lang/String
dup
aload 0
ldc "UTF8"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
ldc "UTF8"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L6
L5:
ldc "UTF8"
areturn
L6:
getstatic net/lingala/zip4j/util/InternalZipConstants/CHARSET_DEFAULT Ljava/lang/String;
astore 0
L7:
aload 0
areturn
L2:
astore 0
getstatic net/lingala/zip4j/util/InternalZipConstants/CHARSET_DEFAULT Ljava/lang/String;
areturn
L3:
astore 0
getstatic net/lingala/zip4j/util/InternalZipConstants/CHARSET_DEFAULT Ljava/lang/String;
areturn
.limit locals 1
.limit stack 5
.end method

.method public static dosToJavaTme(I)J
invokestatic java/util/Calendar/getInstance()Ljava/util/Calendar;
astore 1
aload 1
iload 0
bipush 25
ishr
bipush 127
iand
sipush 1980
iadd
iload 0
bipush 21
ishr
bipush 15
iand
iconst_1
isub
iload 0
bipush 16
ishr
bipush 31
iand
iload 0
bipush 11
ishr
bipush 31
iand
iload 0
iconst_5
ishr
bipush 63
iand
iload 0
bipush 31
iand
iconst_2
imul
invokevirtual java/util/Calendar/set(IIIIII)V
aload 1
bipush 14
iconst_0
invokevirtual java/util/Calendar/set(II)V
aload 1
invokevirtual java/util/Calendar/getTime()Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
lreturn
.limit locals 2
.limit stack 8
.end method

.method public static getAbsoluteFilePath(Ljava/lang/String;)Ljava/lang/String;
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "filePath is null or empty, cannot get absolute file path"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method public static getAllHeaderSignatures()[J
bipush 11
newarray long
dup
iconst_0
ldc2_w 67324752L
lastore
dup
iconst_1
ldc2_w 134695760L
lastore
dup
iconst_2
ldc2_w 33639248L
lastore
dup
iconst_3
ldc2_w 101010256L
lastore
dup
iconst_4
ldc2_w 84233040L
lastore
dup
iconst_5
ldc2_w 134630224L
lastore
dup
bipush 6
ldc2_w 134695760L
lastore
dup
bipush 7
ldc2_w 117853008L
lastore
dup
bipush 8
ldc2_w 101075792L
lastore
dup
bipush 9
lconst_1
lastore
dup
bipush 10
ldc2_w 39169L
lastore
areturn
.limit locals 0
.limit stack 5
.end method

.method public static getCp850EncodedString([B)Ljava/lang/String;
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
L0:
new java/lang/String
dup
aload 0
ldc "Cp850"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
astore 1
L1:
aload 1
areturn
L2:
astore 1
new java/lang/String
dup
aload 0
invokespecial java/lang/String/<init>([B)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public static getEncodedStringLength(Ljava/lang/String;)I
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input string is null, cannot calculate encoded String length"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/detectCharSet(Ljava/lang/String;)Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/getEncodedStringLength(Ljava/lang/String;Ljava/lang/String;)I
ireturn
.limit locals 1
.limit stack 3
.end method

.method public static getEncodedStringLength(Ljava/lang/String;Ljava/lang/String;)I
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch java/io/UnsupportedEncodingException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
.catch java/io/UnsupportedEncodingException from L6 to L7 using L2
.catch java/lang/Exception from L6 to L7 using L3
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L8
new net/lingala/zip4j/exception/ZipException
dup
ldc "input string is null, cannot calculate encoded String length"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L8:
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "encoding is not defined, cannot calculate string length"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
ldc "Cp850"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
aload 0
ldc "Cp850"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
astore 1
L1:
aload 1
astore 0
L9:
aload 0
invokevirtual java/nio/ByteBuffer/limit()I
ireturn
L4:
aload 1
ldc "UTF8"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L6
aload 0
ldc "UTF8"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
astore 1
L5:
aload 1
astore 0
goto L9
L6:
aload 0
aload 1
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
astore 1
L7:
aload 1
astore 0
goto L9
L2:
astore 1
aload 0
invokevirtual java/lang/String/getBytes()[B
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
astore 0
goto L9
L3:
astore 0
new net/lingala/zip4j/exception/ZipException
dup
aload 0
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public static getFileHeader(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Lnet/lingala/zip4j/model/FileHeader;
.throws net/lingala/zip4j/exception/ZipException
aload 0
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "zip model is null, cannot determine file header for fileName: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L1
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "file name is null, cannot determine file header for fileName: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileHeaderWithExactMatch(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Lnet/lingala/zip4j/model/FileHeader;
astore 3
aload 3
astore 2
aload 3
ifnonnull L2
aload 1
ldc "\\\\"
ldc "/"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 3
aload 0
aload 3
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileHeaderWithExactMatch(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Lnet/lingala/zip4j/model/FileHeader;
astore 1
aload 1
astore 2
aload 1
ifnonnull L2
aload 0
aload 3
ldc "/"
ldc "\\\\"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileHeaderWithExactMatch(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Lnet/lingala/zip4j/model/FileHeader;
astore 2
L2:
aload 2
areturn
.limit locals 4
.limit stack 4
.end method

.method public static getFileHeaderWithExactMatch(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Lnet/lingala/zip4j/model/FileHeader;
.throws net/lingala/zip4j/exception/ZipException
aload 0
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "zip model is null, cannot determine file header with exact match for fileName: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L1
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "file name is null, cannot determine file header with exact match for fileName: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnonnull L2
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "central directory is null, cannot determine file header with exact match for fileName: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
ifnonnull L3
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "file Headers are null, cannot determine file header with exact match for fileName: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifgt L4
aconst_null
areturn
L4:
aload 0
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
astore 0
iconst_0
istore 2
L5:
iload 2
aload 0
invokevirtual java/util/ArrayList/size()I
if_icmpge L6
aload 0
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
astore 3
aload 3
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
astore 4
aload 4
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L7
L8:
iload 2
iconst_1
iadd
istore 2
goto L5
L7:
aload 1
aload 4
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L8
aload 3
areturn
L6:
aconst_null
areturn
.limit locals 5
.limit stack 4
.end method

.method public static getFileLengh(Ljava/io/File;)J
.throws net/lingala/zip4j/exception/ZipException
aload 0
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file is null, cannot calculate file length"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual java/io/File/isDirectory()Z
ifeq L1
ldc2_w -1L
lreturn
L1:
aload 0
invokevirtual java/io/File/length()J
lreturn
.limit locals 1
.limit stack 3
.end method

.method public static getFileLengh(Ljava/lang/String;)J
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid file name"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileLengh(Ljava/io/File;)J
lreturn
.limit locals 1
.limit stack 3
.end method

.method public static getFileNameFromFilePath(Ljava/io/File;)Ljava/lang/String;
.throws net/lingala/zip4j/exception/ZipException
aload 0
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file is null, cannot get file name"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual java/io/File/isDirectory()Z
ifeq L1
aconst_null
areturn
L1:
aload 0
invokevirtual java/io/File/getName()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method public static getFilesInDirectoryRec(Ljava/io/File;Z)Ljava/util/ArrayList;
.throws net/lingala/zip4j/exception/ZipException
aload 0
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input path is null, cannot read files in the directory"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 3
aload 0
invokevirtual java/io/File/listFiles()[Ljava/io/File;
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
astore 4
aload 0
invokevirtual java/io/File/canRead()Z
ifne L1
L2:
aload 3
areturn
L1:
iconst_0
istore 2
L3:
iload 2
aload 4
invokeinterface java/util/List/size()I 0
if_icmpge L2
aload 4
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/io/File
astore 0
aload 0
invokevirtual java/io/File/isHidden()Z
ifeq L4
iload 1
ifeq L2
L4:
aload 3
aload 0
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
invokevirtual java/io/File/isDirectory()Z
ifeq L5
aload 3
aload 0
iload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFilesInDirectoryRec(Ljava/io/File;Z)Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/addAll(Ljava/util/Collection;)Z
pop
L5:
iload 2
iconst_1
iadd
istore 2
goto L3
.limit locals 5
.limit stack 3
.end method

.method public static getIndexOfFileHeader(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;)I
.throws net/lingala/zip4j/exception/ZipException
aload 0
ifnull L0
aload 1
ifnonnull L1
L0:
new net/lingala/zip4j/exception/ZipException
dup
ldc "input parameters is null, cannot determine index of file header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnonnull L2
new net/lingala/zip4j/exception/ZipException
dup
ldc "central directory is null, ccannot determine index of file header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
ifnonnull L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "file Headers are null, cannot determine index of file header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifgt L4
iconst_m1
ireturn
L4:
aload 1
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
astore 1
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L5
new net/lingala/zip4j/exception/ZipException
dup
ldc "file name in file header is empty or null, cannot determine index of file header"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L5:
aload 0
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
astore 0
iconst_0
istore 2
L6:
iload 2
aload 0
invokevirtual java/util/ArrayList/size()I
if_icmpge L7
aload 0
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
astore 3
aload 3
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L8
L9:
iload 2
iconst_1
iadd
istore 2
goto L6
L8:
aload 1
aload 3
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L9
iload 2
ireturn
L7:
iconst_m1
ireturn
.limit locals 4
.limit stack 3
.end method

.method public static getLastModifiedFileTime(Ljava/io/File;Ljava/util/TimeZone;)J
.throws net/lingala/zip4j/exception/ZipException
aload 0
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file is null, cannot read last modified file time"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual java/io/File/exists()Z
ifne L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file does not exist, cannot read last modified file time"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
invokevirtual java/io/File/lastModified()J
lreturn
.limit locals 2
.limit stack 3
.end method

.method public static getRelativeFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file path/name is empty, cannot calculate relative file name"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifeq L1
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getPath()Ljava/lang/String;
astore 3
aload 3
astore 2
aload 3
getstatic net/lingala/zip4j/util/InternalZipConstants/FILE_SEPARATOR Ljava/lang/String;
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic net/lingala/zip4j/util/InternalZipConstants/FILE_SEPARATOR Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
L2:
aload 0
aload 2
invokevirtual java/lang/String/length()I
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 3
aload 3
astore 2
aload 3
ldc "file.separator"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L3
aload 3
iconst_1
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 2
L3:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/isDirectory()Z
ifeq L4
aload 2
ldc "\\\\"
ldc "/"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L5:
aload 0
astore 2
aload 1
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifeq L6
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
L6:
aload 2
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L7
new net/lingala/zip4j/exception/ZipException
dup
ldc "Error determining file name"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 2
iconst_0
aload 2
aload 0
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
ldc "\\\\"
ldc "/"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
goto L5
L1:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 2
aload 2
invokevirtual java/io/File/isDirectory()Z
ifeq L8
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
goto L5
L8:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileNameFromFilePath(Ljava/io/File;)Ljava/lang/String;
astore 0
goto L5
L7:
aload 2
areturn
.limit locals 4
.limit stack 4
.end method

.method public static getSplitZipFiles(Lnet/lingala/zip4j/model/ZipModel;)Ljava/util/ArrayList;
.throws net/lingala/zip4j/exception/ZipException
aload 0
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "cannot get split zip files: zipmodel is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
ifnonnull L1
aconst_null
astore 3
L2:
aload 3
areturn
L1:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 5
aload 0
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
astore 6
new java/io/File
dup
aload 6
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getName()Ljava/lang/String;
astore 7
aload 6
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "cannot get split zip files: zipfile is null"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifne L4
aload 5
aload 6
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 5
areturn
L4:
aload 0
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getNoOfThisDisk()I
istore 2
iload 2
ifne L5
aload 5
aload 6
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 5
areturn
L5:
iconst_0
istore 1
L6:
aload 5
astore 3
iload 1
iload 2
if_icmpgt L2
iload 1
iload 2
if_icmpne L7
aload 5
aload 0
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L8:
iload 1
iconst_1
iadd
istore 1
goto L6
L7:
ldc ".z0"
astore 3
iload 1
bipush 9
if_icmple L9
ldc ".z"
astore 3
L9:
aload 7
ldc "."
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iflt L10
aload 6
iconst_0
aload 6
ldc "."
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 4
L11:
aload 5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L8
L10:
aload 6
astore 4
goto L11
.limit locals 8
.limit stack 4
.end method

.method public static getZipFileNameWithoutExt(Ljava/lang/String;)Ljava/lang/String;
.throws net/lingala/zip4j/exception/ZipException
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip file name is empty or null, cannot determine zip file name"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
astore 1
aload 0
ldc "file.separator"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iflt L1
aload 0
aload 0
ldc "file.separator"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 1
L1:
aload 1
astore 0
aload 1
ldc "."
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
ifle L2
aload 1
iconst_0
aload 1
ldc "."
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 0
L2:
aload 0
areturn
.limit locals 2
.limit stack 4
.end method

.method public static isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
aload 0
ifnull L0
aload 0
invokevirtual java/lang/String/trim()Ljava/lang/String;
invokevirtual java/lang/String/length()I
ifgt L1
L0:
iconst_0
ireturn
L1:
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static isSupportedCharset(Ljava/lang/String;)Z
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
aload 0
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "charset is null or empty, cannot check if it is supported"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/lang/String
dup
ldc "a"
invokevirtual java/lang/String/getBytes()[B
aload 0
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
pop
L1:
iconst_1
ireturn
L2:
astore 0
iconst_0
ireturn
L3:
astore 0
new net/lingala/zip4j/exception/ZipException
dup
aload 0
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 1
.limit stack 4
.end method

.method public static isWindows()Z
ldc "os.name"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "win"
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iflt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 0
.limit stack 2
.end method

.method public static javaToDosTime(J)J
invokestatic java/util/Calendar/getInstance()Ljava/util/Calendar;
astore 3
aload 3
lload 0
invokevirtual java/util/Calendar/setTimeInMillis(J)V
aload 3
iconst_1
invokevirtual java/util/Calendar/get(I)I
istore 2
iload 2
sipush 1980
if_icmpge L0
ldc2_w 2162688L
lreturn
L0:
iload 2
sipush 1980
isub
bipush 25
ishl
aload 3
iconst_2
invokevirtual java/util/Calendar/get(I)I
iconst_1
iadd
bipush 21
ishl
ior
aload 3
iconst_5
invokevirtual java/util/Calendar/get(I)I
bipush 16
ishl
ior
aload 3
bipush 11
invokevirtual java/util/Calendar/get(I)I
bipush 11
ishl
ior
aload 3
bipush 12
invokevirtual java/util/Calendar/get(I)I
iconst_5
ishl
ior
aload 3
bipush 13
invokevirtual java/util/Calendar/get(I)I
iconst_1
ishr
ior
i2l
lreturn
.limit locals 4
.limit stack 3
.end method

.method public static setFileArchive(Ljava/io/File;)V
.throws net/lingala/zip4j/exception/ZipException
return
.limit locals 1
.limit stack 0
.end method

.method public static setFileHidden(Ljava/io/File;)V
.throws net/lingala/zip4j/exception/ZipException
return
.limit locals 1
.limit stack 0
.end method

.method public static setFileReadOnly(Ljava/io/File;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "input file is null. cannot set read only file attribute"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual java/io/File/exists()Z
ifeq L1
aload 0
invokevirtual java/io/File/setReadOnly()Z
pop
L1:
return
.limit locals 1
.limit stack 3
.end method

.method public static setFileSystemMode(Ljava/io/File;)V
.throws net/lingala/zip4j/exception/ZipException
return
.limit locals 1
.limit stack 0
.end method
