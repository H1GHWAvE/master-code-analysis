.bytecode 50.0
.class public synchronized net/lingala/zip4j/util/ArchiveMaintainer
.super java/lang/Object
.inner class inner net/lingala/zip4j/util/ArchiveMaintainer$1
.inner class inner net/lingala/zip4j/util/ArchiveMaintainer$2

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$000(Lnet/lingala/zip4j/util/ArchiveMaintainer;Lnet/lingala/zip4j/model/ZipModel;Ljava/io/File;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
.throws net/lingala/zip4j/exception/ZipException
aload 0
aload 1
aload 2
aload 3
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/initMergeSplitZipFile(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/File;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
return
.limit locals 4
.limit stack 4
.end method

.method private calculateTotalWorkForMergeOp(Lnet/lingala/zip4j/model/ZipModel;)J
.throws net/lingala/zip4j/exception/ZipException
lconst_0
lstore 4
lload 4
lstore 6
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifeq L0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getNoOfThisDisk()I
istore 3
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
astore 9
iconst_0
istore 2
L1:
lload 4
lstore 6
iload 2
iload 3
if_icmpgt L0
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getNoOfThisDisk()I
ifne L2
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
astore 8
L3:
lload 4
new java/io/File
dup
aload 8
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileLengh(Ljava/io/File;)J
ladd
lstore 4
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
bipush 9
iflt L4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 9
iconst_0
aload 9
ldc "."
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".z"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iconst_1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
goto L3
L4:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 9
iconst_0
aload 9
ldc "."
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".z0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iconst_1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
goto L3
L0:
lload 6
lreturn
.limit locals 10
.limit stack 5
.end method

.method private calculateTotalWorkForRemoveOp(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;)J
.throws net/lingala/zip4j/exception/ZipException
new java/io/File
dup
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic net/lingala/zip4j/util/Zip4jUtil/getFileLengh(Ljava/io/File;)J
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getCompressedSize()J
lsub
lreturn
.limit locals 3
.limit stack 4
.end method

.method private copyFile(Ljava/io/RandomAccessFile;Ljava/io/OutputStream;JJLnet/lingala/zip4j/progress/ProgressMonitor;)V
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch java/io/IOException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
.catch java/io/IOException from L6 to L7 using L2
.catch java/lang/Exception from L6 to L7 using L3
.catch java/io/IOException from L8 to L9 using L2
.catch java/lang/Exception from L8 to L9 using L3
.catch java/io/IOException from L10 to L11 using L2
.catch java/lang/Exception from L10 to L11 using L3
.catch java/io/IOException from L12 to L13 using L2
.catch java/lang/Exception from L12 to L13 using L3
aload 1
ifnull L14
aload 2
ifnonnull L15
L14:
new net/lingala/zip4j/exception/ZipException
dup
ldc "input or output stream is null, cannot copy file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L15:
lload 3
lconst_0
lcmp
ifge L16
new net/lingala/zip4j/exception/ZipException
dup
ldc "starting offset is negative, cannot copy file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L16:
lload 5
lconst_0
lcmp
ifge L17
new net/lingala/zip4j/exception/ZipException
dup
ldc "end offset is negative, cannot copy file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L17:
lload 3
lload 5
lcmp
ifle L18
new net/lingala/zip4j/exception/ZipException
dup
ldc "start offset is greater than end offset, cannot copy file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L18:
lload 3
lload 5
lcmp
ifne L19
L20:
return
L19:
aload 7
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/isCancelAllTasks()Z
ifeq L0
aload 7
iconst_3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setResult(I)V
aload 7
iconst_0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
return
L0:
aload 1
lload 3
invokevirtual java/io/RandomAccessFile/seek(J)V
L1:
lconst_0
lstore 9
lload 5
lload 3
lsub
lstore 11
lload 5
lload 3
lsub
ldc2_w 4096L
lcmp
ifge L10
L4:
lload 5
lload 3
lsub
l2i
newarray byte
astore 13
L5:
lload 9
lstore 3
L6:
aload 1
aload 13
invokevirtual java/io/RandomAccessFile/read([B)I
istore 8
L7:
iload 8
iconst_m1
if_icmpeq L20
L8:
aload 2
aload 13
iconst_0
iload 8
invokevirtual java/io/OutputStream/write([BII)V
aload 7
iload 8
i2l
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/updateWorkCompleted(J)V
aload 7
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/isCancelAllTasks()Z
ifeq L21
aload 7
iconst_3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setResult(I)V
L9:
return
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L10:
sipush 4096
newarray byte
astore 13
L11:
lload 9
lstore 3
goto L6
L21:
lload 3
iload 8
i2l
ladd
lstore 5
lload 5
lload 11
lcmp
ifeq L20
lload 5
lstore 3
L12:
aload 13
arraylength
i2l
lload 5
ladd
lload 11
lcmp
ifle L6
lload 11
lload 5
lsub
l2i
newarray byte
astore 13
L13:
lload 5
lstore 3
goto L6
L3:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 14
.limit stack 4
.end method

.method private createFileHandler(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Ljava/io/RandomAccessFile;
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/FileNotFoundException from L0 to L1 using L2
aload 1
ifnull L3
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokestatic net/lingala/zip4j/util/Zip4jUtil/isStringNotNullAndNotEmpty(Ljava/lang/String;)Z
ifne L0
L3:
new net/lingala/zip4j/exception/ZipException
dup
ldc "input parameter is null in getFilePointer, cannot create file handler to remove file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/io/RandomAccessFile
dup
new java/io/File
dup
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 2
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 1
L1:
aload 1
areturn
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 5
.end method

.method private createSplitZipFileHandler(Lnet/lingala/zip4j/model/ZipModel;I)Ljava/io/RandomAccessFile;
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch java/io/FileNotFoundException from L1 to L2 using L2
.catch java/lang/Exception from L1 to L2 using L3
.catch java/io/FileNotFoundException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
.catch java/io/FileNotFoundException from L6 to L7 using L2
.catch java/lang/Exception from L6 to L7 using L3
.catch java/io/FileNotFoundException from L8 to L9 using L2
.catch java/lang/Exception from L8 to L9 using L3
aload 1
ifnonnull L10
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip model is null, cannot create split file handler"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L10:
iload 2
ifge L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "invlaid part number, cannot create split file handler"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
astore 3
iload 2
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getNoOfThisDisk()I
if_icmpne L11
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
astore 1
L1:
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 3
aload 3
invokestatic net/lingala/zip4j/util/Zip4jUtil/checkFileExists(Ljava/io/File;)Z
ifne L8
new net/lingala/zip4j/exception/ZipException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "split file does not exist: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L11:
iload 2
bipush 9
if_icmplt L6
L4:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
iconst_0
aload 3
ldc "."
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".z"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L5:
goto L1
L6:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
iconst_0
aload 3
ldc "."
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".z0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L7:
goto L1
L8:
new java/io/RandomAccessFile
dup
aload 3
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 1
L9:
aload 1
areturn
L3:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 4
.limit stack 5
.end method

.method private initMergeSplitZipFile(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/File;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/IOException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch all from L0 to L1 using L4
.catch java/io/IOException from L5 to L2 using L2
.catch java/lang/Exception from L5 to L2 using L3
.catch all from L5 to L2 using L4
.catch all from L6 to L7 using L4
.catch all from L8 to L4 using L4
.catch java/io/IOException from L9 to L10 using L11
.catch java/io/IOException from L12 to L13 using L14
.catch java/io/IOException from L15 to L16 using L2
.catch java/lang/Exception from L15 to L16 using L3
.catch all from L15 to L16 using L4
.catch java/io/IOException from L17 to L18 using L2
.catch java/lang/Exception from L17 to L18 using L3
.catch all from L17 to L18 using L4
.catch java/io/IOException from L19 to L20 using L2
.catch java/lang/Exception from L19 to L20 using L3
.catch all from L19 to L20 using L4
.catch java/io/IOException from L21 to L22 using L2
.catch java/lang/Exception from L21 to L22 using L3
.catch all from L21 to L22 using L4
.catch java/io/IOException from L23 to L24 using L2
.catch java/lang/Exception from L23 to L24 using L3
.catch all from L23 to L24 using L4
.catch java/io/IOException from L25 to L26 using L2
.catch java/lang/Exception from L25 to L26 using L3
.catch all from L25 to L26 using L4
.catch java/io/IOException from L27 to L28 using L2
.catch java/lang/Exception from L27 to L28 using L3
.catch all from L27 to L28 using L4
.catch java/io/IOException from L29 to L30 using L2
.catch java/lang/Exception from L29 to L30 using L3
.catch all from L29 to L30 using L4
.catch java/io/IOException from L31 to L32 using L2
.catch java/lang/Exception from L31 to L32 using L3
.catch all from L31 to L32 using L4
.catch java/io/IOException from L33 to L34 using L2
.catch java/lang/Exception from L33 to L34 using L3
.catch all from L33 to L34 using L4
.catch java/io/IOException from L35 to L36 using L2
.catch java/lang/Exception from L35 to L36 using L3
.catch all from L35 to L36 using L4
.catch java/io/IOException from L37 to L38 using L2
.catch java/lang/Exception from L37 to L38 using L3
.catch all from L37 to L38 using L4
.catch java/io/IOException from L39 to L40 using L2
.catch java/lang/Exception from L39 to L40 using L3
.catch all from L39 to L40 using L4
.catch java/io/IOException from L41 to L42 using L2
.catch java/lang/Exception from L41 to L42 using L3
.catch all from L41 to L42 using L4
.catch java/io/IOException from L43 to L44 using L2
.catch java/lang/Exception from L43 to L44 using L3
.catch all from L43 to L44 using L4
.catch java/io/IOException from L45 to L46 using L2
.catch java/lang/Exception from L45 to L46 using L3
.catch all from L45 to L46 using L4
.catch java/io/IOException from L47 to L48 using L49
.catch java/io/IOException from L50 to L51 using L52
.catch java/io/IOException from L53 to L54 using L2
.catch java/lang/Exception from L53 to L54 using L3
.catch all from L53 to L54 using L4
.catch java/io/IOException from L55 to L56 using L57
.catch java/lang/Exception from L55 to L56 using L3
.catch all from L55 to L56 using L4
.catch java/io/IOException from L58 to L59 using L2
.catch java/lang/Exception from L58 to L59 using L3
.catch all from L58 to L59 using L4
.catch java/io/IOException from L60 to L61 using L2
.catch java/lang/Exception from L60 to L61 using L3
.catch all from L60 to L61 using L4
.catch java/io/IOException from L62 to L63 using L2
.catch java/lang/Exception from L62 to L63 using L3
.catch all from L62 to L63 using L4
.catch java/io/IOException from L64 to L65 using L2
.catch java/lang/Exception from L64 to L65 using L3
.catch all from L64 to L65 using L4
.catch java/io/IOException from L66 to L67 using L2
.catch java/lang/Exception from L66 to L67 using L3
.catch all from L66 to L67 using L4
.catch java/io/IOException from L68 to L69 using L70
.catch java/io/IOException from L71 to L72 using L73
.catch all from L74 to L75 using L4
.catch all from L76 to L49 using L4
aload 1
ifnonnull L77
new net/lingala/zip4j/exception/ZipException
dup
ldc "one of the input parameters is null, cannot merge split zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
astore 1
aload 3
aload 1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorError(Ljava/lang/Throwable;)V
aload 1
athrow
L77:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifne L78
new net/lingala/zip4j/exception/ZipException
dup
ldc "archive not a split zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
astore 1
aload 3
aload 1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorError(Ljava/lang/Throwable;)V
aload 1
athrow
L78:
aconst_null
astore 21
aconst_null
astore 22
aconst_null
astore 18
aconst_null
astore 23
aconst_null
astore 19
aconst_null
astore 24
aconst_null
astore 20
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 25
lconst_0
lstore 8
iconst_0
istore 10
aload 20
astore 14
aload 18
astore 16
aload 23
astore 12
aload 21
astore 13
aload 24
astore 15
aload 22
astore 17
L0:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getNoOfThisDisk()I
istore 7
L1:
iload 7
ifgt L79
aload 20
astore 14
aload 18
astore 16
aload 23
astore 12
aload 21
astore 13
aload 24
astore 15
aload 22
astore 17
L5:
new net/lingala/zip4j/exception/ZipException
dup
ldc "corrupt zip model, archive not a split zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
aload 14
astore 12
aload 16
astore 13
L6:
aload 3
aload 1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorError(Ljava/lang/Throwable;)V
L7:
aload 14
astore 12
aload 16
astore 13
L8:
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L4:
astore 1
aload 13
ifnull L10
L9:
aload 13
invokevirtual java/io/OutputStream/close()V
L10:
aload 12
ifnull L13
L12:
aload 12
invokevirtual java/io/RandomAccessFile/close()V
L13:
aload 1
athrow
L79:
aload 20
astore 14
aload 18
astore 16
aload 23
astore 12
aload 21
astore 13
aload 24
astore 15
aload 22
astore 17
L15:
aload 0
aload 2
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/prepareOutputStreamForMerge(Ljava/io/File;)Ljava/io/OutputStream;
astore 18
L16:
iconst_0
istore 4
aload 19
astore 2
L80:
iload 4
iload 7
if_icmpgt L81
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L17:
aload 0
aload 1
iload 4
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/createSplitZipFileHandler(Lnet/lingala/zip4j/model/ZipModel;I)Ljava/io/RandomAccessFile;
astore 2
L18:
iconst_0
istore 6
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L19:
new java/lang/Long
dup
aload 2
invokevirtual java/io/RandomAccessFile/length()J
invokespecial java/lang/Long/<init>(J)V
astore 19
L20:
iload 10
istore 11
iload 6
istore 5
iload 4
ifne L82
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
iload 10
istore 11
iload 6
istore 5
aload 2
astore 15
aload 18
astore 17
L21:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnull L82
L22:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
iload 10
istore 11
iload 6
istore 5
aload 2
astore 15
aload 18
astore 17
L23:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
ifnull L82
L24:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
iload 10
istore 11
iload 6
istore 5
aload 2
astore 15
aload 18
astore 17
L25:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L82
L26:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L27:
iconst_4
newarray byte
astore 20
L28:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L29:
aload 2
lconst_0
invokevirtual java/io/RandomAccessFile/seek(J)V
L30:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L31:
aload 2
aload 20
invokevirtual java/io/RandomAccessFile/read([B)I
pop
L32:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
iload 10
istore 11
iload 6
istore 5
aload 2
astore 15
aload 18
astore 17
L33:
aload 20
iconst_0
invokestatic net/lingala/zip4j/util/Raw/readIntLittleEndian([BI)I
i2l
ldc2_w 134695760L
lcmp
ifne L82
L34:
iconst_4
istore 5
iconst_1
istore 11
L82:
iload 4
iload 7
if_icmpne L36
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L35:
new java/lang/Long
dup
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getOffsetOfStartOfCentralDir()J
invokespecial java/lang/Long/<init>(J)V
astore 19
L36:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L37:
aload 0
aload 2
aload 18
iload 5
i2l
aload 19
invokevirtual java/lang/Long/longValue()J
aload 3
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/copyFile(Ljava/io/RandomAccessFile;Ljava/io/OutputStream;JJLnet/lingala/zip4j/progress/ProgressMonitor;)V
L38:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L39:
lload 8
aload 19
invokevirtual java/lang/Long/longValue()J
iload 5
i2l
lsub
ladd
lstore 8
L40:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L41:
aload 3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/isCancelAllTasks()Z
ifeq L83
L42:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L43:
aload 3
iconst_3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setResult(I)V
L44:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L45:
aload 3
iconst_0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
L46:
aload 18
ifnull L48
L47:
aload 18
invokevirtual java/io/OutputStream/close()V
L48:
aload 2
ifnull L51
L50:
aload 2
invokevirtual java/io/RandomAccessFile/close()V
L51:
return
L83:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L53:
aload 25
aload 19
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L54:
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L55:
aload 2
invokevirtual java/io/RandomAccessFile/close()V
L56:
iload 4
iconst_1
iadd
istore 4
iload 11
istore 10
goto L80
L81:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L58:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/clone()Ljava/lang/Object;
checkcast net/lingala/zip4j/model/ZipModel
astore 1
L59:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L60:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
lload 8
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setOffsetOfStartOfCentralDir(J)V
L61:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L62:
aload 0
aload 1
aload 25
iload 10
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/updateSplitZipModel(Lnet/lingala/zip4j/model/ZipModel;Ljava/util/ArrayList;Z)V
L63:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L64:
new net/lingala/zip4j/core/HeaderWriter
dup
invokespecial net/lingala/zip4j/core/HeaderWriter/<init>()V
aload 1
aload 18
invokevirtual net/lingala/zip4j/core/HeaderWriter/finalizeZipFileWithoutValidations(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;)V
L65:
aload 2
astore 14
aload 18
astore 16
aload 2
astore 12
aload 18
astore 13
aload 2
astore 15
aload 18
astore 17
L66:
aload 3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorSuccess()V
L67:
aload 18
ifnull L69
L68:
aload 18
invokevirtual java/io/OutputStream/close()V
L69:
aload 2
ifnull L51
L71:
aload 2
invokevirtual java/io/RandomAccessFile/close()V
L72:
return
L73:
astore 1
return
L3:
astore 1
aload 15
astore 12
aload 17
astore 13
L74:
aload 3
aload 1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorError(Ljava/lang/Throwable;)V
L75:
aload 15
astore 12
aload 17
astore 13
L76:
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L49:
astore 1
goto L48
L52:
astore 1
return
L57:
astore 12
goto L56
L70:
astore 1
goto L69
L11:
astore 2
goto L10
L14:
astore 2
goto L13
.limit locals 26
.limit stack 8
.end method

.method private prepareOutputStreamForMerge(Ljava/io/File;)Ljava/io/OutputStream;
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "outFile is null, cannot create outputstream"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 1
L1:
aload 1
areturn
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method private restoreFileName(Ljava/io/File;Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
invokevirtual java/io/File/delete()Z
ifeq L0
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/io/File/renameTo(Ljava/io/File;)Z
ifne L1
new net/lingala/zip4j/exception/ZipException
dup
ldc "cannot rename modified zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
new net/lingala/zip4j/exception/ZipException
dup
ldc "cannot delete old zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
return
.limit locals 3
.limit stack 3
.end method

.method private updateSplitEndCentralDirectory(Lnet/lingala/zip4j/model/ZipModel;)V
.throws net/lingala/zip4j/exception/ZipException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L1
.catch java/lang/Exception from L0 to L1 using L2
.catch net/lingala/zip4j/exception/ZipException from L3 to L2 using L1
.catch java/lang/Exception from L3 to L2 using L2
.catch net/lingala/zip4j/exception/ZipException from L4 to L5 using L1
.catch java/lang/Exception from L4 to L5 using L2
aload 1
ifnonnull L3
L0:
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip model is null - cannot update end of central directory for split zip model"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 1
aload 1
athrow
L3:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnonnull L4
new net/lingala/zip4j/exception/ZipException
dup
ldc "corrupt zip model - getCentralDirectory, cannot update split zip model"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L4:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
iconst_0
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setNoOfThisDisk(I)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
iconst_0
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setNoOfThisDiskStartOfCentralDir(I)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setTotNoOfEntriesInCentralDir(I)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setTotNoOfEntriesInCentralDirOnThisDisk(I)V
L5:
return
.limit locals 2
.limit stack 3
.end method

.method private updateSplitFileHeader(Lnet/lingala/zip4j/model/ZipModel;Ljava/util/ArrayList;Z)V
.throws net/lingala/zip4j/exception/ZipException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L1
.catch java/lang/Exception from L0 to L1 using L2
.catch net/lingala/zip4j/exception/ZipException from L3 to L4 using L1
.catch java/lang/Exception from L3 to L4 using L2
.catch net/lingala/zip4j/exception/ZipException from L5 to L6 using L1
.catch java/lang/Exception from L5 to L6 using L2
.catch net/lingala/zip4j/exception/ZipException from L7 to L8 using L1
.catch java/lang/Exception from L7 to L8 using L2
L0:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
ifnonnull L3
new net/lingala/zip4j/exception/ZipException
dup
ldc "corrupt zip model - getCentralDirectory, cannot update split zip model"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 1
aload 1
athrow
L3:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 7
L4:
iconst_0
istore 4
iload 3
ifeq L9
iconst_4
istore 4
goto L9
L5:
iload 6
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
invokevirtual net/lingala/zip4j/model/FileHeader/getDiskNumberStart()I
if_icmpge L7
lload 8
aload 2
iload 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
ladd
lstore 8
L6:
iload 6
iconst_1
iadd
istore 6
goto L5
L7:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
invokevirtual net/lingala/zip4j/model/FileHeader/getOffsetLocalHeader()J
lload 8
ladd
iload 4
i2l
lsub
invokevirtual net/lingala/zip4j/model/FileHeader/setOffsetLocalHeader(J)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
iconst_0
invokevirtual net/lingala/zip4j/model/FileHeader/setDiskNumberStart(I)V
L8:
iload 5
iconst_1
iadd
istore 5
goto L10
L2:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L11:
return
L9:
iconst_0
istore 5
L10:
iload 5
iload 7
if_icmpge L11
lconst_0
lstore 8
iconst_0
istore 6
goto L5
.limit locals 10
.limit stack 5
.end method

.method private updateSplitZip64EndCentralDirLocator(Lnet/lingala/zip4j/model/ZipModel;Ljava/util/ArrayList;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip model is null, cannot update split Zip64 end of central directory locator"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
ifnonnull L1
return
L1:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
iconst_0
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setNoOfDiskStartOfZip64EndOfCentralDirRec(I)V
lconst_0
lstore 4
iconst_0
istore 3
L2:
iload 3
aload 2
invokevirtual java/util/ArrayList/size()I
if_icmpge L3
lload 4
aload 2
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
ladd
lstore 4
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/getOffsetZip64EndOfCentralDirRec()J
lload 4
ladd
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setOffsetZip64EndOfCentralDirRec(J)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirLocator()Lnet/lingala/zip4j/model/Zip64EndCentralDirLocator;
iconst_1
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirLocator/setTotNumberOfDiscs(I)V
return
.limit locals 6
.limit stack 5
.end method

.method private updateSplitZip64EndCentralDirRec(Lnet/lingala/zip4j/model/ZipModel;Ljava/util/ArrayList;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip model is null, cannot update split Zip64 end of central directory record"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
ifnonnull L1
return
L1:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
iconst_0
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setNoOfThisDisk(I)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
iconst_0
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setNoOfThisDiskStartOfCentralDir(I)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getTotNoOfEntriesInCentralDir()I
i2l
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setTotNoOfEntriesInCentralDirOnThisDisk(J)V
lconst_0
lstore 4
iconst_0
istore 3
L2:
iload 3
aload 2
invokevirtual java/util/ArrayList/size()I
if_icmpge L3
lload 4
aload 2
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
ladd
lstore 4
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/getOffsetStartCenDirWRTStartDiskNo()J
lload 4
ladd
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/setOffsetStartCenDirWRTStartDiskNo(J)V
return
.limit locals 6
.limit stack 5
.end method

.method private updateSplitZipModel(Lnet/lingala/zip4j/model/ZipModel;Ljava/util/ArrayList;Z)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip model is null, cannot update split zip model"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
iconst_0
invokevirtual net/lingala/zip4j/model/ZipModel/setSplitArchive(Z)V
aload 0
aload 1
aload 2
iload 3
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/updateSplitFileHeader(Lnet/lingala/zip4j/model/ZipModel;Ljava/util/ArrayList;Z)V
aload 0
aload 1
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/updateSplitEndCentralDirectory(Lnet/lingala/zip4j/model/ZipModel;)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/isZip64Format()Z
ifeq L1
aload 0
aload 1
aload 2
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/updateSplitZip64EndCentralDirLocator(Lnet/lingala/zip4j/model/ZipModel;Ljava/util/ArrayList;)V
aload 0
aload 1
aload 2
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/updateSplitZip64EndCentralDirRec(Lnet/lingala/zip4j/model/ZipModel;Ljava/util/ArrayList;)V
L1:
return
.limit locals 4
.limit stack 4
.end method

.method public initProgressMonitorForMergeOp(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnonnull L0
new net/lingala/zip4j/exception/ZipException
dup
ldc "zip model is null, cannot calculate total work for merge op"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
iconst_4
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setCurrentOperation(I)V
aload 2
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setFileName(Ljava/lang/String;)V
aload 2
aload 0
aload 1
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/calculateTotalWorkForMergeOp(Lnet/lingala/zip4j/model/ZipModel;)J
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setTotalWork(J)V
aload 2
iconst_1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
return
.limit locals 3
.limit stack 3
.end method

.method public initProgressMonitorForRemoveOp(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
.throws net/lingala/zip4j/exception/ZipException
aload 1
ifnull L0
aload 2
ifnull L0
aload 3
ifnonnull L1
L0:
new net/lingala/zip4j/exception/ZipException
dup
ldc "one of the input parameters is null, cannot calculate total work"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 3
iconst_2
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setCurrentOperation(I)V
aload 3
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setFileName(Ljava/lang/String;)V
aload 3
aload 0
aload 1
aload 2
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/calculateTotalWorkForRemoveOp(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;)J
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setTotalWork(J)V
aload 3
iconst_1
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
return
.limit locals 4
.limit stack 4
.end method

.method public initRemoveZipFile(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;Lnet/lingala/zip4j/progress/ProgressMonitor;)Ljava/util/HashMap;
.throws net/lingala/zip4j/exception/ZipException
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch all from L0 to L1 using L4
.catch net/lingala/zip4j/exception/ZipException from L5 to L2 using L2
.catch java/lang/Exception from L5 to L2 using L3
.catch all from L5 to L2 using L4
.catch all from L6 to L7 using L8
.catch all from L9 to L8 using L8
.catch java/io/IOException from L10 to L11 using L12
.catch java/io/IOException from L13 to L14 using L12
.catch net/lingala/zip4j/exception/ZipException from L15 to L16 using L2
.catch java/lang/Exception from L15 to L16 using L3
.catch all from L15 to L16 using L4
.catch net/lingala/zip4j/exception/ZipException from L17 to L3 using L2
.catch java/lang/Exception from L17 to L3 using L3
.catch all from L17 to L3 using L4
.catch all from L18 to L19 using L8
.catch all from L20 to L21 using L8
.catch net/lingala/zip4j/exception/ZipException from L22 to L23 using L2
.catch java/lang/Exception from L22 to L23 using L3
.catch all from L22 to L23 using L4
.catch net/lingala/zip4j/exception/ZipException from L24 to L25 using L2
.catch java/lang/Exception from L24 to L25 using L3
.catch all from L24 to L25 using L4
.catch net/lingala/zip4j/exception/ZipException from L26 to L27 using L2
.catch java/lang/Exception from L26 to L27 using L3
.catch all from L26 to L27 using L4
.catch net/lingala/zip4j/exception/ZipException from L28 to L29 using L2
.catch java/lang/Exception from L28 to L29 using L3
.catch all from L28 to L29 using L4
.catch net/lingala/zip4j/exception/ZipException from L30 to L31 using L2
.catch java/lang/Exception from L30 to L31 using L3
.catch all from L30 to L31 using L4
.catch net/lingala/zip4j/exception/ZipException from L32 to L33 using L2
.catch java/lang/Exception from L32 to L33 using L3
.catch all from L32 to L33 using L4
.catch net/lingala/zip4j/exception/ZipException from L34 to L35 using L2
.catch java/lang/Exception from L34 to L35 using L3
.catch all from L34 to L35 using L4
.catch java/io/FileNotFoundException from L36 to L37 using L38
.catch net/lingala/zip4j/exception/ZipException from L36 to L37 using L2
.catch java/lang/Exception from L36 to L37 using L3
.catch all from L36 to L37 using L4
.catch net/lingala/zip4j/exception/ZipException from L39 to L40 using L41
.catch java/lang/Exception from L39 to L40 using L42
.catch all from L39 to L40 using L8
.catch net/lingala/zip4j/exception/ZipException from L43 to L44 using L45
.catch java/lang/Exception from L43 to L44 using L46
.catch all from L43 to L44 using L47
.catch net/lingala/zip4j/exception/ZipException from L48 to L49 using L45
.catch java/lang/Exception from L48 to L49 using L46
.catch all from L48 to L49 using L47
.catch net/lingala/zip4j/exception/ZipException from L50 to L45 using L45
.catch java/lang/Exception from L50 to L45 using L46
.catch all from L50 to L45 using L47
.catch net/lingala/zip4j/exception/ZipException from L51 to L4 using L2
.catch java/lang/Exception from L51 to L4 using L3
.catch all from L51 to L4 using L4
.catch net/lingala/zip4j/exception/ZipException from L52 to L53 using L45
.catch java/lang/Exception from L52 to L53 using L46
.catch all from L52 to L53 using L47
.catch net/lingala/zip4j/exception/ZipException from L54 to L55 using L45
.catch java/lang/Exception from L54 to L55 using L46
.catch all from L54 to L55 using L47
.catch net/lingala/zip4j/exception/ZipException from L56 to L57 using L45
.catch java/lang/Exception from L56 to L57 using L46
.catch all from L56 to L57 using L47
.catch net/lingala/zip4j/exception/ZipException from L58 to L59 using L45
.catch java/lang/Exception from L58 to L59 using L46
.catch all from L58 to L59 using L47
.catch net/lingala/zip4j/exception/ZipException from L60 to L61 using L45
.catch java/lang/Exception from L60 to L61 using L46
.catch all from L60 to L61 using L47
.catch net/lingala/zip4j/exception/ZipException from L62 to L63 using L45
.catch java/lang/Exception from L62 to L63 using L46
.catch all from L62 to L63 using L47
.catch net/lingala/zip4j/exception/ZipException from L64 to L65 using L45
.catch java/lang/Exception from L64 to L65 using L46
.catch all from L64 to L65 using L47
.catch net/lingala/zip4j/exception/ZipException from L66 to L67 using L45
.catch java/lang/Exception from L66 to L67 using L46
.catch all from L66 to L67 using L47
.catch net/lingala/zip4j/exception/ZipException from L68 to L69 using L45
.catch java/lang/Exception from L68 to L69 using L46
.catch all from L68 to L69 using L47
.catch net/lingala/zip4j/exception/ZipException from L70 to L71 using L45
.catch java/lang/Exception from L70 to L71 using L46
.catch all from L70 to L71 using L47
.catch net/lingala/zip4j/exception/ZipException from L72 to L73 using L45
.catch java/lang/Exception from L72 to L73 using L46
.catch all from L72 to L73 using L47
.catch net/lingala/zip4j/exception/ZipException from L74 to L75 using L45
.catch java/lang/Exception from L74 to L75 using L46
.catch all from L74 to L75 using L47
.catch net/lingala/zip4j/exception/ZipException from L76 to L77 using L45
.catch java/lang/Exception from L76 to L77 using L46
.catch all from L76 to L77 using L47
.catch net/lingala/zip4j/exception/ZipException from L78 to L79 using L45
.catch java/lang/Exception from L78 to L79 using L46
.catch all from L78 to L79 using L47
.catch net/lingala/zip4j/exception/ZipException from L80 to L81 using L45
.catch java/lang/Exception from L80 to L81 using L46
.catch all from L80 to L81 using L47
.catch net/lingala/zip4j/exception/ZipException from L82 to L83 using L45
.catch java/lang/Exception from L82 to L83 using L46
.catch all from L82 to L83 using L47
.catch net/lingala/zip4j/exception/ZipException from L84 to L85 using L45
.catch java/lang/Exception from L84 to L85 using L46
.catch all from L84 to L85 using L47
.catch net/lingala/zip4j/exception/ZipException from L86 to L87 using L45
.catch java/lang/Exception from L86 to L87 using L46
.catch all from L86 to L87 using L47
.catch net/lingala/zip4j/exception/ZipException from L88 to L89 using L45
.catch java/lang/Exception from L88 to L89 using L46
.catch all from L88 to L89 using L47
.catch net/lingala/zip4j/exception/ZipException from L90 to L91 using L45
.catch java/lang/Exception from L90 to L91 using L46
.catch all from L90 to L91 using L47
.catch net/lingala/zip4j/exception/ZipException from L92 to L93 using L45
.catch java/lang/Exception from L92 to L93 using L46
.catch all from L92 to L93 using L47
.catch java/io/IOException from L94 to L95 using L96
.catch java/io/IOException from L97 to L98 using L96
.catch net/lingala/zip4j/exception/ZipException from L99 to L100 using L45
.catch java/lang/Exception from L99 to L100 using L46
.catch all from L99 to L100 using L47
.catch net/lingala/zip4j/exception/ZipException from L101 to L102 using L45
.catch java/lang/Exception from L101 to L102 using L46
.catch all from L101 to L102 using L47
.catch net/lingala/zip4j/exception/ZipException from L103 to L104 using L45
.catch java/lang/Exception from L103 to L104 using L46
.catch all from L103 to L104 using L47
.catch net/lingala/zip4j/exception/ZipException from L105 to L106 using L45
.catch java/lang/Exception from L105 to L106 using L46
.catch all from L105 to L106 using L47
.catch net/lingala/zip4j/exception/ZipException from L107 to L108 using L45
.catch java/lang/Exception from L107 to L108 using L46
.catch all from L107 to L108 using L47
.catch net/lingala/zip4j/exception/ZipException from L109 to L110 using L45
.catch java/lang/Exception from L109 to L110 using L46
.catch all from L109 to L110 using L47
.catch net/lingala/zip4j/exception/ZipException from L111 to L112 using L45
.catch java/lang/Exception from L111 to L112 using L46
.catch all from L111 to L112 using L47
.catch net/lingala/zip4j/exception/ZipException from L113 to L114 using L45
.catch java/lang/Exception from L113 to L114 using L46
.catch all from L113 to L114 using L47
.catch net/lingala/zip4j/exception/ZipException from L115 to L116 using L45
.catch java/lang/Exception from L115 to L116 using L46
.catch all from L115 to L116 using L47
.catch net/lingala/zip4j/exception/ZipException from L117 to L118 using L45
.catch java/lang/Exception from L117 to L118 using L46
.catch all from L117 to L118 using L47
.catch net/lingala/zip4j/exception/ZipException from L119 to L120 using L45
.catch java/lang/Exception from L119 to L120 using L46
.catch all from L119 to L120 using L47
.catch net/lingala/zip4j/exception/ZipException from L121 to L122 using L45
.catch java/lang/Exception from L121 to L122 using L46
.catch all from L121 to L122 using L47
.catch net/lingala/zip4j/exception/ZipException from L123 to L124 using L45
.catch java/lang/Exception from L123 to L124 using L46
.catch all from L123 to L124 using L47
.catch net/lingala/zip4j/exception/ZipException from L125 to L126 using L45
.catch java/lang/Exception from L125 to L126 using L46
.catch all from L125 to L126 using L47
.catch net/lingala/zip4j/exception/ZipException from L127 to L128 using L45
.catch java/lang/Exception from L127 to L128 using L46
.catch all from L127 to L128 using L47
.catch net/lingala/zip4j/exception/ZipException from L129 to L130 using L45
.catch java/lang/Exception from L129 to L130 using L46
.catch all from L129 to L130 using L47
.catch java/io/IOException from L131 to L132 using L133
.catch java/io/IOException from L134 to L135 using L133
aload 2
ifnull L136
aload 1
ifnonnull L137
L136:
new net/lingala/zip4j/exception/ZipException
dup
ldc "input parameters is null in maintain zip file, cannot remove file from archive"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L137:
aconst_null
astore 34
aconst_null
astore 25
aconst_null
astore 29
aconst_null
astore 27
aconst_null
astore 35
aconst_null
astore 26
aconst_null
astore 30
aconst_null
astore 33
aconst_null
astore 32
aconst_null
astore 31
aconst_null
astore 28
iconst_0
istore 4
iconst_0
istore 11
iconst_0
istore 6
iconst_0
istore 8
iconst_0
istore 9
iconst_0
istore 10
iconst_0
istore 5
aconst_null
astore 24
aconst_null
astore 36
aconst_null
astore 20
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 37
aload 20
astore 22
aload 24
astore 23
aload 36
astore 21
L0:
aload 1
aload 2
invokestatic net/lingala/zip4j/util/Zip4jUtil/getIndexOfFileHeader(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;)I
istore 7
L1:
iload 7
ifge L138
aload 20
astore 22
aload 24
astore 23
aload 36
astore 21
L5:
new net/lingala/zip4j/exception/ZipException
dup
ldc "file header not found in zip model, cannot remove file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 2
aconst_null
astore 24
aload 27
astore 26
aload 22
astore 20
aload 28
astore 1
L139:
aload 1
astore 21
aload 24
astore 22
iload 5
istore 4
aload 20
astore 23
aload 26
astore 25
L6:
aload 3
aload 2
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorError(Ljava/lang/Throwable;)V
L7:
aload 1
astore 21
aload 24
astore 22
iload 5
istore 4
aload 20
astore 23
aload 26
astore 25
L9:
aload 2
athrow
L8:
astore 1
aload 23
astore 20
aload 21
astore 23
L140:
aload 23
ifnull L11
L10:
aload 23
invokevirtual java/io/RandomAccessFile/close()V
L11:
aload 22
ifnull L14
L13:
aload 22
invokevirtual java/io/OutputStream/close()V
L14:
iload 4
ifeq L141
aload 0
aload 25
aload 20
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/restoreFileName(Ljava/io/File;Ljava/lang/String;)V
L142:
aload 1
athrow
L138:
aload 20
astore 22
aload 24
astore 23
aload 36
astore 21
L15:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/isSplitArchive()Z
ifeq L21
L16:
aload 20
astore 22
aload 24
astore 23
aload 36
astore 21
L17:
new net/lingala/zip4j/exception/ZipException
dup
ldc "This is a split archive. Zip file format does not allow updating split/spanned files"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L3:
astore 2
aconst_null
astore 24
aload 29
astore 26
aload 23
astore 20
iload 6
istore 5
aload 30
astore 1
L143:
aload 1
astore 21
aload 24
astore 22
iload 5
istore 4
aload 20
astore 23
aload 26
astore 25
L18:
aload 3
aload 2
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorError(Ljava/lang/Throwable;)V
L19:
aload 1
astore 21
aload 24
astore 22
iload 5
istore 4
aload 20
astore 23
aload 26
astore 25
L20:
new net/lingala/zip4j/exception/ZipException
dup
aload 2
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L21:
aload 20
astore 22
aload 24
astore 23
aload 36
astore 21
L22:
invokestatic java/lang/System/currentTimeMillis()J
lstore 12
L23:
aload 20
astore 22
aload 24
astore 23
aload 36
astore 21
L24:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 12
ldc2_w 1000L
lrem
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 20
L25:
aload 20
astore 22
aload 20
astore 23
aload 20
astore 21
L26:
new java/io/File
dup
aload 20
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 24
L27:
aload 20
astore 22
aload 20
astore 23
aload 20
astore 21
L28:
aload 24
invokevirtual java/io/File/exists()Z
ifeq L144
L29:
aload 20
astore 22
aload 20
astore 23
aload 20
astore 21
L30:
invokestatic java/lang/System/currentTimeMillis()J
lstore 12
L31:
aload 20
astore 22
aload 20
astore 23
aload 20
astore 21
L32:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 12
ldc2_w 1000L
lrem
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 20
L33:
aload 20
astore 22
aload 20
astore 23
aload 20
astore 21
L34:
new java/io/File
dup
aload 20
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 24
L35:
goto L27
L144:
aload 20
astore 22
aload 20
astore 23
aload 20
astore 21
L36:
new net/lingala/zip4j/io/SplitOutputStream
dup
new java/io/File
dup
aload 20
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokespecial net/lingala/zip4j/io/SplitOutputStream/<init>(Ljava/io/File;)V
astore 24
L37:
aload 35
astore 21
aload 24
astore 22
aload 20
astore 23
aload 34
astore 25
L39:
new java/io/File
dup
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 26
L40:
aload 33
astore 21
iload 8
istore 4
aload 32
astore 22
iload 9
istore 5
aload 31
astore 23
iload 10
istore 6
L43:
aload 0
aload 1
ldc "r"
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/createFileHandler(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Ljava/io/RandomAccessFile;
astore 25
L44:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L48:
new net/lingala/zip4j/core/HeaderReader
dup
aload 25
invokespecial net/lingala/zip4j/core/HeaderReader/<init>(Ljava/io/RandomAccessFile;)V
aload 2
invokevirtual net/lingala/zip4j/core/HeaderReader/readLocalFileHeader(Lnet/lingala/zip4j/model/FileHeader;)Lnet/lingala/zip4j/model/LocalFileHeader;
ifnonnull L145
L49:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L50:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid local file header, cannot remove file from archive"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L45:
astore 2
aload 21
astore 1
iload 4
istore 5
goto L139
L38:
astore 1
aload 20
astore 22
aload 20
astore 23
aload 20
astore 21
L51:
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L4:
astore 1
aconst_null
astore 22
aload 26
astore 23
iload 11
istore 4
aload 21
astore 20
goto L140
L145:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L52:
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getOffsetLocalHeader()J
lstore 12
L53:
aload 25
astore 21
iload 8
istore 4
lload 12
lstore 14
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L54:
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
ifnull L59
L55:
aload 25
astore 21
iload 8
istore 4
lload 12
lstore 14
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L56:
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getOffsetLocalHeader()J
ldc2_w -1L
lcmp
ifeq L59
L57:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L58:
aload 2
invokevirtual net/lingala/zip4j/model/FileHeader/getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getOffsetLocalHeader()J
lstore 14
L59:
ldc2_w -1L
lstore 12
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L60:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getOffsetOfStartOfCentralDir()J
lstore 18
L61:
aload 25
astore 21
iload 8
istore 4
lload 18
lstore 16
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L62:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/isZip64Format()Z
ifeq L67
L63:
aload 25
astore 21
iload 8
istore 4
lload 18
lstore 16
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L64:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
ifnull L67
L65:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L66:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/getOffsetStartCenDirWRTStartDiskNo()J
lstore 16
L67:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L68:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
astore 2
L69:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L70:
iload 7
aload 2
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
if_icmpne L73
L71:
lload 16
lconst_1
lsub
lstore 12
goto L146
L147:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L72:
new net/lingala/zip4j/exception/ZipException
dup
ldc "invalid offset for start and end of local file, cannot remove file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L73:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L74:
aload 2
iload 7
iconst_1
iadd
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
astore 27
L75:
aload 27
ifnull L146
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L76:
aload 27
invokevirtual net/lingala/zip4j/model/FileHeader/getOffsetLocalHeader()J
lconst_1
lsub
lstore 18
L77:
aload 25
astore 21
iload 8
istore 4
lload 18
lstore 12
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L78:
aload 27
invokevirtual net/lingala/zip4j/model/FileHeader/getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
ifnull L146
L79:
aload 25
astore 21
iload 8
istore 4
lload 18
lstore 12
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L80:
aload 27
invokevirtual net/lingala/zip4j/model/FileHeader/getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getOffsetLocalHeader()J
ldc2_w -1L
lcmp
ifeq L146
L81:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L82:
aload 27
invokevirtual net/lingala/zip4j/model/FileHeader/getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getOffsetLocalHeader()J
lconst_1
lsub
lstore 12
L83:
goto L146
L148:
iload 7
ifne L149
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L84:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
if_icmple L87
L85:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L86:
aload 0
aload 25
aload 24
lconst_1
lload 12
ladd
lload 16
aload 3
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/copyFile(Ljava/io/RandomAccessFile;Ljava/io/OutputStream;JJLnet/lingala/zip4j/progress/ProgressMonitor;)V
L87:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L88:
aload 3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/isCancelAllTasks()Z
ifeq L150
L89:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L90:
aload 3
iconst_3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setResult(I)V
L91:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L92:
aload 3
iconst_0
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/setState(I)V
L93:
aload 25
ifnull L95
L94:
aload 25
invokevirtual java/io/RandomAccessFile/close()V
L95:
aload 24
ifnull L98
L97:
aload 24
invokevirtual java/io/OutputStream/close()V
L98:
iconst_0
ifeq L151
aload 0
aload 26
aload 20
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/restoreFileName(Ljava/io/File;Ljava/lang/String;)V
aconst_null
areturn
L149:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L99:
iload 7
aload 2
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
if_icmpne L152
L100:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L101:
aload 0
aload 25
aload 24
lconst_0
lload 14
aload 3
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/copyFile(Ljava/io/RandomAccessFile;Ljava/io/OutputStream;JJLnet/lingala/zip4j/progress/ProgressMonitor;)V
L102:
goto L87
L152:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L103:
aload 0
aload 25
aload 24
lconst_0
lload 14
aload 3
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/copyFile(Ljava/io/RandomAccessFile;Ljava/io/OutputStream;JJLnet/lingala/zip4j/progress/ProgressMonitor;)V
L104:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L105:
aload 0
aload 25
aload 24
lconst_1
lload 12
ladd
lload 16
aload 3
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/copyFile(Ljava/io/RandomAccessFile;Ljava/io/OutputStream;JJLnet/lingala/zip4j/progress/ProgressMonitor;)V
L106:
goto L87
L96:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
ldc "cannot close input stream or output stream when trying to delete a file from zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L151:
new java/io/File
dup
aload 20
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
aconst_null
areturn
L150:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L107:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
aload 24
checkcast net/lingala/zip4j/io/SplitOutputStream
invokevirtual net/lingala/zip4j/io/SplitOutputStream/getFilePointer()J
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setOffsetOfStartOfCentralDir(J)V
L108:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L109:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getTotNoOfEntriesInCentralDir()I
iconst_1
isub
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setTotNoOfEntriesInCentralDir(I)V
L110:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L111:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getTotNoOfEntriesInCentralDirOnThisDisk()I
iconst_1
isub
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setTotNoOfEntriesInCentralDirOnThisDisk(I)V
L112:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L113:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iload 7
invokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;
pop
L114:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L115:
iload 7
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L153
L116:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L117:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iload 7
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
invokevirtual net/lingala/zip4j/model/FileHeader/getOffsetLocalHeader()J
lstore 18
L118:
aload 25
astore 21
iload 8
istore 4
lload 18
lstore 16
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L119:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iload 7
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
invokevirtual net/lingala/zip4j/model/FileHeader/getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
ifnull L124
L120:
aload 25
astore 21
iload 8
istore 4
lload 18
lstore 16
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L121:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iload 7
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
invokevirtual net/lingala/zip4j/model/FileHeader/getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getOffsetLocalHeader()J
ldc2_w -1L
lcmp
ifeq L124
L122:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L123:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iload 7
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
invokevirtual net/lingala/zip4j/model/FileHeader/getZip64ExtendedInfo()Lnet/lingala/zip4j/model/Zip64ExtendedInfo;
invokevirtual net/lingala/zip4j/model/Zip64ExtendedInfo/getOffsetLocalHeader()J
lstore 16
L124:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L125:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;
invokevirtual net/lingala/zip4j/model/CentralDirectory/getFileHeaders()Ljava/util/ArrayList;
iload 7
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/lingala/zip4j/model/FileHeader
lload 16
lload 12
lload 14
lsub
lsub
lconst_1
lsub
invokevirtual net/lingala/zip4j/model/FileHeader/setOffsetLocalHeader(J)V
L126:
iload 7
iconst_1
iadd
istore 7
goto L114
L153:
aload 25
astore 21
iload 8
istore 4
aload 25
astore 22
iload 9
istore 5
aload 25
astore 23
iload 10
istore 6
L127:
new net/lingala/zip4j/core/HeaderWriter
dup
invokespecial net/lingala/zip4j/core/HeaderWriter/<init>()V
aload 1
aload 24
invokevirtual net/lingala/zip4j/core/HeaderWriter/finalizeZipFile(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;)V
L128:
iconst_1
istore 5
iconst_1
istore 6
iconst_1
istore 4
aload 25
astore 21
aload 25
astore 22
aload 25
astore 23
L129:
aload 37
ldc "offsetCentralDir"
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getOffsetOfStartOfCentralDir()J
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L130:
aload 25
ifnull L132
L131:
aload 25
invokevirtual java/io/RandomAccessFile/close()V
L132:
aload 24
ifnull L135
L134:
aload 24
invokevirtual java/io/OutputStream/close()V
L135:
iconst_1
ifeq L154
aload 0
aload 26
aload 20
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/restoreFileName(Ljava/io/File;Ljava/lang/String;)V
aload 37
areturn
L133:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
ldc "cannot close input stream or output stream when trying to delete a file from zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L154:
new java/io/File
dup
aload 20
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
aload 37
areturn
L12:
astore 1
new net/lingala/zip4j/exception/ZipException
dup
ldc "cannot close input stream or output stream when trying to delete a file from zip file"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L141:
new java/io/File
dup
aload 20
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
goto L142
L42:
astore 2
aload 30
astore 1
iload 6
istore 5
aload 29
astore 26
goto L143
L41:
astore 2
aload 28
astore 1
aload 27
astore 26
goto L139
L146:
lload 14
lconst_0
lcmp
iflt L147
lload 12
lconst_0
lcmp
ifge L148
goto L147
L46:
astore 2
aload 22
astore 1
goto L143
L47:
astore 1
aload 26
astore 25
aload 24
astore 22
iload 6
istore 4
goto L140
.limit locals 38
.limit stack 8
.end method

.method public mergeSplitZipFiles(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/File;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
.throws net/lingala/zip4j/exception/ZipException
iload 4
ifeq L0
new net/lingala/zip4j/util/ArchiveMaintainer$2
dup
aload 0
ldc "Zip4j"
aload 1
aload 2
aload 3
invokespecial net/lingala/zip4j/util/ArchiveMaintainer$2/<init>(Lnet/lingala/zip4j/util/ArchiveMaintainer;Ljava/lang/String;Lnet/lingala/zip4j/model/ZipModel;Ljava/io/File;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
invokevirtual java/lang/Thread/start()V
return
L0:
aload 0
aload 1
aload 2
aload 3
invokespecial net/lingala/zip4j/util/ArchiveMaintainer/initMergeSplitZipFile(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/File;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
return
.limit locals 5
.limit stack 7
.end method

.method public removeZipFile(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)Ljava/util/HashMap;
.throws net/lingala/zip4j/exception/ZipException
iload 4
ifeq L0
new net/lingala/zip4j/util/ArchiveMaintainer$1
dup
aload 0
ldc "Zip4j"
aload 1
aload 2
aload 3
invokespecial net/lingala/zip4j/util/ArchiveMaintainer$1/<init>(Lnet/lingala/zip4j/util/ArchiveMaintainer;Ljava/lang/String;Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
invokevirtual java/lang/Thread/start()V
aconst_null
areturn
L0:
aload 0
aload 1
aload 2
aload 3
invokevirtual net/lingala/zip4j/util/ArchiveMaintainer/initRemoveZipFile(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;Lnet/lingala/zip4j/progress/ProgressMonitor;)Ljava/util/HashMap;
astore 1
aload 3
invokevirtual net/lingala/zip4j/progress/ProgressMonitor/endProgressMonitorSuccess()V
aload 1
areturn
.limit locals 5
.limit stack 7
.end method

.method public setComment(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)V
.throws net/lingala/zip4j/exception/ZipException
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/io/UnsupportedEncodingException from L1 to L3 using L4
.catch java/io/FileNotFoundException from L5 to L6 using L7
.catch java/io/IOException from L5 to L6 using L8
.catch all from L5 to L6 using L9
.catch java/io/FileNotFoundException from L10 to L11 using L7
.catch java/io/IOException from L10 to L11 using L8
.catch all from L10 to L11 using L9
.catch java/io/FileNotFoundException from L11 to L12 using L13
.catch java/io/IOException from L11 to L12 using L14
.catch all from L11 to L12 using L15
.catch java/io/FileNotFoundException from L12 to L16 using L13
.catch java/io/IOException from L12 to L16 using L14
.catch all from L12 to L16 using L15
.catch java/io/IOException from L17 to L18 using L19
.catch java/io/FileNotFoundException from L20 to L21 using L13
.catch java/io/IOException from L20 to L21 using L14
.catch all from L20 to L21 using L15
.catch all from L22 to L9 using L9
.catch java/io/IOException from L23 to L24 using L25
.catch all from L26 to L19 using L9
aload 2
ifnonnull L27
new net/lingala/zip4j/exception/ZipException
dup
ldc "comment is null, cannot update Zip file with comment"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L27:
aload 1
ifnonnull L28
new net/lingala/zip4j/exception/ZipException
dup
ldc "zipModel is null, cannot update Zip file with comment"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L28:
aload 2
astore 5
aload 2
invokevirtual java/lang/String/getBytes()[B
astore 4
aload 2
invokevirtual java/lang/String/length()I
istore 3
ldc "windows-1254"
invokestatic net/lingala/zip4j/util/Zip4jUtil/isSupportedCharset(Ljava/lang/String;)Z
ifeq L3
L0:
new java/lang/String
dup
aload 2
ldc "windows-1254"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
ldc "windows-1254"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
astore 5
L1:
aload 5
ldc "windows-1254"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
astore 4
aload 5
invokevirtual java/lang/String/length()I
istore 3
L3:
iload 3
ldc_w 65535
if_icmple L29
new net/lingala/zip4j/exception/ZipException
dup
ldc "comment length exceeds maximum length"
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 4
L30:
aload 2
astore 5
aload 2
invokevirtual java/lang/String/getBytes()[B
astore 4
aload 2
invokevirtual java/lang/String/length()I
istore 3
goto L3
L29:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
aload 5
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setComment(Ljava/lang/String;)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
aload 4
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setCommentBytes([B)V
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
iload 3
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/setCommentLength(I)V
aconst_null
astore 4
aconst_null
astore 6
aconst_null
astore 5
aload 4
astore 2
L5:
new net/lingala/zip4j/core/HeaderWriter
dup
invokespecial net/lingala/zip4j/core/HeaderWriter/<init>()V
astore 7
L6:
aload 4
astore 2
L10:
new net/lingala/zip4j/io/SplitOutputStream
dup
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZipFile()Ljava/lang/String;
invokespecial net/lingala/zip4j/io/SplitOutputStream/<init>(Ljava/lang/String;)V
astore 4
L11:
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/isZip64Format()Z
ifeq L20
aload 4
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getZip64EndCentralDirRecord()Lnet/lingala/zip4j/model/Zip64EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/Zip64EndCentralDirRecord/getOffsetStartCenDirWRTStartDiskNo()J
invokevirtual net/lingala/zip4j/io/SplitOutputStream/seek(J)V
L12:
aload 7
aload 1
aload 4
invokevirtual net/lingala/zip4j/core/HeaderWriter/finalizeZipFileWithoutValidations(Lnet/lingala/zip4j/model/ZipModel;Ljava/io/OutputStream;)V
L16:
aload 4
ifnull L18
L17:
aload 4
invokevirtual net/lingala/zip4j/io/SplitOutputStream/close()V
L18:
return
L20:
aload 4
aload 1
invokevirtual net/lingala/zip4j/model/ZipModel/getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
invokevirtual net/lingala/zip4j/model/EndCentralDirRecord/getOffsetOfStartOfCentralDir()J
invokevirtual net/lingala/zip4j/io/SplitOutputStream/seek(J)V
L21:
goto L12
L13:
astore 1
aload 4
astore 2
L22:
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L9:
astore 1
L31:
aload 2
ifnull L24
L23:
aload 2
invokevirtual net/lingala/zip4j/io/SplitOutputStream/close()V
L24:
aload 1
athrow
L8:
astore 1
aload 6
astore 2
L26:
new net/lingala/zip4j/exception/ZipException
dup
aload 1
invokespecial net/lingala/zip4j/exception/ZipException/<init>(Ljava/lang/Throwable;)V
athrow
L19:
astore 1
return
L25:
astore 2
goto L24
L15:
astore 1
aload 4
astore 2
goto L31
L14:
astore 1
aload 4
astore 2
goto L26
L7:
astore 1
aload 5
astore 2
goto L22
L4:
astore 4
goto L30
.limit locals 8
.limit stack 4
.end method
