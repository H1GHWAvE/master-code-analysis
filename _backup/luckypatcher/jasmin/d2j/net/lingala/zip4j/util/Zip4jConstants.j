.bytecode 50.0
.class public abstract interface net/lingala/zip4j/util/Zip4jConstants
.super java/lang/Object

.field public static final 'AES_STRENGTH_128' I = 1


.field public static final 'AES_STRENGTH_192' I = 2


.field public static final 'AES_STRENGTH_256' I = 3


.field public static final 'COMP_AES_ENC' I = 99


.field public static final 'COMP_DEFLATE' I = 8


.field public static final 'COMP_STORE' I = 0


.field public static final 'DEFLATE_LEVEL_FAST' I = 3


.field public static final 'DEFLATE_LEVEL_FASTEST' I = 1


.field public static final 'DEFLATE_LEVEL_MAXIMUM' I = 7


.field public static final 'DEFLATE_LEVEL_NORMAL' I = 5


.field public static final 'DEFLATE_LEVEL_ULTRA' I = 9


.field public static final 'ENC_METHOD_AES' I = 99


.field public static final 'ENC_METHOD_STANDARD' I = 0


.field public static final 'ENC_NO_ENCRYPTION' I = -1

