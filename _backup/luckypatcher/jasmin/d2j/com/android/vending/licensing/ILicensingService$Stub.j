.bytecode 50.0
.class public synchronized abstract com/android/vending/licensing/ILicensingService$Stub
.super android/os/Binder
.implements com/android/vending/licensing/ILicensingService
.inner class public static abstract Stub inner com/android/vending/licensing/ILicensingService$Stub outer com/android/vending/licensing/ILicensingService
.inner class private static Proxy inner com/android/vending/licensing/ILicensingService$Stub$Proxy outer com/android/vending/licensing/ILicensingService$Stub

.field private static final 'DESCRIPTOR' Ljava/lang/String; = "com.android.vending.licensing.ILicensingService"

.field static final 'TRANSACTION_checkLicense' I = 1


.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.android.vending.licensing.ILicensingService"
invokevirtual com/android/vending/licensing/ILicensingService$Stub/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/vending/licensing/ILicensingService;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "com.android.vending.licensing.ILicensingService"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof com/android/vending/licensing/ILicensingService
ifeq L1
aload 1
checkcast com/android/vending/licensing/ILicensingService
areturn
L1:
new com/android/vending/licensing/ILicensingService$Stub$Proxy
dup
aload 0
invokespecial com/android/vending/licensing/ILicensingService$Stub$Proxy/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
1598968902 : L1
default : L2
L2:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L1:
aload 3
ldc "com.android.vending.licensing.ILicensingService"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.android.vending.licensing.ILicensingService"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readLong()J
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
invokestatic com/android/vending/licensing/ILicenseResultListener$Stub/asInterface(Landroid/os/IBinder;)Lcom/android/vending/licensing/ILicenseResultListener;
invokevirtual com/android/vending/licensing/ILicensingService$Stub/checkLicense(JLjava/lang/String;Lcom/android/vending/licensing/ILicenseResultListener;)V
iconst_1
ireturn
.limit locals 5
.limit stack 5
.end method
