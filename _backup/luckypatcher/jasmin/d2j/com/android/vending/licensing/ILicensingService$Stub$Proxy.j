.bytecode 50.0
.class synchronized com/android/vending/licensing/ILicensingService$Stub$Proxy
.super java/lang/Object
.implements com/android/vending/licensing/ILicensingService
.inner class public static abstract Stub inner com/android/vending/licensing/ILicensingService$Stub outer com/android/vending/licensing/ILicensingService
.inner class private static Proxy inner com/android/vending/licensing/ILicensingService$Stub$Proxy outer com/android/vending/licensing/ILicensingService$Stub

.field private 'mRemote' Landroid/os/IBinder;

.method <init>(Landroid/os/IBinder;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/android/vending/licensing/ILicensingService$Stub$Proxy/mRemote Landroid/os/IBinder;
return
.limit locals 2
.limit stack 2
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
getfield com/android/vending/licensing/ILicensingService$Stub$Proxy/mRemote Landroid/os/IBinder;
areturn
.limit locals 1
.limit stack 1
.end method

.method public checkLicense(JLjava/lang/String;Lcom/android/vending/licensing/ILicenseResultListener;)V
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
aconst_null
astore 5
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 6
L0:
aload 6
ldc "com.android.vending.licensing.ILicensingService"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 6
lload 1
invokevirtual android/os/Parcel/writeLong(J)V
aload 6
aload 3
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
L1:
aload 5
astore 3
aload 4
ifnull L4
L3:
aload 4
invokeinterface com/android/vending/licensing/ILicenseResultListener/asBinder()Landroid/os/IBinder; 0
astore 3
L4:
aload 6
aload 3
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
aload 0
getfield com/android/vending/licensing/ILicensingService$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_1
aload 6
aconst_null
iconst_1
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
L5:
aload 6
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 3
aload 6
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 7
.limit stack 5
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
ldc "com.android.vending.licensing.ILicensingService"
areturn
.limit locals 1
.limit stack 1
.end method
