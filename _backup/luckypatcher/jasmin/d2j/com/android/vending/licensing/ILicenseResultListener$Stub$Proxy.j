.bytecode 50.0
.class synchronized com/android/vending/licensing/ILicenseResultListener$Stub$Proxy
.super java/lang/Object
.implements com/android/vending/licensing/ILicenseResultListener
.inner class public static abstract Stub inner com/android/vending/licensing/ILicenseResultListener$Stub outer com/android/vending/licensing/ILicenseResultListener
.inner class private static Proxy inner com/android/vending/licensing/ILicenseResultListener$Stub$Proxy outer com/android/vending/licensing/ILicenseResultListener$Stub

.field private 'mRemote' Landroid/os/IBinder;

.method <init>(Landroid/os/IBinder;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/android/vending/licensing/ILicenseResultListener$Stub$Proxy/mRemote Landroid/os/IBinder;
return
.limit locals 2
.limit stack 2
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
getfield com/android/vending/licensing/ILicenseResultListener$Stub$Proxy/mRemote Landroid/os/IBinder;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
ldc "com.android.vending.licensing.ILicenseResultListener"
areturn
.limit locals 1
.limit stack 1
.end method

.method public verifyLicense(ILjava/lang/String;Ljava/lang/String;)V
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
L0:
aload 4
ldc "com.android.vending.licensing.ILicenseResultListener"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 4
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
aload 4
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 4
aload 3
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield com/android/vending/licensing/ILicenseResultListener$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_1
aload 4
aconst_null
iconst_1
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
L1:
aload 4
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 2
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 2
athrow
.limit locals 5
.limit stack 5
.end method
