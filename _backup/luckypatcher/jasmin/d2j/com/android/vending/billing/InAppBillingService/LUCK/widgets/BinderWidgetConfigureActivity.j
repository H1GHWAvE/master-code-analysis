.bytecode 50.0
.class public synchronized com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity
.super android/app/Activity
.inner class inner com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$1
.inner class inner com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$2

.field private static final 'PREFS_NAME' Ljava/lang/String; = "com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget"

.field private static final 'PREF_PREFIX_KEY' Ljava/lang/String; = "appwidget_"

.field public 'bindes' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;>;"

.field public 'context' Landroid/content/Context;

.field public 'lv' Landroid/widget/ListView;

.field 'mAppWidgetId' I

.field 'mAppWidgetText' Landroid/widget/EditText;

.field public 'sizeText' I

.method public <init>()V
aload 0
invokespecial android/app/Activity/<init>()V
aload 0
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/mAppWidgetId I
aload 0
aconst_null
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/bindes Ljava/util/ArrayList;
aload 0
aconst_null
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/lv Landroid/widget/ListView;
aload 0
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/sizeText I
return
.limit locals 1
.limit stack 2
.end method

.method static deleteTitlePref(Landroid/content/Context;I)V
aload 0
ldc "com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget"
iconst_4
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 0
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "appwidget_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
pop
aload 0
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
.limit locals 2
.limit stack 3
.end method

.method static loadTitlePref(Landroid/content/Context;I)Ljava/lang/String;
aload 0
ldc "com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget"
iconst_4
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "appwidget_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aconst_null
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 0
aload 0
ifnull L0
aload 0
areturn
L0:
ldc "NOT_SAVED_BIND"
areturn
.limit locals 2
.limit stack 3
.end method

.method static saveTitlePref(Landroid/content/Context;ILjava/lang/String;)V
aload 0
ldc "com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget"
iconst_4
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 0
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "appwidget_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
pop
aload 0
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
.limit locals 3
.limit stack 3
.end method

.method public onCreate(Landroid/os/Bundle;)V
.catch java/io/IOException from L0 to L1 using L2
aload 0
aload 1
invokespecial android/app/Activity/onCreate(Landroid/os/Bundle;)V
aload 0
iconst_0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/setResult(I)V
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
astore 1
aload 1
ifnull L3
aload 0
aload 1
ldc "appWidgetId"
iconst_0
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/mAppWidgetId I
L3:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/mAppWidgetId I
ifne L4
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/finish()V
return
L4:
aload 0
aload 0
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/context Landroid/content/Context;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
ldc "binder"
iconst_0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/getDir(Ljava/lang/String;I)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/bind.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifne L1
L0:
aload 1
invokevirtual java/io/File/createNewFile()Z
pop
L1:
aload 0
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/context Landroid/content/Context;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/BinderActivity/getBindes(Landroid/content/Context;)Ljava/util/ArrayList;
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/bindes Ljava/util/ArrayList;
aload 0
new android/widget/ListView
dup
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/context Landroid/content/Context;
invokespecial android/widget/ListView/<init>(Landroid/content/Context;)V
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/lv Landroid/widget/ListView;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/bindes Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L5
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/bindes Ljava/util/ArrayList;
new com/android/vending/billing/InAppBillingService/LUCK/BindItem
dup
ldc "empty"
ldc "empty"
invokespecial com/android/vending/billing/InAppBillingService/LUCK/BindItem/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L5:
new com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$1
dup
aload 0
aload 0
ldc_w 2130968605
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/bindes Ljava/util/ArrayList;
invokespecial com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$1/<init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;Landroid/content/Context;ILjava/util/List;)V
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/adaptBind Landroid/widget/ArrayAdapter;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/lv Landroid/widget/ListView;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/adaptBind Landroid/widget/ArrayAdapter;
invokevirtual android/widget/ListView/setAdapter(Landroid/widget/ListAdapter;)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/lv Landroid/widget/ListView;
invokevirtual android/widget/ListView/invalidateViews()V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/lv Landroid/widget/ListView;
ldc_w -16777216
invokevirtual android/widget/ListView/setBackgroundColor(I)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/bindes Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
if_icmpne L6
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/bindes Ljava/util/ArrayList;
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/BindItem
getfield com/android/vending/billing/InAppBillingService/LUCK/BindItem/TargetDir Ljava/lang/String;
ldc "empty"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L6
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/bindes Ljava/util/ArrayList;
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/BindItem
getfield com/android/vending/billing/InAppBillingService/LUCK/BindItem/SourceDir Ljava/lang/String;
ldc "empty"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L6
L7:
aload 0
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/lv Landroid/widget/ListView;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/setContentView(Landroid/view/View;)V
return
L2:
astore 1
aload 1
invokevirtual java/io/IOException/printStackTrace()V
goto L1
L6:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity/lv Landroid/widget/ListView;
new com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$2
dup
aload 0
invokespecial com/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity$2/<init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;)V
invokevirtual android/widget/ListView/setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
goto L7
.limit locals 2
.limit stack 6
.end method
