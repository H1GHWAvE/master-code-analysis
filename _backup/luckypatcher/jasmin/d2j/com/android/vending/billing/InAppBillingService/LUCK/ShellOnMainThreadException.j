.bytecode 50.0
.class public synchronized com/android/vending/billing/InAppBillingService/LUCK/ShellOnMainThreadException
.super java/lang/RuntimeException

.field public static final 'EXCEPTION_COMMAND' Ljava/lang/String; = "Application attempted to run a shell command from the main thread"

.field public static final 'EXCEPTION_NOT_IDLE' Ljava/lang/String; = "Application attempted to wait for a non-idle shell to close on the main thread"

.field public static final 'EXCEPTION_WAIT_IDLE' Ljava/lang/String; = "Application attempted to wait for a shell to become idle on the main thread"

.method public <init>(Ljava/lang/String;)V
aload 0
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 2
.end method
