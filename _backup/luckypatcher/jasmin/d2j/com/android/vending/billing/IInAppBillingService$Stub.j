.bytecode 50.0
.class public synchronized abstract com/android/vending/billing/IInAppBillingService$Stub
.super android/os/Binder
.implements com/android/vending/billing/IInAppBillingService
.inner class public static abstract Stub inner com/android/vending/billing/IInAppBillingService$Stub outer com/android/vending/billing/IInAppBillingService
.inner class private static Proxy inner com/android/vending/billing/IInAppBillingService$Stub$Proxy outer com/android/vending/billing/IInAppBillingService$Stub

.field private static final 'DESCRIPTOR' Ljava/lang/String; = "com.android.vending.billing.IInAppBillingService"

.field static final 'TRANSACTION_consumePurchase' I = 5


.field static final 'TRANSACTION_getBuyIntent' I = 3


.field static final 'TRANSACTION_getBuyIntentToReplaceSkus' I = 6


.field static final 'TRANSACTION_getPurchases' I = 4


.field static final 'TRANSACTION_getSkuDetails' I = 2


.field static final 'TRANSACTION_isBillingSupported' I = 1


.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.android.vending.billing.IInAppBillingService"
invokevirtual com/android/vending/billing/IInAppBillingService$Stub/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/vending/billing/IInAppBillingService;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "com.android.vending.billing.IInAppBillingService"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof com/android/vending/billing/IInAppBillingService
ifeq L1
aload 1
checkcast com/android/vending/billing/IInAppBillingService
areturn
L1:
new com/android/vending/billing/IInAppBillingService$Stub$Proxy
dup
aload 0
invokespecial com/android/vending/billing/IInAppBillingService$Stub$Proxy/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
2 : L1
3 : L2
4 : L3
5 : L4
6 : L5
1598968902 : L6
default : L7
L7:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L6:
aload 3
ldc "com.android.vending.billing.IInAppBillingService"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.android.vending.billing.IInAppBillingService"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readInt()I
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
invokevirtual com/android/vending/billing/IInAppBillingService$Stub/isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I
istore 1
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
L1:
aload 2
ldc "com.android.vending.billing.IInAppBillingService"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readInt()I
istore 1
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 5
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 6
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L8
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 2
L9:
aload 0
iload 1
aload 5
aload 6
aload 2
invokevirtual com/android/vending/billing/IInAppBillingService$Stub/getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L10
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_1
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
L11:
iconst_1
ireturn
L8:
aconst_null
astore 2
goto L9
L10:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
goto L11
L2:
aload 2
ldc "com.android.vending.billing.IInAppBillingService"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readInt()I
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
invokevirtual com/android/vending/billing/IInAppBillingService$Stub/getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L12
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_1
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
L13:
iconst_1
ireturn
L12:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
goto L13
L3:
aload 2
ldc "com.android.vending.billing.IInAppBillingService"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readInt()I
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
invokevirtual com/android/vending/billing/IInAppBillingService$Stub/getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L14
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_1
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
L15:
iconst_1
ireturn
L14:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
goto L15
L4:
aload 2
ldc "com.android.vending.billing.IInAppBillingService"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readInt()I
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
invokevirtual com/android/vending/billing/IInAppBillingService$Stub/consumePurchase(ILjava/lang/String;Ljava/lang/String;)I
istore 1
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
L5:
aload 2
ldc "com.android.vending.billing.IInAppBillingService"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readInt()I
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
aload 2
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getClassLoader()Ljava/lang/ClassLoader;
invokevirtual android/os/Parcel/readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
invokevirtual com/android/vending/billing/IInAppBillingService$Stub/getBuyIntentToReplaceSkus(ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L16
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_1
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
L17:
iconst_1
ireturn
L16:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
goto L17
.limit locals 7
.limit stack 7
.end method
