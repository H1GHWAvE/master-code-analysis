.bytecode 50.0
.class public synchronized com/android/vending/billing/InAppBillingService/LUCK/LogCollector
.super java/lang/Object

.field public static final 'LINE_SEPARATOR' Ljava/lang/String; = "\n"

.field private static final 'LOG_TAG' Ljava/lang/String;

.field private 'mLastLogs' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/lang/String;>;"

.field private 'mPackageName' Ljava/lang/String;

.field private 'mPattern' Ljava/util/regex/Pattern;

.field private 'mPrefs' Landroid/content/SharedPreferences;

.method static <clinit>()V
ldc com/android/vending/billing/InAppBillingService/LUCK/LogCollector
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
putstatic com/android/vending/billing/InAppBillingService/LUCK/LogCollector/LOG_TAG Ljava/lang/String;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
putfield com/android/vending/billing/InAppBillingService/LUCK/LogCollector/mPackageName Ljava/lang/String;
aload 0
ldc "(.*)E\\/AndroidRuntime\\(\\s*\\d+\\)\\:\\s*at\\s%s.*"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/LogCollector/mPackageName Ljava/lang/String;
ldc "."
ldc "\\."
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
putfield com/android/vending/billing/InAppBillingService/LUCK/LogCollector/mPattern Ljava/util/regex/Pattern;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
pop
aload 0
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "LogCollector"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
putfield com/android/vending/billing/InAppBillingService/LUCK/LogCollector/mPrefs Landroid/content/SharedPreferences;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/android/vending/billing/InAppBillingService/LUCK/LogCollector/mLastLogs Ljava/util/ArrayList;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/LogCollector/mPrefs Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
invokeinterface android/content/SharedPreferences$Editor/clear()Landroid/content/SharedPreferences$Editor; 0
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
.limit locals 1
.limit stack 8
.end method

.method private collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V
.signature "(Ljava/util/List<Ljava/lang/String;>;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V"
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L4 to L5 using L2
.catch java/io/IOException from L5 to L6 using L2
.catch java/io/IOException from L6 to L7 using L2
.catch java/io/IOException from L8 to L9 using L2
.catch java/io/IOException from L10 to L11 using L12
.catch java/io/IOException from L11 to L13 using L12
.catch java/io/IOException from L13 to L14 using L12
.catch java/io/IOException from L15 to L16 using L12
.catch java/io/IOException from L17 to L18 using L2
.catch java/io/IOException from L19 to L20 using L2
.catch java/io/IOException from L20 to L21 using L2
.catch java/io/IOException from L21 to L22 using L2
.catch java/io/IOException from L22 to L23 using L2
.catch java/io/IOException from L24 to L25 using L2
.catch java/io/IOException from L26 to L27 using L2
.catch java/io/IOException from L27 to L28 using L2
.catch java/lang/Exception from L28 to L29 using L30
.catch java/io/IOException from L28 to L29 using L2
.catch java/lang/Exception from L31 to L32 using L30
.catch java/io/IOException from L31 to L32 using L2
.catch java/io/IOException from L33 to L34 using L2
.catch java/io/IOException from L35 to L36 using L2
.catch java/io/IOException from L36 to L37 using L2
.catch java/io/IOException from L37 to L38 using L2
.catch java/io/IOException from L38 to L39 using L2
.catch java/io/IOException from L39 to L40 using L2
.catch java/io/IOException from L41 to L42 using L2
.catch java/io/IOException from L43 to L44 using L2
.catch java/io/IOException from L45 to L46 using L2
.catch java/io/IOException from L47 to L48 using L2
.catch java/io/IOException from L49 to L50 using L2
.catch java/io/IOException from L51 to L52 using L2
.catch java/io/IOException from L53 to L54 using L2
.catch java/io/IOException from L55 to L56 using L12
.catch java/io/IOException from L57 to L58 using L12
aload 1
invokeinterface java/util/List/clear()V 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 7
aload 2
astore 6
aload 2
ifnonnull L59
ldc "time"
astore 6
L59:
aload 7
ldc "-v"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 7
aload 6
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 3
ifnull L60
aload 7
ldc "-b"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 7
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L60:
aload 4
ifnull L0
aload 7
aload 4
invokestatic java/util/Collections/addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z
pop
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L3
L1:
iload 5
ifeq L26
L3:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Collect logs no root."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
aload 2
ldc "logcat"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 2
ldc "-d"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 2
aload 7
invokevirtual java/util/ArrayList/addAll(Ljava/util/Collection;)Z
pop
aload 2
aload 2
invokevirtual java/util/ArrayList/size()I
anewarray java/lang/String
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Ljava/lang/String;
astore 2
L4:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
ldc "sdcard"
iconst_0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/patchActivity/getDir(Ljava/lang/String;I)Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L5
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "logcat"
aastore
dup
iconst_1
ldc "-d"
aastore
dup
iconst_2
ldc "System.out:*"
aastore
dup
iconst_3
ldc "*:S"
aastore
astore 2
L5:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
aload 2
invokevirtual java/lang/Runtime/exec([Ljava/lang/String;)Ljava/lang/Process;
astore 2
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 2
invokevirtual java/lang/Process/getInputStream()Ljava/io/InputStream;
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 3
L6:
aload 3
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 4
L7:
aload 4
ifnull L17
L8:
aload 1
aload 4
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L9:
goto L6
L2:
astore 2
L10:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
ldc "su"
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 2
new java/io/DataOutputStream
dup
aload 2
invokevirtual java/lang/Process/getOutputStream()Ljava/io/OutputStream;
invokespecial java/io/DataOutputStream/<init>(Ljava/io/OutputStream;)V
astore 3
new java/io/DataInputStream
dup
aload 2
invokevirtual java/lang/Process/getErrorStream()Ljava/io/InputStream;
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
astore 4
aload 4
invokevirtual java/io/DataInputStream/available()I
newarray byte
astore 6
aload 4
aload 6
invokevirtual java/io/DataInputStream/read([B)I
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/String
dup
aload 6
invokespecial java/lang/String/<init>([B)V
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 2
invokevirtual java/lang/Process/getInputStream()Ljava/io/InputStream;
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 4
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
ldc "sdcard"
iconst_0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/patchActivity/getDir(Ljava/lang/String;I)Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L55
aload 3
ldc "logcat -d System.out:* *:S\n"
invokevirtual java/io/DataOutputStream/writeBytes(Ljava/lang/String;)V
L11:
aload 3
invokevirtual java/io/DataOutputStream/flush()V
aload 3
invokevirtual java/io/DataOutputStream/close()V
L13:
aload 4
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 3
L14:
aload 3
ifnull L57
L15:
aload 1
aload 3
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L16:
goto L13
L12:
astore 1
aload 1
invokevirtual java/io/IOException/printStackTrace()V
L61:
return
L17:
aload 2
invokevirtual java/lang/Process/destroy()V
L18:
aload 3
ifnull L20
L19:
aload 3
invokevirtual java/io/BufferedReader/close()V
L20:
aload 1
invokeinterface java/util/List/size()I 0
ifne L61
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
ldc "su"
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 2
new java/io/DataOutputStream
dup
aload 2
invokevirtual java/lang/Process/getOutputStream()Ljava/io/OutputStream;
invokespecial java/io/DataOutputStream/<init>(Ljava/io/OutputStream;)V
astore 3
new java/io/DataInputStream
dup
aload 2
invokevirtual java/lang/Process/getErrorStream()Ljava/io/InputStream;
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
astore 4
aload 4
invokevirtual java/io/DataInputStream/available()I
newarray byte
astore 6
aload 4
aload 6
invokevirtual java/io/DataInputStream/read([B)I
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/String
dup
aload 6
invokespecial java/lang/String/<init>([B)V
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 2
invokevirtual java/lang/Process/getInputStream()Ljava/io/InputStream;
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 4
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
ldc "sdcard"
iconst_0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/patchActivity/getDir(Ljava/lang/String;I)Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L49
aload 3
ldc "logcat -d System.out:* *:S\n"
invokevirtual java/io/DataOutputStream/writeBytes(Ljava/lang/String;)V
L21:
aload 3
invokevirtual java/io/DataOutputStream/flush()V
aload 3
invokevirtual java/io/DataOutputStream/close()V
L22:
aload 4
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 3
L23:
aload 3
ifnull L51
L24:
aload 1
aload 3
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L25:
goto L22
L26:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
ldc "su"
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 2
new java/io/DataOutputStream
dup
aload 2
invokevirtual java/lang/Process/getOutputStream()Ljava/io/OutputStream;
invokespecial java/io/DataOutputStream/<init>(Ljava/io/OutputStream;)V
astore 3
new java/io/DataInputStream
dup
aload 2
invokevirtual java/lang/Process/getErrorStream()Ljava/io/InputStream;
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
astore 4
aload 4
invokevirtual java/io/DataInputStream/available()I
newarray byte
astore 6
aload 4
aload 6
invokevirtual java/io/DataInputStream/read([B)I
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/String
dup
aload 6
invokespecial java/lang/String/<init>([B)V
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 2
invokevirtual java/lang/Process/getInputStream()Ljava/io/InputStream;
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 4
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
ldc "sdcard"
iconst_0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/patchActivity/getDir(Ljava/lang/String;I)Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L43
aload 3
ldc "logcat -d System.out:* *:S\n"
invokevirtual java/io/DataOutputStream/writeBytes(Ljava/lang/String;)V
L27:
aload 3
invokevirtual java/io/DataOutputStream/flush()V
aload 3
invokevirtual java/io/DataOutputStream/close()V
L28:
aload 4
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 3
L29:
aload 3
ifnull L34
L31:
aload 1
aload 3
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L32:
goto L28
L30:
astore 3
L33:
aload 3
invokevirtual java/lang/Exception/printStackTrace()V
L34:
aload 4
ifnull L36
L35:
aload 4
invokevirtual java/io/BufferedReader/close()V
L36:
aload 2
invokevirtual java/lang/Process/destroy()V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Collect logs no root."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
ldc "\n\n\n*********************************** NO ROOT *******************************************************\n\n\n"
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
aload 2
ldc "logcat"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 2
ldc "-d"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 2
aload 7
invokevirtual java/util/ArrayList/addAll(Ljava/util/Collection;)Z
pop
aload 2
aload 2
invokevirtual java/util/ArrayList/size()I
anewarray java/lang/String
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Ljava/lang/String;
astore 2
L37:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
ldc "sdcard"
iconst_0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/patchActivity/getDir(Ljava/lang/String;I)Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L38
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "logcat"
aastore
dup
iconst_1
ldc "-d"
aastore
dup
iconst_2
ldc "System.out:*"
aastore
dup
iconst_3
ldc "*:S"
aastore
astore 2
L38:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
aload 2
invokevirtual java/lang/Runtime/exec([Ljava/lang/String;)Ljava/lang/Process;
astore 2
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 2
invokevirtual java/lang/Process/getInputStream()Ljava/io/InputStream;
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 3
L39:
aload 3
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 4
L40:
aload 4
ifnull L45
L41:
aload 1
aload 4
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L42:
goto L39
L43:
aload 3
ldc "logcat -d -v time\n"
invokevirtual java/io/DataOutputStream/writeBytes(Ljava/lang/String;)V
L44:
goto L27
L45:
aload 2
invokevirtual java/lang/Process/destroy()V
L46:
aload 3
ifnull L20
L47:
aload 3
invokevirtual java/io/BufferedReader/close()V
L48:
goto L20
L49:
aload 3
ldc "logcat -d\n"
invokevirtual java/io/DataOutputStream/writeBytes(Ljava/lang/String;)V
L50:
goto L21
L51:
aload 2
invokevirtual java/lang/Process/destroy()V
L52:
aload 4
ifnull L61
L53:
aload 4
invokevirtual java/io/BufferedReader/close()V
L54:
return
L55:
aload 3
ldc "logcat -d\n"
invokevirtual java/io/DataOutputStream/writeBytes(Ljava/lang/String;)V
L56:
goto L11
L57:
aload 2
invokevirtual java/lang/Process/destroy()V
L58:
return
.limit locals 8
.limit stack 5
.end method

.method private collectPhoneInfo()Ljava/lang/String;
ldc "Carrier:%s\nModel:%s\nFirmware:%s\n"
iconst_3
anewarray java/lang/Object
dup
iconst_0
getstatic android/os/Build/BRAND Ljava/lang/String;
aastore
dup
iconst_1
getstatic android/os/Build/MODEL Ljava/lang/String;
aastore
dup
iconst_2
getstatic android/os/Build$VERSION/RELEASE Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 5
.end method

.method public collect(Landroid/content/Context;Z)Z
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch java/util/ConcurrentModificationException from L3 to L4 using L5
.catch java/util/ConcurrentModificationException from L4 to L6 using L5
.catch java/io/IOException from L7 to L8 using L9
.catch java/io/IOException from L8 to L10 using L11
.catch java/io/IOException from L10 to L12 using L11
.catch java/io/IOException from L12 to L13 using L11
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/LogCollector/mLastLogs Ljava/util/ArrayList;
astore 4
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L14
aload 0
aload 4
aconst_null
aconst_null
aconst_null
iconst_0
invokespecial com/android/vending/billing/InAppBillingService/LUCK/LogCollector/collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V
L15:
iload 2
ifeq L13
aload 4
invokevirtual java/util/ArrayList/size()I
ifle L13
ldc ""
astore 3
L0:
aload 1
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 1
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/versionName Ljava/lang/String;
astore 1
L1:
new java/lang/StringBuilder
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Lucky Patcher "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 3
aload 0
invokespecial com/android/vending/billing/InAppBillingService/LUCK/LogCollector/collectPhoneInfo()Ljava/lang/String;
astore 5
aload 3
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L3:
aload 4
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 6
L4:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L16
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 7
aload 3
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L6:
goto L4
L5:
astore 3
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher (LogCollector): try again collect log."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
aload 1
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 3
aload 3
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L3
L14:
aload 0
aload 4
aconst_null
aconst_null
aconst_null
iconst_1
invokespecial com/android/vending/billing/InAppBillingService/LUCK/LogCollector/collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V
goto L15
L2:
astore 1
aload 1
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
aload 3
astore 1
goto L1
L16:
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
new java/io/File
dup
ldc "abrakakdabra"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
pop
L7:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Log/log.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 3
L8:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Log"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
aload 3
invokevirtual java/io/File/exists()Z
ifeq L10
aload 3
invokevirtual java/io/File/delete()Z
pop
L10:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Log/log.zip"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L12
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Log/log.zip"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L12:
aload 3
invokevirtual java/io/File/createNewFile()Z
pop
new java/io/FileOutputStream
dup
aload 3
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
aload 1
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/FileOutputStream/write([B)V
L13:
aload 4
invokevirtual java/util/ArrayList/size()I
ifle L17
iconst_1
ireturn
L9:
astore 1
L18:
aload 1
invokevirtual java/io/IOException/printStackTrace()V
goto L13
L17:
iconst_0
ireturn
L11:
astore 1
goto L18
.limit locals 8
.limit stack 6
.end method

.method public hasForceCloseHappened()Z
iconst_1
anewarray java/lang/String
astore 5
aload 5
iconst_0
ldc "*:E"
aastore
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 6
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L0
aload 0
aload 6
ldc "time"
aconst_null
aload 5
iconst_0
invokespecial com/android/vending/billing/InAppBillingService/LUCK/LogCollector/collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V
L1:
aload 6
invokevirtual java/util/ArrayList/size()I
ifle L2
iconst_0
istore 3
iconst_0
istore 1
aload 6
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 5
L3:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 6
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/LogCollector/mPattern Ljava/util/regex/Pattern;
aload 6
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 7
aload 7
invokevirtual java/util/regex/Matcher/matches()Z
istore 4
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/LogCollector/mPrefs Landroid/content/SharedPreferences;
astore 6
iload 4
ifeq L3
iconst_1
istore 2
aload 7
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 7
iload 2
istore 1
aload 6
aload 7
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L3
iconst_1
istore 3
aload 6
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
aload 7
iconst_1
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
iload 2
istore 1
goto L3
L0:
aload 0
aload 6
ldc "time"
aconst_null
aload 5
iconst_1
invokespecial com/android/vending/billing/InAppBillingService/LUCK/LogCollector/collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V
goto L1
L4:
iload 3
ifne L5
iload 1
ifne L5
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Clear all FC logcat prefs..."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/LogCollector/mPrefs Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
invokeinterface android/content/SharedPreferences$Editor/clear()Landroid/content/SharedPreferences$Editor; 0
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L5:
iload 3
ireturn
L2:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Clear all FC logcat prefs..."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/LogCollector/mPrefs Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
invokeinterface android/content/SharedPreferences$Editor/clear()Landroid/content/SharedPreferences$Editor; 0
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
iconst_0
ireturn
.limit locals 8
.limit stack 6
.end method

.method public sendLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.catch net/lingala/zip4j/exception/ZipException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch java/lang/Exception from L4 to L5 using L6
.catch java/lang/Exception from L7 to L8 using L6
.catch java/lang/Exception from L9 to L10 using L6
.catch java/lang/Exception from L11 to L12 using L6
.catch java/lang/RuntimeException from L13 to L14 using L15
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Log/log.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
ldc "file://"
invokestatic android/net/Uri/parse(Ljava/lang/String;)Landroid/net/Uri;
astore 5
L0:
new net/lingala/zip4j/core/ZipFile
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Log/log.zip"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/lang/String;)V
astore 4
new net/lingala/zip4j/model/ZipParameters
dup
invokespecial net/lingala/zip4j/model/ZipParameters/<init>()V
astore 6
aload 6
bipush 8
invokevirtual net/lingala/zip4j/model/ZipParameters/setCompressionMethod(I)V
aload 6
iconst_5
invokevirtual net/lingala/zip4j/model/ZipParameters/setCompressionLevel(I)V
aload 4
aload 1
aload 6
invokevirtual net/lingala/zip4j/core/ZipFile/addFile(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "file://"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Log/log.zip"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/net/Uri/parse(Ljava/lang/String;)Landroid/net/Uri;
astore 1
L1:
aload 1
astore 5
L16:
ldc "not found"
astore 1
ldc "/system/bin/dalvikvm"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L17
ldc "/system/bin/dalvikvm"
astore 1
L17:
ldc "/system/bin/dalvikvm32"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L18
ldc "/system/bin/dalvikvm32"
astore 1
L18:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
ldc "sdcard"
iconst_0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/patchActivity/getDir(Ljava/lang/String;I)Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L19
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Email contain log.txt with bugs by Lucky Patcher! Please describe the Problem and tap to send email.\n\n\nRoot:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "Runtime: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic com/chelpus/Utils/getCurrentRuntimeValue()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "DeviceID: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
ldc "android_id"
invokestatic android/provider/Settings$Secure/getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/os/Build/BOARD Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/os/Build/BRAND Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/os/Build/CPU_ABI Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/os/Build/DEVICE Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/os/Build/DISPLAY Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/os/Build/FINGERPRINT Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "Lucky Patcher ver: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/version Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "LP files directory: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "dalvikvm file:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L20:
aload 1
astore 4
L4:
invokestatic java/lang/System/getenv()Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 7
L5:
aload 1
astore 4
aload 1
astore 6
L7:
aload 7
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L21
L8:
aload 1
astore 4
L9:
aload 7
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 6
L10:
aload 1
astore 4
L11:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L12:
goto L5
L2:
astore 1
aload 1
invokevirtual net/lingala/zip4j/exception/ZipException/printStackTrace()V
goto L16
L19:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Email contain log.txt with bugs by Lucky Patcher! Please describe the Problem and tap to send email.\n\n\nRoot:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "Runtime: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic com/chelpus/Utils/getCurrentRuntimeValue()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "DeviceID: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
ldc "android_id"
invokestatic android/provider/Settings$Secure/getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/os/Build/BOARD Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/os/Build/BRAND Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/os/Build/CPU_ABI Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/os/Build/DEVICE Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/os/Build/DISPLAY Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/os/Build/FINGERPRINT Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "Lucky Patcher ver: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/version Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "LP files directory: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "dalvikvm file:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
goto L20
L6:
astore 1
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
aload 4
astore 6
L21:
new android/content/Intent
dup
ldc "android.intent.action.SEND"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 1
aload 1
ldc "text/plain"
invokevirtual android/content/Intent/setType(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
ldc "android.intent.extra.EMAIL"
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 2
aastore
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
ldc "android.intent.extra.SUBJECT"
aload 3
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
ldc "android.intent.extra.TEXT"
aload 6
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
ldc "android.intent.extra.STREAM"
aload 5
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
pop
L13:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
aload 1
ldc "Send mail"
invokestatic android/content/Intent/createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/patchActivity/startActivity(Landroid/content/Intent;)V
L14:
return
L15:
astore 1
aload 1
invokevirtual java/lang/RuntimeException/printStackTrace()V
return
L3:
astore 1
goto L16
.limit locals 8
.limit stack 6
.end method
