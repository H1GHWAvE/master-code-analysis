.bytecode 50.0
.class public synchronized abstract com/android/vending/billing/IMarketBillingService$Stub
.super android/os/Binder
.implements com/android/vending/billing/IMarketBillingService
.inner class public static abstract Stub inner com/android/vending/billing/IMarketBillingService$Stub outer com/android/vending/billing/IMarketBillingService
.inner class private static Proxy inner com/android/vending/billing/IMarketBillingService$Stub$Proxy outer com/android/vending/billing/IMarketBillingService$Stub

.field private static final 'DESCRIPTOR' Ljava/lang/String; = "com.android.vending.billing.IMarketBillingService"

.field static final 'TRANSACTION_sendBillingRequest' I = 1


.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.android.vending.billing.IMarketBillingService"
invokevirtual com/android/vending/billing/IMarketBillingService$Stub/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/vending/billing/IMarketBillingService;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "com.android.vending.billing.IMarketBillingService"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof com/android/vending/billing/IMarketBillingService
ifeq L1
aload 1
checkcast com/android/vending/billing/IMarketBillingService
areturn
L1:
new com/android/vending/billing/IMarketBillingService$Stub$Proxy
dup
aload 0
invokespecial com/android/vending/billing/IMarketBillingService$Stub$Proxy/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
1598968902 : L1
default : L2
L2:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L1:
aload 3
ldc "com.android.vending.billing.IMarketBillingService"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.android.vending.billing.IMarketBillingService"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L3
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 2
L4:
aload 0
aload 2
invokevirtual com/android/vending/billing/IMarketBillingService$Stub/sendBillingRequest(Landroid/os/Bundle;)Landroid/os/Bundle;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L5
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_1
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
iconst_1
ireturn
L3:
aconst_null
astore 2
goto L4
L5:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
.limit locals 5
.limit stack 5
.end method
