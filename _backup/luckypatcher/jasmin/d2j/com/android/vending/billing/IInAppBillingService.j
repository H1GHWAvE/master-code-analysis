.bytecode 50.0
.class public abstract interface com/android/vending/billing/IInAppBillingService
.super java/lang/Object
.implements android/os/IInterface
.inner class public static abstract Stub inner com/android/vending/billing/IInAppBillingService$Stub outer com/android/vending/billing/IInAppBillingService
.inner class private static Proxy inner com/android/vending/billing/IInAppBillingService$Stub$Proxy outer com/android/vending/billing/IInAppBillingService

.method public abstract consumePurchase(ILjava/lang/String;Ljava/lang/String;)I
.throws android/os/RemoteException
.end method

.method public abstract getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.throws android/os/RemoteException
.end method

.method public abstract getBuyIntentToReplaceSkus(ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.throws android/os/RemoteException
.end method

.method public abstract getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.throws android/os/RemoteException
.end method

.method public abstract getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
.throws android/os/RemoteException
.end method

.method public abstract isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I
.throws android/os/RemoteException
.end method
