.bytecode 50.0
.class synchronized com/android/vending/billing/IInAppBillingService$Stub$Proxy
.super java/lang/Object
.implements com/android/vending/billing/IInAppBillingService
.inner class public static abstract Stub inner com/android/vending/billing/IInAppBillingService$Stub outer com/android/vending/billing/IInAppBillingService
.inner class private static Proxy inner com/android/vending/billing/IInAppBillingService$Stub$Proxy outer com/android/vending/billing/IInAppBillingService$Stub

.field private 'mRemote' Landroid/os/IBinder;

.method <init>(Landroid/os/IBinder;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/android/vending/billing/IInAppBillingService$Stub$Proxy/mRemote Landroid/os/IBinder;
return
.limit locals 2
.limit stack 2
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
getfield com/android/vending/billing/IInAppBillingService$Stub$Proxy/mRemote Landroid/os/IBinder;
areturn
.limit locals 1
.limit stack 1
.end method

.method public consumePurchase(ILjava/lang/String;Ljava/lang/String;)I
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 5
L0:
aload 4
ldc "com.android.vending.billing.IInAppBillingService"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 4
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
aload 4
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 4
aload 3
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield com/android/vending/billing/IInAppBillingService$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_5
aload 4
aload 5
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 5
invokevirtual android/os/Parcel/readException()V
aload 5
invokevirtual android/os/Parcel/readInt()I
istore 1
L1:
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 4
invokevirtual android/os/Parcel/recycle()V
iload 1
ireturn
L2:
astore 2
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 2
athrow
.limit locals 6
.limit stack 5
.end method

.method public getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 6
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 7
L0:
aload 6
ldc "com.android.vending.billing.IInAppBillingService"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 6
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
aload 6
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 6
aload 3
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 6
aload 4
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 6
aload 5
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield com/android/vending/billing/IInAppBillingService$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_3
aload 6
aload 7
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 7
invokevirtual android/os/Parcel/readException()V
aload 7
invokevirtual android/os/Parcel/readInt()I
ifeq L3
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 7
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 2
L1:
aload 7
invokevirtual android/os/Parcel/recycle()V
aload 6
invokevirtual android/os/Parcel/recycle()V
aload 2
areturn
L3:
aconst_null
astore 2
goto L1
L2:
astore 2
aload 7
invokevirtual android/os/Parcel/recycle()V
aload 6
invokevirtual android/os/Parcel/recycle()V
aload 2
athrow
.limit locals 8
.limit stack 5
.end method

.method public getBuyIntentToReplaceSkus(ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 7
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 8
L0:
aload 7
ldc "com.android.vending.billing.IInAppBillingService"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 7
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
aload 7
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 7
aload 3
invokevirtual android/os/Parcel/writeList(Ljava/util/List;)V
aload 7
aload 4
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 7
aload 5
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 7
aload 6
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield com/android/vending/billing/IInAppBillingService$Stub$Proxy/mRemote Landroid/os/IBinder;
bipush 6
aload 7
aload 8
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 8
invokevirtual android/os/Parcel/readException()V
aload 8
invokevirtual android/os/Parcel/readInt()I
ifeq L3
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 8
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 2
L1:
aload 8
invokevirtual android/os/Parcel/recycle()V
aload 7
invokevirtual android/os/Parcel/recycle()V
aload 2
areturn
L3:
aconst_null
astore 2
goto L1
L2:
astore 2
aload 8
invokevirtual android/os/Parcel/recycle()V
aload 7
invokevirtual android/os/Parcel/recycle()V
aload 2
athrow
.limit locals 9
.limit stack 5
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
ldc "com.android.vending.billing.IInAppBillingService"
areturn
.limit locals 1
.limit stack 1
.end method

.method public getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 5
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 6
L0:
aload 5
ldc "com.android.vending.billing.IInAppBillingService"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 5
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
aload 5
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 5
aload 3
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 5
aload 4
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield com/android/vending/billing/IInAppBillingService$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_4
aload 5
aload 6
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 6
invokevirtual android/os/Parcel/readException()V
aload 6
invokevirtual android/os/Parcel/readInt()I
ifeq L3
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 6
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 2
L1:
aload 6
invokevirtual android/os/Parcel/recycle()V
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 2
areturn
L3:
aconst_null
astore 2
goto L1
L2:
astore 2
aload 6
invokevirtual android/os/Parcel/recycle()V
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 2
athrow
.limit locals 7
.limit stack 5
.end method

.method public getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 5
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 6
L0:
aload 5
ldc "com.android.vending.billing.IInAppBillingService"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 5
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
aload 5
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 5
aload 3
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
L1:
aload 4
ifnull L6
L3:
aload 5
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 4
aload 5
iconst_0
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 0
getfield com/android/vending/billing/IInAppBillingService$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_2
aload 5
aload 6
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 6
invokevirtual android/os/Parcel/readException()V
aload 6
invokevirtual android/os/Parcel/readInt()I
ifeq L8
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 6
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 2
L5:
aload 6
invokevirtual android/os/Parcel/recycle()V
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 2
areturn
L6:
aload 5
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L7:
goto L4
L2:
astore 2
aload 6
invokevirtual android/os/Parcel/recycle()V
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 2
athrow
L8:
aconst_null
astore 2
goto L5
.limit locals 7
.limit stack 5
.end method

.method public isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 5
L0:
aload 4
ldc "com.android.vending.billing.IInAppBillingService"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 4
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
aload 4
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 4
aload 3
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield com/android/vending/billing/IInAppBillingService$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_1
aload 4
aload 5
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 5
invokevirtual android/os/Parcel/readException()V
aload 5
invokevirtual android/os/Parcel/readInt()I
istore 1
L1:
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 4
invokevirtual android/os/Parcel/recycle()V
iload 1
ireturn
L2:
astore 2
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 2
athrow
.limit locals 6
.limit stack 5
.end method
