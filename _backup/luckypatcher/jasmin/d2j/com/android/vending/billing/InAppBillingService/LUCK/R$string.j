.bytecode 50.0
.class public final synchronized com/android/vending/billing/InAppBillingService/LUCK/R$string
.super java/lang/Object
.inner class public static final string inner com/android/vending/billing/InAppBillingService/LUCK/R$string outer com/android/vending/billing/InAppBillingService/LUCK/R

.field public static final 'AppTools' I = 2131165184


.field public static final 'P_not_found' I = 2131165185


.field public static final 'PatchResult' I = 2131165186


.field public static final 'Yes' I = 2131165187


.field public static final 'about_working_dir' I = 2131165188


.field public static final 'aboutlp' I = 2131165189


.field public static final 'aboutmenu' I = 2131165190


.field public static final 'abouttitle' I = 2131165191


.field public static final 'activity_ads_descr' I = 2131165192


.field public static final 'activity_descr' I = 2131165193


.field public static final 'add_widget' I = 2131165789


.field public static final 'additinfo' I = 2131165194


.field public static final 'adsdescr' I = 2131165195


.field public static final 'adstitle' I = 2131165196


.field public static final 'advanced_menu' I = 2131165197


.field public static final 'all_custom_patches_updated' I = 2131165198


.field public static final 'allchanges' I = 2131165199


.field public static final 'android_patches_for_bootlist' I = 2131165754


.field public static final 'android_patches_for_bootlist_status' I = 2131165755


.field public static final 'apk_create_option' I = 2131165200


.field public static final 'apk_not_found_error' I = 2131165201


.field public static final 'apk_size' I = 2131165202


.field public static final 'apkdescr' I = 2131165203


.field public static final 'apklabel' I = 2131165204


.field public static final 'apkpackage' I = 2131165205


.field public static final 'app_dialog_no_root_app_control' I = 2131165206


.field public static final 'app_disabler_widget_pkg_not_found' I = 2131165756


.field public static final 'app_info' I = 2131165207


.field public static final 'app_install_success' I = 2131165208


.field public static final 'app_name' I = 2131165209


.field public static final 'app_not_found' I = 2131165210


.field public static final 'app_not_found_adapter' I = 2131165757


.field public static final 'apps_not_found' I = 2131165758


.field public static final 'appwidget_text' I = 2131165790


.field public static final 'back' I = 2131165211


.field public static final 'backbutton' I = 2131165212


.field public static final 'backup_data_empty' I = 2131165213


.field public static final 'backup_data_error' I = 2131165214


.field public static final 'backup_data_success' I = 2131165215


.field public static final 'backup_delete_message' I = 2131165216


.field public static final 'backup_done' I = 2131165217


.field public static final 'backups_not_found' I = 2131165218


.field public static final 'begin_changelog' I = 2131165219


.field public static final 'billing_buy' I = 2131165220


.field public static final 'billing_hack' I = 2131165221


.field public static final 'billing_hack2_auto_repeat_note' I = 2131165222


.field public static final 'billing_hack2_new' I = 2131165223


.field public static final 'billing_hack_menu' I = 2131165224


.field public static final 'billing_hack_menu_descr' I = 2131165225


.field public static final 'billing_hack_menu_disable' I = 2131165226


.field public static final 'billing_hack_menu_enable' I = 2131165227


.field public static final 'billing_hack_option' I = 2131165228


.field public static final 'billing_hack_option2' I = 2131165229


.field public static final 'bind_button' I = 2131165230


.field public static final 'bind_delete' I = 2131165231


.field public static final 'bind_dialog_button' I = 2131165232


.field public static final 'bind_dialog_source' I = 2131165233


.field public static final 'bind_dialog_target' I = 2131165234


.field public static final 'bind_error' I = 2131165235


.field public static final 'bind_source' I = 2131165236


.field public static final 'bind_target' I = 2131165237


.field public static final 'binder_browser' I = 2131165238


.field public static final 'binder_browser_create_dir' I = 2131165239


.field public static final 'binder_browser_current_dir' I = 2131165240


.field public static final 'binder_browser_select' I = 2131165241


.field public static final 'binder_delete' I = 2131165242


.field public static final 'bootview' I = 2131165243


.field public static final 'busybox_not_found' I = 2131165244


.field public static final 'button_advanced_filter' I = 2131165245


.field public static final 'button_block_internet' I = 2131165759


.field public static final 'button_for_delete' I = 2131165246


.field public static final 'button_integrate_update_apps' I = 2131165760


.field public static final 'button_move_to_internal' I = 2131165761


.field public static final 'button_move_to_sdcard' I = 2131165247


.field public static final 'button_unblock_internet' I = 2131165762


.field public static final 'button_uninstall_apps' I = 2131165763


.field public static final 'cancel' I = 2131165248


.field public static final 'change_icon_lp' I = 2131165249


.field public static final 'change_icon_lp_descr' I = 2131165250


.field public static final 'changelog' I = 2131165251


.field public static final 'cleardalvik' I = 2131165252


.field public static final 'cleardata' I = 2131165253


.field public static final 'cloneApplication' I = 2131165254


.field public static final 'collect_logs' I = 2131165255


.field public static final 'configure' I = 2131165791


.field public static final 'context_automodeslvl' I = 2131165256


.field public static final 'context_backup_apk' I = 2131165257


.field public static final 'context_backup_data' I = 2131165258


.field public static final 'context_batch_operations' I = 2131165259


.field public static final 'context_block_internet_selected_apps' I = 2131165764


.field public static final 'context_blockads' I = 2131165260


.field public static final 'context_change_components_menu' I = 2131165261


.field public static final 'context_clearhosts' I = 2131165262


.field public static final 'context_crt_apk_permission' I = 2131165263


.field public static final 'context_dependencies' I = 2131165264


.field public static final 'context_dexopt_app' I = 2131165265


.field public static final 'context_disableActivity' I = 2131165266


.field public static final 'context_disableComponents' I = 2131165267


.field public static final 'context_disable_google_ads' I = 2131165268


.field public static final 'context_disable_package' I = 2131165269


.field public static final 'context_enable_google_ads' I = 2131165270


.field public static final 'context_enable_package' I = 2131165271


.field public static final 'context_hard_apk_permission' I = 2131165272


.field public static final 'context_hide_lp' I = 2131165273


.field public static final 'context_hide_lp_descr' I = 2131165274


.field public static final 'context_install' I = 2131165275


.field public static final 'context_install_as_system' I = 2131165276


.field public static final 'context_integrate_update_selected_apps' I = 2131165277


.field public static final 'context_move_selected_apps_to_internal' I = 2131165278


.field public static final 'context_move_selected_apps_to_sdcard' I = 2131165279


.field public static final 'context_patch_framework' I = 2131165765


.field public static final 'context_rebuild' I = 2131165280


.field public static final 'context_removeAds' I = 2131165281


.field public static final 'context_remove_saved_purchaces' I = 2131165282


.field public static final 'context_restore_apk' I = 2131165283


.field public static final 'context_restore_data' I = 2131165284


.field public static final 'context_safe_apk_permission' I = 2131165285


.field public static final 'context_safe_sign_apk_permission' I = 2131165286


.field public static final 'context_support_patch' I = 2131165287


.field public static final 'context_tools' I = 2131165288


.field public static final 'context_unblock_internet_selected_apps' I = 2131165766


.field public static final 'context_unblockads' I = 2131165289


.field public static final 'context_uninstall_if_installed' I = 2131165290


.field public static final 'context_uninstall_selected_apps' I = 2131165291


.field public static final 'contextads' I = 2131165292


.field public static final 'contextbackup' I = 2131165293


.field public static final 'contextblockfileads' I = 2131165294


.field public static final 'contextblockfileads_descr' I = 2131165295


.field public static final 'contextboot' I = 2131165296


.field public static final 'contextbootads' I = 2131165297


.field public static final 'contextbootcustom' I = 2131165298


.field public static final 'contextbootlvl' I = 2131165299


.field public static final 'contextcorepatch1' I = 2131165300


.field public static final 'contextcorepatch1_descr' I = 2131165301


.field public static final 'contextcorepatch2' I = 2131165302


.field public static final 'contextcorepatch2_descr' I = 2131165303


.field public static final 'contextcorepatch3' I = 2131165304


.field public static final 'contextcorepatch3_descr' I = 2131165305


.field public static final 'contextcorepatch3_descr_plus' I = 2131165306


.field public static final 'contextcorepatch4' I = 2131165307


.field public static final 'contextcorepatch4_descr' I = 2131165308


.field public static final 'contextcorepatch4_descr_xposed' I = 2131165309


.field public static final 'contextcorepatch4_note' I = 2131165310


.field public static final 'contextcorepatchrestore' I = 2131165311


.field public static final 'contextcorepatchrestore2' I = 2131165312


.field public static final 'contextcorepatchrestore2_descr' I = 2131165313


.field public static final 'contextcorepatchrestore_descr' I = 2131165314


.field public static final 'contextcustompatch' I = 2131165315


.field public static final 'contextdeodex' I = 2131165316


.field public static final 'contextextpatch' I = 2131165317


.field public static final 'contextfix' I = 2131165318


.field public static final 'contextfulloffline' I = 2131165319


.field public static final 'contextfulloffline_descr' I = 2131165320


.field public static final 'contextignore' I = 2131165321


.field public static final 'contextlaunchapp' I = 2131165322


.field public static final 'contextlivepatch' I = 2131165323


.field public static final 'contextodex' I = 2131165324


.field public static final 'contextpatt1' I = 2131165325


.field public static final 'contextpatt1_descr' I = 2131165326


.field public static final 'contextpatt2' I = 2131165327


.field public static final 'contextpatt2_descr' I = 2131165328


.field public static final 'contextpatt3' I = 2131165329


.field public static final 'contextpatt3_descr' I = 2131165330


.field public static final 'contextpatt4' I = 2131165331


.field public static final 'contextpatt4_descr' I = 2131165332


.field public static final 'contextpatt5' I = 2131165333


.field public static final 'contextpatt5_descr' I = 2131165334


.field public static final 'contextpatt6' I = 2131165335


.field public static final 'contextpatt6_descr' I = 2131165336


.field public static final 'contextpattads1' I = 2131165337


.field public static final 'contextpattads1_descr' I = 2131165338


.field public static final 'contextpattads2' I = 2131165339


.field public static final 'contextpattads2_descr' I = 2131165340


.field public static final 'contextpattads3' I = 2131165341


.field public static final 'contextpattads3_descr' I = 2131165342


.field public static final 'contextpattads4' I = 2131165343


.field public static final 'contextpattads4_descr' I = 2131165344


.field public static final 'contextpattads5' I = 2131165345


.field public static final 'contextpattads5_descr' I = 2131165346


.field public static final 'contextpattads6' I = 2131165347


.field public static final 'contextpattads6_descr' I = 2131165348


.field public static final 'contextpattamazon' I = 2131165349


.field public static final 'contextpattamazon_descr' I = 2131165350


.field public static final 'contextpattdependencies_descr' I = 2131165351


.field public static final 'contextpattlvl1' I = 2131165352


.field public static final 'contextpattlvl1_descr' I = 2131165353


.field public static final 'contextpattlvl2' I = 2131165354


.field public static final 'contextpattlvl2_descr' I = 2131165355


.field public static final 'contextpattlvl3' I = 2131165356


.field public static final 'contextpattlvl3_descr' I = 2131165357


.field public static final 'contextpattlvl4' I = 2131165358


.field public static final 'contextpattlvl4_descr' I = 2131165359


.field public static final 'contextpattlvl5' I = 2131165360


.field public static final 'contextpattlvl5_descr' I = 2131165361


.field public static final 'contextpattlvl6' I = 2131165362


.field public static final 'contextpattlvl6_descr' I = 2131165363


.field public static final 'contextpattsamsung' I = 2131165364


.field public static final 'contextpattsamsung_descr' I = 2131165365


.field public static final 'contextrestore' I = 2131165366


.field public static final 'contextselpatt' I = 2131165367


.field public static final 'copyright' I = 2131165368


.field public static final 'core_error_dalvik_cache_not_found_for_core_jar' I = 2131165369


.field public static final 'core_error_dalvik_cache_not_found_for_services_jar' I = 2131165370


.field public static final 'core_only_dalvik_cache_end' I = 2131165767


.field public static final 'core_only_dalvik_cache_finished' I = 2131165768


.field public static final 'core_option' I = 2131165371


.field public static final 'core_patch_apply' I = 2131165372


.field public static final 'core_patch_nopatch' I = 2131165373


.field public static final 'core_patch_nopatch_no_space1' I = 2131165374


.field public static final 'core_result_checkbox' I = 2131165769


.field public static final 'corepatches' I = 2131165375


.field public static final 'corrupt_download' I = 2131165376


.field public static final 'create' I = 2131165377


.field public static final 'create_clone_app' I = 2131165378


.field public static final 'create_dir_error' I = 2131165379


.field public static final 'create_dir_error_2' I = 2131165380


.field public static final 'create_lic_done' I = 2131165381


.field public static final 'create_perm_ok' I = 2131165382


.field public static final 'createapk' I = 2131165383


.field public static final 'createapkads' I = 2131165384


.field public static final 'createapkcustom' I = 2131165385


.field public static final 'createapklvl' I = 2131165386


.field public static final 'createapklvlautoinverse' I = 2131165387


.field public static final 'createapklvlextreme' I = 2131165388


.field public static final 'createapksupport' I = 2131165389


.field public static final 'createdialog1' I = 2131165390


.field public static final 'createdialog1_clone' I = 2131165391


.field public static final 'createdialog2' I = 2131165392


.field public static final 'createdialog3' I = 2131165393


.field public static final 'createdialog3_clone' I = 2131165394


.field public static final 'createdialog4' I = 2131165395


.field public static final 'createdialog5' I = 2131165396


.field public static final 'custom_descript' I = 2131165397


.field public static final 'custom_patches_update_not_found' I = 2131165398


.field public static final 'custom_sel_app' I = 2131165399


.field public static final 'customdescr' I = 2131165400


.field public static final 'customtitle' I = 2131165401


.field public static final 'dalvik_cache_size' I = 2131165402


.field public static final 'data_dir_size' I = 2131165403


.field public static final 'days_on_up' I = 2131165404


.field public static final 'days_on_up_descr' I = 2131165405


.field public static final 'default_target' I = 2131165406


.field public static final 'delboot' I = 2131165407


.field public static final 'delete_file' I = 2131165408


.field public static final 'delete_file_message' I = 2131165409


.field public static final 'dialog_system' I = 2131165410


.field public static final 'dir_change' I = 2131165411


.field public static final 'dir_change_descr' I = 2131165412


.field public static final 'dirbinder' I = 2131165413


.field public static final 'dirr_chg' I = 2131165414


.field public static final 'disable_autoupdate' I = 2131165415


.field public static final 'disable_autoupdate_descr' I = 2131165416


.field public static final 'disable_inapp_services_google' I = 2131165417


.field public static final 'disable_inapp_services_google_descr' I = 2131165418


.field public static final 'disable_license_services_google' I = 2131165419


.field public static final 'disable_license_services_google_descr' I = 2131165420


.field public static final 'disabled_package' I = 2131165421


.field public static final 'dontusepatch' I = 2131165422


.field public static final 'download' I = 2131165423


.field public static final 'empty' I = 2131165424


.field public static final 'empty_binders' I = 2131165425


.field public static final 'enabled_package' I = 2131165426


.field public static final 'enter_name_dir' I = 2131165427


.field public static final 'equals_version' I = 2131165428


.field public static final 'error' I = 2131165770


.field public static final 'error_apk_to_system' I = 2131165429


.field public static final 'error_app_installed_on_sdcard' I = 2131165430


.field public static final 'error_app_installed_on_sdcard_block' I = 2131165431


.field public static final 'error_backup_apk' I = 2131165432


.field public static final 'error_classes_not_found' I = 2131165771


.field public static final 'error_collect_logs' I = 2131165433


.field public static final 'error_create_oat' I = 2131165434


.field public static final 'error_detect' I = 2131165435


.field public static final 'error_get_pkgInfo' I = 2131165436


.field public static final 'error_launch' I = 2131165437


.field public static final 'error_lenghts_patterns' I = 2131165438


.field public static final 'error_message_for_share_app' I = 2131165772


.field public static final 'error_not_found_lic_data' I = 2131165439


.field public static final 'error_not_found_storage_lic' I = 2131165440


.field public static final 'error_odex_app' I = 2131165441


.field public static final 'error_odex_app_impossible' I = 2131165773


.field public static final 'error_original_pattern' I = 2131165442


.field public static final 'error_out_of_memory' I = 2131165443


.field public static final 'error_path' I = 2131165444


.field public static final 'error_path_exists' I = 2131165445


.field public static final 'error_replace_pattern' I = 2131165446


.field public static final 'error_restore' I = 2131165447


.field public static final 'extpat1' I = 2131165448


.field public static final 'extpat2' I = 2131165449


.field public static final 'extpat3' I = 2131165450


.field public static final 'extres1' I = 2131165451


.field public static final 'extres2' I = 2131165452


.field public static final 'fast_start' I = 2131165453


.field public static final 'fast_start_descr' I = 2131165454


.field public static final 'file_not_deleted' I = 2131165774


.field public static final 'file_not_found' I = 2131165455


.field public static final 'filter' I = 2131165456


.field public static final 'filter0' I = 2131165457


.field public static final 'filter1' I = 2131165458


.field public static final 'filter10' I = 2131165459


.field public static final 'filter11' I = 2131165460


.field public static final 'filter12' I = 2131165461


.field public static final 'filter13' I = 2131165462


.field public static final 'filter2' I = 2131165463


.field public static final 'filter3' I = 2131165464


.field public static final 'filter4' I = 2131165465


.field public static final 'filter5' I = 2131165466


.field public static final 'filter6' I = 2131165467


.field public static final 'filter7' I = 2131165468


.field public static final 'filter8' I = 2131165469


.field public static final 'filter9' I = 2131165470


.field public static final 'filterdescr' I = 2131165471


.field public static final 'final_market_message' I = 2131165472


.field public static final 'fixbutton' I = 2131165473


.field public static final 'fixeddescr' I = 2131165474


.field public static final 'fixedtitle' I = 2131165475


.field public static final 'framework_file_patched' I = 2131165775


.field public static final 'framework_limit' I = 2131165776


.field public static final 'framework_patch_not_result' I = 2131165777


.field public static final 'go_to_file_path' I = 2131165476


.field public static final 'goodresult' I = 2131165477


.field public static final 'have_luck' I = 2131165478


.field public static final 'header_activities' I = 2131165479


.field public static final 'header_providers' I = 2131165480


.field public static final 'header_receivers' I = 2131165481


.field public static final 'header_services' I = 2131165482


.field public static final 'help' I = 2131165483


.field public static final 'help_common' I = 2131165484


.field public static final 'help_custom' I = 2131165485


.field public static final 'hide_notify' I = 2131165486


.field public static final 'hide_notify_descr' I = 2131165487


.field public static final 'hide_title_app' I = 2131165488


.field public static final 'hide_title_app_descr' I = 2131165489


.field public static final 'host_down' I = 2131165490


.field public static final 'hosts_default' I = 2131165491


.field public static final 'hosts_not_found_changes' I = 2131165492


.field public static final 'hosts_updated' I = 2131165493


.field public static final 'inapp_emulation_auto_repeat' I = 2131165494


.field public static final 'information' I = 2131165495


.field public static final 'install' I = 2131165496


.field public static final 'install_as_user_app' I = 2131165497


.field public static final 'install_as_user_app_note' I = 2131165498


.field public static final 'install_cloned_app' I = 2131165499


.field public static final 'install_date' I = 2131165500


.field public static final 'installsupersu' I = 2131165501


.field public static final 'integrate_to_system' I = 2131165502


.field public static final 'integrate_updates_result' I = 2131165503


.field public static final 'integrate_updates_to_system' I = 2131165504


.field public static final 'internet_not_found' I = 2131165505


.field public static final 'is_an_invalid_number' I = 2131165506


.field public static final 'langdef' I = 2131165507


.field public static final 'langen' I = 2131165508


.field public static final 'langmenu' I = 2131165509


.field public static final 'launchbutton' I = 2131165510


.field public static final 'licensing_hack_menu' I = 2131165511


.field public static final 'licensing_hack_menu_descr' I = 2131165512


.field public static final 'licensing_hack_menu_disable' I = 2131165513


.field public static final 'licensing_hack_menu_enable' I = 2131165514


.field public static final 'line' I = 2131165792


.field public static final 'loadpkg' I = 2131165515


.field public static final 'lp_billing_hack_title' I = 2131165516


.field public static final 'lvldescr' I = 2131165517


.field public static final 'lvltitle' I = 2131165518


.field public static final 'market_description_text' I = 2131165519


.field public static final 'market_install' I = 2131165520


.field public static final 'market_version1' I = 2131165521


.field public static final 'market_version2' I = 2131165522


.field public static final 'market_version3' I = 2131165523


.field public static final 'market_version4' I = 2131165524


.field public static final 'market_version5' I = 2131165525


.field public static final 'market_version6' I = 2131165526


.field public static final 'menu_download_custom_patches' I = 2131165527


.field public static final 'messageAndroidManifest' I = 2131165528


.field public static final 'messageODEX1' I = 2131165529


.field public static final 'messagePath' I = 2131165530


.field public static final 'messageSD' I = 2131165531


.field public static final 'message_allow_non_market_app' I = 2131165532


.field public static final 'message_clear_all_dalvik_cache' I = 2131165533


.field public static final 'message_clear_data' I = 2131165534


.field public static final 'message_delete_odex_file' I = 2131165535


.field public static final 'message_delete_package_new' I = 2131165778


.field public static final 'message_done_move_to_internal' I = 2131165536


.field public static final 'message_done_move_to_sdcard' I = 2131165537


.field public static final 'message_error_move_to_internal' I = 2131165538


.field public static final 'message_error_move_to_sdcard' I = 2131165539


.field public static final 'message_exit' I = 2131165540


.field public static final 'message_reboot' I = 2131165541


.field public static final 'message_remove_all_odex' I = 2131165542


.field public static final 'message_warning_InApp_service_disable' I = 2131165543


.field public static final 'message_warning_LVL_emulation_service_enable' I = 2131165544


.field public static final 'message_warning_LVL_service_disable' I = 2131165545


.field public static final 'mod_market_check' I = 2131165546


.field public static final 'mod_market_check_failed' I = 2131165547


.field public static final 'mod_market_check_internet_off' I = 2131165548


.field public static final 'mod_market_check_title' I = 2131165549


.field public static final 'mod_market_check_true' I = 2131165550


.field public static final 'modifdescr' I = 2131165551


.field public static final 'modiftitle' I = 2131165552


.field public static final 'move_data' I = 2131165553


.field public static final 'move_to_internal' I = 2131165554


.field public static final 'move_to_internal_message' I = 2131165555


.field public static final 'move_to_sdcard' I = 2131165556


.field public static final 'move_to_sdcard_message' I = 2131165557


.field public static final 'move_to_sys' I = 2131165558


.field public static final 'move_to_system' I = 2131165559


.field public static final 'mpatcher_help' I = 2131165560


.field public static final 'new_method_lvl' I = 2131165561


.field public static final 'new_version' I = 2131165562


.field public static final 'no' I = 2131165563


.field public static final 'no_freespace_patch' I = 2131165564


.field public static final 'no_icon' I = 2131165565


.field public static final 'no_icon_descr' I = 2131165566


.field public static final 'no_root' I = 2131165567


.field public static final 'no_site' I = 2131165568


.field public static final 'no_space' I = 2131165569


.field public static final 'no_space_in_data_all' I = 2131165570


.field public static final 'no_space_in_system' I = 2131165571


.field public static final 'no_space_in_system_all' I = 2131165572


.field public static final 'nofilter' I = 2131165573


.field public static final 'nonedescr' I = 2131165574


.field public static final 'nonetitle' I = 2131165575


.field public static final 'not_detect' I = 2131165576


.field public static final 'not_found_changelog' I = 2131165577


.field public static final 'notify_android_patch_on_boot' I = 2131165578


.field public static final 'notify_android_patch_on_boot_mes_core_jar' I = 2131165779


.field public static final 'notify_android_patch_on_boot_mes_services_jar' I = 2131165780


.field public static final 'notify_directory_binder' I = 2131165579


.field public static final 'notify_directory_binder_from' I = 2131165580


.field public static final 'notify_directory_binder_to' I = 2131165581


.field public static final 'notify_patch_on_boot' I = 2131165582


.field public static final 'notify_patched' I = 2131165583


.field public static final 'odex_all_system_app' I = 2131165584


.field public static final 'odex_all_system_app_message' I = 2131165585


.field public static final 'odexed_not_all_apps' I = 2131165586


.field public static final 'ok' I = 2131165587


.field public static final 'on_top_apps_marker' I = 2131165588


.field public static final 'or_hex_str' I = 2131165589


.field public static final 'orientmenu' I = 2131165590


.field public static final 'others' I = 2131165591


.field public static final 'patch_ok' I = 2131165592


.field public static final 'patch_progress_data_parse' I = 2131165593


.field public static final 'patch_progress_get_classes' I = 2131165594


.field public static final 'patch_progress_strings_analisis' I = 2131165595


.field public static final 'patch_step1' I = 2131165596


.field public static final 'patch_step2' I = 2131165597


.field public static final 'patch_step3' I = 2131165598


.field public static final 'patch_step4' I = 2131165599


.field public static final 'patch_step6' I = 2131165600


.field public static final 'patch_step_http' I = 2131165601


.field public static final 'patch_to_Android_At_once' I = 2131165602


.field public static final 'patchbutton' I = 2131165603


.field public static final 'pattern1' I = 2131165604


.field public static final 'pattern1_f' I = 2131165605


.field public static final 'pattern2' I = 2131165606


.field public static final 'pattern2_f' I = 2131165607


.field public static final 'pattern3' I = 2131165608


.field public static final 'pattern3_f' I = 2131165609


.field public static final 'pattern4' I = 2131165610


.field public static final 'pattern4_f' I = 2131165611


.field public static final 'pattern5' I = 2131165612


.field public static final 'pattern5_f' I = 2131165613


.field public static final 'pattern6' I = 2131165614


.field public static final 'pattern6_f' I = 2131165615


.field public static final 'pattern7' I = 2131165616


.field public static final 'pattern7_f' I = 2131165617


.field public static final 'patternamazon' I = 2131165618


.field public static final 'patternamazon_f' I = 2131165619


.field public static final 'patterndependencies' I = 2131165620


.field public static final 'patternsamsung' I = 2131165621


.field public static final 'patternsamsung_f' I = 2131165622


.field public static final 'perm_ok' I = 2131165623


.field public static final 'permission' I = 2131165624


.field public static final 'permission_not_descr' I = 2131165625


.field public static final 'permission_not_found' I = 2131165626


.field public static final 'permissions' I = 2131165627


.field public static final 'poorresult' I = 2131165628


.field public static final 'proxyGP' I = 2131165793


.field public static final 'proxyGP_descr' I = 2131165794


.field public static final 'punkt1' I = 2131165629


.field public static final 'reboot' I = 2131165630


.field public static final 'reboot_message' I = 2131165631


.field public static final 'rebuild_info' I = 2131165632


.field public static final 'rebuild_message' I = 2131165633


.field public static final 'remodex' I = 2131165634


.field public static final 'remodexerror' I = 2131165635


.field public static final 'remove_all_saved_purchases' I = 2131165636


.field public static final 'remove_all_saved_purchases_message' I = 2131165637


.field public static final 'removefixes' I = 2131165638


.field public static final 'rep_hex_str' I = 2131165639


.field public static final 'restore_build' I = 2131165640


.field public static final 'restore_data_error' I = 2131165641


.field public static final 'restore_data_success' I = 2131165642


.field public static final 'restore_message' I = 2131165643


.field public static final 'restore_version' I = 2131165644


.field public static final 'restorebutton' I = 2131165645


.field public static final 'restored' I = 2131165781


.field public static final 'root_access_not_found' I = 2131165646


.field public static final 'root_needed' I = 2131165647


.field public static final 'safe_perm_ok' I = 2131165648


.field public static final 'safe_perm_use_warning' I = 2131165649


.field public static final 'savebutton' I = 2131165650


.field public static final 'saved_object' I = 2131165651


.field public static final 'search' I = 2131165652


.field public static final 'sel_target' I = 2131165653


.field public static final 'select_filter' I = 2131165654


.field public static final 'select_market' I = 2131165655


.field public static final 'send_logs' I = 2131165656


.field public static final 'sendlog' I = 2131165657


.field public static final 'set_default_to_install' I = 2131165658


.field public static final 'set_default_to_install_auto' I = 2131165659


.field public static final 'set_default_to_install_internal_memory' I = 2131165660


.field public static final 'set_default_to_install_sdcard' I = 2131165661


.field public static final 'set_switchers_def' I = 2131165662


.field public static final 'setting_confirm_exit' I = 2131165663


.field public static final 'setting_confirm_exit_descr' I = 2131165664


.field public static final 'settings' I = 2131165665


.field public static final 'settings_force_root' I = 2131165666


.field public static final 'settings_force_root_description' I = 2131165667


.field public static final 'settings_force_root_off' I = 2131165668


.field public static final 'settings_force_root_on' I = 2131165669


.field public static final 'settings_root_auto' I = 2131165670


.field public static final 'share' I = 2131165782


.field public static final 'share_message' I = 2131165671


.field public static final 'share_this_app' I = 2131165672


.field public static final 'site_pattern' I = 2131165673


.field public static final 'sortbyname' I = 2131165674


.field public static final 'sortbystatus' I = 2131165675


.field public static final 'sortbytime' I = 2131165676


.field public static final 'sortmenu' I = 2131165677


.field public static final 'stat_boot_ads' I = 2131165678


.field public static final 'stat_boot_custom' I = 2131165679


.field public static final 'stat_boot_lvl' I = 2131165680


.field public static final 'stat_launch_activity' I = 2131165681


.field public static final 'statads' I = 2131165682


.field public static final 'statappplace' I = 2131165683


.field public static final 'statbilling' I = 2131165684


.field public static final 'statbuild' I = 2131165685


.field public static final 'statcustom' I = 2131165686


.field public static final 'statdataplace' I = 2131165687


.field public static final 'statfix' I = 2131165688


.field public static final 'statlvl' I = 2131165689


.field public static final 'statmodified' I = 2131165690


.field public static final 'statnotfix' I = 2131165691


.field public static final 'statnotfound' I = 2131165692


.field public static final 'statnotmodified' I = 2131165693


.field public static final 'statnotsys' I = 2131165694


.field public static final 'statnull' I = 2131165695


.field public static final 'statsys' I = 2131165696


.field public static final 'statuserid' I = 2131165697


.field public static final 'statversion' I = 2131165698


.field public static final 'support_patch_billing' I = 2131165699


.field public static final 'support_patch_billing_descr' I = 2131165700


.field public static final 'support_patch_free_intent' I = 2131165701


.field public static final 'support_patch_free_intent_descr' I = 2131165702


.field public static final 'support_patch_lvl' I = 2131165703


.field public static final 'support_patch_lvl_descr' I = 2131165704


.field public static final 'switcher_auto_backup' I = 2131165705


.field public static final 'switcher_auto_backup_descr' I = 2131165706


.field public static final 'switcher_auto_backup_only_gp' I = 2131165707


.field public static final 'switcher_auto_backup_only_gp_descr' I = 2131165708


.field public static final 'switcher_auto_integrate_update' I = 2131165709


.field public static final 'switcher_auto_integrate_update_descr' I = 2131165710


.field public static final 'switcher_auto_move_to_internal' I = 2131165711


.field public static final 'switcher_auto_move_to_internal_descr' I = 2131165712


.field public static final 'switcher_auto_move_to_sd' I = 2131165713


.field public static final 'switcher_auto_move_to_sd_descr' I = 2131165714


.field public static final 'sysdescr' I = 2131165715


.field public static final 'system_app_change' I = 2131165716


.field public static final 'systitle' I = 2131165717


.field public static final 'text_size' I = 2131165718


.field public static final 'title_for_block_internet' I = 2131165783


.field public static final 'title_for_select_integrate_update_apps' I = 2131165719


.field public static final 'title_for_select_move_to_internal' I = 2131165720


.field public static final 'title_for_select_move_to_sdcard' I = 2131165721


.field public static final 'title_for_select_uninstall' I = 2131165722


.field public static final 'title_for_unblock_internet' I = 2131165784


.field public static final 'title_upd' I = 2131165723


.field public static final 'toolbar_adfree' I = 2131165724


.field public static final 'toolbar_backups' I = 2131165725


.field public static final 'toolbar_rebuild' I = 2131165726


.field public static final 'toolbar_switchers' I = 2131165727


.field public static final 'toolbar_system_utils' I = 2131165728


.field public static final 'tools_menu_block_internet' I = 2131165785


.field public static final 'tools_menu_unblock_internet' I = 2131165786


.field public static final 'truble' I = 2131165729


.field public static final 'uninstall' I = 2131165730


.field public static final 'uninstallapp' I = 2131165731


.field public static final 'unknown_error' I = 2131165732


.field public static final 'unused_odex_delete' I = 2131165733


.field public static final 'update' I = 2131165734


.field public static final 'updatebusybox' I = 2131165735


.field public static final 'usepatch' I = 2131165736


.field public static final 'vers' I = 2131165737


.field public static final 'vibration' I = 2131165738


.field public static final 'vibration_descr' I = 2131165739


.field public static final 'viewlandscape' I = 2131165740


.field public static final 'viewlarge' I = 2131165741


.field public static final 'viewmedium' I = 2131165742


.field public static final 'viewmenu' I = 2131165743


.field public static final 'viewportret' I = 2131165744


.field public static final 'viewsensor' I = 2131165745


.field public static final 'viewsmall' I = 2131165746


.field public static final 'wait' I = 2131165747


.field public static final 'warning' I = 2131165748


.field public static final 'warning_inapp_emulation' I = 2131165787


.field public static final 'well_done' I = 2131165749


.field public static final 'xposed_module_option' I = 2131165750


.field public static final 'xposed_notify' I = 2131165751


.field public static final 'xposed_option_off' I = 2131165752


.field public static final 'xposed_settings' I = 2131165753


.field public static final 'xposeddescription' I = 2131165788


.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method
