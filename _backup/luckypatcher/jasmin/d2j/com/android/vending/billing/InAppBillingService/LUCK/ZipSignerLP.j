.bytecode 50.0
.class public synchronized com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP
.super java/lang/Object
.inner class public static AutoKeyObservable inner com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable outer com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP

.field private static final 'CERT_RSA_NAME' Ljava/lang/String; = "META-INF/CERT.RSA"

.field private static final 'CERT_SF_NAME' Ljava/lang/String; = "META-INF/CERT.SF"

.field public static final 'KEY_NONE' Ljava/lang/String; = "none"

.field public static final 'KEY_TESTKEY' Ljava/lang/String; = "testkey"

.field public static final 'MODE_AUTO' Ljava/lang/String; = "auto"

.field public static final 'MODE_AUTO_NONE' Ljava/lang/String; = "auto-none"

.field public static final 'MODE_AUTO_TESTKEY' Ljava/lang/String; = "auto-testkey"

.field public static final 'SUPPORTED_KEY_MODES' [Ljava/lang/String;

.field static 'log' Lkellinwood/logging/LoggerInterface;

.field private static 'stripPattern' Ljava/util/regex/Pattern;

.field 'autoKeyDetect' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"

.field 'autoKeyObservable' Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable;

.field private 'canceled' Z

.field 'keySet' Lkellinwood/security/zipsigner/KeySet;

.field 'keymode' Ljava/lang/String;

.field 'loadedKeys' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Lkellinwood/security/zipsigner/KeySet;>;"

.field private 'progressHelper' Lkellinwood/security/zipsigner/ProgressHelper;

.method static <clinit>()V
aconst_null
putstatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/log Lkellinwood/logging/LoggerInterface;
ldc "^META-INF/(.*)[.](SF|RSA|DSA)$"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
putstatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/stripPattern Ljava/util/regex/Pattern;
bipush 8
anewarray java/lang/String
dup
iconst_0
ldc "auto-testkey"
aastore
dup
iconst_1
ldc "auto"
aastore
dup
iconst_2
ldc "auto-none"
aastore
dup
iconst_3
ldc "media"
aastore
dup
iconst_4
ldc "platform"
aastore
dup
iconst_5
ldc "shared"
aastore
dup
bipush 6
ldc "testkey"
aastore
dup
bipush 7
ldc "none"
aastore
putstatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/SUPPORTED_KEY_MODES [Ljava/lang/String;
return
.limit locals 0
.limit stack 4
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
aload 0
new kellinwood/security/zipsigner/ProgressHelper
dup
invokespecial kellinwood/security/zipsigner/ProgressHelper/<init>()V
putfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/loadedKeys Ljava/util/Map;
aload 0
aconst_null
putfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
aload 0
ldc "testkey"
putfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keymode Ljava/lang/String;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/autoKeyDetect Ljava/util/Map;
aload 0
new com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable
dup
invokespecial com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable/<init>()V
putfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/autoKeyObservable Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/autoKeyDetect Ljava/util/Map;
ldc "aa9852bc5a53272ac8031d49b65e4b0e"
ldc "media"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/autoKeyDetect Ljava/util/Map;
ldc "e60418c4b638f20d0721e115674ca11f"
ldc "platform"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/autoKeyDetect Ljava/util/Map;
ldc "3e24e49741b60c215c010dc6048fca7d"
ldc "shared"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/autoKeyDetect Ljava/util/Map;
ldc "dab2cead827ef5313f28e22b6fa8479f"
ldc "testkey"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 1
.limit stack 3
.end method

.method private addDigestsToManifest(Ljava/util/Map;Ljava/util/ArrayList;)Ljava/util/jar/Manifest;
.signature "(Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;)Ljava/util/jar/Manifest;"
.throws java/io/IOException
.throws java/security/GeneralSecurityException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L4
aconst_null
astore 5
aload 1
ldc "META-INF/MANIFEST.MF"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/zipio/ZioEntry
astore 6
aload 6
ifnull L5
new java/util/jar/Manifest
dup
invokespecial java/util/jar/Manifest/<init>()V
astore 5
aload 5
aload 6
invokevirtual kellinwood/zipio/ZioEntry/getInputStream()Ljava/io/InputStream;
invokevirtual java/util/jar/Manifest/read(Ljava/io/InputStream;)V
L5:
new java/util/jar/Manifest
dup
invokespecial java/util/jar/Manifest/<init>()V
astore 8
aload 8
invokevirtual java/util/jar/Manifest/getMainAttributes()Ljava/util/jar/Attributes;
astore 6
aload 5
ifnull L6
aload 6
aload 5
invokevirtual java/util/jar/Manifest/getMainAttributes()Ljava/util/jar/Attributes;
invokevirtual java/util/jar/Attributes/putAll(Ljava/util/Map;)V
L7:
ldc "SHA1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 9
sipush 512
newarray byte
astore 10
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
astore 6
aload 6
aload 1
invokevirtual java/util/TreeMap/putAll(Ljava/util/Map;)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
istore 4
iload 4
ifeq L8
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Manifest entries:"
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L8:
aload 6
invokevirtual java/util/TreeMap/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 11
L9:
aload 11
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L10
aload 11
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast kellinwood/zipio/ZioEntry
astore 1
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
ifeq L11
L10:
aload 8
areturn
L6:
aload 6
ldc "Manifest-Version"
ldc "1.0"
invokevirtual java/util/jar/Attributes/putValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
pop
aload 6
ldc "Created-By"
ldc "1.0 (Android SignApk)"
invokevirtual java/util/jar/Attributes/putValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
pop
goto L7
L11:
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
astore 12
iload 4
ifeq L12
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
aload 12
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L12:
aload 1
invokevirtual kellinwood/zipio/ZioEntry/isDirectory()Z
ifne L9
aload 12
ldc "META-INF/MANIFEST.MF"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L9
aload 12
ldc "META-INF/CERT.SF"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L9
aload 12
ldc "META-INF/CERT.RSA"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L9
getstatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/stripPattern Ljava/util/regex/Pattern;
ifnull L13
getstatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/stripPattern Ljava/util/regex/Pattern;
aload 12
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
invokevirtual java/util/regex/Matcher/matches()Z
ifne L9
L13:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_0
ldc "Generating manifest"
invokevirtual kellinwood/security/zipsigner/ProgressHelper/progress(ILjava/lang/String;)V
aload 1
invokevirtual kellinwood/zipio/ZioEntry/getInputStream()Ljava/io/InputStream;
astore 1
aload 2
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 13
L14:
aload 13
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L15
aload 13
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem
astore 7
aload 12
aload 7
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
aload 7
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/basePath Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L14
L0:
new java/io/FileInputStream
dup
aload 7
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 6
L1:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher (signer): Additional files to manifest added! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L3:
aload 6
astore 1
goto L14
L2:
astore 6
L16:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 6
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
goto L14
L15:
aload 1
aload 10
invokevirtual java/io/InputStream/read([B)I
istore 3
iload 3
ifle L17
aload 9
aload 10
iconst_0
iload 3
invokevirtual java/security/MessageDigest/update([BII)V
goto L15
L17:
aload 1
invokevirtual java/io/InputStream/close()V
aconst_null
astore 6
aload 6
astore 1
aload 5
ifnull L18
aload 5
aload 12
invokevirtual java/util/jar/Manifest/getAttributes(Ljava/lang/String;)Ljava/util/jar/Attributes;
astore 7
aload 6
astore 1
aload 7
ifnull L18
new java/util/jar/Attributes
dup
aload 7
invokespecial java/util/jar/Attributes/<init>(Ljava/util/jar/Attributes;)V
astore 1
L18:
aload 1
astore 6
aload 1
ifnonnull L19
new java/util/jar/Attributes
dup
invokespecial java/util/jar/Attributes/<init>()V
astore 6
L19:
aload 6
ldc "SHA1-Digest"
aload 9
invokevirtual java/security/MessageDigest/digest()[B
invokestatic kellinwood/security/zipsigner/Base64/encode([B)Ljava/lang/String;
invokevirtual java/util/jar/Attributes/putValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
pop
aload 8
invokevirtual java/util/jar/Manifest/getEntries()Ljava/util/Map;
aload 12
aload 6
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L9
L4:
astore 7
aload 6
astore 1
aload 7
astore 6
goto L16
.limit locals 14
.limit stack 4
.end method

.method private copyFiles(Ljava/util/Map;Lkellinwood/zipio/ZipOutput;Ljava/util/ArrayList;)V
.signature "(Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;Lkellinwood/zipio/ZipOutput;Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;)V"
.throws java/io/IOException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L11 to L12 using L2
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L15 to L16 using L2
.catch java/lang/Exception from L17 to L18 using L2
.catch java/lang/Exception from L19 to L20 using L2
.catch java/lang/Exception from L21 to L22 using L2
.catch java/lang/Exception from L23 to L24 using L2
.catch java/lang/Exception from L25 to L26 using L2
.catch java/lang/Exception from L27 to L28 using L2
.catch java/lang/Exception from L29 to L30 using L2
.catch java/lang/Exception from L31 to L32 using L2
.catch java/lang/Exception from L33 to L34 using L2
aload 1
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L35:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L36
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast kellinwood/zipio/ZioEntry
astore 7
aload 7
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
astore 8
iconst_0
istore 4
aload 3
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 9
L37:
aload 9
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L38
aload 9
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem
astore 10
aload 8
aload 10
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
aload 10
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/basePath Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L37
iload 4
istore 5
L0:
new java/io/File
dup
aload 10
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 11
L1:
iload 4
istore 5
L3:
sipush 8192
newarray byte
astore 12
L4:
iload 4
istore 5
L5:
new java/io/FileInputStream
dup
aload 10
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 13
L6:
iload 4
istore 5
L7:
new kellinwood/zipio/ZioEntry
dup
aload 8
invokespecial kellinwood/zipio/ZioEntry/<init>(Ljava/lang/String;)V
astore 14
L8:
iload 4
istore 5
L9:
aload 14
aload 7
invokevirtual kellinwood/zipio/ZioEntry/getCompression()S
invokevirtual kellinwood/zipio/ZioEntry/setCompression(I)V
L10:
iload 4
istore 5
L11:
aload 14
aload 7
invokevirtual kellinwood/zipio/ZioEntry/getTime()J
invokevirtual kellinwood/zipio/ZioEntry/setTime(J)V
L12:
iload 4
istore 5
L13:
aload 14
invokevirtual kellinwood/zipio/ZioEntry/getOutputStream()Ljava/io/OutputStream;
astore 15
L14:
iload 4
istore 5
L15:
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 16
L16:
iload 4
istore 5
L17:
aload 16
invokevirtual java/util/zip/CRC32/reset()V
L18:
iload 4
istore 5
L19:
aload 13
aload 12
invokevirtual java/io/FileInputStream/read([B)I
istore 6
L20:
iload 6
ifle L39
iload 4
istore 5
L21:
aload 15
aload 12
iconst_0
iload 6
invokevirtual java/io/OutputStream/write([BII)V
L22:
iload 4
istore 5
L23:
aload 16
aload 12
iconst_0
iload 6
invokevirtual java/util/zip/CRC32/update([BII)V
L24:
goto L18
L2:
astore 10
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 10
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
iload 5
istore 4
goto L37
L39:
iload 4
istore 5
L25:
aload 15
invokevirtual java/io/OutputStream/flush()V
L26:
iload 4
istore 5
L27:
aload 13
invokevirtual java/io/FileInputStream/close()V
L28:
iload 4
istore 5
L29:
aload 2
aload 14
invokevirtual kellinwood/zipio/ZipOutput/write(Lkellinwood/zipio/ZioEntry;)V
L30:
iconst_1
istore 6
iconst_1
istore 4
iload 6
istore 5
L31:
aload 11
invokevirtual java/io/File/delete()Z
pop
L32:
iload 6
istore 5
L33:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher (signer): Additional files added! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L34:
goto L37
L38:
iload 4
ifne L35
aload 2
aload 7
invokevirtual kellinwood/zipio/ZipOutput/write(Lkellinwood/zipio/ZioEntry;)V
goto L35
L36:
return
.limit locals 17
.limit stack 4
.end method

.method private copyFiles(Ljava/util/jar/Manifest;Ljava/util/Map;Ljava/util/jar/JarOutputStream;JLjava/util/ArrayList;)V
.signature "(Ljava/util/jar/Manifest;Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;Ljava/util/jar/JarOutputStream;JLjava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;)V"
.throws java/io/IOException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L5
.catch java/lang/Exception from L8 to L9 using L5
.catch java/lang/Exception from L10 to L11 using L5
.catch java/lang/Exception from L12 to L13 using L5
.catch java/lang/Exception from L14 to L15 using L16
.catch java/lang/Exception from L17 to L18 using L5
.catch java/lang/Exception from L19 to L20 using L5
.catch java/lang/Exception from L21 to L22 using L5
.catch java/lang/Exception from L23 to L24 using L5
.catch java/lang/Exception from L25 to L26 using L5
.catch java/lang/Exception from L27 to L28 using L5
.catch java/lang/Exception from L29 to L30 using L5
.catch java/lang/Exception from L31 to L32 using L2
.catch java/lang/Exception from L32 to L33 using L2
.catch java/lang/Exception from L34 to L35 using L36
.catch java/lang/Exception from L37 to L38 using L36
.catch java/lang/Exception from L39 to L40 using L36
.catch java/lang/Exception from L41 to L42 using L36
.catch java/lang/Exception from L43 to L44 using L36
.catch java/lang/Exception from L45 to L46 using L36
.catch java/lang/Exception from L47 to L48 using L2
.catch java/lang/Exception from L49 to L50 using L2
.catch java/lang/Exception from L51 to L52 using L5
.catch java/lang/Exception from L53 to L54 using L5
.catch java/lang/Exception from L55 to L56 using L36
.catch java/lang/Exception from L57 to L58 using L36
.catch java/lang/Exception from L59 to L60 using L36
.catch java/lang/Exception from L61 to L62 using L36
.catch java/lang/Exception from L63 to L64 using L2
.catch java/lang/Exception from L64 to L65 using L2
.catch java/lang/Exception from L66 to L67 using L2
.catch java/lang/Exception from L68 to L69 using L2
new java/util/ArrayList
dup
aload 1
invokevirtual java/util/jar/Manifest/getEntries()Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
astore 1
aload 1
invokestatic java/util/Collections/sort(Ljava/util/List;)V
aload 1
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 12
L70:
aload 12
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L71
aload 12
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 13
aload 2
aload 13
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/zipio/ZioEntry
astore 14
L0:
aload 2
aload 13
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/zipio/ZioEntry
invokevirtual kellinwood/zipio/ZioEntry/getCompression()S
ifne L49
new java/util/jar/JarEntry
dup
aload 2
aload 13
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/zipio/ZioEntry
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
invokespecial java/util/jar/JarEntry/<init>(Ljava/lang/String;)V
astore 1
L1:
aload 1
astore 11
L3:
aload 1
iconst_0
invokevirtual java/util/jar/JarEntry/setMethod(I)V
L4:
iconst_0
istore 7
aload 1
astore 11
L6:
aload 6
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 15
L7:
aload 1
astore 11
L8:
aload 15
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L72
L9:
aload 1
astore 11
L10:
aload 15
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem
astore 16
L11:
aload 1
astore 11
L12:
aload 13
aload 16
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
aload 16
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/basePath Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
istore 10
L13:
iload 10
ifeq L7
L14:
new java/io/File
dup
aload 16
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 17
aload 17
invokevirtual java/io/File/length()J
l2i
newarray byte
astore 11
new java/io/FileInputStream
dup
aload 16
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 16
aload 16
aload 11
invokevirtual java/io/FileInputStream/read([B)I
pop
aload 16
invokevirtual java/io/FileInputStream/close()V
aload 1
aload 17
invokevirtual java/io/File/length()J
invokevirtual java/util/jar/JarEntry/setCompressedSize(J)V
aload 1
aload 17
invokevirtual java/io/File/length()J
invokevirtual java/util/jar/JarEntry/setSize(J)V
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 16
aload 16
aload 11
invokevirtual java/util/zip/CRC32/update([B)V
aload 1
aload 16
invokevirtual java/util/zip/CRC32/getValue()J
invokevirtual java/util/jar/JarEntry/setCrc(J)V
aload 1
aload 2
aload 13
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/zipio/ZioEntry
invokevirtual kellinwood/zipio/ZioEntry/getTime()J
invokevirtual java/util/jar/JarEntry/setTime(J)V
L15:
iconst_1
istore 7
goto L7
L16:
astore 16
aload 1
astore 11
L17:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 16
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
L18:
goto L7
L5:
astore 1
L73:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 1
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
goto L70
L72:
iload 7
ifne L30
aload 1
astore 11
L19:
aload 1
aload 2
aload 13
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/zipio/ZioEntry
invokevirtual kellinwood/zipio/ZioEntry/getSize()I
i2l
invokevirtual java/util/jar/JarEntry/setCompressedSize(J)V
L20:
aload 1
astore 11
L21:
aload 1
aload 2
aload 13
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/zipio/ZioEntry
invokevirtual kellinwood/zipio/ZioEntry/getSize()I
i2l
invokevirtual java/util/jar/JarEntry/setSize(J)V
L22:
aload 1
astore 11
L23:
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 15
L24:
aload 1
astore 11
L25:
aload 15
aload 14
invokevirtual kellinwood/zipio/ZioEntry/getData()[B
invokevirtual java/util/zip/CRC32/update([B)V
L26:
aload 1
astore 11
L27:
aload 1
aload 15
invokevirtual java/util/zip/CRC32/getValue()J
invokevirtual java/util/jar/JarEntry/setCrc(J)V
L28:
aload 1
astore 11
L29:
aload 1
aload 2
aload 13
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/zipio/ZioEntry
invokevirtual kellinwood/zipio/ZioEntry/getTime()J
invokevirtual java/util/jar/JarEntry/setTime(J)V
L30:
iconst_0
istore 7
L31:
aload 6
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 11
L32:
aload 11
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L74
aload 11
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem
astore 15
aload 13
aload 15
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
aload 15
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/basePath Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
istore 10
L33:
iload 10
ifeq L32
iload 7
istore 8
L34:
new java/io/File
dup
aload 15
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 16
L35:
iload 7
istore 8
L37:
sipush 8192
newarray byte
astore 17
L38:
iload 7
istore 8
L39:
new java/io/FileInputStream
dup
aload 15
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 18
L40:
iload 7
istore 8
L41:
aload 3
aload 1
invokevirtual java/util/jar/JarOutputStream/putNextEntry(Ljava/util/zip/ZipEntry;)V
L42:
iload 7
istore 8
L43:
aload 18
aload 17
invokevirtual java/io/FileInputStream/read([B)I
istore 9
L44:
iload 9
ifle L75
iload 7
istore 8
L45:
aload 3
aload 17
iconst_0
iload 9
invokevirtual java/util/jar/JarOutputStream/write([BII)V
L46:
goto L42
L36:
astore 15
L47:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 15
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
L48:
iload 8
istore 7
goto L32
L49:
new java/util/jar/JarEntry
dup
aload 13
invokespecial java/util/jar/JarEntry/<init>(Ljava/lang/String;)V
astore 1
L50:
aload 1
astore 11
L51:
aload 1
aload 2
aload 13
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/zipio/ZioEntry
invokevirtual kellinwood/zipio/ZioEntry/getTime()J
invokevirtual java/util/jar/JarEntry/setTime(J)V
L52:
aload 1
astore 11
L53:
aload 1
aload 14
invokevirtual kellinwood/zipio/ZioEntry/getCompression()S
invokevirtual java/util/jar/JarEntry/setMethod(I)V
L54:
goto L30
L75:
iload 7
istore 8
L55:
aload 3
invokevirtual java/util/jar/JarOutputStream/flush()V
L56:
iload 7
istore 8
L57:
aload 18
invokevirtual java/io/FileInputStream/close()V
L58:
iconst_1
istore 9
iconst_1
istore 7
iload 9
istore 8
L59:
aload 16
invokevirtual java/io/File/delete()Z
pop
L60:
iload 9
istore 8
L61:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher (signer): Additional files added! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 15
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L62:
goto L32
L74:
iload 7
ifne L70
L63:
aload 3
aload 1
invokevirtual java/util/jar/JarOutputStream/putNextEntry(Ljava/util/zip/ZipEntry;)V
aload 14
invokevirtual kellinwood/zipio/ZioEntry/getInputStream()Ljava/io/InputStream;
astore 1
sipush 8192
newarray byte
astore 11
L64:
aload 1
aload 11
invokevirtual java/io/InputStream/read([B)I
istore 7
L65:
iload 7
ifle L68
L66:
aload 3
aload 11
iconst_0
iload 7
invokevirtual java/util/jar/JarOutputStream/write([BII)V
L67:
goto L64
L68:
aload 3
invokevirtual java/util/jar/JarOutputStream/flush()V
L69:
goto L70
L71:
return
L2:
astore 1
goto L73
.limit locals 19
.limit stack 4
.end method

.method private decryptPrivateKey([BLjava/lang/String;)Ljava/security/spec/KeySpec;
.throws java/security/GeneralSecurityException
.catch java/io/IOException from L0 to L1 using L2
.catch java/security/spec/InvalidKeySpecException from L3 to L4 using L5
L0:
new javax/crypto/EncryptedPrivateKeyInfo
dup
aload 1
invokespecial javax/crypto/EncryptedPrivateKeyInfo/<init>([B)V
astore 1
L1:
aload 2
invokevirtual java/lang/String/toCharArray()[C
astore 2
aload 1
invokevirtual javax/crypto/EncryptedPrivateKeyInfo/getAlgName()Ljava/lang/String;
invokestatic javax/crypto/SecretKeyFactory/getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;
new javax/crypto/spec/PBEKeySpec
dup
aload 2
invokespecial javax/crypto/spec/PBEKeySpec/<init>([C)V
invokevirtual javax/crypto/SecretKeyFactory/generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;
astore 2
aload 1
invokevirtual javax/crypto/EncryptedPrivateKeyInfo/getAlgName()Ljava/lang/String;
invokestatic javax/crypto/Cipher/getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
astore 3
aload 3
iconst_2
aload 2
aload 1
invokevirtual javax/crypto/EncryptedPrivateKeyInfo/getAlgParameters()Ljava/security/AlgorithmParameters;
invokevirtual javax/crypto/Cipher/init(ILjava/security/Key;Ljava/security/AlgorithmParameters;)V
L3:
aload 1
aload 3
invokevirtual javax/crypto/EncryptedPrivateKeyInfo/getKeySpec(Ljavax/crypto/Cipher;)Ljava/security/spec/PKCS8EncodedKeySpec;
astore 1
L4:
aload 1
areturn
L2:
astore 1
aconst_null
areturn
L5:
astore 1
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "signapk: Password for private key may be bad."
invokeinterface kellinwood/logging/LoggerInterface/error(Ljava/lang/String;)V 1
aload 1
athrow
.limit locals 4
.limit stack 4
.end method

.method private generateSignatureFile(Ljava/util/jar/Manifest;Ljava/io/OutputStream;)V
.throws java/io/IOException
.throws java/security/GeneralSecurityException
aload 2
ldc "Signature-Version: 1.0\r\n"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/OutputStream/write([B)V
aload 2
ldc "Created-By: 1.0 (Android SignApk)\r\n"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/OutputStream/write([B)V
ldc "SHA1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 3
new java/io/PrintStream
dup
new java/security/DigestOutputStream
dup
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
aload 3
invokespecial java/security/DigestOutputStream/<init>(Ljava/io/OutputStream;Ljava/security/MessageDigest;)V
iconst_1
ldc "UTF-8"
invokespecial java/io/PrintStream/<init>(Ljava/io/OutputStream;ZLjava/lang/String;)V
astore 4
aload 1
aload 4
invokevirtual java/util/jar/Manifest/write(Ljava/io/OutputStream;)V
aload 4
invokevirtual java/io/PrintStream/flush()V
aload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "SHA1-Digest-Manifest: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/security/MessageDigest/digest()[B
invokestatic kellinwood/security/zipsigner/Base64/encode([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\r\n\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/OutputStream/write([B)V
aload 1
invokevirtual java/util/jar/Manifest/getEntries()Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 6
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
ifeq L2
L1:
return
L2:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_0
ldc "Generating signature file"
invokevirtual kellinwood/security/zipsigner/ProgressHelper/progress(ILjava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Name: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 5
aload 4
aload 5
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
aload 6
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/util/jar/Attributes
invokevirtual java/util/jar/Attributes/entrySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 6
L3:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 7
aload 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 7
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
goto L3
L4:
aload 4
ldc "\r\n"
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
aload 4
invokevirtual java/io/PrintStream/flush()V
aload 2
aload 5
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/OutputStream/write([B)V
aload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "SHA1-Digest: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/security/MessageDigest/digest()[B
invokestatic kellinwood/security/zipsigner/Base64/encode([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\r\n\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/OutputStream/write([B)V
goto L0
.limit locals 8
.limit stack 6
.end method

.method public static getLogger()Lkellinwood/logging/LoggerInterface;
getstatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/log Lkellinwood/logging/LoggerInterface;
ifnonnull L0
ldc com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic kellinwood/logging/LoggerManager/getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;
putstatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/log Lkellinwood/logging/LoggerInterface;
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/log Lkellinwood/logging/LoggerInterface;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static getSupportedKeyModes()[Ljava/lang/String;
getstatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/SUPPORTED_KEY_MODES [Ljava/lang/String;
areturn
.limit locals 0
.limit stack 1
.end method

.method private writeSignatureBlock([B[BLjava/security/cert/X509Certificate;Ljava/io/OutputStream;)V
.throws java/io/IOException
.throws java/security/GeneralSecurityException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L2 using L2
.catch java/lang/Exception from L4 to L5 using L2
aload 1
ifnull L0
aload 4
aload 1
invokevirtual java/io/OutputStream/write([B)V
aload 4
aload 2
invokevirtual java/io/OutputStream/write([B)V
return
L0:
ldc "kellinwood.sigblock.SignatureBlockWriter"
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
ldc "writeSignatureBlock"
iconst_3
anewarray java/lang/Class
dup
iconst_0
iconst_1
newarray byte
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
aastore
dup
iconst_1
ldc java/security/cert/X509Certificate
aastore
dup
iconst_2
ldc java/io/OutputStream
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
astore 1
L1:
aload 1
ifnonnull L4
L3:
new java/lang/IllegalStateException
dup
ldc "writeSignatureBlock() method not found."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
aload 1
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
aload 1
invokeinterface kellinwood/logging/LoggerInterface/error(Ljava/lang/String;Ljava/lang/Throwable;)V 2
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Failed to invoke writeSignatureBlock(): "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 1
aconst_null
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
dup
iconst_1
aload 3
aastore
dup
iconst_2
aload 4
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L5:
return
.limit locals 5
.limit stack 6
.end method

.method public addAutoKeyObserver(Ljava/util/Observer;)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/autoKeyObservable Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable;
aload 1
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable/addObserver(Ljava/util/Observer;)V
return
.limit locals 2
.limit stack 2
.end method

.method public addProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
aload 1
invokevirtual kellinwood/security/zipsigner/ProgressHelper/addProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected autoDetectKey(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
.signature "(Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;)Ljava/lang/String;"
.throws java/security/NoSuchAlgorithmException
.throws java/io/IOException
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
istore 5
aload 1
ldc "auto"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L0
aload 1
areturn
L0:
aconst_null
astore 6
aload 2
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 7
aload 6
astore 2
L1:
aload 7
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 7
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 6
aload 6
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/String
astore 8
aload 8
ldc "META-INF/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L1
aload 8
ldc ".RSA"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L1
ldc "MD5"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 8
aload 6
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast kellinwood/zipio/ZioEntry
invokevirtual kellinwood/zipio/ZioEntry/getData()[B
astore 6
aload 6
arraylength
sipush 1458
if_icmpge L3
L2:
aload 1
ldc "auto-testkey"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
iload 5
ifeq L5
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Falling back to key="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L5:
ldc "testkey"
areturn
L3:
aload 8
aload 6
iconst_0
sipush 1458
invokevirtual java/security/MessageDigest/update([BII)V
aload 8
invokevirtual java/security/MessageDigest/digest()[B
astore 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 6
aload 2
arraylength
istore 4
iconst_0
istore 3
L6:
iload 3
iload 4
if_icmpge L7
aload 6
ldc "%02x"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 2
iload 3
baload
invokestatic java/lang/Byte/valueOf(B)Ljava/lang/Byte;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 3
iconst_1
iadd
istore 3
goto L6
L7:
aload 6
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/autoKeyDetect Ljava/util/Map;
aload 2
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 6
iload 5
ifeq L8
aload 6
ifnull L9
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Auto-determined key=%s using md5=%s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 6
aastore
dup
iconst_1
aload 2
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L8:
aload 6
astore 2
aload 6
ifnull L1
aload 6
areturn
L9:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Auto key determination failed for md5=%s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
goto L8
L4:
aload 1
ldc "auto-none"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L10
iload 5
ifeq L11
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
ldc "Unable to determine key, returning: none"
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L11:
ldc "none"
areturn
L10:
aconst_null
areturn
.limit locals 9
.limit stack 7
.end method

.method public cancel()V
aload 0
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
return
.limit locals 1
.limit stack 2
.end method

.method public getKeySet()Lkellinwood/security/zipsigner/KeySet;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getKeymode()Ljava/lang/String;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keymode Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public isCanceled()Z
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public loadKeys(Ljava/lang/String;)V
.throws java/io/IOException
.throws java/security/GeneralSecurityException
aload 0
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/loadedKeys Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast kellinwood/security/zipsigner/KeySet
putfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
ifnull L0
L1:
return
L0:
aload 0
new kellinwood/security/zipsigner/KeySet
dup
invokespecial kellinwood/security/zipsigner/KeySet/<init>()V
putfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
aload 1
invokevirtual kellinwood/security/zipsigner/KeySet/setName(Ljava/lang/String;)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/loadedKeys Ljava/util/Map;
aload 1
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
ldc "none"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_1
ldc "Loading certificate and private key"
invokevirtual kellinwood/security/zipsigner/ProgressHelper/progress(ILjava/lang/String;)V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/keys/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".pk8"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Class/getResource(Ljava/lang/String;)Ljava/net/URL;
astore 2
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
aload 0
aload 2
aconst_null
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/readPrivateKey(Ljava/net/URL;Ljava/lang/String;)Ljava/security/PrivateKey;
invokevirtual kellinwood/security/zipsigner/KeySet/setPrivateKey(Ljava/security/PrivateKey;)V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/keys/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".x509.pem"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Class/getResource(Ljava/lang/String;)Ljava/net/URL;
astore 2
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
aload 0
aload 2
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/readPublicKey(Ljava/net/URL;)Ljava/security/cert/X509Certificate;
invokevirtual kellinwood/security/zipsigner/KeySet/setPublicKey(Ljava/security/cert/X509Certificate;)V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/keys/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".sbt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Class/getResource(Ljava/lang/String;)Ljava/net/URL;
astore 1
aload 1
ifnull L1
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
aload 0
aload 1
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/readContentAsBytes(Ljava/net/URL;)[B
invokevirtual kellinwood/security/zipsigner/KeySet/setSigBlockTemplate([B)V
return
.limit locals 3
.limit stack 4
.end method

.method public loadProvider(Ljava/lang/String;)V
.throws java/lang/ClassNotFoundException
.throws java/lang/IllegalAccessException
.throws java/lang/InstantiationException
aload 1
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
invokevirtual java/lang/Class/newInstance()Ljava/lang/Object;
checkcast java/security/Provider
iconst_1
invokestatic java/security/Security/insertProviderAt(Ljava/security/Provider;I)I
pop
return
.limit locals 2
.limit stack 2
.end method

.method public readContentAsBytes(Ljava/io/InputStream;)[B
.throws java/io/IOException
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 3
sipush 2048
newarray byte
astore 4
aload 1
aload 4
invokevirtual java/io/InputStream/read([B)I
istore 2
L0:
iload 2
iconst_m1
if_icmpeq L1
aload 3
aload 4
iconst_0
iload 2
invokevirtual java/io/ByteArrayOutputStream/write([BII)V
aload 1
aload 4
invokevirtual java/io/InputStream/read([B)I
istore 2
goto L0
L1:
aload 3
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
areturn
.limit locals 5
.limit stack 4
.end method

.method public readContentAsBytes(Ljava/net/URL;)[B
.throws java/io/IOException
aload 0
aload 1
invokevirtual java/net/URL/openStream()Ljava/io/InputStream;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/readContentAsBytes(Ljava/io/InputStream;)[B
areturn
.limit locals 2
.limit stack 2
.end method

.method public readPrivateKey(Ljava/net/URL;Ljava/lang/String;)Ljava/security/PrivateKey;
.throws java/io/IOException
.throws java/security/GeneralSecurityException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch java/security/spec/InvalidKeySpecException from L4 to L5 using L6
.catch all from L4 to L5 using L2
.catch all from L7 to L8 using L2
new java/io/DataInputStream
dup
aload 1
invokevirtual java/net/URL/openStream()Ljava/io/InputStream;
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
astore 3
L0:
aload 0
aload 3
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/readContentAsBytes(Ljava/io/InputStream;)[B
astore 4
aload 0
aload 4
aload 2
invokespecial com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/decryptPrivateKey([BLjava/lang/String;)Ljava/security/spec/KeySpec;
astore 2
L1:
aload 2
astore 1
aload 2
ifnonnull L4
L3:
new java/security/spec/PKCS8EncodedKeySpec
dup
aload 4
invokespecial java/security/spec/PKCS8EncodedKeySpec/<init>([B)V
astore 1
L4:
ldc "RSA"
invokestatic java/security/KeyFactory/getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;
aload 1
invokevirtual java/security/KeyFactory/generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
astore 2
L5:
aload 3
invokevirtual java/io/DataInputStream/close()V
aload 2
areturn
L6:
astore 2
L7:
ldc "DSA"
invokestatic java/security/KeyFactory/getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;
aload 1
invokevirtual java/security/KeyFactory/generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
astore 1
L8:
aload 3
invokevirtual java/io/DataInputStream/close()V
aload 1
areturn
L2:
astore 1
aload 3
invokevirtual java/io/DataInputStream/close()V
aload 1
athrow
.limit locals 5
.limit stack 3
.end method

.method public readPublicKey(Ljava/net/URL;)Ljava/security/cert/X509Certificate;
.throws java/io/IOException
.throws java/security/GeneralSecurityException
.catch all from L0 to L1 using L2
aload 1
invokevirtual java/net/URL/openStream()Ljava/io/InputStream;
astore 1
L0:
ldc "X.509"
invokestatic java/security/cert/CertificateFactory/getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;
aload 1
invokevirtual java/security/cert/CertificateFactory/generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
checkcast java/security/cert/X509Certificate
astore 2
L1:
aload 1
invokevirtual java/io/InputStream/close()V
aload 2
areturn
L2:
astore 2
aload 1
invokevirtual java/io/InputStream/close()V
aload 2
athrow
.limit locals 3
.limit stack 2
.end method

.method public removeProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
aload 1
invokevirtual kellinwood/security/zipsigner/ProgressHelper/removeProgressListener(Lkellinwood/security/zipsigner/ProgressListener;)V
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public resetCanceled()V
aload 0
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
return
.limit locals 1
.limit stack 2
.end method

.method public setKeymode(Ljava/lang/String;)V
.throws java/io/IOException
.throws java/security/GeneralSecurityException
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
ifeq L0
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "setKeymode: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L0:
aload 0
aload 1
putfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keymode Ljava/lang/String;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keymode Ljava/lang/String;
ldc "auto"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L1
aload 0
aconst_null
putfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
return
L1:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
invokevirtual kellinwood/security/zipsigner/ProgressHelper/initProgress()V
aload 0
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keymode Ljava/lang/String;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/loadKeys(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method public setKeys(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V
aload 0
new kellinwood/security/zipsigner/KeySet
dup
aload 1
aload 2
aload 3
aload 4
invokespecial kellinwood/security/zipsigner/KeySet/<init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V
putfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
return
.limit locals 5
.limit stack 7
.end method

.method public signZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
.signature "(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;)V"
.throws java/io/IOException
.throws java/security/GeneralSecurityException
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getCanonicalFile()Ljava/io/File;
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getCanonicalFile()Ljava/io/File;
invokevirtual java/io/File/equals(Ljava/lang/Object;)Z
ifeq L0
new java/lang/IllegalArgumentException
dup
ldc "Input and output files are the same.  Specify a different name for the output."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
invokevirtual kellinwood/security/zipsigner/ProgressHelper/initProgress()V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_1
ldc "Parsing the input's central directory"
invokevirtual kellinwood/security/zipsigner/ProgressHelper/progress(ILjava/lang/String;)V
aload 0
aload 1
invokestatic kellinwood/zipio/ZipInput/read(Ljava/lang/String;)Lkellinwood/zipio/ZipInput;
invokevirtual kellinwood/zipio/ZipInput/getEntries()Ljava/util/Map;
new java/io/FileOutputStream
dup
aload 2
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
aload 2
aload 3
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/signZip(Ljava/util/Map;Ljava/io/OutputStream;Ljava/lang/String;Ljava/util/ArrayList;)V
return
.limit locals 4
.limit stack 5
.end method

.method public signZip(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
.signature "(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;)V"
.throws java/lang/ClassNotFoundException
.throws java/lang/IllegalAccessException
.throws java/lang/InstantiationException
.throws java/io/IOException
.throws java/security/GeneralSecurityException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
.catch all from L11 to L12 using L2
aconst_null
astore 10
aload 2
astore 9
aload 2
ifnonnull L1
aload 10
astore 2
L0:
invokestatic java/security/KeyStore/getDefaultType()Ljava/lang/String;
astore 9
L1:
aload 10
astore 2
L3:
aload 9
invokestatic java/security/KeyStore/getInstance(Ljava/lang/String;)Ljava/security/KeyStore;
astore 9
L4:
aload 10
astore 2
L5:
aload 1
invokevirtual java/net/URL/openStream()Ljava/io/InputStream;
astore 1
L6:
aload 1
astore 2
L7:
aload 9
aload 1
aload 3
invokevirtual java/lang/String/toCharArray()[C
invokevirtual java/security/KeyStore/load(Ljava/io/InputStream;[C)V
L8:
aload 1
astore 2
L9:
aload 0
ldc "custom"
aload 9
aload 4
invokevirtual java/security/KeyStore/getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;
checkcast java/security/cert/X509Certificate
aload 9
aload 4
aload 5
invokevirtual java/lang/String/toCharArray()[C
invokevirtual java/security/KeyStore/getKey(Ljava/lang/String;[C)Ljava/security/Key;
checkcast java/security/PrivateKey
aconst_null
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/setKeys(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V
L10:
aload 1
astore 2
L11:
aload 0
aload 6
aload 7
aload 8
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/signZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
L12:
aload 1
ifnull L13
aload 1
invokevirtual java/io/InputStream/close()V
L13:
return
L2:
astore 1
aload 2
ifnull L14
aload 2
invokevirtual java/io/InputStream/close()V
L14:
aload 1
athrow
.limit locals 11
.limit stack 6
.end method

.method public signZip(Ljava/util/Map;Ljava/io/OutputStream;Ljava/lang/String;Ljava/util/ArrayList;)V
.signature "(Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;Ljava/io/OutputStream;Ljava/lang/String;Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;)V"
.throws java/io/IOException
.throws java/security/GeneralSecurityException
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L4
.catch all from L3 to L5 using L6
.catch java/lang/Throwable from L7 to L8 using L9
.catch all from L10 to L11 using L6
.catch all from L11 to L12 using L6
.catch all from L13 to L14 using L6
.catch java/lang/Throwable from L15 to L16 using L17
.catch all from L18 to L19 using L6
.catch java/lang/Throwable from L20 to L21 using L22
.catch all from L23 to L24 using L6
.catch all from L25 to L26 using L6
.catch all from L26 to L27 using L6
.catch all from L28 to L29 using L6
.catch all from L29 to L30 using L6
.catch java/lang/Throwable from L31 to L32 using L33
.catch all from L34 to L35 using L6
.catch java/lang/Throwable from L36 to L37 using L38
.catch java/lang/Throwable from L39 to L40 using L41
.catch java/lang/Throwable from L42 to L43 using L44
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
invokeinterface kellinwood/logging/LoggerInterface/isDebugEnabled()Z 0
istore 6
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
invokevirtual kellinwood/security/zipsigner/ProgressHelper/initProgress()V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
ifnonnull L0
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keymode Ljava/lang/String;
ldc "auto"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L45
new java/lang/IllegalStateException
dup
ldc "No keys configured for signing the file!"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L45:
aload 0
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keymode Ljava/lang/String;
aload 1
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/autoDetectKey(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
astore 10
aload 10
ifnonnull L46
new kellinwood/security/zipsigner/AutoKeyException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unable to auto-select key for signing "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial kellinwood/security/zipsigner/AutoKeyException/<init>(Ljava/lang/String;)V
athrow
L46:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/autoKeyObservable Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable;
aload 10
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP$AutoKeyObservable/notifyObservers(Ljava/lang/Object;)V
aload 0
aload 10
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/loadKeys(Ljava/lang/String;)V
L0:
new kellinwood/zipio/ZipOutput
dup
aload 2
invokespecial kellinwood/zipio/ZipOutput/<init>(Ljava/io/OutputStream;)V
astore 10
L1:
new java/util/jar/JarOutputStream
dup
new java/io/FileOutputStream
dup
aload 3
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
invokespecial java/util/jar/JarOutputStream/<init>(Ljava/io/OutputStream;)V
astore 2
L3:
ldc "none"
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
invokevirtual kellinwood/security/zipsigner/KeySet/getName()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L47
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
aload 1
invokeinterface java/util/Map/size()I 0
invokevirtual kellinwood/security/zipsigner/ProgressHelper/setProgressTotalItems(I)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_0
invokevirtual kellinwood/security/zipsigner/ProgressHelper/setProgressCurrentItem(I)V
aload 0
aload 1
aload 10
aload 4
invokespecial com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/copyFiles(Ljava/util/Map;Lkellinwood/zipio/ZipOutput;Ljava/util/ArrayList;)V
aload 10
invokevirtual kellinwood/zipio/ZipOutput/close()V
L5:
aload 2
ifnull L48
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keymode Ljava/lang/String;
ldc "none"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L48
aload 2
invokevirtual java/util/jar/JarOutputStream/close()V
L48:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
ifeq L8
aload 3
ifnull L8
L7:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L8:
return
L9:
astore 1
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
return
L47:
iconst_0
istore 5
L10:
aload 1
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 10
L11:
aload 10
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L13
aload 10
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast kellinwood/zipio/ZioEntry
astore 11
aload 11
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
astore 12
aload 11
invokevirtual kellinwood/zipio/ZioEntry/isDirectory()Z
ifne L11
aload 12
ldc "META-INF/MANIFEST.MF"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L11
aload 12
ldc "META-INF/CERT.SF"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L11
aload 12
ldc "META-INF/CERT.RSA"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L11
getstatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/stripPattern Ljava/util/regex/Pattern;
ifnull L49
getstatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/stripPattern Ljava/util/regex/Pattern;
aload 12
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
invokevirtual java/util/regex/Matcher/matches()Z
ifne L11
L12:
goto L49
L13:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iload 5
iconst_1
iadd
invokevirtual kellinwood/security/zipsigner/ProgressHelper/setProgressTotalItems(I)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_0
invokevirtual kellinwood/security/zipsigner/ProgressHelper/setProgressCurrentItem(I)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
invokevirtual kellinwood/security/zipsigner/KeySet/getPublicKey()Ljava/security/cert/X509Certificate;
invokevirtual java/security/cert/X509Certificate/getNotBefore()Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
ldc2_w 3600000L
ladd
lstore 8
aload 0
aload 1
aload 4
invokespecial com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/addDigestsToManifest(Ljava/util/Map;Ljava/util/ArrayList;)Ljava/util/jar/Manifest;
astore 10
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
istore 7
L14:
iload 7
ifeq L18
aload 2
ifnull L50
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keymode Ljava/lang/String;
ldc "none"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L50
aload 2
invokevirtual java/util/jar/JarOutputStream/close()V
L50:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
ifeq L8
aload 3
ifnull L8
L15:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L16:
return
L17:
astore 1
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
return
L18:
new java/util/jar/JarEntry
dup
ldc "META-INF/MANIFEST.MF"
invokespecial java/util/jar/JarEntry/<init>(Ljava/lang/String;)V
astore 11
aload 11
lload 8
invokevirtual java/util/jar/JarEntry/setTime(J)V
aload 2
aload 11
invokevirtual java/util/jar/JarOutputStream/putNextEntry(Ljava/util/zip/ZipEntry;)V
aload 10
aload 2
invokevirtual java/util/jar/Manifest/write(Ljava/io/OutputStream;)V
new kellinwood/security/zipsigner/ZipSignature
dup
invokespecial kellinwood/security/zipsigner/ZipSignature/<init>()V
astore 11
aload 11
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
invokevirtual kellinwood/security/zipsigner/KeySet/getPrivateKey()Ljava/security/PrivateKey;
invokevirtual kellinwood/security/zipsigner/ZipSignature/initSign(Ljava/security/PrivateKey;)V
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 12
aload 0
aload 10
aload 12
invokespecial com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/generateSignatureFile(Ljava/util/jar/Manifest;Ljava/io/OutputStream;)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
istore 7
L19:
iload 7
ifeq L23
aload 2
ifnull L51
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keymode Ljava/lang/String;
ldc "none"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L51
aload 2
invokevirtual java/util/jar/JarOutputStream/close()V
L51:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
ifeq L8
aload 3
ifnull L8
L20:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L21:
return
L22:
astore 1
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
return
L23:
aload 12
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
astore 12
L24:
iload 6
ifeq L26
L25:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Signature File: \n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/lang/String
dup
aload 12
invokespecial java/lang/String/<init>([B)V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 12
invokestatic kellinwood/security/zipsigner/HexDumpEncoder/encode([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L26:
new java/util/jar/JarEntry
dup
ldc "META-INF/CERT.SF"
invokespecial java/util/jar/JarEntry/<init>(Ljava/lang/String;)V
astore 13
aload 13
lload 8
invokevirtual java/util/jar/JarEntry/setTime(J)V
aload 2
aload 13
invokevirtual java/util/jar/JarOutputStream/putNextEntry(Ljava/util/zip/ZipEntry;)V
aload 2
aload 12
invokevirtual java/util/jar/JarOutputStream/write([B)V
aload 11
aload 12
invokevirtual kellinwood/security/zipsigner/ZipSignature/update([B)V
aload 11
invokevirtual kellinwood/security/zipsigner/ZipSignature/sign()[B
astore 11
L27:
iload 6
ifeq L29
L28:
ldc "SHA1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 13
aload 13
aload 12
invokevirtual java/security/MessageDigest/update([B)V
aload 13
invokevirtual java/security/MessageDigest/digest()[B
astore 12
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Sig File SHA1: \n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 12
invokestatic kellinwood/security/zipsigner/HexDumpEncoder/encode([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Signature: \n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokestatic kellinwood/security/zipsigner/HexDumpEncoder/encode([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
ldc "RSA/ECB/PKCS1Padding"
invokestatic javax/crypto/Cipher/getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
astore 12
aload 12
iconst_2
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
invokevirtual kellinwood/security/zipsigner/KeySet/getPublicKey()Ljava/security/cert/X509Certificate;
invokevirtual javax/crypto/Cipher/init(ILjava/security/cert/Certificate;)V
aload 12
aload 11
invokevirtual javax/crypto/Cipher/doFinal([B)[B
astore 12
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Signature Decrypted: \n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 12
invokestatic kellinwood/security/zipsigner/HexDumpEncoder/encode([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/debug(Ljava/lang/String;)V 1
L29:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
iconst_0
ldc "Generating signature block file"
invokevirtual kellinwood/security/zipsigner/ProgressHelper/progress(ILjava/lang/String;)V
new java/util/jar/JarEntry
dup
ldc "META-INF/CERT.RSA"
invokespecial java/util/jar/JarEntry/<init>(Ljava/lang/String;)V
astore 12
aload 12
lload 8
invokevirtual java/util/jar/JarEntry/setTime(J)V
aload 2
aload 12
invokevirtual java/util/jar/JarOutputStream/putNextEntry(Ljava/util/zip/ZipEntry;)V
aload 0
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
invokevirtual kellinwood/security/zipsigner/KeySet/getSigBlockTemplate()[B
aload 11
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keySet Lkellinwood/security/zipsigner/KeySet;
invokevirtual kellinwood/security/zipsigner/KeySet/getPublicKey()Ljava/security/cert/X509Certificate;
aload 2
invokespecial com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/writeSignatureBlock([B[BLjava/security/cert/X509Certificate;Ljava/io/OutputStream;)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
istore 6
L30:
iload 6
ifeq L34
aload 2
ifnull L52
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keymode Ljava/lang/String;
ldc "none"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L52
aload 2
invokevirtual java/util/jar/JarOutputStream/close()V
L52:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
ifeq L8
aload 3
ifnull L8
L31:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L32:
return
L33:
astore 1
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
return
L34:
aload 0
aload 10
aload 1
aload 2
lload 8
aload 4
invokespecial com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/copyFiles(Ljava/util/jar/Manifest;Ljava/util/Map;Ljava/util/jar/JarOutputStream;JLjava/util/ArrayList;)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
istore 6
L35:
iload 6
ifeq L53
aload 2
ifnull L54
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keymode Ljava/lang/String;
ldc "none"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L54
aload 2
invokevirtual java/util/jar/JarOutputStream/close()V
L54:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
ifeq L8
aload 3
ifnull L8
L36:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L37:
return
L38:
astore 1
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
return
L53:
aload 2
ifnull L55
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keymode Ljava/lang/String;
ldc "none"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L55
aload 2
invokevirtual java/util/jar/JarOutputStream/close()V
L55:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
ifeq L8
aload 3
ifnull L8
L39:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L40:
return
L41:
astore 1
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
return
L2:
astore 1
aconst_null
astore 2
L56:
aload 2
ifnull L57
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/keymode Ljava/lang/String;
ldc "none"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L57
aload 2
invokevirtual java/util/jar/JarOutputStream/close()V
L57:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/canceled Z
ifeq L43
aload 3
ifnull L43
L42:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L43:
aload 1
athrow
L44:
astore 2
invokestatic com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/getLogger()Lkellinwood/logging/LoggerInterface;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface kellinwood/logging/LoggerInterface/warning(Ljava/lang/String;)V 1
goto L43
L4:
astore 1
aconst_null
astore 2
goto L56
L6:
astore 1
goto L56
L49:
iload 5
iconst_3
iadd
istore 5
goto L11
.limit locals 14
.limit stack 7
.end method

.method public signZip(Ljava/util/Map;Ljava/lang/String;Ljava/util/ArrayList;)V
.signature "(Ljava/util/Map<Ljava/lang/String;Lkellinwood/zipio/ZioEntry;>;Ljava/lang/String;Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;)V"
.throws java/io/IOException
.throws java/security/GeneralSecurityException
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/progressHelper Lkellinwood/security/zipsigner/ProgressHelper;
invokevirtual kellinwood/security/zipsigner/ProgressHelper/initProgress()V
aload 0
aload 1
new java/io/FileOutputStream
dup
aload 2
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
aload 2
aload 3
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP/signZip(Ljava/util/Map;Ljava/io/OutputStream;Ljava/lang/String;Ljava/util/ArrayList;)V
return
.limit locals 4
.limit stack 5
.end method
