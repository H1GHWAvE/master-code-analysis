.bytecode 50.0
.class public synchronized com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper
.super android/database/sqlite/SQLiteOpenHelper

.field static final 'ads' Ljava/lang/String; = "ads"

.field static final 'billing' Ljava/lang/String; = "billing"

.field static final 'boot_ads' Ljava/lang/String; = "boot_ads"

.field static final 'boot_custom' Ljava/lang/String; = "boot_custom"

.field static final 'boot_lvl' Ljava/lang/String; = "boot_lvl"

.field static final 'boot_manual' Ljava/lang/String; = "boot_manual"

.field public static 'contextdb' Landroid/content/Context;

.field static final 'custom' Ljava/lang/String; = "custom"

.field public static 'db' Landroid/database/sqlite/SQLiteDatabase;

.field static final 'dbName' Ljava/lang/String; = "PackagesDB"

.field public static 'getPackage' Z = 0


.field static final 'hidden' Ljava/lang/String; = "hidden"

.field static final 'icon' Ljava/lang/String; = "icon"

.field static final 'lvl' Ljava/lang/String; = "lvl"

.field static final 'modified' Ljava/lang/String; = "modified"

.field static final 'odex' Ljava/lang/String; = "odex"

.field static final 'packagesTable' Ljava/lang/String; = "Packages"

.field static final 'pkgLabel' Ljava/lang/String; = "pkgLabel"

.field static final 'pkgName' Ljava/lang/String; = "pkgName"

.field public static 'savePackage' Z = 0


.field static final 'statusi' Ljava/lang/String; = "statusi"

.field static final 'stored' Ljava/lang/String; = "stored"

.field static final 'storepref' Ljava/lang/String; = "storepref"

.field static final 'system' Ljava/lang/String; = "system"

.field static final 'updatetime' Ljava/lang/String; = "updatetime"

.method static <clinit>()V
aconst_null
putstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/contextdb Landroid/content/Context;
aconst_null
putstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/db Landroid/database/sqlite/SQLiteDatabase;
iconst_0
putstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/getPackage Z
iconst_0
putstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/savePackage Z
return
.limit locals 0
.limit stack 1
.end method

.method public <init>(Landroid/content/Context;)V
.catch android/database/sqlite/SQLiteException from L0 to L1 using L2
aload 0
aload 1
ldc "PackagesDB"
aconst_null
bipush 42
invokespecial android/database/sqlite/SQLiteOpenHelper/<init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
aload 1
putstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/contextdb Landroid/content/Context;
L0:
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
putstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/db Landroid/database/sqlite/SQLiteDatabase;
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "SQLite base version is "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/db Landroid/database/sqlite/SQLiteDatabase;
invokevirtual android/database/sqlite/SQLiteDatabase/getVersion()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/db Landroid/database/sqlite/SQLiteDatabase;
invokevirtual android/database/sqlite/SQLiteDatabase/getVersion()I
bipush 42
if_icmpeq L1
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "SQL delete and recreate."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/db Landroid/database/sqlite/SQLiteDatabase;
ldc "DROP TABLE IF EXISTS Packages"
invokevirtual android/database/sqlite/SQLiteDatabase/execSQL(Ljava/lang/String;)V
aload 0
getstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/db Landroid/database/sqlite/SQLiteDatabase;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
L1:
return
L2:
astore 1
aload 1
invokevirtual android/database/sqlite/SQLiteException/printStackTrace()V
return
.limit locals 2
.limit stack 5
.end method

.method public deletePackage(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/db Landroid/database/sqlite/SQLiteDatabase;
ldc "Packages"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "pkgName = '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
pop
L1:
return
L2:
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher-Error: deletePackage "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 4
.end method

.method public deletePackage(Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/db Landroid/database/sqlite/SQLiteDatabase;
ldc "Packages"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "pkgName = '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
pop
L1:
return
L2:
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher-Error: deletePackage "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 4
.end method

.method public getPackage(ZZ)Ljava/util/ArrayList;
.signature "(ZZ)Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L3 to L5 using L6
.catch java/lang/IllegalArgumentException from L3 to L5 using L7
.catch java/lang/Exception from L3 to L5 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L8 to L9 using L6
.catch java/lang/IllegalArgumentException from L8 to L9 using L7
.catch java/lang/Exception from L8 to L9 using L4
.catch java/lang/Exception from L10 to L11 using L12
.catch java/lang/OutOfMemoryError from L10 to L11 using L13
.catch android/content/pm/PackageManager$NameNotFoundException from L10 to L11 using L6
.catch java/lang/IllegalArgumentException from L10 to L11 using L7
.catch java/lang/Exception from L14 to L15 using L12
.catch java/lang/OutOfMemoryError from L14 to L15 using L13
.catch android/content/pm/PackageManager$NameNotFoundException from L14 to L15 using L6
.catch java/lang/IllegalArgumentException from L14 to L15 using L7
.catch android/content/pm/PackageManager$NameNotFoundException from L15 to L16 using L6
.catch java/lang/IllegalArgumentException from L15 to L16 using L7
.catch java/lang/Exception from L15 to L16 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L17 to L18 using L6
.catch java/lang/IllegalArgumentException from L17 to L18 using L7
.catch java/lang/Exception from L17 to L18 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L19 to L20 using L6
.catch java/lang/IllegalArgumentException from L19 to L20 using L7
.catch java/lang/Exception from L19 to L20 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L20 to L21 using L6
.catch java/lang/IllegalArgumentException from L20 to L21 using L7
.catch java/lang/Exception from L20 to L21 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L21 to L22 using L6
.catch java/lang/IllegalArgumentException from L21 to L22 using L7
.catch java/lang/Exception from L21 to L22 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L23 to L24 using L6
.catch java/lang/IllegalArgumentException from L23 to L24 using L7
.catch java/lang/Exception from L23 to L24 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L24 to L6 using L6
.catch java/lang/IllegalArgumentException from L24 to L6 using L7
.catch java/lang/Exception from L24 to L6 using L4
.catch java/lang/Exception from L25 to L26 using L27
.catch java/lang/Exception from L26 to L28 using L2
.catch android/content/pm/PackageManager$NameNotFoundException from L29 to L30 using L6
.catch java/lang/IllegalArgumentException from L29 to L30 using L7
.catch java/lang/Exception from L29 to L30 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L31 to L32 using L6
.catch java/lang/IllegalArgumentException from L31 to L32 using L7
.catch java/lang/Exception from L31 to L32 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L33 to L34 using L6
.catch java/lang/IllegalArgumentException from L33 to L34 using L7
.catch java/lang/Exception from L33 to L34 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L35 to L36 using L6
.catch java/lang/IllegalArgumentException from L35 to L36 using L7
.catch java/lang/Exception from L35 to L36 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L37 to L38 using L6
.catch java/lang/IllegalArgumentException from L37 to L38 using L7
.catch java/lang/Exception from L37 to L38 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L38 to L39 using L6
.catch java/lang/IllegalArgumentException from L38 to L39 using L7
.catch java/lang/Exception from L38 to L39 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L39 to L40 using L6
.catch java/lang/IllegalArgumentException from L39 to L40 using L7
.catch java/lang/Exception from L39 to L40 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L40 to L41 using L6
.catch java/lang/IllegalArgumentException from L40 to L41 using L7
.catch java/lang/Exception from L40 to L41 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L41 to L42 using L6
.catch java/lang/IllegalArgumentException from L41 to L42 using L7
.catch java/lang/Exception from L41 to L42 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L43 to L44 using L6
.catch java/lang/IllegalArgumentException from L43 to L44 using L7
.catch java/lang/Exception from L43 to L44 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L44 to L45 using L6
.catch java/lang/IllegalArgumentException from L44 to L45 using L7
.catch java/lang/Exception from L44 to L45 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L45 to L46 using L6
.catch java/lang/IllegalArgumentException from L45 to L46 using L7
.catch java/lang/Exception from L45 to L46 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L46 to L47 using L6
.catch java/lang/IllegalArgumentException from L46 to L47 using L7
.catch java/lang/Exception from L46 to L47 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L47 to L48 using L6
.catch java/lang/IllegalArgumentException from L47 to L48 using L7
.catch java/lang/Exception from L47 to L48 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L48 to L49 using L6
.catch java/lang/IllegalArgumentException from L48 to L49 using L7
.catch java/lang/Exception from L48 to L49 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L49 to L50 using L6
.catch java/lang/IllegalArgumentException from L49 to L50 using L7
.catch java/lang/Exception from L49 to L50 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L50 to L51 using L6
.catch java/lang/IllegalArgumentException from L50 to L51 using L7
.catch java/lang/Exception from L50 to L51 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L51 to L52 using L6
.catch java/lang/IllegalArgumentException from L51 to L52 using L7
.catch java/lang/Exception from L51 to L52 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L52 to L53 using L6
.catch java/lang/IllegalArgumentException from L52 to L53 using L7
.catch java/lang/Exception from L52 to L53 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L54 to L55 using L6
.catch java/lang/IllegalArgumentException from L54 to L55 using L7
.catch java/lang/Exception from L54 to L55 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L56 to L57 using L6
.catch java/lang/IllegalArgumentException from L56 to L57 using L7
.catch java/lang/Exception from L56 to L57 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L57 to L58 using L6
.catch java/lang/IllegalArgumentException from L57 to L58 using L7
.catch java/lang/Exception from L57 to L58 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L58 to L59 using L6
.catch java/lang/IllegalArgumentException from L58 to L59 using L7
.catch java/lang/Exception from L58 to L59 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L59 to L60 using L6
.catch java/lang/IllegalArgumentException from L59 to L60 using L7
.catch java/lang/Exception from L59 to L60 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L60 to L61 using L6
.catch java/lang/IllegalArgumentException from L60 to L61 using L7
.catch java/lang/Exception from L60 to L61 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L61 to L62 using L6
.catch java/lang/IllegalArgumentException from L61 to L62 using L7
.catch java/lang/Exception from L61 to L62 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L62 to L63 using L6
.catch java/lang/IllegalArgumentException from L62 to L63 using L7
.catch java/lang/Exception from L62 to L63 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L63 to L64 using L6
.catch java/lang/IllegalArgumentException from L63 to L64 using L7
.catch java/lang/Exception from L63 to L64 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L64 to L65 using L6
.catch java/lang/IllegalArgumentException from L64 to L65 using L7
.catch java/lang/Exception from L64 to L65 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L65 to L66 using L6
.catch java/lang/IllegalArgumentException from L65 to L66 using L7
.catch java/lang/Exception from L65 to L66 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L66 to L67 using L6
.catch java/lang/IllegalArgumentException from L66 to L67 using L7
.catch java/lang/Exception from L66 to L67 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L67 to L68 using L6
.catch java/lang/IllegalArgumentException from L67 to L68 using L7
.catch java/lang/Exception from L67 to L68 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L68 to L69 using L6
.catch java/lang/IllegalArgumentException from L68 to L69 using L7
.catch java/lang/Exception from L68 to L69 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L69 to L70 using L6
.catch java/lang/IllegalArgumentException from L69 to L70 using L7
.catch java/lang/Exception from L69 to L70 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L70 to L71 using L6
.catch java/lang/IllegalArgumentException from L70 to L71 using L7
.catch java/lang/Exception from L70 to L71 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L71 to L72 using L6
.catch java/lang/IllegalArgumentException from L71 to L72 using L7
.catch java/lang/Exception from L71 to L72 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L72 to L73 using L6
.catch java/lang/IllegalArgumentException from L72 to L73 using L7
.catch java/lang/Exception from L72 to L73 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L73 to L74 using L6
.catch java/lang/IllegalArgumentException from L73 to L74 using L7
.catch java/lang/Exception from L73 to L74 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L74 to L75 using L6
.catch java/lang/IllegalArgumentException from L74 to L75 using L7
.catch java/lang/Exception from L74 to L75 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L75 to L76 using L6
.catch java/lang/IllegalArgumentException from L75 to L76 using L7
.catch java/lang/Exception from L75 to L76 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L76 to L77 using L6
.catch java/lang/IllegalArgumentException from L76 to L77 using L7
.catch java/lang/Exception from L76 to L77 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L78 to L79 using L6
.catch java/lang/IllegalArgumentException from L78 to L79 using L7
.catch java/lang/Exception from L78 to L79 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L80 to L81 using L6
.catch java/lang/IllegalArgumentException from L80 to L81 using L7
.catch java/lang/Exception from L80 to L81 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L81 to L82 using L6
.catch java/lang/IllegalArgumentException from L81 to L82 using L7
.catch java/lang/Exception from L81 to L82 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L83 to L84 using L6
.catch java/lang/IllegalArgumentException from L83 to L84 using L7
.catch java/lang/Exception from L83 to L84 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L84 to L27 using L6
.catch java/lang/IllegalArgumentException from L84 to L27 using L7
.catch java/lang/Exception from L84 to L27 using L4
.catch java/lang/Exception from L85 to L86 using L2
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 22
aload 22
invokevirtual java/util/ArrayList/clear()V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
astore 24
iconst_1
putstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/getPackage Z
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/db Landroid/database/sqlite/SQLiteDatabase;
ldc "Packages"
bipush 19
anewarray java/lang/String
dup
iconst_0
ldc "pkgName"
aastore
dup
iconst_1
ldc "pkgLabel"
aastore
dup
iconst_2
ldc "stored"
aastore
dup
iconst_3
ldc "storepref"
aastore
dup
iconst_4
ldc "hidden"
aastore
dup
iconst_5
ldc "statusi"
aastore
dup
bipush 6
ldc "boot_ads"
aastore
dup
bipush 7
ldc "boot_lvl"
aastore
dup
bipush 8
ldc "boot_custom"
aastore
dup
bipush 9
ldc "boot_manual"
aastore
dup
bipush 10
ldc "custom"
aastore
dup
bipush 11
ldc "lvl"
aastore
dup
bipush 12
ldc "ads"
aastore
dup
bipush 13
ldc "modified"
aastore
dup
bipush 14
ldc "system"
aastore
dup
bipush 15
ldc "odex"
aastore
dup
bipush 16
ldc "icon"
aastore
dup
bipush 17
ldc "updatetime"
aastore
dup
bipush 18
ldc "billing"
aastore
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 23
aload 23
invokeinterface android/database/Cursor/moveToFirst()Z 0
pop
L1:
aload 23
aload 23
ldc "pkgName"
invokeinterface android/database/Cursor/getColumnIndexOrThrow(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
astore 25
L3:
aload 24
aload 25
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/applicationInfo Landroid/content/pm/ApplicationInfo;
astore 26
aload 26
getfield android/content/pm/ApplicationInfo/enabled Z
istore 19
aload 26
getfield android/content/pm/ApplicationInfo/flags I
ldc_w 262144
iand
ldc_w 262144
if_icmpne L87
L5:
iconst_1
istore 18
L8:
aload 23
aload 23
ldc "pkgLabel"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
astore 27
aload 23
aload 23
ldc "stored"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 3
aload 23
aload 23
ldc "storepref"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 5
aload 23
aload 23
ldc "hidden"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 6
aload 23
aload 23
ldc "statusi"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
astore 28
aload 23
aload 23
ldc "boot_ads"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 7
aload 23
aload 23
ldc "boot_lvl"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 8
aload 23
aload 23
ldc "boot_custom"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 9
aload 23
aload 23
ldc "boot_manual"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 10
aload 23
aload 23
ldc "custom"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 11
aload 23
aload 23
ldc "lvl"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 12
aload 23
aload 23
ldc "ads"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 13
aload 23
aload 23
ldc "modified"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 14
aload 23
aload 23
ldc "system"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 15
aload 23
aload 23
ldc "odex"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 16
aload 23
aload 23
ldc "billing"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 17
L9:
aconst_null
astore 21
aload 21
astore 20
iload 2
ifne L15
L10:
aload 23
aload 23
ldc "icon"
invokeinterface android/database/Cursor/getColumnIndexOrThrow(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getBlob(I)[B 1
astore 29
L11:
aload 21
astore 20
aload 29
ifnull L15
L14:
new java/io/ByteArrayInputStream
dup
aload 29
invokespecial java/io/ByteArrayInputStream/<init>([B)V
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
astore 20
L15:
aload 23
aload 23
ldc "updatetime"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
istore 4
L16:
iload 3
ifeq L31
L17:
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 1000L
ldiv
l2i
iload 4
isub
invokestatic java/lang/Math/abs(I)I
ldc_w 86400
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/days I
imul
if_icmpge L20
L18:
iconst_0
istore 3
L19:
getstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/db Landroid/database/sqlite/SQLiteDatabase;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "UPDATE Packages SET stored=0 WHERE pkgName='"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/database/sqlite/SQLiteDatabase/execSQL(Ljava/lang/String;)V
L20:
new com/android/vending/billing/InAppBillingService/LUCK/PkgListItem
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
aload 25
aload 27
iload 3
iload 5
iload 6
aload 28
iload 7
iload 8
iload 9
iload 10
iload 11
iload 12
iload 13
iload 14
iload 15
iload 16
aload 20
iload 4
iload 17
iload 19
iload 18
iload 2
invokespecial com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;IIIIIIIIIILandroid/graphics/Bitmap;IIZZZ)V
astore 20
L21:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "systemapp"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
istore 18
L22:
iload 1
ifne L49
L23:
aload 25
ldc "android"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L24
aload 25
ldc com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment
invokevirtual java/lang/Class/getPackage()Ljava/lang/Package;
invokevirtual java/lang/Package/getName()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L35
L24:
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L6:
astore 20
L25:
aload 23
invokeinterface android/database/Cursor/moveToNext()Z 0
ifne L1
aload 23
invokeinterface android/database/Cursor/close()V 0
L26:
iconst_0
putstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/getPackage Z
L28:
aload 22
areturn
L87:
iconst_0
istore 18
goto L8
L12:
astore 20
L29:
aload 20
invokevirtual java/lang/Exception/printStackTrace()V
L30:
aload 21
astore 20
goto L15
L31:
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 1000L
ldiv
l2i
iload 4
isub
invokestatic java/lang/Math/abs(I)I
ldc_w 86400
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/days I
imul
if_icmple L33
new com/android/vending/billing/InAppBillingService/LUCK/PkgListItem
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
aload 25
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/days I
iload 2
invokespecial com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/<init>(Landroid/content/Context;Ljava/lang/String;IZ)V
astore 20
aload 0
aload 20
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/savePackage(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V
L32:
goto L21
L33:
new com/android/vending/billing/InAppBillingService/LUCK/PkgListItem
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
aload 25
aload 27
iload 3
iload 5
iload 6
aload 28
iload 7
iload 8
iload 9
iload 10
iload 11
iload 12
iload 13
iload 14
iload 15
iload 16
aload 20
iload 4
iload 17
iload 19
iload 18
iload 2
invokespecial com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;IIIIIIIIIILandroid/graphics/Bitmap;IIZZZ)V
astore 20
L34:
goto L21
L35:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/system Z
ifeq L38
L36:
iload 18
ifne L38
L37:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/custom Z
ifne L38
new java/lang/IllegalArgumentException
dup
ldc "package scan filter"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L38:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "lvlapp"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L39
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/lvl Z
ifeq L39
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/ads Z
ifne L39
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/custom Z
ifne L39
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/odex Z
ifne L39
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/modified Z
ifne L39
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L39:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "adsapp"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L40
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/ads Z
ifeq L40
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/lvl Z
ifne L40
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/custom Z
ifne L40
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/odex Z
ifne L40
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/modified Z
ifne L40
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L40:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "lvlapp"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L41
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "adsapp"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L41
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/lvl Z
ifeq L41
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/ads Z
ifeq L41
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/custom Z
ifne L41
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/odex Z
ifne L41
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/modified Z
ifne L41
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L41:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "noneapp"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L44
L42:
iload 3
bipush 6
if_icmpne L44
L43:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/odex Z
ifne L44
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/modified Z
ifne L44
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L44:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "customapp"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L45
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/custom Z
ifeq L45
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/odex Z
ifne L45
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/modified Z
ifne L45
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L45:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "fixedapp"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L46
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/odex Z
ifeq L46
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/modified Z
ifne L46
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L46:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "modifapp"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L47
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/odex Z
ifne L47
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/modified Z
ifeq L47
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L47:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "fixedapp"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L52
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "modifapp"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L52
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/odex Z
ifne L48
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/modified Z
ifeq L52
L48:
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L49:
aload 25
ldc "android"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L50
aload 25
getstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/contextdb Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L51
L50:
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L51:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/advancedFilter I
ifeq L52
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/advancedFilter I
lookupswitch
1929 : L65
1930 : L70
1931 : L71
1932 : L73
2131558415 : L54
2131558416 : L56
2131558417 : L57
2131558418 : L58
2131558419 : L59
2131558420 : L60
2131558421 : L61
2131558422 : L62
2131558423 : L63
2131558424 : L64
2131558425 : L84
2131558426 : L67
2131558427 : L68
2131558428 : L76
default : L88
L52:
aload 22
aload 20
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L53:
goto L25
L54:
iconst_0
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/advancedFilter I
L55:
goto L52
L56:
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 1000L
ldiv
l2i
iload 4
isub
invokestatic java/lang/Math/abs(I)I
ldc_w 86400
if_icmple L52
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L57:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/lvl Z
ifne L52
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L58:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/ads Z
ifne L52
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L59:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/custom Z
ifne L52
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L60:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/modified Z
ifne L52
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L61:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/odex Z
ifne L52
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L62:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/billing Z
ifne L52
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L63:
aload 26
getfield android/content/pm/ApplicationInfo/sourceDir Ljava/lang/String;
ldc "/data/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L52
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L64:
aload 26
getfield android/content/pm/ApplicationInfo/sourceDir Ljava/lang/String;
ldc "/mnt/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L52
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L65:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/system Z
ifne L66
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/pkgName Ljava/lang/String;
invokestatic com/chelpus/Utils/isInstalledOnSdCard(Ljava/lang/String;)Z
ifeq L52
L66:
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L67:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/system Z
ifne L52
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L68:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/system Z
ifeq L69
aload 26
getfield android/content/pm/ApplicationInfo/sourceDir Ljava/lang/String;
ldc "/data/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L52
aload 26
getfield android/content/pm/ApplicationInfo/sourceDir Ljava/lang/String;
ldc "/mnt/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L52
L69:
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L70:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/system Z
ifeq L52
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L71:
aload 24
ldc "android.permission.INTERNET"
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/pkgName Ljava/lang/String;
invokevirtual android/content/pm/PackageManager/checkPermission(Ljava/lang/String;Ljava/lang/String;)I
iconst_m1
if_icmpne L72
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L72:
aload 24
new android/content/ComponentName
dup
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/pkgName Ljava/lang/String;
ldc "android.permission.INTERNET"
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_2
if_icmpne L52
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L73:
aload 24
ldc "android.permission.INTERNET"
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/pkgName Ljava/lang/String;
invokevirtual android/content/pm/PackageManager/checkPermission(Ljava/lang/String;Ljava/lang/String;)I
iconst_m1
if_icmpne L74
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L74:
aload 24
new android/content/ComponentName
dup
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/pkgName Ljava/lang/String;
ldc "android.permission.INTERNET"
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_1
if_icmpeq L75
aload 24
new android/content/ComponentName
dup
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/pkgName Ljava/lang/String;
ldc "android.permission.INTERNET"
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
ifne L52
L75:
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L76:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/pkgName Ljava/lang/String;
sipush 4096
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/requestedPermissions [Ljava/lang/String;
astore 21
L77:
iconst_0
istore 5
iconst_0
istore 3
aload 21
ifnull L89
L78:
aload 21
arraylength
istore 6
L79:
iconst_0
istore 4
L90:
iload 3
istore 5
iload 4
iload 6
if_icmpge L89
aload 21
iload 4
aaload
astore 25
L80:
aload 25
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "SEND_SMS"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L91
L81:
aload 25
ldc "CALL_PHONE"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L92
L82:
goto L91
L89:
iload 5
ifne L52
L83:
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L84:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/enable Z
ifeq L52
new java/lang/IllegalArgumentException
dup
ldc "package scan error"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L27:
astore 20
L85:
aload 23
invokeinterface android/database/Cursor/close()V 0
L86:
goto L26
L2:
astore 20
iconst_0
putstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/getPackage Z
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher-Error: getPackage "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 20
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 22
areturn
L7:
astore 20
goto L25
L13:
astore 20
aconst_null
astore 20
goto L15
L4:
astore 20
goto L25
L88:
goto L52
L91:
iconst_1
istore 3
L92:
iload 4
iconst_1
iadd
istore 4
goto L90
.limit locals 30
.limit stack 25
.end method

.method public isOpen()Z
getstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/db Landroid/database/sqlite/SQLiteDatabase;
invokevirtual android/database/sqlite/SQLiteDatabase/isOpen()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
aload 1
ldc "CREATE TABLE Packages (pkgName TEXT PRIMARY KEY, pkgLabel TEXT, stored Integer, storepref Integer, hidden Integer, statusi TEXT, boot_ads Integer, boot_lvl Integer, boot_custom Integer, boot_manual Integer, custom Integer, lvl Integer, ads Integer, modified Integer, system Integer, odex Integer, icon BLOB, updatetime Integer, billing Integer );"
invokevirtual android/database/sqlite/SQLiteDatabase/execSQL(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 2
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
aload 1
ldc "DROP TABLE IF EXISTS Packages"
invokevirtual android/database/sqlite/SQLiteDatabase/execSQL(Ljava/lang/String;)V
aload 0
aload 1
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
return
.limit locals 4
.limit stack 2
.end method

.method public savePackage(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V
.throws android/database/sqlite/SQLiteException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/OutOfMemoryError from L3 to L4 using L5
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/OutOfMemoryError from L6 to L7 using L5
.catch java/lang/Exception from L6 to L7 using L2
.catch java/lang/Exception from L7 to L8 using L9
.catch java/lang/Exception from L8 to L10 using L2
.catch java/lang/Exception from L11 to L12 using L2
L0:
iconst_1
putstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/savePackage Z
new android/content/ContentValues
dup
invokespecial android/content/ContentValues/<init>()V
astore 3
aload 3
ldc "pkgName"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/pkgName Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 3
ldc "pkgLabel"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/name Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 3
ldc "stored"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/stored I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 3
ldc "storepref"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/storepref I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 3
ldc "hidden"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/hidden Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Boolean;)V
aload 3
ldc "statusi"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/statusi Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 3
ldc "boot_ads"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/boot_ads Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Boolean;)V
aload 3
ldc "boot_lvl"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/boot_lvl Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Boolean;)V
aload 3
ldc "boot_custom"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/boot_custom Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Boolean;)V
aload 3
ldc "boot_manual"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/boot_manual Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Boolean;)V
aload 3
ldc "custom"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/custom Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Boolean;)V
aload 3
ldc "lvl"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/lvl Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Boolean;)V
aload 3
ldc "ads"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/ads Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Boolean;)V
aload 3
ldc "modified"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/modified Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Boolean;)V
aload 3
ldc "system"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/system Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Boolean;)V
aload 3
ldc "odex"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/odex Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Boolean;)V
aload 3
ldc "updatetime"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/updatetime I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 3
ldc "billing"
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/billing Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Boolean;)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/pkgName Ljava/lang/String;
iconst_0
invokevirtual android/content/pm/PackageManager/getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/sourceDir Ljava/lang/String;
astore 2
L1:
aconst_null
astore 2
L3:
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/icon Landroid/graphics/drawable/Drawable;
ifnull L4
aload 1
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/icon Landroid/graphics/drawable/Drawable;
checkcast android/graphics/drawable/BitmapDrawable
invokevirtual android/graphics/drawable/BitmapDrawable/getBitmap()Landroid/graphics/Bitmap;
astore 2
L4:
aload 2
ifnull L7
L6:
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 1
aload 2
getstatic android/graphics/Bitmap$CompressFormat/PNG Landroid/graphics/Bitmap$CompressFormat;
bipush 100
aload 1
invokevirtual android/graphics/Bitmap/compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
pop
aload 3
ldc "icon"
aload 1
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
invokevirtual android/content/ContentValues/put(Ljava/lang/String;[B)V
L7:
getstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/db Landroid/database/sqlite/SQLiteDatabase;
ldc "Packages"
ldc "pkgName"
aload 3
invokevirtual android/database/sqlite/SQLiteDatabase/insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
pop2
L8:
iconst_0
putstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/savePackage Z
iconst_0
putstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/savePackage Z
L10:
return
L9:
astore 1
L11:
getstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/db Landroid/database/sqlite/SQLiteDatabase;
ldc "Packages"
aconst_null
aload 3
invokevirtual android/database/sqlite/SQLiteDatabase/replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
pop2
L12:
goto L8
L2:
astore 1
iconst_0
putstatic com/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper/savePackage Z
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher-Error: savePackage "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
L5:
astore 1
goto L7
.limit locals 4
.limit stack 4
.end method

.method public updatePackage(Ljava/util/List;)V
.signature "(Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;)V"
return
.limit locals 2
.limit stack 0
.end method
