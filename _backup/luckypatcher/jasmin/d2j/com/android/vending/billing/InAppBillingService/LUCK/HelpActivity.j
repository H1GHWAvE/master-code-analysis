.bytecode 50.0
.class public synchronized com/android/vending/billing/InAppBillingService/LUCK/HelpActivity
.super android/app/Activity

.field public 'context' Landroid/content/Context;

.field 'mLocalActivityManager' Landroid/app/LocalActivityManager;

.field 'tabHost' Landroid/widget/TabHost;

.method public <init>()V
aload 0
invokespecial android/app/Activity/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static createTabView(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;
aload 0
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
ldc_w 2130968636
aconst_null
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
astore 0
aload 0
ldc_w 2131558635
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private initTabs(Landroid/os/Bundle;)V
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/getResources()Landroid/content/res/Resources;
astore 3
aload 0
aload 0
ldc_w 16908306
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/TabHost
putfield com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/tabHost Landroid/widget/TabHost;
aload 0
new android/app/LocalActivityManager
dup
aload 0
iconst_0
invokespecial android/app/LocalActivityManager/<init>(Landroid/app/Activity;Z)V
putfield com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/mLocalActivityManager Landroid/app/LocalActivityManager;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/mLocalActivityManager Landroid/app/LocalActivityManager;
aload 1
invokevirtual android/app/LocalActivityManager/dispatchCreate(Landroid/os/Bundle;)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/tabHost Landroid/widget/TabHost;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/mLocalActivityManager Landroid/app/LocalActivityManager;
invokevirtual android/widget/TabHost/setup(Landroid/app/LocalActivityManager;)V
aload 0
ldc_w 16908306
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/TabHost
astore 1
aload 1
invokevirtual android/widget/TabHost/getTabWidget()Landroid/widget/TabWidget;
ldc_w 2130837589
invokevirtual android/widget/TabWidget/setDividerDrawable(I)V
aload 1
invokevirtual android/widget/TabHost/getContext()Landroid/content/Context;
aload 3
ldc_w 2131165484
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/createTabView(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;
astore 2
aload 1
invokevirtual android/widget/TabHost/getContext()Landroid/content/Context;
aload 3
ldc_w 2131165485
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/createTabView(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;
astore 3
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
aload 0
ldc com/android/vending/billing/InAppBillingService/LUCK/HelpCommon
invokevirtual android/content/Intent/setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
astore 4
aload 1
aload 1
ldc "Common"
invokevirtual android/widget/TabHost/newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;
aload 2
invokevirtual android/widget/TabHost$TabSpec/setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;
aload 4
invokevirtual android/widget/TabHost$TabSpec/setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;
invokevirtual android/widget/TabHost/addTab(Landroid/widget/TabHost$TabSpec;)V
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
aload 0
ldc com/android/vending/billing/InAppBillingService/LUCK/HelpCustom
invokevirtual android/content/Intent/setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
astore 2
aload 1
aload 1
ldc "Create"
invokevirtual android/widget/TabHost/newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;
aload 3
invokevirtual android/widget/TabHost$TabSpec/setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;
aload 2
invokevirtual android/widget/TabHost$TabSpec/setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;
invokevirtual android/widget/TabHost/addTab(Landroid/widget/TabHost$TabSpec;)V
aload 1
iconst_0
invokevirtual android/widget/TabHost/setCurrentTab(I)V
aload 1
invokevirtual android/widget/TabHost/getTabWidget()Landroid/widget/TabWidget;
iconst_0
invokevirtual android/widget/TabWidget/setCurrentTab(I)V
return
.limit locals 5
.limit stack 5
.end method

.method public onCreate(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/app/Activity/onCreate(Landroid/os/Bundle;)V
aload 0
ldc_w 2130968621
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/setContentView(I)V
aload 0
aload 1
invokespecial com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/initTabs(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected onPause()V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/mLocalActivityManager Landroid/app/LocalActivityManager;
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/isFinishing()Z
invokevirtual android/app/LocalActivityManager/dispatchPause(Z)V
aload 0
invokespecial android/app/Activity/onPause()V
return
.limit locals 1
.limit stack 2
.end method

.method protected onResume()V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/HelpActivity/mLocalActivityManager Landroid/app/LocalActivityManager;
invokevirtual android/app/LocalActivityManager/dispatchResume()V
aload 0
invokespecial android/app/Activity/onResume()V
return
.limit locals 1
.limit stack 1
.end method
