.bytecode 50.0
.class public synchronized com/android/vending/billing/InAppBillingService/LUCK/widgets/lvl_widget
.super android/appwidget/AppWidgetProvider

.field public static 'ACTION_WIDGET_RECEIVER' Ljava/lang/String;

.field public static 'ACTION_WIDGET_RECEIVER_Updater' Ljava/lang/String;

.method static <clinit>()V
ldc "ActionReceiverLVLWidget"
putstatic com/android/vending/billing/InAppBillingService/LUCK/widgets/lvl_widget/ACTION_WIDGET_RECEIVER Ljava/lang/String;
ldc "ActionReceiverWidgetLVLUpdate"
putstatic com/android/vending/billing/InAppBillingService/LUCK/widgets/lvl_widget/ACTION_WIDGET_RECEIVER_Updater Ljava/lang/String;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial android/appwidget/AppWidgetProvider/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static pushWidgetUpdate(Landroid/content/Context;Landroid/widget/RemoteViews;)V
new android/content/ComponentName
dup
aload 0
ldc com/android/vending/billing/InAppBillingService/LUCK/widgets/lvl_widget
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 2
aload 0
invokestatic android/appwidget/AppWidgetManager/getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;
aload 2
aload 1
invokevirtual android/appwidget/AppWidgetManager/updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V
return
.limit locals 3
.limit stack 4
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 2
invokevirtual android/content/Intent/getAction()Ljava/lang/String;
astore 3
getstatic com/android/vending/billing/InAppBillingService/LUCK/widgets/lvl_widget/ACTION_WIDGET_RECEIVER Ljava/lang/String;
aload 3
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/init()V
new android/widget/RemoteViews
dup
aload 1
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
ldc_w 2130968638
invokespecial android/widget/RemoteViews/<init>(Ljava/lang/String;I)V
astore 4
aload 4
ldc_w 2131558638
ldc ""
invokevirtual android/widget/RemoteViews/setTextViewText(ILjava/lang/CharSequence;)V
aload 4
ldc_w 2131558639
iconst_0
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 1
invokestatic android/appwidget/AppWidgetManager/getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;
astore 5
aload 5
aload 5
new android/content/ComponentName
dup
aload 1
ldc com/android/vending/billing/InAppBillingService/LUCK/widgets/lvl_widget
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/Class;)V
invokevirtual android/appwidget/AppWidgetManager/getAppWidgetIds(Landroid/content/ComponentName;)[I
aload 4
invokevirtual android/appwidget/AppWidgetManager/updateAppWidget([ILandroid/widget/RemoteViews;)V
aload 1
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
aload 1
ldc com/google/android/finsky/services/LicensingService
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/Class;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_2
if_icmpne L4
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc com/google/android/finsky/services/LicensingService
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/Class;)V
iconst_1
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "LVL_enable"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L5
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "LVL_enable"
iconst_1
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L5:
aload 1
ldc "LVL-ON"
iconst_0
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
invokevirtual android/widget/Toast/show()V
L3:
getstatic com/android/vending/billing/InAppBillingService/LUCK/widgets/lvl_widget/ACTION_WIDGET_RECEIVER_Updater Ljava/lang/String;
aload 3
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
L0:
iconst_1
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/appDisabler Z
aload 1
invokestatic android/appwidget/AppWidgetManager/getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;
astore 3
aload 0
aload 1
aload 3
aload 3
new android/content/ComponentName
dup
aload 1
ldc com/android/vending/billing/InAppBillingService/LUCK/widgets/lvl_widget
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/Class;)V
invokevirtual android/appwidget/AppWidgetManager/getAppWidgetIds(Landroid/content/ComponentName;)[I
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/widgets/lvl_widget/onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
L1:
aload 0
aload 1
aload 2
invokespecial android/appwidget/AppWidgetProvider/onReceive(Landroid/content/Context;Landroid/content/Intent;)V
return
L4:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc com/google/android/finsky/services/LicensingService
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/Class;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L6
iconst_1
invokestatic com/chelpus/Utils/market_licensing_services(Z)V
L6:
aload 1
ldc "LVL-OFF"
iconst_0
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
invokevirtual android/widget/Toast/show()V
goto L3
L2:
astore 3
aload 3
invokevirtual java/lang/Exception/printStackTrace()V
goto L1
.limit locals 6
.limit stack 8
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
new android/widget/RemoteViews
dup
aload 1
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
ldc_w 2130968638
invokespecial android/widget/RemoteViews/<init>(Ljava/lang/String;I)V
astore 4
new android/content/Intent
dup
aload 1
ldc com/android/vending/billing/InAppBillingService/LUCK/widgets/lvl_widget
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 5
aload 5
getstatic com/android/vending/billing/InAppBillingService/LUCK/widgets/lvl_widget/ACTION_WIDGET_RECEIVER Ljava/lang/String;
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 5
ldc "msg"
ldc "Hello Habrahabr"
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 4
ldc_w 2131558638
aload 1
iconst_0
aload 5
iconst_0
invokestatic android/app/PendingIntent/getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
invokevirtual android/widget/RemoteViews/setOnClickPendingIntent(ILandroid/app/PendingIntent;)V
aload 4
ldc_w 2131558638
ldc "LVL"
invokevirtual android/widget/RemoteViews/setTextViewText(ILjava/lang/CharSequence;)V
aload 4
ldc_w 2131558639
bipush 8
invokevirtual android/widget/RemoteViews/setViewVisibility(II)V
aload 1
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
aload 1
ldc com/google/android/finsky/services/LicensingService
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/Class;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_2
if_icmpne L0
aload 4
ldc_w 2131558638
ldc "#FF0000"
invokestatic android/graphics/Color/parseColor(Ljava/lang/String;)I
invokevirtual android/widget/RemoteViews/setTextColor(II)V
L1:
aload 2
aload 3
aload 4
invokevirtual android/appwidget/AppWidgetManager/updateAppWidget([ILandroid/widget/RemoteViews;)V
return
L0:
aload 4
ldc_w 2131558638
ldc "#00FF00"
invokestatic android/graphics/Color/parseColor(Ljava/lang/String;)I
invokevirtual android/widget/RemoteViews/setTextColor(II)V
goto L1
.limit locals 6
.limit stack 6
.end method
