.bytecode 50.0
.class public synchronized com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity
.super android/app/Activity
.inner class inner com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$1
.inner class inner com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$2
.inner class byPkgName inner com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$byPkgName outer com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity

.field private static final 'PREFS_NAME' Ljava/lang/String; = "com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget"

.field private static final 'PREF_PREFIX_KEY' Ljava/lang/String; = "appwidget_"

.field public 'context' Landroid/content/Context;

.field public 'lv' Landroid/widget/ListView;

.field 'mAppWidgetId' I

.field 'mAppWidgetText' Landroid/widget/EditText;

.field public 'packages' [Ljava/lang/String;

.field public 'sizeText' I

.method public <init>()V
aload 0
invokespecial android/app/Activity/<init>()V
aload 0
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/mAppWidgetId I
aload 0
aconst_null
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/packages [Ljava/lang/String;
aload 0
aconst_null
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/lv Landroid/widget/ListView;
aload 0
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/sizeText I
return
.limit locals 1
.limit stack 2
.end method

.method static deleteTitlePref(Landroid/content/Context;I)V
aload 0
ldc "com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget"
iconst_4
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 0
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "appwidget_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
pop
aload 0
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
.limit locals 2
.limit stack 3
.end method

.method static loadTitlePref(Landroid/content/Context;I)Ljava/lang/String;
aload 0
ldc "com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget"
iconst_4
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "appwidget_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aconst_null
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 0
aload 0
ifnull L0
aload 0
areturn
L0:
ldc "NOT_SAVED_APP_DISABLER"
areturn
.limit locals 2
.limit stack 3
.end method

.method static saveTitlePref(Landroid/content/Context;ILjava/lang/String;)V
aload 0
ldc "com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget"
iconst_4
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 0
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "appwidget_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
pop
aload 0
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
.limit locals 3
.limit stack 3
.end method

.method public onCreate(Landroid/os/Bundle;)V
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
iconst_0
istore 2
aload 0
aload 1
invokespecial android/app/Activity/onCreate(Landroid/os/Bundle;)V
aload 0
iconst_0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/setResult(I)V
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
astore 1
aload 1
ifnull L6
aload 0
aload 1
ldc "appWidgetId"
iconst_0
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/mAppWidgetId I
L6:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/mAppWidgetId I
ifne L7
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/finish()V
return
L7:
aload 0
aload 0
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/context Landroid/content/Context;
aload 0
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPackages()[Ljava/lang/String;
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/packages [Ljava/lang/String;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 1
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/packages [Ljava/lang/String;
astore 4
aload 4
arraylength
istore 3
L8:
iload 2
iload 3
if_icmpge L9
aload 4
iload 2
aaload
astore 5
aload 5
ldc "android"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 5
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/context Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
L0:
aload 1
new com/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem
dup
aload 5
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 5
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/applicationInfo Landroid/content/pm/ApplicationInfo;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
invokevirtual android/content/pm/ApplicationInfo/loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
invokespecial com/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L1:
iload 2
iconst_1
iadd
istore 2
goto L8
L2:
astore 5
aload 5
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
goto L1
L9:
aload 1
invokevirtual java/util/ArrayList/size()I
anewarray com/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem
astore 4
aload 1
aload 4
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
pop
L3:
aload 4
new com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$byPkgName
dup
aload 0
invokespecial com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$byPkgName/<init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;)V
invokestatic java/util/Arrays/sort([Ljava/lang/Object;Ljava/util/Comparator;)V
L4:
aload 0
new android/widget/ListView
dup
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/context Landroid/content/Context;
invokespecial android/widget/ListView/<init>(Landroid/content/Context;)V
putfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/lv Landroid/widget/ListView;
new com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$1
dup
aload 0
aload 0
ldc_w 2130968622
aload 4
invokespecial com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$1/<init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;Landroid/content/Context;I[Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;)V
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/adapt Landroid/widget/ArrayAdapter;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/lv Landroid/widget/ListView;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/adapt Landroid/widget/ArrayAdapter;
invokevirtual android/widget/ListView/setAdapter(Landroid/widget/ListAdapter;)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/lv Landroid/widget/ListView;
invokevirtual android/widget/ListView/invalidateViews()V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/lv Landroid/widget/ListView;
ldc_w -16777216
invokevirtual android/widget/ListView/setBackgroundColor(I)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/lv Landroid/widget/ListView;
new com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$2
dup
aload 0
invokespecial com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$2/<init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;)V
invokevirtual android/widget/ListView/setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
aload 0
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/lv Landroid/widget/ListView;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity/setContentView(Landroid/view/View;)V
return
L5:
astore 1
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
goto L4
.limit locals 6
.limit stack 7
.end method
