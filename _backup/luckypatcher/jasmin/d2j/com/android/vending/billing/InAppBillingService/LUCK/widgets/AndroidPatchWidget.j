.bytecode 50.0
.class public synchronized com/android/vending/billing/InAppBillingService/LUCK/widgets/AndroidPatchWidget
.super android/appwidget/AppWidgetProvider

.method public <init>()V
aload 0
invokespecial android/appwidget/AppWidgetProvider/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static updateAppWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V
aload 0
ldc_w 2131165790
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
pop
new android/widget/RemoteViews
dup
aload 0
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
ldc_w 2130968581
invokespecial android/widget/RemoteViews/<init>(Ljava/lang/String;I)V
astore 9
iconst_0
istore 3
iconst_0
istore 4
iconst_0
istore 6
iconst_0
istore 7
invokestatic com/chelpus/Utils/checkCoreJarPatch11()Z
ifeq L0
iconst_0
iconst_1
iadd
istore 3
iconst_1
istore 4
L0:
iload 3
istore 5
invokestatic com/chelpus/Utils/checkCoreJarPatch12()Z
ifeq L1
iload 3
iconst_1
iadd
istore 5
iconst_1
istore 4
L1:
iload 5
ifle L2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc_w 2131165300
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 5
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "/2 patched)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
L3:
invokestatic com/chelpus/Utils/checkCoreJarPatch20()Z
ifeq L4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc_w 2131165302
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n(patched)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
iconst_1
istore 6
L5:
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
invokestatic com/chelpus/Utils/checkCoreJarPatch30(Landroid/content/pm/PackageManager;)Z
ifeq L6
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc_w 2131165304
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n(patched)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
iconst_1
istore 3
L7:
new android/text/SpannableString
dup
aload 0
invokespecial android/text/SpannableString/<init>(Ljava/lang/CharSequence;)V
astore 8
iload 4
ifeq L8
ldc_w 2131165300
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 5
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "/2 patched)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/length()I
iadd
istore 4
aload 8
new android/text/style/ForegroundColorSpan
dup
ldc_w -16711936
invokespecial android/text/style/ForegroundColorSpan/<init>(I)V
ldc_w 2131165300
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
iload 4
iconst_0
invokevirtual android/text/SpannableString/setSpan(Ljava/lang/Object;III)V
L9:
iload 6
ifeq L10
ldc_w 2131165302
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
iload 4
iadd
iconst_1
iadd
ldc " (patched)"
invokevirtual java/lang/String/length()I
iadd
istore 5
aload 8
new android/text/style/ForegroundColorSpan
dup
ldc_w -16711936
invokespecial android/text/style/ForegroundColorSpan/<init>(I)V
ldc_w 2131165302
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
iload 4
iadd
iconst_1
iadd
iload 5
iconst_0
invokevirtual android/text/SpannableString/setSpan(Ljava/lang/Object;III)V
L11:
iload 3
ifeq L12
ldc_w 2131165304
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
istore 3
ldc " (patched)"
invokevirtual java/lang/String/length()I
istore 4
aload 8
new android/text/style/ForegroundColorSpan
dup
ldc_w -16711936
invokespecial android/text/style/ForegroundColorSpan/<init>(I)V
ldc_w 2131165304
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
iload 5
iadd
iconst_1
iadd
iload 3
iload 5
iadd
iconst_1
iadd
iload 4
iadd
iconst_0
invokevirtual android/text/SpannableString/setSpan(Ljava/lang/Object;III)V
L13:
aload 9
ldc_w 2131558442
aload 8
invokevirtual android/widget/RemoteViews/setTextViewText(ILjava/lang/CharSequence;)V
aload 1
iload 2
aload 9
invokevirtual android/appwidget/AppWidgetManager/updateAppWidget(ILandroid/widget/RemoteViews;)V
return
L2:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc_w 2131165300
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n(not patched)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
goto L3
L4:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc_w 2131165302
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n(not patched)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
goto L5
L6:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc_w 2131165304
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n(not patched)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
iload 7
istore 3
goto L7
L8:
ldc_w 2131165300
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
ldc " (not patched)"
invokevirtual java/lang/String/length()I
iadd
istore 4
aload 8
new android/text/style/ForegroundColorSpan
dup
ldc_w -65536
invokespecial android/text/style/ForegroundColorSpan/<init>(I)V
ldc_w 2131165300
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
iload 4
iconst_0
invokevirtual android/text/SpannableString/setSpan(Ljava/lang/Object;III)V
goto L9
L10:
ldc_w 2131165302
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
iconst_1
iadd
iload 4
iadd
ldc " (not patched)"
invokevirtual java/lang/String/length()I
iadd
istore 5
aload 8
new android/text/style/ForegroundColorSpan
dup
ldc_w -65536
invokespecial android/text/style/ForegroundColorSpan/<init>(I)V
ldc_w 2131165302
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
iconst_1
iadd
iload 4
iadd
iload 5
iconst_0
invokevirtual android/text/SpannableString/setSpan(Ljava/lang/Object;III)V
goto L11
L12:
ldc_w 2131165304
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
istore 3
ldc " (not patched)"
invokevirtual java/lang/String/length()I
istore 6
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 4
invokevirtual java/io/PrintStream/println(I)V
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 5
invokevirtual java/io/PrintStream/println(I)V
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/lang/String/length()I
invokevirtual java/io/PrintStream/println(I)V
aload 8
new android/text/style/ForegroundColorSpan
dup
ldc_w -65536
invokespecial android/text/style/ForegroundColorSpan/<init>(I)V
ldc_w 2131165304
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
iload 5
iadd
iconst_1
iadd
iload 3
iload 5
iadd
iconst_1
iadd
iload 6
iadd
iconst_0
invokevirtual android/text/SpannableString/setSpan(Ljava/lang/Object;III)V
goto L13
.limit locals 10
.limit stack 5
.end method

.method public onDisabled(Landroid/content/Context;)V
return
.limit locals 2
.limit stack 0
.end method

.method public onEnabled(Landroid/content/Context;)V
return
.limit locals 2
.limit stack 0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
new android/widget/RemoteViews
dup
aload 1
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
ldc_w 2130968581
invokespecial android/widget/RemoteViews/<init>(Ljava/lang/String;I)V
pop
aload 3
arraylength
istore 4
aload 3
arraylength
istore 5
iconst_0
istore 4
L0:
iload 4
iload 5
if_icmpge L1
aload 1
aload 2
aload 3
iload 4
iaload
invokestatic com/android/vending/billing/InAppBillingService/LUCK/widgets/AndroidPatchWidget/updateAppWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
return
.limit locals 6
.limit stack 4
.end method
