.bytecode 50.0
.class public synchronized com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter
.super android/widget/ArrayAdapter
.signature "Landroid/widget/ArrayAdapter<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"

.field public static final 'TEXT_DEFAULT' I = 0


.field public static final 'TEXT_LARGE' I = 2


.field public static final 'TEXT_MEDIUM' I = 1


.field public static final 'TEXT_SMALL' I = 0


.field 'context' Landroid/content/Context;

.field 'data' Ljava/util/List; signature "Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"

.field 'imgIcon' Landroid/widget/ImageView;

.field 'layoutResourceId' I

.field 'size' I

.field public 'sorter' Ljava/util/Comparator; signature "Ljava/util/Comparator<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"

.field 'txtStatus' Landroid/widget/TextView;

.field 'txtTitle' Landroid/widget/TextView;

.method public <init>(Landroid/content/Context;IILjava/util/List;)V
.signature "(Landroid/content/Context;IILjava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;)V"
aload 0
aload 1
iload 2
aload 4
invokespecial android/widget/ArrayAdapter/<init>(Landroid/content/Context;ILjava/util/List;)V
aload 0
aload 1
putfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/context Landroid/content/Context;
aload 0
iload 2
putfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/layoutResourceId I
aload 0
iload 3
putfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/size I
return
.limit locals 5
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;ILjava/util/List;)V
.signature "(Landroid/content/Context;ILjava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;)V"
aload 0
aload 1
iload 2
aload 3
invokespecial android/widget/ArrayAdapter/<init>(Landroid/content/Context;ILjava/util/List;)V
aload 0
aload 1
putfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/context Landroid/content/Context;
aload 0
iload 2
putfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/layoutResourceId I
aload 0
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/size I
aload 0
aload 3
putfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/data Ljava/util/List;
return
.limit locals 4
.limit stack 4
.end method

.method public getItem(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
iconst_0
istore 2
L0:
iload 2
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/getCount()I
if_icmpge L1
aload 0
iload 2
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/getItem(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PkgListItem
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/pkgName Ljava/lang/String;
aload 1
invokevirtual java/lang/String/contentEquals(Ljava/lang/CharSequence;)Z
ifeq L2
aload 0
iload 2
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/getItem(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PkgListItem
areturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aconst_null
areturn
.limit locals 3
.limit stack 2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
aload 0
iload 1
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/getItem(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PkgListItem
astore 4
aload 3
ldc_w -16777216
invokevirtual android/view/ViewGroup/setBackgroundColor(I)V
aload 4
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/name Ljava/lang/String;
ifnull L3
aload 4
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/boot_ads Z
ifne L3
aload 4
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/boot_custom Z
ifne L3
aload 4
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/boot_lvl Z
ifne L3
aload 4
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/boot_manual Z
ifne L3
new android/view/View
dup
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/context Landroid/content/Context;
invokespecial android/view/View/<init>(Landroid/content/Context;)V
areturn
L3:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/context Landroid/content/Context;
checkcast android/app/Activity
invokevirtual android/app/Activity/getLayoutInflater()Landroid/view/LayoutInflater;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/layoutResourceId I
aload 3
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 5
aload 5
ldc_w -16777216
invokevirtual android/view/View/setBackgroundColor(I)V
aload 0
aload 5
ldc_w 2131558478
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtTitle Landroid/widget/TextView;
aload 0
aload 5
ldc_w 2131558479
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtStatus Landroid/widget/TextView;
aload 0
aload 5
ldc_w 2131558456
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/imgIcon Landroid/widget/ImageView;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtTitle Landroid/widget/TextView;
aload 4
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/name Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/imgIcon Landroid/widget/ImageView;
iconst_1
invokevirtual android/widget/ImageView/setMaxHeight(I)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/imgIcon Landroid/widget/ImageView;
iconst_1
invokevirtual android/widget/ImageView/setMaxWidth(I)V
L0:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/imgIcon Landroid/widget/ImageView;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 4
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/pkgName Ljava/lang/String;
invokevirtual android/content/pm/PackageManager/getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
L1:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtTitle Landroid/widget/TextView;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/context Landroid/content/Context;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getSizeText()I
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtStatus Landroid/widget/TextView;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/context Landroid/content/Context;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getSizeText()I
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtTitle Landroid/widget/TextView;
ldc_w -7829368
invokevirtual android/widget/TextView/setTextColor(I)V
ldc ""
astore 3
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getRes()Landroid/content/res/Resources;
astore 6
aload 4
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/boot_ads Z
ifeq L4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
ldc_w 2131165678
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "; "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtTitle Landroid/widget/TextView;
ldc_w -16711681
invokevirtual android/widget/TextView/setTextColor(I)V
L4:
aload 3
astore 2
aload 4
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/boot_lvl Z
ifeq L5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
ldc_w 2131165680
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "; "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtTitle Landroid/widget/TextView;
ldc_w -16711936
invokevirtual android/widget/TextView/setTextColor(I)V
L5:
aload 2
astore 3
aload 4
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/boot_custom Z
ifeq L6
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
ldc_w 2131165679
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "; "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtTitle Landroid/widget/TextView;
sipush -256
invokevirtual android/widget/TextView/setTextColor(I)V
L6:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtStatus Landroid/widget/TextView;
aload 3
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 4
getfield com/android/vending/billing/InAppBillingService/LUCK/PkgListItem/pkgName Ljava/lang/String;
ldc_w 2131165754
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L7
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/imgIcon Landroid/widget/ImageView;
ldc_w 2130837551
invokevirtual android/widget/ImageView/setImageResource(I)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtStatus Landroid/widget/TextView;
ldc_w 2131165755
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L7:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtStatus Landroid/widget/TextView;
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/context Landroid/content/Context;
ldc_w 16973894
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtStatus Landroid/widget/TextView;
ldc_w -7829368
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtTitle Landroid/widget/TextView;
ldc_w -16777216
invokevirtual android/widget/TextView/setBackgroundColor(I)V
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/txtStatus Landroid/widget/TextView;
ldc_w -16777216
invokevirtual android/widget/TextView/setBackgroundColor(I)V
aload 5
areturn
L2:
astore 2
aload 2
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
goto L1
.limit locals 7
.limit stack 4
.end method

.method public getViewTypeCount()I
iconst_2
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setTextSize(I)V
aload 0
iload 1
putfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/size I
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/notifyDataSetChanged()V
return
.limit locals 2
.limit stack 2
.end method

.method public sort()V
aload 0
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/sorter Ljava/util/Comparator;
invokespecial android/widget/ArrayAdapter/sort(Ljava/util/Comparator;)V
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/BootListItemAdapter/notifyDataSetChanged()V
return
.limit locals 1
.limit stack 2
.end method
