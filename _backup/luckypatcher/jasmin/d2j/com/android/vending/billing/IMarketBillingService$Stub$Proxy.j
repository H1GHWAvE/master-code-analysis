.bytecode 50.0
.class synchronized com/android/vending/billing/IMarketBillingService$Stub$Proxy
.super java/lang/Object
.implements com/android/vending/billing/IMarketBillingService
.inner class public static abstract Stub inner com/android/vending/billing/IMarketBillingService$Stub outer com/android/vending/billing/IMarketBillingService
.inner class private static Proxy inner com/android/vending/billing/IMarketBillingService$Stub$Proxy outer com/android/vending/billing/IMarketBillingService$Stub

.field private 'mRemote' Landroid/os/IBinder;

.method <init>(Landroid/os/IBinder;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/android/vending/billing/IMarketBillingService$Stub$Proxy/mRemote Landroid/os/IBinder;
return
.limit locals 2
.limit stack 2
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
getfield com/android/vending/billing/IMarketBillingService$Stub$Proxy/mRemote Landroid/os/IBinder;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
ldc "com.android.vending.billing.IMarketBillingService"
areturn
.limit locals 1
.limit stack 1
.end method

.method public sendBillingRequest(Landroid/os/Bundle;)Landroid/os/Bundle;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "com.android.vending.billing.IMarketBillingService"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 2
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 2
iconst_0
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 0
getfield com/android/vending/billing/IMarketBillingService$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_1
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
aload 3
invokevirtual android/os/Parcel/readInt()I
ifeq L8
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 3
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 1
L5:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
areturn
L6:
aload 2
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L7:
goto L4
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
L8:
aconst_null
astore 1
goto L5
.limit locals 4
.limit stack 5
.end method
