.bytecode 50.0
.class public synchronized com/google/android/vending/licensing/APKExpansionPolicy
.super java/lang/Object
.implements com/google/android/vending/licensing/Policy

.field private static final 'DEFAULT_MAX_RETRIES' Ljava/lang/String; = "0"

.field private static final 'DEFAULT_RETRY_COUNT' Ljava/lang/String; = "0"

.field private static final 'DEFAULT_RETRY_UNTIL' Ljava/lang/String; = "0"

.field private static final 'DEFAULT_VALIDITY_TIMESTAMP' Ljava/lang/String; = "0"

.field public static final 'MAIN_FILE_URL_INDEX' I = 0


.field private static final 'MILLIS_PER_MINUTE' J = 60000L


.field public static final 'PATCH_FILE_URL_INDEX' I = 1


.field private static final 'PREFS_FILE' Ljava/lang/String; = "com.android.vending.licensing.APKExpansionPolicy"

.field private static final 'PREF_LAST_RESPONSE' Ljava/lang/String; = "lastResponse"

.field private static final 'PREF_MAX_RETRIES' Ljava/lang/String; = "maxRetries"

.field private static final 'PREF_RETRY_COUNT' Ljava/lang/String; = "retryCount"

.field private static final 'PREF_RETRY_UNTIL' Ljava/lang/String; = "retryUntil"

.field private static final 'PREF_VALIDITY_TIMESTAMP' Ljava/lang/String; = "validityTimestamp"

.field private static final 'TAG' Ljava/lang/String; = "APKExpansionPolicy"

.field private 'mExpansionFileNames' Ljava/util/Vector; signature "Ljava/util/Vector<Ljava/lang/String;>;"

.field private 'mExpansionFileSizes' Ljava/util/Vector; signature "Ljava/util/Vector<Ljava/lang/Long;>;"

.field private 'mExpansionURLs' Ljava/util/Vector; signature "Ljava/util/Vector<Ljava/lang/String;>;"

.field private 'mLastResponse' I

.field private 'mLastResponseTime' J

.field private 'mMaxRetries' J

.field private 'mPreferences' Lcom/google/android/vending/licensing/PreferenceObfuscator;

.field private 'mRetryCount' J

.field private 'mRetryUntil' J

.field private 'mValidityTimestamp' J

.method public <init>(Landroid/content/Context;Lcom/google/android/vending/licensing/Obfuscator;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lconst_0
putfield com/google/android/vending/licensing/APKExpansionPolicy/mLastResponseTime J
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionURLs Ljava/util/Vector;
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionFileNames Ljava/util/Vector;
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionFileSizes Ljava/util/Vector;
aload 0
new com/google/android/vending/licensing/PreferenceObfuscator
dup
aload 1
ldc "com.android.vending.licensing.APKExpansionPolicy"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
aload 2
invokespecial com/google/android/vending/licensing/PreferenceObfuscator/<init>(Landroid/content/SharedPreferences;Lcom/google/android/vending/licensing/Obfuscator;)V
putfield com/google/android/vending/licensing/APKExpansionPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
aload 0
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "lastResponse"
sipush 291
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
putfield com/google/android/vending/licensing/APKExpansionPolicy/mLastResponse I
aload 0
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "validityTimestamp"
ldc "0"
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/google/android/vending/licensing/APKExpansionPolicy/mValidityTimestamp J
aload 0
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "retryUntil"
ldc "0"
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/google/android/vending/licensing/APKExpansionPolicy/mRetryUntil J
aload 0
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "maxRetries"
ldc "0"
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/google/android/vending/licensing/APKExpansionPolicy/mMaxRetries J
aload 0
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "retryCount"
ldc "0"
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/google/android/vending/licensing/APKExpansionPolicy/mRetryCount J
return
.limit locals 3
.limit stack 6
.end method

.method private decodeExtras(Ljava/lang/String;)Ljava/util/Map;
.signature "(Ljava/lang/String;)Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
.catch java/net/URISyntaxException from L0 to L1 using L2
.catch java/net/URISyntaxException from L1 to L3 using L2
.catch java/net/URISyntaxException from L4 to L5 using L2
.catch java/net/URISyntaxException from L6 to L7 using L2
.catch java/net/URISyntaxException from L8 to L9 using L2
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 3
L0:
new java/net/URI
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "?"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/net/URI/<init>(Ljava/lang/String;)V
ldc "UTF-8"
invokestatic org/apache/http/client/utils/URLEncodedUtils/parse(Ljava/net/URI;Ljava/lang/String;)Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L1:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L10
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/apache/http/NameValuePair
astore 5
aload 5
invokeinterface org/apache/http/NameValuePair/getName()Ljava/lang/String; 0
astore 1
L3:
iconst_0
istore 2
L4:
aload 3
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L8
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 5
invokeinterface org/apache/http/NameValuePair/getName()Ljava/lang/String; 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 1
L5:
iload 2
iconst_1
iadd
istore 2
L6:
aload 1
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L7:
goto L4
L8:
aload 3
aload 1
aload 5
invokeinterface org/apache/http/NameValuePair/getValue()Ljava/lang/String; 0
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L9:
goto L1
L2:
astore 1
ldc "APKExpansionPolicy"
ldc "Invalid syntax error while decoding extras data from server."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L10:
aload 3
areturn
.limit locals 6
.limit stack 4
.end method

.method private setLastResponse(I)V
aload 0
invokestatic java/lang/System/currentTimeMillis()J
putfield com/google/android/vending/licensing/APKExpansionPolicy/mLastResponseTime J
aload 0
iload 1
putfield com/google/android/vending/licensing/APKExpansionPolicy/mLastResponse I
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "lastResponse"
iload 1
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/putString(Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method private setMaxRetries(Ljava/lang/String;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
L0:
aload 1
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 2
L1:
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 5
aload 1
astore 4
aload 5
astore 1
L3:
aload 0
aload 1
invokevirtual java/lang/Long/longValue()J
putfield com/google/android/vending/licensing/APKExpansionPolicy/mMaxRetries J
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "maxRetries"
aload 4
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/putString(Ljava/lang/String;Ljava/lang/String;)V
return
L2:
astore 1
ldc "APKExpansionPolicy"
ldc "Licence retry count (GR) missing, grace period disabled"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
ldc "0"
astore 4
lconst_0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 1
goto L3
.limit locals 6
.limit stack 3
.end method

.method private setRetryCount(J)V
aload 0
lload 1
putfield com/google/android/vending/licensing/APKExpansionPolicy/mRetryCount J
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "retryCount"
lload 1
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/putString(Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method

.method private setRetryUntil(Ljava/lang/String;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
L0:
aload 1
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 2
L1:
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 5
aload 1
astore 4
aload 5
astore 1
L3:
aload 0
aload 1
invokevirtual java/lang/Long/longValue()J
putfield com/google/android/vending/licensing/APKExpansionPolicy/mRetryUntil J
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "retryUntil"
aload 4
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/putString(Ljava/lang/String;Ljava/lang/String;)V
return
L2:
astore 1
ldc "APKExpansionPolicy"
ldc "License retry timestamp (GT) missing, grace period disabled"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
ldc "0"
astore 4
lconst_0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 1
goto L3
.limit locals 6
.limit stack 3
.end method

.method private setValidityTimestamp(Ljava/lang/String;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
L0:
aload 1
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 2
L1:
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 5
aload 1
astore 4
aload 5
astore 1
L3:
aload 0
aload 1
invokevirtual java/lang/Long/longValue()J
putfield com/google/android/vending/licensing/APKExpansionPolicy/mValidityTimestamp J
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "validityTimestamp"
aload 4
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/putString(Ljava/lang/String;Ljava/lang/String;)V
return
L2:
astore 1
ldc "APKExpansionPolicy"
ldc "License validity timestamp (VT) missing, caching for a minute"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 60000L
ladd
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 1
aload 1
invokevirtual java/lang/Long/longValue()J
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
astore 4
goto L3
.limit locals 6
.limit stack 4
.end method

.method public allowAccess()Z
iconst_0
istore 2
invokestatic java/lang/System/currentTimeMillis()J
lstore 3
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mLastResponse I
sipush 256
if_icmpne L0
iload 2
istore 1
lload 3
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mValidityTimestamp J
lcmp
ifgt L1
iconst_1
istore 1
L1:
iload 1
ireturn
L0:
iload 2
istore 1
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mLastResponse I
sipush 291
if_icmpne L1
iload 2
istore 1
lload 3
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mLastResponseTime J
ldc2_w 60000L
ladd
lcmp
ifge L1
lload 3
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mRetryUntil J
lcmp
ifle L2
iload 2
istore 1
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mRetryCount J
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mMaxRetries J
lcmp
ifgt L1
L2:
iconst_1
ireturn
.limit locals 5
.limit stack 6
.end method

.method public getExpansionFileName(I)Ljava/lang/String;
iload 1
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionFileNames Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmpge L0
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionFileNames Ljava/util/Vector;
iload 1
invokevirtual java/util/Vector/elementAt(I)Ljava/lang/Object;
checkcast java/lang/String
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public getExpansionFileSize(I)J
iload 1
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionFileSizes Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmpge L0
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionFileSizes Ljava/util/Vector;
iload 1
invokevirtual java/util/Vector/elementAt(I)Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
lreturn
L0:
ldc2_w -1L
lreturn
.limit locals 2
.limit stack 2
.end method

.method public getExpansionURL(I)Ljava/lang/String;
iload 1
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionURLs Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmpge L0
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionURLs Ljava/util/Vector;
iload 1
invokevirtual java/util/Vector/elementAt(I)Ljava/lang/Object;
checkcast java/lang/String
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public getExpansionURLCount()I
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionURLs Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMaxRetries()J
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mMaxRetries J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getRetryCount()J
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mRetryCount J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getRetryUntil()J
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mRetryUntil J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getValidityTimestamp()J
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mValidityTimestamp J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public processServerResponse(ILcom/google/android/vending/licensing/ResponseData;)V
iload 1
sipush 291
if_icmpeq L0
aload 0
lconst_0
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/setRetryCount(J)V
L1:
iload 1
sipush 256
if_icmpne L2
aload 0
aload 2
getfield com/google/android/vending/licensing/ResponseData/extra Ljava/lang/String;
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/decodeExtras(Ljava/lang/String;)Ljava/util/Map;
astore 2
aload 0
iload 1
putfield com/google/android/vending/licensing/APKExpansionPolicy/mLastResponse I
aload 0
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 60000L
ladd
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/setValidityTimestamp(Ljava/lang/String;)V
aload 2
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L3:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 4
aload 4
ldc "VT"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
aload 0
aload 2
aload 4
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/setValidityTimestamp(Ljava/lang/String;)V
goto L3
L0:
aload 0
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mRetryCount J
lconst_1
ladd
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/setRetryCount(J)V
goto L1
L5:
aload 4
ldc "GT"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L6
aload 0
aload 2
aload 4
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/setRetryUntil(Ljava/lang/String;)V
goto L3
L6:
aload 4
ldc "GR"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L7
aload 0
aload 2
aload 4
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/setMaxRetries(Ljava/lang/String;)V
goto L3
L7:
aload 4
ldc "FILE_URL"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L8
aload 0
aload 4
ldc "FILE_URL"
invokevirtual java/lang/String/length()I
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
iconst_1
isub
aload 2
aload 4
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokevirtual com/google/android/vending/licensing/APKExpansionPolicy/setExpansionURL(ILjava/lang/String;)V
goto L3
L8:
aload 4
ldc "FILE_NAME"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L9
aload 0
aload 4
ldc "FILE_NAME"
invokevirtual java/lang/String/length()I
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
iconst_1
isub
aload 2
aload 4
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokevirtual com/google/android/vending/licensing/APKExpansionPolicy/setExpansionFileName(ILjava/lang/String;)V
goto L3
L9:
aload 4
ldc "FILE_SIZE"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L3
aload 0
aload 4
ldc "FILE_SIZE"
invokevirtual java/lang/String/length()I
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
iconst_1
isub
aload 2
aload 4
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
invokevirtual com/google/android/vending/licensing/APKExpansionPolicy/setExpansionFileSize(IJ)V
goto L3
L2:
iload 1
sipush 561
if_icmpne L4
aload 0
ldc "0"
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/setValidityTimestamp(Ljava/lang/String;)V
aload 0
ldc "0"
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/setRetryUntil(Ljava/lang/String;)V
aload 0
ldc "0"
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/setMaxRetries(Ljava/lang/String;)V
L4:
aload 0
iload 1
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/setLastResponse(I)V
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/commit()V
return
.limit locals 5
.limit stack 5
.end method

.method public resetPolicy()V
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "lastResponse"
sipush 291
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 0
ldc "0"
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/setRetryUntil(Ljava/lang/String;)V
aload 0
ldc "0"
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/setMaxRetries(Ljava/lang/String;)V
aload 0
ldc "0"
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/setRetryCount(J)V
aload 0
ldc "0"
invokespecial com/google/android/vending/licensing/APKExpansionPolicy/setValidityTimestamp(Ljava/lang/String;)V
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/commit()V
return
.limit locals 1
.limit stack 3
.end method

.method public setExpansionFileName(ILjava/lang/String;)V
iload 1
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionFileNames Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmplt L0
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionFileNames Ljava/util/Vector;
iload 1
iconst_1
iadd
invokevirtual java/util/Vector/setSize(I)V
L0:
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionFileNames Ljava/util/Vector;
iload 1
aload 2
invokevirtual java/util/Vector/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 3
.limit stack 3
.end method

.method public setExpansionFileSize(IJ)V
iload 1
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionFileSizes Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmplt L0
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionFileSizes Ljava/util/Vector;
iload 1
iconst_1
iadd
invokevirtual java/util/Vector/setSize(I)V
L0:
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionFileSizes Ljava/util/Vector;
iload 1
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/Vector/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 4
.limit stack 4
.end method

.method public setExpansionURL(ILjava/lang/String;)V
iload 1
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionURLs Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmplt L0
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionURLs Ljava/util/Vector;
iload 1
iconst_1
iadd
invokevirtual java/util/Vector/setSize(I)V
L0:
aload 0
getfield com/google/android/vending/licensing/APKExpansionPolicy/mExpansionURLs Ljava/util/Vector;
iload 1
aload 2
invokevirtual java/util/Vector/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 3
.limit stack 3
.end method
