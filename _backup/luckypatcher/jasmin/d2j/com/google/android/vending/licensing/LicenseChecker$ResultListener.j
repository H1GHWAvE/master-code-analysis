.bytecode 50.0
.class public synchronized com/google/android/vending/licensing/LicenseChecker$ResultListener
.super com/google/android/vending/licensing/ILicenseResultListener$Stub
.inner class public ResultListener inner com/google/android/vending/licensing/LicenseChecker$ResultListener outer com/google/android/vending/licensing/LicenseChecker
.inner class inner com/google/android/vending/licensing/LicenseChecker$ResultListener$1
.inner class inner com/google/android/vending/licensing/LicenseChecker$ResultListener$2

.field private static final 'ERROR_CONTACTING_SERVER' I = 257


.field private static final 'ERROR_INVALID_PACKAGE_NAME' I = 258


.field private static final 'ERROR_NON_MATCHING_UID' I = 259


.field private 'mOnTimeout' Ljava/lang/Runnable;

.field private final 'mValidator' Lcom/google/android/vending/licensing/LicenseValidator;

.field final synthetic 'this$0' Lcom/google/android/vending/licensing/LicenseChecker;

.method public <init>(Lcom/google/android/vending/licensing/LicenseChecker;Lcom/google/android/vending/licensing/LicenseValidator;)V
aload 0
aload 1
putfield com/google/android/vending/licensing/LicenseChecker$ResultListener/this$0 Lcom/google/android/vending/licensing/LicenseChecker;
aload 0
invokespecial com/google/android/vending/licensing/ILicenseResultListener$Stub/<init>()V
aload 0
aload 2
putfield com/google/android/vending/licensing/LicenseChecker$ResultListener/mValidator Lcom/google/android/vending/licensing/LicenseValidator;
aload 0
new com/google/android/vending/licensing/LicenseChecker$ResultListener$1
dup
aload 0
aload 1
invokespecial com/google/android/vending/licensing/LicenseChecker$ResultListener$1/<init>(Lcom/google/android/vending/licensing/LicenseChecker$ResultListener;Lcom/google/android/vending/licensing/LicenseChecker;)V
putfield com/google/android/vending/licensing/LicenseChecker$ResultListener/mOnTimeout Ljava/lang/Runnable;
aload 0
invokespecial com/google/android/vending/licensing/LicenseChecker$ResultListener/startTimeout()V
return
.limit locals 3
.limit stack 5
.end method

.method static synthetic access$000(Lcom/google/android/vending/licensing/LicenseChecker$ResultListener;)Lcom/google/android/vending/licensing/LicenseValidator;
aload 0
getfield com/google/android/vending/licensing/LicenseChecker$ResultListener/mValidator Lcom/google/android/vending/licensing/LicenseValidator;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$400(Lcom/google/android/vending/licensing/LicenseChecker$ResultListener;)V
aload 0
invokespecial com/google/android/vending/licensing/LicenseChecker$ResultListener/clearTimeout()V
return
.limit locals 1
.limit stack 1
.end method

.method private clearTimeout()V
ldc "LicenseChecker"
ldc "Clearing timeout."
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/google/android/vending/licensing/LicenseChecker$ResultListener/this$0 Lcom/google/android/vending/licensing/LicenseChecker;
invokestatic com/google/android/vending/licensing/LicenseChecker/access$600(Lcom/google/android/vending/licensing/LicenseChecker;)Landroid/os/Handler;
aload 0
getfield com/google/android/vending/licensing/LicenseChecker$ResultListener/mOnTimeout Ljava/lang/Runnable;
invokevirtual android/os/Handler/removeCallbacks(Ljava/lang/Runnable;)V
return
.limit locals 1
.limit stack 2
.end method

.method private startTimeout()V
ldc "LicenseChecker"
ldc "Start monitoring timeout."
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/google/android/vending/licensing/LicenseChecker$ResultListener/this$0 Lcom/google/android/vending/licensing/LicenseChecker;
invokestatic com/google/android/vending/licensing/LicenseChecker/access$600(Lcom/google/android/vending/licensing/LicenseChecker;)Landroid/os/Handler;
aload 0
getfield com/google/android/vending/licensing/LicenseChecker$ResultListener/mOnTimeout Ljava/lang/Runnable;
ldc2_w 10000L
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 1
.limit stack 4
.end method

.method public verifyLicense(ILjava/lang/String;Ljava/lang/String;)V
aload 0
getfield com/google/android/vending/licensing/LicenseChecker$ResultListener/this$0 Lcom/google/android/vending/licensing/LicenseChecker;
aload 2
putfield com/google/android/vending/licensing/LicenseChecker/signedData Ljava/lang/String;
aload 0
getfield com/google/android/vending/licensing/LicenseChecker$ResultListener/this$0 Lcom/google/android/vending/licensing/LicenseChecker;
aload 3
putfield com/google/android/vending/licensing/LicenseChecker/signature Ljava/lang/String;
aload 0
getfield com/google/android/vending/licensing/LicenseChecker$ResultListener/this$0 Lcom/google/android/vending/licensing/LicenseChecker;
iload 1
putfield com/google/android/vending/licensing/LicenseChecker/responseCode I
aload 0
getfield com/google/android/vending/licensing/LicenseChecker$ResultListener/this$0 Lcom/google/android/vending/licensing/LicenseChecker;
invokestatic com/google/android/vending/licensing/LicenseChecker/access$600(Lcom/google/android/vending/licensing/LicenseChecker;)Landroid/os/Handler;
new com/google/android/vending/licensing/LicenseChecker$ResultListener$2
dup
aload 0
iload 1
aload 2
aload 3
invokespecial com/google/android/vending/licensing/LicenseChecker$ResultListener$2/<init>(Lcom/google/android/vending/licensing/LicenseChecker$ResultListener;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual android/os/Handler/post(Ljava/lang/Runnable;)Z
pop
return
.limit locals 4
.limit stack 7
.end method
