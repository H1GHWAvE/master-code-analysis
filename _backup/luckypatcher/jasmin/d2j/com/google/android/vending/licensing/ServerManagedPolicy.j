.bytecode 50.0
.class public synchronized com/google/android/vending/licensing/ServerManagedPolicy
.super java/lang/Object
.implements com/google/android/vending/licensing/Policy

.field private static final 'DEFAULT_MAX_RETRIES' Ljava/lang/String; = "0"

.field private static final 'DEFAULT_RETRY_COUNT' Ljava/lang/String; = "0"

.field private static final 'DEFAULT_RETRY_UNTIL' Ljava/lang/String; = "0"

.field private static final 'DEFAULT_VALIDITY_TIMESTAMP' Ljava/lang/String; = "0"

.field private static final 'MILLIS_PER_MINUTE' J = 60000L


.field private static final 'PREFS_FILE' Ljava/lang/String; = "com.android.vending.licensing.ServerManagedPolicy"

.field private static final 'PREF_LAST_RESPONSE' Ljava/lang/String; = "lastResponse"

.field private static final 'PREF_MAX_RETRIES' Ljava/lang/String; = "maxRetries"

.field private static final 'PREF_RETRY_COUNT' Ljava/lang/String; = "retryCount"

.field private static final 'PREF_RETRY_UNTIL' Ljava/lang/String; = "retryUntil"

.field private static final 'PREF_VALIDITY_TIMESTAMP' Ljava/lang/String; = "validityTimestamp"

.field private static final 'TAG' Ljava/lang/String; = "ServerManagedPolicy"

.field private 'mLastResponse' I

.field private 'mLastResponseTime' J

.field private 'mMaxRetries' J

.field private 'mPreferences' Lcom/google/android/vending/licensing/PreferenceObfuscator;

.field private 'mRetryCount' J

.field private 'mRetryUntil' J

.field private 'mValidityTimestamp' J

.method public <init>(Landroid/content/Context;Lcom/google/android/vending/licensing/Obfuscator;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lconst_0
putfield com/google/android/vending/licensing/ServerManagedPolicy/mLastResponseTime J
aload 0
new com/google/android/vending/licensing/PreferenceObfuscator
dup
aload 1
ldc "com.android.vending.licensing.ServerManagedPolicy"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
aload 2
invokespecial com/google/android/vending/licensing/PreferenceObfuscator/<init>(Landroid/content/SharedPreferences;Lcom/google/android/vending/licensing/Obfuscator;)V
putfield com/google/android/vending/licensing/ServerManagedPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
aload 0
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "lastResponse"
sipush 291
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
putfield com/google/android/vending/licensing/ServerManagedPolicy/mLastResponse I
aload 0
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "validityTimestamp"
ldc "0"
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/google/android/vending/licensing/ServerManagedPolicy/mValidityTimestamp J
aload 0
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "retryUntil"
ldc "0"
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/google/android/vending/licensing/ServerManagedPolicy/mRetryUntil J
aload 0
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "maxRetries"
ldc "0"
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/google/android/vending/licensing/ServerManagedPolicy/mMaxRetries J
aload 0
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "retryCount"
ldc "0"
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/google/android/vending/licensing/ServerManagedPolicy/mRetryCount J
return
.limit locals 3
.limit stack 6
.end method

.method private decodeExtras(Ljava/lang/String;)Ljava/util/Map;
.signature "(Ljava/lang/String;)Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
.catch java/net/URISyntaxException from L0 to L1 using L2
.catch java/net/URISyntaxException from L1 to L3 using L2
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 2
L0:
new java/net/URI
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "?"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/net/URI/<init>(Ljava/lang/String;)V
ldc "UTF-8"
invokestatic org/apache/http/client/utils/URLEncodedUtils/parse(Ljava/net/URI;Ljava/lang/String;)Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L1:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/apache/http/NameValuePair
astore 3
aload 2
aload 3
invokeinterface org/apache/http/NameValuePair/getName()Ljava/lang/String; 0
aload 3
invokeinterface org/apache/http/NameValuePair/getValue()Ljava/lang/String; 0
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L3:
goto L1
L2:
astore 1
ldc "ServerManagedPolicy"
ldc "Invalid syntax error while decoding extras data from server."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L4:
aload 2
areturn
.limit locals 4
.limit stack 4
.end method

.method private setLastResponse(I)V
aload 0
invokestatic java/lang/System/currentTimeMillis()J
putfield com/google/android/vending/licensing/ServerManagedPolicy/mLastResponseTime J
aload 0
iload 1
putfield com/google/android/vending/licensing/ServerManagedPolicy/mLastResponse I
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "lastResponse"
iload 1
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/putString(Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method private setMaxRetries(Ljava/lang/String;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
L0:
aload 1
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 2
L1:
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 5
aload 1
astore 4
aload 5
astore 1
L3:
aload 0
aload 1
invokevirtual java/lang/Long/longValue()J
putfield com/google/android/vending/licensing/ServerManagedPolicy/mMaxRetries J
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "maxRetries"
aload 4
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/putString(Ljava/lang/String;Ljava/lang/String;)V
return
L2:
astore 1
ldc "ServerManagedPolicy"
ldc "Licence retry count (GR) missing, grace period disabled"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
ldc "0"
astore 4
lconst_0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 1
goto L3
.limit locals 6
.limit stack 3
.end method

.method private setRetryCount(J)V
aload 0
lload 1
putfield com/google/android/vending/licensing/ServerManagedPolicy/mRetryCount J
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "retryCount"
lload 1
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/putString(Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method

.method private setRetryUntil(Ljava/lang/String;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
L0:
aload 1
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 2
L1:
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 5
aload 1
astore 4
aload 5
astore 1
L3:
aload 0
aload 1
invokevirtual java/lang/Long/longValue()J
putfield com/google/android/vending/licensing/ServerManagedPolicy/mRetryUntil J
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "retryUntil"
aload 4
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/putString(Ljava/lang/String;Ljava/lang/String;)V
return
L2:
astore 1
ldc "ServerManagedPolicy"
ldc "License retry timestamp (GT) missing, grace period disabled"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
ldc "0"
astore 4
lconst_0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 1
goto L3
.limit locals 6
.limit stack 3
.end method

.method private setValidityTimestamp(Ljava/lang/String;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
L0:
aload 1
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 2
L1:
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 5
aload 1
astore 4
aload 5
astore 1
L3:
aload 0
aload 1
invokevirtual java/lang/Long/longValue()J
putfield com/google/android/vending/licensing/ServerManagedPolicy/mValidityTimestamp J
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
ldc "validityTimestamp"
aload 4
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/putString(Ljava/lang/String;Ljava/lang/String;)V
return
L2:
astore 1
ldc "ServerManagedPolicy"
ldc "License validity timestamp (VT) missing, caching for a minute"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 60000L
ladd
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 1
aload 1
invokevirtual java/lang/Long/longValue()J
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
astore 4
goto L3
.limit locals 6
.limit stack 4
.end method

.method public allowAccess()Z
iconst_0
istore 2
invokestatic java/lang/System/currentTimeMillis()J
lstore 3
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mLastResponse I
sipush 256
if_icmpne L0
iload 2
istore 1
lload 3
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mValidityTimestamp J
lcmp
ifgt L1
iconst_1
istore 1
L1:
iload 1
ireturn
L0:
iload 2
istore 1
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mLastResponse I
sipush 291
if_icmpne L1
iload 2
istore 1
lload 3
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mLastResponseTime J
ldc2_w 60000L
ladd
lcmp
ifge L1
lload 3
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mRetryUntil J
lcmp
ifle L2
iload 2
istore 1
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mRetryCount J
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mMaxRetries J
lcmp
ifgt L1
L2:
iconst_1
ireturn
.limit locals 5
.limit stack 6
.end method

.method public getMaxRetries()J
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mMaxRetries J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getRetryCount()J
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mRetryCount J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getRetryUntil()J
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mRetryUntil J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getValidityTimestamp()J
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mValidityTimestamp J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public processServerResponse(ILcom/google/android/vending/licensing/ResponseData;)V
iload 1
sipush 291
if_icmpeq L0
aload 0
lconst_0
invokespecial com/google/android/vending/licensing/ServerManagedPolicy/setRetryCount(J)V
L1:
iload 1
sipush 256
if_icmpne L2
aload 0
aload 2
getfield com/google/android/vending/licensing/ResponseData/extra Ljava/lang/String;
invokespecial com/google/android/vending/licensing/ServerManagedPolicy/decodeExtras(Ljava/lang/String;)Ljava/util/Map;
astore 2
aload 0
iload 1
putfield com/google/android/vending/licensing/ServerManagedPolicy/mLastResponse I
aload 0
aload 2
ldc "VT"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokespecial com/google/android/vending/licensing/ServerManagedPolicy/setValidityTimestamp(Ljava/lang/String;)V
aload 0
aload 2
ldc "GT"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokespecial com/google/android/vending/licensing/ServerManagedPolicy/setRetryUntil(Ljava/lang/String;)V
aload 0
aload 2
ldc "GR"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokespecial com/google/android/vending/licensing/ServerManagedPolicy/setMaxRetries(Ljava/lang/String;)V
L3:
aload 0
iload 1
invokespecial com/google/android/vending/licensing/ServerManagedPolicy/setLastResponse(I)V
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mPreferences Lcom/google/android/vending/licensing/PreferenceObfuscator;
invokevirtual com/google/android/vending/licensing/PreferenceObfuscator/commit()V
return
L0:
aload 0
aload 0
getfield com/google/android/vending/licensing/ServerManagedPolicy/mRetryCount J
lconst_1
ladd
invokespecial com/google/android/vending/licensing/ServerManagedPolicy/setRetryCount(J)V
goto L1
L2:
iload 1
sipush 561
if_icmpne L3
aload 0
ldc "0"
invokespecial com/google/android/vending/licensing/ServerManagedPolicy/setValidityTimestamp(Ljava/lang/String;)V
aload 0
ldc "0"
invokespecial com/google/android/vending/licensing/ServerManagedPolicy/setRetryUntil(Ljava/lang/String;)V
aload 0
ldc "0"
invokespecial com/google/android/vending/licensing/ServerManagedPolicy/setMaxRetries(Ljava/lang/String;)V
goto L3
.limit locals 3
.limit stack 5
.end method
