.bytecode 50.0
.class public synchronized com/google/android/vending/licensing/util/Base64
.super java/lang/Object

.field static final synthetic '$assertionsDisabled' Z

.field private static final 'ALPHABET' [B

.field private static final 'DECODABET' [B

.field public static final 'DECODE' Z = 0


.field public static final 'ENCODE' Z = 1


.field private static final 'EQUALS_SIGN' B = 61


.field private static final 'EQUALS_SIGN_ENC' B = -1


.field private static final 'NEW_LINE' B = 10


.field private static final 'WEBSAFE_ALPHABET' [B

.field private static final 'WEBSAFE_DECODABET' [B

.field private static final 'WHITE_SPACE_ENC' B = -5


.method static <clinit>()V
ldc com/google/android/vending/licensing/util/Base64
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic com/google/android/vending/licensing/util/Base64/$assertionsDisabled Z
bipush 64
newarray byte
dup
iconst_0
ldc_w 65
bastore
dup
iconst_1
ldc_w 66
bastore
dup
iconst_2
ldc_w 67
bastore
dup
iconst_3
ldc_w 68
bastore
dup
iconst_4
ldc_w 69
bastore
dup
iconst_5
ldc_w 70
bastore
dup
bipush 6
ldc_w 71
bastore
dup
bipush 7
ldc_w 72
bastore
dup
bipush 8
ldc_w 73
bastore
dup
bipush 9
ldc_w 74
bastore
dup
bipush 10
ldc_w 75
bastore
dup
bipush 11
ldc_w 76
bastore
dup
bipush 12
ldc_w 77
bastore
dup
bipush 13
ldc_w 78
bastore
dup
bipush 14
ldc_w 79
bastore
dup
bipush 15
ldc_w 80
bastore
dup
bipush 16
ldc_w 81
bastore
dup
bipush 17
ldc_w 82
bastore
dup
bipush 18
ldc_w 83
bastore
dup
bipush 19
ldc_w 84
bastore
dup
bipush 20
ldc_w 85
bastore
dup
bipush 21
ldc_w 86
bastore
dup
bipush 22
ldc_w 87
bastore
dup
bipush 23
ldc_w 88
bastore
dup
bipush 24
ldc_w 89
bastore
dup
bipush 25
ldc_w 90
bastore
dup
bipush 26
ldc_w 97
bastore
dup
bipush 27
ldc_w 98
bastore
dup
bipush 28
ldc_w 99
bastore
dup
bipush 29
ldc_w 100
bastore
dup
bipush 30
ldc_w 101
bastore
dup
bipush 31
ldc_w 102
bastore
dup
bipush 32
ldc_w 103
bastore
dup
bipush 33
ldc_w 104
bastore
dup
bipush 34
ldc_w 105
bastore
dup
bipush 35
ldc_w 106
bastore
dup
bipush 36
ldc_w 107
bastore
dup
bipush 37
ldc_w 108
bastore
dup
bipush 38
ldc_w 109
bastore
dup
bipush 39
ldc_w 110
bastore
dup
bipush 40
ldc_w 111
bastore
dup
bipush 41
ldc_w 112
bastore
dup
bipush 42
ldc_w 113
bastore
dup
bipush 43
ldc_w 114
bastore
dup
bipush 44
ldc_w 115
bastore
dup
bipush 45
ldc_w 116
bastore
dup
bipush 46
ldc_w 117
bastore
dup
bipush 47
ldc_w 118
bastore
dup
bipush 48
ldc_w 119
bastore
dup
bipush 49
ldc_w 120
bastore
dup
bipush 50
ldc_w 121
bastore
dup
bipush 51
ldc_w 122
bastore
dup
bipush 52
ldc_w 48
bastore
dup
bipush 53
ldc_w 49
bastore
dup
bipush 54
ldc_w 50
bastore
dup
bipush 55
ldc_w 51
bastore
dup
bipush 56
ldc_w 52
bastore
dup
bipush 57
ldc_w 53
bastore
dup
bipush 58
ldc_w 54
bastore
dup
bipush 59
ldc_w 55
bastore
dup
bipush 60
ldc_w 56
bastore
dup
bipush 61
ldc_w 57
bastore
dup
bipush 62
ldc_w 43
bastore
dup
bipush 63
ldc_w 47
bastore
putstatic com/google/android/vending/licensing/util/Base64/ALPHABET [B
bipush 64
newarray byte
dup
iconst_0
ldc_w 65
bastore
dup
iconst_1
ldc_w 66
bastore
dup
iconst_2
ldc_w 67
bastore
dup
iconst_3
ldc_w 68
bastore
dup
iconst_4
ldc_w 69
bastore
dup
iconst_5
ldc_w 70
bastore
dup
bipush 6
ldc_w 71
bastore
dup
bipush 7
ldc_w 72
bastore
dup
bipush 8
ldc_w 73
bastore
dup
bipush 9
ldc_w 74
bastore
dup
bipush 10
ldc_w 75
bastore
dup
bipush 11
ldc_w 76
bastore
dup
bipush 12
ldc_w 77
bastore
dup
bipush 13
ldc_w 78
bastore
dup
bipush 14
ldc_w 79
bastore
dup
bipush 15
ldc_w 80
bastore
dup
bipush 16
ldc_w 81
bastore
dup
bipush 17
ldc_w 82
bastore
dup
bipush 18
ldc_w 83
bastore
dup
bipush 19
ldc_w 84
bastore
dup
bipush 20
ldc_w 85
bastore
dup
bipush 21
ldc_w 86
bastore
dup
bipush 22
ldc_w 87
bastore
dup
bipush 23
ldc_w 88
bastore
dup
bipush 24
ldc_w 89
bastore
dup
bipush 25
ldc_w 90
bastore
dup
bipush 26
ldc_w 97
bastore
dup
bipush 27
ldc_w 98
bastore
dup
bipush 28
ldc_w 99
bastore
dup
bipush 29
ldc_w 100
bastore
dup
bipush 30
ldc_w 101
bastore
dup
bipush 31
ldc_w 102
bastore
dup
bipush 32
ldc_w 103
bastore
dup
bipush 33
ldc_w 104
bastore
dup
bipush 34
ldc_w 105
bastore
dup
bipush 35
ldc_w 106
bastore
dup
bipush 36
ldc_w 107
bastore
dup
bipush 37
ldc_w 108
bastore
dup
bipush 38
ldc_w 109
bastore
dup
bipush 39
ldc_w 110
bastore
dup
bipush 40
ldc_w 111
bastore
dup
bipush 41
ldc_w 112
bastore
dup
bipush 42
ldc_w 113
bastore
dup
bipush 43
ldc_w 114
bastore
dup
bipush 44
ldc_w 115
bastore
dup
bipush 45
ldc_w 116
bastore
dup
bipush 46
ldc_w 117
bastore
dup
bipush 47
ldc_w 118
bastore
dup
bipush 48
ldc_w 119
bastore
dup
bipush 49
ldc_w 120
bastore
dup
bipush 50
ldc_w 121
bastore
dup
bipush 51
ldc_w 122
bastore
dup
bipush 52
ldc_w 48
bastore
dup
bipush 53
ldc_w 49
bastore
dup
bipush 54
ldc_w 50
bastore
dup
bipush 55
ldc_w 51
bastore
dup
bipush 56
ldc_w 52
bastore
dup
bipush 57
ldc_w 53
bastore
dup
bipush 58
ldc_w 54
bastore
dup
bipush 59
ldc_w 55
bastore
dup
bipush 60
ldc_w 56
bastore
dup
bipush 61
ldc_w 57
bastore
dup
bipush 62
ldc_w 45
bastore
dup
bipush 63
ldc_w 95
bastore
putstatic com/google/android/vending/licensing/util/Base64/WEBSAFE_ALPHABET [B
sipush 128
newarray byte
dup
iconst_0
ldc_w -9
bastore
dup
iconst_1
ldc_w -9
bastore
dup
iconst_2
ldc_w -9
bastore
dup
iconst_3
ldc_w -9
bastore
dup
iconst_4
ldc_w -9
bastore
dup
iconst_5
ldc_w -9
bastore
dup
bipush 6
ldc_w -9
bastore
dup
bipush 7
ldc_w -9
bastore
dup
bipush 8
ldc_w -9
bastore
dup
bipush 9
ldc_w -5
bastore
dup
bipush 10
ldc_w -5
bastore
dup
bipush 11
ldc_w -9
bastore
dup
bipush 12
ldc_w -9
bastore
dup
bipush 13
ldc_w -5
bastore
dup
bipush 14
ldc_w -9
bastore
dup
bipush 15
ldc_w -9
bastore
dup
bipush 16
ldc_w -9
bastore
dup
bipush 17
ldc_w -9
bastore
dup
bipush 18
ldc_w -9
bastore
dup
bipush 19
ldc_w -9
bastore
dup
bipush 20
ldc_w -9
bastore
dup
bipush 21
ldc_w -9
bastore
dup
bipush 22
ldc_w -9
bastore
dup
bipush 23
ldc_w -9
bastore
dup
bipush 24
ldc_w -9
bastore
dup
bipush 25
ldc_w -9
bastore
dup
bipush 26
ldc_w -9
bastore
dup
bipush 27
ldc_w -9
bastore
dup
bipush 28
ldc_w -9
bastore
dup
bipush 29
ldc_w -9
bastore
dup
bipush 30
ldc_w -9
bastore
dup
bipush 31
ldc_w -9
bastore
dup
bipush 32
ldc_w -5
bastore
dup
bipush 33
ldc_w -9
bastore
dup
bipush 34
ldc_w -9
bastore
dup
bipush 35
ldc_w -9
bastore
dup
bipush 36
ldc_w -9
bastore
dup
bipush 37
ldc_w -9
bastore
dup
bipush 38
ldc_w -9
bastore
dup
bipush 39
ldc_w -9
bastore
dup
bipush 40
ldc_w -9
bastore
dup
bipush 41
ldc_w -9
bastore
dup
bipush 42
ldc_w -9
bastore
dup
bipush 43
ldc_w 62
bastore
dup
bipush 44
ldc_w -9
bastore
dup
bipush 45
ldc_w -9
bastore
dup
bipush 46
ldc_w -9
bastore
dup
bipush 47
ldc_w 63
bastore
dup
bipush 48
ldc_w 52
bastore
dup
bipush 49
ldc_w 53
bastore
dup
bipush 50
ldc_w 54
bastore
dup
bipush 51
ldc_w 55
bastore
dup
bipush 52
ldc_w 56
bastore
dup
bipush 53
ldc_w 57
bastore
dup
bipush 54
ldc_w 58
bastore
dup
bipush 55
ldc_w 59
bastore
dup
bipush 56
ldc_w 60
bastore
dup
bipush 57
ldc_w 61
bastore
dup
bipush 58
ldc_w -9
bastore
dup
bipush 59
ldc_w -9
bastore
dup
bipush 60
ldc_w -9
bastore
dup
bipush 61
ldc_w -1
bastore
dup
bipush 62
ldc_w -9
bastore
dup
bipush 63
ldc_w -9
bastore
dup
bipush 64
ldc_w -9
bastore
dup
bipush 65
ldc_w 0
bastore
dup
bipush 66
ldc_w 1
bastore
dup
bipush 67
ldc_w 2
bastore
dup
bipush 68
ldc_w 3
bastore
dup
bipush 69
ldc_w 4
bastore
dup
bipush 70
ldc_w 5
bastore
dup
bipush 71
ldc_w 6
bastore
dup
bipush 72
ldc_w 7
bastore
dup
bipush 73
ldc_w 8
bastore
dup
bipush 74
ldc_w 9
bastore
dup
bipush 75
ldc_w 10
bastore
dup
bipush 76
ldc_w 11
bastore
dup
bipush 77
ldc_w 12
bastore
dup
bipush 78
ldc_w 13
bastore
dup
bipush 79
ldc_w 14
bastore
dup
bipush 80
ldc_w 15
bastore
dup
bipush 81
ldc_w 16
bastore
dup
bipush 82
ldc_w 17
bastore
dup
bipush 83
ldc_w 18
bastore
dup
bipush 84
ldc_w 19
bastore
dup
bipush 85
ldc_w 20
bastore
dup
bipush 86
ldc_w 21
bastore
dup
bipush 87
ldc_w 22
bastore
dup
bipush 88
ldc_w 23
bastore
dup
bipush 89
ldc_w 24
bastore
dup
bipush 90
ldc_w 25
bastore
dup
bipush 91
ldc_w -9
bastore
dup
bipush 92
ldc_w -9
bastore
dup
bipush 93
ldc_w -9
bastore
dup
bipush 94
ldc_w -9
bastore
dup
bipush 95
ldc_w -9
bastore
dup
bipush 96
ldc_w -9
bastore
dup
bipush 97
ldc_w 26
bastore
dup
bipush 98
ldc_w 27
bastore
dup
bipush 99
ldc_w 28
bastore
dup
bipush 100
ldc_w 29
bastore
dup
bipush 101
ldc_w 30
bastore
dup
bipush 102
ldc_w 31
bastore
dup
bipush 103
ldc_w 32
bastore
dup
bipush 104
ldc_w 33
bastore
dup
bipush 105
ldc_w 34
bastore
dup
bipush 106
ldc_w 35
bastore
dup
bipush 107
ldc_w 36
bastore
dup
bipush 108
ldc_w 37
bastore
dup
bipush 109
ldc_w 38
bastore
dup
bipush 110
ldc_w 39
bastore
dup
bipush 111
ldc_w 40
bastore
dup
bipush 112
ldc_w 41
bastore
dup
bipush 113
ldc_w 42
bastore
dup
bipush 114
ldc_w 43
bastore
dup
bipush 115
ldc_w 44
bastore
dup
bipush 116
ldc_w 45
bastore
dup
bipush 117
ldc_w 46
bastore
dup
bipush 118
ldc_w 47
bastore
dup
bipush 119
ldc_w 48
bastore
dup
bipush 120
ldc_w 49
bastore
dup
bipush 121
ldc_w 50
bastore
dup
bipush 122
ldc_w 51
bastore
dup
bipush 123
ldc_w -9
bastore
dup
bipush 124
ldc_w -9
bastore
dup
bipush 125
ldc_w -9
bastore
dup
bipush 126
ldc_w -9
bastore
dup
bipush 127
ldc_w -9
bastore
putstatic com/google/android/vending/licensing/util/Base64/DECODABET [B
sipush 128
newarray byte
dup
iconst_0
ldc_w -9
bastore
dup
iconst_1
ldc_w -9
bastore
dup
iconst_2
ldc_w -9
bastore
dup
iconst_3
ldc_w -9
bastore
dup
iconst_4
ldc_w -9
bastore
dup
iconst_5
ldc_w -9
bastore
dup
bipush 6
ldc_w -9
bastore
dup
bipush 7
ldc_w -9
bastore
dup
bipush 8
ldc_w -9
bastore
dup
bipush 9
ldc_w -5
bastore
dup
bipush 10
ldc_w -5
bastore
dup
bipush 11
ldc_w -9
bastore
dup
bipush 12
ldc_w -9
bastore
dup
bipush 13
ldc_w -5
bastore
dup
bipush 14
ldc_w -9
bastore
dup
bipush 15
ldc_w -9
bastore
dup
bipush 16
ldc_w -9
bastore
dup
bipush 17
ldc_w -9
bastore
dup
bipush 18
ldc_w -9
bastore
dup
bipush 19
ldc_w -9
bastore
dup
bipush 20
ldc_w -9
bastore
dup
bipush 21
ldc_w -9
bastore
dup
bipush 22
ldc_w -9
bastore
dup
bipush 23
ldc_w -9
bastore
dup
bipush 24
ldc_w -9
bastore
dup
bipush 25
ldc_w -9
bastore
dup
bipush 26
ldc_w -9
bastore
dup
bipush 27
ldc_w -9
bastore
dup
bipush 28
ldc_w -9
bastore
dup
bipush 29
ldc_w -9
bastore
dup
bipush 30
ldc_w -9
bastore
dup
bipush 31
ldc_w -9
bastore
dup
bipush 32
ldc_w -5
bastore
dup
bipush 33
ldc_w -9
bastore
dup
bipush 34
ldc_w -9
bastore
dup
bipush 35
ldc_w -9
bastore
dup
bipush 36
ldc_w -9
bastore
dup
bipush 37
ldc_w -9
bastore
dup
bipush 38
ldc_w -9
bastore
dup
bipush 39
ldc_w -9
bastore
dup
bipush 40
ldc_w -9
bastore
dup
bipush 41
ldc_w -9
bastore
dup
bipush 42
ldc_w -9
bastore
dup
bipush 43
ldc_w -9
bastore
dup
bipush 44
ldc_w -9
bastore
dup
bipush 45
ldc_w 62
bastore
dup
bipush 46
ldc_w -9
bastore
dup
bipush 47
ldc_w -9
bastore
dup
bipush 48
ldc_w 52
bastore
dup
bipush 49
ldc_w 53
bastore
dup
bipush 50
ldc_w 54
bastore
dup
bipush 51
ldc_w 55
bastore
dup
bipush 52
ldc_w 56
bastore
dup
bipush 53
ldc_w 57
bastore
dup
bipush 54
ldc_w 58
bastore
dup
bipush 55
ldc_w 59
bastore
dup
bipush 56
ldc_w 60
bastore
dup
bipush 57
ldc_w 61
bastore
dup
bipush 58
ldc_w -9
bastore
dup
bipush 59
ldc_w -9
bastore
dup
bipush 60
ldc_w -9
bastore
dup
bipush 61
ldc_w -1
bastore
dup
bipush 62
ldc_w -9
bastore
dup
bipush 63
ldc_w -9
bastore
dup
bipush 64
ldc_w -9
bastore
dup
bipush 65
ldc_w 0
bastore
dup
bipush 66
ldc_w 1
bastore
dup
bipush 67
ldc_w 2
bastore
dup
bipush 68
ldc_w 3
bastore
dup
bipush 69
ldc_w 4
bastore
dup
bipush 70
ldc_w 5
bastore
dup
bipush 71
ldc_w 6
bastore
dup
bipush 72
ldc_w 7
bastore
dup
bipush 73
ldc_w 8
bastore
dup
bipush 74
ldc_w 9
bastore
dup
bipush 75
ldc_w 10
bastore
dup
bipush 76
ldc_w 11
bastore
dup
bipush 77
ldc_w 12
bastore
dup
bipush 78
ldc_w 13
bastore
dup
bipush 79
ldc_w 14
bastore
dup
bipush 80
ldc_w 15
bastore
dup
bipush 81
ldc_w 16
bastore
dup
bipush 82
ldc_w 17
bastore
dup
bipush 83
ldc_w 18
bastore
dup
bipush 84
ldc_w 19
bastore
dup
bipush 85
ldc_w 20
bastore
dup
bipush 86
ldc_w 21
bastore
dup
bipush 87
ldc_w 22
bastore
dup
bipush 88
ldc_w 23
bastore
dup
bipush 89
ldc_w 24
bastore
dup
bipush 90
ldc_w 25
bastore
dup
bipush 91
ldc_w -9
bastore
dup
bipush 92
ldc_w -9
bastore
dup
bipush 93
ldc_w -9
bastore
dup
bipush 94
ldc_w -9
bastore
dup
bipush 95
ldc_w 63
bastore
dup
bipush 96
ldc_w -9
bastore
dup
bipush 97
ldc_w 26
bastore
dup
bipush 98
ldc_w 27
bastore
dup
bipush 99
ldc_w 28
bastore
dup
bipush 100
ldc_w 29
bastore
dup
bipush 101
ldc_w 30
bastore
dup
bipush 102
ldc_w 31
bastore
dup
bipush 103
ldc_w 32
bastore
dup
bipush 104
ldc_w 33
bastore
dup
bipush 105
ldc_w 34
bastore
dup
bipush 106
ldc_w 35
bastore
dup
bipush 107
ldc_w 36
bastore
dup
bipush 108
ldc_w 37
bastore
dup
bipush 109
ldc_w 38
bastore
dup
bipush 110
ldc_w 39
bastore
dup
bipush 111
ldc_w 40
bastore
dup
bipush 112
ldc_w 41
bastore
dup
bipush 113
ldc_w 42
bastore
dup
bipush 114
ldc_w 43
bastore
dup
bipush 115
ldc_w 44
bastore
dup
bipush 116
ldc_w 45
bastore
dup
bipush 117
ldc_w 46
bastore
dup
bipush 118
ldc_w 47
bastore
dup
bipush 119
ldc_w 48
bastore
dup
bipush 120
ldc_w 49
bastore
dup
bipush 121
ldc_w 50
bastore
dup
bipush 122
ldc_w 51
bastore
dup
bipush 123
ldc_w -9
bastore
dup
bipush 124
ldc_w -9
bastore
dup
bipush 125
ldc_w -9
bastore
dup
bipush 126
ldc_w -9
bastore
dup
bipush 127
ldc_w -9
bastore
putstatic com/google/android/vending/licensing/util/Base64/WEBSAFE_DECODABET [B
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static decode(Ljava/lang/String;)[B
.throws com/google/android/vending/licensing/util/Base64DecoderException
aload 0
invokevirtual java/lang/String/getBytes()[B
astore 0
aload 0
iconst_0
aload 0
arraylength
invokestatic com/google/android/vending/licensing/util/Base64/decode([BII)[B
areturn
.limit locals 1
.limit stack 3
.end method

.method public static decode([B)[B
.throws com/google/android/vending/licensing/util/Base64DecoderException
aload 0
iconst_0
aload 0
arraylength
invokestatic com/google/android/vending/licensing/util/Base64/decode([BII)[B
areturn
.limit locals 1
.limit stack 3
.end method

.method public static decode([BII)[B
.throws com/google/android/vending/licensing/util/Base64DecoderException
aload 0
iload 1
iload 2
getstatic com/google/android/vending/licensing/util/Base64/DECODABET [B
invokestatic com/google/android/vending/licensing/util/Base64/decode([BII[B)[B
areturn
.limit locals 3
.limit stack 4
.end method

.method public static decode([BII[B)[B
.throws com/google/android/vending/licensing/util/Base64DecoderException
iload 2
iconst_3
imul
iconst_4
idiv
iconst_2
iadd
newarray byte
astore 10
iconst_0
istore 6
iconst_4
newarray byte
astore 11
iconst_0
istore 7
iconst_0
istore 5
L0:
iload 7
iload 2
if_icmpge L1
aload 0
iload 7
iload 1
iadd
baload
bipush 127
iand
i2b
istore 4
aload 3
iload 4
baload
istore 8
iload 8
bipush -5
if_icmplt L2
iload 8
iconst_m1
if_icmplt L3
iload 4
bipush 61
if_icmpne L4
iload 2
iload 7
isub
istore 8
aload 0
iload 2
iconst_1
isub
iload 1
iadd
baload
bipush 127
iand
i2b
istore 1
iload 5
ifeq L5
iload 5
iconst_1
if_icmpne L6
L5:
new com/google/android/vending/licensing/util/Base64DecoderException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "invalid padding byte '=' at byte offset "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 7
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/google/android/vending/licensing/util/Base64DecoderException/<init>(Ljava/lang/String;)V
athrow
L6:
iload 5
iconst_3
if_icmpne L7
iload 8
iconst_2
if_icmpgt L8
L7:
iload 5
iconst_4
if_icmpne L9
iload 8
iconst_1
if_icmple L9
L8:
new com/google/android/vending/licensing/util/Base64DecoderException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "padding byte '=' falsely signals end of encoded value at offset "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 7
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/google/android/vending/licensing/util/Base64DecoderException/<init>(Ljava/lang/String;)V
athrow
L9:
iload 1
bipush 61
if_icmpeq L1
iload 1
bipush 10
if_icmpeq L1
new com/google/android/vending/licensing/util/Base64DecoderException
dup
ldc "encoded value has invalid trailing byte"
invokespecial com/google/android/vending/licensing/util/Base64DecoderException/<init>(Ljava/lang/String;)V
athrow
L4:
iload 5
iconst_1
iadd
istore 9
aload 11
iload 5
iload 4
bastore
iload 9
istore 5
iload 6
istore 8
iload 9
iconst_4
if_icmpne L10
iload 6
aload 11
iconst_0
aload 10
iload 6
aload 3
invokestatic com/google/android/vending/licensing/util/Base64/decode4to3([BI[BI[B)I
iadd
istore 8
iconst_0
istore 5
L10:
iload 7
iconst_1
iadd
istore 7
iload 8
istore 6
goto L0
L2:
new com/google/android/vending/licensing/util/Base64DecoderException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Bad Base64 input character at "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 7
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
iload 7
iload 1
iadd
baload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "(decimal)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/google/android/vending/licensing/util/Base64DecoderException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 5
ifeq L11
iload 5
iconst_1
if_icmpne L12
new com/google/android/vending/licensing/util/Base64DecoderException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "single trailing character at offset "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
iconst_1
isub
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/google/android/vending/licensing/util/Base64DecoderException/<init>(Ljava/lang/String;)V
athrow
L12:
aload 11
iload 5
bipush 61
bastore
iload 6
aload 11
iconst_0
aload 10
iload 6
aload 3
invokestatic com/google/android/vending/licensing/util/Base64/decode4to3([BI[BI[B)I
iadd
istore 6
L13:
iload 6
newarray byte
astore 0
aload 10
iconst_0
aload 0
iconst_0
iload 6
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
areturn
L11:
goto L13
L3:
iload 6
istore 8
goto L10
.limit locals 12
.limit stack 6
.end method

.method private static decode4to3([BI[BI[B)I
aload 0
iload 1
iconst_2
iadd
baload
bipush 61
if_icmpne L0
aload 2
iload 3
aload 4
aload 0
iload 1
baload
baload
bipush 24
ishl
bipush 6
iushr
aload 4
aload 0
iload 1
iconst_1
iadd
baload
baload
bipush 24
ishl
bipush 12
iushr
ior
bipush 16
iushr
i2b
bastore
iconst_1
ireturn
L0:
aload 0
iload 1
iconst_3
iadd
baload
bipush 61
if_icmpne L1
aload 4
aload 0
iload 1
baload
baload
bipush 24
ishl
bipush 6
iushr
aload 4
aload 0
iload 1
iconst_1
iadd
baload
baload
bipush 24
ishl
bipush 12
iushr
ior
aload 4
aload 0
iload 1
iconst_2
iadd
baload
baload
bipush 24
ishl
bipush 18
iushr
ior
istore 1
aload 2
iload 3
iload 1
bipush 16
iushr
i2b
bastore
aload 2
iload 3
iconst_1
iadd
iload 1
bipush 8
iushr
i2b
bastore
iconst_2
ireturn
L1:
aload 4
aload 0
iload 1
baload
baload
bipush 24
ishl
bipush 6
iushr
aload 4
aload 0
iload 1
iconst_1
iadd
baload
baload
bipush 24
ishl
bipush 12
iushr
ior
aload 4
aload 0
iload 1
iconst_2
iadd
baload
baload
bipush 24
ishl
bipush 18
iushr
ior
aload 4
aload 0
iload 1
iconst_3
iadd
baload
baload
bipush 24
ishl
bipush 24
iushr
ior
istore 1
aload 2
iload 3
iload 1
bipush 16
ishr
i2b
bastore
aload 2
iload 3
iconst_1
iadd
iload 1
bipush 8
ishr
i2b
bastore
aload 2
iload 3
iconst_2
iadd
iload 1
i2b
bastore
iconst_3
ireturn
.limit locals 5
.limit stack 7
.end method

.method public static decodeWebSafe(Ljava/lang/String;)[B
.throws com/google/android/vending/licensing/util/Base64DecoderException
aload 0
invokevirtual java/lang/String/getBytes()[B
astore 0
aload 0
iconst_0
aload 0
arraylength
invokestatic com/google/android/vending/licensing/util/Base64/decodeWebSafe([BII)[B
areturn
.limit locals 1
.limit stack 3
.end method

.method public static decodeWebSafe([B)[B
.throws com/google/android/vending/licensing/util/Base64DecoderException
aload 0
iconst_0
aload 0
arraylength
invokestatic com/google/android/vending/licensing/util/Base64/decodeWebSafe([BII)[B
areturn
.limit locals 1
.limit stack 3
.end method

.method public static decodeWebSafe([BII)[B
.throws com/google/android/vending/licensing/util/Base64DecoderException
aload 0
iload 1
iload 2
getstatic com/google/android/vending/licensing/util/Base64/WEBSAFE_DECODABET [B
invokestatic com/google/android/vending/licensing/util/Base64/decode([BII[B)[B
areturn
.limit locals 3
.limit stack 4
.end method

.method public static encode([B)Ljava/lang/String;
aload 0
iconst_0
aload 0
arraylength
getstatic com/google/android/vending/licensing/util/Base64/ALPHABET [B
iconst_1
invokestatic com/google/android/vending/licensing/util/Base64/encode([BII[BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 5
.end method

.method public static encode([BII[BZ)Ljava/lang/String;
aload 0
iload 1
iload 2
aload 3
ldc_w 2147483647
invokestatic com/google/android/vending/licensing/util/Base64/encode([BII[BI)[B
astore 0
aload 0
arraylength
istore 1
L0:
iload 4
ifne L1
iload 1
ifle L1
aload 0
iload 1
iconst_1
isub
baload
bipush 61
if_icmpeq L2
L1:
new java/lang/String
dup
aload 0
iconst_0
iload 1
invokespecial java/lang/String/<init>([BII)V
areturn
L2:
iload 1
iconst_1
isub
istore 1
goto L0
.limit locals 5
.limit stack 5
.end method

.method public static encode([BII[BI)[B
iload 2
iconst_2
iadd
iconst_3
idiv
iconst_4
imul
istore 5
iload 5
iload 4
idiv
iload 5
iadd
newarray byte
astore 10
iconst_0
istore 7
iconst_0
istore 5
iconst_0
istore 6
L0:
iload 7
iload 2
iconst_2
isub
if_icmpge L1
aload 0
iload 7
iload 1
iadd
baload
bipush 24
ishl
bipush 8
iushr
aload 0
iload 7
iconst_1
iadd
iload 1
iadd
baload
bipush 24
ishl
bipush 16
iushr
ior
aload 0
iload 7
iconst_2
iadd
iload 1
iadd
baload
bipush 24
ishl
bipush 24
iushr
ior
istore 8
aload 10
iload 5
aload 3
iload 8
bipush 18
iushr
baload
bastore
aload 10
iload 5
iconst_1
iadd
aload 3
iload 8
bipush 12
iushr
bipush 63
iand
baload
bastore
aload 10
iload 5
iconst_2
iadd
aload 3
iload 8
bipush 6
iushr
bipush 63
iand
baload
bastore
aload 10
iload 5
iconst_3
iadd
aload 3
iload 8
bipush 63
iand
baload
bastore
iload 6
iconst_4
iadd
istore 9
iload 5
istore 8
iload 9
istore 6
iload 9
iload 4
if_icmpne L2
aload 10
iload 5
iconst_4
iadd
bipush 10
bastore
iload 5
iconst_1
iadd
istore 8
iconst_0
istore 6
L2:
iload 7
iconst_3
iadd
istore 7
iload 8
iconst_4
iadd
istore 5
goto L0
L1:
iload 5
istore 8
iload 7
iload 2
if_icmpge L3
aload 0
iload 7
iload 1
iadd
iload 2
iload 7
isub
aload 10
iload 5
aload 3
invokestatic com/google/android/vending/licensing/util/Base64/encode3to4([BII[BI[B)[B
pop
iload 5
istore 1
iload 6
iconst_4
iadd
iload 4
if_icmpne L4
aload 10
iload 5
iconst_4
iadd
bipush 10
bastore
iload 5
iconst_1
iadd
istore 1
L4:
iload 1
iconst_4
iadd
istore 8
L3:
getstatic com/google/android/vending/licensing/util/Base64/$assertionsDisabled Z
ifne L5
iload 8
aload 10
arraylength
if_icmpeq L5
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L5:
aload 10
areturn
.limit locals 11
.limit stack 6
.end method

.method private static encode3to4([BII[BI[B)[B
iconst_0
istore 8
iload 2
ifle L0
aload 0
iload 1
baload
bipush 24
ishl
bipush 8
iushr
istore 6
L1:
iload 2
iconst_1
if_icmple L2
aload 0
iload 1
iconst_1
iadd
baload
bipush 24
ishl
bipush 16
iushr
istore 7
L3:
iload 2
iconst_2
if_icmple L4
aload 0
iload 1
iconst_2
iadd
baload
bipush 24
ishl
bipush 24
iushr
istore 8
L4:
iload 7
iload 6
ior
iload 8
ior
istore 1
iload 2
tableswitch 1
L5
L6
L7
default : L8
L8:
aload 3
areturn
L0:
iconst_0
istore 6
goto L1
L2:
iconst_0
istore 7
goto L3
L7:
aload 3
iload 4
aload 5
iload 1
bipush 18
iushr
baload
bastore
aload 3
iload 4
iconst_1
iadd
aload 5
iload 1
bipush 12
iushr
bipush 63
iand
baload
bastore
aload 3
iload 4
iconst_2
iadd
aload 5
iload 1
bipush 6
iushr
bipush 63
iand
baload
bastore
aload 3
iload 4
iconst_3
iadd
aload 5
iload 1
bipush 63
iand
baload
bastore
aload 3
areturn
L6:
aload 3
iload 4
aload 5
iload 1
bipush 18
iushr
baload
bastore
aload 3
iload 4
iconst_1
iadd
aload 5
iload 1
bipush 12
iushr
bipush 63
iand
baload
bastore
aload 3
iload 4
iconst_2
iadd
aload 5
iload 1
bipush 6
iushr
bipush 63
iand
baload
bastore
aload 3
iload 4
iconst_3
iadd
bipush 61
bastore
aload 3
areturn
L5:
aload 3
iload 4
aload 5
iload 1
bipush 18
iushr
baload
bastore
aload 3
iload 4
iconst_1
iadd
aload 5
iload 1
bipush 12
iushr
bipush 63
iand
baload
bastore
aload 3
iload 4
iconst_2
iadd
bipush 61
bastore
aload 3
iload 4
iconst_3
iadd
bipush 61
bastore
aload 3
areturn
.limit locals 9
.limit stack 5
.end method

.method public static encodeWebSafe([BZ)Ljava/lang/String;
aload 0
iconst_0
aload 0
arraylength
getstatic com/google/android/vending/licensing/util/Base64/WEBSAFE_ALPHABET [B
iload 1
invokestatic com/google/android/vending/licensing/util/Base64/encode([BII[BZ)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 5
.end method
