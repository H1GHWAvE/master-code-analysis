.bytecode 50.0
.class public synchronized com/google/android/vending/licensing/PreferenceObfuscator
.super java/lang/Object

.field private static final 'TAG' Ljava/lang/String; = "PreferenceObfuscator"

.field private 'mEditor' Landroid/content/SharedPreferences$Editor;

.field private final 'mObfuscator' Lcom/google/android/vending/licensing/Obfuscator;

.field private final 'mPreferences' Landroid/content/SharedPreferences;

.method public <init>(Landroid/content/SharedPreferences;Lcom/google/android/vending/licensing/Obfuscator;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/google/android/vending/licensing/PreferenceObfuscator/mPreferences Landroid/content/SharedPreferences;
aload 0
aload 2
putfield com/google/android/vending/licensing/PreferenceObfuscator/mObfuscator Lcom/google/android/vending/licensing/Obfuscator;
aload 0
aconst_null
putfield com/google/android/vending/licensing/PreferenceObfuscator/mEditor Landroid/content/SharedPreferences$Editor;
return
.limit locals 3
.limit stack 2
.end method

.method public commit()V
aload 0
getfield com/google/android/vending/licensing/PreferenceObfuscator/mEditor Landroid/content/SharedPreferences$Editor;
ifnull L0
aload 0
getfield com/google/android/vending/licensing/PreferenceObfuscator/mEditor Landroid/content/SharedPreferences$Editor;
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
aconst_null
putfield com/google/android/vending/licensing/PreferenceObfuscator/mEditor Landroid/content/SharedPreferences$Editor;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.catch com/google/android/vending/licensing/ValidationException from L0 to L1 using L2
aload 0
getfield com/google/android/vending/licensing/PreferenceObfuscator/mPreferences Landroid/content/SharedPreferences;
aload 1
aconst_null
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 3
aload 3
ifnull L3
L0:
aload 0
getfield com/google/android/vending/licensing/PreferenceObfuscator/mObfuscator Lcom/google/android/vending/licensing/Obfuscator;
aload 3
aload 1
invokeinterface com/google/android/vending/licensing/Obfuscator/unobfuscate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 3
L1:
aload 3
areturn
L2:
astore 3
ldc "PreferenceObfuscator"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Validation error while reading preference: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 2
areturn
L3:
aload 2
areturn
.limit locals 4
.limit stack 3
.end method

.method public putString(Ljava/lang/String;Ljava/lang/String;)V
aload 0
getfield com/google/android/vending/licensing/PreferenceObfuscator/mEditor Landroid/content/SharedPreferences$Editor;
ifnonnull L0
aload 0
aload 0
getfield com/google/android/vending/licensing/PreferenceObfuscator/mPreferences Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
putfield com/google/android/vending/licensing/PreferenceObfuscator/mEditor Landroid/content/SharedPreferences$Editor;
L0:
aload 0
getfield com/google/android/vending/licensing/PreferenceObfuscator/mObfuscator Lcom/google/android/vending/licensing/Obfuscator;
aload 2
aload 1
invokeinterface com/google/android/vending/licensing/Obfuscator/obfuscate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 2
aload 0
getfield com/google/android/vending/licensing/PreferenceObfuscator/mEditor Landroid/content/SharedPreferences$Editor;
aload 1
aload 2
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
pop
return
.limit locals 3
.limit stack 3
.end method
