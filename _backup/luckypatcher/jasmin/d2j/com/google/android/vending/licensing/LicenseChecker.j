.bytecode 50.0
.class public synchronized com/google/android/vending/licensing/LicenseChecker
.super java/lang/Object
.implements android/content/ServiceConnection
.inner class public ResultListener inner com/google/android/vending/licensing/LicenseChecker$ResultListener outer com/google/android/vending/licensing/LicenseChecker
.inner class inner com/google/android/vending/licensing/LicenseChecker$ResultListener$1
.inner class inner com/google/android/vending/licensing/LicenseChecker$ResultListener$2

.field private static final 'DEBUG_LICENSE_ERROR' Z = 0


.field private static final 'KEY_FACTORY_ALGORITHM' Ljava/lang/String; = "RSA"

.field private static final 'RANDOM' Ljava/security/SecureRandom;

.field private static final 'TAG' Ljava/lang/String; = "LicenseChecker"

.field private static final 'TIMEOUT_MS' I = 10000


.field private final 'mChecksInProgress' Ljava/util/Set; signature "Ljava/util/Set<Lcom/google/android/vending/licensing/LicenseValidator;>;"

.field private final 'mContext' Landroid/content/Context;

.field private 'mHandler' Landroid/os/Handler;

.field private final 'mPackageName' Ljava/lang/String;

.field private final 'mPendingChecks' Ljava/util/Queue; signature "Ljava/util/Queue<Lcom/google/android/vending/licensing/LicenseValidator;>;"

.field private final 'mPolicy' Lcom/google/android/vending/licensing/Policy;

.field private 'mPublicKey' Ljava/security/PublicKey;

.field private 'mService' Lcom/google/android/vending/licensing/ILicensingService;

.field private final 'mVersionCode' Ljava/lang/String;

.field public 'responseCode' I

.field public 'signature' Ljava/lang/String;

.field public 'signedData' Ljava/lang/String;

.method static <clinit>()V
new java/security/SecureRandom
dup
invokespecial java/security/SecureRandom/<init>()V
putstatic com/google/android/vending/licensing/LicenseChecker/RANDOM Ljava/security/SecureRandom;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Landroid/content/Context;Lcom/google/android/vending/licensing/Policy;Ljava/lang/String;Ljava/lang/String;)V
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
putfield com/google/android/vending/licensing/LicenseChecker/mChecksInProgress Ljava/util/Set;
aload 0
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
putfield com/google/android/vending/licensing/LicenseChecker/mPendingChecks Ljava/util/Queue;
aload 0
aload 1
putfield com/google/android/vending/licensing/LicenseChecker/mContext Landroid/content/Context;
aload 0
aload 2
putfield com/google/android/vending/licensing/LicenseChecker/mPolicy Lcom/google/android/vending/licensing/Policy;
aload 0
aload 3
invokestatic com/google/android/vending/licensing/LicenseChecker/generatePublicKey(Ljava/lang/String;)Ljava/security/PublicKey;
putfield com/google/android/vending/licensing/LicenseChecker/mPublicKey Ljava/security/PublicKey;
aload 0
aload 4
putfield com/google/android/vending/licensing/LicenseChecker/mPackageName Ljava/lang/String;
L0:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 4
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/versionCode I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L1:
aload 0
aload 1
putfield com/google/android/vending/licensing/LicenseChecker/mVersionCode Ljava/lang/String;
new android/os/HandlerThread
dup
ldc "background thread"
invokespecial android/os/HandlerThread/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual android/os/HandlerThread/start()V
aload 0
new android/os/Handler
dup
aload 1
invokevirtual android/os/HandlerThread/getLooper()Landroid/os/Looper;
invokespecial android/os/Handler/<init>(Landroid/os/Looper;)V
putfield com/google/android/vending/licensing/LicenseChecker/mHandler Landroid/os/Handler;
return
L2:
astore 1
aload 1
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
ldc "0"
astore 1
goto L1
.limit locals 5
.limit stack 4
.end method

.method static synthetic access$100(Lcom/google/android/vending/licensing/LicenseChecker;Lcom/google/android/vending/licensing/LicenseValidator;)V
aload 0
aload 1
invokespecial com/google/android/vending/licensing/LicenseChecker/handleServiceConnectionError(Lcom/google/android/vending/licensing/LicenseValidator;)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$200(Lcom/google/android/vending/licensing/LicenseChecker;Lcom/google/android/vending/licensing/LicenseValidator;)V
aload 0
aload 1
invokespecial com/google/android/vending/licensing/LicenseChecker/finishCheck(Lcom/google/android/vending/licensing/LicenseValidator;)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$300(Lcom/google/android/vending/licensing/LicenseChecker;)Ljava/util/Set;
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mChecksInProgress Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$500(Lcom/google/android/vending/licensing/LicenseChecker;)Ljava/security/PublicKey;
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mPublicKey Ljava/security/PublicKey;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$600(Lcom/google/android/vending/licensing/LicenseChecker;)Landroid/os/Handler;
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mHandler Landroid/os/Handler;
areturn
.limit locals 1
.limit stack 1
.end method

.method private cleanupService()V
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mService Lcom/google/android/vending/licensing/ILicensingService;
ifnull L3
L0:
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mContext Landroid/content/Context;
aload 0
invokevirtual android/content/Context/unbindService(Landroid/content/ServiceConnection;)V
L1:
aload 0
aconst_null
putfield com/google/android/vending/licensing/LicenseChecker/mService Lcom/google/android/vending/licensing/ILicensingService;
L3:
return
L2:
astore 1
ldc "LicenseChecker"
ldc "Unable to unbind from licensing service (already unbound)"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L1
.limit locals 2
.limit stack 2
.end method

.method private finishCheck(Lcom/google/android/vending/licensing/LicenseValidator;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mChecksInProgress Ljava/util/Set;
aload 1
invokeinterface java/util/Set/remove(Ljava/lang/Object;)Z 1
pop
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mChecksInProgress Ljava/util/Set;
invokeinterface java/util/Set/isEmpty()Z 0
ifeq L1
aload 0
invokespecial com/google/android/vending/licensing/LicenseChecker/cleanupService()V
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method private generateNonce()I
getstatic com/google/android/vending/licensing/LicenseChecker/RANDOM Ljava/security/SecureRandom;
invokevirtual java/security/SecureRandom/nextInt()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static generatePublicKey(Ljava/lang/String;)Ljava/security/PublicKey;
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch com/google/android/vending/licensing/util/Base64DecoderException from L0 to L1 using L3
.catch java/security/spec/InvalidKeySpecException from L0 to L1 using L4
L0:
aload 0
invokestatic com/google/android/vending/licensing/util/Base64/decode(Ljava/lang/String;)[B
astore 0
ldc "RSA"
invokestatic java/security/KeyFactory/getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;
new java/security/spec/X509EncodedKeySpec
dup
aload 0
invokespecial java/security/spec/X509EncodedKeySpec/<init>([B)V
invokevirtual java/security/KeyFactory/generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/RuntimeException
dup
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 0
ldc "LicenseChecker"
ldc "Could not decode from Base64."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
new java/lang/IllegalArgumentException
dup
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/Throwable;)V
athrow
L4:
astore 0
ldc "LicenseChecker"
ldc "Invalid key specification."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
new java/lang/IllegalArgumentException
dup
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 1
.limit stack 4
.end method

.method private static getVersionCode(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
L0:
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 1
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/versionCode I
istore 2
L1:
iload 2
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
areturn
L2:
astore 0
ldc "LicenseChecker"
ldc "Package not found. could not get version code."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
ldc ""
areturn
.limit locals 3
.limit stack 3
.end method

.method private handleServiceConnectionError(Lcom/google/android/vending/licensing/LicenseValidator;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mPolicy Lcom/google/android/vending/licensing/Policy;
sipush 291
aconst_null
invokeinterface com/google/android/vending/licensing/Policy/processServerResponse(ILcom/google/android/vending/licensing/ResponseData;)V 2
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method private runChecks()V
.catch android/os/RemoteException from L0 to L1 using L2
L3:
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mPendingChecks Ljava/util/Queue;
invokeinterface java/util/Queue/poll()Ljava/lang/Object; 0
checkcast com/google/android/vending/licensing/LicenseValidator
astore 1
aload 1
ifnull L4
L0:
ldc "LicenseChecker"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Calling checkLicense on service for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual com/google/android/vending/licensing/LicenseValidator/getPackageName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mService Lcom/google/android/vending/licensing/ILicensingService;
aload 1
invokevirtual com/google/android/vending/licensing/LicenseValidator/getNonce()I
i2l
aload 1
invokevirtual com/google/android/vending/licensing/LicenseValidator/getPackageName()Ljava/lang/String;
new com/google/android/vending/licensing/LicenseChecker$ResultListener
dup
aload 0
aload 1
invokespecial com/google/android/vending/licensing/LicenseChecker$ResultListener/<init>(Lcom/google/android/vending/licensing/LicenseChecker;Lcom/google/android/vending/licensing/LicenseValidator;)V
invokeinterface com/google/android/vending/licensing/ILicensingService/checkLicense(JLjava/lang/String;Lcom/google/android/vending/licensing/ILicenseResultListener;)V 4
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mChecksInProgress Ljava/util/Set;
aload 1
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
L1:
goto L3
L2:
astore 2
ldc "LicenseChecker"
ldc "RemoteException in checkLicense call."
aload 2
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aload 0
aload 1
invokespecial com/google/android/vending/licensing/LicenseChecker/handleServiceConnectionError(Lcom/google/android/vending/licensing/LicenseValidator;)V
goto L3
L4:
return
.limit locals 3
.limit stack 8
.end method

.method public checkAccess(Lcom/google/android/vending/licensing/LicenseCheckerCallback;)V
.catch all from L0 to L1 using L2
.catch java/lang/SecurityException from L1 to L3 using L4
.catch com/google/android/vending/licensing/util/Base64DecoderException from L1 to L3 using L5
.catch all from L1 to L3 using L2
.catch java/lang/SecurityException from L6 to L7 using L4
.catch com/google/android/vending/licensing/util/Base64DecoderException from L6 to L7 using L5
.catch all from L6 to L7 using L2
.catch java/lang/SecurityException from L8 to L9 using L4
.catch com/google/android/vending/licensing/util/Base64DecoderException from L8 to L9 using L5
.catch all from L8 to L9 using L2
.catch java/lang/SecurityException from L10 to L11 using L4
.catch com/google/android/vending/licensing/util/Base64DecoderException from L10 to L11 using L5
.catch all from L10 to L11 using L2
.catch java/lang/SecurityException from L12 to L13 using L4
.catch com/google/android/vending/licensing/util/Base64DecoderException from L12 to L13 using L5
.catch all from L12 to L13 using L2
.catch all from L14 to L15 using L2
.catch all from L16 to L17 using L2
.catch all from L18 to L19 using L2
aload 0
monitorenter
L0:
new com/google/android/vending/licensing/LicenseValidator
dup
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mPolicy Lcom/google/android/vending/licensing/Policy;
new com/google/android/vending/licensing/NullDeviceLimiter
dup
invokespecial com/google/android/vending/licensing/NullDeviceLimiter/<init>()V
aload 1
aload 0
invokespecial com/google/android/vending/licensing/LicenseChecker/generateNonce()I
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mPackageName Ljava/lang/String;
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mVersionCode Ljava/lang/String;
invokespecial com/google/android/vending/licensing/LicenseValidator/<init>(Lcom/google/android/vending/licensing/Policy;Lcom/google/android/vending/licensing/DeviceLimiter;Lcom/google/android/vending/licensing/LicenseCheckerCallback;ILjava/lang/String;Ljava/lang/String;)V
astore 4
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mService Lcom/google/android/vending/licensing/ILicensingService;
ifnonnull L18
ldc "LicenseChecker"
ldc "Binding to licensing service."
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
new android/content/Intent
dup
new java/lang/String
dup
ldc "Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U="
invokestatic com/google/android/vending/licensing/util/Base64/decode(Ljava/lang/String;)[B
invokespecial java/lang/String/<init>([B)V
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 5
aload 5
ldc "com.android.vending"
invokevirtual android/content/Intent/setPackage(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 5
ldc "xexe"
ldc "lp"
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
L3:
iconst_0
istore 3
iconst_0
istore 2
L6:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 5
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifne L20
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 5
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 5
L7:
iload 2
istore 3
L8:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L20
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/content/pm/ResolveInfo
astore 6
aload 6
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
ifnull L7
aload 6
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
ldc "com.android.vending"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L7
new android/content/ComponentName
dup
aload 6
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
aload 6
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 6
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 7
aload 7
aload 6
invokevirtual android/content/Intent/setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
pop
aload 7
ldc "xexe"
ldc "lp"
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
aload 7
aload 0
iconst_1
invokevirtual android/content/Context/bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
istore 2
L9:
goto L7
L20:
iload 3
ifeq L12
L10:
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mPendingChecks Ljava/util/Queue;
aload 4
invokeinterface java/util/Queue/offer(Ljava/lang/Object;)Z 1
pop
L11:
aload 0
monitorexit
return
L12:
ldc "LicenseChecker"
ldc "Could not bind to service."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
aload 4
invokespecial com/google/android/vending/licensing/LicenseChecker/handleServiceConnectionError(Lcom/google/android/vending/licensing/LicenseValidator;)V
L13:
goto L11
L4:
astore 4
L14:
aload 1
bipush 6
invokeinterface com/google/android/vending/licensing/LicenseCheckerCallback/applicationError(I)V 1
L15:
goto L11
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L5:
astore 1
L16:
aload 1
invokevirtual com/google/android/vending/licensing/util/Base64DecoderException/printStackTrace()V
L17:
goto L11
L18:
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mPendingChecks Ljava/util/Queue;
aload 4
invokeinterface java/util/Queue/offer(Ljava/lang/Object;)Z 1
pop
aload 0
invokespecial com/google/android/vending/licensing/LicenseChecker/runChecks()V
L19:
goto L11
.limit locals 8
.limit stack 8
.end method

.method public onDestroy()V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
invokespecial com/google/android/vending/licensing/LicenseChecker/cleanupService()V
aload 0
getfield com/google/android/vending/licensing/LicenseChecker/mHandler Landroid/os/Handler;
invokevirtual android/os/Handler/getLooper()Landroid/os/Looper;
invokevirtual android/os/Looper/quit()V
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 1
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
aload 2
invokestatic com/google/android/vending/licensing/ILicensingService$Stub/asInterface(Landroid/os/IBinder;)Lcom/google/android/vending/licensing/ILicensingService;
putfield com/google/android/vending/licensing/LicenseChecker/mService Lcom/google/android/vending/licensing/ILicensingService;
aload 0
invokespecial com/google/android/vending/licensing/LicenseChecker/runChecks()V
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
ldc "LicenseChecker"
ldc "Service unexpectedly disconnected."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
aconst_null
putfield com/google/android/vending/licensing/LicenseChecker/mService Lcom/google/android/vending/licensing/ILicensingService;
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method
