.bytecode 50.0
.class public synchronized com/google/android/finsky/billing/iab/BuyMarketActivity
.super android/app/Activity
.inner class inner com/google/android/finsky/billing/iab/BuyMarketActivity$1
.inner class inner com/google/android/finsky/billing/iab/BuyMarketActivity$2

.field public static final 'BUY_INTENT' Ljava/lang/String; = "org.billinghack.BUY"

.field public static final 'EXTRA_DEV_PAYLOAD' Ljava/lang/String; = "payload"

.field public static final 'EXTRA_PACKAGENAME' Ljava/lang/String; = "packageName"

.field public static final 'EXTRA_PRODUCT_ID' Ljava/lang/String; = "product"

.field public static final 'TAG' Ljava/lang/String; = "BillingHack"

.field 'check' Landroid/widget/CheckBox;

.field 'check2' Landroid/widget/CheckBox;

.field 'check3' Landroid/widget/CheckBox;

.field public 'context' Lcom/google/android/finsky/billing/iab/BuyMarketActivity;

.field 'pData' Ljava/lang/String;

.field public 'packageName' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial android/app/Activity/<init>()V
aload 0
aconst_null
putfield com/google/android/finsky/billing/iab/BuyMarketActivity/context Lcom/google/android/finsky/billing/iab/BuyMarketActivity;
aload 0
ldc ""
putfield com/google/android/finsky/billing/iab/BuyMarketActivity/packageName Ljava/lang/String;
aload 0
aconst_null
putfield com/google/android/finsky/billing/iab/BuyMarketActivity/check Landroid/widget/CheckBox;
aload 0
aconst_null
putfield com/google/android/finsky/billing/iab/BuyMarketActivity/check2 Landroid/widget/CheckBox;
aload 0
aconst_null
putfield com/google/android/finsky/billing/iab/BuyMarketActivity/check3 Landroid/widget/CheckBox;
return
.limit locals 1
.limit stack 2
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
aload 1
getfield android/content/res/Configuration/orientation I
istore 2
iload 2
iconst_2
if_icmpne L0
aload 0
iconst_0
invokevirtual com/google/android/finsky/billing/iab/BuyMarketActivity/setRequestedOrientation(I)V
L0:
iload 2
iconst_1
if_icmpne L1
aload 0
iconst_1
invokevirtual com/google/android/finsky/billing/iab/BuyMarketActivity/setRequestedOrientation(I)V
L1:
aload 0
aload 1
invokespecial android/app/Activity/onConfigurationChanged(Landroid/content/res/Configuration;)V
return
.limit locals 3
.limit stack 2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/app/Activity/onCreate(Landroid/os/Bundle;)V
aload 0
aload 0
putfield com/google/android/finsky/billing/iab/BuyMarketActivity/context Lcom/google/android/finsky/billing/iab/BuyMarketActivity;
ldc "BillingHack"
ldc "Buy intent!"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 1
ifnull L0
aload 0
aload 1
ldc "packageName"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/BuyMarketActivity/packageName Ljava/lang/String;
L1:
aload 0
invokevirtual com/google/android/finsky/billing/iab/BuyMarketActivity/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "autorepeat"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 1
ifnull L2
aload 1
ldc "1"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "config"
iconst_4
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "UnSign"
iconst_0
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L4:
new android/content/Intent
dup
ldc "com.android.vending.billing.IN_APP_NOTIFY"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 1
aload 1
aload 0
getfield com/google/android/finsky/billing/iab/BuyMarketActivity/packageName Ljava/lang/String;
invokevirtual android/content/Intent/setPackage(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
ldc "notification_id"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc2_w 1000000000000000000L
ldc2_w 9223372036854775807L
invokestatic com/chelpus/Utils/getRandom(JJ)J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
aload 1
invokevirtual com/google/android/finsky/billing/iab/BuyMarketActivity/sendBroadcast(Landroid/content/Intent;)V
aload 0
invokevirtual com/google/android/finsky/billing/iab/BuyMarketActivity/finish()V
L2:
aload 0
ldc_w 2130968594
invokevirtual com/google/android/finsky/billing/iab/BuyMarketActivity/setContentView(I)V
aload 0
ldc_w 2131558407
invokevirtual com/google/android/finsky/billing/iab/BuyMarketActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
astore 1
aload 0
ldc_w 2131558408
invokevirtual com/google/android/finsky/billing/iab/BuyMarketActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
astore 2
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "config"
iconst_4
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "UnSign"
iconst_0
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "config"
iconst_4
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "SavePurchase"
iconst_0
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
aload 0
ldc_w 2131558483
invokevirtual com/google/android/finsky/billing/iab/BuyMarketActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
putfield com/google/android/finsky/billing/iab/BuyMarketActivity/check Landroid/widget/CheckBox;
aload 0
aload 0
ldc_w 2131558484
invokevirtual com/google/android/finsky/billing/iab/BuyMarketActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
putfield com/google/android/finsky/billing/iab/BuyMarketActivity/check2 Landroid/widget/CheckBox;
aload 0
aload 0
ldc_w 2131558485
invokevirtual com/google/android/finsky/billing/iab/BuyMarketActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
putfield com/google/android/finsky/billing/iab/BuyMarketActivity/check3 Landroid/widget/CheckBox;
aload 0
ldc_w 2131558471
invokevirtual com/google/android/finsky/billing/iab/BuyMarketActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 3
aload 3
ldc_w 2131165222
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc_w 2131165223
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/append(Ljava/lang/CharSequence;)V
aload 0
getfield com/google/android/finsky/billing/iab/BuyMarketActivity/check Landroid/widget/CheckBox;
iconst_0
invokevirtual android/widget/CheckBox/setChecked(Z)V
aload 0
getfield com/google/android/finsky/billing/iab/BuyMarketActivity/check2 Landroid/widget/CheckBox;
iconst_0
invokevirtual android/widget/CheckBox/setChecked(Z)V
aload 0
getfield com/google/android/finsky/billing/iab/BuyMarketActivity/check3 Landroid/widget/CheckBox;
iconst_0
invokevirtual android/widget/CheckBox/setChecked(Z)V
invokestatic com/chelpus/Utils/checkCoreJarPatch11()Z
ifne L5
aload 0
getfield com/google/android/finsky/billing/iab/BuyMarketActivity/packageName Ljava/lang/String;
aload 0
invokestatic com/chelpus/Utils/isRebuildedOrOdex(Ljava/lang/String;Landroid/content/Context;)Z
ifne L5
aload 0
getfield com/google/android/finsky/billing/iab/BuyMarketActivity/check Landroid/widget/CheckBox;
iconst_1
invokevirtual android/widget/CheckBox/setChecked(Z)V
L5:
aload 1
new com/google/android/finsky/billing/iab/BuyMarketActivity$1
dup
aload 0
invokespecial com/google/android/finsky/billing/iab/BuyMarketActivity$1/<init>(Lcom/google/android/finsky/billing/iab/BuyMarketActivity;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
new com/google/android/finsky/billing/iab/BuyMarketActivity$2
dup
aload 0
invokespecial com/google/android/finsky/billing/iab/BuyMarketActivity$2/<init>(Lcom/google/android/finsky/billing/iab/BuyMarketActivity;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
L0:
aload 0
aload 0
invokevirtual com/google/android/finsky/billing/iab/BuyMarketActivity/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "packageName"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/BuyMarketActivity/packageName Ljava/lang/String;
goto L1
L3:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "config"
iconst_4
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "UnSign"
iconst_1
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
goto L4
.limit locals 4
.limit stack 7
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "load instance"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
aload 1
ldc "packageName"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/BuyMarketActivity/packageName Ljava/lang/String;
aload 0
aload 1
invokespecial android/app/Activity/onRestoreInstanceState(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 3
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "save instance"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
ldc "packageName"
aload 0
getfield com/google/android/finsky/billing/iab/BuyMarketActivity/packageName Ljava/lang/String;
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 0
aload 1
invokespecial android/app/Activity/onSaveInstanceState(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 3
.end method
