.bytecode 50.0
.class public synchronized com/google/android/finsky/services/LicensingService
.super android/app/Service
.inner class inner com/google/android/finsky/services/LicensingService$1
.inner class inner com/google/android/finsky/services/LicensingService$1$1
.inner class inner com/google/android/finsky/services/LicensingService$2

.field static 'mServiceConn' Landroid/content/ServiceConnection;

.field private final 'mBinder' Lcom/android/vending/licensing/ILicensingService$Stub;

.field public 'mChecker' Lcom/google/android/vending/licensing/LicenseChecker;

.field private 'mListener' Lcom/android/vending/licensing/ILicenseResultListener;

.field 'mService' Lcom/android/vending/licensing/ILicensingService;

.field 'mSetupDone' Z

.field public 'responseCode' I

.field public 'signature' Ljava/lang/String;

.field public 'signedData' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial android/app/Service/<init>()V
aload 0
iconst_0
putfield com/google/android/finsky/services/LicensingService/mSetupDone Z
aload 0
sipush 255
putfield com/google/android/finsky/services/LicensingService/responseCode I
aload 0
ldc ""
putfield com/google/android/finsky/services/LicensingService/signature Ljava/lang/String;
aload 0
ldc ""
putfield com/google/android/finsky/services/LicensingService/signedData Ljava/lang/String;
aload 0
new com/google/android/finsky/services/LicensingService$2
dup
aload 0
invokespecial com/google/android/finsky/services/LicensingService$2/<init>(Lcom/google/android/finsky/services/LicensingService;)V
putfield com/google/android/finsky/services/LicensingService/mBinder Lcom/android/vending/licensing/ILicensingService$Stub;
return
.limit locals 1
.limit stack 4
.end method

.method private cleanupService()V
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
aload 0
getfield com/google/android/finsky/services/LicensingService/mService Lcom/android/vending/licensing/ILicensingService;
ifnull L3
L0:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
getstatic com/google/android/finsky/services/LicensingService/mServiceConn Landroid/content/ServiceConnection;
invokevirtual android/content/Context/unbindService(Landroid/content/ServiceConnection;)V
L1:
aload 0
aconst_null
putfield com/google/android/finsky/services/LicensingService/mService Lcom/android/vending/licensing/ILicensingService;
L3:
return
L2:
astore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method public connectToLicensing(JLjava/lang/String;)V
.catch java/lang/SecurityException from L0 to L1 using L2
.catch com/google/android/vending/licensing/util/Base64DecoderException from L0 to L1 using L3
.catch java/lang/SecurityException from L4 to L5 using L2
.catch com/google/android/vending/licensing/util/Base64DecoderException from L4 to L5 using L3
.catch java/lang/SecurityException from L5 to L6 using L2
.catch com/google/android/vending/licensing/util/Base64DecoderException from L5 to L6 using L3
.catch java/lang/SecurityException from L7 to L8 using L2
.catch com/google/android/vending/licensing/util/Base64DecoderException from L7 to L8 using L3
.catch java/lang/SecurityException from L9 to L10 using L2
.catch com/google/android/vending/licensing/util/Base64DecoderException from L9 to L10 using L3
.catch java/lang/SecurityException from L11 to L12 using L2
.catch com/google/android/vending/licensing/util/Base64DecoderException from L11 to L12 using L3
.catch java/lang/SecurityException from L13 to L14 using L2
.catch com/google/android/vending/licensing/util/Base64DecoderException from L13 to L14 using L3
new com/google/android/finsky/services/LicensingService$1
dup
aload 0
aload 3
lload 1
invokespecial com/google/android/finsky/services/LicensingService$1/<init>(Lcom/google/android/finsky/services/LicensingService;Ljava/lang/String;J)V
putstatic com/google/android/finsky/services/LicensingService/mServiceConn Landroid/content/ServiceConnection;
aload 0
getfield com/google/android/finsky/services/LicensingService/mService Lcom/android/vending/licensing/ILicensingService;
ifnonnull L15
L0:
new android/content/Intent
dup
new java/lang/String
dup
ldc "Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U="
invokestatic com/google/android/vending/licensing/util/Base64/decode(Ljava/lang/String;)[B
invokespecial java/lang/String/<init>([B)V
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 3
aload 3
ldc "com.android.vending"
invokevirtual android/content/Intent/setPackage(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 3
ldc "xexe"
ldc "lp"
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
L1:
iconst_0
istore 6
iconst_0
istore 5
L4:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 3
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifeq L5
new com/chelpus/Utils
dup
ldc "w"
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
ldc2_w 5000L
invokevirtual com/chelpus/Utils/waitLP(J)V
L5:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 3
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifne L16
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 3
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L6:
iload 5
istore 6
L7:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L16
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/content/pm/ResolveInfo
astore 7
aload 7
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
ifnull L6
aload 7
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
ldc "com.android.vending"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L6
new android/content/ComponentName
dup
aload 7
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
aload 7
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 7
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 8
aload 8
aload 7
invokevirtual android/content/Intent/setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
pop
aload 8
ldc "xexe"
ldc "lp"
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
aload 8
getstatic com/google/android/finsky/services/LicensingService/mServiceConn Landroid/content/ServiceConnection;
iconst_1
invokevirtual android/content/Context/bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
istore 5
L8:
goto L6
L9:
aload 0
getfield com/google/android/finsky/services/LicensingService/mSetupDone Z
ifne L11
new com/chelpus/Utils
dup
ldc "w"
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
ldc2_w 2000L
invokevirtual com/chelpus/Utils/waitLP(J)V
L10:
iload 4
bipush 10
if_icmple L17
L11:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Stop licensing"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
invokespecial com/google/android/finsky/services/LicensingService/cleanupService()V
L12:
return
L13:
aload 0
invokespecial com/google/android/finsky/services/LicensingService/cleanupService()V
L14:
return
L2:
astore 3
aload 0
invokespecial com/google/android/finsky/services/LicensingService/cleanupService()V
return
L3:
astore 3
aload 3
invokevirtual com/google/android/vending/licensing/util/Base64DecoderException/printStackTrace()V
aload 0
invokespecial com/google/android/finsky/services/LicensingService/cleanupService()V
return
L16:
iload 6
ifeq L13
iconst_0
istore 4
goto L9
L15:
return
L17:
iload 4
iconst_1
iadd
istore 4
goto L9
.limit locals 9
.limit stack 6
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
aload 0
getfield com/google/android/finsky/services/LicensingService/mBinder Lcom/android/vending/licensing/ILicensingService$Stub;
areturn
.limit locals 2
.limit stack 1
.end method
